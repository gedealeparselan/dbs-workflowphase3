﻿using System.Web;
using System.Web.Mvc;
using DBS.WebAPI.Filters;

namespace DBS.WebAPI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new ModelValidationFilter());
        }
    }
}
