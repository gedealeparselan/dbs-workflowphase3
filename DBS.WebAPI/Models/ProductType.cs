﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("ProductType")]
    public class ProductTypeModel
    {
        /// <summary>
        /// Statement Letter ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Statement Letter Name.
        /// </summary>
        //[MaxLength(50, ErrorMessage = "Statement Letter Name is must be in {1} characters.")]
        public string Code { get; set; }

        public string Description { get; set; }

        public bool IsFlowValas { get; set; }
        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class ProductTypeRow : ProductTypeModel
    {
        public int RowID { get; set; }

    }

    public class ProductTypeGrid : Grid
    {
        public IList<ProductTypeRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class ProductTypeFilter : Filter { }
    #endregion

    #region Interface
    public interface IProductTypeRepository : IDisposable
    {
        bool GetProductTypeByID(int id, ref ProductTypeModel ProductType, ref string message);
        bool GetProductType(ref IList<ProductTypeModel> ProductType, int limit, int index, ref string message);
        bool GetProductType(ref IList<ProductTypeModel> ProductType, ref string message);
        bool GetProductType(int page, int size, IList<ProductTypeFilter> filters, string sortColumn, string sortOrder, ref ProductTypeGrid ProductType, ref string message);
        bool GetProductType(ProductTypeFilter filter, ref IList<ProductTypeModel> ProductType, ref string message);
        bool GetProductType(string key, int limit, ref IList<ProductTypeModel> ProductType, ref string message);
        bool AddProductType(ProductTypeModel ProductType, ref string message);
        bool UpdateProductType(int id, ProductTypeModel ProductType, ref string message);
        bool DeleteProductType(int id, ref string message);
    }
    #endregion

    #region Repository
    public class ProductTypeRepository : IProductTypeRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();


        public bool GetProductType(ref IList<ProductTypeModel> ProductTypes, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    ProductTypes = (from a in context.ProductTypes
                                    where a.IsDeleted.Equals(false)
                                    orderby new { a.ProductTypeCode }
                                    select new ProductTypeModel
                                    {
                                        ID = a.ProductTypeID,
                                        Code = a.ProductTypeCode,
                                        Description = a.ProductTypeDesc,
                                        IsFlowValas = a.IsFlowValas,
                                        LastModifiedBy = a.CreateBy,
                                        LastModifiedDate = a.CreateDate
                                    }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public bool GetProductTypeByID(int id, ref ProductTypeModel ProductType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                ProductType = (from a in context.ProductTypes
                               where a.IsDeleted.Equals(false) && a.ProductTypeID.Equals(id)
                               select new ProductTypeModel
                               {
                                   ID = a.ProductTypeID,
                                   Code = a.ProductTypeCode,
                                   Description = a.ProductTypeDesc,
                                   IsFlowValas = a.IsFlowValas,
                                   LastModifiedDate = a.CreateDate,
                                   LastModifiedBy = a.CreateBy
                               }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetProductType(int page, int size, IList<ProductTypeFilter> filters, string sortColumn, string sortOrder, ref ProductTypeGrid ProductType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                ProductTypeModel filter = new ProductTypeModel();
                if (filters != null)
                {
                    filter.Code = filters.Where(a => a.Field.Equals("Code")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    //filter.IsFlowValas = filters.Where(a => a.Field.Equals("IsFlowValas")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.ProductTypes
                                let isCode = !string.IsNullOrEmpty(filter.Code)
                                let isDescription = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isCode ? a.ProductTypeCode.Contains(filter.Code) : true) &&
                                    (isDescription ? a.ProductTypeDesc.Contains(filter.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new ProductTypeModel
                                {
                                    ID = a.ProductTypeID,
                                    Code = a.ProductTypeCode,
                                    Description = a.ProductTypeDesc,
                                    IsFlowValas = a.IsFlowValas,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    ProductType.Page = page;
                    ProductType.Size = size;
                    ProductType.Total = data.Count();
                    ProductType.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    ProductType.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new ProductTypeRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Code = a.Code,
                            Description = a.Description,
                            IsFlowValas = a.IsFlowValas,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetProductType(ProductFilter filter, ref IList<ProductTypeModel> productType, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool AddProductType(ProductTypeModel ProductType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.ProductTypes.Where(a => a.ProductTypeCode.Equals(ProductType.Code) && a.IsDeleted.Equals(false)).Any())
                {
                    context.ProductTypes.Add(new Entity.ProductType()
                    {
                        ProductTypeCode = ProductType.Code,
                        ProductTypeDesc = ProductType.Description,
                        IsFlowValas = ProductType.IsFlowValas,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Product data for Product Type code {0} is already exist.", ProductType.Code);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateProductType(int id, ProductTypeModel Product, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.ProductType data = context.ProductTypes.Where(a => a.ProductTypeID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.Products.Where(a => a.ProductID != Product.ID && a.ProductCode.Equals(Product.Code) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Product Type Code {0} is exist.", Product.Code);
                    }
                    else
                    {
                        data.ProductTypeCode = Product.Code;
                        data.ProductTypeDesc = Product.Description;
                        data.IsFlowValas = Product.IsFlowValas;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Product data for Product Type ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteProductType(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.ProductType data = context.ProductTypes.Where(a => a.ProductTypeID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Product data for Product Type ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


        public bool GetProductType(ref IList<ProductTypeModel> ProductTypes, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    ProductTypes = (from a in context.ProductTypes
                               where a.IsDeleted.Equals(false) 
                               orderby new { a.ProductTypeID, a.ProductTypeCode }
                               select new ProductTypeModel
                               {
                                   ID = a.ProductTypeID,
                                   Code = a.ProductTypeCode,
                                   Description = a.ProductTypeDesc,
                                   IsFlowValas = a.IsFlowValas,
                                   LastModifiedDate = a.CreateDate,
                                   LastModifiedBy = a.CreateBy
                               }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


        public bool GetProductType(ProductTypeFilter filter, ref IList<ProductTypeModel> ProductType, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetProductType(string key, int limit, ref IList<ProductTypeModel> Product, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Product = (from a in context.ProductTypes
                           where a.IsDeleted.Equals(false) &&
                                (a.ProductTypeDesc.Equals(key) || a.ProductTypeCode.Contains(key)) 
                           select new ProductTypeModel
                           {
                               ID = a.ProductTypeID,
                               Code = a.ProductTypeCode,
                               Description = a.ProductTypeDesc,
                               IsFlowValas = a.IsFlowValas,
                               LastModifiedDate = a.CreateDate,
                               LastModifiedBy = a.CreateBy
                           }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
    }
    #endregion
}