﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class CustomerUnderlyingController : ApiController
    {
        private string message = string.Empty;

        private readonly ICustomerUnderlyingRepository repository = null;

        public CustomerUnderlyingController()
        {
            this.repository = new CustomerUnderlyingRepository();
        }

        public CustomerUnderlyingController(ICustomerUnderlyingRepository repository)
        {
            this.repository = repository;
        }

        #region Tambahan Agung Draft
        /// <summary>
        /// Get all Underlying.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(CustomerUnderlyingModel))]
        [Route("api/CustomerUnderlyingDraft")]
        [AcceptVerbs("Get")]
        public HttpResponseMessage Underlyings()
        {
            IList<CustomerUnderlyingModel> output = new List<CustomerUnderlyingModel>();

            if (!repository.GetCustomerUnderlying(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerUnderlyingModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Customer Underlying Parameter by Underlying.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(CustomerUnderlyingGrid))]
        [Route("api/CustomerUnderlying/UnderlyingDraft")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetUnderlyingDraft(int? page, int? size, string sort_column, string sort_order, IList<CustomerUnderlyingFilter> filters)
        {
            CustomerUnderlyingGrid output = new CustomerUnderlyingGrid();

            if (!repository.GetCustomerUnderlyingDraft(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerUnderlyingGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Underlying by ID.
        /// </summary>
        /// <param name="id">Role ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(CustomerUnderlyingModel))]
        [Route("api/CustomerUnderlyingDraft/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(string id)
        {
            CustomerUnderlyingModel output = new CustomerUnderlyingModel();

            if (!repository.GetCustomerUnderlyingDraftByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<CustomerUnderlyingModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new  Underlying Parameter by Underlying.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(string))]
        [Route("api/CustomerUnderlying/SaveExcel")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage AddUnderlyingExcel(IList<CustomerUnderlyingDraftModel> CustomerUnderlying)
        {
            var UnderlyingData = new List<DataUnderlying>();

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddCustomerUnderlyingExcelDraft(CustomerUnderlying, ref UnderlyingData, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { UnderlyingData, message });
                }
            }
        }
        #endregion


        /// <summary>
        /// Get Customer Underlyings with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<CustomerUnderlyingModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<CustomerUnderlyingModel> output = new List<CustomerUnderlyingModel>();

            if (!repository.GetCustomerUnderlying(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerUnderlyingModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Customer Underlyings for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<SelectedProformaModel>))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(IList<CustomerUnderlyingFilter> filters)
        {
            IList<SelectedProformaModel> output = new List<SelectedProformaModel>();

            if (!repository.GetCustomerUnderlying(filters, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<SelectedProformaModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find Customer Underlyings using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<CustomerUnderlyingModel>))]
        [HttpGet]
        [Route("api/CustomerUnderlying/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<CustomerUnderlyingModel> output = new List<CustomerUnderlyingModel>();

            if (!repository.GetCustomerUnderlying(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerUnderlyingModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Single value Parameter by Proforma.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(CustomerUnderlyingGrid))]
        [Route("api/CustomerUnderlying/Proforma")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetProforma(int? page, int? size, string sort_column, string sort_order, IList<CustomerUnderlyingFilter> filters)
        {
            CustomerUnderlyingGrid output = new CustomerUnderlyingGrid();

            if (!repository.GetCustomerUnderlyingProforma(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerUnderlyingGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Single value Parameter by Bulk Underlying.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(CustomerUnderlyingGrid))]
        [Route("api/CustomerUnderlying/Bulk")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetBulk(int? page, int? size, string sort_column, string sort_order, IList<CustomerUnderlyingFilter> filters)
        {
            CustomerUnderlyingGrid output = new CustomerUnderlyingGrid();

            if (!repository.GetCustomerBulkUnderlying(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerUnderlyingGrid>(HttpStatusCode.OK, output);
            }
        }
        /// <summary>
        /// Get Single value Parameter by Bulk Underlying.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(CustomerUnderlyingGrid))]
        [Route("api/CustomerUnderlying/BulkJoint")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetBulkJoint(int? page, int? size, string sort_column, string sort_order, IList<CustomerUnderlyingFilter> filters)
        {
            CustomerUnderlyingGrid output = new CustomerUnderlyingGrid();

            if (!repository.GetCustomerBulkUnderlyingJoint(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerUnderlyingGrid>(HttpStatusCode.OK, output);
            }
        }
        /// <summary>
        /// Get Single value Parameter by Proforma.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(CustomerUnderlyingGrid))]
        [Route("api/CustomerUnderlying/Proforma/Transaction/{id}")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetProforma(long id, int? page, int? size, string sort_column, string sort_order, IList<CustomerUnderlyingFilter> filters)
        {
            CustomerUnderlyingGrid output = new CustomerUnderlyingGrid();

            if (!repository.GetCustomerUnderlyingProforma(id, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerUnderlyingGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Selected value ID Proforma.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<SelectedProformaModel>))]
        [Route("api/CustomerUnderlying/SelectedProformaID")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetSelectedProforma(IList<CustomerUnderlyingFilter> filters)
        {
            IList<SelectedProformaModel> output = new List<SelectedProformaModel>();

            if (!repository.GetCustomerUnderlying(filters, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<SelectedProformaModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Selected value ID Proforma.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<SelectedProformaModel>))]
        [Route("api/CustomerUnderlying/SelectedBulkID")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetSelectedBulk(IList<CustomerUnderlyingFilter> filters)
        {
            IList<SelectedBulkModel> output = new List<SelectedBulkModel>();

            if (!repository.GetCustomerBulkUnderlying(filters, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<SelectedBulkModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Selected value Utilizee ID.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<SelectedUtilizeModel>))]
        [Route("api/CustomerUnderlying/SelectedUtilizeID")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetSelectedUtilize(IList<CustomerUnderlyingFilter> filters)
        {
            IList<SelectedUtilizeModel> output = new List<SelectedUtilizeModel>();

            if (!repository.GetCustomerUnderlyingUtilizeID(filters, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<SelectedUtilizeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Customer Underlying Parameter by Underlying.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(CustomerUnderlyingGrid))]
        [Route("api/CustomerUnderlying/Underlying")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetUnderlying(int? page, int? size, string sort_column, string sort_order, IList<CustomerUnderlyingFilter> filters)
        {
            CustomerUnderlyingGrid output = new CustomerUnderlyingGrid();

            if (!repository.GetCustomerUnderlying(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerUnderlyingGrid>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(CustomerUnderlyingGrid))]
        [Route("api/CustomerUnderlying/UnderlyingWithFilterExpiredDate")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetUnderlyingWithFilterExpiredDate(int? page, int? size, string sort_column, string sort_order, IList<CustomerUnderlyingFilter> filters)
        {
            CustomerUnderlyingGrid output = new CustomerUnderlyingGrid();

            if (!repository.GetCustomerUnderlyingWithFilterExpiredDate(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerUnderlyingGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Customer Underlying Parameter by Underlying by transaction ID.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(CustomerUnderlyingGrid))]
        [Route("api/CustomerUnderlying/Transaction/{id}")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetUnderlying(long id, int? page, int? size, string sort_column, string sort_order, IList<CustomerUnderlyingFilter> filters)
        {

            CustomerUnderlyingGrid output = new CustomerUnderlyingGrid();

            //return Request.CreateResponse<string>(HttpStatusCode.BadRequest, "back return");

            if (!repository.GetCustomerUnderlying(id, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerUnderlyingGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Customer Underlying Parameter by Underlying by transaction ID.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(CustomerUnderlyingGrid))]
        [Route("api/CustomerUnderlying/UnionTransactionDeal/{id}")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetUnderlyingByDealID(long id, int? page, int? size, string sort_column, string sort_order, IList<CustomerUnderlyingFilter> filters)
        {

            CustomerUnderlyingGrid output = new CustomerUnderlyingGrid();

            if (!repository.GetCustomerUnderlyingUnionDeal(id, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerUnderlyingGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Customer Underlying Parameter by Underlying by transaction ID.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(CustomerUnderlyingGrid))]
        [Route("api/CustomerUnderlying/TransactionDeal/{id}")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetUnderlyingDeal(long id, int? page, int? size, string sort_column, string sort_order, IList<CustomerUnderlyingFilter> filters)
        {

            CustomerUnderlyingGrid output = new CustomerUnderlyingGrid();

            if (!repository.GetCustomerUnderlyingByID(id, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerUnderlyingGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Customer Underlying by Attach.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(CustomerUnderlyingGrid))]
        [Route("api/CustomerUnderlying/Attach")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetAttach(int? page, int? size, string sort_column, string sort_order, IList<CustomerUnderlyingFilter> filters)
        {
            CustomerUnderlyingGrid output = new CustomerUnderlyingGrid();

            if (!repository.GetCustomerUnderlyingAttach(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerUnderlyingGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Customer Underlying by Attach.
        /// </summary>
        /// <param name="id">Customer Underlying ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(CustomerUnderlyingGrid))]
        [Route("api/CustomerUnderlying/Attach/{id}")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetAttach(long id, int? page, int? size, string sort_column, string sort_order, IList<CustomerUnderlyingFilter> filters)
        {
            CustomerUnderlyingGrid output = new CustomerUnderlyingGrid();

            if (!repository.GetCustomerUnderlyingAttachByTransactionID(id, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerUnderlyingGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Customer Underlying by ID.
        /// </summary>
        /// <param name="id">Customer Underlying ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(CustomerUnderlyingModel))]
        [Route("api/CustomerUnderlying/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            CustomerUnderlyingModel output = new CustomerUnderlyingModel();

            if (!repository.GetCustomerUnderlyingByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<CustomerUnderlyingModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Customer Underlying.
        /// </summary>
        /// <param name="id">Customer Underlying ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [Route("api/CustomerUnderlying/Attachment")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetUnderlying(string cif, IList<long> filters)
        {
            List<CustomerUnderlyingModel> output = new List<CustomerUnderlyingModel>();
            if (filters.Count > 0)
            {
                if (!repository.GetCustomerUnderlyingAttach(filters, cif, ref output, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
                }
                else
                {
                    if (output == null)
                        return Request.CreateResponse(HttpStatusCode.NoContent);
                    else
                        return Request.CreateResponse<List<CustomerUnderlyingModel>>(HttpStatusCode.OK, output);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
        }

        /// <summary>
        /// Create a new  Underlying Parameter by Underlying.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(string))]
        [Route("api/CustomerUnderlying/Save")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage AddUnderlying(CustomerUnderlyingModel CustomerUnderlying)
        {
            SelectUtillizeModel SelecUtilize = new SelectUtillizeModel();
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddCustomerUnderlying(CustomerUnderlying, ref SelecUtilize, ref message))
                //if (!repository.AddCustomerUnderlyingDraft(CustomerUnderlying, ref UnderlyingDraftID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new
                    {
                        SelecUtilize = SelecUtilize,
                        Message = "New Customer Underlying data has been created."
                        //Message = "Workflow New Customer Underlying data has been created."
                    });
                }
            }
        }
        /// <summary>
        /// Create a new  Underlying Parameter by Underlying.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(string))]
        [Route("api/CustomerUnderlying/SaveDraft")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage AddUnderlyingDraft(CustomerUnderlyingModel CustomerUnderlying)
        {
            long UnderlyingDraftID = 0;
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {

                if (!repository.AddCustomerUnderlyingDraft(CustomerUnderlying, ref UnderlyingDraftID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new
                    {
                        UnderlyingDraftID = UnderlyingDraftID,
                        Message = "Workflow New Customer Underlying data has been created."
                    });
                }
            }
        }
        [ResponseType(typeof(string))]
        [Route("api/CustomerUnderlying/UpdateDraft/{id}")]
        [AcceptVerbs("PUT")]
        public HttpResponseMessage UpdateUnderlyingDraft(long id, CustomerUnderlyingModel CustomerUnderlying)
        {

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {

                if (!repository.UpdateCustomerUnderlyingDraftSubmit(id, CustomerUnderlying, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new
                    {

                        Message = "Workflow New Customer Underlying data has been updated."
                    });
                }
            }
        }
        /// <summary>
        /// Modify existing Customer Underlying.
        /// </summary>
        /// <param name="id">ID of CustomerUnderlying to be modify.</param>
        /// <param name="CustomerUnderlying">Customer Underlying data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/CustomerUnderlying/{id}")]
        // [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, CustomerUnderlyingModel CustomerUnderlying)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateCustomerUnderlying(id, CustomerUnderlying, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Customer Underlying data has been updated." });
                }
            }
        }

        /// <summary>
        /// Modify existing Customer Underlying.
        /// </summary>
        /// <param name="id">ID of CustomerUnderlying to be modify.</param>
        /// <param name="CustomerUnderlying">Customer Underlying data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/CustomerUnderlying/Draft/{id}")]
        // [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage PutDraft(int id, CustomerUnderlyingModel CustomerUnderlying)
        {
            long UnderlyingDraftID = 0;
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateCustomerUnderlyingDraft(id, CustomerUnderlying, ref UnderlyingDraftID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        UnderlyingDraftID = UnderlyingDraftID,
                        Message = "Workflow Update Customer Underlying data has been created."
                    });
                }
            }
        }

        /// <summary>
        /// Modify Utilize Column existing Customer Underlying.
        /// </summary>       
        /// <param name="customerUnderlying">Customer Underlyings data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/CustomerUnderlying/UpdateUtilize")]
        public HttpResponseMessage UpdateUtilize(IList<TransUnderlyingModel> customerUnderlying)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateCustomerUnderlyingUtilize(customerUnderlying, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Customer Underlying data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting Customer Underlying.
        /// </summary>
        /// <param name="id">ID of Customer Underlying to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/CustomerUnderlying/{id}")]
        // [Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id, CustomerUnderlyingModel CustomerUnderlyingDraft)
        {
            long UnderlyingDraftID = 0;
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteCustomerUnderlyingDraft(id, CustomerUnderlyingDraft, ref UnderlyingDraftID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        UnderlyingDraftID = UnderlyingDraftID,
                        Message = "Workflow Delete Customer Underlying data has been created."
                    });
                }
            }
        }


        /// <summary>
        /// Remove exisiting Customer Underlying.
        /// </summary>
        /// <param name="id">ID of Customer Underlying to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/CustomerUnderlying/remove/{id}")]
        // [Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage DeleteLazy(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteCustomerUnderlying(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Customer Underlying data has been deleted." });
                }
            }
        }

        /// <summary>
        /// Get Customer Underlying History by Underlying ID.
        /// </summary>
        [ResponseType(typeof(object))]
        [Route("api/CustomerUnderlying/History/{id}")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetUnderlyingHistoryByUnderlyingID(long id, IList<CustomerUnderlyingHistoryModel> customerUnderlyingHistoryModel)
        {
            Object[] output = new object[2];
            IList<CustomerUnderlyingHistoryModel> underlyingHistory = new List<CustomerUnderlyingHistoryModel>();
            IList<CustomerUnderlyingTransactionHistoryModel> underlyingTransactionHistory = new List<CustomerUnderlyingTransactionHistoryModel>();

            if (!repository.GetUnderlyingHistoryByUnderlyingID(id, ref underlyingHistory, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (!repository.GetUnderlyingTransactionHistoryByUnderlyingID(id, ref underlyingTransactionHistory, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
                }
                else
                {
                    output[0] = underlyingHistory;
                    output[1] = underlyingTransactionHistory;

                    return Request.CreateResponse(HttpStatusCode.OK, output);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}