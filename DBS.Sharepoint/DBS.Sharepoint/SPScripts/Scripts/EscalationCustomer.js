﻿var ViewModel = function () {


    var self = this;
    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);

    self.ID = ko.observable("");
    self.CIF = ko.observable("");
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");

    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    // filter
    self.FilterCIFAvailableCustomer = ko.observable("");
    self.FilterCustomerNameAvailableCustomer = ko.observable("");
    self.FilterCIFEscalatedCustomer = ko.observable("");
    self.FilterCustomerNameEscalatedCustomer = ko.observable("");

    // New Data flag
    self.IsNewData = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.AvailableCustomers = ko.observableArray([]);
    self.EscalatedCustomers = ko.observableArray([]);
    self.EscalatedCustomersApproval = ko.observableArray([]);
    self.SelectedAvailableCustomers = ko.observableArray([]);
    self.SelectedEscalatedCustomers = ko.observableArray([]);

    // grid properties
    self.GridPropertiesAvailableCustomer = ko.observable();
    self.GridPropertiesAvailableCustomer(new GridPropertiesModel(GetDataAvailableCustomer));

    // set default sorting
    self.GridPropertiesAvailableCustomer().SortColumn("Name");
    self.GridPropertiesAvailableCustomer().SortOrder("ASC");

    // grid properties
    self.GridPropertiesEscalatedCustomer = ko.observable();
    self.GridPropertiesEscalatedCustomer(new GridPropertiesModel(GetDataEscalatedCustomer));

    // set default sorting
    self.GridPropertiesEscalatedCustomer().SortColumn("Name");
    self.GridPropertiesEscalatedCustomer().SortOrder("ASC");

    // grid properties
    self.GridPropertiesEscalatedCustomerApproval = ko.observable();
    self.GridPropertiesEscalatedCustomerApproval(new GridPropertiesModel(self.GetSelectedRow));

    // set default sorting
    self.GridPropertiesEscalatedCustomerApproval().SortColumn("Name");
    self.GridPropertiesEscalatedCustomerApproval().SortOrder("ASC");


    self.searchParameter = ko.observable("");
    // bind get data function to view
    self.GetDataAvailableCustomer = function () {
        GetDataAvailableCustomer();

    };
    self.GetDataEscalatedCustomer = function () {
        GetDataEscalatedCustomer();
    };
    self.ClearFiltersAvailableCustomer = function () {
        self.FilterCIFAvailableCustomer("");
        self.FilterCustomerNameAvailableCustomer("");
        GetDataAvailableCustomer();
    };
    self.ClearFiltersEscalatedCustomer = function () {
        self.FilterCIFEscalatedCustomer("");
        self.FilterCustomerNameEscalatedCustomer("");
        GetDataEscalatedCustomer();
    };
    self.ClearFiltersEscalatedCustomerApproval = function () {
        self.FilterCIFEscalatedCustomerApproval = "";
        self.FilterCustomerNameEscalatedCustomerApproval = "";
        self.GetSelectedRow();
    };
    //The Object which stored data entered in the observables
    var EscalationCustomer = {
        EscalationCustomer: self.EscalatedCustomers
    };
    self.SelectAvailableCustomer = function (item) {
        var isSelected = false;
        var itemFilter = ko.utils.arrayFirst(self.SelectedAvailableCustomers(), function (data) {
            if (data.CIF == item.CIF) {
                return isSelected = true;
            }
        });

        if (isSelected) {
            removeSelectAvailableCustomer(itemFilter);
        } else {
            self.SelectedAvailableCustomers.push({
                RowID: 0,
                ID: 0,
                CIF: item.CIF,
                Name: item.Name,
                LastModifiedDate: null,
                LastModifiedBy: null
            });
        }

    };

    function removeSelectAvailableCustomer(item) {
        self.SelectedAvailableCustomers.remove(item);
    };
    self.SelectEscalatedCustomer = function (item) {
        var isSelected = false;
        var itemFilter = ko.utils.arrayFirst(self.SelectedEscalatedCustomers(), function (data) {
            if (data.CIF == item.CIF) {
                return isSelected = true;
            }
        });

        if (isSelected) {
            removeSelectEscalatedCustomer(itemFilter);
        } else {
            self.SelectedEscalatedCustomers.push({
                RowID: 0,
                ID: 0,
                CIF: item.CIF,
                Name: item.Name,
                LastModifiedDate: null,
                LastModifiedBy: null
            });
        }

    };

    function removeSelectEscalatedCustomer(item) {
        self.SelectedEscalatedCustomers.remove(item);
    };

    self.IsPermited = function (IsPermitedResult) {

        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    };

    self.pullLeft = function () {
        var sel = self.SelectedEscalatedCustomers();
        var selEscalated = self.EscalatedCustomers();
        var selAvailable = self.AvailableCustomers();
        if (sel.length) {
            for (var i = 0; i < sel.length; i++) {
                var selCat = sel[i];
                self.AvailableCustomers.push(sel[i]);
                self.EscalatedCustomers.remove(function (item) {
                    return item.CIF == selCat.CIF;
                });

            }

        }
        if (selEscalated.length) {
            for (var i = 0; i < selEscalated.length; i++) {
                $('#EscalatedCustomerRowID' + i).text('');
                selEscalated[i].RowID = i + 1;
                $('#EscalatedCustomerRowID' + i).text(i + 1);

            }

        }
        if (selAvailable.length) {
            for (var i = 0; i < selAvailable.length; i++) {
                $('#AvailableCustomerRowID' + i).text('');
                selAvailable[i].RowID = i + 1;
                $('#AvailableCustomerRowID' + i).text(i + 1);
            }
        }

        self.SelectedEscalatedCustomers.removeAll();



    }.bind(this);
    self.pullRight = function () {
        var sel = self.SelectedAvailableCustomers();
        var selEscalated = self.EscalatedCustomers();
        var selAvailable = self.AvailableCustomers();

        if (sel.length) {

            for (var i = 0; i < sel.length; i++) {
                var selCat = sel[i];
                self.EscalatedCustomers.push(sel[i]);
                self.AvailableCustomers.remove(function (item) {
                    return item.CIF == selCat.CIF;
                });

            }

        }
        if (selEscalated.length) {

            for (var i = 0; i < selEscalated.length; i++) {
                $('#EscalatedCustomerRowID' + i).text('');
                selEscalated[i].RowID = i + 1;
                $('#EscalatedCustomerRowID' + i).text(i + 1);

            }

        }
        if (selAvailable.length) {

            for (var i = 0; i < selAvailable.length; i++) {
                $('#AvailableCustomerRowID' + i).text('');
                selAvailable[i].RowID = i + 1;
                $('#AvailableCustomerRowID' + i).text(i + 1);
            }
        }
        self.SelectedAvailableCustomers.removeAll();



    }.bind(this);
    self.UpdateEscalation = function () {
        // validation

        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            //Ajax call to insert the EscalationCustomer
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {

                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.escalationcustomer,
                        data: ko.toJSON(EscalationCustomer.EscalationCustomer),//Convert the Observable Data into JSON
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        contentType: "application/json",
                        success: function (data, textStatus, jqXHR) {
                            // send notification
                            ShowNotification('Create Success', jqXHR.responseText, 'gritter-success', false);

                            // hide current popup window
                            $("#modal-form").modal('hide');

                        },

                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification('An Error Occured', jqXHR.responseText, 'gritter-error', false);
                        }
                    });
                }
            });
        }
    };



    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return ""
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {

        self.IsNewData(false);
        self.EscalatedCustomersApproval(data);

        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');
        if (self.Readonly() == false) {
            AddSlimScroll();
        }


    };


    //insert new
    self.NewData = function () {
        // flag as new Data
        self.IsNewData(true);
        self.Readonly(false);
        // bind empty data
        self.ID(0);
        self.Name('');
    };

    //Function to Read All Customers
    function GetDataAvailableCustomer() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.availablecustomer,
            params: {
                page: self.GridPropertiesAvailableCustomer().Page(),
                size: self.GridPropertiesAvailableCustomer().Size(),
                sort_column: self.GridPropertiesAvailableCustomer().SortColumn(),
                sort_order: self.GridPropertiesAvailableCustomer().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filtersAvailableCustomer = GetFilteredColumnsAvailableCustomer();

        if (filtersAvailableCustomer.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filtersAvailableCustomer);

            Helper.Ajax.Post(options, OnSuccessGetDataAvailableCustomer, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataAvailableCustomer, OnError, OnAlways);
        }
    }
    function GetDataEscalatedCustomer() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.escalationcustomer,
            params: {
                page: self.GridPropertiesEscalatedCustomer().Page(),
                size: self.GridPropertiesEscalatedCustomer().Size(),
                sort_column: self.GridPropertiesEscalatedCustomer().SortColumn(),
                sort_order: self.GridPropertiesEscalatedCustomer().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filtersEscalatedCustomer = GetFilteredColumnEscalatedCustomer();
        if (filtersEscalatedCustomer.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filtersEscalatedCustomer);
            Helper.Ajax.Post(options, OnSuccessGetDataEscalatedCustomer, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataEscalatedCustomer, OnError, OnAlways);
        }
    }



    // Get filtered columns value
    function GetFilteredColumnsAvailableCustomer() {
        // define filter
        var filters = [];

        if (self.FilterCIFAvailableCustomer() != "") filters.push({ Field: 'CIFAvailableCustomer', Value: self.FilterCIFAvailableCustomer });
        if (self.FilterCustomerNameAvailableCustomer() != "") filters.push({ Field: 'CustomerNameAvailableCustomer', Value: self.FilterCustomerNameAvailableCustomer() });


        return filters;
    };

    // Get filtered columns value
    function GetFilteredColumnEscalatedCustomer() {
        // define filter
        var filters = [];

        if (self.FilterCIFEscalatedCustomer() != "") filters.push({ Field: 'CIFEscalatedCustomer', Value: self.FilterCIFEscalatedCustomer });
        if (self.FilterCustomerNameEscalatedCustomer() != "") filters.push({ Field: 'CustomerNameEscalatedCustomer', Value: self.FilterCustomerNameEscalatedCustomer() });

        return filters;
    };

    // On success GetData callback
    function OnSuccessGetDataAvailableCustomer(data, textStatus, jqXHR) {

        if (jqXHR.status = 200) {
            self.AvailableCustomers(data.Rows);

            self.GridPropertiesAvailableCustomer().Page(data['Page']);
            self.GridPropertiesAvailableCustomer().Size(data['Size']);
            self.GridPropertiesAvailableCustomer().Total(data['Total']);
            self.GridPropertiesAvailableCustomer().TotalPages(Math.ceil(self.GridPropertiesAvailableCustomer().Total() / self.GridPropertiesAvailableCustomer().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetDataEscalatedCustomer(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.EscalatedCustomers(data.Rows);
            self.GridPropertiesEscalatedCustomer().Page(data['Page']);
            self.GridPropertiesEscalatedCustomer().Size(data['Size']);
            self.GridPropertiesEscalatedCustomer().Total(data['Total']);
            self.GridPropertiesEscalatedCustomer().TotalPages(Math.ceil(self.GridPropertiesEscalatedCustomer().Total() / self.GridPropertiesEscalatedCustomer().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });
    function AddSlimScroll() {
        $('.slim-scroll').each(function () {
            var $this = $(this);
            $this.slimscroll({
                height: $this.data('height') || 100,
                //width: $this.data('width') || 100,
                railVisible: true,
                alwaysVisible: true,
                color: '#D15B47'
            });
        });

    }

};
