﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("Rank")]
    public class RankModel
    {
        /// <summary>
        /// Rank ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Rank Code.
        /// </summary>
        [MaxLength(25, ErrorMessage = "Rank Code is must be in {1} characters.")]
        public string Code { get; set; }

        public string Name { get; set; }
        /// <summary>
        /// Rank Description.
        /// </summary>
        [MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class RankRow : RankModel
    {
        public int RowID { get; set; }

    }

    public class RankGrid : Grid
    {
        public IList<RankRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class RankFilter : Filter { }
    #endregion

    #region Interface
    public interface IRankRepository : IDisposable
    {
        bool GetRankByID(int id, ref RankModel rank, ref string message);
        bool GetRank(ref IList<RankModel> currencies, int limit, int index, ref string message);
        bool GetRank(int page, int size, IList<RankFilter> filters, string sortColumn, string sortOrder, ref RankGrid rank, ref string message);
        bool GetRank(RankFilter filter, ref IList<RankModel> currencies, ref string message);
        bool GetRank(ref IList<RankModel> rank, ref string message);
        bool GetRank(string key, int limit, ref IList<RankModel> currencies, ref string message);
        bool AddRank(RankModel rank, ref string message);
        bool UpdateRank(int id, RankModel rank, ref string message);
        bool DeleteRank(int id, ref string message);
    }
    #endregion

    #region Repository
    public class RankRepository : IRankRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetRankByID(int id, ref RankModel rank, ref string message)
        {
            bool isSuccess = false;

            try
            {
                rank = (from a in context.Ranks
                        where a.IsDeleted.Equals(false) && a.RankID.Equals(id)
                        select new RankModel
                        {
                            ID = a.RankID,
                            Code = a.RankCode,
                            Description = a.RankDescription,
                            LastModifiedDate = a.CreateDate,
                            LastModifiedBy = a.CreateBy
                        }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRank(ref IList<RankModel> rank, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    rank = (from a in context.Ranks
                            where a.IsDeleted.Equals(false)
                            orderby new { a.RankCode, a.RankDescription }
                            select new RankModel
                            {
                                ID = a.RankID,
                                Code = a.RankCode,
                                Description = a.RankDescription,
                                LastModifiedBy = a.CreateBy,
                                LastModifiedDate = a.CreateDate
                            }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRank(ref IList<RankModel> rank, ref string message)
        {
            bool isSuccess = false;

            try
            {
               
                using (DBSEntities context = new DBSEntities())
                {
                    rank = (from a in context.Ranks
                            where a.IsDeleted.Equals(false)
                            orderby new { a.RankCode, a.RankDescription }
                            select new RankModel
                            {
                                ID = a.RankID,
                                Code = a.RankCode,
                                Name = a.RankCode,
                                Description = a.RankDescription,
                                LastModifiedBy = a.CreateBy,
                                LastModifiedDate = a.CreateDate
                            }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRank(int page, int size, IList<RankFilter> filters, string sortColumn, string sortOrder, ref RankGrid customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                RankModel filter = new RankModel();
                if (filters != null)
                {
                    filter.Code = (string)filters.Where(a => a.Field.Equals("Code")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.Ranks
                                let isCode = !string.IsNullOrEmpty(filter.Code)
                                let isDesc = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isCode ? a.RankCode.Contains(filter.Code) : true) &&
                                    (isDesc ? a.RankDescription.Contains(filter.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new RankModel
                                {
                                    ID = a.RankID,
                                    Code = a.RankCode,
                                    Description = a.RankDescription,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    customer.Page = page;
                    customer.Size = size;
                    customer.Total = data.Count();
                    customer.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    customer.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new RankRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Code = a.Code,
                            Description = a.Description,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRank(RankFilter filter, ref IList<RankModel> customer, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetRank(string key, int limit, ref IList<RankModel> rank, ref string message)
        {
            bool isSuccess = false;

            try
            {
                rank = (from a in context.Ranks
                        where a.IsDeleted.Equals(false) &&
                            a.RankCode.Contains(key) || a.RankDescription.Contains(key)
                        orderby new { a.RankCode, a.RankDescription }
                        select new RankModel
                        {
                            Code = a.RankCode,
                            Description = a.RankDescription
                        }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddRank(RankModel rank, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.Ranks.Where(a => a.RankCode.Equals(rank.Code) && a.IsDeleted.Equals(false)).Any())
                {
                    context.Ranks.Add(new Entity.Rank()
                    {
                        RankCode = rank.Code,
                        RankDescription = rank.Description,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Rank data for rank code {0} is already exist.", rank.Code);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateRank(int id, RankModel rank, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Rank data = context.Ranks.Where(a => a.RankID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.Ranks.Where(a => a.RankID != rank.ID && a.RankCode.Equals(rank.Code) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Rank Code {0} is exist.", rank.Code);
                    }
                    else
                    {
                        data.RankCode = rank.Code;
                        data.RankDescription = rank.Description;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Rank data for Rank ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteRank(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Rank data = context.Ranks.Where(a => a.RankID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Rank data for Rank ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}