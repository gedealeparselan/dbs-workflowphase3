﻿using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;

namespace DBS.WebAPI.Models
{
    #region property
    [ModelName("TBOTMOModel")]
    public class TBOTMOModel
    {
        public long ID { get; set; }
        public long TBOTMOTransactionID { get; set; }
        public long TransactionID { get; set; }
        public string ApplicationID { get; set; }
        public int TBOTMOID { get; set; }
        public string TBOTMOName { get; set; }
        public DateTime ExpectedDateTBO { get; set; }
        public long? TBOSLAID { get; set; }
        public string RemaksTBO { get; set; }
        public DateTime? ExptDate { get; set; }
        public DateTime? ActDate { get; set; }
        public string CIF { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int? Days { get; set; }
        public int DocumentCount { get; set; }
        public IList<DocumentTMOCount> CountDoc { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public DateTime? Modified { get; set; }
        public DocumentTypeTBOTMOModel TypeofDoc { get; set; }
        public DocumentPurposeTBOModel PurposeofDoc { get; set; }
        public DocumentModelTBOTMO FileName { get; set; }
        public string DocumentPath { get; set; }
        public IList<TransactionTBOTMODocumentModel> Documents { get; set; }
        public string Status { get; set; }
        public string CustomerName { get; set; }
        public long TypeDocID { get; set; }
        public int DocumentPurposeID { get; set; }
        public long DocumentID { get; set; }
        public string TBOApplicationID { get; set; }
        public Guid? SPTaskListID { get; set; }
        public int? SPTaskListItemID { get; set; }
        public Guid? WorkflowInstanceID { get; set; }
        public long TransactionDocumentTBOID { get; set; }
    }

    public class TBOTMOModelWF
    {
        public IList<TransactionTBOTMODocumentModelWF> Documents { get; set; }
    }

    public class TransactionTBOTMODocumentModel
    {
        public long ID { get; set; }
        public string ApplicationID { get; set; }
        public string TBOApplicationID { get; set; }
        public string CIF { get; set; }
        public string Name { get; set; }
        public DateTime ActDate { get; set; }
        public DateTime ExptDate { get; set; }
        public int TBOSLAID { get; set; }
        public DocumentTypeModel Type { get; set; }
        public DocumentPurposeTBOModel Purpose { get; set; }
        public string FileName { get; set; }
        public string DocumentPath { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public string Status { get; set; }
    }

    public class TransactionTBOTMOTimelineModel
    {
        public long ApproverID { get; set; }
        public string Activity { get; set; }
        public DateTime? Time { get; set; }
        public string Outcome { get; set; }
        public string UserOrGroup { get; set; }
        public bool IsGroup { get; set; }
        public string DisplayName { get; set; }
        public string Comment { get; set; }
    }

    public class TransactionTBOTMODocumentModelWF
    {
        public long ID { get; set; }
        public string ApplicationID { get; set; }
        public string CIF { get; set; }
        public string Name { get; set; }
        public DateTime ActDate { get; set; }
        public DateTime ExptDate { get; set; }
        public int TBOSLAID { get; set; }
        public DocumentTypeModel Type { get; set; }
        public DocumentPurposeTBOModel Purpose { get; set; }
        public DocumentModelTBOTMO FileName { get; set; }
        public string DocumentPath { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public string Status { get; set; }
        public string TBOApplicationID { get; set; }
        public bool IsChecklistSubmit { get; set; }
    }

    public class DocumentModelTBOTMO
    {
        public long ID { get; set; }
        public DocumentTypeTBOTMOModel Type { get; set; }
        public DocumentPurposeTBOModel Purpose { get; set; }
        public string name { get; set; }
        public string DocumentPath { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }

    public class DocumentTypeTBOTMOModel
    {
        public long ID { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }

    public class DocumentTMOCount
    {
        public int Count { get; set; }
    }
    public class TBOTMOModelRow : TBOTMOModel
    {
        public int RowID { get; set; }
    }

    public class TBOTMOModelGrid : Grid
    {
        public IList<TBOTMOModelRow> Rows { get; set; }

        public IList<TBOTMOModelRow> Data { get; set; }
    }
    #endregion

    #region Filter
    public class TBOTMOModelFilter : Filter
    {

    }
    #endregion

    #region interface
    public interface ITBOTMOModelInterface : IDisposable
    {
        bool GetTBOTMOByID(int id, ref TBOTMOModel TBO, ref string message);
        bool GetTBOTMO(ref IList<TBOTMOModel> TBO, int limit, int index, ref string message);
        bool GetTBOTMO(ref IList<TBOTMOModel> TBO, ref string message);
        bool GetTBOTMO(int page, int size, IList<TBOTMOModelFilter> filters, string sortColumn, string sortOrder, ref TBOTMOModelGrid TBO, ref string message);
        bool GetTBOTMOWF(int page, int size, IList<TBOTMOModelFilter> filters, string sortColumn, string sortOrder, ref TBOTMOModelGrid TBO, ref string message);
        bool GetTBOTMO(TBOTMOModelFilter filter, ref IList<TBOTMOModel> TBO, ref string message);
        bool GetTBOTMO(ref IList<TBOTMOModel> TBO, string cif, ref string message);
        bool GetTBOTMOWF(ref IList<TBOTMOModel> TBO, string cif, ref string message);
        bool GetTransactionTimeline(ref IList<TransactionTBOTMOTimelineModel> timelines, Guid workflowInstanceID, ref string message);
        bool UpdateTBOTMO(TBOTMOModel TBO, ref string message);
        bool SaveTBOTMO(TBOTMOModelWF TBO, ref string message);
        bool GetTransactionTBOTMOByCif(ref IList<TBOTMOModel> TBO, string id, ref string message);
        bool GetTBOTMOWFID(ref IList<TBOTMOModel> TBO, string spUserLoginName, ref string message);
    }
    #endregion

    public class TBOTMOModelRepository : ITBOTMOModelInterface
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetTransactionTimeline(ref IList<TransactionTBOTMOTimelineModel> timelines, Guid workflowInstanceID, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                timelines = (from a in context.SP_GetNintexTaskTimeline(workflowInstanceID)
                             select new TransactionTBOTMOTimelineModel
                             {
                                 ApproverID = a.WorkflowApproverID,
                                 Activity = a.ActivityTitle,
                                 Time = a.ActivityTime,
                                 UserOrGroup = a.UserOrGroup,
                                 IsGroup = a.IsSPGroup.Value,
                                 Outcome = a.Outcome,
                                 DisplayName = a.DisplayName,
                                 Comment = a.Comment
                             }).ToList();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public bool GetTBOTMOByID(int id, ref TBOTMOModel TBO, ref string message)
        {
            bool isSucces = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                    TBO = (from a in context.TBOTMOTransactions
                           join b in context.TransactionDeals on a.TransactionDealsID equals b.TransactionDealID
                           join c in context.Customers on b.CIF equals c.CIF

                           where a.TBOTMOTransactionID.Equals(id)
                           select new TBOTMOModel
                           {
                               ID = a.TBOTMOTransactionID,
                               Name = c.CustomerName,
                               CIF = c.CIF,
                               LastModifiedBy = a.ModifiedDate == null ? a.CreatedBy : a.ModifiedBy,
                               LastModifiedDate = a.ModifiedDate == null ? a.CreatedDate : a.ModifiedDate.Value,
                           }).FirstOrDefault();
                if (TBO != null)
                {
                    TBOTMOModel TBOData = new TBOTMOModel();
                    TBO.DocumentCount = (from a in context.TBOTransactions where a.TransactionID.Equals(id) select a).Count();
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool GetTBOTMO(ref IList<TBOTMOModel> TBO, int limit, int index, ref string message)
        {
            bool isSucces = false;
            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    TBO = (from a in context.TBOTMOTransactions
                           join b in context.TransactionDeals on a.TransactionDealsID equals b.TransactionDealID
                           join c in context.Customers on b.CIF equals c.CIF
                           select new TBOTMOModel
                           {
                               ID = a.TBOTMOTransactionID,
                               Name = c.CustomerName,
                               CIF = c.CIF,
                               TransactionID = b.TransactionDealID,
                               LastModifiedBy = a.ModifiedDate == null ? a.CreatedBy : a.ModifiedBy,
                               LastModifiedDate = a.ModifiedDate == null ? a.CreatedDate : a.ModifiedDate.Value,
                           }).Skip(skip).Take(limit).ToList();
                    TBOTMOModel TBOData = new TBOTMOModel();
                    TBOData.DocumentCount = TBO.Count();
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool GetTBOTMO(ref IList<TBOTMOModel> TBO, ref string message)
        {
            bool isSucces = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    TBO = (from a in context.TBOSLAs
                           select new TBOTMOModel
                           {
                               ID = a.TBOSLAID,
                               Code = a.TBOCode,
                               Name = a.Description,
                               Days = a.Days
                           }).ToList();
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool GetTBOTMO(int page, int size, IList<TBOTMOModelFilter> filters, string sortColumn, string sortOrder, ref TBOTMOModelGrid TBO, ref string message)
        {
            bool isSucces = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                TBOTMOModel filter = new TBOTMOModel();
                if (filters != null)
                {
                    filter.CIF = filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.Name = filters.Where(a => a.Field.Equals("CustomerName")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("ModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("ModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("ModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.TBOTMOTransactions
                                join b in context.TransactionDeals on a.TransactionDealsID equals b.TransactionDealID
                                join c in context.Customers on b.CIF equals c.CIF
                                let isCif = !string.IsNullOrEmpty(filter.CIF)
                                let isName = !string.IsNullOrEmpty(filter.Name)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where
                                (isCif ? c.CIF.Contains(filter.CIF) : true) &&
                                (isName ? c.CustomerName.Contains(filter.Name) : true) &&
                                (isCreateBy ? (a.ModifiedDate == null ? a.CreatedBy.Contains(filter.LastModifiedBy) : a.ModifiedBy.Contains(filter.LastModifiedBy)) : true) &&
                                (isCreateDate ? ((a.ModifiedDate == null ? a.CreatedDate : a.ModifiedDate) > filter.LastModifiedDate.Value && ((a.ModifiedDate == null ? a.CreatedDate : a.ModifiedDate.Value) < maxDate)) : true)
                                && a.Status != "Complete"
                                select new TBOTMOModel
                                {
                                    ID = a.TransactionDealsID.Value,
                                    TBOTMOTransactionID = a.TBOTMOTransactionID,
                                    ApplicationID = b.ApplicationID,
                                    Name = c.CustomerName,
                                    CIF = c.CIF,
                                    CreatedBy = a.CreatedBy,
                                    CreatedDate = a.CreatedDate,
                                    LastModifiedBy = a.ModifiedDate == null ? a.CreatedBy : a.ModifiedBy,
                                    LastModifiedDate = a.ModifiedDate == null ? a.CreatedDate : a.ModifiedDate.Value,
                                });

                    TBO.Page = page;
                    TBO.Size = data.Count();
                    TBO.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    TBO.Rows = data.OrderBy(orderBy).AsEnumerable().Select((a, i) => new TBOTMOModelRow
                    {
                        ID = a.ID,
                        TBOTMOTransactionID = a.TBOTMOTransactionID,
                        ApplicationID = a.ApplicationID,
                        Name = a.Name,
                        CIF = a.CIF,
                        DocumentCount = (from ab in context.TBOTMOTransactions
                                         join bc in context.TransactionDeals on ab.TransactionDealsID equals bc.TransactionDealID
                                         join cd in context.Customers on bc.CIF equals cd.CIF
                                         where cd.CIF == a.CIF && (ab.Status.Contains("Add") || ab.Status.Contains("Revise") || string.IsNullOrEmpty(ab.Status))
                                         select ab.TBOTMODocumentID.Value).Count(),
                        CreatedBy = a.CreatedBy,
                        CreatedDate = a.CreatedDate,
                        LastModifiedBy = a.LastModifiedBy,
                        LastModifiedDate = a.LastModifiedDate,
                    }).Skip(skip).Take(data.Count()).ToList();
                    TBO.Data = TBO.Rows.GroupBy(a => a.CIF).Select(g => g.FirstOrDefault()).ToList();
                    TBO.Total = TBO.Data.Count();
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool GetTBOTMOWF(int page, int size, IList<TBOTMOModelFilter> filters, string sortColumn, string sortOrder, ref TBOTMOModelGrid TBO, ref string message)
        {
            bool isSucces = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                TBOTMOModel filter = new TBOTMOModel();
                if (filters != null)
                {
                    filter.Name = filters.Where(a => a.Field.Equals("CustomerName")).Select(a => a.Value).SingleOrDefault();
                    filter.CIF = filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.Status = filters.Where(a => a.Field.Equals("Status")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("ModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("ModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.TBOTMOTransactions
                                join b in context.TransactionDeals on a.TransactionDealsID equals b.TransactionDealID
                                join c in context.Customers on b.CIF equals c.CIF
                                let isCif = !string.IsNullOrEmpty(filter.CIF)
                                let isName = !string.IsNullOrEmpty(filter.Name)
                                let isStatus = !string.IsNullOrEmpty(filter.Status)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where
                                (isCif ? c.CIF.Contains(filter.CIF) : true) &&
                                (isName ? c.CustomerName.Contains(filter.Name) : true) &&
                                (isStatus ? a.Status.Contains(filter.Status) : true) &&
                                (isCreateBy ? (a.ModifiedDate == null ? a.CreatedBy.Contains(filter.LastModifiedBy) : a.ModifiedBy.Contains(filter.LastModifiedBy)) : true) &&
                                (isCreateDate ? ((a.ModifiedDate == null ? a.CreatedDate : a.ModifiedDate) > filter.LastModifiedDate.Value && ((a.ModifiedDate == null ? a.CreatedDate : a.ModifiedDate.Value) < maxDate)) : true)
                                && a.Status == "Add" && a.WorkflowInstanceID != null
                                select new TBOTMOModel
                                {
                                    TransactionID = b.TransactionDealID,
                                    TBOTMOTransactionID = a.TBOTMOTransactionID,
                                    ApplicationID = b.ApplicationID,
                                    CIF = c.CIF,
                                    ID = a.TBOTMOTransactionID,
                                    TBOTMOID = a.TBOTMODocumentID.Value,
                                    TBOTMOName = a.TBOTMODocumentName,
                                    CustomerName = c.CustomerName,
                                    Status = a.Status,
                                    TBOApplicationID = a.TBOApplicationID,
                                    WorkflowInstanceID = a.WorkflowInstanceID,
                                    CreatedBy = a.CreatedBy,
                                    CreatedDate = a.CreatedDate,
                                    LastModifiedBy = a.ModifiedDate == null ? a.CreatedBy : a.ModifiedBy,
                                    LastModifiedDate = a.ModifiedDate == null ? a.CreatedDate : a.ModifiedDate.Value,
                                });

                    TBO.Page = page;
                    TBO.Size = data.Count();
                    TBO.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    TBO.Rows = data.OrderBy(orderBy).AsEnumerable().Select((a, i) => new TBOTMOModelRow
                    {
                        TransactionID = a.TransactionID,
                        TBOTMOTransactionID = a.TBOTMOTransactionID,
                        ApplicationID = a.ApplicationID,
                        CIF = a.CIF,
                        ID = a.ID,
                        TBOTMOID = a.TBOTMOID,
                        TBOTMOName = a.TBOTMOName,
                        CustomerName = a.CustomerName,
                        Status = a.Status,
                        TBOApplicationID = a.TBOApplicationID,
                        WorkflowInstanceID = a.WorkflowInstanceID,
                        CreatedBy = a.CreatedBy,
                        CreatedDate = a.CreatedDate,
                        LastModifiedBy = a.LastModifiedBy,
                        LastModifiedDate = a.LastModifiedDate,
                    }).Skip(skip)
                    .Take(data.Count()).ToList();
                    TBO.Data = TBO.Rows.GroupBy(a => a.TBOApplicationID).Select(g => g.FirstOrDefault()).ToList();
                    TBO.Total = TBO.Data.Count();
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool GetTBOTMO(TBOTMOModelFilter filter, ref IList<TBOTMOModel> TBO, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetTBOTMO(ref IList<TBOTMOModel> TBO, string cif, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    var tbo = (from a in context.TBOTMOTransactions
                               from b in context.TransactionDeals.Where(x => a.TransactionDealsID == x.TransactionDealID).DefaultIfEmpty()
                               from c in context.Customers.Where(x => b.CIF == x.CIF).DefaultIfEmpty()
                               where b.CIF == cif && a.Status != "Complete"
                               select new TBOTMOModel()
                               {
                                   TBOTMOTransactionID = a.TBOTMOTransactionID,
                                   TransactionID = b.TransactionDealID,
                                   ApplicationID = b.ApplicationID,
                                   CIF = b.CIF,
                                   CustomerName = c.CustomerName,
                                   ID = a.TBOTMOTransactionID,
                                   TBOSLAID = a.TBOSLAID.Value,
                                   ActDate = a.TBOTMOActualDate.HasValue ? a.TBOTMOActualDate : null,
                                   ExptDate = a.TBOTMOExpectedReceivedDate,
                                   Status = a.Status == null ? a.Status : a.Status,
                                   TypeDocID = a.TBOSLAID.HasValue ? a.TBOSLAID.Value : 0,
                                   DocumentPurposeID = a.DocumentPurposeID.HasValue ? a.DocumentPurposeID.Value : 0,
                                   DocumentID = a.TransactionDocumentID.HasValue ? a.TransactionDocumentID.Value : 0,
                                   CreatedBy = a.CreatedBy,
                                   CreatedDate = a.CreatedDate,
                                   LastModifiedBy = a.ModifiedBy,
                                   LastModifiedDate = a.ModifiedDate,
                                   TBOApplicationID = a.TBOApplicationID
                               }).ToList();


                    foreach (var item in tbo)
                    {
                        if (item.TypeDocID != 0)
                        {
                            var TypeofDoc = (from a in context.TBOSLAs
                                             where a.TBOSLAID == item.TypeDocID
                                             select a).SingleOrDefault();
                            if (TypeofDoc != null)
                            {
                                item.TypeofDoc = new DocumentTypeTBOTMOModel()
                                {
                                    ID = TypeofDoc.TBOSLAID,
                                    Name = TypeofDoc.Description,
                                    Description = TypeofDoc.Description
                                };
                            }
                            else
                            {
                                item.TypeofDoc = new DocumentTypeTBOTMOModel()
                                {
                                    ID = 0,
                                    Name = "",
                                    Description = ""
                                };
                            }
                        }
                        else
                        {
                            item.TypeofDoc = new DocumentTypeTBOTMOModel()
                            {
                                ID = 0,
                                Name = "",
                                Description = ""
                            };
                        }

                        if (item.DocumentPurposeID != 0)
                        {
                            var PurposeDoc = (from a in context.DocumentPurposes
                                              where a.PurposeID == item.DocumentPurposeID
                                              select a).SingleOrDefault();
                            if (PurposeDoc != null)
                            {
                                item.PurposeofDoc = new DocumentPurposeTBOModel()
                                {
                                    ID = PurposeDoc.PurposeID,
                                    Name = PurposeDoc.PurposeName,
                                    Description = PurposeDoc.PurposeDescription
                                };
                            }
                            else
                            {
                                item.PurposeofDoc = new DocumentPurposeTBOModel()
                                {
                                    ID = 0,
                                    Name = "",
                                    Description = ""
                                };
                            }
                        }
                        else
                        {
                            item.PurposeofDoc = new DocumentPurposeTBOModel()
                            {
                                ID = 0,
                                Name = "",
                                Description = ""
                            };
                        }


                        if (item.DocumentID != 0)
                        {
                            var Document = (from a in context.TransactionDocumentTBOes
                                            where a.TransactionDocumentTBOID == item.ID
                                            select a).SingleOrDefault();
                            if (Document != null)
                            {
                                item.FileName = new DocumentModelTBOTMO()
                                {
                                    ID = Document.TransactionDocumentID.Value,
                                    name = Document.Filename,
                                    DocumentPath = Document.DocumentPath
                                };
                            }
                            else
                            {
                                item.FileName = new DocumentModelTBOTMO()
                                {
                                    ID = 0,
                                    name = "",
                                    DocumentPath = ""
                                };
                            }
                        }
                        else
                        {
                            item.FileName = new DocumentModelTBOTMO()
                            {
                                ID = 0,
                                name = "",
                                DocumentPath = ""
                            };
                        }
                    }
                    TBO = tbo;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetTBOTMOWF(ref IList<TBOTMOModel> TBO, string cif, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    TBO = (from a in context.TBOTMOTransactions
                           join b in context.TransactionDeals on a.TransactionDealsID equals b.TransactionDealID
                           join c in context.Customers on b.CIF equals c.CIF
                           join d in context.TransactionDocumentTBOes on a.TransactionDocumentID equals d.TransactionDocumentID
                           join e in context.DocumentPurposes on a.DocumentPurposeID equals e.PurposeID
                           join f in context.TBOSLAs on a.TBOSLAID.Value equals f.TBOSLAID
                           where b.CIF == cif && a.Status == "Add"
                           select new TBOTMOModel
                           {
                               TransactionID = b.TransactionDealID,
                               TBOTMOTransactionID = a.TBOTMOTransactionID,
                               ApplicationID = b.ApplicationID,
                               CIF = b.CIF,
                               CustomerName = c.CustomerName,
                               ID = a.TBOTMOTransactionID,
                               TBOSLAID = a.TBOSLAID.Value,
                               ActDate = a.TBOTMOActualDate,
                               ExptDate = a.TBOTMOExpectedReceivedDate,
                               Status = a.Status,
                               TypeofDoc = new DocumentTypeTBOTMOModel()
                               {
                                   ID = f.TBOSLAID,
                                   Name = f.Description,
                                   Description = f.Description
                               },
                               PurposeofDoc = new DocumentPurposeTBOModel()
                               {
                                   ID = e.PurposeID,
                                   Name = e.PurposeName,
                                   Description = e.PurposeDescription
                               },
                               FileName = new DocumentModelTBOTMO()
                               {
                                   ID = d.TransactionDocumentID.Value,
                                   name = d.Filename,
                                   DocumentPath = d.DocumentPath
                               },
                               CreatedBy = a.CreatedBy,
                               CreatedDate = a.CreatedDate,
                               LastModifiedBy = a.ModifiedBy,
                               LastModifiedDate = a.ModifiedDate,
                               TBOApplicationID = a.TBOApplicationID
                           }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateTBOTMO(TBOTMOModel TBO, ref string message)
        {
            bool isSuccess = false;
            IList<TBOTMOTransaction> TBOTMO = new List<TBOTMOTransaction>();
            try
            {
                DateTime utcTime = DateTime.UtcNow;
                string strUtcTime_o = utcTime.ToString("o").Replace("-", "").Replace(":", "").Replace(".", "").Replace("T", "").Replace("Z", "");

                long id;
                foreach (var item in TBO.Documents)
                {
                    id = context.TransactionDeals.Where(a => a.ApplicationID == item.ApplicationID).Select(a => a.TransactionDealID).FirstOrDefault();

                    #region Transaction Document
                    Entity.TransactionDocumentTBO document = new Entity.TransactionDocumentTBO()
                    {
                        TransactionDocumentID = id,
                        DocTypeID = item.Type.ID,
                        PurposeID = item.Purpose.ID,
                        Filename = item.FileName,
                        DocumentPath = item.DocumentPath,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    };
                    context.TransactionDocumentTBOes.Add(document);
                    #endregion

                    #region Update flag TBO Transaction Draft
                    Entity.TBOTMOTransaction data = new TBOTMOTransaction()
                    {
                        TBOTMOActualDate = item.ActDate,
                        TBOTMOTransactionID = item.ID,
                        DocumentPurposeID = item.Purpose.ID,
                        DocumentTypeID = item.Type.ID,
                        ModifiedBy = currentUser.GetCurrentUser().DisplayName,
                        ModifiedDate = DateTime.UtcNow,
                        TBOSLAID = item.TBOSLAID,
                        TransactionDocumentID = document.TransactionDocumentID,
                        TransactionDealsID = document.TransactionDocumentID,
                        TBOApplicationID = item.TBOApplicationID
                    };
                    #endregion
                    TBOTMO.Add(data);

                }
                context.SaveChanges();
                foreach (var item in TBOTMO)
                {
                    Entity.TBOTMOTransaction data = context.TBOTMOTransactions.Where(a => a.TBOTMOTransactionID == item.TBOTMOTransactionID).SingleOrDefault();
                    if (data != null)
                    {
                        data.TBOTMOActualDate = item.TBOTMOActualDate;
                        data.DocumentPurposeID = item.DocumentPurposeID;
                        data.DocumentTypeID = item.DocumentTypeID;
                        data.ModifiedBy = item.ModifiedBy;
                        data.ModifiedDate = item.ModifiedDate;
                        data.Status = "Add";
                        data.TransactionDocumentID = item.TransactionDocumentID;
                        data.TBOApplicationID = item.TBOApplicationID;
                        if (data.TBOSLAID == 4)
                        {
                            data.TBOTMODocumentName = "MT103";
                        }
                        else
                        {
                            data.TBOTMODocumentName = "Other Document";
                        }
                        context.SaveChanges();
                    }

                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    if (ex.InnerException.InnerException != null)
                        message = ex.InnerException.InnerException.Message;
                    else
                        message = ex.Message;
                else
                    message = ex.Message;
            }
            return isSuccess;
        }

        public bool SaveTBOTMO(TBOTMOModelWF TBO, ref string message)
        {
            bool isSuccess = false;
            long id;
            try
            {
                foreach (var item in TBO.Documents)
                {
                    id = context.TransactionDeals.Where(a => a.ApplicationID == item.ApplicationID).Select(a => a.TransactionDealID).FirstOrDefault();
                    Entity.TBOTMOTransaction data = context.TBOTMOTransactions.Where(a => a.TBOApplicationID == item.TBOApplicationID && a.TransactionDealsID == id && a.TBOSLAID == item.TBOSLAID).SingleOrDefault();
                    if (data != null)
                    {
                        if (item.IsChecklistSubmit == true)
                        {
                            data.ModifiedBy = currentUser.GetCurrentUser().DisplayName;
                            data.ModifiedDate = DateTime.UtcNow;
                            data.Status = "Complete";
                        }
                        else
                        {
                            data.ModifiedBy = currentUser.GetCurrentUser().DisplayName;
                            data.ModifiedDate = DateTime.UtcNow;
                            data.Status = "Revise";
                        }
                        context.SaveChanges();
                    }
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetTransactionTBOTMOByCif(ref IList<TBOTMOModel> TBO, string id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    TBO = (from a in context.TBOTMOTransactions
                           join b in context.TransactionDeals on a.TransactionDealsID equals b.TransactionDealID
                           join c in context.Customers on b.CIF equals c.CIF
                           where b.CIF == id
                           select new TBOTMOModel
                           {
                               TransactionID = b.TransactionDealID,
                               TBOTMOTransactionID = a.TBOTMOTransactionID,
                               ApplicationID = b.ApplicationID,
                               CIF = b.CIF,
                               CustomerName = c.CustomerName,
                               ID = a.TBOTMOTransactionID,
                               TBOTMOID = a.TBOTMODocumentID.Value,
                               ActDate = a.TBOTMOActualDate,
                               ExptDate = a.TBOTMOExpectedReceivedDate,
                               Status = a.Status,
                               CreatedBy = a.CreatedBy,
                               CreatedDate = a.CreatedDate,
                               LastModifiedBy = a.ModifiedBy,
                               LastModifiedDate = a.ModifiedDate,
                               TBOApplicationID = a.TBOApplicationID
                           }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetTBOTMOWFID(ref IList<TBOTMOModel> TBO, string spUserLoginName, ref string message)
        {
            bool isSuccess = false;
            var spUser = "i:0#.f|dbsmembership|" + spUserLoginName;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    context.Database.CommandTimeout = 900;
                    var tbo = (from a in context.SP_GetTaskHomeTBOTMO(spUser)
                               select new TBOTMOModel
                               {
                                   TransactionID = a.TransactionDealID,
                                   ApplicationID = a.ApplicationID,
                                   CIF = a.CIF,
                                   CustomerName = a.CustomerName,
                                   ID = a.TBOTMOTransactionID,
                                   TBOSLAID = a.TBOSLAID.Value,
                                   ActDate = a.TBOTMOActualDate,
                                   ExptDate = a.TBOTMOExpectedReceivedDate,
                                   Status = a.Status,
                                   TypeofDoc = new DocumentTypeTBOTMOModel()
                                   {
                                       ID = a.DocTypeID.Value,
                                       Name = a.DocTypeName,
                                       Description = a.DocTypeDescription
                                   },
                                   PurposeofDoc = new DocumentPurposeTBOModel()
                                   {
                                       ID = a.PurposeID.Value,
                                       Name = a.PurposeName,
                                       Description = a.PurposeDescription
                                   },
                                   FileName = new DocumentModelTBOTMO()
                                   {
                                       ID = a.TransactionDocumentID.Value,
                                       name = a.Filename,
                                       DocumentPath = a.DocumentPath
                                   },
                                   CreatedBy = a.CreatedBy,
                                   CreatedDate = a.CreatedDate,
                                   LastModifiedBy = a.ModifiedBy,
                                   LastModifiedDate = a.ModifiedDate,
                                   TBOApplicationID = a.TBOApplicationID,
                                   SPTaskListID = a.SPTaskListID,
                                   SPTaskListItemID = a.SPTaskListItemID,
                                   WorkflowInstanceID = a.WorkflowInstanceID
                               }).ToList();

                    if (tbo != null)
                    {
                        TBO = tbo;
                    }
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
    }
}