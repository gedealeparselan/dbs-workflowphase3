﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;

namespace DBS.WebAPI.Controllers
{
    //[Authorize]
    public class VolumeMatrixProductController : ApiController
    {
        private string message = string.Empty;

        private readonly IVolumeMatrixProduct repositoryProduct = null;


        public VolumeMatrixProductController()
        {
            this.repositoryProduct = new VolumeMatrixProductRepository();

        }

        public VolumeMatrixProductController(IVolumeMatrixProduct repositoryProduct)
        {
            this.repositoryProduct = repositoryProduct;
        }

        [ResponseType(typeof(IList<VolumeMatrixProductModel>))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(string productTypeDetailID, string isTAT, DateTime startDate, DateTime endDate, DateTime clientDateTime, string paymentMode)
        {
            IList<VolumeMatrixProductModel> output = new List<VolumeMatrixProductModel>();

            if (!repositoryProduct.GetVolumeMatrixDashboardProduct(productTypeDetailID, isTAT, startDate, endDate, clientDateTime, paymentMode, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<VolumeMatrixProductModel>>(HttpStatusCode.OK, output);
            }
        }

    }
}
