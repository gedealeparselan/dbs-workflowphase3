﻿using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;

namespace DBS.WebAPI.Models
{
    #region Property
      public class ThresholdGroupModel
    {
        public int ID { get; set; }//
        public int ProductTypeID { get; set; }//
        public int ThresholdGroupID { get; set; }//
        public string GroupName { get; set; }//
        public string ProductTypeCode { get; set; }
        public string TransactionType { get; set; }//
        public string ResidentType { get; set; }//
        public string ThresholdType { get; set; }//
        public string TransactionType2 { get; set; }//
        public string ResidentType2 { get; set; }//
        public string ThresholdType2 { get; set; }//
        public decimal? ThresholdValue  { get; set; }//
        public decimal? RoundingValue { get; set; }//
        public bool? IsDelete { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public ProductTypeModel ProductType { get; set; }//
        public string LastModifiedBy { get; set; }//
        public DateTime LastModifiedDate { get; set; }//
    }

    public class ThresholdGroupModelRow : ThresholdGroupModel {
        public int RowID { get; set; }
    }

    public class ThresholdGroupModelGrid : Grid {
        public IList<ThresholdGroupModelRow> Rows { get; set; }
    }
    #endregion

    #region Filter
    public class ProductTypeThresholdModelFilter : Filter { }

    public class ThresholdGroupModelFilter : Filter { }
    #endregion

    #region InterFace
    public interface IProductTypeThresholdMapping : IDisposable
    {
        bool GetProductTypeThresholdMappingByID(int id, ref ThresholdGroupModel ThresholdGroup, ref string message);
        bool GetProductTypeThresholdMapping(ref IList<ThresholdGroupModel> ThresholdGroup, int limit, int index, ref string message);//dpt
        bool getThreshold(ref IList<ThresholdGroupModel> model, int limit, int index, ref string message);      
        bool GetProductTypeThresholdMapping(ref IList<ThresholdGroupModel> ThresholdGroup, ref string message);//dpt
        bool GetProductTypeThresholdMapping(int page, int size, IList<ThresholdGroupModelFilter> filters, string sortColumn, string sortOrder, ref ThresholdGroupModelGrid ThresholdGroup, ref string message);//dpt
        bool GetProductTypeThresholdMapping(ProductTypeThresholdModelFilter filter, ref IList<ThresholdGroupModel> ProductThreshold, ref string message);
        bool AddProductTypeThresholdMapping(ThresholdGroupModel ThresholdGroup, ref string message);
        bool UpdateProductThreshold(int Id, ThresholdGroupModel ThresholdGroup, ref string message);
        bool DeleteProductTypeThresholdMapping(int id, ref string message);
    }
    #endregion

    #region Repository
    public class ProductTypeThresholdModelRepository : IProductTypeThresholdMapping
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetProductTypeThresholdMappingByID(int id, ref ThresholdGroupModel ThresholdGroup, ref string message)
        {
            bool isSucces = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                    ThresholdGroup = (from a in context.ProductTypeThresholdMappings
                                        join b in context.ProductTypes on a.ProductTypeID equals b.ProductTypeID
                                        join c in context.ThresholdGroups on a.ThresholdGroupID equals c.ThresholdGroupID
                                        select new ThresholdGroupModel
                                        {
                                            ID = a.ProductTypeThresholdMappingID,
                                            ProductTypeID = b.ProductTypeID,
                                            ThresholdGroupID = c.ThresholdGroupID,
                                            GroupName = c.GroupName,
                                            ProductTypeCode = b.ProductTypeCode,
                                            TransactionType = c.TransactionType,
                                            ResidentType = c.ResidentType,
                                            ThresholdValue = c.ThresholdValue,
                                            RoundingValue = c.RoundingValue,
                                            LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                            LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                        }
                                        ).SingleOrDefault();
            
                                        
                                        
            }catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }
        public bool GetProductTypeThresholdMapping(ref IList<ThresholdGroupModel> ThresholdGroup, ref string message)
        {
            bool isSucces = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    ThresholdGroup = (from a in context.ProductTypeThresholdMappings
                                        join b in context.ProductTypes on a.ProductTypeID equals b.ProductTypeID
                                        join c in context.ThresholdGroups on a.ThresholdGroupID equals c.ThresholdGroupID
                                        where a.IsDeleted.Equals(false)
                                        select new ThresholdGroupModel
                                        {
                                            ID = a.ProductTypeThresholdMappingID,
                                            ProductTypeID = b.ProductTypeID,
                                            ThresholdGroupID = c.ThresholdGroupID,
                                            GroupName = c.GroupName,

                                            ProductType= new ProductTypeModel(){
                                                ID = a.ProductType.ProductTypeID,
                                                Code = a.ProductType.ProductTypeCode 
                                            },
                                            
                                            TransactionType = c.TransactionType,
                                            ResidentType = c.ResidentType,
                                            ThresholdValue = c.ThresholdValue,
                                            RoundingValue = c.RoundingValue,
                                            LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                            LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,   
                                            
                                           
                                        }
                        ).ToList();
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }
        public bool GetProductTypeThresholdMapping(int page, int size, IList<ThresholdGroupModelFilter> filters, string sortColumn, string sortOrder, ref ThresholdGroupModelGrid ThresholdGroup, ref string message)
        {
            bool isSucces = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                ThresholdGroupModel filter = new ThresholdGroupModel();

                if (filters != null)
                {
                    filter.GroupName = filters.Where(a => a.Field.Equals("GroupName")).Select(a => a.Value).SingleOrDefault();
                    filter.ProductTypeCode = filters.Where(a => a.Field.Equals("ProductTypeCode")).Select(a => a.Value).SingleOrDefault();
                    filter.TransactionType = filters.Where(a => a.Field.Equals("TransactionType")).Select(a => a.Value).SingleOrDefault();
                    filter.ResidentType = filters.Where(a => a.Field.Equals("ResidentType")).Select(a => a.Value).SingleOrDefault();
                    filter.ThresholdType = filters.Where(a => a.Field.Equals("ThresholdType")).Select(a => a.Value).SingleOrDefault();
                    filter.ThresholdValue = filters.Where(a => a.Field.Equals("ThresholdValue")).SingleOrDefault() != null ? Convert.ToDecimal(filters.Where(a => a.Field.Equals("ThresholdValue")).Select(a => a.Value).SingleOrDefault()) : (decimal?)null;
                    filter.RoundingValue = filters.Where(a => a.Field.Equals("RoundingValue")).SingleOrDefault() != null ? Convert.ToDecimal(filters.Where(a => a.Field.Equals("RoundingValue")).Select(a => a.Value).SingleOrDefault()) : (decimal?)null;
                    
                    //filter.ThresholdValue =Convert.ToDecimal(filters.Where(a => a.Field.Equals("ThresholdValue")).Select(a => a.Value).SingleOrDefault());
                   // filter.ThresholdValue = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("ThresholdValue")).Select(a => a.Value == null ? null : a.Value).SingleOrDefault());
                    //filter.RoundingValue = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("RoundingValue")).Select(a => a.Value == null ? "0" : a.Value).SingleOrDefault());
                    //filter.RoundingValue = Convert.ToDecimal(filters.Where(a => a.Field.Equals("RoundingValue")).Select(a => a.Value).SingleOrDefault());
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.ProductTypeThresholdMappings
                                join b in context.ProductTypes on a.ProductTypeID equals b.ProductTypeID
                                join c in context.ThresholdGroups on a.ThresholdGroupID equals c.ThresholdGroupID
                                let isGroupName = !string.IsNullOrEmpty(filter.GroupName)
                                let isProductTypeCode = !string.IsNullOrEmpty(filter.ProductTypeCode)
                                let isTransactionType = !string.IsNullOrEmpty(filter.TransactionType)
                                let isResidentType = !string.IsNullOrEmpty(filter.ResidentType)
                                let isThresholdType = !string.IsNullOrEmpty(filter.ThresholdType)
                                let isThresholdValue = filter.ThresholdValue.HasValue
                                let isRoundingValue = filter.RoundingValue.HasValue
                                
                                //let isRoundingValue = filter.RoundingValue == 0 ? false : true
                                //let isThresholdValue = filter.ThresholdValue
                                //let isRoundingValue = filter.RoundingValue 
                                where a.IsDeleted.Value.Equals(false) &&
                                b.IsDeleted.Equals(false) &&
                                c.IsDeleted.Value.Equals(false) &&
                                (isGroupName ? c.GroupName.Contains(filter.GroupName) : true) &&
                                (isProductTypeCode ? b.ProductTypeCode.Contains(filter.ProductTypeCode) : true) &&
                                  (isTransactionType ? (c.TransactionType == "B" ? "Buy" : "Sell").Contains(filter.TransactionType) : true) &&
                                  (isResidentType ? (c.ResidentType == "N" ? "Non Resident" : c.ResidentType == "R" ? "Resident" : "ALL").Contains(filter.ResidentType) : true) &&
                                  (isThresholdType ? (c.ThresholdType == "A"?"Accumulation":"Transaction").Contains(filter.ThresholdType) : true) &&
                                  (isThresholdValue ? c.ThresholdValue == filter.ThresholdValue.Value : true) &&
                                  (isRoundingValue ? c.RoundingValue == filter.RoundingValue.Value : true) 
                                //  (isThresholdValue ? filter.ThresholdValue == "0" ? !string.IsNullOrEmpty(c.ThresholdValue) : c.ThresholdValue == string.Empty : true) //&&
                                //(isThresholdValue ? c.ThresholdValue == filter.ThresholdValue : true) //&&
                                //  (isThresholdValue ? c.ThresholdValue.Equals(filter.ThresholdValue) : true)                                 
                                select new ThresholdGroupModel
                                {
                                    ID = a.ProductTypeThresholdMappingID,
                                    ProductTypeID = a.ProductType.ProductTypeID,
                                    ThresholdGroupID = c.ThresholdGroupID,
                                    GroupName = c.GroupName,
                                    ProductType = new ProductTypeModel()
                                    {
                                        ID = a.ProductType.ProductTypeID,
                                        Code = a.ProductType.ProductTypeCode
                                    },
                                    ProductTypeCode = a.ProductType.ProductTypeCode,

                                    TransactionType = c.TransactionType,
                                    ResidentType = c.ResidentType,
                                    ThresholdType = c.ThresholdType,

                                    TransactionType2 = c.TransactionType == "S" ? "Sell" : "Buy",
                                    ResidentType2 = c.ResidentType == "N" ? "Non Resident" : c.ResidentType == "R" ? "Resident" : "All",
                                    ThresholdType2 = c.ThresholdType  == "A" ? "Accumulation" : "Transaction",

                                    ThresholdValue = c.ThresholdValue.HasValue ? c.ThresholdValue.Value : 0,
                                    RoundingValue = c.RoundingValue.HasValue ? c.RoundingValue.Value : 0,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,   
                                    
                                   
                                });

                    ThresholdGroup.Page = page;
                    ThresholdGroup.Size = size;
                    ThresholdGroup.Total = data.Count();
                    ThresholdGroup.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    ThresholdGroup.Rows = data.OrderBy(orderBy).AsEnumerable().Select((a, i) => new ThresholdGroupModelRow
                    {
                        RowID = i + 1,
                        ID = a.ID,
                        GroupName = a.GroupName,
                        ProductTypeID = a.ProductTypeID,
                        ProductTypeCode = a.ProductTypeCode,
                        ProductType = new ProductTypeModel() {ID = a.ProductType.ID, Code = a.ProductType.Code},
                        TransactionType = a.TransactionType,
                        ResidentType = a.ResidentType,
                        ThresholdType = a.ThresholdType,
                        
                        TransactionType2 = a.TransactionType2,
                        ResidentType2 = a.ResidentType2,
                        ThresholdType2 = a.ThresholdType2,

                        ThresholdValue = a.ThresholdValue,
                        RoundingValue = a.RoundingValue,
                        LastModifiedDate = a.LastModifiedDate,
                        LastModifiedBy = a.LastModifiedBy,  
                    }).Skip(skip).Take(size).ToList();
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }
        public bool GetProductTypeThresholdMapping(ProductTypeThresholdModelFilter filter, ref IList<ThresholdGroup> ThresholdGroup, ref string message)
        {
            throw new NotImplementedException();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public bool GetProductTypeThresholdMapping(ref IList<ThresholdGroupModel> ThresholdGroup, int limit, int index, ref string message)
        {
            bool isSuuces = false;
            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    ThresholdGroup = (from a in context.ProductTypeThresholdMappings
                                        join b in context.ProductTypes on a.ProductTypeID equals b.ProductTypeID
                                        join c in context.ThresholdGroups on a.ThresholdGroupID equals c.ThresholdGroupID
                                      select new ThresholdGroupModel
                                        {
                                            ID = a.ProductTypeThresholdMappingID,
                                            ProductTypeID = b.ProductTypeID,
                                            ThresholdGroupID = c.ThresholdGroupID,
                                            GroupName = c.GroupName,
                                            ProductTypeCode = b.ProductTypeCode,
                                            TransactionType = c.TransactionType,
                                            ResidentType = c.ResidentType,
                                            ThresholdType = c.ThresholdType,
                                            ThresholdValue = c.ThresholdValue,
                                            RoundingValue = c.RoundingValue,
                                            LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                            LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,

                                            ProductType = new ProductTypeModel()
                                            {
                                                ID = b.ProductTypeID,
                                                Code = b.ProductTypeCode
                                            }

                                        }).Skip(skip).Take(limit).ToList();
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuuces;
        }
        public bool AddProductTypeThresholdMapping(ThresholdGroupModel ThresholdGroup, ref string message)
        {
            bool IsSucces = false;
            try
            {
                //insert into threshold group by adi
                int ThresholdGroupID;

                // context.ThresholdGroups.Add(new DBS.Entity.ThresholdGroup()
                DBS.Entity.ThresholdGroup Threshold = new DBS.Entity.ThresholdGroup()
                {
                    //ThresholdGroupID = Threshold.ThresholdGroupID,
                    GroupName = ThresholdGroup.GroupName,
                    TransactionType = ThresholdGroup.TransactionType,
                    ResidentType = ThresholdGroup.ResidentType,
                    ThresholdType = ThresholdGroup.ThresholdType,
                    ThresholdValue = ThresholdGroup.ThresholdValue,
                    RoundingValue = ThresholdGroup.RoundingValue,
                    IsDeleted = false,
                    CreateDate = DateTime.UtcNow,
                    CreateBy = currentUser.GetCurrentUser().LoginName
                };
                context.ThresholdGroups.Add(Threshold);
                context.SaveChanges();

                //Get ThresholdGroupID after insert Threshold Group
                ThresholdGroupID = Threshold.ThresholdGroupID;

                //insert into Product Type Threhold Mapping
                if (ThresholdGroupID > 0)
                {
                    if (!context.ProductTypeThresholdMappings.Any(a => a.ThresholdGroupID == ThresholdGroupID && a.IsDeleted == false))
                    {
                        context.ProductTypeThresholdMappings.Add(new DBS.Entity.ProductTypeThresholdMapping()
                        {
                            //ProductTypeThresholdMappingID = ProductTypeThreshold.ID,
                            ProductTypeID = ThresholdGroup.ProductType.ID,
                            ThresholdGroupID = ThresholdGroupID,
                            IsDeleted = false,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().LoginName
                        });

                        context.SaveChanges();
                    }
                }
                IsSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return IsSucces;

        }
        public bool UpdateProductThreshold(int Id, ThresholdGroupModel ThresholdGroup, ref string message)
        {
            bool IsSucces = false;
            try {
                DBS.Entity.ProductTypeThresholdMapping datamapping = context.ProductTypeThresholdMappings.Where(a => a.ProductTypeThresholdMappingID == Id).SingleOrDefault();  
                DBS.Entity.ThresholdGroup data = context.ThresholdGroups.Where(a => a.ThresholdGroupID == datamapping.ThresholdGroupID).SingleOrDefault();
                //Entity.ThresholdGroup data = context.ThresholdGroups.Where(a => a.ThresholdGroupID.Equals(Id)).SingleOrDefault();
                if (data != null && datamapping != null)
                
                {
                    //update ThreholdGroup
                    data.GroupName = ThresholdGroup.GroupName;
                    data.TransactionType = ThresholdGroup.TransactionType;
                    data.ResidentType = ThresholdGroup.ResidentType;
                    data.ThresholdType = ThresholdGroup.ThresholdType;
                    data.ThresholdValue = ThresholdGroup.ThresholdValue;
                    data.RoundingValue = ThresholdGroup.RoundingValue;
                    data.IsDeleted = false;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().LoginName;
                    //Update product Type Threshold Mapping
                    datamapping.ProductTypeID = ThresholdGroup.ProductType.ID;
                    datamapping.IsDeleted = false;
                    datamapping.UpdateDate = DateTime.UtcNow;
                    datamapping.UpdateBy = currentUser.GetCurrentUser().LoginName;
                    context.SaveChanges();
                    IsSucces = true;
                }
                else {
                    message = string.Format("Threshold Group or Product Type Threshold Mapping data does not exist..!");
                }
                
            }catch(Exception ex)
                {
                message = ex.Message;
            }
            return IsSucces;
        }
        public bool DeleteProductTypeThresholdMapping(int id, ref string message)
        {
            bool IsSucces = false;
            try
            {
                Entity.ProductTypeThresholdMapping datamapping = context.ProductTypeThresholdMappings.Where(a => a.ProductTypeThresholdMappingID == id).SingleOrDefault();
                Entity.ThresholdGroup data = context.ThresholdGroups.Where(b => b.ThresholdGroupID == datamapping.ThresholdGroupID).SingleOrDefault();

                if (datamapping != null && data != null)
                {

                    datamapping.IsDeleted = true;
                    datamapping.UpdateDate = DateTime.UtcNow;
                    datamapping.UpdateBy = currentUser.GetCurrentUser().LoginName;
                    context.SaveChanges();
                    IsSucces = true;

                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().LoginName;

                    context.SaveChanges();
                    IsSucces = true;
                }
                else {
                    message = string.Format("Product Type Threshold Mapping or Threshold Group is does not exist.", id);
                }
            }
            catch(Exception ex) {
                message = ex.Message;
            }
            return IsSucces;
        }












        public bool getThreshold(ref IList<ThresholdGroupModel> model, int limit, int index, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetProductTypeThresholdMapping(ProductTypeThresholdModelFilter filter, ref IList<ThresholdGroupModel> ProductThreshold, ref string message)
        {
            throw new NotImplementedException();
        }
    }

    #endregion
}