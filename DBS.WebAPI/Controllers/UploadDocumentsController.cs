﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class UploadDocumentController : ApiController
    {
        private string message = string.Empty;

        private readonly IUploadDocumentRepository repository = null;

        public UploadDocumentController()
        {
            this.repository = new UploadDocumentRepository();
        }

        public UploadDocumentController(IUploadDocumentRepository repository)
        {
            this.repository = repository;
        }

     /*   /// <summary>
        /// Get Customer Type for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(FailedUplaodGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<FailedUplaodFilter> filters)
        {
            FailedUplaodGrid output = new FailedUplaodGrid();

            if (!repository.GetUploadDocument(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            if(!repository.)
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<FailedUplaodGrid>(HttpStatusCode.OK, output);
            } 
        } */

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}