﻿using DBS.Entity;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;

using System.Linq.Dynamic;
namespace DBS.WebAPI.Models
{
    #region Model
    public class FDIM
    {
        public long FDMaturityID { get; set; }
        public bool FDIMSelect { get; set; }
        public string SchemeType { get; set; }
        public string AccNumber { get; set; }
        public string IntTableCode { get; set; }
        public string Version { get; set; }
        public decimal IntCr { get; set; }
        public int IntDr { get; set; }
        public string IntPegged { get; set; }
        public string PegReviewDate { get; set; }
        public string PegFreq { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal IntMargin { get; set; }
    }

    public class FDIMRow : FDIM
    {
        public int RowID { get; set; }

    }
    public class FDIMGrid : Grid
    {
        public IList<FDIMRow> Rows { get; set; }
    }
    #endregion

    #region Filter
    public class FDIMFilter : Filter { }
    #endregion

    #region Interface
    public interface IFDGenerateRepository : IDisposable
    {
        bool GetFDInterest(DateTime? date, ref IList<FDIM> output, ref string message);
        bool GetFDInterest(DateTime? date, int page, int size, IList<FDIMFilter> filters, string sortColumn, string sortOrder, ref FDIMGrid transaction, ref string message);

    }
    #endregion

    #region Repository
    public class FDGenerateRepository : IFDGenerateRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);

            GC.Collect();
            GC.SuppressFinalize(this);
        }

       
        public bool GetFDInterest(DateTime? date, int page, int size, IList<FDIMFilter> filters, string sortColumn, string sortOrder, ref FDIMGrid transaction, ref string message)
        {
            bool isSuccess = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                FDIM filter = new FDIM();
                if (filters != null)
                {
                    filter.SchemeType = (string)filters.Where(a => a.Field.Equals("SchemeType")).Select(a => a.Value).SingleOrDefault();
                    filter.AccNumber = (string)filters.Where(a => a.Field.Equals("AccountNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.IntTableCode = (string)filters.Where(a => a.Field.Equals("IntTableCode")).Select(a => a.Value).SingleOrDefault();
                    filter.Version = (string)filters.Where(a => a.Field.Equals("Version")).Select(a => a.Value).SingleOrDefault();
                    filter.IntCr = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("PrefIntCr")).Select(a => a.Value == null ? "0" : a.Value).SingleOrDefault());
                    filter.IntDr = Convert.ToInt32((string)filters.Where(a => a.Field.Equals("PrefIntDr")).Select(a => a.Value == null ? "0" : a.Value).SingleOrDefault());
                    filter.IntPegged = (string)filters.Where(a => a.Field.Equals("IntPegged")).Select(a => a.Value).SingleOrDefault();
                    filter.PegFreq = (string)filters.Where(a => a.Field.Equals("PeggingFreq")).Select(a => a.Value).SingleOrDefault();
                    filter.PegReviewDate = (string)filters.Where(a => a.Field.Equals("PegReviewDate")).Select(a => a.Value == null ? "0" : a.Value).SingleOrDefault();
                    filter.IntMargin = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("InterestMargin")).Select(a => a.Value).SingleOrDefault());
                    if (filters.Where(a => a.Field.Equals("StartDate")).Any())
                    {
                        filter.StartDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("StartDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("EndDate")).Any())
                    {
                        filter.EndDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EndDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.EndDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.V_GenerateFDIM

                                let isSchemeType = !string.IsNullOrEmpty(filter.SchemeType)
                                let isAccNumber = !string.IsNullOrEmpty(filter.AccNumber)
                                let isIntTableCode = !string.IsNullOrEmpty(filter.IntTableCode)
                                let isVersion = !string.IsNullOrEmpty(filter.Version)
                                let isIntCr = filter.IntCr == 0 ? false : true
                                let isIntDr = filter.IntDr == 0 ? false : true
                                let isIntPegged = !string.IsNullOrEmpty(filter.IntPegged)
                                let isPegFreq = !string.IsNullOrEmpty(filter.PegFreq)
                                let isPegReviewDate = !string.IsNullOrEmpty(filter.PegReviewDate)
                                let isIntMargin = filter.IntMargin == 0 ? false : true
                                let isStartDate = filter.StartDate.HasValue ? true : false
                                let isEndDate = filter.EndDate.HasValue ? true : false

                                where
                                    a.CreateDate.Year == date.Value.Year
                                    && a.CreateDate.Month == date.Value.Month
                                    && a.CreateDate.Day == date.Value.Day &&
                                    (isSchemeType ? a.Scheme_Type.Contains(filter.SchemeType) : true) &&
                                    (isAccNumber ? a.Account_Number.Contains(filter.AccNumber) : true) &&
                                    (isIntTableCode ? a.Int__Table_Code.Contains(filter.IntTableCode) : true) &&
                                    (isVersion ? a.Version.Contains(filter.Version) : true) &&
                                    (isIntCr ? a.A_c__Pref__Int___Cr__.Value == filter.IntCr : true) &&
                                    (isIntDr ? a.A_c__Pref__Int___Dr__ == filter.IntDr : true) &&
                                    (isIntPegged ? a.Int__Pegged.Contains(filter.IntPegged) : true) &&
                                    (isPegFreq ? a.Pegging_Freq___Months_Days_.Contains(filter.PegFreq) : true) &&
                                    (isPegReviewDate ? a.Peg_Review_Date.Contains(filter.PegReviewDate) : true) &&
                                    (isIntMargin ? a.A_c__Pref__Int___Cr__.Value == filter.IntCr : true) &&
                                    (isStartDate ? a.Start_Date == filter.StartDate : true) &&
                                    (isEndDate ? a.End_Date == filter.EndDate : true)

                                select new FDIM
                                {
                                    FDMaturityID = a.FDMaturityID.HasValue ? a.FDMaturityID.Value : 0,
                                    FDIMSelect = false,
                                    SchemeType = a.Scheme_Type,
                                    AccNumber = a.Account_Number,
                                    IntTableCode = a.Int__Table_Code,
                                    Version = a.Version,
                                    IntCr = (decimal)a.A_c__Pref__Int___Cr__,
                                    IntDr = a.A_c__Pref__Int___Dr__,
                                    IntPegged = a.Int__Pegged,
                                    PegReviewDate = a.Peg_Review_Date,
                                    PegFreq = a.Pegging_Freq___Months_Days_,
                                    StartDate = a.Start_Date,
                                    EndDate = a.End_Date,
                                    IntMargin = a.InterestMargin
                                });

                    transaction.Page = page;
                    transaction.Size = size;
                    transaction.Total = data.Count();
                    transaction.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    transaction.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new FDIMRow
                        {
                            RowID = i + 1,
                            FDMaturityID = a.FDMaturityID,
                            FDIMSelect = false,
                            SchemeType = a.SchemeType,
                            AccNumber = a.AccNumber,
                            IntTableCode = a.IntTableCode,
                            Version = a.Version,
                            IntCr = a.IntCr,
                            IntDr = a.IntDr,
                            IntPegged = a.IntPegged,
                            PegReviewDate = a.PegReviewDate,
                            PegFreq = a.PegFreq,
                            StartDate = a.StartDate,
                            EndDate = a.EndDate,
                            IntMargin = a.IntMargin
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetFDInterest(DateTime? date, ref IList<FDIM> output, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    output = (from a in context.V_GenerateFDIM
                              where a.CreateDate.Year == date.Value.Year
                              && a.CreateDate.Month == date.Value.Month
                              && a.CreateDate.Day == date.Value.Day
                              select new FDIM
                              {
                                  FDMaturityID = a.FDMaturityID.HasValue ? a.FDMaturityID.Value : 0,
                                  FDIMSelect = false,
                                  SchemeType = a.Scheme_Type,
                                  AccNumber = a.Account_Number,
                                  IntTableCode = a.Int__Table_Code,
                                  Version = a.Version,
                                  IntCr = (decimal)a.A_c__Pref__Int___Cr__,
                                  IntDr = a.A_c__Pref__Int___Dr__,
                                  IntPegged = a.Int__Pegged,
                                  PegReviewDate = a.Peg_Review_Date,
                                  PegFreq = a.Pegging_Freq___Months_Days_,
                                  StartDate = a.Start_Date,
                                  EndDate = a.End_Date,
                                  IntMargin = a.InterestMargin
                              }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
    }
    #endregion
}