﻿using DBS.Sharepoint.Helpers;
using Microsoft.SharePoint;
using Nintex.Workflow.HumanApproval;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Data.SqlClient;
using System.Xml.Linq;
using System.Xml;

namespace DBS.Sharepoint
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class GenerateFileServices : IGenerateFileServices
    {
        public Test HelloWorld()
        {
            return new Test { LocalName = "dsada", RemoteName = "asdas", Size = 41 };
        }

        public GeneratedFileInfo GenerateFile(DataInformationCollection data)
        {
            GeneratedFileInfo result = new GeneratedFileInfo();

            //Request 17 Juni 2015 by Dennes, format penamaan txt diganti menjadi: ddmmyyMakernameProductnameSequencenumber.TXT
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    long InsertedID = 0;
                    string InsertedURL = string.Empty;
                    string Message = string.Empty; string BatchMessage = string.Empty;
                    string FileFormat = string.Empty;
                    string FileNameFormat = string.Empty;
                    bool IsSuccess = false; bool IsBatchSuccess = false;
                    long BatchInsertedID = 0; string BatchInsertedURL = string.Empty;

                    StringBuilder sbApplicationID = new StringBuilder();
                    DateTime Date = DateTime.Now;
                    //string strDate = string.Format("{0}-{1}-{2}", Date.Year,Date.Month,Date.Day);
                    //DateTime DateNow = DateTime.Parse(strDate);

                    int seqNumber;
                    //string DateFormat = String.Format("{0:yyyyMMdd_hhmmss}", Date);
                    string DateFormat = String.Format("{0:ddMMyy}", Date).ToUpper();
                    string DateFormatSKN = DateTime.UtcNow.ToString("ddMMyy");
                    int SKNBulkRow = data.DataInformation.Count;
                    String[] FileFormatSKNBulk = new String[data.DataInformation.Count];
                    String[] FileNameFormatSKNBulk = new String[data.DataInformation.Count];
                    String[] BatchFileNameFormatSKNBulk = new String[data.DataInformation.Count];
                    String[] AppSKNBulk = new String[data.DataInformation.Count];

                    //data.DataInformation.ToList().ForEach(item => sb.Append(item.ApplicationID + ","));
                    foreach (var item in data.DataInformation)
                    {
                        if (data.DataInformation.IndexOf(item) == data.DataInformation.Count - 1)
                        {
                            sbApplicationID.Append("'" + item.ApplicationID + "'");
                        }
                        else
                        {
                            sbApplicationID.Append("'" + item.ApplicationID + "',");
                        }
                    }

                    char splitter = '|';
                    int position = SPContext.Current.Web.CurrentUser.LoginName.LastIndexOf(splitter) + 1;
                    string Maker = SPContext.Current.Web.CurrentUser.LoginName.Substring(position).ToUpper();
                    using (SqlConnection conn = new SqlConnection(DataHelper.StrConn))
                    {
                        if (conn.State == ConnectionState.Closed) conn.Open();
                        using (SqlCommand cmd = new SqlCommand("SP_GetNextSequenceFileGeneration", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandTimeout = DataHelper.SQLCommandTimeout;
                            //cmd.Parameters.AddWithValue("Date", DateNow);
                            var returnParameter = cmd.Parameters.Add("NewSequence", SqlDbType.Int);
                            returnParameter.Direction = ParameterDirection.ReturnValue;
                            cmd.ExecuteNonQuery();
                            seqNumber = (int)returnParameter.Value;

                        }
                    }

                    string sql = "";
                    if (data.Format == "RT" || data.Format == "SK")
                    {
                        sql = "SELECT a.CIF, "
                                   + "a.AccountNumber, "
                                   + "a.ApplicationDate, "
                                   + "a.Amount, "
                                   + "a.BeneName, "
                                   + "a.BeneAccNumber, "
                                   + "b.BankCode, "
                                   + "b.BranchCode, "
                                   + "b.BankDescription, "
                                   + "c.CustomerName, "
                                   + "a.DetailCompliance, "
                                   + "a.IsResident, "
                                   + "a.IsCitizen, "
                                   + "d.ChargesCode, "
                                   + "e.ChargesCode, "
                                   + "a.Type, "
                                   + "a.BeneficiaryBusinessTypeID, "
                                   + "a.IsBeneficiaryResident "
                                   + "FROM [dbo].[V_GenerateTransaction] a "
                                   + "JOIN [Master].[Bank] b ON a.BankID = b.BankID "
                                   + "JOIN [Customer].[Customer] c ON a.CIF = c.CIF "
                                   + "JOIN [Master].[Charges] d ON a.BankChargesID = d.ChargesID "
                                   + "JOIN [Master].[Charges] e ON a.AgentChargesID = e.ChargesID "
                                   + "WHERE a.ApplicationID IN (" + sbApplicationID + ")";
                    }
                    else if (data.Format == "OV")
                    {
                        sql = "SELECT a.AccountNumber, "
                                   + "f.CurrencyCode, "
                                   + "'D' as PartTranType, "
                                   + "a.ApplicationDate, "
                                   + "a.Amount, "
                                   + "a.[DetailCompliance], "
                                   + "a.CurrID, "
                                   + "a.DebitCurrencyID, "
                                   + "a.executiondate, "
                                   + "a.rate "
                                   + "FROM [dbo].[V_GenerateTransaction] a "
                                   + "JOIN [Master].currency f on a.DebitCurrencyID = f.CurrencyID "
                                   + "JOIN [Master].currency g on a.CurrID = g.CurrencyID "
                                   + "WHERE a.ApplicationID IN (" + sbApplicationID + ")";
                    }
                    else if (data.Format == "SB")
                    {
                        sql = " SELECT a.TransactionID,a.IDFlag, b.ApplicationID, a.RequestUUID "
                                + " FROM [Middleware].[Flagging] a "
                                + " LEFT JOIN [Data].[Transaction] b ON b.TransactionID = a.TransactionID "
                                + " WHERE a.Contents IS NOT NULL and b.ApplicationID IN (" + sbApplicationID + ")";
                    }
                    DataTable dtTransaction = new DataTable();
                    dtTransaction = DataHelper.ExecuteDataTable(sql);

                    if (data.Format == "RT")
                    {
                        FileNameFormat = DateFormat + Maker + "RTGS" + seqNumber.ToString().ToUpper() + ".txt";
                        FileFormat = FileHelper.RTGSFormat(dtTransaction);
                    }
                    else if (data.Format == "SK")
                    {
                        FileNameFormat = DateFormat + Maker + "SKN" + seqNumber.ToString().ToUpper() + ".txt";
                        FileFormat = FileHelper.SKNFormat(dtTransaction);
                    }

                    else if (data.Format == "OV")
                    {
                        //if (dtTransaction.Rows.Count > 0)
                        //{
                        FileNameFormat = DateFormat + Maker + "OVB" + seqNumber.ToString().ToUpper() + ".txt";
                        FileFormat = FileHelper.OVBFormat(dtTransaction);
                        //}                        
                    }

                    else if (data.Format == "SB")
                    {

                        if (dtTransaction.Rows.Count > 0)
                        {
                            SKNBulkRow = dtTransaction.Rows.Count;
                            for (int i = 0; i < dtTransaction.Rows.Count ; i++)
                            {
                                string SQL = " SELECT TOP 1 a.Contents "
                                  + " FROM [Middleware].[Flagging] a "
                                  + " JOIN [Data].[Transaction] b ON a.TransactionID = b.TransactionID "
                                  + " WHERE a.Contents IS NOT NULL"
                                  + " AND a.TransactionID =" + dtTransaction.Rows[i][0].ToString()
                                  + " ORDER BY a.IDFlag DESC ";

                                DataTable dtContents = new DataTable();
                                dtContents = DataHelper.ExecuteDataTable(SQL);

                                FileNameFormatSKNBulk[i] = dtTransaction.Rows[i][3].ToString() + ".txt";
                                BatchFileNameFormatSKNBulk[i] = dtTransaction.Rows[i][3].ToString() + ".txt.ctrl";
                                FileFormatSKNBulk[i] = dtContents.Rows[i][0].ToString();
                                AppSKNBulk[i] = dtTransaction.Rows[i][2].ToString();
                            }
                        }
                    }


                    //result = new GeneratedFileInfo
                    //{
                    //    InsertedID = 0,
                    //    InsertedURL = string.Empty,
                    //    IsSuccess = false,
                    //    Message = FileFormat
                    //};
                    //ts.Complete();

                    using (SPSite site = new SPSite(SPContext.Current.Site.Url))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            if (data.Format == "SB")
                            {
                                #region SKN Bulk
                                for (int sb = 0; sb < SKNBulkRow ; sb++)
                                {
                                    web.AllowUnsafeUpdates = true;
                                    string sqlGenerated = string.Empty;

                                    using (Stream dataStream = FileHelper.GenerateStreamFromString(FileFormatSKNBulk[sb]))
                                    {
                                        if (FileHelper.AddToLibrary(web, dataStream, FileNameFormatSKNBulk[sb], "Documents", true, ref InsertedID, ref InsertedURL, ref IsSuccess, ref Message))
                                        {

                                            sqlGenerated = "INSERT INTO [Data].[GeneratedTransaction] ("
                                                         + "[ApplicationID],"
                                                         + "[FileName],"
                                                         + "[ItemID],"
                                                         + "[URL],"
                                                         + "[IsDeleted],"
                                                         + "[CreateDate],"
                                                         + "[CreateBy]) "
                                                         + "VALUES "
                                                         + "('" + AppSKNBulk[sb] + "',"
                                                         + "'" + FileNameFormatSKNBulk[sb] + "',"
                                                         + "" + InsertedID + ","
                                                         + "'" + InsertedURL + "',"
                                                         + "" + "0" + ","
                                                         + "'" + DateTime.UtcNow + "',"
                                                         + "'" + data.Username + "')";

                                            DataHelper.ExecuteDataTable(sqlGenerated);

                                        }
                                    }
                                    //using (Stream dataStreamBatch = FileHelper.GenerateStreamFromString(""))
                                    //{
                                    //    if (FileHelper.AddToLibrary(web, dataStreamBatch, BatchFileNameFormatSKNBulk[sb], "Documents", true, ref BatchInsertedID, ref BatchInsertedURL, ref IsBatchSuccess, ref BatchMessage))
                                    //    {
                                    //    }
                                    //}

                                    if (string.IsNullOrEmpty(Message))
                                    {
                                        result = new GeneratedFileInfo
                                        {
                                            InsertedID = InsertedID,
                                            InsertedURL = InsertedURL,
                                            IsSuccess = IsSuccess,
                                            Message = Message
                                        };
                                    }
                                    else
                                    {
                                        result = new GeneratedFileInfo
                                        {
                                            InsertedID = InsertedID,
                                            InsertedURL = InsertedURL,
                                            IsSuccess = false,
                                            Message = Message
                                        };
                                        ts.Dispose();
                                    }

                                    web.AllowUnsafeUpdates = false;
                                }
                                ts.Complete();
                                #endregion
                            }
                            else
                            {
                                #region Non SKN Bulk
                                web.AllowUnsafeUpdates = true;
                                string sqlGenerated = string.Empty;

                                using (Stream dataStream = FileHelper.GenerateStreamFromString(FileFormat))
                                {
                                    if (FileHelper.AddToLibrary(web, dataStream, FileNameFormat, "Documents", true, ref InsertedID, ref InsertedURL, ref IsSuccess, ref Message))
                                    {
                                        foreach (var item in data.DataInformation)
                                        {
                                            sqlGenerated = "INSERT INTO [Data].[GeneratedTransaction] ("
                                                         + "[ApplicationID],"
                                                         + "[FileName],"
                                                         + "[ItemID],"
                                                         + "[URL],"
                                                         + "[IsDeleted],"
                                                         + "[CreateDate],"
                                                         + "[CreateBy]) "
                                                         + "VALUES "
                                                         + "('" + item.ApplicationID + "',"
                                                         + "'" + FileNameFormat + "',"
                                                         + "" + InsertedID + ","
                                                         + "'" + InsertedURL + "',"
                                                         + "" + "0" + ","
                                                         + "'" + DateTime.UtcNow + "',"
                                                         + "'" + data.Username + "')";

                                            DataHelper.ExecuteDataTable(sqlGenerated);
                                        }
                                    }
                                }

                                if (string.IsNullOrEmpty(Message))
                                {
                                    result = new GeneratedFileInfo
                                    {
                                        InsertedID = InsertedID,
                                        InsertedURL = InsertedURL,
                                        IsSuccess = IsSuccess,
                                        Message = Message
                                    };
                                    ts.Complete();
                                }
                                else
                                {
                                    result = new GeneratedFileInfo
                                    {
                                        InsertedID = InsertedID,
                                        InsertedURL = InsertedURL,
                                        IsSuccess = false,
                                        Message = Message
                                    };
                                    ts.Dispose();
                                }

                                web.AllowUnsafeUpdates = false;
                                #endregion
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = new GeneratedFileInfo
                    {
                        InsertedID = 0,
                        InsertedURL = string.Empty,
                        IsSuccess = false,
                        Message = ex.Message
                    };
                    ts.Dispose();
                }
            }

            return result;
        }

        public GeneratedFileInfo GenerateFileXML(DataInformationCollection data)
        {

            GeneratedFileInfo result = null;
            var obj = data;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    long InsertedID = 0;
                    string InsertedURL = string.Empty;
                    string Message = string.Empty;
                    string FileFormat = string.Empty;
                    string FileNameFormat = string.Empty;
                    bool IsSuccess = false;

                    //StringBuilder sbApplicationID = new StringBuilder();
                    DateTime Date = DateTime.Now;

                    int seqNumber;
                    string DateFormat = String.Format("{0:ddMMyy}", Date).ToUpper();

                    string isiAppID = string.Empty;
                    long isiTransactionID = 0;

                    //temporary untuk menampung ApplicationID
                    String[] tempApplicationID = new String[data.DataInformation.Count];
                    int Counter = 0;

                    foreach (var item in data.DataInformation)
                    {
                        isiAppID = item.ApplicationID;
                        tempApplicationID[Counter] = item.ApplicationID;
                        Counter++;
                    }

                    char splitter = '|';
                    int position = SPContext.Current.Web.CurrentUser.LoginName.LastIndexOf(splitter) + 1;
                    string username = SPContext.Current.Web.CurrentUser.LoginName.Substring(position).ToUpper();
                    using (SqlConnection conn = new SqlConnection(DataHelper.StrConn))
                    {
                        if (conn.State == ConnectionState.Closed) conn.Open();
                        using (SqlCommand cmd = new SqlCommand("SP_GetNextSequenceFileGeneration", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandTimeout = DataHelper.SQLCommandTimeout;
                            var returnParameter = cmd.Parameters.Add("NewSequence", SqlDbType.Int);
                            returnParameter.Direction = ParameterDirection.ReturnValue;
                            cmd.ExecuteNonQuery();
                            seqNumber = (int)returnParameter.Value;
                        }
                    }

                    //nampung TransactionID yang dipilih
                    int[] arrTransactionID = new int[tempApplicationID.Length];
                    // nampung data content
                    //String[] arrContents = new String[arrTransactionID.Length];

                    for (int i = 0; i < tempApplicationID.Length; i++)
                    {
                        string tempTransactionid = " SELECT a.TransactionID "
                                                + " FROM [Middleware].[Flagging] a "
                                                + " LEFT JOIN [Data].[Transaction] b ON b.TransactionID = a.TransactionID "
                                                + " WHERE a.Contents IS NOT NULL and b.ApplicationID = '" + tempApplicationID[i] + "'";
                        //isiAppID
                        DataTable dtTransactionID = new DataTable();
                        dtTransactionID = DataHelper.ExecuteDataTable(tempTransactionid);

                        arrTransactionID[i] = int.Parse(dtTransactionID.Rows[0][0].ToString());
                    }

                    for (int x = 0; x < arrTransactionID.Length; x++)
                    {
                        string sql = " SELECT TOP 1 a.Contents "
                              + " FROM [Middleware].[Flagging] a "
                              + " JOIN [Data].[Transaction] b ON a.TransactionID = b.TransactionID "
                              + " WHERE a.Contents IS NOT NULL"
                              + " AND a.TransactionID =" + arrTransactionID[x]
                              + " ORDER BY a.IDFlag DESC ";

                        DataTable dtContents = new DataTable();
                        dtContents = DataHelper.ExecuteDataTable(sql);
                        //arrContents[x] = dtContents.Rows[0][0].ToString();

                        FileNameFormat = "XML" + tempApplicationID[x] + username + seqNumber.ToString().ToUpper() + ".xml";
                        FileFormat = FileHelper.XMLFormat(dtContents);

                        //upload to sharepoint library
                        using (SPSite site = new SPSite(SPContext.Current.Site.Url))
                        {
                            using (SPWeb web = site.OpenWeb())
                            {
                                web.AllowUnsafeUpdates = true;
                                string sqlGenerated = string.Empty;

                                using (Stream dataStream = FileHelper.GenerateStreamFromString(FileFormat))
                                {
                                    if (FileHelper.AddToLibrary(web, dataStream, FileNameFormat, "Documents", true, ref InsertedID, ref InsertedURL, ref IsSuccess, ref Message))
                                    {

                                        sqlGenerated = "INSERT INTO [Data].[GeneratedTransaction] ("
                                                     + "[ApplicationID],"
                                                     + "[FileName],"
                                                     + "[ItemID],"
                                                     + "[URL],"
                                                     + "[IsDeleted],"
                                                     + "[CreateDate],"
                                                     + "[CreateBy]) "
                                                     + "VALUES "
                                                     + "('" + tempApplicationID[x] + "',"
                                                     + "'" + FileNameFormat + "',"
                                                     + "" + InsertedID + ","
                                                     + "'" + InsertedURL + "',"
                                                     + "" + "0" + ","
                                                     + "'" + DateTime.UtcNow + "',"
                                                     + "'" + data.Username + "')";

                                        DataHelper.ExecuteDataTable(sqlGenerated);

                                    }
                                }

                                if (string.IsNullOrEmpty(Message))
                                {
                                    result = new GeneratedFileInfo
                                    {
                                        InsertedID = InsertedID,
                                        InsertedURL = InsertedURL,
                                        IsSuccess = IsSuccess,
                                        Message = Message
                                    };

                                }
                                else
                                {
                                    result = new GeneratedFileInfo
                                    {
                                        InsertedID = InsertedID,
                                        InsertedURL = InsertedURL,
                                        IsSuccess = false,
                                        Message = Message
                                    };
                                    ts.Dispose();
                                }

                                web.AllowUnsafeUpdates = false;
                            }
                        }

                    }
                    ts.Complete();
                }
                catch (Exception ex)
                {
                    result = new GeneratedFileInfo
                    {
                        InsertedID = 0,
                        InsertedURL = string.Empty,
                        IsSuccess = false,
                        Message = ex.Message
                    };
                    ts.Dispose();
                }

            }

            return result;

        }




        /*public GeneratedFileInfo GenerateFile(DataInformationCollection data)
        {
            GeneratedFileInfo result;

            long InsertedID = 0;
            string InsertedURL = string.Empty;
            string Message = string.Empty;
            bool IsSuccess = false;

            StringBuilder sb = new StringBuilder();
            //data.DataInformation.ToList().ForEach(item => sb.Append(item.ApplicationID + ","));
            foreach (var item in data.DataInformation)
            {
                if (data.DataInformation.IndexOf(item) == data.DataInformation.Count - 1)
                {
                    sb.Append("'" + item.ApplicationID + "'");
                }
                else
                {
                    sb.Append("'" + item.ApplicationID + "',");
                }
            }


            string sql = "SELECT * FROM [V_LastTransactionPayment] "
                       + "WHERE ApplicationID IN(" + sb + ")";

            DataTable dtTransaction = new DataTable();
            dtTransaction = DataHelper.ExecuteDataTable(sql);

            string dataFormat = FileHelper.DataTableToCSV(dtTransaction, "^", false);

            using (SPSite site = new SPSite(SPContext.Current.Site.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;
                    string sqlGenerated = string.Empty;

                    using (Stream dataStream = FileHelper.GenerateStreamFromString(dataFormat))
                    {
                        if (FileHelper.AddToLibrary(web, dataStream, "filename", "Documents", true, ref InsertedID, ref InsertedURL, ref IsSuccess, ref Message))
                        {
                            foreach (var item in data.DataInformation)
                            {
                                sqlGenerated = "INSERT INTO [Data].[GeneratedTransaction] ("
                                             + "[ApplicationID],"
                                             + "[FileName],"
                                             + "[ItemID],"
                                             + "[URL],"
                                             + "[IsDeleted],"
                                             + "[CreateDate],"
                                             + "[CreateBy]) "
                                             + "VALUES "
                                             + "('" + item.ApplicationID + "',"
                                             + "'" + "testbro" + "',"
                                             + "" + InsertedID + ","
                                             + "'" + InsertedURL + "',"
                                             + "" + "0" + ","
                                             + "'" + DateTime.UtcNow + "',"
                                             + "'" + "Unknown" + "')";

                                DataHelper.ExecuteDataTable(sqlGenerated);
                            }
                            
                        }
                    }

                    result = new GeneratedFileInfo
                    {
                        InsertedID = InsertedID,
                        InsertedURL = InsertedURL,
                        IsSuccess = IsSuccess,
                        Message = Message
                    };

                    web.AllowUnsafeUpdates = false;
                }
            }


            return result;
        }*/

        Test IGenerateFileServices.HelloWorld()
        {
            throw new NotImplementedException();
        }


    }
}
