var accessToken;
var $box;
var $remove = false;

var DocumentModels = ko.observable({FileName:'',DocumentPath:''});

/* dodit@2014.11.08:add format function */
var formatNumber = function (num) {
    if(num != null) {
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    }else{return 0;}
}

var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};

var UnderlyingDocumentModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var BizSegmentModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var BranchModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var TypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var DocumentTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var DocumentPurposeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var DocumentModel = {
    ID: ko.observable(),
    Type: ko.observable(DocumentTypeModel),
    Purpose: ko.observable(DocumentPurposeModel),
    DocumentPath: ko.observable(),
    LastModifiedDate: ko.observable(new Date),
    LastModifiedBy: ko.observable()
};

var RMModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    RankID: ko.observable(),
    Rank: ko.observable(),
    SegmentID: ko.observable(),
    Segment: ko.observable(),
    BranchID: ko.observable(),
    Branch: ko.observable(),
    Email: ko.observable()
};

var ContactModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    PhoneNumber: ko.observable(),
    DateOfBirth: ko.observable(),
    Address: ko.observable(),
    IDNumber: ko.observable()
};

var FunctionModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable(),
    LastModifiedBy: ko.observable(),
    LastModifiedDate: ko.observable()
};

var CurrencyModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var AccountModel = {
    AccountNumber: ko.observable(''),
    Currency: CurrencyModel,
    LastModifiedBy: ko.observable(),
    LastModifiedDate: ko.observable()
};

var SelectedModel = {
    BizSegment: ko.observable(),
    Branch: ko.observable(),
    Type: ko.observable(),
    UserApproval: ko.observable(),
    CurrencyAccount : ko.observable()
}


var UnderlyingDocumentModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var UnderlyingModel = {
    ID: ko.observable(),
    UnderlyingDocument: ko.observable(UnderlyingDocumentModel),
    DocumentType: ko.observable(DocumentTypeModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD:ko.observable(),
    AttachmentNo: ko.observable(),
    IsStatementLetter: ko.observable(false),
    IsDeclarationOfException: ko.observable(false),
    StartDate: ko.observable(),
    EndDate: ko.observable(),
    DateOfUnderlying: ko.observable(),
    ExpiredDate: ko.observable(),
    ReferenceNumber: ko.observable(),
    SupplierName: ko.observable(),
    InvoiceNumber: ko.observable()
};


var CustomerModel = {
    CIF: ko.observable(),
    Name: ko.observable(),
    POAName: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Branch: ko.observable(BranchModel),
    Type: ko.observable(TypeModel),
    RM: ko.observable(RMModel),
    Contacts: ko.observableArray([ContactModel]),
    Functions: ko.observableArray([FunctionModel]),
    Accounts: ko.observableArray([AccountModel]),
    Underlyings: ko.observableArray([UnderlyingModel])
};


var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;

    // Make show / hide grid view customer
    self.IsGridEnable = ko.observable(true);
    self.IsNewData = ko.observable(true);
    self.IsNewAccountData = ko.observable(true);


    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetDataCustomers));

    // set default sorting
    self.GridProperties().SortColumn("Name");
    self.GridProperties().SortOrder("ASC");

    // Options selected value
    self.Selected = ko.observable(SelectedModel);

    self.Customer = ko.observable(CustomerModel);
    self.Account = ko.observable(AccountModel);
    self.Function = ko.observable(FunctionModel);
    self.Contact = ko.observable(ContactModel);

    //parameters
    self.BizSegments = ko.observableArray([]);
    self.Branchs = ko.observableArray([]);
    self.Types = ko.observableArray([]);
    self.UserApprovals = ko.observableArray([]);
    self.Currencies = ko.observableArray([]);

    // filter
    self.FilterCIF = ko.observable('');
    self.FilterName = ko.observable("");
    self.FilterPOAName = ko.observable("");
    self.FilterBizSegment = ko.observable("");
    self.FilterBranch = ko.observable("");
    self.FilterType = ko.observable("");
    self.FilterLastModifiedBy = ko.observable("");
    self.FilterLastModifiedDate = ko.observable('');

    // Declare an ObservableArray for Storing the JSON Response
    self.Customers = ko.observableArray([]);
    self.Accounts = ko.observableArray([]);

    // bind clear filters
    self.ClearCustomerFilters = function () {
        self.FilterCIF("");
        self.FilterName("");
        self.FilterPOAName("");
        self.FilterBizSegment("");
        self.FilterBranch("");
        self.FilterType("");
        self.FilterLastModifiedBy("");
        self.FilterLastModifiedDate('');

        GetDataCustomers();
    };

    self.GetDataCustomers = function()
    {
        GetDataCustomers();
    }

    self.GetParameters = function(){
        GetParameters();
    }



    // Get filtered columns value
    function GetCustomerFilteredColumns(){
        // define filter
        var filters = [];

        if (self.FilterCIF() != "") filters.push({ Field: 'CIF', Value: self.FilterCIF() });
        if (self.FilterName() != "") filters.push({ Field: 'Name', Value: self.FilterName() });
        if (self.FilterPOAName() != "") filters.push({ Field: 'POAName', Value: self.FilterPOAName() });
        if (self.FilterBizSegment() != "") filters.push({ Field: 'BizSegment', Value: self.FilterBizSegment() });
        if (self.FilterBranch() != "") filters.push({ Field: 'Branch', Value: self.FilterBranch() });
        if (self.FilterType() != "") filters.push({ Field: 'Type', Value: self.FilterType() });
        if (self.FilterLastModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterLastModifiedBy() });
        if (self.FilterLastModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterLastModifiedDate() });

        return filters;
    };

    // Customer insert new
    self.NewData = function () {
        self.IsNewData(true);
        self.Customer().CIF('');
        self.Customer().Name('');
        self.Customer().POAName('');
        self.Selected().BizSegment('');
        self.Selected().Branch('');
        self.Selected().Type('');
        self.Selected().UserApproval('');
    };

    self.GetSelectedRow = function (data) {
        self.IsNewData(false);
        $("#modal-form").modal('show');
        self.Customer().CIF(data.CIF);
        self.Customer().Name(data.Name);
        self.Customer().POAName(data.POAName);
        self.Selected().BizSegment(data.BizSegment.ID);
        self.Selected().Branch(data.Branch.ID);
        self.Selected().Type(data.Type.ID);
        self.Selected().UserApproval(data.RM.ID);
        SelectedCustomer(data.CIF);
    }

    self.GetAccountSelectedRow = function (data)
    {

        if(data != null ){
            self.Account().AccountNumber(data.AccountNumber()) ;
            //var tmpData = ko.mapping.formJS(data);
           self.Selected().CurrencyAccount(data.Currency);
        }
        $('#account-modal-form').modal('show');
        $('#backDrop').show();

    }
    self.GetFunctionSelectedRow= function (data)
    {
        alert('Select Function');
    }

    //Function to Read All Customers
    function GetDataCustomers() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customer,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetCustomerFilteredColumns();

        if(filters.length > 0){
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataCustomers, OnError, OnAlways);
        }else{
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataCustomers, OnError, OnAlways);
        }
    }

    function SelectedCustomer(cif)
    {
        $.ajax({
            type: "GET",
            url: api.server + api.url.customer+"/"+cif,
            contentType: "application/json",
            headers: {
                "Authorization" : "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if(jqXHR.status = 200){
                    var tempData = ko.mapping.fromJS(data);
                    tempData().Currency = ko.observable(tempData().Currency);

                    self.Customer(tempData);
                }else{
                    ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }


    self.save = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if(form.valid()){
            self.PopulateSelected();
            //Ajax call to insert the Customers
            $.ajax({
                type: "POST",
                url: api.server + api.url.customer,
                data: ko.toJSON(self.Customer()), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization" : "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if(jqXHR.status = 200){
                        // send notification
                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                        // refresh data
                        GetDataCustomers();
                        self.IsGridEnable(true);
                        self.IsNewData(true);
                    }else{
                        ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }
    };

    self.update = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if(form.valid()){
            // hide current popup window

            bootbox.confirm("Are you sure?", function(result) {
                if(!result) {
                    $("#modal-form").modal('show');
                }else{
                    self.PopulateSelected();
                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.customer + "/" + self.Customer().CIF(),
                        data: ko.toJSON(self.Customer()),
                        contentType: "application/json",
                        headers: {
                            "Authorization" : "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if(jqXHR.status = 200){
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                                // refresh data
                                GetDataCustomers();
                                self.IsGridEnable(true);
                                self.IsNewData(true);
                            }else{
                                ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.delete = function()
    {
        alert('delete');
    }

    self.AddAccount = function()
    {
        $('#account-modal-form').modal('show');
        $('#backDrop').show();
        self.IsNewData(true);
        self.Account().AccountNumber('');
        self.Selected().CurrencyAccount('');
    }

    // On success GetData callback
    function OnSuccessGetDataCustomers(data, textStatus, jqXHR){
        if(jqXHR.status = 200){
            self.Customers(data.Rows);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        }else{
            ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // get all parameters need
    function GetParameters() {
        var options = {
            url: api.server + api.url.parameter,
            params: {
                select: "BizSegment,branch,customertype,userapproval,currency"
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
    }

    // Event handlers declaration start
    function OnSuccessGetParameters(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            self.BizSegments(data.BizSegment);
            self.Branchs(data.Branch);
            self.Types(data.CustomerType);
            self.UserApprovals(data.UserApproval);
            self.Currencies(data.Currency);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    self.PopulateSelected = function() {
        // Biz Segement
        var data = ko.utils.arrayFirst(self.BizSegments(),
            function (item) {
                return item.ID == self.Selected().BizSegment();
            });
        if (data != null) {
            self.Customer().BizSegment(ObjectToObservable(data));
        }
        // Branch
        var data = ko.utils.arrayFirst(self.Branchs(),
            function (item) {
                return item.ID == self.Selected().Branch();
            });
        if (data != null) {
            self.Customer().Branch(ObjectToObservable(data));
        }
        // Customer Type
        var data = ko.utils.arrayFirst(self.Types(),
            function (item) {
                return item.ID == self.Selected().Type();
            });
        if (data != null) {
            self.Customer().Type(ObjectToObservable(data));
        }
        // RM (User Approval)
        var data = ko.utils.arrayFirst(self.UserApprovals(),
            function (item) {
                return item.ID == self.Selected().UserApproval();
            });
        if (data != null) {
            self.Customer().RM(ObjectToObservable(data));
        }
        // Currency Account
        var data = ko.utils.arrayFirst(self.Currencies(),
            function(item){
                return item.ID == self.Selected().CurrencyAccount();
            });
        if (data != null)
        {
            self.Account().Currency(ObjectToObservable(data));
        }

    }
    function ObjectToObservable(data)
    {
        var tmpData = ko.mapping.fromJS(data);
        return tmpData;

    }
    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown){
        ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways(){
        $box.trigger('reloaded.ace.widget');
    }
    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);
        if(date =='1970/01/01 00:00:00' || date==null){
            return "";
        }

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return ""
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    <!-- Start All Function Account Customer -->
    self.save_account = function ()
    {
        //console.log("save Account");
        self.PopulateSelected();
        self.Customer().Accounts.push(self.Account);
        $('#account-modal-form').modal('hide');
        $('#backDrop').hide();
    }
    self.update_account = function ()
    {

        $('#account-modal-form').modal('hide');
        $('#backDrop').hide();
    }
    self.delete_account = function ()
    {
        self.Customer().Accounts.Remove(self.Account());
        $('#account-modal-form').modal('hide');
        $('#backDrop').hide();
    }
    <!-- End All Function Account Customer -->
};

$(document).ready(function () {
    // widget loader
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    /// block enter key from user to prevent submitted data.
    $("input").keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

            return false;
        }
    });

    // scrollables
    $('.slim-scroll').each(function () {
        var $this = $(this);
        $this.slimscroll({
            height: $this.data('height') || 100,
            //width: $this.data('width') || 100,
            railVisible: true,
            alwaysVisible: true,
            color: '#D15B47'
        });
    });

    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);

    // Token Validation
    if($.cookie(api.cookie.name) == undefined){
        Helper.Token.Request(TokenOnSuccess, TokenOnError);
    }else{
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // call spuser function
        GetCurrentUser(viewModel);

        // call get data inside view model
        viewModel.GetDataCustomers();
        viewModel.GetParameters();
    }

    $('.date-picker').datepicker({autoclose: true}).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    //$("#aspnetForm").validate({onsubmit: false});
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });
});

// Get SPUser from cookies
function GetCurrentUser(vm) {
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);

        vm.SPUser = spUser;
    }
}

// Token validation On Success function
function TokenOnSuccess(data, textStatus, jqXHR){
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // call spuser function
    GetCurrentUser();

    // call get data inside view model
    viewModel.GetParameters();
    viewModel.GetData();
}

// Token validation On Error function
function TokenOnError(jqXHR, textStatus, errorThrown){
    ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

