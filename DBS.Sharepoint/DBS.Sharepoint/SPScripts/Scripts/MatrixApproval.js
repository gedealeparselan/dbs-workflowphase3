var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;
    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);
    //Declare observable which will be bind with UI
    self.ID = ko.observable("");
    self.DetailID = ko.observable("");
    self.Name = ko.observable("");
    self.Description = ko.observable("");
    self.MailSubject = ko.observable("");
    self.MailBody = ko.observable("");
    self.MatrixApprovalID = ko.observable("");
    self.MatrixApprovalName = ko.observable("");
    self.IsRank = ko.observable(false);
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");

    // filter
    self.FilterName = ko.observable("");
    self.FilterDescription = ko.observable("");
    //self.FilterApproval = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");

    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    // New Data flag
    self.IsNewData = ko.observable(false);
    self.IsApprovalPage = ko.observable(false);

    // Show Grid for Function Role or Rank
    self.IsGridShow = ko.observable(false);


    // Declare an ObservableArray for Storing the JSON Response
    self.MatrixApprovals = ko.observableArray([]);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    // set default sorting
    self.GridProperties().SortColumn("Name");
    self.GridProperties().SortOrder("ASC");

    //dropdown
    self.ddlField = ko.observableArray([]);
    self.ddlValue = ko.observableArray([]);

    self.FieldText = ko.observable("");
    self.FieldDetails = ko.observableArray([new MatrixFieldDetailModel('', '', [new MatrixFieldSubDetailModel('', '', '')])]);
    self.FieldSubDetails = ko.observableArray([new MatrixFieldSubDetailModel('', '', '')]);

    self.Matrix = ko.observableArray([]);
    self.MatrixDetails = ko.observableArray([]);
    self.MatrixSubDetails = ko.observableArray([]);


    // New Index Header
    self.Index = ko.observable(0);

    // bind clear filters
    self.ClearFilters = function () {
        self.FilterName("");
        self.FilterDescription("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");

        GetData();
    };


    // bind get data function to view
    self.GetData = function () {
        GetData();
    };

    self.GetParameters = function () {
        GetParameters();
    }

    //The Object which stored data entered in the observables
    var MatrixApproval = {
        ID: self.ID,
        Name: self.Name,
        Description: self.Description,
        MailSubject: self.MailSubject,
        MailBody: self.MailBody,
        IsRank: self.IsRank,
        MatrixDetails: ko.observableArray([self.FieldDetails]),
        MatrixSubDetails: ko.observableArray([]),
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
    };

    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    console.log("spUser Roles : " + ko.toJSON(spUser.Roles));
                    console.log("spUser Permited : " + data);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    self.save = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {

                    var mailBodyText = CKEDITOR.instances['editor1'].getData();
                    var fieldID = $('#ddlField option:selected').val();
                    var fieldName = $('#ddlField option:selected').text();
                    self.MailBody(mailBodyText);

                    self.FieldDetails().FieldID = fieldID;
                    self.FieldDetails().Name = fieldName;


                    //Ajax call to insert the MatrixApprovals
                    if (MatrixApproval.MatrixSubDetails().length <= 0) {
                        ShowNotification("Form Validation Warning", "Field SubDetail must be selected.", 'gritter-warning', true);
                        return;
                    }

                    for (var j = 0, max = MatrixApproval.MatrixSubDetails().length; j < max; j++) {


                        self.FieldDetails().MatrixSubDetails.push(MatrixApproval.MatrixSubDetails()[j]);
                        self.FieldDetails().MatrixSubDetails()[j].FieldValueID = MatrixApproval.MatrixSubDetails()[j].ID;

                        if (ko.toJSON(MatrixApproval.IsRank) == "true") {

                            self.FieldDetails().MatrixSubDetails()[j].RankID = MatrixApproval.MatrixSubDetails()[j].ID;
                            self.FieldDetails().MatrixSubDetails()[j].FunctionRoleID = null;

                        } else if (ko.toJSON(MatrixApproval.IsRank) == "false") {

                            self.FieldDetails().MatrixSubDetails()[j].RankID = null;
                            self.FieldDetails().MatrixSubDetails()[j].FunctionRoleID = MatrixApproval.MatrixSubDetails()[j].ID;


                        }

                    }


                    delete MatrixApproval.MatrixSubDetails;

                    //console.log(ko.toJSON(MatrixApproval));
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.matrixapproval,
                        data: ko.toJSON(MatrixApproval), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {

                            if (jqXHR.status = 200) {
                                // send notification
                                //console.log(data.ID);
                                if (data.ID != 0) {

                                    ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                } else {

                                    ShowNotification('Warning', jqXHR.responseJSON.Message, 'gritter-warning', true);

                                }
                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();

                            } else {

                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification

                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.update = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            // hide current popup window
            //$("#modal-form").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    var mailBodyText = CKEDITOR.instances['editor1'].getData();
                    var fieldID = $('#ddlField option:selected').val();
                    var fieldName = $('#ddlField option:selected').text();
                    self.MailBody(mailBodyText);

                    self.FieldDetails().FieldID = fieldID;
                    self.FieldDetails().Name = fieldName;


                    //Ajax call to insert the MatrixApprovals
                    if (MatrixApproval.MatrixSubDetails().length <= 0) {
                        ShowNotification("Form Validation Warning", "Field SubDetail must be selected.", 'gritter-warning', true);
                        return;
                    }

                    for (var j = 0, max = MatrixApproval.MatrixSubDetails().length; j < max; j++) {

                        self.FieldDetails().MatrixSubDetails.push(MatrixApproval.MatrixSubDetails()[j]);
                        self.FieldDetails().MatrixSubDetails()[j].FieldValueID = MatrixApproval.MatrixSubDetails()[j].ID;

                        if (ko.toJSON(MatrixApproval.IsRank) == "true") {

                            self.FieldDetails().MatrixSubDetails()[j].RankID = MatrixApproval.MatrixSubDetails()[j].ID;
                            self.FieldDetails().MatrixSubDetails()[j].FunctionRoleID = null;

                        } else if (ko.toJSON(MatrixApproval.IsRank) == "false") {

                            self.FieldDetails().MatrixSubDetails()[j].RankID = null;
                            self.FieldDetails().MatrixSubDetails()[j].FunctionRoleID = MatrixApproval.MatrixSubDetails()[j].ID;


                        }

                    }

                    delete MatrixApproval.MatrixSubDetails;
                    //console.log(ko.toJSON(MatrixApproval));

                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.matrixapproval + "/" + MatrixApproval.ID(),
                        data: ko.toJSON(MatrixApproval),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification

                                if (data.ID != 0) {

                                    ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                } else {

                                    ShowNotification('Warning', jqXHR.responseJSON.Message, 'gritter-warning', true);

                                }

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.delete = function () {
        // hide current popup window
        //$("#modal-form").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer

                for (var j = 0, max = MatrixApproval.MatrixSubDetails().length; j < max; j++) {

                    self.FieldDetails().MatrixSubDetails.push(MatrixApproval.MatrixSubDetails()[j]);
                    self.FieldDetails().MatrixSubDetails()[j].FieldValueID = MatrixApproval.MatrixSubDetails()[j].ID;

                    if (ko.toJSON(MatrixApproval.IsRank) == "true") {

                        self.FieldDetails().MatrixSubDetails()[j].RankID = MatrixApproval.MatrixSubDetails()[j].ID;
                        self.FieldDetails().MatrixSubDetails()[j].FunctionRoleID = null;

                    } else if (ko.toJSON(MatrixApproval.IsRank) == "false") {

                        self.FieldDetails().MatrixSubDetails()[j].RankID = null;
                        self.FieldDetails().MatrixSubDetails()[j].FunctionRoleID = MatrixApproval.MatrixSubDetails()[j].ID;


                    }


                }
                delete MatrixApproval.MatrixSubDetails;

                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.matrixapproval + "/" + MatrixApproval.ID(),
                    data: ko.toJSON(MatrixApproval), //Convert the Observable Data into JSON
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {

                            if (data.ID != 0) {

                                ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            } else {

                                ShowNotification('Warning', jqXHR.responseJSON.Message, 'gritter-warning', true);

                            }

                            // refresh data
                            GetData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    function AddListItem(ActionType, FieldID) {
        var body = {
            Title: FieldID.toString(),
            ActionType: ActionType,
            __metadata: {
                type: config.sharepoint.metadata.listMaster
            }
        };

        var options = {
            url: config.sharepoint.url.api + "/Lists(guid'" + config.sharepoint.listIdMaster + "')/Items",
            data: JSON.stringify(body),
            digest: jQuery("#__REQUESTDIGEST").val()
        };

        Helper.Sharepoint.List.Add(options, OnSuccessAddListItem, OnError, OnAlways);
    }
    function OnSuccessAddListItem(data, textStatus, jqXHR) {
        // clear transaction model
        //viewModel.TransactionModel(null);

        // send notification
        //ShowNotification('Submit Success', 'New transaction has been submitted at '+ viewModel.LocalDate(data.d.Created), 'gritter-success', false);

        // redirect to all transaction
        //window.location = "/home/all-transaction";
        //bangkit
        //window.location = "/home";
    }


    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    self.selectedChkbox = function (selectedItem) {
        var selectedIndex = self.FieldSubDetails().indexOf(selectedItem);
        var itemFounded = false;

        var selectedValChkbox = $('#checkbox' + selectedIndex).val();
        //console.log(selectedValChkbox);
        if ($('#checkbox' + selectedIndex).is(':checked')) {
            for (var i = 0, max = MatrixApproval.MatrixSubDetails().length; i < max ; i++) {
                if (ko.toJSON(MatrixApproval.MatrixSubDetails()[i]) == selectedIndex) {
                    itemFounded = true;
                }
            }
            if (!itemFounded) {
                MatrixApproval.MatrixSubDetails().push(selectedItem);
            }
        } else {

            MatrixApproval.MatrixSubDetails.remove(function (selectedItem) { return selectedItem.ID == selectedValChkbox });


        }
        //console.log(ko.toJSON(MatrixApproval.MatrixSubDetails));
    }

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {

        //GetParameters();		
        self.IsNewData(false);

        if ($('#name').is(':disabled')) {
            self.IsApprovalPage(true);
        }

        self.IsGridShow(true);
        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');

        $('#ddlFieldApproval').val(data.MatrixDetails[0].FieldID);
        self.FieldDetails(new MatrixFieldDetailModel(data.MatrixDetails[0].FieldID, data.MatrixDetails[0].Name, []));
        //self.FieldDetails(data.MatrixDetails[0].FieldID, data.MatrixDetails[0].Name,[]);	

        self.OnGridShow(data.MatrixDetails[0].MatrixSubDetails, true);

        var url = window.location.href.toLowerCase();

        if (url.indexOf('parameter-maintenance') > -1) {
            CKEDITOR.instances['editor1'].setData(data.MailBody);
            $('#editorapproval1').hide();
        } else {
            $('#editor1').hide();
            $('#editorapproval1')[0].innerHTML = data.MailBody;

        }
        self.ID(data.ID);
        self.Name(data.Name);
        self.MailSubject(data.MailSubject);
        self.MailBody(data.MailBody);
        self.Description(data.Description);

        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);

    };

    //insert new
    self.NewData = function () {
        // flag as new Data
        self.IsNewData(true);
        self.IsGridShow(false);
        self.Readonly(false);
        self.IsApprovalPage(false);
        // bind empty data
        self.ID(0);
        self.Name('');
        self.MailSubject('');
        self.MailBody('');
        self.Description('');
        self.FieldSubDetails([]);
        self.FieldDetails(new MatrixFieldDetailModel('', '', []));
        $('#editorapproval1').hide();
        if (MatrixApproval.MatrixSubDetails != undefined)
            MatrixApproval.MatrixSubDetails([]);
        ClearMailBody();


    };
    self.ClearMailBody = function () {
        ClearMailBody();
    }
    function ClearMailBody() {
        CKEDITOR.instances['editor1'].setData("");
    }
    //Field dropdown change
    self.OnGridShow = function (FieldSubDetails, state) {
        //console.log(FieldSubDetails); 	

        var fieldValue = $('#ddlField option:selected').val();
        var fieldText = $('#ddlField option:selected').text();

        if (fieldValue == '' || fieldText == 'Please Select...') {
            self.IsGridShow(false);
            self.FieldText('');

        } else {
            self.IsGridShow(true);
            self.FieldText(fieldText);
            GetRankOrFunctionRole(FieldSubDetails, state);
        }

    };
    function GetRankOrFunctionRole(FieldSubDetails, state) {
        var FieldValue = '';
        var FieldSelected;
        FieldSelected = $('#ddlField option:selected').text();

        switch (FieldSelected) {
            case 'Rank':
                FieldValue = 'Rank';
                self.IsRank(true);
                break;
            case 'Role':
                FieldValue = 'FunctionRole';
                self.IsRank(false);
                break;
        }

        $.ajax({
            type: "GET",
            async: false,
            url: api.server + api.url.parameter + '?select=' + FieldValue,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.FieldSubDetails(data[FieldValue]);

                    if (MatrixApproval.MatrixSubDetails == undefined) {
                        MatrixApproval.MatrixSubDetails = ko.observableArray([]);

                    }

                    if (state) {
                        //console.log(FieldSubDetails);
                        $.each(FieldSubDetails, function (index, item) {
                            $(':checkbox[value=' + item.FieldValueID + ']').prop('checked', true);
                            item.ID = item.FieldValueID;
                            MatrixApproval.MatrixSubDetails.push({ "ID": item.ID.toString(), "Name": "", "Description": "", "LastModifiedDate": "", "LastModifiedBy": "" });
                        });
                    } else {
                        MatrixApproval.MatrixSubDetails([]);
                    }

                    //console.log(ko.toJSON(MatrixApproval.MatrixSubDetails));

                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });


    }

    //Function to Read All Customers
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.matrixapproval,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }

    // Function to Get All Parameter Dropdown

    function GetParameters() {
        $.ajax({
            type: "GET",
            async: false,
            url: api.server + api.url.parameter + '?select=MatrixField',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.ddlField(data['MatrixField']);
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });

        /*$.ajax({
            type: "GET",
            url: api.server + api.url.matrixapproval +'/Parameters',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization" : "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if(jqXHR.status = 200){
					console.log(data);
                }else{
                    ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });		*/

    }

    // Function onChangeObject
    function changeObject(matrixs) {
        var id = self.ID();
        var name = self.Name();
        var desc = self.Description();
        self.MatrixResultModels([]);
        self.MatrixsResultModels([]);
        self.SubMatrixsResultModels([]);
        for (var i = 0 ; i < matrixs.length; i++) {
            var fieldID = matrixs[i].selectDetail();
            for (var j = 0; j < matrixs[i].Subdetails().length; j++) {
                var subdetailID = matrixs[i].Subdetails()[j].selectSubdetail();
                var subdetail = new SubMatrix(fieldID, subdetailID);
                self.SubMatrixsResultModels.push(subdetail);
            }
            var tmpmatrixs = new Matrixs(fieldID, self.SubMatrixsResultModels());
            self.MatrixsResultModels.push(tmpmatrixs);
            self.SubMatrixsResultModels([]);
        }
        var resultMatrixModel = new MatrixModels(id, name, desc, self.MatrixsResultModels());
        self.MatrixResultModels(resultMatrixModel);
    }

    // Function Set subdetail dropdown
    /*function SetSubdetailDdl(fieldId,isNew,indexHeader){
        var detailIdx = indexHeader;// self.Details().length -1;
        var subdetailIdx = self.Details()[detailIdx].Subdetails().length -1;
        for(var i=0; i<=subdetailIdx ;i++){
            switch(fieldId)
            {
                case 1:
                    if(isNew)
                    {
                        self.Details()[detailIdx].Subdetails.push(new SubMatrixModel(1,self.ddlRanks()));
                        return;
                    }else{
                        self.Details()[detailIdx].Subdetails()[i].ddlSubdetails(self.ddlRanks());
                    }
                    break;
                case 2:
                    if(isNew)
                    {
                        self.Details()[detailIdx].Subdetails.push(new SubMatrixModel(1,self.ddlSegments()));
                        return;
                    }else{

                        self.Details()[detailIdx].Subdetails()[i].ddlSubdetails(self.ddlSegments());
                    }
                    break;
                case 3:
                    if(isNew){
                        self.Details()[detailIdx].Subdetails.push(new SubMatrixModel(1,self.ddlRoles()));
                        return;
                    }else{
                        self.Details()[detailIdx].Subdetails()[i].ddlSubdetails(self.ddlRoles());
                    }
                    break;
                case 4:
                    if(isNew){
                        self.Details()[detailIdx].Subdetails.push(new SubMatrixModel(1,self.ddlBranchs()));
                        return;
                    }else{
                        self.Details()[detailIdx].Subdetails()[i].ddlSubdetails(self.ddlBranchs());
                    }
                    break;
                default:
                    //self.tmpDdlSubdetails.removeAll();
                    break;
            }
        }
    }
	*/

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterName() != "") filters.push({ Field: 'Name', Value: self.FilterName() });
        if (self.FilterDescription() != "") filters.push({ Field: 'Description', Value: self.FilterDescription() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

        return filters;
    };

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.MatrixApprovals(data.Rows);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });

    // text editor (Body Email from Exception Handling)
    function showErrorAlert(reason, detail) {
        var msg = '';
        if (reason === 'unsupported-file-type') { msg = "Unsupported format " + detail; }
        else {
            console.log("error uploading file", reason, detail);
        }
        $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
    }

    //$('#editor1').ace_wysiwyg();//this will create the default editor will all buttons
    //but we want to change a few buttons colors for the third style
    /*$('#editor1').ace_wysiwyg({
        toolbar:
            [
                'font',
                null,
                'fontSize',
                null,
                {name:'bold', className:'btn-info'},
                {name:'italic', className:'btn-info'},
                {name:'strikethrough', className:'btn-info'},
                {name:'underline', className:'btn-info'},
                null,
                {name:'insertunorderedlist', className:'btn-success'},
                {name:'insertorderedlist', className:'btn-success'},
                {name:'outdent', className:'btn-purple'},
                {name:'indent', className:'btn-purple'},
                null,
                {name:'justifyleft', className:'btn-primary'},
                {name:'justifycenter', className:'btn-primary'},
                {name:'justifyright', className:'btn-primary'},
                {name:'justifyfull', className:'btn-inverse'},
                null,
                {name:'createLink', className:'btn-pink'},
                {name:'unlink', className:'btn-pink'},
                null,
                null,//{name:'insertImage', className:'btn-success'},
                null,
                'foreColor',
                null,
                {name:'undo', className:'btn-grey'},
                {name:'redo', className:'btn-grey'}
            ],
        'wysiwyg': {
            fileUploadError: showErrorAlert
        }
    }).prev().addClass('wysiwyg-style2');*/


    $('[data-toggle="buttons"] .btn').on('click', function (e) {
        var target = $(this).find('input[type=radio]');
        var which = parseInt(target.val());
        var toolbar = $('#editor1').prev().get(0);
        if (which == 1 || which == 2 || which == 3) {
            toolbar.className = toolbar.className.replace(/wysiwyg\-style(1|2)/g, '');
            if (which == 1) $(toolbar).addClass('wysiwyg-style1');
            else if (which == 2) $(toolbar).addClass('wysiwyg-style2');
        }
    });


    if (typeof jQuery.ui !== 'undefined' && /applewebkit/.test(navigator.userAgent.toLowerCase())) {

        var lastResizableImg = null;
        function destroyResizable() {
            if (lastResizableImg == null) return;
            lastResizableImg.resizable("destroy");
            lastResizableImg.removeData('resizable');
            lastResizableImg = null;
        }

        var enableImageResize = function () {
            $('.wysiwyg-editor')
                .on('mousedown', function (e) {
                    var target = $(e.target);
                    if (e.target instanceof HTMLImageElement) {
                        if (!target.data('resizable')) {
                            target.resizable({
                                aspectRatio: e.target.width / e.target.height,
                            });
                            target.data('resizable', true);

                            if (lastResizableImg != null) {//disable previous resizable image
                                lastResizableImg.resizable("destroy");
                                lastResizableImg.removeData('resizable');
                            }
                            lastResizableImg = target;
                        }
                    }
                })
                .on('click', function (e) {
                    if (lastResizableImg != null && !(e.target instanceof HTMLImageElement)) {
                        destroyResizable();
                    }
                })
                .on('keydown', function () {
                    destroyResizable();
                });
        }
        enableImageResize();
    }
};

$.holdReady(true);
var intervalRoleName = setInterval(function () {
    if (Object.keys(Const_RoleName).length != 0) {
        $.holdReady(false);
        clearInterval(intervalRoleName);
    }
}, 100);

//$(document).on("RoleNameReady", function() {
$(document).ready(function () {
    var url = window.location.href.toLowerCase();

    if (url.indexOf('parameter-maintenance') > -1) {
        var CKEDITORScript = '';
        CKEDITORScript = document.createElement('script');
        CKEDITORScript.type = 'text/javascript';
        CKEDITORScript.src = '/SiteAssets/Scripts/ckeditor/ckeditor.js';
        document.getElementsByTagName('head')[0].appendChild(CKEDITORScript);
    }


});