﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AceNavigation.Model
{
    public class RoleModel
    {
        public int RoleID { get; set; }
        public string RoleName { get; set; }

    }
}
