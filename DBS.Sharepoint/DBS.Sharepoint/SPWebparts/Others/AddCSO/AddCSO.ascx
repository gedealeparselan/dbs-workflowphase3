﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddCSO.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.Others.AddCSO.AddCSO" %>

<div class="row">
    <div class="row">
        <div class="col-xs-12">
            <div id="transaction-form" class="form-horizontal" role="form">

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right">
                        <span data-bind="text: $root.LocalDate(TransactionFD().CreateDate(), true, true)"></span>
                    </label>

                    <label class="col-sm-4 pull-right">
                        <b>Application ID</b> : <span data-bind="text: TransactionFD().ApplicationID"></span>
                        <br />
                        <b>User Name</b> : <span data-bind="text: $root.SPUser().DisplayName"></span>
                    </label>
                </div>

                <div class="hr hr8 hr-double"></div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div id="dataTable" class="widget-box"></div>
                    </div>
                </div>
            </div>
            <br />
            <div class="form-group">
                <label class="col-sm-3">
                    Attachment
                </label>

                <label class="col-sm-5 pull-right">
                    <button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.UploadDocument">
                        <i class="icon-plus"></i>
                        Attach
                    </button>
                </label>
            </div>
            <table class="table table-striped table-bordered table-hover dataTable">
                <thead>
                    <tr>
                        <th style="width: 50px">No.</th>
                        <th>File Name</th>
                        <th>Purpose of Docs</th>
                        <th>Type of Docs</th>
                        <th>Modified</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody data-bind="foreach: $root.Documents, visible: $root.Documents().length > 0">
                    <tr>
                        <td><span data-bind="text: $index() + 1"></span></td>
                        <td><span data-bind="text: DocumentPath.name"></span></td>
                        <td><span data-bind="text: Purpose.Name"></span></td>
                        <td><span data-bind="text: Type.Name"></span></td>
                        <td><span data-bind="text: LastModifiedDate"></span></td>
                        <td><a href="#" data-bind="click: $root.RemoveDocument">Remove</a></td>
                    </tr>
                </tbody>
                <tbody data-bind="visible: $root.Documents().length == 0">
                    <tr>
                        <td colspan="13" align="center">No entries available.</td>
                    </tr>
                </tbody>
            </table>
            <!-- Modal upload form start -->
            <div id="modal-form-upload" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body overflow-visible">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div id="customer-form" class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="document-type">Document Type</label>

                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <select id="document-type" name="document-type" data-rule-required="true" data-rule-value="true" data-bind="options: $root.Parameter().DocumentTypes, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Options..', value: $root.DocumentType"></select>
                                                    <label class="control-label bolder text-danger">*</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="space-4"></div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="document-purpose">Purpose of Docs</label>

                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <select id="document-purpose" name="document-purpose" data-rule-required="true" data-rule-value="true" data-bind="options: $root.Parameter().DocumentPurposes, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Options..', value: $root.DocumentPurpose"></select>
                                                    <label class="control-label bolder text-danger">*</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="space-4"></div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="document-path">Document</label>

                                            <div class="col-sm-8">
                                                <div class="clearfix">
                                                    <input type="file" id="document-path" name="document-path" data-bind="file: DocumentPath" />
                                                </div>
                                            </div>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button class="btn btn-sm btn-primary" data-bind="click: $root.AddDocument">
                                <i class="icon-save"></i>
                                Save
                            </button>
                            <button class="btn btn-sm" data-dismiss="modal">
                                <i class="icon-remove"></i>
                                Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal upload form end -->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.save_u">
                    <i class="icon-save"></i>
                    Submit
                </button>
                <button class="btn btn-sm btn-primary">
                    <i class="icon-edit"></i>
                    Draft
                </button>
                <button class="btn btn-sm" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/CSOFD.js"></script>

<link rel="stylesheet" media="screen" href="/SiteAssets/Scripts/handsontable/dist/handsontable.full.css">
<script src="/SiteAssets/Scripts/handsontable/dist/handsontable.full.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/parser.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/ruleJS.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/lodash.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/underscore.string.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/moment.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/numeral.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/md5.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/jstat.js"></script>
