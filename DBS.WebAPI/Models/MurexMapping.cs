﻿using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DBS.WebAPI.Models
{
    #region Property
    public class MurexMappingModel
    {
        public int ID { set; get; }
        public string MurexName { set; get; }
        public string CIF {set; get;}
        public string CustomerName { set; get; }
        public bool IsDeleted { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public CustomerModel Customer { get; set; }
    }
    public class MurexMapppingRow : MurexMappingModel
    {
        public int RowID { get; set; }
    }

    public class MurexMappingGrid : Grid
    {
        public IList<MurexMapppingRow> Rows { get; set; }
    }
    #endregion

    #region Filter
    public class MurexMappingFilter : Filter
    {

    }
    #endregion

    #region Interface
    public interface IMurexMappingRepository : IDisposable
    {
        bool AddMurex(MurexMappingModel Murex, ref string message);
        bool UpdateMurex(int ID, MurexMappingModel Murex, ref string message);
        bool DeleteMurex(int id, ref string message);
        bool GetMurexByPagination(int page, int size, IList<MurexMappingFilter> filters, string sortColumn, string sortOrder, ref MurexMappingGrid MurexGrid, ref string message);
        bool GetMurexByLimit(ref IList<MurexMappingModel> Murex, int limit, int index, ref string message);
        bool GetMurexBySearch(string key, int limit, ref IList<MurexMappingModel> Murex, ref string message);
        bool GetMurexAll(ref IList<MurexMappingModel> Murex, ref string message);
        bool GetMurexById(int id, ref MurexMappingModel Murex, ref string message);
    }
    #endregion

    #region Repository
    public class MurexMappingRepository : IMurexMappingRepository
    {
        private bool disposed = false;
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public bool AddMurex(MurexMappingModel Murex, ref string message)
        {
            bool IsSuccess = false;
            if (Murex != null)
            {
                try
                {
                    if (!context.MurexMappings.Where(a => a.MurexName.Trim().ToLower() == Murex.MurexName.Trim().ToLower() && a.CIF.Trim().ToLower() == Murex.Customer.CIF.Trim().ToLower() && a.IsDeleted == false).Any())
                    {
                        context.MurexMappings.Add(new Entity.MurexMapping()
                        {
                            MurexName = Murex.MurexName,
                            CIF = Murex.Customer.CIF,
                            IsDeleted = false,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().LoginName
                        });

                        context.SaveChanges();
                        IsSuccess = true;
                    }
                    else
                    {
                        message = string.Format("Murex data for Murex Name {0} and CIF {1} are already exist.", Murex.MurexName, Murex.Customer.CIF);
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return IsSuccess;
        }
        public bool UpdateMurex(int ID, MurexMappingModel Murex, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                Entity.MurexMapping data = context.MurexMappings.Where(a => a.MurexMappingID.Equals(ID)).SingleOrDefault();

                if (data != null)
                {
                    if (!context.MurexMappings.Where(a => a.MurexName.Trim().ToLower() == Murex.MurexName.Trim().ToLower() && a.CIF.Trim().ToLower() == Murex.Customer.CIF.Trim().ToLower() && a.IsDeleted == false).Any())
                    {
                        data.MurexName = Murex.MurexName;
                        data.CIF = Murex.Customer.CIF;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        context.SaveChanges();
                        IsSuccess = true;
                    }
                    else
                    {
                        message = string.Format("Murex data for Murex Name {0} and CIF {1} are already exist.", Murex.MurexName, Murex.Customer.CIF);
                    }
                }
                else
                {
                    message = string.Format("Murex data for Murex ID {0} is does not exist.", ID);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return IsSuccess;
        }
        public bool DeleteMurex(int id, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                Entity.MurexMapping data = context.MurexMappings.Where(a => a.MurexMappingID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().LoginName;

                    context.SaveChanges();

                    IsSuccess = true;
                }
                else
                {
                    message = string.Format("Murex data for Murex ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return IsSuccess;
        }
        public bool GetMurexByPagination(int page, int size, IList<MurexMappingFilter> filters, string sortColumn, string sortOrder, ref MurexMappingGrid MurexGrid, ref string message)
        {
            bool isSuccess = false;
            try
            {
                MurexMappingModel filter = new MurexMappingModel();
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();
                filter.Customer = new CustomerModel { CIF = string.Empty };

                if (filters != null)
                {
                    filter.MurexName = filters.Where(a => a.Field.Equals("MurexName")).Select(a => a.Value).SingleOrDefault();
                    filter.Customer.CIF = filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.Customer.Name = filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from murex in context.MurexMappings
                                join customer in context.Customers on murex.CIF equals customer.CIF
                                let isMurexName = !string.IsNullOrEmpty(filter.MurexName)
                                let isCIF = !string.IsNullOrEmpty(filter.Customer.CIF)
                                let isName = !string.IsNullOrEmpty(filter.Customer.Name)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where murex.IsDeleted == false && customer.IsDeleted == false
                                && (isMurexName ? murex.MurexName.Contains(filter.MurexName) : true)
                                && (isCIF ? customer.CIF.Contains(filter.Customer.CIF) : true)
                                && (isName ? customer.CustomerName.Contains(filter.Customer.Name) : true)
                                && (isCreateBy ? (murex.UpdateDate == null ? murex.CreateBy.Contains(filter.LastModifiedBy) : murex.UpdateBy.Contains(filter.LastModifiedBy)) : true)
                                && (isCreateDate ? ((murex.UpdateDate == null ? murex.CreateDate : murex.UpdateDate.Value) > filter.LastModifiedDate.Value && ((murex.UpdateDate == null ? murex.CreateDate : murex.UpdateDate.Value) < maxDate)) : true)
                                select new MurexMappingModel
                                {
                                    ID = murex.MurexMappingID,
                                    MurexName = murex.MurexName,
                                    Customer = new CustomerModel
                                    {
                                        CIF = murex.Customer != null ? (string.IsNullOrEmpty(murex.Customer.CIF) ? "-" : murex.Customer.CIF) : "-",
                                        Name = murex.Customer != null ? (string.IsNullOrEmpty(murex.Customer.CustomerName) ? "-" : murex.Customer.CustomerName) : "-"
                                    },
                                    CIF = murex.Customer != null ? (string.IsNullOrEmpty(murex.Customer.CIF) ? "-" : murex.Customer.CIF) : "-",
                                    CustomerName = murex.Customer != null ? (string.IsNullOrEmpty(murex.Customer.CustomerName) ? "-" : murex.Customer.CustomerName) : "-",
                                    LastModifiedDate = murex.UpdateDate ?? murex.CreateDate,
                                    LastModifiedBy = murex.UpdateDate == null ? murex.CreateBy : murex.UpdateBy
                                });

                    MurexGrid.Page = page;
                    MurexGrid.Size = size;
                    MurexGrid.Total = data.Count();
                    MurexGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    MurexGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                    .Select((a, i) => new MurexMapppingRow
                    {
                        RowID = i + 1,
                        ID = a.ID,
                        MurexName = a.MurexName,
                        CIF = a.CIF,
                        Customer = a.Customer,
                        LastModifiedDate = a.LastModifiedDate,
                        LastModifiedBy = a.LastModifiedBy,
                    })
                    .Skip(skip)
                    .Take(size)
                    .ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetMurexByLimit(ref IList<MurexMappingModel> Murex, int limit, int index, ref string message)
        {
            bool isSuccess = false;
            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    Murex = (from murex in context.MurexMappings
                             join customer in context.Customers on murex.CIF equals customer.CIF
                             where murex.IsDeleted.Equals(false)
                             && customer.IsDeleted.Equals(false)
                             orderby new { murex.MurexName }
                             select new MurexMappingModel
                             {
                                 ID = murex.MurexMappingID,
                                 MurexName = murex.MurexName,
                                 Customer = new CustomerModel
                                 {
                                     CIF = murex.Customer != null ? (string.IsNullOrEmpty(murex.Customer.CIF) ? "-" : murex.Customer.CIF) : "-",
                                     Name = murex.Customer != null ? (string.IsNullOrEmpty(murex.Customer.CustomerName) ? "-" : murex.Customer.CustomerName) : "-"
                                 },
                                 CIF = murex.Customer != null ? (string.IsNullOrEmpty(murex.Customer.CIF) ? "-" : murex.Customer.CIF) : "-",
                                 CustomerName = murex.Customer != null ? (string.IsNullOrEmpty(murex.Customer.CustomerName) ? "-" : murex.Customer.CustomerName) : "-",
                                 LastModifiedDate = murex.UpdateDate ?? murex.CreateDate,
                                 LastModifiedBy = murex.UpdateDate == null ? murex.CreateBy : murex.UpdateBy
                             }).Skip(skip).Take(limit).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetMurexBySearch(string key, int limit, ref IList<MurexMappingModel> Murex, ref string message)
        {
            string kunci = key.Trim().ToLower();
            bool isSuccess = false;
            try
            {
                Murex = (from murex in context.MurexMappings
                         join customer in context.Customers on murex.CIF equals customer.CIF
                         where murex.IsDeleted.Equals(false)
                         && customer.IsDeleted.Equals(false)
                         && murex.MurexName.ToLower().Contains(kunci)
                         orderby new { murex.MurexName }
                         select new MurexMappingModel
                         {
                             ID = murex.MurexMappingID,
                             MurexName = murex.MurexName,
                             Customer = new CustomerModel
                             {
                                 CIF = murex.Customer != null ? (string.IsNullOrEmpty(murex.Customer.CIF) ? "-" : murex.Customer.CIF) : "-",
                                 Name = murex.Customer != null ? (string.IsNullOrEmpty(murex.Customer.CustomerName) ? "-" : murex.Customer.CustomerName) : "-"
                             },
                             CIF = murex.Customer != null ? (string.IsNullOrEmpty(murex.Customer.CIF) ? "-" : murex.Customer.CIF) : "-",
                             CustomerName = murex.Customer != null ? (string.IsNullOrEmpty(murex.Customer.CustomerName) ? "-" : murex.Customer.CustomerName) : "-",
                             LastModifiedDate = murex.UpdateDate ?? murex.CreateDate,
                             LastModifiedBy = murex.UpdateDate == null ? murex.CreateBy : murex.UpdateBy
                           }).Take(limit).ToList();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetMurexAll(ref IList<MurexMappingModel> Murex, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    Murex = (from murex in context.MurexMappings
                             join customer in context.Customers on murex.CIF equals customer.CIF
                             where murex.IsDeleted.Equals(false)
                             && customer.IsDeleted.Equals(false)
                             orderby new { murex.MurexName }
                             select new MurexMappingModel
                               {
                                   ID = murex.MurexMappingID,
                                   MurexName = murex.MurexName,
                                   Customer = new CustomerModel
                                   {
                                       CIF = murex.Customer != null ? (string.IsNullOrEmpty(murex.Customer.CIF) ? "-" : murex.Customer.CIF) : "-",
                                       Name = murex.Customer != null ? (string.IsNullOrEmpty(murex.Customer.CustomerName) ? "-" : murex.Customer.CustomerName) : "-"
                                   },
                                   CIF = murex.Customer != null ? (string.IsNullOrEmpty(murex.Customer.CIF) ? "-" : murex.Customer.CIF) : "-",
                                   CustomerName = murex.Customer != null ? (string.IsNullOrEmpty(murex.Customer.CustomerName) ? "-" : murex.Customer.CustomerName) : "-",
                                   LastModifiedDate = murex.UpdateDate ?? murex.CreateDate,
                                   LastModifiedBy = murex.UpdateDate == null ? murex.CreateBy : murex.UpdateBy
                               }).ToList();
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return IsSuccess;
        }
        public bool GetMurexById(int id, ref MurexMappingModel Murex, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                Murex = (from murex in context.MurexMappings
                         join customer in context.Customers on murex.CIF equals customer.CIF
                         where murex.IsDeleted.Equals(false) && customer.IsDeleted.Equals(false) && murex.MurexMappingID.Equals(id)
                         select new MurexMappingModel
                         {
                             ID = murex.MurexMappingID,
                             MurexName = murex.MurexName,
                             Customer = new CustomerModel
                             {
                                 CIF = murex.Customer != null ? (string.IsNullOrEmpty(murex.Customer.CIF) ? "-" : murex.Customer.CIF) : "-",
                                 Name = murex.Customer != null ? (string.IsNullOrEmpty(murex.Customer.CustomerName) ? "-" : murex.Customer.CustomerName) : "-"
                             },
                             CIF = murex.Customer != null ? (string.IsNullOrEmpty(murex.Customer.CIF) ? "-" : murex.Customer.CIF) : "-",
                             CustomerName = murex.Customer != null ? (string.IsNullOrEmpty(murex.Customer.CustomerName) ? "-" : murex.Customer.CustomerName) : "-",
                             LastModifiedDate = murex.UpdateDate ?? murex.CreateDate,
                             LastModifiedBy = murex.UpdateDate == null ? murex.CreateBy : murex.UpdateBy
                         }).SingleOrDefault();
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return IsSuccess;
        }

    }
    #endregion

    
}