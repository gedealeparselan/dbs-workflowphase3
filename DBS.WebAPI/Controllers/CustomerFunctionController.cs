﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class CustomerFunctionController : ApiController
    {
        private string message = string.Empty;

        private readonly ICustomerFunctionRepository repository = null;

        public CustomerFunctionController()
        {
            this.repository = new CustomerFunctionRepository();
        }

        public CustomerFunctionController(ICustomerFunctionRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all Customer Functions.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<CustomerFunctionModel>))]
        public HttpResponseMessage Get()
        {
            IList<CustomerFunctionModel> output = new List<CustomerFunctionModel>();

            if (!repository.GetCustomerFunction(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerFunctionModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Customer Functions with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<CustomerFunctionModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<CustomerFunctionModel> output = new List<CustomerFunctionModel>();

            if (!repository.GetCustomerFunction(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerFunctionModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Customer Functions for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(BankGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<CustomerFunctionFilter> filters)
        {
            CustomerFunctionGrid output = new CustomerFunctionGrid();

            if (!repository.GetCustomerFunction(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerFunctionGrid>(HttpStatusCode.OK, output);
            }
        }

    
        /// <summary>
        /// Get Customer Function by ID.
        /// </summary>
        /// <param name="id">Customer Function ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(CustomerFunctionModel))]
        [Route("api/CustomerFunction/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            CustomerFunctionModel output = new CustomerFunctionModel();

            if (!repository.GetCustomerFunctionByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<CustomerFunctionModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new Customer Function.
        /// </summary>
        /// <param name="CustomerFunction">Customer Function data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(CustomerFunctionModel CustomerFunction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddCustomerFunction(CustomerFunction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Customer Function data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing CustomerFunction.
        /// </summary>
        /// <param name="id">ID of Customer Function to be modify.</param>
        /// <param name="CustomerFunction">Customer Function data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        //[Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        [Route("api/CustomerFunction/{id}")]
        public HttpResponseMessage Put(int id, CustomerFunctionModel CustomerFunction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateCustomerFunction(id, CustomerFunction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Customer Function data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting Customer Function.
        /// </summary>
        /// <param name="id">ID of Customer Function to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/CustomerFunction/{id}")]
        [ResponseType(typeof(string))]
       // [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteCustomerFunction(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Customer Function data has been deleted." });
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}