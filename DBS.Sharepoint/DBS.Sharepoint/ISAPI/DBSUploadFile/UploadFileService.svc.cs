﻿using DBS.Sharepoint.Helpers;
using Excel;
using Microsoft.SharePoint;
using Nintex.Workflow.HumanApproval;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using DBS.Sharepoint.Entity;
using System.Data.SqlClient;

namespace DBS.Sharepoint
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class UploadFileService : IUploadFileService
    {
        bool IsFirstRow = true;
        int IndexHeader = 0;
        public UploadFileAllInfo UploadFile(UploadFileAllModel data)
        {
            UploadFileAllInfo result = null;
            string message = string.Empty;
            int RowsAffected = 0;
            int activityHistoryID = 0;
            int FailedRows = 0;
            IList<RetModel> retRows = new List<RetModel>();
            try
            {
                if (data.ID != 0)
                {
                    ExecBulkCopy(data, ref message, ref RowsAffected, ref activityHistoryID, ref FailedRows, ref retRows);

                    if (string.IsNullOrEmpty(message))
                    {
                        result = new UploadFileAllInfo
                        {
                            Message = "Upload File Success.",
                            Filepath = data.Path,
                            ActivityHistoryID = activityHistoryID,
                            RowsAffected = RowsAffected,
                            FailedRows = FailedRows,
                            ReturnRows = retRows
                        };
                    }
                    else
                    {
                        result = new UploadFileAllInfo
                        {
                            Message = message,
                            Filepath = data.Path,
                            ActivityHistoryID = activityHistoryID,
                            RowsAffected = RowsAffected,
                            FailedRows = FailedRows,
                            ReturnRows = retRows
                        };
                    }
                }
                else
                {
                    result = new UploadFileAllInfo
                    {
                        Message = "File does not exist.",
                        Filepath = data.Path,
                        ActivityHistoryID = activityHistoryID,
                        RowsAffected = RowsAffected,
                        FailedRows = FailedRows,
                        ReturnRows = retRows
                    };
                }
            }
            catch (Exception ex)
            {
                result = new UploadFileAllInfo
                {
                    Message = ex.Message
                };
            }

            return result;
        }
        public void ExecBulkCopy(UploadFileAllModel model, ref string message, ref int RowsAffected, ref int activityHistoryID, ref int FailedRows, ref IList<RetModel> retRows)
        {
            //using (TransactionScope ts = new TransactionScope())
            //{
            try
            {
                if (model.Type != UploadFileHelper.xlsmimeType && model.Type != UploadFileHelper.xlsxmimeType)
                {
                    message = "Invalid format file";
                    return;
                }
                StringBuilder SbSql = new StringBuilder();
                bool IsFailedRowReturn = false;

                #region Set Upload Type Parameter
                #region Variable Declaration
                string[] ColumnFrom;
                string[] ColumnTo;
                string DestinationTable = string.Empty;
                string SpName = string.Empty;
                string Col1NameRet = string.Empty;
                string Col2NameRet = string.Empty;
                IsFirstRow = true;
                IndexHeader = 0;
                #endregion

                #region FD
                if (model.UploadType.Equals(UploadFileHelper.UTypeFDInterest))
                {
                    ColumnFrom = UploadFileHelper.ColumnFromFDInterest;
                    ColumnTo = UploadFileHelper.ColumnToFDInterest;
                    DestinationTable = UploadFileHelper.DestTableFDInterest;
                    SpName = UploadFileHelper.SPNameFDInterest;
                    IsFailedRowReturn = true;
                    IsFirstRow = UploadFileHelper.IsFirstRowFDInterest;
                    IndexHeader = UploadFileHelper.IndexHeaderFDInterest;
                }
                else if (model.UploadType.Equals(UploadFileHelper.UTypeFDIquote))
                {
                    ColumnFrom = UploadFileHelper.ColumnFromFDIquote;
                    ColumnTo = UploadFileHelper.ColumnToFDIquote;
                    DestinationTable = UploadFileHelper.DestTableFDIquote;
                    SpName = UploadFileHelper.SPNameFDIquote;
                    IsFailedRowReturn = false;
                    IsFirstRow = UploadFileHelper.IsFirstRowFDIquote;
                    IndexHeader = UploadFileHelper.IndexHeaderFDIquote;
                }
                #endregion

                #region TMO
                else if (model.UploadType.Equals(UploadFileHelper.UTypeMurex))
                {
                    ColumnFrom = UploadFileHelper.ColumnFromMurex;
                    ColumnTo = UploadFileHelper.ColumnToMurex;
                    DestinationTable = UploadFileHelper.DestTableMurex;
                    SpName = UploadFileHelper.SPNameMurex;
                    IsFailedRowReturn = true;
                    IsFirstRow = UploadFileHelper.IsFirstRowMurex;
                    IndexHeader = UploadFileHelper.IndexHeaderMurex;
                }
                else if (model.UploadType.Equals(UploadFileHelper.UTypeMurexName))
                {
                    ColumnFrom = UploadFileHelper.ColumnFromMurexName;
                    ColumnTo = UploadFileHelper.ColumnToMurexName;
                    DestinationTable = UploadFileHelper.DestTableMurexName;
                    SpName = UploadFileHelper.SPNameMurexName;
                    IsFailedRowReturn = true;
                    IsFirstRow = UploadFileHelper.IsFirstRowMurexMapping;
                    IndexHeader = UploadFileHelper.IndexHeaderMurexMapping;
                }
                #endregion

                #region Payment
                else if (model.UploadType.Equals(UploadFileHelper.UTypeReconPayment))
                {
                    ColumnFrom = UploadFileHelper.ColumnFromReconPayment;
                    ColumnTo = UploadFileHelper.ColumnToReconPayment;
                    DestinationTable = UploadFileHelper.DestTableReconPayment;
                    SpName = UploadFileHelper.SPNameReconPayment;
                    IsFailedRowReturn = false;
                    IsFirstRow = UploadFileHelper.IsFirstRowReconPayment;
                    IndexHeader = UploadFileHelper.IndexHeaderReconPayment;
                }
                #endregion

                #region Loan
                else if (model.UploadType.Equals(UploadFileHelper.UTypeRollover))
                {
                    ColumnFrom = UploadFileHelper.ColumnFromRollover;
                    ColumnTo = UploadFileHelper.ColumnToRollover;
                    DestinationTable = UploadFileHelper.DestTableRollover;
                    SpName = UploadFileHelper.SPNameRollover;
                    IsFailedRowReturn = true;
                    IsFirstRow = UploadFileHelper.IsFirstRowRollover;
                    IndexHeader = UploadFileHelper.IndexHeaderRollover;
                }
                else if (model.UploadType.Equals(UploadFileHelper.UTypeLoanIM))
                {
                    ColumnFrom = UploadFileHelper.ColumnFromLoanIM;
                    ColumnTo = UploadFileHelper.ColumnToLoanIM;
                    DestinationTable = UploadFileHelper.DestTableLoanIM;
                    SpName = UploadFileHelper.SPNameLoanIM;
                    IsFailedRowReturn = true;
                    IsFirstRow = UploadFileHelper.IsFirstRowLoanIM;
                    IndexHeader = UploadFileHelper.IndexHeaderLoanIM;
                }
                else if (model.UploadType.Equals(UploadFileHelper.UTypeSamAccount))
                {
                    ColumnFrom = UploadFileHelper.ColumnFromSamAccount;
                    ColumnTo = UploadFileHelper.ColumnToSamAccount;
                    DestinationTable = UploadFileHelper.DestTableSamAccount;
                    SpName = UploadFileHelper.SPNameSamAccount;
                    IsFailedRowReturn = true;
                    IsFirstRow = UploadFileHelper.IsFirstRowSamAccount;
                    IndexHeader = UploadFileHelper.IndexHeaderSamAccount;
                }
                else if (model.UploadType.Equals(UploadFileHelper.UTypeLoanEOD))
                {
                    ColumnFrom = UploadFileHelper.ColumnFromLoanEOD;
                    ColumnTo = UploadFileHelper.ColumnToLoanEOD;
                    DestinationTable = UploadFileHelper.DestTableLoanEOD;
                    SpName = UploadFileHelper.SPNameLoanEOD;
                    IsFailedRowReturn = true;
                    IsFirstRow = UploadFileHelper.IsFirstRowLoanEOD;
                    IndexHeader = UploadFileHelper.IndexHeaderLoanEOD;
                }
                else if (model.UploadType.Equals(UploadFileHelper.UTypeLoanSG))
                {
                    ColumnFrom = UploadFileHelper.ColumnFromLoanSG;
                    ColumnTo = UploadFileHelper.ColumnToLoanSG;
                    DestinationTable = UploadFileHelper.DestTableLoanSG;
                    SpName = UploadFileHelper.SPNameLoanSG;
                    IsFailedRowReturn = true;
                    IsFirstRow = UploadFileHelper.IsFirstRowLoanSG;
                    IndexHeader = UploadFileHelper.IndexHeaderLoanSG;
                }
                else if (model.UploadType.Equals(UploadFileHelper.UTypeLoanDue))
                {
                    ColumnFrom = UploadFileHelper.ColumnFromLoanDue;
                    ColumnTo = UploadFileHelper.ColumnToLoanDue;
                    DestinationTable = UploadFileHelper.DestTableLoanDue;
                    SpName = UploadFileHelper.SPNameLoanDue;
                    IsFailedRowReturn = true;
                    IsFirstRow = UploadFileHelper.IsFirstRowLoanDue;
                    IndexHeader = UploadFileHelper.IndexHeaderLoanDue;
                }
                else if (model.UploadType.Equals(UploadFileHelper.UTypeLoanAdhoc))
                {
                    ColumnFrom = UploadFileHelper.ColumnFromLoanAdhoc;
                    ColumnTo = UploadFileHelper.ColumnToLoanAdhoc;
                    DestinationTable = UploadFileHelper.DestTableLoanAdhoc;
                    SpName = UploadFileHelper.SPNameLoanAdhoc;
                    IsFailedRowReturn = true;
                    IsFirstRow = UploadFileHelper.IsFirstRowLoanAdhoc;
                    IndexHeader = UploadFileHelper.IndexHeaderLoanAdhoc;
                }
                else if (model.UploadType.Equals(UploadFileHelper.UTypeLoanIQuote))
                {
                    ColumnFrom = UploadFileHelper.ColumnFromLoanIQuote;
                    ColumnTo = UploadFileHelper.ColumnToLoanIQuote;
                    DestinationTable = UploadFileHelper.DestTableLoanIQuote;
                    SpName = UploadFileHelper.SPNameLoanIQuote;
                    IsFailedRowReturn = true;
                    IsFirstRow = UploadFileHelper.IsFirstRowLoanIQuote;
                    IndexHeader = UploadFileHelper.IndexHeaderLoanIQuote;
                }
                #endregion

                #region CIF
                else if (model.UploadType.Equals(UploadFileHelper.UTypeCifRM))
                {
                    ColumnFrom = UploadFileHelper.ColumnFromCifRM;
                    ColumnTo = UploadFileHelper.ColumnToCifRM;
                    DestinationTable = UploadFileHelper.DestTableCifRM;
                    SpName = UploadFileHelper.SPNameCifRM;
                    IsFailedRowReturn = true;
                    IsFirstRow = UploadFileHelper.IsFirstRowCifRM;
                    IndexHeader = UploadFileHelper.IndexHeaderCifRM;
                }
                #endregion

                #region IBG
                else if (model.UploadType.Equals(UploadFileHelper.UTypeAnnual))
                {
                    ColumnFrom = UploadFileHelper.ColumnFromAnnual;
                    ColumnTo = UploadFileHelper.ColumnToAnnual;
                    DestinationTable = UploadFileHelper.DestTableAnnual;
                    SpName = UploadFileHelper.SPNameAnnual;
                    IsFailedRowReturn = true;
                    IsFirstRow = UploadFileHelper.IsFirstRowAnnual;
                    IndexHeader = UploadFileHelper.IndexHeaderAnnual;
                }
                #endregion

                #region CSO
                else if (model.UploadType.Equals(UploadFileHelper.UTypeCSO))
                {
                    ColumnFrom = UploadFileHelper.ColumnFromCSO;
                    ColumnTo = UploadFileHelper.ColumnToCSO;
                    DestinationTable = UploadFileHelper.DestTableCSO;
                    SpName = UploadFileHelper.SPNameCSO;
                    IsFailedRowReturn = true;
                    IsFirstRow = UploadFileHelper.IsFirstRowCSO;
                    IndexHeader = UploadFileHelper.IndexHeaderCSO;
                }
                #endregion
                else
                {
                    message = "Invalid upload type";
                    return;
                }
                #endregion

                #region Bulk Copy
                DataSet result = null;
                if (ReadStreamToDataSet(model, ref result, ref message))
                {
                    System.Data.DataTable dt = result.Tables[0];

                    if (model.UploadType.Equals(UploadFileHelper.UTypeLoanIQuote))
                    {
                        dt = result.Tables[1];
                    }
                    if (model.UploadType.Equals(UploadFileHelper.UTypeFDIquote))
                    {
                        dt = result.Tables[1];
                    }

                    if (dt != null)
                    {
                        if (ColumnFrom.Length != ColumnTo.Length)
                        {
                            message = "Invalid column mapping";
                            return;
                        }

                        DBSSharePointEntities context = new DBSSharePointEntities();
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(DataHelper.StrConn))
                        {
                            DateTime dtime = DateTime.Now;
                            string splogin = SPContext.Current.Web.CurrentUser.LoginName;
                            #region Insert upload log
                            StringBuilder inserSQl = new StringBuilder();
                            inserSQl.AppendLine("INSERT INTO [Master].[DownloadUploadLog]([ActivityType],[ActivityName],[Description],[IsDeleted],[CreateDate],[CreateBy])");
                            inserSQl.AppendLine("VALUES('" + UploadFileHelper.ActivityTypeisUpload + "','" + model.Path + "','ListID:" + model.ID + "," + model.Type + "',0,cast('" + dtime + "' as datetime),'" + splogin + "')");
                            int activityHistory = 0;
                            activityHistory = DataHelper.InsertUploadLog(inserSQl.ToString());
                            #endregion

                            #region Exec sql bulk copy
                            if (activityHistory > 0)
                            {
                                #region Set Default value
                                dt.Columns.Add("ActivityHistoryID", typeof(System.Int64));
                                dt.Columns.Add("CreateDate", typeof(DateTime));
                                dt.Columns.Add("CreateBy", typeof(string));
                                foreach (DataRow item in dt.Rows)
                                {
                                    item["ActivityHistoryID"] = activityHistory;
                                    item["CreateDate"] = dtime;
                                    item["CreateBy"] = splogin;
                                }
                                #endregion

                                #region Mapping column n bulk copy
                                bulkCopy.DestinationTableName = DestinationTable;
                                bulkCopy.ColumnMappings.Clear();
                                for (int i = 0; i < ColumnFrom.Length; i++)
                                {
                                    bulkCopy.ColumnMappings.Add(ColumnFrom[i], ColumnTo[i]);
                                }
                                bulkCopy.WriteToServer(dt);
                                #endregion

                                RowsAffected = dt.Rows.Count;
                                activityHistoryID = activityHistory;

                                #region Insert to Real Table
                                try
                                {
                                    if (!string.IsNullOrEmpty(SpName))
                                    {
                                        using (SqlConnection conn = new SqlConnection(DataHelper.StrConn))
                                        {
                                            if (conn.State == ConnectionState.Closed) conn.Open();
                                            using (SqlCommand cmd = new SqlCommand())
                                            {
                                                cmd.CommandText = SpName;
                                                cmd.CommandTimeout = DataHelper.SQLCommandTimeout; ;
                                                cmd.CommandType = CommandType.StoredProcedure;

                                                #region Sp Param
                                                SqlParameter returnParameter = new SqlParameter();
                                                cmd.Parameters.AddWithValue("ActivityHistoryID", activityHistory);
                                                if (IsFailedRowReturn)
                                                {
                                                    returnParameter = cmd.Parameters.Add("FailedRows", SqlDbType.Int);
                                                    returnParameter.Direction = ParameterDirection.ReturnValue;
                                                }

                                                #endregion

                                                cmd.Connection = conn;
                                                cmd.ExecuteNonQuery();

                                                if (IsFailedRowReturn)
                                                    FailedRows = (int)returnParameter.Value;
                                                else FailedRows = 0;
                                            }

                                            if (FailedRows > 0)
                                            {
                                                #region Get Rows Failed
                                                SbSql = new StringBuilder();
                                                if (conn.State == ConnectionState.Closed) conn.Open();
                                                using (SqlCommand cmd = new SqlCommand())
                                                {
                                                    #region Set SSQL command for failed row
                                                    #region TMO
                                                    if (model.UploadType.Equals(UploadFileHelper.UTypeMurex))
                                                    {
                                                        SbSql.AppendLine("SELECT NB,CounterParty");
                                                        SbSql.AppendLine("FROM " + DestinationTable + " _stg");
                                                        SbSql.AppendLine("WHERE ((([EXTERNAL REFERENCE] IS NULL OR [EXTERNAL REFERENCE]='0' OR [EXTERNAL REFERENCE] like '%#%')");
                                                        SbSql.AppendLine("AND [COUNTERPART TYPE]>0) OR ([GROUP] IN ('CS','OPT')))");
                                                        SbSql.AppendLine("AND (select top 1 CIF from [master].[MurexMapping] where MurexName=_stg.Counterparty) IS NULL");
                                                        SbSql.AppendLine("AND _stg.ActivityHistoryID=" + activityHistoryID + "");
                                                        Col1NameRet = "NB";
                                                        Col2NameRet = "CounterParty";
                                                    }
                                                    else if (model.UploadType.Equals(UploadFileHelper.UTypeMurexName))
                                                    {
                                                        SbSql.AppendLine("SELECT _stg.[Murex Name],_stg.CIF");
                                                        SbSql.AppendLine("FROM " + DestinationTable + " _stg");
                                                        SbSql.AppendLine("WHERE (select top 1 CIF from [customer].[customer] _cst where _cst.cif=_stg.cif) IS NULL AND _stg.ActivityHistoryID=" + activityHistoryID + "");
                                                        Col1NameRet = "Murex Name";
                                                        Col2NameRet = "CIF";
                                                    }
                                                    #endregion
                                                    #region FD
                                                    else if (model.UploadType.Equals(UploadFileHelper.UTypeFDInterest))
                                                    {
                                                        SbSql.AppendLine("SELECT _stg.[CUST_ID],_stg.CUST_NAME");
                                                        SbSql.AppendLine("FROM " + DestinationTable + " _stg");
                                                        SbSql.AppendLine("WHERE (select top 1 CIF from [customer].[customer] _cst where _cst.cif=_stg.CUST_ID) IS NULL AND _stg.ActivityHistoryID=" + activityHistoryID + "");
                                                        Col1NameRet = "CUST_ID";
                                                        Col2NameRet = "CUST_NAME";
                                                    }
                                                    #endregion
                                                    #region Loan
                                                    else if (model.UploadType.Equals(UploadFileHelper.UTypeRollover))
                                                    {
                                                        SbSql.AppendLine("SELECT _stg.[CIF],_stg.[LOAN CONTRACT NO]");
                                                        SbSql.AppendLine("FROM " + DestinationTable + " _stg");
                                                        SbSql.AppendLine("WHERE (select top 1 CIF from [customer].[customer] _cst where _cst.cif=_stg.CIF) IS NULL AND _stg.ActivityHistoryID=" + activityHistoryID + "");
                                                        Col1NameRet = "CIF";
                                                        Col2NameRet = "LOAN CONTRACT NO";
                                                    }
                                                    else if (model.UploadType.Equals(UploadFileHelper.UTypeLoanIM))
                                                    {
                                                        SbSql.AppendLine("SELECT _stg.[CIF],_stg.[LOAN CONTRACT NO]");
                                                        SbSql.AppendLine("FROM " + DestinationTable + " _stg");
                                                        SbSql.AppendLine("WHERE (select top 1 CIF from [customer].[customer] _cst where _cst.cif=_stg.CIF) IS NULL AND _stg.ActivityHistoryID=" + activityHistoryID + "");
                                                        Col1NameRet = "CIF";
                                                        Col2NameRet = "LOAN CONTRACT NO";
                                                    }
                                                    else if (model.UploadType.Equals(UploadFileHelper.UTypeSamAccount))
                                                    {
                                                        SbSql.AppendLine("SELECT _stg.[borrowersourceid],_stg.[Liability Name]");
                                                        SbSql.AppendLine("FROM " + DestinationTable + " _stg");
                                                        SbSql.AppendLine("WHERE (select top 1 CIF from [customer].[customer] _cst where _cst.cif=_stg.borrowersourceid) IS NULL AND _stg.ActivityHistoryID=" + activityHistoryID + "");
                                                        Col1NameRet = "borrowersourceid";
                                                        Col2NameRet = "Liability Name";
                                                    }
                                                    else if (model.UploadType.Equals(UploadFileHelper.UTypeLoanEOD))
                                                    {
                                                        SbSql.AppendLine("SELECT _stg.[SOL ID],_stg.[Account No]");
                                                        SbSql.AppendLine("FROM " + DestinationTable + " _stg");
                                                        SbSql.AppendLine("WHERE (select top 1 CIF from [Customer].[CustomerAccount] ca where ca.[AccountNumber]=_stg.[Account No]) IS NULL AND _stg.ActivityHistoryID=" + activityHistoryID + "");
                                                        Col1NameRet = "SOL ID";
                                                        Col2NameRet = "Account No";
                                                    }
                                                    else if (model.UploadType.Equals(UploadFileHelper.UTypeLoanSG))
                                                    {
                                                        SbSql.AppendLine("SELECT _stg.[SOL ID],_stg.[Account No]");
                                                        SbSql.AppendLine("FROM " + DestinationTable + " _stg");
                                                        SbSql.AppendLine("WHERE (select top 1 CIF from [Customer].[CustomerAccount] ca where ca.[AccountNumber]=_stg.[Account No]) IS NULL AND _stg.ActivityHistoryID=" + activityHistoryID + "");
                                                        Col1NameRet = "SOL ID";
                                                        Col2NameRet = "Account No";
                                                    }
                                                    else if (model.UploadType.Equals(UploadFileHelper.UTypeLoanDue))
                                                    {
                                                        SbSql.AppendLine("SELECT _stg.[Cust No],_stg.[Customer Name]");
                                                        SbSql.AppendLine("FROM " + DestinationTable + " _stg");
                                                        SbSql.AppendLine("WHERE (select top 1 CIF from [Customer].[Customer] cus where cus.CIF=_stg.[Cust No]) IS NULL AND _stg.ActivityHistoryID=" + activityHistoryID + "");
                                                        Col1NameRet = "Cust No";
                                                        Col2NameRet = "Customer Name";
                                                    }
                                                    else if (model.UploadType.Equals(UploadFileHelper.UTypeLoanAdhoc))
                                                    {
                                                        SbSql.AppendLine("SELECT _stg.[CIF],_stg.[BranchCode]");
                                                        SbSql.AppendLine("FROM " + DestinationTable + " _stg");
                                                        SbSql.AppendLine("WHERE (select top 1 [CIF] from [Customer].[Customer] cus where cus.CIF=_stg.CIF) IS NULL AND _stg.ActivityHistoryID=" + activityHistoryID + "");
                                                        Col1NameRet = "CIF";
                                                        Col2NameRet = "BranchCode";
                                                    }
                                                    else if (model.UploadType.Equals(UploadFileHelper.UTypeCifRM))
                                                    {
                                                        SbSql.AppendLine("SELECT _stg.[CIF],_stg.[CUSTOMER NAME]");
                                                        SbSql.AppendLine("FROM " + DestinationTable + " _stg");
                                                        SbSql.AppendLine("WHERE (SELECT TOP 1 CIF FROM [Customer].[Customer] cus where cus.CIF=_stg.CIF)IS NULL AND _stg.ActivityHistoryID=" + activityHistoryID + "");
                                                        Col1NameRet = "CIF";
                                                        Col2NameRet = "CUSTOMER NAME";
                                                    }
                                                    #endregion
                                                    #region IBG
                                                    else if (model.UploadType.Equals(UploadFileHelper.UTypeAnnual))
                                                    {
                                                        SbSql.AppendLine("SELECT _stg.[CIF],_stg.[Customer Name]");
                                                        SbSql.AppendLine("FROM " + DestinationTable + " _stg");
                                                        SbSql.AppendLine("WHERE (SELECT TOP 1 CIF FROM [Customer].[Customer] cus where cus.CIF=_stg.CIF)IS NULL AND _stg.ActivityHistoryID=" + activityHistoryID + "");
                                                        Col1NameRet = "CIF";
                                                        Col2NameRet = "Customer Name";
                                                    }
                                                    #endregion
                                                    #region CSO
                                                    else if (model.UploadType.Equals(UploadFileHelper.UTypeCSO))
                                                    {
                                                        SbSql.AppendLine("SELECT STG_.[CUST_ID],STG_.[csoname]");
                                                        SbSql.AppendLine("FROM");
                                                        SbSql.AppendLine("(");
                                                        SbSql.AppendLine("select	_cso.[CUST_ID],cso1  csoname");
                                                        SbSql.AppendLine("FROM [STG].[CSO] _cso");
                                                        SbSql.AppendLine("WHERE _cso.ActivityHistoryID=" + activityHistoryID + " AND (_cso.cso1 IS NOT NULL AND _cso.cso1!='' AND _cso.cso1!='0')");
                                                        SbSql.AppendLine("UNION ALL");
                                                        SbSql.AppendLine("select	_cso.[CUST_ID],cso2  csoname");
                                                        SbSql.AppendLine("FROM [STG].[CSO] _cso");
                                                        SbSql.AppendLine("WHERE _cso.ActivityHistoryID=" + activityHistoryID + " AND (_cso.cso2 IS NOT NULL AND _cso.cso2!='' AND _cso.cso2!='0')");
                                                        SbSql.AppendLine("UNION ALL");
                                                        SbSql.AppendLine("select	_cso.[CUST_ID],cso3  csoname");
                                                        SbSql.AppendLine("FROM [STG].[CSO] _cso");
                                                        SbSql.AppendLine("WHERE _cso.ActivityHistoryID=" + activityHistoryID + " AND (_cso.cso3 IS NOT NULL AND _cso.cso3!='' AND _cso.cso3!='0')");
                                                        //-- 
                                                        SbSql.AppendLine("UNION ALL");
                                                        SbSql.AppendLine("select	_cso.[CUST_ID],cso4  csoname");
                                                        SbSql.AppendLine("FROM [STG].[CSO] _cso");
                                                        SbSql.AppendLine("WHERE _cso.ActivityHistoryID=" + activityHistoryID + " AND (_cso.cso4 IS NOT NULL AND _cso.cso4!='' AND _cso.cso4!='0')");
                                                        SbSql.AppendLine("UNION ALL");
                                                        SbSql.AppendLine("select	_cso.[CUST_ID],cso5  csoname");
                                                        SbSql.AppendLine("FROM [STG].[CSO] _cso");
                                                        SbSql.AppendLine("WHERE _cso.ActivityHistoryID=" + activityHistoryID + " AND (_cso.cso5 IS NOT NULL AND _cso.cso5!='' AND _cso.cso5!='0')");
                                                        SbSql.AppendLine("UNION ALL");
                                                        SbSql.AppendLine("select	_cso.[CUST_ID],cso6  csoname");
                                                        SbSql.AppendLine("FROM [STG].[CSO] _cso");
                                                        SbSql.AppendLine("WHERE _cso.ActivityHistoryID=" + activityHistoryID + " AND (_cso.cso6 IS NOT NULL AND _cso.cso6!='' AND _cso.cso6!='0')");
                                                        SbSql.AppendLine("UNION ALL");
                                                        SbSql.AppendLine("select	_cso.[CUST_ID],cso7  csoname");
                                                        SbSql.AppendLine("FROM [STG].[CSO] _cso");
                                                        SbSql.AppendLine("WHERE _cso.ActivityHistoryID=" + activityHistoryID + " AND (_cso.cso7 IS NOT NULL AND _cso.cso7!='' AND _cso.cso7!='0')");
                                                        SbSql.AppendLine("UNION ALL");
                                                        SbSql.AppendLine("select	_cso.[CUST_ID],cso8  csoname");
                                                        SbSql.AppendLine("FROM [STG].[CSO] _cso");
                                                        SbSql.AppendLine("WHERE _cso.ActivityHistoryID=" + activityHistoryID + " AND (_cso.cso8 IS NOT NULL AND _cso.cso8!='' AND _cso.cso8!='0')");
                                                        SbSql.AppendLine("UNION ALL");
                                                        SbSql.AppendLine("select	_cso.[CUST_ID],cso9  csoname");
                                                        SbSql.AppendLine("FROM [STG].[CSO] _cso");
                                                        SbSql.AppendLine("WHERE _cso.ActivityHistoryID=" + activityHistoryID + " AND (_cso.cso9 IS NOT NULL AND _cso.cso9!='' AND _cso.cso9!='0')");
                                                        SbSql.AppendLine("UNION ALL");
                                                        SbSql.AppendLine("select	_cso.[CUST_ID],cso10  csoname");
                                                        SbSql.AppendLine("FROM [STG].[CSO] _cso");
                                                        SbSql.AppendLine("WHERE _cso.ActivityHistoryID=" + activityHistoryID + " AND (_cso.cso10 IS NOT NULL AND _cso.cso10 !='' AND _cso.cso10!='0')");
                                                        //-- 
                                                        SbSql.AppendLine(") STG_");
                                                        SbSql.AppendLine("WHERE (select top 1 CIF from [Customer].[Customer] cus where cus.CIF=STG_.[CUST_ID]) IS NULL");
                                                        SbSql.AppendLine("OR (select top 1 EmployeeID from [Master].[Employee] emp where emp.EmployeeUsername='i:0#.f|dbsmembership|' + STG_.csoname) is null");
                                                        Col1NameRet = "CUST_ID";
                                                        Col2NameRet = "csoname";
                                                    }
                                                    #endregion
                                                    #endregion

                                                    #region Execute SSQL command
                                                    if (!string.IsNullOrEmpty(SbSql.ToString()))
                                                    {
                                                        cmd.CommandText = SbSql.ToString();
                                                        cmd.CommandTimeout = DataHelper.SQLCommandTimeout; ;
                                                        cmd.CommandType = CommandType.Text;
                                                        cmd.Connection = conn;
                                                        DataSet ds = new DataSet();
                                                        SqlDataAdapter sqlAdapter = new SqlDataAdapter(cmd);
                                                        sqlAdapter.Fill(ds);
                                                        if (ds != null)
                                                        {
                                                            foreach (DataRow row in ds.Tables[0].Rows)
                                                            {
                                                                RetModel rm = new RetModel();
                                                                rm.Column1 = row[Col1NameRet].ToString();
                                                                rm.Column2 = row[Col2NameRet].ToString();
                                                                retRows.Add(rm);
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                #region Get Row Succeed Count
                                                SbSql = new StringBuilder();
                                                if (conn.State == ConnectionState.Closed) conn.Open();
                                                using (SqlCommand cmd = new SqlCommand())
                                                {
                                                    #region Set SSQL command for succeed rows
                                                    #region TMO
                                                    if (model.UploadType.Equals(UploadFileHelper.UTypeMurex))
                                                    {
                                                        SbSql.AppendLine("SELECT  NB,CounterParty");
                                                        SbSql.AppendLine("FROM " + DestinationTable + " _stg");
                                                        SbSql.AppendLine("WHERE ((([EXTERNAL REFERENCE] IS NULL OR [EXTERNAL REFERENCE]='0' OR [EXTERNAL REFERENCE] like '%#%') AND [COUNTERPART TYPE]>0) OR ([GROUP] IN ('CS','OPT')))");
                                                        SbSql.AppendLine("AND _stg.ActivityHistoryID=" + activityHistoryID + "");
                                                        Col1NameRet = "NB";
                                                        Col2NameRet = "CounterParty";
                                                    }
                                                    #endregion
                                                    #endregion

                                                    #region Execute SSQL command
                                                    if (!string.IsNullOrEmpty(SbSql.ToString()))
                                                    {
                                                        cmd.CommandText = SbSql.ToString();
                                                        cmd.CommandTimeout = DataHelper.SQLCommandTimeout; ;
                                                        cmd.CommandType = CommandType.Text;
                                                        cmd.Connection = conn;
                                                        DataSet ds = new DataSet();
                                                        SqlDataAdapter sqlAdapter = new SqlDataAdapter(cmd);
                                                        sqlAdapter.Fill(ds);
                                                        if (ds != null)
                                                        {
                                                            foreach (DataRow row in ds.Tables[0].Rows)
                                                            {
                                                                RetModel rm = new RetModel();
                                                                rm.Column1 = row[Col1NameRet].ToString();
                                                                rm.Column2 = row[Col2NameRet].ToString();
                                                                retRows.Add(rm);
                                                            }
                                                            RowsAffected = ds.Tables[0].Rows.Count;
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    message = "Insert to table failed;" + ex.Message;
                                    return;
                                }
                                #endregion
                            }
                            else
                            {
                                message = "Insert log failed";
                                return;
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        message = "Data is empty";
                        return;
                    }
                }
                #endregion
                //ts.Complete();
            }
            catch (Exception ex)
            {
                //ts.Dispose();
                message = ex.Message;
                return;
            }
            //}
        }

        public bool ReadStreamToDataSet(UploadFileAllModel model, ref DataSet result, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (SPSite site = new SPSite(SPContext.Current.Site.RootWeb.Url))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        SPList myList = web.Lists[UploadFileHelper.ListName];
                        SPListItem myListItem = myList.GetItemById(model.ID);

                        SPFile attchFile = web.GetFile(site.Url + "/" + myListItem.Url);

                        Stream stream = attchFile.OpenBinaryStream();
                        IExcelDataReader excelReader = null;

                        if (stream != null)
                        {
                            if (model.Type == UploadFileHelper.xlsmimeType)
                            {
                                excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                            }
                            else if (model.Type == UploadFileHelper.xlsxmimeType)
                            {
                                excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                            }
                        }

                        if (string.IsNullOrEmpty(message))
                        {
                            excelReader.IsFirstRowAsColumnNames = IsFirstRow;
                            result = excelReader.AsDataSet();
                            /// If you need to rebuild your dataset for NON default upload's template
                            /// Function to rebuild dataset which first row of template is not header data
                            if (!IsFirstRow)
                            {
                                RebuildDataset(model.UploadType, result, ref result);
                            }
                            excelReader.Close();
                            isSuccess = true;
                        }

                        stream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public DataSet RebuildDataset(string UploadType, DataSet result, ref DataSet ds)
        {
            int indexData = 0;
            bool IsNeedCustomizeDataset = false;
            #region FD
            if (UploadType.Equals(UploadFileHelper.UTypeFDInterest))
            {
                indexData = UploadFileHelper.IndexDataFDInterest;
            }
            else if (UploadType.Equals(UploadFileHelper.UTypeFDIquote))
            {
                indexData = UploadFileHelper.IndexDataFDIquote;
                IsNeedCustomizeDataset = true;
            }
            #endregion
            #region Payment
            else if (UploadType.Equals(UploadFileHelper.UTypeReconPayment))
            {
                indexData = UploadFileHelper.IndexDataReconPayment;
            }
            #endregion
            #region Loan
            else if (UploadType.Equals(UploadFileHelper.UTypeRollover))
            {
                indexData = UploadFileHelper.IndexDataRollover;
            }
            else if (UploadType.Equals(UploadFileHelper.UTypeLoanIM))
            {
                indexData = UploadFileHelper.IndexDataLoanIM;
            }
            else if (UploadType.Equals(UploadFileHelper.UTypeLoanEOD))
            {
                indexData = UploadFileHelper.IndexDataLoanEOD;
            }
            else if (UploadType.Equals(UploadFileHelper.UTypeLoanSG))
            {
                indexData = UploadFileHelper.IndexDataLoanSG;
            }
            else if (UploadType.Equals(UploadFileHelper.UTypeLoanIQuote))
            {
                indexData = UploadFileHelper.IndexDataLoanIQuote;
                IsNeedCustomizeDataset = true;
            }
            else if (UploadType.Equals(UploadFileHelper.UTypeLoanDue))
            {
                indexData = UploadFileHelper.IndexDataLoanDue;
            }
            else if (UploadType.Equals(UploadFileHelper.UTypeLoanAdhoc))
            {
                indexData = UploadFileHelper.IndexDataLoanAdhoc;
            }
            #endregion
            #region CIF
            else if (UploadType.Equals(UploadFileHelper.UTypeCifRM))
            {
                indexData = UploadFileHelper.IndexDataCifRM;
            }
            #endregion
            #region CSO
            else if (UploadType.Equals(UploadFileHelper.UTypeCSO))
            {
                indexData = UploadFileHelper.IndexDataCSO;
            }
            #endregion
            #region Rebuild Dataset
            ///IF you doesnt need to customize dataset, only to exclude header data
            ///Function to exclude header of template
            if (!IsNeedCustomizeDataset)
            {
                #region Default Dataset
                if (indexData > 0)
                {
                    DataRow drHeader = result.Tables[0].Rows[IndexHeader - 1];
                    int i = 0;
                    foreach (DataColumn item in result.Tables[0].Columns)
                    {
                        string cname = drHeader[i].ToString();
                        if (cname == string.Empty)
                        {
                            cname = "Column" + i;
                        }
                        item.ColumnName = cname;
                        i++;
                    }
                    for (int idx = 0; idx < (indexData - 1); idx++)
                    {
                        result.Tables[0].Rows[idx].Delete();
                    }
                    result.Tables[0].AcceptChanges();
                    ds = result;
                }
                #endregion
            }
            ///If you need to totally rebuild your dataset, not only in header data
            ///Use your upload type conditional here
            else
            {
                #region Non Default Dataset
                #region Loan
                if (UploadType.Equals(UploadFileHelper.UTypeLoanIQuote))
                {
                    if (indexData > 0)
                    {
                        System.Data.DataTable NewDT = new System.Data.DataTable();
                        System.Data.DataTable TempDT = new System.Data.DataTable();
                        TempDT = result.Tables[0].Clone();
                        NewDT.Columns.Add("Date");
                        NewDT.Columns.Add("Product");
                        NewDT.Columns.Add("TenorCode");
                        NewDT.Columns.Add("TenorName");
                        NewDT.Columns.Add("CurrencyCode");
                        NewDT.Columns.Add("BaseRate");
                        NewDT.Columns.Add("AllInLP");
                        NewDT.Columns.Add("AllInFTPRate");
                        for (int idx = (indexData - 4); idx < result.Tables[0].Rows.Count; idx++)
                        {
                            TempDT.ImportRow(result.Tables[0].Rows[idx]);
                        }
                        DateTime currDate = DateTime.UtcNow;
                        for (int i = 1; i < (TempDT.Columns.Count / 3); i++)
                        {
                            string Currency = TempDT.Rows[1][i * 3].ToString();
                            for (int j = 0; j < TempDT.Rows.Count; j++)
                            {
                                if (TempDT.Rows[j + 4][0].ToString() == "Overdraft (OD)") break;
                                DataRow row = NewDT.NewRow();
                                row["Date"] = currDate;
                                row["Product"] = "Fixed Loans";
                                row["TenorCode"] = TempDT.Rows[j + 4][1].ToString();
                                row["TenorName"] = TempDT.Rows[j + 4][2].ToString();
                                row["CurrencyCode"] = Currency;
                                row["BaseRate"] = TempDT.Rows[j + 4][(i * 3) + 0].ToString();
                                row["AllInLP"] = TempDT.Rows[j + 4][(i * 3) + 1].ToString();
                                row["AllInFTPRate"] = TempDT.Rows[j + 4][(i * 3) + 2].ToString();
                                NewDT.Rows.Add(row);
                            }
                        }
                        ds.Tables.Add(NewDT);
                    }
                }
                #endregion

                #region FD Iquote
                if (UploadType.Equals(UploadFileHelper.UTypeFDIquote))
                {
                    if (indexData > 0)
                    {
                        System.Data.DataTable NewDT = new System.Data.DataTable();
                        System.Data.DataTable TempDT = new System.Data.DataTable();
                        TempDT = result.Tables[0].Clone();
                        NewDT.Columns.Add("Product");
                        NewDT.Columns.Add("Tenor");
                        NewDT.Columns.Add("IDR");
                        NewDT.Columns.Add("USD");
                        NewDT.Columns.Add("SGD");
                        NewDT.Columns.Add("JPY");
                        NewDT.Columns.Add("AUD");
                        NewDT.Columns.Add("NZD");
                        NewDT.Columns.Add("GBP");
                        NewDT.Columns.Add("EUR");
                        NewDT.Columns.Add("CHF");
                        NewDT.Columns.Add("CNH");
                        for (int idx = indexData; idx < result.Tables[0].Rows.Count; idx++)
                        {
                            TempDT.ImportRow(result.Tables[0].Rows[idx]);
                        }
                        for (int j = 0; j < 16; j++)
                        {
                            DataRow row = NewDT.NewRow();
                            row["Product"] = "Fixed Deposits";
                            row["Tenor"] = TempDT.Rows[j][1].ToString();
                            row["IDR"] = TempDT.Rows[j][3];
                            row["USD"] = TempDT.Rows[j][4];
                            row["SGD"] = TempDT.Rows[j][5];
                            row["JPY"] = TempDT.Rows[j][6];
                            row["AUD"] = TempDT.Rows[j][7];
                            row["NZD"] = TempDT.Rows[j][8];
                            row["GBP"] = TempDT.Rows[j][9];
                            row["EUR"] = TempDT.Rows[j][10];
                            row["CHF"] = TempDT.Rows[j][11];
                            row["CNH"] = TempDT.Rows[j][12];
                            NewDT.Rows.Add(row);
                        }
                        ds.Tables.Add(NewDT);
                    }
                }
                #endregion
                #endregion
            }
            #endregion

            return ds;
        }

        //add aridya 20161018 for skn bulk excel ~OFFLINE~
        public bool ReadSKNBulkStreamToDataSet(UploadSKNBulkExcelModel model, ref DataSet result, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (SPSite site = new SPSite(SPContext.Current.Site.RootWeb.Url))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        SPList myList = web.Lists["SKNBulkData"];
                        SPListItem myListItem = myList.GetItemById(model.ID);

                        SPFile attchFile = web.GetFile(site.Url + "/" + myListItem.Url);

                        Stream stream = attchFile.OpenBinaryStream();
                        IExcelDataReader excelReader = null;

                        if (stream != null)
                        {
                            if (model.Type == UploadFileHelper.xlsmimeType)
                            {
                                excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                            }
                            else if (model.Type == UploadFileHelper.xlsxmimeType)
                            {
                                excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                            }
                        }

                        if (string.IsNullOrEmpty(message))
                        {
                            excelReader.IsFirstRowAsColumnNames = IsFirstRow;
                            result = excelReader.AsDataSet();
                            
                            excelReader.Close();
                            isSuccess = true;
                        }

                        stream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public void SKNBulkCopy(UploadSKNBulkExcelModel model, ref string message, ref DataSet retval, ref IList<TransactionSKNBulkModel> retvalSKNBulk, ref IList<TransactionSKNBulkModelRaw> retvalSKNBulkRaw)
        {
            try
            {
                if (model.Type != UploadFileHelper.xlsmimeType && model.Type != UploadFileHelper.xlsxmimeType)
                {
                    message = "Invalid format file";
                    return;
                }
                if(ReadSKNBulkStreamToDataSet(model, ref retval, ref message)){
                    System.Data.DataTable dt = retval.Tables[0];

                    if (dt != null)
                    {
                        DBSSharePointEntities context = new DBSSharePointEntities();
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(DataHelper.StrConn))
                        {
                            DateTime dtime = DateTime.Now;
                            string splogin = SPContext.Current.Web.CurrentUser.LoginName;
                            #region Insert upload log
                            StringBuilder inserSQl = new StringBuilder();
                            inserSQl.AppendLine("INSERT INTO [Master].[DownloadUploadLog]([ActivityType],[ActivityName],[Description],[IsDeleted],[CreateDate],[CreateBy])");
                            inserSQl.AppendLine("VALUES('" + UploadFileHelper.ActivityTypeisUpload + "','" + model.Path + "','ListID:" + model.ID + "," + model.Type + "',0,cast('" + dtime + "' as datetime),'" + splogin + "')");
                            int activityHistory = 0;
                            activityHistory = DataHelper.InsertUploadLog(inserSQl.ToString());
                            #endregion

                            #region Exec sql bulk copy
                            if (activityHistory > 0)
                            {
                                foreach (DataRow row in dt.Rows)
                                {
                                    bool readable = false;
                                    for (int ii = 0; ii < 15; ii++)
                                    {
                                        if (!String.IsNullOrEmpty(row[ii].ToString().Trim()))
                                        {
                                            readable = true;
                                            break;
                                        }
                                    }
                                    if (readable)
                                    {
                                        TransactionSKNBulkModel trxSKNBulk = new TransactionSKNBulkModel();
                                        trxSKNBulk.AccountNumber = row[1].ToString();// item[0];
                                        trxSKNBulk.CustomerName = row[2].ToString(); //item[1];
                                        trxSKNBulk.ValueDate = ChangeDate(row[3].ToString()); //ChangeDate(item[2]);
                                        trxSKNBulk.PaymentAmount = row[4].ToString();// item[3];// decimal.Parse(item[3]);
                                        trxSKNBulk.BeneficiaryName = row[5].ToString();// item[4];
                                        trxSKNBulk.BeneficiaryAccountNumber = row[6].ToString();// item[5];
                                        trxSKNBulk.BankCode = row[7].ToString();// item[6];
                                        trxSKNBulk.BranchCode = row[8].ToString();// item[7];
                                        trxSKNBulk.Citizen = row[9].ToString();// item[8];//.Trim().ToLower() == "yes" ? true : false;
                                        trxSKNBulk.Resident = row[10].ToString();// item[9];//.Trim().ToLower() == "yes" ? true : false;
                                        trxSKNBulk.PaymentDetails = row[11].ToString();// item[10];
                                        trxSKNBulk.Charges = row[12].ToString();//item[11];
                                        trxSKNBulk.Type = row[13].ToString();//item[12];
                                        trxSKNBulk.BeneficiaryCustomer = row[14].ToString();//item[13];
                                        trxSKNBulk.ParentTransactionID = model.transactionID;
                                        trxSKNBulk.ParentWorkflowInstanceID = model.instanceID;

                                        TransactionSKNBulkModelRaw trxSKNBulkRaw = new TransactionSKNBulkModelRaw();
                                        trxSKNBulkRaw.AccountNumber = row[1].ToString();// item[0];
                                        trxSKNBulkRaw.CustomerName = row[2].ToString(); //item[1];
                                        trxSKNBulkRaw.ValueDate = ChangeDate(row[3].ToString()).ToString("dd-MMM-yyyy"); //ChangeDate(item[2]);
                                        trxSKNBulkRaw.PaymentAmount = row[4].ToString();// item[3];// decimal.Parse(item[3]);
                                        trxSKNBulkRaw.BeneficiaryName = row[5].ToString();// item[4];
                                        trxSKNBulkRaw.BeneficiaryAccountNumber = row[6].ToString();// item[5];
                                        trxSKNBulkRaw.BankCode = row[7].ToString();// item[6];
                                        trxSKNBulkRaw.BranchCode = row[8].ToString();// item[7];
                                        trxSKNBulkRaw.Citizen = row[9].ToString();// item[8];//.Trim().ToLower() == "yes" ? true : false;
                                        trxSKNBulkRaw.Resident = row[10].ToString();// item[9];//.Trim().ToLower() == "yes" ? true : false;
                                        trxSKNBulkRaw.PaymentDetails = row[11].ToString();// item[10];
                                        trxSKNBulkRaw.Charges = row[12].ToString();//item[11];
                                        trxSKNBulkRaw.Type = row[13].ToString();//item[12];
                                        trxSKNBulkRaw.BeneficiaryCustomer = row[14].ToString();//item[13];
                                        trxSKNBulkRaw.ParentTransactionID = model.transactionID;
                                        trxSKNBulkRaw.ParentWorkflowInstanceID = model.instanceID;

                                        retvalSKNBulk.Add(trxSKNBulk);
                                        retvalSKNBulkRaw.Add(trxSKNBulkRaw);
                                    }                                    
                                }
                            }
                            else
                            {
                                message = "Insert log failed";
                                return;
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        message = "Data is empty";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return;
            }
        }

        public UploadSKNBulkExcelInfo ExcelSKNBulkData(UploadSKNBulkExcelModel excelSknBulkModel)
        {
            UploadSKNBulkExcelInfo result = null;
            string message = string.Empty;
            //int RowsAffected = 0;
            //int activityHistoryID = 0;
            //int FailedRows = 0;
            IList<RetModel> retRows = new List<RetModel>();
            try
            {
                if (excelSknBulkModel.ID != 0)
                {
                    DataSet retval = null;
                    IList<TransactionSKNBulkModel> retvalSKNBulk = new List<TransactionSKNBulkModel>();
                    IList<TransactionSKNBulkModelRaw> retvalSKNBulkRaw = new List<TransactionSKNBulkModelRaw>();
                    SKNBulkCopy(excelSknBulkModel, ref message, ref retval, ref retvalSKNBulk, ref retvalSKNBulkRaw);

                    if (string.IsNullOrEmpty(message))
                    {
                        result = new UploadSKNBulkExcelInfo
                        {
                            Message = "Upload File Success.",
                            output = retvalSKNBulk,
                            outputRaw = retvalSKNBulkRaw
                        };
                    }
                    else
                    {
                        result = new UploadSKNBulkExcelInfo
                        {
                            Message = message
                        };
                    }
                }
                else
                {
                    result = new UploadSKNBulkExcelInfo
                    {
                        Message = "File does not exist."
                    };
                }
            }
            catch (Exception ex)
            {
                result = new UploadSKNBulkExcelInfo
                {
                    Message = ex.Message
                };
            }

            return result;
            //UploadSKNBulkExcelInfo result = null;
            //string downloadedFilePath = null;
            //try
            //{
            //    IList<TransactionSKNBulkModel> retval = new List<TransactionSKNBulkModel>();
            //    IList<TransactionSKNBulkModelRaw> retvalRaw = new List<TransactionSKNBulkModelRaw>();
                

            //    using (SPSite site = new SPSite(SPContext.Current.Site.RootWeb.Url))
            //    {
            //        using (SPWeb web = site.OpenWeb())
            //        {
            //            SPListItem myListItem;
            //            //SPListCollection a = web.Lists;
            //            //a
            //            //SPList myList = web.Lists[UploadFileHelper.ListName];
            //            //SPListItem myListItem = myList.GetItemById(model.ID);
            //            SPList myList = web.Lists["SKNBulkData"];

            //            string name = excelSknBulkModel.filePath.Split('/')[excelSknBulkModel.filePath.Split('/').Length - 1];

            //            SPQuery query = new SPQuery();    
            //            query.Query = "<Where>" +
            //                                "<Eq>" +
            //                                     "<FieldRef Name=\"FileLeafRef\"/>" +
            //                                     "<Value Type=\"Text\">" + name + "</Value>" +
            //                                "</Eq>" +
            //                            "</Where>";

            //            if (myList.GetItems(query).Count > 0)
            //            {
            //                myListItem = myList.GetItems(query)[0];
            //            }
            //            else
            //            {
            //                myListItem = null;
            //            }

            //             //myList.GetItemById(model.ID);

            //            SPFile attchFile = web.GetFile(site.Url + "/" + myListItem.Url);

            //            Stream stream = attchFile.OpenBinaryStream();

            //            string rootApp = AppDomain.CurrentDomain.BaseDirectory;

            //            string tempDir = rootApp + "TempExcelSKNBulk";

            //            if (!Directory.Exists(tempDir))
            //            {
            //                Directory.CreateDirectory(tempDir);
            //            }

            //            string[] filenameToSaveTemp = name.Split('.');

            //            string fileNameToSave = "";

            //            for (int ii = 0; ii < filenameToSaveTemp.Length - 1; ii++)
            //            {
            //                fileNameToSave += filenameToSaveTemp[ii];
            //            }

            //            fileNameToSave += ("_" + excelSknBulkModel.loginUser);

            //            fileNameToSave += ("." + filenameToSaveTemp[filenameToSaveTemp.Length - 1]);

            //            downloadedFilePath = tempDir + "\\" + fileNameToSave;
            //            FileStream fs = new FileStream(downloadedFilePath, FileMode.Create);
            //            byte[] read = new byte[256];

            //            int count = stream.Read(read, 0, read.Length);

            //            while (count > 0)
            //            {
            //                fs.Write(read, 0, count);
            //                count = stream.Read(read, 0, read.Length);
            //            }
            //            fs.Close();
            //            stream.Close();

            //            //string[] tempSplit = excelSknBulkModel.filePath.Split('/');
            //            //tempSplit[1] = tempSplit[1].Replace(" ", "%20");
            //            //excelSknBulkModel.filePath = "";

            //            //for (int ii = 1; ii < tempSplit.Length; ii++)
            //            //{
            //            //    excelSknBulkModel.filePath += "/" + tempSplit[ii];
            //            //}

            //            //Uri a = new Uri(web.Url + excelSknBulkModel.filePath);

            //            //excelSknBulkModel.filePath = web.Url + excelSknBulkModel.filePath;
            //            foreach (var item in OpenExcel(downloadedFilePath, excelSknBulkModel.pass, true))
            //            {
            //                TransactionSKNBulkModel trxSKNBulk = new TransactionSKNBulkModel();
            //                trxSKNBulk.AccountNumber = item[0];
            //                trxSKNBulk.CustomerName = item[1];
            //                trxSKNBulk.ValueDate = ChangeDate(item[2]);
            //                trxSKNBulk.PaymentAmount = item[3];// decimal.Parse(item[3]);
            //                trxSKNBulk.BeneficiaryName = item[4];
            //                trxSKNBulk.BeneficiaryAccountNumber = item[5];
            //                trxSKNBulk.BankCode = item[6];
            //                trxSKNBulk.BranchCode = item[7];
            //                trxSKNBulk.Citizen = item[8];//.Trim().ToLower() == "yes" ? true : false;
            //                trxSKNBulk.Resident = item[9];//.Trim().ToLower() == "yes" ? true : false;
            //                trxSKNBulk.PaymentDetails = item[10];
            //                trxSKNBulk.Charges = item[11];
            //                trxSKNBulk.Type = item[12];
            //                trxSKNBulk.BeneficiaryCustomer = item[13];
            //                trxSKNBulk.ParentTransactionID = excelSknBulkModel.transactionID;
            //                trxSKNBulk.ParentWorkflowInstanceID = excelSknBulkModel.instanceID;

            //                retval.Add(trxSKNBulk);
            //            }
            //            foreach (var item in OpenExcel(downloadedFilePath, excelSknBulkModel.pass, true))
            //            {
            //                TransactionSKNBulkModelRaw trxSKNBulkRaw = new TransactionSKNBulkModelRaw();
            //                trxSKNBulkRaw.AccountNumber = item[0];
            //                trxSKNBulkRaw.CustomerName = item[1];
            //                trxSKNBulkRaw.ValueDate = ChangeDate(item[2]).ToString("yyyy-MM-dd");
            //                trxSKNBulkRaw.PaymentAmount = item[3];// decimal.Parse(item[3]);
            //                trxSKNBulkRaw.BeneficiaryName = item[4];
            //                trxSKNBulkRaw.BeneficiaryAccountNumber = item[5];
            //                trxSKNBulkRaw.BankCode = item[6];
            //                trxSKNBulkRaw.BranchCode = item[7];
            //                trxSKNBulkRaw.Citizen = item[8];//.Trim().ToLower() == "yes" ? true : false;
            //                trxSKNBulkRaw.Resident = item[9];//.Trim().ToLower() == "yes" ? true : false;
            //                trxSKNBulkRaw.PaymentDetails = item[10];
            //                trxSKNBulkRaw.Charges = item[11];
            //                trxSKNBulkRaw.Type = item[12];
            //                trxSKNBulkRaw.BeneficiaryCustomer = item[13];
            //                trxSKNBulkRaw.ParentTransactionID = excelSknBulkModel.transactionID;
            //                trxSKNBulkRaw.ParentWorkflowInstanceID = excelSknBulkModel.instanceID;

            //                retvalRaw.Add(trxSKNBulkRaw);
            //            }
            //            if (File.Exists(downloadedFilePath))
            //            {
            //                File.Delete(downloadedFilePath);
            //            }
            //        }
            //    }

            //    result = new UploadSKNBulkExcelInfo
            //    {
            //        output = retval,
            //        outputRaw = retvalRaw,
            //        Message = ""
            //    };
            //}
            //catch (Exception ex)
            //{
            //    result = new UploadSKNBulkExcelInfo
            //    {
            //        Message = ex.Message
            //    };
            //}
            //finally
            //{
            //    if (downloadedFilePath != null)
            //    {
            //        if (File.Exists(downloadedFilePath))
            //        {
            //            File.Delete(downloadedFilePath);
            //        }
            //    }
            //}

            //return result;
        }

        //public List<string[]> OpenExcel(string path, string password, bool isHeader)
        //{
        //    List<string[]> result = null;

        //    Microsoft.Office.Interop.Excel.Application ExcelApp
        //            = new Microsoft.Office.Interop.Excel.Application();
        //    ExcelApp.Visible = false;
        //    Workbook wb = ExcelApp.Workbooks.Open(path,
        //      Type.Missing, Type.Missing, Type.Missing, password, Type.Missing, Type.Missing,
        //      Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
        //      Type.Missing);
        //    Worksheet ws1 = wb.Worksheets[1] as Worksheet; //kondisi sekarang selalu sheet 1, ubah ini kalo pola sudah ada dan/atau pasti
        //    try
        //    {
        //        result = IterateRows(ws1, isHeader);
        //        wb.Close(false, Type.Missing, Type.Missing);
        //        ExcelApp.Quit();
        //    }
        //    catch (Exception ex)
        //    {
        //        wb.Close(false, Type.Missing, Type.Missing);
        //        ExcelApp.Quit();
        //    }

        //    return result;
        //}

        //public List<string[]> IterateRows(Microsoft.Office.Interop.Excel.Worksheet worksheet, bool isHeader)
        //{
        //    List<string[]> result = new List<string[]>();
        //    worksheet.Columns.ClearFormats();
        //    worksheet.Rows.ClearFormats();
        //    Microsoft.Office.Interop.Excel.Range usedRange = worksheet.UsedRange;
        //    foreach (Microsoft.Office.Interop.Excel.Range row in usedRange.Rows)
        //    {
        //        if (isHeader)
        //        {
        //            isHeader = false;
        //        }
        //        else
        //        {
        //            //String[] rowData = new String[row.Columns.Count];
        //            String[] rowData = new String[14]; //hard code jumlah kolom, bug di interop terkait formatting
        //            for (int i = 1; i < 15; i++) //mulai dari kolom ke 2 karena kolom pertama pasti untuk nomor
        //            {
        //                rowData[i - 1] = (string)(row.Cells[1, i + 1] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
        //            }
        //            result.Add(rowData);
        //        }
        //    }
        //    return result;
        //}

        public static DateTime ChangeDate(string date)
        {
            return DateTime.FromOADate(double.Parse(date));
            //return new DateTime(Int32.Parse(date.Split('/')[2]), Int32.Parse(date.Split('/')[0]), Int32.Parse(date.Split('/')[1]));
        }
        //end add
    }
}