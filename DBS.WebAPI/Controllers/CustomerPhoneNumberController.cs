﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    public class CustomerPhoneNumberController : ApiController
    {
        private string message = string.Empty;

        private readonly ICustomerPhoneNumberRepository repository = null;

        public CustomerPhoneNumberController()
        {
            this.repository = new CustomerPhoneNumberRepository();
        }

        public CustomerPhoneNumberController(CustomerPhoneNumberRepository repository)
        {
            this.repository = repository;
        }
        [ResponseType(typeof(CustomerPhoneNumberGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(string CIF, IList<CustomerPhoneNumberFilter> filters, int? page, int? size, string sort_column, string sort_order)
        {
            CustomerPhoneNumberGrid output = new CustomerPhoneNumberGrid();

            if (!repository.GetCustomerPhoneNumberGrid(CIF, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "UpdateDate" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerPhoneNumberGrid>(HttpStatusCode.OK, output);
            }
        }
    }
}
