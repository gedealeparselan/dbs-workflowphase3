﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    [Route("api/Customer/{cif}/CSO")]
    public class CustomerCSOController : ApiController
    {
        private string message = string.Empty;

        private readonly ICustomerCSORepository repository = null;

        public CustomerCSOController()
        {   
            this.repository = new CustomerCSORepository();
        }
        public CustomerCSOController(ICustomerCSORepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all customer CSO
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<CustomerCSOModel>))]
        public HttpResponseMessage Get(string cif)
        {
            IList<CustomerCSOModel> output = new List<CustomerCSOModel>();

            if (!repository.GetCustomerCSO(cif, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerCSOModel>>(HttpStatusCode.OK, output);
            }
        } 

        //start     
        [ResponseType(typeof(WorkflowEntityGrid))]
        [Route("api/Customer/CustomerCsoDraft/{cif}/CSO")]
        [AcceptVerbs("GET", "POST")]        
       
       public HttpResponseMessage GetDraftCso(string cif, int? page, int? size, string sort_column, string sort_order, IList<WorkflowEntityFilter> filters)
        {
            
            WorkflowEntityGrid output = new WorkflowEntityGrid();
             if (!repository.GetSP(cif,page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
                {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<WorkflowEntityGrid>(HttpStatusCode.OK, output);
                
            }
        }
        //end 

        /// <summary>
        /// Get customer CSO by ID.
        /// </summary>
        /// <param name="id">Customer CSO ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(CustomerCSOModel))]
        [HttpGet]
        [Route("api/CustomerCSO/{id}/{cif}")]
        public HttpResponseMessage Get(int id, string cif)
        {
            CustomerCSOModel output = new CustomerCSOModel();

            if (!repository.GetCustomerCSOByID(cif, id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<CustomerCSOModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find customer CSO using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<CustomerCSOModel>))]
        [HttpGet]
        [Route("api/CustomerCSO/Search")]
        public HttpResponseMessage Search(string cif, string query, int? limit)
        {
            IList<CustomerCSOModel> output = new List<CustomerCSOModel>();

            if (!repository.GetCustomerCSO(cif, query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerCSOModel>>(HttpStatusCode.OK, output);
            }
        }
        /// <summary>
        /// Get customer CSO for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(CustomerCSOGrid))]
        [AcceptVerbs("GET", "POST")]       
        public HttpResponseMessage Get(string cif, int? page, int? size, string sort_column, string sort_order, IList<CustomerCSOFilter> filters)
        {
            CustomerCSOGrid output = new CustomerCSOGrid();

            if (!repository.GetCustomerCSO(cif, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Employee.EmployeeName" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerCSOGrid>(HttpStatusCode.OK, output);
            }
        }
        
        /// <summary>
        /// Get customer CSO with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<CustomerCSOModel>))]
        public HttpResponseMessage Get(string cif, int? limit, int? index)
        {
            IList<CustomerCSOModel> output = new List<CustomerCSOModel>();

            if (!repository.GetCustomerCSO(cif, ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerCSOModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new customer CSO.
        /// </summary>
        /// <param name="CustomerCSO">Customer CSO data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/CustomerCSO/{cif}")]
        public HttpResponseMessage Post(string cif, CustomerCSOModel CustomerCSO)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {                
                if (!repository.AddCustomerCSO(cif, CustomerCSO, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Customer CSO data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing customer CSO.
        /// </summary>
        /// <param name="id">ID of CustomerCSO to be modify.</param>
        /// <param name="CustomerCSO">Customer CSO data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/CustomerCSO/{id}/{cif}")]   
        public HttpResponseMessage Put(int id, string cif, CustomerCSOModel CustomerCSO)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateCustomerCSO(cif, id, CustomerCSO, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Customer CSO data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting customer CSO.
        /// </summary>
        /// <param name="id">ID of customer CSO to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/CustomerCSO/{id}/{cif}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id, string cif)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteCustomerCSO(cif, id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Customer CSO data has been deleted." });
                }
            }
        }
    
    }
}
