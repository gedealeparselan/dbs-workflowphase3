// JavaScript source code

/**
 * Created by SPAdmin on 21/11/2014.
 */

var accessToken;

/* dodit@2014.11.08:add format function */
var formatNumber = function (num) {
    if(num !=null) {
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    }
}

var $box;
var $remove = false;
var cifData = '0';
var customerNameData = '';
var DocumentModels = ko.observable({ FileName: '', DocumentPath: '' });
var DealModel = {cif : cifData,token : accessToken}
var idrrate = 0;



var AmountModel = {
    TotalAmountsUSD: ko.observable(0),
    RemainingBalance: ko.observable(0),
    TotalUtilization : ko.observable(0)
}


var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};

var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};

var StatementModel = {
    ID: ko.observable(0),
    Name: ko.observable("")
};

var BranchModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var TypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var RMModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    RankID: ko.observable(),
    Rank: ko.observable(),
    SegmentID: ko.observable(),
    Segment: ko.observable(),
    BranchID: ko.observable(),
    Branch: ko.observable(),
    Email: ko.observable()
};

var ContactModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    PhoneNumber: ko.observable(),
    DateOfBirth: ko.observable(),
    Address: ko.observable(),
    IDNumber: ko.observable()
};

var FunctionModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var CurrencyModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var ProductTypeModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    IsFlowValas:ko.observable(),
    Description: ko.observable()
};

var AccountModel = {
    AccountNumber: ko.observable(),
    Currency: ko.observable(CurrencyModel)
};

var UnderlyingDocumentModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var DocumentTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var DocumentPurposeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var RateTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
}

var FXComplianceModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ChargesModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var UnderlyingModel = {
    ID: ko.observable(),
    UnderlyingDocument: ko.observable(UnderlyingDocumentModel),
    DocumentType: ko.observable(DocumentTypeModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    AttachmentNo: ko.observable(),
    IsStatementLetter: ko.observable(),
    IsDeclarationOfException: ko.observable(),
    StartDate: ko.observable(),
    EndDate: ko.observable(),
    DateOfUnderlying: ko.observable(),
    ExpiredDate: ko.observable(),
    ReferenceNumber: ko.observable(),
    SupplierName: ko.observable(),
    InvoiceNumber: ko.observable()
};

var DocumentModel = {
    ID: ko.observable(),
    Type: ko.observable(DocumentTypeModel),
    Purpose: ko.observable(DocumentPurposeModel),
    FileName: ko.observable(),
    DocumentPath: ko.observable(),
    LastModifiedDate: ko.observable(new Date),
    LastModifiedBy: ko.observable()
};

var CustomerModel = {
    CIF: ko.observable(),
    Name: ko.observable(),
    POAName: ko.observable(),
    Branch: ko.observable(BranchModel),
    Type: ko.observable(TypeModel),
    RM: ko.observable(RMModel),
    Contacts: ko.observableArray([ContactModel]),
    Functions: ko.observableArray([FunctionModel]),
    Accounts: ko.observableArray([AccountModel]),
    Underlyings: ko.observableArray([UnderlyingModel])
};

var TransactionDealModel = {
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    ValueDate: ko.observable(),
    Account: ko.observable(AccountModel),
    StatementLetter: ko.observable(StatementModel),
    TZRef: ko.observable(),
    RateType: ko.observable(RateTypeModel),
    Rate: ko.observable(0),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(0),
    AmountUSD: ko.observable(0),
    UnderlyingCurrency: ko.observable(CurrencyModel),
    UnderlyingAmount: ko.observable(0),
    UtilizedAmount: ko.observable(0),
    CreateDate: ko.observable(new Date)
};

var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};

var SelectModel = function (cif, id) {
    var self = this;
    self.ID = ko.observable(0);
    self.CIF = ko.observable(cif);
    self.UnderlyingID = ko.observable(id);
    self.ParentID = ko.observable(0);
}

var UnderlyingDocumentModel2 = function (id, name, code) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Name = ko.observable(name);
    self.CodeName = ko.observable(code + ' (' + name + ')');
}

var DocumentTypeModel2 = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);

}

var StatementLetterModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}

var CurrencyModel2 = function (id, code, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Description = ko.observable(description);
}

var RateTypeModel2 = function (id, code, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Description = ko.observable(description);
}

var DocumentPurposeModel2 = function (id, name, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(description);
}

var CustomerUnderlyingMappingModel = function (FileID, UnderlyingID, attachNo) {
    var self = this;
    self.ID = ko.observable(0);
    self.UnderlyingID = ko.observable(UnderlyingID);
    self.UnderlyingFileID = ko.observable(FileID);
    self.AttachmentNo = ko.observable(attachNo);
}

var SelectUtillizeModel = function (id, enable, uSDAmount, statementLetter) {
    var self = this;
    self.ID = ko.observable(id);
    self.Enable = ko.observable(enable);
    self.USDAmount = ko.observable(uSDAmount);
    self.StatementLetter = ko.observable(statementLetter);
}

var TransactionDealDetailModel = {
    ValueDate: ko.observable(),
    AccountNumber: ko.observable(),
    StatementLetter: ko.observable(StatementModel),
    TZRef: ko.observable(),
    RateType: ko.observable(),
    ProductType:ko.observable(ProductTypeModel),
    Rate: ko.observable(),
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(0),
    AmountUSD: ko.observable(0),
    Account: ko.observable(AccountModel),
    Type: ko.observable(20),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]),
    DetailCompliance: ko.observable(),
    BeneName: ko.observable(),
    BeneAccNumber: ko.observable(),
    utilizationAmount: ko.observable(0.00),
    underlyings: ko.observableArray([]),
    TZReference: ko.observable(),
    IsBookDeal: ko.observable(false),
    bookunderlyingamount: ko.observable(),
    bookunderlyingcurrency: ko.observable(),
    bookunderlyingcode: ko.observable(),
    bookunderlyingdesc: ko.observable(),
    IsFxTransaction: ko.observable(false),
    OtherUnderlying: ko.observable()
};

var Parameter = {
    Currencies: ko.observableArray([CurrencyModel]),
    ProductType:ko.observableArray([ProductTypeModel]),
    FXCompliances: ko.observableArray([FXComplianceModel]),
    DocumentTypes: ko.observableArray([DocumentTypeModel]),
    DocumentPurposes: ko.observableArray([DocumentPurposeModel]),
    RateType: ko.observableArray([RateTypeModel]),
    StatementLetter: ko.observableArray([StatementModel])
};

var SelectedModel = {
    Currency: ko.observable(),
    Account: ko.observable(),
    DocumentType: ko.observable(),
    DocumentPurpose: ko.observable(),
    NewCustomer: ko.observable({
        Currency: ko.observable()
    }),
    ProductType : ko.observable(),
    RateType: ko.observable(),
    StatementLetter: ko.observable()
};

var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;
    self.IsRole = function (name) {
        var result = false;
        $.each($.cookie(api.cookie.spUser).Roles, function (index, item) {
            if (Const_RoleName[item.ID] == name) result = true; //if (item.Name == name) result = true;
        });
        return result;
    };

    // Sharepoint User
    self.SPUser = ko.observable(SPUser);

    // rate param
    self.Rate = ko.observable(0);

    // Is check attachments
    self.IsFXTransactionAttach = ko.observable(false);
    self.IsUploaded = ko.observable(false);
    self.IsUploading = ko.observable(false);
    self.SelectingUnderlying = ko.observable(false);

    //bu group val
    self.isDealBUGroup= ko.observable(false);

    //new underlying type
    self.isNewUnderlying= ko.observable(false);
    self.underlyingCode = ko.observable();

    //is fx transaction
    self.t_IsFxTransaction = ko.observable(false);
    self.t_IsFxTransactionToIDR = ko.observable(false);
    self.t_IsFxTransactionToIDRB = ko.observable(false);

    //underlying declare
    self.AmountModel = ko.observable(AmountModel);

    // input form controls
    self.IsEditable = ko.observable(true);

    // Parameters
    self.Parameter = ko.observable(Parameter);

    // Main Model
    self.TransactionDealDetailModel = ko.observable(TransactionDealDetailModel);

    // Main Model
    self.TransactionDealModel = ko.observable(TransactionDealModel);

    // Double Transaction
    self.DoubleTransactions = ko.observableArray([]);

    // Validation User
    self.UserValidation = ko.observable({
        UserID: ko.observable(),
        Password: ko.observable(),
        Error: ko.observable()
    });

    // Options selected value
    self.Selected = ko.observable(SelectedModel);
    // set Indicator Submited Statement letter
    self.Submitted = ko.observable();
    // Temporary documents
    self.Documents = ko.observableArray([]);
    self.DocumentPath = ko.observable();
    self.DocumentType = ko.observable();
    self.DocumentPurpose = ko.observable();
    self.DocumentFileName = ko.observable();

    // Uploading document
    self.UploadDocumentUnderlying = function () {

        // show the dialog task form
        $("#modal-form-Attach").modal('show');
        self.TempSelectedAttachUnderlying([]);
        self.ID_a(0);
        self.DocumentPurpose_a(new DocumentPurposeModel2('', '', ''));
        self.DocumentType_a(new DocumentTypeModel2('', ''));
        self.DocumentPath_a('');
        GetDataUnderlyingAttach();
    }

    self.UploadDocument = function () {
        self.DocumentPath(null);
        self.Selected().DocumentType(null);
        self.Selected().DocumentPurpose(null);
        self.DocumentType(null);
        self.DocumentPurpose(null);
        $("#modal-form-upload").modal('show');

    }

    // Add new document handler
    self.AddDocument = function () {
        var doc = {
            ID: 0,
            Type: self.DocumentType(),
            Purpose: self.DocumentPurpose(),
            FileName: self.DocumentFileName(),
            DocumentPath: self.DocumentPath(),
            LastModifiedDate: new Date(),
            LastModifiedBy: null
        };

        //alert(JSON.stringify(self.DocumentPath()))
        if (doc.Type == null || doc.Purpose == null || doc.DocumentPath == null) {
            alert("Please complete the upload form fields.")
        } else {
            self.Documents.push(doc);

            // hide upload dialog
            $("#modal-form-upload").modal('hide');
        }
    };

    self.EditDocument = function (data) {
        self.DocumentPath(data.DocumentPath);
        self.Selected().DocumentType(data.Type.ID);
        self.Selected().DocumentPurpose(data.Purpose.ID);

        // show upload dialog
        $("#modal-form-upload").modal('show');
    };

    self.RemoveDocument = function (data) {
        //alert(JSON.stringify(data))
        self.Documents.remove(data);
    };

    // Save as draft handler
    self.Submit = function () {
        // validation
        self.IsDeal(false);
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            self.IsEditable(true);
            ValidateTransaction();
        }
        else {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        }
    };

    // User Validation process
    self.ValidationProcess = function () {
        UploadDocuments(); // start upload docs and save the transaction
    };

    // Continue editing
    self.ContinueEditing = function () {
        // enable form input controls
        self.IsEditable(true);
    };


    //--------------------------- set Underlying function & variable start
    self.IsUnderlyingMode = ko.observable(true);
    self.ID_u = ko.observable("");
    self.CIF_u = ko.observable(cifData);
    self.CustomerName_u = ko.observable(customerNameData);
    self.UnderlyingDocument_u = ko.observable(new UnderlyingDocumentModel2('', '', ''));
    self.ddlUnderlyingDocument_u = ko.observableArray([]);
    self.DocumentType_u = ko.observable(new DocumentTypeModel2('', ''));
    self.ddlDocumentType_u = ko.observableArray([]);

    self.AmountUSD_u = ko.observable(0.00);

    //self.Currency_u = ko.observableArray([new CurrencyModel2('', '', '')]);
    self.Currency_u = ko.observable(new CurrencyModel2('', '', ''));

    //self.RateType_u = ko.observableArray([new RateTypeModel2('', '', '')]);
    self.RateType_u = ko.observable(new RateTypeModel2('', '', ''));

    self.Rate_u = ko.observable(0);

    self.ddlCurrency_u = ko.observableArray([]);
    self.ddlRateType_u = ko.observableArray([]);

    //self.StatementLetter_u = ko.observableArray([new StatementLetterModel('', '')]);
    self.StatementLetter_u = ko.observable(new StatementLetterModel('', ''));

    self.ddlStatementLetter_u = ko.observableArray([]);
    self.Amount_u = ko.observable(0);
    self.AttachmentNo_u = ko.observable("");
    self.SelectUnderlyingDocument_u = ko.observable("");
    self.IsDeclarationOfException_u = ko.observable(false);
    self.StartDate_u = ko.observable('');
    self.EndDate_u = ko.observable('');
    self.DateOfUnderlying_u = ko.observable();
    self.ExpiredDate_u = ko.observable("");
    self.ReferenceNumber_u = ko.observable("");
    self.SupplierName_u = ko.observable("");
    self.InvoiceNumber_u = ko.observable("");
    self.IsStatementA = ko.observable(true);
    self.IsProforma_u = ko.observable(false);
    self.IsSelectedProforma = ko.observable(false);
    self.IsUtilize_u = ko.observable(false);


    self.Amounttmp_u = ko.computed({
        read: function () {
            if (self.StatementLetter_u().ID() == 1) {
                self.Currency_u().ID(1);
                self.Amount_u(TotalDealModel.TreshHold);
                // set default last day of month
                var date = new Date();
                var lastDay = new Date(date.getFullYear(),date.getMonth()+1,0);
                var day = lastDay.getDate();
                var month = lastDay.getMonth()+1;
                var year = lastDay.getFullYear()
                var fixLastDay = year+"/"+month+"/"+day;
                self.ExpiredDate_u(fixLastDay);
            }
            var value = self.Amount_u();

            var e = document.getElementById("Amount_u").value;
            e = e.toString().replace('.', '').replace('.', '').replace('.', '').replace('.', '');
            self.Amount_u(e);

            value = value.toString().replace('.', '').replace('.', '').replace('.', '').replace('.', '');
            var res = parseFloat(value) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res).toFixed(2));

        },
        write: function (data) {
            var res = data * parseInt(self.Rate_u()) / parseInt(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res).toFixed(2));

        }
    }, this);

    self.CaluclateRate_u = ko.computed({
        read: function () {
            var CurrencyID = self.Currency_u().ID();
            var AccountID = self.Selected().Account;
            if (CurrencyID != '' && CurrencyID != undefined) {
                GetRateIDRAmount(CurrencyID);
            } else {
                self.Rate_u(0);
            }
            if (AccountID == '' && AccountID != undefined) {
                alert('test');
            }
        },
        write: function () { }
    });

    self.Proformas = ko.observableArray([new SelectModel('', '')]);
    self.CodeUnderlying = ko.computed({
        read: function () {
            if (self.UnderlyingDocument_u().ID() != undefined && self.UnderlyingDocument_u().ID() != '') {
                var item = ko.utils.arrayFirst(self.ddlUnderlyingDocument_u(), function (x) {
                        return self.UnderlyingDocument_u().ID() == x.ID();
                    }
                )
                if (item != null) {
                    return item.Code();
                }
                else {
                    return ''
                }
            } else { return '' }
        },
        write: function (value) {
            return value;
        }
    });

    self.OtherUnderlying_u = ko.observable('');
    self.tmpOtherUnderlying = ko.observable('');
    self.IsSelected_u = ko.observable(false);
    self.LastModifiedBy_u = ko.observable("");
    self.LastModifiedDate_u = ko.observable("");

    // Attach Variable
    self.ID_a = ko.observable(0);
    self.FileName_a = ko.observable('');
    self.DocumentPurpose_a = ko.observable(new DocumentPurposeModel2('', '', ''));
    self.ddlDocumentPurpose_a = ko.observableArray([]);
    self.DocumentType_a = ko.observable(new DocumentTypeModel2('', ''));
    self.ddlDocumentType_a = ko.observableArray([]);
    self.DocumentRefNumber_a = ko.observable("");
    self.LastModifiedBy_a = ko.observable("");
    self.LastModifiedDate_a = ko.observable("");
    self.CustomerUnderlyingMappings_a = ko.observableArray([new CustomerUnderlyingMappingModel('', '', '')]);

    self.Documents = ko.observableArray([]);
    self.DocumentPath_a = ko.observable("");

    // filter Underlying
    self.UnderlyingFilterCIF = ko.observable("");
    self.UnderlyingFilterParentID = ko.observable("");
    self.UnderlyingFilterIsSelected = ko.observable(false);
    self.UnderlyingFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingFilterDocumentType = ko.observable("");
    self.UnderlyingFilterCurrency = ko.observable("");
    self.UnderlyingFilterStatementLetter = ko.observable("");
    self.UnderlyingFilterAmount = ko.observable("");
    self.UnderlyingFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingFilterExpiredDate = ko.observable("");
    self.UnderlyingFilterReferenceNumber = ko.observable("");
    self.UnderlyingFilterSupplierName = ko.observable("");
    self.UnderlyingFilterIsProforma = ko.observable("");
    self.UnderlyingFilterIsAvailable = ko.observable(false);
    self.UnderlyingFilterIsExpiredDate = ko.observable(false);

    // filter Attach Underlying File
    self.AttachFilterFileName = ko.observable("");
    self.AttachFilterDocumentPurpose = ko.observable("");
    self.AttachFilterModifiedDate = ko.observable("");
    self.AttachFilterDocumentRefNumber = ko.observable("");
    self.AttachFilterAttachmentReference = ko.observable("");

    // filter Underlying for Attach
    self.UnderlyingAttachFilterIsSelected = ko.observable("");
    self.UnderlyingAttachFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingAttachFilterDocumentType = ko.observable("");
    self.UnderlyingAttachFilterCurrency = ko.observable("");
    self.UnderlyingAttachFilterStatementLetter = ko.observable("");
    self.UnderlyingAttachFilterAmount = ko.observable("");
    self.UnderlyingAttachFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingAttachFilterExpiredDate = ko.observable("");
    self.UnderlyingAttachFilterReferenceNumber = ko.observable("");
    self.UnderlyingAttachFilterSupplierName = ko.observable("");

    // filter Underlying for Attach
    self.ProformaFilterIsSelected = ko.observable(false);
    self.ProformaFilterUnderlyingDocument = ko.observable("");
    self.ProformaFilterDocumentType = ko.observable("Proforma");
    self.ProformaFilterCurrency = ko.observable("");
    self.ProformaFilterStatementLetter = ko.observable("");
    self.ProformaFilterAmount = ko.observable("");
    self.ProformaFilterDateOfUnderlying = ko.observable("");
    self.ProformaFilterExpiredDate = ko.observable("");
    self.ProformaFilterReferenceNumber = ko.observable("");
    self.ProformaFilterSupplierName = ko.observable("");
    self.ProformaFilterIsProforma = ko.observable(false);



    //// start Checked Calculate for Attach Grid and Underlying Grid
    self.TempSelectedAttachUnderlying = ko.observableArray([]);

    //// start Checked Calculate for Underlying Proforma Grid
    self.TempSelectedUnderlyingProforma = ko.observableArray([]);

    //// start Checked Calculate for Underlying Grid
    self.TempSelectedUnderlying = ko.observableArray([]);

    function ResetDataAttachment(index, isSelect) {
        self.CustomerAttachUnderlyings()[index].IsSelectedAttach = isSelect;
        var dataRows = self.CustomerAttachUnderlyings().slice(0);
        self.CustomerAttachUnderlyings([]);
        self.CustomerAttachUnderlyings(dataRows);
    }

    function ResetDataProforma(index, isSelect) {
        self.CustomerUnderlyingProformas()[index].IsSelectedProforma = isSelect;
        var dataRows = self.CustomerUnderlyingProformas().slice(0);
        self.CustomerUnderlyingProformas([]);
        self.CustomerUnderlyingProformas(dataRows);
    }

    function ResetDataUnderlying(index, isSelect) {
        self.CustomerUnderlyings()[index].IsEnable = isSelect;
        var dataRows = self.CustomerUnderlyings().slice(0);
        self.CustomerUnderlyings([]);
        self.CustomerUnderlyings(dataRows);
    }

    function SetSelectedProforma(data) {
        // if(self.TempSelectedUnderlyingProforma().length>0) {
        for (var i = 0 ; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlyingProforma(), function (item) {
                return item.ID == data[i].ID;
            });
            if (selected != null) {
                data[i].IsSelectedProforma = true;
            }
            else {
                data[i].IsSelectedProforma = false;
            }
        }
    }

    function TotalUtilize() {
        var total = 0;
        ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (item) {
            if (item.Enable() == false)
                total += parseFloat(ko.utils.unwrapObservable(item.USDAmount()));
        });
        var fixTotal = parseFloat(total).toFixed(2);
        return fixTotal;
    }

    function GetRateIDRAmount(CurrencyID) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
            params: {
            },
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    self.Rate_u(data.RupiahRate);
                    //if(CurrencyID!=1){ // if not USD

                    var e = document.getElementById("Amount_u").value;
                    e = e.replace('.', '').replace('.', '').replace('.', '').replace('.', '');
                    self.Amount_u(e);

                    res = e * data.RupiahRate / idrrate;
                    res = Math.round(res * 100) / 100;
                    self.AmountUSD_u(parseFloat(res).toFixed(2));
                    //}
                }
            }
        });
    }

    function SetSelectedUnderlying(data) {

        for (var i = 0 ; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (item) {
                return item.ID == data[i].ID;
            });
            if (selected != null) {
                data[i].IsUtilize = true;
            }
            else {
                data[i].IsUtilize = false;
            }
        }
    }

    self.onSelectionAttach = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            //self.IsSelectedAttach(false);
            self.TempSelectedAttachUnderlying.remove(data);
            ResetDataAttachment(index, false);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        } else {
            //self.IsSelectedAttach(true);
            self.TempSelectedAttachUnderlying.push({
                ID: item.ID
            });
            ResetDataAttachment(index, true);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        }
    }

    self.onSelectionProforma = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlyingProforma(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            //self.IsSelectedAttach(false);
            self.TempSelectedUnderlyingProforma.remove(data);
            ResetDataProforma(index, false);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        } else {
            //self.IsSelectedAttach(true);
            self.TempSelectedUnderlyingProforma.push({
                ID: item.ID
            });
            ResetDataProforma(index, true);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        }
    }

    self.TransactionDealDetailModel().bookunderlyingcode.subscribe(function(underlyingID) {
        //get underlying code based on selected
        if(underlyingID > 0)
        {
            GetUnderlyingDocName(underlyingID);
        }
    });

    function GetUnderlyingDocName(underlyingID) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.underlyingdoc + "/" + underlyingID,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if(data != null)
                    {
                        self.isNewUnderlying(false); //return default disabled

                        // available type to input for new underlying name if code is 999
                        if(data.Code == '999')
                        {
                            self.TransactionDealDetailModel().bookunderlyingdesc('');
                            self.isNewUnderlying(true);
                            $('#book-underlying-desc').focus();

                            $('#book-underlying-desc').attr('data-rule-required', true);
                        }
                        else
                        {
                            self.isNewUnderlying(false);
                            self.TransactionDealDetailModel().OtherUnderlying('');

                            $('#book-underlying-desc').attr('data-rule-required', false);
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    self.onSelectionUtilize = function (index, item) {

        var data = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
            return x.ID() == item.ID;
        });

        if (data != null) { //uncheck condition

            //console.log('onSelectionUtilize data not null');

            self.TempSelectedUnderlying.remove(data);
            ResetDataUnderlying(index, false);
            setTotalUtilize();

        } else {

            //console.log('onSelectionUtilize data is null');

            var statementA = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                return x.StatementLetter() == 1;
            });
            var statementB = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                return x.StatementLetter() == 2;
            });

            var amount = document.getElementById("new-eqv-usd").value;
            if (amount != null) {
                amount = amount.replace(',', '').replace(',','').replace(',','').replace(',','');
            }

            if (((statementA == null && item.StatementLetter.ID == 2) || (statementB == null && item.StatementLetter.ID == 1))
                && parseFloat(self.TransactionDealDetailModel().utilizationAmount()) < parseFloat(amount) &&
                //viewModel.TransactionDealDetailModel().StatementLetter().ID == item.StatementLetter.ID) {
                self.Selected().StatementLetter() == item.StatementLetter.ID) {
                self.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, false, item.AvailableAmount, item.StatementLetter.ID));
                ResetDataUnderlying(index, true);
                setTotalUtilize();
            } else {
                ResetDataUnderlying(index, false);
            }
        }

    }

    // New Data flag
    self.IsDeal = ko.observable(false);

    // New Data flag
    self.IsNewDataUnderlying = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyings = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyingProformas = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyingFiles = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerAttachUnderlyings = ko.observableArray([]);

    // grid properties Underlying
    self.UnderlyingGridProperties = ko.observable();
    self.UnderlyingGridProperties(new GridPropertiesModel(GetDataUnderlying));

    // grid properties Attach Grid
    self.AttachGridProperties = ko.observable();
    self.AttachGridProperties(new GridPropertiesModel(GetDataAttachFile));

    // grid properties Underlying File
    self.UnderlyingAttachGridProperties = ko.observable();
    self.UnderlyingAttachGridProperties(new GridPropertiesModel(GetDataUnderlyingAttach));

    // grid properties Proforma
    self.UnderlyingProformaGridProperties = ko.observable();
    self.UnderlyingProformaGridProperties(new GridPropertiesModel(GetDataUnderlyingProforma));

    // set default sorting for Underlying Grid
    self.UnderlyingGridProperties().SortColumn("ID");
    self.UnderlyingGridProperties().SortOrder("ASC");

    // set default sorting for Underlying File Grid
    self.AttachGridProperties().SortColumn("FileName");
    self.AttachGridProperties().SortOrder("ASC");

    // set default sorting for Attach Underlying Grid
    self.UnderlyingAttachGridProperties().SortColumn("ID");
    self.UnderlyingAttachGridProperties().SortOrder("ASC");

    // set default sorting for Proforma grid
    self.UnderlyingProformaGridProperties().SortColumn("ID");
    self.UnderlyingProformaGridProperties().SortOrder("ASC");

    // bind clear filters
    self.UnderlyingClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.UnderlyingFilterIsSelected(false);
        self.UnderlyingFilterUnderlyingDocument("");
        self.UnderlyingFilterDocumentType("");
        self.UnderlyingFilterCurrency("");
        self.UnderlyingFilterStatementLetter("");
        self.UnderlyingFilterAmount("");
        self.UnderlyingFilterDateOfUnderlying("");
        self.UnderlyingFilterExpiredDate("");
        self.UnderlyingFilterReferenceNumber("");
        self.UnderlyingFilterSupplierName("");
        self.UnderlyingFilterIsProforma("");
        self.UnderlyingFilterIsAvailable(false);
        self.UnderlyingFilterIsExpiredDate(false);

        GetDataUnderlying();
    };

    self.ProformaClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.ProformaFilterIsSelected(false);
        self.ProformaFilterUnderlyingDocument("");
        self.ProformaFilterDocumentType("Proforma");
        self.ProformaFilterCurrency("");
        self.ProformaFilterStatementLetter("");
        self.ProformaFilterAmount("");
        self.ProformaFilterDateOfUnderlying("");
        self.ProformaFilterExpiredDate("");
        self.ProformaFilterReferenceNumber("");
        self.ProformaFilterSupplierName("");
        self.ProformaFilterIsProforma("");

        GetDataUnderlyingProforma();
    };

    // bind clear filters
    self.AttachClearFilters = function () {
        self.AttachFilterFileName("");
        self.AttachFilterDocumentPurpose("");
        self.AttachFilterModifiedDate("");
        self.AttachFilterDocumentRefNumber("");
        self.AttachFilterAttachmentReference("");

        GetDataAttachFile();
    };

    // bind clear filters
    self.UnderlyingAttachClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.UnderlyingAttachFilterIsSelected(false);
        self.UnderlyingAttachFilterUnderlyingDocument("");
        self.UnderlyingAttachFilterDocumentType("");
        self.UnderlyingAttachFilterCurrency("");
        self.UnderlyingAttachFilterStatementLetter("");
        self.UnderlyingAttachFilterAmount("");
        self.UnderlyingAttachFilterDateOfUnderlying("");
        self.UnderlyingAttachFilterExpiredDate("");
        self.UnderlyingAttachFilterReferenceNumber("");
        self.UnderlyingAttachFilterSupplierName("");

        GetDataUnderlyingAttach();
    };

    self.GetApplicationID = function () { GetApplicationID(); };

    // bind get data function to view
    self.GetDataUnderlying = function () {
        GetDataUnderlying();
    };

    self.GetSelectedUtilizeID = function () {
        GetSelectedUtilizeID();
    }

    self.GetTotalAmount = function (CIFData, AmountUSD) {
        GetTotalAmount(CIFData, AmountUSD);
    }

    self.GetDataAttachFile = function () {
        GetDataAttachFile();
    }

    self.GetDataUnderlyingAttach = function () {
        GetDataUnderlyingAttach();
    }

    self.GetDataUnderlyingProforma = function () {
        GetDataUnderlyingProforma();
    }

    self.GetUnderlyingParameters = function (data) {
        GetUnderlyingParameters(data);
    }


    self.CalculateFX = function(amountUSD){
        CalculateFX(amountUSD);
    }

    //The Object which stored data entered in the observables
    var CustomerUnderlying = {
        ID: self.ID_u,
        CIF: self.CIF_u,
        CustomerName: self.CustomerName_u,
        UnderlyingDocument: self.UnderlyingDocument_u,
        DocumentType: self.DocumentType_u,
        Currency: self.Currency_u,
        Amount: self.Amount_u,
        Rate: self.Rate_u,
        AmountUSD: self.AmountUSD_u,
        AvailableAmountUSD: self.AvailableAmount_u,
        AttachmentNo: self.AttachmentNo_u,
        StatementLetter: self.StatementLetter_u,
        IsDeclarationOfException: self.IsDeclarationOfException_u,
        StartDate: self.StartDate_u,
        EndDate: self.EndDate_u,
        DateOfUnderlying: self.DateOfUnderlying_u,
        ExpiredDate: self.ExpiredDate_u,
        ReferenceNumber: self.ReferenceNumber_u,
        SupplierName: self.SupplierName_u,
        InvoiceNumber: self.InvoiceNumber_u,
        IsSelectedProforma: self.IsSelectedProforma,
        IsProforma: self.IsProforma_u,
        IsUtilize: self.IsUtilize_u,
        IsEnable: self.IsEnable_u,
        Proformas: self.Proformas,
        OtherUnderlying: self.OtherUnderlying_u,
        LastModifiedBy: self.LastModifiedBy_u,
        LastModifiedDate: self.LastModifiedDate_u
    };

    var Documents = {
        Documents: self.Documents,
        DocumentPath: self.DocumentPath_a,
        Type: self.DocumentType_a,
        Purpose: self.DocumentPurpose_a
    }

    var CustomerUnderlyingFile = {
        ID: self.ID_a,
        FileName: DocumentModels().FileName,
        DocumentPath: DocumentModels().DocumentPath,
        DocumentPurpose: self.DocumentPurpose_a,
        DocumentType: self.DocumentType_a,
        DocumentRefNumber: self.DocumentRefNumber_a,
        LastModifiedBy: self.LastModifiedBy_a,
        LastModifiedDate: self.LastModifiedDate_a,
        CustomerUnderlyingMappings: self.CustomerUnderlyingMappings_a
    };

    function CalculateFX(amountUsd){
        var t_TotalAmountsUSD = TotalDealModel.Total.IDRFCYDeal;
        var t_RemainingBalance = TotalDealModel.Total.RemainingBalance;
        viewModel.AmountModel().TotalUtilization(TotalDealModel.Total.UtilizationDeal);
        if (amountUsd != "") {
            viewModel.TransactionDealDetailModel().AmountUSD(amountUsd);
            var t_Treshold = TotalDealModel.TreshHold;
            var t_FCYTreshold = TotalDealModel.FCYIDRTrashHold;
            var t_FcyAmount = parseFloat(amountUsd);
            var t_IsFxTransaction = viewModel.t_IsFxTransaction();
            var t_IsFxTransactionToIDR = viewModel.t_IsFxTransactionToIDR();
            if (t_IsFxTransactionToIDR){
                if (t_FcyAmount >= t_FCYTreshold && viewModel.TransactionDealDetailModel().ProductType().IsFlowValas == true) {
                    viewModel.Selected().StatementLetter(2);
                    viewModel.TransactionDealDetailModel().StatementLetter().ID = 2;
                    t_TotalAmountsUSD = parseFloat(t_TotalAmountsUSD) + parseFloat(amountUsd);
                } else {
                    viewModel.Selected().StatementLetter(3);
                    viewModel.TransactionDealDetailModel().StatementLetter().ID = 3;
                }
            }
            if(t_IsFxTransaction)
            {
                t_TotalAmountsUSD =  parseFloat(t_TotalAmountsUSD) + parseFloat(amountUsd);
            }
            if(viewModel.TransactionDealDetailModel().StatementLetter() != undefined ) {
                if (viewModel.TransactionDealDetailModel().StatementLetter().ID == 1) {
                    viewModel.TransactionDealDetailModel().bookunderlyingcode(36);
                    if(t_TotalAmountsUSD <= t_Treshold) {
                        t_RemainingBalance = 0.00;
                    }
                }else{
                    t_RemainingBalance = t_RemainingBalance - t_FcyAmount;
                }
                if(t_IsFxTransactionToIDR) {
                    if(t_FcyAmount >= t_FCYTreshold &&  viewModel.TransactionDealDetailModel().ProductType().IsFlowValas==true) {
                        t_RemainingBalance = t_RemainingBalance - t_FcyAmount;
                    }
                }
            }
        }else{
            var t_TotalAmountsUSD = TotalDealModel.Total.IDRFCYDeal;
        }
        viewModel.AmountModel().TotalAmountsUSD(t_TotalAmountsUSD);
        viewModel.AmountModel().RemainingBalance(t_RemainingBalance);
    }
    function GetApplicationID() {
        var applicationID = 'FX-' + $.now();
        viewModel.TransactionDealDetailModel().ApplicationID(applicationID);
    };

    function SetMappingAttachment() {
        CustomerUnderlyingFile.CustomerUnderlyingMappings([]);
        for (var i = 0 ; i < self.TempSelectedAttachUnderlying().length; i++) {
            CustomerUnderlyingFile.CustomerUnderlyingMappings.push(new CustomerUnderlyingMappingModel(0, self.TempSelectedAttachUnderlying()[i].ID, customerNameData + '-(A)'));
        }
    }

    function SetMappingProformas() {
        CustomerUnderlying.Proformas([]);
        for (var i = 0 ; i < self.TempSelectedUnderlyingProforma().length; i++) {
            CustomerUnderlying.Proformas.push(new SelectModel(cifData, self.TempSelectedUnderlyingProforma()[i].ID));
        }
    }

    function setTotalUtilize() {
        var total = 0;
        ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (item) {
            total += parseFloat(ko.utils.unwrapObservable(item.USDAmount()));
        });
        viewModel.TransactionDealDetailModel().utilizationAmount(parseFloat(total).toFixed(2));
    }

    function setStatementA() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetStatementA/" + cifData,
            data: null, //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            }, success: function (data) {
                if (data.IsStatementA) {
                    self.StatementLetter_u(new StatementLetterModel(2, 'Statement B'));
                    self.IsStatementA(false);
                }else
                { self.IsStatementA(true);}
            }
        });
    }
    function setRefID() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetRefID/" + cifData,
            data: null, //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            }, success: function (data) {
                self.ReferenceNumber_u(customerNameData + '-' + data.ID);
            }
        });

    }

    // Save is book deal
    self.IsBooked = function () {
        self.IsDeal(true);

        var t_TreshHold = TotalDealModel.TreshHold;
        var t_IsFxTransaction = viewModel.t_IsFxTransaction();
        var t_IsFxTransactionToIDR = viewModel.t_IsFxTransactionToIDR();
        var t_IsFxTransactionToIDRB = viewModel.t_IsFxTransactionToIDRB();
        var t_TotalAmountsUSD = viewModel.AmountModel().TotalAmountsUSD();
        var t_StatementLeter = self.Selected().StatementLetter();
        var t_AmountUSD = parseFloat(viewModel.TransactionDealDetailModel().AmountUSD());

        if (t_IsFxTransaction)
        {
            viewModel.TransactionDealDetailModel().IsFxTransaction(true);
        }
        else
        {
            viewModel.TransactionDealDetailModel().IsFxTransaction(false);
        }

        $('#book-underlying-code').attr('data-rule-required', true);
        $('#book-underlying-code').rules('add', 'required');

        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            viewModel.IsEditable(false);
            if (t_IsFxTransaction||t_IsFxTransactionToIDR) {
                if ((t_StatementLeter == 1 && parseFloat(t_TotalAmountsUSD) <= t_TreshHold) || (t_StatementLeter == 2)||t_StatementLeter == 3) {
                   if (t_IsFxTransactionToIDR) {
                       if (t_AmountUSD >= TotalDealModel.FCYIDRTrashHold && t_IsFxTransactionToIDRB) {
                           viewModel.Selected().StatementLetter(2);
                       } else {
                           viewModel.Selected().StatementLetter(3);
                       }
                           //alert('book'); viewModel.IsEditable(true);
                           ValidateTransaction();
                       }
                    else {
                       if (!(viewModel.TransactionDealDetailModel().Currency().Code != 'IDR' && t_StatementLeter == 3)) {
                           //alert('book1'); viewModel.IsEditable(true);
                           ValidateTransaction();
                       }else{
                           viewModel.IsEditable(true);
                           ShowNotification("Form Validation Warning", "Please Select A or B Statement Letter.", 'gritter-warning', false);
                       }
                   }
                   // ValidateTransaction();
                } else {
                    viewModel.IsEditable(true);
                    ShowNotification("Form Validation Warning", "Total Amount Statement A hit " + t_TreshHold + ", Please Select Statement B  ", 'gritter-warning', false);
                }
            } else {
                viewModel.IsEditable(true);
                ShowNotification("Form Validation Warning", "Transaction FCY->FCY, please select IDR->FCY or FCY->IDR transaction", 'gritter-warning', false);
            }
        } else {
            viewModel.IsEditable(true);
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
            $('#book-underlying-code').attr('data-rule-required', false);
            $('#book-underlying-code').rules('remove', 'required');
        }
    };

    self.save_u = function () {
        // validation
        var form = $("#frmUnderlying");
        form.validate();

        if (form.valid()) {
            CustomerUnderlying.OtherUnderlying = self.CodeUnderlying() == '999' ? self.OtherUnderlying_u : '';
            SetMappingProformas();
            CustomerUnderlying.AvailableAmountUSD = self.AmountUSD_u();
            //Ajax call to insert the CustomerUnderlyings
            $.ajax({
                type: "POST",
                url: api.server + api.url.customerunderlying + "/Save",
                data: ko.toJSON(CustomerUnderlying), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        $("#modal-form-Underlying").modal('hide');

                        // refresh data
                        GetDataUnderlying();

                        //new calculated
                        DealModel.cif = cifData;
                        GetRemainingBalance(data,onSuccessRemainingBlnc,OnErrorDeal);
                        //GetAvailableStatementB(cifData);
                        //viewModel.GetTotalAmount(cifData, viewModel.TransactionDealDetailModel().AmountUSD());

                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }
    };

    function onSuccessRemainingBlnc()
    {
        CalculateFX(viewModel.TransactionDealDetailModel().AmountUSD());
    }

    self.save_a = function () {

        var form = $("#frmUnderlying");
        form.validate();
        if (form.valid()) {
            //viewModel.IsEditable(false);
            self.IsUploading(true)

            var FxWithUnderlying = (self.TempSelectedAttachUnderlying().length > 0);
            var FxWithNoUnderlying = (self.DocumentPurpose_a().ID() != 2);

            if(FxWithUnderlying || FxWithNoUnderlying) {
                var data = {
                    CIF: CustomerUnderlying.CIF,
                    Name: CustomerUnderlying.CustomerName,
                    Type: ko.utils.arrayFirst(self.ddlDocumentType_a(), function (item) {
                        return item.ID == self.DocumentType_a().ID();
                    }),
                    Purpose: ko.utils.arrayFirst(self.ddlDocumentPurpose_a(), function (item) {
                        return item.ID == self.DocumentPurpose_a().ID();
                    })
                };

                if(FxWithUnderlying)
                {
                    UploadFileUnderlying(data, Documents, SaveDataFile);
                    // refresh data
                    GetDataUnderlying();
                }
                else if(FxWithNoUnderlying) {
                    self.IsFXTransactionAttach(true);

                    var doc = {
                        ID: 0,
                        Type: data.Type,
                        Purpose: data.Purpose,
                        FileName: Documents.DocumentPath().name,
                        DocumentPath: self.DocumentPath_a(),
                        LastModifiedDate: new Date(),
                        LastModifiedBy: null
                    };

                    //alert(JSON.stringify(self.DocumentPath()))
                    if (doc.Type == null || doc.Purpose == null || doc.DocumentPath == null) {
                        alert("Please complete the upload form fields.")
                    } else {
                        self.Documents.push(doc);

                        // hide upload dialog
                        $("#modal-form-Attach").modal('hide');
                        //viewModel.IsEditable(true);
                        viewModel.IsUploading(false);
                    }
                }
            } else {
                //viewModel.IsEditable(true);
                viewModel.IsUploading(false);
                alert('Please select the underlying document');
            }


        };
    };

    function SaveDataFile() {

        SetMappingAttachment();
        //Ajax call to insert the CustomerUnderlyings
        CustomerUnderlyingFile.FileName = DocumentModels().FileName;
        CustomerUnderlyingFile.DocumentPath = DocumentModels().DocumentPath;
        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlyingfile,
            data: ko.toJSON(CustomerUnderlyingFile), //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    viewModel.IsEditable(true);

                    // send notification
                    ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                    // hide current popup window
                    $("#modal-form-Attach").modal('hide');

                    viewModel.IsUploading(false);

                    // refresh data
                    GetDataAttachFile();
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    //viewModel.IsEditable(true);
                    viewModel.IsUploading(false);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                //viewModel.IsEditable(true);
                viewModel.IsUploading(false);
            }
        });


    }

    self.update_u = function () {
        var form = $("#frmUnderlying");
        form.validate();

        if (form.valid()) {
            // hide current popup window
            $("#modal-form-Underlying").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form-Underlying").modal('show');
                } else {
                    CustomerUnderlying.OtherUnderlying = self.CodeUnderlying() == '999' ? self.OtherUnderlying_u : '';
                    SetMappingProformas();
                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.customerunderlying + "/" + CustomerUnderlying.ID(),
                        data: ko.toJSON(CustomerUnderlying),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {

                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                $("#modal-form-Underlying").modal('hide');

                                // refresh data
                                GetDataUnderlying();

                                //new calculated
                                DealModel.cif = cifData;
                                GetRemainingBalance(data,onSuccessRemainingBlnc,OnErrorDeal);
                                //GetAvailableStatementB(cifData)
                                //viewModel.GetTotalAmount(cifData,viewModel.TransactionDealDetailModel().AmountUSD());
                                //viewModel.GetTotalAmount(cifData, true);
                                //viewModel.Calculating(viewModel.TransactionDealDetailModel().AmountUSD);
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            //ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.delete_u = function () {
        // hide current popup window
        $("#modal-form-Underlying").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form-Underlying").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customerunderlying + "/" + CustomerUnderlying.ID(),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {

                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                            // refresh data
                            GetDataUnderlying();

                            //new calculated
                            DealModel.cif = cifData;
                            GetRemainingBalance(data,onSuccessRemainingBlnc,OnErrorDeal);
                            //GetAvailableStatementB(cifData);
                            //viewModel.GetTotalAmount(cifData, viewModel.TransactionDealDetailModel().AmountUSD());
                            //viewModel.GetTotalAmount(cifData, false);
                            //viewModel.GetTotalAmount(cifData, true);
                            //Calculating(viewModel.TransactionDealDetailModel().AmountUSD);
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    self.cancel_u = function () {
    }

    self.cancel_a = function () {
    }

    self.delete_a = function (item) {

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                //$("#modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customerunderlyingfile + "/" + item.ID,
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            DeleteFile(item.DocumentPath);
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetDataAttachFile();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    }

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);
        if(date =='1970/01/01 00:00:00'){
            return "";
        }

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    //Function to Display record to be updated
    self.GetUnderlyingSelectedRow = function (data) {

        self.IsNewDataUnderlying(false);

        $("#modal-form-Underlying").modal('show');

        self.ID_u(data.ID);
        self.CustomerName_u(data.CustomerName);
        self.UnderlyingDocument_u(new UnderlyingDocumentModel2(data.UnderlyingDocument.ID, data.UnderlyingDocument.Name, data.UnderlyingDocument.Code));
        self.OtherUnderlying_u(data.OtherUnderlying);
        self.DocumentType_u(new DocumentTypeModel2(data.DocumentType.ID, data.DocumentType.Name));

        //self.Currency_u(data.Currency);
        self.Currency_u(new CurrencyModel2(data.Currency.ID, data.Currency.Code, data.Currency.Description));

        self.Amount_u(data.Amount);
        self.AttachmentNo_u(data.AttachmentNo);

        //self.StatementLetter_u(data.StatementLetter);
        self.StatementLetter_u(new StatementLetterModel(data.StatementLetter.ID, data.StatementLetter.Name));

        self.IsDeclarationOfException_u(data.IsDeclarationOfException);
        self.StartDate_u(self.LocalDate(data.StartDate, true, false));
        self.EndDate_u(self.LocalDate(data.EndDate, true, false));
        self.DateOfUnderlying_u(self.LocalDate(data.DateOfUnderlying, true, false));        
        self.ExpiredDate_u(self.LocalDate(data.ExpiredDate, true, false));
        self.ReferenceNumber_u(data.ReferenceNumber);
        self.SupplierName_u(data.SupplierName);
        self.InvoiceNumber_u(data.InvoiceNumber);
        self.IsProforma_u(data.IsProforma);
        self.IsUtilize_u(data.IsUtilize);
        self.IsSelectedProforma(data.IsSelectedProforma);
        self.LastModifiedBy_u(data.LastModifiedBy);
        self.LastModifiedDate_u(data.LastModifiedDate);
        //Call Grid Underlying for Check Proforma
        GetSelectedID();
        GetDataUnderlyingProforma();
    };

    //Add Attach File
    self.NewAttachFile = function () {
        // show the dialog task form
        $("#modal-form-Attach").modal('show');
        self.TempSelectedAttachUnderlying([]);
        self.ID_a(0);
        self.DocumentPurpose_a(new DocumentPurposeModel2('', '', ''));
        self.DocumentType_a(new DocumentTypeModel2('', ''));
        self.DocumentPath_a('');
        GetDataUnderlyingAttach();
    }

    //insert new
    self.NewDataUnderlying = function () {
        $("#modal-form-Underlying").modal('show');
        // flag as new Data
        self.IsNewDataUnderlying(true);

        // bind empty data
        self.ID_u(0);
        self.UnderlyingDocument_u(new UnderlyingDocumentModel2('', '', ''));
        self.OtherUnderlying_u('');
        self.DocumentType_u(new DocumentTypeModel2('', ''));
       // self.StatementLetter_u().ID(viewModel.TransactionDealDetailModel().StatementLetter().ID);
        //self.Currency_u().ID(viewModel.TransactionDealDetailModel().Currency().ID);
        self.StatementLetter_u(new StatementLetterModel('',''));
        self.Currency_u(new CurrencyModel2('','',''));
        self.Amount_u(0);
        self.AttachmentNo_u();
        self.IsDeclarationOfException_u(false);
        self.StartDate_u('');
        self.EndDate_u('');
        self.DateOfUnderlying_u();
        self.ExpiredDate_u('');
        self.ReferenceNumber_u('');
        self.SupplierName_u('');
        self.InvoiceNumber_u('');
        self.IsProforma_u(false);
        self.IsSelectedProforma(false);
        self.IsUtilize_u(false);
        //Call Grid Underlying for Check Proforma
        GetDataUnderlyingProforma();
        self.TempSelectedUnderlyingProforma([]);
        setStatementA();
        setRefID();
    };

    // Function to Read parameter Underlying
    function GetUnderlyingParameters(data) {

        var underlying = data['UnderltyingDoc'];
        for (var i = 0; i < underlying.length; i++) {
            self.ddlUnderlyingDocument_u.push(new UnderlyingDocumentModel2(underlying[i].ID, underlying[i].Name, underlying[i].Code));
        }

        self.ddlDocumentType_u(data['DocType']);
        self.ddlDocumentType_a(data['DocType']);
        self.ddlCurrency_u(data['Currency']);
        self.ddlDocumentPurpose_a(data['PurposeDoc']);
        ko.utils.arrayForEach(data['StatementLetter'],function(item){
            if(item.ID==1 || item.ID==2){
                self.ddlStatementLetter_u.push(item) ;
            }
        });

            //self.ddlStatementLetter_u(data['StatementLetter']);

        self.ddlRateType_u(data['RateType']);
    }

    //Function to Read All Customers Underlying
    function GetDataUnderlying() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Underlying",
            params: {
                page: self.UnderlyingGridProperties().Page(),
                size: self.UnderlyingGridProperties().Size(),
                sort_column: self.UnderlyingGridProperties().SortColumn(),
                sort_order: self.UnderlyingGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        self.UnderlyingFilterIsAvailable(true);
        self.UnderlyingFilterIsExpiredDate(true);
        var filters = GetUnderlyingFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        }
    }

    //Function to Read All Customers Underlying File
    function GetDataAttachFile() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlyingfile,
            params: {
                page: self.AttachGridProperties().Page(),
                size: self.AttachGridProperties().Size(),
                sort_column: self.AttachGridProperties().SortColumn(),
                sort_order: self.AttachGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        }
    }

    //Function to Read All Customers Underlying for Attach File
    function GetDataUnderlyingAttach() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Attach",
            params: {
                page: self.UnderlyingAttachGridProperties().Page(),
                size: self.UnderlyingAttachGridProperties().Size(),
                sort_column: self.UnderlyingAttachGridProperties().SortColumn(),
                sort_order: self.UnderlyingAttachGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetUnderlyingAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingDataAttachFile, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingDataAttachFile, OnError, OnAlways);
        }
    }

    //Function to Read All Customers Underlying Proforma
    function GetDataUnderlyingProforma() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Proforma",
            params: {
                page: self.UnderlyingProformaGridProperties().Page(),
                size: self.UnderlyingProformaGridProperties().Size(),
                sort_column: self.UnderlyingProformaGridProperties().SortColumn(),
                sort_order: self.UnderlyingProformaGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetUnderlyingProformaFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingProforma, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingProforma, OnError, OnAlways);
        }
    }
    function GetTotalAmount(CIFData,AmountUSD){
        var totalValue1=   $.ajax({
            type: "GET",
            url: api.server + api.url.transactiondeal + "/GetAmountTransaction=" + CIFData + "?isStatementB=false",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }
        });
        var totalValue2=   $.ajax({
            type: "GET",
            url: api.server + api.url.transactiondeal + "/GetAmountTransaction=" + CIFData + "?isStatementB=true",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }
        });
        $.when(totalValue1,totalValue2).done(function(data1,data2) {
            var t_TotalAmounts = 0;
            if (data1!=null || data1 != undefined) {
                var t_TotalAmounts1 = parseFloat(data1);
                var t_TotalAmounts2 = parseFloat(data2);

                    self.AmountModel().TotalAmounts(t_TotalAmounts2);
                    self.AmountModel().TotalAmountsStatementB(t_TotalAmounts1);
                    viewModel.Calculating(AmountUSD);
            }
            else
            {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        }) ;
    }

    function GetSelectedUtilizeID() {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });

        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SelectedUtilizeID",
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON(filters),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlying([]);
                    for (var i = 0; i < data.length; i++) {
                        self.TempSelectedUnderlying.push(new SelectUtillizeModel(data[i].ID, true, 0));
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetSelectedID() {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.ProformaFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.ProformaFilterIsSelected() });
        if (self.ProformaFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.ProformaFilterUnderlyingDocument() });
        if (self.ProformaFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.ProformaFilterDocumentType() });


        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SelectedProformaID",
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON(filters),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlyingProforma([]);
                    for (var i = 0; i < data.length; i++) {
                        self.TempSelectedUnderlyingProforma.push({ ID: data[i].ID });
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    // Get filtered columns value
    function GetUnderlyingFilteredColumns() {
        // define filter

        var filters = [];
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingFilterIsSelected() });
        if (self.UnderlyingFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingFilterUnderlyingDocument() });
        if (self.UnderlyingFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingFilterDocumentType() });
        if (self.UnderlyingFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingFilterCurrency() });
        if (self.UnderlyingFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingFilterStatementLetter() });
        if (self.UnderlyingFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingFilterAmount() });
        if (self.UnderlyingFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingFilterDateOfUnderlying() });
        if (self.UnderlyingFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingFilterExpiredDate() });
        if (self.UnderlyingFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingFilterReferenceNumber() });
        if (self.UnderlyingFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingFilterSupplierName() });
        if (self.UnderlyingFilterIsAvailable() != "") filters.push({ Field: 'IsAvailable', Value: self.UnderlyingFilterIsAvailable() });
        if (self.UnderlyingFilterIsExpiredDate() != "") filters.push({ Field: 'IsExpiredDate', Value: self.UnderlyingFilterIsExpiredDate() });

        return filters;
    };

    // Get filtered columns value
    function GetUnderlyingAttachFilteredColumns() {
        var filters = [];
        self.UnderlyingFilterIsSelected(true); // flag for get all datas
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingAttachFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingAttachFilterIsSelected() });
        if (self.UnderlyingAttachFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingAttachFilterUnderlyingDocument() });
        if (self.UnderlyingAttachFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingFilterDocumentType() });
        if (self.UnderlyingAttachFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingFilterCurrency() });
        if (self.UnderlyingAttachFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingFilterStatementLetter() });
        if (self.UnderlyingAttachFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingFilterAmount() });
        if (self.UnderlyingAttachFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingFilterDateOfUnderlying() });
        if (self.UnderlyingAttachFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingFilterExpiredDate() });
        if (self.UnderlyingAttachFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingFilterReferenceNumber() });
        if (self.UnderlyingAttachFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingFilterSupplierName() });

        return filters;
    }

    // Get filtered columns value
    function GetAttachFilteredColumns() {
        // define filter

        var filters = [];
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.AttachFilterFileName() != "") filters.push({ Field: 'FileName', Value: self.AttachFilterFileName() });
        if (self.AttachFilterDocumentPurpose() != "") filters.push({ Field: 'DocumentPurpose', Value: self.AttachFilterDocumentPurpose() });
        if (self.AttachFilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.AttachFilterModifiedDate() });
        if (self.AttachFilterDocumentRefNumber() != "") filters.push({ Field: 'DocumentRefNumber', Value: self.AttachFilterDocumentRefNumber() });
        if (self.AttachFilterAttachmentReference() !="") filters.push({ Field: 'AttachmentReference', Value: self.AttachFilterAttachmentReference()});

        return filters;
    };

    // Get filtered columns value
    function GetUnderlyingProformaFilteredColumns() {
        // define filter
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.ProformaFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.ProformaFilterIsSelected() });
        if (self.ProformaFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.ProformaFilterUnderlyingDocument() });
        if (self.ProformaFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.ProformaFilterDocumentType() });
        if (self.ProformaFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.ProformaFilterCurrency() });
        if (self.ProformaFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.ProformaFilterStatementLetter() });
        if (self.ProformaFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.ProformaFilterAmount() });
        if (self.ProformaFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.ProformaFilterDateOfUnderlying() });
        if (self.ProformaFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.ProformaFilterExpiredDate() });
        if (self.ProformaFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.ProformaFilterReferenceNumber() });
        if (self.ProformaFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.ProformaFilterSupplierName() });

        return filters;
    };

    // On success GetData callback
    function OnSuccessGetDataUnderlying(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerUnderlyings(data.Rows);

            self.UnderlyingGridProperties().Page(data['Page']);
            self.UnderlyingGridProperties().Size(data['Size']);
            self.UnderlyingGridProperties().Total(data['Total']);
            self.UnderlyingGridProperties().TotalPages(Math.ceil(self.UnderlyingGridProperties().Total() / self.UnderlyingGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetUnderlyingDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {

            for (var i = 0 ; i < data.Rows.length; i++) {
                var selected = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (item) {
                    return item.ID == data.Rows[i].ID;
                });
                if (selected != null) {
                    data.Rows[i].IsSelectedAttach = true;
                }
                else {
                    data.Rows[i].IsSelectedAttach = false;
                }
            }
            self.CustomerAttachUnderlyings(data.Rows);

            self.UnderlyingAttachGridProperties().Page(data['Page']);
            self.UnderlyingAttachGridProperties().Size(data['Size']);
            self.UnderlyingAttachGridProperties().Total(data['Total']);
            self.UnderlyingAttachGridProperties().TotalPages(Math.ceil(self.UnderlyingAttachGridProperties().Total() / self.UnderlyingAttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerUnderlyingFiles(data.Rows);

            self.AttachGridProperties().Page(data['Page']);
            self.AttachGridProperties().Size(data['Size']);
            self.AttachGridProperties().Total(data['Total']);
            self.AttachGridProperties().TotalPages(Math.ceil(self.AttachGridProperties().Total() / self.AttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetUnderlyingProforma(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            SetSelectedProforma(data.Rows);

            self.CustomerUnderlyingProformas(data.Rows);

            self.UnderlyingProformaGridProperties().Page(data['Page']);
            self.UnderlyingProformaGridProperties().Size(data['Size']);
            self.UnderlyingProformaGridProperties().Total(data['Total']);
            self.UnderlyingProformaGridProperties().TotalPages(Math.ceil(self.UnderlyingProformaGridProperties().Total() / self.UnderlyingProformaGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
    //--------------------------- set Underlying Function & variable end
};

// Knockout View Model
var viewModel = new ViewModel();
function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

$.holdReady(true);
var intervalRoleName = setInterval(function () {
    if (Object.keys(Const_RoleName).length != 0) {
        $.holdReady(false);
        clearInterval(intervalRoleName);
    }
}, 100);

// jQuery Init
//$(document).on("RoleNameReady", function() {
$(document).ready(function () {
    //$('#modal-form-Underlying')

    // show book transaction, hide new transaction

    // ace file upload
    $('#document-path').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        thumbnail: false, //| true | large
        blacklist: 'exe|dll'
    });

    // Knockout custom file handler
    ko.bindingHandlers.file = {
        init: function (element, valueAccessor) {
            $(element).change(function () {
                var file = this.files[0];

                if (ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };


    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        focusCleanup: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });

    $('#frmUnderlying').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        focusCleanup: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });

    $('#aspnetForm').after('<form id="frmUnderlying"></form>');
    $("#modal-form-Underlying").appendTo("#frmUnderlying");
    $("#modal-form-Attach").appendTo("#frmUnderlying");
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    var element = $("#book-customer-name");
    GetCustomerAutocompleted(element);

    // datepicker
    $('.date-picker').datepicker({
        autoclose: true,
        onSelect: function () {
            this.focus();
        }
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });


    // Knockout Datepicker custom binding handler
    ko.bindingHandlers.datepicker = {
        init: function (element) {
            $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).datepicker("refresh");
        }
    };

    // Dependant Observable for dropdown auto fill
    ko.dependentObservable(function () {


        var currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().Currency(); });
        var account = ko.utils.arrayFirst(viewModel.TransactionDealDetailModel().Customer().Accounts, function (item) { return item.AccountNumber == viewModel.Selected().Account(); });
        var docPurpose = ko.utils.arrayFirst(viewModel.Parameter().DocumentPurposes(), function (item) { return item.ID == viewModel.Selected().DocumentPurpose(); });
        var rateType = ko.utils.arrayFirst(viewModel.Parameter().RateType(), function (item) { return item.ID == viewModel.Selected().RateType(); });
        var statementLetter = ko.utils.arrayFirst(viewModel.Parameter().StatementLetter(), function (item) { return item.ID == viewModel.Selected().StatementLetter(); });
        var productType = ko.utils.arrayFirst(viewModel.Parameter().ProductType(), function (item) { return item.ID == viewModel.Selected().ProductType(); });
        var docType = ko.utils.arrayFirst(viewModel.Parameter().DocumentTypes(), function (item) { return item.ID == viewModel.Selected().DocumentType(); });


        if(viewModel.DocumentPurpose_a() != null)
        {
            if(viewModel.DocumentPurpose_a().ID() == 2)
            {
                viewModel.SelectingUnderlying(true);
            }
            else
            {
                viewModel.SelectingUnderlying(false);
            }
        }

        if (productType != null) {
            viewModel.TransactionDealDetailModel().ProductType(productType);
        }
        // if currency is selected
        if (currency != null) {
            viewModel.TransactionDealDetailModel().Currency(currency);
            GetRateAmount(currency.ID);
            //viewModel.GetAvailableStatementB(cifData);

            // set value of transaction 'IS FX TRANSACTION'
            var t_IsFxTransaction = (viewModel.TransactionDealDetailModel().Currency().Code != 'IDR' && viewModel.TransactionDealDetailModel().Account().Currency.Code == 'IDR') ? true : false;
            var t_IsFxTransactionToIDR = (viewModel.TransactionDealDetailModel().Currency().Code == 'IDR' && viewModel.TransactionDealDetailModel().Account().Currency.Code != 'IDR') ? true : false;
            var t_IsFxTransactionToIDRB = (viewModel.TransactionDealDetailModel().Currency().Code == 'IDR' && viewModel.TransactionDealDetailModel().Account().Currency.Code != 'IDR' && viewModel.TransactionDealDetailModel().ProductType().IsFlowValas==1) ? true : false;

            viewModel.t_IsFxTransaction(t_IsFxTransaction);
            viewModel.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);
            viewModel.t_IsFxTransactionToIDRB(t_IsFxTransactionToIDRB);

        }

        if (account != null) {
            viewModel.TransactionDealDetailModel().Account(account);
            // set value of transaction 'IS FX TRANSACTION'
            var t_IsFxTransaction = (viewModel.TransactionDealDetailModel().Currency().Code != 'IDR' && viewModel.TransactionDealDetailModel().Account().Currency.Code == 'IDR') ? true : false;
            var t_IsFxTransactionToIDR = (viewModel.TransactionDealDetailModel().Currency().Code == 'IDR' && viewModel.TransactionDealDetailModel().Account().Currency.Code != 'IDR') ? true : false;
            var t_IsFxTransactionToIDRB = (viewModel.TransactionDealDetailModel().Currency().Code == 'IDR' && viewModel.TransactionDealDetailModel().Account().Currency.Code != 'IDR' && viewModel.TransactionDealDetailModel().ProductType().IsFlowValas==1) ? true : false;

            viewModel.t_IsFxTransaction(t_IsFxTransaction);
            viewModel.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);
            viewModel.t_IsFxTransactionToIDRB(t_IsFxTransactionToIDRB);

            calculate();
        }
        if (docType != null) viewModel.DocumentType(docType);
        if (docPurpose != null) viewModel.DocumentPurpose(docPurpose);
        if (rateType != null) viewModel.TransactionDealDetailModel().RateType(rateType);
        if (statementLetter != null) {
            viewModel.TransactionDealDetailModel().StatementLetter(statementLetter);

            if(viewModel.TransactionDealDetailModel().StatementLetter() != undefined ) {
                if (viewModel.TransactionDealDetailModel().StatementLetter().ID  == 1  ) {
                    viewModel.TransactionDealDetailModel().bookunderlyingcode(36);
                }
                else
                {
                    viewModel.TransactionDealDetailModel().bookunderlyingcode('');
                }
            }

            calculate();
        }

    });

    // Token Validation
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(OnSuccessToken, OnError);
    } else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);

            viewModel.GetApplicationID();
            viewModel.SPUser(ko.mapping.toJS(spUser));

            //if(spUser)
            for(i=0;i<spUser.Roles.length;i++)
            {
                for(j=0;j<api.url.dealbizunitgroup.length;j++)
                {
                    if (Const_RoleName[spUser.Roles[i].ID] == api.url.dealbizunitgroup[j]) //if(spUser.Roles[i].Name == api.url.dealbizunitgroup[j])
                    {
                        viewModel.isDealBUGroup(true);
                    }
                }
            }
        }
        // call get data inside view model
        DealModel.token = accessToken;
        GetDataDeal(DealModel,OnSuccessGetTotal,OnErrorDeal);
        GetParameters();
    }

    // Knockout Bindings
    ko.applyBindings(viewModel);

    //================================= Eqv Calculation ====================================

    var x = document.getElementById("book-amount");
    var y = document.getElementById("book-rate");
    var z = document.getElementById("book-currency");
    var d = document.getElementById("book-eqv-usd");
    var xstored = x.getAttribute("data-in");
    var ystored = y.getAttribute("data-in");

    setInterval(function () {
        if (x == document.activeElement) {
            var temp = x.value;
            if (xstored != temp) {
                xstored = temp;
                x.setAttribute("data-in", temp);
                calculate();
            }
        }
        if (y == document.activeElement) {
            var temp = y.value;
            if (ystored != temp) {
                ystored = temp;
                y.setAttribute("data-in", temp);
                calculate();
            }
        }
        /* dodit@2014.11.08:add posibility calculate if currency changed */
        if (z == document.activeElement) { // add by dodit 2014/11/8
            calculate();
        }
    }, 100);

    function calculate() {
        /* dodit@2014.11.08:altering calculate process to avoid NaN, avoid formula on USD, and formating feature */

        x.value = x.value.replace(',','').replace(',','').replace(',','').replace(',','');
        y.value = y.value.replace(',','').replace(',','').replace(',','').replace(',','');
        viewModel.TransactionDealDetailModel().Amount(x.value);
        viewModel.TransactionDealDetailModel().Rate(y.value);
        res = x.value;
        ret = y.value;

        x.value = formatNumber(x.value);
        y.value = formatNumber(y.value);

        currency = viewModel.Selected().Currency();

        if (currency != 1) { // if not USD
            res = res * ret / idrrate;
            res = Math.round(res * 100) / 100;
        }

        res = isNaN(res) ? 0 : res; //avoid NaN
        viewModel.CalculateFX(res);
    }

    x.onblur = calculate;
    calculate();

    $('#book-transaction').show();
    $('#new-transaction').hide();

    //  $('#book-underlying-currency').attr('data-rule-required', false);
    //	$('#book-underlying-amount').attr('data-rule-required', false);
    $('#book-underlying-code').attr('data-rule-required', false);

    //		$('#book-underlying-currency').rules('remove', 'required');
    //		$('#book-underlying-amount').rules('remove', 'required');
    $('#book-underlying-code').rules('remove', 'required');

});

function GetRateAmount(CurrencyID) {
    var options = {
        url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
        params: {
        },
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetRateAmount, OnError, OnAlways);
}

function OnSuccessGetRateAmount(data, textStatus, jqXHR) {

    if (jqXHR.status = 200) {
        // bind result to observable array
        viewModel.TransactionDealDetailModel().Rate(data.RupiahRate);
        viewModel.Rate(data.RupiahRate);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function GetCustomerAutocompleted(element) {
    // autocomplete
    element.autocomplete({
        source: function (request, response) {
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.customer + "/Search",
                data: {
                    query: request.term,
                    limit: 20
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
        },
        minLength: 2,
        select: function (event, ui) {
            // set customer data model
            if (ui.item.data != undefined || ui.item.data != null) {
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                viewModel.TransactionDealDetailModel().Customer(ko.mapping.toJS(ui.item.data, mapping));
                cifData = ui.item.data.CIF;
                DealModel.cif = cifData;
                customerNameData = ui.item.data.Name;
                GetTotalDeal(DealModel,OnSuccessTotal,OnErrorDeal);
                //viewModel.GetAvailableStatementB(cifData);
            }
            else
                viewModel.TransactionDealDetailModel().Customer(null);
        }
    });
}
function OnSuccessTotal(){
    //viewModel.GetTotalAmount(cifData,0);
    viewModel.Calculating(0);
}
function OnErrorDeal(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

function NewTransactioin() {

    var t_TreshHold = TotalDealModel.TreshHold;
    var t_IsFxTransaction = viewModel.t_IsFxTransaction();
    var t_IsFxTransactionToIDR = viewModel.t_IsFxTransactionToIDR();
    var t_IsFxTransactionToIDRB = viewModel.t_IsFxTransactionToIDRB();
    var t_TotalAmountsUSD = parseFloat(viewModel.AmountModel().TotalAmountsUSD());
    var t_StatementLeter = viewModel.Selected().StatementLetter();
    var t_AmountUSD = parseFloat(viewModel.TransactionDealDetailModel().AmountUSD());
    var form = $("#aspnetForm");
    form.validate();
    if (form.valid()) {
        if (t_IsFxTransaction||t_IsFxTransactionToIDR) {
            if ((t_StatementLeter == 1 && parseFloat(t_TotalAmountsUSD) <= t_TreshHold) || (t_StatementLeter == 2)||t_StatementLeter == 3) {
                if (t_IsFxTransactionToIDR) {
                    if (t_AmountUSD >= TotalDealModel.FCYIDRTrashHold && t_IsFxTransactionToIDRB) {
                        viewModel.Selected().StatementLetter(2);
                    } else {
                        viewModel.Selected().StatementLetter(3);
                    }

                    viewModel.TransactionDealDetailModel().utilizationAmount(0.00);
                    viewModel.TempSelectedUnderlying([]);
                    viewModel.GetDataUnderlying();
                    viewModel.GetDataAttachFile();
                    viewModel.IsDeal(false);
                    $('#book-transaction').hide();
                    $('#new-transaction').show();
                    viewModel.IsEditable(true);
                    $('#s4-workspace').animate({scrollTop:0}, 'fast');
                }
                else {
                    if (!(viewModel.TransactionDealDetailModel().Currency().Code != 'IDR' && t_StatementLeter == 3)) {
                        viewModel.TransactionDealDetailModel().utilizationAmount(0.00);
                        viewModel.TempSelectedUnderlying([]);
                        viewModel.GetDataUnderlying();
                        viewModel.GetDataAttachFile();
                        viewModel.IsDeal(false);
                        $('#book-transaction').hide();
                        $('#new-transaction').show();
                        viewModel.IsEditable(true);
                        $('#s4-workspace').animate({scrollTop:0}, 'fast');
                    }else{
                        viewModel.IsEditable(true);
                        ShowNotification("Form Validation Warning", "Please Select A or B Statement Letter.", 'gritter-warning', false);
                    }
                }
            } else {
                viewModel.IsEditable(true);
                ShowNotification("Form Validation Warning", "Total Amount Statement A hit " + t_TreshHold + ", Please Select Statement B  ", 'gritter-warning', false);
            }
        } else {
            viewModel.IsEditable(true);
            ShowNotification("Form Validation Warning", "Transaction FCY->FCY, please select IDR->FCY or FCY->IDR transaction", 'gritter-warning', false);
        }
    } else{
    ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
    $('#book-underlying-code').attr('data-rule-required', false);
    $('#book-underlying-code').rules('remove', 'required');
}
}

function BookTransactioin() {
    // hide book transaction, show new transaction

    viewModel.Documents([]);

    $('#book-transaction').show();
    $('#new-transaction').hide();

    $('#s4-workspace').animate({scrollTop:0}, 'fast');
}

// reset model
function ResetModel() {
    viewModel = new ViewModel();
}

// get all parameters need
function GetParameters() {
    var options = {
        url: api.server + api.url.parameter,
        params: {
            select: "Currency,AgentCharge,FXCompliance,ChargesType,DocType,PurposeDoc,statementletter,underlyingdoc,customer,ratetype,producttype"
        },
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
}

function SaveTransaction() {
    // save transaction  fx

    viewModel.TransactionDealDetailModel().IsBookDeal(viewModel.IsDeal());

    var t_TreshHold = parseFloat(TotalDealModel.TreshHold);
    var t_IsFxTransaction = viewModel.t_IsFxTransaction();
    var t_IsFxTransactionToIDR = viewModel.t_IsFxTransactionToIDR();
    var t_TotalAmountsUSD = parseFloat(viewModel.AmountModel().TotalAmountsUSD());
    var t_StatementLeter = viewModel.TransactionDealDetailModel().StatementLetter().ID;
    var t_AmountUSD = parseFloat(viewModel.TransactionDealDetailModel().AmountUSD());
    var t_UtilizeAmount = parseFloat(viewModel.TransactionDealDetailModel().utilizationAmount());

    if (t_IsFxTransaction)
    {
        viewModel.TransactionDealDetailModel().IsFxTransaction(true);
    }
    else
    {
        viewModel.TransactionDealDetailModel().IsFxTransaction(false);
    }

    if (viewModel.IsDeal() == false) {

        if ((t_IsFxTransaction)||(viewModel.TransactionDealDetailModel().ProductType().IsFlowValas ==true && t_AmountUSD >= TotalDealModel.FCYIDRTrashHold)) {

            if (t_AmountUSD <= t_UtilizeAmount) {

                if (t_TotalAmountsUSD >= t_TreshHold && viewModel.TempSelectedUnderlying()[0].StatementLetter() == 1) {
                    viewModel.IsUploaded(true);
                    ShowNotification("Form Underlying Warning", "Total Transaction greater than " + t_TreshHold + " (USD), Please add statement B underlying", 'gritter-warning', false);
                    return;
                } else {
                    viewModel.TransactionDealDetailModel().underlyings([]);
                    ko.utils.arrayForEach(viewModel.TempSelectedUnderlying(), function (item) {
                        if (item.Enable() == false)
                            viewModel.TransactionDealDetailModel().underlyings.push(new SelectUtillizeModel(item.ID(), false, item.USDAmount(), ''));
                    });

                    // save transaction  fx
                    var options = {
                        url: api.server + api.url.transactiondeal,
                        token: accessToken,
                        data: ko.toJSON(viewModel.TransactionDealDetailModel())
                    };

                    //var obj = ko.toJSON(viewModel.TransactionDealDetailModel());
                    //console.log(obj);

                    if (viewModel.TransactionDealDetailModel().ID() == null) {
                        // if id is null = Save
                        Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
                    }
                }
            } else {
                viewModel.IsEditable(true);
                viewModel.IsUploaded(true);
                ShowNotification("Form Underlying Warning", "The Utilization Amount must be equal greater than Transaction Amount", 'gritter-warning', false);
                return;
            }

        } else {
            // save transactio non fx

            if (viewModel.TransactionDealDetailModel().Documents().length == viewModel.Documents().length) {
                var options = {
                    url: api.server + api.url.transactiondeal,
                    token: accessToken,
                    data: ko.toJSON(viewModel.TransactionDealDetailModel())
                };
                var obj = ko.toJSON(viewModel.TransactionDealDetailModel());
                //console.log(obj);

                if (viewModel.TransactionDealDetailModel().ID() == null) {
                    // if id is null = Save
                    Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
                }
            }
        }
    }
    else {
        // save transaction  fx

        var options = {
            url: api.server + api.url.transactiondeal,
            token: accessToken,
            data: ko.toJSON(viewModel.TransactionDealDetailModel())
        };

        var obj = ko.toJSON(viewModel.TransactionDealDetailModel());
        //console.log(obj);

        if (viewModel.TransactionDealDetailModel().ID() == null) {
            // if id is null = Save
            Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
        }
    }
}

function AddListItem() {
    var body = {
        Title: viewModel.TransactionDealDetailModel().ApplicationID(),
        __metadata: {
            type: config.sharepoint.metadata.listDeal
        }
    };

    //console.log('body: ' + body);

    var options = {
        url: config.sharepoint.url.api + "/Lists(guid'" + config.sharepoint.listIdDeal + "')/Items",
        data: JSON.stringify(body),
        digest: jQuery("#__REQUESTDIGEST").val()
    };

    //console.log('option: ' + options);

    Helper.Sharepoint.List.Add(options, OnSuccessAddListItem, OnError, OnAlways);
}

function ValidateTransaction() {
    UploadDocuments();
}

function UploadDocuments() {
    // uploading documents. after upload completed, see SaveTransaction()
    var data = {
        ApplicationID: viewModel.TransactionDealDetailModel().ApplicationID(),
        CIF: viewModel.TransactionDealDetailModel().Customer().CIF,
        Name: viewModel.TransactionDealDetailModel().Customer().Name
    };

    if (viewModel.Documents().length > 0 && !(self.TransactionDealDetailModel.Currency().Code != 'IDR' && self.TransactionDealDetailModel.Account().Currency.Code == 'IDR')) {
        for (var i = 0; i < viewModel.Documents().length; i++) {
            UploadFile(data, viewModel.Documents()[i], SaveTransaction);
        }
    }
    else if(viewModel.IsFXTransactionAttach() && !(viewModel.IsUploaded()))
    {
        for (var i = 0; i < viewModel.Documents().length; i++) {
            UploadFile(data, viewModel.Documents()[i], SaveTransaction);
        }
    }
    else {
        SaveTransaction();
    }
}

// Event handlers declaration start
function OnSuccessUpload(data, textStatus, jqXHR) {
    ShowNotification("Upload Success", JSON.stringify(data), "gritter-success", true);
}

function OnSuccessGetParameters(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        // bind result to observable array
        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };

        viewModel.Parameter().Currencies(ko.mapping.toJS(data.Currency, mapping));
        viewModel.Parameter().ProductType(ko.mapping.toJS(data.ProductType, mapping));
        viewModel.Parameter().FXCompliances(ko.mapping.toJS(data.FXCompliance, mapping));
        viewModel.Parameter().DocumentTypes(ko.mapping.toJS(data.DocType, mapping));
        viewModel.Parameter().DocumentPurposes(ko.mapping.toJS(data.PurposeDoc, mapping));
        viewModel.Parameter().RateType(ko.mapping.toJS(data.RateType, mapping));
        viewModel.Parameter().StatementLetter(ko.mapping.toJS(data.StatementLetter, mapping));

        // underlying parameter
        viewModel.GetUnderlyingParameters(data);

        // enabling form input controls
        viewModel.IsEditable(true);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function OnSuccessUpdateUnderlying(data, textStatus, jqXHR) {
    if (jqXHR.status != 200) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-success', true);
    }
}

function OnSuccessAddListItem(data, textStatus, jqXHR) {
    ShowNotification("Book Success", JSON.stringify(data.Message), "gritter-success", true);
    viewModel.IsEditable(false);
    window.location = "/home";
}

function OnSuccessSaveAPI(data, textStatus, jqXHR) {
    // Get transaction id from api result

    if (data.ID != null || data.ID != undefined) {
        viewModel.TransactionDealDetailModel().ID(data.ID);
        viewModel.TransactionDealDetailModel().bookunderlyingamount("");

        if (viewModel.IsDeal() == false) {
            // insert to sharepoint list item
            AddListItem();
        }
        else {
            //viewModel.isEditable(false);
            ShowNotification("Book Success", JSON.stringify(data.Message), "gritter-success", true);
            window.location = "/home";
        }
    }
}

// Autocomplete
function OnSuccessAutoComplete(response, data, textStatus, jqXHR) {
    response($.map(data, function (item) {
            return {
                // default autocomplete object
                label: item.CIF + " - " + item.Name,
                value: item.Name,

                // custom object binding
                data: {
                    CIF: item.CIF,
                    Name: item.Name,
                    POAName: item.POAName,
                    Accounts: item.Accounts,
                    Branch: item.Branch,
                    Contacts: item.Contacts,
                    Functions: item.Functions,
                    RM: item.RM,
                    Type: item.Type,
                    Underlyings: item.Underlyings,
                    LastModifiedBy: item.LastModifiedBy,
                    LastModifiedDate: item.LastModifiedDate
                }
            }
        })
    );
}

// Token validation On Success function
function OnSuccessToken(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // read spuser from cookie
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);
        viewModel.SPUser(ko.mapping.toJS(spUser));
    }
    // get parameter values
    GetParameters();
}

// delete & upload document underlying start
function DeleteFile(documentPath) {
    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Delete the file to the SharePoint folder.
    var deleteFile = deleteFileToFolder();
    deleteFile.done(function (file, status, xhr) {
        output = file.d;
    });
    deleteFile.fail(OnError);
    // Call function delete File Shrepoint Folder
    function deleteFileToFolder() {
        return $.ajax({
            url: serverUrl + "/_api/web/GetFileByServerRelativeUrl('" + documentPath + "')",
            type: "POST",
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "X-HTTP-Method": "DELETE",
                "IF-MATCH": "*"
                //"content-length": arrayBuffer.byteLength
            }
        });
    }
}

function UploadFileUnderlying(context, document, callBack) {

    // Define the folder path for this example.
    var serverRelativeUrlToFolder = '/Underlying Documents';

    // Get the file name from the file input control on the page.
    var parts = document.DocumentPath().name.split('.');

    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBufferUnderlying();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolderUnderlying(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItemUnderlying(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                // Change the display name and title of the list item.
                var changeItem = updateListItemUnderlying(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    DocumentModels({ "FileName": document.DocumentPath().name, "DocumentPath": output.ServerRelativeUrl });
                    //viewModel.TransactionDealDetailModel().Documents.push(kodok);

                    //self.Documents.push(kodok);
                    callBack();
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBufferUnderlying() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }

        //reader.readAsArrayBuffer(fileInput[0].files[0]);
        reader.readAsArrayBuffer(document.DocumentPath());
        //reader.readAsDataURL(document.DocumentPath());
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolderUnderlying(arrayBuffer) {

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                //"content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItemUnderlying(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItemUnderlying(itemMetadata) {

        // Define the list item changes. Use the FileLeafRef property to change the display name.
        // For simplicity, also use the name as the title.
        // The example gets the list item type from the item's metadata, but you can also get it from the
        // ListItemEntityTypeFullName property of the list.
        var body = {
            Title: document.DocumentPath().name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: context.Type.DocTypeName,
            Document_x0020_Purpose: context.Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}

// Upload the file
function UploadFile(context, document, callBack) {

    var serverRelativeUrlToFolder = '/Instruction Documents';
    var parts = document.DocumentPath.name.split('.');
    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp + "." + fileExtension;

    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Get the local file as an array buffer.
    var getFile = getFileBuffer();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolder(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                // Change the display name and title of the list item.
                var changeItem = updateListItem(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {

                    //return output;
                    var kodok = {
                        ID: 0,
                        Type: document.Type,
                        Purpose: document.Purpose,
                        LastModifiedDate: null,
                        LastModifiedBy: null,
                        DocumentPath: output.ServerRelativeUrl,
                        FileName: document.DocumentPath.name
                    };
                    viewModel.TransactionDealDetailModel().Documents.push(kodok);

                    callBack();
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBuffer() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }

        reader.readAsArrayBuffer(document.DocumentPath);
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(arrayBuffer) {

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItem(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItem(itemMetadata) {
        var body = {
            Title: document.DocumentPath.name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: document.Type.Name,
            Document_x0020_Purpose: document.Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}

// AJAX On Error Callback
function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}
function OnSuccessGetTotal(data) {
    //viewModel.TransactionDealDetailModel().Rate(data);
    //viewModel.Rate(data);
    idrrate = data;

}
// AJAX On Alway Callback
function OnAlways() {
    // enabling form input controls
    // viewModel.IsEditable(true);
}
