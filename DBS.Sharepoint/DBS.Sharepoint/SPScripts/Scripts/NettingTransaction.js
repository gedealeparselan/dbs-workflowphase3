﻿var accessToken;
var $box;
var $remove = false;
var cifData = '0';
var customerNameData = '';
var DealModel = { cif: cifData, token: accessToken };
var formatNumber = function (num) {
    if (num == null) {
        return;
    }
    var n = num.toString(), p = n.indexOf('.');
    return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
        return p < 0 || i < p ? ($0 + ',') : $0;
    });
}

var countResult = {
    CustomerName: 0,
    DealNumber: 0,
    NettingDealNumber: 0
};

var today = Date.now();

var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};

var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};
var CustomerModel = {
    CIF: ko.observable(),
    Name: ko.observable(),
    POAName: ko.observable(),
    Branch: ko.observable(),
    Type: ko.observable(),
    RM: ko.observable(),
    Contacts: ko.observableArray(),
    Functions: ko.observableArray(),
    Accounts: ko.observableArray(),
    Underlyings: ko.observableArray()
};

var SelectUtillizeModel = function (id, enable, uSDAmount, statementLetter) {
    var self = this;
    self.ID = ko.observable(id);
    self.Enable = ko.observable(enable);
    self.USDAmount = ko.observable(uSDAmount);
    self.StatementLetter = ko.observable(statementLetter);
};

var ProductModel = {
    ID: ko.observable(2020),
    Code: ko.observable('NT'),
    Name: ko.observable('TMO Netting'),
    WorkflowID: ko.observable(),
    Workflow: ko.observable()
};

var ProductTypeModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    IsFlowValas: ko.observable(),
    Description: ko.observable()
};

var SelectedModel = {
    Currency: ko.observable(),
    Account: ko.observable(),
    DocumentType: ko.observable(),
    DocumentPurpose: ko.observable(),
    NewCustomer: ko.observable(),
    Currency: ko.observable(),
    ProductType: ko.observable(),
    StatementLetter: ko.observable(),
    DebitCurrency: ko.observable(),
    NettingPurpose: ko.observable()
};

var CurrencyModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var CurrencyModel2 = function (id, code, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Description = ko.observable(description);
}

var UnderlyingDocumentModel2 = function (id, name, code) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Name = ko.observable(name);
    self.CodeName = ko.observable(code + ' (' + name + ')');
}

var DocumentTypeModel2 = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}

var StatementLetterModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}

var DocumentPurposeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var NettingPurposeModel = {
    ID: ko.observable(),
    name: ko.observable()
};

var UnderlyingDocumentModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var DocumentTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var StatementModel = {
    ID: ko.observable(0),
    Name: ko.observable("")
};

var ParameterModel = {
    ProductType: ko.observableArray([ProductTypeModel]),
    DocumentTypes: ko.observableArray([DocumentTypeModel]),
    DocumentPurposes: ko.observableArray([DocumentPurposeModel]),
    NettingPurpose: ko.observableArray([NettingPurposeModel]),
    StatementLetter: ko.observableArray([StatementModel]),
    UnderlyingDocs: ko.observableArray([UnderlyingDocumentModel]),
    Currencies: ko.observableArray([CurrencyModel])
};

var TransactionNettingModel = {
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Product: ko.observable(ProductModel),
    Customer: ko.observable(CustomerModel),
    TZReference: ko.observable(),
    SwapNumber: ko.observable(),
    NettingPurpose: ko.observable(NettingPurposeModel),
    NettingDealNumber: ko.observableArray([]),
    ExpectedDateStatementLetter: ko.observable(),
    ExpectedDateUnderlying: ko.observable(),
    ActualDateStatementLetter: ko.observable(),
    ActualDateUnderlying: ko.observable(),
    Remarks: ko.observable(),
    UtilizationAmount: ko.observable(),
    Underlyings: ko.observableArray([]),
    Documents: ko.observableArray([]),
    IsDraft: ko.observable(false),
    IsTMO: ko.observable(true),
    IsNettingTransaction: ko.observable(true),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
};

var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);
        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }
        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);
            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);
            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);
        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());
        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);
        self.Callback();
    };

    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";
        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }
        return sort;
    };

    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }
        self.SortColumn(column);
        self.Page(1);
        self.Callback();
    };
};

var ViewModel = function () {
    var self = this;
    self.IsRole = function (name) {
        var result = false;
        $.each($.cookie(api.cookie.spUser).Roles, function (index, item) {
            if (Const_RoleName[item.ID] == name) result = true; //if (item.Name == name) result = true;
        });
        return result;
    };
    self.SPUser = ko.observable(SPUser);
    self.isDealBUGroup = ko.observable(false);
    self.TransactionNetting = ko.observable(TransactionNettingModel);
    self.Parameter = ko.observable(ParameterModel);
    self.IsEditable = ko.observable(true);
    self.Selected = ko.observable(SelectedModel);
    self.CIFData = ko.observable();
    self.IsEnableSubmit = ko.observable(true);
    self.IsCustomerAvailable = ko.observable(false);
    self.CustomerUnderlyings = ko.observableArray([]);
    self.DFNumber = ko.observable();
    self.SwapDF = ko.observable();
    self.IndexUpdate = ko.observable();
    self.ID = ko.observable(0);
    self.ApplicationIDColl = ko.observableArray([]);
    self.FinishBulk = function () {
        window.location = "/home";
    }
    self.IsEditNettingDealNumber = ko.observable(false);
    self.IsNewNettingDealNumber = ko.observable(false);
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        return Helper.LocalDate(date, isDateOnly, isDateLong);
    };
    self.CIFJointAccounts = ko.observableArray([{ ID: false, Name: "Single" }, { ID: true, Name: "Join" }]);
    self.IsEditTableUnderlying = ko.observable(true);
    self.UnderlyingGridProperties = ko.observable();
    self.UnderlyingGridProperties(new GridPropertiesModel(GetDataUnderlying));
    self.TempSelectedUnderlying = ko.observableArray([]);
    self.CIF_u = ko.observable(cifData);
    self.UnderlyingFilterCIF = ko.observable("");
    self.UnderlyingFilterParentID = ko.observable("");
    self.UnderlyingFilterIsSelected = ko.observable(false);
    self.UnderlyingFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingFilterDocumentType = ko.observable("");
    self.UnderlyingFilterCurrency = ko.observable("");
    self.UnderlyingFilterStatementLetter = ko.observable("");
    self.UnderlyingFilterAmount = ko.observable("");
    self.UnderlyingFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingFilterExpiredDate = ko.observable("");
    self.UnderlyingFilterReferenceNumber = ko.observable("");
    self.UnderlyingFilterSupplierName = ko.observable("");
    self.UnderlyingFilterIsProforma = ko.observable("");
    self.UnderlyingFilterIsAvailable = ko.observable("");
    self.UnderlyingFilterIsExpiredDate = ko.observable("");
    self.UnderlyingFilterIsSelectedBulk = ko.observable(false);
    self.UnderlyingFilterAvailableAmount = ko.observable("");
    self.UnderlyingFilterAttachmentNo = ko.observable("");
    self.UnderlyingFilterAccountNumber = ko.observable("");
    self.UnderlyingFilterShow = ko.observable(1);
    self.NettingAllDocumentsTemp = ko.observableArray([]);
    self.CustomerDocuments = ko.observableArray([]);
    self.IsUploading = ko.observable(false);
    self.SelectingUnderlying = ko.observable(false);
    self.ID_a = ko.observable(0);
    // grid properties Underlying File
    self.UnderlyingAttachGridProperties = ko.observable();
    self.UnderlyingAttachGridProperties(new GridPropertiesModel(GetDataUnderlyingAttach));
    self.CustomerAttachUnderlyings = ko.observableArray([]);
    self.TempSelectedAttachUnderlying = ko.observableArray([]);
    // filter Underlying for Attach
    self.UnderlyingAttachFilterIsSelected = ko.observable("");
    self.UnderlyingAttachFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingAttachFilterDocumentType = ko.observable("");
    self.UnderlyingAttachFilterCurrency = ko.observable("");
    self.UnderlyingAttachFilterStatementLetter = ko.observable("");
    self.UnderlyingAttachFilterAmount = ko.observable("");
    self.UnderlyingAttachFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingAttachFilterExpiredDate = ko.observable("");
    self.UnderlyingAttachFilterReferenceNumber = ko.observable("");
    self.UnderlyingAttachFilterSupplierName = ko.observable("");
    self.UnderlyingAttachFilterAvailableAmount = ko.observable("");

    // set default sorting for Attach Underlying Grid
    self.UnderlyingAttachGridProperties().SortColumn("ID");
    self.UnderlyingAttachGridProperties().SortOrder("ASC");
    self.DocumentPath_a = ko.observable("");

    self.IsNewDataUnderlying = ko.observable(false);
    self.IsUnderlyingMode = ko.observable(true);
    self.ID_u = ko.observable("");
    self.CustomerName_u = ko.observable(customerNameData);
    self.UnderlyingDocument_u = ko.observable(new UnderlyingDocumentModel2('', '', ''));
    self.ddlUnderlyingDocument_u = ko.observableArray([]);
    self.DocumentType_u = ko.observable(new DocumentTypeModel2('', ''));
    self.ddlDocumentType_u = ko.observableArray([]);
    self.Currency_u = ko.observable(new CurrencyModel2('', '', ''));
    self.ddlCurrency_u = ko.observableArray([]);
    self.StatementLetter_u = ko.observable(new StatementLetterModel('', ''));
    self.ddlStatementLetter_u = ko.observableArray([]);
    self.DynamicStatementLetter_u = ko.observableArray([]);
    self.AvailableAmount_u = ko.observable(0);
    self.Rate_u = ko.observable(0);
    self.AmountUSD_u = ko.observable(0.00);
    self.Amount_u = ko.observable(0);
    self.AttachmentNo_u = ko.observable("");
    self.SelectUnderlyingDocument_u = ko.observable("");
    self.IsDeclarationOfException_u = ko.observable(false);
    self.StartDate_u = ko.observable();
    self.EndDate_u = ko.observable();
    self.DateOfUnderlying_u = ko.observable();
    self.ExpiredDate_u = ko.observable();
    self.ReferenceNumber_u = ko.observable("");
    self.SupplierName_u = ko.observable("");
    self.InvoiceNumber_u = ko.observable("");
    self.IsStatementA = ko.observable(true);
    self.IsProforma_u = ko.observable(false);
    self.IsSelectedProforma = ko.observable(false);
    self.IsUtilize_u = ko.observable(false);
    self.IsEnable_u = ko.observable(false);
    self.IsJointAccount_u = ko.observable(false);
    self.AccountNumber_u = ko.observable("");
    // add bulkUnderlying // add 2015.03.09
    self.IsBulkUnderlying_u = ko.observable(false);
    self.IsSelectedBulk = ko.observable(false);
    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyingProformas = ko.observableArray([]);
    self.UnderlyingProformaGridProperties = ko.observable();
    self.UnderlyingProformaGridProperties(new GridPropertiesModel(GetDataUnderlyingProforma));
    // filter Underlying for Attach
    self.ProformaFilterIsSelected = ko.observable(false);
    self.ProformaFilterUnderlyingDocument = ko.observable("");
    self.ProformaFilterDocumentType = ko.observable("Proforma");
    self.ProformaFilterCurrency = ko.observable("");
    self.ProformaFilterStatementLetter = ko.observable("");
    self.ProformaFilterAmount = ko.observable("");
    self.ProformaFilterDateOfUnderlying = ko.observable("");
    self.ProformaFilterExpiredDate = ko.observable("");
    self.ProformaFilterReferenceNumber = ko.observable("");
    self.ProformaFilterSupplierName = ko.observable("");
    self.ProformaFilterIsProforma = ko.observable(false);
    self.ProformaClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.ProformaFilterIsSelected(false);
        self.ProformaFilterUnderlyingDocument("");
        self.ProformaFilterDocumentType("Proforma");
        self.ProformaFilterCurrency("");
        self.ProformaFilterStatementLetter("");
        self.ProformaFilterAmount("");
        self.ProformaFilterDateOfUnderlying("");
        self.ProformaFilterExpiredDate("");
        self.ProformaFilterReferenceNumber("");
        self.ProformaFilterSupplierName("");
        self.ProformaFilterIsProforma("");

        GetDataUnderlyingProforma();
    };
    // grid properties Bulk // 2015.03.09
    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerBulkUnderlyings = ko.observableArray([]); // add 2015.03.09
    self.UnderlyingBulkGridProperties = ko.observable();
    self.UnderlyingBulkGridProperties(new GridPropertiesModel(GetDataBulkUnderlying));
    self.BulkClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.BulkFilterIsSelected(false);
        self.BulkFilterUnderlyingDocument("");
        self.BulkFilterDocumentType("");
        self.BulkFilterCurrency("");
        self.BulkFilterStatementLetter("");
        self.BulkFilterAmount("");
        self.BulkFilterDateOfUnderlying("");
        self.BulkFilterExpiredDate("");
        self.BulkFilterReferenceNumber("");
        self.BulkFilterInvoiceNumber("");
        self.BulkFilterSupplierName("");
        self.BulkFilterIsBulkUnderlying("");

        GetDataBulkUnderlying();
    };
    self.GetDataBulkUnderlying = function () {
        GetDataBulkUnderlying();
    }

    self.GetDataUnderlyingProforma = function () {
        GetDataUnderlyingProforma();
    }
    self.GetDataUnderlyingAttach = function () {
        GetDataUnderlyingAttach();
    }
    self.GetParameters = function () {
        var options = {
            url: api.server + api.url.parameter,
            params: {
                select: "DocType,PurposeDoc,statementletter,underlyingdoc,Currency"
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
    }

    function OnSuccessGetParameters(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            var mapping = {
                'ignore': ["LastModifiedDate", "LastModifiedBy"]
            };

            self.Parameter().DocumentTypes(ko.mapping.toJS(data.DocType, mapping));
            self.Parameter().DocumentPurposes(ko.mapping.toJS(data.PurposeDoc, mapping));
            self.Parameter().StatementLetter(ko.mapping.toJS(data.StatementLetter, mapping));
            self.Parameter().UnderlyingDocs(ko.mapping.toJS(data.UnderltyingDoc, mapping));
            self.Parameter().Currencies(ko.mapping.toJS(data.Currency, mapping));

        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }
    // Uploading document
    self.UploadDocumentUnderlying = function () {
        // show upload dialog
        // show the dialog task form
        $("#modal-form-Attach").modal('show');
        self.TempSelectedAttachUnderlying([]);
        self.ID_a(0);
        self.IsUploading(false);
        GetDataUnderlyingAttach();
        $('.remove').click();
    }
    function GetUnderlyingFilteredColumns() {
        // define filter

        var filters = [];
        self.CIF_u(cifData);

        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingFilterIsSelected() });
        if (self.UnderlyingFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingFilterUnderlyingDocument() });
        if (self.UnderlyingFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingFilterDocumentType() });
        if (self.UnderlyingFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingFilterCurrency() });
        if (self.UnderlyingFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingFilterStatementLetter() });
        if (self.UnderlyingFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingFilterAmount() });
        if (self.UnderlyingFilterAvailableAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingFilterAvailableAmount() });
        if (self.UnderlyingFilterAttachmentNo() != "") filters.push({ Field: 'AttachmentNo', Value: self.UnderlyingFilterAttachmentNo() });
        if (self.UnderlyingFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingFilterDateOfUnderlying() });
        if (self.UnderlyingFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingFilterExpiredDate() });
        if (self.UnderlyingFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingFilterReferenceNumber() });
        if (self.UnderlyingFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingFilterSupplierName() });
        if (self.UnderlyingFilterIsAvailable() != "") filters.push({ Field: 'IsAvailable', Value: self.UnderlyingFilterIsAvailable() });
        if (self.UnderlyingFilterIsExpiredDate() != "") filters.push({ Field: 'IsExpiredDate', Value: self.UnderlyingFilterIsExpiredDate() });
        if (self.UnderlyingFilterIsSelectedBulk() == false) filters.push({ Field: 'IsSelectedBulk', Value: self.UnderlyingFilterIsSelectedBulk() });
        if (self.UnderlyingFilterAccountNumber() != "") filters.push({ Field: 'AccountNumber', Value: self.UnderlyingFilterAccountNumber() });

        filters.push({ Field: 'StatusShowData', Value: self.UnderlyingFilterShow() });
        return filters;
    };
    function GetUnderlyingAttachFilteredColumns() {
        var filters = [];

        self.UnderlyingFilterIsSelected(true); // flag for get all datas
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingAttachFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingAttachFilterIsSelected() });
        if (self.UnderlyingAttachFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingAttachFilterUnderlyingDocument() });
        if (self.UnderlyingAttachFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingAttachFilterDocumentType() });
        if (self.UnderlyingAttachFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingAttachFilterCurrency() });
        if (self.UnderlyingAttachFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingAttachFilterStatementLetter() });
        if (self.UnderlyingAttachFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingAttachFilterAmount() });
        if (self.UnderlyingAttachFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingAttachFilterDateOfUnderlying() });
        if (self.UnderlyingAttachFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingAttachFilterExpiredDate() });
        if (self.UnderlyingAttachFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingAttachFilterReferenceNumber() });
        if (self.UnderlyingAttachFilterAvailableAmount() != "") filters.push({ Field: 'AvailableAmount', Value: self.UnderlyingAttachFilterAvailableAmount() });
        if (self.UnderlyingAttachFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingAttachFilterSupplierName() });

        return filters;
    }
    self.NewDataUnderlying = function () {
        $('.form-group').removeClass('has-error').addClass('has-info');
        $('.help-block').remove();

        $("#modal-form-Underlying").modal('show');

    };
    self.save_a = function () {

        self.IsUploading(true);
        var type = ko.utils.arrayFirst(self.Parameter().DocumentTypes(), function (item) {
            return item.ID == self.Selected().DocumentType();
        });
        var purpose = ko.utils.arrayFirst(self.Parameter().DocumentPurposes(), function (item) {
            return item.ID == self.Selected().DocumentPurpose();
        });
        var doc = {
            ID: 0,
            FileName: self.DocumentPath_a() != null ? self.DocumentPath_a().name : null,
            DocumentPath: self.DocumentPath_a(),
            Type: type,
            Purpose: purpose,
            IsNewDocument: true,
            LastModifiedDate: new Date(),
            LastModifiedBy: null
        };
        if (doc.Type == null || doc.Purpose == null || doc.DocumentPath == null || doc.FileName == null) {
            ShowNotification("", "Please complete the upload form fields.", 'gritter-warning', true);
            self.IsUploading(false);
        } else {
            var FxWithUnderlying = (self.TempSelectedAttachUnderlying().length > 0);
            var FxWithNoUnderlying = (doc.Purpose.ID != 2);
            if (FxWithUnderlying || FxWithNoUnderlying) {
                var dataCIF = null;
                var dataName = null;
                dataCIF = viewModel.TransactionNetting().Customer().CIF;
                dataName = viewModel.TransactionNetting().Customer().Name;
                var data = {
                    CIF: dataCIF,
                    Name: dataName,
                    Type: type,
                    Purpose: purpose
                };
                if (FxWithUnderlying && data.Purpose.ID == 2) {
                    UploadFileUnderlying(data, doc, SaveDataFile);
                }
                if (FxWithNoUnderlying) {
                    self.CustomerDocuments.push(doc);
                }
                $("#modal-form-Attach").modal('hide');
                $("#modal-form-Attach1").modal('hide');
                $('#backDrop').hide();
            } else {
                ShowNotification("", "Please select underlying form the table.", 'gritter-warning', true);
                self.IsUploading(false);
            }
        }
    };
    function UploadFileUnderlying(context, document, callBack) {
        var serverRelativeUrlToFolder = '/Underlying Documents';
        var parts = document.DocumentPath.name.split('.');
        var fileExtension = parts[parts.length - 1];
        var timeStamp = new Date().getTime();
        var fileName = timeStamp + "." + fileExtension;
        var serverUrl = _spPageContextInfo.webAbsoluteUrl;

        var output;

        var getFile = getFileBufferUnderlying();
        getFile.done(function (arrayBuffer) {

            // Add the file to the SharePoint folder.
            var addFile = addFileToFolderUnderlying(arrayBuffer);
            addFile.done(function (file, status, xhr) {
                output = file.d;

                // Get the list item that corresponds to the uploaded file.
                var getItem = getListItemUnderlying(file.d.ListItemAllFields.__deferred.uri);
                getItem.done(function (listItem, status, xhr) {

                    // Change the display name and title of the list item.
                    var changeItem = updateListItemUnderlying(listItem.d.__metadata);
                    changeItem.done(function (data, status, xhr) {
                        DocumentModels({ "FileName": document.DocumentPath.name, "DocumentPath": output.ServerRelativeUrl });

                        callBack();
                    });
                    changeItem.fail(OnError);
                });
                getItem.fail(OnError);
            });
            addFile.fail(OnError);
        });
        getFile.fail(OnError);

        function getFileBufferUnderlying() {
            var deferred = $.Deferred();
            var reader = new FileReader();
            reader.onloadend = function (e) {
                deferred.resolve(e.target.result);
            }
            reader.onerror = function (e) {
                deferred.reject(e.target.error);
            }

            reader.readAsArrayBuffer(document.DocumentPath);
            return deferred.promise();
        }

        // Add the file to the file collection in the Shared Documents folder.
        function addFileToFolderUnderlying(arrayBuffer) {

            // Construct the endpoint.
            var fileCollectionEndpoint = String.format(
                    "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                    "/add(overwrite=false, url='{2}')",
                serverUrl, serverRelativeUrlToFolder, fileName);

            // Send the request and return the response.
            // This call returns the SharePoint file.
            return $.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val()
                    //"content-length": arrayBuffer.byteLength
                }
            });
        }

        function getListItemUnderlying(fileListItemUri) {

            // Send the request and return the response.
            return $.ajax({
                url: fileListItemUri,
                type: "GET",
                headers: { "accept": "application/json;odata=verbose" }
            });
        }

        function updateListItemUnderlying(itemMetadata) {

            var body = {
                Title: document.DocumentPath.name,
                Application_x0020_ID: context.ApplicationID,
                CIF: context.CIF,
                Customer_x0020_Name: context.Name,
                Document_x0020_Type: context.Type.DocTypeName,
                Document_x0020_Purpose: context.Purpose.Name,
                __metadata: {
                    type: itemMetadata.type,
                    FileLeafRef: fileName,
                    Title: fileName
                }
            };

            // Send the request and return the promise.
            // This call does not return response content from the server.
            return $.ajax({
                url: itemMetadata.uri,
                type: "POST",
                data: ko.toJSON(body),
                headers: {
                    "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                    "content-type": "application/json;odata=verbose",
                    //"content-length": body.length,
                    "IF-MATCH": itemMetadata.etag,
                    "X-HTTP-Method": "MERGE"
                }
            });
        }
    }
    self.GetCustomerAutocompleted = function (element) {
        element.autocomplete({
            source: function (request, response) {
                var options = {
                    url: api.server + api.url.customer + "/Search",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };
                Helper.Ajax.AutoComplete(options, response, OnSuccessCustomerAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set customer data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.CIFData(ui.item.data.CIF);
                    viewModel.TransactionNetting().Customer(ko.mapping.fromJS(ui.item.data, mapping));
                    viewModel.TransactionNetting().TZReference(null);
                    viewModel.TransactionNetting().SwapNumber(null);
                    viewModel.TransactionNetting().NettingDealNumber([]);
                    viewModel.TransactionNetting().ExpectedDateStatementLetter(null);
                    viewModel.TransactionNetting().ExpectedDateUnderlying(null);
                    viewModel.TransactionNetting().ActualDateStatementLetter(null);
                    viewModel.TransactionNetting().ActualDateUnderlying(null);
                    cifData = ui.item.data.CIF;
                    DealModel.cif = cifData;
                    customerNameData = ui.item.data.Name;
                    GetDataUnderlying();
                    viewModel.IsCustomerAvailable(true);
                }
                else {
                    viewModel.TransactionNetting().Customer(null);
                    viewModel.TransactionNetting().TZReference(null);
                    viewModel.TransactionNetting().SwapNumber(null);
                    viewModel.TransactionNetting().NettingDealNumber([]);
                    viewModel.TransactionNetting().ExpectedDateStatementLetter(null);
                    viewModel.TransactionNetting().ExpectedDateUnderlying(null);
                    viewModel.TransactionNetting().ActualDateStatementLetter(null);
                    viewModel.TransactionNetting().ActualDateUnderlying(null);
                }
            },
            response: function (event, ui) {
                if (ui.content.length === 0) {
                    countResult.CustomerName = 0;
                } else {
                    countResult.CustomerName = 1;
                }
            }

        });
    }
    function OnSuccessCustomerAutoComplete(response, data, textStatus, jqXHR) {
        response($.map(data, function (item) {

            return {
                label: item.CIF + " - " + item.Name,
                value: item.Name,

                data: {
                    CIF: item.CIF,
                    Name: item.Name,
                    POAName: item.POAName,
                    Accounts: item.Accounts,
                    Branch: item.Branch,
                    Contacts: item.Contacts,
                    Functions: item.Functions,
                    RM: item.RM,
                    Type: item.Type,
                    Underlyings: item.Underlyings,
                    LastModifiedBy: item.LastModifiedBy,
                    LastModifiedDate: item.LastModifiedDate
                }
            }
        })
        );
    }
    self.SaveDraftNettingTransaction = function () {
        self.TransactionNetting().IsDraft(true);
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            viewModel.IsEditable(false);

            if (viewModel.ID() != null) {
                viewModel.TransactionNetting().NettingTZReference('');
                viewModel.TransactionNetting().TZReference('');
                viewModel.TransactionNetting().TZReference(viewModel.TZReferenceTemp);
                viewModel.TransactionNetting().NettingTZReference(viewModel.NettingTZReferenceTemp);
            }
            ValidateTransaction();

        } else {
            viewModel.IsEditable(true);
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);

        }

    };
    self.SaveNettingTransaction = function () {
        self.TransactionNetting().IsDraft(false);
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            self.IsEnableSubmit(false);
            ValidateTransaction();
        }
        else {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
            self.IsEnableSubmit(true);
        }
    };

    function SaveTransaction() {
        ko.utils.arrayForEach(viewModel.TempSelectedUnderlying(), function (item) {
            viewModel.TransactionNetting().Underlyings.push(new SelectUtillizeModel(item.ID(), true, item.USDAmount(), ''));
        });

        var options = {
            url: api.server + "api/TMONetting/Add",
            token: accessToken,
            data: ko.toJSON(viewModel.TransactionNetting())
        };
        var obj = ko.toJS(viewModel.TransactionNetting());
        Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
    }
    function ValidateTransaction() {
        UploadDocuments();
    }
    function UploadDocuments() {
        var data = {
            ApplicationID: viewModel.TransactionNetting().ApplicationID(),
            CIF: viewModel.TransactionNetting().Customer().CIF,
            Name: viewModel.TransactionNetting().Customer().Name
        };

        var items = ko.utils.arrayFilter(viewModel.CustomerDocuments(), function (item) {
            return item.Purpose.ID != 2 && item.IsNewDocument == true;
        });

        if (items != null && items.length > 0) {
            UploadFileRecuresive(data, items, SaveTransaction, items.length);
        }
        else {
            SaveTransaction();
        }
    }
    function UploadFileRecuresive(context, document, callBack, numFile) {

        var indexDocument = document.length - numFile;
        var IsDraft;

        IsDraft = viewModel.TransactionNetting().IsDraft();
        var serverRelativeUrlToFolder = '';
        if (IsDraft == true) {
            serverRelativeUrlToFolder = '/DraftDocument'
        } else {
            serverRelativeUrlToFolder = '/Instruction Documents'
        }

        var parts = document[indexDocument].DocumentPath.name.split('.');
        var fileExtension = parts[parts.length - 1];
        var timeStamp = new Date().getTime();
        var fileName = timeStamp + "." + fileExtension;

        var serverUrl = _spPageContextInfo.webAbsoluteUrl;
        var output;

        // Initiate method calls using jQuery promises.
        // Get the local file as an array buffer.
        var getFile = getFileBuffer();
        getFile.done(function (arrayBuffer) {

            // Add the file to the SharePoint folder.
            var addFile = addFileToFolder(arrayBuffer);
            addFile.done(function (file, status, xhr) {
                output = file.d;

                // Get the list item that corresponds to the uploaded file.
                var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
                getItem.done(function (listItem, status, xhr) {

                    // Change the display name and title of the list item.
                    var changeItem = updateListItem(listItem.d.__metadata);
                    changeItem.done(function (data, status, xhr) {
                        //alert('file uploaded and updated');
                        //return output;
                        var newDoc = {
                            ID: 0,
                            Type: document[indexDocument].Type,
                            Purpose: document[indexDocument].Purpose,
                            IsNewDocument: false,
                            LastModifiedDate: null,
                            LastModifiedBy: null,
                            DocumentPath: output.ServerRelativeUrl,
                            FileName: document[indexDocument].DocumentPath.name
                        };

                        viewModel.TransactionNetting().Documents.push(newDoc);

                        if (numFile > 1) {
                            UploadFileRecuresive(context, document, callBack, numFile - 1); // recursive function
                        } else {
                            callBack();
                        }
                    });
                    changeItem.fail(OnError);
                });
                getItem.fail(OnError);
            });
            addFile.fail(OnError);
        });
        getFile.fail(OnError);
        // Get the local file as an array buffer.
        function getFileBuffer() {
            var deferred = $.Deferred();
            var reader = new FileReader();
            reader.onloadend = function (e) {
                deferred.resolve(e.target.result);
            }
            reader.onerror = function (e) {
                deferred.reject(e.target.error);
            }

            if (document[indexDocument].DocumentPath.type == Util.spitemdoc) {
                var fileURI = serverUrl + document[indexDocument].DocumentPath.DocPath;
                // load file:
                fetch(fileURI, convert, alert);

                function convert(buffer) {
                    var blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });

                    var domURL = self.URL || self.webkitURL || self,
                      url = domURL.createObjectURL(blob),
                      img = new Image;

                    img.onload = function () {
                        domURL.revokeObjectURL(url); // clean up
                        //document.body.appendChild(this);
                        // this = image
                    };
                    img.src = url;
                    reader.readAsArrayBuffer(blob);
                }

                function fetch(url, callback, error) {

                    var xhr = new XMLHttpRequest();
                    try {
                        xhr.open("GET", url);
                        xhr.responseType = "arraybuffer";
                        xhr.onerror = function () {
                            error("Network error")
                        };
                        xhr.onload = function () {
                            if (xhr.status === 200) callback(xhr.response);
                            else error(xhr.statusText);
                        };
                        xhr.send();
                    } catch (err) {
                        error(err.message)
                    }
                }


            } else {
                reader.readAsArrayBuffer(document[indexDocument].DocumentPath);
            }
            //bangkit

            //reader.readAsDataURL(document.DocumentPath);
            return deferred.promise();
        }

        // Add the file to the file collection in the Shared Documents folder.
        function addFileToFolder(arrayBuffer) {

            // Construct the endpoint.
            var fileCollectionEndpoint = String.format(
                    "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                    "/add(overwrite=false, url='{2}')",
                serverUrl, serverRelativeUrlToFolder, fileName);

            // Send the request and return the response.
            // This call returns the SharePoint file.
            return $.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val()
                    //"content-length": arrayBuffer.byteLength
                }
            });
        }

        // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
        function getListItem(fileListItemUri) {

            // Send the request and return the response.
            return $.ajax({
                url: fileListItemUri,
                type: "GET",
                headers: { "accept": "application/json;odata=verbose" }
            });
        }

        // Change the display name and title of the list item.
        function updateListItem(itemMetadata) {
            var body = {
                Title: document[indexDocument].DocumentPath.name,
                Application_x0020_ID: context.ApplicationID,
                CIF: context.CIF,
                Customer_x0020_Name: context.Name,
                Document_x0020_Type: document[indexDocument].Type.Name,
                Document_x0020_Purpose: document[indexDocument].Purpose.Name,
                __metadata: {
                    type: itemMetadata.type,
                    FileLeafRef: fileName,
                    Title: fileName
                }
            };

            // Send the request and return the promise.
            // This call does not return response content from the server.
            return $.ajax({
                url: itemMetadata.uri,
                type: "POST",
                data: ko.toJSON(body),
                headers: {
                    "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                    "content-type": "application/json;odata=verbose",
                    //"content-length": body.length,
                    "IF-MATCH": itemMetadata.etag,
                    "X-HTTP-Method": "MERGE"
                }
            });
        }

    }
    function OnSuccessSaveAPI(data, textStatus, jqXHR) {
        var AppID = {};
        var DraftID;

        if (data.ID != null || data.ID != undefined) {
            AppID = {
                TransactionID: data.ID,
                ApplicationID: data.AppID
            };
            viewModel.TransactionNetting().ID(data.ID);
            viewModel.TransactionNetting().ApplicationID(data.AppID);
            if (viewModel.TransactionNetting().IsDraft() == false) {
                if (viewModel.ID() > 0) {
                    DeleteDraft(viewModel.ID());
                }
                viewModel.AddListItem();
            }
            else {
                ShowNotification("Transaction Draft Success", "Transaction has been saved as a draft", "gritter-warning", true);
                window.location = "/home/draft-transactions";
            }
        }
    }
    self.AddListItem = function () {
        var body = {
            Title: viewModel.TransactionNetting().ApplicationID(),
            //IsNetting: true,
            __metadata: {
                type: config.sharepoint.metadata.listNetting
            }
        };

        var options = {
            url: config.sharepoint.url.api + "/Lists(guid'" + config.sharepoint.listIdNetting + "')/Items",
            data: JSON.stringify(body),
            digest: jQuery("#__REQUESTDIGEST").val()
        };
        Helper.Sharepoint.List.Add(options, OnSuccessAddListItem, OnError, OnAlways);
    }
    function OnSuccessAddListItem(data, textStatus, jqXHR) {
        if (viewModel.TransactionNetting().ApplicationID() != null) {
            viewModel.ApplicationIDColl([]);
            var appColl = {
                TransactionID: viewModel.TransactionNetting().ID(),
                ApplicationID: viewModel.TransactionNetting().ApplicationID(),
                Customer: viewModel.TransactionNetting().Customer(),
            }

            viewModel.ApplicationIDColl.push(appColl);
            ShowNotification("Book Success", JSON.stringify(data.Message), "gritter-success", true);
            $("#modal-form-applicationID").modal('show');
        }
    }
    self.SaveNettingDealNumber = function () {
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            var NettingDFNumber = {
                DFNumber: self.DFNumber(),
                SwapDF: self.SwapDF()
            };
            self.TransactionNetting().NettingDealNumber.push(ko.mapping.fromJS(NettingDFNumber));
            $("#modal-form-netting-deal-number").modal('hide');
        }
        else {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        }
    };
    self.RemoveNettingDealNumber = function (data) {
        //alert(JSON.stringify(data))
        self.TransactionNetting().NettingDealNumber.remove(data);
    };
    self.GetSelectedNettingDealNumber = function (index, data) {
        $("#modal-form-netting-deal-number").modal('show');
        $('.remove').click();
        $('#backDrop').show();
        self.IndexUpdate(index);
        self.IsNewNettingDealNumber(false);
        self.IsEditNettingDealNumber(true);
        self.DFNumber(data.DFNumber());
        self.SwapDF(data.SwapDF());
    };
    self.EditNettingDealNumber = function () {
        var NettingDFNumber = {
            DFNumber: self.DFNumber(),
            SwapDF: self.SwapDF()
        };
        self.TransactionNetting().NettingDealNumber.replace(self.TransactionNetting().NettingDealNumber()[self.IndexUpdate()], ko.mapping.fromJS(NettingDFNumber));
        $("#modal-form-netting-deal-number").modal('hide');
    };
    self.AddNetting = function () {
        $("#modal-form-netting-deal-number").modal('show');
        $('.remove').click();
        $('#backDrop').show();
        var dfnumber = $("#df-number");
        viewModel.GetDealNumberAutocompleted(dfnumber, viewModel.CIFData());
        self.DFNumber(null);
        self.SwapDF(null);
        self.IsNewNettingDealNumber(true);
        self.IsEditNettingDealNumber(false);
    };
    self.GetNettingPurpose = function () {
        $.ajax({
            async: false,
            type: "GET",
            url: api.server + api.url.parametersystem + "/partype/" + ConsPARSYS.tmoNettingPurpose,
            contentType: "application/json; charset=utf-8; odata=verbose;",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                viewModel.Parameter().NettingPurpose(ko.mapping.toJS(data.Parsys));
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });

    }
    self.GetDealNumberAutocompleted = function (element, cif) {
        // autocomplete
        element.autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.helper + "/TZRefTMO/Complete/Search",
                    data: {
                        query: request.term,
                        limit: 20,
                        cif: viewModel.CIFData() != null ? viewModel.CIFData() : ""
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessDealNumberAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set customer data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    if (element.selector == "#df-number") {
                        viewModel.DFNumber(ko.mapping.toJS(ui.item.data.TZReference, mapping));
                        if (ui.item.data.SwapDealNumber != null) {
                            viewModel.SwapDF(ko.mapping.toJS(ui.item.data.SwapDealNumber, mapping));
                        }
                    } else {
                        viewModel.TransactionNetting().TZReference(null);
                        viewModel.TransactionNetting().SwapNumber(null);
                        viewModel.TransactionNetting().TZReference(ko.mapping.toJS(ui.item.data, mapping));
                        if (ui.item.data.SwapDealNumber != null) {
                            viewModel.TransactionNetting().SwapNumber(ko.mapping.toJS(ui.item.data.SwapDealNumber, mapping));
                        }
                        if (ui.item.data.ExpectedDateStatementLetter != null) {
                            viewModel.TransactionNetting().ExpectedDateStatementLetter(viewModel.LocalDate(ui.item.data.ExpectedDateStatementLetter, true, false));
                        } else {
                            viewModel.TransactionNetting().ExpectedDateStatementLetter(null);
                        }

                        if (ui.item.data.ExpectedDateUnderlying != null) {// || ui.item.data.ExpectedDateUnderlying.substring(0, 4) != UnvalidYear) {
                            viewModel.TransactionNetting().ExpectedDateUnderlying(viewModel.LocalDate(ui.item.data.ExpectedDateUnderlying, true, false));
                        } else {
                            viewModel.TransactionNetting().ExpectedDateUnderlying(null);
                        }

                        if (ui.item.data.ActualDateStatementLetter != null) {// || ui.item.data.ActualDateStatementLetter.substring(0, 4) != UnvalidYear) {
                            viewModel.TransactionNetting().ActualDateStatementLetter(viewModel.LocalDate(ui.item.data.ActualDateStatementLetter, true, false));
                        } else {
                            viewModel.TransactionNetting().ActualDateStatementLetter(null);
                        }

                        if (ui.item.data.ActualDateUnderlying != null) {// || ui.item.data.ActualDateUnderlying.substring(0, 4) != UnvalidYear) {
                            viewModel.TransactionNetting().ActualDateUnderlying(viewModel.LocalDate(ui.item.data.ActualDateUnderlying, true, false));
                        } else {
                            viewModel.TransactionNetting().ActualDateUnderlying(null);
                        }
                    }
                }
                else {
                    if (element.selector == "#df-number") {
                        viewModel.DFNumber(null);
                        viewModel.SwapDF(null);
                    } else {
                        viewModel.TransactionNetting().TZReference(null);
                        viewModel.TransactionNetting().ExpectedDateStatementLetter(null);
                        viewModel.TransactionNetting().ExpectedDateUnderlying(null);
                        viewModel.TransactionNetting().ActualDateStatementLetter(null);
                        viewModel.TransactionNetting().ActualDateUnderlying(null);
                    }
                }
            },
            response: function (event, ui) {
                if (ui.content.length === 0) {
                    countResult.DealNumber = 0;
                } else {
                    countResult.DealNumber = 1;
                }
            }
        });
    }
    self.onSelectionUtilize = function (index, item) {

        var data = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
            return x.ID() == item.ID;
        });

        if (data != null) {
            self.TempSelectedUnderlying.remove(data);
            self.ResetDataUnderlying(index, false);
            setTotalUtilize();
            SetNettingDocuments();

        } else {
            self.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, false, parseFloat(item.UtilizeAmountDeal.replace(/,/g, '')), item.StatementLetter.ID));
            self.ResetDataUnderlying(index, true);
            self.setTotalUtilize();
            self.SetNettingDocuments();
        }
    }
    self.setTotalUtilize = function () {
        var total = 0;
        ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (item) {
            total += parseFloat(ko.utils.unwrapObservable(item.USDAmount()));
        });
        viewModel.TransactionNetting().UtilizationAmount(parseFloat(total).toFixed(2));
    }
    self.SetNettingDocuments = function () {
        ko.utils.arrayForEach(self.NettingAllDocumentsTemp(), function (item) {
            if (item.Purpose.ID == 2) {
                self.CustomerDocuments.remove(item);
                var dataDoc = jQuery.grep(self.TransactionNetting().Documents, function (value) {
                    return value.ID != item.ID;
                });
                self.TransactionNetting().Documents(dataDoc);
            }
        });
        ko.utils.arrayForEach(self.NettingAllDocumentsTemp(), function (item) {
            ko.utils.arrayForEach(self.NettingUnderlyings(), function (item2) {
                if (item2 == item.UnderlyingID && item.Purpose.ID == 2) {
                    self.CustomerDocuments.push(item);
                    self.TransactionNetting().Documents.push(item);
                }
            });
        });
    }
    self.ResetDataUnderlying = function (index, isSelect) {
        self.CustomerUnderlyings()[index].IsEnable = isSelect;
        var dataRows = self.CustomerUnderlyings().slice(0);
        self.CustomerUnderlyings([]);
        self.CustomerUnderlyings(dataRows);
    }
    function OnSuccessDealNumberAutoComplete(response, data, textStatus, jqXHR) {

        response($.map(data, function (item) {

            return {
                // default autocomplete object
                label: item.TZReference != '' ? item.TZReference + " - " + item.CustomerName + "(" + item.CIF + ")" : item.MurexNumber + " - " + item.CustomerName + "(" + item.CIF + ")",
                value: item.TZReference != '' ? item.TZReference : item.MurexNumber,

                // custom object binding
                data: {
                    ID: item.ID,
                    CIF: item.CIF,
                    Name: item.CustomerName,
                    TZReference: item.TZReference,
                    SwapDealNumber: item.SwapDealNumber,
                    MurexNumber: item.MurexNumber,
                    ExpectedDateStatementLetter: item.ExpectedDateStatementLetter,
                    ExpectedDateUnderlying: item.ExpectedDateUnderlying,
                    ActualDateStatementLetter: item.ActualDateStatementLetter,
                    ActualDateUnderlying: item.ActualDateUnderlying
                }

            }

        })
        );
    }
    //Function to Read All Customers Bulk Underlying // add 2015.03.09
    function GetDataBulkUnderlying() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Bulk",
            params: {
                page: self.UnderlyingBulkGridProperties().Page(),
                size: self.UnderlyingBulkGridProperties().Size(),
                sort_column: self.UnderlyingBulkGridProperties().SortColumn(),
                sort_order: self.UnderlyingBulkGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns

        var filters = GetBulkUnderlyingFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingBulk, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingBulk, OnError, OnAlways);
        }
    }
    // On success GetData callback // Add 2015.03.09
    function OnSuccessGetUnderlyingBulk(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            var databulk = SetSelectedBulk(data.Rows);

            self.CustomerBulkUnderlyings(databulk);

            self.UnderlyingBulkGridProperties().Page(data['Page']);
            self.UnderlyingBulkGridProperties().Size(data['Size']);
            self.UnderlyingBulkGridProperties().Total(data['Total']);
            self.UnderlyingBulkGridProperties().TotalPages(Math.ceil(self.UnderlyingBulkGridProperties().Total() / self.UnderlyingBulkGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }
    function GetDataUnderlyingProforma() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Proforma",
            params: {
                page: self.UnderlyingProformaGridProperties().Page(),
                size: self.UnderlyingProformaGridProperties().Size(),
                sort_column: self.UnderlyingProformaGridProperties().SortColumn(),
                sort_order: self.UnderlyingProformaGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetUnderlyingProformaFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingProforma, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingProforma, OnError, OnAlways);
        }
    }
    function OnSuccessGetUnderlyingProforma(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            SetSelectedProforma(data.Rows);

            self.CustomerUnderlyingProformas(data.Rows);

            self.UnderlyingProformaGridProperties().Page(data['Page']);
            self.UnderlyingProformaGridProperties().Size(data['Size']);
            self.UnderlyingProformaGridProperties().Total(data['Total']);
            self.UnderlyingProformaGridProperties().TotalPages(Math.ceil(self.UnderlyingProformaGridProperties().Total() / self.UnderlyingProformaGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }
    function OnSuccessToken(data, textStatus, jqXHR) {
        // store token on browser cookies
        $.cookie(api.cookie.name, data.AccessToken);
        accessToken = $.cookie(api.cookie.name);

        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);
            viewModel.SPUser(ko.mapping.toJS(spUser));
        }

        // get parameter values
        DealModel.token = accessToken;
        //GetParameterData(DealModel, OnSuccessGetTotal, OnErrorDeal);
        GetParameters();
    }
    function GetDataUnderlying() {
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        var options = {
            url: api.server + api.url.customerunderlying + "/Underlying",
            params: {
                page: self.UnderlyingGridProperties().Page(),
                size: self.UnderlyingGridProperties().Size(),
                sort_column: self.UnderlyingGridProperties().SortColumn(),
                sort_order: self.UnderlyingGridProperties().SortOrder()
            },
            token: accessToken
        };

        self.UnderlyingFilterIsAvailable(true);
        self.UnderlyingFilterIsExpiredDate(true);
        var filters = GetUnderlyingFilteredColumns();

        if (filters.length > 0) {
            options.data = ko.toJSON(filters);
            Helper.Ajax.Post(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        } else {
            Helper.Ajax.Get(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        }
    }
    function OnSuccessGetDataUnderlying(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            var dataunderlying = SetSelectedUnderlying(data.Rows);
            self.CustomerUnderlyings([]);
            ko.utils.arrayForEach(dataunderlying, function (itemd, index) {
                itemd.UtilizeAmountDeal = formatNumber(itemd.UtilizeAmountDeal);
                itemd.TransactionID = index;
            });
            self.CustomerUnderlyings(dataunderlying);
            self.UnderlyingGridProperties().Page(data['Page']);
            self.UnderlyingGridProperties().Size(data['Size']);
            self.UnderlyingGridProperties().Total(data['Total']);
            self.UnderlyingGridProperties().TotalPages(Math.ceil(self.UnderlyingGridProperties().Total() / self.UnderlyingGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }
    function GetDataUnderlyingAttach() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Attach",
            params: {
                page: self.UnderlyingAttachGridProperties().Page(),
                size: self.UnderlyingAttachGridProperties().Size(),
                sort_column: self.UnderlyingAttachGridProperties().SortColumn(),
                sort_order: self.UnderlyingAttachGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetUnderlyingAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingDataAttachFile, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingDataAttachFile, OnError, OnAlways);
        }
    }

    // On success GetData callback
    function OnSuccessGetUnderlyingDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            for (var i = 0 ; i < data.Rows.length; i++) {
                var selected = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (item) {
                    return item.ID == data.Rows[i].ID;
                });
                if (selected != null) {
                    data.Rows[i].IsSelectedAttach = true;
                }
                else {
                    data.Rows[i].IsSelectedAttach = false;
                }
            }
            self.CustomerAttachUnderlyings(data.Rows);

            self.UnderlyingAttachGridProperties().Page(data['Page']);
            self.UnderlyingAttachGridProperties().Size(data['Size']);
            self.UnderlyingAttachGridProperties().Total(data['Total']);
            self.UnderlyingAttachGridProperties().TotalPages(Math.ceil(self.UnderlyingAttachGridProperties().Total() / self.UnderlyingAttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    function SetSelectedUnderlying(data) {
        for (var i = 0; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (item) {
                return item.ID() == data[i].ID;
            });
            if (selected != null) {
                data[i].IsEnable = true;
                data[i].UtilizeAmountDeal = formatNumber(selected.USDAmount());
            }
            else {
                data[i].IsEnable = false;
            }
        }
        return data;
    }
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
};

var viewModel = new ViewModel();

$.holdReady(true);
var intervalRoleName = setInterval(function () {
    if (Object.keys(Const_RoleName).length != 0) {
        $.holdReady(false);
        clearInterval(intervalRoleName);
    }
}, 100);


$(document).ready(function () {
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(OnSuccessToken, OnError);
    } else {
        accessToken = $.cookie(api.cookie.name);
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);
            viewModel.SPUser(ko.mapping.toJS(spUser));
            viewModel.isDealBUGroup(true);
            $.ajax({
                type: "GET",
                url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + $.cookie(api.cookie.name)
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        viewModel.isDealBUGroup(false);
                        for (i = 0; i < spUser.Roles.length; i++) {
                            for (j = 0; j < data.length; j++) {
                                if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {

                                    if (Const_RoleName[spUser.Roles[i].ID].toLowerCase().startsWith("dbs fx") && Const_RoleName[spUser.Roles[i].ID].toLowerCase().endsWith("maker")) { // develop //if (spUser.Roles[i].Name.toLowerCase().startsWith("dbs fx") && spUser.Roles[i].Name.toLowerCase().endsWith("maker")) { // develop
                                        viewModel.isDealBUGroup(true);
                                    }
                                }
                            }
                        }
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }
        DealModel.token = accessToken;
        viewModel.GetNettingPurpose();
        viewModel.GetParameters();
    }

    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        focusCleanup: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });

    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        focusCleanup: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });

    $('.date-picker').datepicker({
        autoclose: true,
        onSelect: function () {
            this.focus();
        }
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    ko.bindingHandlers.datepicker = {
        init: function (element) {
            $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).datepicker("refresh");
        }
    };

    ko.bindingHandlers.file = {
        init: function (element, valueAccessor) {
            $(element).change(function () {
                var file = this.files[0];
                if (ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };

    ko.dependentObservable(function () {

        var nettingPurpose = ko.utils.arrayFirst(viewModel.Parameter().NettingPurpose(), function (item) { return item.ID == viewModel.Selected().NettingPurpose(); });
        if (nettingPurpose != null) {
            viewModel.TransactionNetting().NettingPurpose(nettingPurpose);
        }
    });

    var customerAutocomplete = $("#customer-name");
    var dealnumberAutocomplete = $("#tz-reference");
    var CIFCustomer = viewModel.CIFData();
    viewModel.GetDealNumberAutocompleted(dealnumberAutocomplete, viewModel.CIFData());
    viewModel.GetCustomerAutocompleted(customerAutocomplete);

    ko.applyBindings(viewModel);

});