﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    public class TBOTMOController : ApiController
    {
         private string message = string.Empty;
        private readonly ITBOTMOModelInterface repository = null;

        public TBOTMOController() {
            this.repository = new TBOTMOModelRepository();
        }
        public TBOTMOController(ITBOTMOModelInterface repository)
        {
            this.repository = repository;
        }

        [ResponseType(typeof(IList<TBOTMOModel>))]
        [HttpGet]
        [Route("api/TBOTMOToolByCIF")]
        public HttpResponseMessage GetTBOTMOByCif(string id) 
        {
            IList<TBOTMOModel> output = new List<TBOTMOModel>();

            if (!repository.GetTransactionTBOTMOByCif(ref output, id, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TBOTMOModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(IList<TBOTMOModel>))]
        [HttpGet]
        [Route("api/TBOTMOByIDWFAppID/{spUserLoginName}")]
        public HttpResponseMessage GetTBOTMOWFID(string spUserLoginName) 
        {
            IList<TBOTMOModel> output = new List<TBOTMOModel>();

            if (!repository.GetTBOTMOWFID(ref output, spUserLoginName, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(IList<TBOTMOModel>))]
        [HttpGet]
        [Route("api/TBOTMOToolByID/{cif}")]
        public HttpResponseMessage GetTBOTMO(string cif)  
        {
            IList<TBOTMOModel> output = new List<TBOTMOModel>();

            if (!repository.GetTBOTMO(ref output, cif, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TBOTMOModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(IList<TBOTMOModel>))]
        [HttpGet]
        [Route("api/TBOTMOToolByIDWF/{cif}")]
        public HttpResponseMessage GetTBOTMOWF(string cif) 
        { 
            IList<TBOTMOModel> output = new List<TBOTMOModel>();

            if (!repository.GetTBOTMOWF(ref output, cif, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TBOTMOModel>>(HttpStatusCode.OK, output);
            }
        }


        [ResponseType(typeof(IList<TransactionTBOTMOTimelineModel>))]
        [HttpGet]
        [Route("api/TBOTMOToolByIDWFInstanceID/{workflowInstanceID}")]
        public HttpResponseMessage GetTBOTMOWFTimelineID(Guid workflowInstanceID) 
        {
            IList<TransactionTBOTMOTimelineModel> output = new List<TransactionTBOTMOTimelineModel>();

            if (!repository.GetTransactionTimeline(ref output, workflowInstanceID, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/TBOTMOToolData")]
        public HttpResponseMessage Put(TBOTMOModel TBO)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTBOTMO(TBO, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "TBO data has been Submit." });
                }
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/TBOTMOToolData")]
        public HttpResponseMessage Post(TBOTMOModelWF TBO) 
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.SaveTBOTMO(TBO, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "TBO data has been Submit to Workflow." });
                }
            }
        }

        [ResponseType(typeof(IList<TBOTMOModel>))]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            IList<TBOTMOModel> output = new List<TBOTMOModel>();

            if (!repository.GetTBOTMO(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TBOTMOModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(IList<TBOTMOModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<TBOTMOModel> output = new List<TBOTMOModel>();

            if (!repository.GetTBOTMO(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<TBOTMOModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(TBOTMOModelGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/TBOTMOTool")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<TBOTMOModelFilter> filters)
        {
            TBOTMOModelGrid output = new TBOTMOModelGrid();

            if (!repository.GetTBOTMO(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "CreatedDate" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<TBOTMOModelGrid>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(TBOTMOModelGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/TBOTMOToolWF")]
        public HttpResponseMessage GetTBOWF(int? page, int? size, string sort_column, string sort_order, IList<TBOTMOModelFilter> filters) 
        {
            TBOTMOModelGrid output = new TBOTMOModelGrid();

            if (!repository.GetTBOTMOWF(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "CreatedDate" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<TBOTMOModelGrid>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(TBOTMOModel))]
        [Route("api/TBOTMOTool/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            TBOTMOModel output = new TBOTMOModel();

            if (!repository.GetTBOTMOByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<TBOTMOModel>(HttpStatusCode.OK, output);
            }
        }
    }
}
