﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Net.Http;
using System.Web.Script.Serialization;
using System.Transactions;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("CBOMaintenacePriorityOrder")]
    public class CBOMaintenancePriorityOrderModel
    {
        /// <summary>
        /// EcalationCustomer ID.
        /// </summary>
        /// 
        public int CBOMaintainPriority { get; set; }  
        
        public int CBOMaintainID { get; set; }
        public int TransTypeID { get; set; }
        public int ProductID { get; set; }
        /// <summary>
        /// EcalationCustomer Name.
        /// </summary>        
        public string MaintenanceTypeName { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public bool IsDeleted { get; set; }

        /// <summary>
        /// EcalationCustomer Name.
        /// </summary>        
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string UpdateBy { get; set; }

        public int OrderNo { get; set; }
    }
    public class CBOMaintenancePriorityOrderModelRow : CBOMaintenancePriorityOrderModel
    {
        public int RowID { get; set; }

    }

    public class CBOMaintenancePriorityOrderModelGrid : Grid
    {
        public IList<CBOMaintenancePriorityOrderModelRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class CBOMaintenancePriorityOrderModelRowFilter : Filter { }
    #endregion  

    #region Interface
    public interface ICBOMaintenancePriorityOrderModelRepository : IDisposable
    {
        bool GetCBOMaintenaceType(ref IList<CBOMaintenancePriorityOrderModel> CBOMaintenaceTypes, ref string message);
        bool GetCBOMaintenaceTypePriority(ref IList<CBOMaintenancePriorityOrderModel> CBOMaintenaceTypes, ref string message);
        bool AddCBOMaintenaceType(IList<CBOMaintenancePriorityOrderModel> CBOMaintenaceTypes, ref string message);        
    }
    #endregion

    #region Repository
    public class CBOMaintenanceOrderRepository : ICBOMaintenancePriorityOrderModelRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetCBOMaintenaceType(ref IList<CBOMaintenancePriorityOrderModel> CBOMaintenanceTypesGabungan, ref string message)
        {
            bool isSuccess = false;
            
            try
            {
                #region old code
                /*CBOMaintenaceTypes = (from a in context.MaintenanceTypes
                                      //join b in context.MaintenanceTypePriorities on a.CBOMaintainID equals b.CBOMaintainID 
                                      where !(from b in context.MaintenanceTypePriorities where b.IsDeleted==false select b.CBOMaintainID).Contains(a.CBOMaintainID) 
                                     // from abc in ab.Where(b=>b.IsDeleted==false && b.CBOMaintainID != a.CBOMaintainID)
                                      select new CBOMaintenancePriorityOrderModel
                                       {   
                                          
                                           CBOMaintainID = a.CBOMaintainID,
                                           MaintenanceTypeName = a.MaintenanceTypeName,
                                           IsDeleted = a.IsDeleted
                                           
                                         

                                           
                                       }).ToList();*/
                #endregion

                IList<CBOMaintenancePriorityOrderModel> CBONotMaintenanceTypes, CBOMaintenaceTypes, UTTypes;

                //Untuk Retail CIF yang tipe transaksinya bukan maintenence

                //CBONotMaintenanceTypes = (from a in context.TransactionTypes
                //                          join b in context.Products on a.ProductID equals b.ProductID
                //                          where a.IsDeleted == false && b.IsDeleted == false && b.ProductCode.ToLower() == "rc" && a.TransactionType1.ToLower() != "maintenance" 
                //                          && !(from c in context.MaintenanceTypePriorities where c.IsDeleted == false select c.TransTypeID).Contains(a.TransTypeID)
                //                          select new CBOMaintenancePriorityOrderModel {
                //                              CBOMaintainID = 0,
                //                              ProductID = 0,
                //                              TransTypeID = a.TransTypeID,
                //                              MaintenanceTypeName = a.TransactionType1,
                //                              IsDeleted = a.IsDeleted
                //                          }).ToList();
                CBONotMaintenanceTypes = (from a in context.MaintenanceTypePriorities
                                          join b in context.TransactionTypes on a.TransTypeID equals b.TransTypeID
                                          join c in context.Products on b.ProductID equals c.ProductID
                                          where a.IsDeleted.Equals(false) && b.IsDeleted.Equals(false) && c.IsDeleted.Equals(false) && c.ProductCode.ToLower() == "rc" && b.TransactionType1.ToLower() != "maintenance" && a.CBOMaintainID == null && a.ProductID == null && (a.Ordered == false || a.Ordered == null)
                                          orderby new { a.OrderNo }
                                          select new CBOMaintenancePriorityOrderModel
                                          {
                                              CBOMaintainPriority = a.CBOMaintainPriority,
                                              CBOMaintainID = 0,
                                              ProductID = 0,
                                              TransTypeID = b.TransTypeID,
                                              MaintenanceTypeName = b.TransactionType1,
                                              IsDeleted = a.IsDeleted,
                                              OrderNo = a.OrderNo
                                          }).ToList();
                #region retail cif maintenance
                //Untuk Retail CIF yang tipe transaksinya maintenence
                //CBOMaintenaceTypes = (from a in context.MaintenanceTypes
                //                      where a.IsDeleted == false 
                //                      && !(from b in context.MaintenanceTypePriorities where b.IsDeleted==false select b.CBOMaintainID).Contains(a.CBOMaintainID) 
                //                      select new CBOMaintenancePriorityOrderModel
                //                          {
                //                              CBOMaintainID = a.CBOMaintainID,
                //                              ProductID = 0,
                //                              TransTypeID = 0,
                //                              MaintenanceTypeName = a.MaintenanceTypeName + " (Maintenance)",
                //                              IsDeleted = a.IsDeleted
                //                          }).ToList();
                CBOMaintenaceTypes = (from a in context.MaintenanceTypePriorities
                                      join b in context.MaintenanceTypes on a.CBOMaintainID equals b.CBOMaintainID
                                      where a.IsDeleted.Equals(false) && a.TransTypeID.Equals(null) && a.ProductID == null && (a.Ordered == false || a.Ordered == null)// && a.CBOMaintainID!=b.CBOMaintainID
                                      orderby new { a.OrderNo }
                                      select new CBOMaintenancePriorityOrderModel
                                      {
                                          CBOMaintainPriority = a.CBOMaintainPriority,
                                          CBOMaintainID = a.CBOMaintainID.Value,
                                          TransTypeID = 0,
                                          ProductID = 0,
                                          MaintenanceTypeName = b.MaintenanceTypeName + " (Maintenance)",
                                          IsDeleted = a.IsDeleted,
                                          OrderNo = a.OrderNo
                                      }).ToList();
                #endregion

                //UT ID Invesment

                //UTTypes = (from a in context.Products
                //           where a.IsDeleted == false && a.ProductCode.ToLower() == "in" 
                //           && !(from b in context.MaintenanceTypePriorities where b.IsDeleted == false select b.ProductID).Contains(a.ProductID)
                //           select new CBOMaintenancePriorityOrderModel {
                //                   CBOMaintainID = 0,
                //                   ProductID = a.ProductID,
                //                   TransTypeID = 0,
                //                   MaintenanceTypeName = a.ProductName + " (UT)",
                //                   IsDeleted = a.IsDeleted
                //           }).ToList();
                UTTypes = (from a in context.MaintenanceTypePriorities
                           join c in context.Products on a.ProductID equals c.ProductID
                           //where a.IsDeleted.Equals(false) && c.IsDeleted.Equals(false) && c.ProductCode.ToLower() == "ii" && a.CBOMaintainID == null && a.TransTypeID == null && (a.Ordered == false || a.Ordered == null)
                           where a.IsDeleted.Equals(false) && c.IsDeleted.Equals(false) && c.ProductCode.ToLower() == "in" && a.CBOMaintainID == null && a.TransTypeID == null && (a.Ordered == false || a.Ordered == null)
                           orderby new { a.OrderNo }
                           select new CBOMaintenancePriorityOrderModel
                           {
                               CBOMaintainPriority = a.CBOMaintainPriority,
                               CBOMaintainID = 0,
                               TransTypeID = 0,
                               ProductID = c.ProductID,
                               MaintenanceTypeName = c.ProductName + " (UT)",
                               IsDeleted = a.IsDeleted,
                               OrderNo = a.OrderNo
                           }).ToList();

                CBOMaintenanceTypesGabungan = CBONotMaintenanceTypes.Union(CBOMaintenaceTypes).Union(UTTypes).OrderBy(a => a.OrderNo).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetCBOMaintenaceTypePriority(ref IList<CBOMaintenancePriorityOrderModel> CBOMaintenanceTypesGabungan, ref string message)
        {
            bool isSuccess = false;
          
            try
            {
                IList<CBOMaintenancePriorityOrderModel> CBO_CIF_NotMaintenanceTypes, CBO_CIF_MaintenaceTypes, UTTypes;

                //Untuk Retail CIF yang tipe transaksinya bukan maintenence
                CBO_CIF_NotMaintenanceTypes = (from a in context.MaintenanceTypePriorities
                                          join b in context.TransactionTypes on a.TransTypeID equals b.TransTypeID
                                          join c in context.Products on b.ProductID equals c.ProductID
                                          where a.IsDeleted.Equals(false) && b.IsDeleted.Equals(false) && c.IsDeleted.Equals(false) && c.ProductCode.ToLower() == "rc" && b.TransactionType1.ToLower() != "maintenance" && a.CBOMaintainID == null && a.ProductID == null && a.Ordered == true
                                          orderby new { a.OrderNo }
                                          select new CBOMaintenancePriorityOrderModel
                                          {
                                              CBOMaintainPriority = a.CBOMaintainPriority,
                                              CBOMaintainID = 0,
                                              ProductID = 0,
                                              TransTypeID = b.TransTypeID,
                                              MaintenanceTypeName = b.TransactionType1,
                                              IsDeleted = a.IsDeleted,
                                              OrderNo = a.OrderNo
                                          }).ToList();

                //Untuk Retail CIF yang tipe transaksinya maintenence
                CBO_CIF_MaintenaceTypes = (from a in context.MaintenanceTypePriorities
                                      join b in context.MaintenanceTypes on a.CBOMaintainID equals b.CBOMaintainID
                                      where a.IsDeleted.Equals(false) && a.TransTypeID == null && a.ProductID == null && a.Ordered == true// && a.CBOMaintainID!=b.CBOMaintainID
                                      orderby new { a.OrderNo }
                                      select new CBOMaintenancePriorityOrderModel
                                       {
                                           CBOMaintainPriority = a.CBOMaintainPriority,                                      
                                           CBOMaintainID = a.CBOMaintainID.Value,
                                           TransTypeID = 0,
                                           ProductID = 0,
                                           MaintenanceTypeName = b.MaintenanceTypeName + " (Maintenance)",
                                           IsDeleted = a.IsDeleted,
                                           OrderNo = a.OrderNo                                          
                                       }).ToList();

                //
                UTTypes = (from a in context.MaintenanceTypePriorities
                           join c in context.Products on a.ProductID equals c.ProductID
                           //where a.IsDeleted.Equals(false) && c.IsDeleted.Equals(false) && c.ProductCode.ToLower() == "ii" && a.CBOMaintainID == null && a.TransTypeID == null && a.Ordered == true
                           where a.IsDeleted.Equals(false) && c.IsDeleted.Equals(false) && c.ProductCode.ToLower() == "in" && a.CBOMaintainID == null && a.TransTypeID == null && a.Ordered == true
                           orderby new { a.OrderNo }
                           select new CBOMaintenancePriorityOrderModel
                           {
                              CBOMaintainPriority = a.CBOMaintainPriority,
                              CBOMaintainID = 0,
                              TransTypeID = 0,
                              ProductID = c.ProductID,
                              MaintenanceTypeName = c.ProductName + " (UT)",
                              IsDeleted = a.IsDeleted,
                              OrderNo = a.OrderNo
                           }).ToList();
                
                CBOMaintenanceTypesGabungan = CBO_CIF_NotMaintenanceTypes.Union(CBO_CIF_MaintenaceTypes).Union(UTTypes).OrderBy(a => a.OrderNo).ToList();
                /*
                 * CBOMaintenanceTypesGabungan = (from a in context.MaintenanceTypePriorities
                                               join b in context.MaintenanceTypes on a.CBOMaintainID equals b.CBOMaintainID
                                               join c in context.Products on a.ProductID equals c.ProductID
                                               join d in context.TransactionTypes on a.TransTypeID equals d.TransTypeID
                                               where a.IsDeleted == false && b.IsDeleted == false && c.IsDeleted == false && d.IsDeleted
                                               orderby new { a.OrderNo }
                                               select new CBOMaintenancePriorityOrderModel
                                               {
                                                   CBOMaintainPriority = a.CBOMaintainPriority,
                                                   CBOMaintainID = b.CBOMaintainID,
                                                   TransTypeID = d.TransTypeID,
                                                   ProductID = c.ProductID,
                                                   MaintenanceTypeName = !string.IsNullOrEmpty(b.MaintenanceTypeName) ? b.MaintenanceTypeName : (!string.IsNullOrEmpty(c.ProductName) ?  c.ProductName + " (UT)" : d.TransactionType1),
                                                   IsDeleted = a.IsDeleted,
                                                   OrderNo = a.OrderNo
                                               }).ToList();
                 */
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool AddCBOMaintenaceType(IList<CBOMaintenancePriorityOrderModel> CBOMaintenanceTypes, ref string message)
        {
            bool isSuccess = false;
            
            using (TransactionScope ts = new TransactionScope())
            {
                int iOrder = 1;
                var CboMaintenanceTypes = context.MaintenanceTypePriorities.ToList();
                if (CBOMaintenanceTypes != null)
                {
                    try
                    {                                     
                        int[] id = new int[CBOMaintenanceTypes.Count];
                        #region old code
                        /*if (CboMaintenanceTypes != null && CboMaintenanceTypes.Count > 0)
                        {
                            CboMaintenanceTypes.ForEach(a => 
                            {
                                a.IsDeleted = true;
                            });

                        }*/
                        /*int iOrder = 1;
                        foreach(var data in CBOMaintenanceTypes)
                        {

                            context.MaintenanceTypePriorities.Add(new Entity.MaintenanceTypePriority()
                            {
                                CBOMaintainPriority = data.CBOMaintainPriority,
                                CBOMaintainID = data.CBOMaintainID > 0 ? data.CBOMaintainID : (int?)null,
                                TransTypeID = data.TransTypeID > 0 ? data.TransTypeID : (int?)null,
                                ProductID = data.ProductID > 0 ? data.ProductID : (int?)null,
                                OrderNo = iOrder,
                                IsDeleted = false,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().DisplayName,
                                //UpdateDate = data.UpdateDate,
                                //UpdateBy = data.UpdateBy
                                
                            });

                            context.SaveChanges();
                            iOrder++;
                        }*/

                        /*Entity.MaintenanceTypePriority data = new Entity.MaintenanceTypePriority()
                        {
                            CBOMaintainID = CBOMaintenaceTypes.CBOMaintainID,
                            CBOMaintainPriority = CBOMaintenaceTypes.CBOMaintainPriority,
                            OrderNo = CBOMaintenaceTypes.OrderNo,
                            UpdateBy = CBOMaintenaceTypes.UpdateBy,
                            UpdateDate = CBOMaintenaceTypes.UpdateDate,
                            CreateBy = currentUser.GetCurrentUser().DisplayName,
                            CreateDate = DateTime.UtcNow
                        };
                        context.MaintenanceTypePriorities.Add(data);
                        context.SaveChanges();*/
                        #endregion
                        
                        if (CBOMaintenanceTypes.Count > 0)
                        {                            
                            foreach (var data in CBOMaintenanceTypes)
                            {
                                Entity.MaintenanceTypePriority prio = context.MaintenanceTypePriorities.Where(a => a.CBOMaintainPriority.Equals(data.CBOMaintainPriority)).Where(a=>a.IsDeleted.Equals(false)).SingleOrDefault();
                                id[iOrder - 1] = data.CBOMaintainPriority;
                                if (prio != null)
                                {
                                    prio.Ordered = true;
                                    prio.OrderNo = iOrder;
                                    context.SaveChanges();
                                    iOrder++;
                                }            
                            }
                            
                            var dt2 = context.MaintenanceTypePriorities.Where(a => !id.Contains(a.CBOMaintainPriority)).Where(a => a.IsDeleted.Equals(false)).ToList();
                            
                            if (dt2 != null)
                            {
                                if (dt2.Count > 0)
                                {
                                    foreach(var ab in dt2)
                                    {
                                        Entity.MaintenanceTypePriority prio = context.MaintenanceTypePriorities.Where(a => a.CBOMaintainPriority.Equals(ab.CBOMaintainPriority)).Where(a => a.IsDeleted.Equals(false)).SingleOrDefault();
                                        if (prio != null)
                                        {
                                            prio.Ordered = false;
                                            prio.OrderNo = iOrder;
                                            context.SaveChanges();
                                            iOrder++;
                                        }
                                    }
                                }
                            }                   
                            
                            ts.Complete();
                            isSuccess = true;
                        }
                    }
                    
                    catch (Exception ex)
                    {
                        ts.Dispose();
                        message = ex.Message;
                    }
                }

                if (CBOMaintenanceTypes != null)
                {
                    if (CBOMaintenanceTypes.Count == 0)
                    {
                        try
                        {
                            if (CboMaintenanceTypes.Count != 0)
                            {
                                foreach (var data in CboMaintenanceTypes)
                                {
                                    Entity.MaintenanceTypePriority prio = context.MaintenanceTypePriorities.Where(a => a.CBOMaintainPriority == data.CBOMaintainPriority).Where(a => a.IsDeleted.Equals(false)).SingleOrDefault();
                                    if (prio != null)
                                    {
                                        prio.Ordered = false;
                                        prio.OrderNo = iOrder;
                                        context.SaveChanges();
                                        iOrder++;
                                    }                                    
                                }
                            }

                            ts.Complete();
                            isSuccess = true;
                        }

                        catch (Exception ex)
                        {
                            ts.Dispose();
                            message = ex.Message;
                        }
                    }                    
                }
            }
            
            return isSuccess;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                        context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

    #endregion
}