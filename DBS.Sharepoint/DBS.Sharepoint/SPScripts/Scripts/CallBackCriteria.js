/**
 * Created by Roman Bangkit S on 10/10/2014.
 */
var ViewModel = function () {
    var self = this;

    //Parameter
    self.ThresholdAmount = ko.observable();
    self.ThresholdAmountApproval = ko.observable();
    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);
    self.IsNewData = ko.observable(false);
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");

    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    var SingleValueParameterData = {
        ID: 0,
        Name: "CALLBACK_AMOUNT_TRESHOLD",
        Value: self.ThresholdAmount,
        LastModifiedDate: new Date(),
        LastModifiedBy: "Unknown"
    }

    //Body
    self.SingleValueParameter = ko.observable(new SingleValueParameter(SingleValueParameterData));

    //Get ThresholdAmount
    self.GetData = function () { GetData(); }

    function GetData() {
        var options = {
            url: api.server + api.url.callbackcriteriaamount,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetThresholdAmount, OnError, OnAlways);
    }
    self.GetSelectedRow = function (data) {
        self.IsNewData(false);

        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');

        self.ThresholdAmountApproval(data.Value);
        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);
    };
    function OnSuccessGetThresholdAmount(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            self.ThresholdAmount(data);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    //
    self.UpdateThresholdAmount = function () { UpdateThresholdAmount(); }

    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function UpdateThresholdAmount() {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {

                    var options = {
                        url: api.server + api.url.singlevalueparameter + "/" + self.SingleValueParameter().Name(),
                        data: ko.toJSON(self.SingleValueParameter()),
                        params: {
                        },
                        token: accessToken
                    };

                    Helper.Ajax.Put(options, OnSuccessUpdateThresholdAmount, OnError, OnAlways);
                    GetData();
                }
            });
        }
    }

    function OnSuccessUpdateThresholdAmount(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-success', false);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });
}