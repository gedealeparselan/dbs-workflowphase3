
var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;
    var CurrencyBankModel = function (id, code, description) {
        var self = this;
        self.ID = ko.observable(id);
        self.Code = ko.observable(code);
        self.Description = ko.observable(description);
    }

    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);
    self.BranchCode = ko.observable("");
    self.Currencies = ko.observable(new CurrencyBankModel('', '', ''));
    self.Currency = ko.observable("");

    self.IsCheckOTT = ko.observable(false);


    //Declare observable which will be bind with UI
    self.ID = ko.observable("");
    self.Code = ko.observable("");
    self.Description = ko.observable("");
    self.BankAccount = ko.observable("");
    self.CommonName = ko.observable("");
    self.PGSL = ko.observable("");
    self.SwiftCode = ko.observable("");
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");
    self.IsRTGS = ko.observable(false);
    self.IsSKN = ko.observable(false);
    self.IsOTT = ko.observable(false);
    self.IsOVB = ko.observable(false);

    //Declare observable which will be bind with UI for Branch Bank Adi 28-sept-2016
    self.Branch_BranchCode = ko.observable("");
    self.Branch_BankAccount = ko.observable("");
    self.Branch_CommonName = ko.observable("");
    self.Branch_PGSL = ko.observable("");
    self.City = ko.observable("");
    self.CityCode = ko.observable("");
    self.CityID = ko.observable("");
    self.Branch_LastModifiedBy = ko.observable("");
    self.Branch_LastModifiedDate = ko.observable("");

    //Dropdownlist
    self.ddlCurrency = ko.observableArray([]);
    self.ddlBranch = ko.observableArray([]);

    // filter
    self.FilterCode = ko.observable("");
    self.FilterDescription = ko.observable("");
    self.FilterBankAccount = ko.observable("");
    self.FilterBranchCode = ko.observable("");
    self.FilterCommonName = ko.observable("");
    self.FilterCurrency = ko.observable("");
    self.FilterPGSL = ko.observable("");
    self.FilterSwiftCode = ko.observable("");
    self.FilterIsRTGS = ko.observable("");
    self.FilterIsSKN = ko.observable("");
    self.FilterIsOTT = ko.observable("");
    self.FilterIsOVB = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");

    //filter branch bank adi 28-sept-2016
    self.FilterBranch_BranchCode = ko.observable("");
    self.FilterBranch_BankAccount = ko.observable("");
    self.FilterBranch_CommonName = ko.observable("");
    //self.FilterBranchBankCurrency = ko.observable("");
    self.FilterBranch_PGSL = ko.observable("");
    self.FilterCity = ko.observable("");

    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    // New Data flag
    self.IsNewData = ko.observable(false);
    self.IsNewDataBranchBank = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.Banks = ko.observableArray([]);
    self.BranchBanks = ko.observableArray([]);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    //grid branch bank
    self.GridBranchBankProperties = ko.observable();
    self.GridBranchBankProperties(new GridPropertiesModel(GetDataBranchBank));

    // set default sorting
    self.GridProperties().SortColumn("Code");
    self.GridProperties().SortOrder("ASC");

    //set default sorting for branch bank
    self.GridBranchBankProperties().SortColumn("BranchCode");
    self.GridBranchBankProperties().SortOrder("ASC");

    // bind clear filters
    self.ClearFilters = function () {

        self.FilterCode("");
        self.FilterDescription("");
        self.FilterBankAccount("");
        self.FilterBranchCode("");
        self.FilterCommonName("");
        self.FilterCurrency("");
        self.FilterPGSL("");
        self.FilterSwiftCode("");
        self.FilterIsRTGS("");
        self.FilterIsSKN("");
        self.FilterIsOTT("");
        self.FilterIsOVB("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");

        GetData();
    };

    //bind clear filter for branchbank
    self.ClearFilterBranchBank = function () {
        self.FilterBranch_BranchCode("");
        self.FilterBranch_BankAccount("");
        self.FilterBranch_PGSL("");
        self.FilterBranch_CommonName("");
        self.FilterCityCode("");
        self.FilterCurrency("");
        GetDataBranchBank();
    };

    self.GetDropdown = function () {
        GetDropdown();
    };

    // bind get data function to view
    self.GetData = function () {
        GetData();
    };


    self.GetDataBranchBank = function () {
        GetDataBranchBank();
    }

    //GetDataBranchBank();
    //The Object which stored data entered in the observables
    var Bank = {
        ID: self.ID,
        Code: self.Code,
        Description: self.Description,
        BankAccount: self.BankAccount,
        BranchCode: self.BranchCode,
        CommonName: self.CommonName,
        Currency: self.Currency,
        PGSL: self.PGSL,
        SwiftCode: self.SwiftCode,
        IsRTGS: self.IsRTGS,
        IsSKN: self.IsSKN,
        IsOTT: self.IsOTT,
        IsOVB: self.IsOVB,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
    };

    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    console.log("spUser Roles : " + ko.toJSON(spUser.Roles));
                    console.log("spUser Permited : " + data);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    self.save = function () {
        // validation
        var form = $("#aspnetForm");
        //var CurrencySave = $('#currencyhidden option:selected').val();
        form.validate();

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {

                    //Bank.Currency(CurrencySave);
                    //console.log(ko.toJSON(self.BranchCode));
                    //console.log(ko.toJSON(Bank));        
                    //Ajax call to insert the Banks            
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.bank,
                        data: ko.toJSON(Bank), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.update = function () {
        // validation
        var form = $("#aspnetForm");
        //var CurrencyUpdate = $('#currencyhidden option:selected').val();

        form.validate();

        if (form.valid()) {

            //Bank.Currency(CurrencyUpdate);
            //console.log(ko.toJSON(Bank));
            // hide current popup window

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.bank + "/" + Bank.ID(),
                        data: ko.toJSON(Bank),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.delete = function () {

        var CurrencyDelete = $('#currencyhidden option:selected').val();

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
                Bank.Currency(CurrencyDelete);
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.bank + "/" + Bank.ID(),
                    data: ko.toJSON(Bank),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };
    self.OnChangeCurrency = function () {
        // var indexOption = $('#currency option:selected').index();
        // $('#currencyhidden option').eq(indexOption).prop('selected', true);
        var currencyItem = ko.utils.arrayFirst(self.ddlCurrency(), function (item) {
            return item.Code == self.Currency();
        });
        if (currencyItem != null) {
            self.Currency(currencyItem.ID);
        }
    };

    $('#modal-forms').on('hidden.bs.modal', function () {

        console.log("tutup modal");
    });

    self.SetOTT = ko.computed(function () {
        if ((self.IsRTGS() || self.IsSKN()) && (self.IsOTT() || self.IsOVB())) {
            $("#code").data({ ruleRequired: true });
            self.IsCheckOTT(true);
        }
        if (self.IsSKN()) {
            $("#code").data({ ruleRequired: true });
            self.IsCheckOTT(true);
        }
        if (self.IsRTGS()) {
            $("#code").data({ ruleRequired: true });
            self.IsCheckOTT(true);
        }
        if ((!self.IsRTGS() && !self.IsSKN()) && self.IsOTT()) {
            $("#code").data({ ruleRequired: false });
            self.IsCheckOTT(false);

            valTest.resetForm();
        }
        if ((!self.IsRTGS() && !self.IsSKN()) && self.IsOVB()) {
            $("#code").data({ ruleRequired: false });
            self.IsCheckOTT(false);

            valTest.resetForm();

        }
        if ((!self.IsRTGS() && !self.IsSKN()) && (self.IsOVB() && self.IsOTT())) {
            $("#code").data({ ruleRequired: false });
            self.IsCheckOTT(false);

            valTest.resetForm();

        }
        if (!self.IsRTGS() && !self.IsSKN() && !self.IsOVB() && !self.IsOTT()) {

            $("#code").data({ ruleRequired: false });
            self.IsCheckOTT(false);


        }





    });

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        console.log("select");
        self.IsNewData(false);

        //if (self.IsNewData(false)) {
        //    self.TableBranch(true);
        //}

        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-forms").modal('show');

        self.ID(data.ID);
        self.Code(data.Code);
        self.Description(data.Description);
        self.BankAccount(data.BankAccount);
        self.BranchCode(data.BranchCode);
        self.CommonName(data.CommonName);
        //self.Currency(data.Currency);
        self.PGSL(data.PGSL);
        self.SwiftCode(data.SwiftCode);
        self.IsRTGS(data.IsRTGS);
        self.IsSKN(data.IsSKN);
        self.IsOTT(data.IsOTT);
        self.IsOVB(data.IsOVB);
        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);
        /* $('#currency').val(data.Currency);
         if (self.Readonly == true) {
             $('#currencyhidden').val(data.CurrencyID);
         } else {
             $('#currencyhidden').val(data.Currency);
         }*/
        if (typeof (data.Currency) == 'number') {
            var currencyItem = ko.utils.arrayFirst(self.ddlCurrency(), function (item) {
                return item.ID == data.Currency
            });
        }
        else if (typeof (data.Currency) == 'string') {
            var currencyItem = ko.utils.arrayFirst(self.ddlCurrency(), function (item) {
                return item.Code == data.Currency
            });
        }

        if (currencyItem != null) {
            self.Currency(currencyItem.ID);
        }

        GetDataBranchBank();
    };


    //insert new
    self.NewData = function () {

        // flag as new Data
        self.IsNewData(true);
        self.Readonly(false);
        // bind empty data
        self.ID(0);
        self.Code('');
        self.Description('');
        self.BankAccount('');
        self.BranchCode('');
        self.Currencies(new CurrencyBankModel('', '', ''));
        self.CommonName('');
        self.Currency('');
        self.PGSL('');
        self.SwiftCode('');
        self.IsRTGS(null);
        self.IsSKN(null);
        self.IsOTT(null);
        self.IsOVB(null);
    };


    //Function to Read All Customers
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.bank,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }

    //function to Get Data Branch Bank add By Adi 27 sept 2016
    function GetDataBranchBank() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.branchbank + "/Detail/" + self.ID(),
            params: {
                page: self.GridBranchBankProperties().Page(),
                size: self.GridBranchBankProperties().Size(),
                sort_column: self.GridBranchBankProperties().SortColumn(),
                sort_order: self.GridBranchBankProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumnBranchBank();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataBranchBank, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataBranchBank, OnError, OnAlways);
        }
    }

    //Function to validation dynamic field

    function IsvalidField() { /*
     $("[name^=days]").each(function () {
     $(this).rules('add', {
     required: true,
     maxlength: 2,
     number: true,
     messages: {
     required: "Please provide a valid Day.",
     maxlength: "Please provide maximum 2 number valid day value.",
     number: "Please provide number"
     }
     });
     }); */
        return true;
    }

    function GetDropdown() {
        $.ajax({
            async: false,
            type: "GET",
            url: api.server + api.url.parameter + '?select=Currency',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    //console.log(data['Currency']);					
                    self.ddlCurrency(data['Currency']);
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }

        });

    }

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterCode() != "") filters.push({ Field: 'Code', Value: self.FilterCode() });
        if (self.FilterDescription() != "") filters.push({ Field: 'Description', Value: self.FilterDescription() });
        if (self.FilterBankAccount() != "") filters.push({ Field: 'BankAccount', Value: self.FilterBankAccount() });
        if (self.FilterBranchCode() != "") filters.push({ Field: 'BranchCode', Value: self.FilterBranchCode() });
        if (self.FilterCommonName() != "") filters.push({ Field: 'CommonName', Value: self.FilterCommonName() });
        if (self.FilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.FilterCurrency() });
        if (self.FilterPGSL() != "") filters.push({ Field: 'PGSL', Value: self.FilterPGSL() });
        if (self.FilterSwiftCode() != "") filters.push({ Field: 'SwiftCode', Value: self.FilterSwiftCode() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

        return filters;
    };

    // Get filtered columns value
    function GetFilteredColumnBranchBank() {
        // define filter
        var filters = [];
        if (self.FilterBranch_BankAccount() != "") filters.push({ Field: 'BankAccount', Value: self.FilterBranch_BankAccount() });
        if (self.FilterBranch_BranchCode() != "") filters.push({ Field: 'BranchCode', Value: self.FilterBranch_BranchCode() });
        if (self.FilterBranch_CommonName() != "") filters.push({ Field: 'CommonName', Value: self.FilterBranch_CommonName() });
        //if (self.FilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.FilterCurrency() });
        if (self.FilterBranch_PGSL() != "") filters.push({ Field: 'PGSL', Value: self.FilterBranch_PGSL() });
        //if (self.FilterSwiftCode() != "") filters.push({ Field: 'CityCode', Value: self.FilterCityCode() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

        return filters;
    };

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        console.log('awal');
        if (jqXHR.status = 200) {
            self.Banks(data.Rows);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    //on success GetDataBranchBank callback
    function OnSuccessGetDataBranchBank(data, textStatus, jqXHR) {
        console.log('masuk');
        console.log(data);
        if (jqXHR.status = 200) {
            self.BranchBanks(data.Rows);
            console.log(self.BranchBanks());
            self.GridBranchBankProperties().Page(data['Page']);
            self.GridBranchBankProperties().Size(data['Size']);
            self.GridBranchBankProperties().Total(data['Total']);
            self.GridBranchBankProperties().TotalPages(Math.ceil(self.GridBranchBankProperties().Total() / self.GridBranchBankProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
    var valTest = $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });
};
