﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    //[Authorize]
    public class SingleValueParameterController : ApiController
    {
        private string message = string.Empty;

        private readonly ISingleValueParameterRepository repository = null;

        public SingleValueParameterController()
        {
            this.repository = new SingleValueParameterRepository();
        }

        public SingleValueParameterController(ISingleValueParameterRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all Single Value Parameter.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<SingleValueParameter>))]
        public HttpResponseMessage Get()
        {
            IList<SingleValueParameter> output = new List<SingleValueParameter>();

            if (!repository.GetSingleValueParameter(ref output, 10, 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<SingleValueParameter>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Single Value parameter with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<SingleValueParameter>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<SingleValueParameter> output = new List<SingleValueParameter>();

            if (!repository.GetSingleValueParameter(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<SingleValueParameter>>(HttpStatusCode.OK, output);
            }
        }



        /// <summary>
        /// Find Single Value parameter using criterias.
        /// </summary>
        /// <param name="query">parameter or value.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<SingleValueParameter>))]
        [HttpGet]
        [Route("api/SingleValueParameter/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<SingleValueParameter> output = new List<SingleValueParameter>();

            if (!repository.GetSingleValueParameter(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<SingleValueParameter>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Single value Parameter by Name.
        /// </summary>
        /// <param name="name">Parameter Name.</param>
        /// <returns></returns>
        [ResponseType(typeof(string))]
        [Route("api/SingleValueParameter/{name}")]
        [HttpGet]
        public HttpResponseMessage Get(string name)
        {
            // SingleValueParameter output = new SingleValueParameter();
            string output = string.Empty;
            if (!repository.GetSingleValueParameterByName(name, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    // return Request.CreateResponse<SingleValueParameter>(HttpStatusCode.OK, output);
                    return Request.CreateErrorResponse(HttpStatusCode.OK, output);
            }
        }
        #region Get by Name Parameter
        /// <summary>
        /// Get Single value Parameter by UTC_ATTEMPS.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(int))]
        [Route("api/SingleValueParameter/UTC_ATTEMPS")]
        [HttpGet]
        public HttpResponseMessage GetUTC_ATTEMPS()
        {
            string output = string.Empty;
            string name = "UTC_ATTEMPS";

            if (!repository.GetSingleValueParameterByName(name, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse(HttpStatusCode.OK, Convert.ToInt32(output));
            }
        }

        /// <summary>
        /// Get Single value Parameter by CALLBACK_AMOUNT_TRESHOLD.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(decimal))]
        [Route("api/SingleValueParameter/CALLBACK_AMOUNT_TRESHOLD")]
        [HttpGet]
        public HttpResponseMessage GetCALLBACK_AMOUNT_TRESHOLD()
        {
            //SingleValueParameter output = new SingleValueParameter();
            string output = string.Empty;
            string name = "CALLBACK_AMOUNT_TRESHOLD";
            if (!repository.GetSingleValueParameterByName(name, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse(HttpStatusCode.OK, Convert.ToDecimal(output));
            }
        }

        /// <summary>
        /// Get Single value Parameter by FX_TRANSACTION_TRESHOLD.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(decimal))]
        [Route("api/SingleValueParameter/FX_TRANSACTION_TRESHOLD")]
        [HttpGet]
        public HttpResponseMessage GetFX_TRANSACTION_TRESHOLD()
        {
            //SingleValueParameter output = new SingleValueParameter();
            string output = string.Empty;
            string name = "FX_TRANSACTION_TRESHOLD";

            if (!repository.GetSingleValueParameterByName(name, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse(HttpStatusCode.OK, Convert.ToDecimal(output));
            }
        }
        #region IPE
        /// <summary>
        /// Get Single value Parameter by FX_TRANSACTION_TRESHOLD.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(string))]
        [Route("api/SingleValueParameter/PAYMENT_MODE")]
        [HttpGet]
        public HttpResponseMessage GetPAYMENT_MODE()
        {
            //SingleValueParameter output = new SingleValueParameter();
            string output = string.Empty;
            string name = "PAYMENT_MODE";

            if (!repository.GetSingleValueParameterByName(name, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Single value Parameter by LLD Rounding amount.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(string))]
        [Route("api/SingleValueParameter/LLD_ROUNDING_AMOUNT")]
        [HttpGet]
        public HttpResponseMessage GetLLD_ROUNDING_AMOUNT() 
        {
            string output = string.Empty;
            string name = "LLD_ROUNDING_AMOUNT";

            if (!repository.GetSingleValueParameterByName(name, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }
        #endregion

        /// <summary>
        /// Get Single value Parameter by DAYS_OF_DOUBLE_TRANSACTION_CHECK.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(int))]
        [Route("api/SingleValueParameter/DAYS_OF_DOUBLE_TRANSACTION_CHECK")]
        [HttpGet]
        public HttpResponseMessage GetDAYS_OF_DOUBLE_TRANSACTION_CHECK()
        {
            // SingleValueParameter output = new SingleValueParameter();
            string output = string.Empty;
            string name = "DAYS_OF_DOUBLE_TRANSACTION_CHECK";

            if (!repository.GetSingleValueParameterByName(name, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse(HttpStatusCode.OK, Convert.ToInt32(output));
            }
        }

        /// <summary>
        /// Get Single value Parameter by UNDERLYING_DOCUMENT_EXPIRY.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(int))]
        [Route("api/SingleValueParameter/UNDERLYING_DOCUMENT_EXPIRY")]
        [HttpGet]
        public HttpResponseMessage GetUNDERLYING_DOCUMENT_EXPIRY()
        {
            // SingleValueParameter output = new SingleValueParameter();
            string output = string.Empty;
            string name = "UNDERLYING_DOCUMENT_EXPIRY";

            if (!repository.GetSingleValueParameterByName(name, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse(HttpStatusCode.OK, Convert.ToInt32(output));
            }
        }

        /// <summary>
        /// Get Single value Parameter by FX_TRANSACTION_TRESHOLD_DAYS_WITHIN.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(int))]
        [Route("api/SingleValueParameter/FX_TRANSACTION_TRESHOLD_DAYS_WITHIN")]
        [HttpGet]
        public HttpResponseMessage GetFX_TRANSACTION_TRESHOLD_DAYS_WITHIN()
        {
            // SingleValueParameter output = new SingleValueParameter();
            string output = string.Empty;
            string name = "FX_TRANSACTION_TRESHOLD_DAYS_WITHIN";

            if (!repository.GetSingleValueParameterByName(name, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse(HttpStatusCode.OK, Convert.ToInt32(output));
            }
        }


        /// <summary>
        /// Get Single value Parameter by SCHEDULLER_CIF_FROM_FINACLE.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(int))]
        [Route("api/SingleValueParameter/SCHEDULLER_CIF_FROM_FINACLE")]
        [HttpGet]
        public HttpResponseMessage GetSCHEDULLER_CIF_FROM_FINACLE()
        {
            //SingleValueParameter output = new SingleValueParameter();
            string output = string.Empty;
            string name = "SCHEDULLER_CIF_FROM_FINACLE";

            if (!repository.GetSingleValueParameterByName(name, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse(HttpStatusCode.OK, Convert.ToInt32(output));
            }
        }

        /// <summary>
        /// Get Single value Parameter by SCHEDULLER_BI_MIDRATE.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(decimal))]
        [Route("api/SingleValueParameter/SCHEDULLER_BI_MIDRATE")]
        [HttpGet]
        public HttpResponseMessage GetSCHEDULLER_BI_MIDRATE()
        {
            //SingleValueParameter output = new SingleValueParameter();
            string output = string.Empty;
            string name = "SCHEDULLER_BI_MIDRATE";

            if (!repository.GetSingleValueParameterByName(name, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse(HttpStatusCode.OK, Convert.ToDecimal(output));
            }
        }

        /// <summary>
        /// Get Single value Parameter by SCHEDULLER_TZ_REPORT_RECONCILE.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(int))]
        [Route("api/SingleValueParameter/SCHEDULLER_TZ_REPORT_RECONCILE")]
        [HttpGet]
        public HttpResponseMessage GetSCHEDULLER_TZ_REPORT_RECONCILE()
        {
            string output = string.Empty;
            string name = "SCHEDULLER_TZ_REPORT_RECONCILE";

            if (!repository.GetSingleValueParameterByName(name, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse(HttpStatusCode.OK, Convert.ToInt32(output));
            }
        }

        /// <summary>
        /// Get Single value Parameter by FX_TRANSACTION_TRESHOLD.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(decimal))]
        [Route("api/SingleValueParameter/FCY_IDR_TRANSACTION_TRESHOLD")]
        [HttpGet]
        public HttpResponseMessage GetFCY_IDR_TRANSACTION_TRESHOLD()
        {
            //SingleValueParameter output = new SingleValueParameter();
            string output = string.Empty;
            string name = "FCY_IDR_TRANSACTION_TRESHOLD";

            if (!repository.GetSingleValueParameterByName(name, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse(HttpStatusCode.OK, Convert.ToDecimal(output));
            }
        }
        /// <summary>
        /// Get Single value Parameter by DAYS_OF_DOUBLE_TRANSACTION_CHECK.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(string))]
        [Route("api/SingleValueParameter/SMS")]
        [HttpGet]
        public HttpResponseMessage GetSMS()
        {
            // SingleValueParameter output = new SingleValueParameter();
            string output = string.Empty;
            string name = "SMS";

            if (!repository.GetSingleValueParameterByName(name, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }
        #endregion

        /// <summary>
        /// Modify existing Single Value Parameter.
        /// </summary>
        /// <param name="name">Name of SingleValueParameter to be modify.</param>
        /// <param name="SingleValueParameter">Single ValueParameter data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/SingleValueParameter/{name}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(string name, SingleValueParameter SingleValueParameter)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateSingleValueParameter(name, SingleValueParameter, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = string.Format("Single Value Parameter data {0} has been updated.", name) });
                }
            }
        }

        /// <summary>
        /// Remove exisiting bene segment.
        /// </summary>
        /// <param name="id">ID of bene segment to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/SingleValueParameter/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteSingleValueParameter(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "SingleValueParameter data has been deleted." });
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}