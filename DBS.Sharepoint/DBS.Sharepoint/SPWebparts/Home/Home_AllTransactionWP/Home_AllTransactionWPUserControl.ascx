﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Home_AllTransactionWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.Home.Home_AllTransactionWP.Home_AllTransactionWPUserControl" %>


<h1 class="header smaller no-margin-top lighter dark">All Transactions</h1>

<!-- widget box start -->
<div id="widget-box" class="widget-box">
    <!-- widget header start -->
    <div class="widget-header widget-hea1der-small header-color-dark">
        <h6>All Transactions Tasks</h6>

        <div class="widget-toolbar">
            <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                <i class="blue icon-filter"></i>
                <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridProperties().AllowFilter" />
                <span class="lbl"></span>
            </label>
            <a href="#" data-bind="click: GetData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                <i class="blue icon-refresh"></i>
            </a>
        </div>
    </div>
    <!-- widget header end -->
    <!-- widget body start -->
    <div class="widget-body">
        <!-- widget main start -->
        <div class="widget-main padding-0">
            <!-- widget slim control start -->
            <div class="slim-scroll" data-height="400">
                <!-- widget content start -->
                <div class="content">

                    <!-- table responsive start -->
                    <div class="table-responsive" id="form-menu-custom" form="AllTransaction">
                        <div class="dataTables_wrapper" role="grid">
                            <table id="workflow-task-table" class="table table-striped table-bordered table-hover dataTable"
                                   workflow-state="running, error"
                                   workflow-outcome="pending, custom"
                                   workflow-custom-outcome=""
                                   workflow-show-contribute="true">
                                <thead>
                                    <tr data-bind="with: GridProperties">
                                        <th>No.</th>
                                        <th data-bind="click: function () { Sorting('ApplicationID') }, css: GetSortedColumn('ApplicationID')">Application ID</th>
                                        <th data-bind="click: function () { Sorting('Customer') }, css: GetSortedColumn('Customer')">Customer Name</th>
                                        <th data-bind="click: function () { Sorting('Product') }, css: GetSortedColumn('Product')">Product</th>
                                        <th data-bind="click: function () { Sorting('Currency') }, css: GetSortedColumn('Currency')">Currency</th>
                                        <th data-bind="click: function () { Sorting('Amount') }, css: GetSortedColumn('Amount')">Trxn Amount</th>
                                        <!--<th data-bind="click: function(){ Sorting('AmountUSD')}, css: GetSortedColumn('AmountUSD')">Eqv. USD</th>-->
                                        <th data-bind="click: function () { Sorting('AccountNumber') }, css: GetSortedColumn('AccountNumber')">Debit Acc Number</th>
                                        <th data-bind="click: function () { Sorting('IsFXTransaction') }, css: GetSortedColumn('IsFXTransaction')">FX Transaction</th>
                                        <th data-bind="click: function () { Sorting('IsTopUrgent') }, css: GetSortedColumn('IsTopUrgent')">Urgency</th>
                                        <th data-bind="click: function () { Sorting('ActivityTitle') }, css: GetSortedColumn('ActivityTitle')">Transaction Status</th>
                                        <th data-bind="click: function () { Sorting('Approver') }, css: GetSortedColumn('Approver')">Modified By</th>
                                        <th data-bind="click: function () { Sorting('EntryTime') }, css: GetSortedColumn('EntryTime')">Modified Date</th>


                                        <!--<th data-bind="click: function(){ Sorting('WorkflowName')}, css: GetSortedColumn('WorkflowName')">Name</th>
                                        <th data-bind="click: function () { Sorting('Initiator') }, css: GetSortedColumn('Initiator')" >Initiator</th>
                                        <th data-bind="click: function () { Sorting('StateDescription') }, css: GetSortedColumn('StateDescription')" >State</th>
                                        <th data-bind="click: function () { Sorting('StartTime') }, css: GetSortedColumn('StartTime')">Start</th>
                                        <th data-bind="click: function () { Sorting('Title') }, css: GetSortedColumn('Title')">Title</th>
                                        <th data-bind="click: function () { Sorting('ActivityTitle') }, css: GetSortedColumn('ActivityTitle')">Activity</th>
                                        <th data-bind="click: function () { Sorting('UserDisplayName') }, css: GetSortedColumn('UserDisplayName')">User</th>
                                        <th data-bind="click: function () { Sorting('EntryTime') }, css: GetSortedColumn('EntryTime')">Entry Time</th>
                                        <th data-bind="click: function () { Sorting('ExitTime') }, css: GetSortedColumn('ExitTime')">Exit Time</th>
                                        <th data-bind="click: function () { Sorting('OutcomeDescription') }, css: GetSortedColumn('OutcomeDescription')">Outcome</th>
                                        <th data-bind="click: function () { Sorting('CustomOutcomeDescription') }, css: GetSortedColumn('CustomOutcomeDescription')">Custom Outcome</th>
                                        <th data-bind="click: function () { Sorting('Comments') }, css: GetSortedColumn('Comments')">Comments</th>-->
                                    </tr>
                                </thead>
                                <thead data-bind="visible: GridProperties().AllowFilter" class="table-filter">
                                    <tr>
                                        <th class="clear-filter">
                                            <a href="#" data-bind="click: ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                <i class="green icon-trash"></i>
                                            </a>
                                        </th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterApplicationID, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCustomer, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterProduct, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCurrency, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterAmount, event: { change: GridProperties().Filter }" /></th>
                                        <!--<th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterAmountUSD, event: { change: GridProperties().Filter }" /></th>-->
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterDebitAccNumber, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterFXTransaction, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterTopUrgent, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterActivityTitle, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterUser, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterEntryTime, event: { change: GridProperties().Filter }" /></th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: Tasks">
                                    <tr data-bind="click: $root.GetSelectedRow, css: $root.SetColorStatus(WorkflowContext.StateID)" data-toggle="modal">
                                        <td><span data-bind="text: RowID"></span></td>
                                        <td style="white-space: nowrap"><span data-bind="text: Transaction.ApplicationID"></span></td>
                                        <td><span data-bind="text: Transaction.Customer"></span></td>
                                        <td><span data-bind="text: Transaction.Product"></span></td>
                                        <td><span data-bind="text: Transaction.Currency"></span></td>
                                        <td><span data-bind="text: formatNumber(Transaction.Amount)"></span></td>
                                        <!--<td><span data-bind="text: formatNumber(Transaction.AmountUSD)"></span></td>-->
                                        <td><span data-bind="text: Transaction.DebitAccNumber"></span></td>

                                        <td><span data-bind="text: $root.RenameStatus('FX Transaction', Transaction.IsFXTransaction)"></span></td>
                                        <td><span data-bind="text: $root.RenameStatus('Urgency', Transaction.IsTopUrgent)"></span></td>
                                        <td><span data-bind="text: CurrentTask.ActivityTitle"></span></td>
                                        <td><span data-bind="text: CurrentTask.User"></span></td>
                                        <td><span data-bind="text: $root.LocalDate(CurrentTask.EntryTime)"></span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- table responsive end -->
                </div>
                <!-- widget content end -->
            </div>
            <!-- widget slim control end -->
            <!-- widget footer start -->
            <div class="widget-toolbox padding-8 clearfix">
                <div class="row" data-bind="with: GridProperties">
                    <!-- pagination size start -->
                    <div class="col-sm-6">
                        <div class="dataTables_paginate paging_bootstrap pull-left">
                            Showing <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                            rows of <span data-bind="text: Total"></span>
                            entries
                        </div>
                    </div>
                    <!-- pagination size end -->
                    <!-- pagination page jump start -->
                    <div class="col-sm-3">
                        <div class="dataTables_paginate paging_bootstrap">
                            Page <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width:50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                            of <span data-bind="text: TotalPages"></span>
                        </div>
                    </div>
                    <!-- pagination page jump end -->
                    <!-- pagination navigation start -->
                    <div class="col-sm-3">
                        <div class="dataTables_paginate paging_bootstrap">
                            <ul class="pagination">
                                <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                        <i class="icon-double-angle-left"></i>
                                    </a>
                                </li>
                                <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                        <i class="icon-angle-left"></i>
                                    </a>
                                </li>
                                <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                        <i class="icon-angle-right"></i>
                                    </a>
                                </li>
                                <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                        <i class="icon-double-angle-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- pagination navigation end -->

                </div>
            </div>
            <!-- widget footer end -->

        </div>
        <!-- widget main end -->
    </div>
    <!-- widget body end -->
</div>
<!-- widget box end -->
<!-- modal form start -->
<div id="modal-form" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width:100%;">
        <div id="backDrop" class="absCustomBackDrop topCustomBackDrop darkCustomBackDrop hideCustomBackDrop fullHeightCustomBackDrop fullWidthCustomBackDrop centerCustomBackDrop"></div>
        <div class="modal-content">
            <!-- modal header start
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="blue bigger">
                    <span data-bind="text: $root.Title"></span>
                </h4>
            </div> -->
            <!--<div class="alert alert-danger" data-bind="visible: !$root.IsAuthorizedNintex()">
                &lt;!&ndash; <button type="button" class="close" data-dismiss="alert">
                    <i class="icon-remove"></i>
                </button> &ndash;&gt;
                <strong>
                    <i class="icon-remove"></i>
                    Unauthorized!
                </strong>
                <span data-bind="text: $root.MessageNintex"></span>
            </div>-->
            <!-- modal header end -->
            <!-- modal body start -->
            <div class="modal-body overflow-visible">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" style="margin-top: -10px;font-size:30px;margin-right: -8px;" data-bind:"$root.OnCloseApproval">×</button>
                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div class="tabbable">
                            <ul class="nav nav-tabs" id="myTab">
                                <li class="active">
                                    <a data-toggle="tab" href="#transaction">
                                        <i class="blue icon-credit-card bigger-110"></i>
                                        <span data-bind="text: ActivityTitle"></span>
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#history">
                                        <i class="blue icon-comments bigger-110"></i>
                                        History
                                        <!--<span class="badge badge-danger">4</span>-->
                                    </a>
                                </li>
                                <!--<li>
                                    <a data-toggle="tab" href="#context">
                                        <i class="blue icon-cogs bigger-110"></i>
                                        Context
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#ko-model">
                                        <i class="blue icon-cogs bigger-110"></i>
                                        Knockout Model
                                    </a>
                                </li>-->
                            </ul>

                            <div class="tab-content">
                                <div id="transaction" class="tab-pane in active">
                                    <div id="transaction-progress" class="col-sm-4 col-sm-offset-4" style="display: none">
                                        <h4 class="smaller blue">Please wait...</h4>
                                        <span class="space-4"></span>
                                        <div class="progress progress-small progress-striped active">
                                            <div class="progress-bar progress-bar-info" style="width:100%"></div>
                                        </div>
                                    </div>

                                    <div id="transaction-data" data-bind="template: { name: ApprovalTemplate(), data: ApprovalData() }"></div>
                                </div>

                                <div id="history" class="tab-pane">
                                    <div id="timeline-2" data-bind="with: ApprovalData()">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                                                <div class="timeline-container timeline-style2">
                                                    <!--<span class="timeline-label">
                                                        <b>Today</b>
                                                    </span>-->

                                                    <div data-bind="if: $root.IsTimelines">

                                                        <div class="timeline-items" data-bind="foreach: Timelines">
                                                            <div class="timeline-item clearfix">
                                                                <div class="timeline-info">
                                                                    <span class="timeline-date" data-bind="text: $root.LocalDate(Time)"></span>

                                                                    <i class="timeline-indicator btn btn-info no-hover"></i>
                                                                </div>

                                                                <div class="widget-box transparent">
                                                                    <div class="widget-body">
                                                                        <div class="widget-main no-padding">
                                                                            <span class="bigger-110">
                                                                                <span class="blue bolder" data-bind="text: DisplayName"></span>
                                                                                <!-- ko if: IsGroup -->
                                                                                (<span data-bind="text: UserOrGroup"></span>)
                                                                                <!-- /ko -->
                                                                            </span>

                                                                            <span class="bolder" data-bind="text: Outcome"></span>
                                                                            <span data-bind="text: Activity"></span>

                                                                            <!-- ko ifnot: ApproverID == 0 -->
                                                                            <br />
                                                                            <i class="icon-comments-alt bigger-110 blue"></i>
                                                                            <span data-bind="text: Comment"></span>
                                                                            <!-- /ko -->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="context" class="tab-pane">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Parameter</th>
                                                <th>Value</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Web ID</td>
                                                <td><span data-bind="text: SPWebID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Site ID</td>
                                                <td><span data-bind="text: SPSiteID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>List ID</td>
                                                <td><span data-bind="text: SPListID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>List Item ID</td>
                                                <td><span data-bind="text: SPListItemID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Task List ID</td>
                                                <td><span data-bind="text: SPTaskListID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Task List Item ID</td>
                                                <td><span data-bind="text: SPTaskListItemID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Initiator</td>
                                                <td><span data-bind="text: Initiator"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Workflow Instance ID</td>
                                                <td><span data-bind="text: WorkflowInstanceID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Workflow ID</td>
                                                <td><span data-bind="text: WorkflowID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Workflow Name</td>
                                                <td><span data-bind="text: WorkflowName"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Start Time</td>
                                                <td><span data-bind="text: LocalDate(StartTime())"></span></td>
                                            </tr>
                                            <tr>
                                                <td>State ID </td>
                                                <td><span data-bind="text: StateID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>State Description</td>
                                                <td><span data-bind="text: StateDescription"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Is Authorized</td>
                                                <td><span data-bind="text: IsAuthorized"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Task Type</td>
                                                <td><span data-bind="text: TaskTypeDescription"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Title</td>
                                                <td><span data-bind="text: Title"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Activity Title</td>
                                                <td><span data-bind="text: ActivityTitle"></span></td>
                                            </tr>
                                            <tr>
                                                <td>User</td>
                                                <td><span data-bind="text: User"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Is Sharepoint Group</td>
                                                <td><span data-bind="text: IsSPGroup"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Entry Time</td>
                                                <td><span data-bind="text: LocalDate(EntryTime())"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Exit Time</td>
                                                <td><span data-bind="text: LocalDate(ExitTime())"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Outcome ID</td>
                                                <td><span data-bind="text: OutcomeID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Outcome Description</td>
                                                <td><span data-bind="text: OutcomeDescription"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Custom Outcome ID</td>
                                                <td><span data-bind="text: CustomOutcomeID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Custom Outcom Description</td>
                                                <td><span data-bind="text: CustomOutcomeDescription"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Comments</td>
                                                <td><span data-bind="text: Comments"></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div id="ko-model" class="tab-pane">
                                    <pre data-bind="text: ko.toJSON($root.ApprovalData, null, 2)"></pre>
                                    <!--<pre data-bind="text: JSON.stringify(ko.toJS(ApprovalData), null, 2)"></pre>-->
                                </div>
                            </div>
                        </div>
                        <!-- modal body form end -->
                    </div>
                </div>
            </div>
            <!-- modal body end -->
            <!-- modal footer start -->
            <div class="modal-footer">
                <div class="row" data-bind="visible: $root.IsAuthorizedNintex()">
                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="workflow-comments">Comments</label>

                        <div class="col-xs-9 align-left">
                            <div class="clearfix">
                                <textarea class="col-lg-12" id="workflow-comments" name="workflow-comments" rows="4" data-bind="value: $root.Comments, enable: $root.IsPendingNintex()" placeholder="Type any comment here..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="space-8"></div>

                <!-- dynamic outcomes button start -->
                <!--<span data-bind="foreach: Outcomes, visible: $root.IsAuthorizedNintex()">
                    <button data-bind="click: $root.ApprovalProcess, enable: $root.IsPendingNintex()" class="btn btn-sm btn-primary">
                        <i class="icon-approve"></i>
                        <span data-bind="text: $root.ApprovalButton(Name)"></span>
                    </button>
                </span>-->
                <!-- dynamic outcomes button end -->

                <button class="btn btn-sm btn-success" data-dismiss="modal" data-bind="click: $root.OnCloseApproval">
                    <i class="icon-remove"></i>
                    Close
                </button>
            </div>
            <!-- modal footer end -->

        </div>
    </div>
</div>
<!-- modal form end -->
<!-- modal form child -->
<div id="modal-form-child" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="0" role="dialog">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">

            <!-- modal header start -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span data-bind="if: $root.IsNewData()">Create a new Customer Contact Parameter</span>
                    <span data-bind="if: !$root.IsNewData()">Modify Customer Contact Parameter</span>
                </h4>
            </div>
            <!-- modal header end -->
            <!-- modal body start -->
            <div class="modal-body overflow-visible">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div id="customer-form" class="form-horizontal" role="form">

                            <div class="form-group" data-bind="visible: false">
                                <label class="col-sm-3 control-label no-padding-right" for="id">
                                    ID
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="id" name="id" data-bind="value: $root.ID_c" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="Name">
                                    Contact Name
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="Name" name="Name" autocomplete="off" data-bind="value: $root.Name_c" class="col-xs-10 col-sm-5" data-rule-required="true" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="PhoneNumber">
                                    Phone Number
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="PhoneNumber" name="PhoneNumber" autocomplete="off" data-bind="value: $root.PhoneNumber_c" class="col-xs-10 col-sm-5" data-rule-required="true" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="DateOfBirth">
                                    Date of Birth
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix input-group col-sm-3 no-padding">
                                        <input class="form-control date-picker" type="text" data-date-format="dd-M-yyyy" id="DateOfBirth" name="DateOfBirth" data-bind="value: $root.DateOfBirth_c" />
                                        <span class="input-group-addon">
                                            <i class="icon-calendar bigger-110"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="IDNumber">
                                    IDNumber
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="IDNumber" name="IDNumber" autocomplete="off" data-bind="value: $root.IDNumber_c" class="col-xs-10 col-sm-5" data-rule-required="true" data-rule-number="true" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="poafunction">
                                    POA Function
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select class="col-sm-12" id="poa-function" name="poa-function" data-rule-required="true" data-rule-value="true"
                                                data-bind="options: $root.POAFunctions,
    optionsText: function (item) { if (item.Name != null) return item.Name + ' (' + item.Description + ')' },
    optionsValue: 'ID',
    optionsCaption: 'Please Select...',
    value: $root.Selected().POAFunction,
    event: {
        change: function () {
            var data = ko.utils.arrayFirst($root.POAFunctions(),
                        function (item) {
                            return item.ID == $root.Selected().POAFunction();
                        });
            if (data != undefined && data != null) { $root.POAFunction_c(data); }
        }
    }"></select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" data-bind="visible: $root.Selected().POAFunction() == 9">
                                <label class="col-sm-3 control-label no-padding-right" for="POAFunction">
                                    POA Function Other
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="functionOther" name="functionOther" autocomplete="off" data-bind="value: $root.POAFunctionOther_c" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="OccupationInID">
                                    OccupationIn ID
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="OccupationInID" name="OccupationInID" autocomplete="off" data-bind="value: $root.OccupationInID_c" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="PlaceofBirth">
                                    Place of Birth
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="PlaceOfBirth" name="PlaceOfBirth" autocomplete="off" data-bind="value: $root.PlaceOfBirth_c" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="EffectiveDate">
                                    Effective Date
                                </label>

                                <div class="col-sm-9">
                                    <div class="clearfix input-group col-sm-3 no-padding">
                                        <input class="form-control date-picker" type="text" data-date-format="dd-M-yyyy" id="EffectiveDate" name="EffectiveDate" data-bind="value: $root.EffectiveDate_c" />
                                        <span class="input-group-addon">
                                            <i class="icon-calendar bigger-110"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="CancellationDate">
                                    Cancellation Date
                                </label>

                                <div class="col-sm-9">
                                    <div class="clearfix input-group col-sm-3 no-padding">
                                        <input class="form-control date-picker" type="text" data-date-format="dd-M-yyyy" id="cancellationDate" name="cancellationDate" data-bind="value: $root.CancellationDate_c" />
                                        <span class="input-group-addon">
                                            <i class="icon-calendar bigger-110"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal body form end -->
                    </div>
                </div>
            </div>
            <!-- modal body end -->
            <!-- modal footer start -->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.save_c, visible: $root.IsNewData()">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm btn-primary" data-bind="click: $root.update_c, visible: !$root.IsNewData()">
                    <i class="icon-edit"></i>
                    Update
                </button>
                <button class="btn btn-sm btn-warning" data-bind="click: $root.delete_c, visible: !$root.IsNewData()">
                    <i class="icon-trash"></i>
                    Delete
                </button>
                <button class="btn btn-sm" data-bind="click: $root.Close">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
            <!-- modal footer end -->

        </div>
    </div>
</div>
<!-- modal form child end-->
<!-- modal form underlying -->
<div id="modal-form-Underlying" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width:100%">
        <div class="modal-content">
            <!-- modal header start Underlying Form -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span>View a Customer Underlying</span>
                </h4>
            </div>
            <!-- modal header end -->
            <!-- modal body start -->
            <div class="modal-body overflow-visible">
                <div id="modalUnderlyingtest">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->

                            <div id="customerUnderlyingform" class="form-horizontal" role="form">
                                <div class="space-2"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="StatementLetterID">
                                        Statement Letter
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="statementLetter" name="statementLetter" data-bind="value: $root.StatementLetter_u" class="col-xs-7" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="space-2"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="UnderlyingDocument">
                                        Underlying Document
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <!-- <select id="underlyingDocument" name="underlyingDocument" data-bind="value:UnderlyingDocument_u().ID, options: ddlUnderlyingDocument_u, optionsText: 'CodeName',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select> -->
                                            <input type="text" id="otherUnderlying" name="otherUnderlying" data-bind="value: $root.UnderlyingDocument_u" class="col-xs-7" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="space-2"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="DocumentType">
                                        Type of Document
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <!--<select id="documentType" name="documentType" data-bind="value:$root.DocumentType_u().ID, options: ddlDocumentType_u, optionsText: 'Name',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select> -->
                                            <input type="text" id="documentType" name="documentType" data-bind="value: $root.DocumentType_u" class="col-xs-7" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="space-2"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="Currency">
                                        Currency
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <!--  <select id="currency_u" name="currency_u" data-bind="value:$root.Currency_u().ID,disable:$root.StatementLetter_u().ID()==1, options:ddlCurrency_u, optionsText:'Code',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-2"></select>
                                            <select id="currency_u" name="currency_u" data-bind="value:$root.Currency_u().ID, options:ddlCurrency_u, optionsText:'Code',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-2"  ></select>-->
                                            <input type="text" id="currency_u" name="currency_u" data-bind="value: $root.Currency_u" class="col-xs-7" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="space-2"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="Amount">
                                        Invoice Amount
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="Amount_u" name="Amount_u" data-bind="value: formatNumber($root.Amount_u())" data-rule-required="true" data-rule-number="true" class="col-xs-4 align-right" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="space-2"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="rate">Rate</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" class="col-sm-4 align-right" name="rate_u" id="rate_u" disabled="disabled" data-rule-required="true" data-rule-number="true" data-bind="value: formatNumber(Rate_u())" />
                                        </div>
                                    </div>
                                </div>
                                <div class="space-2"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="eqv-usd">Eqv. USD</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" class="col-sm-4 align-right" disabled="disabled" name="eqv-usd_u" id="eqv-usd_u" data-rule-required="true" data-rule-number="true" data-bind="value: formatNumber(AmountUSD_u())" />
                                        </div>
                                    </div>
                                </div>
                                <div class="space-2"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="DateOfUnderlying">
                                        Date of Underlying
                                    </label>
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            <input class="form-control date-picker" type="text" data-date-format="dd-M-yyyy" id="book-underlying-date" name="book-underlying-date" data-bind="value: DateOfUnderlying_u" disabled="disabled" />
                                            <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                        </div>
                                    </div>
                                    <label class="col-sm-2 control-label no-padding-right" for="ExpiredDate">
                                        Expiry Date
                                    </label>
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            <input type="text" id="ExpiredDate" name="ExpiredDate" data-date-format="dd-M-yyyy" data-bind="value: ExpiredDate_u" class="form-control date-picker col-xs-6" data-rule-required="true" disabled="disabled" />
                                            <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-2"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="ReferenceNumber">
                                        Reference Number
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="ReferenceNumber" name="ReferenceNumber" data-bind="value: $root.ReferenceNumber_u" disabled="disabled" class="col-xs-9" />
                                        </div>
                                    </div>
                                </div>
                                <div class="space-2"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="SupplierName">
                                        Supplier Name
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="SupplierName" name="SupplierName" data-bind="value: $root.SupplierName_u" class="col-xs-8" data-rule-required="true" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="space-2"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="InvoiceNumber">
                                        Invoice Number
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="invoiceNumber" name="invoiceNumber" data-bind="value: $root.InvoiceNumber_u" class="col-xs-8" data-rule-required="true" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="space-2"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="AttachmentNo">
                                        Is This doc Purposed to replace the proforma doc
                                    </label>
                                    <div class="col-sm-9">
                                        <label>
                                            <input name="switch-field-1" id="IsProforma" data-bind="checked: IsProforma_u" class="ace ace-switch ace-switch-6" type="checkbox" disabled="disabled" />
                                            <span class="lbl"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="dataTables_wrapper" role="grid" data-bind="visible: $root.IsProforma_u()">
                                    <table class="table table-striped table-bordered table-hover dataTable">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Statement Letter</th>
                                                <th>Underlying Document</th>
                                                <th>Type of Doc</th>
                                                <th>Currency</th>
                                                <th>Amount</th>
                                                <th>Supplier Name</th>
                                                <th>Invoice Number</th>
                                                <th>Reference Number</th>
                                                <th>Date</th>
                                                <th>Expiry Date</th>
                                            </tr>
                                        </thead>
                                        <tbody data-bind="foreach: $root.ProformaDetails, visible: $root.ProformaDetails().length > 0">
                                            <tr data-toggle="modal">
                                                <td><span data-bind="text: $index() + 1"></span></td>
                                                <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                <td><span data-bind="text: DocumentType.Name"></span></td>
                                                <td><span data-bind="text: Currency.Code"></span></td>
                                                <td align="right"><span data-bind="text: $root.FormatNumber(Amount)"></span></td>
                                                <td><span data-bind="text: SupplierName"></span></td>
                                                <td><span data-bind="text: InvoiceNumber"></span></td>
                                                <td><span data-bind="text: ReferenceNumber"></span></td>
                                                <td><span data-bind="text: $root.LocalDate(DateOfUnderlying, true, false)"></span></td>
                                                <td><span data-bind="text: $root.LocalDate(ExpiredDate, true, false)"></span></td>
                                            </tr>
                                        </tbody>
                                        <tbody data-bind="visible: $root.ProformaDetails().length == 0">
                                            <tr>
                                                <td colspan="10" class="text-center">
                                                    No entries available.
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="space-2"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="IsBulkUnderlying">
                                        Is This Bulk Underlying
                                    </label>
                                    <div class="col-sm-9">
                                        <label>
                                            <input name="switch-field-1" id="IsBulkUnderlying" data-bind="checked: IsBulkUnderlying_u" class="ace ace-switch ace-switch-6" type="checkbox" disabled="disabled" />
                                            <span class="lbl"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="dataTables_wrapper" role="grid" data-bind="visible: $root.IsBulkUnderlying_u()">
                                    <table class="table table-striped table-bordered table-hover dataTable">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Statement Letter</th>
                                                <th>Underlying Document</th>
                                                <th>Type of Doc</th>
                                                <th>Currency</th>
                                                <th>Amount</th>
                                                <th>Supplier Name</th>
                                                <th>Invoice Number</th>
                                                <th>Reference Number</th>
                                                <th>Date</th>
                                                <th>Expiry Date</th>
                                            </tr>
                                        </thead>
                                        <tbody data-bind="foreach: $root.BulkDetails, visible: $root.BulkDetails().length > 0">
                                            <tr data-toggle="modal">
                                                <td><span data-bind="text: $index() + 1"></span></td>
                                                <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                <td><span data-bind="text: DocumentType.Name"></span></td>
                                                <td><span data-bind="text: Currency.Code"></span></td>
                                                <td align="right"><span data-bind="text: $root.FormatNumber(Amount)"></span></td>
                                                <td><span data-bind="text: SupplierName"></span></td>
                                                <td><span data-bind="text: InvoiceNumber"></span></td>
                                                <td><span data-bind="text: ReferenceNumber"></span></td>
                                                <td><span data-bind="text: $root.LocalDate(DateOfUnderlying, true, false)"></span></td>
                                                <td><span data-bind="text: $root.LocalDate(ExpiredDate, true, false)"></span></td>
                                            </tr>
                                        </tbody>
                                        <tbody data-bind="visible: $root.BulkDetails().length == 0">
                                            <tr>
                                                <td colspan="10" class="text-center">
                                                    No entries available.
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- modal body form end -->
                        </div>
                    </div>
                </div>
            </div>


            <!-- modal body end -->                            <!-- modal footer start -->
            <div class="modal-footer">
                <button class="btn btn-sm" data-bind="click: $root.CloseUnderlying" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Close
                </button>
            </div>
            <!-- modal footer end -->
        </div>
    </div>
</div>
<!-- modal form enderlying end -->
<!-- modal form callbacktime -->
<div id="modal-form-callbacktime" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="0" role="dialog">
    <div class="modal-dialog" style="width: 50%">
        <div class="modal-content">

            <!-- modal header start -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span>Call Back Time</span>
                </h4>
            </div>
            <!-- modal header end -->
            <!-- modal body start -->
            <div class="modal-body overflow-visible">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div id="customer-form" class="form-horizontal" role="form">

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="NameCallback">
                                    Contact Name
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="NameCallback" name="NameCallback" class="col-xs-10 col-sm-5" disabled="disabled" />
                                    </div>
                                </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="TimePickerCallBack">
                                    Call back time
                                </label>
                                <div class="col-sm-4">
                                    <div class="clearfix">
                                        <div class="input-group bootstrap-timepicker">
                                            <input type="text" id="TimePickerCallBack" class="form-control time-picker" data-bind="timepicker: true" data-rule-required="true" data-rule-value="true" disabled="disabled" />
                                            <span class="input-group-addon">
                                                <i class="icon-time bigger-110"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="IsUtc">
                                    Is UTC
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input id="IsUtc" class="ace ace-switch ace-switch-6" type="checkbox" />
                                        <span class="lbl"></span>
                                    </div>
                                </div>
                            </div>

                            <!--<div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">
                                    Remarks
                                </label>

                                <div class="col-sm-9">
                                    <textarea name="RemarksCallBack" id="remarks" class="col-sm-12" rows="5"></textarea>
                                </div>
                            </div>-->

                        </div>
                        <!-- modal body form end -->
                    </div>
                </div>
            </div>
            <!-- modal body end -->
            <!-- modal footer start -->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.SaveCallBackTime">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm" data-bind="click: $root.CloseCallBackTime">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
            <!-- modal footer end -->

        </div>
    </div>
</div>
<!-- modal form callbacktime end-->
<!-- modal form start Attach Document Underlying Form start -->
<div id="modal-form-Attach" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="0" role="dialog">
    <div class="modal-dialog" style="width:100%">
        <div class="modal-content">
            <!-- modal header start Attach FIle Form -->
            <div class="modal-header">
                <h5 class="blue bigger">
                    <span>Add Attachment File</span>
                </h5>
            </div>
            <!-- modal header end Attach file -->
            <!-- modal body start Attach File -->
            <div class="modal-body overflow-visible">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div id="customer-form" class="form-horizontal" role="form">
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="Name">
                                    Purpose of Doc
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="documentPurpose" name="documentPurpose" data-bind="value: $root.Selected().DocumentPurpose_a, options: ddlDocumentPurpose_a, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                    </div>
                                </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="description">
                                    Type of Doc
                                </label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="documentType" name="documentType" data-bind="value: $root.Selected().DocumentType_a, options: ddlDocumentType_a, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="document-path-upload">Document</label>

                                <div class="col-sm-6 ace-file-input">
                                    <div class="clearfix">
                                        <input type="file" id="document-path-upload" name="document-path-upload" data-bind="file: DocumentPath_a" class="col-xs-7" /><!---->
                                        <!--<span data-bind="text: DocumentPath"></span>-->
                                        <!--<input type="text" id="document-path-text" name="document-path-text" data-bind="value: FileName_a, visible: $root.IsEditableDocument" />-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal body form Attach File End -->
                        <!-- widget box Underlying Attach start -->
                        <div class="dataTables_wrapper" role="grid">
                            <table class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <!-- <th style="width:50px">Select</th> -->
                                        <th>No.</th>
                                        <th>Select</th>
                                        <th>Statement Letter</th>
                                        <th>Underlying Document</th>
                                        <th>Type of Doc</th>
                                        <th>Currency</th>
                                        <th>Amount</th>
                                        <th>Date</th>
                                        <th>Supplier Name</th>
                                        <th>Reference Number</th>
                                        <th>Expiry Date</th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: { data: $root.CustomerAttachUnderlyings, as: 'AttachData' }, visible: $root.CustomerAttachUnderlyings().length > 0">
                                    <tr data-toggle="modal">
                                        <td><span data-bind="text: $index() + 1"></span></td>
                                        <td align="center"><input type="checkbox" id="isSelected2" name="isSelect2" data-bind="checked: IsSelectedAttach, event: { change: function (data) { $root.onSelectionAttach($index(), $data) } }, disable: !($root.SelectingUnderlying())" /></td>
                                        <td><span data-bind="text: StatementLetter.Name"></span></td>
                                        <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                        <td><span data-bind="text: DocumentType.Name"></span></td>
                                        <td><span data-bind="text: Currency.Code"></span></td>
                                        <td><span data-bind="text: Amount"></span></td>
                                        <td><span data-bind="text: $root.LocalDate(DateOfUnderlying, true, false)"></span></td>
                                        <td><span data-bind="text: SupplierName"></span></td>
                                        <td><span data-bind="text: ReferenceNumber"></span></td>
                                        <td><span data-bind="text: $root.LocalDate(ExpiredDate, true, false)"></span></td>
                                    </tr>
                                </tbody>
                                <tbody data-bind="visible: $root.CustomerAttachUnderlyings().length == 0">
                                    <tr>
                                        <td colspan="11" class="text-center">
                                            No entries available.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- modal body end Attach File -->
            <!-- modal footer start Attach File-->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.save_a, disable: $root.IsUploading()">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.cancel_a">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
            <!-- modal footer end attach file -->
        </div>
    </div>
</div>
<!-- modal form end Attach Document Underlying Form end -->
<!-- modal form start Attach Document Underlying Form start -->
<div id="modal-form-Attach1" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="0" role="dialog">
    <div class="modal-dialog" style="width:80%">
        <div class="modal-content">
            <!-- modal header start Attach FIle Form -->
            <div class="modal-header">
                <h5 class="blue bigger">
                    <span>Add Attachment File</span>
                </h5>
            </div>
            <!-- modal header end Attach file -->
            <!-- modal body start Attach File -->
            <div class="modal-body overflow-visible">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div id="customer-form1" class="form-horizontal" role="form">
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="Name">
                                    Purpose of Doc
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="documentPurpose" name="documentPurpose" data-bind="value: $root.Selected().DocumentPurpose_a, options: ddlDocumentPurpose_a, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                    </div>
                                </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="description">
                                    Type of Doc
                                </label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="documentType" name="documentType" data-bind="value: $root.Selected().DocumentType_a, options: ddlDocumentType_a, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="document-path-upload1">Document</label>

                                <div class="col-sm-6 ace-file-input">
                                    <div class="clearfix">
                                        <input type="file" id="document-path-upload1" name="document-path-upload1" data-bind="file: DocumentPath_a" class="col-xs-7" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal body form Attach File End -->

                    </div>
                </div>
            </div>
            <!-- modal body end Attach File -->
            <!-- modal footer start Attach File-->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.save_a, disable: $root.IsUploading()">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.cancel_a">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
            <!-- modal footer end attach file -->
        </div>
    </div>
</div>
<!-- modal form end Attach Document Underlying Form end -->
<!-- Modal upload form start -->
<div id="modal-form-upload" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="0" role="dialog">
    <div class="modal-dialog" style="width:100%">
        <div class="modal-content">
            <!-- modal header start Attach FIle Form -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span>Added Attachment File</span>
                </h4>
            </div>
            <!-- modal header end Attach file -->
            <!-- modal body start Attach File -->
            <div class="modal-body overflow-visible">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="document-type">Document Type</label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <select id="document-type" name="document-type" data-rule-required="true" data-rule-value="true"
                                            data-bind="options: $root.DocumentTypes,
    optionsText: function (item) { if (item.ID != null) return item.Name + ' (' + item.Description + ')' },
    optionsValue: 'ID',
    optionsCaption: 'Please Select...',
    value: $root.Selected().DocumentType "></select>
                                </div>
                            </div>
                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="document-purpose">Purpose of Docs</label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <select id="document-purpose" name="document-purpose" data-rule-required="true" data-rule-value="true"
                                            data-bind="options: $root.DocumentPurposes,
    optionsText: function (item) { if (item.ID != null) return item.Name + ' (' + item.Description + ')' },
    optionsValue: 'ID',
    optionsCaption: 'Please Select...',
    value: $root.Selected().DocumentPurpose "></select>
                                </div>
                            </div>
                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="document-path">Document</label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="file" id="document-path" name="document-path" data-bind="file: DocumentPath" data-rule-required="true" data-rule-value="true" /><!---->
                                    <!--<span data-bind="text: DocumentPath"></span>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <button class="btn btn-sm btn-primary" data-bind="click: $root.AddDocument">
                <i class="icon-save"></i>
                Save
            </button>
            <button class="btn btn-sm" data-bind="click: $root.Close">
                <i class="icon-remove"></i>
                Cancel
            </button>
        </div>
    </div>
</div>
<!-- Modal upload form end -->
<!-- approval templates start -->
<!-- All transaction Normal -->
<script type="text/html" id="TransactionAll" src="/Pages/Approval/TransactionAll.html">
</script>
<!-- PPU Maker templates end -->
<!-- PPU Maker templates start -->
<script type="text/html" id="TaskPPUMaker" src="/Pages/Approval/TransactionMaker.html">
</script>
<!-- PPU Maker templates end -->
<!-- PPU Maker After Checker and Caller templates start -->
<script type="text/html" id="TaskPPUMakerAfterCheckerandCaller" src="/Pages/Approval/TransactionMakerAfterCheckerandCaller.html">
</script>
<!-- PPU Maker After Checker and Caller templates end -->
<!-- PPU Checker templates start -->
<script type="text/html" id="TaskPPUChecker" src="/Pages/Approval/TransactionChecker.html">
</script>
<!-- PPU Checker templates end -->
<!-- PPU BranchMakerReactivation templates start -->
<script type="text/html" id="TaskBranchMakerReactivation" src="/Pages/Approval/TransactionBranchMakerReactivation.html">
</script>
<!-- PPU BranchMakerReactivation templates end -->
<!-- PPU TaskBranchCheckerReactivation templates start -->
<script type="text/html" id="TaskBranchCheckerReactivation" src="/Pages/Approval/TransactionBranchCheckerReactivation.html">
</script>
<!-- PPU TaskBranchCheckerReactivation templates end -->
<!-- PPU TaskCBOMakerReactivation templates start -->
<script type="text/html" id="TaskCBOMakerReactivation" src="/Pages/Approval/TransactionCBOMakerReactivation.html">
</script>
<!-- PPU TaskCBOMakerReactivation templates end -->
<!-- PPU TaskCBOCheckerReactivation templates start -->
<script type="text/html" id="TaskCBOCheckerReactivation" src="/Pages/Approval/TransactionCBOCheckerReactivation.html">
</script>
<!-- PPU TaskCBOCheckerReactivation templates end -->
<!-- PPU Checker After PPU Caller templates start -->
<script type="text/html" id="TaskPPUCheckerAfterPPUCaller" src="/Pages/Approval/TransactionCheckerAfterCaller.html">
</script>
<!-- PPU Checker After PPU Caller templates end -->
<!-- PPU Caller templates start -->
<script type="text/html" id="TaskPPUCaller" src="/Pages/Approval/TransactionCaller.html">
</script>
<!-- PPU Caller templates end -->
<!-- PPU Checker Canceled templates start -->
<script type="text/html" id="TaskPPUCheckerCanceled" src="/Pages/Approval/TransactionCheckerCanceled.html">
</script>
<!-- PPU Checker Canceled templates end -->
<!-- CBO Maker templates start -->
<script type="text/html" id="TaskCBOMaker" src="/Pages/Approval/TransactionCBOMaker.html">
</script>
<!-- CBO Maker templates end -->
<!-- CBO Checker templates start -->
<script type="text/html" id="TaskCBOChecker" src="/Pages/Approval/TransactionCBOChecker.html">
</script>
<!-- CBO Checker templates end -->
<!-- Payment Maker templates start -->
<script type="text/html" id="TaskPaymentMaker" src="/Pages/Approval/PaymentMaker.html">
</script>
<!-- Payment Maker templates end -->
<!-- Payment Checker templates start -->
<script type="text/html" id="TaskPaymentChecker" src="/Pages/Approval/PaymentChecker.html">
</script>
<!-- Payment Checker templates end -->
<!-- Lazy Approval templates start -->
<script type="text/html" id="TaskLazyApproval" src="/Pages/Approval/TransactionLazyApprovalExceptionHandling.html">
</script>
<!-- Lazy Approval templates end -->
<!-- DealChecker templates start -->
<script type="text/html" id="TaskFXDealChecker" src="/Pages/Approval/TransactionDealChecker.html">
</script>
<!-- DealChecker templates end -->
<!-- DealChecker templates start -->
<script type="text/html" id="TaskPendingDocumentsChecker" src="/Pages/Approval/TransactionPendingDocuments.html">
</script>
<!-- DealChecker templates end -->
<!-- DealChecker templates end -->
<!-- approval templates end -->
<!-- Timeline templates start -->
<script type="text/html" id="TransactionTimeline" src="/Pages/Approval/Timeline.html">
</script>
<!-- Timeline templates end -->
<!-- additional page scripts start -->
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/CalculatedFX.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/DataFormat.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/AllTransaction.js"></script>
<!-- additional page scripts end -->