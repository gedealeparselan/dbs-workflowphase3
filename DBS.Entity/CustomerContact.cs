//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class CustomerContact
    {
        public CustomerContact()
        {
            this.TransactionCallbackContacts = new HashSet<TransactionCallbackContact>();
            this.CustomerCallbacks = new HashSet<CustomerCallback>();
            this.CustomerContactFunctions = new HashSet<CustomerContactFunction>();
        }
    
        public long ContactID { get; set; }
        public string CIF { get; set; }
        public Nullable<int> POAFunctionID { get; set; }
        public string IdentificationNumber { get; set; }
        public string ContactName { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string PhoneNumber { get; set; }
        public string OccupationInId { get; set; }
        public string PlaceOfBirth { get; set; }
        public Nullable<System.DateTime> EffectiveDate { get; set; }
        public Nullable<System.DateTime> CancellationDate { get; set; }
        public string POAFunctionOther { get; set; }
        public int SourceID { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    
        public virtual Customer Customer { get; set; }
        public virtual POAFunction POAFunction { get; set; }
        public virtual Source Source { get; set; }
        public virtual ICollection<TransactionCallbackContact> TransactionCallbackContacts { get; set; }
        public virtual ICollection<CustomerCallback> CustomerCallbacks { get; set; }
        public virtual ICollection<CustomerContactFunction> CustomerContactFunctions { get; set; }
    }
}
