﻿var ViewModel = function () {
    var self = this;
    var DecodeLogin = function (username) {
        var index = username.lastIndexOf("|");
        var str = username.substring(index + 1);
        return str;
    };

    //vairable observable declaration
    self.Parameter = ko.observable({
        Recipients: ko.observableArray([]),
        TransactionTypes: ko.observableArray([]),
        DocumentTypes: ko.observableArray([]),
        DocumentPurposes: ko.observableArray([]),
    });
    self.Selected = ko.observable({
        Recipient: ko.observable(),
        TransactionType: ko.observable(),
        DocumentType: ko.observable(),
        DocumentPurpose: ko.observable(),
    });
    self.Documents = ko.observableArray([]);
    self.IsNewDocument = ko.observable(false);
    self.DocumentFlowData = ko.observable({
        TransactionDFID: ko.observable(),
        ApplicationID: ko.observable(),
        TransactionID: ko.observable(),
        Product: ko.observable("SFS Document Flow"),
        ProductID: ko.observable(),
        Client: ko.observable(),
        TransactionTypeID: ko.observable(),
        Others: ko.observable(),
        RefNumber: ko.observable(),
        DFRecipientID: ko.observable(),
        Sheets: ko.observable(),
        Remarks: ko.observable(),
        Documents: ko.observableArray([]),
        IsDraft: ko.observable(false),
        IsTopUrgent: ko.observable(false),
        IsUrgent: ko.observable(false),
        IsNormal: ko.observable(false),
        IsSignatureVerification: ko.observable(false)
    });       
    self.SPUser = ko.observable();
    self.CreateDate = ko.observable(new Date().toUTCString());
    self.BranchName = ko.observable();
    self.DocumentPath = ko.observable();
    self.DocumentType = ko.observable();
    self.DocumentPurpose = ko.observable();
    self.IsOthers = ko.observable(false);
    self.RetIDColl = ko.observableArray([]);
    self.IsEditable = ko.observable(true);
    self.ApplicationIDColl = ko.observableArray([]);
    self.IsLoadDraft = ko.observable(false);
    self.IsDraftForm = ko.observable(false);

    //non observable declaration
    self.secretToken = null;

    //get secret magic
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(self.OnSuccessToken, self.OnError);
    } else {
        // read token from cookie
        self.secretToken = $.cookie(api.cookie.name);

        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);
            self.SPUser(ko.mapping.toJS(spUser));
        }
    }

    //function for event click
    self.RemoveDocument = function (data) {
        //alert(JSON.stringify(data))
        self.Documents.remove(data);
    };
    self.UploadDocument = function () {
        self.DocumentPath(null);
        self.Selected().DocumentType(null);
        self.Selected().DocumentPurpose(null);
        self.DocumentType(null);
        $("#modal-form-upload").modal('show');
        $('.remove').click();
    };
    self.AddDocument = function () {
        //var purposeDoc = {
        //    ID: 1,
        //    Name: "Instruction",
        //    Description: "Instruction"
        //};
        //var file = $('#document-path').data().ace_input_files[0];
        var doc = {
            ID: 0,
            Type: self.DocumentType(),
            Purpose: self.DocumentPurpose(),
            FileName: self.DocumentPath().name.replace(/[<>:"\/\\|?!@#$%^&*]+/g, '_'),//file.name
            DocumentPath: self.DocumentPath(),
            LastModifiedDate: new Date(),
            LastModifiedBy: null
        };

        if (doc.Type == null || doc.DocumentPath == null) {
            alert("Please complete the upload form fields.")
        } else {
            self.Documents.push(doc);
            $('.remove').click();

            $("#modal-form-upload").modal('hide');
        }
    };
    self.Submit = function () {
        var form = $("#aspnetForm");
        form.validate();
        if (self.DocumentFlowData().IsTopUrgent() === false && self.DocumentFlowData().IsUrgent() === false && self.DocumentFlowData().IsNormal() === false) {
            ShowNotification("Form Validation Warning", "Please tick 'Top Urgent' or 'Urgent' or 'Normal' ", 'gritter-warning', false);
            return false;
        }
        if (self.DocumentFlowData().IsSignatureVerification() === false || self.DocumentFlowData().IsSignatureVerification() === undefined || self.DocumentFlowData().IsSignatureVerification() === '') {
            ShowNotification("Form Validation Warning", "Please tick 'Signature Verification'", 'gritter-warning', false);
            return false;
        }
        if (form.valid()) {
            self.IsEditable(false);
            self.DocumentFlowData().IsDraft(false);
            self.DocumentFlowData().TransactionTypeID(self.Selected().TransactionType());
            self.DocumentFlowData().DFRecipientID(self.Selected().Recipient());
            self.DocumentFlowData().ProductID(ConsProductID.DFProductIDCons);
            if (viewModel.Selected().TransactionType() !== ConsDocFlowTransType.Others) {
                self.DocumentFlowData().Others("");
            }           
            //console.log(self.DocumentFlowData());
            var data = {
                ApplicationID: self.DocumentFlowData().ApplicationID(),
                CIF: "DUMMYCIF",
                Name: "Dummy CIF"
            };
            if (self.Documents().length > 0) {
                self.UploadFileRecuresive(data, self.Documents(), self.SaveTransaction, self.Documents().length);
            } else {
                self.SaveTransaction();
            }
        }
        else {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        }
    };
    self.CloseTransaction = function () {
        //if (self.IsDraftForm() == true) {
        //    window.location = "/home/draft-transactions";
        //    return;
        //}
        window.location = "/home";
        return;
    };
    self.SaveAsDraft = function () {
        self.IsEditable(false);
        self.DocumentFlowData().IsDraft(true);
        self.DocumentFlowData().TransactionTypeID(self.Selected().TransactionType());
        self.DocumentFlowData().DFRecipientID(self.Selected().Recipient());
        self.DocumentFlowData().ProductID(ConsProductID.DFProductIDCons);
        if (viewModel.Selected().TransactionType() !== ConsDocFlowTransType.Others) {
            self.DocumentFlowData().Others("");
        }
        var data = {
            ApplicationID: self.DocumentFlowData().ApplicationID(),
            CIF: "DUMMYCIF",
            Name: "Dummy CIF"
        };
        if (self.Documents().length > 0) {
            self.UploadFileRecuresive(data, self.Documents(), self.SaveTransaction, self.Documents().length);
        } else {
            self.SaveTransaction();
        }
    };
    self.OnChangeTransactionType = function () {
        var whatIsSelected = $("#typeOfTransaction option:selected").text();
        if (whatIsSelected.trim() === "Others") {
            self.IsOthers(true);
        } else {
            self.IsOthers(false);
        }
    };


    //function to get parameter
    self.SetAutoCompleteClientField = function () {
        $("#client").autocomplete({
            source: function (request, response) {
                var options = {
                    url: api.server + api.url.documentFlow.clientSearch,
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: self.secretToken
                };
                Helper.Ajax.AutoComplete(options, response, function (response, data, textStatus, jqXHR) {
                    response($.map(data, function (item) {
                        return {
                            // default autocomplete object
                            label: item.TransactionDFID + " - " + item.Client,
                            value: item.Client,

                            // custom object binding
                            dataCustom: {
                                Client: item.Client,
                                TransactionDFID: item.TransactionDFID,
                                TransactionID: item.TransactionID,
                                TransactionTypeID: item.TransactionTypeID
                            }
                        }
                    })
                    );
                }, self.OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set customer data model
                //console.log(ui.item.data);
                //console.log(ui.item.value);
                //console.log(ui.item.dataCustom.Client);
                if (ui.item.dataCustom !== undefined && ui.item.dataCustom !== null && ui.item.dataCustom !== '') {
                    if (ui.item.dataCustom.Client !== undefined && ui.item.dataCustom.Client !== null && ui.item.dataCustom.Client !== '') {
                        self.DocumentFlowData().Client(ui.item.dataCustom.Client);
                    }
                }
                //if (ui.item.data != undefined || ui.item.data != null) {
                //    var mapping = {
                //        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                //    };
                //    self.DocumentFlowData().Client(ui.item.data.Client);
                //}
            }
        });
    };
    self.GetRecipient = function () {
        var options = {
            url: api.server + api.url.documentFlow.getRecipient,
            token: self.secretToken
        };
        Helper.Ajax.Get(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                self.Parameter().Recipients(ko.mapping.toJS(data, mapping));
            }
        }, self.OnError, self.OnAlways);
    };
    self.GetTransactionType = function () {
        var options = {
            url: api.server + api.url.transactiontype,
            token: self.secretToken
        };
        Helper.Ajax.Get(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                if (data.length > 0) {
                    var docFlowType = ko.utils.arrayFilter(data, function (itemA) {
                        return itemA.ProductID == ConsProductID.DFProductIDCons;
                    });
                    if (docFlowType.length > 0) {
                        self.Parameter().TransactionTypes(ko.mapping.toJS(docFlowType, mapping));
                    }
                }                
            }
        }, self.OnError, self.OnAlways);
    };
    self.GetEmployeeLocation = function () {
        var LogName = DecodeLogin(spUser.LoginName);
        $.ajax({
            type: "GET",
            url: api.server + api.url.userapproval + "/Loc/" + LogName.trim(),
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON({
                ID: ko.observable(),
                Name: ko.observable(),
                Code: ko.observable()
            }),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + self.secretToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    if (data != null) {
                        //viewModel.IsHO(data["IsHO"]);
                        //viewModel.IsJakartaBranch(data["IsJakartaBranch"]);
                        //viewModel.IsUpcountryBranch(data["IsUpcountryBranch"]);
                        self.BranchName(data["Name"]);
                    }
                    else {
                        //viewModel.IsHO(true);
                        //viewModel.IsJakartaBranch(false);
                        //viewModel.IsUpcountryBranch(false);
                        self.BranchName(null);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    };
    self.GetParameters = function () {
        var options = {
            url: api.server + api.url.parameter,
            params: {
                select: "DocType,PurposeDoc"
            },
            token: self.secretToken
        };
        Helper.Ajax.Get(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                self.Parameter().DocumentTypes(ko.mapping.toJS(data.DocType, mapping));
                self.Parameter().DocumentPurposes(ko.mapping.toJS(data.PurposeDoc, mapping));
            }
        }, self.OnError, self.OnAlways);
    };


    //function helper
    //use together
    self.GetUserRole = function (userData) {
        var sValue;
        if (userData != undefined && userData.length > 0) {
            $.each(userData, function (index, item) {
                if (Const_RoleName[item.ID].toLowerCase().startsWith("dbs ppu") && Const_RoleName[item.ID].toLowerCase().endsWith("maker")) { //if (item.Name.toLowerCase().startsWith("dbs ppu") && item.Name.toLowerCase().endsWith("maker")) {
                    sValue = item.ID;
                    return sValue;
                }
            });

            //Untuk user non PPU tetapi bisa New Transaction
            $.each(userData, function (index, item) {
                if (Const_RoleName[item.ID].toLowerCase().endsWith("maker")) {
                    sValue = item.ID;
                    return sValue;
                }
            });
        }
        if (sValue == null) {
            sValue = userData[0].ID;
        }
        return sValue;
    };
    self.OnError = function (jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    };
    self.OnAlways = function () {
        if (viewModel != undefined) {
        }
    };
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);
        if (date == '1970/01/01 00:00:00' || date == null) {
            return "";
        }

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };
    self.IsRole = function (name) {
        var result = false;
        $.each($.cookie(api.cookie.spUser).Roles, function (index, item) {
            if (Const_RoleName[item.ID] == name) result = true;
        });
        return result;
    };
    self.AddListItem = function () {
        var body;
        var urlListTransaction = config.sharepoint.listDocumentFlow;
        var Listtitle = self.DocumentFlowData().ApplicationID() + " - " + "DUMMYCIF";
        var ListInitGroup = self.GetUserRole(self.SPUser().Roles);
        var ListTransactionID = self.DocumentFlowData().TransactionID();
        var ListAppID = self.DocumentFlowData().ApplicationID();
        var ListTipe = config.sharepoint.metadata.listDocumentFlow;

        body = {
            Title: Listtitle,
            Initiator_x0020_GroupId: ListInitGroup,
            Transaction_x0020_ID: ListTransactionID,
            Application_x0020_ID: ListAppID,
            __metadata: {
                type: ListTipe
            }
        };

        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval);

        var options = {
            url: config.sharepoint.url.api + "/Lists(guid'" + urlListTransaction + "')/Items",
            data: JSON.stringify(body),
            digest: jQuery("#__REQUESTDIGEST").val()
        };
        Helper.Sharepoint.List.Add(options, function (data, textStatus, jqXHR) {
            var AppIDColl = self.RetIDColl();
            var Cust = "DUMMYCIF";
            self.DocumentFlowData().ApplicationID(AppIDColl[0].ApplicationID);
            self.DocumentFlowData().TransactionID(AppIDColl[0].TransactionID);
            if (AppIDColl != null) {

                self.ApplicationIDColl([]);
                var appColl = {
                    TransactionID: AppIDColl[0].TransactionID,
                    ApplicationID: AppIDColl[0].ApplicationID,
                    Customer: Cust,
                }
                self.ApplicationIDColl.push(appColl);
                ShowNotification("Submit Transaction Success", "", "gritter-success", true);
                // $("#modal-form-applicationID").modal('show');
                window.location = "/document-flow";
            }
            else
                window.location = "/document-flow";
        }, self.OnError, self.OnAlways);
    };
    self.DeleteDraftTr = function (DraftID) {
        $.ajax({
            type: "DELETE",
            url: api.server + api.url.documentFlow.deleteDraftTransaction + "/" + DraftID,
            headers: {
                "Authorization": "Bearer " + self.secretToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    };
    self.OnSuccessToken = function (data, textStatus, jqXHR) {
        // store token on browser cookies
        $.cookie(api.cookie.name, data.AccessToken);
        self.secretToken = $.cookie(api.cookie.name);

        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);
            self.SPUser(ko.mapping.toJS(spUser));
        }

        // get parameter values
        //GetParameters();
        //PPUModel.token = accessToken;
        //GetEmployeeLocation();
    };    
    self.Urgent = function () {
        if (self.DocumentFlowData().IsUrgent() === false) {
            self.DocumentFlowData().IsTopUrgent(false);
            self.DocumentFlowData().IsNormal(false);
            self.DocumentFlowData().IsUrgent(false);
        } else {
            self.DocumentFlowData().IsTopUrgent(false);
            self.DocumentFlowData().IsNormal(false);
            self.DocumentFlowData().IsUrgent(true);
        }        
    }
    self.Normal = function () {
        if (self.DocumentFlowData().IsNormal() === false) {
            self.DocumentFlowData().IsTopUrgent(false);
            self.DocumentFlowData().IsNormal(false);
            self.DocumentFlowData().IsUrgent(false);
        } else {
            self.DocumentFlowData().IsTopUrgent(false);
            self.DocumentFlowData().IsUrgent(false);
            self.DocumentFlowData().IsNormal(true);
        }
    }
        
    self.TopUrgent = function () {
        if (self.DocumentFlowData().IsTopUrgent() === false) {
            self.DocumentFlowData().IsTopUrgent(false);
            self.DocumentFlowData().IsNormal(false);
            self.DocumentFlowData().IsUrgent(false);
        } else {
            self.DocumentFlowData().IsUrgent(false);
            self.DocumentFlowData().IsNormal(false);
            self.DocumentFlowData().IsTopUrgent(true);
        }        
    }
    //
    self.SaveTransaction = function () {
        if (self.DocumentFlowData().IsDraft()) {
            if (self.DocumentFlowData().Documents().length == self.Documents().length) {
                var options = {
                    url: api.server + api.url.documentFlow.submitTransDocumentFlow,
                    token: self.secretToken,
                    data: ko.toJSON(self.DocumentFlowData())
                };
                if (self.DocumentFlowData().TransactionID() == null) {//draft baru
                    Helper.Ajax.Post(options, function (dataB, textStatus, jqXHR) {
                        //console.log(dataB);
                        if (dataB.ID != null || dataB.ID != undefined) {
                            var AppID = {
                                TransactionID: dataB.ID,
                                ApplicationID: dataB.AppID
                            };
                            self.RetIDColl([]);
                            self.RetIDColl.push(AppID);
                            self.DocumentFlowData().TransactionID(dataB.ID);
                            self.DocumentFlowData().ApplicationID(dataB.AppID);
                            ShowNotification("Transaction Draft Success", "Transaction draft save attachments", "gritter-success", true);
                            window.location = "/home/draft-transactions";
                            return;
                        }
                    }, self.OnError, self.OnAlways);
                } else {//draft lama, yang pernah ada
                    Helper.Ajax.Post(options, function (dataC, textStatus, jqXHR) {
                        //console.log(dataC);
                        if (dataC.ID != null || dataC.ID != undefined) {
                            if (self.DocumentFlowData().TransactionID() != null) {
                                var DraftID = self.DocumentFlowData().TransactionID();
                                var AppID = {
                                    TransactionID: dataC.ID,
                                    ApplicationID: dataC.AppID
                                };
                                self.RetIDColl([]);
                                self.RetIDColl.push(AppID);
                                self.DocumentFlowData().TransactionID(dataC.ID);
                                self.DocumentFlowData().ApplicationID(dataC.AppID);
                                ShowNotification("Transaction Draft Success", "Transaction draft save attachments", "gritter-success", true);
                                window.location = "/home/draft-transactions";                                
                                return;
                            }
                        }
                    }, self.OnError, self.OnAlways);
                }
                return;
            }
        };

        if (self.DocumentFlowData().Documents().length == self.Documents().length) {
            var options = {
                url: api.server + api.url.documentFlow.submitTransDocumentFlow,
                token: self.secretToken,
                data: ko.toJSON(self.DocumentFlowData())
            };
            if (self.DocumentFlowData().TransactionID() == null) {//baru yang dulunya bukan draft
                Helper.Ajax.Post(options, function (dataB, textStatus, jqXHR) {
                    //console.log(dataB);
                    if (dataB.ID != null || dataB.ID != undefined) {
                        var AppID = {
                            TransactionID: dataB.ID,
                            ApplicationID: dataB.AppID
                        };
                        self.RetIDColl([]);
                        self.RetIDColl.push(AppID);
                        self.DocumentFlowData().TransactionID(dataB.ID);
                        self.DocumentFlowData().ApplicationID(dataB.AppID);
                        self.AddListItem();
                    }
                }, self.OnError, self.OnAlways);
            } else {//baru yang dulunya draft
                Helper.Ajax.Post(options, function (dataC, textStatus, jqXHR) {
                    //console.log(dataC);
                    if (dataC.ID != null || dataC.ID != undefined) {
                        if (self.DocumentFlowData().TransactionID() != null) {
                            var DraftID = self.DocumentFlowData().TransactionID();
                            var AppID = {
                                TransactionID: dataC.ID,
                                ApplicationID: dataC.AppID
                            };
                            self.RetIDColl([]);
                            self.RetIDColl.push(AppID);
                            self.DeleteDraftTr(self.DocumentFlowData().TransactionID());
                            self.DocumentFlowData().TransactionID(dataC.ID);
                            self.DocumentFlowData().ApplicationID(dataC.AppID);
                            self.AddListItem();
                        }
                    }
                }, self.OnError, self.OnAlways);
            }
        };
    };
    self.UploadFileRecuresive = function (context, document, callBack, numFile) {
        var indexDocument = document.length - numFile;
        var IsDraft = self.DocumentFlowData().IsDraft();
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth();
        if (month != '10' || month != '11' || month != '12') {
            month = '0' + month;
        }

        var serverRelativeUrlToFolder = '';
        if (IsDraft == true)
            serverRelativeUrlToFolder = '/DocumentFlowDraftLibrary/' + year + "-" + month;
        else {
            serverRelativeUrlToFolder = '/DocumentFlowLibrary/'+year+"-"+month;
        }

        var parts = document[indexDocument].DocumentPath.name.split('.');
        var fileExtension = parts[parts.length - 1];
        var timeStamp = new Date().getTime();
        var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

        // Get the server URL.
        var serverUrl = _spPageContextInfo.webAbsoluteUrl;

        // output variable
        var output;

        // Initiate method calls using jQuery promises.
        // Get the local file as an array buffer.
        var getFile = getFileBuffer();
        getFile.done(function (arrayBuffer) {
            // Add the file to the SharePoint folder.
            var addFile = addFileToFolder(arrayBuffer);
            addFile.done(function (file, status, xhr) {
                output = file.d;

                // Get the list item that corresponds to the uploaded file.
                var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
                getItem.done(function (listItem, status, xhr) {
                    // Change the display name and title of the list item.
                    var changeItem = updateListItem(listItem.d.__metadata);
                    changeItem.done(function (data, status, xhr) {
                        //alert('file uploaded and updated');
                        //return output;
                        var newDoc = {
                            ID: 0,
                            Type: document[indexDocument].Type,
                            Purpose: document[indexDocument].Purpose,
                            LastModifiedDate: null,
                            LastModifiedBy: null,
                            DocumentPath: {
                                DocPath: output.ServerRelativeUrl,
                                name: document[indexDocument].DocumentPath.name,
                                //type: null,
                                //lastModifiedDate: null
                            },
                            FileName: document[indexDocument].DocumentPath.name
                        };

                        self.DocumentFlowData().Documents.push(newDoc);

                        if (numFile > 1) {
                            self.UploadFileRecuresive(context, document, callBack, numFile - 1); // recursive function
                        }
                        callBack();
                    });
                    changeItem.fail(self.OnError);
                });
                getItem.fail(self.OnError);
            });
            addFile.fail(self.OnError);
        });
        getFile.fail(self.OnError);

        // Get the local file as an array buffer.
        function getFileBuffer() {
            var deferred = $.Deferred();
            var reader = new FileReader();
            reader.onloadend = function (e) {
                deferred.resolve(e.target.result);
            }
            reader.onerror = function (e) {
                deferred.reject(e.target.error);
            }

            if (document[indexDocument].DocumentPath.type == Util.spitemdoc) {
                var fileURI = serverUrl + document[indexDocument].DocumentPath.DocPath;
                // load file:
                fetch(fileURI, convert, alert);

                function convert(buffer) {
                    var blob; // 
                    if (fileExtension == "txt") {
                        blob = new Blob([buffer], { type: "text/plain" });
                    } else if (fileExtension == "xlsx") {
                        blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                    } else if (fileExtension == "docx") {
                        blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.wordprocessingml.document"});
                    } else if (fileExtension == "doc") {
                        blob = new Blob([buffer], { type: "application/msword"});
                    } else if (fileExtension == "xls") {
                        blob = new Blob([buffer], { type: "application/vnd.ms-excel"});
                    } else if (fileExtension == "pdf") {
                        blob = new Blob([buffer], {type: "application/pdf"});
                    } else if (fileExtension == "zip" ) {
                        blob = new Blob([buffer], { type: "application/zip, application/x-compressed-zip" });
                    } else if (fileExtension == "bmp") {
                        blob = new Blob([buffer], { type: "image/bmp" });
                    } else if (fileExtension == "dotx") {
                        blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.wordprocessingml.template" });
                    } else if (fileExtension == "jpg") {
                        blob = new Blob([buffer], { type: "image/jpeg" });
                    } else if (fileExtension == "png") {
                        blob = new Blob([buffer], { type: "image/png" });
                    } else if (fileExtension == "potx") {
                        blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.presentationml.template" });
                    } else if (fileExtension == "ppsx") {
                        blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.presentationml.slideshow" });
                    } else if (fileExtension == "pptx") {
                        blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.presentationml.presentation" });
                    } else if (fileExtension == "tsv") {
                        blob = new Blob([buffer], { type: "text/tab-separated-values" });
                    } else if (fileExtension == "rtf") {
                        blob = new Blob([buffer], { type: "application/rtf" });
                    } else if (fileExtension == "xltx") {
                        blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.template" });
                    } 
                    var domURL = window.URL || self.URL || self.webkitURL;
                    var url = domURL.createObjectURL(blob),
                      img = new Image;

                    img.onload = function () {
                        domURL.revokeObjectURL(url); // clean up
                        //document.body.appendChild(this);
                        // this = image
                    };
                    img.src = url;
                    reader.readAsArrayBuffer(blob);
                }

                function fetch(url, callback, error) {

                    var xhr = new XMLHttpRequest();
                    try {
                        xhr.open("GET", url);
                        xhr.responseType = "arraybuffer";
                        xhr.onerror = function () {
                            error("Network error")
                        };
                        xhr.onload = function () {
                            if (xhr.status === 200) callback(xhr.response);
                            else error(xhr.statusText);
                        };
                        xhr.send();
                    } catch (err) {
                        error(err.message)
                    }
                }


            } else {
                reader.readAsArrayBuffer(document[indexDocument].DocumentPath);
            }
            //bangkit

            //reader.readAsDataURL(document.DocumentPath);
            return deferred.promise();
        }

        // Add the file to the file collection in the Shared Documents folder.
        function addFileToFolder(arrayBuffer) {

            // Construct the endpoint.
            var fileCollectionEndpoint = String.format(
                    "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                    "/add(overwrite=false, url='{2}')",
                serverUrl, serverRelativeUrlToFolder, fileName);

            UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token: Rizki 2017-01-31

            // Send the request and return the response.
            // This call returns the SharePoint file.
            return $.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val()
                    //"content-length": arrayBuffer.byteLength
                }
            });
        };

        // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
        function getListItem(fileListItemUri) {

            // Send the request and return the response.
            return $.ajax({
                url: fileListItemUri,
                type: "GET",
                headers: { "accept": "application/json;odata=verbose" }
            });
        };

        // Change the display name and title of the list item.
        function updateListItem(itemMetadata) {
            var body = {
                Title: document[indexDocument].DocumentPath.name,
                //Application_x0020_ID: context.ApplicationID,
                //CIF: context.CIF,
                //Customer_x0020_Name: context.Name,
                //Document_x0020_Type: document[indexDocument].Type.Name,
                //Document_x0020_Purpose: document[indexDocument].Purpose.Name,
                __metadata: {
                    type: itemMetadata.type,
                    FileLeafRef: fileName,
                    Title: fileName
                }
            };

            UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token: Rizki 2017-01-31

            // Send the request and return the promise.
            // This call does not return response content from the server.
            return $.ajax({
                url: itemMetadata.uri,
                type: "POST",
                data: ko.toJSON(body),
                headers: {
                    "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                    "content-type": "application/json;odata=verbose",
                    //"content-length": body.length,
                    "IF-MATCH": itemMetadata.etag,
                    "X-HTTP-Method": "MERGE"
                }
            });
        };
    };
    self.LoadDraft = function () {
        var uri = '';
        ar = window.location.hash.split('#');
        if (ar.length < 2) {
            self.IsDraftForm(false);
            return;
        }
        self.IsDraftForm(true);
        uri = api.server + api.url.documentFlow.getFromDraft(ar[1]);
        var options = {
            url: uri,
            token: self.secretToken
        };
        Helper.Ajax.Get(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status = 200) {
                self.IsLoadDraft(true);
                if (data == null) return;
                self.LoadDraftDF(data);
            } else {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
            }
        }, self.OnError, self.OnAlways);


    };
    self.LoadDraftDF = function (data) {
        self.DocumentFlowData().TransactionID(data.TransactionID);
        self.DocumentFlowData().Product("SFS Document Flow");
        self.DocumentFlowData().ProductID(data.ProductID);
        self.DocumentFlowData().Client(data.Client);
        self.DocumentFlowData().TransactionTypeID(data.TransactionTypeID);
        self.DocumentFlowData().Others(data.Others);
        self.DocumentFlowData().RefNumber(data.RefNumber);
        self.DocumentFlowData().DFRecipientID(data.DFRecipientID);
        self.DocumentFlowData().Sheets(data.Sheets);
        self.DocumentFlowData().Remarks(data.Remarks);
        self.DocumentFlowData().Documents([]);
        self.DocumentFlowData().IsDraft(data.IsDraft);
        self.DocumentFlowData().IsTopUrgent(data.IsTopUrgent);
        self.DocumentFlowData().IsUrgent(data.IsUrgent);
        self.DocumentFlowData().IsNormal(data.IsNormal);
        self.DocumentFlowData().IsSignatureVerification(data.IsSignatureVerification);
        self.Selected().Recipient(data.DFRecipientID);
        self.Selected().TransactionType(data.TransactionTypeID);
        //self.Selected().

        //console.log(data);
        if (data.Documents != null && data.Documents.length > 0) {
            ko.utils.arrayForEach(data.Documents, function (item) {
                self.Documents.push(item);
                //self.DocumentFlowData().Documents.push(item);
            });
        }      

    };

    //invoked
    self.GetRecipient();
    self.GetEmployeeLocation();
    self.GetParameters();
    self.SetAutoCompleteClientField();
    self.GetTransactionType();
};

var viewModel = new ViewModel();

$(document).ready(function () {
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    // block enter key from user to prevent submitted data.
    $(this).keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

            return false;
        }
    });

    // Load all templates
    $('script[src][type="text/html"]').each(function () {
        //alert($(this).attr("src"))
        var src = $(this).attr("src");
        if (src != undefined) {
            $(this).load(src);
        }
    });

    // scrollables
    $('.slim-scroll').each(function () {
        var $this = $(this);
        $this.slimscroll({
            height: $this.data('height') || 100,
            railVisible: true,
            alwaysVisible: true,
            color: '#D15B47'
        });
    });

    // tooltip
    $('[data-rel=tooltip]').tooltip();

    $.fn.ForceNumericOnly = function () {
        return this.each(function () {
            $(this).keydown(function (e) {
                var key = e.charCode || e.keyCode || 0;
                return (
                    key == 8 ||
                    key == 9 ||
                    key == 13 ||
                    key == 46 ||
                    key == 110 ||
                    key == 190 ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105));
            });

        });

    };
    $(".input-numericonly").ForceNumericOnly();

    $('.btn').each(function () {
        dataBind = $(this).attr('data-bind');
        if (dataBind == "click: SaveAsDraft, disable: !IsEditable(), visible: IsRole('DBS Admin')") {
            $(this).attr('data-bind', "click: SaveAsDraft, disable: !IsEditable(), visible: true")
        }
    });

    /// block enter key from user to prevent submitted data.
    $("input").keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

            return false;
        }
    });

    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    // datepicker
    $('.date-picker').datepicker({
        autoclose: true,
        onSelect: function () {
            this.focus();
        }
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    // validation
    //$('#aspnetForm1').validate({
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        focusCleanup: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });

    // ace file upload
    $('#document-path').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });

    $('#document-path-upload').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });

    // Knockout Datepicker custom binding handler
    ko.bindingHandlers.datepicker = {
        init: function (element) {
            $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).datepicker("refresh");
        }
    };

    // Knockout custom file handler
    ko.bindingHandlers.file = {
        init: function (element, valueAccessor) {
            $(element).change(function () {
                var file = this.files[0];

                if (ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };

    if (viewModel != undefined) {
        ko.dependentObservable(function () {
            var docType = ko.utils.arrayFirst(viewModel.Parameter().DocumentTypes(), function (item) {
                return item.ID == viewModel.Selected().DocumentType();
            });

            if (docType != null) viewModel.DocumentType(docType);

            var docPurpose = ko.utils.arrayFirst(viewModel.Parameter().DocumentPurposes(), function (item) {
                return item.ID == viewModel.Selected().DocumentPurpose();
            });

            if (docPurpose != null) viewModel.DocumentPurpose(docPurpose);

            if (viewModel.Selected().TransactionType() === ConsDocFlowTransType.Others) {
                viewModel.IsOthers(true);
            } else {
                viewModel.IsOthers(false);
            }            
        });

        
    };

    self.SPUser = ko.observable();
    // Token Validation
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(OnSuccessToken, OnError);
    }
    else {
        // read token from cookie
        viewModel.secretToken = $.cookie(api.cookie.name);

        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);
            self.SPUser(ko.mapping.toJS(spUser));
        }

        // call get data inside view model
        viewModel.GetTransactionType();

        //PPUModel.token = accessToken;
        //GetThresholdParameter(PPUModel, OnSuccessThresholdPrm, OnErrorDeal);
        //ResetBeneBank();
        //GetEmployeeLocation();
        viewModel.LoadDraft();
    }
    ko.applyBindings(viewModel);
});