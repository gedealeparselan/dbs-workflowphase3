﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class DocTypeController : ApiController
    {
        private string message = string.Empty;

        private readonly IDocumentTypeRepository repository = null;

        public DocTypeController()
        {
            this.repository = new DocumentTypeRepository();
        }

        public DocTypeController(IDocumentTypeRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all currencies.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<DocumentTypeModel>))]
        [Route("api/DocType")]
        public HttpResponseMessage Get()
        {
            IList<DocumentTypeModel> output = new List<DocumentTypeModel>();

            if (!repository.GetDocType(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<DocumentTypeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get currencies with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<DocumentTypeModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<DocumentTypeModel> output = new List<DocumentTypeModel>();

            if (!repository.GetDocType(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<DocumentTypeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get currencies for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(DocumentTypeGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<DocumentTypeFilter> filters)
        {
            DocumentTypeGrid output = new DocumentTypeGrid();

            if (!repository.GetDocType(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<DocumentTypeGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find currencies using criterias Name or Description.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<DocumentTypeModel>))]
        [HttpGet]
        [Route("api/DocType/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<DocumentTypeModel> output = new List<DocumentTypeModel>();

            if (!repository.GetDocType(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<DocumentTypeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get DocType by ID.
        /// </summary>
        /// <param name="id">DocType ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(DocumentTypeModel))]
        [Route("api/DocType/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            DocumentTypeModel output = new DocumentTypeModel();

            if (!repository.GetDocTypeByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<DocumentTypeModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new DocType.
        /// </summary>
        /// <param name="DocType">Cumrrency data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(DocumentTypeModel docType)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddDocType(docType, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Document type data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing DocType.
        /// </summary>
        /// <param name="id">ID of DocType to be modify.</param>
        /// <param name="customer">DocType data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/DocType/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, DocumentTypeModel docType)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateDocType(id, docType, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Document type data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting DocType.
        /// </summary>
        /// <param name="id">ID of DocType to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/DocType/{id}")]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteDocType(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "DocType data has been deleted." });
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}