﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class ExceptionHandlingController : ApiController
    {
        private string message = string.Empty;

        private readonly IExceptionHandlingRepository repository = null;

        public ExceptionHandlingController()
        {
            this.repository = new ExceptionHandlingRepository();
        }

        public ExceptionHandlingController(IExceptionHandlingRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all ExceptionHandlings.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<ExceptionHandlingModel>))]
        public HttpResponseMessage Get()
        {
            IList<ExceptionHandlingModel> output = new List<ExceptionHandlingModel>();

            if (!repository.GetExceptionHandling(ref output,ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ExceptionHandlingModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get ExceptionHandling Approvals with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<ExceptionHandlingModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<ExceptionHandlingModel> output = new List<ExceptionHandlingModel>();

            if (!repository.GetExceptionHandling(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<ExceptionHandlingModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get ExceptionHandling Approvals for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(ExceptionHandlingGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<ExceptionHandlingFilter> filters)
        {
            ExceptionHandlingGrid output = new ExceptionHandlingGrid();

            if (!repository.GetExceptionHandling(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<ExceptionHandlingGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find ExceptionHandling s using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<ExceptionHandlingModel>))]
        [HttpGet]
        [Route("api/ExceptionHandling/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<ExceptionHandlingModel> output = new List<ExceptionHandlingModel>();

            if (!repository.GetExceptionHandling(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ExceptionHandlingModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get ExceptionHandling  by ID.
        /// </summary>
        /// <param name="id">ExceptionHandling  ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(ExceptionHandlingModel))]
        [Route("api/ExceptionHandling/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            ExceptionHandlingModel output = new ExceptionHandlingModel();

            if (!repository.GetExceptionHandlingByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<ExceptionHandlingModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new ExceptionHandling .
        /// </summary>
        /// <param name="ExceptionHandling">ExceptionHandling  data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(ExceptionHandlingModel exceptionHandling)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddExceptionHandling(exceptionHandling, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Exception Handling data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing ExceptionHandling Approval.
        /// </summary>
        /// <param name="id">ID of ExceptionHandlingApproval to be modify.</param>
        /// <param name="ExceptionHandlingApproval">ExceptionHandling Approval data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/ExceptionHandling/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, ExceptionHandlingModel exceptionHandling)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateExceptionHandling(id, exceptionHandling,ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Exception Handling data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting ExceptionHandling Approval.
        /// </summary>
        /// <param name="id">ID of ExceptionHandling Approval to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/ExceptionHandling/{id}")]
        [Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteExceptionHandling(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Exception Handling  data has been deleted." });
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}