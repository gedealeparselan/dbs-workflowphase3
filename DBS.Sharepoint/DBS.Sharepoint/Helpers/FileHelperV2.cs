﻿using Excel;
using Microsoft.SharePoint;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBS.Sharepoint.Helpers
{
    public static class FileHelperV2
    {
        public static string DataTableToCSV(DataTable table, string delimiter, bool includeHeader)
        {
            StringBuilder result = new StringBuilder();

            if (includeHeader)
            {
                foreach (DataColumn column in table.Columns)
                {
                    result.Append(column.ColumnName);
                    result.Append(delimiter);
                }

                result.Remove(result.Length, 0);
                result.Append(Environment.NewLine);
            }

            foreach (DataRow row in table.Rows)
            {
                foreach (object item in row.ItemArray)
                {

                    if (item is DBNull)
                        result.Append(delimiter);
                    else
                    {
                        string itemAsString;

                        if (item.GetType() == typeof(DateTime))
                        {
                            itemAsString = DateTime.Parse(item.ToString()).ToString("dd-MM-yyyy");
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(item.ToString()) && item.ToString().Substring(0, 1) == "0")
                                itemAsString = "'" + item.ToString();
                            else
                                itemAsString = item.ToString();
                        }

                        // Double up all embedded double quotes
                        itemAsString = itemAsString.Replace("\"", "\"\"");

                        // To keep things simple, always delimit with double-quotes
                        // so we don't have to determine in which cases they're necessary
                        // and which cases they're not.
                        //itemAsString = "\"" + itemAsString + "\"";

                        result.Append(itemAsString + delimiter);
                    }
                }

                result.Remove(result.Length, 0);
                result.Append(Environment.NewLine);
            }

            return result.ToString();
        }

        public static Stream GenerateStreamFromString(string data)
        {
            MemoryStream memoryStream = new MemoryStream();
            StreamWriter streamWriter = new StreamWriter(memoryStream);
            streamWriter.Write(data);
            streamWriter.Flush();
            memoryStream.Position = 0;
            return memoryStream;
        }

        public static bool AddToLibrary(SPWeb web, Stream stream, string fileName, string libraryName, bool replaceExisiting, ref long insertedID, ref string insertedURL, ref bool result, ref string errorMessage)
        {
            result = false;

            try
            {
                //SPFolder libFolder = web.Folders[libraryName]; --> BUG (if library name is not equals to lib folder)
                SPFolder libFolder = web.Lists[libraryName].RootFolder;

                //Check the existing File out if the Library Requires CheckOut
                if (libFolder.RequiresCheckout)
                {
                    try
                    {
                        SPFile fileOld = libFolder.Files[fileName];
                        fileOld.CheckOut();
                    }
                    catch (Exception ex)
                    {
                        errorMessage = ex.InnerException.ToString();
                    }
                }

                // read bytes
                byte[] contents = new byte[stream.Length];
                stream.Read(contents, 0, (int)stream.Length);
                stream.Close();

                //Property
                Hashtable properties = new Hashtable();
                properties.Add("Name", "Name");

                // Upload document
                SPFile spfile = libFolder.Files.Add(fileName, contents, properties, replaceExisiting);
                insertedID = spfile.Item.ID;
                insertedURL = string.Format("{0}/{1}", web.Url, spfile.Item.Url);

                // Commit 
                libFolder.Update();

                result = true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.InnerException.ToString();
            }
            finally
            {
                web.Dispose();
                stream.Dispose();
            }


            return result;
        }

        public static string Left(string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            maxLength = Math.Abs(maxLength);

            return (value.Length <= maxLength
                   ? value
                   : value.Substring(0, maxLength)
                   );
        }

        public static string Right(this string value, int length)
        {
            return value.Substring(value.Length - length);
        }

        #region FD
        public static string ViewNameFDRTGS = "V_GenerateFDRTGS";
        public static string ViewNameFDSKN = "V_GenerateFDSKN";
        public static string ViewNameFDIM = "V_GenerateFDIM";

        public static string FTipeFDRTGS = "FD_RTGS";
        public static string FTipeFDSKN = "FD_SKN";
        public static string FTipeFDInterestMaintenance = "FD_IM";
        public static string POAEmail = "POAEmail";

        public static string[] SelectColumnFDRTGS = {
                                                      "[No]","[Cust RefNo]","[Debt Acct No]","[Value Date]","[Payment Amount]","[Cust Name]","[Benef Name]",
                                                      "[Benef Acc No]","[Bank Code]","[Branch Code]","[Citizen]","[Resident]","[Bank Charge]","[Agent Charge]",
                                                      "[Payment Detail]"
                                                    };
        public static string[] SelectColumnFDSKN = {
                                                      "[No]","[Debit Acct No]","[Cust Name]","[Value Date]","[Payment Amount]","[Benef Name]",
                                                      "[Benef Acc No]","[Bank Code]","[Branch Code]","[Citizen]","[Resident]","[Payment Detail]","[Bank Charge]","[Type]"
                                                   };
        public static string[] SelectColumnFDIM = {
                                                      "[Scheme Type]","[Account Number]","[Int. Table Code]","[Version]","[A/c. Pref. Int. (Cr.)]",
                                                      "[A/c. Pref. Int. (Dr.)]","[Int. Pegged]","[Pegging Freq. (Months/Days)]","[Peg Review Date]",
                                                      "[Start Date]","[End Date]","[InterestMargin]"
                                                  };
        #endregion

        #region Loan
        public static string ViewNameLoanRollover = "V_GenerateLoanRollover";
        public static string ViewNameLoanIM = "V_GenerateLoanInterestMaintenance";

        public static string FTipeLoanRollover = "LOAN_RO";
        public static string FTipeLoanIM = "LOAN_IM";

        public static string[] SelectColumnLoanRollover = {
                                                        "[Scheme Type]","[Account Number]","[Int. Table Code]","[Version]","[A/c. Pref. Int. (Cr.)]",
                                                        "[A/c. Pref. Int. (Dr.)]","[Int. Pegged]","[Peggin Freq. (Month/Days)]","[Peg Review Date]",
                                                        "[Start Date]","[End Date]","[Interest Margin]"
                                                    };
        public static string[] SelectColumnLoanIM = {
                                                     "[Scheme Type]","[Account Number]","[Int. Table Code]","[Version]","[A/c. Pref. Int. (Cr.)]",
                                                     "[A/c. Pref. Int. (Dr.)]","[Int. Pegged]","[Peggin Freq. (Month/Days)]","[Peg Review Date]",
                                                     "[Start Date]","[End Date]","[Interest Margin]"
                                                   };
        #endregion

        #region CIF Change RM
        public static string SP_CRM = "SP_FileGenerationCIFChangeRM";
        public static string FTipeChangeRMCSV = "ChangeRMCSV";
        public static string FTipeChangeRMExcel = "ChangeRMExcel";
        public static string[] SelectColumnChangeRM = { "[No]", "[CIF]", "[Customer Name]", "[Change Segment From]",
                                                        "[Change Segment To]", "[Change From Branch Code]", "[Change From RM Code]",
                                                        "[Change From BU]", "[Change To Branch Code]", "[Change To RM Code]", "[Change To BU]",
                                                        "[Account]", "[PC Code]", "[EA/TPB]", "[Remarks]"};
        #endregion
    }
}
