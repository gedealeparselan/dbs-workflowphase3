﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PM_UserCategoryWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.PM.PM_UserCategoryWP.PM_UserCategoryWPUserControl" %>


<h1 class="header smaller lighter dark">User Category</h1>

<!-- widget box start -->
<div id="widget-box" class="widget-box">
    <!-- widget header start -->
    <div class="widget-header widget-hea1der-small header-color-dark">
        <h6>User Category Table</h6>

        <div class="widget-toolbar">
            <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                <i class="blue icon-filter"></i>
                <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridProperties().AllowFilter" />
                <span class="lbl"></span>
            </label>
            <a href="#" data-bind="click: GetData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                <i class="blue icon-refresh"></i>
            </a>
        </div>
    </div>
    <!-- widget header end -->

    <!-- widget body start -->
    <div class="widget-body">
        <!-- widget main start -->
        <div class="widget-main padding-0">
            <!-- widget slim control start -->
            <div class="slim-scroll" data-height="400">
                <!-- widget content start -->
                <div class="content">

                    <!-- table responsive start -->
                    <div class="table-responsive">
                        <div class="dataTables_wrapper" role="grid">
                            <table id="Branch-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr data-bind="with: GridProperties">
                                        <th style="width: 50px">No.</th>
                                        <th data-bind="click: function () { Sorting('UserCategoryCode'); }, css: GetSortedColumn('UserCategoryCode')">User Category Code</th>
                                        <th data-bind="click: function () { Sorting('Product'); }, css: GetSortedColumn('Product')">Product</th>
                                        <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                        <th data-bind="click: function () { Sorting('MaxTransaction'); }, css: GetSortedColumn('MaxTransaction')">Max Transaction</th>
                                        <th data-bind="click: function () { Sorting('MinTransaction'); }, css: GetSortedColumn('MinTransaction')">Min Transaction</th>
                                        <th data-bind="click: function () { Sorting('Unlimited'); }, css: GetSortedColumn('Unlimited')">Unlimited</th>
                                        <th data-bind="click: function () { Sorting('UserGroup'); }, css: GetSortedColumn('UserGroup')">User Group</th>
                                        <th data-bind="click: function () { Sorting('UpdateBy'); }, css: GetSortedColumn('UpdateBy')">Update By</th>
                                        <th data-bind="click: function () { Sorting('UpdateDate'); }, css: GetSortedColumn('UpdateDate')">Update Date</th>

                                    </tr>
                                </thead>
                                <thead data-bind="visible: GridProperties().AllowFilter" class="table-filter">
                                    <tr>
                                        <th class="clear-filter">
                                            <a href="#" data-bind="click: ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                <i class="green icon-trash"></i>
                                            </a>
                                        </th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterUserCategoryCode, event: { change: GridProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterProduct, event: { change: GridProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCurrency, event: { change: GridProperties().Filter }" style="text-align: center;" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterMaxTransaction, event: { change: GridProperties().Filter }" style="text-align: right;" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterMinTransaction, event: { change: GridProperties().Filter }" style="text-align: right;" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterUnlimited, event: { change: GridProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterUserGroup, event: { change: GridProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterModifiedBy, event: { change: GridProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterModifiedDate, event: { change: GridProperties().Filter }" /></th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: UserCategories, visible: UserCategories().length > 0">
                                    <tr data-bind="click: $root.GetSelectedRow" data-toggle="modal">
                                        <td><span data-bind="text: RowID"></span></td>
                                        <td><span data-bind="text: UserCategoryCode"></span></td>
                                        <td><span data-bind="text: Product.Name"></span></td>
                                        <td align="center"><span data-bind="text: Currency.Code"></span></td>
                                        <td align="right"><span data-bind="text: formatNumber(MaxTransaction)"></span></td>
                                        <td align="right"><span data-bind="text: formatNumber(MinTransaction)"></span></td>
                                         <td><span data-bind="text: (Unlimited ==true? 'Yes' : 'No')"></span></td>
                                        <td><span data-bind="text: UserGroup"></span></td>
                                        <td><span data-bind="text: UpdateBy"></span></td>
                                        <td><span data-bind="text: UpdateDate != null? $root.LocalDate(UpdateDate) : ''  "></span></td>

<%--                                        <td><span data-bind="text: $root.LocalDate(UpdateDate) "></span></td>--%>

                                    </tr>
                                </tbody>
                                <tbody data-bind="visible: UserCategories().length == 0">
                                    <tr>
                                        <td colspan="10" class="text-center">No entries available.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- table responsive end -->
                </div>
                <!-- widget content end -->
            </div>
            <!-- widget slim control end -->

            <!-- widget footer start -->
            <div class="widget-toolbox padding-8 clearfix">
                <div class="row" data-bind="with: GridProperties">
                    <!-- pagination size start -->
                    <div class="col-sm-6">
                        <div class="dataTables_paginate paging_bootstrap pull-left">
                            Showing
                            <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                            rows of <span data-bind="text: Total"></span>
                            entries
                        </div>
                    </div>
                    <!-- pagination size end -->

                    <!-- pagination page jump start -->
                    <div class="col-sm-3">
                        <div class="dataTables_paginate paging_bootstrap">
                            Page
                            <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                            of <span data-bind="text: TotalPages"></span>
                        </div>
                    </div>
                    <!-- pagination page jump end -->

                    <!-- pagination navigation start -->
                    <div class="col-sm-3">
                        <div class="dataTables_paginate paging_bootstrap">
                            <ul class="pagination">
                                <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                        <i class="icon-double-angle-left"></i>
                                    </a>
                                </li>
                                <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                        <i class="icon-angle-left"></i>
                                    </a>
                                </li>
                                <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                        <i class="icon-angle-right"></i>
                                    </a>
                                </li>
                                <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                        <i class="icon-double-angle-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- pagination navigation end -->

                </div>
            </div>
            <!-- widget footer end -->

        </div>
        <!-- widget main end -->
    </div>
    <!-- widget body end -->
</div>
<!-- widget box end -->

<div data-bind="if: $root.IsRoleMaker()">
    <h4 class="pink">
        <i class="icon-hand-right green"></i>
        <a href="#modal-form" role="button" class="blue" data-toggle="modal" data-bind="click: NewData">New User Category</a>
    </h4>
</div>

<!-- modal form start -->
<div id="modal-form" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- modal header start -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span data-bind="if: $root.IsNewData()">Create a new user category 
					Parameter</span>
                    <span data-bind="if: !$root.IsNewData()">Modify a user category 
					Parameter</span>
                </h4>
            </div>
            <!-- modal header end -->

            <!-- modal body start -->
            <div class="modal-body overflow-visible">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div id="customer-form" class="form-horizontal" role="form">
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="UserCategoryCode">
                                    User category code</label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="UserCategoryCode" name="UserCategoryCode" data-bind="value: $root.UserCategoryCode, disable: !$root.IsNewData()" class="col-xs-10 col-sm-5" data-rule-required="true" data-rule-maxlength="5"/>
                                        <label class="control-label bolder text-danger" for="maxTrx">*</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="maxTrx">Max Transaction</label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="maxTrx" name="maxTrx" data-bind="value: $root.MaxTransaction, disable: $root.Readonly" class="col-sm-4 align-right valid" data-rule-required="true" data-rule-number="true" data-rule-maxlength="15" />
                                        <label class="control-label bolder text-danger" for="maxTrx">*</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="minTrx">Min Transaction</label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="minTrx" name="maxTrx" data-bind="value: $root.MinTransaction, disable: $root.Readonly" class="col-sm-4 align-right valid" data-rule-required="true" data-rule-number="true" data-rule-maxlength="15" />
                                        <label class="control-label bolder text-danger" for="minTrx">*</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="ccy">
                                    CCY</label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="ccy" name="ccy" data-rule-required="true" data-rule-value="true" data-bind="value: $root.Currency().ID, options: ddlCCYName, optionsText: 'Code', optionsValue: 'ID', optionsCaption: 'Please Select...', event: { change: $root.OnChangeModeCCY }, disable: $root.Readonly"></select>
                                        <label class="control-label bolder text-danger" for="maxTrx">*</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="product">
                                    Product</label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="product" name="product" data-rule-required="true" data-rule-value="true" data-bind="value: $root.Product().ProductID, options: ddlProductName, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...', event: { change: $root.OnChangeModeProduct }, disable: $root.Readonly"></select>
                                        <label class="control-label bolder text-danger" for="maxTrx">*</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label bolder no-padding-right" for="UserGroup">
                                    User Group
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="UserGroup" name="UserGroup" data-rule-required="true" data-rule-value="true" data-bind="options: $root.ddlUserGroup, optionsText: 'name', optionsValue: 'id', value: $root.UserGroup, optionsCaption: 'Please Select...', event: { change: $root.OnChangeMode }"></select>
                                        <label class="control-label bolder text-danger" for="maxTrx">*</label>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label bolder no-padding-right" for="Unlimited">
                                    Unlimited
                                </label>
                                <label class="col-sm-2">
                                    <input name="Unlimited" data-bind="checked: Unlimited, enable: $root.IsRoleMaker()" class="ace ace-switch ace-switch-6" type="checkbox">
                                    <span class="lbl"></span>
                                </label>
                            </div>

                        </div>
                        <!-- modal body form end -->
                    </div>
                </div>
            </div>
            <!-- modal body end -->

            <!-- modal footer start -->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.save, visible: $root.IsNewData() && $root.IsRoleMaker()">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm btn-primary" data-bind="click: $root.update, visible: !$root.IsNewData() && $root.IsRoleMaker()">
                    <i class="icon-edit"></i>
                    Update
                </button>
                <button class="btn btn-sm btn-warning" data-bind="click: $root.delete, visible: !$root.IsNewData() && $root.IsRoleMaker()">
                    <i class="icon-trash"></i>
                    Delete
                </button>
                <button class="btn btn-sm" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Close
                </button>
            </div>
            <!-- modal footer end -->

        </div>
    </div>
</div>
<!-- modal form end -->

<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
