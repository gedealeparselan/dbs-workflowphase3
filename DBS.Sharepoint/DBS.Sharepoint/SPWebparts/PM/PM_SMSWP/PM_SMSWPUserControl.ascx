﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PM_SMSWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.PM.PM_SMSWP.PM_SMSWPUserControl" %>

<h1 class="header smaller lighter dark">SMS Body Parameter</h1>
<div class="modal-body overflow-visible">
        <div class="row">
            <div class="col-xs-12">
                <div id="customer-form" class="form-horizontal" role="form">
                    <div class="form-group no-margin-bottom no-padding-bottom">
                        <label class="col-sm-2" for="SingleValue">
                            SMS Body
                        </label>
                        <div class="col-sm-10">
                            <div class="clearfix">
                                <!--<input class="col-sm-12" type="text" name="SingleValue" id="SingleValue" data-in="" value="0" data-bind="value: SingleValue" data-rule-required = "true" data-rule-value = "true" data-rule-number="true" />-->
                                <div class="col-sm-9">										
	                                </div>
	                                <div class="col-sm-9">
										<div class="clearfix">
											<textarea class="ckeditor" cols="80" id="editor1" name="editor1" rows="10" data-bind="visible: $root.Readonly() == false">
											</textarea>
										</div>
									</div>	                                
                               <label class="control-label bolder text-danger" style="position:absolute;" for="Name">*</label>
                            </div>
                        </div>
                        
                    </div>
                    <div class="form-group">
                            <button id="btnUpdate" class="btn btn-sm btn-primary" data-bind="click: UpdateValue, visible: $root.IsRoleMaker()">
                                <i class="icon-save"></i>
                                Save
                            </button>
                        </div>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.hotkeys.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/bootstrap-wysiwyg.min.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/ckeditor/ckeditor.js"></script>