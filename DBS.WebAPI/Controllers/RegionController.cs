﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class RegionController : ApiController
    {
        private string message = string.Empty;

        private readonly IRegionRepository repository = null;

        public RegionController()
        {
            this.repository = new RegionRepository();
        }

        public RegionController(IRegionRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all Region.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<RegionModel>))]
        public HttpResponseMessage Get()
        {
            IList<RegionModel> output = new List<RegionModel>();

            if (!repository.GetRegion(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<RegionModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get region with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<RegionModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<RegionModel> output = new List<RegionModel>();

            if (!repository.GetRegion(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<RegionModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get region for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(RegionGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<RegionFilter> filters)
        {
            RegionGrid output = new RegionGrid();
            
            if (!repository.GetRegion(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<RegionGrid>(HttpStatusCode.OK, output);
            }
        }


        [ResponseType(typeof(IList<RegionModel>))]
        [HttpGet]
        [Route("api/Region/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<RegionModel> output = new List<RegionModel>();

            if (!repository.GetRegion(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<RegionModel>>(HttpStatusCode.OK, output);
            }
        }


        [ResponseType(typeof(RegionModel))]
        [Route("api/Region/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            RegionModel output = new RegionModel();

            if (!repository.GetRegionByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<RegionModel>(HttpStatusCode.OK, output);
            }
        }


        [HttpPost]
        [ResponseType(typeof(string))]
       // [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(RegionModel region)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddRegion(region, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Region data has been created." });
                }
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/Region/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, RegionModel region)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateRegion(id, region, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Region data has been updated." });
                }
            }
        }

        [HttpDelete]
        [Route("api/Region/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteRegion(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Region data has been deleted." });
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}
