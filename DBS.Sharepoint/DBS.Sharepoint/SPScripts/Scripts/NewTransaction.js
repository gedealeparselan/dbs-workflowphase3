/**
 * Created by ruddycahyadi on 9/29/2014.
 */

var accessToken;

var NewCustomerFXWarning = "Cannot proccess FX Transaction for New Customer Added.";

/* dodit@2014.11.08:add format function */
var formatNumber = function (num) {
    var n = num.toString(), p = n.indexOf('.');
    return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
        return p < 0 || i < p ? ($0 + ',') : $0;
    });
}

var formatDateValue = function (date, isDateOnly, isDateLong) {
    var DateToFormat = new Date(date);

    if (date == '1970/01/01 00:00:00' || date == null) {
        return "";
    }
    if (moment(DateToFormat).isValid()) {
        return moment(DateToFormat).format(config.format.date);
    } else {
        return "";
    }
};

var formatAccount = function () {
    var num = document.getElementById("bene-acc-number").value;
    num = num.replace(/-/g, '');

    viewModel.TransactionModel().BeneAccNumber(num);

    sheet_a = (num.length > 0) ? num.substr(0, 4) : '';
    sheet_b = (num.length > 4) ? '-' + num.substr(4, 4) : '';
    sheet_c = (num.length > 8) ? '-' + num.substr(8) : '';

    viewModel.BeneAccNumberMask(sheet_a + sheet_b + sheet_c);
}


var $box;
var $remove = false;
var cifData = '0';
var customerNameData = '';


var idrrate = 0;
var DocumentModels = ko.observable({ FileName: '', DocumentPath: '' });
var PPUModel = { cif: cifData, token: accessToken }

var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};

var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};

var BizSegmentModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var BranchModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var TypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ProductModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    WorkflowID: ko.observable(),
    Workflow: ko.observable()
};

var LLDModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var UnderlyingDocModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ChannelModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var BankModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    BranchCode: ko.observable(),
    SwiftCode: ko.observable(),
    BankAccount: ko.observable(),
    Description: ko.observable(),
    CommonName: ko.observable(),
    Currency: ko.observable(),
    PGSL: ko.observable()
};

var RMModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    RankID: ko.observable(),
    Rank: ko.observable(),
    SegmentID: ko.observable(),
    Segment: ko.observable(),
    BranchID: ko.observable(),
    Branch: ko.observable(),
    Email: ko.observable()
};

var ContactModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    PhoneNumber: ko.observable(),
    DateOfBirth: ko.observable(),
    Address: ko.observable(),
    IDNumber: ko.observable()
};

var FunctionModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var CurrencyModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var AccountModel = {
    AccountNumber: ko.observable(),
    Currency: ko.observable(CurrencyModel)
};

var CustomerDraftModel = {
    DraftCIF: ko.observable(),
    DraftCustomerName: ko.observable(),
    DraftAccountNumber: ko.observable(AccountModel)
}

var ProductTypeModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    IsFlowValas: ko.observable(),
    Description: ko.observable()
};

var UnderlyingDocumentModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var DocumentTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var DocumentPurposeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var FXComplianceModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ChargesModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var UnderlyingModel = {
    ID: ko.observable(),
    UnderlyingDocument: ko.observable(UnderlyingDocumentModel),
    DocumentType: ko.observable(DocumentTypeModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    AttachmentNo: ko.observable(),
    IsStatementLetter: ko.observable(),
    IsDeclarationOfException: ko.observable(),
    StartDate: ko.observable(),
    EndDate: ko.observable(),
    DateOfUnderlying: ko.observable(),
    ExpiredDate: ko.observable(),
    ReferenceNumber: ko.observable(),
    SupplierName: ko.observable(),
    InvoiceNumber: ko.observable()
};

var DocumentModel = {
    ID: ko.observable(),
    Type: ko.observable(DocumentTypeModel),
    Purpose: ko.observable(DocumentPurposeModel),
    FileName: ko.observable(),
    DocumentPath: ko.observable(),
    LastModifiedDate: ko.observable(new Date),
    LastModifiedBy: ko.observable()
};

var CustomerModel = {
    CIF: ko.observable(),
    Name: ko.observable(),
    POAName: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Branch: ko.observable(BranchModel),
    Type: ko.observable(TypeModel),
    RM: ko.observable(RMModel),
    Contacts: ko.observableArray([ContactModel]),
    Functions: ko.observableArray([FunctionModel]),
    Accounts: ko.observableArray([AccountModel]),
    Underlyings: ko.observableArray([UnderlyingModel])
};

// added model form underlying -----------

var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};

var SelectModel = function (cif, id) {
    var self = this;
    self.ID = ko.observable(0);
    self.CIF = ko.observable(cif);
    self.UnderlyingID = ko.observable(id);
    self.ParentID = ko.observable(0);
}
var SelectBulkModel = function (cif, id) {
    var self = this;
    self.ID = ko.observable(0);
    self.CIF = ko.observable(cif);
    self.UnderlyingID = ko.observable(id);
    self.ParentID = ko.observable(0);
}

var UnderlyingDocumentModel2 = function (id, name, code) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Name = ko.observable(name);
    self.CodeName = ko.observable(code + ' (' + name + ')');
}

var DocumentTypeModel2 = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}

var StatementLetterModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}

var CurrencyModel2 = function (id, code, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Description = ko.observable(description);
}

var ProductTypeModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    IsFlowValas: ko.observable(),
    Description: ko.observable()
};

var DocumentPurposeModel2 = function (id, name, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(description);
}

var CustomerUnderlyingMappingModel = function (FileID, UnderlyingID, attachNo) {
    var self = this;
    self.ID = ko.observable(0);
    self.UnderlyingID = ko.observable(UnderlyingID);
    self.UnderlyingFileID = ko.observable(FileID);
    self.AttachmentNo = ko.observable(attachNo);
}

var SelectUtillizeModel = function (id, enable, uSDAmount, statementLetter) {
    var self = this;
    self.ID = ko.observable(id);
    self.Enable = ko.observable(enable);
    self.USDAmount = ko.observable(uSDAmount);
    self.StatementLetter = ko.observable(statementLetter);
}
var today = Date.now();
var TransactionModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    IsTopUrgentChain: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    IsNewCustomer: ko.observable(false),
    Product: ko.observable(ProductModel),
    ProductType: ko.observable(ProductTypeModel),
    LLD: ko.observable(LLDModel),
    UnderlyingDoc: ko.observable(UnderlyingDocModel),
    OtherUnderlyingDoc: ko.observable(),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(0),
    Rate: ko.observable(),
    AmountUSD: ko.observable(0.00),
    Channel: ko.observable(ChannelModel),
    Account: ko.observable(AccountModel),
    ApplicationDate: ko.observable(moment(today).format(config.format.date)),
    BizSegment: ko.observable(BizSegmentModel),
    Bank: ko.observable(BankModel),
    BankCharges: ko.observable(ChargesModel),
    AgentCharges: ko.observable(ChargesModel),
    Type: ko.observable(20),
    IsResident: ko.observable(true),
    IsCitizen: ko.observable(true),
    PaymentDetails: ko.observable(null),
    IsSignatureVerified: ko.observable(false),
    IsDormantAccount: ko.observable(false),
    IsFreezeAccount: ko.observable(false),
    Others: ko.observable(null),
    IsDraft: ko.observable(false),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]),
    DetailCompliance: ko.observable(),
    BeneName: ko.observable(),
    BeneAccNumber: ko.observable(),
    TZNumber: ko.observable(),
    TotalTransFX: ko.observable(0.00),
    TotalUtilization: ko.observable(0.00),
    utilizationAmount: ko.observable(0.00),
    underlyings: ko.observableArray([]),
    IsOtherBeneBank: ko.observable(true),
    OtherBeneBankName: ko.observable(),
    OtherBeneBankSwift: ko.observable(),
    UnderlyingDocID: ko.observable(),
    CustomerDraft: ko.observable(CustomerDraftModel),
    IsOtherAccountNumber: ko.observable(false),
    OtherAccountNumber: ko.observable(),
    DebitCurrency: ko.observable(CurrencyModel)
};

var Parameter = {
    Products: ko.observableArray([ProductModel]),
    ProductType: ko.observableArray([ProductTypeModel]),
    Currencies: ko.observableArray([CurrencyModel]),
    Channels: ko.observableArray([ChannelModel]),
    BizSegments: ko.observableArray([BizSegmentModel]),
    Banks: ko.observableArray([BankModel]),
    LLDs: ko.observableArray([LLDModel]),
    BankCharges: ko.observableArray([ChargesModel]),
    AgentCharges: ko.observableArray([ChargesModel]),
    FXCompliances: ko.observableArray([FXComplianceModel]),
    DocumentTypes: ko.observableArray([DocumentTypeModel]),
    DocumentPurposes: ko.observableArray([DocumentPurposeModel]),
    UnderlyingDocs: ko.observableArray([UnderlyingDocModel]),
};

var SelectedModel = {
    Product: ko.observable(),
    Currency: ko.observable(),

    //20150528-reizvan : Channel default "Original"
    Channel: ko.observable(1),

    Account: ko.observable(),
    BizSegment: ko.observable(),
    Bank: ko.observable(),
    LLD: ko.observable(),
    UnderlyingDoc: ko.observable(),
    DocumentType: ko.observable(),
    DocumentPurpose: ko.observable(),
    NewCustomer: ko.observable({
        Currency: ko.observable(),
        BizSegment: ko.observable()
    }),
    BankCharges: ko.observable(2),
    AgentCharges: ko.observable(2),
    ProductType: ko.observable(),
    DebitCurrency: ko.observable()
};

var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;

    self.FormatNumber = function (num) {
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    }
    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);
        if (date == '1970/01/01 00:00:00' || date == null) {
            return "";
        }

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    //dodit@2014.11.14:Add new function to know the role
    self.DraftID = ko.observable(0);

    self.IsRole = function (name) {
        var result = false;
        $.each($.cookie(api.cookie.spUser).Roles, function (index, item) {
            if (Const_RoleName[item.ID] == name) result = true; //if (item.Name == name) result = true;
        });
        return result;
    };

    // Sharepoint User
    self.SPUser = ko.observable(SPUser);

    self.IsEditTableUnderlying = ko.observable(true);

    // rate param
    self.Rate = ko.observable(0);
    // temp amount_u for bulk underlying // add 2015.03.09
    self.tempAmountBulk = ko.observable(0);
    // Is check attachments
    self.IsFXTransactionAttach = ko.observable(false);
    self.IsUploading = ko.observable(false);
    self.IsUploaded = ko.observable(false);
    self.SelectingUnderlying = ko.observable(false);
    self.BeneAccNumberMask = ko.observable();
    self.IsLoadDraft = ko.observable(false);
    self.IsEmptyAccountNumber = ko.observable(false);

    // FX FCY-IDR & IDR-FCY
    self.IsFxTransaction = ko.observable(false);
    self.IsFxTransactionToIDR = ko.observable(false);
    self.IsTransactionIDRToFCY = ko.observable(false);
    //editable file attach
    self.IsEditableDocument = ko.observable(false);

    // input form controls
    self.IsEditable = ko.observable(false);

    // optional other for bank
    self.IsOtherBank = ko.observable(false);

    // Parameters
    self.Parameter = ko.observable(Parameter);

    // Disable Product Type
    self.IsProductType = ko.observable(false);

    // Main Model
    self.TransactionModel = ko.observable(TransactionModel);

    // Double Transaction
    self.DoubleTransactions = ko.observableArray([]);

    //new underlying type
    self.isNewUnderlying = ko.observable(false);

    // Validation User
    self.UserValidation = ko.observable({
        UserID: ko.observable(),
        Password: ko.observable(),
        Error: ko.observable()
    });

    // Options selected value
    self.Selected = ko.observable(SelectedModel);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyings = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyingProformas = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyingFiles = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerAttachUnderlyings = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerBulkUnderlyings = ko.observableArray([]); // add 2015.03.09

    // Temporary documents
    self.Documents = ko.observableArray([]);
    self.DocumentPath = ko.observable();
    self.DocumentType = ko.observable();
    self.DocumentPurpose = ko.observable();
    self.DocumentFileName = ko.observable();

    // New Customer on change handler
    self.IsNewCustomerOnChange = function () {

        self.CustomerUnderlyings([]);
        self.CustomerUnderlyingFiles([]);
        self.CustomerAttachUnderlyings([]);

        cifData = '0';
        customerNameData = '';

        self.TransactionModel().Customer().Name = "";
        $("#customer-name").val("");

        // disabled autocomplete if this is a new customer
        $("#customer-name").autocomplete({ disabled: self.TransactionModel().IsNewCustomer() });
        self.TransactionModel().Customer().CIF = "";
        $('#cif').val("");

        //Basri
        if ($('#debit-acc-number option[value=-]').length <= 0) {
            $('#debit-acc-number').append('<option value=->-</option>');

        }
        self.IsEmptyAccountNumber(false);

        return true;

    };

    // set checkbox topurgent
    self.setTopUrgenChaint = ko.computed(function () {
            if (self.TransactionModel().IsTopUrgent()) {
                self.TransactionModel().IsTopUrgentChain(0); } 
    });
    self.setTopUgent = ko.computed(function () {
     if (self.TransactionModel().IsTopUrgentChain()) {
        self.TransactionModel().IsTopUrgent(0); }
    });


    self.OnChangeAccountCurrency = function () {
        var debitcurrency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) {
            if (viewModel.Selected().Account() == '-') {
                viewModel.TransactionModel().IsOtherAccountNumber(false);
                return item.ID == viewModel.Selected().DebitCurrency();
            }
        });
        if (debitcurrency != null) {
            viewModel.TransactionModel().DebitCurrency(debitcurrency);
            var AccountNumberSelected = $('#debit-acc-number option:selected').text().trim();
            if (AccountNumberSelected != null && AccountNumberSelected == '-') {
                var IsCurrencyChanged = viewModel.TransactionModel().DebitCurrency().Code;
                var IsFxTransaction = (viewModel.TransactionModel().Currency().Code != 'IDR' && IsCurrencyChanged == 'IDR');
                var IsFxTransactionToIDR = (viewModel.TransactionModel().Currency().Code == 'IDR' && IsCurrencyChanged != 'IDR' &&
                    viewModel.TransactionModel().ProductType().IsFlowValas == true && parseFloat(viewModel.TransactionModel().AmountUSD()) >= TotalPPUModel.FCYIDRTrashHold);
                var IsTransactionIDRToFCY = (viewModel.TransactionModel().Currency().Code == 'IDR' && IsCurrencyChanged != 'IDR');
                viewModel.IsFxTransaction(IsFxTransaction);
                viewModel.IsFxTransactionToIDR(IsFxTransactionToIDR);
                viewModel.IsTransactionIDRToFCY(IsTransactionIDRToFCY);
            }
        }

    }
    self.CheckEmptyAccountNumber = function () {
        if (ko.isObservable(viewModel.TransactionModel().OtherAccountNumber)) {
            var AccountNumberSelected = $('#debit-acc-number option:selected').text().trim();
            if (AccountNumberSelected == '-') {
                self.IsEmptyAccountNumber(true);
                //viewModel.TransactionModel().Account().AccountNumber = null; 
                viewModel.TransactionModel().IsOtherAccountNumber(true);
            } else {
                self.IsEmptyAccountNumber(false);
                viewModel.TransactionModel().OtherAccountNumber(null);
                viewModel.TransactionModel().IsOtherAccountNumber(false);
            }
        }
    };

    self.GetCalculating = function (amountUSD) {
        GetCalculating(amountUSD);
    };

    self.SetBankAutoCompleted = function () {
        // autocomplete
        $("#bank-name").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.bank + "/Search",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessBankAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set bank data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    self.TransactionModel().Bank(ko.mapping.toJS(ui.item.data, mapping));
                    self.IsOtherBank(true);
                    self.TransactionModel().IsOtherBeneBank(false);
                }
            }
        });
    };

    self.SetLLDAutoCompleted = function () {
        // autocomplete for lld code
        $("#lld-code").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.LLD + "/Search",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessLLDAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set bank data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    self.TransactionModel().LLD(ko.mapping.toJS(ui.item.data, mapping));


                }
            }
        });
    };
    var GetCalculating = function (amountUSD) {
        amountUSD = isNaN(amountUSD) || amountUSD == '' ? 0 : amountUSD; //avoid NaN
        var TotalIDRFCY = parseFloat(TotalPPUModel.Total.IDRFCYPPU) + parseFloat(amountUSD);
        var totalUtilize = TotalPPUModel.Total.UtilizationPPU + parseFloat(amountUSD);
        self.TransactionModel().AmountUSD(parseFloat(amountUSD).toFixed(2));

        TotalIDRFCY = isNaN(TotalIDRFCY) ? 0 : TotalIDRFCY; //avoid NaN
        self.TransactionModel().TotalTransFX(parseFloat(TotalPPUModel.Total.IDRFCYPPU).toFixed(2));
        if (self.TransactionModel().Currency().Code != 'IDR' && self.TransactionModel().Account().Currency.Code == 'IDR') {
            self.TransactionModel().TotalUtilization(totalUtilize.toFixed(2));
        } else {
            self.TransactionModel().TotalUtilization(totalUtilize.toFixed(2));
        }
        var IsFxTransactionToIDR = (self.TransactionModel().Currency().Code == 'IDR' && self.TransactionModel().Account().Currency.Code != 'IDR' &&
            self.TransactionModel().ProductType().IsFlowValas == true && self.TransactionModel().AmountUSD() >= TotalPPUModel.FCYIDRTrashHold);
        self.IsFxTransactionToIDR(IsFxTransactionToIDR);
    };

    var x = document.getElementById("trxn-amount");
    var y = document.getElementById("rate");
    var z = document.getElementById("currency");
    var d = document.getElementById("eqv-usd");

    var xstored = x.getAttribute("data-in");
    var ystored = y.getAttribute("data-in");

    setInterval(function () {
        if (x == document.activeElement) {
            var temp = x.value;
            if (xstored != temp) {
                xstored = temp;
                x.setAttribute("data-in", temp);
                calculate();
            }
        }
        if (y == document.activeElement) {
            var temp = y.value;
            if (ystored != temp) {
                ystored = temp;
                y.setAttribute("data-in", temp);
                calculate();
            }
        }
        /* dodit@2014.11.08:add posibility calculate if currency changed */
        if (z == document.activeElement) { // add by dodit 2014/11/8
            calculate();
        }
    }, 100);

    x.onblur = calculate;
    calculate();

    function calculate() {
        /* dodit@2014.11.08:altering calculate process to avoid NaN, avoid formula on USD, and formating feature */

        x.value = x.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        y.value = y.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        self.TransactionModel().Amount(x.value);
        self.TransactionModel().Rate(y.value);
        res = x.value;
        ret = y.value;

        x.value = formatNumber(x.value);
        y.value = formatNumber(y.value);

        currency = self.Selected().Currency();

        if (currency != 1) { // if not USD
            res = res * ret / idrrate;
            res = Math.round(res * 100) / 100;
        }

        self.GetCalculating(res);
    };



    self.GetRateAmount = function (CurrencyID) {
        GetRateAmount(CurrencyID);
    };

    self.GetRateAmountDraft = function (CurrencyID) {
        GetRateAmountDraft(CurrencyID);
    };

    var GetRateAmount = function (CurrencyID) {
        var options = {
            url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetRateAmount, OnError, OnAlways);
    }

    var GetRateAmountDraft = function (CurrencyID) {
        $.ajax({
            type: "Get",
            url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    // send notification
                    viewModel.TransactionModel().Rate(data.RupiahRate);
                    viewModel.Rate(data.RupiahRate);

                    var res = parseInt(viewModel.TransactionModel().Amount()) * parseFloat(viewModel.TransactionModel().Rate()) / parseFloat(idrrate);
                    res = Math.round(res * 100) / 100;
                    res = isNaN(res) ? 0 : res; //avoid NaN

                    $('#rate').value = formatNumber(data.RupiahRate);

                    viewModel.GetCalculating(res);
                } else {

                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    // Product on change handler
    self.OnProductChange = function () {
        //20150528-reizvan : RTGS/SKN default TransactionCurrency = IDR
        if (viewModel.TransactionModel().Product().Name == 'RTGS' || viewModel.TransactionModel().Product().Name == 'SKN') {
            viewModel.Selected().Currency(13);
        }
        var options = {
            url: api.server + api.url.transaction + "/GetApplicationID",
            params: {
                ProductID: self.TransactionModel().Product().ID
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetApplicationID, OnError, OnAlways);
    };

    // Uploading document
    self.UploadDocumentUnderlying = function () {
        // show upload dialog
        // show the dialog task form
        $("#modal-form-Attach").modal('show');
        self.TempSelectedAttachUnderlying([]);
        self.ID_a(0);
        self.DocumentPurpose_a(new DocumentPurposeModel2('', '', ''));
        self.DocumentType_a(new DocumentTypeModel2('', ''));
        self.DocumentPath_a('');
        GetDataUnderlyingAttach();
        $('.remove').click();
    }

    self.UploadDocument = function () {
        self.DocumentPath(null);
        self.Selected().DocumentType(null);
        self.Selected().DocumentPurpose(null);
        self.DocumentType(null);
        self.DocumentPurpose(null);
        $("#modal-form-upload").modal('show');
        $('.remove').click();
    }

    self.GetDocument = function (data) {

        //console.log(ko.toJSON(data.ID));
        GetDataUnderlyingAttach();

        $.ajax({
            type: "Get",
            url: api.server + api.url.customerunderlyingfile + "/" + data.ID,
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                // send notification
                if (jqXHR.status = 200) {
                    //console.log(ko.toJSON(data));

                    self.DocumentPurpose_a(data.DocumentPurpose);
                    self.DocumentPath_a(data.DocumentPath);
                    self.DocumentType_a(data.DocumentType);
                    self.FileName_a(data.FileName);

                    self.IsEditableDocument(true);

                    GetDataUnderlying();

                    // refresh data
                    // GetDataAttachFile();
                } else
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
        $("#modal-form-Attach").modal('show');
    }

    // Add new document handler
    self.AddDocument = function () {
        var doc = {
            ID: 0,
            Type: self.DocumentType(),
            Purpose: self.DocumentPurpose(),
            FileName: self.DocumentFileName(),
            DocumentPath: self.DocumentPath(),
            LastModifiedDate: new Date(),
            LastModifiedBy: null
        };
        //alert(JSON.stringify(self.DocumentPath()))
        //UploadFile(self.DocumentPath());

        //alert(JSON.stringify(self.DocumentPath()))
        if (doc.Type == null || doc.Purpose == null || doc.DocumentPath == null) {
            alert("Please complete the upload form fields.")
        } else {
            self.Documents.push(doc);
            $('.remove').click();
            // hide upload dialog
            $("#modal-form-upload").modal('hide');
        }

        // test upload
        /*var options = {
         url: "/_api/web/GetFolderByServerRelativeUrl('/sites/fileuploadtest/Documents/Communities)/Files/Add(url='"+ file.name + "',overwrite=true)"
         };
         Helper.Ajax.Post(options, OnSuccessUpload, OnError);*/
    };

    self.EditDocument = function (data) {
        self.DocumentPath(data.DocumentPath);
        self.Selected().DocumentType(data.Type.ID);
        self.Selected().DocumentPurpose(data.Purpose.ID);

        // show upload dialog
        $("#modal-form-upload").modal('show');
    };

    self.RemoveDocument = function (data) {
        //alert(JSON.stringify(data))
        self.Documents.remove(data);
    };

    // Save as draft handler
    self.SaveAsDraft = function () {

        var data = {
            ApplicationID: self.TransactionModel().ApplicationID(),
            CIF: self.TransactionModel().Customer().CIF,
            Name: self.TransactionModel().Customer().Name
        };
        // validation LLD description auto complete
        /*if (!ko.isObservable(viewModel.TransactionModel().LLD().Description)) {
            if (viewModel.TransactionModel().LLD().Description != null && viewModel.TransactionModel().LLD().ID == null) {
                ShowNotification("Form Validation Warning", "LLD Description not found on master data, Please retype LLD description.", 'gritter-warning', false);
                return false;
            } else {
                if (ko.isObservable(viewModel.TransactionModel().LLD().ID)) {
                    if (viewModel.TransactionModel().LLD().ID() == null) {
                        viewModel.TransactionModel().LLD().Description('');
                        viewModel.TransactionModel().LLD().Code('');
                    };
                } else {
                    if (viewModel.TransactionModel().LLD().ID == null) {
                        viewModel.TransactionModel().LLD().ID = ko.observable();
                        viewModel.TransactionModel().LLD().Description = ko.observable();
                        viewModel.TransactionModel().LLD().Code = ko.observable();
                    };
                }
            }
        }*/


        // set as draft
        self.TransactionModel().IsDraft(true);

        // set Application date if null
        if (self.TransactionModel().ApplicationDate() == '') {
            self.TransactionModel().ApplicationDate('1970/01/01');
        }

        // disabling form input controls
        self.IsEditable(false);

        // start upload docs and save the transaction
        SaveTransaction();
        //UploadDocuments();
        //}
    };

    self.SaveAsDraftLoan = function () {

        var data = {
            ApplicationID: self.TransactionModel().ApplicationID(),
            CIF: self.TransactionModel().Customer().CIF,
            Name: self.TransactionModel().Customer().Name
        };
        
        // set as draft
        self.TransactionModel().IsDraft(true);

        // set Application date if null
        if (self.TransactionModel().ApplicationDate() == '') {
            self.TransactionModel().ApplicationDate('1970/01/01');
        }

        // disabling form input controls
        self.IsEditable(false);

        // start upload docs and save the transaction
        SaveTransaction();
        //UploadDocuments();
        //}
    };

    // Save as draft handler
    self.SubmitLoan = function () {

        // validation
        var form = $("#aspnetForm");
        form.validate();
        
        if (form.valid() && viewModel.TransactionModel().Customer().CIF != "") {
            
            // set as not draft
            self.TransactionModel().IsDraft(false);

            // disabling form input controls
            self.IsEditable(false);

            // clear double transactions
            self.DoubleTransactions().removeAll;


            // Validation #1 : Check "Others" field
            if (self.TransactionModel().Others() == undefined || self.TransactionModel().Others() == "") {
                // Validation #2 : Check new transaction status
                ValidateTransaction();
            } else {
                var text = "Are you sure to make this transaction as an <b>Exceptional Handling</b>?";

                bootbox.confirm(text, function (result) {
                    if (result) {
                        // Validation #2 : Check new transaction status
                        ValidateTransaction();
                    } else {
                        // enable form input controls
                        self.IsEditable(true);
                    }
                });
            }
        }
        else {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        }
    };

    self.Submit = function () {



        if (self.TransactionModel().IsNewCustomer() && (self.IsFxTransaction() || self.IsFxTransactionToIDR())) {
            ShowNotification("Page Information", NewCustomerFXWarning, "gritter-warning", false);
            self.IsEditable(true);
            return false;
            //NewCustomerFXWarning
        }

        //console.log(ko.toJSON(viewModel.TransactionModel().Bank()));

        // validation
        var form = $("#aspnetForm");
        form.validate();
        //tz-number
        /*if (viewModel.TransactionModel().Currency().Code != 'IDR' && viewModel.TransactionModel().Account().Currency.Code == 'IDR') {
            // $('#tz-number').attr('data-rule-required', true);
            // $('#tz-number').rules('add', 'required');
        }*/
        if (form.valid() && viewModel.TransactionModel().Customer().CIF != "") {
            // validation LLD description auto complete
            /*if (!ko.isObservable(viewModel.TransactionModel().LLD().Description)) {
                if (viewModel.TransactionModel().LLD().Description != null && viewModel.TransactionModel().LLD().Description.trim() != "" && viewModel.TransactionModel().LLD().ID== null) {
                    ShowNotification("Form Validation Warning", "LLD Description not found on master data, Please retype LLD description.", 'gritter-warning', false);
                    return false;
                } else {
                    if (ko.isObservable(viewModel.TransactionModel().LLD().ID)) {
                        if (viewModel.TransactionModel().LLD().ID() == null) {
                            viewModel.TransactionModel().LLD().Description('');
                            viewModel.TransactionModel().LLD().Code('');
                        };
                    } else {
                        if (viewModel.TransactionModel().LLD().ID == null) {
                            viewModel.TransactionModel().LLD().ID = ko.observable();
                            viewModel.TransactionModel().LLD().Description = ko.observable();
                            viewModel.TransactionModel().LLD().Code = ko.observable();
                        };
                    }
                }
            } */

            // set as not draft
            self.TransactionModel().IsDraft(false);

            // disabling form input controls
            self.IsEditable(false);

            // clear double transactions
            self.DoubleTransactions().removeAll;


            // Validation #1 : Check "Others" field
            if (self.TransactionModel().Others() == undefined || self.TransactionModel().Others() == "") {
                // Validation #2 : Check new transaction status
                ValidateTransaction();
            } else {
                var text = "Are you sure to make this transaction as an <b>Exceptional Handling</b>?";

                bootbox.confirm(text, function (result) {
                    if (result) {
                        // Validation #2 : Check new transaction status
                        ValidateTransaction();
                    } else {
                        // enable form input controls
                        self.IsEditable(true);
                    }
                });
            }
        }
        else {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        }
    };

    // User Validation process Leleana
    self.ValidationProcess = function () {
        // Ruddy 21.10.2014 : Pending task. Still can't validate user & password to sharepoint.
        // edited 24-01-2015

        var options = {
            url: "/_vti_bin/DBSUserValidation/UserValidationServices.svc/UserAD",
            data: ko.toJSON(self.UserValidation())
        };

        Helper.Sharepoint.Validate.User(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // hide Double Transaction dialog
                console.log(ko.toJSON(data));
                if (data == "Succeded") {
                    $("#modal-double-transaction").modal('hide');

                    self.UserValidation().UserID("");
                    self.UserValidation().Password("");
                    self.UserValidation().Error("");

                    // start upload docs and save the transaction
                    UploadDocuments();
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, data, 'gritter-error', true);
                }

                //} else {
                // show error
                //self.UserValidation().Error(data.Message);
                //}
            }
        }, OnError, OnAlways);

        // remove this when pending task (user validation has been completed).
        //UploadDocuments(); // start upload docs and save the transaction
    };

    // Continue editing
    self.ContinueEditing = function () {
        // enable form input controls
        self.IsEditable(true);
        self.UserValidation().UserID("");
        self.UserValidation().Password("");
        self.UserValidation().Error("");
    };


    //--------------------------- set Underlying function & variable start
    self.IsUnderlyingMode = ko.observable(true);
    self.ID_u = ko.observable("");
    self.CIF_u = ko.observable(cifData);
    self.CustomerName_u = ko.observable(customerNameData);
    self.UnderlyingDocument_u = ko.observable(new UnderlyingDocumentModel2('', '', ''));
    self.ddlUnderlyingDocument_u = ko.observableArray([]);
    self.DocumentType_u = ko.observable(new DocumentTypeModel2('', ''));
    self.ddlDocumentType_u = ko.observableArray([]);
    self.Currency_u = ko.observable(new CurrencyModel2('', '', ''));
    self.ddlCurrency_u = ko.observableArray([]);
    self.StatementLetter_u = ko.observable(new StatementLetterModel('', ''));
    self.ddlStatementLetter_u = ko.observableArray([]);
    self.AvailableAmount_u = ko.observable(0);
    self.Rate_u = ko.observable(0);
    self.AmountUSD_u = ko.observable(0.00);
    self.Amount_u = ko.observable(0);
    self.AttachmentNo_u = ko.observable("");
    self.SelectUnderlyingDocument_u = ko.observable("");
    self.IsDeclarationOfException_u = ko.observable(false);
    self.StartDate_u = ko.observable();
    self.EndDate_u = ko.observable();
    self.DateOfUnderlying_u = ko.observable();
    self.ExpiredDate_u = ko.observable();
    self.ReferenceNumber_u = ko.observable("");
    self.SupplierName_u = ko.observable("");
    self.InvoiceNumber_u = ko.observable("");
    self.IsStatementA = ko.observable(true);
    self.IsProforma_u = ko.observable(false);
    self.IsSelectedProforma = ko.observable(false);
    self.IsUtilize_u = ko.observable(false);
    self.IsEnable_u = ko.observable(false);
    // add bulkUnderlying // add 2015.03.09
    self.IsBulkUnderlying_u = ko.observable(false);
    self.IsSelectedBulk = ko.observable(false);

    //yes
    self.TransactionModel.BeneName = ko.computed(function () {
        /*var maxLength=$('textarea').attr('maxlength');
        return this.comment().length+ " of "+maxLength;*/
    }, this);

    self.Amounttmp_u = ko.computed({
        read: function () {
            var fromFormat = document.getElementById("Amount_u").value;
            fromFormat = fromFormat.toString().replace(/,/g, '');
            //var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            if (self.StatementLetter_u().ID() == 1) {
                self.Currency_u().ID(1);
                self.Amount_u(TotalPPUModel.TreshHold);
                var date = new Date();
                var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                var day = lastDay.getDate();
                var month = lastDay.getMonth() + 1;
                var year = lastDay.getFullYear();
                var fixLastDay = year + "/" + month + "/" + day;
                //var fixLastDay = day + "-" + monthNames[month - 1] + "-" + year;
                self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
                SetDefaultValueStatementA();
            } else if (self.StatementLetter_u().ID() == 2 && self.DateOfUnderlying_u() != "" && self.IsNewDataUnderlying()) {
                if (self.LocalDate(self.DateOfUnderlying_u()) != "") {
                    var date = new Date(self.DateOfUnderlying_u());
                    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                    var day = date.getDate();
                    var month = lastDay.getMonth() + 13;
                    var year = lastDay.getFullYear();
                    if (month > 12) {
                        month -= 12;
                        year += 1;
                    }
                    var fixLastDay = year + "/" + month + "/" + day;
                    if (!moment(fixLastDay, 'YYYY/MM/DD').isValid()) {
                        var LastDay_ = new Date(year, month, 0);
                        day = LastDay_.getDate();
                    }
                    //fixLastDay = day + "-" + monthNames[month - 1] + "-" + year;
                    self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
                }
            }

            var e = document.getElementById("Amount_u").value;
            e = e.toString().replace(/,/g, '');

            var res = parseInt(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res).toFixed(2));
            self.Amount_u(formatNumber(e));

        },
        write: function (data) {
            var res = parseInt(fromFormat) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res).toFixed(2));
            self.Amount_u(formatNumber(fromFormat));
        }
    }, this);
    self.CaluclateRate_u = ko.computed({
        read: function () {
            var CurrencyID = self.Currency_u().ID();
            var AccountID = self.Selected().Account;
            if (CurrencyID != '' && CurrencyID != undefined) {
                GetRateIDRAmount(CurrencyID);
            } else {
                self.Rate_u(0);
            }
            if (AccountID == '' && AccountID != undefined) {
                alert('test');
            }
        },
        write: function () { }
    });

    self.Proformas = ko.observableArray([new SelectModel('', '')]);
    self.BulkUnderlyings = ko.observableArray([new SelectBulkModel('', '')]); // add 2015.03.09
    self.CodeUnderlying = ko.computed({
        read: function () {
            if (self.UnderlyingDocument_u().ID() != undefined && self.UnderlyingDocument_u().ID() != '') {
                var item = ko.utils.arrayFirst(self.ddlUnderlyingDocument_u(), function (x) {
                    return self.UnderlyingDocument_u().ID() == x.ID();
                }
                )
                if (item != null) {
                    console.log(item.Code() + "|" + item.ID());
                    return item.Code();
                }
                else {
                    return ''
                }
            } else {
                return ''
            }
        },
        write: function (value) {
            return value;
        }
    });

    self.OtherUnderlying_u = ko.observable('');
    self.tmpOtherUnderlying = ko.observable('');
    self.IsSelected_u = ko.observable(false);
    self.LastModifiedBy_u = ko.observable("");
    self.LastModifiedDate_u = ko.observable(new Date());

    // Attach Variable
    self.ID_a = ko.observable(0);
    self.FileName_a = ko.observable('');
    self.DocumentPurpose_a = ko.observable(new DocumentPurposeModel2('', '', ''));
    self.ddlDocumentPurpose_a = ko.observableArray([]);
    self.DocumentType_a = ko.observable(new DocumentTypeModel2('', ''));
    self.ddlDocumentType_a = ko.observableArray([]);
    self.DocumentRefNumber_a = ko.observable("");
    self.LastModifiedBy_a = ko.observable("");
    self.LastModifiedDate_a = ko.observable(new Date());
    self.CustomerUnderlyingMappings_a = ko.observableArray([new CustomerUnderlyingMappingModel('', '', '')]);

    self.Documents = ko.observableArray([]);
    self.DocumentPath_a = ko.observable("");

    // filter Underlying
    self.UnderlyingFilterCIF = ko.observable("");
    self.UnderlyingFilterParentID = ko.observable("");
    self.UnderlyingFilterIsSelected = ko.observable(false);
    self.UnderlyingFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingFilterDocumentType = ko.observable("");
    self.UnderlyingFilterCurrency = ko.observable("");
    self.UnderlyingFilterStatementLetter = ko.observable("");
    self.UnderlyingFilterAmount = ko.observable("");
    self.UnderlyingFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingFilterExpiredDate = ko.observable("");
    self.UnderlyingFilterReferenceNumber = ko.observable("");
    self.UnderlyingFilterSupplierName = ko.observable("");
    self.UnderlyingFilterIsProforma = ko.observable("");
    self.UnderlyingFilterIsAvailable = ko.observable("");
    self.UnderlyingFilterIsExpiredDate = ko.observable("");
    self.UnderlyingFilterIsSelectedBulk = ko.observable(false);
    self.UnderlyingFilterAvailableAmount = ko.observable("");
    self.UnderlyingFilterAttachmentNo = ko.observable("");

    // filter Attach Underlying File
    self.AttachFilterFileName = ko.observable("");
    self.AttachFilterDocumentPurpose = ko.observable("");
    self.AttachFilterModifiedDate = ko.observable("");
    self.AttachFilterDocumentRefNumber = ko.observable("");

    // filter Underlying for Attach
    self.UnderlyingAttachFilterIsSelected = ko.observable("");
    self.UnderlyingAttachFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingAttachFilterDocumentType = ko.observable("");
    self.UnderlyingAttachFilterCurrency = ko.observable("");
    self.UnderlyingAttachFilterStatementLetter = ko.observable("");
    self.UnderlyingAttachFilterAmount = ko.observable("");
    self.UnderlyingAttachFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingAttachFilterExpiredDate = ko.observable("");
    self.UnderlyingAttachFilterReferenceNumber = ko.observable("");
    self.UnderlyingAttachFilterSupplierName = ko.observable("");

    // filter Underlying for Attach
    self.ProformaFilterIsSelected = ko.observable(false);
    self.ProformaFilterUnderlyingDocument = ko.observable("");
    self.ProformaFilterDocumentType = ko.observable("Proforma");
    self.ProformaFilterCurrency = ko.observable("");
    self.ProformaFilterStatementLetter = ko.observable("");
    self.ProformaFilterAmount = ko.observable("");
    self.ProformaFilterDateOfUnderlying = ko.observable("");
    self.ProformaFilterExpiredDate = ko.observable("");
    self.ProformaFilterReferenceNumber = ko.observable("");
    self.ProformaFilterSupplierName = ko.observable("");
    self.ProformaFilterIsProforma = ko.observable(false);

    // filter Underlying for Attach // add 2015.03.09
    self.BulkFilterIsSelected = ko.observable(false);
    self.BulkFilterUnderlyingDocument = ko.observable("");
    self.BulkFilterDocumentType = ko.observable("");
    self.BulkFilterCurrency = ko.observable("");
    self.BulkFilterStatementLetter = ko.observable("");
    self.BulkFilterAmount = ko.observable("");
    self.BulkFilterDateOfUnderlying = ko.observable("");
    self.BulkFilterExpiredDate = ko.observable("");
    self.BulkFilterReferenceNumber = ko.observable("");
    self.BulkFilterSupplierName = ko.observable("");
    self.BulkFilterInvoiceNumber = ko.observable("");
    self.BulkFilterIsBulkUnderlying = ko.observable(false);

    //// start Checked Calculate for Attach Grid and Underlying Grid
    self.TempSelectedAttachUnderlying = ko.observableArray([]);

    //// start Checked Calculate for Underlying Proforma Grid
    self.TempSelectedUnderlyingProforma = ko.observableArray([]);

    //// start Checked Calculate for Underlying Bulk Grid // add 2015.03.09
    self.TempSelectedUnderlyingBulk = ko.observableArray([]);

    //// start Checked Calculate for Underlying Grid
    self.TempSelectedUnderlying = ko.observableArray([]);

    self.TransactionModel().UnderlyingDoc.subscribe(function (UnderlyingDoc) {
        //get underlying code based on selected
        if (UnderlyingDoc > 0) {
            GetUnderlyingDocName(UnderlyingDoc);
        }
    });

    //The Object which stored data entered in the observables
    var CustomerUnderlying = {
        ID: self.ID_u,
        CIF: self.CIF_u,
        CustomerName: self.CustomerName_u,
        UnderlyingDocument: self.UnderlyingDocument_u,
        DocumentType: self.DocumentType_u,
        Currency: self.Currency_u,
        Amount: self.Amount_u,
        Rate: self.Rate_u,
        AmountUSD: self.AmountUSD_u,
        AvailableAmountUSD: self.AvailableAmount_u,
        AttachmentNo: self.AttachmentNo_u,
        StatementLetter: self.StatementLetter_u,
        IsDeclarationOfException: self.IsDeclarationOfException_u,
        StartDate: self.StartDate_u,
        EndDate: self.EndDate_u,
        DateOfUnderlying: self.DateOfUnderlying_u,
        ExpiredDate: self.ExpiredDate_u,
        ReferenceNumber: self.ReferenceNumber_u,
        SupplierName: self.SupplierName_u,
        InvoiceNumber: self.InvoiceNumber_u,
        IsSelectedProforma: self.IsSelectedProforma,
        IsProforma: self.IsProforma_u,
        IsUtilize: self.IsUtilize_u,
        IsEnable: self.IsEnable_u,
        Proformas: self.Proformas,
        IsSelectedBulk: self.IsSelectedBulk,
        IsBulkUnderlying: self.IsBulkUnderlying_u,
        BulkUnderlyings: self.BulkUnderlyings,
        OtherUnderlying: self.OtherUnderlying_u,
        LastModifiedBy: self.LastModifiedBy_u,
        LastModifiedDate: self.LastModifiedDate_u
    };

    var Documents = {
        Documents: self.Documents,
        DocumentPath: self.DocumentPath_a,
        Type: self.DocumentType_a,
        Purpose: self.DocumentPurpose_a
    }

    var CustomerUnderlyingFile = {
        ID: self.ID_a,
        FileName: DocumentModels().FileName,
        DocumentPath: DocumentModels().DocumentPath,
        DocumentPurpose: self.DocumentPurpose_a,
        DocumentType: self.DocumentType_a,
        DocumentRefNumber: self.DocumentRefNumber_a,
        LastModifiedBy: self.LastModifiedBy_a,
        LastModifiedDate: self.LastModifiedDate_a,
        CustomerUnderlyingMappings: self.CustomerUnderlyingMappings_a
    };

    self.onSelectionAttach = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            //self.IsSelectedAttach(false);
            self.TempSelectedAttachUnderlying.remove(data);
            ResetDataAttachment(index, false);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        } else {
            //self.IsSelectedAttach(true);
            self.TempSelectedAttachUnderlying.push({
                ID: item.ID
            });
            ResetDataAttachment(index, true);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        }
    }

    self.onSelectionProforma = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlyingProforma(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            //self.IsSelectedAttach(false);
            self.TempSelectedUnderlyingProforma.remove(data);
            ResetDataProforma(index, false);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        } else {
            //self.IsSelectedAttach(true);
            self.TempSelectedUnderlyingProforma.push({
                ID: item.ID
            });
            ResetDataProforma(index, true);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        }
    };
    // add 2015.03.09
    self.OnCurrencyChange = function (CurrencyID) {
        if (CurrencyID != null) {
            var item = ko.utils.arrayFirst(self.ddlCurrency_u(), function (x) { return CurrencyID == x.ID(); });
            if (item != null) {
                self.Currency_u(new CurrencyModel2(item.ID(), item.Code(), item.Description()));
            }
            if (!self.IsNewDataUnderlying()) {
                GetSelectedBulkID(GetDataBulkUnderlying);
            } else {
                self.TempSelectedUnderlyingBulk([]);
                GetDataBulkUnderlying();
            }

        }
    }
    self.onSelectionBulk = function (index, item) {
        // var temp = self.Amount_u();
        var total = 0;
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlyingBulk(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            //self.IsSelectedAttach(false);
            self.TempSelectedUnderlyingBulk.remove(data);
            ResetBulkData(index, false);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        } else {
            //self.IsSelectedAttach(true);
            self.TempSelectedUnderlyingBulk.push({
                ID: item.ID,
                Amount: item.Amount
            });
            ResetBulkData(index, true);
        }
        for (var i = 0; self.TempSelectedUnderlyingBulk.push() > i; i++) {
            total += self.TempSelectedUnderlyingBulk()[i].Amount;
        }
        if (total > 0) {
            //self.Amount_u(total);
            var e = total;
            var res = parseInt(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res).toFixed(2));
            self.Amount_u(formatNumber(e));
        } else {
            var e = self.tempAmountBulk();
            var res = parseInt(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res).toFixed(2));
            self.Amount_u(formatNumber(e));
            //self.Amount_u(self.tempAmountBulk());
        }
    }

    self.onSelectionUtilize = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
            return x.ID() == item.ID;
        });
        if (data != null) { //uncheck condition
            self.TempSelectedUnderlying.remove(data);
            ResetDataUnderlying(index, false);
            setTotalUtilize();
        } else {
            var statementA = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                return x.StatementLetter() == 1;
            });
            var statementB = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                return x.StatementLetter() == 2;
            });
            var amount = document.getElementById("eqv-usd").value.replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '');
            if (((statementA == null && item.StatementLetter.ID == 2) || (statementB == null && item.StatementLetter.ID == 1))
                //&& parseFloat(self.TransactionModel().utilizationAmount()) < parseFloat(amount)
                ) {
                self.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, false, item.AvailableAmount, item.StatementLetter.ID));
                ResetDataUnderlying(index, true);
                setTotalUtilize();
            } else {
                ResetDataUnderlying(index, false);
            }
        }
        console.log(ko.toJSON(self.TempSelectedUnderlying()));
    };

    // New Data flag
    self.IsNewDataUnderlying = ko.observable(false);


    // grid properties Underlying
    self.UnderlyingGridProperties = ko.observable();
    self.UnderlyingGridProperties(new GridPropertiesModel(GetDataUnderlying));

    // grid properties Attach Grid
    self.AttachGridProperties = ko.observable();
    self.AttachGridProperties(new GridPropertiesModel(GetDataAttachFile));

    // grid properties Underlying File
    self.UnderlyingAttachGridProperties = ko.observable();
    self.UnderlyingAttachGridProperties(new GridPropertiesModel(GetDataUnderlyingAttach));

    // grid properties Proforma
    self.UnderlyingProformaGridProperties = ko.observable();
    self.UnderlyingProformaGridProperties(new GridPropertiesModel(GetDataUnderlyingProforma));

    // grid properties Bulk // 2015.03.09
    self.UnderlyingBulkGridProperties = ko.observable();
    self.UnderlyingBulkGridProperties(new GridPropertiesModel(GetDataBulkUnderlying));

    // set default sorting for Underlying Grid
    self.UnderlyingGridProperties().SortColumn("ID");
    self.UnderlyingGridProperties().SortOrder("ASC");

    // set default sorting for Underlying File Grid
    self.AttachGridProperties().SortColumn("FileName");
    self.AttachGridProperties().SortOrder("ASC");

    // set default sorting for Attach Underlying Grid
    self.UnderlyingAttachGridProperties().SortColumn("ID");
    self.UnderlyingAttachGridProperties().SortOrder("ASC");

    // set default sorting for Proforma grid
    self.UnderlyingProformaGridProperties().SortColumn("IsSelectedBulk");
    self.UnderlyingProformaGridProperties().SortOrder("DESC");

    // set default sorting for Bulk grid //add 2015.03.09
    self.UnderlyingBulkGridProperties().SortColumn("IsSelectedBulk");
    self.UnderlyingBulkGridProperties().SortOrder("DESC");


    // bind clear filters
    self.UnderlyingClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.UnderlyingFilterIsSelected(false);
        self.UnderlyingFilterUnderlyingDocument("");
        self.UnderlyingFilterDocumentType("");
        self.UnderlyingFilterCurrency("");
        self.UnderlyingFilterStatementLetter("");
        self.UnderlyingFilterAmount("");
        self.UnderlyingFilterAvailableAmount("");
        self.UnderlyingFilterDateOfUnderlying("");
        self.UnderlyingFilterExpiredDate("");
        self.UnderlyingFilterReferenceNumber("");
        self.UnderlyingFilterSupplierName("");
        self.UnderlyingFilterIsProforma("");
        self.UnderlyingFilterIsAvailable(false);
        self.UnderlyingFilterIsExpiredDate(false);
        self.UnderlyingFilterIsSelectedBulk(false);

        GetDataUnderlying();
    };

    self.ProformaClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.ProformaFilterIsSelected(false);
        self.ProformaFilterUnderlyingDocument("");
        self.ProformaFilterDocumentType("Proforma");
        self.ProformaFilterCurrency("");
        self.ProformaFilterStatementLetter("");
        self.ProformaFilterAmount("");
        self.ProformaFilterDateOfUnderlying("");
        self.ProformaFilterExpiredDate("");
        self.ProformaFilterReferenceNumber("");
        self.ProformaFilterSupplierName("");
        self.ProformaFilterIsProforma("");

        GetDataUnderlyingProforma();
    };

    //add 2015.03.09
    self.BulkClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.BulkFilterIsSelected(false);
        self.BulkFilterUnderlyingDocument("");
        self.BulkFilterDocumentType("");
        self.BulkFilterCurrency("");
        self.BulkFilterStatementLetter("");
        self.BulkFilterAmount("");
        self.BulkFilterDateOfUnderlying("");
        self.BulkFilterExpiredDate("");
        self.BulkFilterReferenceNumber("");
        self.BulkFilterInvoiceNumber("");
        self.BulkFilterSupplierName("");
        self.BulkFilterIsBulkUnderlying("");

        GetDataBulkUnderlying();
    };

    // bind clear filters
    self.AttachClearFilters = function () {
        self.AttachFilterFileName("");
        self.AttachFilterDocumentPurpose("");
        self.AttachFilterModifiedDate("");
        self.AttachFilterDocumentRefNumber("");

        GetDataAttachFile();
    };

    // bind clear filters
    self.UnderlyingAttachClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.UnderlyingAttachFilterIsSelected(false);
        self.UnderlyingAttachFilterUnderlyingDocument("");
        self.UnderlyingAttachFilterDocumentType("");
        self.UnderlyingAttachFilterCurrency("");
        self.UnderlyingAttachFilterStatementLetter("");
        self.UnderlyingAttachFilterAmount("");
        self.UnderlyingAttachFilterDateOfUnderlying("");
        self.UnderlyingAttachFilterExpiredDate("");
        self.UnderlyingAttachFilterReferenceNumber("");
        self.UnderlyingAttachFilterSupplierName("");

        GetDataUnderlyingAttach();
    };

    // bind get data function to view
    self.GetDataUnderlying = function () {
        GetDataUnderlying();
    };

    self.GetSelectedUtilizeID = function () {
        GetSelectedUtilizeID();
    }

    self.GetDataAttachFile = function () {
        GetDataAttachFile();
    }

    self.GetDataUnderlyingAttach = function () {
        GetDataUnderlyingAttach();
    }

    self.GetDataUnderlyingProforma = function () {
        GetDataUnderlyingProforma();
    }

    // 2015.03.09
    self.GetDataBulkUnderlying = function () {
        GetDataBulkUnderlying();
    }

    self.GetUnderlyingParameters = function (data) {
        GetUnderlyingParameters(data);
    }


    self.save_u = function () {
        self.IsEditTableUnderlying(false);
        var form = $("#frmUnderlying");
        form.validate();

        if (form.valid()) {
            viewModel.Amount_u(viewModel.Amount_u().toString().replace(/,/g, ''));
            CustomerUnderlying.OtherUnderlying = self.CodeUnderlying() == '999' ? self.OtherUnderlying_u : '';
            SetMappingProformas();
            SetMappingBulks(); // add 2015.03.09
            self.AmountUSD_u(self.AmountUSD_u().toString().replace(/,/g, ''));
            CustomerUnderlying.AvailableAmountUSD(self.AmountUSD_u());
            //Ajax call to insert the CustomerUnderlyings
            $.ajax({
                type: "POST",
                url: api.server + api.url.customerunderlying + "/Save",
                data: ko.toJSON(CustomerUnderlying), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {

                        // send notification
                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        // hide current popup window
                        $("#modal-form-Underlying").modal('hide');
                        self.IsEditTableUnderlying(true);
                        // refresh data
                        GetDataUnderlying();
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                        self.IsEditTableUnderlying(true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    if (jqXHR.responseJSON.Message.toLowerCase().startsWith("double underlying")) {
                        ShowNotification("", jqXHR.responseJSON.Message, 'gritter-warning', true);
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    } self.IsEditTableUnderlying(true);
                }
            });
        } else {
            self.IsEditTableUnderlying(true);
        }
    };

    self.save_a = function () {

        self.IsUploading(true);

        var form = $("#frmUnderlying");
        form.validate();
        if (form.valid()) {

            var FxWithUnderlying = (self.TempSelectedAttachUnderlying().length > 0);
            var FxWithNoUnderlying = (self.DocumentPurpose_a().ID() != 2);

            if (FxWithUnderlying || FxWithNoUnderlying) {
                var data = {
                    CIF: CustomerUnderlying.CIF,
                    Name: CustomerUnderlying.CustomerName,
                    Type: ko.utils.arrayFirst(self.ddlDocumentType_a(), function (item) {
                        return item.ID == self.DocumentType_a().ID();
                    }),
                    Purpose: ko.utils.arrayFirst(self.ddlDocumentPurpose_a(), function (item) {
                        return item.ID == self.DocumentPurpose_a().ID();
                    })
                };

                if (FxWithUnderlying) {
                    UploadFileUnderlying(data, Documents, SaveDataFile);
                }
                else if (FxWithNoUnderlying) {
                    self.IsFXTransactionAttach(true);

                    var doc = {
                        ID: 0,
                        Type: data.Type,
                        Purpose: data.Purpose,
                        FileName: Documents.DocumentPath().name,
                        DocumentPath: self.DocumentPath_a(),
                        LastModifiedDate: new Date(),
                        LastModifiedBy: null
                    };

                    //alert(JSON.stringify(self.DocumentPath()))
                    if (doc.Type == null || doc.Purpose == null || doc.DocumentPath == null) {
                        alert("Please complete the upload form fields.");
                    } else {
                        self.Documents.push(doc);

                        $('.remove').click();

                        // hide upload dialog
                        $("#modal-form-Attach").modal('hide');
                        viewModel.IsEditable(true);
                        viewModel.IsUploading(false);
                    }
                }
            }
            else {
                viewModel.IsEditable(true);
                viewModel.IsUploading(false);
                alert('Please select the underlying document');
            }
        } else {
            viewModel.IsEditable(true);
            viewModel.IsUploading(false);
        };
    };

    self.update_u = function () {


        var form = $("#frmUnderlying");
        form.validate();

        if (form.valid()) {
            // hide current popup window
            $("#modal-form-Underlying").modal('hide');
            viewModel.Amount_u(viewModel.Amount_u().toString().replace(/,/g, ''));

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form-Underlying").modal('show');
                } else {
                    CustomerUnderlying.OtherUnderlying = self.CodeUnderlying() == '999' ? self.OtherUnderlying_u : '';
                    SetMappingProformas();
                    SetMappingBulks(); // Add 2015.03.09
                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.customerunderlying + "/" + CustomerUnderlying.ID(),
                        data: ko.toJSON(CustomerUnderlying),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                                // enable Rule validation
                                $("#modal-form-Underlying").modal('hide');

                                // refresh data
                                GetDataUnderlying();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            if (jqXHR.responseJSON.Message.toLowerCase().startsWith("double underlying")) {
                                ShowNotification("", jqXHR.responseJSON.Message, 'gritter-warning', true);
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                            }
                        }
                    });
                }
            });
        }
    };

    self.delete_u = function () {
        // hide current popup window
        $("#modal-form-Underlying").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form-Underlying").modal('show');
            } else {
                // remove check utilize underlying
                RemoveTempUtilizeByID(underlyingID);
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customerunderlying + "/" + CustomerUnderlying.ID(),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                            // refresh data
                            GetDataUnderlying();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    self.cancel_u = function () {

    }

    self.cancel_a = function () {

    }
    function RemoveTempUtilizeByID(underlyingID) {


    }
    self.delete_a = function (item) {
        // hide current popup window
        //$("#modal-form").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                //$("#modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customerunderlyingfile + "/" + item.ID,
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            DeleteFile(item.DocumentPath);
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetDataAttachFile();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    }

    //Function to Display record to be updated
    self.GetUnderlyingSelectedRow = function (data) {
        self.IsNewDataUnderlying(false);

        $("#modal-form-Underlying").modal('show');

        self.ID_u(data.ID);
        self.IsUtilize_u(data.IsUtilize);
        self.CustomerName_u(data.CustomerName);

        self.UnderlyingDocument_u(new UnderlyingDocumentModel2(data.UnderlyingDocument.ID, data.UnderlyingDocument.Name, data.UnderlyingDocument.Code));
        self.OtherUnderlying_u(data.OtherUnderlying);
        self.DocumentType_u(new DocumentTypeModel2(data.DocumentType.ID, data.DocumentType.Name));
        self.Currency_u(new CurrencyModel2(data.Currency.ID, data.Currency.Code, data.Currency.Description));
        self.Amount_u(data.Amount);
        self.Rate_u(data.Rate);
        self.AvailableAmount_u(data.AvailableAmountUSD);
        self.AttachmentNo_u(data.AttachmentNo);
        self.StatementLetter_u(new StatementLetterModel(data.StatementLetter.ID, data.StatementLetter.Name));
        self.IsDeclarationOfException_u(data.IsDeclarationOfException);
        self.StartDate_u(self.LocalDate(data.StartDate, true, false));
        self.EndDate_u(self.LocalDate(data.EndDate, true, false));
        self.DateOfUnderlying_u(self.LocalDate(data.DateOfUnderlying, true, false));
        self.ExpiredDate_u(self.LocalDate(data.ExpiredDate, true, false));
        self.ReferenceNumber_u(data.ReferenceNumber);
        self.SupplierName_u(data.SupplierName);
        self.InvoiceNumber_u(data.InvoiceNumber);
        self.IsProforma_u(data.IsProforma);
        self.IsSelectedProforma(data.IsSelectedProforma);
        self.IsBulkUnderlying_u(data.IsBulkUnderlying); // add 2015.03.09
        self.IsSelectedBulk(data.IsSelectedBulk); // add 2015.03.09
        self.tempAmountBulk(0);//add 2015.03.09
        self.LastModifiedBy_u(data.LastModifiedBy);
        self.LastModifiedDate_u(data.LastModifiedDate);

        //Call Grid Underlying for Check Proforma
        GetSelectedProformaID(GetDataUnderlyingProforma);
        //GetDataUnderlyingProforma();
        //Call Grid Underlying for Check Bullk // add 2015.03.09
        GetSelectedBulkID(GetDataBulkUnderlying);
        //GetDataBulkUnderlying();
        // save temp Amount
        self.tempAmountBulk(data.Amount);
    };

    //Add Attach File
    self.NewAttachFile = function () {
        // show the dialog task form
        // ace file upload


        $("#modal-form-Attach").modal('show');
        self.TempSelectedAttachUnderlying([]);
        self.ID_a(0);
        self.DocumentPurpose_a(new DocumentPurposeModel2('', '', ''));
        self.DocumentType_a(new DocumentTypeModel2('', ''));
        self.DocumentPath_a('');
        GetDataUnderlyingAttach();
    }
    //insert new
    self.NewDataUnderlying = function () {
        if (self.TransactionModel().IsNewCustomer() && !viewModel.IsLoadDraft() && (self.IsFxTransaction() || self.IsFxTransactionToIDR())) {
            ShowNotification("Page Information", NewCustomerFXWarning, "gritter-warning", false);
            self.IsEditable(true);
            return false;
            //NewCustomerFXWarning
        }
        //Remove required filed if show
        $('.form-group').removeClass('has-error').addClass('has-info');
        $('.help-block').remove();

        $("#modal-form-Underlying").modal('show');
        // flag as new Data
        self.IsNewDataUnderlying(true);
        // bind empty data
        self.ID_u(0);
        self.IsUtilize_u(false);
        self.UnderlyingDocument_u(new UnderlyingDocumentModel2('', '', ''));
        self.OtherUnderlying_u('');
        self.DocumentType_u(new DocumentTypeModel2('', ''));
        self.StatementLetter_u(new StatementLetterModel('', ''));
        self.Currency_u(new CurrencyModel2('', '', ''));
        self.Amount_u(0);
        self.AvailableAmount_u(0);
        self.Rate_u(0);
        self.AttachmentNo_u('');
        self.IsDeclarationOfException_u(false);
        self.StartDate_u('');
        self.EndDate_u('');
        self.DateOfUnderlying_u('');
        self.ExpiredDate_u('');
        self.ReferenceNumber_u('');
        self.SupplierName_u('');
        self.InvoiceNumber_u('');
        self.IsProforma_u(false);
        self.IsSelectedProforma(false);
        self.IsBulkUnderlying_u(false); //add 2015.03.09
        self.IsSelectedBulk(false); //add 2015.03.09
        //Call Grid Underlying for Check Proforma
        GetDataUnderlyingProforma();
        GetDataBulkUnderlying();
        self.TempSelectedUnderlyingProforma([]);
        self.TempSelectedUnderlyingBulk([]);
        setStatementA();
        setRefID();
    };


    function GetUnderlyingDocName(UnderlyingDoc) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.underlyingdoc + "/" + UnderlyingDoc.ID,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if (data != null) {
                        self.isNewUnderlying(false); //return default disabled

                        // available type to input for new underlying name if code is 999
                        if (data.Code == '999') {
                            self.TransactionModel().UnderlyingDoc().Description('');
                            self.isNewUnderlying(true);
                            $('#book-underlying-desc').focus();
                            $('#book-underlying-desc').attr('data-rule-required', true);
                        }
                        else {
                            self.isNewUnderlying(false);
                            self.TransactionModel().OtherUnderlyingDoc('');

                            $('#book-underlying-desc').attr('data-rule-required', false);
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function ResetDataAttachment(index, isSelect) {
        self.CustomerAttachUnderlyings()[index].IsSelectedAttach = isSelect;
        var dataRows = self.CustomerAttachUnderlyings().slice(0);
        self.CustomerAttachUnderlyings([]);
        self.CustomerAttachUnderlyings(dataRows);
    }

    function ResetDataProforma(index, isSelect) {
        self.CustomerUnderlyingProformas()[index].IsSelectedProforma = isSelect;
        var dataRows = self.CustomerUnderlyingProformas().slice(0);
        self.CustomerUnderlyingProformas([]);
        self.CustomerUnderlyingProformas(dataRows);
    }

    function ResetBulkData(index, isSelect) { //add 2015.03.09
        self.CustomerBulkUnderlyings()[index].IsSelectedBulk = isSelect;
        var dataRows = self.CustomerBulkUnderlyings().slice(0);
        self.CustomerBulkUnderlyings([]);
        self.CustomerBulkUnderlyings(dataRows);
    }

    function ResetDataUnderlying(index, isSelect) {
        self.CustomerUnderlyings()[index].IsEnable = isSelect;
        var dataRows = self.CustomerUnderlyings().slice(0);
        self.CustomerUnderlyings([]);
        self.CustomerUnderlyings(dataRows);
    }

    function SetSelectedProforma(data) {
        for (var i = 0; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlyingProforma(), function (item) {
                return item.ID == data[i].ID;
            });
            if (selected != null) {
                data[i].IsSelectedProforma = true;
            }
            else {
                data[i].IsSelectedProforma = false;
            }
        }
    }

    function SetSelectedBulk(data) { //add 2015.03.09

        for (var i = 0 ; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlyingBulk(), function (item) {
                return item.ID == data[i].ID;
            });
            if (selected != null) {
                data[i].IsSelectedBulk = true;
            }
            else {
                data[i].IsSelectedBulk = false;
            }
        }
        return data;
    }


    function TotalUtilize() {
        var total = 0;
        ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (item) {
            if (item.Enable() == false)
                total += parseFloat(ko.utils.unwrapObservable(item.USDAmount()));
        });
        var fixTotal = parseFloat(total).toFixed(2);
        return fixTotal;
    }

    function GetRateIDRAmount(CurrencyID) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
            params: {
            },
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    self.Rate_u(data.RupiahRate);
                    //if(CurrencyID!=1){ // if not USD
                    var fromFormat = document.getElementById("Amount_u").value;
                    fromFormat = fromFormat.toString().replace(/,/g, '');
                    res = parseFloat(fromFormat) * data.RupiahRate / idrrate;
                    res = Math.round(res * 100) / 100;
                    res = isNaN(res) ? 0 : res; //avoid NaN
                    self.AmountUSD_u(parseFloat(res).toFixed(2));

                    self.Amount_u(formatNumber(fromFormat));
                }
            }
        });
    }

    function GetUSDAmount(currencyID, amount, IdUnderlying) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetAmountUSD/" + currencyID + "/" + amount,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlying.push(new SelectUtillizeModel(IdUnderlying, false, data, ''));
                    var total = TotalUtilize();
                    viewModel.TransactionModel().utilizationAmount(parseFloat(total));
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }


    function SetSelectedUnderlying(data) {
        for (var i = 0; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (item) {
                return item.ID() == data[i].ID;
            });
            if (selected != null) {
                data[i].IsEnable = true;
            }
            else {
                data[i].IsEnable = false;
            }
        }
        return data;
    }

    function SetMappingAttachment() {
        CustomerUnderlyingFile.CustomerUnderlyingMappings([]);
        for (var i = 0 ; i < self.TempSelectedAttachUnderlying().length; i++) {
            CustomerUnderlyingFile.CustomerUnderlyingMappings.push(new CustomerUnderlyingMappingModel(0, self.TempSelectedAttachUnderlying()[i].ID, customerNameData + '-(A)'));
        }
    }

    function SetMappingProformas() {
        CustomerUnderlying.Proformas([]);
        for (var i = 0 ; i < self.TempSelectedUnderlyingProforma().length; i++) {
            CustomerUnderlying.Proformas.push(new SelectModel(cifData, self.TempSelectedUnderlyingProforma()[i].ID));
        }
    }
    function SetMappingBulks() { //add 2015.03.09
        CustomerUnderlying.BulkUnderlyings([]);
        for (var i = 0 ; i < self.TempSelectedUnderlyingBulk().length; i++) {
            CustomerUnderlying.BulkUnderlyings.push(new SelectBulkModel(cifData, self.TempSelectedUnderlyingBulk()[i].ID));
        }
    }
    function setTotalUtilize() {
        var total = 0;
        ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (item) {
            total += parseFloat(ko.utils.unwrapObservable(item.USDAmount()));
        });
        viewModel.TransactionModel().utilizationAmount(parseFloat(total).toFixed(2));
    }

    function setStatementA() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetStatementA/" + cifData,
            data: null, //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            }, success: function (data) {
                if (data.IsStatementA) {
                    self.StatementLetter_u(new StatementLetterModel(2, 'Statement B'));
                    self.IsStatementA(false);
                }
            }
        });
    }

    function setRefID() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetRefID/" + cifData,
            data: null, //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            }, success: function (data) {
                self.ReferenceNumber_u(customerNameData + '-' + data.ID);
            }
        });
    }

    function SaveDataFile() {

        SetMappingAttachment();
        //Ajax call to insert the CustomerUnderlyings
        CustomerUnderlyingFile.FileName = DocumentModels().FileName;
        CustomerUnderlyingFile.DocumentPath = DocumentModels().DocumentPath;
        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlyingfile,
            data: ko.toJSON(CustomerUnderlyingFile), //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    // send notification
                    ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                    // hide current popup window
                    $("#modal-form-Attach").modal('hide');
                    viewModel.IsUploading(false);

                    // refresh data
                    GetDataAttachFile();
                } else {
                    viewModel.IsUploading(false);
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                viewModel.IsUploading(false);
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });


    }

    // Function to Read parameter Underlying
    function GetUnderlyingParameters(data) {
        var underlying = data['UnderltyingDoc'];
        for (var i = 0; i < underlying.length; i++) {
            self.ddlUnderlyingDocument_u.push(new UnderlyingDocumentModel2(underlying[i].ID, underlying[i].Name, underlying[i].Code));
        }
        self.ddlDocumentType_u(data['DocType']);
        self.ddlDocumentType_a(data['DocType']);

        self.ddlCurrency_u([]);
        ko.utils.arrayForEach(data['Currency'], function (item) {
            self.ddlCurrency_u.push(new CurrencyModel2(item.ID, item.Code, item.Description));
        });
        //self.ddlCurrency_u(data['Currency']);
        self.ddlDocumentPurpose_a(data['PurposeDoc']);
        ko.utils.arrayForEach(data['StatementLetter'], function (item) {
            if (item.ID == 1 || item.ID == 2) {
                self.ddlStatementLetter_u.push(item);
            }
        });
        //self.ddlStatementLetter_u(data['StatementLetter']);
    }
    //Function to Read All Customers Underlying
    function GetDataUnderlying() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Underlying",
            params: {
                page: self.UnderlyingGridProperties().Page(),
                size: self.UnderlyingGridProperties().Size(),
                sort_column: self.UnderlyingGridProperties().SortColumn(),
                sort_order: self.UnderlyingGridProperties().SortOrder()
            },
            token: accessToken
        };

        self.UnderlyingFilterIsAvailable(true);
        self.UnderlyingFilterIsExpiredDate(true);
        // get filtered columns
        var filters = GetUnderlyingFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        }
    }

    // On success GetData callback
    function OnSuccessGetDataUnderlying(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            var dataunderlying = SetSelectedUnderlying(data.Rows);
            self.CustomerUnderlyings([]);
            self.CustomerUnderlyings(dataunderlying);
            //self.CustomerUnderlyings(data.Rows);
            self.UnderlyingGridProperties().Page(data['Page']);
            self.UnderlyingGridProperties().Size(data['Size']);
            self.UnderlyingGridProperties().Total(data['Total']);
            self.UnderlyingGridProperties().TotalPages(Math.ceil(self.UnderlyingGridProperties().Total() / self.UnderlyingGridProperties().Size()));
            //self.TempSelectedUnderlying([]);
            viewModel.TransactionModel().utilizationAmount(0.00);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }


    //Function to Read All Customers Underlying File
    function GetDataAttachFile() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlyingfile,
            params: {
                page: self.AttachGridProperties().Page(),
                size: self.AttachGridProperties().Size(),
                sort_column: self.AttachGridProperties().SortColumn(),
                sort_order: self.AttachGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        }
    }


    // On success GetData callback
    function OnSuccessGetDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerUnderlyingFiles(data.Rows);

            self.AttachGridProperties().Page(data['Page']);
            self.AttachGridProperties().Size(data['Size']);
            self.AttachGridProperties().Total(data['Total']);
            self.AttachGridProperties().TotalPages(Math.ceil(self.AttachGridProperties().Total() / self.AttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }


    //Function to Read All Customers Underlying for Attach File
    function GetDataUnderlyingAttach() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Attach",
            params: {
                page: self.UnderlyingAttachGridProperties().Page(),
                size: self.UnderlyingAttachGridProperties().Size(),
                sort_column: self.UnderlyingAttachGridProperties().SortColumn(),
                sort_order: self.UnderlyingAttachGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetUnderlyingAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingDataAttachFile, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingDataAttachFile, OnError, OnAlways);
        }
    }

    // On success GetData callback
    function OnSuccessGetUnderlyingDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {

            for (var i = 0 ; i < data.Rows.length; i++) {
                var selected = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (item) {
                    return item.ID == data.Rows[i].ID;
                });
                if (selected != null) {
                    data.Rows[i].IsSelectedAttach = true;
                }
                else {
                    data.Rows[i].IsSelectedAttach = false;
                }
            }
            self.CustomerAttachUnderlyings(data.Rows);

            self.UnderlyingAttachGridProperties().Page(data['Page']);
            self.UnderlyingAttachGridProperties().Size(data['Size']);
            self.UnderlyingAttachGridProperties().Total(data['Total']);
            self.UnderlyingAttachGridProperties().TotalPages(Math.ceil(self.UnderlyingAttachGridProperties().Total() / self.UnderlyingAttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }


    //Function to Read All Customers Underlying Proforma
    function GetDataUnderlyingProforma() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Proforma",
            params: {
                page: self.UnderlyingProformaGridProperties().Page(),
                size: self.UnderlyingProformaGridProperties().Size(),
                sort_column: self.UnderlyingProformaGridProperties().SortColumn(),
                sort_order: self.UnderlyingProformaGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetUnderlyingProformaFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingProforma, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingProforma, OnError, OnAlways);
        }
    }

    //Function to Read All Customers Bulk Underlying // add 2015.03.09
    function GetDataBulkUnderlying() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Bulk",
            params: {
                page: self.UnderlyingBulkGridProperties().Page(),
                size: self.UnderlyingBulkGridProperties().Size(),
                sort_column: self.UnderlyingBulkGridProperties().SortColumn(),
                sort_order: self.UnderlyingBulkGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns

        var filters = GetBulkUnderlyingFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingBulk, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingBulk, OnError, OnAlways);
        }
    }

    // On success GetData callback
    function OnSuccessGetUnderlyingProforma(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            SetSelectedProforma(data.Rows);

            self.CustomerUnderlyingProformas(data.Rows);

            self.UnderlyingProformaGridProperties().Page(data['Page']);
            self.UnderlyingProformaGridProperties().Size(data['Size']);
            self.UnderlyingProformaGridProperties().Total(data['Total']);
            self.UnderlyingProformaGridProperties().TotalPages(Math.ceil(self.UnderlyingProformaGridProperties().Total() / self.UnderlyingProformaGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback // Add 2015.03.09
    function OnSuccessGetUnderlyingBulk(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            var databulk = SetSelectedBulk(data.Rows);

            self.CustomerBulkUnderlyings(databulk);

            self.UnderlyingBulkGridProperties().Page(data['Page']);
            self.UnderlyingBulkGridProperties().Size(data['Size']);
            self.UnderlyingBulkGridProperties().Total(data['Total']);
            self.UnderlyingBulkGridProperties().TotalPages(Math.ceil(self.UnderlyingBulkGridProperties().Total() / self.UnderlyingBulkGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    function GetSelectedUtilizeID() {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });

        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SelectedUtilizeID",
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON(filters),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlying([]);
                    for (var i = 0; i < data.length; i++) {
                        self.TempSelectedUnderlying.push(new SelectUtillizeModel(data[i].ID, true, 0, data[i].StatementLetter.ID));
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetSelectedProformaID(callback) {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.ProformaFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.ProformaFilterIsSelected() });
        if (self.ProformaFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.ProformaFilterUnderlyingDocument() });
        if (self.ProformaFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.ProformaFilterDocumentType() });

        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SelectedProformaID",
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON(filters),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlyingProforma([]);
                    for (var i = 0; i < data.length; i++) {
                        self.TempSelectedUnderlyingProforma.push({ ID: data[i].ID });
                    }
                    if (callback != null) {
                        callback();
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    // Function to get All selected Bulk Underlying // add 2015.03.09
    function GetSelectedBulkID(callback) {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: self.CIF_u() });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.BulkFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.BulkFilterIsSelected() });
        if (self.BulkFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.BulkFilterUnderlyingDocument() });
        if (self.BulkFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.BulkFilterDocumentType() });


        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SelectedBulkID",
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON(filters),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlyingBulk([]);
                    for (var i = 0; i < data.length; i++) {
                        self.TempSelectedUnderlyingBulk.push({ ID: data[i].ID, Amount: data[i].Amount });
                    }
                    if (callback != null) {
                        callback();
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    // Get filtered columns value
    function GetUnderlyingFilteredColumns() {
        // define filter

        var filters = [];
        self.CIF_u(cifData);

        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingFilterIsSelected() });
        if (self.UnderlyingFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingFilterUnderlyingDocument() });
        if (self.UnderlyingFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingFilterDocumentType() });
        if (self.UnderlyingFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingFilterCurrency() });
        if (self.UnderlyingFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingFilterStatementLetter() });
        if (self.UnderlyingFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingFilterAmount() });
        if (self.UnderlyingFilterAvailableAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingFilterAvailableAmount() });
        if (self.UnderlyingFilterAttachmentNo() != "") filters.push({ Field: 'AttachmentNo', Value: self.UnderlyingFilterAttachmentNo() });
        if (self.UnderlyingFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingFilterDateOfUnderlying() });
        if (self.UnderlyingFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingFilterExpiredDate() });
        if (self.UnderlyingFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingFilterReferenceNumber() });
        if (self.UnderlyingFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingFilterSupplierName() });
        if (self.UnderlyingFilterIsAvailable() != "") filters.push({ Field: 'IsAvailable', Value: self.UnderlyingFilterIsAvailable() });
        if (self.UnderlyingFilterIsExpiredDate() != "") filters.push({ Field: 'IsExpiredDate', Value: self.UnderlyingFilterIsExpiredDate() });
        if (self.UnderlyingFilterIsSelectedBulk() == false) filters.push({ Field: 'IsSelectedBulk', Value: self.UnderlyingFilterIsSelectedBulk() });
        return filters;
    };

    // Get filtered columns value

    function GetUnderlyingAttachFilteredColumns() {
        var filters = [];
        self.UnderlyingFilterIsSelected(true); // flag for get all datas
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingAttachFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingAttachFilterIsSelected() });
        if (self.UnderlyingAttachFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingAttachFilterUnderlyingDocument() });
        if (self.UnderlyingAttachFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingFilterDocumentType() });
        if (self.UnderlyingAttachFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingFilterCurrency() });
        if (self.UnderlyingAttachFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingFilterStatementLetter() });
        if (self.UnderlyingAttachFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingFilterAmount() });
        if (self.UnderlyingAttachFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingFilterDateOfUnderlying() });
        if (self.UnderlyingAttachFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingFilterExpiredDate() });
        if (self.UnderlyingAttachFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingFilterReferenceNumber() });
        if (self.UnderlyingAttachFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingFilterSupplierName() });

        return filters;
    }

    // Get filtered columns value
    function GetAttachFilteredColumns() {
        // define filter

        var filters = [];
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.AttachFilterFileName() != "") filters.push({ Field: 'FileName', Value: self.AttachFilterFileName() });
        if (self.AttachFilterDocumentPurpose() != "") filters.push({ Field: 'DocumentPurpose', Value: self.AttachFilterDocumentPurpose() });
        if (self.AttachFilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.AttachFilterModifiedDate() });
        if (self.AttachFilterDocumentRefNumber() != "") filters.push({ Field: 'DocumentRefNumber', Value: self.AttachFilterDocumentRefNumber() });

        return filters;
    };

    // Get filtered columns value
    function GetUnderlyingProformaFilteredColumns() {
        // define filter
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.ProformaFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.ProformaFilterIsSelected() });
        if (self.ProformaFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.ProformaFilterUnderlyingDocument() });
        if (self.ProformaFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.ProformaFilterDocumentType() });
        if (self.ProformaFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.ProformaFilterCurrency() });
        if (self.ProformaFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.ProformaFilterStatementLetter() });
        if (self.ProformaFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.ProformaFilterAmount() });
        if (self.ProformaFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.ProformaFilterDateOfUnderlying() });
        if (self.ProformaFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.ProformaFilterExpiredDate() });
        if (self.ProformaFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.ProformaFilterReferenceNumber() });
        if (self.ProformaFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.ProformaFilterSupplierName() });

        return filters;
    };


    // Get filtered columns value // add 2015.03.09
    function GetBulkUnderlyingFilteredColumns() {
        // define filter
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.BulkFilterCurrency(self.Currency_u().Code() != "" || self.Currency_u().Code() != null ? self.Currency_u().Code() : "xyz");
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: self.CIF_u() });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.BulkFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.BulkFilterIsSelected() });
        if (self.BulkFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.BulkFilterUnderlyingDocument() });
        if (self.BulkFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.BulkFilterDocumentType() });
        if (self.BulkFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.BulkFilterCurrency() });
        if (self.BulkFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.BulkFilterStatementLetter() });
        if (self.BulkFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.BulkFilterAmount() });
        if (self.BulkFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.BulkFilterDateOfUnderlying() });
        if (self.BulkFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.BulkFilterExpiredDate() });
        if (self.BulkFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.BulkFilterReferenceNumber() });
        if (self.BulkFilterInvoiceNumber() != "") filters.push({ Field: 'InvoiceNumber', Value: self.BulkFilterInvoiceNumber() });
        if (self.BulkFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.BulkFilterSupplierName() });

        return filters;
    };

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }

    //--------------------------- set Underlying Function & variable end
    self.GetCustomerData = function (cif, acc) {
        $.ajax({
            //type: "DELETE",
            url: api.server + api.url.customer + "/" + cif,
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TransactionModel().Customer(data);
                    self.Selected().Account(acc);
                    self.Selected().BizSegment(data.BizSegment.ID);
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
    /*
     self.LoadDraft = LoadDraft;*/
};

// Knockout View Model
var viewModel = new ViewModel();

function ResetBeneBank() {
    //reset bene bank mapped data
    viewModel.TransactionModel().Bank().SwiftCode('');
    viewModel.TransactionModel().Bank().BankAccount('');
    viewModel.TransactionModel().Bank().Code('999');
    viewModel.IsOtherBank(false);
    viewModel.TransactionModel().IsOtherBeneBank(true);
    $('#bank-code').val('');
    $('#bene-acc-number').val('');
}

$.holdReady(true);
var intervalRoleName = setInterval(function () {
    if (Object.keys(Const_RoleName).length != 0) {
        $.holdReady(false);
        clearInterval(intervalRoleName);
    }
}, 100);

// jQuery Init
//$(document).on("RoleNameReady", function() {
$(document).ready(function () {
    $.fn.ForceNumericOnly = function () {

        return this.each(function () {

            $(this).keydown(function (e) {

                var key = e.charCode || e.keyCode || 0;

                return (

    				key == 8 ||
    				key == 9 ||
    				key == 13 ||
    				key == 46 ||
    				key == 110 ||
    				key == 190 ||
    				(key >= 35 && key <= 40) ||
    				(key >= 48 && key <= 57) ||
    				(key >= 96 && key <= 105));

            });

        });

    };
    $(".input-numericonly").ForceNumericOnly();

    $('.btn').each(function () {
        dataBind = $(this).attr('data-bind');
        if (dataBind == "click: SaveAsDraft, disable: !IsEditable(), visible: IsRole('DBS Admin')") {
            $(this).attr('data-bind', "click: SaveAsDraft, disable: !IsEditable(), visible: true")
        }
    });

    /// block enter key from user to prevent submitted data.
    $("input").keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

            return false;
        }
    });

    $('#bank-name').keyup(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 46 || code == 8) {
            if (event.target.value == '') {
                viewModel.TransactionModel().Bank().SwiftCode = '';
                viewModel.TransactionModel().Bank().BankAccount = '';
                viewModel.TransactionModel().Bank().Code = '999';
                viewModel.IsOtherBank(false);
                viewModel.TransactionModel().IsOtherBeneBank(true);
                $('#bank-code').val('');
            }
        }
    });

    //$('#modal-form-Underlying')
    //console.log(viewModel.IsRole('DBS Admin'));
    $('#aspnetForm').after('<form id="frmUnderlying"></form>');
    $("#modal-form-Underlying").appendTo("#frmUnderlying");
    $("#modal-form-Attach").appendTo("#frmUnderlying");
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    // datepicker
    $('.date-picker').datepicker({
        autoclose: true,
        onSelect: function () {
            this.focus();
        }
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    // validation
    //$('#aspnetForm1').validate({
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        focusCleanup: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });

    $('#frmUnderlying').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        focusCleanup: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });

    // autocomplete
    $("#customer-name").autocomplete({
        source: function (request, response) {
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.customer + "/Search",
                data: {
                    query: request.term,
                    limit: 20
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
        },
        minLength: 2,
        select: function (event, ui) {
            // set customer data model
            if (ui.item.data != undefined || ui.item.data != null) {
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                viewModel.TransactionModel().Customer(ko.mapping.toJS(ui.item.data, mapping));
                viewModel.Selected().BizSegment(ui.item.data.BizSegment.ID);
                cifData = ui.item.data.CIF;
                customerNameData = ui.item.data.Name;
                viewModel.GetDataUnderlying();
                viewModel.GetDataAttachFile();
                viewModel.IsStatementA(true); //?
                viewModel.TransactionModel().utilizationAmount(0.00);
                PPUModel.cif = cifData;
                GetTotalPPU(PPUModel, OnSuccessTotal, OnErrorDeal);

                viewModel.Selected().Account(null);
                //viewModel.TransactionModel().Account().Currency().Code(null);
                viewModel.Selected().DebitCurrency(null);
                viewModel.IsEmptyAccountNumber(false);
                viewModel.TransactionModel().OtherAccountNumber(null);
                viewModel.TransactionModel().IsOtherAccountNumber(false);
                //Basri
                //var objLength = 0;
                //objLength = $('#debit-acc-number option').length + 1;

                if ($('#debit-acc-number option[value=-]').length <= 0) {
                    $('#debit-acc-number').append('<option value=->-</option>');

                }
                viewModel.Selected().Channel(1)

            }
            else
                viewModel.TransactionModel().Customer(null);
        }
    });

    viewModel.SetBankAutoCompleted();
    //viewModel.SetLLDAutoCompleted();

    // ace file upload
    $('#document-path').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });

    $('#document-path-upload').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });

    // Knockout Datepicker custom binding handler
    ko.bindingHandlers.datepicker = {
        init: function (element) {
            $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).datepicker("refresh");
        }
    };

    // Knockout custom file handler
    ko.bindingHandlers.file = {
        init: function (element, valueAccessor) {
            $(element).change(function () {
                var file = this.files[0];

                if (ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };

    // Dependant Observable for dropdown auto fill
    ko.dependentObservable(function () {
        if (viewModel.Selected().Account() == '-') {
            viewModel.TransactionModel().IsOtherAccountNumber(true);
            viewModel.IsEmptyAccountNumber(true);
        } else {
            viewModel.TransactionModel().IsOtherAccountNumber(false);
            $('#debitroom').next(".help-block").addClass('hidden');
        }

        var IsCurrencyChanged;
        var product = ko.utils.arrayFirst(viewModel.Parameter().Products(), function (item) {
            return item.ID == viewModel.Selected().Product();
        });


        
        var account = ko.utils.arrayFirst(viewModel.TransactionModel().Customer().Accounts, function (item) {
            return item.AccountNumber == viewModel.Selected().Account();
        });

        var debitcurrency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) {
            if (viewModel.Selected().Account() == '-') {
                viewModel.TransactionModel().IsOtherAccountNumber(false);
                return item.ID == viewModel.Selected().DebitCurrency();
            } else {
                viewModel.TransactionModel().IsOtherAccountNumber(true);
                if (account != null) {
                    return item.ID == account.Currency.ID;
                }
            }
        });

        var channel = ko.utils.arrayFirst(viewModel.Parameter().Channels(), function (item) {
            return item.ID == viewModel.Selected().Channel();
        });
        var bizSegment = ko.utils.arrayFirst(viewModel.Parameter().BizSegments(), function (item) {
            return item.ID == viewModel.Selected().BizSegment();
        });
      
        var lld = ko.utils.arrayFirst(viewModel.Parameter().LLDs(), function (item) {
            return item.ID == viewModel.Selected().LLD();
        });
        var underlyingDoc = ko.utils.arrayFirst(viewModel.Parameter().UnderlyingDocs(), function (item) {
            return item.ID == viewModel.Selected().UnderlyingDoc();
        });

        var docPurpose = ko.utils.arrayFirst(viewModel.Parameter().DocumentPurposes(), function (item) {
            return item.ID == viewModel.Selected().DocumentPurpose();
        });
        var docType = ko.utils.arrayFirst(viewModel.Parameter().DocumentTypes(), function (item) {
            return item.ID == viewModel.Selected().DocumentType();
        });
        var bankCharges = ko.utils.arrayFirst(viewModel.Parameter().BankCharges(), function (item) {
            return item.ID == viewModel.Selected().BankCharges();
        });
        var agentCharges = ko.utils.arrayFirst(viewModel.Parameter().AgentCharges(), function (item) {
            return item.ID == viewModel.Selected().AgentCharges();
        });
        var productType = ko.utils.arrayFirst(viewModel.Parameter().ProductType(), function (item) {
            if (viewModel.Selected().Account() == '-')
            {
                viewModel.TransactionModel().IsOtherAccountNumber(true);
                viewModel.IsEmptyAccountNumber(false);
            } else {
                viewModel.TransactionModel().IsOtherAccountNumber(false);
                viewModel.IsEmptyAccountNumber(true);
            } return item.ID == viewModel.Selected().ProductType();
        });

        if (viewModel.DocumentPurpose_a() != null) {
            if (viewModel.DocumentPurpose_a().ID() == 2) {
                viewModel.SelectingUnderlying(true);
            }
            else {
                viewModel.SelectingUnderlying(false);
            }
        }
        if (account != null) {
            viewModel.TransactionModel().Account(account);
        }
         if (productType != null) {
            if (viewModel.TransactionModel().IsNewCustomer()) {
                var new_currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().NewCustomer().Currency(); });
                if (new_currency != null) viewModel.TransactionModel().Account().Currency = new_currency;
            }

            viewModel.TransactionModel().ProductType(productType);

            if (!viewModel.TransactionModel().IsOtherAccountNumber())
                IsCurrencyChanged = viewModel.TransactionModel().Account().Currency.Code;
            else
                IsCurrencyChanged = viewModel.TransactionModel().DebitCurrency().Code;

            var IsFxTransaction = (viewModel.TransactionModel().Currency().Code != 'IDR' && IsCurrencyChanged == 'IDR');
            var IsFxTransactionToIDR = (viewModel.TransactionModel().Currency().Code == 'IDR' && IsCurrencyChanged != 'IDR' &&
                viewModel.TransactionModel().ProductType().IsFlowValas == true && parseFloat(viewModel.TransactionModel().AmountUSD()) >= TotalPPUModel.FCYIDRTrashHold);
            var IsTransactionIDRToFCY = (viewModel.TransactionModel().Currency().Code == 'IDR' && IsCurrencyChanged != 'IDR');

            viewModel.IsFxTransaction(IsFxTransaction);
            viewModel.IsFxTransactionToIDR(IsFxTransactionToIDR);
            viewModel.IsTransactionIDRToFCY(IsTransactionIDRToFCY);
        }

        if (underlyingDoc != null) {
            viewModel.TransactionModel().UnderlyingDoc(underlyingDoc);
            viewModel.isNewUnderlying(false); //return default disabled

            // available type to input for new underlying name if code is 999
            if (underlyingDoc.Code == '999') {
                self.TransactionModel().OtherUnderlyingDoc('');
                self.isNewUnderlying(true);
                $('#underlying-desc').focus();

                $('#underlying-desc').attr('data-rule-required', true);
            }
            else {
                viewModel.isNewUnderlying(false);
                //viewModel.TransactionModel().OtherUnderlyingDoc('');

                $('#underlying-desc').attr('data-rule-required', false);
            }
        }



        var currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) {
            if (viewModel.Selected().Account() == '-') { 
                viewModel.TransactionModel().IsOtherAccountNumber(true);
                viewModel.IsEmptyAccountNumber(true);
            } else {
                viewModel.TransactionModel().IsOtherAccountNumber(false);
                viewModel.IsEmptyAccountNumber(false);
            }
            return item.ID == viewModel.Selected().Currency();
        });

        if (debitcurrency != null) {
            if (!viewModel.TransactionModel().IsOtherAccountNumber())
                IsCurrencyChanged = viewModel.TransactionModel().Account().Currency.Code;
            else
                //IsCurrencyChanged = viewModel.TransactionModel().DebitCurrency().Code;
                IsCurrencyChanged = viewModel.TransactionModel().DebitCurrency().Code;
            if (viewModel.TransactionModel().IsNewCustomer()) {
                var new_currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().NewCustomer().Currency(); });
                if (new_currency != null) viewModel.TransactionModel().Account().Currency = new_currency;
            }

            viewModel.TransactionModel().DebitCurrency(debitcurrency);
            //viewModel.GetRateAmount(currency.ID);

            if (!viewModel.TransactionModel().IsOtherAccountNumber())
                IsCurrencyChanged = viewModel.TransactionModel().Account().Currency.Code;
            else
                IsCurrencyChanged = viewModel.TransactionModel().DebitCurrency().Code;

            var IsFxTransaction = (viewModel.TransactionModel().Currency().Code != 'IDR' && IsCurrencyChanged == 'IDR');
            var IsFxTransactionToIDR = (viewModel.TransactionModel().Currency().Code == 'IDR' && IsCurrencyChanged != 'IDR' &&
                viewModel.TransactionModel().ProductType().IsFlowValas == true && parseFloat(viewModel.TransactionModel().AmountUSD()) >= TotalPPUModel.FCYIDRTrashHold);
            var IsTransactionIDRToFCY = (viewModel.TransactionModel().Currency().Code == 'IDR' && IsCurrencyChanged != 'IDR');
            viewModel.IsFxTransaction(IsFxTransaction);
            viewModel.IsFxTransactionToIDR(IsFxTransactionToIDR);
            viewModel.IsTransactionIDRToFCY(IsTransactionIDRToFCY);
        }

        if (currency != null) {
            if (viewModel.TransactionModel().IsNewCustomer()) {
                var new_currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().NewCustomer().Currency(); });
                if (new_currency != null) viewModel.TransactionModel().Account().Currency = new_currency;
            }
            viewModel.TransactionModel().Currency(currency);
            viewModel.GetRateAmount(currency.ID);

            if (!viewModel.TransactionModel().IsOtherAccountNumber())
                IsCurrencyChanged = viewModel.TransactionModel().Account().Currency.Code;
            else
                IsCurrencyChanged = viewModel.TransactionModel().DebitCurrency().Code;

            var IsFxTransaction = (viewModel.TransactionModel().Currency().Code != 'IDR' && IsCurrencyChanged == 'IDR');
            var IsFxTransactionToIDR = (viewModel.TransactionModel().Currency().Code == 'IDR' && IsCurrencyChanged != 'IDR' &&
                viewModel.TransactionModel().ProductType().IsFlowValas == true && parseFloat(viewModel.TransactionModel().AmountUSD()) >= TotalPPUModel.FCYIDRTrashHold);
            var IsTransactionIDRToFCY = (viewModel.TransactionModel().Currency().Code == 'IDR' && IsCurrencyChanged != 'IDR');
            viewModel.IsFxTransaction(IsFxTransaction);
            viewModel.IsFxTransactionToIDR(IsFxTransactionToIDR);
            viewModel.IsTransactionIDRToFCY(IsTransactionIDRToFCY);
        }
        if (channel != null) viewModel.TransactionModel().Channel(channel);
        if (bizSegment != null) viewModel.TransactionModel().BizSegment(bizSegment);
        if (lld != null) viewModel.TransactionModel().LLD(lld);
        if (account != null) {
            if (viewModel.TransactionModel().IsNewCustomer()) {
                var new_currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().NewCustomer().Currency(); });
                if (new_currency != null) viewModel.TransactionModel().Account().Currency = new_currency;
            }
            viewModel.TransactionModel().Account(account);

            if (!viewModel.TransactionModel().IsOtherAccountNumber()) {
                IsCurrencyChanged = viewModel.TransactionModel().Account().Currency.Code;
            }
            else {
                IsCurrencyChanged = viewModel.TransactionModel().DebitCurrency().Code;
            }

            var IsFxTransaction = (viewModel.TransactionModel().Currency().Code != 'IDR' && IsCurrencyChanged == 'IDR');
            var IsFxTransactionToIDR = (viewModel.TransactionModel().Currency().Code == 'IDR' && IsCurrencyChanged != 'IDR' &&
                viewModel.TransactionModel().ProductType().IsFlowValas == true && parseFloat(viewModel.TransactionModel().AmountUSD()) >= TotalPPUModel.FCYIDRTrashHold);
            var IsTransactionIDRToFCY = (viewModel.TransactionModel().Currency().Code == 'IDR' && IsCurrencyChanged != 'IDR');
            viewModel.IsFxTransaction(IsFxTransaction);
            viewModel.IsFxTransactionToIDR(IsFxTransactionToIDR);
            viewModel.IsTransactionIDRToFCY(IsTransactionIDRToFCY);
        };

        if (product != null) {
            viewModel.TransactionModel().Product(product);
            var productSelected = viewModel.TransactionModel().Product().Name;
            switch (productSelected) {
                case "RTGS":
                    viewModel.Selected().BankCharges(viewModel.TransactionModel().BankCharges);
                    viewModel.Selected().AgentCharges(viewModel.TransactionModel().AgentCharges);
                    $('#bene-name').data({ ruleMaxlength: 35 });
                    $('#customer-name').data({ ruleMaxlength: 35 });
                    break;
                case "SKN":
                    viewModel.Selected().BankCharges(viewModel.TransactionModel().BankCharges);
                    viewModel.Selected().AgentCharges(2);
                    $('#bene-name').data({ ruleMaxlength: 35 });
                    $('#customer-name').data({ ruleMaxlength: 35 });
                    break;
                case "OTT":
                    viewModel.Selected().BankCharges(viewModel.TransactionModel().BankCharges);
                    viewModel.Selected().AgentCharges(2);
                    $('#bene-name').data({ ruleMaxlength: 255 });
                    $('#customer-name').data({ ruleMaxlength: 255 });
                    break;
                default:
                    $('#bene-name').data({ ruleMaxlength: 255 });
                    $('#customer-name').data({ ruleMaxlength: 255 });
                    break;
            }
        }
        if (docType != null) viewModel.DocumentType(docType);
        if (docPurpose != null) viewModel.DocumentPurpose(docPurpose);
        if (bankCharges != null) viewModel.TransactionModel().BankCharges(bankCharges);
        if (agentCharges != null) viewModel.TransactionModel().AgentCharges(agentCharges);

        if (viewModel.TransactionModel().IsNewCustomer()) {
            var new_currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().NewCustomer().Currency(); });
            var new_bizSegment = ko.utils.arrayFirst(viewModel.Parameter().BizSegments(), function (item) { return item.ID == viewModel.Selected().NewCustomer().BizSegment(); });

            if (new_currency != null) viewModel.TransactionModel().Account().Currency = new_currency;
            if (new_bizSegment != null) viewModel.TransactionModel().Customer().BizSegment(new_bizSegment);
        }

        if (viewModel.TransactionModel().IsNewCustomer() && !viewModel.IsLoadDraft() && (viewModel.IsFxTransaction() || viewModel.IsFxTransactionToIDR())) {
            ShowNotification("Page Information", NewCustomerFXWarning, "gritter-warning", false);
            
        }

        //console.log('IsProductType : ' + viewModel.IsProductType());
    });



    //viewModel['Calculate'] = calculate();
    //================================= End Eqv Calculation ====================================

    // Token Validation
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(OnSuccessToken, OnError);
    } else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);
            viewModel.SPUser(ko.mapping.toJS(spUser));
        }

        // call get data inside view model
        GetParameters();
        PPUModel.token = accessToken;
        GetParameterData(PPUModel, OnSuccessGetTotal, OnErrorDeal);
        ResetBeneBank();
    }

    // Knockout Bindings
    ko.applyBindings(viewModel);

    GetRateIDR();

    function GetRateIDR() {
        var options = {
            url: api.server + api.url.currency + "/CurrencyRate/" + "1",
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetRateIDR, OnError, OnAlways);
    }

    //================================= Eqv eion ====================================

});

// reset model
function ResetModel() {
    viewModel = new ViewModel();
}

// get all parameters need
function GetParameters() {
    var options = {
        url: api.server + api.url.parameter,
        params: {
            select: "Product,Currency,Channel,BizSegment,Bank,BankCharge,AgentCharge,FXCompliance,ChargesType,DocType,PurposeDoc,statementletter,underlyingdoc,customer,lld,producttype"
        },
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
}

function SetDefaultValueStatementA() {
    var today = Date.now();
    var documentType = ko.utils.arrayFirst(viewModel.ddlDocumentType_u(), function (item) { return item.Name == '-' });
    var underlyingDocument = ko.utils.arrayFirst(viewModel.ddlUnderlyingDocument_u(), function (item) { return item.Code() == 998 });

    if (documentType != null) {
        viewModel.DocumentType_u(new DocumentTypeModel2(documentType.ID, documentType.Name));
    }

    if (underlyingDocument != null) {
        viewModel.UnderlyingDocument_u(new UnderlyingDocumentModel2(underlyingDocument.ID(), underlyingDocument.Name(), underlyingDocument.Code()));
    }
    viewModel.DateOfUnderlying_u(viewModel.LocalDate(today, true, false));
    viewModel.SupplierName_u('-');
    viewModel.InvoiceNumber_u('-');
}

function LoadDraft() {
    ar = window.location.hash.split('#');
    if (ar.length < 2) return;
    var options = {
        url: api.server + api.url.transactiondraft + '/' + ar[1],
        token: accessToken
    };
    Helper.Ajax.Get(options, OnSuccessLoadDraft, OnError, OnAlways);
}

function SaveTransaction() {
    //console.log(ko.toJSON(viewModel.TransactionModel()));
    if (ko.toJS(viewModel.IsEmptyAccountNumber()) == true) {
        var new_debitcurrency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().DebitCurrency(); });
        if (new_debitcurrency != null) {
            //viewModel.TransactionModel().DebitCurrency = new_debitcurrency;
            viewModel.TransactionModel().DebitCurrency(new_debitcurrency);
        }

    } else if (ko.toJS(viewModel.TransactionModel().IsNewCustomer()) == true) {
        var new_debitcurrency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().NewCustomer().Currency(); });
        if (new_debitcurrency != null) {
            //viewModel.TransactionModel().DebitCurrency = new_debitcurrency;
            viewModel.TransactionModel().DebitCurrency(new_debitcurrency);
        }
    } else if ((ko.toJS(viewModel.IsEmptyAccountNumber()) == false) && (ko.toJS(viewModel.TransactionModel().IsNewCustomer()) == false)) {
        var new_debitcurrency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.TransactionModel().Account().Currency.ID; });
        if (new_debitcurrency != null) {
            //viewModel.TransactionModel().DebitCurrency = new_debitcurrency;
            viewModel.TransactionModel().DebitCurrency(new_debitcurrency);
        }
    }
    // convert top urgent to integer
    viewModel.TransactionModel().IsTopUrgent(viewModel.TransactionModel().IsTopUrgent()==1 ? 1 : 0);
    viewModel.TransactionModel().IsTopUrgentChain(viewModel.TransactionModel().IsTopUrgentChain()==1 ? 1 : 0)
    // Set BeneName IS NULL
    if (viewModel.TransactionModel().BeneName() == null) {
        viewModel.TransactionModel().BeneName('');
    }

    // Set BeneAccNumber IS NULL
    if (viewModel.TransactionModel().BeneAccNumber() == null) {
        viewModel.TransactionModel().BeneAccNumber('');
    }

  
    // begin dodit@2014.11.14:Add draft posting
    if (viewModel.TransactionModel().IsDraft() == true) {
        viewModel.TransactionModel().Amount((!viewModel.TransactionModel().Amount() > 0) ? 0 : viewModel.TransactionModel().Amount());
        viewModel.TransactionModel().Rate((!viewModel.TransactionModel().Rate() > 0) ? 0 : viewModel.TransactionModel().Rate());

        if (viewModel.TransactionModel().IsNewCustomer()) {
            var CustomerName = $("#customer-name").val();
            viewModel.TransactionModel().CustomerDraft().DraftCustomerName(CustomerName);
            viewModel.TransactionModel().CustomerDraft().DraftCIF(viewModel.TransactionModel().Customer().CIF);
            viewModel.TransactionModel().CustomerDraft().DraftAccountNumber(viewModel.TransactionModel().Account());

            viewModel.TransactionModel().Currency().DraftCurrencyID = viewModel.Selected().NewCustomer().Currency();
        }

        //FilterLastModifiedBy
        //FilterLastModifiedDate

        if (viewModel.TransactionModel().ID() == null) {
            //viewModel.TransactionModel().DebitCurrencyID = $('#debit-acc-ccy').val(data.Account.Currency.Code); 
            var options = {
                url: api.server + api.url.transaction,
                token: accessToken,
                data: ko.toJSON(viewModel.TransactionModel())
            };

            //console.log('Saving new draft....');
            // if id is null = Save
            Helper.Ajax.Post(options, OnSuccessSaveDraft, OnError, OnAlways);
        } else {
            var options = {
                url: api.server + api.url.transaction + "/Draft/" + viewModel.TransactionModel().ID(),
                token: accessToken,
                data: ko.toJSON(viewModel.TransactionModel())
            };

            //console.log('Modifying new draft....');
            Helper.Ajax.Put(options, OnSuccessSaveDraft, OnError, OnAlways);
        }
        return;
    }




    
    // add chandra - set transaction underlying
    if (viewModel.IsFxTransaction() || viewModel.IsFxTransactionToIDR()) {
        //var amount = document.getElementById("eqv-usd").value.replace(',','').replace(' ','');
        var amountUSD = viewModel.TransactionModel().AmountUSD();

        if (parseFloat(amountUSD) <= parseFloat(viewModel.TransactionModel().utilizationAmount())) {
            //if (parseFloat(viewModel.TransactionModel().TotalTransFX()) > parseFloat(TotalPPUModel.TreshHold) && viewModel.TempSelectedUnderlying()[0].StatementLetter() == 1) {
            if (parseFloat(viewModel.TransactionModel().TotalUtilization()) > parseFloat(TotalPPUModel.TreshHold) && viewModel.TempSelectedUnderlying()[0].StatementLetter() == 1) {
                viewModel.IsEditable(true);
                viewModel.IsUploaded(true);
                ShowNotification("Form Underlying Warning", "Total Utilization greater than " + TotalPPUModel.TreshHold + " (USD), Please add statement B underlying", 'gritter-warning', false);
                return;
            } else {
                viewModel.TransactionModel().underlyings([]);
                ko.utils.arrayForEach(viewModel.TempSelectedUnderlying(), function (item) {
                    if (item.Enable() == false)
                        viewModel.TransactionModel().underlyings.push(new SelectUtillizeModel(item.ID(), false, item.USDAmount(), ''));
                });
                // save transaction  fx
                if (viewModel.TransactionModel().Documents().length == viewModel.Documents().length) {
                    var options = {
                        url: api.server + api.url.transaction,
                        token: accessToken,
                        data: ko.toJSON(viewModel.TransactionModel())
                    };

                    // Save or Update
                    if (viewModel.TransactionModel().ID() == null) {
                        // if id is null = Save
                        Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
                    } else {
                        // if id is not null = Update
                        //options.url += "/" + viewModel.TransactionModel().ID();
                        //Helper.Ajax.Put(options, OnSuccessSaveAsDraft, OnError, OnAlways);
                        Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
                    }
                }

            }
        } else {
            viewModel.IsEditable(true);
            viewModel.IsUploaded(true);
            ShowNotification("Form Underlying Warning", "Total Utilization Amount must be equal greater than Transaction Amount", 'gritter-warning', false);
            return;
        }
    } else {
        // save transactio non fx
        if (viewModel.TransactionModel().Documents().length == viewModel.Documents().length) {
            var options = {
                url: api.server + api.url.transaction,
                token: accessToken,
                data: ko.toJSON(viewModel.TransactionModel())
            };

            // Save to api
            
            // Save or Update
            if (viewModel.TransactionModel().ID() == null) {
                // if id is null = Save
                Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
            } else {
                // if id is not null = Update
                
                //Helper.Ajax.Put(options, OnSuccessSaveAsDraft, OnError, OnAlways);
                Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
            }
        }
    }
}

function SaveTransactionLoan() {
    
    viewModel.TransactionModel().IsTopUrgent(viewModel.TransactionModel().IsTopUrgent() == 1 ? 1 : 0);
    viewModel.TransactionModel().IsTopUrgentChain(viewModel.TransactionModel().IsTopUrgentChain() == 1 ? 1 : 0)
    
    // begin dodit@2014.11.14:Add draft posting
    if (viewModel.TransactionModel().IsDraft() == true) {
        viewModel.TransactionModel().Amount((!viewModel.TransactionModel().Amount() > 0) ? 0 : viewModel.TransactionModel().Amount());
        viewModel.TransactionModel().Rate((!viewModel.TransactionModel().Rate() > 0) ? 0 : viewModel.TransactionModel().Rate());

        if (viewModel.TransactionModel().IsNewCustomer()) {
            var CustomerName = $("#customer-name").val();
            viewModel.TransactionModel().CustomerDraft().DraftCustomerName(CustomerName);
            viewModel.TransactionModel().CustomerDraft().DraftCIF(viewModel.TransactionModel().Customer().CIF);
            viewModel.TransactionModel().CustomerDraft().DraftAccountNumber(viewModel.TransactionModel().Account());

            viewModel.TransactionModel().Currency().DraftCurrencyID = viewModel.Selected().NewCustomer().Currency();
        }

        if (viewModel.TransactionModel().ID() == null) {
            //viewModel.TransactionModel().DebitCurrencyID = $('#debit-acc-ccy').val(data.Account.Currency.Code); 
            var options = {
                url: api.server + api.url.transaction,
                token: accessToken,
                data: ko.toJSON(viewModel.TransactionModel())
            };

            //console.log('Saving new draft....');
            // if id is null = Save
            Helper.Ajax.Post(options, OnSuccessSaveDraft, OnError, OnAlways);
        } else {
            var options = {
                url: api.server + api.url.transaction + "/Draft/" + viewModel.TransactionModel().ID(),
                token: accessToken,
                data: ko.toJSON(viewModel.TransactionModel())
            };

            //console.log('Modifying new draft....');
            Helper.Ajax.Put(options, OnSuccessSaveDraft, OnError, OnAlways);
        }
        return;
    }
}

function CheckOtherBank(bank) {
    if (bank == undefined) {
        viewModel.TransactionModel().Bank().SwiftCode = '';
        viewModel.TransactionModel().Bank().BankAccount = '';
        viewModel.TransactionModel().Bank().Code = '999';
        viewModel.IsOtherBank(false);
        viewModel.TransactionModel().IsOtherBeneBank(true);
    }
}
function AddListItem() {
    var body = {
        Title: viewModel.TransactionModel().ApplicationID() + " - " + viewModel.TransactionModel().Customer().CIF,
        //Application_x0020_Date: viewModel.TransactionModel().ApplicationDate(),
        Application_x0020_ID: viewModel.TransactionModel().ApplicationID(),
        /*Top_x0020_Urgent: viewModel.TransactionModel().IsTopUrgent(),
        CIF: viewModel.TransactionModel().Customer().CIF,
        Customer_x0020_Name: viewModel.TransactionModel().Customer().Name,
        New_x0020_Customer: viewModel.TransactionModel().IsNewCustomer(),
        Product: viewModel.TransactionModel().Product().Name,
        Currency: viewModel.TransactionModel().Currency().Code,
        Amount: viewModel.TransactionModel().Amount(),
        Rate: viewModel.TransactionModel().Rate(),
        Amount_x0020_USD: parseFloat(viewModel.TransactionModel().AmountUSD().replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(' ', '')),
        Channel: viewModel.TransactionModel().Channel().Name,
        Account_x0020_Number: viewModel.TransactionModel().Account().AccountNumber,
        Account_x0020_Currency: viewModel.TransactionModel().Account().Currency.Code,
        Bene_x0020_Name: viewModel.TransactionModel().BeneName(),
        BeneAccNumber: viewModel.TransactionModel().BeneAccNumber(),
        Bank: viewModel.TransactionModel().Bank().Description,
        Swift_x0020_Code: viewModel.TransactionModel().Bank().SwiftCode,
        Bene_x0020_Account: viewModel.TransactionModel().Bank().BankAccount,
        Biz_x0020_Segment: viewModel.TransactionModel().BizSegment().Name,
        Signature_x0020_Verified: viewModel.TransactionModel().IsSignatureVerified(),
        Dormant_x0020_Account: viewModel.TransactionModel().IsDormantAccount(),
        Freeze_x0020_Account: viewModel.TransactionModel().IsFreezeAccount(),
        Detail_x0020_Compliance: viewModel.TransactionModel().DetailCompliance(),
        Others: viewModel.TransactionModel().Others(), */
        Transaction_x0020_ID: viewModel.TransactionModel().ID(),
        /* Require_x0020_Callback: false, // New callback column */
        Initiator_x0020_GroupId: GetUserRole(viewModel.SPUser().Roles), //viewModel.SPUser().Roles[0].ID,
        __metadata: {
            type: config.sharepoint.metadata.list
        }
    };

    var options = {
        url: config.sharepoint.url.api + "/Lists(guid'" + config.sharepoint.listId + "')/Items",
        data: JSON.stringify(body),
        digest: jQuery("#__REQUESTDIGEST").val()
    };

    Helper.Sharepoint.List.Add(options, OnSuccessAddListItem, OnError, OnAlways);
}
// added chandra
function GetUserRole(userData) {
    var sValue;
    if (userData != undefined && userData.length > 0) {
        $.each(userData, function (index, item) {
            if (Const_RoleName[item.ID].toLowerCase().startsWith("dbs ppu") && Const_RoleName[item.ID].toLowerCase().endsWith("maker")) { //if (item.Name.toLowerCase().startsWith("dbs ppu") && item.Name.toLowerCase().endsWith("maker")) {
                sValue = item.ID;
            }
        });
    }
    if (sValue == undefined) {
        sValue = userData[0].ID;
    }
    return sValue;
}
//edited Lele 26 jan 2015
//add From Pak Dennes checking double Transaction 25 mar 2015
function ValidateTransaction() {
    // Ruddy : new exception to validate double transaction/underlying
    //test
    var OtherAccNumber = viewModel.TransactionModel().OtherAccountNumber();
    if (viewModel.TransactionModel().IsOtherAccountNumber()) {
        viewModel.TransactionModel().Account().AccountNumber = OtherAccNumber;
    }
    // IS NULL Bene Account Number
    if (viewModel.TransactionModel().BeneAccNumber() != null && viewModel.TransactionModel().BeneAccNumber().trim() == '') {
        viewModel.TransactionModel().BeneAccNumber(null);
    }
    // IS NULL Bene Name
    if (viewModel.TransactionModel().BeneName() != null && viewModel.TransactionModel().BeneName().trim() == '') {
        viewModel.TransactionModel().BeneName(null);
    }
    var options = {
        url: api.server + api.url.workflow.transactionCheck,
        token: accessToken,
        params: {
            cif: viewModel.TransactionModel().Customer().CIF,
            productID: viewModel.TransactionModel().Product().ID,
            currencyID: viewModel.TransactionModel().Currency().ID,
            amountUSD: parseFloat(viewModel.TransactionModel().AmountUSD().replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(' ', '')),
            applicationDate: viewModel.TransactionModel().ApplicationDate(),
            accountNumber: viewModel.TransactionModel().Account().AccountNumber,
            //lele add
            beneAccount: viewModel.TransactionModel().BeneAccNumber(),
            beneName: viewModel.TransactionModel().BeneName()
        }
    };

    Helper.Ajax.Get(options, function (data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            // Double transaction handling
            if (data.IsDoubleTransaction) {
                // fil double transactions
                viewModel.DoubleTransactions(data.Transactions);

                // Show Double Transaction dialog
                $("#modal-double-transaction").modal('show');
            } else {
                // start upload docs and save the transaction
                UploadDocuments();
            }
        }
    }, OnError);
}

function ValidateTransactionLoan() {
    
    var options = {
        url: api.server + api.url.workflow.transactionLoanCheck,
        token: accessToken,
        params: {
            cif: viewModel.TransactionModel().Customer().CIF,
            productID: viewModel.TransactionModel().Product().ID,
            currencyID: viewModel.TransactionModel().Currency().ID,
            applicationDate: viewModel.TransactionModel().ApplicationDate(),
            accountNumber: viewModel.TransactionModel().Account().AccountNumber
        }
    };

    Helper.Ajax.Get(options, function (data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            // Double transaction handling
            if (data.IsDoubleTransaction) {
                // fil double transactions
                viewModel.DoubleTransactions(data.Transactions);

                // Show Double Transaction dialog
                $("#modal-double-transaction").modal('show');
            } else {
                // start upload docs and save the transaction
                UploadDocuments();
            }
        }
    }, OnError);
}

function UploadDocuments() {
    // uploading documents. after upload completed, see SaveTransaction()
    var data = {
        ApplicationID: viewModel.TransactionModel().ApplicationID(),
        CIF: viewModel.TransactionModel().Customer().CIF,
        Name: viewModel.TransactionModel().Customer().Name
    };

    //bangkit
    // chandra - underlying
    if (viewModel.Documents().length > 0 && !(self.TransactionModel.Currency().Code != 'IDR' && self.TransactionModel.Account().Currency.Code == 'IDR')) { // FXCONDITION YES
        for (var i = 0; i < viewModel.Documents().length; i++) {
            UploadFile(data, viewModel.Documents()[i], SaveTransaction);
        }
    }
    else if (viewModel.IsFXTransactionAttach() && !(viewModel.IsUploaded())) {
        for (var i = 0; i < viewModel.Documents().length; i++) {
            UploadFile(data, viewModel.Documents()[i], SaveTransaction);
        }
    }
    else {
        SaveTransaction();
    }
}

// delete & upload document underlying start

function DeleteFile(documentPath) {
    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;
    // output variable
    var output;

    // Delete the file to the SharePoint folder.
    var deleteFile = deleteFileToFolder();
    deleteFile.done(function (file, status, xhr) {
        output = file.d;
    });
    deleteFile.fail(OnError);
    // Call function delete File Shrepoint Folder
    function deleteFileToFolder() {
        return $.ajax({
            url: serverUrl + "/_api/web/GetFileByServerRelativeUrl('" + documentPath + "')",
            type: "POST",
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "X-HTTP-Method": "DELETE",
                "IF-MATCH": "*"
                //"content-length": arrayBuffer.byteLength
            }
        });
    }
}

function UploadFileUnderlying(context, document, callBack) {

    // Define the folder path for this example.
    var serverRelativeUrlToFolder = '/Underlying Documents';


    // Get the file name from the file input control on the page.
    if (document.DocumentPath().name != undefined) {
        var parts = document.DocumentPath().name.split('.');
    } else {
        //self.DocumentPath_a('');
        ShowNotification("", "Please select a file", 'gritter-warning', true);
    }

    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBufferUnderlying();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolderUnderlying(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItemUnderlying(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                // Change the display name and title of the list item.
                var changeItem = updateListItemUnderlying(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    DocumentModels({ "FileName": document.DocumentPath().name, "DocumentPath": output.ServerRelativeUrl });

                    callBack();
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBufferUnderlying() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }
        //reader.readAsArrayBuffer(fileInput[0].files[0]);
        //reader.readAsDataURL(document.DocumentPath());
        reader.readAsArrayBuffer(document.DocumentPath());
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolderUnderlying(arrayBuffer) {

        // Get the file name from the file input control on the page.
        //var parts = fileInput[0].value.split('\\');
        //var fileName = parts[parts.length - 1];

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                //"content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItemUnderlying(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItemUnderlying(itemMetadata) {

        // Define the list item changes. Use the FileLeafRef property to change the display name.
        // For simplicity, also use the name as the title.
        // The example gets the list item type from the item's metadata, but you can also get it from the
        // ListItemEntityTypeFullName property of the list.
        //var body = String.format("{{'__metadata':{{'type':'{0}'}},'FileLeafRef':'{1}','Title':'{2}'}}", itemMetadata.type, fileName, fileName);
        var body = {
            Title: document.DocumentPath().name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: context.Type.DocTypeName,
            Document_x0020_Purpose: context.Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}

// delete & upload document underlying end

// Upload the file
function UploadFile(context, document, callBack) {

    // Define the folder path for this example.
    var serverRelativeUrlToFolder = '/Instruction Documents';
    var parts = document.DocumentPath.name.split('.');
    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBuffer();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolder(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                // Change the display name and title of the list item.
                var changeItem = updateListItem(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    //alert('file uploaded and updated');
                    //return output;
                    var kodok = {
                        ID: 0,
                        Type: document.Type,
                        Purpose: document.Purpose,
                        LastModifiedDate: null,
                        LastModifiedBy: null,
                        DocumentPath: output.ServerRelativeUrl,
                        FileName: document.DocumentPath.name
                    };
                    viewModel.TransactionModel().Documents.push(kodok);

                    callBack();
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBuffer() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }
        //bangkit
        reader.readAsArrayBuffer(document.DocumentPath);
        //reader.readAsDataURL(document.DocumentPath);
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(arrayBuffer) {

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                //"content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItem(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItem(itemMetadata) {
        var body = {
            Title: document.DocumentPath.name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: document.Type.Name,
            Document_x0020_Purpose: document.Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}


// On success GetData callback
function OnSuccessGetUnderlyingProforma(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        SetSelectedProforma(data.Rows);

        self.CustomerUnderlyingProformas(data.Rows);

        self.UnderlyingProformaGridProperties().Page(data['Page']);
        self.UnderlyingProformaGridProperties().Size(data['Size']);
        self.UnderlyingProformaGridProperties().Total(data['Total']);
        self.UnderlyingProformaGridProperties().TotalPages(Math.ceil(self.UnderlyingProformaGridProperties().Total() / self.UnderlyingProformaGridProperties().Size()));
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
    }
}

function OnSuccessGetRateIDR(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        // bind result to observable array
        idrrate = data.RupiahRate;
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function OnSuccessSaveDraft(data, textStatus, jqXHR) {
    // Get transaction id from api result
    if (data.ID != null || data.ID != undefined) {
        viewModel.TransactionModel().ID(data.ID);
        console.log(data);
    }

    ShowNotification("Transaction Draft Success", "Transaction draft not save attachments and underlying", "gritter-warning", true);

    window.location = "/home/draft-transactions";
}

// Event handlers declaration start
function OnSuccessUpload(data, textStatus, jqXHR) {
    ShowNotification("Upload Success", JSON.stringify(data), "gritter-success", true);
}

function OnSuccessGetApplicationID(data, textStatus, jqXHR) {
    // set ApplicationID
    if (data != null) {
        viewModel.TransactionModel().ApplicationID(data.ApplicationID);
    }
}

function OnSuccessGetRateAmount(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        // bind result to observable array
        viewModel.TransactionModel().Rate(data.RupiahRate);
        viewModel.Rate(data.RupiahRate);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function OnSuccessGetParameters(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        // bind result to observable array

        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };
        viewModel.Parameter().LLDs(ko.mapping.toJS(data.LLD, mapping));
        viewModel.Parameter().UnderlyingDocs(ko.mapping.toJS(data.UnderltyingDoc, mapping));

        viewModel.Parameter().Products(ko.mapping.toJS(data.Product, mapping));
        viewModel.Parameter().Channels(ko.mapping.toJS(data.Channel, mapping));
        viewModel.Parameter().Currencies(ko.mapping.toJS(data.Currency, mapping));
        viewModel.Parameter().BizSegments(ko.mapping.toJS(data.BizSegment, mapping));
        viewModel.Parameter().Banks(ko.mapping.toJS(data.Bank, mapping));

        viewModel.Parameter().FXCompliances(ko.mapping.toJS(data.FXCompliance, mapping));
        viewModel.Parameter().BankCharges(ko.mapping.toJS(data.ChargesType, mapping));
        viewModel.Parameter().AgentCharges(ko.mapping.toJS(data.ChargesType, mapping));
        viewModel.Parameter().DocumentTypes(ko.mapping.toJS(data.DocType, mapping));
        viewModel.Parameter().ProductType(ko.mapping.toJS(data.ProductType, mapping));

        viewModel.Selected().Channel(1);

        var items = ko.utils.arrayFilter(data.PurposeDoc, function (item) {
            return item.ID != 2;
        });
        if (items != null) {
            viewModel.Parameter().DocumentPurposes(ko.mapping.toJS(items, mapping));
        }
        // load draft transaction if hastag
        LoadDraft();

        // underlying parameter
        viewModel.GetUnderlyingParameters(data);

        // enabling form input controls
        viewModel.IsEditable(true);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function GetCustomerByCIF(cif) {
    $.ajax({
        type: "GET",
        url: api.server + api.url.customer + "/" + cif,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + accessToken
        },
        crossDomain: true,
        cache: false,
        success: function (data, textStatus, jqXHR) {
            if (jqXHR.status = 200) {
                //console.log(ko.toJSON(data)); //Put the response in ObservableArray
                if (data != null) {
                    ShowNotification("Page Information", "Customer CIF has added as New Customer.", "gritter-warning", false);
                    viewModel.TransactionModel().Customer(data);

                    var update = viewModel.TransactionModel().Customer();
                    viewModel.TransactionModel().Customer(ko.mapping.toJS(update));
                    viewModel.TransactionModel().IsNewCustomer(false);

                    customerNameData = data.Name;
                    cifData = data.CIF;
                    viewModel.GetDataUnderlying();
                    viewModel.GetDataAttachFile();
                    viewModel.IsStatementA(true); //?
                    viewModel.TransactionModel().utilizationAmount(0.00);
                    PPUModel.cif = cifData;
                    GetTotalPPU(PPUModel, OnSuccessTotal, OnErrorDeal);
                }

            } else {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // send notification
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    });
}

function OnSuccessLoadDraft(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        viewModel.IsLoadDraft(true);

        if (data == null) return;
        //console.log(data);

        viewModel.TransactionModel().IsOtherAccountNumber(data.IsOtherAccountNumber);
        viewModel.IsEmptyAccountNumber(data.IsOtherAccountNumber);


        var objLength = 0;
        objLength = $('#debit-acc-number option').length + 1;

        if ($('#debit-acc-number option[value=-]').length <= 0) {
            $('#debit-acc-number').append('<option value=->-</option>');

        }

        if (data.ApplicationDate != '1970-01-01T00:00:00') {
            viewModel.TransactionModel().ApplicationDate(formatDateValue(data.ApplicationDate));
        }
        else {
            viewModel.TransactionModel().ApplicationDate('');
            }

        viewModel.TransactionModel().ID(data.ID);

        if (data.Currency != null) {
            viewModel.Selected().Currency(data.Currency.ID);
            viewModel.GetRateAmountDraft(data.Currency.ID);
        }
        else if (data.DraftCurrencyID != null) {
            viewModel.Selected().Currency(data.DraftCurrencyID);
            viewModel.GetRateAmountDraft(data.DraftCurrencyID);
        }

        viewModel.TransactionModel().Amount(data.Amount);

        var amount_d = formatNumber(data.Amount);
        $('#trxn-amount').val(amount_d);

        viewModel.TransactionModel().IsNewCustomer(data.IsNewCustomer);

        if (!data.IsNewCustomer) {
            viewModel.TransactionModel().Customer(data.Customer);

            var update = viewModel.TransactionModel().Customer();
            viewModel.TransactionModel().Customer(ko.mapping.toJS(update));

            if (data.Account != null) {
                if (!data.IsOtherAccountNumber) {
                    viewModel.Selected().Account(data.Account.AccountNumber);
                    viewModel.TransactionModel().Account().Currency.Code = data.DebitCurrency.Code;


                    $('#debit-acc-ccy').val(data.DebitCurrency.Code);
                    if (data.Account != null) {
                        viewModel.Selected().Account(data.Account.AccountNumber);
                    }
                }
                else {
                    viewModel.Selected().Account('-');
                    viewModel.TransactionModel().DebitCurrency().Code = data.DebitCurrency.Code;
                    viewModel.Selected().DebitCurrency(data.DebitCurrency.ID);
                    viewModel.TransactionModel().OtherAccountNumber(data.OtherAccountNumber);
                }
            }

            customerNameData = data.Customer.Name;
            cifData = data.Customer.CIF;
            viewModel.GetDataUnderlying();
            viewModel.GetDataAttachFile();
            viewModel.IsStatementA(true); 
            viewModel.TransactionModel().utilizationAmount(0.00);
            PPUModel.cif = cifData;
            GetTotalPPU(PPUModel, OnSuccessTotal, OnErrorDeal);
        }
        else {
            viewModel.TransactionModel().Customer().Name(data.DraftCustomerName);
            viewModel.TransactionModel().Customer().CIF(data.DraftCIF);
            viewModel.TransactionModel().Customer().Accounts().AccountNumber = data.DraftAccountNumber;
            if (data.DraftCurrencyID != null) {
                viewModel.TransactionModel().Customer().Accounts().Currency = data.DraftCurrencyID;
                viewModel.Selected().NewCustomer().Currency(data.DraftCurrencyID);
                viewModel.TransactionModel().Customer().Accounts().AccountNumber = data.DraftAccountNumber;
                $('#debit-acc-number').val(data.DraftAccountNumber);
            }

            // check new customer process by CIF
            GetCustomerByCIF(data.DraftCIF);
        }

        //----- end load data customer-----

        viewModel.TransactionModel().IsTopUrgent(data.IsTopUrgent == 1 ? 1 : 0);
        viewModel.TransactionModel().IsTopUrgentChain(data.IsTopUrgent == 2 ? 1 : 0);
        viewModel.TransactionModel().BeneName(data.BeneName);
        viewModel.TransactionModel().PaymentDetails(data.PaymentDetails);
        viewModel.TransactionModel().DetailCompliance(data.DetailCompliance);

        viewModel.TransactionModel().BeneAccNumber(data.BeneAccNumber);
        viewModel.BeneAccNumberMask(data.BeneAccNumber);

        //formatAccount();

        viewModel.TransactionModel().ApplicationID(data.ApplicationID);

        if (data.Channel != null) {
            viewModel.Selected().Channel(data.Channel.ID);
        }

        viewModel.TransactionModel().TZNumber(data.TZNumber);

        if (data.Bank != null) {
            if (data.Bank.Code == '999') {
                data.Bank.Description = data.OtherBeneBankName;
                data.Bank.SwiftCode = data.OtherBeneBankSwift;
            }

            viewModel.IsOtherBank(true);
            viewModel.TransactionModel().IsOtherBeneBank(false);
        }
        else {
            viewModel.IsOtherBank(false);
            viewModel.TransactionModel().IsOtherBeneBank(true);
        }

        viewModel.TransactionModel().Bank(data.Bank);


        if (data.BizSegment != null) {
            viewModel.Selected().BizSegment(data.BizSegment.ID);
        }
        if (data.Product != null) {
            viewModel.Selected().Product(data.Product.ID);
            viewModel.OnProductChange();
        }

        if (data.Channel != null) {
            viewModel.Selected().Channel(data.Channel.ID);
        }
        if (data.ProductType != null) {
            viewModel.Selected().ProductType(data.ProductType.ID);

        }
        if (data.BankCharges != null) {
            viewModel.Selected().BankCharges(data.BankCharges.ID);
        }
        if (data.AgentCharges != null) {
            viewModel.Selected().AgentCharges(data.AgentCharges.ID);
        }
        if (data.LLD != null) {
            viewModel.Selected().LLD(data.LLD.ID);
        }

        viewModel.IsEmptyAccountNumber(data.IsOtherAccountNumber);
        viewModel.TransactionModel().OtherAccountNumber(data.OtherAccountNumber);

        viewModel.IsLoadDraft(false);

        viewModel.SetBankAutoCompleted();

    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
    }
}

function OnSuccessSaveAsDraft(data, textStatus, jqXHR) {
    if (data.ID != null || data.ID != undefined) {
        viewModel.TransactionModel().ID(data.ID);
    }
    // send notification
    ShowNotification(self.TransactionModel().IsDraft() ? 'Saving Success' : 'Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);
}
function OnSuccessUpdateUnderlying(data, textStatus, jqXHR) {
    if (jqXHR.status != 200) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function OnSuccessAddListItem(data, textStatus, jqXHR) {
    window.location = "/home";
}

function OnSuccessSaveAPI(data, textStatus, jqXHR) {
    // Get transaction id from api result
    if (data.ID != null || data.ID != undefined) {
        if (viewModel.TransactionModel().ID() != null) {
            DeleteDraft(viewModel.TransactionModel().ID(), data.ID);
        }
        else {
            viewModel.TransactionModel().ID(data.ID);
            AddListItem();
        }
        // insert to sharepoint list item

    }
}

function DeleteDraft(id, transactionID) {
    //Ajax call to delete the Customer
    $.ajax({
        type: "DELETE",
        url: api.server + api.url.transaction + "/Draft/Delete/" + id,
        //data: ko.toJSON(Product),
        headers: {
            "Authorization": "Bearer " + accessToken
        },
        success: function (data, textStatus, jqXHR) {
            // send notification
            if (jqXHR.status = 200) {
                viewModel.TransactionModel().ID(transactionID);
                AddListItem();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // send notification
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    });


};
// Event handlers declaration end

// Autocomplete
function OnSuccessAutoComplete(response, data, textStatus, jqXHR) {
    response($.map(data, function (item) {
        return {
            // default autocomplete object
            label: item.CIF + " - " + item.Name,
            value: item.Name,

            // custom object binding
            data: {
                CIF: item.CIF,
                Name: item.Name,
                POAName: item.POAName,
                Accounts: item.Accounts,
                BizSegment: item.BizSegment,
                Branch: item.Branch,
                Contacts: item.Contacts,
                Functions: item.Functions,
                RM: item.RM,
                Type: item.Type,
                Underlyings: item.Underlyings,
                LastModifiedBy: item.LastModifiedBy,
                LastModifiedDate: item.LastModifiedDate
            }
        }
    })
    );
}

function OnSuccessBankAutoComplete(response, data, textStatus, jqXHR) {

    //reset bene bank mapped data
    viewModel.TransactionModel().Bank().SwiftCode = '';
    viewModel.TransactionModel().Bank().BankAccount = '';
    viewModel.TransactionModel().Bank().Code = '999';
    viewModel.IsOtherBank(false);
    viewModel.TransactionModel().IsOtherBeneBank(true);
    $('#bank-code').val('');
    $('#bene-acc-number').val('');

    response($.map(data, function (item) {
        return {
            // default autocomplete object
            label: item.Description,
            value: item.Description,

            // custom object binding
            data: {
                BankAccount: item.BankAccount,
                BranchCode: item.BranchCode,
                Code: item.Code,
                CommonName: item.CommonName,
                Currency: item.Currency,
                Description: item.Description,
                ID: item.ID,
                LastModifiedBy: item.LastModifiedBy,
                LastModifiedDate: item.LastModifiedDate,
                PGSL: item.PGSL,
                SwiftCode: item.SwiftCode
            }
        }
    })
    );
}

function OnSuccessLLDAutoComplete(response, data, textStatus, jqXHR) {
    //reset LLD null mapped data
    viewModel.TransactionModel().LLD().ID = null;
    viewModel.TransactionModel().LLD().Description = '';
    viewModel.TransactionModel().LLD().Code = '';

    response($.map(data, function (item) {
        return {
            // default autocomplete object
            label: item.Description,
            value: item.Description,

            // custom object binding
            data: {
                ID: item.ID,
                Code: item.Code,
                Description: item.Description,

            }
        }
    })
    );
}

function enforceMaxlength(data, event) {
    if (viewModel.TransactionModel().Product().Name != 'OTT') {
        if (event.target.value.length >= 35) {
            return false;
        }
    }
    else {
        if (event.target.value.length >= 255) {
            return false;
        }
    }

    return true;
}


/*
function changeSiftCode(data, event) {
    if (event.target.value.length == 0) {
        viewModel.TransactionModel().Bank().SwiftCode = '';
        viewModel.TransactionModel().Bank().BankAccount = '';
        viewModel.TransactionModel().Bank().Code = '999';
        viewModel.IsOtherBank(false);
        viewModel.TransactionModel().IsOtherBeneBank(true);
        $('#bank-code').val('');
    }
    return true;
} */

function read_u() {
    var e = document.getElementById("Amount_u").value;
    e = e.toString().replace(/,/g, '');
    var res = parseInt(e) * parseFloat(viewModel.Rate_u()) / parseFloat(idrrate);
    res = Math.round(res * 100) / 100;
    res = isNaN(res) ? 0 : res; //avoid NaN
    viewModel.AmountUSD_u(parseFloat(res).toFixed(2));
    viewModel.Amount_u(formatNumber(e));
}

// Token validation On Success function
function OnSuccessToken(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // read spuser from cookie
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);
        viewModel.SPUser(ko.mapping.toJS(spUser));
    }

    // get parameter values
    GetParameters();
    PPUModel.token = accessToken;
    GetParameterData(PPUModel, OnSuccessGetTotal, OnErrorDeal);
}

function OnSuccessGetTotal(dataCall) {
    idrrate = dataCall.RateIDR;;
}

function OnSuccessTotal() {
    viewModel.GetCalculating(viewModel.TransactionModel().AmountUSD());
}

// AJAX On Error Callback
function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}


function OnErrorDeal(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}
// AJAX On Alway Callback
function OnAlways() {
    // $box.trigger('reloaded.ace.widget');

    // enabling form input controls
    viewModel.IsEditable(true);
}

