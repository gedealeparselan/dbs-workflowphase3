﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    public class SingleValueParameter
    {
        /// <summary>
        /// SingleValueParameter ID.
        /// </summary>

        public int ID { get; set; }

        /// <summary>
        /// Parameter Name
        /// </summary>
        [MaxLength(100, ErrorMessage = "Parameter Name is must be in {1} characters.")]
        public string Name { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        [MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Value { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class SingleValueParameterRow : SingleValueParameter
    {
        public int RowID { get; set; }

    }


    #endregion

    #region Filter
    public class SingleValueParameterFilter : Filter { }
    #endregion

    #region Interface
    public interface ISingleValueParameterRepository : IDisposable
    {
        bool GetSingleValueParameterByName(string name, ref string value, ref string message);
        bool GetSingleValueParameter(ref IList<SingleValueParameter> singleValueParameters, int limit, int index, ref string message);

        bool GetSingleValueParameter(string key, int limit, ref IList<SingleValueParameter> singleValueParameters, ref string message);

        bool UpdateSingleValueParameter(string name, SingleValueParameter singleValueParameter, ref string message);
        bool DeleteSingleValueParameter(int id, ref string message);
    }
    #endregion

    #region Repository
    public class SingleValueParameterRepository : ISingleValueParameterRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetSingleValueParameterByName(string name, ref string value, ref string message)
        {
            bool isSuccess = false;

            try
            {
                value = (from a in context.Configs
                         where a.IsDelete.Equals(false) && a.Parameter.Equals(name)
                         select a.Value).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetSingleValueParameter(ref IList<SingleValueParameter> singleValueParameters, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    singleValueParameters = (from a in context.Configs
                                             where a.IsDelete.Equals(false)
                                             orderby new { a.Parameter, a.Value }
                                             select new SingleValueParameter
                                             {
                                                 ID = a.ConfigID,
                                                 Name = a.Parameter,
                                                 Value = a.Value,
                                                 LastModifiedBy = a.CreateBy,
                                                 LastModifiedDate = a.CreateDate
                                             }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


        public bool GetSingleValueParameter(string key, int limit, ref IList<SingleValueParameter> singleValueParameters, ref string message)
        {
            bool isSuccess = false;

            try
            {
                singleValueParameters = (from a in context.Configs
                                         where a.IsDelete.Equals(false) && a.Parameter.Contains(key) || a.Value.Contains(key)
                                         orderby new { a.Parameter, a.Value }
                                         select new SingleValueParameter
                                         {
                                             Name = a.Parameter,
                                             Value = a.Value
                                         }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


        public bool UpdateSingleValueParameter(string name, SingleValueParameter singleValueParameter, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Config data = context.Configs.Where(a => a.Parameter.Equals(name)).SingleOrDefault();

                if (data != null)
                {

                    //data.Parameter = singleValueParameter.Name;
                    data.Value = singleValueParameter.Value;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                    context.SaveChanges();
                    isSuccess = true;

                }
                else
                {
                    message = string.Format("Single value parameter data for Parameter Name {0} is does not exist.", name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteSingleValueParameter(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Config data = context.Configs.Where(a => a.ConfigID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDelete = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Single value parameter data for Parameter ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}