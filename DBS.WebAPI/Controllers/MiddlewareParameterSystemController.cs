﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    public class MiddlewareParameterSystemController : ApiController
    {
        private string message = string.Empty;

        private readonly MiddlewareParameterSystemRepository repository = null;

          public MiddlewareParameterSystemController()
        {
            this.repository = new MiddlewareParameterSystemRepository();
        }

          public MiddlewareParameterSystemController(MiddlewareParameterSystemRepository repository)
        {
            this.repository = repository;
        }

          //Data Grid
          [ResponseType(typeof(MiddlewareParameterSystemGrid))]
          [AcceptVerbs("GET", "POST")]
          [Route("api/MiddlewareParameterSystem")]
          public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<MiddlewareParameterSystemFilter> filters)
          {
              MiddlewareParameterSystemGrid output = new MiddlewareParameterSystemGrid();

              if (!repository.GetMiddlewareParameterSystem(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
              {
                  return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
              }
              else
              {
                  return Request.CreateResponse<MiddlewareParameterSystemGrid>(HttpStatusCode.OK, output);
              }
          }

        //update
          [HttpPut]
          [ResponseType(typeof(string))]
          [Route("api/MiddlewareParameterSystem/{id}")]
          [Authorize(Roles = "DBS Admin")]
          public HttpResponseMessage Put(int id, MiddlewareParameterSystemModel Middleware)
          {
              if (!ModelState.IsValid)
              {
                  return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
              }
              else
              {
                  if (!repository.UpdateMiddlewareParameterSystem(id, Middleware, ref message))
                  {
                      return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                  }
                  else
                  {
                      return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Middleware Parameter System data has been updated." });
                  }
              }
          }

          protected override void Dispose(bool disposing)
          {
              if (repository != null)
                  repository.Dispose();
              base.Dispose(disposing);
          }
    }
}