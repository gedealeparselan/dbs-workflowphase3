﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Transactions;
using DBS.Entity;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Data.Entity.Validation;
using System.Diagnostics;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;


namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("NotificationEmail")]
    public class NotificationEmailModel
    {
        public int ID { get; set; }

        public string TypeOfEmail { get; set; }

        public string To { get; set; }

        public string Cc { get; set; }

        public string MailSubject { get; set; }

        public string MailBody { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public string LastModifiedBy { get; set; }
        
        
    }
    

    public class StaticDropdownParameterModel
    {
        /// <summary>
        /// Static Dropdown Parameter ID
        /// </summary>
        public int ID { get; set; }



        /// <summary>
        /// Static Dropdown Parameter Item
        /// </summary>
        public string DropdownItem { get; set; }


        /// <summary>
        /// Static Dropdown Parameter Item Value
        /// </summary>
        public string DropdownItemValue { get; set; }


        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>

        public string LastModifiedBy { get; set; }
    }
    public class NotificationEmailRow : NotificationEmailModel
    {
        public int RowID { get; set; }

    }

    public class NotificationEmailGrid : Grid
    {
        public IList<NotificationEmailRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class NotificationEmailFilter : Filter { }

    #endregion

    #region Interface
    public interface INotificationEmailRepository : IDisposable
    {
        bool GetNotificationEmailByID(string modul, int id, ref NotificationEmailModel notificationemail, ref string message);
        bool GetNotificationEmail(string modul, ref IList<NotificationEmailModel> NotificationEmails, int limit, int index, ref string message);
        bool GetNotificationEmail(string modul, ref IList<NotificationEmailModel> NotificationEmails, ref string message);
        bool GetNotificationEmail(string modul, int page, int size, IList<NotificationEmailFilter> filters, string sortColumn, string sortOrder, ref NotificationEmailGrid notificationemails, ref string message);
        bool GetNotificationEmail(string modul, NotificationEmailFilter filter, ref IList<NotificationEmailModel> NotificationEmails, ref string message);
        bool GetNotificationEmail(string modul, string key, int limit, ref IList<NotificationEmailModel> NotificationEmails, ref string message);
        bool AddNotificationEmail(string modul, NotificationEmailModel notificationemail, ref string message);
        bool UpdateNotificationEmail(string modul, int id, NotificationEmailModel notificationemail, ref string message);
        bool DeleteNotificationEmail(string modul, int id, ref string message);
        bool GetParameters(string modul, ref IList<StaticDropdownParameterModel> parameters, ref string message);
    }
    #endregion

    #region Repository
    public class NotificationEmailRepository : INotificationEmailRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetNotificationEmailByID(string modul, int id, ref NotificationEmailModel notificationemail, ref string message)
        {
            bool isSuccess = false;
            try 
            {
                notificationemail = (from a in context.EmailNotifications
                                     join b in context.EmailCategories
                                        on a.EmailCategoryID equals b.EmailCategoryID                                        
                                      where a.IsDeleted.Equals(false) &&
                                            b.IsDeleted.Equals(false) &&
                                            a.EmailNotifID.Equals(id)
                                      select new NotificationEmailModel
                                      {
                                          ID = a.EmailNotifID,
                                          TypeOfEmail = b.EmailCategoryName,
                                          To = a.To,
                                          Cc = a.Cc,
                                          MailSubject = a.MailSubject,
                                          MailBody = a.MailBody,
                                          LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                          LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value                                          
                                      }).SingleOrDefault();
                isSuccess = true;
            }
            catch(Exception ex)
            {
                message = ex.Message;
            }


            return isSuccess;
        }

        public bool GetNotificationEmail(string modul, ref IList<NotificationEmailModel> NotificationEmails, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    var result = (from a in context.EmailNotifications
                                  join b in context.EmailCategories
                                    on a.EmailCategoryID equals b.EmailCategoryID
                                  where a.IsDeleted.Equals(false) &&
                                        b.IsDeleted.Equals(false)
                                  orderby new { b.EmailCategoryName}
                                  select new NotificationEmailModel
                                  {
                                      ID = a.EmailNotifID,
                                      TypeOfEmail = b.EmailCategoryName,
                                      To = a.To,
                                      Cc = a.Cc,
                                      MailSubject = a.MailSubject,
                                      MailBody = a.MailBody,
                                      LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                      LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value                                     
                                  }).Skip(skip).Take(limit).ToList();
                    NotificationEmails = result.ToList<NotificationEmailModel>();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetNotificationEmail(string modul, ref IList<NotificationEmailModel> NotificationEmails, ref string message)
        {
            bool isSuccess = false;

            try
            {

                using (DBSEntities context = new DBSEntities())
                {
                    var result = (from a in context.EmailNotifications
                                  join b in context.EmailCategories
                                    on a.EmailCategoryID equals b.EmailCategoryID
                                  where a.IsDeleted.Equals(false) &&
                                        b.IsDeleted.Equals(false)
                                  orderby new { b.EmailCategoryName }
                                  select new NotificationEmailModel
                                  {
                                      ID = a.EmailNotifID,
                                      TypeOfEmail = b.EmailCategoryName, 
                                      To = a.To,
                                      Cc = a.Cc,
                                      MailSubject = a.MailSubject,
                                      MailBody = a.MailBody,
                                      LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                      LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,                                      
                                  }).ToList();
                    NotificationEmails = result.ToList<NotificationEmailModel>();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetNotificationEmail(string modul, int page, int size, IList<NotificationEmailFilter> filters, string sortColumn, string sortOrder, ref NotificationEmailGrid notificationemails, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                NotificationEmailModel filter = new NotificationEmailModel();
                if (filters != null)
                {
                    filter.TypeOfEmail = (string)filters.Where(a => a.Field.Equals("TypeOfEmail")).Select(a => a.Value).SingleOrDefault();
                    filter.To = (string)filters.Where(a => a.Field.Equals("To")).Select(a => a.Value).SingleOrDefault();
                    filter.Cc = (string)filters.Where(a => a.Field.Equals("Cc")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {                    
                    var categoryEmail = context.StaticDropdowns.Where(a => a.ModuleName.Equals(modul)).Where(a => a.DropdownCategoryname.Equals("TYPE_OF_EMAIL")).Select(a => a.DropdownItemvalue).ToList();
                    var data = (from a in context.EmailNotifications
                                join b in context.EmailCategories
                                    on a.EmailCategoryID equals b.EmailCategoryID
                                let isTypeOfEmail = !string.IsNullOrEmpty(filter.TypeOfEmail)
                                let isTo = !string.IsNullOrEmpty(filter.To)
                                let isCc = !string.IsNullOrEmpty(filter.Cc)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where categoryEmail.Contains(b.EmailCategoryName) &&
                                    a.IsDeleted.Equals(false) &&
                                    b.IsDeleted.Equals(false) &&
                                    (isTypeOfEmail ? b.EmailCategoryName.Contains(filter.TypeOfEmail) : true) &&
                                    (isTo? a.To.Contains(filter.To) : true) &&
                                    (isCc? a.Cc.Contains(filter.Cc) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new NotificationEmailModel
                                {
                                    ID = a.EmailNotifID,
                                    TypeOfEmail = b.EmailCategoryName,
                                    To = a.To,
                                    Cc = a.Cc,
                                    MailSubject = a.MailSubject,
                                    MailBody = a.MailBody,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    notificationemails.Page = page;
                    notificationemails.Size = size;
                    notificationemails.Total = data.Count();
                    notificationemails.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    notificationemails.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new NotificationEmailRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            TypeOfEmail = a.TypeOfEmail,
                            To = a.To,
                            Cc = a.Cc,
                            MailSubject = a.MailSubject,
                            MailBody = a.MailBody,                            
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetNotificationEmail(string modul, NotificationEmailFilter filter, ref IList<NotificationEmailModel> NotificationEmails, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetNotificationEmail(string modul, string key, int limit, ref IList<NotificationEmailModel> NotificationEmails, ref string message)
        {
            bool isSuccess = false;

            try
            {
                  NotificationEmails = (from a in context.EmailNotifications
                                        join b in context.EmailCategories
                                        on a.EmailCategoryID equals b.EmailCategoryID
                                        where a.IsDeleted.Equals(false) && b.IsDeleted.Equals(false) &&
                                                b.EmailCategoryName.Contains(key)
                                        orderby new { b.EmailCategoryName }
                                        select new NotificationEmailModel
                                        {
                                            TypeOfEmail = b.EmailCategoryName,
                                            To = a.To,
                                            Cc = a.Cc,
                                            MailSubject = a.MailSubject,
                                            MailBody = a.MailBody,                                       
                                            LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                            LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                        }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddNotificationEmail(string modul, NotificationEmailModel notificationemail, ref string message)
        {
            bool isSuccess = false;
            if (notificationemail != null)
            {
                
                    using (TransactionScope ts = new TransactionScope())
                    {
                        try
                        {

                            Entity.EmailCategory resultCategory = new Entity.EmailCategory()
                            {
                                EmailCategoryName = notificationemail.TypeOfEmail,
                                IsDeleted = false,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().DisplayName
                                
                            };
                            context.EmailCategories.Add(resultCategory);
                            context.SaveChanges();

                            var lastEmailCategoryID = context.EmailCategories.OrderByDescending(a => a.EmailCategoryID).Select(a => a.EmailCategoryID).FirstOrDefault();
                            Entity.EmailNotification resultNotification = new Entity.EmailNotification()
                            {
                                EmailCategoryID = lastEmailCategoryID,
                                To = notificationemail.To,
                                Cc = notificationemail.Cc,
                                MailSubject = notificationemail.MailSubject,
                                MailBody = notificationemail.MailBody,
                                IsDeleted = false,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().DisplayName
                            };
                            context.EmailNotifications.Add(resultNotification);
                            context.SaveChanges();
                            ts.Complete();
                            return isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            ts.Dispose();
                            message = ex.Message;
                        }
                    }
                    
                

            }

            return isSuccess;
        }

        public bool UpdateNotificationEmail(string modul, int id, NotificationEmailModel notificationemail, ref string message)
        {
            bool isSuccess = false;
            try
            {
                Entity.EmailNotification data = context.EmailNotifications.Where(a => a.EmailNotifID.Equals(id)).SingleOrDefault();
                using (TransactionScope ts = new TransactionScope())
                {
                    if (data != null)
                    {
                        try
                        {
                            
                            DeletedDetail(data.EmailCategoryID);
                            Entity.EmailCategory resultEmailCategory = new Entity.EmailCategory()
                            {
                                EmailCategoryName = notificationemail.TypeOfEmail,
                                IsDeleted = false,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().DisplayName
                            };
                            context.EmailCategories.Add(resultEmailCategory);
                            context.SaveChanges();

                            var lastEmailCategoryID = context.EmailCategories.OrderByDescending(a => a.EmailCategoryID).Select(a => a.EmailCategoryID).FirstOrDefault();

                            data.EmailCategoryID = lastEmailCategoryID;
                            data.To = notificationemail.To;
                            data.Cc = notificationemail.Cc;                            
                            data.MailSubject = notificationemail.MailSubject;
                            data.MailBody = notificationemail.MailBody;
                            data.UpdateDate = DateTime.UtcNow;
                            data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                            context.SaveChanges();

                            ts.Complete();
                            isSuccess = true;

                        }
                        catch(Exception ex)
                        {
                            ts.Dispose();
                            message = ex.Message;
                        }
                    }
                    else
                    {
                        ts.Dispose();
                        message = string.Format("Notification Email data for Email Notification ID {0} is does not exist.", id);
                    }
                }
                

            }
            catch(Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool DeleteNotificationEmail(string modul, int id, ref string message)
        {
            bool isSuccess = false;
            try
            {
                var data = context.EmailNotifications.Where(a => a.EmailNotifID.Equals(id)).SingleOrDefault();
                if (data != null)
                {
                    using(TransactionScope ts = new TransactionScope())
                    {
                        try
                        {
                            data.IsDeleted = true;
                            data.UpdateDate = DateTime.UtcNow;
                            data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                            context.SaveChanges();
                            var dataDetail = context.EmailCategories.Where(a => a.EmailCategoryID.Equals(data.EmailCategoryID)).ToList();
                            if (dataDetail != null && dataDetail.Count() > 0)
                            {
                                dataDetail.ForEach(a =>
                                {
                                    a.IsDeleted = true;

                                });
                                context.SaveChanges();
                            }
                            ts.Complete();
                            isSuccess = true;
                        }
                        catch(Exception ex)
                        {
                            ts.Dispose();
                            message = ex.Message;
                        }
                    }

                }
                else
                {
                    message = string.Format("Notification Email data for Email Notification ID {0} is does not exist.", id);
                }
               
            }
            catch(Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetParameters(string modul, ref IList<StaticDropdownParameterModel> parameters, ref string message)
        {
            bool isSuccess = false;
            string Modul = modul.Trim().ToLower();
            if (!string.IsNullOrEmpty(Modul))
            {
                try
                {
                    parameters = (from a in context.StaticDropdowns
                                  where a.ModuleName.Trim().ToLower().Equals(Modul) &&
                                         a.DropdownCategoryname.Equals("TYPE_OF_EMAIL") &&
                                         a.IsDeleted.Equals(false)
                                  select new StaticDropdownParameterModel
                                  {
                                      ID = a.DropdownID,
                                      DropdownItem = a.DropdownItem,
                                      DropdownItemValue = a.DropdownItemvalue
                                  }).ToList();

                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return isSuccess;
        }
        private void DeletedDetail(int id)
        {

            try
            {
                var datadetail = context.EmailCategories.Where(a => a.EmailCategoryID.Equals(id) && a.IsDeleted.Equals(false)).ToList();
                if (datadetail != null && datadetail.Count > 0)
                {
                    datadetail.ForEach(a =>
                    {
                        a.IsDeleted = true;
                        a.UpdateDate = DateTime.UtcNow;
                        a.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                    });
                    context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

    #endregion
}