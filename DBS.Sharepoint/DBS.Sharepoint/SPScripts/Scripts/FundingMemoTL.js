﻿var formatNumber = function (num) {
    if (num == null) {
        return;
    }
    var n = num.toString(), p = n.indexOf('.');
    return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
        return p < 0 || i < p ? ($0 + ',') : $0;
    });
}

var ViewModel = function () {

    var BizSegmentID = '0';
    var self = this;
    var Customertype = "";
    $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });
    self.ID_a = ko.observable(0);
    //Andriyanto 04 Januari 2016
    self.ApprovalDoA = ko.observable();
    self.CustomerName = ko.observable();
    self.ddlDocumentPurpose_a = ko.observableArray([]);
    self.ddlDocumentType_a = ko.observableArray([]);
    self.IsUploading = ko.observable(false);
    self.DocumentPurposes = ko.observableArray([]);
    self.DocumentPurpose_a = ko.observable();
    self.DocumentType_a = ko.observable();
    self.DocumentPath_a = ko.observable();
    self.SolIDList = ko.observable(SolIDModel);
    self.ShortTerm = ko.observable(true);

    //End
    var EmployeeFunding = function (EmployeeID, EmployeeName) {
        self.EmployeeID = ko.observable(EmployeeID);
        self.EmployeeName = ko.observable(EmployeeName)
    };
    var EmployeeModel =
    {
        EmployeeID: ko.observable(),
        EmployeeUsername: ko.observable(),
        EmployeeName: ko.observable(),
        EmployeeEmail: ko.observable(),
        RankID: ko.observable(),
    };
    var DocumentTypeModel = {
        ID: ko.observable(),
        Name: ko.observable(),
        Description: ko.observable()
    };
    var DocumentPurposeModel = {
        ID: ko.observable(),
        Name: ko.observable(),
        Description: ko.observable()
    };
    var SelectedModel = {
        FinacleSchemeCodeName: ko.observable(),
        DocumentPurpose_a: ko.observable(),
        DocumentType_a: ko.observable()
    };
    var DocumentModel = {
        ID: ko.observable(),
        Type: ko.observable(DocumentTypeModel),
        Purpose: ko.observable(DocumentPurposeModel),
        TypeID: self.DocumentType_a,
        PurposeID: self.DocumentPurpose_a,
        DocumentPath: self.DocumentPath_a,
        IsNewDocument: ko.observable(false),
        LastModifiedDate: ko.observable(new Date),
        LastModifiedBy: ko.observable(),
        IsDormant: ko.observable(false)
    };
    var fundingmemodocument = {
        Documents: self.Documents,
        DocumentPath: self.DocumentPath_a,
        Type: self.DocumentType_a,
        Purpose: self.DocumentPurpose_a,
        Type: ko.observable(DocumentTypeModel),
        Purpose: ko.observable(DocumentPurposeModel),
    }
    var DocumentsFundingModel = {
        Documents: self.Documents,
        DocumentPath: self.DocumentPath_a,
        Type: self.DocumentType_a,
        Purpose: self.DocumentPurpose_a,
        Type: ko.observable(DocumentTypeModel),
        Purpose: ko.observable(DocumentPurposeModel),
    }

    var SolIDModel = {
        SolID: ko.observable(),
        ID: ko.observable(),
        Name: ko.observable(),
        Code: ko.observable()
    }
    self.UploadDocument = function () {
        self.IsUploading(false);
        $("#modal-form-Attach1").modal('show');
        $('.remove').click();
        var item = ko.utils.arrayFilter(self.DocumentPurposes(), function (dta) { return dta.ID != 2 });
        //self.ddlDocumentPurpose_a(item);
        self.ID_a(0);
        self.DocumentPurpose_a(null);
        self.Selected().DocumentPurpose_a(null);
        self.DocumentType_a(null);
        self.Selected().DocumentType_a(null);
        self.DocumentPath_a(null);
        self.Selected().DocumentPath_a(null);

        $('#backDrop').show();
    }

    self.GetBaseRate = function () {
        var fundingCurrency = $('#AmountDisburseID option:selected').text().split(' ')[0];
        var tenor = $('#Interest option:selected').text();

        var options = {
            url: api.server + api.url.fundingmemotl + "/GetBaseRate",
            params: {
                currency: fundingCurrency,
                tenorCode: tenor
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, function (result, textStatus, jqXHR) {
            var rate = result.BaseRate;
            if (rate != undefined) {
                self.BaseRate(rate);
            }
        }, OnError);
    }

    self.GetAllInRate = function () {
        var currCode = $('#AmountDisburseID option:selected').text().split(' ')[0];
        var intRateCode = $('#Interest option:selected').text();
        var baseRate = $("#BaseRate").val() == '' ? 0 : $("#BaseRate").val();
        var accPref = $("#AccountPreferencial").val() == '' ? 0 : $("#AccountPreferencial").val();
        var specFTP = $("#SpecialFTP").val() == '' ? 0 : $("#SpecialFTP").val();
        var Margin = $("#Margin").val() == '' ? 0 : $("#Margin").val();
        var AllInSpecialRate = $("#AllInSpecialRate").val() == '' ? 0 : $('#AllInSpecialRate').val();
        var allInRate = "0";
        if (currCode == "" || intRateCode == "") allInRate = "0";
        else {
            if (specFTP == 0) allInRate = parseFloat(baseRate) + parseFloat(accPref);
            else {
                if (baseRate + accPref == specFTP + Margin) allInRate = parseFloat(baseRate) + parseFloat(accPref);
                else allInRate = parseFloat(baseRate) + parseFloat(accPref);
            }
            AllInSpecialRate = parseFloat(specFTP) + parseFloat(Margin);
        }

        self.AllInRate(allInRate.toFixed(2));
        self.AllInSpecialRate(AllInSpecialRate.toFixed(2))
        if (specFTP != 0 || Margin != 0) {
            if (AllInSpecialRate != 0) {
                if (AllInSpecialRate != allInRate) {
                    ShowNotification("Not Match", "All In Rate And All In Special Rate Not Match", "gritter-warning", false);
                    return;
                }

            }
        }
        if (AllInSpecialRate > allInRate) {
            ShowNotification("Not Match", "All In Rate And All In Special Rate Not Match", "gritter-warning", false);
            return;
        }


    }

    var SelectedModel = {
        DocumentPurpose_a: ko.observable(),
        DocumentType_a: ko.observable(),
        DocumentType: ko.observable(),
        DocumentPurpose: ko.observable(),
        DocumentPath_a: ko.observable()
    };

    var CustomerModel = {
        CIF: ko.observable(),
        CustomerName: ko.observable(),
        CustomerCategoryID: ko.observable(),
        Name: ko.observable(),
        POAName: ko.observable(),
        BizSegment: ko.observable(BizSegmentModel),
        BizSegmentID: ko.observable(),
        CustomerTypeID: ko.observable(),
        Branch: ko.observable(),
        Type: ko.observable(),
        RM: ko.observable(),
        Contacts: ko.observableArray(),
        Functions: ko.observableArray(),
        Accounts: ko.observableArray(),
        Underlyings: ko.observableArray(),
        Documents: ko.observableArray([])
    };
    var FundingMemoDocumentModel = {
        TypeID: ko.observable(),
        PurposeID: ko.observable(),
        FundingMemoID: ko.observable(),
        FileName: ko.observable(),
        DocumentPath: ko.observable(),
        DocumentType: ko.observable(),
        LastModifiedBy: ko.observable(),
    }

    var LocationModel = {
        ID: ko.observable(),
        Code: ko.observable(),
        LocationName: ko.observable(),
        Region: ko.observable()
    }

    var BizSegmentModel = {
        ID: ko.observable(),
        Name: ko.observable(),
        Description: ko.observable()
    };

    //for loanType
    var ParameterSystemModel = function (LoanTypeID, LoanTypeName) {
        var self = this;
        self.LoanTypeID = ko.observable(LoanTypeID);
        self.LoanTypeName = ko.observable(LoanTypeName);
    }

    var ProgramType = function (ProgramTypeID, ProgramTypeName) {
        var self = this;
        self.ProgramTypeID = ko.observable(ProgramTypeID);
        self.ProgramTypeName = ko.observable(ProgramTypeName);
    };

    var FinacleScheme = function (FinacleSchemeCodeID, FinacleSchemeCodeName) {
        var self = this;
        self.FinacleSchemeCodeID = ko.observable(FinacleSchemeCodeID),
        self.FinacleSchemeCodeName = ko.observable(FinacleSchemeCodeName)
    };
    var Interest = function (InterestID, InterestCodeID, InterestCodeName) {
        var self = this;
        self.InterestID = ko.observable(InterestID),
        self.InterestCodeID = ko.observable(InterestCodeID),
        self.InterestCodeName = ko.observable(InterestCodeName)
    };
    var RepricingPlan = function (RepricingPlanID, RepricingPlanName) {
        var self = this;
        self.RepricingPlanID = ko.observable(RepricingPlanID),
        self.RepricingPlanName = ko.observable(RepricingPlanName)
    };
    var PeggingFrequency = function (PeggingFrequencyID, PeggingFrequencyName) {
        var self = this;
        self.PeggingFrequencyID = ko.observable(PeggingFrequencyID),
        self.PeggingFrequencyName = ko.observable(PeggingFrequencyName)
    };
    var InterestFrequency = function (InterestFrequencyID, InterestFrequencyName) {
        var self = this;
        self.InterestFrequencyID = ko.observable(InterestFrequencyID),
        self.InterestFrequencyName = ko.observable(InterestFrequencyName)
    };
    var PrincipalFrequency = function (PrincipalFrequencyID, PrincipalFrequencyName) {
        var self = this;
        self.PrincipalFrequencyID = ko.observable(PrincipalFrequencyID),
        self.PrincipalFrequencyName = ko.observable(PrincipalFrequencyName)
    };

    var CurrencyModel = function (id, name) {
        var self = this;
        self.ID = ko.observable(id);
        self.CodeDescription = ko.observable(name);

    };
    var OutstandingModel = function (id, name) {
        var self = this;
        self.ID = ko.observable(id);
        self.CodeDescription = ko.observable(name);

    };
    var utilizationModel = function (id, name) {
        var self = this;
        self.ID = ko.observable(id);
        self.CodeDescription = ko.observable(name);

    };
    var SanctionLimitModel = function (id, name) {
        var self = this;
        self.ID = ko.observable(id);
        self.CodeDescription = ko.observable(name);

    };

    var GridPropertiesModel = function (callback) {
        var self = this;

        self.SortColumn = ko.observable();
        self.SortOrder = ko.observable();
        self.AllowFilter = ko.observable(false);
        self.AvailableSizes = ko.observableArray(config.sizes);
        self.Page = ko.observable(1);
        self.Size = ko.observable(config.sizeDefault);
        self.Total = ko.observable(0);
        self.TotalPages = ko.observable(0);

        self.Callback = function () {
            callback();
        };

        self.OnPageSizeChange = function () {
            self.Page(1);

            self.Callback();
        };

        self.OnPageChange = function () {
            if (self.Page() < 1) {
                self.Page(1);
            } else {
                if (self.Page() > self.TotalPages())
                    self.Page(self.TotalPages());
            }

            self.Callback();
        };

        self.NextPage = function () {
            var page = self.Page();
            if (page < self.TotalPages()) {
                self.Page(page + 1);

                self.Callback();
            }
        };

        self.PreviousPage = function () {
            var page = self.Page();
            if (page > 1) {
                self.Page(page - 1);

                self.Callback();
            }
        };

        self.FirstPage = function () {
            self.Page(1);

            self.Callback();
        };

        self.LastPage = function () {
            self.Page(self.TotalPages());

            self.Callback();
        };

        self.Filter = function (data) {
            self.Page(1);

            self.Callback();
        };

        self.GetSortedColumn = function (columnName) {
            var sort = "sorting";

            if (self.SortColumn() == columnName) {
                if (self.SortOrder() == "DESC")
                    sort = sort + "_asc";
                else
                    sort = sort + "_desc";
            }

            return sort;
        };

        // Sorting data
        self.Sorting = function (column) {
            if (self.SortColumn() == column) {
                if (self.SortOrder() == "ASC")
                    self.SortOrder("DESC");
                else
                    self.SortOrder("ASC");
            } else {
                self.SortOrder("ASC");
            }

            self.SortColumn(column);

            self.Page(1);

            self.Callback();
        };
    };
    //Andri 12 Januari
    self.Employee = ko.observableArray([]);
    self.FinacleSchemeCodeNames = ko.observableArray([]);
    self.Selected = ko.observable(SelectedModel);
    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);
    //Declare observable which will be bind with UI
    self.CSOFundingMemoID = ko.observable("");
    self.CIF = ko.observable(CustomerModel);
    self.Customer = ko.observable(CustomerModel);
    self.CustomerName = ko.observable("");
    self.CustomerCategoryID = ko.observable("");
    self.LoanID = ko.observable(new ParameterSystemModel('', ''));
    self.Currency = ko.observable(new CurrencyModel('', ''));
    self.SolID = ko.observable("");
    self.BizSegment = ko.observable(BizSegmentModel);
    self.BizName = ko.observable("");
    self.BizSegmentID = ko.observable("");
    self.ProgramType = ko.observable(new ProgramType('', ''));
    self.ProgramTypeID = ko.observable("");
    self.FinacleScheme = ko.observable(new FinacleScheme('', ''));
    self.AmountDisburseID = ko.observable(new CurrencyModel('', ''));
    self.AmountDisburse = ko.observable("");
    self.CreditingOperative = ko.observable("");
    self.DebitingOperative = ko.observable("");
    self.ValueDate = ko.observable("");
    self.MaturityDate = ko.observable("");
    self.Interest = ko.observable(new Interest('', ''));
    self.BaseRate = ko.observable("");
    self.AllInRate = ko.observable("");
    self.RepricingPlan = ko.observable(new RepricingPlan('', ''));
    self.AllInSpecialRate = ko.observable("");
    self.PeggingDate = ko.observable("");
    self.PeggingFrequency = ko.observable(new PeggingFrequency('', ''));
    self.Margin = ko.observable("");
    self.SpecialFTP = ko.observable("");
    self.ApprovedMarginAsPerCM = ko.observable("");
    self.AccountPreferencial = ko.observable("");
    self.PrincipalFrequency = ko.observable(new PrincipalFrequency('', ''))
    self.DOANAme = ko.observable("");
    //lineIV
    self.NoOfInstallment = ko.observable("");
    self.InterestFrequency = ko.observable(new InterestFrequency('', ''));//use in Principal
    self.PrincipalStartDate = ko.observable("");//add
    self.InterestStartDate = ko.observable("");//add
    //LineV
    self.LimitIDinFin10 = ko.observable("");
    self.LimitExpiryDate = ko.observable("");
    //tambah baru
    self.SanctionLimitCurrID = ko.observable(new SanctionLimitModel('', ''));
    self.SanctionLimitCurr = ko.observable(new SanctionLimitModel('', ''));
    self.SanctionLimitAmount = ko.observable("");
    self.utilization = ko.observable(new utilizationModel('', ''));
    self.utilizationID = ko.observable(new utilizationModel('', ''));
    self.utilizationAmount = ko.observable("");
    self.Outstanding = ko.observable(new OutstandingModel('', ''));
    self.OutstandingID = ko.observable(new OutstandingModel('', ''));
    self.OutstandingAmount = ko.observable("");
    //LineVI
    self.CSOName = ko.observable("");
    //LineVII
    self.Remarks = ko.observable("");
    self.IsAdhoc = ko.observable("");
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");
    //Dropdownlist
    self.ddlAmountDisburseID = ko.observableArray([]);
    self.ddlFinacleScheme = ko.observableArray([]);
    self.ddlProgramType = ko.observableArray([]);
    self.ddlLoanID = ko.observableArray([]);
    self.ddlInterest = ko.observableArray([]);
    self.ddlRepricing = ko.observableArray([]);
    self.ddlBizSegmentDesc = ko.observableArray([]);
    self.ddlSanctionLimitCurrID = ko.observableArray([]);
    self.ddlUtilizationCurrID = ko.observableArray([]);
    self.ddlOutstandingCurrID = ko.observableArray([]);
    self.ddlFrequency = ko.observableArray([]);

    // Temporary documents
    self.Documents = ko.observableArray([]);
    self.DocumentPath = ko.observable();
    self.DocumentType = ko.observable();
    self.DocumentPurpose = ko.observable();
    self.DocumentFileName = ko.observable();
    self.fundingmemodocument = ko.observable(DocumentModel);
    self.fundingmemodocument = ko.observableArray([]);
    self.TempNewDocument = ko.observableArray([]);
    self.Employee = ko.observable(EmployeeModel);
    self.Employee = ko.observableArray([]);
    self.DocumentsFunding = ko.observable(DocumentsFundingModel);
    self.fundingmemodocument = ko.observableArray([]);

    // filter
    self.FilterCIF = ko.observable("");
    self.FilterCustomer = ko.observable("");
    //self.FilterBizSegment = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");

    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    // New Data flag
    self.IsNewData = ko.observable(false);
    self.IsUpdateData = ko.observable(false);
    self.IsDelete = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.Fundings = ko.observableArray([]);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    // set default sorting
    self.GridProperties().SortColumn("CIF");
    self.GridProperties().SortOrder("ASC");

    // bind clear filters
    self.ClearFilters = function () {

        self.FilterCIF("");
        self.FilterCustomer("");
        //self.FilterBizSegment("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");

        GetData();
    };
    self.GetDropdown = function () {
        GetDropdown();
    };

    // bind get data function to view
    self.GetData = function () {
        GetData();
    };

    self.GetDataAttachFile = function () {
        GetDataAttachFile();
    }
    // Add new document handler
    self.AddDocument = function () {
        var doc = {
            ID: 0,
            FileName: self.DocumentPath_a().name,
            Type: ko.utils.arrayFirst(self.ddlDocumentType_a(), function (item) {
                return item.ID == self.Selected().DocumentType_a();
            }),
            Purpose: ko.utils.arrayFirst(self.ddlDocumentPurpose_a(), function (item) {
                return item.ID == self.Selected().DocumentPurpose_a();
            }),
            DocumentPath: self.DocumentPath_a(),
            LastModifiedDate: new Date(),
            LastModifiedBy: null,
        };

        if (doc.DocumentPath == null) {
            alert("Please complete the upload form fields.")
        } else {
            self.Documents.push(doc);
            self.TempNewDocument.push(doc);
            $('.remove').click();
            $("#modal-form-Attach1").modal('hide');
        }

    };

    //Remove Document
    self.RemoveDocument = function (data) {
        self.Documents.remove(data);
        viewModel.Documents.remove(data);
    }
    //End
    self.cancel_a = function () {
        //$("#modal-form-Attach1").modal('hide');
        $("#modal-form").modal('show');
        //$('#backDrop').hide();
        $("#modal-form-Attach1").modal('hide');
        return false;
    };

    //The Object which stored data entered in the observables
    var Funding = {
        Customer: self.Customer,
        CustomerName: self.CustomerName,
        CIF: self.CIF,
        BizName: self.BizName,
        BizSegment: self.BizSegment,
        BizSegmentID: self.BizSegmentID,
        // Currency: self.Currency,
        Location: self.Location,
        CSOName: self.CSOName,
        SolID: self.SolID,
        LoanID: self.LoanID,
        CustomerCategoryID: self.CustomerCategoryID,
        AmountDisburseID: self.AmountDisburseID,
        AmountDisburse: self.AmountDisburse,
        Interest: self.Interest,
        PeggingFrequency: self.PeggingFrequency,
        RepricingPlan: self.RepricingPlan,
        CSOFundingMemoID: self.CSOFundingMemoID,
        TransactionFundingMemoID: self.TransactionFundingMemoID,
        ValueDate: self.ValueDate,
        IsAdhoc: self.IsAdhoc,
        CreditingOperativeID: self.CreditingOperative,
        DebitingOperativeID: self.DebitingOperative,
        CreditingOperative: self.CreditingOperative,
        DebitingOperative: self.DebitingOperative,
        MaturityDate: self.MaturityDate,
        PrincipalStartDate: self.PrincipalStartDate,//add
        InterestStartDate: self.InterestStartDate,//add
        InterestFrequency: self.InterestFrequency,
        PrincipalFrequency: self.PrincipalFrequency,
        BaseRate: self.BaseRate,
        AccountPreferencial: self.AccountPreferencial,
        AllInRate: self.AllInRate,
        ApprovedMarginAsPerCM: self.ApprovedMarginAsPerCM,
        SpecialFTP: self.SpecialFTP,
        Margin: self.Margin,
        AllInSpecialRate: self.AllInSpecialRate,
        PeggingDate: self.PeggingDate,
        NoOfInstallment: self.NoOfInstallment,
        LimitIDinFin10: self.LimitIDinFin10,
        LimitExpiryDate: self.LimitExpiryDate,
        SanctionLimitCurr: self.SanctionLimitCurrID,
        SanctionLimitAmount: self.SanctionLimitAmount,//add
        utilizationID: self.utilization,
        utilizationAmount: self.utilizationAmount,//add
        OutstandingID: self.Outstanding,
        OutstandingAmount: self.OutstandingAmount,//add
        OutstandingCurrID: self.OutstandingCurrID,
        Remarks: self.Remarks,
        OtherRemarks: self.OtherRemarks,
        ProgramType: self.ProgramType,
        FinacleScheme: self.FinacleScheme,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate,
        fundingmemodocument: self.fundingmemodocument,
        EmployeeFunding: self.Employee,
        CSOName: self.CSOName,
        DoAName: self.DOANAme,
        ProgramTypeID: self.ProgramTypeID,


    };
    self.GetCSOName = function () {
        var cif = $('#CIF').val();

        var options = {
            url: api.server + api.url.fundingmemotl + "/getcso",
            params: {
                query: cif,
            },
            token: accessToken
        };
        Helper.Ajax.Get(options, OnSuccessGetCSOName, OnError, OnAlways);
    }

    self.GetDOA = function () {
        GetDOA(accessToken);
    };
    self.IsPermited = function (IsPermitedResult) {
        spUser = $.cookie(api.cookie.spUser);
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    console.log("spUser Roles : " + ko.toJSON(spUser.Roles));
                    console.log("spUser Permited : " + data);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
    self.save = function () {

        //var Validasi I
        var cif = $('#CIF').val();

        var CustomerCategoryID = $('#CustomerCategoryID').val();

        if (CustomerCategoryID == "" || CustomerCategoryID == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Customer Category)", 'gritter-warning', false);
            return;
        }
        if (CustomerCategoryID == "Affiliate") {
            CustomerCategoryID = ConsCustomer.Affiliate;
        } else {
            CustomerCategoryID = ConsCustomer.NonAffiliate;
        }
        var BizSegment = $('#BizSegmentDesc').val();

        var customername = $('#Customer').val();
        var ProgramType = $('#ProgramType option:selected').val();
        //if (ProgramType == "" || ProgramType == null) {
        //    ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        //    return;
        //}
        var FinacleScheme = $('#FinacleScheme option:selected').val();
        if (FinacleScheme == "" || FinacleScheme == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Finacle Scheme Code)", 'gritter-warning', false);
            return;
        }
        var LoanType = $('#LoanID').val();
        if (LoanType == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
            return;
        }
        //End Validasi I
        //Validasi II
        var AmountDisburse = $('#AmountDisburseID option:selected').val();
        if (AmountDisburse == "" || AmountDisburse == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Amount Disbursement Currency)", 'gritter-warning', false);
            return;
        }
        var AmountDisburseAmount = $('#AmountDisburse').val();
        if (AmountDisburseAmount == "" || AmountDisburseAmount == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Amount Disbursement)", 'gritter-warning', false);
            return;
        }
        var CreditOperative = $('#CreditingOperative').val();
        if (CreditOperative == "" || CreditOperative == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Crediting Operative)", 'gritter-warning', false);
            return;
        }
        var DebitingOperative = $('#DebitingOperative').val();
        if (DebitingOperative == "" || DebitingOperative == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Debiting Operative)", 'gritter-warning', false);
            return;
        }
        var MaturityDate = $('#MaturityDate').val();
        if (MaturityDate == "" || MaturityDate == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Maturity Date)", 'gritter-warning', false);
            return;
        }
        var ValueDate = $('#ValueDate').val();
        if (ValueDate == "" || ValueDate == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Value Date)", 'gritter-warning', false);
            return;

        }
        //EndValidasiII


        //Validasi III
        var InterestRestCode = $('#Interest').val();
        if (InterestRestCode == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Interest Rate Code)", 'gritter-warning', false);
            return;
        }
        var BaseRate = $('#BaseRate').val();
        if (BaseRate == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Base Rate)", 'gritter-warning', false);
            return;
        }
        var AccountPreferencial = $('#AccountPreferencial')
        if (AccountPreferencial == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Account Preferential)", 'gritter-warning', false);
            return;
        }
        var RepricingPlan = $('#RepricingPlan')
        if (RepricingPlan == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Repricing Plan)", 'gritter-warning', false);
            return;
        }
        var PeggingDate = $('#PeggingDate').val();
        if (PeggingDate == "" || PeggingDate == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Pegging Date)", 'gritter-warning', false);
            return;
        }
        var PeggingFrequency = $('#PeggingFrequency').val();
        if (PeggingFrequency == "" || PeggingFrequency == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Pegging Frequency)", 'gritter-warning', false);
            return;
        }
        //EndValidasi III
        //  var CurrencyID = $('#Currency').val();
        //Validasi IV
        if (FinacleScheme == 107 || FinacleScheme == 109) {
            var PrincipalStartDate = $('#PrincipalStartDate').val();
            if (PrincipalStartDate == "" || PrincipalStartDate == null) {
                ShowNotification("Form Validation Warning", "Mandatory field must be filled(Principal Start Date)", 'gritter-warning', false);
                return;
            }
            var InterestStartDate = $('#InterestStartDate').val();
            if (InterestStartDate == "" || InterestStartDate == null) {
                ShowNotification("Form Validation Warning", "Mandatory field must be filled(Interest Start Date)", 'gritter-warning', false);
                return;
            }
            var PrincipalFrequency = $('#PrincipalFrequency').val();
            if (PrincipalFrequency == "" || PrincipalFrequency == null) {
                ShowNotification("Form Validation Warning", "Mandatory field must be filled(Principal Frequency)", 'gritter-warning', false);
                return;
            }
            var InterestFrequency = $('#InterestFrequency').val();
            if (InterestFrequency == "" || InterestFrequency == null) {
                ShowNotification("Form Validation Warning", "Mandatory field must be filled(Interest Frequency)", 'gritter-warning', false);
                return;
            }

        }

        //End Validasi
        //Validasi V
        var SanctionLimitCurr = $('#SanctionLimitCurrID option:selected').val();
        if (SanctionLimitCurr == "" || SanctionLimitCurr == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Sanction Limit Currency)", 'gritter-warning', false);
            return;
        }
        var SanctionLimitAmount = $('#SanctionLimitAmount').val();
        if (SanctionLimitAmount == "" || SanctionLimitAmount == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Sanction Limit Amount)", 'gritter-warning', false);
            return;
        }
        var Utilization = $('#utilization option:selected').val();
        //if (Utilization == "" || Utilization == null) {
        //    ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        //    return;
        //}
        var Outstanding = $('#Outstanding').val();
        //if (Utilization == "" || Utilization == null) {
        //    ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        //    return;
        //}
        var LimitExpiryDate = $('#LimitExpiryDate').val();
        if (LimitExpiryDate == "" || LimitExpiryDate == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Limit Expiry Date)", 'gritter-warning', false);
            return;
        }
        var limit = $('#LimitIDinFin10').val();
        if (limit == "" || limit == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Limit ID in Fin 10)", 'gritter-warning', false);
            return;
        }
        //End Validasi V

        var BizName = $('#BizSegmentDesc').val();

        var LoanID = $('#LoanID option:selected').val();
        if (LoanID == "" || LoanID == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
            return;
        }

        var interestid = $('#Interest').val();
        if (interestid == "" || interestid == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Interest)", 'gritter-warning', false);
            return;
        }
        var CSOName = $('#Employee').val();
        var RepricingPlanID = $('#RepricingPlan').val();
        if (RepricingPlanID == "" || RepricingPlanID == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Repricing Plan)", 'gritter-warning', false);
            return;
        }
        var PeggingFrequencyID = $('#PeggingFrequency').val();
        if (PeggingFrequencyID == "" || PeggingFrequencyID == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Pagging Frequency)", 'gritter-warning', false);
            return;
        }

        if ($('#Employee').val() == null || $('#Employee').val() == "") {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(CSO Name)", 'gritter-warning', false)
            return;
        }
        if ($('#DOAName').val() == null || $('#DOAName').val() == "") {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(DOA Approval)", 'gritter-warning', false)
            return;
        }

        var DoAName = $('#DOAName').val();
        if (DoAName != null) {

            var items = ko.utils.arrayFilter(viewModel.Documents(), function (item) {
                return item.Type.Name == config.validate.approval.emailapp;

            });
            if (items.length == 0) {
                ShowNotification("Form Validation Warning", "No Attachment Document Type Email Approval", 'gritter-warning', false);
                return;
            }
        }


        var BizID = $('#BizSegmentID').val();
        if ($('#IsAdhoc').is(":checked")) {
            Funding.IsAdhoc(true);
        } else {
            Funding.IsAdhoc(false);
        }
        var adhocval = $('#IsAdhoc').val();
        //if (adhocval = "true") {
        if (Funding.IsAdhoc() == true) {
            var items = ko.utils.arrayFilter(viewModel.Documents(), function (item) {
                return item.Type.Name == config.validate.approval.adhocapp;

            });
            if (items.length == 0) {
                ShowNotification("Form Validation Warning", "No Attachment Document Type adhoc Approval", 'gritter-warning', false);
                return;
            }
        }
        var specFTP = $("#SpecialFTP").val() == '' ? 0 : $("#SpecialFTP").val();
        var Margin = $("#Margin").val() == '' ? 0 : $("#Margin").val();
        var AllInSpecialRate = $("#AllInSpecialRate").val() == '' ? 0 : $('#AllInSpecialRate').val();
        var allInRate = $("#AllInRate").val() == '' ? 0 : $('#AllInRate').val();
        if (specFTP != "" || specFTP != "0" || Margin != "" || Margin != "0") {
            if (AllInSpecialRate != allInRate) {
                ShowNotification("Not Match", "All In Rate And All In Special Rate Not Match", "gritter-warning", false);
                return;
            }

        }
        if (AllInSpecialRate > allInRate) {
            ShowNotification("Not Match", "All In Rate And All In Special Rate Not Match", "gritter-warning", false);
            return;
        }

        self.LoanID().LoanTypeID(LoanID);
        self.ProgramType().ProgramTypeID(ProgramType);
        self.FinacleScheme().FinacleSchemeCodeID(FinacleScheme);
        self.SanctionLimitCurrID().ID(SanctionLimitCurr);
        self.utilization().ID(Utilization);
        self.Outstanding().ID(Outstanding);
        self.AmountDisburseID().ID(AmountDisburse);
        self.Interest().InterestID(interestid);
        self.RepricingPlan().RepricingPlanID(RepricingPlanID);
        self.PeggingFrequency().PeggingFrequencyID(PeggingFrequencyID);
        CustomerModel.Name(customername);
        CustomerModel.CIF(cif);
        CustomerModel.CustomerCategoryID(CustomerCategoryID);
        Funding.CreditingOperative(CreditOperative);
        Funding.LimitIDinFin10(limit);
        Funding.DoAName(DoAName);
        Funding.CSOName(CSOName);
        Funding.BizName(BizName);
        Funding.CIF(cif);
        Funding.Customer(CustomerModel);
        Funding.BizSegmentID(BizID);

        bootbox.confirm("Are you sure?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
                var data = {
                    ApplicationID: '',
                    CIF: viewModel.Customer().CIF(),
                    Name: viewModel.Customer().Name()
                };
                if (viewModel.TempNewDocument().length > 0) {
                    UploadFileRecuresive(data, viewModel.TempNewDocument(), SaveFundingMemo, viewModel.TempNewDocument().length);
                }
                else {
                    SaveFundingMemo();
                }

            }
        });
    };
    function StartOutStanding() {
        OutstandingAmount = document.getElementById("OutstandingAmount");
        if (OutstandingAmount != null) {
            var xstored = OutstandingAmount.getAttribute("data-in");

            setInterval(function () {
                if (OutstandingAmount == document.activeElement) {
                    calculateOutStanding();
                }
            }, 100);
        }
    }
    function StartSanctionLimit() {
        Sanction = document.getElementById("SanctionLimitAmount");
        if (Sanction != null) {
            var xstored = Sanction.getAttribute("data-in");

            setInterval(function () {
                if (Sanction == document.activeElement) {
                    calculateSanctionLimit();
                }
            }, 100);
        }
    }
    function StartutilizationAmount() {
        utilizationAmount = document.getElementById("utilizationAmount");
        if (utilizationAmount != null) {
            var xstored = Sanction.getAttribute("data-in");

            setInterval(function () {
                if (utilizationAmount == document.activeElement) {
                    calculateUtilaziton();
                }
            }, 100);
        }
    }
    function startCalculateLoan() {
        loan_a = document.getElementById("AmountDisburse");
        if (loan_a != null) {
            var xstored = loan_a.getAttribute("data-in");

            setInterval(function () {
                if (loan_a == document.activeElement) {
                    calculateLoan();
                }
            }, 100);
        }

    }
    function calculateLoan() {
        loan_a.value = loan_a.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        viewModel.AmountDisburse(loan_a.value);
        loan_a.value = formatNumber(loan_a.value);
    };
    function calculateSanctionLimit() {
        Sanction.value = Sanction.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        viewModel.SanctionLimitAmount(Sanction.value);
        Sanction.value = formatNumber(Sanction.value);
    };
    function calculateUtilaziton() {
        utilizationAmount.value = utilizationAmount.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        viewModel.utilizationAmount(utilizationAmount.value);
        utilizationAmount.value = formatNumber(utilizationAmount.value);
    };
    function calculateOutStanding() {
        OutstandingAmount.value = OutstandingAmount.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        viewModel.OutstandingAmount(OutstandingAmount.value);
        OutstandingAmount.value = formatNumber(OutstandingAmount.value);
    };

    function SaveFundingMemo() {
        if (viewModel.fundingmemodocument().length == viewModel.Documents().length) {
            $.ajax({
                type: "POST",
                url: api.server + api.url.fundingmemotl,
                data: ko.toJSON(Funding),
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                        $("#modal-form").modal('hide');
                        GetData();
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }
    }
    function UploadFileRecuresive(context, document, callBack, numFile) {

        var indexDocument = document.length - numFile;
        var serverRelativeUrlToFolder = '';
        serverRelativeUrlToFolder = '/Instruction Documents';

        var parts = document[indexDocument].DocumentPath.name.split('.');
        var fileExtension = parts[parts.length - 1];
        var timeStamp = new Date().getTime();
        var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

        // Get the server URL.
        var serverUrl = _spPageContextInfo.webAbsoluteUrl;

        // output variable
        var output;

        // Initiate method calls using jQuery promises.
        // Get the local file as an array buffer.
        var getFile = getFileBuffer();
        getFile.done(function (arrayBuffer) {

            // Add the file to the SharePoint folder.
            var addFile = addFileToFolder(arrayBuffer);
            addFile.done(function (file, status, xhr) {
                output = file.d;

                // Get the list item that corresponds to the uploaded file.
                var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
                getItem.done(function (listItem, status, xhr) {

                    // Change the display name and title of the list item.
                    var changeItem = updateListItem(listItem.d.__metadata);
                    changeItem.done(function (data, status, xhr) {
                        //alert('file uploaded and updated');
                        //return output;
                        var newDoc = {
                            ID: 0,
                            Type: document[indexDocument].Type,
                            Purpose: document[indexDocument].Purpose,
                            LastModifiedDate: null,
                            LastModifiedBy: null,
                            DocumentPath: output.ServerRelativeUrl,
                            FileName: document[indexDocument].DocumentPath.name
                        };
                        viewModel.fundingmemodocument.push(newDoc);
                        if (numFile > 1) {
                            UploadFileRecuresive(context, document, callBack, numFile - 1); // recursive function
                        }
                        callBack();
                    });
                    changeItem.fail(OnError);
                });
                getItem.fail(OnError);
            });
            addFile.fail(OnError);
        });
        getFile.fail(OnError);

        // Get the local file as an array buffer.
        function getFileBuffer() {
            var deferred = $.Deferred();
            var reader = new FileReader();
            reader.onloadend = function (e) {
                deferred.resolve(e.target.result);
            }
            reader.onerror = function (e) {
                deferred.reject(e.target.error);
            }

            if (document[indexDocument].DocumentPath.type == Util.spitemdoc) {
                var fileURI = serverUrl + document[indexDocument].DocumentPath.DocPath;
                // load file:
                fetch(fileURI, convert, alert);

                function convert(buffer) {
                    var blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });

                    var domURL = self.URL || self.webkitURL || self,
                      url = domURL.createObjectURL(blob),
                      img = new Image;

                    img.onload = function () {
                        domURL.revokeObjectURL(url); // clean up
                        //document.body.appendChild(this);
                        // this = image
                    };
                    img.src = url;
                    reader.readAsArrayBuffer(blob);
                }

                function fetch(url, callback, error) {

                    var xhr = new XMLHttpRequest();
                    try {
                        xhr.open("GET", url);
                        xhr.responseType = "arraybuffer";
                        xhr.onerror = function () {
                            error("Network error")
                        };
                        xhr.onload = function () {
                            if (xhr.status === 200) callback(xhr.response);
                            else error(xhr.statusText);
                        };
                        xhr.send();
                    } catch (err) {
                        error(err.message)
                    }
                }


            } else {
                reader.readAsArrayBuffer(document[indexDocument].DocumentPath);
            }
            //bangkit

            //reader.readAsDataURL(document.DocumentPath);
            return deferred.promise();
        }

        // Add the file to the file collection in the Shared Documents folder.
        function addFileToFolder(arrayBuffer) {

            // Construct the endpoint.
            var fileCollectionEndpoint = String.format(
                    "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                    "/add(overwrite=false, url='{2}')",
                serverUrl, serverRelativeUrlToFolder, fileName);

            // Send the request and return the response.
            // This call returns the SharePoint file.
            return $.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val()
                    //"content-length": arrayBuffer.byteLength
                }
            });
        }

        // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
        function getListItem(fileListItemUri) {

            // Send the request and return the response.
            return $.ajax({
                url: fileListItemUri,
                type: "GET",
                headers: { "accept": "application/json;odata=verbose" }
            });
        }

        // Change the display name and title of the list item.
        function updateListItem(itemMetadata) {
            var body = {
                Title: document[indexDocument].DocumentPath.name,
                Application_x0020_ID: context.ApplicationID,
                CIF: context.CIF,
                Customer_x0020_Name: context.Name,
                Document_x0020_Type: document[indexDocument].Type.Name,
                Document_x0020_Purpose: document[indexDocument].Purpose.Name,
                __metadata: {
                    type: itemMetadata.type,
                    FileLeafRef: fileName,
                    Title: fileName
                }
            };

            // Send the request and return the promise.
            // This call does not return response content from the server.
            return $.ajax({
                url: itemMetadata.uri,
                type: "POST",
                data: ko.toJSON(body),
                headers: {
                    "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                    "content-type": "application/json;odata=verbose",
                    //"content-length": body.length,
                    "IF-MATCH": itemMetadata.etag,
                    "X-HTTP-Method": "MERGE"
                }
            });
        }
    }
    self.update = function () {

        //var Validasi I
        var cif = $('#CIF').val();

        var CustomerCategoryID = $('#CustomerCategoryID').val();

        if (CustomerCategoryID == "" || CustomerCategoryID == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
            return;
        }
        if (CustomerCategoryID == "Affiliate") {
            CustomerCategoryID = ConsCustomer.Affiliate;
        } else {
            CustomerCategoryID = ConsCustomer.NonAffiliate;
        }
        var BizSegment = $('#BizSegmentDesc').val();

        var customername = $('#Customer').val();
        var ProgramType = $('#ProgramType option:selected').val();
        //if (ProgramType == "" || ProgramType == null) {
        //    ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        //    return;
        //}
        var FinacleScheme = $('#FinacleScheme option:selected').val();
        if (FinacleScheme == "" || FinacleScheme == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
            return;
        }
        var LoanType = $('#LoanID').val();
        if (LoanType == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
            return;
        }
        //End Validasi I
        //Validasi II
        var AmountDisburse = $('#AmountDisburseID option:selected').val();
        if (AmountDisburse == "" || AmountDisburse == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Amount Disbursement Currency)", 'gritter-warning', false);
            return;
        }
        var AmountDisburseAmount = $('#AmountDisburse').val();
        if (AmountDisburseAmount == "" || AmountDisburseAmount == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Amount Disbursement)", 'gritter-warning', false);
            return;
        }
        var CreditOperative = $('#CreditingOperative').val();
        if (CreditOperative == "" || CreditOperative == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Crediting Operative)", 'gritter-warning', false);
            return;
        }
        var DebitingOperative = $('#DebitingOperative').val();
        if (DebitingOperative == "" || DebitingOperative == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Debiting Operative)", 'gritter-warning', false);
            return;
        }
        var MaturityDate = $('#MaturityDate').val();
        if (MaturityDate == "" || MaturityDate == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Maturity Date)", 'gritter-warning', false);
            return;
        }
        var ValueDate = $('#ValueDate').val();
        if (ValueDate == "" || ValueDate == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Value Date)", 'gritter-warning', false);
            return;

        }
        //EndValidasiII


        //Validasi III
        var InterestRestCode = $('#Interest').val();
        if (InterestRestCode == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Interest)", 'gritter-warning', false);
            return;
        }
        var BaseRate = $('#BaseRate').val();
        if (BaseRate == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Base Rate)", 'gritter-warning', false);
            return;
        }
        var AccountPreferencial = $('#AccountPreferencial')
        if (AccountPreferencial == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Account Preferential)", 'gritter-warning', false);
            return;
        }
        var RepricingPlan = $('#RepricingPlan')
        if (RepricingPlan == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Repricing Plan)", 'gritter-warning', false);
            return;
        }
        var PeggingDate = $('#PeggingDate').val();
        if (PeggingDate == "" || PeggingDate == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Pegging Date)", 'gritter-warning', false);
            return;
        }
        var PeggingFrequency = $('#PeggingFrequency').val();
        if (PeggingFrequency == "" || PeggingFrequency == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Pegging Frequency)", 'gritter-warning', false);
            return;
        }
        //EndValidasi III
        //  var CurrencyID = $('#Currency').val();
        //Validasi IV
        if (FinacleScheme == 107 || FinacleScheme == 109) {
            var PrincipalStartDate = $('#PrincipalStartDate').val();
            if (PrincipalStartDate == "" || PrincipalStartDate == null) {
                ShowNotification("Form Validation Warning", "Mandatory field must be filled(Principal Start Date)", 'gritter-warning', false);
                return;
            }
            var InterestStartDate = $('#InterestStartDate').val();
            if (InterestStartDate == "" || InterestStartDate == null) {
                ShowNotification("Form Validation Warning", "Mandatory field must be filled(Interest Start Date)", 'gritter-warning', false);
                return;
            }
            var PrincipalFrequency = $('#PrincipalFrequency').val();
            if (PrincipalFrequency == "" || PrincipalFrequency == null) {
                ShowNotification("Form Validation Warning", "Mandatory field must be filled(Principal Frequency)", 'gritter-warning', false);
                return;
            }
            var InterestFrequency = $('#InterestFrequency').val();
            if (InterestFrequency == "" || InterestFrequency == null) {
                ShowNotification("Form Validation Warning", "Mandatory field must be filled(Interest Frequency)", 'gritter-warning', false);
                return;
            }

        }

        //End Validasi
        //Validasi V
        var SanctionLimitCurr = $('#SanctionLimitCurrID option:selected').val();
        if (SanctionLimitCurr == "" || SanctionLimitCurr == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Sanction Limit Currency)", 'gritter-warning', false);
            return;
        }
        var SanctionLimitAmount = $('#SanctionLimitAmount').val();
        if (SanctionLimitAmount == "" || SanctionLimitAmount == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Sanction Limit Amount)", 'gritter-warning', false);
            return;
        }
        var Utilization = $('#utilization option:selected').val();
        //if (Utilization == "" || Utilization == null) {
        //    ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        //    return;
        //}
        var Outstanding = $('#Outstanding').val();
        //if (Utilization == "" || Utilization == null) {
        //    ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        //    return;
        //}
        var LimitExpiryDate = $('#LimitExpiryDate').val();
        if (LimitExpiryDate == "" || LimitExpiryDate == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Limit Expiry Date)", 'gritter-warning', false);
            return;
        }
        var limit = $('#LimitIDinFin10').val();
        if (limit == "" || limit == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Limit ID in FIn10)", 'gritter-warning', false);
            return;
        }
        //End Validasi V

        var BizName = $('#BizSegmentDesc').val();

        var LoanID = $('#LoanID option:selected').val();
        if (LoanID == "" || LoanID == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Loan ID)", 'gritter-warning', false);
            return;
        }

        var interestid = $('#Interest').val();
        if (interestid == "" || interestid == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Interest)", 'gritter-warning', false);
            return;
        }
        var CSOName = $('#Employee').val();
        var RepricingPlanID = $('#RepricingPlan').val();
        if (RepricingPlanID == "" || RepricingPlanID == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
            return;
        }
        var PeggingFrequencyID = $('#PeggingFrequency').val();
        if (PeggingFrequencyID == "" || PeggingFrequencyID == null) {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(Pegging Frequency)", 'gritter-warning', false);
            return;
        }

        var DoAName = $('#DOAName').val();
        if (DoAName != null) {

            var items = ko.utils.arrayFilter(viewModel.Documents(), function (item) {
                return item.Type.Name == config.validate.approval.emailapp;

            });
            if (items.length == 0) {
                ShowNotification("Form Validation Warning", "No Attachment Document Type Email Approval", 'gritter-warning', false);
                return;
            }
        }


        var BizID = $('#BizSegmentID').val();
        if ($('#IsAdhoc').is(":checked")) {
            Funding.IsAdhoc(true);
        } else {
            Funding.IsAdhoc(false);
        }
        var adhocval = $('#IsAdhoc').val();
        //if (adhocval = "true") {
        if (Funding.IsAdhoc() == true) {
            var items = ko.utils.arrayFilter(viewModel.Documents(), function (item) {
                return item.Type.Name == config.validate.approval.adhocapp;

            });
            if (items.length == 0) {
                ShowNotification("Form Validation Warning", "No Attachment Document Type adhoc Approval", 'gritter-warning', false);
                return;
            }
        }
        var specFTP = $("#SpecialFTP").val() == '' ? 0 : $("#SpecialFTP").val();
        var Margin = $("#Margin").val() == '' ? 0 : $("#Margin").val();
        var AllInSpecialRate = $("#AllInSpecialRate").val() == '' ? 0 : $('#AllInSpecialRate').val();
        var allInRate = $("#AllInRate").val() == '' ? 0 : $('#AllInRate').val();
        if (specFTP != "" || specFTP != "0" || Margin != "" || Margin != "0") {
            if (AllInSpecialRate != allInRate) {
                ShowNotification("Not Match", "All In Rate And All In Special Rate Not Match", "gritter-warning", false);
                return;
            }

        }
        if (AllInSpecialRate > allInRate) {
            ShowNotification("Not Match", "All In Rate And All In Special Rate Not Match", "gritter-warning", false);
            return;
        }
        if ($('#Employee').val() == null || $('#Employee').val() == "") {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled(CSO Name)", 'gritter-warning', false)
            return;
        }

        self.LoanID().LoanTypeID(LoanID);
        self.ProgramType().ProgramTypeID(ProgramType);
        self.FinacleScheme().FinacleSchemeCodeID(FinacleScheme);
        self.SanctionLimitCurrID().ID(SanctionLimitCurr);
        self.utilization().ID(Utilization);
        self.Outstanding().ID(Outstanding);
        self.AmountDisburseID().ID(AmountDisburse);
        self.Interest().InterestID(interestid);
        self.RepricingPlan().RepricingPlanID(RepricingPlanID);
        self.PeggingFrequency().PeggingFrequencyID(PeggingFrequencyID);
        CustomerModel.Name(customername);
        CustomerModel.CIF(cif);
        CustomerModel.CustomerCategoryID(CustomerCategoryID);
        Funding.CreditingOperative(CreditOperative);
        Funding.LimitIDinFin10(limit);
        Funding.DoAName(DoAName);
        Funding.CSOName(CSOName);
        Funding.BizName(BizName);
        Funding.CIF(cif);
        Funding.Customer(CustomerModel);
        Funding.BizSegmentID(BizID);
        if (viewModel.TempNewDocument != null) {
            for (var i = 0; viewModel.TempNewDocument.length > i; i++) {
                self.fundingmemodocument.push(viewModel.TempNewDocument[i]);
            }
        }

        bootbox.confirm("Are you sure?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
                var data = {
                    ApplicationID: '',
                    CIF: viewModel.Customer().CIF(),
                    Name: viewModel.Customer().Name()
                };
                if (viewModel.TempNewDocument().length > 0) {
                    UploadFileRecuresive(data, viewModel.TempNewDocument(), UpdateFundingMemo, viewModel.TempNewDocument().length);
                }
                else {
                    UpdateFundingMemo();
                }

            }
        });
        function UpdateFundingMemo() {
            if (viewModel.fundingmemodocument().length == viewModel.Documents().length) {
                $.ajax({
                    type: "PUT",
                    url: api.server + api.url.fundingmemotl + "/" + Funding.CSOFundingMemoID(),
                    data: ko.toJSON(Funding),
                    contentType: "application/json",
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        if (jqXHR.status = 200) {
                            ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                            $("#modal-form").modal('hide');
                            GetData();
                        } else {
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        }
        //    bootbox.confirm("Are you sure?", function (result) {
        //        if (!result) {
        //            $("#modal-form").modal('show');
        //        } else {
        //            CustomerModel.Name(customername);
        //            CustomerModel.CIF(cif);
        //            CustomerModel.CustomerCategoryID(CustomerCategoryID);
        //            BizSegmentModel.Name(BizSegment);
        //            Funding.CIF(cif);
        //            Funding.Customer(CustomerModel);
        //            Funding.BizSegment(BizSegmentModel);

        //            $.ajax({
        //                type: "PUT",
        //                url: api.server + api.url.fundingmemotl + "/" + Funding.CSOFundingMemoID(),
        //                data: ko.toJSON(Funding),
        //                contentType: "application/json",
        //                headers: {
        //                    "Authorization": "Bearer " + accessToken
        //                },
        //                success: function (data, textStatus, jqXHR) {
        //                    if (jqXHR.status = 200) {
        //                        // send notification
        //                        ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

        //                        // hide current popup window
        //                        $("#modal-form").modal('hide');

        //                        // refresh data
        //                        GetData();
        //                    } else {
        //                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        //                    }
        //                },
        //                error: function (jqXHR, textStatus, errorThrown) {
        //                    // send notification
        //                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        //                }
        //            });
        //        }
        //    });
        //    //}
    };

    self.delete = function () {
        var customername = $('#Customer').val();
        var ProgramType = $('#ProgramType option:selected').val();
        var FinacleScheme = $('#FinacleScheme option:selected').val();
        var SanctionLimitCurr = $('#SanctionLimitCurrID option:selected').val();
        var Utilization = $('#utilization option:selected').val();
        var Outstanding = $('#Outstanding option:selected').val();
        var AmountDisburse = $('#AmountDisburseID option:selected').val();
        var LoanID = $('#LoanID option:selected').val();
        var interestid = $('#Interest option:selected').val();
        var RepricingPlanID = $('#RepricingPlan option:selected').val();
        var PeggingFrequencyID = $('#PeggingFrequency option:selected').val();



        var cif = $('#CIF').val();
        var CustomerCategoryID = $('#CustomerCategoryID').val();
        if (CustomerCategoryID == "Affiliate") {
            CustomerCategoryID = '1';
        } else {
            CustomerCategoryID = '2';
        }
        // var CurrencyID = $('#Currency').val();
        var Bizsegment = $('#BizSegment').val();

        self.LoanID().LoanTypeID(LoanID);
        self.ProgramType().ProgramTypeID(ProgramType);
        self.FinacleScheme().FinacleSchemeCodeID(FinacleScheme);
        self.SanctionLimitCurrID().ID(SanctionLimitCurr);
        self.utilization().ID(Utilization);
        self.Outstanding().ID(Outstanding);
        self.AmountDisburseID().ID(AmountDisburse);
        self.Interest().InterestID(interestid);
        self.RepricingPlan().RepricingPlanID(RepricingPlanID);
        self.PeggingFrequency().PeggingFrequencyID(PeggingFrequencyID);

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.fundingmemotlDel + "/" + Funding.CSOFundingMemoID(),
                    data: ko.toJSON(Funding),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    self.OnChangeLoan = function () {
        var indexOption = $('#LoanID option:selected').index();
        $('#Loanhidden option').eq(indexOption).prop('selected', true);

    };
    self.OnChangeProgramType = function () {
        var indexOption = $('#ProgramType option:selected').index();
        $('#ProgramTypehidden option').eq(indexOption).prop('selected', true);

    };
    self.OnChangeFinacleScheme = function () {
        var indexOption = $('#FinacleScheme option:selected').index();
        $('#FinacleSchemeTypehidden option').eq(indexOption).prop('selected', true);

    };

    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);
        if (moment(localDate).isValid()) {
            if (date == '1970/01/01 00:00:00' || date == '1970-01-01T00:00:00' || date == null) {
                return "";
            }
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return ""
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };
    self.Ishidden = function () {
        if (viewModel.FinacleScheme().FinacleSchemeCodeID() == ConsLoanSchemeCode.LongLTLAA || viewModel.FinacleScheme().FinacleSchemeCodeID() == ConsLoanSchemeCode.LongSLLAA) {
            viewModel.ShortTerm(true);
            viewModel.RepricingPlan().RepricingPlanID('');
        }
        if (viewModel.FinacleScheme().FinacleSchemeCodeID() == ConsLoanSchemeCode.ShortSTLA || viewModel.FinacleScheme().FinacleSchemeCodeID() == ConsLoanSchemeCode.ShortSSLAA) {
            viewModel.ShortTerm(false);
            viewModel.RepricingPlan().RepricingPlanID(118);
            viewModel.PeggingFrequency().PeggingFrequencyID(135);
        }
        else {
            viewModel.RepricingPlan().RepricingPlanID('');
        }

    }
    // Auto populate data from select
    self.PopulateSelected = function (form, element) {
        switch (form) {
            case "FinacleSchemeCodeName":
                switch (element) {
                    case "SchemaCode":
                        var data = ko.utils.arrayFirst(self.ddlFinacleScheme(),
                            function (item) {
                                //return item.ID == self.Selected().FinacleSchemeCodeNames();
                                if (item.FinacleSchemeCodeID == 107 || item.FinacleSchemeCodeID == 108 || item.FinacleSchemeCodeID == 109)//longterm
                                {
                                    $('#ShortTerm').show();
                                } else if (item.FinacleSchemeCodeID == 106) {
                                    $('#ShortTerm').hide();
                                }
                            });

                        //if (data != null) {
                        //    self.TransactionMakerCBO().Transaction.ProductType = data;
                        //}
                        break;
                }
                break;
        }
    };

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        self.IsNewData(false);
        self.IsUpdateData(true);
        self.IsDelete(true);

        $("#CustomerCategoryID").val("");
        self.CustomerName('');
        self.CustomerCategoryID('');
        self.Customer('');
        self.CIF('');
        self.BizSegment('');
        //self.Currency(new CurrencyModel('', ''));
        self.SolID(''),
        self.ProgramType(new ProgramType('', ''));
        self.FinacleScheme(new FinacleScheme('', ''));
        self.IsAdhoc('');
        self.AmountDisburseID(new CurrencyModel('', ''));//add
        self.ShortTerm(true);
        self.AmountDisburse('');
        self.CreditingOperative('');
        self.DebitingOperative('')
        self.ValueDate('');
        self.MaturityDate('');
        self.Interest(new Interest('', ''));
        self.BaseRate('');
        self.AllInRate('');
        self.RepricingPlan(new RepricingPlan(''));
        self.AllInSpecialRate('');
        self.PeggingDate('31-Dec-2099');
        self.PeggingFrequency(new PeggingFrequency('', ''));
        self.Margin('');
        self.SpecialFTP('');
        self.ApprovedMarginAsPerCM('');
        self.AccountPreferencial('');
        self.NoOfInstallment('');
        self.SanctionLimitAmount('');
        self.OutstandingID(new OutstandingModel('', ''));
        self.InterestFrequency(new InterestFrequency('', ''));//use in Principal
        self.PrincipalFrequency(new PrincipalFrequency('', ''));
        self.PrincipalStartDate('');//add
        self.InterestStartDate('');//add
        self.LimitIDinFin10('');
        self.LimitExpiryDate('');
        self.SanctionLimitCurr(new SanctionLimitModel('', ''));
        self.OutstandingAmount('')
        self.utilization('');
        self.utilization(new utilizationModel('', ''));
        self.Outstanding(new OutstandingModel('', ''));
        self.utilizationID(new utilizationModel('', ''));
        self.CSOName('');
        self.LoanID(new ParameterSystemModel('', ''));
        self.ApprovalDoA('');
        self.utilizationAmount('');
        self.Remarks('');
        self.Documents([]);
        self.Employee('');
        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');

        //LineI
        if (data.FinacleScheme.FinacleSchemeCodeID == 107 || data.FinacleScheme.FinacleSchemeCodeID == 109) {
            viewModel.ShortTerm(true);
        }
        if (data.FinacleScheme.FinacleSchemeCodeID == 108 || data.FinacleScheme.FinacleSchemeCodeID == 106) {
            viewModel.ShortTerm(false);
        }
        self.CustomerName(data.Customer.Name);
        self.CSOFundingMemoID(data.CSOFundingMemoID);
        self.Customer(data.Customer.Name);
        self.CIF(data.Customer.CIF);
        self.BizSegment(data.Bizsegment);
        self.BizName(data.BizName);
        //self.Currency(new CurrencyModel(data.Currency.ID, data.Currency.Name));
        var CustomerCategoryData = data.CustomerCategoryID;
        if (CustomerCategoryData == 1) {
            $("#CustomerCategoryID").val("Affiliate");
        } else {
            $("#CustomerCategoryID").val("Non Affiliate");
        }

        self.SolID(data.SolID),
        self.ProgramType(new ProgramType(data.ProgramType.ProgramTypeID, data.ProgramType.ProgramTypeName));
        self.FinacleScheme(new FinacleScheme(data.FinacleScheme.FinacleSchemeCodeID, data.FinacleScheme.FinacleSchemeCodeName));
        var adhoc = data.IsAdhoc;
        if (adhoc == true) {
            document.getElementById('IsAdhoc').checked = true;
        }
        self.IsAdhoc(data.IsAdhoc);
        self.LoanID(new ParameterSystemModel(data.LoanID.LoanTypeID, data.LoanID.LoanTypeName));
        //lineII

        self.AmountDisburseID(new CurrencyModel(data.AmountDisburseID.ID, data.AmountDisburseID.CodeDescription));//add
        var amount = formatNumber(data.AmountDisburse);
        self.AmountDisburse(amount);
        self.CreditingOperative(data.CreditingOperative);
        self.DebitingOperative(data.DebitingOperative)
        self.ValueDate(self.LocalDate(data.ValueDate, true, false));
        self.MaturityDate(self.LocalDate(data.MaturityDate, true, false));
        //lineIII
        //self.Interest(new Interest(data.Interest.InterestCodeID, data.Interest.InterestCodeName));
        self.Interest(new Interest(data.Interest.InterestCodeName, data.Interest.InterestCodeID));
        self.BaseRate(data.BaseRate);
        self.AllInRate(data.AllInRate);
        self.RepricingPlan(new RepricingPlan(data.RepricingPlan.RepricingPlanID, data.RepricingPlan.RepricingPlanName));
        self.AllInSpecialRate(data.AllInSpecialRate);
        self.PeggingDate(self.LocalDate(data.PeggingDate, true, false));
        self.PeggingFrequency(new PeggingFrequency(data.PeggingFrequency.PeggingFrequencyID, data.PeggingFrequency.PeggingFrequencyName));
        self.Margin(data.Margin);
        self.SpecialFTP(data.SpecialFTP);
        self.ApprovedMarginAsPerCM(data.ApprovedMarginAsPerCM);
        self.AccountPreferencial(data.AccountPreferencial);
        //lineIV
        self.NoOfInstallment(data.NoOfInstallment);
        self.InterestFrequency(new InterestFrequency(data.InterestFrequency.InterestFrequencyID, data.InterestFrequency.InterestFrequencyName));//use in Principal
        self.PrincipalFrequency(new PrincipalFrequency(data.PrincipalFrequency.PrincipalFrequencyID, data.PrincipalFrequency.PrincipalFrequencyName));
        self.PrincipalStartDate(self.LocalDate(data.PrincipalStartDate, true, false));//add
        self.InterestStartDate(self.LocalDate(data.InterestStartDate, true, false));//add
        //LineV
        self.LimitIDinFin10(data.LimitIDinFin10);
        self.LimitExpiryDate(self.LocalDate(data.LimitExpiryDate, true, false));
        self.SanctionLimitCurrID(new SanctionLimitModel(data.SanctionLimitCurr.ID, data.SanctionLimitCurr.CodeDescription));
        self.SanctionLimitCurr(new SanctionLimitModel(data.SanctionLimitCurr.ID, data.SanctionLimitCurr.CodeDescription));
        self.utilization(new utilizationModel(data.utilizationID.ID, data.utilizationID.CodeDescription));
        self.utilizationID(new utilizationModel(data.utilizationID.ID, data.utilizationID.CodeDescription));
        self.Outstanding(new OutstandingModel(data.OutstandingID.ID, data.OutstandingID.CodeDescription));
        self.OutstandingID(new OutstandingModel(data.OutstandingID.ID, data.OutstandingID.CodeDescription));

        var SanctionLimitAmount = formatNumber(data.SanctionLimitAmount);
        self.SanctionLimitAmount(SanctionLimitAmount);

        var utilizationAmount = formatNumber(data.utilizationAmount);
        self.utilizationAmount(utilizationAmount);
        var OutstandingAmount = formatNumber(data.utilizationAmount);
        self.OutstandingAmount(OutstandingAmount);

        self.Employee(data.EmployeeFunding);
        //LineVI
        self.CSOName(data.CSOName);
        self.ApprovalDoA(data.ApprovalDOA);
        //LineVII
        //self.Remarks(data.Remarks);
        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(self.LocalDate(data.LastModifiedDate, true, false));
        //self.Documents(data.DocumentsFunding);
        for (i = 0; i < data.DocumentsFunding.length; i++) {
            self.Documents.push(data.DocumentsFunding[i])
            self.fundingmemodocument.push(data.DocumentsFunding[i])
        }

        //self.SetSOLIDAutoComplete();
        StartSanctionLimit();
        startCalculateLoan();
        StartutilizationAmount();
        StartOutStanding();
    };

    //insert new
    self.NewData = function () {
        // flag as new Data
        StartOutStanding()
        startCalculateLoan();
        StartSanctionLimit()
        StartutilizationAmount();
        self.IsNewData(true);
        self.IsUpdateData(false);
        self.IsDelete(false);
        self.Readonly(false);
        // bind empty data
        $("#DOAName").val("");
        $("#Customer").val("");
        $("#CustomerCategoryID").val("");
        self.CustomerCategoryID('');
        self.CustomerName('');
        self.BizName('');
        self.CIF('');
        self.BizSegment('');
        //self.Currency(new CurrencyModel('', ''));
        self.SolID(''),
        self.ProgramType(new ProgramType('', ''));
        self.FinacleScheme(new FinacleScheme('', ''));
        self.IsAdhoc('');
        self.AmountDisburseID(new CurrencyModel('', ''));//add
        self.ShortTerm(true);
        self.AmountDisburse('');
        self.CreditingOperative('');
        self.DebitingOperative('')
        self.ValueDate('');
        self.MaturityDate('');
        self.Interest(new Interest('', ''));
        self.BaseRate('');
        self.AllInRate('');
        self.RepricingPlan(new RepricingPlan(''));
        self.AllInSpecialRate('');
        self.PeggingDate('31-Dec-2099');
        self.PeggingFrequency(new PeggingFrequency('', ''));
        self.PrincipalFrequency(new PrincipalFrequency('', ''));
        self.Margin('');
        self.SpecialFTP('');
        self.ApprovedMarginAsPerCM('');
        self.AccountPreferencial('');
        self.NoOfInstallment('');
        self.SanctionLimitAmount('');
        self.OutstandingID(new OutstandingModel('', ''));
        self.InterestFrequency(new InterestFrequency('', ''));//use in Principal
        self.PrincipalStartDate('');//add
        self.InterestStartDate('');//add
        self.LimitIDinFin10('');
        self.LimitExpiryDate('');
        self.SanctionLimitCurr(new SanctionLimitModel('', ''));
        self.OutstandingAmount('')
        self.utilization('');
        self.utilization(new utilizationModel('', ''));
        self.Outstanding(new OutstandingModel('', ''));
        self.utilizationID(new utilizationModel('', ''));
        self.CSOName('');
        self.LoanID(new ParameterSystemModel('', ''));
        self.utilizationAmount('');
        self.Remarks('');
        self.Documents([]);
        self.Employee('');
        self.ApprovalDoA('');
        self.SetSOLIDAutoComplete();
    };

    //Function to Read All Customers
    function GetData() {
        var options = {
            url: api.server + api.url.fundingmemotl,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }

    //Function to validation dynamic field

    function IsvalidField() { /*
     $("[name^=days]").each(function () {
     $(this).rules('add', {
     required: true,
     maxlength: 2,
     number: true,
     messages: {
     required: "Please provide a valid Day.",
     maxlength: "Please provide maximum 2 number valid day value.",
     number: "Please provide number"
     }
     });
     }); */
        return true;
    }

    function GetDropdown() {
        $.ajax({
            async: false,
            type: "GET",
            //url: api.server + api.url.parameter + '?select=BizSegment,SchemeCode,LoanType,ProgramType,Currency,Currency,Currency,Currency,Interest,RepricingPlan',
            url: api.server + api.url.parameter + '?select=LoanType,DocType,PurposeDoc,ProgramType,SchemeCode,AmountDisburse,Interest,RepricingPlan,Frequency,SanctionLimitCurrID,utilization,Outstanding',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    //self.ddlBizSegmentDesc(data['BizSegmentDesc']);
                    self.ddlFinacleScheme(data['FinacleScheme']);
                    self.ddlLoanID(data['LoanID']);
                    self.ddlProgramType(data['ProgramType']);
                    self.ddlDocumentPurpose_a(data['PurposeDoc'])
                    self.ddlSanctionLimitCurrID(data['SanctionLimitCurrID']);
                    self.ddlUtilizationCurrID(data['utilization']);
                    self.ddlOutstandingCurrID(data['Outstanding']);
                    self.ddlDocumentType_a(data['DocType']);
                    self.ddlAmountDisburseID(data['AmountDisburseID']);
                    self.ddlInterest(data['Interest']);
                    self.ddlRepricing(data['RepricingPlan']);
                    self.ddlFrequency(data['Frequency']);
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }

        });

    }
    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterCIF() != "") filters.push({ Field: 'CIF', Value: self.FilterCIF() });
        if (self.FilterCustomer() != "") filters.push({ Field: 'Name', Value: self.FilterCustomer() });
        //if (self.FilterBizSegment() != "") filters.push({ Field: 'Name', Value: self.FilterBizSegment() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

        return filters;
    };
    function GetDOA(accessToken) {
        var cif = $('#CIF').val();

        var CustomerCategoryID = $('#CustomerCategoryID').val();
        if (CustomerCategoryID == "Affiliate") {
            CustomerCategoryID = '1';
        } else {
            CustomerCategoryID = '2';
        }
        //var CurrencyID = $('#Currency').val();
        var BizSegment = $('#BizSegmentDesc').val();

        var customername = $('#Customer').val();
        var ProgramType = $('#ProgramType option:selected').val();
        var FinacleScheme = $('#FinacleScheme option:selected').val();
        var SanctionLimitCurr = $('#SanctionLimitCurrID option:selected').val();
        var Utilization = $('#utilization option:selected').val();
        var ValueDate = $('#ValueDate').val();
        var MaturityDate = $('#MaturityDate').val();
        var PeggingDate = $('#PeggingDate').val();
        var InterestRestCode = $('#Interest').val();
        var PrincipalStartDate = $('#PrincipalStartDate').val();
        var InterestStartDate = $('#InterestStartDate').val();
        var Outstanding = $('#Outstanding').val();
        var BizName = $('#BizSegmentDesc').val();
        var AmountDisburse = $('#AmountDisburseID option:selected').val();
        var LoanID = $('#LoanID option:selected').val();
        var LimitExpiryDate = $('#LimitExpiryDate').val();
        var interestid = $('#Interest').val();
        var RepricingPlanID = $('#RepricingPlan').val();
        var PeggingFrequencyID = $('#PeggingFrequency').val();
        var BizID = $('#BizSegmentID').val();
        if ($('#IsAdhoc').is(":checked")) {
            Funding.IsAdhoc(true);
        } else {
            Funding.IsAdhoc(false);
        }

        self.LoanID().LoanTypeID(LoanID);
        self.ProgramType().ProgramTypeID(ProgramType);
        self.ProgramTypeID(ProgramType);
        self.FinacleScheme().FinacleSchemeCodeID(FinacleScheme);
        self.SanctionLimitCurrID().ID(SanctionLimitCurr);
        self.utilization().ID(Utilization);
        self.Outstanding().ID(Outstanding);
        self.AmountDisburseID().ID(AmountDisburse);
        self.Interest().InterestID(interestid);
        self.RepricingPlan().RepricingPlanID(RepricingPlanID);
        self.PeggingFrequency().PeggingFrequencyID(PeggingFrequencyID);
        CustomerModel.Name(customername);
        CustomerModel.CIF(cif);
        CustomerModel.CustomerCategoryID(CustomerCategoryID);

        BizSegmentModel.Name(BizName);
        BizSegmentModel.ID(BizID);
        Funding.BizName(BizName);
        Funding.CIF(cif);
        Funding.Customer(CustomerModel);
        Funding.BizSegmentID(BizID);
        var endPoint = api.server + api.url.fundingmemotl + "/GetDOA";

        var options = {
            url: endPoint,
            token: accessToken,
            data: ko.toJSON(Funding)
        };

        Helper.Ajax.Post(options, function (result, textStatus, jqXHR) {
            var doa = result.Funding.ApprovalDOA;
            $('#DOAName').val(doa);
        }, OnError);
    }
    function OnSuccessGetCSOName(data, textStatus, jqXHR) {

        if (jqXHR.status = 200) {
            var GetCSOName = '';
            if (data != null || data != '') {
                GetCSOName = data[0].nameEmp;
            }
            self.Employee(GetCSOName);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }

    }
    function OnSuccessSolIDAutoComplete(response, data, textStatus, jqXHR) {
        response($.map(data, function (item) {
            return {
                label: item.SolID + " - " + item.Name,
                value: item.SolID,
                data: {
                    ID: item.ID,
                    Code: item.Code,
                    Name: item.Name,
                    SolID: item.SolID
                }

            }
            //if (data == []) {
            //    ShowNotification("Form Validation Warning", "Data Empty", 'gritter-warning', false);
            //    return;
            //}
        })
        );
    }
    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.Fundings(data.Rows);
            self.Employee(data.Employee);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        //$box.trigger('reloaded.ace.widget');
    }
    //$('#aspnetForm').validate({
    //    errorElement: 'div',
    //    errorClass: 'help-block',
    //    focusInvalid: true,
    //    highlight: function (e) {
    //        $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
    //    },

    //    success: function (e) {
    //        $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
    //        $(e).remove();
    //    },

    //    errorPlacement: function (error, element) {
    //        if (element.is(':checkbox') || element.is(':radio')) {
    //            var controls = element.closest('div[class*="col-"]');
    //            if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
    //            else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
    //        }
    //        else if (element.is('.select2')) {
    //            error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
    //        }
    //        else if (element.is('.chosen-select')) {
    //            error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
    //        }
    //        else error.insertAfter(element.parent());
    //    }

    //});
    self.SetSOLIDAutoComplete = function () {
        $("#solidut").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.branch + "/SOLID",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessSolIDAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set customer data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.SolIDList(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.SolID(ui.item.data.SolID);
                }
                else {
                    viewModel.SolIDList(null);
                    viewModel.SolID(null);
                }
            }
        });
    }

    $("#Customer").autocomplete({

        source: function (request, response) {
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.GetCustomerFunding,
                data: {
                    query: request.term,
                    limit: 20
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
        },
        minLength: 2,
        select: function (event, ui) {
            // set customer data model
            if (ui.item.data != undefined || ui.item.data != null) {
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                $("#IsAdhoc").val(ui.item.data.IsAdhoc);
                $("#CIF").val(ui.item.data.CIF);
                Customertype = ui.item.data.CustomerTypeID;
                isAdhoc = (ui.item.data.IsAdhoc);
                if (ui.item.data.isAdhoc == true) {
                    document.getElementById("IsAdhoc").checked = true;
                }
                if (ui.item.data.isAdhoc == false || ui.item.data.isAdhoc == null) {
                    document.getElementById("IsAdhoc").checked = false;
                }

                if (Customertype == 1) {
                    $("#CustomerCategoryID").val("Affiliate");
                } else {
                    $("#CustomerCategoryID").val("Non Affiliate");
                }
                $("#SolID").val(ui.item.data.Branch.SolID);
                $("#BizSegmentDesc").val(ui.item.data.BizSegment.Description);
                $("#BizSegmentID").val(ui.item.data.BizSegment.ID);
                BizName = ui.item.data.BizSegment.Description;//get value BizsegmentID

            }
        }
    });

    // Autocomplete
    function OnSuccessAutoComplete(response, data, textStatus, jqXHR) {
        response($.map(data, function (item) {
            return {
                // default autocomplete object
                label: item.CIF + " - " + item.Name + " - " + item.Branch.Name,
                value: item.Name,

                // custom object binding
                data: {
                    isAdhoc: item.IsAdhoc,
                    CIF: item.CIF,
                    Customer: item.Name,
                    CustomerTypeID: item.CustomerTypeID,
                    Branch: item.Branch,
                    BizSegment: item.BizSegment
                }
            }
        })
      );
    }

    var CustomerResult = {
        CIF: ko.observable(),
        Name: ko.observable()
    };



    function UploadFile(context) {
        var isValid = true;
        var serverRelativeUrlToFolder = '/Instruction Documents';
        var parts = context.FileName.split('.');
        var fileExtension = parts[parts.length - 1];
        var timeStamp = new Date().getTime();
        var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

        var serverUrl = _spPageContextInfo.webAbsoluteUrl;

        var output;
        var getFile = getFileBuffer();
        getFile.done(function (arrayBuffer) {

            // Add the file to the SharePoint folder.
            var addFile = addFileToFolder(arrayBuffer);
            addFile.done(function (file, status, xhr) {
                output = file.d;

                // Get the list item that corresponds to the uploaded file.
                var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
                getItem.done(function (listItem, status, xhr) {

                    // Change the display name and title of the list item.
                    var changeItem = updateListItem(listItem.d.__metadata);
                    changeItem.done(function (data, status, xhr) {
                        var kodok = {
                            ID: 0,
                            Type: context.Type,
                            Purpose: context.Purpose,
                            LastModifiedDate: null,
                            LastModifiedBy: null,
                            DocumentPath: output.ServerRelativeUrl,
                            FileName: context.FileName
                        };
                        viewModel.Documents.push(kodok);
                        return isValid;
                        //callBack();
                    });
                    changeItem.fail(OnError);
                });
                getItem.fail(OnError);
            });
            addFile.fail(OnError);
        });
        getFile.fail(OnError);

        // Get the local file as an array buffer.
        function getFileBuffer() {
            var deferred = $.Deferred();
            var reader = new FileReader();
            reader.onloadend = function (e) {
                deferred.resolve(e.target.result);
            }
            reader.onerror = function (e) {
                deferred.reject(e.target.error);
            }
            //bangkit
            var f = new File([""], context.DocumentPath.name)
            reader.readAsArrayBuffer(f);
            //reader.readAsDataURL(document.DocumentPath);
            return deferred.promise();
        }

        // Add the file to the file collection in the Shared Documents folder.
        function addFileToFolder(arrayBuffer) {

            // Construct the endpoint.
            var fileCollectionEndpoint = String.format(
                    "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                    "/add(overwrite=false, url='{2}')",
                serverUrl, serverRelativeUrlToFolder, fileName);

            // Send the request and return the response.
            // This call returns the SharePoint file.
            return $.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val()
                    //"content-length": arrayBuffer.byteLength
                }
            });
        }

        // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
        function getListItem(fileListItemUri) {

            // Send the request and return the response.
            return $.ajax({
                url: fileListItemUri,
                type: "GET",
                headers: { "accept": "application/json;odata=verbose" }
            });
        }

        // Change the display name and title of the list item.
        function updateListItem(itemMetadata) {
            var body = {
                Title: document.DocumentPath.name,
                CIF: context.CIF,
                Customer_x0020_Name: context.Name,
                __metadata: {
                    type: itemMetadata.type,
                    FileLeafRef: fileName,
                    Title: fileName
                }
            };

            // Send the request and return the promise.
            return $.ajax({
                url: itemMetadata.uri,
                type: "POST",
                data: ko.toJSON(body),
                headers: {
                    "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                    "content-type": "application/json;odata=verbose",
                    "IF-MATCH": itemMetadata.etag,
                    "X-HTTP-Method": "MERGE"
                }
            });
        }
    }
};

var viewModel = new ViewModel();

$.holdReady(true);
var intervalRoleName = setInterval(function () {
    if (Object.keys(Const_RoleName).length != 0) {
        $.holdReady(false);
        clearInterval(intervalRoleName);
    }
}, 100);

//$(document).on("RoleNameReady", function() {
$(document).ready(function () {
    // Knockout custom file handler
    ko.bindingHandlers.file = {
        init: function (element, valueAccessor) {
            $(element).change(function () {
                var file = this.files[0];

                if (ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };

    //$('#aspnetForm').validate({
    //    errorElement: 'div',
    //    errorClass: 'help-block',
    //    focusInvalid: true,
    //    focusCleanup: true,
    //    highlight: function (e) {
    //        $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
    //    },

    //    success: function (e) {
    //        $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
    //        $(e).remove();
    //    },

    //    errorPlacement: function (error, element) {
    //        if (element.is(':checkbox') || element.is(':radio')) {
    //            var controls = element.closest('div[class*="col-"]');
    //            if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
    //            else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
    //        }
    //        else if (element.is('.select2')) {
    //            error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
    //        }
    //        else if (element.is('.chosen-select')) {
    //            error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
    //        }
    //        else error.insertAfter(element.parent());
    //    }
    //});

    // Knockout Bindings
    ko.applyBindings(viewModel);

    //get access token
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(OnSuccessToken, OnError);
    } else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);
            //viewModel.SPUser(ko.mapping.toJS(spUser));
        }
    }
    // Token validation On Success function
    function OnSuccessToken(data, textStatus, jqXHR) {
        // store token on browser cookies
        $.cookie(api.cookie.name, data.AccessToken);
        accessToken = $.cookie(api.cookie.name);

        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);
            //viewModel.SPUser(ko.mapping.toJS(spUser));
        }

    }

    // AJAX On Error Callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    viewModel.GetData();
    viewModel.GetDropdown();

    $('#document-path').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        thumbnail: false,
        blacklist: 'exe|dll'
    });
});