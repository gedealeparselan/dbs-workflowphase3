﻿var accessToken;
var $box;
var $remove = false;
var DocumentArray = [];
var DocumentTBO = [];
var datatboaftersubmit = [];
var dataTemp = [];
var dataTempTBOwf = [];

var formatNumber = function (num) {
    var n = num.toString(), p = n.indexOf('.');
    return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
        return p < 0 || i < p ? ($0 + ',') : $0;
    });
}

var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};

var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};

var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};

var DocumentTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable(),
    Days: ko.observable()
};

var DocumentPurposeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var DocumentModel = {
    ID: ko.observable(),
    Type: ko.observable(DocumentTypeModel),
    Purpose: ko.observable(DocumentPurposeModel),
    DocumentPath: ko.observable(),
    LastModifiedDate: ko.observable(new Date),
    LastModifiedBy: ko.observable()
};

var ParameterTBO = {
    DocumentTypes: ko.observableArray([DocumentTypeModel]),
    DocumentPurposes: ko.observableArray([DocumentPurposeModel]),
}

var SelectedModelTBO = {
    DocumentType: ko.observable(),
    DocumentPurpose: ko.observable(),
}

var DocumentModels = ko.observable({ FileName: '', DocumentPath: '' });

var TBOData = {
    TransactionID: ko.observable(),
    TBOTransactionDraftID: ko.observable(),
    ID: ko.observable(),
    TransactionID: ko.observable(),
    Cif: ko.observable(),
    ApplicationID: ko.observable(),
    TBOApplicationID: ko.observable(),
    TBOSLAID: ko.observable(),
    Name: ko.observable(),
    ExptDate: ko.observable(),
    ActDate: ko.observable(),
    FileName: ko.observable(),
    PurposeofDoc: ko.observable(),
    TypeofDoc: ko.observable(),
    Modified: ko.observable(),
    Status: ko.observable(),
    CreatedBy: ko.observable(),
    CreatedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    LastModifiedDate: ko.observable(),
    IsApprove: ko.observable(),
    CustomerName: ko.observable(),
    IsDisableTBO: ko.observable(),
    IsChecklistSubmit: ko.observable(),
    SPTaskListID: ko.observable(),
    SPTaskListItemID: ko.observable(),
    WorkflowInstanceID: ko.observable()
}

var TimelineModel = {
    ApproverID: ko.observable(),
    Activity: ko.observable(),
    Time: ko.observable(),
    Outcome: ko.observable(),
    UserOrGroup: ko.observable(),
    IsGroup: ko.observable(),
    DisplayName: ko.observable(),
    Comment: ko.observable()
};


var TransactionTBOTool = {
    Documents: ko.observableArray([])
}

var TransactionTBOToolWF = {
    Documents: ko.observableArray([])
}

var TransactionTBOToolWFAfterSubmit = {
    Documents: ko.observableArray([])
}

var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;

    // Sharepoint User
    self.SPUser = ko.observable();

    // Workflow task config
    self.WorkflowConfig = ko.observable();

    // parameters
    self.TBOTools = ko.observableArray([]);
    self.TBOToolsWF = ko.observableArray([]);
    self.DocumentTBO = ko.observableArray([TBOData]);
    self.DocumentTBOWF = ko.observableArray([TBOData]);
    self.DocumentTBOWFAfterSubmit = ko.observableArray([TBOData]);
    self.DocumentTypes = ko.observableArray([DocumentTypeModel]);
    self.DocumentPurposes = ko.observableArray([DocumentPurposeModel]);
    self.ParameterTBO = ko.observable(ParameterTBO);
    self.SelectedTBO = ko.observable(SelectedModelTBO);
    self.TransactionTBOModel = ko.observable(TransactionTBOTool);
    self.TransactionTBOModelWF = ko.observable(TransactionTBOToolWF);
    self.TransactionTBOModelWFAfterSubmit = ko.observable(TransactionTBOToolWFAfterSubmit);
    self.TransactionTBOModelTimeLine = ko.observable([TimelineModel]);
    self.DocumentTypeTBO = ko.observableArray([]);
    self.IsFlagTBOWF = ko.observable(false);
    self.IsSubmitTBO = ko.observable(false);
    self.IsGetSelectedRow = ko.observable(false);
    self.IsGetFilter = ko.observable(false);
    self.Message = ko.observable();
    self.IsMessage = ko.observable(false);
    self.CheckAll = ko.observable(false);

    self.ApplicationID = ko.observable();
    self.TBOTransactionDraftID = ko.observable();
    self.ID = ko.observable();
    self.Cif = ko.observable();
    self.Name = ko.observable();
    self.IsFlag = ko.observable(0);
    self.CreateBy = ko.observable();
    self.CreateDate = ko.observable();
    self.LastModifiedBy = ko.observable();
    self.LastModifiedDate = ko.observable();
    self.IsNewTBO = ko.observable();
    self.NameDocTBO = ko.observable();
    self.IsDisableTBO = ko.observable();
    self.NameWF = ko.observable();
    self.CifWF = ko.observable();

    // Temporary documents
    self.Documents = ko.observableArray([]);
    self.DocumentsWF = ko.observableArray([]);
    self.TBOToolsByCIFWF = ko.observableArray([]);
    self.DocumentPath = ko.observable();
    self.DocumentType = ko.observable();
    self.DocumentPurpose = ko.observable();
    self.DocumentFileName = ko.observable();
    self.IsNewDocument = ko.observable();

    self.TransactionID = ko.observable();
    self.IDRowSelected = ko.observable();
    self.IDSelected = ko.observable();
    self.CIFSelected = ko.observable();
    self.NameSelected = ko.observable();
    self.IsFlagSelected = ko.observable();
    self.ExpectedSUbDate = ko.observable();
    self.ActSubmissionDate = ko.observable();
    self.CreateBySelected = ko.observable();
    self.CreateDateSelected = ko.observable();
    self.LastModifiedBySelected = ko.observable();
    self.LastModifiedDateSelected = ko.observable();
    self.DateNow = ko.observable();
    self.LoginName = ko.observable();

    // task properties
    self.ApproverId = ko.observable();
    self.SPWebID = ko.observable();
    self.SPSiteID = ko.observable();
    self.SPListID = ko.observable();
    self.SPListItemID = ko.observable();
    self.SPTaskListID = ko.observable();
    self.SPTaskListItemID = ko.observable();
    self.Initiator = ko.observable();
    self.WorkflowInstanceID = ko.observable();
    self.WorkflowID = ko.observable();
    self.WorkflowName = ko.observable();
    self.StartTime = ko.observable();
    self.StateID = ko.observable();
    self.StateDescription = ko.observable();
    self.IsAuthorized = ko.observable();

    self.TaskTypeID = ko.observable();
    self.TaskTypeDescription = ko.observable();
    self.Title = ko.observable();
    self.ActivityTitle = ko.observable();
    self.User = ko.observable();
    self.IsSPGroup = ko.observable();
    self.EntryTime = ko.observable();
    self.ExitTime = ko.observable();
    self.OutcomeID = ko.observable();
    self.OutcomeDescription = ko.observable();
    self.CustomOutcomeID = ko.observable();
    self.CustomOutcomeDescription = ko.observable();
    self.Comments = ko.observable();

    // Task status from Nintex custom REST API
    self.IsPendingNintex = ko.observable();
    self.IsAuthorizedNintex = ko.observable();
    self.MessageNintex = ko.observable();

    // Task activity properties
    self.PPUChecker = ko.observable();
    //self.PPUChecker(new PPUCheckerModel());
    //alert(self.PPUChecker().data());

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    self.GridProperties().SortColumn("CreatedDate");
    self.GridProperties().SortOrder("DESC");

    //grid properties wf
    self.GridPropertiesWF = ko.observable();
    self.GridPropertiesWF(new GridPropertiesModel(GetDataWF));

    self.GridPropertiesWF().SortColumn("CreatedDate");
    self.GridPropertiesWF().SortOrder("DESC");

    // filters
    self.FilterCustomerName = ko.observable("");
    self.FilterCif = ko.observable("");
    self.FilterTBODoc = ko.observable("");
    self.FilterLastModifiedBy = ko.observable("");
    self.FilterLastModifiedDate = ko.observable("");

    self.FilterApplicationID = ko.observable("");
    self.FilterStatus = ko.observable("");


    // bind get data function to view
    self.GetData = function () { GetData(); };
    self.GetParameters = function () { GetParameters(); };
    self.GetDataWF = function () { GetDataWF(); };
    self.GetDataWFByCIF = function () { self.GetSelectedRowWF(data) };
    self.OnChangeDocumentType = function () {
        OnChangeDocumentType();
    }

    // bind clear filters
    self.ClearFilters = function () {

        self.FilterCustomerName("");
        self.FilterCif("");
        self.FilterLastModifiedBy("");
        self.FilterLastModifiedDate("");

        GetData();
    };

    self.ClearFiltersWF = function () {

        self.FilterApplicationID("");
        self.FilterCustomerName("");
        self.FilterStatus("");
        self.FilterLastModifiedBy("");
        self.FilterLastModifiedDate("");

        GetDataWF();
    };


    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-18
    };

    function PushCompletingDocument(x) {
        if (self.IDRowSelected() != undefined) {
            var mapping = {
                //'ignore': ["LastModifiedDate", "LastModifiedBy"]
            };
            self.DocumentTBO()[self.IDRowSelected()].Status = '';
            self.DocumentTBO()[self.IDRowSelected()].TBOTransactionDraftID = self.TBOTransactionDraftID();
            self.DocumentTBO()[self.IDRowSelected()].ApplicationID = self.ApplicationID();
            self.DocumentTBO()[self.IDRowSelected()].Cif = self.Cif();
            self.DocumentTBO()[self.IDRowSelected()].ActDate = $('#application-date-actsubmissiondate').val();
            self.DocumentTBO()[self.IDRowSelected()].PurposeofDoc = x.Purpose;
            self.DocumentTBO()[self.IDRowSelected()].TypeofDoc = x.Type;
            self.DocumentTBO()[self.IDRowSelected()].FileName = x.DocumentPath;
            self.DocumentTBO()[self.IDRowSelected()].Modified = self.LocalDate(x.LastModifiedDate, true);
            self.Documents.push(self.DocumentTBO()[self.IDRowSelected()]);
            self.DocumentsWF.push(self.DocumentTBO()[self.IDRowSelected()]);
            self.DocumentTBO(ko.mapping.toJS(self.DocumentTBO(), mapping));
        }
    }

    self.GetSelectedRowTBOData = function (index, data) {
        var form = $('#aspnetForm').validate();
        form.resetForm();
        $('.form-group').removeClass('has-error has-feedback');
        $('.form-group').find('small.help-block').hide();
        $('.form-group').find('i.form-control-feedback').hide();
        if (data.Status == "Revise") {
            self.IsNewTBO(false);
            self.TransactionID(data.TransactionID);
            self.TBOTransactionDraftID(data.TBOTransactionDraftID);
            self.ApplicationID(data.ApplicationID);
            self.IDRowSelected(index);
            self.IDSelected(data.ID);
            self.CIFSelected(self.Cif());
            self.NameSelected(data.Name);
            self.IsFlagSelected(true);
            self.ExpectedSUbDate(self.LocalDate(data.ExptDate, true));
            self.ActSubmissionDate(self.LocalDate(data.ActDate, true));
            self.CreateBySelected(data.LastModifiedBy);
            self.CreateDateSelected(data.LastModifiedDate);
            self.LastModifiedBySelected(data.LastModifiedBy);
            self.LastModifiedDateSelected(data.LastModifiedDate);
            if (data.PurposeofDoc.ID != 0) {
                self.SelectedTBO().DocumentPurpose(data.PurposeofDoc.ID);
            } else {
                self.SelectedTBO().DocumentPurpose(null);
            }
            if (data.TypeofDoc.ID != 0) {
                self.SelectedTBO().DocumentType(data.TypeofDoc.ID);
            } else {
                self.SelectedTBO().DocumentType(null);
            }
            $('#application-date-actsubmissiondate').val(self.ActSubmissionDate());
        } else {
            self.IsNewTBO(true);
            self.TransactionID(data.TransactionID);
            self.TBOTransactionDraftID(data.TBOTransactionDraftID);
            self.ApplicationID(data.ApplicationID);
            self.IDRowSelected(index);
            self.IDSelected(data.ID);
            self.CIFSelected(self.Cif());
            self.NameSelected(data.Name);
            self.IsFlagSelected(true);
            self.ExpectedSUbDate(self.LocalDate(data.ExptDate, true));
            self.ActSubmissionDate(self.LocalDate(data.ActDate, true));
            self.CreateBySelected(data.LastModifiedBy);
            self.CreateDateSelected(data.LastModifiedDate);
            self.LastModifiedBySelected(data.LastModifiedBy);
            self.LastModifiedDateSelected(data.LastModifiedDate);
            if (data.PurposeofDoc.ID != 0) {
                self.SelectedTBO().DocumentPurpose(data.PurposeofDoc.ID);
            } else {
                self.SelectedTBO().DocumentPurpose(null);
            }

            if (data.TypeofDoc.ID != 0) {
                self.SelectedTBO().DocumentType(data.TypeofDoc.ID);
            } else {
                self.SelectedTBO().DocumentType(null);
            }
            $('#application-date-actsubmissiondate').val(null);
            self.DocumentPath(null);
        }

        $("#tbotmo-doc-detail").modal('show');
        $('.remove').click();
        console.log(data);
    };

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        self.ApplicationID(data.ApplicationID);
        self.TBOTransactionDraftID(data.TBOTMOTransactionID);
        self.ID(data.ID);
        self.Cif(data.CIF);
        self.Name(data.Name);
        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);
        self.CreateBy(data.CreatedBy);
        self.CreateDate(data.CreatedDate);
        self.LoginName(spUser.DisplayName);
        self.LocalDate(self.DateNow(Date.now()), true);
        console.log(data);
        self.IsFlagTBOWF(false);
        self.IsGetSelectedRow(false);
        GetTransactionDataTBO(data.CIF);
        GetDataWF();
        self.GetDataHomeTMOWF();
        self.IsGetFilter(false);
        if (spUser.LoginName != null || spUser.LoginName != undefined) {
            if (data.LastModifiedBy.toLowerCase().replace(' ', '').replace(' ', '') == spUser.LoginName.split('|')[2]) {
                self.IsMessage(true);
            } else {
                self.IsMessage(false);

            }
        }
    };


    function GetTimeLine(data) {
        var endPointURL;
        endPointURL = api.server + api.url.tbodatawfwfid(data.WorkflowInstanceID);

        var options = {
            url: endPointURL,
            token: accessToken
        };
        Helper.Ajax.Get(options, OnSuccessGetTimeLine, OnError, OnAlways);
    }

    function OnSuccessGetTimeLine(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            var mapping = {
                //'ignore': ["LastModifiedDate", "LastModifiedBy"]
            };
            self.TransactionTBOModelTimeLine(ko.mapping.toJS(data, mapping));
            $('#modaltbotmo-form').modal('show');
            $('#tbotmo-form-wf').show();
            $("#transactiontmo-progress").hide();
        }
    }

    self.GetSelectedRowWF = function (data) {
        if (self.IsFlagTBOWF() == true) {
            self.NameWF(data.CustomerName);
            self.CifWF(data.CIF);
            dataTemp = [];
            if (dataTempTBOwf.length > 0) {
                for (i = 0; i <= dataTempTBOwf.length - 1; i++) {
                    if (dataTempTBOwf[i].TBOApplicationID != null) {
                        if (dataTempTBOwf[i].TBOApplicationID == data.TBOApplicationID) {
                            dataTemp.push(dataTempTBOwf[i]);
                        }
                    }
                }
            }

            $('#modaltbotmo-form').modal('show');
            $('#tbotmo-form-wf').hide();
            $("#transactiontmo-progress").show();

            OnSuccessGetTBOWFData(dataTemp, data);
            console.log(dataTemp);
        } else {
            if (self.IsGetSelectedRow() == true) {
                self.IsSubmitTBO(false);
                self.IsFlagTBOWF(true);
                self.GetSelectedRowWF(data);
            }
            if (self.IsSubmitTBO() == true) {
                self.IsGetSelectedRow(false);
                self.IsFlagTBOWF(true);
                self.GetSelectedRowWF(data);
            }
            if (self.IsGetFilter() == true) {
                self.IsGetSelectedRow(false);
                self.IsFlagTBOWF(true);
            }
        }
    }

    function OnSuccessGetTBOWFData(data, param) {
        if (data.length > 0) {
            var mapping = {
                //'ignore': ["LastModifiedDate", "LastModifiedBy"]
            };
            var DataTBOWF = [];
            var TBOToolsByCIFWF = [];
            if (data.length > 0) {
                for (var i = 0 ; i < data.length; i++) {
                    PushNamebyArray(data[i]);
                    DataTBOWF = {
                        TransactionID: data[i].TransactionID,
                        ID: data[i].ID,
                        CIF: data[i].CIF,
                        ApplicationID: data[i].ApplicationID,
                        TBOApplicationID: data[i].TBOApplicationID,
                        TBOSLAID: data[i].TBOSLAID,
                        Name: self.NameDocTBO(),
                        ExptDate: data[i].ExptDate,
                        ActDate: data[i].ActDate,
                        FileName: data[i].FileName,
                        PurposeofDoc: data[i].PurposeofDoc,
                        TypeofDoc: data[i].TypeofDoc,
                        Modified: data[i].LastModifiedDate,
                        Status: data[i].Status,
                        CreatedBy: data[i].CreatedBy,
                        CreatedDate: data[i].CreatedDate,
                        LastModifiedBy: data[i].LastModifiedBy,
                        LastModifiedDate: data[i].LastModifiedDate,
                        IsApprove: data[i].IsApprove,
                        CustomerName: data[i].CustomerName,
                        IsChecklistSubmit: false,
                        SPTaskListID: data[i].SPTaskListID,
                        SPTaskListItemID: data[i].SPTaskListItemID
                    };

                    TBOToolsByCIFWF.push(DataTBOWF);
                    self.TBOToolsByCIFWF.push(DataTBOWF);
                    viewModel.TransactionTBOModelWF().Documents().push(DataTBOWF);
                }
            }
            self.DocumentTBOWFAfterSubmit(ko.mapping.toJS(TBOToolsByCIFWF, mapping));
            datatboaftersubmit = [];
            datatboaftersubmit = self.DocumentTBOWFAfterSubmit();

            var text = "You can't approve this transaction because you are maker of this transaction";
            if (spUser.LoginName != null || spUser.LoginName != undefined) {
                if (param.LastModifiedBy.toLowerCase().replace(' ', '').replace(' ', '') == spUser.LoginName.split('|')[2]) {
                    self.Message(text);
                    self.IsMessage(true);
                } else {
                    self.Message(text);
                    self.IsMessage(false);
                }
            }
            GetTimeLine(param);
        } else {
            ShowNotification('Get Data Eror', 'eror', 'gritter-error', true);
        }
    }

    self.SetChecklistSubmit = function (index, data) {
        var mapping = {
            //'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };
        if (self.DocumentTBOWFAfterSubmit()[index].IsChecklistSubmit) {
            self.DocumentTBOWFAfterSubmit()[index].IsChecklistSubmit = true;
        } else {
            self.DocumentTBOWFAfterSubmit()[index].IsChecklistSubmit = false;
        }
        self.DocumentTBOWFAfterSubmit(ko.mapping.toJS(self.DocumentTBOWFAfterSubmit(), mapping));
        viewModel.TransactionTBOModelWFAfterSubmit().Documents(self.DocumentTBOWFAfterSubmit());
        console.log(self.DocumentTBOWFAfterSubmit());
    }

    self.UpdateTemplateUIBind = function (datatbo) {
        var mapping = {
            //'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };
        var data = datatbo;
        self.CheckAll(document.getElementById('check-all').checked);
        viewModel.CheckAll(document.getElementById('check-all').checked);
        viewModel.TransactionTBOModelWFAfterSubmit().Documents(data);
        self.DocumentTBOWFAfterSubmit(ko.mapping.toJS(data, mapping));
        //viewModel.DocumentTBOWFAfterSubmit(ko.mapping.toJS(data, mapping));
        $("#check-all").prop('checked', viewModel.CheckAll());
    }

    self.CheckAllData = function (tbo) {
        console.log(tbo);
        var datatbo = null;
        var isVerified = document.getElementById('check-all').checked;
        datatbo = datatboaftersubmit; //self.DocumentTBOWFAfterSubmit();
        if (datatbo.length != 0) {
            for (i = 0; i <= datatbo.length - 1; i++) {
                datatbo[i].IsChecklistSubmit = isVerified;
            }
        }
        self.UpdateTemplateUIBind(datatbo);
    }

    function UpdateTBO() {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modaltbotmo-form").modal('show');
                } else {
                    //Ajax call to insert 
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.tbotmotoolData,
                        data: ko.toJSON(viewModel.TransactionTBOModel()),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {

                                // refresh data
                                self.IsGetSelectedRow(false);
                                self.IsFlagTBOWF(false);
                                self.IsSubmitTBO(false);
                                GetTransactionDataTBO(self.Cif());
                                GetData();
                                GetDataWF();
                                self.GetDataHomeTMOWF();
                                self.IsGetFilter(false);
                                self.Documents([]);
                                viewModel.TransactionTBOModel().Documents([]);
                                ShowNotification("Submit Success", jqXHR.responseJSON.Message, 'gritter-success', false);
                                var param = {
                                    ApplicationID: self.ApplicationID(),
                                    CIF: self.Cif(),
                                    Name: self.Name(),
                                    Date: (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().replace("-", "").replace("-", "").replace(":", "").replace(":", "").replace(".", "").replace("T", "").replace("Z", "")
                                };
                                AddListItemTBOTransaction(param, self.DocumentsWF(), self.DocumentsWF().length);
                                self.DocumentsWF([]);
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
                        }
                    });
                }
            });
        }
    };

    function SubmitTBOWF() {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            bootbox.confirm("Submit TBO Transaction ?", function (result) {
                if (!result) {
                    $("#modaltbotmo-form").modal('show');
                } else {
                    //Ajax call to insert 
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.tbotmotoolData,
                        data: ko.toJSON(viewModel.TransactionTBOModelWFAfterSubmit()),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                self.IsGetSelectedRow(false);
                                self.IsFlagTBOWF(false);
                                self.IsSubmitTBO(false);
                                GetTransactionDataTBO(self.Cif());
                                GetData();
                                GetDataWF();
                                self.GetDataHomeTMOWF();
                                self.IsGetFilter(false);
                                self.Documents([]);
                                self.DocumentsWF([]);
                                viewModel.TransactionTBOModel().Documents([]);

                                self.TBOToolsByCIFWF([]);
                                self.CloseTBOWFDetail();
                                ShowNotification('Completing Task Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                                //self.close();
                            } else {
                                ShowNotification('Completing Task Failed', jqXHR.responseJSON.Message, 'gritter-error', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification('Completing Task Failed', jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    function GetUserRole(userData) {
        var sValue;
        if (userData != undefined && userData.length > 0) {
            $.each(userData, function (index, item) {
                if (Const_RoleName[item.ID].toLowerCase().startsWith("dbs tmo") && Const_RoleName[item.ID].toLowerCase().endsWith("checker")) {
                    return sValue;
                }
            });

            $.each(userData, function (index, item) {
                if (Const_RoleName[item.ID].toLowerCase().endsWith("checker")) {
                    sValue = item.ID;
                    return sValue;
                }
            });
        }
        if (sValue == null) {
            sValue = userData[0].ID;
        }
        return sValue;
    }

    function AddListItemTBOTransaction(context, document, numFile) {
        var indexDocument = document.length - numFile;
        var body;
        var urlListTransaction;
        var Listtitle;
        var ListInitGroup = GetUserRole(self.SPUser.Roles);
        var ListTransactionID;
        var ListTBOTransactionDraftID;
        var ListAppID;
        var ListTBO;

        urlListTransaction = config.sharepoint.listIdTBOTMOTransaction;
        Listtitle = document[indexDocument].ApplicationID + " - " + document[indexDocument].Cif;
        ListTransactionID = document[indexDocument].TransactionID;
        ListTBOTransactionDraftID = document[indexDocument].TBOTransactionDraftID;
        ListAppID = document[indexDocument].ApplicationID;
        ListTBO = config.sharepoint.metadata.listTBOTMOTransaction;

        body = {
            Title: document[indexDocument].ApplicationID + " - " + document[indexDocument].Cif,
            Application_x0020_ID: ListAppID,
            Initiator_x0020_GroupId: ListInitGroup,
            CIF: document[indexDocument].Cif,
            Customer_x0020_Name: self.Name(),
            TBOApplicationID: context.Date,
            TBOTransactionDraftID: "" + ListTBOTransactionDraftID,
            TransactionID: "" + self.TransactionID(),
            __metadata: {
                type: ListTBO
            }
        };

        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval);

        var options = {
            url: config.sharepoint.url.api + "/Lists(guid'" + urlListTransaction + "')/Items",
            data: JSON.stringify(body),
            digest: jQuery("#__REQUESTDIGEST").val()
        };
        Helper.Sharepoint.List.Add(options, OnSuccessAddListWorkflow(context, document, numFile), OnError, OnAlways);
    }

    function OnSuccessAddListWorkflow(context, document, numFile) {
        if (numFile > 1) {
            AddListItemTBOTransaction(context, document, numFile - 1);
        }
    }

    self.InitTBO = ko.observable();
    function GetInitiatorRole(username) {
        var options = {
            url: api.server + api.url.workflow.transactiontboinit(username),
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetTBOInit, OnError, OnAlways);
    }
    function OnSuccessGetTBOInit(data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            viewModel.InitTBO(data);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    function CompletingTask(context, numFile) {
        var indexDocument = context.length - numFile;
        var task = {
            TaskListID: context[indexDocument].SPTaskListID,
            TaskListItemID: context[indexDocument].SPTaskListItemID,
            OutcomeID: 2,
            Comment: ""
        };

        var endPointURL;
        endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondReviewTask";

        var options = {
            url: endPointURL,
            data: ko.toJSON(task)
        };

        Helper.Sharepoint.Nintex.Post(options, OnSuccessCompletingTask(context, numFile), OnError, OnAlways);
    }

    function OnSuccessCompletingTask(context, numFile) {
        if (numFile > 1) {
            CompletingTask(context, numFile - 1);
        } else {
            viewModel.TransactionTBOModelWFAfterSubmit().Documents(self.DocumentTBOWFAfterSubmit());
            SubmitTBOWF();
        }
    }

    self.save = function () {
        var data = {
            ApplicationID: self.ApplicationID(),
            CIF: self.Cif(),
            Name: self.Name(),
            Date: (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().replace("-", "").replace("-", "").replace(":", "").replace(":", "").replace(".", "").replace("T", "").replace("Z", "")
        };

        if (self.Documents().length > 0) {
            UploadFileRecuresive(data, self.Documents(), self.Documents().length);
        } else {
            ShowNotification("Warning validation", "Please Completed fill 1 or more document in attachment document", 'gritter-warning', false);
        }
    }

    self.savewf = function () {
        if (self.DocumentTBOWFAfterSubmit().length > 0) {
            CompletingTask(self.DocumentTBOWFAfterSubmit(), self.DocumentTBOWFAfterSubmit().length);
        } else {
            ShowNotification("Warning validation", "Please Thick toggle 1 or more document in attachment", 'gritter-warning', false);
        }
    }

    self.CloseTBODetail = function () {
        $("#tbotmo-doc-detail").modal('hide');
        $('#backDrop').hide();
    }

    self.CloseTBOWFDetail = function () {
        $("#modaltbotmo-form").modal('hide');
        $('#backDrop').hide();
        dataTemp = [];
    }

    self.OnCloseApproval = function () {
        $('#modaltbotmo-form').modal('hide');
        $('#backDrop').hide();
        dataTemp = [];

    }
    //dayatcoba
    self.CheckAllTBO = function () {
        var checkAll = document.getElementById("TBOCheckAll").checked;

        if (checkAll) {
            $("#TabelTBO input[type='checkbox']").prop('checked', true);
        } else {
            $("#TabelTBO input[type='checkbox']").prop('checked', false);
        }
    }
    //dayatcoba
    function UploadFileRecuresive(context, document, numFile) {
        var indexDocument = document.length - numFile;

        var serverRelativeUrlToFolder = '';
        serverRelativeUrlToFolder = '/TBOTool';

        var parts = document[indexDocument].FileName.name.split('.');
        var fileExtension = parts[parts.length - 1];
        var timeStamp = new Date().getTime();
        var fileName = timeStamp + "." + fileExtension; //document.FileName.name;

        // Get the server URL.
        var serverUrl = _spPageContextInfo.webAbsoluteUrl;

        // output variable
        var output;

        // Initiate method calls using jQuery promises.
        // Get the local file as an array buffer.
        var getFile = getFileBuffer();
        getFile.done(function (arrayBuffer) {
            // Add the file to the SharePoint folder.
            var addFile = addFileToFolder(arrayBuffer);
            addFile.done(function (file, status, xhr) {
                output = file.d;

                // Get the list item that corresponds to the uploaded file.
                var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
                getItem.done(function (listItem, status, xhr) {
                    // Change the display name and title of the list item.
                    var changeItem = updateListItem(listItem.d.__metadata);
                    changeItem.done(function (data, status, xhr) {
                        //return output;
                        var newDoc = {
                            ID: document[indexDocument].ID,
                            ApplicationID: document[indexDocument].ApplicationID,
                            Cif: document[indexDocument].Cif,
                            Name: document[indexDocument].Name,
                            ActDate: document[indexDocument].ActDate,
                            ExptDate: document[indexDocument].ExptDate,
                            TBOSLAID: document[indexDocument].TBOSLAID,
                            Type: document[indexDocument].TypeofDoc,
                            Purpose: document[indexDocument].PurposeofDoc,
                            LastModifiedDate: document[indexDocument].Modified,
                            LastModifiedBy: null,
                            DocumentPath: output.ServerRelativeUrl,
                            FileName: document[indexDocument].FileName.name,
                            Status: document[indexDocument].Status,
                            TBOApplicationID: context.Date
                        };
                        DocumentArray.push(newDoc);
                        viewModel.TransactionTBOModel().Documents().push(newDoc);
                        console.log(DocumentArray);
                        if (numFile > 1) {
                            UploadFileRecuresive(context, document, numFile - 1); // recursive function
                        } else {
                            UpdateTBO();
                        }
                    });
                    changeItem.fail(OnError);
                });
                getItem.fail(OnError);
            });
            addFile.fail(OnError);
        });
        getFile.fail(OnError);

        // Get the local file as an array buffer.
        function getFileBuffer() {
            var deferred = $.Deferred();
            var reader = new FileReader();
            reader.onloadend = function (e) {
                deferred.resolve(e.target.result);
            }
            reader.onerror = function (e) {
                deferred.reject(e.target.error);
            }

            if (document[indexDocument].FileName.type == Util.spitemdoc) {
                var fileURI = serverUrl + document[indexDocument].FileName.DocPath;
                // load file:
                fetch(fileURI, convert, alert);

                function convert(buffer) {
                    var blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });

                    var domURL = self.URL || self.webkitURL || self,
                      url = domURL.createObjectURL(blob),
                      img = new Image;

                    img.onload = function () {
                        domURL.revokeObjectURL(url); // clean up
                    };
                    img.src = url;
                    reader.readAsArrayBuffer(blob);
                }

                function fetch(url, callback, error) {

                    var xhr = new XMLHttpRequest();
                    try {
                        xhr.open("GET", url);
                        xhr.responseType = "arraybuffer";
                        xhr.onerror = function () {
                            error("Network error")
                        };
                        xhr.onload = function () {
                            if (xhr.status === 200) callback(xhr.response);
                            else error(xhr.statusText);
                        };
                        xhr.send();
                    } catch (err) {
                        error(err.message)
                    }
                }


            } else {
                reader.readAsArrayBuffer(document[indexDocument].FileName);
            }
            //bangkit

            return deferred.promise();
        }

        // Add the file to the file collection in the Shared Documents folder.
        function addFileToFolder(arrayBuffer) {

            // Construct the endpoint.
            var fileCollectionEndpoint = String.format(
                    "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                    "/add(overwrite=false, url='{2}')",
                serverUrl, serverRelativeUrlToFolder, fileName);

            UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token: Rizki 2017-01-31

            // Send the request and return the response.
            // This call returns the SharePoint file.
            return $.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val()
                    //"content-length": arrayBuffer.byteLength
                }
            });
        }

        // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
        function getListItem(fileListItemUri) {

            // Send the request and return the response.
            return $.ajax({
                url: fileListItemUri,
                type: "GET",
                headers: { "accept": "application/json;odata=verbose" }
            });
        }

        // Change the display name and title of the list item.
        function updateListItem(itemMetadata) {
            var body = {
                Title: document[indexDocument].FileName.name,
                Customer_x0020_Name: context.Name,
                Document_x0020_Type: document[indexDocument].TypeofDoc.Name,
                Document_x0020_Purpose: document[indexDocument].PurposeofDoc.Name,
                Application_x0020_ID: context.ApplicationID,
                CIF: context.CIF,
                __metadata: {
                    type: itemMetadata.type,
                    FileLeafRef: fileName,
                    Title: fileName
                }
            };

            UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token: Rizki 2017-01-31

            // Send the request and return the promise.
            // This call does not return response content from the server.
            return $.ajax({
                url: itemMetadata.uri,
                type: "POST",
                data: ko.toJSON(body),
                headers: {
                    "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                    "content-type": "application/json;odata=verbose",
                    //"content-length": body.length,
                    "IF-MATCH": itemMetadata.etag,
                    "X-HTTP-Method": "MERGE"
                }
            });
        }
    }

    self.close = function () {
        window.location = "/home-tmo/tbo-tmo";
    }

    function OnChangeDocumentType() {
        var newDate;
        var ActualSubmissionDate;
        var data = ko.utils.arrayFirst(self.ParameterTBO().DocumentTypes(),
               function (item) {
                   if (item.ID == $('#document-type-tbo').val()) {
                       Date.prototype.addDays = function (days) {
                           this.setDate(this.getDate() + parseInt(days));
                           return this;
                       }
                       newDate = self.LocalDate(new Date().addDays(item.Days), true);
                       ActualSubmissionDate = $('#application-date-actsubmissiondate').val(newDate);
                       viewModel.ActSubmissionDate(moment(ActualSubmissionDate.value).format('YYYY-MM-DD'));
                   }
               });
    }

    // Add new document handler
    self.AddDocument = function () {
        var vDocumentType = ko.utils.arrayFirst(self.ParameterTBO().DocumentTypes(), function (item) {
            return item.ID == self.SelectedTBO().DocumentType();
        });

        var vDocumentPurpose = ko.utils.arrayFirst(self.ParameterTBO().DocumentPurposes(), function (item) {
            return item.ID == self.SelectedTBO().DocumentPurpose();
        });

        var file = $('#document-path-tbo').data().ace_input_files;
        var doc = [];
        if (file != null) {
            doc = {
                ID: 0,
                Type: vDocumentType,
                Purpose: vDocumentPurpose,
                FileName: file[0].name,
                DocumentPath: file[0],
                LastModifiedDate: new Date(),
                LastModifiedBy: null
            };
        }

        if (doc.Type == null || doc.Purpose == null || doc.DocumentPath == null) {
            //alert("Please complete the upload form fields.")
            if (doc.Type == null) {
                $('#document-type-tbo').validate();
                $('#document-type-tbo').valid();
            }
            if (doc.Purpose) {
                $('#document-purpose-tbo').validate();
                $('#document-purpose-tbo').valid();
            }
            if (doc.DocumentPath) {
                $('#document-path-tbo').validate();
                $('#document-path-tbo').valid();
            }
        } else {
            //self.Documents.push(doc);
            self.IsNewDocument(false);
            PushCompletingDocument(doc);
            $('.remove').click();
            $("#tbotmo-doc-detail").modal('hide');
        }

        if ($('#application-date-actsubmissiondate').val() == "") {
            $('#application-date-actsubmissiondate').validate();
            $('#application-date-actsubmissiondate').valid()
        }

        if (self.SelectedTBO().DocumentType() == null) {
            $('#document-type-tbo').validate();
            $('#document-type-tbo').valid();
        }

        if (self.SelectedTBO().DocumentPurpose() == null) {
            $('#document-purpose-tbo').validate();
            $('#document-purpose-tbo').valid();
        }

        if ($('#document-path-tbo').data().ace_input_files == null) {
            $('#document-path-tbo').validate();
            $('#document-path-tbo').valid();
        }
    };

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterCustomerName() != "") filters.push({ Field: 'CustomerName', Value: self.FilterCustomerName() });
        if (self.FilterCif() != "") filters.push({ Field: 'CIF', Value: self.FilterCif() });
        if (self.FilterLastModifiedBy() != "") filters.push({ Field: 'ModifiedBy', Value: self.FilterLastModifiedBy() });
        if (self.FilterLastModifiedDate() != "") filters.push({ Field: 'ModifiedDate', Value: self.FilterLastModifiedDate() });

        return filters;
    };

    function GetFilteredColumnsWF() {
        // define filter
        var filters = [];
        self.IsFlagTBOWF(false);
        self.IsSubmitTBO(false);
        self.IsGetFilter(false);
        if (self.FilterApplicationID() != "") filters.push({ Field: 'CustomerName', Value: self.FilterApplicationID() });
        if (self.FilterCustomerName() != "") filters.push({ Field: 'CIF', Value: self.FilterCustomerName() });
        if (self.FilterLastModifiedDate() != "") filters.push({ Field: 'ModifiedDate', Value: self.FilterLastModifiedDate() });
        if (self.FilterStatus() != "") filters.push({ Field: 'Status', Value: self.FilterStatus() });
        return filters;
    };

    // Get data / refresh data
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') { $remove = true; $box.addClass('position-relative'); }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.tbotmotool,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }

    function GetDataWF() {
        if ($box.css('position') == 'static') { $remove = true; $box.addClass('position-relative'); }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        var options = {
            url: api.server + api.url.tbotmotoolwf,
            params: {
                page: self.GridPropertiesWF().Page(),
                size: self.GridPropertiesWF().Size(),
                sort_column: self.GridPropertiesWF().SortColumn(),
                sort_order: self.GridPropertiesWF().SortOrder()
            },
            token: accessToken
        };

        var filters = GetFilteredColumnsWF();

        if (filters.length > 0) {
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataWF, OnError, OnAlways);
        } else {
            Helper.Ajax.Get(options, OnSuccessGetDataWF, OnError, OnAlways);
        }
    }

    self.GetDataHomeTMOWF = function (data) {
        var splituser = spUser.LoginName.split('|').length;
        var paramUser = spUser.LoginName.split('|')[splituser - 1]

        var endPointURL;
        endPointURL = api.server + api.url.tbodatawftmoappid(paramUser);

        var options = {
            url: endPointURL,
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetDataHomeTMOWF, OnError, function () {
        });
    }

    function OnSuccessGetDataHomeTMOWF(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            var mapping = {
                //'ignore': ["LastModifiedDate", "LastModifiedBy"]
            };
            dataTempTBOwf = [];
            if (data != null) {
                for (var i = 0 ; i < data.length; i++) {
                    PushNamebyArray(data[i]);
                    DataTBOWF = {
                        TransactionID: data[i].TransactionID,
                        ID: data[i].ID,
                        CIF: data[i].CIF,
                        ApplicationID: data[i].ApplicationID,
                        TBOApplicationID: data[i].TBOApplicationID,
                        TBOSLAID: data[i].TBOSLAID,
                        Name: self.NameDocTBO(),
                        ExptDate: data[i].ExptDate,
                        ActDate: data[i].ActDate,
                        FileName: data[i].FileName,
                        PurposeofDoc: data[i].PurposeofDoc,
                        TypeofDoc: data[i].TypeofDoc,
                        Modified: data[i].LastModifiedDate,
                        Status: data[i].Status,
                        CreatedBy: data[i].CreatedBy,
                        CreatedDate: data[i].CreatedDate,
                        LastModifiedBy: data[i].LastModifiedBy,
                        LastModifiedDate: data[i].LastModifiedDate,
                        IsApprove: data[i].IsApprove,
                        CustomerName: data[i].CustomerName,
                        IsChecklistSubmit: false,
                        SPTaskListID: data[i].SPTaskListID,
                        SPTaskListItemID: data[i].SPTaskListItemID,
                        WorkflowInstanceID: data[i].WorkflowInstanceID,
                    };
                    dataTempTBOwf.push(DataTBOWF);
                }
            }
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    function GetTransactionDataTBO(cif) {
        var endPointURL;
        endPointURL = api.server + api.url.tbotmodata(cif);

        // declare options variable for ajax get request
        var options = {
            url: endPointURL,
            token: accessToken
        };

        // hide transaction data
        $('#gridTBOTMOList').hide();

        // call ajax
        Helper.Ajax.Get(options, OnSuccessGetTransactionDataTBO, OnError, function () {
            $("#newtransaction-tbotmo-form").hide();
            $("#newtransaction-tbotmo-form").show();
            $("#transaction-tbotmo-data").show();
            $("#transaction-tbotmo-data-wf").hide();
            $("#transaction-tbotmo-data-wf").show();
            self.IsFlagTBOWF();
        });
    }

    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {

            self.TBOTools(data.Data);
            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    function OnSuccessGetDataWF(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {

            self.TBOToolsWF(data.Data);
            self.GetDataHomeTMOWF();
            self.GridPropertiesWF().Page(data['Page']);
            self.GridPropertiesWF().Size(data['Size']);
            self.GridPropertiesWF().Total(data['Total']);
            self.GridPropertiesWF().TotalPages(Math.ceil(self.GridPropertiesWF().Total() / self.GridPropertiesWF().Size()));
            self.IsFlagTBOWF();

            if (self.IsGetSelectedRow()) {
                self.IsGetSelectedRow(true);
                self.IsSubmitTBO(false);
            } else {
                self.IsGetSelectedRow(false);
                self.IsSubmitTBO(true);
            }
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    function GetParameters() {
        var options = {
            url: api.server + api.url.parameter,
            params: {
                select: "PurposeDoc,TBOTransaction"
            },
            token: accessToken
        };
        Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
    }

    function OnSuccessGetParameters(data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            // bind result to observable array
            var mapping = {
                'ignore': ["LastModifiedDate", "LastModifiedBy"]
            };
            viewModel.ParameterTBO().DocumentTypes(ko.mapping.toJS(data.TBOTransaction, mapping));
            viewModel.ParameterTBO().DocumentPurposes(ko.mapping.toJS(data.PurposeDoc, mapping));
        }
    }

    function PushNamebyArray(x) {
        var name;
        self.NameDocTBO('');
        switch (x.TBOSLAID) {
            case Const_TBOSLADoc.MT103:
                name = 'MT103';
                break;
            case Const_TBOSLADoc.Other:
                name = 'Other Document';
                break;
        }
        self.NameDocTBO(name);
        if (x.Status == "Add" || x.Status == "Complete") {
            self.IsDisableTBO(true);
        } else {
            self.IsDisableTBO(false);
        }
    }

    function OnSuccessGetTransactionDataTBO(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            var mapping = {
                //'ignore': ["LastModifiedDate", "LastModifiedBy"]
            };
            console.log(data);
            var DataTBO = [];
            var TBOToolsByCIF = [];
            for (var i = 0 ; i < data.length; i++) {
                PushNamebyArray(data[i]);
                DataTBO = {
                    TransactionID: data[i].TransactionID,
                    TBOTransactionDraftID: data[i].TBOTMOTransactionID,
                    ID: data[i].ID,
                    ApplicationID: data[i].ApplicationID,
                    TBOSLAID: data[i].TBOSLAID,
                    Name: self.NameDocTBO(),
                    ExptDate: self.LocalDate(data[i].ExptDate, true),
                    ActDate: self.LocalDate(data[i].ActDate, true),
                    FileName: data[i].FileName,
                    PurposeofDoc: data[i].PurposeofDoc,
                    TypeofDoc: data[i].TypeofDoc,
                    Modified: self.LocalDate(data[i].LastModifiedDate, true),
                    Status: data[i].Status,
                    CreatedBy: data[i].CreatedBy,
                    CreatedDate: data[i].CreatedDate,
                    LastModifiedBy: data[i].LastModifiedBy,
                    LastModifiedDate: self.LocalDate(data[i].LastModifiedDate, true),
                    IsApprove: data[i].IsApprove,
                    CustomerName: data[i].CustomerName,
                    IsDisableTBO: self.IsDisableTBO(),
                    TBOApplicationID: data[i].TBOApplicationID
                };

                TBOToolsByCIF.push(DataTBO);
            }
            self.DocumentTBO(ko.mapping.toJS(TBOToolsByCIF, mapping));
            console.log(self.DocumentTBO());
            self.DocumentTBOWF(ko.mapping.toJS(TBOToolsByCIF, mapping));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
    // Event handlers declaration end
};

function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

function OnAlways() {
    $box.trigger('reloaded.ace.widget');
}

var formatCheckerDateApplicationTBO = function (task) {
    var ActualSubmissionDate = $('#application-date-actsubmissiondate');

    switch (task) {
        case "ActualSubmissionDate":
            viewModel.ActSubmissionDate(moment(ActualSubmissionDate.value).format('YYYY-MM-DD'));
            break;
    }
}

function CheckAllData(tbo) {
    viewModel.CheckAllData(tbo);
}

// View Model
var viewModel = new ViewModel();

// jQuery Init
$(document).ready(function () {
    // widget loader
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    // block enter key from user to prevent submitted data.
    $(this).keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

            return false;
        }
    });

    // Load all templates
    $('script[src][type="text/html"]').each(function () {
        //alert($(this).attr("src"))
        var src = $(this).attr("src");
        if (src != undefined) {
            $(this).load(src);
        }
    });

    // scrollables
    $('.slim-scroll').each(function () {
        var $this = $(this);
        $this.slimscroll({
            height: $this.data('height') || 100,
            //width: $this.data('width') || 100,
            railVisible: true,
            alwaysVisible: true,
            color: '#D15B47'
        });
    });

    // tooltip
    $('[data-rel=tooltip]').tooltip();

    // datepicker
    $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    $('.time-picker').timepicker({
        defaultTime: "",
        minuteStep: 1,
        showSeconds: false,
        showMeridian: false
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    $('#document-path-tbo').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        thumbnail: false,
        blacklist: 'exe|dll'
    });

    // validation
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,

        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });

    // Knockout Datepicker custom binding handler
    ko.bindingHandlers.datepicker = {
        init: function (element) {
            $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).datepicker("refresh");
        }
    };

    // Knockout Timepicker custom binding handler
    ko.bindingHandlers.timepicker = {
        init: function (element) {
            $(element).timepicker({
                defaultTime: "",
                minuteStep: 1,
                showSeconds: false,
                showMeridian: false
            }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).timepicker('showWidget');
        }
    };

    // Knockout custom file handler
    ko.bindingHandlers.file = {
        init: function (element, valueAccessor) {
            $(element).change(function () {
                var file = this.files[0];

                if (ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };

    // Knockout Bindings
    ko.applyBindings(viewModel);

    // Token Validation
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(TokenOnSuccess, TokenOnError);
    } else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // call spuser function
        GetCurrentUser(viewModel);

        // call get data inside view model
        viewModel.GetData();
        viewModel.GetParameters();
        //GetParameters();

        StartTaskHub();
    }

    // Modal form on close handler
    $("#modaltbotmo-form").on('hidden.bs.modal', function () {
        //alert("close")

        LockAndUnlockTaskHub("unlock", viewModel.WorkflowInstanceID(), viewModel.ApproverId(), viewModel.SPTaskListItemID(), viewModel.ActivityTitle());
    });
});

// Function to display notification.
// class name : gritter-success, gritter-warning, gritter-error, gritter-info
function ShowNotification(title, text, className, isSticky) {
    $.gritter.add({
        title: title,
        text: text,
        class_name: className,
        sticky: isSticky
    });
}

// Get SPUser from cookies
function GetCurrentUser() {
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);

        viewModel.SPUser = spUser;

    }
}

// Token validation On Success function
function TokenOnSuccess(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // call spuser function
    GetCurrentUser();

    viewModel.GetData();
    viewModel.GetParameters();
    //GetParameters();
}

// Token validation On Error function
function TokenOnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

function OnErrorSignal() {
    ShowNotification("Connection Failed", "Failed connected to signal server", "gritter-error", true);
}

function OnReceivedSignal(data) {
    ShowNotification(data.Sender, data.Message, "gritter-info", false);
}

// SignalR ----------------------
var taskHub = $.hubConnection(config.signal.server + "hub");
var taskProxy = taskHub.createHubProxy("TaskService");
var IsTaskHubConnected = false;

function StartTaskHub() {
    taskHub.start()
        .done(function () {

            IsTaskHubConnected = true;
        })
        .fail(function () {
            ShowNotification("Hub Connection Failed", "Fail while try connecting to hub server", "gritter-error", true);
        }
    );
}

function LockAndUnlockTaskHub(lockType, instanceId, approverID, taskId, activityTitle) {
    if (IsTaskHubConnected) {
        var data = {
            WorkflowInstanceID: instanceId,
            ApproverID: approverID,
            TaskID: taskId,
            ActivityTitle: activityTitle,
            LoginName: spUser.LoginName,
            DisplayName: spUser.DisplayName
        };

        var LockType = lockType.toLowerCase() == "lock" ? "LockTask" : "UnlockTask";

        // try lock current task
        taskProxy.invoke(LockType, data)
            .fail(function (err) {
                ShowNotification("LockType Task Failed", err, "gritter-error", true);
            }
        );
    } else {
        ShowNotification("Task Manager", "There is no active connection to task server manager.", "gritter-error", true);
    }
}


taskProxy.on("Message", function (data) {
    ShowNotification(data.From, data.Message, "gritter-info", false);
});

taskProxy.on("NotifyInfo", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-info", false);
});

taskProxy.on("NotifyWarning", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-warning", false);
});

taskProxy.on("NotifyError", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-error", true);
});
