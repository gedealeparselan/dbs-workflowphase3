﻿using DBS.Sharepoint.Helpers;
using Microsoft.SharePoint;
using Nintex.Workflow.HumanApproval;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DBS.Sharepoint
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class Group : IGroup
    {
        #region AddUserToGroup
        //tidak dipakai
        public string RemoveUserFromGroup(Remove vRemove)
        {
            string sHasil = vRemove.sWeb + " - " + vRemove.sGroupName + " - " + vRemove.sLoginName; // +" - " + vRemove.sGroupNameRemove;
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate
                {
                    using (SPSite site = new SPSite(SPContext.Current.Site.RootWeb.Url))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            web.AllowUnsafeUpdates = true;

                            SPUser user = null;
                            bool IsUser;
                            string[] usr = vRemove.sLoginName.Split('|');
                            try
                            {
                                string claimUser = string.Format("i:0#.f|{0}|{1}", usr[1], usr[2]);
                                user = web.EnsureUser(claimUser);
                                IsUser = true;
                            }
                            catch (Exception)
                            {
                                IsUser = false;
                            }

                            if (IsUser)
                            {
                                SPGroup group = null;
                                bool IsGroup;
                                try
                                {
                                    group = web.SiteGroups[vRemove.sGroupName];
                                    IsGroup = true;
                                }
                                catch (Exception)
                                {
                                    IsGroup = false;
                                }

                                if (IsGroup)
                                {
                                    group.RemoveUser(user);
                                }
                            }

                            web.AllowUnsafeUpdates = false;
                        }
                    }                    
                });                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return sHasil;
        }

        public string AddUserToAGroup(Add vAdd)
        {
            string web = SPContext.Current.Site.Url;
            string sHasil = vAdd.userFullName + " - " + vAdd.sWeb + " - " + vAdd.userGroupName + " - " + vAdd.userLoginName;
            var curSite = SPContext.Current.Site;
            SPSecurity.RunWithElevatedPrivileges(delegate
            {
                using (SPSite spSite = new SPSite(curSite.Url))
                {
                    using (SPWeb spWeb = spSite.OpenWeb())
                    {
                        try
                        {
                            spWeb.AllowUnsafeUpdates = true;

                            SPUser spUser = null;
                            bool IsSpUser;
                            string [] usr = vAdd.userLoginName.Split('|');
                            try
                            {
                                string claimUser = string.Format("i:0#.f|{0}|{1}", usr[1], usr[2]);
                                spUser = spWeb.EnsureUser(claimUser);
                                
                                IsSpUser = true;                                
                            }
                            catch (Exception)
                            {
                                IsSpUser = false;
                            }

                            if (IsSpUser)
                            {
                                if (vAdd.userGroupName != "")
                                {
                                    SPGroup spGroup = null;
                                    bool IsSpGroup;
                                    try
                                    {
                                        spGroup = spWeb.SiteGroups[vAdd.userGroupName];
                                        IsSpGroup = true;
                                    }
                                    catch (Exception)
                                    {
                                        IsSpGroup = false;
                                    }

                                    if (IsSpGroup)
                                    {
                                        spGroup.AddUser(spUser);
                                    }
                                }

                                if (vAdd.userGroupNameRemove != "")
                                {
                                    SPGroup spGroup = null;
                                    bool IsSpGroup;
                                    try
                                    {
                                        spGroup = spWeb.SiteGroups[vAdd.userGroupNameRemove];
                                        IsSpGroup = true;
                                    }
                                    catch (Exception)
                                    {
                                        IsSpGroup = false;
                                    }

                                    if (IsSpGroup)
                                    {
                                        spGroup.RemoveUser(spUser);
                                    }
                                }

                                SPListItem item;
                                SPList list = spWeb.Lists["User Information List"];
                                item = list.Items.GetItemById(spUser.ID);

                                item["Title"] = vAdd.userFullName;

                                item.Update();
                            }                            
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                        finally
                        {
                            spWeb.AllowUnsafeUpdates = false;
                        }
                    }
                }
            });
            return sHasil;
        }

        //tidak dipakai
        public string UpdateUserName(Update vUpdate)
        {
            string sHasil = vUpdate.sWeb + " - " + vUpdate.userFullName + " - " + vUpdate.sLoginName;
            SPSecurity.RunWithElevatedPrivileges(delegate
            {
                using (SPSite spSite = new SPSite(SPContext.Current.Site.RootWeb.Url))
                {
                    using (SPWeb spWeb = spSite.OpenWeb())
                    {
                        try
                        {
                            spWeb.AllowUnsafeUpdates = true;

                            SPUser spUser = null;
                            bool IsSpUser;
                            string[] usr = vUpdate.sLoginName.Split('|');
                            try
                            {
                                string claimUser = string.Format("i:0#.f|{0}|{1}", usr[1], usr[2]);
                                spUser = spWeb.EnsureUser(claimUser);
                                
                                IsSpUser = true;
                            }
                            catch (Exception)
                            {
                                IsSpUser = false;
                            }

                            if (IsSpUser)
                            {
                                SPListItem item;
                                SPList list = spWeb.Lists["User Information List"];
                                item = list.Items.GetItemById(spUser.ID);

                                item["Title"] = vUpdate.userFullName;

                                item.Update();
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                        finally
                        {
                            spWeb.AllowUnsafeUpdates = false;
                        }
                    }
                }

            });
            return sHasil;
        }
        //tidak dipakai
        public string DeleteUserFromSite(Delete vDelete)
        {
            string sHasil = vDelete.sWeb + " - " + vDelete.sLoginName;
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate
                {
                    using (SPSite site = new SPSite(SPContext.Current.Site.RootWeb.Url))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            SPUser spUser = null;
                            bool IsSpUser = false;
                            string[] usr = vDelete.sLoginName.Split('|');
                            web.AllowUnsafeUpdates = true;
                            try
                            {
                                string claimUser = string.Format("i:0#.f|{0}|{1}", usr[1], usr[2]);
                                spUser = web.EnsureUser(claimUser);

                                IsSpUser = true;

                                if (IsSpUser)
                                {
                                    //web.Users.RemoveByID(spUser.ID);
                                    web.SiteAdministrators.Remove(claimUser);
                                    //web.SiteUsers.RemoveByID(spUser.ID);
                                }
                            }
                            catch (Exception)
                            {
                                IsSpUser = false;
                            }                            

                            web.AllowUnsafeUpdates = false;
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return sHasil;
        }
        #endregion

        #region CreateGroup

        public string CreateGroup(AddGroup vAdd)
        {
            string sHasil = "";
            var curSite = SPContext.Current.Site;
            SPSecurity.RunWithElevatedPrivileges(delegate
            {
                using (SPSite spSite = new SPSite(curSite.Url))
                {
                    using (SPWeb spWeb = spSite.OpenWeb())
                    {
                        try
                        {
                            spWeb.AllowUnsafeUpdates = true;
                            if (vAdd.Type == "ADD")
                            {                                
                                SPRoleDefinition roleDefAdmin = spWeb.RoleDefinitions.GetByType(SPRoleType.Reader);

                                spWeb.SiteGroups.Add(vAdd.NamaGroup, spWeb.Site.Owner, spWeb.Site.Owner, vAdd.DescGroup);
                                
                                SPGroup groupAdmin = spWeb.SiteGroups[vAdd.NamaGroup];

                                SPRoleAssignment roleAssignAdmin = new SPRoleAssignment(groupAdmin);
                                roleAssignAdmin.RoleDefinitionBindings.Add(roleDefAdmin);
                                spWeb.RoleAssignments.Add(roleAssignAdmin);

                            }
                            else if (vAdd.Type == "UPDATE")
                            {
                                SPGroup group = spWeb.SiteGroups[vAdd.NamaGroupOld];
                                //group.Description = vAdd.DescGroup;
                                SPFieldMultiLineText text = (SPFieldMultiLineText)spWeb.SiteUserInfoList.Fields[SPBuiltInFieldId.Notes];
                                SPListItem groupItem = spWeb.SiteUserInfoList.GetItemById(group.ID);
                                groupItem[text.InternalName] = vAdd.DescGroup;
                                groupItem.Update();

                                group.Name = vAdd.NamaGroup;
                                group.Update();
                            }
                            else if(vAdd.Type == "DELETE")
                            {
                                SPGroupCollection spGroupColl = spWeb.SiteGroups;
                                spGroupColl.Remove(vAdd.NamaGroup);
                            }
                            spWeb.Update();
                           
                        }
                        catch (Exception ex)
                        {
                            sHasil = ex.Message;
                            throw new Exception(ex.Message);
                        }
                        finally
                        {
                            spWeb.AllowUnsafeUpdates = false;
                        }
                    }
                }
            });
            return sHasil;
        }

        public string UpdateGroup(UpdateGroup vUpdate)
        {
            string sHasil = "";
            var curSite = SPContext.Current.Site;
            SPSecurity.RunWithElevatedPrivileges(delegate
            {
                using (SPSite spSite = new SPSite(curSite.Url))
                {
                    using (SPWeb spWeb = spSite.OpenWeb())
                    {
                        try
                        {
                            spWeb.AllowUnsafeUpdates = true;

                            SPGroup group = spWeb.SiteGroups[vUpdate.NamaGroupOld];
                            group.Description = vUpdate.DescGroup;
                            group.Name = vUpdate.NamaGroup;
                            
                            group.Update();
                            
                            spWeb.Update();

                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                        finally
                        {
                            spWeb.AllowUnsafeUpdates = false;
                        }
                    }
                }
            });
            return sHasil;
        }

        public string DeleteUserFromSiteCollection(Delete vDelete)
        {
            string sHasil = "";
            string sCurrentSite = vDelete.sWeb;

            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate
                {
                    using (SPSite site = new SPSite(sCurrentSite))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            string sSQL = "EXEC SP_WF_GetNonActiveEmployee";

                            DataTable dtNonActiveEmployee = new DataTable();
                            dtNonActiveEmployee = DataHelper.ExecuteDataTable(sSQL);

                            if (dtNonActiveEmployee != null && dtNonActiveEmployee.Rows.Count > 0)
                            {
                                web.AllowUnsafeUpdates = true;

                                for (int i = 0; i < dtNonActiveEmployee.Rows.Count; i++)
                                {
                                    try
                                    {
                                        web.SiteAdministrators.Remove(dtNonActiveEmployee.Rows[i]["EmployeeUsername"].ToString());

                                        sSQL = string.Format("INSERT INTO [Workflow].[Log] (LogName, LogDescription, CreateDate, CreateBy) ");
                                        sSQL = sSQL + string.Format("VALUES ('Remove Non Active User from Workflow WF', '{0}', GETUTCDATE(), 'System Account')", dtNonActiveEmployee.Rows[i]["EmployeeUsername"].ToString());

                                        DataHelper.ExecuteNonQuery(sSQL);
                                    }
                                    catch (Exception ex)
                                    {
                                        sHasil = ex.Message;
                                    }    
                                }

                                web.AllowUnsafeUpdates = false;
                            }

                        }
                    }
                });
            }
            catch (Exception ex)
            {
                sHasil = ex.Message;
            }

            return sHasil;
        }

        #endregion

    }
}
