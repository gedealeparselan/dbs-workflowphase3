﻿using DBS.Sharepoint.Helpers;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Publishing.Navigation;
using Microsoft.SharePoint.Taxonomy;
using Nintex.Workflow.HumanApproval;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading.Tasks;

namespace DBS.Sharepoint
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class Taxonomys : ITaxonomy
    {
        private bool IsSuccess { get; set; }
        private string ErrorMessage { get; set; }
        private string CurrentFurl { get; set; }
        public string TermStore { get; set; }
        public string TermGroup { get; set; }
        public string TermSet { get; set; }

        private IList<Model.NavigationNodes> model = new List<Model.NavigationNodes>();
        public Test HelloWorld()
        {
            return new Test { LocalName = "aaaa", RemoteName = "asdas", Size = 41 };
        }

        private SPListItemCollection RetrieveItems(SPWeb web, string sList, string sFolder, string sQuery, string sViewName)
        {
            try
            {
                SPListItemCollection itemColl = null;
                SPList list = web.Lists[sList];

                SPQuery query = sViewName == null ? new SPQuery() : new SPQuery(web.Lists[sList].Views[sViewName]);
                query.Query = sQuery;
                query.RowLimit = 2000;

                if (sFolder != null)
                {
                    SPFolder folder = list.RootFolder.SubFolders[sFolder];
                    sFolder = "/";
                    query.Folder = folder;
                }

                itemColl = list.GetItems(query);

                return itemColl;
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        private SPListItemCollection RetrievePages(SPWeb web)
        {
            SPListItemCollection colls = null;

            string sQuery = string.Format(@"<OrderBy><FieldRef Name='FileRef' Ascending='True' /></OrderBy>");
            colls = RetrieveItems(web, "Pages", null, sQuery, "FlatViews");

            return colls;
        }

        private void ReadTaxonomy(SPSite site, SPWeb web)
        {
            TaxonomySession taxonomy;
            TermCollection terms;

            try
            {
                taxonomy = new TaxonomySession(site);
                terms = taxonomy.TermStores[TermStore].Groups[TermGroup].TermSets[TermSet].GetAllTerms();
                // get current term
                TaxonomyNavigationContext context = TaxonomyNavigationContext.Current;
                CurrentFurl = context.NavigationTerm != null ? context.NavigationTerm.GetWebRelativeFriendlyUrl() : string.Empty;
                //SPListItemCollection collsPages = RetrievePages(web);
                foreach (Term term in terms)
                {
                    if (term.IsDeprecated) return;

                    NavigationTerm navTerm = NavigationTerm.GetAsResolvedByWeb(term, web, TermSet);

                    //var pages = from SPListItem item in collsPages
                    //            where "/" + item.Url.ToString().ToLower() == navTerm.TargetUrl.Value.ToLower()
                    //            select item;
                    //if (pages != null && pages.Count() > 0)
                    //{
                    model.Add(new Model.NavigationNodes()
                    {
                        ID = term.Id.ToString(),
                        ParentID = term.Parent != null ? term.Parent.Id.ToString() : "",
                        ParentTitle = term.Parent != null ? term.Parent.Name.ToString() : "",
                        Title = term.Name,
                        Url = navTerm.TargetUrl.Value,
                        FriendlyUrl = navTerm.GetWebRelativeFriendlyUrl(),
                        Icon = term.CustomProperties.ContainsKey("Icon") ? term.CustomProperties["Icon"] : "",
                        IsHide = term.CustomProperties.ContainsKey("IsHide") ? Convert.ToBoolean(term.CustomProperties["IsHide"]) : false
                    });
                    //}
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                ErrorMessage = ex.Message + "<br/>" + ex.InnerException;
                model.Add(new Model.NavigationNodes()
                {
                    ParentTitle = ErrorMessage
                });
            }
            finally
            {
                taxonomy = null;
                terms = null;
            }
        }
        public IEnumerable<Model.NavigationNodes> NavigationNodes()
        {
            List<Model.NavigationNodes> result = new List<Model.NavigationNodes>();

            using (SPSite site = new SPSite(SPContext.Current.Site.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    TermStore = System.Web.Configuration.WebConfigurationManager.AppSettings["TermStore"].ToString();
                    TermGroup = System.Web.Configuration.WebConfigurationManager.AppSettings["TermGroup"].ToString();
                    TermSet = System.Web.Configuration.WebConfigurationManager.AppSettings["TermSet"].ToString();
                    //<add key="TermStore" value="Managed Metadata Services" />
	                //<add key="TermGroup" value="Site Collection - w07gwrfweb01" />
	                //<add key="TermSet" value="DBS Workflow Navigation" />
                    ReadTaxonomy(site, web);
                }
            }

            return model;
        }
    }
}
