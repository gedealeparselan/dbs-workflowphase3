﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DBS.WebAPI.Models
{
    public class Verify
    {
        public bool IsVerified { get; set; }
        public Column Column { get; set; }
    }

    public class Column
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}