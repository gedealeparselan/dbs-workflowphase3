﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    public class BranchBankTesController : ApiController
    { 
    private string message = string.Empty;

        private readonly IBranchBankTes repository = null;

        public BranchBankTesController()
        {
            this.repository = new BranchBankTesRepository();
        }

        public BranchBankTesController(IBranchBankTes repository)
        {
            this.repository = repository;
        }

        //Get All Data Branch Bank
        [ResponseType(typeof(IList<BranchBankTesModel>))]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            IList<BranchBankTesModel> output = new List<BranchBankTesModel>();

            if (!repository.GetBranchBank(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<BranchBankTesModel>>(HttpStatusCode.OK, output);
            }
        }

        //Get All Data for Grid
        [ResponseType(typeof(BranchBankTesGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/BranchBank")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<BranchBankTesFilter> filters)
        {
            BranchBankTesGrid output = new BranchBankTesGrid();

            if (!repository.GetBranchBankGrid(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<BranchBankTesGrid>(HttpStatusCode.OK, output);
            }
        }

        //Get Detail Data Branch Bank For Grid
        [ResponseType(typeof(BranchBankTesGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/BranchBank/Detail/{ID}")]
        public HttpResponseMessage Get(int ID,int? page, int? size, string sort_column, string sort_order, IList<BranchBankTesFilter> filters)
        {
            BranchBankTesGrid output = new BranchBankTesGrid();

            if (!repository.GetBranchBankDetail(ID, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<BranchBankTesGrid>(HttpStatusCode.OK, output);
            }
        }

        //Get Data Branch Bank By ID
        [ResponseType(typeof(BranchBankTesModel))]
        [Route("api/BranchBank/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            BranchBankTesModel output = new BranchBankTesModel();

            if (!repository.GetBranchBankByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<BranchBankTesModel>(HttpStatusCode.OK, output);
            }
        }

        //Create new Data Branch Bank
        [HttpPost]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Post(BranchBankTesModel BranchBank)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.addBranchBank(BranchBank, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Branch Bank data has been created." });
                }
            }
        }

        //Modify Data Branch Bank
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/BranchBank/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, BranchBankTesModel branchbank)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateBranchBank(id, branchbank, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Branch Bank data has been updated." });
                }
            }
        }

        //Remove Data Branch Bank
        [HttpDelete]
        [Route("api/BranchBank/{id}")]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteBranchBank(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Product data has been deleted." });
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }

    }
}