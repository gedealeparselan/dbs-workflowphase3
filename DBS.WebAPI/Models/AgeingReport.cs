﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Models;
using DBS.WebAPI.Helpers;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("AgeingReport")]
    public class AgeingReportModel
    {
        /// <summary>
        /// AgeingReportParameter ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// AgeingReportParameters Name.
        /// </summary>
        [MaxLength(50, ErrorMessage = "Type Of Ageing is must be in {1} characters.")]
        public string Name { get; set; }


        /// <summary>
        /// AgeingReportParameters Name.
        /// </summary>
        //[MaxLength(4, ErrorMessage = "Times is must be in {1} characters.")]
        public TimeSpan? Times { get; set; }

        /// <summary>
        /// AgeingReportParameter Description.
        /// </summary>
        [MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Days { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class AgeingReportRow : AgeingReportModel
    {
        public int RowID { get; set; }

    }

    public class AgeingReportGrid : Grid
    {
        public IList<AgeingReportRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class AgeingReportFilter : Filter { }
    #endregion

    #region Interface
    public interface IAgeingReportRepository : IDisposable
    {
        bool GetAgeingReportByID(int id, ref AgeingReportModel ageingReport, ref string message);
        bool GetAgeingReport(ref IList<AgeingReportModel> ageingReports, int limit, int index, ref string message);
        bool GetAgeingReport(ref IList<AgeingReportModel> ageingReports, ref string message);
        bool GetAgeingReport(int page, int size, IList<AgeingReportFilter> filters, string sortColumn, string sortOrder, ref AgeingReportGrid ageingReportGrid, ref string message);
        bool GetAgeingReport(AgeingReportFilter filter, ref IList<AgeingReportModel> ageingReports, ref string message);
        bool GetAgeingReport(string key, int limit, ref IList<AgeingReportModel> ageingReports, ref string message);
        bool AddAgeingReport(AgeingReportModel ageingReport, ref string message);
        bool UpdateAgeingReport(int id, AgeingReportModel ageingReport, ref string message);
        bool DeleteAgeingReport(int id, ref string message);
    }
    #endregion

    #region Repository
    public class AgeingReportRepository : IAgeingReportRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetAgeingReportByID(int id, ref AgeingReportModel ageingReport, ref string message)
        {
            bool isSuccess = false;

            try
            {
                ageingReport = (from a in context.Ageings
                                where a.IsDeleted.Equals(false) && a.AgeingID.Equals(id)
                                select new AgeingReportModel
                                {
                                    ID = a.AgeingID,
                                    Name = a.AgeingName,
                                    Times = a.AngeingTimes,
                                    Days = a.AgeingDays,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetAgeingReport(ref IList<AgeingReportModel> ageingReports, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    ageingReports = (from a in context.Ageings
                                     where a.IsDeleted.Equals(false)
                                     orderby new { a.AgeingName, a.AngeingTimes, a.AgeingDays }
                                     select new AgeingReportModel
                                     {
                                         ID = a.AgeingID,
                                         Name = a.AgeingName,
                                         Times = a.AngeingTimes,
                                         Days = a.AgeingDays,
                                         LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                         LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                     }).ToList();
                }


                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetAgeingReport(ref IList<AgeingReportModel> ageingReports, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    ageingReports = (from a in context.Ageings
                                     where a.IsDeleted.Equals(false)
                                     orderby new { a.AgeingName, a.AngeingTimes, a.AgeingDays }
                                     select new AgeingReportModel
                                     {
                                         ID = a.AgeingID,
                                         Name = a.AgeingName,
                                         Times = a.AngeingTimes,
                                         Days = a.AgeingDays,
                                         LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                         LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                     }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetAgeingReport(int page, int size, IList<AgeingReportFilter> filters, string sortColumn, string sortOrder, ref AgeingReportGrid ageingReportGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                AgeingReportModel filter = new AgeingReportModel();
                if (filters != null)
                {
                    //string times = (string)filters.Where(a => a.Field.Equals("Times")).Select(a => a.Value).Where(a => a != null).SingleOrDefault();
                    filter.Name = (string)filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).Where(a => a != null).SingleOrDefault();
                    filter.Days = (string)filters.Where(a => a.Field.Equals("Days")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.Ageings
                                let isName = !string.IsNullOrEmpty(filter.Name)
                                //let isTimes = filter.StringTimes != null
                                let isDays = !string.IsNullOrEmpty(filter.Days)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isName ? a.AgeingName.Contains(filter.Name) : true) &&
                                    //(isTimes ? a.AngeingTimes.Value.Equals(filter.Times) : true) &&
                                    (isDays ? a.AgeingDays.Contains(filter.Days) : true) &&
                                    (isCreateBy ? a.CreateBy.Contains(filter.LastModifiedBy) : true) &&
                                    (isCreateDate ? (a.CreateDate > filter.LastModifiedDate.Value && a.CreateDate < maxDate) : true)
                                select new AgeingReportModel
                                {
                                    ID = a.AgeingID,
                                    Name = a.AgeingName,
                                    Times = a.AngeingTimes,
                                    Days = a.AgeingDays,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    ageingReportGrid.Page = page;
                    ageingReportGrid.Size = size;
                    ageingReportGrid.Total = data.Count();
                    ageingReportGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    ageingReportGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new AgeingReportRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Name = a.Name,
                            Times = a.Times,
                            Days = a.Days,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetAgeingReport(AgeingReportFilter filter, ref IList<AgeingReportModel> ageingReports, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetAgeingReport(string key, int limit, ref IList<AgeingReportModel> ageingReports, ref string message)
        {
            bool isSuccess = false;

            try
            {
                ageingReports = (from a in context.Ageings
                                 where a.IsDeleted.Equals(false) &&
                                     a.AgeingName.Contains(key) || a.AngeingTimes.Equals(key) || a.AgeingDays.Contains(key)
                                 orderby new { a.AgeingName, a.AngeingTimes, a.AgeingDays }
                                 select new AgeingReportModel
                                 {
                                     Name = a.AgeingName,
                                     Times = a.AngeingTimes,
                                     Days = a.AgeingDays,
                                     LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                     LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                 }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddAgeingReport(AgeingReportModel ageingReport, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.Ageings.Where(a => a.AgeingName.Equals(ageingReport.Name) && a.IsDeleted.Equals(false)).Any())
                {
                    context.Ageings.Add(new Entity.Ageing()
                    {
                        AgeingName = ageingReport.Name,
                        AngeingTimes = ageingReport.Times,
                        AgeingDays = ageingReport.Days,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Ageing Report Parameter data for Ageing Report Parameter Name {0} is already exist.", ageingReport.Name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateAgeingReport(int id, AgeingReportModel ageingReport, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Ageing data = context.Ageings.Where(a => a.AgeingID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.Ageings.Where(a => a.AgeingID != ageingReport.ID && a.AgeingName.Equals(ageingReport.Name) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Ageing Report Name {0} is exist.", ageingReport.Name);
                    }
                    else
                    {
                        data.AgeingName = ageingReport.Name;
                        data.AngeingTimes = ageingReport.Times;
                        data.AgeingDays = ageingReport.Days;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Ageing Report Parameter data for Ageing Report Parameter ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteAgeingReport(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Ageing data = context.Ageings.Where(a => a.AgeingID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Ageing Report data for Ageing Report Parameter ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}