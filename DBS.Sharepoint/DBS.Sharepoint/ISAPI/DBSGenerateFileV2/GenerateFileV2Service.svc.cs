﻿using DBS.Sharepoint.Helpers;
using Microsoft.SharePoint;
using Nintex.Workflow.HumanApproval;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Data.SqlClient;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;


namespace DBS.Sharepoint
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class GenerateFileV2Service : IGenerateFileV2Service
    {
        public Test HelloWorld()
        {
            return new Test { LocalName = "dsada", RemoteName = "asdas", Size = 41 };
        }

        public GeneratedFileInfoV2 GenerateFileV2(DataInformationCollectionV2 data)
        {
            GeneratedFileInfoV2 result = null;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    #region Property
                    long InsertedID = 0;
                    string InsertedURL = string.Empty;
                    string Message = string.Empty;
                    string FileFormat = string.Empty;
                    string FileNameFormat = string.Empty;
                    bool IsSuccess = false;

                    string ViewName = string.Empty;
                    StringBuilder sql = new StringBuilder();
                    string SelCol = string.Empty;
                    DateTime Date = DateTime.Now;
                    string DateFormat = String.Format("{0:ddMMyyyy}", Convert.ToDateTime(data.dtimeFile)).ToUpper();
                    string splogin = SPContext.Current.Web.CurrentUser.LoginName;
                    #endregion

                    #region Set File Property
                    #region FD
                    if (data.FileTipe == FileHelperV2.FTipeFDRTGS)
                    {
                        //sql = new StringBuilder();
                        ViewName = FileHelperV2.ViewNameFDRTGS;
                        sql.AppendLine("SELECT ");
                        foreach (var item in FileHelperV2.SelectColumnFDRTGS)
                        {
                            SelCol += item + ",";
                        }
                        if (SelCol.EndsWith(","))
                        {
                            SelCol = SelCol.Substring(0, SelCol.Length - 1);
                        }
                        sql.AppendLine(SelCol);
                        sql.AppendLine("FROM " + ViewName + "");
                        sql.AppendLine("WHERE CONVERT(VARCHAR, CREATEDATE,105) = CONVERT(VARCHAR,'" + Convert.ToDateTime(data.dtimeFile).ToString("dd-MM-yyyy") + "',105)");
                        FileNameFormat = "FDTransfer_RTGS_" + DateFormat + ".csv";
                    }
                    else if (data.FileTipe == FileHelperV2.FTipeFDSKN)
                    {
                        // sql = new StringBuilder();
                        ViewName = FileHelperV2.ViewNameFDSKN;
                        sql.AppendLine("SELECT ");
                        foreach (var item in FileHelperV2.SelectColumnFDSKN)
                        {
                            SelCol += item + ",";
                        }
                        if (SelCol.EndsWith(","))
                        {
                            SelCol = SelCol.Substring(0, SelCol.Length - 1);
                        }
                        sql.AppendLine(SelCol);
                        sql.AppendLine("FROM " + ViewName + "");
                        sql.AppendLine("WHERE CONVERT(VARCHAR, CREATEDATE,105) = CONVERT(VARCHAR,'" + Convert.ToDateTime(data.dtimeFile).ToString("dd-MM-yyyy") + "',105)");
                        FileNameFormat = "FDTransfer_SKN_" + DateFormat + ".csv";
                    }
                    else if (data.FileTipe == FileHelperV2.FTipeFDInterestMaintenance)
                    {
                        //sql = new StringBuilder();
                        //data.DataInformation.Clear();

                        string FdMaturityID = string.Empty;
                        if (data.DataInformation.Count() > 0)
                        {
                            foreach (var item in data.DataInformation)
                                FdMaturityID += item.ID.ToString() + ",";
                            if (FdMaturityID.EndsWith(","))
                                FdMaturityID = FdMaturityID.Substring(0, FdMaturityID.Length - 1);
                        }
                        ViewName = FileHelperV2.ViewNameFDIM;
                        sql.AppendLine("SELECT ");
                        foreach (var item in FileHelperV2.SelectColumnFDIM)
                        {
                            SelCol += item + ",";
                        }
                        if (SelCol.EndsWith(","))
                        {
                            SelCol = SelCol.Substring(0, SelCol.Length - 1);
                        }
                        sql.AppendLine(SelCol);
                        sql.AppendLine("FROM " + ViewName + "");
                        //sql.AppendLine("WHERE CONVERT(VARCHAR, CREATEDATE,105) = CONVERT(VARCHAR,'" + Convert.ToDateTime(data.dtimeFile).ToString("dd-MM-yyyy") + "',105)");
                        sql.AppendLine("WHERE FDMaturityID IN (" + FdMaturityID + ")");
                        FileNameFormat = "RO_" + DateFormat + ".csv";


                    }
                    #endregion
                    #region Loan
                    else if (data.FileTipe == FileHelperV2.FTipeLoanRollover)
                    {
                        sql = new StringBuilder();
                        string SelectedIdIN = string.Empty;
                        if (data.DataInformation.Count() > 0)
                        {
                            foreach (var item in data.DataInformation)
                                SelectedIdIN += item.ID.ToString() + ",";
                            if (SelectedIdIN.EndsWith(","))
                                SelectedIdIN = SelectedIdIN.Substring(0, SelectedIdIN.Length - 1);
                        }
                        ViewName = FileHelperV2.ViewNameLoanRollover;
                        sql.AppendLine("SELECT ");
                        foreach (var item in FileHelperV2.SelectColumnLoanRollover)
                        {
                            SelCol += item + ",";
                        }
                        if (SelCol.EndsWith(","))
                        {
                            SelCol = SelCol.Substring(0, SelCol.Length - 1);
                        }
                        sql.AppendLine(SelCol);
                        sql.AppendLine("FROM " + ViewName + "");
                        sql.AppendLine("WHERE CSORolloverID IN (" + SelectedIdIN + ")");
                        FileNameFormat = "LOAN_" + DateFormat + "_RO" + ".csv";
                    }
                    else if (data.FileTipe == FileHelperV2.FTipeLoanIM)
                    {
                        sql = new StringBuilder();
                        string SelectedIdIN = string.Empty;
                        if (data.DataInformation.Count() > 0)
                        {
                            foreach (var item in data.DataInformation)
                                SelectedIdIN += item.ID.ToString() + ",";
                            if (SelectedIdIN.EndsWith(","))
                                SelectedIdIN = SelectedIdIN.Substring(0, SelectedIdIN.Length - 1);
                        }
                        ViewName = FileHelperV2.ViewNameLoanIM;
                        sql.AppendLine("SELECT ");
                        foreach (var item in FileHelperV2.SelectColumnLoanIM)
                        {
                            SelCol += item + ",";
                        }
                        if (SelCol.EndsWith(","))
                        {
                            SelCol = SelCol.Substring(0, SelCol.Length - 1);
                        }
                        sql.AppendLine(SelCol);
                        sql.AppendLine("FROM " + ViewName + "");
                        sql.AppendLine("WHERE CSOInterestMaintenanceID IN (" + SelectedIdIN + ")");
                        FileNameFormat = "LOAN_" + DateFormat + "_LP" + ".csv";
                    }
                    #endregion
                    #region POAEmail
                    else if (data.FileTipe == FileHelperV2.POAEmail)
                    {
                        sql.AppendLine("EXEC SP_GeneratePOAEmail '" + data.CIF + "'");
                        FileNameFormat = "POAEmail_" + data.CIF + ".txt";
                    }
                    #endregion
                    else
                    {
                        result = new GeneratedFileInfoV2
                        {
                            InsertedID = 0,
                            InsertedURL = string.Empty,
                            IsSuccess = false,
                            Message = "Generate File Failed",
                            FileName = ""
                        };
                        return result;
                    }
                    #endregion

                    #region Get Datarow for File
                    DataTable dtTransaction = new DataTable();
                    dtTransaction = DataHelper.ExecuteDataTable(sql.ToString());
                    string GetFiletipe = data.FileTipe;
                    data = new DataInformationCollectionV2();
                    if (GetFiletipe != FileHelperV2.POAEmail)
                        FileFormat = FileHelperV2.DataTableToCSV(dtTransaction, ",", true);
                    else
                        FileFormat = FileHelper.POAFormat(dtTransaction);
                    #endregion

                    #region Procesing Generate
                    if (!string.IsNullOrEmpty(FileFormat))
                    {
                        using (SPSite site = new SPSite(SPContext.Current.Site.Url))
                        {
                            using (SPWeb web = site.OpenWeb())
                            {
                                web.AllowUnsafeUpdates = true;
                                string sqlGenerated = string.Empty;

                                #region Generate File To Lib
                                using (Stream dataStream = FileHelper.GenerateStreamFromString(FileFormat))
                                {
                                    if (FileHelper.AddToLibrary(web, dataStream, FileNameFormat, "Documents", true, ref InsertedID, ref InsertedURL, ref IsSuccess, ref Message))
                                    {
                                        StringBuilder inserSQl = new StringBuilder();
                                        inserSQl.AppendLine("INSERT INTO [Master].[DownloadUploadLog]([ActivityType],[ActivityName],[Description],[IsDeleted],[CreateDate],[CreateBy])");
                                        inserSQl.AppendLine("VALUES('" + UploadFileHelper.ActivityTypeisDownload + "','" + InsertedURL + "','ListID:" + InsertedID + "," + FileNameFormat + "',0,cast('" + DateTime.UtcNow + "' as datetime),'" + splogin + "')");

                                        DataHelper.ExecuteDataTable(inserSQl.ToString());
                                    }
                                }
                                #endregion

                                #region Send Return Value
                                if (string.IsNullOrEmpty(Message))
                                {
                                    result = new GeneratedFileInfoV2
                                    {
                                        InsertedID = InsertedID,
                                        InsertedURL = InsertedURL,
                                        IsSuccess = IsSuccess,
                                        Message = Message,
                                        FileName = FileNameFormat
                                    };
                                    ts.Complete();
                                }
                                else
                                {
                                    result = new GeneratedFileInfoV2
                                    {
                                        InsertedID = InsertedID,
                                        InsertedURL = InsertedURL,
                                        IsSuccess = false,
                                        Message = Message,
                                        FileName = ""
                                    };
                                    ts.Dispose();
                                }

                                web.AllowUnsafeUpdates = false;
                                #endregion
                            }
                        }
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    result = new GeneratedFileInfoV2
                    {
                        InsertedID = 0,
                        InsertedURL = string.Empty,
                        IsSuccess = false,
                        Message = ex.Message,
                        FileName = ""
                    };
                    ts.Dispose();
                }
            }

            return result;
        }

        public GeneratedFileInfoV2 GenerateFileInfoV2_2(DataInformationCollectionV2Turunan data)
        {
            GeneratedFileInfoV2 result = null;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    #region Property
                    long InsertedID = 0;
                    string InsertedURL = string.Empty;
                    string Message = string.Empty;
                    string FileFormat = string.Empty;
                    string FileNameFormat = string.Empty;
                    bool IsSuccess = false;
                    GeneratedFileInfoV2 res = null;

                    string ViewName = string.Empty;
                    StringBuilder sql = new StringBuilder();
                    string SelCol = string.Empty;
                    DateTime Date = DateTime.Now;
                    data.dtimeFile = string.IsNullOrEmpty(data.dtimeFile) ? Date.ToShortDateString() : data.dtimeFile; 
                    string DateFormat = String.Format("{0:ddMMyyyy}", Convert.ToDateTime(data.dtimeFile)).ToUpper();
                    string splogin = SPContext.Current.Web.CurrentUser.LoginName;
                    #endregion

                    #region query processing
                    if (data.FileTipe == FileHelperV2.FTipeChangeRMCSV)
                    {
                        sql.AppendLine("EXEC " + FileHelperV2.SP_CRM + "'" + data.ID + "'");
                        FileNameFormat = "ChangeRM_" + data.ID + ".csv";
                        DataTable dtTransaction = new DataTable();
                        dtTransaction = DataHelper.ExecuteDataTable(sql.ToString());
                        FileFormat = FileHelperV2.DataTableToCSV(dtTransaction, ",", true);
                    }
                    else if(data.FileTipe == FileHelperV2.FTipeChangeRMExcel) {
                        sql.AppendLine("EXEC " + FileHelperV2.SP_CRM + "'" + data.ID + "'");
                        FileNameFormat = "ChangeRM_" + data.ID + ".xlsx";
                        DataTable dtTransaction = new DataTable();
                        dtTransaction = DataHelper.ExecuteDataTable(sql.ToString());
                        //CreateExcelDoc(@"C:\"+FileNameFormat, dtTransaction, "Chenge RM");
                        res = CreateStreamExcelDoc(dtTransaction, "Chenge RM", FileNameFormat, splogin);
                        FileFormat = "Change RM";
                    }
                    else
                    {
                        result = new GeneratedFileInfoV2
                        {
                            InsertedID = 0,
                            InsertedURL = string.Empty,
                            IsSuccess = false,
                            Message = "Generate File Failed",
                            FileName = ""
                        };
                        return result;
                    }
                    #endregion

                    #region Procesing Generate
                    if (!string.IsNullOrEmpty(FileFormat))
                    {
                        using (SPSite site = new SPSite(SPContext.Current.Site.Url))
                        {
                            using (SPWeb web = site.OpenWeb())
                            {
                                web.AllowUnsafeUpdates = true;
                                string sqlGenerated = string.Empty;

                                #region Generate File To Lib
                                if (data.FileTipe == FileHelperV2.FTipeChangeRMCSV)
                                {
                                    using (Stream dataStream = FileHelper.GenerateStreamFromString(FileFormat))
                                    {
                                        if (FileHelper.AddToLibrary(web, dataStream, FileNameFormat, "Documents", true, ref InsertedID, ref InsertedURL, ref IsSuccess, ref Message))
                                        {
                                            StringBuilder inserSQl = new StringBuilder();
                                            inserSQl.AppendLine("INSERT INTO [Master].[DownloadUploadLog]([ActivityType],[ActivityName],[Description],[IsDeleted],[CreateDate],[CreateBy])");
                                            inserSQl.AppendLine("VALUES('" + UploadFileHelper.ActivityTypeisDownload + "','" + InsertedURL + "','ListID:" + InsertedID + "," + FileNameFormat + "',0,cast('" + DateTime.UtcNow + "' as datetime),'" + splogin + "')");

                                            DataHelper.ExecuteDataTable(inserSQl.ToString());
                                        }
                                    }
                                }
                                else if (data.FileTipe == FileHelperV2.FTipeChangeRMExcel)
                                {
                                    InsertedID = res.InsertedID;
                                    InsertedURL = res.InsertedURL;
                                    IsSuccess = res.IsSuccess;
                                    Message = res.Message;
                                }
                                
                                #endregion

                                #region Send Return Value
                                if (string.IsNullOrEmpty(Message))
                                {
                                    result = new GeneratedFileInfoV2
                                    {
                                        InsertedID = InsertedID,
                                        InsertedURL = InsertedURL,
                                        IsSuccess = IsSuccess,
                                        Message = Message,
                                        FileName = FileNameFormat
                                    };
                                    ts.Complete();
                                }
                                else
                                {
                                    result = new GeneratedFileInfoV2
                                    {
                                        InsertedID = InsertedID,
                                        InsertedURL = InsertedURL,
                                        IsSuccess = false,
                                        Message = Message,
                                        FileName = ""
                                    };
                                    ts.Dispose();
                                }

                                web.AllowUnsafeUpdates = false;
                                #endregion
                            }
                        }
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    result = new GeneratedFileInfoV2
                    {
                        InsertedID = 0,
                        InsertedURL = string.Empty,
                        IsSuccess = false,
                        Message = ex.Message,
                        FileName = ""
                    };
                    ts.Dispose();
                }
            }

            return result;
        }

        /*
         * Spreedsheet Document mempunyai paling tidak satu workbook
         * satu workbook paing tidak mempunyai satu worksheet
         * satu worksheet berisi paling tidak satu sheet dan bisa banyak sheet.
         */
        public void CreateExcelDoc(string fileName, DataTable table, string sheetName)
        {
            using (SpreadsheetDocument document = SpreadsheetDocument.Create(fileName, SpreadsheetDocumentType.Workbook))
            {
                #region Create Blank Excel Document (dokumen kosong tanpa data)
                WorkbookPart workbookPart = document.AddWorkbookPart();
                workbookPart.Workbook = new Workbook();

                WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                var sheetData = new SheetData();
                worksheetPart.Worksheet = new Worksheet(sheetData);
                //worksheetPart.Worksheet = new Worksheet(new SheetData());

                Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

                Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = sheetName };

                sheets.Append(sheet);

                workbookPart.Workbook.Save(); 
                #endregion

                #region create Header dynamicly
                Row headerRow = new Row();

                List<String> columns = new List<string>();
                foreach (System.Data.DataColumn column in table.Columns)
                {
                    columns.Add(column.ColumnName);
                    headerRow.AppendChild(ConstructCell(column.ColumnName, CellValues.String));
                }

                sheetData.AppendChild(headerRow); 
                #endregion

                #region Insert Data To Excecl Document
                foreach (DataRow dsrow in table.Rows)
                {
                    Row newRow = new Row();
                    foreach (String col in columns)
                    {
                        newRow.AppendChild(ConstructCell(dsrow[col].ToString(), CellValues.String));
                    }
                    sheetData.AppendChild(newRow);
                } 
                #endregion

                #region Save
                workbookPart.Workbook.Save(); 
                #endregion
            }
        }

        public GeneratedFileInfoV2 CreateStreamExcelDoc(DataTable table, string sheetName, string fileName, string splogin)
        {
            GeneratedFileInfoV2 result = new GeneratedFileInfoV2();
            long InsertedID = 0;
            string InsertedURL = string.Empty;
            string Message = string.Empty;
            bool IsSuccess = false;
            string ViewName = string.Empty;
            StringBuilder sql = new StringBuilder();
            string SelCol = string.Empty;
            //DateTime Date = DateTime.Now;
            //data.dtimeFile = string.IsNullOrEmpty(data.dtimeFile) ? Date.ToShortDateString() : data.dtimeFile;
            //string DateFormat = String.Format("{0:ddMMyyyy}", Convert.ToDateTime(data.dtimeFile)).ToUpper();
            
            using(MemoryStream memoryStream = new MemoryStream()) {
                SpreadsheetDocument document = SpreadsheetDocument.Create(memoryStream, SpreadsheetDocumentType.Workbook);
                #region Create Blank Excel Document (dokumen kosong tanpa data)
                WorkbookPart workbookPart = document.AddWorkbookPart();
                workbookPart.Workbook = new Workbook();

                WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                var sheetData = new SheetData();
                worksheetPart.Worksheet = new Worksheet(sheetData);
                //worksheetPart.Worksheet = new Worksheet(new SheetData());

                Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

                Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = sheetName };

                sheets.Append(sheet);

                workbookPart.Workbook.Save();
                #endregion

                #region create Header dynamicly
                Row headerRow = new Row();

                List<String> columns = new List<string>();
                foreach (System.Data.DataColumn column in table.Columns)
                {
                    columns.Add(column.ColumnName);
                    headerRow.AppendChild(ConstructCell(column.ColumnName, CellValues.String));
                }

                sheetData.AppendChild(headerRow);
                #endregion

                #region Insert Data To Excecl Document
                foreach (DataRow dsrow in table.Rows)
                {
                    Row newRow = new Row();
                    foreach (String col in columns)
                    {
                        newRow.AppendChild(ConstructCell(dsrow[col].ToString(), CellValues.String));
                    }
                    sheetData.AppendChild(newRow);
                }
                #endregion

                #region Save
                workbookPart.Workbook.Save();
                document.Close();
                StreamWriter streamWriter = new StreamWriter(memoryStream);
                streamWriter.Flush();
                memoryStream.Position = 0;
                using (SPSite site = new SPSite(SPContext.Current.Site.Url))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        web.AllowUnsafeUpdates = true;
                        if (FileHelper.AddToLibrary(web, memoryStream, fileName, "Documents", true, ref InsertedID, ref InsertedURL, ref IsSuccess, ref Message))
                        {
                            StringBuilder inserSQl = new StringBuilder();
                            inserSQl.AppendLine("INSERT INTO [Master].[DownloadUploadLog]([ActivityType],[ActivityName],[Description],[IsDeleted],[CreateDate],[CreateBy])");
                            inserSQl.AppendLine("VALUES('" + UploadFileHelper.ActivityTypeisDownload + "','" + InsertedURL + "','ListID:" + InsertedID + "," + fileName + "',0,cast('" + DateTime.UtcNow + "' as datetime),'" + splogin + "')");

                            DataHelper.ExecuteDataTable(inserSQl.ToString());
                        }
                    }
                }
                
                result = new GeneratedFileInfoV2
                {
                    InsertedID = InsertedID,
                    InsertedURL = InsertedURL,
                    IsSuccess = IsSuccess,
                    Message = Message,
                    FileName = fileName
                };

                return result; //memoryStream.ToArray()
                #endregion
            }
           
        }

        private Cell ConstructCell(string value, CellValues dataType)
        {
            return new Cell()
            {
                CellValue = new CellValue(value),
                DataType = new EnumValue<CellValues>(dataType)
            };
        }    
    }
}
