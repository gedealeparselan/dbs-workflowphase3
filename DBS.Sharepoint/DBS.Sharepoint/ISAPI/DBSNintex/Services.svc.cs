﻿using DBS.Sharepoint.Helpers;
using Microsoft.SharePoint;
using Nintex.Workflow.HumanApproval;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading.Tasks;

namespace DBS.Sharepoint
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class Services : IServices
    {
        public Test HelloWorld()
        {
            return new Test { LocalName = "dsada", RemoteName = "asdas", Size = 41 };
        }

        public string GetCurrentUser()
        {
            using (SPSite site = new SPSite(SPContext.Current.Site.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    return web.CurrentUser.Name;
                }
            }
        }

        public TaskApproval TestTask(TaskApproval approval)
        {
            return approval;
        }


        public Result RespondApprovalTask(Task task)
        {
            Result result;

            using (SPSite site = new SPSite(SPContext.Current.Site.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;

                    NintexWF nintex = new NintexWF();
                    string message = string.Empty;

                    if (nintex.RespondApprovalTask(web, task.TaskListID, task.TaskListItemID, task.OutcomeID, task.Comment, ref message))
                    {
                        result = new Result { IsSuccess = true, Message = "Approval task has been proccessed" };
                    }
                    else
                    {
                        result = new Result { IsSuccess = false, Message = message };
                    }

                    nintex = null;
                    web.AllowUnsafeUpdates = false;
                }
            }

            return result;
        }

        public Result RespondReviewTask(Task task)
        {
            Result result;

            using (SPSite site = new SPSite(SPContext.Current.Site.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;

                    NintexWF nintex = new NintexWF();
                    string message = string.Empty;

                    if (nintex.RespondReviewTask(web, task.TaskListID, task.TaskListItemID, task.Comment, ref message))
                    {
                        result = new Result { IsSuccess = true, Message = "Review task has been proccessed" };
                    }
                    else
                    {
                        result = new Result { IsSuccess = false, Message = message };
                    }

                    nintex = null;
                    web.AllowUnsafeUpdates = false;
                }
            }

            //result = new Result { IsSuccess = false, Message = string.Format("ID: {0}, Name:{1}, Comment:{2}", review.TaskID, review.TaskName, review.Comment) };

            return result;
        }

        public Result RespondFlexiTask(Task task)
        {
            Result result;

            using (SPSite site = new SPSite(SPContext.Current.Site.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    web.AllowUnsafeUpdates = true;

                    NintexWF nintex = new NintexWF();
                    string message = string.Empty;


                    if (nintex.RespondFlexiTask(web, task.TaskListID, task.TaskListItemID, task.OutcomeID, task.Comment, ref message))
                    {
                        result = new Result { IsSuccess = true, Message = "Flexi task has been proccessed" };
                    }
                    else
                    {
                        result = new Result { IsSuccess = false, Message = message };
                    }

                    nintex = null;
                    web.AllowUnsafeUpdates = false;
                }
            }

            return result;
        }

        //18 agustus 2016
        public Result RespondFlexiTaskService(Task task)
        {
            Result result = new Result { IsSuccess = false, Message = "" };
            string sWebUrl = System.Configuration.ConfigurationManager.AppSettings.Get("WebUrl"); //"http://mysweetlife:90"
            string sServiceApprover = System.Configuration.ConfigurationManager.AppSettings.Get("ServiceApprover"); //"i:0#.f|dbsmembership|ppuchecker2"

            SPSecurity.RunWithElevatedPrivileges(delegate
            {
                using (SPSite site = new SPSite(sWebUrl))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        SPUserToken userToken = web.SiteUsers[sServiceApprover].UserToken;

                        using (SPSite siteToken = new SPSite(sWebUrl, userToken))
                        {
                            using (SPWeb webToken = siteToken.OpenWeb())
                            {
                                webToken.AllowUnsafeUpdates = true;

                                NintexWF nintex = new NintexWF();
                                string message = string.Empty;


                                if (nintex.RespondFlexiTask(webToken, task.TaskListID, task.TaskListItemID, task.OutcomeID, task.Comment, ref message))
                                {
                                    result = new Result { IsSuccess = true, Message = "Flexi task has been proccessed" };
                                }
                                else
                                {
                                    result = new Result { IsSuccess = false, Message = message };
                                }

                                nintex = null;

                                webToken.AllowUnsafeUpdates = false;
                            }
                        }
                    }
                }
            });

            return result;
        }
        //
        public Result RespondFlexiTaskByUsername(Task task)
        {
            Result result = new Result { IsSuccess = false, Message = "" };
            string sWebUrl = System.Configuration.ConfigurationManager.AppSettings.Get("WebUrl");
            string sServiceApprover = task.Username;

            SPSecurity.RunWithElevatedPrivileges(delegate
            {
                using (SPSite site = new SPSite(sWebUrl))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        SPUserToken userToken = web.SiteUsers[sServiceApprover].UserToken;

                        using (SPSite siteToken = new SPSite(sWebUrl, userToken))
                        {
                            using (SPWeb webToken = siteToken.OpenWeb())
                            {
                                webToken.AllowUnsafeUpdates = true;

                                NintexWF nintex = new NintexWF();
                                string message = string.Empty;


                                if (nintex.RespondFlexiTask(webToken, task.TaskListID, task.TaskListItemID, task.OutcomeID, task.Comment, ref message))
                                {
                                    result = new Result { IsSuccess = true, Message = "Flexi task has been proccessed" };
                                }
                                else
                                {
                                    result = new Result { IsSuccess = false, Message = message };
                                }

                                nintex = null;

                                webToken.AllowUnsafeUpdates = false;
                            }
                        }
                    }
                }
            });

            return result;
        }

        public ResultOutcome FlexiTaskOutcomes(Task task)
        {
            ResultOutcome result;

            using (SPSite site = new SPSite(SPContext.Current.Site.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    ConfiguredOutcomeCollection configuredOutcomes = new ConfiguredOutcomeCollection();
                    NintexWF nintex = new NintexWF();
                    string message = string.Empty;

                    if (nintex.GetOutcomeFlexiTask(web, task.TaskListID, task.TaskListItemID, ref configuredOutcomes, ref message))
                    {
                        IList<OutcomeTask> outcomes = new List<OutcomeTask>();

                        if (configuredOutcomes.Any())
                        {
                            foreach (var item in configuredOutcomes)
                            {
                                outcomes.Add(new OutcomeTask
                                {
                                    ID = item.Id,
                                    Name = item.Name,
                                    Description = item.Description
                                });
                            }
                        }

                        result = new ResultOutcome
                        {
                            IsSuccess = true,
                            Message = "Get outcomes data success.",
                            Outcomes = outcomes
                        };
                    }
                    else
                    {
                        result = new ResultOutcome
                        {
                            IsSuccess = false,
                            Message = message
                        };
                    }

                    configuredOutcomes = null;
                    nintex = null;
                }
            }

            return result;
        }


        public ResultOutcome GetTaskOutcomes(Task task)
        {
            return _GetTaskOutcomes(task, null);
        }

        public ResultOutcome GetTaskOutcomesByListID(SPTask spTask)
        {
            return _GetTaskOutcomes(null, spTask);
        }

        private ResultOutcome _GetTaskOutcomes(Task task, SPTask spTask)
        {
            ResultOutcome result;

            using (SPSite site = new SPSite(SPContext.Current.Site.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    ConfiguredOutcomeCollection configuredOutcomes = new ConfiguredOutcomeCollection();
                    NintexWF nintex = new NintexWF();

                    bool isAuthorized = false;
                    bool isPending = false;
                    string message = string.Empty;

                    bool process = false;
                    if (task != null)
                    {
                        process = nintex.GetTaskOutcomes(web, task.TaskListID, task.TaskListItemID, ref isAuthorized, ref isPending, ref configuredOutcomes, ref message);
                    }
                    else
                    {
                        process = nintex.GetTaskOutcomes(web, spTask.TaskListID, spTask.TaskListItemID, ref isAuthorized, ref isPending, ref configuredOutcomes, ref message);
                    }

                    if (process)
                    {
                        IList<OutcomeTask> outcomes = new List<OutcomeTask>();

                        if (configuredOutcomes.Any())
                        {
                            foreach (var item in configuredOutcomes)
                            {
                                outcomes.Add(new OutcomeTask
                                {
                                    ID = item.Id,
                                    Name = item.Name,
                                    Description = item.Description
                                });
                            }
                        }

                        result = new ResultOutcome
                        {
                            IsSuccess = true,
                            IsAuthorized = isAuthorized,
                            IsPending = isPending,
                            Message = message,
                            Outcomes = outcomes
                        };
                    }
                    else
                    {
                        result = new ResultOutcome
                        {
                            IsSuccess = false,
                            Message = message
                        };
                    }

                    configuredOutcomes = null;
                    nintex = null;
                }
            }

            return result;
        }
    }
}
