﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Taxonomy;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using Microsoft.SharePoint.Publishing.Navigation;

namespace AceNavigation.CONTROLTEMPLATES
{
    public partial class AceSPNavigation : UserControl
    {
        [Serializable]
        private class NavigationNodes
        {
            public string ID { get; set; }
            public string ParentID { get; set; }
            public string ParentTitle { get; set; }
            public string Title { get; set; }
            public string FriendlyUrl { get; set; }
            public string Url { get; set; }
            public string Icon { get; set; }
            public bool IsHide { get; set; }
        }
        private bool IsSuccess { get; set; }
        private string ErrorMessage { get; set; }
        private string CurrentFurl { get; set; }
        private IList<NavigationNodes> model = new List<NavigationNodes>();

        #region Properties
        public string TermStore { get; set; }
        public string TermGroup { get; set; }
        public string TermSet { get; set; }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            using (SPSite site = new SPSite(SPContext.Current.Site.Url))
            {
                using (SPWeb web = site.RootWeb)
                {
                    using (SPWeb webCCU = site.OpenWeb("CCU"))
                    {
                        ReadTaxonomy(site, web, webCCU);
                    }
                }
            }
        }

        private void ReadTaxonomy(SPSite site, SPWeb web, SPWeb webCCU)
        {
            TaxonomySession taxonomy;
            TermCollection terms;

            try
            {
                taxonomy = new TaxonomySession(site);
                terms = taxonomy.TermStores[TermStore].Groups[TermGroup].TermSets[TermSet].GetAllTerms();
                
                // get current term
                TaxonomyNavigationContext context = TaxonomyNavigationContext.Current;
                CurrentFurl = context.NavigationTerm != null ? context.NavigationTerm.GetWebRelativeFriendlyUrl() : string.Empty;

                SPListItemCollection collsPages = RetrievePages(web);
                SPListItemCollection collsPagesCCU = RetrievePages(webCCU);

                foreach (Term term in terms)
                {
                    if (term.IsDeprecated) return;

                    NavigationTerm navTerm = NavigationTerm.GetAsResolvedByWeb(term, web, TermSet);

                    var pages = from SPListItem item in collsPages
                                      where "/" + item.Url.ToString().ToLower() == navTerm.TargetUrl.Value.ToLower()
                                      select item;
                    if (pages != null && pages.Count() > 0)
                    {
                        model.Add(new NavigationNodes()
                        {
                            ID = term.Id.ToString(),
                            ParentID = term.Parent != null ? term.Parent.Id.ToString() : "",
                            ParentTitle = term.Parent != null ? term.Parent.Name.ToString() : "",
                            Title = term.Name,
                            Url = navTerm.TargetUrl.Value,
                            FriendlyUrl = navTerm.GetWebRelativeFriendlyUrl(),
                            Icon = term.CustomProperties.ContainsKey("Icon") ? term.CustomProperties["Icon"] : "",
                            IsHide = term.CustomProperties.ContainsKey("IsHide") ? Convert.ToBoolean(term.CustomProperties["IsHide"]) : false
                        });   
                    }

                    var pagesCCU = from SPListItem item in collsPagesCCU
                                   where "/ccu/" + item.Url.ToString().ToLower() == navTerm.TargetUrl.Value.ToLower()
                                   select item;
                    if (pagesCCU != null && pagesCCU.Count() > 0)
                    {
                        model.Add(new NavigationNodes()
                        {
                            ID = term.Id.ToString(),
                            ParentID = term.Parent != null ? term.Parent.Id.ToString() : "",
                            ParentTitle = term.Parent != null ? term.Parent.Name.ToString() : "",
                            Title = term.Name.Contains("CCU") ? term.Name.Replace("CCU", "").Trim() : term.Name,
                            Url = navTerm.TargetUrl.Value,
                            FriendlyUrl = navTerm.GetWebRelativeFriendlyUrl(),
                            Icon = term.CustomProperties.ContainsKey("Icon") ? term.CustomProperties["Icon"] : "",
                            IsHide = term.CustomProperties.ContainsKey("IsHide") ? Convert.ToBoolean(term.CustomProperties["IsHide"]) : false
                        });
                    }
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                ErrorMessage = ex.Message + "<br/>" + ex.InnerException;
            }
            finally
            {
                taxonomy = null;
                terms = null;
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (IsSuccess)
            {
                // get current url
                Uri uri = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
                string currentPage = uri.LocalPath;

                // get parents from active child
                //var parent = model.Where(a => a.Url.ToLower().Equals(currentPage.ToLower())).FirstOrDefault();
                var parent = model.Where(a => a.FriendlyUrl.ToLower().Equals(CurrentFurl.ToLower())).FirstOrDefault(); 
                IList<string> parentIDs = new List<string>();
                if (parent != null)
                {
                    parentIDs.Add(parent.ParentID);

                    string parentID = parent.ParentID;
                    while (!string.IsNullOrEmpty(parentID))
                    {
                        var x = model.Where(a => a.ID.Equals(parentID)).SingleOrDefault();
                        parentID = x.ParentID;

                        if (!string.IsNullOrEmpty(parent.ParentID))
                        {
                            parentIDs.Add(x.ID);
                        }
                    }
                }

                writer.WriteLine("<ul class='nav nav-list'>");

                // get root menu
                var rootNodes = model.Where(a => a.ParentTitle.Equals(""));
                foreach (NavigationNodes item in rootNodes)
                {
                    string lists = BuildLists(item, parentIDs, currentPage, 1);
                    writer.WriteLine(lists);
                }

                writer.WriteLine("</ul>");
            }
            else
            {
                writer.WriteLine("<i class='icon-bolt'/></i>");
                writer.WriteLine(ErrorMessage);
            }
        }

        private string BuildLists(NavigationNodes node, IList<string> parentIDs, string currentPage, int level)
        {
            // Is current page?
            bool isCurrentPage = string.Equals(node.Url.ToLower(), currentPage.ToLower());
            bool isCurrentParentPage = parentIDs.Any() ? parentIDs.Contains(node.ID) : false;

            StringBuilder sb = new StringBuilder();

            var childs = model.Where(a => a.ParentID.Equals(node.ID) && a.IsHide == false);
            if (childs.Any())
            {
                sb.AppendLine("<li" + (isCurrentParentPage ? " class='active open'" : (isCurrentPage ? " class='active'" : "")) + ">");
                if (level == 1)
                {
                    sb.AppendLine("<a class='dropdown-toggle' href='" + node.FriendlyUrl + "'>");
                    sb.AppendLine("<i class='" + node.Icon + "'/></i>");
                    sb.AppendLine("<span class='menu-text'>" + node.Title + "</span>");
                    sb.AppendLine("<b class='arrow icon-angle-down'/></b>");
                    sb.AppendLine("</a>");
                }
                else
                {
                    sb.AppendLine("<a class='dropdown-toggle' href='" + node.FriendlyUrl + "'>");
                    //sb.AppendLine("<i class='" + (level == 2 ? "icon-double-angle-right" : "icon-eye-open") + "'/></i>");
                    sb.AppendLine("<i class='" + (level == 2 ? "icon-double-angle-right" : "") + "'/></i>");
                    sb.AppendLine(node.Title);
                    sb.AppendLine("<b class='arrow icon-angle-down'/></b>");
                    sb.AppendLine("</a>");
                }

                // Generate submenu
                sb.AppendLine("<ul class='submenu'>");
                foreach (var item in childs)
                {
                    string lists = BuildLists(item, parentIDs, currentPage, level + 1);
                    sb.AppendLine(lists);
                }
                sb.AppendLine("</ul>");

                sb.AppendLine("</li>");
            }
            else
            {
                sb.AppendLine("<li" + (isCurrentPage == true || isCurrentParentPage == true ? " class='active'" : "") + ">");
                sb.AppendLine("<a href='" + (isCurrentPage ? "#" : node.FriendlyUrl) + "'>");
                if (level == 1)
                {
                    sb.AppendLine("<i class='" + node.Icon + "'/></i>");
                    sb.AppendLine("<span class='menu-text'>" + node.Title + "</span>");
                }
                else
                {
                    sb.AppendLine("<i class='" + (level == 2 ? "icon-double-angle-right" : node.Icon) + "'/></i>");
                    sb.AppendLine(node.Title);
                }
                sb.AppendLine("</a>");
                sb.AppendLine("</li>");
            }

            return sb.ToString();
        }

        private SPListItemCollection RetrievePages(SPWeb web)
        {
            SPListItemCollection colls = null;

            string sQuery = string.Format(@"<OrderBy><FieldRef Name='FileRef' Ascending='True' /></OrderBy>");
            colls = RetrieveItems(web, "Pages", null, sQuery, "FlatViews");

            return colls;
        }

        private SPListItemCollection RetrieveItems(SPWeb web, string sList, string sFolder, string sQuery, string sViewName)
        {
            try
            {
                SPListItemCollection itemColl = null;
                SPList list = web.Lists[sList];

                SPQuery query = sViewName == null ? new SPQuery() : new SPQuery(web.Lists[sList].Views[sViewName]);
                query.Query = sQuery;
                query.RowLimit = 2000;

                if (sFolder != null)
                {
                    SPFolder folder = list.RootFolder.SubFolders[sFolder];
                    sFolder = "/";
                    query.Folder = folder;
                }

                itemColl = list.GetItems(query);

                return itemColl;
            }
            catch (Exception x)
            {
                throw x;
            }
        }
    }
}
