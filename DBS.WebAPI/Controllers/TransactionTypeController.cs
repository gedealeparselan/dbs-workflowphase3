﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class TransactionTypeController : ApiController
    {
        private string message = string.Empty;

        private readonly ITransactionTypeRepository repository = null;

        public TransactionTypeController()
        {
            this.repository = new TransactionTypeRepository();
        }

        public TransactionTypeController(ITransactionTypeRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all currencies.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<TransactionTypeModel>))]
        public HttpResponseMessage Get()
        {
            IList<TransactionTypeModel> output = new List<TransactionTypeModel>();
            if (!repository.GetTransactionType(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TransactionTypeModel>>(HttpStatusCode.OK, output);
            }
        }
        /// <summary>
        /// Get currencies with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        /// 
        [ResponseType(typeof(IList<TransactionTypeModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<TransactionTypeModel> output = new List<TransactionTypeModel>();
            if (!repository.GetTransactionType(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<TransactionTypeModel>>(HttpStatusCode.OK, output);
            }
        }
        /// <summary>
        /// Get currencies for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionTypeGrid))]
        [AcceptVerbs("GET", "POST")]

        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<TransactionTypeFilter> filters)
        {
            TransactionTypeGrid output = new TransactionTypeGrid();

            if (!repository.GetTransactionType(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<TransactionTypeGrid>(HttpStatusCode.OK, output);

            }
        }
        /// <summary>
        /// Find currencies using criterias Code or Description.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>

        [ResponseType(typeof(IList<TransactionTypeModel>))]
        [HttpGet]
        [Route("api/TransactionType/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<TransactionTypeModel> output = new List<TransactionTypeModel>();

            if (!repository.GetTransactionType(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TransactionTypeModel>>(HttpStatusCode.OK, output);
            }
        }
        /// <summary>
        /// Get BeneSegment by ID.
        /// </summary>
        /// <param name="id">BeneSegment ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionTypeModel))]
        [Route("api/TransactionType/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            TransactionTypeModel output = new TransactionTypeModel();
            if (!repository.GetTransactionTypeByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<TransactionTypeModel>(HttpStatusCode.OK, output);
            }
        }
        /// <summary>
        /// Create a new BizSegment.
        /// </summary>
        /// <param name="Bank">Cumrrency data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]

        public HttpResponseMessage Post(TransactionTypeModel transactiontype)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionType(transactiontype, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Transaction type Data Has Been Created." });
                }

            }
        }
        /// <summary>
        /// Modify existing BizSegment.
        /// </summary>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/TransactionType/{id}")]

        public HttpResponseMessage put(int id, TransactionTypeModel transactiontype)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTransactionType(id, transactiontype, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "TransactionType data has been updated" });
                }
            }
        }
        /// <summary>
        /// Remove exisiting Biz Segment.
        /// </summary>
        /// <param name="id">ID of Biz Segment to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/TransactionType/{id}")]
        [ResponseType(typeof(string))]

        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteTransactionType(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "TransactionType Data Has Been Deleted" });
                }
            }
        }
        [ResponseType(typeof(TransactionTypeParameterModel))]
        [Route("api/TransactionType/Product/{id}")]
        [HttpGet]
        public HttpResponseMessage GetByProductID(int id)
        {
            IList<TransactionTypeParameterModel> outputRet = new List<TransactionTypeParameterModel>();
            IDictionary<string, object> output = new Dictionary<string, object>();

            if (!repository.GetTransactionTypeByProductID(id, ref outputRet, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                output.Add("TransactionType", outputRet);
            }
            if (output == null)
                return Request.CreateResponse(HttpStatusCode.NoContent);
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}


