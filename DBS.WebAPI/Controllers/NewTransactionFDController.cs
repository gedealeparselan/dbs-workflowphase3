﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class NewTransactionFDController : ApiController
    {
        private string message = string.Empty;
        private readonly INewTransactionFDRepository repository = null;
        public NewTransactionFDController()
        {
            this.repository = new NewTransactionFDRepository();
        }

        public NewTransactionFDController(INewTransactionFDRepository repository)
        {
            this.repository = repository;
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/NewTransactionFD")]
        public HttpResponseMessage AddNewTransactionFD(TransactionFDDetailModel transaction)
        {
            long transactionID = 0;
            string ApplicationID = string.Empty;
            string transactionType = string.Empty;
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddNewTransactionFD(transaction, ref  transactionID, ref  ApplicationID, ref transactionType, ref  message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new
                    {
                        ID = transactionID,
                        AppID = ApplicationID,
                        TransType = transactionType,
                        Message = transaction.IsDraft ? "Transaction has been saved as draft." : "New Transaction has been submitted."
                    });
                }
            }
        }

        [ResponseType(typeof(TransactionFDDraftModel))]
        [Route("api/NewTransactionFD/Draft/{id}")]
        [HttpGet]
        public HttpResponseMessage GetDraft(Int64 id)
        {
            TransactionFDDraftModel output = new TransactionFDDraftModel();

            if (!repository.GetTransactionDraftFDByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<TransactionFDDraftModel>(HttpStatusCode.OK, output);
            }
        }
        [HttpDelete]
        [Route("api/NewTransactionFD/Draft/Delete/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage DeleteDraft(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteTransactionDraftByID(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Draft." });
                }
            }
        }

        [HttpGet]
        [Route("api/GetIquoteFD")]
        [ResponseType(typeof(IList<IquoteModel>))]
        public HttpResponseMessage GetIqouteFD()
        {
            IList<IquoteModel> quote = new List<IquoteModel>();
            if (!repository.GetIQuoteData(ref quote, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (quote == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<IquoteModel>>(HttpStatusCode.OK, quote);
            }
        }
    }
}
