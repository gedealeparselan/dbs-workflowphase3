﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class RoleController : ApiController
    {
        private string message = string.Empty;

        private readonly IRoleRepository repository = null;

        public RoleController()
        {
            this.repository = new RoleRepository();
        }

        public RoleController(IRoleRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all Roles.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<RoleModel>))]
        public HttpResponseMessage Get()
        {
            IList<RoleModel> output = new List<RoleModel>();

            if (!repository.GetRole(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<RoleModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Roles with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<RoleModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<RoleModel> output = new List<RoleModel>();

            if (!repository.GetRole(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<RoleModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Roles for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(RoleGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<RoleFilter> filters)
        {
            RoleGrid output = new RoleGrid();

            if (!repository.GetRole(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<RoleGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find Roles using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<RoleModel>))]
        [HttpGet]
        [Route("api/Role/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<RoleModel> output = new List<RoleModel>();

            if (!repository.GetRole(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<RoleModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Role by ID.
        /// </summary>
        /// <param name="id">Role ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(RoleModel))]
        [Route("api/Role/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            RoleModel output = new RoleModel();

            if (!repository.GetRoleByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<RoleModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new Role.
        /// </summary>
        /// <param name="Role">Role data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(RoleModel role)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddRole(role, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Role data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing Role.
        /// </summary>
        /// <param name="id">ID of Role to be modify.</param>
        /// <param name="Role">Role data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/Role/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, RoleModel role)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateRole(id, role, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Role data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting Role.
        /// </summary>
        /// <param name="id">ID of Role to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/Role/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteRole(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Role data has been deleted." });
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}