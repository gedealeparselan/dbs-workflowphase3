﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    [Route("api/Customer/{cif}/Account")]
    public class CustomerAccountController : ApiController
    {
        private string message = string.Empty;

        private readonly ICustomerAccountRepository repository = null;
        
        public CustomerAccountController()
        {
            this.repository = new CustomerAccountRepository();
        }

        public CustomerAccountController(ICustomerAccountRepository repository)
        {
            this.repository = repository;
        } 
        
        /// <summary>
        /// Get all Customer Accounts.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<CustomerAccountModel>))]
        public HttpResponseMessage Get(string cif)
        {
            IList<CustomerAccountModel> output = new List<CustomerAccountModel>();

           // if (!repository.GetCustomerAccount(cif, ref output, ref message))
            if (!repository.GetCustomerAccountMapping(cif, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.NoContent, new { Message = message });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Customer Account for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(CustomerAccountGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(string cif,int? page, int? size, string sort_column, string sort_order, IList<CustomerAccountFilter> filters)
        {
            CustomerAccountGrid output = new CustomerAccountGrid();

           // if (!repository.GetCustomerAccount(cif,page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            if (!repository.GetCustomerAccountMapping(cif, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerAccountGrid>(HttpStatusCode.OK, output);
            }
        }
        
        /// <summary>
        /// Get customer account by account number.
        /// </summary>
        /// <param name="cif">CIF number.</param>
        /// <param name="accountNumber">Account number.</param>
        /// <returns></returns>
        [ResponseType(typeof(CustomerAccountModel))]
        [Route("api/Customer/{cif}/Account/{accountNumber}")]
        [HttpGet]
        public HttpResponseMessage Get(string cif, string accountNumber)
        {
            CustomerAccountModel output = new CustomerAccountModel();

            if (!repository.GetCustomerAccountByID(cif, accountNumber, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent, new { Message = "Customer account not found." });
                else
                    return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new customer account.
        /// </summary>
        /// <param name="cif">CIF number.</param>
        /// <param name="customerAccount">Customer account data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "CBO")]
        [Route("api/CustomerAccount/{cif}")]
        public HttpResponseMessage Post(string cif, CustomerAccountModel customerAccount)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddCustomerAccount(cif, customerAccount, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New customer account data has been created." });
                }
            }
        }
       
        /// <summary>
        /// Modify existing customer account.
        /// </summary>
        /// <param name="cif">CIF number to be modify.</param>
        /// <param name="accountNumber">Account number.</param>
        /// <param name="customerAccount">Customer data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/CustomerAccount/{cif}")]
        //[Authorize(Roles = "CBO")]
        public HttpResponseMessage Put(string cif,CustomerAccountModel customerAccount)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateCustomerAccount(cif, customerAccount, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Customer account data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting customer account.
        /// </summary>
        /// <param name="cif">CIF number to be deleted.</param>
        /// <param name="accountNumber">Account number.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/CustomerAccount/{cif}/Account/{accountNumber}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "CBO")]
        public HttpResponseMessage Delete(string cif, string accountNumber)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteCustomerAccount(cif, accountNumber, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Customer data has been deleted." });
                }
            }
        }
        /// <summary>
        /// Get customer account by account number.
        /// </summary>
        /// <param name="cif">CIF number.</param>
        /// <returns></returns>
        [ResponseType(typeof(CustomerAccountModel))]
        [Route("api/CustomerAccount/{cif}")]
        [HttpGet]
        public HttpResponseMessage GetAccount(string cif)
        {
            List<CustomerAccountModel> output = new List<CustomerAccountModel>();

            if (!repository.GetCustomerAccountByCIF(cif, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent, new { Message = "Customer account not found." });
                else
                    return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }

}