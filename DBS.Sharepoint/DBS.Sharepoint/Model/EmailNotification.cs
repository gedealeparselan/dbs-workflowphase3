﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System.ServiceModel.Activation;

namespace DBS.Sharepoint.Model
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EmailNotification" in both code and config file together.
    public sealed class EmailNotification : IEmailNotification
    {
        /// <summary>
        /// This method is used to test the service whether it is been deployed properly or not.  This will not do anything functionally apart from the Logging.
        /// </summary>
        /// <param name="SampleValue"></param>
        /// <returns></returns>
        public string GetEmailNotification(EmailProperty properties)
        {
            bool isSend = false;

            string emailTo = string.Empty;

            try
            {
                using (SPSite site = new SPSite(SPContext.Current.Site.Url))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        //get user based on customer location
                        SPGroup groups = web.SiteGroups[properties.UserTo];
                        if (groups != null)
                        {
                            //collect each member email from group
                            SPUserCollection users = groups.Users;
                            foreach (SPUser user in users)
                            {
                                if (!string.IsNullOrEmpty(user.Email))
                                {
                                    emailTo = emailTo + string.Format("{0}; ", user.Email);
                                }
                            }

                            //send email using SP outgoing email
                            isSend = SPUtility.SendEmail(web, true, false, 
                                emailTo, properties.Subject, properties.EmailBody);
                        }
                    }
                }
            }
            catch
            {
                // do nothing
            }

            return isSend.ToString();
        }
    }
}
