﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;

using Nintex.Workflow;
using Nintex.Workflow.Common;
using Nintex.Workflow.HumanApproval;

namespace DBS.Sharepoint.Helpers
{
    public class NintexHelper
    {
        public static NintexContext ParseRequest(SPWeb web, int taskId, Guid taskListId)
        {
            NintexContext context = new NintexContext();
            context.Web = web;
            context.CurrentUser = web.CurrentUser;
            SPListCollection lists = context.Web.Lists;
            lists.ListsForCurrentUser = true;

            try
            {
                context.TaskList = lists.GetList(taskListId, false);
                context.TaskItem = context.TaskList.GetItemById(taskId);
            }
            catch (SPException)
            {
                throw new SPException(string.Format("Cannot find a task list with id {0}.", taskListId));
            }
            catch (ArgumentException)
            {
                throw new SPException(string.Format("Cannot find task with id {0}.", taskListId));
            }

            // Retrieve the Nintex Workflow object model representation of the task
            context.Task = NintexTask.RetrieveTask(taskId, context.Web, context.TaskList);

            // Retrieve the Nintex Workflow object model representation of the approver for this SharePoint task item id.
            // A Nintex Workflow task can have multiple approvers, and each approver will have a separated SharePoint task item.
            context.Approver = context.Task.Approvers.GetBySPId(taskId);

            if (context.Approver == null)
            {
                throw new SPException(string.Format("Cannot find an approver for SharePoint task item {0}.", taskId));
            }

            NWListWorkflowContext listWorkflowContext = context.Task.WFContext as NWListWorkflowContext;

            context.WorkflowListId = listWorkflowContext.ListID;
            SPList list = context.Web.Lists[listWorkflowContext.ListID];

            try
            {
                context.WorkflowItemId = listWorkflowContext.ItemID;
                context.Item = list.GetItemById(listWorkflowContext.ItemID);
            }
            catch
            {
                // do not error the page just because we cannot get hold of the item
            }
            return context;
        }
        /// <summary>
        /// Complete the current task
        /// </summary>
        /// <param name="CurrentContext"></param>
        public static void CompleteTask(NintexContext context)
        {
            SetTaskStatus(context, Outcome.Approved);
        }
        /// <summary>
        /// Complete the current task
        /// </summary>
        /// <param name="CurrentContext"></param>
        public static void RejectTask(NintexContext context)
        {
            SetTaskStatus(context, Outcome.Rejected);
        }
        /// <summary>
        /// Complete the current task
        /// </summary>
        /// <param name="CurrentContext"></param>
        private static void SetTaskStatus(NintexContext context, Outcome outcome)
        {
            if (!AttemptLockTask(context))
                return;
            try
            {
                context.TaskItem[Nintex.Workflow.Common.NWSharePointObjects.FieldDecision] = (int)outcome;
                context.TaskItem.Update();
            }
            catch
            {
                //Unlock
                context.Approver.UpdateTaskLocked(false);
                throw;
            }
        }
        /// <summary>
        /// Try to lock a task
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool AttemptLockTask(NintexContext context)
        {
            if (TaskLocker.AttemptTaskLock(context.Approver, context.Item, context.WorkflowListId, context.WorkflowItemId))
            {
                return true;
            }
            else
            {
                throw new SPException("This task is locked by another user. Please try again.");
            }
        }
    }
}
