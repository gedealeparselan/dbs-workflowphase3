﻿var accessToken;
var $box;
var $remove = false;
var startDate = ''; var strStartDate = '';
var endDate = ''; var strEndDate = '';
var isDailyChecked = false;


var ddlReportPeriodModel = function (id, name) {

    var self = this;

    self.ID = id;

    self.Name = name;

}

var ViewModel = function () {

    var self = this;

    self.ddlTransactionType = ko.observableArray();

    self.ddlReportPeriod = [

		new ddlReportPeriodModel(1, "Daily"),
		new ddlReportPeriodModel(2, "Weekly"),
		new ddlReportPeriodModel(3, "Monthly")

    ];

    self.ddlSegmentation = ko.observableArray([]);

    self.ddlChannel = ko.observableArray([]);

    self.ddlBranch = ko.observableArray([]);

    self.GetParameterToDropDown = function () {

        getParameterToDropDown();


    };

    function getParameterToDropDown() {

        var url = api.server + api.url.parameter + '?select=Product,Bizsegment,Channel,Branch';

        $.ajax({
            async: false,
            type: "GET",
            url: url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (dataValue, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    var url = api.server + api.url.misreportingparam + '?select=TransactionType,Segmentation,Channel,Branch';

                    $.ajax({
                        type: "GET",
                        url: url,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (dataCount, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {

                                var objLength = 0;

                                var dropDownAllElement = {
                                    ID: objLength + 1,
                                    Name: 'All',
                                    Description: 'All'
                                };


                                if (dataValue['Product'] != null) {

                                    if (dataCount['TransactionType'] != null) {
                                        objLength = dataCount['TransactionType'].length;
                                    }
                                    dropDownAllElement.ID = objLength + 1;
                                    dataValue['Product'].unshift(dropDownAllElement);
                                    self.ddlTransactionType(dataValue['Product']);

                                }
                                if (dataValue['BizSegment'] != null) {

                                    if (dataCount['Segmentation'] != null) {
                                        objLength = dataCount['Segmentation'].length;
                                    }
                                    dropDownAllElement.ID = objLength + 1;
                                    dataValue['BizSegment'].unshift(dropDownAllElement);
                                    self.ddlSegmentation(dataValue['BizSegment']);

                                }
                                if (dataValue['Channel'] != null) {

                                    if (dataCount['Channel'] != null) {
                                        objLength = dataCount['Channel'].length;
                                    }
                                    dropDownAllElement.ID = objLength + 1;
                                    dataValue['Channel'].unshift(dropDownAllElement);
                                    self.ddlChannel(dataValue['Channel']);

                                }
                                if (dataValue['Branch'] != null) {

                                    if (dataCount['Branch'] != null) {
                                        objLength = dataCount['Branch'].length;
                                    }
                                    dropDownAllElement.ID = objLength + 1;
                                    dataValue['Branch'].unshift(dropDownAllElement);
                                    self.ddlBranch(dataValue['Branch']);

                                }

                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });

                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }

        });

    }
};
$(document).ready(function () {

    var browser = navigator.userAgent.toLowerCase();
    var reportPeriodType;
    var onScrolling = 'mousewheel';
    var dateNowFromServer;

    $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    if ((browser.indexOf("chrome") > -1) || (browser.indexOf("safari") > -1)
       || (browser.indexOf("opera") > -1) || (browser.indexOf("msie") > -1)) {

        onScrolling = 'mousewheel';
        hideDatePicker(onScrolling);

    } else {

        onScrolling = 'DOMMouseScroll';
        hideDatePicker(onScrolling);

    }
    var GetObjectByName = function (hash, name) {
        result = null;
        $.each(hash, function (index, item) {
            if (item.name == name) result = item;
        });
        return result;
    }
    //KnockoutJS started
    var viewModel = new ViewModel();

    ko.applyBindings(viewModel);

    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(TokenOnSuccess, TokenOnError);
        viewModel.GetParameterToDropDown('transactions');
        viewModel.GetParameterToDropDown('segmentation');
        viewModel.GetParameterToDropDown('channel');
        viewModel.GetParameterToDropDown('branch');


    } else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // call spuser function
        GetCurrentUser(accessToken);

        // call get data inside view model  
        viewModel.GetParameterToDropDown('transactions');
        viewModel.GetParameterToDropDown('segmentation');
        viewModel.GetParameterToDropDown('channel');
        viewModel.GetParameterToDropDown('branch');

    }
    //KnockOutJS ended
    function hideDatePicker(Scrolling) {

        $(window).on(onScrolling, function () {

            $('.datepicker.datepicker-dropdown.dropdown-menu').hide();

        });

    }

    $('#chkDateNull').click(function () {
        if ($('#chkDateNull').is(':checked')) {
            $('#start-date').attr('disabled', 'disabled');
            $('#end-date').attr('disabled', 'disabled');
        } else {
            $('#start-date').removeAttr('disabled');
            $('#end-date').removeAttr('disabled');
        }

    });

    //hide stack value for 0 or 0.0 value
    function hideZeroStackValue() {

        $(".jqplot-point-label").each(
               function () {
                   var content = $(this).text();

                   //Get the length of the content;
                   //var contentLength = $.trim( content ).length;
                   if ((content == "0") || (content == "0.0")) {
                       $(this).hide();
                   }
               }
           );

    }
    function floatValue() {
        $(".jqplot-pie-series.jqplot-data-label").each(
           function () {

               var content = $(this).text();

               var arrContent = content.indexOf(".") > -1 ? content.split(".") : null;

               if (arrContent != null) {

                   content = arrContent[1];

                   if (content.length > 3) {

                       content = content.replace("%", "");

                       content = "0." + content.toString();

                       var flValue = parseFloat(arrContent[0]) + parseFloat(content);

                       flValue = flValue.toFixed(2);

                       $(this).text(flValue + "%");
                   }

               }


           }
       );



    }
    function convertMonth(date) {
        var vDate = [];
        var vDay; var vMonth; var vYear; var sMonth;
        vDate = date.split("-");
        vDay = vDate[0];
        vMonth = vDate[1];
        vYear = vDate[2];
        switch (vMonth) {
            case "Jan":
                sMonth = "01";
                break;
            case "Feb" || "Peb":
                sMonth = "02";
                break;
            case "Mar":
                sMonth = "03";
                break;
            case "Apr":
                sMonth = "04";
                break;
            case "May" || "Mei":
                sMonth = "05";
                break;
            case "Jun":
                sMonth = "06";
                break;
            case "Jul":
                sMonth = "07";
                break;
            case "Aug" || "Agu":
                sMonth = "08";
                break;
            case "Sep":
                sMonth = "09";
                break;
            case "Oct" || "Okt":
                sMonth = "10";
                break;
            case "Nov" || "Nop":
                sMonth = "11";
                break;
            case "Dec" || "Des":
                sMonth = "12";
                break;
        }
        return vYear + "-" + sMonth + "-" + vDay;
    }
    function dateValidation() {
        var messageValidationDate = '';
        var startDateVal;
        var endDateVal;

        startDateVal = convertMonth($("#start-date").val());
        endDateVal = convertMonth($("#end-date").val());

        var startDateValid = new Date(startDateVal);
        var endDateValid = new Date(endDateVal);

        if ((startDateVal != '') && (endDateVal != '') && (startDateValid > endDateValid)) {

            messageValidationDate += '* start date cannot be greater than end date';

            $('#btnViewMIS').attr('disabled', 'disabled');

            ShowNotification('Information', messageValidationDate, 'gritter-error', true);

        } else if ((startDateVal != '') && (endDateVal != '') && (startDateValid <= endDateValid)) {


            $('#btnViewMIS').removeAttr('disabled');

        }

    }
    $('#start-date').bind('change', function () {

        dateValidation();

    });

    $('#end-date').bind('change', function () {

        dateValidation();

    });

    function getDateFromServer() {
        var newDateTime = new Date();
        var newMonth; var newDate;
        var newHour; var newMinute; var newSecond;

        newMonth = newDateTime.getMonth() + 1;
        newMonth = newMonth.toString().length == 2 ? newMonth : "0" + newMonth;
        newDate = newDateTime.getDate();
        newDate = newDate.toString().length == 2 ? newDate : "0" + newDate;
        newHour = newDateTime.getHours();
        newHour = newHour.toString().length == 2 ? newHour : "0" + newHour;
        newMinute = newDateTime.getMinutes();
        newMinute = newMinute.toString().length == 2 ? newMinute : "0" + newMinute;
        newSecond = newDateTime.getSeconds();
        newSecond = newSecond.toString().length == 2 ? newSecond : "0" + newSecond;

        clientDateTime = newDateTime.getFullYear() + "-" + newMonth + "-" + newDate + "T" + newHour + ":" + newMinute + ":" + newSecond;
        //console.log(clientDateTime);
        var options = {

            url: api.server + api.url.currentdate,
            token: accessToken,
            params: {
                clientDateTime: clientDateTime
            }


        };


        Helper.Ajax.Get(options, OnSuccessGetDateNow, OnError, null);



    }
    function OnSuccessGetDateNow(data, textStatus, jqXHR) {

        if (jqXHR.status = 200) {


            renderDateNow(data);


        }

    }
    function renderDateNow(data) {
        var dateNow; var dateNowLast;
        if (data.length > 0) {
            for (var i = 0, max = data.length; i < max; i++) {
                if (i == 0) {
                    dateNow = data[i].clientDateTimeNow;
                    //console.log(dateNow);					
                    dateNowLast = dateNow.toString().indexOf("T") > -1 ? dateNow.substring(0, dateNow.toString().indexOf("T")) : (dateNow.toString().indexOf(" ") > -1 ? dateNow.substring(0, dateNow.toString().indexOf(" ")) : dateNow.substring(0, 10));
                    dateNowFromServer = dateNowLast;

                }
            }
        }


    }
    getDateFromServer();

    $('#btnViewMIS').bind('click', function () {
        var messageValidation = 'Please Choose(s) :';
        //var cRNL = String.fromCharCode(10) + String.fromCharCode(13);
        var CRNL = '<br/>';
        var transactionType = $("#form-field-transaction-type option:selected").val();
        var reportPeriod = $("#form-field-report-period option:selected").val();
        var segmentation = $("#form-field-segmentation option:selected").val();
        var channel = $("#form-field-channel option:selected").val();
        var branch = $("#form-field-branch option:selected").val();
        var monthCompStart; var dateCompStart;
        var monthCompEnd; var dateCompEnd;

        if (transactionType == '') messageValidation += CRNL + '* transaction type';
        if (reportPeriod == '') messageValidation += CRNL + '* report period';
        if (segmentation == '') messageValidation += CRNL + '* segmentation';
        if (channel == '') messageValidation += CRNL + '* channel (fax or original)';
        if (branch == '') messageValidation += CRNL + '* branch';
        if ($('#chkDateNull').is(':checked')) {

            if (reportPeriod == 1) {

                //console.log(dateNowFromServer);
                strStartDate = new Date(dateNowFromServer);

                dateCompStart = (strStartDate.getDate().toString().length == 2) ? (strStartDate.getDate()) : ("0" + strStartDate.getDate());

                monthCompStart = (strStartDate.getMonth().toString().length == 2) ? (strStartDate.getMonth() + 1) : ("0" + (strStartDate.getMonth() + 1));



                startDate = strStartDate.getFullYear() + "-" + monthCompStart + "-" + dateCompStart + "T00:00:00.000";

                strEndDate = new Date(dateNowFromServer);

                dateCompEnd = (strEndDate.getDate().toString().length == 2) ? (strEndDate.getDate()) : ("0" + strEndDate.getDate());

                monthCompEnd = (strEndDate.getMonth().toString().length == 2) ? (strEndDate.getMonth() + 1) : ("0" + (strEndDate.getMonth() + 1));

                endDate = strEndDate.getFullYear() + "-" + monthCompEnd + "-" + dateCompEnd + "T23:59:59.999";

                isDailyChecked = true;

            } else if (reportPeriod == 2) {

                var dateToday = new Date(dateNowFromServer);

                var dayNow = 0;

                dayNow = parseInt(dateToday.getDay(), 10);

                differ = dateToday.getDate() - dayNow;

                strStartDate = new Date(dateToday.setDate(differ));

                dateCompStart = (strStartDate.getDate().toString().length == 2) ? (strStartDate.getDate()) : ("0" + strStartDate.getDate());

                monthCompStart = (strStartDate.getMonth().toString().length == 2) ? (strStartDate.getMonth() + 1) : ("0" + (strStartDate.getMonth() + 1));

                startDate = strStartDate.getFullYear() + "-" + monthCompStart + "-" + dateCompStart + "T00:00:00.000";

                strEndDate = new Date(dateToday.setDate(differ + 6));

                dateCompEnd = (strEndDate.getDate().toString().length == 2) ? (strEndDate.getDate()) : ("0" + strEndDate.getDate());

                monthCompEnd = (strEndDate.getMonth().toString().length == 2) ? (strEndDate.getMonth() + 1) : ("0" + (strEndDate.getMonth() + 1));

                endDate = strEndDate.getFullYear() + "-" + monthCompEnd + "-" + dateCompEnd + "T23:59:59.999";

                isDailyChecked = false;

            } else if (reportPeriod == 3) {

                var dateToday = new Date(dateNowFromServer);

                var startDay = "1";

                var strCurrentMonth = []; var strCurrentYear = [];

                var currentMonth; var currentYear;

                var lastDay = "";

                strCurrentMonth = restructureDate(dateNowFromServer, false);

                strCurrentMonth = strCurrentMonth.toString().split("-");

                currentMonth = strCurrentMonth[1];

                currentYear = strCurrentMonth[0];

                lastDay = getLastDay(currentMonth, currentYear);

                strStartDate = new Date(dateNowFromServer);

                monthCompStart = (strStartDate.getMonth().toString().length == 2) ? (strStartDate.getMonth() + 1) : ("0" + (strStartDate.getMonth() + 1));

                startDate = strStartDate.getFullYear() + "-" + monthCompStart + "-" + startDay + "T00:00:00.000";

                strEndDate = new Date(dateNowFromServer);

                monthCompEnd = (strEndDate.getMonth().toString().length == 2) ? (strEndDate.getMonth() + 1) : ("0" + (strEndDate.getMonth() + 1));

                endDate = strEndDate.getFullYear() + "-" + monthCompEnd + "-" + lastDay + "T23:59:59.999";

                isDailyChecked = false;
            }


        } else {

            startDate = convertMonth($("#start-date").val()) + "T00:00:00.000";

            endDate = convertMonth($("#end-date").val()) + "T23:59:59.999";

            if (startDate == 'undefined-undefined-T00:00:00.000') messageValidation += CRNL + '* start date';

            if (endDate == 'undefined-undefined-T23:59:59.999') messageValidation += CRNL + '* end date';

            isDailyChecked = false;
        }


        //console.log(transactionType + " " + reportPeriod + " " + segmentation + " " + channel + " " + branch + " " +  startDate + " " + endDate);

        if (messageValidation != 'Please Choose(s) :') {

            hideElement();
            ShowNotification('Information', messageValidation, 'gritter-error', true);

        } else {
            renderBarChart(transactionType, segmentation, channel, branch, startDate, endDate, clientDateTime);
            renderPieChart(transactionType, segmentation, channel, branch, startDate, endDate, clientDateTime);
            renderTableSummary(transactionType, segmentation, channel, branch, startDate, endDate, clientDateTime);


        }


    });
    function DecodeParams(params) {
        if (typeof (params) != 'object') {
            alert("The parameter value is not an object!")
            return false;
        }

        var output = "";

        for (var key in params) {
            if (output != "") {
                output += "&";
            } else {
                output += "?";
            }

            output += key + "=" + params[key];
        }

        return output;
    }

    function clearTableTemporary(transactionType, segmentation, channel, branch, startDate, endDate, clientDateTime) {

        var options = {

            url: api.server + api.url.misreportingbarchart + '/' + 'ClearTableTemporary',
            params: {

                StartDate: startDate,
                EndDate: endDate,
            }


        };
        var Url = options.url;
        if (options.params != null || options.params != undefined) {
            Url += DecodeParams(options.params);
        }
        //console.log(Url);
        $.ajax({
            //async: false,
            type: "GET",
            url: Url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    renderBarChart(transactionType, segmentation, channel, branch, startDate, endDate, clientDateTime);
                    renderPieChart(transactionType, segmentation, channel, branch, startDate, endDate, clientDateTime);
                    renderTableSummary(transactionType, segmentation, channel, branch, startDate, endDate, clientDateTime);

                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification'               
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
            }

        });


    }

    function getLastDay(Month, Year) {
        var LastDay = "";

        if (Month.substring(0, 1) == "0")
            Month = Month.replace("0", "");


        switch (Month) {
            case "1":
                LastDay = "31";
                break;
            case "2":
                if ((parseInt(Year, 10) % 4) == 0 || (parseInt(Year, 10) % 100) != 0 || (parseInt(Year, 10) % 400) == 0)
                    LastDay = "29";
                else
                    LastDay = "28";
                break;
            case "3":
                LastDay = "31";
                break;
            case "4":
                LastDay = "30";
                break;
            case "5":
                LastDay = "31";
                break;
            case "6":
                LastDay = "30";
                break;
            case "7":
                LastDay = "31";
                break;
            case "8":
                LastDay = "31";
                break;
            case "9":
                LastDay = "30";
                break;
            case "10":
                LastDay = "31";
                break;
            case "11":
                LastDay = "30";
                break;
            case "12":
                LastDay = "31";
                break;

        }
        return LastDay;
    }
    function renderBarChart(transactionType, segmentation, channel, branch, startDate, endDate, clientDateTime) {

        var options = {

            url: api.server + api.url.misreportingbarchart,
            params: {
                TransactionType: transactionType,
                Segmentation: segmentation,
                Channel: channel,
                Branch: branch,
                StartDate: startDate,
                EndDate: endDate,
                ClientDateTime: clientDateTime

            },
            token: accessToken

        };
        /*console.log('url : ' + options.url);
		console.log('Transaction Type : ' + options.params.TransactionType);
		console.log('Segmentation : ' + options.params.Segmentation);
		console.log('Channel : ' + options.params.Channel);
		console.log('Branch : ' + options.params.Branch);
		console.log('Start Date : ' + options.params.StartDate);
		console.log('End Date : ' + options.params.EndDate);*/
        Helper.Ajax.Get(options, OnSuccessGetBarData, OnError, null);



    }

    function OnSuccessGetBarData(data, textStatus, jqXHR) {

        if (jqXHR.status = 200) {

            renderBarChartSuccess(data);


        }

    }

    function renderPieChart(transactionType, segmentation, channel, branch, startDate, endDate, clientDateTime) {

        var options = {

            url: api.server + api.url.misreportingpiechart,
            params: {
                TransactionType: transactionType,
                Segmentation: segmentation,
                Channel: channel,
                Branch: branch,
                StartDate: startDate,
                EndDate: endDate,
                ClientDateTime: clientDateTime

            },
            token: accessToken

        };
        /*console.log('url : ' + options.url);
		console.log('Transaction Type : ' + options.params.TransactionType);
		console.log('Segmentation : ' + options.params.Segmentation);
		console.log('Channel : ' + options.params.Channel);
		console.log('Branch : ' + options.params.Branch);
		console.log('Start Date : ' + options.params.StartDate);
		console.log('End Date : ' + options.params.EndDate);*/
        Helper.Ajax.Get(options, OnSuccessGetPieData, OnError, null);


    }

    function OnSuccessGetPieData(data, textStatus, jqXHR) {

        if (jqXHR.status = 200) {
            //console.log(data);
            renderPieChartSuccess(data);

        }

    }
    function renderTableSummary(transactionType, segmentation, channel, branch, startDate, endDate, clientDateTime) {

        var options = {

            url: api.server + api.url.misreportingtablesummary,
            params: {
                TransactionType: transactionType,
                Segmentation: segmentation,
                Channel: channel,
                Branch: branch,
                StartDate: startDate,
                EndDate: endDate,
                ClientDateTime: clientDateTime

            },
            token: accessToken

        };
        /*console.log('url : ' + options.url);
		console.log('Transaction Type : ' + options.params.TransactionType);
		console.log('Segmentation : ' + options.params.Segmentation);
		console.log('Channel : ' + options.params.Channel);
		console.log('Branch : ' + options.params.Branch);
		console.log('Start Date : ' + options.params.StartDate);
		console.log('End Date : ' + options.params.EndDate);*/
        Helper.Ajax.Get(options, OnSuccessGetTableSummary, OnError, null);


    }

    function OnSuccessGetTableSummary(data, textStatus, jqXHR) {

        if (jqXHR.status = 200) {
            //console.log(data);
            renderTableSummarySuccess(data);

        }

    }
    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
    function renderBarChartSuccess(data) {
        //console.log(data);
        // Data Modelling //
        var currentProduct = null;
        var arr = [];
        var rowHolder = null;
        var rowHolderLegend = null;
        var htlmLegend = null;
        var chartCount = 0;
        var chartsData = new Array();

        if (data.length > 0) {

            showElement();
            if ($("#chartholderdata").length > 0)
                $("#chartholderdata").remove();

            $('<div id="chartholderdata" style="height:400px;width:100%;"></div>').appendTo("#chartholder");
            holder = $("#chartholderdata").attr("id");

            for (var i = 0, max = data.length ; i < max; i++) {
                var product = data[i].productName;
                var hour = data[i].hour;
                var qtycompleted = data[i].qtyCompleted;
                var qtyonprogress = data[i].qtyOnProgress;
                var qtycancelled = data[i].qtyCancelled;
                var createdDate = data[i].createdDate;

                //console.log(data[i]);

                selChartsData = GetObjectByName(chartsData, product);
                if (selChartsData == null) {
                    selChartsData = {
                        "name": product, "hour": new Array(), "qtycompleted": new Array(), "qtyonprogress": new Array(), "qtycancelled": new Array()
                    };
                }


            }

            $.each(data, function (index, item) {

                selChartsData.hour.push(item.hour);
                selChartsData.qtycompleted.push(item.qtyCompleted);
                selChartsData.qtyonprogress.push(item.qtyOnProgress);
                selChartsData.qtycancelled.push(item.qtyCancelled);

            });

            chartsData.push(selChartsData);

            $.each(chartsData, function (index, item) {
                renderJqPlotBarChart(holder, item.qtycompleted, item.qtyonprogress, item.qtycancelled, item.hour)

            });

            renderPeriodLastUpdate(startDate, endDate, isDailyChecked);

            if ($('#legendholder').length > 0) {

                $('#legendholder').remove();

            }

            // Render View //
            rowHolder = null;
            rowHolderLegend = null;

            rowHolderLegend = $('<div id="legendholder"></div>');
            rowHolderLegend.appendTo("#legendplaceholder");

            htmlLegend = $("<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp<label>Legend :</label><br/><div class='col-xs-6 pull-left'><div style='height:150px;'><table class='jqplot-table-legend' style='right: 0px; top: 0px; left:0px;'>" +
							"<tbody><tr class='jqplot-table-legend'>" +
							"<td class='jqplot-table-legend jqplot-table-legend-swatch' style='text-align: center; padding-top: 0px;'>" +
							"<div class='jqplot-table-legend-swatch-outline'>" +
							"<div class='jqplot-table-legend-swatch' style='border-color: rgb(75, 178, 197); background-color: rgb(75, 178, 197);'></div>" +
							"</div></td><td class='jqplot-table-legend jqplot-table-legend-label'>Completed Transactions</td></tr>" +
							"<tr class='jqplot-table-legend'>" +
							"<td class='jqplot-table-legend jqplot-table-legend-swatch' style='text-align: center;'>" +
							"<div class='jqplot-table-legend-swatch-outline'>" +
							"<div class='jqplot-table-legend-swatch' style='border-color: rgb(234, 162, 40); background-color: rgb(234, 162, 40);'></div>" +
							"</div></td><td class='jqplot-table-legend jqplot-table-legend-label' style='padding-top: 0px;'>In Progress Transactions</td></tr>" +
							"<tr class='jqplot-table-legend'>" +
							"<td class='jqplot-table-legend jqplot-table-legend-swatch' style='text-align: center;'>" +
							"<div class='jqplot-table-legend-swatch-outline'>" +
							"<div class='jqplot-table-legend-swatch' style='border-color: rgb(197, 180, 127); background-color: rgb(197, 180, 127);'></div>" +
							"</div></td><td class='jqplot-table-legend jqplot-table-legend-label' style='padding-top: 0px;'>Cancelled Transactions</td></tr></tbody></table></div></div>");

            htmlLegend.appendTo("#legendholder");

        } else {

            ShowNotification('Information', 'Data not found', 'gritter-info', true);
            hideElement();
        }
    }


    function showElement() {

        $("#lblInfoPeriod").show();
        $("#chartholderdata").show();
        $("#separatorholder").show();
        $("#pieholderdata").show();
        $("#legendplaceholder").show();
        $("#widget-box").show();

    }
    function hideElement() {

        $("#lblInfoPeriod").hide();
        $("#chartholderdata").hide();
        $("#separatorholder").hide();
        $("#pieholderdata").hide();
        $("#legendplaceholder").hide();
        $("#widget-box").hide();

    }
    function renderPieChartSuccess(data) {
        // console.log(data);
        // Data Modelling //
        var currentProduct = null;
        var arr = [];
        var rowHolder = null;
        var rowHolderLegend = null;
        var htlmLegend = null;
        var chartCount = 0;
        var pieData = new Array(); var createdDate;
        var sla = 0; var exceptional = 0; var breach = 0; var cancelled = 0; var totalPieData;
        var totSLA; var totExceptional; var totBreach; var totCancelled;
        var trxID; var createdDate; var SLA;

        if (data.length > 0) {
            if ($("#pieholderdata").length > 0)
                $("#pieholderdata").remove();

            $('<div id="pieholderdata" style="height:370px;width:100%;"></div>').appendTo("#pieholder");

            holder = $("#pieholderdata").attr("id");


            for (var i = 0, max = data.length; i < max; i++) {
                breach = parseInt(breach, 10) + parseInt(data[i].breachTransactions, 10);
                sla = parseInt(sla, 10) + parseInt(data[i].slaTransactions, 10);
                exceptional = parseInt(exceptional, 10) + parseInt(data[i].exceptionalTransactions, 10);
                cancelled = parseInt(cancelled, 10) + parseInt(data[i].cancelledTransactions, 10);
                createdDate = data[i].createdDate;
                //console.log(createdDate + " " + sla + " " + exceptional);
            }
            //console.log(breach + " " + sla + " " + exceptional);    	
            totalPieData = parseInt(breach, 10) + parseInt(sla, 10) + parseInt(exceptional, 10) + parseInt(cancelled, 10);

            if (totalPieData != 0) {

                breach = (breach / totalPieData) * 100;

                totBreach = parseFloat(breach.toFixed(2));


                sla = (sla / totalPieData) * 100;

                totSLA = parseFloat(sla.toFixed(2));


                exceptional = (exceptional / totalPieData) * 100;

                totExceptional = parseFloat(exceptional.toFixed(2));


                cancelled = (cancelled / totalPieData) * 100;

                totCancelled = parseFloat(cancelled.toFixed(2));


                pieData = [['Breach Transaction', totBreach], ['SLA Transaction', totSLA], ['Exceptional Transaction', totExceptional], ['Cancelled Transaction', totCancelled]];

                renderJqPlotPieChart(holder, pieData);

            } else {

                $("#pieholderdata").hide();

            }


        } else {

            $("#pieholderdata").hide();

        }
    }
    function renderTableSummarySuccess(data) {
        //console.log(data);
        var tablesData = new Array();
        var reportPeriodSelected; var reportPeriodType;

        reportPeriodSelected = $("#form-field-report-period option:selected").val();
        reportPeriodSelected = parseInt(reportPeriodSelected, 10);

        switch (reportPeriodSelected) {
            case 1:
                reportPeriodType = "Date(s)";
                break;

            case 2:
                reportPeriodType = "Week(s)";
                break;

            case 3:
                reportPeriodType = "Month(s)";
                break;

        }


        if (data.length > 0) {
            if ($("#widgetholderdata").length > 0)
                $("#widgetholderdata").remove();

            $('<div id="widgetholderdata"></div>').appendTo("#widget-box-holder");

            var tableSummary = $('<div id="widget-box" class="widget-box">' +
									'<div class="widget-header widget-hea1der-small header-color-dark">' +
										'<h6>Transaction Summary</h6>' +
										'<div class="widget-toolbar">' +
										'</div>' +
									'</div>' +
									'<div class="widget-body">' +
										'<div class="widget-main padding-0">' +
											'<div class="slim-scroll" data-height="400">' +
												'<div class="content">' +
													'<div class="table-responsive">' +
														'<div class="dataTables_wrapper" role="grid">' +
													        '<table id="Transactions-table" class="table table-striped table-bordered table-hover dataTable">' +
													            '<thead>' +
													                '<tr>' +
													                	'<th rowspan="3">' +
													                	reportPeriodType +
													                    '<th rowspan="3">' +
																		'Total Trx.</th>' +
													                    '<th rowspan="3" colspan="2">' +
																		'Exceptional Transaction </th>' +
																		'<th colspan="4">' +
													                    'Normal Transaction </th>' +
													                '</tr>' +
													                '<tr>' +
													                	'<th colspan="4">SLA</th>' +
													                '</tr>' +
													                '<tr>' +
													                    '<th>Total Trx.</th>' +
																		'<th>Breach</th>' +
																		'<th>% Breach</th>' +
																		'<th>Average TAT </th>' +
													                '</tr>' +
													            '</thead>' +
																'<tbody id="tbody-holder-value">' +
																'</tbody>' +
													        '</table>' +
													      '</div>' +
													   '</div>' +
													'</div>' +
												'</div>' +
											'</div>' +
										'</div>' +
									'</div>' +
									'</div>');

            tableSummary.appendTo("#widgetholderdata");

            for (var i = 0, max = data.length; i < max; i++) {
                var datePeriodLast = '';
                var datePeriod = data[i].datePeriod;
                var sla = data[i].slaTransactions;
                var exceptional = data[i].exceptionalTransactions;
                var breach = data[i].breachTransactions;
                var tat = data[i].TATTime;

                switch (reportPeriodType) {

                    case "Date(s)":

                        datePeriodLast = datePeriod.indexOf("T") > -1 ? datePeriod.substring(0, datePeriod.indexOf("T")) : (datePeriod.indexOf(" ") > -1 ? datePeriod.substring(0, datePeriod.indexOf(" ")) : datePeriod.substring(0, 10));
                        break;

                    case "Week(s)":

                        datePeriodLast = getWeekFromDate(datePeriod);
                        break;

                    case "Month(s)":

                        datePeriodLast = getMonthFromNumber(datePeriod);
                        break;
                }


                listValueTransactions = GetObjectByName(tablesData, datePeriodLast);
                if (listValueTransactions == null) {
                    listValueTransactions = {
                        "name": datePeriodLast, "valSLA": new Array(), "valExceptional": new Array(), "valBreach": new Array(), "valAVGTAT": new Array()
                    }
                    tablesData.push(listValueTransactions);
                }
                //console.log(tablesData);
                listValueTransactions.valSLA.push(sla);
                listValueTransactions.valExceptional.push(exceptional);
                listValueTransactions.valBreach.push(breach);
                listValueTransactions.valAVGTAT.push(tat);

            }


            $.each(tablesData, function (index, item) {
                var totalTransaction = 0; var totalSLAAndBreach = 0;
                var totalSLA = 0; var totalExceptional = 0; var totalBreach = 0;
                var percentageExceptional = 0; var strpercentageExceptional = '';
                var percentageSLA = 0; var strpercentageSLA = '';
                var rawAVGTAT = 0; var totalAVGTAT = 0; var strAVGTAT; var AVGTAT = 0;
                var strHourTAT; var strMinuteTAT;
                var arrHourTAT; var arrMinuteTAT;
                var strArrMinuteTAT; var strRawHourTAT;
                var dayTAT; var hourTAT; var minuteTAT;
                var strDayTAT;

                for (var i = 0, max = item.valSLA.length; i < max; i++) {

                    totalSLA += parseInt(item.valSLA[i], 10);

                }
                for (var i = 0, max = item.valExceptional.length; i < max; i++) {

                    totalExceptional += parseInt(item.valExceptional[i], 10);

                }
                for (var i = 0, max = item.valBreach.length; i < max; i++) {

                    totalBreach += parseInt(item.valBreach[i], 10);

                }
                for (var i = 0, max = item.valAVGTAT.length; i < max; i++) {


                    totalAVGTAT += parseInt(item.valAVGTAT[i], 10);

                }
                totalSLAAndBreach = totalSLA + totalBreach;
                totalTransaction = totalExceptional + totalSLAAndBreach;
                /*console.log(totalAVGTAT);
                console.log(totalBreach + " " + totalSLA + " " + totalExceptional);
                console.log(totalTransaction);*/
                if (totalTransaction > 0) {

                    percentageExceptional = (totalExceptional / totalTransaction) * 100;

                    rawAVGTAT = totalAVGTAT / totalSLAAndBreach;

                    AVGTAT = rawAVGTAT / 3600;

                    if (isNaN(AVGTAT)) {

                        AVGTAT = 0;
                    }


                    if (AVGTAT.toString().indexOf(".") > -1 || AVGTAT.toString().indexOf(",") > -1) {
                        var splitter = AVGTAT.toString().indexOf(".") > -1 ? "." : ",";
                        strRawHourTAT = AVGTAT.toString().split(splitter);
                        strMinuteTAT = "0." + strRawHourTAT[1];
                        arrHourTAT = parseInt(strRawHourTAT[0], 10);
                        //console.log(strRawHourTAT + " " + strMinuteTAT + " " + arrHourTAT);
                    } else {

                        strRawHourTAT = AVGTAT.toString();
                        strMinuteTAT = "0";
                        arrHourTAT = parseInt(strRawHourTAT, 10);
                    }
                    //console.log(AVGTAT);

                    if (strMinuteTAT.indexOf(".") > -1) {

                        arrMinuteTAT = parseFloat(strMinuteTAT.substring(0, 4)) * 60;
                        arrMinuteTAT = Math.floor(arrMinuteTAT);
                        //console.log(arrMinuteTAT.toString());
                        if (arrMinuteTAT.toString().indexOf(".") > -1) {
                            strArrMinuteTAT = arrMinuteTAT.toString().split(".");
                            minuteTAT = parseInt(strArrMinuteTAT[0], 10);
                            //console.log(minuteTAT);
                        } else {
                            strArrMinuteTAT = arrMinuteTAT;
                            minuteTAT = parseInt(strArrMinuteTAT, 10);
                        }
                    } else {
                        minuteTAT = 0;

                    }
                    //console.log(arrHourTAT);
                    if (arrHourTAT.toString().length > 0) {
                        hourTAT = arrHourTAT;
                        if ((hourTAT / 24) > 0) {
                            dayTAT = hourTAT / 24;
                            dayTAT = parseInt(dayTAT, 10);
                            hourTAT = arrHourTAT % 24;

                        } else {

                            dayTAT = 0;
                            hourTAT = arrHourTAT;

                        }
                        if ((hourTAT.toString().length > 1) && (hourTAT > 0)) {
                            strHourTAT = hourTAT.toString();
                        } else {
                            strHourTAT = "0" + hourTAT.toString();
                        }

                    } else {
                        hourTAT = 0;
                        strHourTAT = "00";

                    }

                    //console.log(strHourTAT);
                    if (dayTAT > 0) {

                        if (dayTAT == 1) {

                            strDayTAT = dayTAT + " day, ";

                        } else {

                            strDayTAT = dayTAT + " days, ";
                        }
                    } else {

                        strDayTAT = "";
                    }



                    if (minuteTAT.toString().length > 1) {

                        strMinuteTAT = minuteTAT.toString().substring(0, 2);

                    } else {

                        strMinuteTAT = "0" + minuteTAT.toString();
                    }

                    strAVGTAT = strDayTAT + strHourTAT + ":" + strMinuteTAT + ":00";


                } else {

                    percentageExceptional = 0;
                    strAVGTAT = "-";
                }

                strpercentageExceptional = percentageExceptional.toFixed(2).toString() + "%";
                //console.log(totalSLAAndBreach);
                //console.log(totalSLA);
                if (totalSLAAndBreach > 0) {

                    percentageSLA = (totalBreach / totalSLAAndBreach) * 100;

                } else {

                    percentageSLA = 0;

                }

                strpercentageSLA = percentageSLA.toFixed(2).toString() + "%";



                var valueTransactions = $('<tr data-toggle="modal">' +
									'<td><span>' + item.name + '</span></td>' +
									'<td><span>' + totalTransaction + '</span></td>' +
									'<td><span>' + totalExceptional + '</span></td>' +
									'<td><span>' + strpercentageExceptional + '</span></td>' +
									'<td><span>' + totalSLAAndBreach + '</span></td>' +
									'<td><span>' + totalBreach + '</span></td>' +
									'<td><span>' + strpercentageSLA + '</span></td>' +
									'<td><span>' + strAVGTAT + '</span></td></tr>');
                valueTransactions.appendTo("#tbody-holder-value");
            });


        } else {
            if ($("#widgetholderdata").length > 0)
                $("#widgetholderdata").remove();
        }
    }


    function getMonthFromNumber(Date) {
        //console.log(Date);
        var arrDate; var monthNumber; var strMonth;
        arrDate = Date.split("/");
        monthNumber = arrDate[0];
        if (monthNumber.substring(0, 1) == "0") {
            monthNumber = monthNumber.substring(2, 1);
        }
        switch (monthNumber) {
            case "1":
                strMonth = "Jan, ";
                break;
            case "2":
                strMonth = "Feb, ";
                break;
            case "3":
                strMonth = "Mar, ";
                break;
            case "4":
                strMonth = "Apr, ";
                break;
            case "5":
                strMonth = "May, ";
                break;
            case "6":
                strMonth = "Jun, ";
                break;
            case "7":
                strMonth = "Jul, ";
                break;
            case "8":
                strMonth = "Aug, ";
                break;
            case "9":
                strMonth = "Sep, ";
                break;
            case "10":
                strMonth = "Oct, ";
                break;
            case "11":
                strMonth = "Nov, ";
                break;
            case "12":
                strMonth = "Dec, ";
                break;

        }

        strMonth = strMonth + arrDate[2].substring(0, 4);
        return strMonth;
    }
    function getWeekFromDate(strDate) {

        var strDate; var newdateNow; var dateNow; var weekOfMonth; var arrStrDate = [];
        arrStrDate = strDate.indexOf("T") > -1 ? strDate.split("T") : strDate.indexOf(" ") > -1 ? strDate.split(" ") : strDate.substring(0, 10);
        strDate = arrStrDate[0];
        newDateNow = new Date(strDate);

        dateNow = newDateNow.getDate();
        dayNow = newDateNow.getDay();

        weekOfMonth = Math.ceil((dateNow - 1 - dayNow) / 7) + 1;

        return "Week " + weekOfMonth + " - " + getMonthFromNumber(strDate);
    }
    function renderJqPlotBarChart(placeholder, arraycompleted, arrayonprogress, arraycancelled, hour) {
        //console.log(title);
        //console.log(placeholder);
        //console.log(array);
        //console.log(hour);

        var plot = $.jqplot(placeholder, [arraycompleted, arrayonprogress, arraycancelled], {
            animate: true,
            animateReplot: true,
            stackSeries: true,
            seriesDefaults: {
                renderer: $.jqplot.BarRenderer,
                rendererOptions: { barWidth: 30, barDirection: 'vertical', animation: { speed: 2500 } },
                pointLabels: { show: true, location: 's' }
            },
            grid: { background: '#006c6c' },
            axesDefaults: {
                pad: 0,
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                tickRenderer: $.jqplot.CategoryAxisTickRenderer,
                rendererOptions: {
                    forceTickAt0: true
                }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    tickRenderer: $.jqplot.CategoryAxisTickRenderer,
                    ticks: hour,
                    label: 'Time'

                },
                yaxis: {
                    pad: 1.05,
                    tickOptions: { formatString: '' },
                    label: 'Volume',
                    //renderer: $.jqplot.CategoryAxisRenderer,
                    //labelRenderer: $.jqplot.CanvasAxisLabelRenderer,					        	
                    rendererOptions: {
                        forceTickAt0: true
                    },
                    min: 0

                },


            },
            highlighter: {
                show: true,
                tooltipLocation: 'n',
                sizeAdjust: 2,
                tooltipOffset: 9,
                showMarker: false,
                tooltipAxes: 'xy',
                fadeTooltip: true,
                formatString: '<table class="jqplot-highlighter">\
		    				  <tr><td>Time : '+ " " + '</td><td>%s</td></tr>\
		    				  <tr><td>Volume : '+ " " + '</td><td>%d</td></tr></table>'


            }


        });
        $('.jqplot-highlighter-tooltip').addClass('ui-corner-all');

        hideZeroStackValue();

    }

    function renderJqPlotPieChart(placeholder, data) {
        //console.log(title);
        //console.log(placeholder);
        //console.log(array);
        //console.log(hour);

        var plot = $.jqplot(placeholder, [data], {
            animate: true,
            animateReplot: true,
            seriesDefaults: {
                renderer: $.jqplot.PieRenderer,
                rendererOptions: { showDataLabels: true, animation: { speed: 2500 }, dataLabelFormatString: '%P%' }

            },
            grid: { background: '#006c6c' },
            seriesColors: ['#aeee00', '#5cb85c', '#d15b47', '#a1a822'],
            highlighter: {
                show: true,
                tooltipLocation: 'w',
                useAxesFormatters: false,
                formatString: '<table class="jqplot-highlighter">\
    				  <tr><td>%s</td><td></td></tr>\
    				  <tr><td>Percentage:</td><td>%P%</td></tr></table>'



            }

        });
        $('.jqplot-highlighter-tooltip').addClass('ui-corner-all');

        floatValue();


    }

    function renderPeriodLastUpdate(startDate, endDate, isDailyChecked) {

        toggleClassPeriod();
        if ($("#chkDateNull").is(":checked")) {

            if (isDailyChecked == true) {

                period = startDate.substring(0, startDate.indexOf("T"));
            } else {

                period = startDate.substring(0, startDate.indexOf("T")) + " - " + endDate.substring(0, endDate.indexOf("T"));
            }

        } else {

            period = $("#start-date").val() + " - " + $("#end-date").val();

        }

        transactionType = $("#form-field-transaction-type option:selected").text();
        reportPeriod = $("#form-field-report-period option:selected").text();
        injectDatePeriod(period, reportPeriod, transactionType);

    }

    function toggleClassPeriod() {

        $("#lblInfoPeriod").addClass("alert alert-info");

    }

    function injectDatePeriod(period, reportPeriod, transactionType) {

        $("#lblMISReporting").html("<b>MIS Reporting</b><br><b>Transaction Type : " + transactionType + "<br/>Period : " + reportPeriod + "<br/>Period " + period + "</b><br/>");

    }


    function injectDateTimePeriod(period) {

        $("#lblLastUpdate").html("<b>Last Update :</b><br/>" + "<b>" + period + "</b>");
    }

    function restructureDate(period, isDateTime) {
        var arrDateTimePeriod = [];
        var arrDatePeriod = []; var arrTimePeriod = []; var ampm = [];
        var arrBuffer = [];
        var year; var hour;
        var month; var minute;
        var day; var second; var ampm;

        var ret;
        if (!isDateTime) {
            arrDateTimePeriod = period.split(" ");
            arrDatePeriod = arrDateTimePeriod[0];
            arrBuffer = arrDatePeriod.split("-");
            year = arrBuffer[2]; month = arrBuffer[0]; day = arrBuffer[1];

            ret = year + "-" + month + "-" + day;
        } else {

            arrDateTimePeriod = period.split(" ");
            arrDatePeriod = arrDateTimePeriod[0];
            arrTimePeriod = arrDateTimePeriod[1];
            ampm = arrDateTimePeriod[2];
            arrBuffer = arrDatePeriod.split("-");
            year = arrBuffer[2]; month = arrBuffer[0]; day = arrBuffer[1];

            arrBuffer = [];
            arrBuffer = arrTimePeriod.split(":");
            hour = arrBuffer[0]; minute = arrBuffer[1]; second = arrBuffer[2];

            ampm = ampm.toLowerCase();
            if (ampm.indexOf("am") > -1 || ampm.indexOf("pm") > -1)
                ampm = ampm.toUpperCase();
            else
                ampm = "";
            ret = year + "-" + month + "-" + day + " " + hour + "." + minute + " " + ampm;
        }
        return ret;
    }




});
function GetCurrentUser(vm) {
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);

        vm.SPUser = spUser;
    }
}


// Token validation On Success function
function TokenOnSuccess(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // call spuser function
    GetCurrentUser();

    // call get data inside view model
    viewModel.GetParameters();
    viewModel.GetData();
}

// Token validation On Error function
function TokenOnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

