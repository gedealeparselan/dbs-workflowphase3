﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;
using System.Data.Entity;
using System.Data.Objects.DataClasses;


namespace DBS.WebAPI.Models
{

    [ModelName("VolumeMatrixTransactions")]
    [DataContract]
    public class VolumeMatrixTransactionsModel
    {

        /// <summary>
        /// Product Name
        /// </summary>
        [DataMember]
        public string productNameTrx { get; set; }

        /// <summary>
        /// Application ID
        /// </summary>
        [DataMember]
        public string applicationIDTrx { get; set; }

        /// <summary>
        /// Customer Name
        /// </summary>
        [DataMember]
        public string customerNameTrx { get; set; }

        /// <summary>
        /// Customer Name
        /// </summary>
        [DataMember]
        public int transactionStatusTrx { get; set; }

        /// <summary>
        /// Transactions Date
        /// </summary>
        [DataMember]
        public DateTime? trxDateTrx { get; set; }

        /// <summary>
        /// Actual Date
        /// </summary>
        public DateTime? actualDateTrx { get; set; }


        /// <summary>
        /// TAT
        /// </summary>
        public int? TATTrx { get; set; }

        /// <summary/>
        /// Percentage
        /// </summary>       
        public long percentageTrx { get; set; }

        /// <summary>
        /// String For Actual Date Transactions
        /// </summary>
        [DataMember(Name = "actualDateTrx")]
        private string strActualDateTrx { get; set; }

        [DataMember(Name = "TATTrx")]
        private string strTATTrx { get; set; }

        [DataMember(Name = "percentageTrx")]
        private string strPercentageTrx { get; set; }

        [OnSerializing]
        void OnSerializing(StreamingContext context)
        {

            this.strActualDateTrx = this.actualDateTrx.ToString();
            this.strTATTrx = this.TATTrx.ToString();
            this.strPercentageTrx = this.percentageTrx.ToString();
        }

    }
    [DataContract]
    public class VolumeMatrixTransactionsRow : VolumeMatrixTransactionsModel
    {
        [DataMember]
        public int RowID { get; set; }
    }

    public class VolumeMatrixTransactionsGrid : Grid
    {
        public IList<VolumeMatrixTransactionsRow> Rows { get; set; }
    }

    public class VolumeMatrixTransactionsFilter : Filter { }

    public interface IVolumeMatrixTransactionsRepository : IDisposable
    {
        bool GetVolumeMatrixTransactions(int page, int size, string productName, DateTime startDate, DateTime endDate, DateTime clientDateTime, IList<VolumeMatrixTransactionsFilter> filters, string sortColumn, string sortOrder, string paymentMode, ref VolumeMatrixTransactionsGrid allDataByProductGrid, ref string message);

    }

    public class VolumeMatrixTransactionsRepository : IVolumeMatrixTransactionsRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetVolumeMatrixTransactions(int page, int size, string productName, DateTime startDate, DateTime endDate, DateTime clientDateTime, IList<VolumeMatrixTransactionsFilter> filters, string sortColumn, string sortOrder, string paymentMode, ref VolumeMatrixTransactionsGrid allDataByProductGrid, ref string message)
        {
            bool isSuccess = false;
            DateTime serverUTC = DateTime.UtcNow;
            string state = string.Empty;
            int hourDiff, oneDayHour = 24;
            if (clientDateTime.Hour > serverUTC.Hour)
            {
                hourDiff = clientDateTime.Hour - serverUTC.Hour;
            }
            else if (clientDateTime.Hour < serverUTC.Hour)
            {
                hourDiff = oneDayHour + clientDateTime.Hour - serverUTC.Hour;
            }
            else
            {
                hourDiff = 0;
            }

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;

                VolumeMatrixTransactionsModel filter = new VolumeMatrixTransactionsModel();

                if (filters != null)
                {
                    string TATValue = (string)filters.Where(a => a.Field.Equals("TATTrx")).Select(a => a.Value).Where(a => a != null).SingleOrDefault();
                    string PercentageValue = (string)filters.Where(a => a.Field.Equals("percentageTrx")).Select(a => a.Value).Where(a => a != null).SingleOrDefault();

                    filter.applicationIDTrx = (string)filters.Where(a => a.Field.Equals("applicationIDTrx")).Select(a => a.Value).Where(a => a != null).SingleOrDefault();
                    filter.customerNameTrx = (string)filters.Where(a => a.Field.Equals("customerNameTrx")).Select(a => a.Value).Where(a => a != null).SingleOrDefault();
                    state = (string)filters.Where(a => a.Field.Equals("stateID")).Select(a => a.Value == null ? "0" : a.Value).SingleOrDefault();
                    filter.TATTrx = TATValue != null ? int.Parse(TATValue) : 0;
                    filter.percentageTrx = PercentageValue != null ? int.Parse(PercentageValue) : 0;

                }
                using (DBSEntities context = new DBSEntities())
                {
                    if (paymentMode == "BCP2")
                    {
                        var maxWorkflowApprovalId = (from workflowhistorie in context.V_VolumeMatrixTransaction
                                                     join transactions in context.Transactions
                                                     on workflowhistorie.WorkflowInstanceID equals transactions.WorkflowInstanceID
                                                     join products in context.Products
                                                     on transactions.ProductID equals products.ProductID
                                                     where (products.ProductName == productName)
                                                            && (products.IsDeleted.Equals(false))
                                                            && (transactions.Mode.Equals("BCP2"))
                                                     group workflowhistorie by workflowhistorie.WorkflowInstanceID into grp
                                                     select grp.Max(x => x.WorkflowApproverID)).ToList();

                        var allDataByProduct = (from sla in context.SLAs
                                                join products in context.Products
                                                on sla.ProductID equals products.ProductID
                                                join transactions in context.Transactions
                                                on products.ProductID equals transactions.ProductID
                                                join customer in context.Customers
                                                on transactions.CIF equals customer.CIF
                                                join progress in context.ProgressStates
                                                on transactions.StateID equals progress.StateID
                                                join workflowhistory in context.V_VolumeMatrixTransaction
                                                on transactions.WorkflowInstanceID equals workflowhistory.WorkflowInstanceID


                                                let utcToLocalTransaction = DbFunctions.AddHours(transactions.CreateDate, hourDiff)
                                                let utcToLocalTransactionUpdate = DbFunctions.AddHours(workflowhistory.WorkflowTaskExitTime, hourDiff)
                                                let isApplicationIDTrx = !string.IsNullOrEmpty(filter.applicationIDTrx)
                                                let isCustomerName = !string.IsNullOrEmpty(filter.customerNameTrx)
                                                let isTransactionStatus = !string.IsNullOrEmpty(state)
                                                let intTATTrx = (int)Math.Ceiling((Double)(DbFunctions.DiffSeconds(utcToLocalTransaction, utcToLocalTransactionUpdate) / 60))
                                                let intPercentageTrx = (int)Math.Ceiling((Decimal)(intTATTrx * 100) / (sla.SLATime * 60))
                                                let isTAT = filter.TATTrx == 0 ? false : true
                                                let isPercentage = filter.percentageTrx == 0 ? false : true


                                                where ((products.IsDeleted.Equals(false))
                                                        && (sla.IsDeleted.Equals(false))
                                                        && (isApplicationIDTrx ? transactions.ApplicationID.Contains(filter.applicationIDTrx) : true)
                                                        && (isCustomerName ? customer.CustomerName.Contains(filter.customerNameTrx) : true)
                                                        && (isTransactionStatus ? progress.StateName.Contains(state) : true)
                                                        && (isTAT ? intTATTrx == filter.TATTrx : true)
                                                        && (isPercentage ? intPercentageTrx == filter.percentageTrx : true)
                                                    //&& (transactions.StateID == 2) 20160913 aridya show all transactions instead of completed transactions only
                                                        && ((utcToLocalTransaction.Value >= startDate)
                                                        && (utcToLocalTransaction.Value <= endDate))
                                                        && (products.ProductName.Equals(productName))
                                                        && (maxWorkflowApprovalId.Contains(workflowhistory.WorkflowApproverID)))
                                                        && !(transactions.IsExceptionHandling.Value.Equals(true))
                                                        && (transactions.Mode.Equals("BCP2"))
                                                select new VolumeMatrixTransactionsModel
                                                {
                                                    productNameTrx = products.ProductName,
                                                    applicationIDTrx = transactions.ApplicationID,
                                                    customerNameTrx = transactions.Customer.CustomerName,
                                                    transactionStatusTrx = progress.StateID,
                                                    trxDateTrx = utcToLocalTransaction,
                                                    actualDateTrx = clientDateTime,
                                                    TATTrx = intTATTrx,
                                                    percentageTrx = intPercentageTrx

                                                });
                        #region group
                        //into aa
                        //group aa by new
                        //{

                        //    productNameTrx = aa.productNameTrx,
                        //    applicationIDTrx = aa.applicationIDTrx,
                        //    customerNameTrx = aa.customerNameTrx,
                        //    transactionStatusTrx = aa.transactionStatusTrx,
                        //    trxDateTrx = aa.trxDateTrx,
                        //    actualDateTrx = aa.actualDateTrx,
                        //    TATTrx = aa.TATTrx,
                        //    percentageTrx = aa.percentageTrx

                        //}into bb
                        //select new VolumeMatrixTransactionsModel
                        //{
                        //    productNameTrx = bb.Key.productNameTrx,
                        //    applicationIDTrx = bb.Key.applicationIDTrx,
                        //    customerNameTrx = bb.Key.customerNameTrx,
                        //    transactionStatusTrx = bb.Key.transactionStatusTrx,
                        //    trxDateTrx = bb.Key.trxDateTrx,
                        //    actualDateTrx = bb.Key.actualDateTrx,
                        //    TATTrx = bb.Key.TATTrx,
                        //    percentageTrx = bb.Key.percentageTrx
                        //});
                        #endregion
                        allDataByProductGrid.Page = page;
                        allDataByProductGrid.Size = size;
                        allDataByProductGrid.Total = allDataByProduct.Count();

                        allDataByProductGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                        allDataByProductGrid.Rows = allDataByProduct.OrderBy(orderBy).AsEnumerable()
                            .Select((a, i) => new VolumeMatrixTransactionsRow
                            {
                                RowID = i + 1,
                                productNameTrx = a.productNameTrx,
                                applicationIDTrx = a.applicationIDTrx,
                                customerNameTrx = a.customerNameTrx,
                                transactionStatusTrx = a.transactionStatusTrx,
                                TATTrx = a.TATTrx,
                                percentageTrx = a.percentageTrx

                            })
                            .Skip(skip)
                            .Take(size)
                            .ToList();
                    }
                    else if (paymentMode == "IPE")
                    {
                        var maxWorkflowApprovalId = (from workflowhistorie in context.V_VolumeMatrixTransaction
                                                     join transactions in context.Transactions
                                                     on workflowhistorie.WorkflowInstanceID equals transactions.WorkflowInstanceID
                                                     join products in context.Products
                                                     on transactions.ProductID equals products.ProductID
                                                     where (products.ProductName == productName)
                                                            && (products.IsDeleted.Equals(false))
                                                            && ((!transactions.Mode.Equals("BCP2")) || transactions.Mode.Equals(null))
                                                     group workflowhistorie by workflowhistorie.WorkflowInstanceID into grp
                                                     select grp.Max(x => x.WorkflowApproverID)).ToList();

                        var allDataByProduct = (from sla in context.SLAs
                                                join products in context.Products
                                                on sla.ProductID equals products.ProductID
                                                join transactions in context.Transactions
                                                on products.ProductID equals transactions.ProductID
                                                join customer in context.Customers
                                                on transactions.CIF equals customer.CIF
                                                join progress in context.ProgressStates
                                                on transactions.StateID equals progress.StateID
                                                join workflowhistory in context.V_VolumeMatrixTransaction
                                                on transactions.WorkflowInstanceID equals workflowhistory.WorkflowInstanceID


                                                let utcToLocalTransaction = DbFunctions.AddHours(transactions.CreateDate, hourDiff)
                                                let utcToLocalTransactionUpdate = DbFunctions.AddHours(workflowhistory.WorkflowTaskExitTime, hourDiff)
                                                let isApplicationIDTrx = !string.IsNullOrEmpty(filter.applicationIDTrx)
                                                let isCustomerName = !string.IsNullOrEmpty(filter.customerNameTrx)
                                                let isTransactionStatus = !string.IsNullOrEmpty(state)
                                                let intTATTrx = (int)Math.Ceiling((Double)(DbFunctions.DiffSeconds(utcToLocalTransaction, utcToLocalTransactionUpdate) / 60))
                                                let intPercentageTrx = (int)Math.Ceiling((Decimal)(intTATTrx * 100) / (sla.SLATime * 60))
                                                let isTAT = filter.TATTrx == 0 ? false : true
                                                let isPercentage = filter.percentageTrx == 0 ? false : true


                                                where ((products.IsDeleted.Equals(false))
                                                        && (sla.IsDeleted.Equals(false))
                                                        && (isApplicationIDTrx ? transactions.ApplicationID.Contains(filter.applicationIDTrx) : true)
                                                        && (isCustomerName ? customer.CustomerName.Contains(filter.customerNameTrx) : true)
                                                        && (isTransactionStatus ? progress.StateName.Contains(state) : true)
                                                        && (isTAT ? intTATTrx == filter.TATTrx : true)
                                                        && (isPercentage ? intPercentageTrx == filter.percentageTrx : true)
                                                    //&& (transactions.StateID == 2) 20160913 aridya show all transactions instead of completed transactions only
                                                        && ((utcToLocalTransaction.Value >= startDate)
                                                        && (utcToLocalTransaction.Value <= endDate))
                                                        && (products.ProductName.Equals(productName))
                                                        && (maxWorkflowApprovalId.Contains(workflowhistory.WorkflowApproverID)))
                                                        && !(transactions.IsExceptionHandling.Value.Equals(true))
                                                        && ((!transactions.Mode.Equals("BCP2")) || transactions.Mode.Equals(null))
                                                select new VolumeMatrixTransactionsModel
                                                {
                                                    productNameTrx = products.ProductName,
                                                    applicationIDTrx = transactions.ApplicationID,
                                                    customerNameTrx = transactions.Customer.CustomerName,
                                                    transactionStatusTrx = progress.StateID,
                                                    trxDateTrx = utcToLocalTransaction,
                                                    actualDateTrx = clientDateTime,
                                                    TATTrx = intTATTrx,
                                                    percentageTrx = intPercentageTrx

                                                });
                        #region group
                        //into aa
                        //group aa by new
                        //{

                        //    productNameTrx = aa.productNameTrx,
                        //    applicationIDTrx = aa.applicationIDTrx,
                        //    customerNameTrx = aa.customerNameTrx,
                        //    transactionStatusTrx = aa.transactionStatusTrx,
                        //    trxDateTrx = aa.trxDateTrx,
                        //    actualDateTrx = aa.actualDateTrx,
                        //    TATTrx = aa.TATTrx,
                        //    percentageTrx = aa.percentageTrx

                        //}into bb
                        //select new VolumeMatrixTransactionsModel
                        //{
                        //    productNameTrx = bb.Key.productNameTrx,
                        //    applicationIDTrx = bb.Key.applicationIDTrx,
                        //    customerNameTrx = bb.Key.customerNameTrx,
                        //    transactionStatusTrx = bb.Key.transactionStatusTrx,
                        //    trxDateTrx = bb.Key.trxDateTrx,
                        //    actualDateTrx = bb.Key.actualDateTrx,
                        //    TATTrx = bb.Key.TATTrx,
                        //    percentageTrx = bb.Key.percentageTrx
                        //});
                        #endregion
                        allDataByProductGrid.Page = page;
                        allDataByProductGrid.Size = size;
                        allDataByProductGrid.Total = allDataByProduct.Count();

                        allDataByProductGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                        allDataByProductGrid.Rows = allDataByProduct.OrderBy(orderBy).AsEnumerable()
                            .Select((a, i) => new VolumeMatrixTransactionsRow
                            {
                                RowID = i + 1,
                                productNameTrx = a.productNameTrx,
                                applicationIDTrx = a.applicationIDTrx,
                                customerNameTrx = a.customerNameTrx,
                                transactionStatusTrx = a.transactionStatusTrx,
                                TATTrx = a.TATTrx,
                                percentageTrx = a.percentageTrx

                            })
                            .Skip(skip)
                            .Take(size)
                            .ToList();
                    }
                    else
                    {
                        var maxWorkflowApprovalId = (from workflowhistorie in context.V_VolumeMatrixTransaction
                                                     join transactions in context.Transactions
                                                     on workflowhistorie.WorkflowInstanceID equals transactions.WorkflowInstanceID
                                                     join products in context.Products
                                                     on transactions.ProductID equals products.ProductID
                                                     where (products.ProductName == productName)
                                                            && (products.IsDeleted.Equals(false))
                                                     group workflowhistorie by workflowhistorie.WorkflowInstanceID into grp
                                                     select grp.Max(x => x.WorkflowApproverID)).ToList();

                        var allDataByProduct = (from sla in context.SLAs
                                                join products in context.Products
                                                on sla.ProductID equals products.ProductID
                                                join transactions in context.Transactions
                                                on products.ProductID equals transactions.ProductID
                                                join customer in context.Customers
                                                on transactions.CIF equals customer.CIF
                                                join progress in context.ProgressStates
                                                on transactions.StateID equals progress.StateID
                                                join workflowhistory in context.V_VolumeMatrixTransaction
                                                on transactions.WorkflowInstanceID equals workflowhistory.WorkflowInstanceID


                                                let utcToLocalTransaction = DbFunctions.AddHours(transactions.CreateDate, hourDiff)
                                                let utcToLocalTransactionUpdate = DbFunctions.AddHours(workflowhistory.WorkflowTaskExitTime, hourDiff)
                                                let isApplicationIDTrx = !string.IsNullOrEmpty(filter.applicationIDTrx)
                                                let isCustomerName = !string.IsNullOrEmpty(filter.customerNameTrx)
                                                let isTransactionStatus = !string.IsNullOrEmpty(state)
                                                let intTATTrx = (int)Math.Ceiling((Double)(DbFunctions.DiffSeconds(utcToLocalTransaction, utcToLocalTransactionUpdate) / 60))
                                                let intPercentageTrx = (int)Math.Ceiling((Decimal)(intTATTrx * 100) / (sla.SLATime * 60))
                                                let isTAT = filter.TATTrx == 0 ? false : true
                                                let isPercentage = filter.percentageTrx == 0 ? false : true


                                                where ((products.IsDeleted.Equals(false))
                                                        && (sla.IsDeleted.Equals(false))
                                                        && (isApplicationIDTrx ? transactions.ApplicationID.Contains(filter.applicationIDTrx) : true)
                                                        && (isCustomerName ? customer.CustomerName.Contains(filter.customerNameTrx) : true)
                                                        && (isTransactionStatus ? progress.StateName.Contains(state) : true)
                                                        && (isTAT ? intTATTrx == filter.TATTrx : true)
                                                        && (isPercentage ? intPercentageTrx == filter.percentageTrx : true)
                                                    //&& (transactions.StateID == 2) 20160913 aridya show all transactions instead of completed transactions only
                                                        && ((utcToLocalTransaction.Value >= startDate)
                                                        && (utcToLocalTransaction.Value <= endDate))
                                                        && (products.ProductName.Equals(productName))
                                                        && (maxWorkflowApprovalId.Contains(workflowhistory.WorkflowApproverID)))
                                                        && !(transactions.IsExceptionHandling.Value.Equals(true))

                                                select new VolumeMatrixTransactionsModel
                                                {
                                                    productNameTrx = products.ProductName,
                                                    applicationIDTrx = transactions.ApplicationID,
                                                    customerNameTrx = transactions.Customer.CustomerName,
                                                    transactionStatusTrx = progress.StateID,
                                                    trxDateTrx = utcToLocalTransaction,
                                                    actualDateTrx = clientDateTime,
                                                    TATTrx = intTATTrx,
                                                    percentageTrx = intPercentageTrx

                                                });
                        #region group
                        //into aa
                        //group aa by new
                        //{

                        //    productNameTrx = aa.productNameTrx,
                        //    applicationIDTrx = aa.applicationIDTrx,
                        //    customerNameTrx = aa.customerNameTrx,
                        //    transactionStatusTrx = aa.transactionStatusTrx,
                        //    trxDateTrx = aa.trxDateTrx,
                        //    actualDateTrx = aa.actualDateTrx,
                        //    TATTrx = aa.TATTrx,
                        //    percentageTrx = aa.percentageTrx

                        //}into bb
                        //select new VolumeMatrixTransactionsModel
                        //{
                        //    productNameTrx = bb.Key.productNameTrx,
                        //    applicationIDTrx = bb.Key.applicationIDTrx,
                        //    customerNameTrx = bb.Key.customerNameTrx,
                        //    transactionStatusTrx = bb.Key.transactionStatusTrx,
                        //    trxDateTrx = bb.Key.trxDateTrx,
                        //    actualDateTrx = bb.Key.actualDateTrx,
                        //    TATTrx = bb.Key.TATTrx,
                        //    percentageTrx = bb.Key.percentageTrx
                        //});
                        #endregion
                        allDataByProductGrid.Page = page;
                        allDataByProductGrid.Size = size;
                        allDataByProductGrid.Total = allDataByProduct.Count();

                        allDataByProductGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                        allDataByProductGrid.Rows = allDataByProduct.OrderBy(orderBy).AsEnumerable()
                            .Select((a, i) => new VolumeMatrixTransactionsRow
                            {
                                RowID = i + 1,
                                productNameTrx = a.productNameTrx,
                                applicationIDTrx = a.applicationIDTrx,
                                customerNameTrx = a.customerNameTrx,
                                transactionStatusTrx = a.transactionStatusTrx,
                                TATTrx = a.TATTrx,
                                percentageTrx = a.percentageTrx

                            })
                            .Skip(skip)
                            .Take(size)
                            .ToList();
                    }
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

}