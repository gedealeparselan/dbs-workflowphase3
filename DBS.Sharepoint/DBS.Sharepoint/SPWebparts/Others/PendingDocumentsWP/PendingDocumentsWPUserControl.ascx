﻿<%@ Assembly Name="DBS.Sharepoint, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f75c72caf4809f4b" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PendingDocumentsWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.Others.PendingDocumentsWP.PendingDocumentsWPUserControl" %>

<div id="transaction-parent">
    <h3 class="header smaller lighter dark">Pending Documents</h3>

    <!-- widget box start -->
    <div id="widget-box" class="widget-box">
        <!-- widget header start -->
        <div class="widget-header widget-hea1der-small header-color-dark">
            <h6>Pending Documents Table</h6>
            <div class="widget-toolbar">
                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                    <i class="blue icon-filter"></i>
                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: $root.PendingDocumentsGridProperties().AllowFilter" />
                    <span class="lbl"></span>
                </label>
                <a href="#" data-bind="click: $root.GetPendingDocuments" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                    <i class="blue icon-refresh"></i>
                </a>
            </div>
        </div>
        <!-- widget header end -->
        <!-- widget body start -->
        <div class="widget-body">
            <!-- widget main start -->
            <div class="widget-main padding-0">
                <!-- widget slim control start -->
                <div class="slim-scroll" data-height="400">
                    <!-- widget content start -->
                    <div class="content">
                        <!-- table responsive start -->
                        <div class="table-responsive">
                            <div class="dataTables_wrapper" role="grid">
                                <table id="pendingdocuments-table" class="table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                        <tr data-bind="with: $root.PendingDocumentsGridProperties">
                                            <th>No.</th>
                                            <th data-bind="click: function () { Sorting('BranchCode') }, css: GetSortedColumn('BranchCode')">Branch Code</th>
                                            <th data-bind="click: function () { Sorting('BranchName') }, css: GetSortedColumn('BranchName')">User Location</th>
                                            <th data-bind="click: function () { Sorting('ApplicationID') }, css: GetSortedColumn('ApplicationID')">Application ID</th>
                                            <th data-bind="click: function () { Sorting('Customer') }, css: GetSortedColumn('Customer')">Customer Name</th>
                                            <th data-bind="click: function () { Sorting('Product') }, css: GetSortedColumn('Product')">Product</th>
                                            <th data-bind="click: function () { Sorting('Currency') }, css: GetSortedColumn('Currency')">Currency</th>
                                            <th data-bind="click: function () { Sorting('Amount') }, css: GetSortedColumn('Amount')">Trxn Amount</th>
                                            <th data-bind="click: function () { Sorting('DebitAccNumber') }, css: GetSortedColumn('DebitAccNumber')">Debit Acc Number</th>
                                            <th data-bind="click: function () { Sorting('IsFXTransaction') }, css: GetSortedColumn('IsFXTransaction')">FX Transaction</th>
                                            <th data-bind="click: function () { Sorting('IsTopUrgent') }, css: GetSortedColumn('IsTopUrgent')">Urgency</th>
                                            <th data-bind="click: function () { Sorting('TransactionStatus') }, css: GetSortedColumn('TransactionStatus')">Transaction Status</th>
                                            <th data-bind="click: function () { Sorting('LastModifiedBy') }, css: GetSortedColumn('LastModifiedBy')">Modified By</th>
                                            <th data-bind="click: function () { Sorting('LastModifiedDate') }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
                                        </tr>
                                    </thead>
                                    <thead data-bind="visible: $root.PendingDocumentsGridProperties().AllowFilter" class="table-filter">
                                        <tr>
                                            <th class="clear-filter">
                                                <a href="#" data-bind="click: PendingDocumentsClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                    <i class="green icon-trash"></i>
                                                </a>
                                            </th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterBranchCode, event: { change: $root.PendingDocumentsGridProperties().Filter }" 
/></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterBranchName, event: { change: $root.PendingDocumentsGridProperties().Filter }" 
/></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterApplicationID, event: { change: $root.PendingDocumentsGridProperties().Filter }" 
/></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCustomer, event: { change: $root.PendingDocumentsGridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterProduct, event: { change: $root.PendingDocumentsGridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCurrency, event: { change: $root.PendingDocumentsGridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterAmount, event: { change: $root.PendingDocumentsGridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterDebitAccNumber, event: { change: $root.PendingDocumentsGridProperties().Filter }" 
/></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterFXTransaction, event: { change: $root.PendingDocumentsGridProperties().Filter }" 
/></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterTopUrgent, event: { change: $root.PendingDocumentsGridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterTransactionStatus, event: { change: $root.PendingDocumentsGridProperties().Filter }" 
/></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterLastModifiedBy, event: { change: $root.PendingDocumentsGridProperties().Filter }" 
/></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterLastModifiedDate, event: { change: 
$root.PendingDocumentsGridProperties().Filter }" /></th>
                                        </tr>
                                    </thead>
                                    <tbody data-bind="foreach: $root.PendingDocuments">
                                        <tr data-bind="click: $root.getSelectedPendingDocuments" data-toggle="modal">
                                            <td><span data-bind="text: RowID"></span></td>
                                            <td><span data-bind="text: Transaction.BranchCode"></span></td>
                                            <td><span data-bind="text: Transaction.BranchName"></span></td>
                                            <td><span data-bind="text: Transaction.ApplicationID"></span></td>
                                            <td><span data-bind="text: Transaction.Customer"></span></td>
                                            <td><span data-bind="text: Transaction.Product"></span></td>
                                            <td><span data-bind="text: Transaction.Currency"></span></td>
                                            <td align="right"><span data-bind="text: formatNumber(Transaction.Amount)"></span></td>
                                            <td><span data-bind="text: Transaction.DebitAccNumber"></span></td>
                                            <td><span data-bind="text: Transaction.IsFXTransactionValue"></span></td>
                                            <td><span data-bind="text: Transaction.IsTopUrgentValue"></span></td>
                                            <td><span data-bind="text: Transaction.TransactionStatus"></span></td>
                                            <td><span data-bind="text: Transaction.LastModifiedBy"></span></td>
                                            <td><span data-bind="text: $root.LocalDate(Transaction.LastModifiedDate)"></span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- table responsive end -->
                    </div>
                    <!-- widget content end -->
                </div>
                <!-- widget slim control end -->
                <!-- widget footer start -->
                <div class="widget-toolbox padding-8 clearfix">
                    <div class="row" data-bind="with: $root.PendingDocumentsGridProperties">
                        <!-- pagination size start -->
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                Showing
                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" 
title="Showing Page Entries"></select>
                                rows of <span data-bind="text: Total"></span>
                                entries
                            </div>
                        </div>
                        <!-- pagination size end -->
                        <!-- pagination page jump start -->
                        <div class="col-sm-3">
                            <div class="dataTables_paginate paging_bootstrap">
                                Page
                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to 
Page" />
                                of <span data-bind="text: TotalPages"></span>
                            </div>
                        </div>
                        <!-- pagination page jump end -->
                        <!-- pagination navigation start -->
                        <div class="col-sm-3">
                            <div class="dataTables_paginate paging_bootstrap">
                                <ul class="pagination">
                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                            <i class="icon-double-angle-left"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                            <i class="icon-angle-left"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                            <i class="icon-angle-right"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                            <i class="icon-double-angle-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- pagination navigation end -->

                    </div>
                </div>
                <!-- widget footer end -->

            </div>
            <!-- widget main end -->
        </div>
        <!-- widget body end -->
    </div>
    <!-- widget box end -->


</div>

<div class="space-10"></div>

<div id="transaction-child" style="display: none;">
    <h3 class="header smaller no-margin-top lighter dark">Transaction</h3>

    <div class="modal-body overflow-visible">
        <div class="row" data-bind="with: TransactionModel">
            <div class="col-xs-12">
                <div id="customer-form" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right">
                            <span data-bind="text: $root.LocalDate(CreateDate, true, true)"></span>
                        </label>

                        <label class="col-sm-4 pull-right">
                            <b>Application ID</b> : <span data-bind="text: ApplicationID"></span>
                            <br />
                            <b>User Name</b> : <span data-bind="text: $root.SPUser().DisplayName"></span>
                        </label>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right">Top Urgent</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsTopUrgent ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                        <label class="col-sm-2 control-label bolder no-padding-right">Top Urgent Chain</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsTopUrgentChain ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="customer-name">Customer Name</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Customer.Name"></span>
                            <span class="label label-warning arrowed" data-bind="if: IsNewCustomer">
                                <i class="icon-warning-sign bigger-120"></i>
                                New Customer
                            </span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="cif">CIF</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Customer.CIF"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="product">Product</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Product.Code"></span>
                            (<span data-bind="text: Product.Name"></span>)
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="currency">Transaction Currency</label>

                        <label class="col-sm-8">
                            <span data-bind="text: Currency.Code"></span>
                            (<span data-bind="text: Currency.Description"></span>)
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="trxn-amount">Trxn Amount</label>

                        <label class="col-sm-9">
                            <span data-bind="text: formatNumber(Amount)"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="rate">Rate</label>

                        <label class="col-sm-9">
                            <span data-bind="text: formatNumber_r(Rate)"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="eqv-usd">Eqv. USD</label>

                        <label class="col-sm-9">
                            <span data-bind="text: formatNumber_r(AmountUSD)"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="channel">Channel</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Channel.Name"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="debit-acc-number">Debit Acc Number</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Account.AccountNumber"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="debit-acc-ccy">Debit Acc CCY</label>

                        <label class="col-sm-9">
                            <span name="debit-acc-ccy" id="debit-acc-ccy-code" data-bind="text: DebitCurrency.Code"></span>
                            (<span name="debit-acc-ccy" id="debit-acc-ccy-desc" data-bind="text: DebitCurrency.Description"></span>)
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="application-date">Application Date</label>

                        <label class="col-sm-9">
                            <span data-bind="text: $root.LocalDate(ApplicationDate, true)"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="biz-segment">Biz Segment</label>

                        <label class="col-sm-9">
                            <span data-bind="text: BizSegment.Description"></span>
                            (<span data-bind="text: BizSegment.Name"></span>)
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom" data-bind="if: Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR' && ProductType !=null">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="product">Product Type</label>

                        <label class="col-sm-9">
                            <span data-bind="text: ProductType.Code"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="bene-name">Bene Name</label>

                        <label class="col-sm-9">
                            <span data-bind="text: BeneName"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="bene-bank">Bene Bank</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Bank.Description"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="bank-code">Swift Code/Bank Code</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Bank.SwiftCode"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="bene-acc-number">Bene Acc Number</label>

                        <label class="col-sm-9">
                            <span data-bind="text: BeneAccNumber"></span>
                        </label>
                    </div>

                    <div data-bind="if: BankCharges != null && (Product.Code == 'OT' || Product.Code == 'RT' || Product.Code == 'SK'), attr: { class: 'form-group form-line no-margin-bottom no-padding-bottom' 
}">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="bank-charges">Bank Charges</label>

                        <label class="col-sm-4">
                            <span data-bind="text: BankCharges.Code"></span>
                        </label>
                    </div>

                    <div data-bind="if: AgentCharges != null && Product.Code == 'RT', attr: { class: 'form-group form-line no-margin-bottom no-padding-bottom' }">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="agent-charges">Agent Charges</label>

                        <label class="col-sm-4">
                            <span data-bind="text: AgentCharges.Code"></span>
                        </label>
                    </div>

                    <div data-bind="if: Product.Code == 'SK', visible: Product.Code == 'SK', attr: { class: 'form-group form-line no-margin-bottom no-padding-bottom' }">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="type">Type</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Type"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Citizen</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsCitizen ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Resident</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsResident ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Payment Details</label>

                        <label class="col-sm-9">
                            <span data-bind="text: PaymentDetails"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="lld-code">LLD Code</label>

                        <label class="col-sm-9" data-bind="if:LLD != null">
                            <span data-bind="text: LLD.Code + ' (' + LLD.Description + ')'"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Signature Verification</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsSignatureVerified ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Dormant Account</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsDormantAccount ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Freeze Account</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsFreezeAccount ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Others Exceptional Handling</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Others"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="is-pending-documents">Documents Completeness</label>

                        <label class="col-sm-9">
                            <input name="is-pending-documents" class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: $root.UpdatePendingDocuments().IsCompleted, click: 
IsDocumentCompleteCheck()" /><!--, disable: !$root.IsEditable()-->
                            <span class="lbl"></span>
                        </label>
                    </div>
                </div>

                <div class="space-4"></div>

                <!--
                                if: Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR'
                -->
                <div class="padding-4" data-bind="if: Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR'">
                    <h3 class="header smaller lighter dark">Un-utilize Underlying Documents
                        <%--<button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.NewDataUnderlying, disable: !$root.IsEditable()">
                            <i class="icon-plus"></i>
                            Add Underlying
                        </button>--%>
                    </h3>
                    <!-- widget box start -->
                    <div id="widget-box" class="widget-box">
                        <!-- widget header start -->
                        <div class="widget-header widget-hea1der-small header-color-dark">
                            <h6>Underlying Table</h6>

                            <div class="widget-toolbar">
                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                    <i class="blue icon-filter"></i>
                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: $root.UnderlyingGridProperties().AllowFilter" />
                                    <span class="lbl"></span>
                                </label>
                                <a href="#" data-bind="click: $root.GetDataUnderlying" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                    <i class="blue icon-refresh"></i>
                                </a>
                            </div>
                        </div>
                        <!-- widget header end -->
                        <!-- widget body start -->
                        <div class="widget-body">
                            <!-- widget main start -->
                            <div class="widget-main padding-0">
                                <!-- widget slim control start -->
                                <div class="slim-scroll" data-height="200">
                                    <!-- widget content start -->
                                    <div class="content">

                                        <!-- table responsive start -->
                                        <div class="table-responsive">
                                            <div class="dataTables_wrapper" role="grid">
                                                <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                    <thead>
                                                        <tr data-bind="with: $root.UnderlyingGridProperties">
                                                            <th style="width: 50px">No.</th>
                                                            <th style="width: 50px">Utilize</th>
                                                            <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                            <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                            <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                            <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                            <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                            <th data-bind="click: function () { Sorting('AvailableAmount'); }, css: GetSortedColumn('AvailableAmount')">Available Amount in USD</th>
                                                            <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                            <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                            <th data-bind="click: function () { Sorting('AttachmentNo'); }, css: GetSortedColumn('AttachmentNo')">Attachment No</th>
                                                            <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th>
                                                            <th style="width: 50px">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <thead data-bind="visible: $root.UnderlyingGridProperties().AllowFilter" class="table-filter">
                                                        <tr>
                                                            <th class="clear-filter">
                                                                <a href="#" data-bind="click: $root.UnderlyingClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear 
Filters">
                                                                    <i class="green icon-trash"></i>
                                                                </a>
                                                            </th>
                                                            <th class="clear-filter"></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterStatementLetter, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterUnderlyingDocument, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterDocumentType, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterCurrency, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterAmount, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterAvailableAmount, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.UnderlyingFilterDateOfUnderlying, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterSupplierName, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterAttachmentNo, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterReferenceNumber, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th class="clear-filter"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody data-bind="foreach: $root.CustomerUnderlyings, visible: $root.CustomerUnderlyings().length > 0">
                                                        <tr>
                                                            <td><span data-bind="text: RowID"></span></td>
                                                            <td align="center">
                                                                <input type="checkbox" id="isUtilize" name="isUtilize" data-bind="checked:IsEnable,event: {change:function(data){$root.onSelectionUtilize
($index(),$data)}}, disable: !$root.IsEditable()" /></td>
                                                            <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                            <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                            <td><span data-bind="text: DocumentType.Name"></span></td>
                                                            <td><span data-bind="text: Currency.Code"></span></td>
                                                            <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                                            <td align="right"><span data-bind="text: formatNumber(AvailableAmount)"></span></td>
                                                            <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                            <td><span data-bind="text: SupplierName"></span></td>
                                                            <td><span data-bind="text: AttachmentNo"></span></td>
                                                            <td><span data-bind="text: ReferenceNumber"></span></td>
                                                            <td><span class="label label-info arrowed-in-right arrowed" data-bind="click: $root.GetUnderlyingSelectedRow,disable: !$root.IsEditable()">view</span></td>
                                                        </tr>
                                                    </tbody>
                                                    <tbody data-bind="visible: $root.CustomerUnderlyings().length == 0">
                                                        <tr>
                                                            <td colspan="13" class="text-center">No entries available.
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- table responsive end -->
                                    </div>
                                    <!-- widget content end -->
                                </div>
                                <!-- widget slim control end -->
                                <!-- widget footer start -->

                                <div class="widget-toolbox padding-8 clearfix">
                                    <div class="row" data-bind="with: $root.UnderlyingGridProperties">
                                        <!-- pagination size start -->
                                        <div class="col-sm-6">
                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                Showing
                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-
placement="bottom" title="Showing Page Entries"></select>
                                                rows of <span data-bind="text: Total"></span>
                                                entries
                                            </div>
                                        </div>
                                        <!-- pagination size end -->
                                        <!-- pagination page jump start -->
                                        <div class="col-sm-3">
                                            <div class="dataTables_paginate paging_bootstrap">
                                                Page
                                                <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" 
data-placement="bottom" title="Jump to Page" />
                                                of <span data-bind="text: TotalPages"></span>
                                            </div>
                                        </div>
                                        <!-- pagination page jump end -->
                                        <!-- pagination navigation start -->
                                        <div class="col-sm-3">
                                            <div class="dataTables_paginate paging_bootstrap">
                                                <ul class="pagination">
                                                    <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                            <i class="icon-double-angle-left"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                            <i class="icon-angle-left"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                            <i class="icon-angle-right"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                            <i class="icon-double-angle-right"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- pagination navigation end -->

                                    </div>
                                </div>

                                <!-- widget footer end -->

                            </div>
                            <!-- widget main end -->
                        </div>
                        <!-- widget body end -->
                    </div>
                    <!-- widget box end -->


                </div>
                <!--- Added form custome Customer Underlying End -->
                <div class="space-10"></div>
                <div data-bind="if: Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR'">
                    <div class="form-horizontal" role="form">

                        <div class="form-group">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="underlying-code">Underlying Code</label>
                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <select class="col-sm-12" id="underlying-code" name="underlying-code" data-bind="options: $root.Parameter().UnderlyingDocs, optionsText: function(item) { if(item.ID != 
null) return item.Code + ' (' + item.Name + ')'} , optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().UnderlyingDoc, disable: !$root.IsEditable()" data-rule-
required="true"></select>

                                </div>
                            </div>
                        </div>

                        <div class="form-group" data-bind="visible: $root.isNewUnderlying()">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="underlying-desc">Other Underlying</label>

                            <div class="col-xs-9 align-left">
                                <div class="clearfix">
                                    <textarea class="col-lg-5" id="underlying-desc" rows="4" data-rule-required="true" data-bind="value: OtherUnderlyingDoc, enable: $root.isNewUnderlying, disable: !
$root.IsEditable()"></textarea>

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="utilization-amount">Utilization Amount</label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="text" autocomplete="off" class="col-sm-4 align-right" name="utilization-amount" id="utilization-amount" rule-required="true" disabled="disabled" data-rule-
number="true" data-bind="value: formatNumber($root.UtilizationAmounts())" />
                                </div>
                            </div>
                        </div>

                        <!--<div class="form-group">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="totalTrx">Total FX-IDR Transaction in USD</label>
                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="text" autocomplete="off" class="col-sm-4 align-right" name="totalTrx" id="totalTrx" disabled="disabled" data-bind="value:TotalTransFX" />
                                </div>
                            </div>
                        </div>-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="tz-number">TZ Number</label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="text" autocomplete="off" class="col-sm-6" name="tz-number" id="tz-number" data-bind="value: TZNumber, disable: !$root.IsEditable()" />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="space-4"></div>
                <div class="padding-4">
                    <h3 class="header smaller lighter dark">Instruction Form and Docs
                        <button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.UploadDocumentUnderlying, disable: false,visible:(Currency.Code != 'IDR' && $root.CurrencyDep().Code == 
'IDR')" onclick="return false">
                            <i class="icon-plus"></i>
                            Attach
                        </button>
                        <button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.UploadDocument, disable: false,visible:!(Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR')" 
onclick="return false">
                            <i class="icon-plus"></i>
                            Attach
                        </button>
                    </h3>
                    <!--data-bind="visible:!$root.IsUnderlyingMode()" -->
                    <div class="dataTables_wrapper" role="grid">
                        <table class="table table-striped table-bordered table-hover dataTable">
                            <thead>
                                <tr>
                                    <th style="width: 50px">No.</th>
                                    <th>File Name</th>
                                    <th>Purpose of Docs</th>
                                    <th>Type of Docs</th>
                                    <th>Modified</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody data-bind="foreach: $root.Documents, visible: $root.Documents().length > 0">
                                <tr>
                                    <!--data-bind="click: $root.EditDocument"-->
                                    <td><span data-bind="text: $index() +1"></span></td>
                                    <td><span data-bind="text: FileName"></span></td>
                                    <td><span data-bind="text: Purpose.Name"></span></td>
                                    <td><span data-bind="text: Type.Name"></span></td>
                                    <td><span data-bind="text: moment(DocumentPath.lastModifiedDate).format(config.format.dateTime)"></span></td>
                                    <!--$root.LocalDate(LastModifiedDate)-->
                                    <td><a href="#" data-bind="click: $root.RemoveDocument">Remove</a></td>
                                </tr>
                            </tbody>
                            <tbody data-bind="visible: $root.Documents().length == 0">
                                <tr>
                                    <td colspan="6" align="center">No entries available.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="space-10"></div>

                    <!--- Grid Underlying Proforma Start -->
                    <!-- widget header start -->
                    <div data-bind="if: Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR'">
                        <h3 class="header smaller lighter dark">Underlying Docs
                        </h3>
                        <div class="widget-header widget-hea1der-small header-color-dark">
                            <h6>Attach File Table</h6>
                            <div class="widget-toolbar">
                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                    <i class="blue icon-filter"></i>
                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: $root.AttachGridProperties().AllowFilter" />
                                    <span class="lbl"></span>
                                </label>
                                <a href="#" data-bind="click: $root.GetDataAttachFile" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                    <i class="blue icon-refresh"></i>
                                </a>
                            </div>
                        </div>
                        <!-- widget header end -->
                        <div class="widget-body">
                            <!-- widget main start -->
                            <div class="widget-main padding-0">
                                <!-- widget slim control start -->
                                <div class="slim-scroll" data-height="200">
                                    <!-- widget content start -->
                                    <div class="content">

                                        <!-- table responsive start -->
                                        <div class="table-responsive">
                                            <div class="dataTables_wrapper" role="grid">
                                                <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                    <thead>
                                                        <tr data-bind="with: $root.AttachGridProperties">
                                                            <th style="width: 50px">No.</th>
                                                            <th data-bind="click: function () { Sorting('FileName'); }, css: GetSortedColumn('FileName')">File Name</th>
                                                            <th data-bind="click: function () { Sorting('DocumentPurpose'); }, css: GetSortedColumn('DocumentPurpose')">Document Purpose</th>
                                                            <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Update</th>
                                                            <th data-bind="click: function () { Sorting('DocumentRefNumber'); }, css: GetSortedColumn('DocumentRefNumber')">Document Ref Number</th>
                                                            <th style="width: 0px; display: none"></th>
                                                            <th style="width: 50px">Delete</th>
                                                        </tr>
                                                    </thead>
                                                    <thead data-bind="visible: $root.AttachGridProperties().AllowFilter" class="table-filter">
                                                        <tr>
                                                            <th class="clear-filter">
                                                                <a href="#" data-bind="click: $root.AttachClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                    <i class="green icon-trash"></i>
                                                                </a>
                                                            </th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.AttachFilterFileName, event: { change: 
$root.AttachGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.AttachFilterDocumentPurpose, event: { change: 
$root.AttachGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.AttachFilterLastModifiedDate, event: { change: $root.AttachGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.AttachFilterDocumentRefNumber, event: { change: 
$root.AttachGridProperties().Filter }" /></th>
                                                            <th class="clear-filter" style="display: none"></th>
                                                            <th class="clear-filter"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody data-bind="foreach: $root.CustomerUnderlyingFiles, visible: $root.CustomerUnderlyingFiles().length > 0">
                                                        <tr data-bind="click: $root.GetAttachSelectedRow" data-toggle="modal">
                                                            <td><span data-bind="text: RowID"></span></td>
                                                            <td><span data-bind="text: FileName"></span></td>
                                                            <td><span data-bind="text: DocumentPurpose.Name"></span></td>
                                                            <td><span data-bind="text: $root.LocalDate(LastModifiedDate,false,false)"></span></td>
                                                            <td><span data-bind="text: DocumentRefNumber"></span></td>
                                                            <td style="width: 0px; display: none"><span data-bind="text: DocumentPath"></span></td>
                                                            <td align="center">
                                                                <!--<a href="#" data-bind="click:$root.delete_a" class="tooltip-info" data-rel="tooltip" data-placement="right">
                                                                    <i class="green icon-trash"></i>
                                                                </a>-->
                                                                <i class="gray icon-trash"></i>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                    <tbody data-bind="visible: $root.CustomerUnderlyingFiles().length == 0">
                                                        <tr>
                                                            <td colspan="10" class="text-center">No entries available.
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- table responsive end -->
                                    </div>
                                    <!-- widget content end -->
                                </div>
                                <!-- widget slim control end -->
                                <!-- widget footer start -->
                                <div class="widget-toolbox padding-8 clearfix">
                                    <div class="row" data-bind="with: $root.AttachGridProperties">
                                        <!-- pagination size start -->
                                        <div class="col-sm-6">
                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                Showing
                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-
placement="bottom" title="Showing Page Entries"></select>
                                                rows of <span data-bind="text: Total"></span>
                                                entries
                                            </div>
                                        </div>
                                        <!-- pagination size end -->
                                        <!-- pagination page jump start -->
                                        <div class="col-sm-3">
                                            <div class="dataTables_paginate paging_bootstrap">
                                                Page
                                                <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" 
data-placement="bottom" title="Jump to Page" />
                                                of <span data-bind="text: TotalPages"></span>
                                            </div>
                                        </div>
                                        <!-- pagination page jump end -->
                                        <!-- pagination navigation start -->
                                        <div class="col-sm-3">
                                            <div class="dataTables_paginate paging_bootstrap">
                                                <ul class="pagination">
                                                    <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                            <i class="icon-double-angle-left"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                            <i class="icon-angle-left"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                            <i class="icon-angle-right"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                            <i class="icon-double-angle-right"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- pagination navigation end -->

                                    </div>
                                </div>
                                <!-- widget footer end -->

                            </div>
                            <!-- widget main end -->
                        </div>
                    </div>
                    <!-- widget body end -->
                    <!--- Grid Underlying Proforma End -->

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 align-right">
            <button class="btn btn-sm btn-primary" data-bind="click: Submit, disable: $root.IsEditable()">
                <i class="icon-save"></i>
                Submit
            </button>

            <button class="btn btn-sm btn-success" data-bind="click: $root.cancel_detail" data-dismiss="modal">
                <i class="icon-remove"></i>
                Cancel
            </button>
        </div>
    </div>

    <!-- modal form start Attach Document Underlying Form start -->
    <div id="modal-form-Attach" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <!-- modal header start Attach FIle Form -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span>Added Attachment File</span>
                    </h4>
                </div>
                <!-- modal header end Attach file -->
                <!-- modal body start Attach File -->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div id="customer-form" class="form-horizontal" role="form">
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="Name">
                                        Purpose of Doc
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentPurpose" name="documentPurpose" data-bind="value:$root.DocumentPurpose_a().ID, options: ddlDocumentPurpose_a, optionsText: 'Name',optionsValue: 
'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="description">
                                        Type of Doc
                                    </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentType" name="documentType" data-bind="value:$root.DocumentType_a().ID, options: ddlDocumentType_a, optionsText: 'Name',optionsValue: 
'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-path">Document</label>

                                    <div class="col-sm-6">
                                        <div class="clearfix">
                                            <input type="file" id="document-path" name="document-path" data-bind="file: DocumentPath_a" class="col-xs-7" data-rule-required="true" data-rule-value="true" 
/><!---->
                                            <!--<span data-bind="text: DocumentPath"></span>-->
                                        </div>
                                    </div>
                                    <label class="control-label bolder text-danger">*</label>
                                </div>
                            </div>
                            <!-- modal body form Attach File End -->
                            <!-- widget box Underlying Attach start -->
                            <div id="widget-box" class="widget-box">
                                <!-- widget header Underlying Attach start -->
                                <div class="widget-header widget-hea1der-small header-color-dark">
                                    <h6>Underlying Form Table</h6>

                                    <div class="widget-toolbar">
                                        <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                            <i class="blue icon-filter"></i>
                                            <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingAttachGridProperties().AllowFilter" />
                                            <span class="lbl"></span>
                                        </label>
                                        <a href="#" data-bind="click: GetDataUnderlyingAttach" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                            <i class="blue icon-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                                <!-- widget header end -->
                                <!-- widget body underlying Attach start -->
                                <div class="widget-body">
                                    <!-- widget main start -->
                                    <div class="widget-main padding-0">
                                        <!-- widget slim control start -->
                                        <div class="slim-scroll" data-height="400">
                                            <!-- widget content start -->
                                            <div class="content">
                                                <!-- table responsive start -->
                                                <div class="table-responsive">
                                                    <div class="dataTables_wrapper" role="grid">
                                                        <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                            <thead>
                                                                <tr data-bind="with: $root.UnderlyingAttachGridProperties">
                                                                    <!-- <th style="width:50px">Select</th> -->
                                                                    <th style="width: 50px">No.</th>
                                                                    <th style="width: 50px">Select</th>
                                                                    <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                    <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                    <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                    <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                    <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                    <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                    <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                    <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th>
                                                                    <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                                </tr>
                                                            </thead>
                                                            <thead data-bind="visible: $root.UnderlyingAttachGridProperties().AllowFilter" class="table-filter">
                                                                <tr>
                                                                    <th class="clear-filter">
                                                                        <a href="#" data-bind="click: $root.UnderlyingAttachClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" 
title="Clear Filters">
                                                                            <i class="green icon-trash"></i>
                                                                        </a>
                                                                    </th>
                                                                    <th class="clear-filter"></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterStatementLetter, event: { 
    change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterUnderlyingDocument, 
    event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterDocumentType, event: { 
    change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterCurrency, event: { 
    change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterAmount, event: { change: 
UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.UnderlyingAttachFilterDateOfUnderlying, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterSupplierName, event: { 
    change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterReferenceNumber, event: { 
    change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.UnderlyingAttachFilterExpiredDate, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody data-bind="foreach: {data: $root.CustomerAttachUnderlyings, as: 'AttachData'}, visible: $root.CustomerAttachUnderlyings().length > 0">
                                                                <tr data-toggle="modal">
                                                                    <td><span data-bind="text: RowID"></span></td>
                                                                    <td align="center">
                                                                        <input type="checkbox" id="isSelected2" name="isSelect2" data-bind="checked:IsSelectedAttach,event:{change:function(data)
{$root.onSelectionAttach($index(),$data)}}, disable: !($root.SelectingUnderlying())" /></td>
                                                                    <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                    <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                    <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                    <td><span data-bind="text: Currency.Code"></span></td>
                                                                    <td><span data-bind="text: Amount"></span></td>
                                                                    <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                                    <td><span data-bind="text: SupplierName"></span></td>
                                                                    <td><span data-bind="text: ReferenceNumber"></span></td>
                                                                    <td><span data-bind="text: $root.LocalDate(ExpiredDate,true,false)"></span></td>
                                                                </tr>
                                                            </tbody>
                                                            <tbody data-bind="visible: $root.CustomerAttachUnderlyings().length == 0">
                                                                <tr>
                                                                    <td colspan="11" class="text-center">No entries available.
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- table responsive end -->
                                            </div>
                                            <!-- widget content end -->
                                        </div>
                                        <!-- widget slim control end -->
                                        <!-- widget footer start -->
                                        <div class="widget-toolbox padding-8 clearfix">
                                            <div class="row" data-bind="with: UnderlyingAttachGridProperties">
                                                <!-- pagination size start -->
                                                <div class="col-sm-6">
                                                    <div class="dataTables_paginate paging_bootstrap pull-left">
                                                        Showing
                                                        <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-
placement="bottom" title="Showing Page Entries"></select>
                                                        rows of <span data-bind="text: Total"></span>
                                                        entries
                                                    </div>
                                                </div>
                                                <!-- pagination size end -->
                                                <!-- pagination page jump start -->
                                                <div class="col-sm-3">
                                                    <div class="dataTables_paginate paging_bootstrap">
                                                        Page
                                                        <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-
rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                        of <span data-bind="text: TotalPages"></span>
                                                    </div>
                                                </div>
                                                <!-- pagination page jump end -->
                                                <!-- pagination navigation start -->
                                                <div class="col-sm-3">
                                                    <div class="dataTables_paginate paging_bootstrap">
                                                        <ul class="pagination">
                                                            <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                    <i class="icon-double-angle-left"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                    <i class="icon-angle-left"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                    <i class="icon-angle-right"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                    <i class="icon-double-angle-right"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- pagination navigation end -->

                                            </div>
                                        </div>
                                        <!-- widget footer end -->

                                    </div>
                                    <!-- widget main end -->
                                </div>
                                <!-- widget body end -->
                            </div>
                            <!-- widget box end -->



                        </div>
                    </div>
                </div>
                <!-- modal body end Attach File -->
                <!-- modal footer start Attach File-->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.save_a">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.cancel_a">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
                <!-- modal footer end attach file -->

            </div>
        </div>
    </div>
    <!-- modal form end Attach Document Underlying Form end -->
    <!-- Modal Double Transaction start -->
    <!-- Modal Double Transaction end -->
    <!-- Modal upload form start -->
    <div id="modal-form-upload" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="customer-form" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-type">Document Type</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="document-type" name="document-type" data-rule-required="true" data-rule-value="true" data-bind="options: Parameter().DocumentTypes, optionsText: 'Name', 
    optionsValue: 'ID', optionsCaption: 'Options..', value: Selected().DocumentType"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-purpose">Purpose of Docs</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="document-purpose" name="document-purpose" data-rule-required="true" data-rule-value="true" data-bind="options: Parameter().DocumentPurposes, 
    optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Options..', value: Selected().DocumentPurpose"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-path">Document</label>

                                    <div class="col-sm-8">
                                        <div class="clearfix">
                                            <input type="file" id="document-path" name="document-path" data-bind="file: DocumentPath" /><!---->
                                            <!--<span data-bind="text: DocumentPath"></span>-->
                                        </div>
                                    </div>
                                    <label class="control-label bolder text-danger">*</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.AddDocument">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.cancel_a">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal upload form end -->
    <!-- modal form start Underlying Form -->
    <div id="modal-form-Underlying" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <!-- modal header start Underlying Form -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span data-bind="if: $root.IsNewDataUnderlying()">Create a new Customer Underlying Parameter</span>
                        <span data-bind="if: !$root.IsNewDataUnderlying()">Modify a Customer Underlying Parameter</span>
                    </h4>
                </div>
                <!-- modal header end -->
                <!-- modal body start -->
                <div class="modal-body overflow-visible">
                    <div id="modalUnderlyingtest">
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- modal body form start -->

                                <div id="customerUnderlyingform" class="form-horizontal" role="form">
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="StatementLetterID">
                                            Statement Letter
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="statementLetter" name="statementLetter" data-bind="value:$root.StatementLetter_u().ID, options: ddlStatementLetter_u, optionsText: 
'Name',optionsValue: 'ID',optionsCaption: 'Please Select...',enable:$root.IsStatementA()" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="UnderlyingDocument">
                                            Underlying Document
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="underlyingDocument" name="underlyingDocument" data-bind="value:UnderlyingDocument_u().ID, options: ddlUnderlyingDocument_u, optionsText: 
'CodeName',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <!--    						<span aa-bind="text:UnderlyingDocument_u().Code()"></span>-->
                                    <div data-bind="visible:UnderlyingDocument_u().ID()=='50'" class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="otherUnderlying">
                                            Other Underlying
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="otherUnderlying" name="otherUnderlying" data-bind="value: $root.OtherUnderlying_u" class="col-xs-7" />
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="DocumentType">
                                            Type of Document
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="documentType" name="documentType" data-bind="value:$root.DocumentType_u().ID, options: ddlDocumentType_u, optionsText: 'Name',optionsValue: 
'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Currency">
                                            Transaction Currency
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="currency_u" name="currency_u" data-bind="value:$root.Currency_u().ID,disable:$root.StatementLetter_u().ID()==1, options:ddlCurrency_u, 
    optionsText:'Code',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-2"></select>
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Amount">
                                            Invoice Amount
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="Amount_u" name="Amount_u" data-bind="value: $root.Amount_u,disable:$root.StatementLetter_u().ID()==1,valueUpdate:
['afterkeydown','propertychange','input']" data-rule-required="true" data-rule-number="true" class="col-xs-4 align-right" />
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Rate">Rate</label>

                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" class="col-sm-4 align-right" name="rate_u" id="rate_u" disabled="disabled" data-rule-required="true" data-rule-
number="true" data-bind="value: Rate_u" />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Eqv.USD">Eqv. USD</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" class="col-sm-4 align-right" disabled="disabled" name="eqv-usd_u" id="eqv-usd_u" data-rule-required="true" data-rule-
number="true" data-bind="value: AmountUSD_u" />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="DateOfUnderlying">
                                            Date of Underlying
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <input type="text" autocomplete="off" id="DateOfUnderlying" data-date-format="dd-M-yyyy" name="DateOfUnderlying_u" data-bind="value:DateOfUnderlying_u" 
class="form-control date-picker col-xs-6" data-rule-required="true" />
                                                <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                <label class="control-label bolder text-danger starremove">*</label>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label no-padding-right" for="ExpiredDate">
                                            Expiry Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <input type="text" autocomplete="off" id="ExpiredDate" data-date-format="dd-M-yyyy" name="ExpiredDate" data-bind="value:ExpiredDate_u" class="form-control 
date-picker col-xs-6" data-rule-required="true" />
                                                <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                <label class="control-label bolder text-danger starremove">*</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="IsDeclarationOfException">
                                            Declaration of Exception
                                        </label>
                                        <div class="col-sm-9">
                                            <label>
                                                <input name="switch-field-1" id="IsDeclarationOfException" data-bind="checked: $root.IsDeclarationOfException_u" class="ace ace-switch ace-switch-6" 
type="checkbox" />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="StartDate">
                                            Start Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <input id="StartDate" name="StartDate" data-date-format="dd-M-yyyy" data-bind="value:StartDate_u" class="form-control date-picker col-xs-6" type="text" 
autocomplete="off" />
                                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                    <label class="control-label bolder text-danger starremove">*</label>
                                                </div>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label no-padding-right" for="EndDate">
                                            End Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <input type="text" autocomplete="off" id="EndDate" name="EndDate" data-date-format="dd-M-yyyy" data-bind="value:EndDate_u" class="form-control date-picker 
col-xs-6" />
                                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                    <label class="control-label bolder text-danger starremove">*</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="ReferenceNumber">
                                            Reference Number
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="ReferenceNumber" name="ReferenceNumber" data-bind="value: $root.ReferenceNumber_u" disabled="disabled" class="col-xs-
9" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="SupplierName">
                                            Supplier Name
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="SupplierName" name="SupplierName" data-bind="value: $root.SupplierName_u" class="col-xs-8" data-rule-required="true" 
/>
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="InvoiceNumber">
                                            Invoice Number
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="invoiceNumber" name="invoiceNumber" data-bind="value: $root.InvoiceNumber_u" class="col-xs-8" data-rule-
required="true" />
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--								<div class="space-2"></div>
                                                                <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="AttachmentNo">
                                                                Attachment No.</label>
                                                                <div class="col-sm-9">
                                                                        <div class="clearfix">
                                                                            <input type="text" autocomplete="off" id="AttachmentNo" name="AttachmentNo" data-bind="value: $root.AttachmentNo_u" class="col-xs-
8"/>
                                                                        </div>
                                                                    </div>
                                                                </div>-->
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="AttachmentNo">
                                            Is This doc Purposed to replace the proforma doc
                                        </label>
                                        <div class="col-sm-9">
                                            <label>
                                                <input name="switch-field-1" id="IsProforma" data-bind="checked:IsProforma_u,disable:DocumentType_u().ID()==2" class="ace ace-switch ace-switch-6" 
type="checkbox" />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <!-- grid proforma begin -- >

                                    <!-- widget box start -->
                                    <div id="widget-box" class="widget-box" data-bind="visible:IsProforma_u()">
                                        <!-- widget header start -->
                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                            <h6>Customer Underlying Proforma Table</h6>

                                            <div class="widget-toolbar">
                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                    <i class="blue icon-filter"></i>
                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingProformaGridProperties().AllowFilter" />
                                                    <span class="lbl"></span>
                                                </label>
                                                <a href="#" data-bind="click: GetDataUnderlyingProforma" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                    <i class="blue icon-refresh"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- widget header end -->
                                        <!-- widget body start -->
                                        <div class="widget-body">
                                            <!-- widget main start -->
                                            <div class="widget-main padding-0">
                                                <!-- widget slim control start -->
                                                <div class="slim-scroll" data-height="400">
                                                    <!-- widget content start -->
                                                    <div class="content">

                                                        <!-- table responsive start -->
                                                        <div class="table-responsive">
                                                            <div class="dataTables_wrapper" role="grid">
                                                                <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                                    <thead>
                                                                        <tr data-bind="with: UnderlyingProformaGridProperties">
                                                                            <th style="width: 50px">No.</th>
                                                                            <th style="width: 50px">Select</th>
                                                                            <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                            <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying 
Document</th>
                                                                            <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                            <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                            <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                            <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                            <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                            <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th>
                                                                            <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <thead data-bind="visible: UnderlyingProformaGridProperties().AllowFilter" class="table-filter">
                                                                        <tr>
                                                                            <th class="clear-filter">
                                                                                <a href="#" data-bind="click: ProformaClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear 
Filters">
                                                                                    <i class="green icon-trash"></i>
                                                                                </a>
                                                                            </th>
                                                                            <th class="clear-filter"></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterStatementLetter, event: { 
    change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterUnderlyingDocument, 
    event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterDocumentType, event: { 
    change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterCurrency, event: { 
    change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterAmount, event: { change: 
UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.ProformaFilterDateOfUnderlying, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterSupplierName, event: { 
    change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterReferenceNumber, event: { 
    change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.ProformaFilterExpiredDate, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody data-bind="foreach: {data: CustomerUnderlyingProformas, as: 'ProformasData'}, visible: CustomerUnderlyingProformas().length > 0">
                                                                        <tr data-toggle="modal">
                                                                            <td><span data-bind="text: RowID"></span></td>
                                                                            <td align="center">
                                                                                <input type="checkbox" id="isSelectedProforma" name="isSelect" data-bind="checked:ProformasData.IsSelectedProforma, event: 
{change:function(data){$root.onSelectionProforma($index(),$data)}}" /></td>
                                                                            <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                            <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                            <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                            <td><span data-bind="text: Currency.Code"></span></td>
                                                                            <td><span data-bind="text: Amount"></span></td>
                                                                            <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                                            <td><span data-bind="text: SupplierName"></span></td>
                                                                            <td><span data-bind="text: ReferenceNumber"></span></td>
                                                                            <td><span data-bind="text: $root.LocalDate(ExpiredDate,true,false)"></span></td>
                                                                        </tr>
                                                                    </tbody>
                                                                    <tbody data-bind="visible: CustomerUnderlyingProformas().length == 0">
                                                                        <tr>
                                                                            <td colspan="11" class="text-center">No entries available.
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <!-- table responsive end -->
                                                    </div>
                                                    <!-- widget content end -->
                                                </div>
                                                <!-- widget slim control end -->
                                                <!-- widget footer start -->
                                                <div class="widget-toolbox padding-8 clearfix">
                                                    <div class="row" data-bind="with: UnderlyingProformaGridProperties">
                                                        <!-- pagination size start -->
                                                        <div class="col-sm-6">
                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                Showing
                                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" 
data-placement="bottom" title="Showing Page Entries"></select>
                                                                rows of <span data-bind="text: Total"></span>
                                                                entries
                                                            </div>
                                                        </div>
                                                        <!-- pagination size end -->
                                                        <!-- pagination page jump start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                Page
                                                                <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-
rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                of <span data-bind="text: TotalPages"></span>
                                                            </div>
                                                        </div>
                                                        <!-- pagination page jump end -->
                                                        <!-- pagination navigation start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                <ul class="pagination">
                                                                    <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                            <i class="icon-double-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                            <i class="icon-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                            <i class="icon-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                            <i class="icon-double-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <!-- pagination navigation end -->

                                                    </div>
                                                </div>
                                                <!-- widget footer end -->

                                            </div>
                                            <!-- widget main end -->
                                        </div>
                                        <!-- widget body end -->

                                    </div>
                                    <!-- widget box end -->
                                    <!-- grid proforma end -- >
                                    </div>

                                    <!-- modal body form end -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal body end -->
                    <!-- modal footer start -->
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-primary" data-bind="click: $root.save_u, visible: $root.IsNewDataUnderlying()">
                            <i class="icon-save"></i>
                            Save
                        </button>
                        <button class="btn btn-sm btn-primary" data-bind="click: $root.update_u, visible: !$root.IsNewDataUnderlying() && !$root.IsUtilize_u()">
                            <i class="icon-edit"></i>
                            Update
                        </button>
                        <button class="btn btn-sm btn-warning" data-bind="click: $root.delete_u, visible: !$root.IsNewDataUnderlying() && !$root.IsUtilize_u()">
                            <i class="icon-trash"></i>
                            Delete
                        </button>
                        <button class="btn btn-sm" data-bind="click: $root.cancel_u" data-dismiss="modal">
                            <i class="icon-remove"></i>
                            Cancel
                        </button>
                    </div>
                    <!-- modal footer end -->

                </div>
            </div>
        </div>
        <!-- modal form end underlying form -->
    </div>

</div>

<%--IPE ADD UDIN--%>
<div id="transaction-child-ipe" style="display: none;">
    <h3 class="header smaller no-margin-top lighter dark">Transaction</h3>

    <div class="modal-body overflow-visible">
        <div class="row" data-bind="with: TransactionModel">
            <div class="col-xs-12">
                <div id="customer-form-ipe" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right">
                            <span data-bind="text: $root.LocalDate(CreateDate, true, true)"></span>
                        </label>

                        <label class="col-sm-4 pull-right">
                            <b>Application ID</b> : <span data-bind="text: ApplicationID"></span>
                            <br />
                            <b>User Name</b> : <span data-bind="text: $root.SPUser().DisplayName"></span>
                        </label>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right">Top Urgent</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsTopUrgent ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                        <label class="col-sm-2 control-label bolder no-padding-right">Top Urgent Chain</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsTopUrgentChain ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>

                        <label class="col-sm-2 control-label bolder no-padding-right">Normal</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsNormal? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="customer-name">Customer Name</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Customer.Name"></span>
                            <span class="label label-warning arrowed" data-bind="if: IsNewCustomer">
                                <i class="icon-warning-sign bigger-120"></i>
                                New Customer
                            </span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="cif">CIF</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Customer.CIF"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="product">Product</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Product.Code"></span>
                            (<span data-bind="text: Product.Name"></span>)
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="currency">Transaction Currency</label>

                        <label class="col-sm-8">
                            <span data-bind="text: Currency.Code"></span>
                            (<span data-bind="text: Currency.Description"></span>)
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="trxn-amount">Trxn Amount</label>

                        <label class="col-sm-9">
                            <span data-bind="text: formatNumber(Amount)"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="rate">IDR Mid Rate</label>

                        <label class="col-sm-9">
                            <span data-bind="text: formatNumber_r(Rate)"></span>
                        </label>
                    </div>
                    <!--Permintaan User
                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="rate">Transaction Rate</label>

                        <label class="col-sm-9" data-bind="if:TrxRate !=null || TrxRate != undefined ">
                            <span data-bind="text: formatNumber_r(TrxRate)"></span>
                        </label>
                    </div>
                    -->
                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="eqv-usd">Eqv. USD</label>

                        <label class="col-sm-9">
                            <span data-bind="text: formatNumber_r(AmountUSD)"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="channel">Channel</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Channel.Name"></span>
                        </label>
                    </div>
                     
                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="debit-acc-number">Debit Acc Number</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Account.AccountNumber"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="debit-acc-ccy">Debit Acc CCY</label>

                        <label class="col-sm-9">
                            <span name="debit-acc-ccy-ipe" id="debit-acc-ccy-code-ipe" data-bind="text: DebitCurrency.Code"></span>
                            (<span name="debit-acc-ccy-ipe" id="debit-acc-ccy-desc-ipe" data-bind="text: DebitCurrency.Description"></span>)
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="Debit Sundry">Debit Sundry</label>
                        <label data-bind="if: Sundry !=null" class="col-sm-8">
                            <span id="DebitSundry" data-bind="text: Sundry.OABAccNo"></span>
                        </label>
                    </div>
                    <!--Permintaan User
                    <div data-bind="visible: Product.Name == 'OTT' && Currency.Code !='IDR', attr: { class: 'form-group form-line no-margin-bottom no-padding-bottom' }">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="Nostro">Nostro</label>
                        <label data-bind="if:Nostro !=null" class="col-sm-8">
                            <span name="Nostro" id="Nostro" data-bind="text: Nostro.NostroUsed"></span>
                        </label>
                    </div>
                    END-->
                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="application-date">Application Date</label>

                        <label class="col-sm-9">
                            <span data-bind="text: $root.LocalDate(ApplicationDate, true)"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="biz-segment">Biz Segment</label>

                        <label class="col-sm-9">
                            <span data-bind="text: BizSegment.Description"></span>
                            (<span data-bind="text: BizSegment.Name"></span>)
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom" data-bind="if: Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR' && ProductType !=null">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="product">Product Type</label>

                        <label class="col-sm-9">
                            <span data-bind="text: ProductType.Code"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="bene-name">Bene Name</label>

                        <label class="col-sm-9">
                            <span data-bind="text: BeneName"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="bene-bank">Bene Bank</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Bank.Description"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="bank-code">Swift Code/Bank Code</label>

                        <label class="col-sm-1">
                            <span data-bind="text: Bank.SwiftCode"></span>
                        </label>

                        <label class="col-sm-1 control-label bolder no-padding-right" for="bank-code-branch">Branch Code</label>
                        <label data-bind="if:Branch !=null" class="col-sm-1">
                            <span name="bank-code-branch" id="bank-code-branch" data-bind="text: Branch.Code"></span>
                            (<span name="bank-code-branch" id="bank-code-branch" data-bind="text: Branch.Name"></span>)
                        </label>

                        <label class="col-sm-1 control-label bolder no-padding-right" for="bank-code-city">City Code</label>

                        <label data-bind="if:City !=null" class="col-sm-3">
                            <span name="bank-code-cityCode" id="bank-code-cityCode" data-bind="text: City.CityCode"></span>
                            <span name="bank-code-cityDesc" id="bank-code-cityDesc" data-bind="text: City.Description"></span>
                        </label>

                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="bene-acc-number">Bene Acc Number</label>

                        <label class="col-sm-9">
                            <span data-bind="text: BeneAccNumber"></span>
                        </label>
                    </div>

                    <div data-bind="if: BankCharges != null && (Product.Code == 'OT' || Product.Code == 'RT' || Product.Code == 'SK'), attr: { class: 'form-group form-line no-margin-bottom no-padding-bottom' 
}">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="bank-charges">Bank Charges</label>

                        <label class="col-sm-4">
                            <span data-bind="text: BankCharges.Code"></span>
                        </label>
                    </div>

                    <div data-bind="if: AgentCharges != null && Product.Code == 'RT', attr: { class: 'form-group form-line no-margin-bottom no-padding-bottom' }">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="agent-charges">Agent Charges</label>

                        <label class="col-sm-4">
                            <span data-bind="text: AgentCharges.Code"></span>
                        </label>
                    </div>

                    <div data-bind="if: Product.Code == 'SK', visible: Product.Code == 'SK', attr: { class: 'form-group form-line no-margin-bottom no-padding-bottom' }">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="type">Type</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Type"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Citizen</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsCitizen ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Resident</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsResident ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom" data-bind="visible:Product.Code == 'SK' || Product.Code == 'OT' || Product.Code == 'OV' || Product.Code == 'RT'">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="type">Beneficiary Resident</label>

                        <label class="col-sm-8">
                            <label data-bind=" visible: IsBeneficiaryResident == true">
                                <span id="IsbeneTrue">Resident</span>
                            </label>
                            <label data-bind="visible: IsBeneficiaryResident == false">
                                <span id="IsbeneFalse">Non Resident</span>
                            </label>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class=" col-sm-3 control-label bolder no-padding-right" for="type">
                            Beneficiary Country Code
                        </label>
                        <label class="col-sm-8" data-bind="if: BeneficiaryCountry != null">
                            <span id="beneficiary-country-code" data-bind="text: BeneficiaryCountry.Code"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="ChAccName">Charging Account Name</label>
                        <label class="col-sm-8">
                            <span id="ChAccName" data-bind="text: ChargingAccountName"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="ChAccBank">Charging Account Bank</label>
                        <label class="col-sm-8">
                            <span id="ChAccBank" data-bind="text: ChargingAccountBank"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="ChAccount">Charging Account</label>
                        <label class="col-sm-3">
                            <span id="ChAccount" data-bind="text: ChargingACCNumber "></span>
                        </label>
                        <label class="col-sm-3 control-label bolder no-padding-right">CCY</label>
                        <label class="col-sm-2" data-bind="if:ChargingAccountCurrency !=null && ChargingAccountCurrency !=0 ">
                            <span id="ChAccountCCY" data-bind="text: $root.ChargingAccCurrency().Code"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="beneficiary-code">Beneficiary Business Type</label>

                        <label class="col-sm-8" data-bind="if: BeneficiaryBusines != null">
                            <label class="col-sm-8">
                                <span id="beneficiary-code" data-bind="text: BeneficiaryBusines.Code"></span>
                                (<span id="beneficiary-desc" data-bind="text: BeneficiaryBusines.Description"></span>)
                            </label>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="beneficiary-addr">Beneficiary Address</label>
                        <div data-bind="ifnot: BeneficiaryAddress == '' ">
                            <div data-bind="if: BeneficiaryAddress != ''">
                                <label class="col-sm-8">
                                    <span id="beneficiary-addr" data-bind="text: BeneficiaryAddress"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class=" col-sm-3 control-label bolder no-padding-right" for="type">
                            Transaction Relationship
                        </label>
                        <label class="col-sm-8" data-bind="if: TransactionRelationship != null">
                            <span id="trrelation" data-bind="text: TransactionRelationship.Code"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="lld-code">LLD Code</label>

                        <label class="col-sm-9" data-bind="if:LLD != null">
                            <span data-bind="text: LLD.Code + ' (' + LLD.Description + ')'"></span>
                        </label>
                    </div>

                      <%-- start LLD Document Code by azam --%>              
                    <div class="form-group form-line no-margin-bottom no-padding-bottom" data-bind="visible : $root.IsLimit">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="rate">LLD Document Code</label>

                        <label class="col-sm-9">
                              <span data-bind="text:LLDDocumentCode"></span> 
				(<span id="Description" data-bind="text: Description"></span>)

                        </label>
                    </div>


                    <div class="form-group form-line no-margin-bottom no-padding-bottom" data-bind="visible : $root.IsLimit">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="rate">LLD Underlying Amount</label>

                        <label class="col-sm-9">
                             <span data-bind="text:formatNumber(LLDUnderlyingAmount)"></span> 
                        </label>
                    </div>



                    <%-- end LLD Document Code --%>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="underlying-code">Underlying Code</label>
                        <div data-bind="ifnot:UnderlyingDoc==null">
                            <label class="col-sm-8">
                                <span id="underlying-code" name="underlying-code" data-bind="text: UnderlyingDoc.Code"></span>
                                (<span id="underlying-code1" name="underlying-code" data-bind="text: UnderlyingDoc.Name"></span>)
                            </label>
                        </div>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="ComCode">Compliance Code</label>
                        <div data-bind="ifnot:Compliance==null">
                            <label class="col-sm-8">
                                <span id="ComCode-name" data-bind="text: Compliance.Name"></span>
                                <span id="ComCode-description" data-bind="text: ' - ' + Compliance.Description"></span>
                            </label>
                        </div>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom" data-bind="if: OtherUnderlyingDoc != null">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="underlying-desc">Other Underlying</label>

                        <div class="col-xs-8 align-left">
                            <div class="clearfix">
                                <textarea class="col-lg-5" id="underlying-desc" rows="4" data-rule-required="true" data-bind="value: OtherUnderlyingDoc"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Payment Details</label>

                        <label class="col-sm-9">
                            <span data-bind="text: PaymentDetails"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom" data-bind="visible: Product.Name == 'SKN' || Product.Name == 'RTGS' || Product.Name == 'OTT'">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="purpose-detail">Waive Charges Options</label>
                        <label class="col-sm-2 control-label bolder no-padding-right" for="purpose-detail">CBG Customer</label>
                        <label class="col-sm-2" data-bind="if: CBGCustomer !=null">
                            <span id="purpose-detail" data-bind="text: CBGCustomer.Code"></span>
                        </label>
                        <label class="col-sm-2 control-label bolder no-padding-right" for="purpose-detail">Transaction Using Debit Sundry</label>
                        <label class="col-sm-2" data-bind="if: TransactionUsingDebitSundry != null">
                            <span id=" purpose-detail" data-bind="text:TransactionUsingDebitSundry.Code"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="check-callback-required">Callback is Required</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsCallbackRequired ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Signature Verification</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsSignatureVerified ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Dormant Account</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsDormantAccount ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Freeze Account</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsFreezeAccount ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <%--<div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="check-statement-Letter-copy">Statement Letter Copy</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsStatementLetterCopy ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>--%>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Others Exceptional Handling</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Others"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="is-pending-documents">Documents Completeness</label>

                        <label class="col-sm-9">
                            <input name="is-pending-documents" class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: $root.UpdatePendingDocuments().IsCompleted , click: 
IsDocumentCompleteCheck()" /><!--, disable: !$root.IsEditable()-->
                            <span class="lbl"></span>
                        </label>
                    </div>
                </div>

                <div class="space-4"></div>

                <!--
                                if: Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR'
                -->
                <div class="padding-4" data-bind="if: Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR'">
                    <h3 class="header smaller lighter dark">Un-utilize Underlying Documents
                        <%--<button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.NewDataUnderlying, disable: !$root.IsEditable()">
                            <i class="icon-plus"></i>
                            Add Underlying
                        </button>--%>
                    </h3>
                    <!-- widget box start -->
                    <div id="widget-box-ipe" class="widget-box">
                        <!-- widget header start -->
                        <div class="widget-header widget-hea1der-small header-color-dark">
                            <h6>Underlying Table</h6>

                            <div class="widget-toolbar">
                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                    <i class="blue icon-filter"></i>
                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: $root.UnderlyingGridProperties().AllowFilter" />
                                    <span class="lbl"></span>
                                </label>
                                <a href="#" data-bind="click: $root.GetDataUnderlying" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                    <i class="blue icon-refresh"></i>
                                </a>
                            </div>
                        </div>
                        <!-- widget header end -->
                        <!-- widget body start -->
                        <div class="widget-body">
                            <!-- widget main start -->
                            <div class="widget-main padding-0">
                                <!-- widget slim control start -->
                                <div class="slim-scroll" data-height="200">
                                    <!-- widget content start -->
                                    <div class="content">

                                        <!-- table responsive start -->
                                        <div class="table-responsive">
                                            <div class="dataTables_wrapper" role="grid">
                                                <table id="Customer Underlying-table-ipe" class="table table-striped table-bordered table-hover dataTable">
                                                    <thead>
                                                        <tr data-bind="with: $root.UnderlyingGridProperties">
                                                            <th style="width: 50px">No.</th>
                                                            <th style="width: 50px">Utilize</th>
                                                            <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                            <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                            <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                            <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                            <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                            <th data-bind="click: function () { Sorting('AvailableAmount'); }, css: GetSortedColumn('AvailableAmount')">Available Amount in USD</th>
                                                            <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                            <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                            <th data-bind="click: function () { Sorting('AttachmentNo'); }, css: GetSortedColumn('AttachmentNo')">Attachment No</th>
                                                            <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th>
                                                            <th style="width: 50px">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <thead data-bind="visible: $root.UnderlyingGridProperties().AllowFilter" class="table-filter">
                                                        <tr>
                                                            <th class="clear-filter">
                                                                <a href="#" data-bind="click: $root.UnderlyingClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear 
Filters">
                                                                    <i class="green icon-trash"></i>
                                                                </a>
                                                            </th>
                                                            <th class="clear-filter"></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterStatementLetter, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterUnderlyingDocument, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterDocumentType, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterCurrency, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterAmount, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterAvailableAmount, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.UnderlyingFilterDateOfUnderlying, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterSupplierName, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterAttachmentNo, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterReferenceNumber, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th class="clear-filter"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody data-bind="foreach: $root.CustomerUnderlyings, visible: $root.CustomerUnderlyings().length > 0">
                                                        <tr>
                                                            <td><span data-bind="text: RowID"></span></td>
                                                            <td align="center">
                                                                <input type="checkbox" id="isUtilize-ipe" name="isUtilize" data-bind="checked:IsEnable,event: {change:function(data){$root.onSelectionUtilize
($index(),$data)}}, disable: !$root.IsEditable()" /></td>
                                                            <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                            <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                            <td><span data-bind="text: DocumentType.Name"></span></td>
                                                            <td><span data-bind="text: Currency.Code"></span></td>
                                                            <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                                            <td align="right"><span data-bind="text: formatNumber(AvailableAmount)"></span></td>
                                                            <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                            <td><span data-bind="text: SupplierName"></span></td>
                                                            <td><span data-bind="text: AttachmentNo"></span></td>
                                                            <td><span data-bind="text: ReferenceNumber"></span></td>
                                                            <td><span class="label label-info arrowed-in-right arrowed" data-bind="click: $root.GetUnderlyingSelectedRow,disable: !$root.IsEditable()">view</span></td>
                                                        </tr>
                                                    </tbody>
                                                    <tbody data-bind="visible: $root.CustomerUnderlyings().length == 0">
                                                        <tr>
                                                            <td colspan="13" class="text-center">No entries available.
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- table responsive end -->
                                    </div>
                                    <!-- widget content end -->
                                </div>
                                <!-- widget slim control end -->
                                <!-- widget footer start -->

                                <div class="widget-toolbox padding-8 clearfix">
                                    <div class="row" data-bind="with: $root.UnderlyingGridProperties">
                                        <!-- pagination size start -->
                                        <div class="col-sm-6">
                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                Showing
                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-
placement="bottom" title="Showing Page Entries"></select>
                                                rows of <span data-bind="text: Total"></span>
                                                entries
                                            </div>
                                        </div>
                                        <!-- pagination size end -->
                                        <!-- pagination page jump start -->
                                        <div class="col-sm-3">
                                            <div class="dataTables_paginate paging_bootstrap">
                                                Page
                                                <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" 
data-placement="bottom" title="Jump to Page" />
                                                of <span data-bind="text: TotalPages"></span>
                                            </div>
                                        </div>
                                        <!-- pagination page jump end -->
                                        <!-- pagination navigation start -->
                                        <div class="col-sm-3">
                                            <div class="dataTables_paginate paging_bootstrap">
                                                <ul class="pagination">
                                                    <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                            <i class="icon-double-angle-left"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                            <i class="icon-angle-left"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                            <i class="icon-angle-right"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                            <i class="icon-double-angle-right"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- pagination navigation end -->

                                    </div>
                                </div>

                                <!-- widget footer end -->

                            </div>
                            <!-- widget main end -->
                        </div>
                        <!-- widget body end -->
                    </div>
                    <!-- widget box end -->


                </div>
                <!--- Added form custome Customer Underlying End -->
                <div class="space-10"></div>
                <div data-bind="if: Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR'">
                    <div class="form-horizontal" role="form">

                        <div class="form-group">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="underlying-code">Underlying Code</label>
                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <select class="col-sm-12" id="underlying-code-ipe" name="underlying-code" data-bind="options: $root.Parameter().UnderlyingDocs, optionsText: function(item) { if(item.ID != 
null) return item.Code + ' (' + item.Name + ')'} , optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().UnderlyingDoc, disable: !$root.IsEditable()" data-rule-
required="true"></select>

                                </div>
                            </div>
                        </div>

                        <div class="form-group" data-bind="visible: $root.isNewUnderlying()">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="underlying-desc">Other Underlying</label>

                            <div class="col-xs-9 align-left">
                                <div class="clearfix">
                                    <textarea class="col-lg-5" id="underlying-desc-ipe" rows="4" data-rule-required="true" data-bind="value: OtherUnderlyingDoc, enable: $root.isNewUnderlying, disable: !
$root.IsEditable()"></textarea>

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="utilization-amount">Utilization Amount</label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="text" autocomplete="off" class="col-sm-4 align-right" name="utilization-amount" id="utilization-amount" rule-required="true" disabled="disabled" data-rule-
number="true" data-bind="value: formatNumber($root.UtilizationAmounts())" />
                                </div>
                            </div>
                        </div>

                        <!--<div class="form-group">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="totalTrx">Total FX-IDR Transaction in USD</label>
                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="text" autocomplete="off" class="col-sm-4 align-right" name="totalTrx" id="totalTrx" disabled="disabled" data-bind="value:TotalTransFX" />
                                </div>
                            </div>
                        </div>-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="tz-number">TZ Number</label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="text" autocomplete="off" class="col-sm-6" name="tz-number" id="tz-number-ipe" data-bind="value: TZNumber, disable: !$root.IsEditable()" />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="space-4"></div>
                <div class="padding-4">
                    <h3 class="header smaller lighter dark">Instruction Form and Docs
                        <button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.UploadDocumentUnderlying, disable: false,visible:(Currency.Code != 'IDR' && $root.CurrencyDep().Code == 
'IDR')" onclick="return false">
                            <i class="icon-plus"></i>
                            Attach
                        </button>
                        <button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.UploadDocument, disable: false,visible:!(Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR')" 
onclick="return false">
                            <i class="icon-plus"></i>
                            Attach
                        </button>
                    </h3>
                    <!--data-bind="visible:!$root.IsUnderlyingMode()" -->
                    <div class="dataTables_wrapper" role="grid">
                        <table class="table table-striped table-bordered table-hover dataTable">
                            <thead>
                                <tr>
                                    <th style="width: 50px">No.</th>
                                    <th>File Name</th>
                                    <th>Purpose of Docs</th>
                                    <th>Type of Docs</th>
                                    <th>Modified</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody data-bind="foreach: $root.Documents, visible: $root.Documents().length > 0">
                                <tr>
                                    <!--data-bind="click: $root.EditDocument"-->
                                    <td><span data-bind="text: $index() +1"></span></td>
                                    <td><span data-bind="text: FileName"></span></td>
                                    <td><span data-bind="text: Purpose.Name"></span></td>
                                    <td><span data-bind="text: Type.Name"></span></td>
                                    <td><span data-bind="text: moment(DocumentPath.lastModifiedDate).format(config.format.dateTime)"></span></td>
                                    <!--$root.LocalDate(LastModifiedDate)-->
                                    <td><a href="#" data-bind="click: $root.RemoveDocument">Remove</a></td>
                                </tr>
                            </tbody>
                            <tbody data-bind="visible: $root.Documents().length == 0">
                                <tr>
                                    <td colspan="6" align="center">No entries available.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="space-10"></div>

                    <!--- Grid Underlying Proforma Start -->
                    <!-- widget header start -->
                    <div data-bind="if: Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR'">
                        <h3 class="header smaller lighter dark">Underlying Docs
                        </h3>
                        <div class="widget-header widget-hea1der-small header-color-dark">
                            <h6>Attach File Table</h6>
                            <div class="widget-toolbar">
                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                    <i class="blue icon-filter"></i>
                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: $root.AttachGridProperties().AllowFilter" />
                                    <span class="lbl"></span>
                                </label>
                                <a href="#" data-bind="click: $root.GetDataAttachFile" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                    <i class="blue icon-refresh"></i>
                                </a>
                            </div>
                        </div>
                        <!-- widget header end -->
                        <div class="widget-body">
                            <!-- widget main start -->
                            <div class="widget-main padding-0">
                                <!-- widget slim control start -->
                                <div class="slim-scroll" data-height="200">
                                    <!-- widget content start -->
                                    <div class="content">

                                        <!-- table responsive start -->
                                        <div class="table-responsive">
                                            <div class="dataTables_wrapper" role="grid">
                                                <table id="Customer Underlying-table-ipe" class="table table-striped table-bordered table-hover dataTable">
                                                    <thead>
                                                        <tr data-bind="with: $root.AttachGridProperties">
                                                            <th style="width: 50px">No.</th>
                                                            <th data-bind="click: function () { Sorting('FileName'); }, css: GetSortedColumn('FileName')">File Name</th>
                                                            <th data-bind="click: function () { Sorting('DocumentPurpose'); }, css: GetSortedColumn('DocumentPurpose')">Document Purpose</th>
                                                            <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Update</th>
                                                            <th data-bind="click: function () { Sorting('DocumentRefNumber'); }, css: GetSortedColumn('DocumentRefNumber')">Document Ref Number</th>
                                                            <th style="width: 0px; display: none"></th>
                                                            <th style="width: 50px">Delete</th>
                                                        </tr>
                                                    </thead>
                                                    <thead data-bind="visible: $root.AttachGridProperties().AllowFilter" class="table-filter">
                                                        <tr>
                                                            <th class="clear-filter">
                                                                <a href="#" data-bind="click: $root.AttachClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                    <i class="green icon-trash"></i>
                                                                </a>
                                                            </th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.AttachFilterFileName, event: { change: 
$root.AttachGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.AttachFilterDocumentPurpose, event: { change: 
$root.AttachGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.AttachFilterLastModifiedDate, event: { change: $root.AttachGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.AttachFilterDocumentRefNumber, event: { change: 
$root.AttachGridProperties().Filter }" /></th>
                                                            <th class="clear-filter" style="display: none"></th>
                                                            <th class="clear-filter"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody data-bind="foreach: $root.CustomerUnderlyingFiles, visible: $root.CustomerUnderlyingFiles().length > 0">
                                                        <tr data-bind="click: $root.GetAttachSelectedRow" data-toggle="modal">
                                                            <td><span data-bind="text: RowID"></span></td>
                                                            <td><span data-bind="text: FileName"></span></td>
                                                            <td><span data-bind="text: DocumentPurpose.Name"></span></td>
                                                            <td><span data-bind="text: $root.LocalDate(LastModifiedDate,false,false)"></span></td>
                                                            <td><span data-bind="text: DocumentRefNumber"></span></td>
                                                            <td style="width: 0px; display: none"><span data-bind="text: DocumentPath"></span></td>
                                                            <td align="center">
                                                                <!--<a href="#" data-bind="click:$root.delete_a" class="tooltip-info" data-rel="tooltip" data-placement="right">
                                                                    <i class="green icon-trash"></i>
                                                                </a>-->
                                                                <i class="gray icon-trash"></i>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                    <tbody data-bind="visible: $root.CustomerUnderlyingFiles().length == 0">
                                                        <tr>
                                                            <td colspan="10" class="text-center">No entries available.
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- table responsive end -->
                                    </div>
                                    <!-- widget content end -->
                                </div>
                                <!-- widget slim control end -->
                                <!-- widget footer start -->
                                <div class="widget-toolbox padding-8 clearfix">
                                    <div class="row" data-bind="with: $root.AttachGridProperties">
                                        <!-- pagination size start -->
                                        <div class="col-sm-6">
                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                Showing
                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-
placement="bottom" title="Showing Page Entries"></select>
                                                rows of <span data-bind="text: Total"></span>
                                                entries
                                            </div>
                                        </div>
                                        <!-- pagination size end -->
                                        <!-- pagination page jump start -->
                                        <div class="col-sm-3">
                                            <div class="dataTables_paginate paging_bootstrap">
                                                Page
                                                <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" 
data-placement="bottom" title="Jump to Page" />
                                                of <span data-bind="text: TotalPages"></span>
                                            </div>
                                        </div>
                                        <!-- pagination page jump end -->
                                        <!-- pagination navigation start -->
                                        <div class="col-sm-3">
                                            <div class="dataTables_paginate paging_bootstrap">
                                                <ul class="pagination">
                                                    <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                            <i class="icon-double-angle-left"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                            <i class="icon-angle-left"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                            <i class="icon-angle-right"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                            <i class="icon-double-angle-right"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- pagination navigation end -->

                                    </div>
                                </div>
                                <!-- widget footer end -->

                            </div>
                            <!-- widget main end -->
                        </div>
                    </div>
                    <!-- widget body end -->
                    <!--- Grid Underlying Proforma End -->

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 align-right">
            <button class="btn btn-sm btn-primary" data-bind="click: Submit, disable: $root.IsEditable()">
                <i class="icon-save"></i>
                Submit
            </button>

            <button class="btn btn-sm btn-success" data-bind="click: $root.cancel_detail" data-dismiss="modal">
                <i class="icon-remove"></i>
                Cancel
            </button>
        </div>
    </div>

    <!-- modal form start Attach Document Underlying Form start -->
    <div id="modal-form-Attach-ipe" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <!-- modal header start Attach FIle Form -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span>Added Attachment File</span>
                    </h4>
                </div>
                <!-- modal header end Attach file -->
                <!-- modal body start Attach File -->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div id="customer-form" class="form-horizontal" role="form">
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="Name">
                                        Purpose of Doc
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentPurpose-ipe" name="documentPurpose" data-bind="value:$root.DocumentPurpose_a().ID, options: ddlDocumentPurpose_a, optionsText: 
'Name',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="description">
                                        Type of Doc
                                    </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentType-ipe" name="documentType" data-bind="value:$root.DocumentType_a().ID, options: ddlDocumentType_a, optionsText: 'Name',optionsValue: 
'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-path">Document</label>

                                    <div class="col-sm-6">
                                        <div class="clearfix">
                                            <input type="file" id="document-path-ipe" name="document-path" data-bind="file: DocumentPath_a" class="col-xs-7" data-rule-required="true" data-rule-value="true" 
/><!---->
                                            <!--<span data-bind="text: DocumentPath"></span>-->
                                        </div>
                                    </div>
                                    <label class="control-label bolder text-danger">*</label>
                                </div>
                            </div>
                            <!-- modal body form Attach File End -->
                            <!-- widget box Underlying Attach start -->
                            <div id="widget-box" class="widget-box">
                                <!-- widget header Underlying Attach start -->
                                <div class="widget-header widget-hea1der-small header-color-dark">
                                    <h6>Underlying Form Table</h6>

                                    <div class="widget-toolbar">
                                        <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                            <i class="blue icon-filter"></i>
                                            <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingAttachGridProperties().AllowFilter" />
                                            <span class="lbl"></span>
                                        </label>
                                        <a href="#" data-bind="click: GetDataUnderlyingAttach" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                            <i class="blue icon-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                                <!-- widget header end -->
                                <!-- widget body underlying Attach start -->
                                <div class="widget-body">
                                    <!-- widget main start -->
                                    <div class="widget-main padding-0">
                                        <!-- widget slim control start -->
                                        <div class="slim-scroll" data-height="400">
                                            <!-- widget content start -->
                                            <div class="content">
                                                <!-- table responsive start -->
                                                <div class="table-responsive">
                                                    <div class="dataTables_wrapper" role="grid">
                                                        <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                            <thead>
                                                                <tr data-bind="with: $root.UnderlyingAttachGridProperties">
                                                                    <!-- <th style="width:50px">Select</th> -->
                                                                    <th style="width: 50px">No.</th>
                                                                    <th style="width: 50px">Select</th>
                                                                    <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                    <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                    <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                    <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                    <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                    <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                    <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                    <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th>
                                                                    <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                                </tr>
                                                            </thead>
                                                            <thead data-bind="visible: $root.UnderlyingAttachGridProperties().AllowFilter" class="table-filter">
                                                                <tr>
                                                                    <th class="clear-filter">
                                                                        <a href="#" data-bind="click: $root.UnderlyingAttachClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" 
title="Clear Filters">
                                                                            <i class="green icon-trash"></i>
                                                                        </a>
                                                                    </th>
                                                                    <th class="clear-filter"></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterStatementLetter, event: { 
    change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterUnderlyingDocument, 
    event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterDocumentType, event: { 
    change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterCurrency, event: { 
    change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterAmount, event: { change: 
UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.UnderlyingAttachFilterDateOfUnderlying, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterSupplierName, event: { 
    change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterReferenceNumber, event: { 
    change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.UnderlyingAttachFilterExpiredDate, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody data-bind="foreach: {data: $root.CustomerAttachUnderlyings, as: 'AttachData'}, visible: $root.CustomerAttachUnderlyings().length > 0">
                                                                <tr data-toggle="modal">
                                                                    <td><span data-bind="text: RowID"></span></td>
                                                                    <td align="center">
                                                                        <input type="checkbox" id="isSelected2-ipe" name="isSelect2" data-bind="checked:IsSelectedAttach,event:{change:function(data)
{$root.onSelectionAttach($index(),$data)}}, disable: !($root.SelectingUnderlying())" /></td>
                                                                    <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                    <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                    <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                    <td><span data-bind="text: Currency.Code"></span></td>
                                                                    <td><span data-bind="text: Amount"></span></td>
                                                                    <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                                    <td><span data-bind="text: SupplierName"></span></td>
                                                                    <td><span data-bind="text: ReferenceNumber"></span></td>
                                                                    <td><span data-bind="text: $root.LocalDate(ExpiredDate,true,false)"></span></td>
                                                                </tr>
                                                            </tbody>
                                                            <tbody data-bind="visible: $root.CustomerAttachUnderlyings().length == 0">
                                                                <tr>
                                                                    <td colspan="11" class="text-center">No entries available.
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- table responsive end -->
                                            </div>
                                            <!-- widget content end -->
                                        </div>
                                        <!-- widget slim control end -->
                                        <!-- widget footer start -->
                                        <div class="widget-toolbox padding-8 clearfix">
                                            <div class="row" data-bind="with: UnderlyingAttachGridProperties">
                                                <!-- pagination size start -->
                                                <div class="col-sm-6">
                                                    <div class="dataTables_paginate paging_bootstrap pull-left">
                                                        Showing
                                                        <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-
placement="bottom" title="Showing Page Entries"></select>
                                                        rows of <span data-bind="text: Total"></span>
                                                        entries
                                                    </div>
                                                </div>
                                                <!-- pagination size end -->
                                                <!-- pagination page jump start -->
                                                <div class="col-sm-3">
                                                    <div class="dataTables_paginate paging_bootstrap">
                                                        Page
                                                        <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-
rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                        of <span data-bind="text: TotalPages"></span>
                                                    </div>
                                                </div>
                                                <!-- pagination page jump end -->
                                                <!-- pagination navigation start -->
                                                <div class="col-sm-3">
                                                    <div class="dataTables_paginate paging_bootstrap">
                                                        <ul class="pagination">
                                                            <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                    <i class="icon-double-angle-left"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                    <i class="icon-angle-left"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                    <i class="icon-angle-right"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                    <i class="icon-double-angle-right"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- pagination navigation end -->

                                            </div>
                                        </div>
                                        <!-- widget footer end -->

                                    </div>
                                    <!-- widget main end -->
                                </div>
                                <!-- widget body end -->
                            </div>
                            <!-- widget box end -->



                        </div>
                    </div>
                </div>
                <!-- modal body end Attach File -->
                <!-- modal footer start Attach File-->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.save_a">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.cancel_a">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
                <!-- modal footer end attach file -->

            </div>
        </div>
    </div>
    <!-- modal form end Attach Document Underlying Form end -->
    <!-- Modal Double Transaction start -->
    <!-- Modal Double Transaction end -->
    <!-- Modal upload form start -->
    <div id="modal-form-upload-ipe" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="customer-form" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-type">Document Type</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="document-type-ipe" name="document-type" data-rule-required="true" data-rule-value="true" data-bind="options:$root.ddlDocumentType_a(), optionsText: 
'Name', optionsValue: 'ID', optionsCaption: 'Please select...', value: DocumentTypeValue"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-purpose">Purpose of Docs</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="document-purpose-ipe" name="document-purpose" data-rule-required="true" data-rule-value="true" data-bind="options: $root.ddlDocumentPurpose_a(), 
    optionsText: 'Name', optionsValue: 'ID',  optionsCaption: 'Please Select...', value: DocumentPurposeValue"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-path">Document</label>

                                    <div class="col-sm-8">
                                        <div class="clearfix">
                                            <input type="file" id="document-path" name="document-path" data-bind="file: DocumentPath" /><!---->
                                            <!--<span data-bind="text: DocumentPath"></span>-->
                                        </div>
                                    </div>
                                    <label class="control-label bolder text-danger">*</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.AddDocument">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.cancel_a">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal upload form end -->
    <!-- modal form start Underlying Form -->
    <div id="modal-form-Underlying-ipe" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <!-- modal header start Underlying Form -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span data-bind="if: $root.IsNewDataUnderlying()">Create a new Customer Underlying Parameter</span>
                        <span data-bind="if: !$root.IsNewDataUnderlying()">Modify a Customer Underlying Parameter</span>
                    </h4>
                </div>
                <!-- modal header end -->
                <!-- modal body start -->
                <div class="modal-body overflow-visible">
                    <div id="modalUnderlyingtest-ipe">
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- modal body form start -->

                                <div id="customerUnderlyingform-ipe" class="form-horizontal" role="form">
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="StatementLetterID">
                                            Statement Letter
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="statementLetter-ipe" name="statementLetter" data-bind="value:$root.StatementLetter_u().ID, options: ddlStatementLetter_u, optionsText: 
'Name',optionsValue: 'ID',optionsCaption: 'Please Select...',enable:$root.IsStatementA()" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="UnderlyingDocument">
                                            Underlying Document
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="underlyingDocument-ipe" name="underlyingDocument" data-bind="value:UnderlyingDocument_u().ID, options: ddlUnderlyingDocument_u, optionsText: 
'CodeName',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <!--    						<span aa-bind="text:UnderlyingDocument_u().Code()"></span>-->
                                    <div data-bind="visible:UnderlyingDocument_u().ID()=='50'" class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="otherUnderlying">
                                            Other Underlying
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="otherUnderlying-ipe" name="otherUnderlying" data-bind="value: $root.OtherUnderlying_u" class="col-xs-7" />
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="DocumentType">
                                            Type of Document
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="documentType" name="documentType" data-bind="value:$root.DocumentType_u().ID, options: ddlDocumentType_u, optionsText: 'Name',optionsValue: 
'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Currency">
                                            Transaction Currency
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="currency_u-ipe" name="currency_u" data-bind="value:$root.Currency_u().ID,disable:$root.StatementLetter_u().ID()==1, options:ddlCurrency_u, 
    optionsText:'Code',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-2"></select>
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Amount">
                                            Invoice Amount
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="Amount_u-ipe" name="Amount_u-ipe" data-bind="value: $root.Amount_u,disable:$root.StatementLetter_u().ID
()==1,valueUpdate:['afterkeydown','propertychange','input']" data-rule-required="true" data-rule-number="true" class="col-xs-4 align-right" />
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Rate">Rate</label>

                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" class="col-sm-4 align-right" name="rate_u" id="rate_u-ipe" disabled="disabled" data-rule-required="true" data-rule-
number="true" data-bind="value: Rate_u" />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Eqv.USD">Eqv. USD</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" class="col-sm-4 align-right" disabled="disabled" name="eqv-usd_u" id="eqv-usd_u" data-rule-required="true" data-rule-
number="true" data-bind="value: AmountUSD_u" />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="DateOfUnderlying">
                                            Date of Underlying
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <input type="text" autocomplete="off" id="DateOfUnderlying-ipe" data-date-format="dd-M-yyyy" name="DateOfUnderlying_u" data-bind="value:DateOfUnderlying_u" 
class="form-control date-picker col-xs-6" data-rule-required="true" />
                                                <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                <label class="control-label bolder text-danger starremove">*</label>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label no-padding-right" for="ExpiredDate">
                                            Expiry Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <input type="text" autocomplete="off" id="ExpiredDate-ipe" data-date-format="dd-M-yyyy" name="ExpiredDate" data-bind="value:ExpiredDate_u" class="form-control 
date-picker col-xs-6" data-rule-required="true" />
                                                <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                <label class="control-label bolder text-danger starremove">*</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="IsDeclarationOfException">
                                            Declaration of Exception
                                        </label>
                                        <div class="col-sm-9">
                                            <label>
                                                <input name="switch-field-1" id="IsDeclarationOfException-ipe" data-bind="checked: $root.IsDeclarationOfException_u" class="ace ace-switch ace-switch-6" 
type="checkbox" />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="StartDate">
                                            Start Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <input id="StartDate-ipe" name="StartDate" data-date-format="dd-M-yyyy" data-bind="value:StartDate_u" class="form-control date-picker col-xs-6" type="text" 
autocomplete="off" />
                                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                    <label class="control-label bolder text-danger starremove">*</label>
                                                </div>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label no-padding-right" for="EndDate">
                                            End Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <input type="text" autocomplete="off" id="EndDate-ipe" name="EndDate" data-date-format="dd-M-yyyy" data-bind="value:EndDate_u" class="form-control date-
picker col-xs-6" />
                                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                    <label class="control-label bolder text-danger starremove">*</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="ReferenceNumber">
                                            Reference Number
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="ReferenceNumber-ipe" name="ReferenceNumber" data-bind="value: $root.ReferenceNumber_u" disabled="disabled" 
class="col-xs-9" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="SupplierName">
                                            Supplier Name
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="SupplierName-ipe" name="SupplierName" data-bind="value: $root.SupplierName_u" class="col-xs-8" data-rule-
required="true" />
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="InvoiceNumber">
                                            Invoice Number
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="invoiceNumber-ipe" name="invoiceNumber" data-bind="value: $root.InvoiceNumber_u" class="col-xs-8" data-rule-
required="true" />
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--								<div class="space-2"></div>
                                                                <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="AttachmentNo">
                                                                Attachment No.</label>
                                                                <div class="col-sm-9">
                                                                        <div class="clearfix">
                                                                            <input type="text" autocomplete="off" id="AttachmentNo" name="AttachmentNo" data-bind="value: $root.AttachmentNo_u" class="col-xs-
8"/>
                                                                        </div>
                                                                    </div>
                                                                </div>-->
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="AttachmentNo">
                                            Is This doc Purposed to replace the proforma doc
                                        </label>
                                        <div class="col-sm-9">
                                            <label>
                                                <input name="switch-field-1" id="IsProforma-ipe" data-bind="checked:IsProforma_u,disable:DocumentType_u().ID()==2" class="ace ace-switch ace-switch-6" 
type="checkbox" />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <!-- grid proforma begin -- >

                                    <!-- widget box start -->
                                    <div id="widget-box" class="widget-box" data-bind="visible:IsProforma_u()">
                                        <!-- widget header start -->
                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                            <h6>Customer Underlying Proforma Table</h6>

                                            <div class="widget-toolbar">
                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                    <i class="blue icon-filter"></i>
                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingProformaGridProperties().AllowFilter" />
                                                    <span class="lbl"></span>
                                                </label>
                                                <a href="#" data-bind="click: GetDataUnderlyingProforma" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                    <i class="blue icon-refresh"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- widget header end -->
                                        <!-- widget body start -->
                                        <div class="widget-body">
                                            <!-- widget main start -->
                                            <div class="widget-main padding-0">
                                                <!-- widget slim control start -->
                                                <div class="slim-scroll" data-height="400">
                                                    <!-- widget content start -->
                                                    <div class="content">

                                                        <!-- table responsive start -->
                                                        <div class="table-responsive">
                                                            <div class="dataTables_wrapper" role="grid">
                                                                <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                                    <thead>
                                                                        <tr data-bind="with: UnderlyingProformaGridProperties">
                                                                            <th style="width: 50px">No.</th>
                                                                            <th style="width: 50px">Select</th>
                                                                            <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                            <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying 
Document</th>
                                                                            <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                            <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                            <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                            <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                            <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                            <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th>
                                                                            <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <thead data-bind="visible: UnderlyingProformaGridProperties().AllowFilter" class="table-filter">
                                                                        <tr>
                                                                            <th class="clear-filter">
                                                                                <a href="#" data-bind="click: ProformaClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear 
Filters">
                                                                                    <i class="green icon-trash"></i>
                                                                                </a>
                                                                            </th>
                                                                            <th class="clear-filter"></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterStatementLetter, event: { 
    change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterUnderlyingDocument, 
    event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterDocumentType, event: { 
    change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterCurrency, event: { 
    change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterAmount, event: { change: 
UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.ProformaFilterDateOfUnderlying, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterSupplierName, event: { 
    change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterReferenceNumber, event: { 
    change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.ProformaFilterExpiredDate, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody data-bind="foreach: {data: CustomerUnderlyingProformas, as: 'ProformasData'}, visible: CustomerUnderlyingProformas().length > 0">
                                                                        <tr data-toggle="modal">
                                                                            <td><span data-bind="text: RowID"></span></td>
                                                                            <td align="center">
                                                                                <input type="checkbox" id="isSelectedProforma-ipe" name="isSelect" data-bind="checked:ProformasData.IsSelectedProforma, event: 
{change:function(data){$root.onSelectionProforma($index(),$data)}}" /></td>
                                                                            <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                            <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                            <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                            <td><span data-bind="text: Currency.Code"></span></td>
                                                                            <td><span data-bind="text: Amount"></span></td>
                                                                            <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                                            <td><span data-bind="text: SupplierName"></span></td>
                                                                            <td><span data-bind="text: ReferenceNumber"></span></td>
                                                                            <td><span data-bind="text: $root.LocalDate(ExpiredDate,true,false)"></span></td>
                                                                        </tr>
                                                                    </tbody>
                                                                    <tbody data-bind="visible: CustomerUnderlyingProformas().length == 0">
                                                                        <tr>
                                                                            <td colspan="11" class="text-center">No entries available.
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <!-- table responsive end -->
                                                    </div>
                                                    <!-- widget content end -->
                                                </div>
                                                <!-- widget slim control end -->
                                                <!-- widget footer start -->
                                                <div class="widget-toolbox padding-8 clearfix">
                                                    <div class="row" data-bind="with: UnderlyingProformaGridProperties">
                                                        <!-- pagination size start -->
                                                        <div class="col-sm-6">
                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                Showing
                                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" 
data-placement="bottom" title="Showing Page Entries"></select>
                                                                rows of <span data-bind="text: Total"></span>
                                                                entries
                                                            </div>
                                                        </div>
                                                        <!-- pagination size end -->
                                                        <!-- pagination page jump start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                Page
                                                                <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-
rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                of <span data-bind="text: TotalPages"></span>
                                                            </div>
                                                        </div>
                                                        <!-- pagination page jump end -->
                                                        <!-- pagination navigation start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                <ul class="pagination">
                                                                    <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                            <i class="icon-double-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                            <i class="icon-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                            <i class="icon-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                            <i class="icon-double-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <!-- pagination navigation end -->

                                                    </div>
                                                </div>
                                                <!-- widget footer end -->

                                            </div>
                                            <!-- widget main end -->
                                        </div>
                                        <!-- widget body end -->

                                    </div>
                                    <!-- widget box end -->
                                    <!-- grid proforma end -- >
                                    </div>

                                    <!-- modal body form end -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal body end -->
                    <!-- modal footer start -->
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-primary" data-bind="click: $root.save_u, visible: $root.IsNewDataUnderlying()">
                            <i class="icon-save"></i>
                            Save
                        </button>
                        <button class="btn btn-sm btn-primary" data-bind="click: $root.update_u, visible: !$root.IsNewDataUnderlying() && !$root.IsUtilize_u()">
                            <i class="icon-edit"></i>
                            Update
                        </button>
                        <button class="btn btn-sm btn-warning" data-bind="click: $root.delete_u, visible: !$root.IsNewDataUnderlying() && !$root.IsUtilize_u()">
                            <i class="icon-trash"></i>
                            Delete
                        </button>
                        <button class="btn btn-sm" data-bind="click: $root.cancel_u" data-dismiss="modal">
                            <i class="icon-remove"></i>
                            Cancel
                        </button>
                    </div>
                    <!-- modal footer end -->

                </div>
            </div>
        </div>
        <!-- modal form end underlying form -->
    </div>

</div>



<div class="space-10"></div>

<div id="transaction-child" style="display: none;">
    <h3 class="header smaller no-margin-top lighter dark">Transaction</h3>

    <div class="modal-body overflow-visible">
        <div class="row" data-bind="with: TransactionModel">
            <div class="col-xs-12">
                <div id="customer-form" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right">
                            <span data-bind="text: $root.LocalDate(CreateDate, true, true)"></span>
                        </label>

                        <label class="col-sm-4 pull-right">
                            <b>Application ID</b> : <span data-bind="text: ApplicationID"></span>
                            <br />
                            <b>User Name</b> : <span data-bind="text: $root.SPUser().DisplayName"></span>
                        </label>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right">Top Urgent</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsTopUrgent ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                        <label class="col-sm-2 control-label bolder no-padding-right">Top Urgent Chain</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsTopUrgentChain ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="customer-name">Customer Name</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Customer.Name"></span>
                            <span class="label label-warning arrowed" data-bind="if: IsNewCustomer">
                                <i class="icon-warning-sign bigger-120"></i>
                                New Customer
                            </span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="cif">CIF</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Customer.CIF"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="product">Product</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Product.Code"></span>
                            (<span data-bind="text: Product.Name"></span>)
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="currency">Transaction Currency</label>

                        <label class="col-sm-8">
                            <span data-bind="text: Currency.Code"></span>
                            (<span data-bind="text: Currency.Description"></span>)
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="trxn-amount">Trxn Amount</label>

                        <label class="col-sm-9">
                            <span data-bind="text: formatNumber(Amount)"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="rate">Rate</label>

                        <label class="col-sm-9">
                            <span data-bind="text: formatNumber_r(Rate)"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="eqv-usd">Eqv. USD</label>

                        <label class="col-sm-9">
                            <span data-bind="text: formatNumber_r(AmountUSD)"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="channel">Channel</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Channel.Name"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="debit-acc-number">Debit Acc Number</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Account.AccountNumber"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="debit-acc-ccy">Debit Acc CCY</label>

                        <label class="col-sm-9">
                            <span name="debit-acc-ccy" id="debit-acc-ccy-code" data-bind="text: DebitCurrency.Code"></span>
                            (<span name="debit-acc-ccy" id="debit-acc-ccy-desc" data-bind="text: DebitCurrency.Description"></span>)
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="application-date">Application Date</label>

                        <label class="col-sm-9">
                            <span data-bind="text: $root.LocalDate(ApplicationDate, true)"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="biz-segment">Biz Segment</label>

                        <label class="col-sm-9">
                            <span data-bind="text: BizSegment.Description"></span>
                            (<span data-bind="text: BizSegment.Name"></span>)
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom" data-bind="if: Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR' && ProductType !=null">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="product">Product Type</label>

                        <label class="col-sm-9">
                            <span data-bind="text: ProductType.Code"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="bene-name">Bene Name</label>

                        <label class="col-sm-9">
                            <span data-bind="text: BeneName"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="bene-bank">Bene Bank</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Bank.Description"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="bank-code">Swift Code/Bank Code</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Bank.SwiftCode"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="bene-acc-number">Bene Acc Number</label>

                        <label class="col-sm-9">
                            <span data-bind="text: BeneAccNumber"></span>
                        </label>
                    </div>

                    <div data-bind="if: BankCharges != null && (Product.Code == 'OT' || Product.Code == 'RT' || Product.Code == 'SK'), attr: { class: 'form-group form-line no-margin-bottom no-padding-bottom' 
}">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="bank-charges">Bank Charges</label>

                        <label class="col-sm-4">
                            <span data-bind="text: BankCharges.Code"></span>
                        </label>
                    </div>

                    <div data-bind="if: AgentCharges != null && Product.Code == 'RT', attr: { class: 'form-group form-line no-margin-bottom no-padding-bottom' }">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="agent-charges">Agent Charges</label>

                        <label class="col-sm-4">
                            <span data-bind="text: AgentCharges.Code"></span>
                        </label>
                    </div>

                    <div data-bind="if: Product.Code == 'SK', visible: Product.Code == 'SK', attr: { class: 'form-group form-line no-margin-bottom no-padding-bottom' }">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="type">Type</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Type"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Citizen</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsCitizen ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Resident</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsResident ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Payment Details</label>

                        <label class="col-sm-9">
                            <span data-bind="text: PaymentDetails"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="lld-code">LLD Code</label>

                        <label class="col-sm-9" data-bind="if:LLD != null">
                            <span data-bind="text: LLD.Code + ' (' + LLD.Description + ')'"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Signature Verification</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsSignatureVerified ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Dormant Account</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsDormantAccount ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Freeze Account</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsFreezeAccount ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Others Exceptional Handling</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Others"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="is-pending-documents">Documents Completeness</label>

                        <label class="col-sm-9">
                            <input name="is-pending-documents" class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: $root.UpdatePendingDocuments().IsCompleted, click: 
IsDocumentCompleteCheck()" /><!--, disable: !$root.IsEditable()-->
                            <span class="lbl"></span>
                        </label>
                    </div>
                </div>

                <div class="space-4"></div>

                <!--
                                if: Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR'
                -->
                <div class="padding-4" data-bind="if: Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR'">
                    <h3 class="header smaller lighter dark">Un-utilize Underlying Documents
                        <%--<button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.NewDataUnderlying, disable: !$root.IsEditable()">
                            <i class="icon-plus"></i>
                            Add Underlying
                        </button>--%>
                    </h3>
                    <!-- widget box start -->
                    <div id="widget-box" class="widget-box">
                        <!-- widget header start -->
                        <div class="widget-header widget-hea1der-small header-color-dark">
                            <h6>Underlying Table</h6>

                            <div class="widget-toolbar">
                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                    <i class="blue icon-filter"></i>
                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: $root.UnderlyingGridProperties().AllowFilter" />
                                    <span class="lbl"></span>
                                </label>
                                <a href="#" data-bind="click: $root.GetDataUnderlying" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                    <i class="blue icon-refresh"></i>
                                </a>
                            </div>
                        </div>
                        <!-- widget header end -->
                        <!-- widget body start -->
                        <div class="widget-body">
                            <!-- widget main start -->
                            <div class="widget-main padding-0">
                                <!-- widget slim control start -->
                                <div class="slim-scroll" data-height="200">
                                    <!-- widget content start -->
                                    <div class="content">

                                        <!-- table responsive start -->
                                        <div class="table-responsive">
                                            <div class="dataTables_wrapper" role="grid">
                                                <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                    <thead>
                                                        <tr data-bind="with: $root.UnderlyingGridProperties">
                                                            <th style="width: 50px">No.</th>
                                                            <th style="width: 50px">Utilize</th>
                                                            <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                            <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                            <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                            <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                            <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                            <th data-bind="click: function () { Sorting('AvailableAmount'); }, css: GetSortedColumn('AvailableAmount')">Available Amount in USD</th>
                                                            <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                            <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                            <th data-bind="click: function () { Sorting('AttachmentNo'); }, css: GetSortedColumn('AttachmentNo')">Attachment No</th>
                                                            <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th>
                                                            <th style="width: 50px">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <thead data-bind="visible: $root.UnderlyingGridProperties().AllowFilter" class="table-filter">
                                                        <tr>
                                                            <th class="clear-filter">
                                                                <a href="#" data-bind="click: $root.UnderlyingClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear 
Filters">
                                                                    <i class="green icon-trash"></i>
                                                                </a>
                                                            </th>
                                                            <th class="clear-filter"></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterStatementLetter, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterUnderlyingDocument, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterDocumentType, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterCurrency, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterAmount, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterAvailableAmount, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.UnderlyingFilterDateOfUnderlying, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterSupplierName, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterAttachmentNo, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterReferenceNumber, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th class="clear-filter"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody data-bind="foreach: $root.CustomerUnderlyings, visible: $root.CustomerUnderlyings().length > 0">
                                                        <tr>
                                                            <td><span data-bind="text: RowID"></span></td>
                                                            <td align="center">
                                                                <input type="checkbox" id="isUtilize" name="isUtilize" data-bind="checked:IsEnable,event: {change:function(data){$root.onSelectionUtilize
($index(),$data)}}, disable: !$root.IsEditable()" /></td>
                                                            <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                            <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                            <td><span data-bind="text: DocumentType.Name"></span></td>
                                                            <td><span data-bind="text: Currency.Code"></span></td>
                                                            <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                                            <td align="right"><span data-bind="text: formatNumber(AvailableAmount)"></span></td>
                                                            <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                            <td><span data-bind="text: SupplierName"></span></td>
                                                            <td><span data-bind="text: AttachmentNo"></span></td>
                                                            <td><span data-bind="text: ReferenceNumber"></span></td>
                                                            <td><span class="label label-info arrowed-in-right arrowed" data-bind="click: $root.GetUnderlyingSelectedRow,disable: !$root.IsEditable()">view</span></td>
                                                        </tr>
                                                    </tbody>
                                                    <tbody data-bind="visible: $root.CustomerUnderlyings().length == 0">
                                                        <tr>
                                                            <td colspan="13" class="text-center">No entries available.
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- table responsive end -->
                                    </div>
                                    <!-- widget content end -->
                                </div>
                                <!-- widget slim control end -->
                                <!-- widget footer start -->

                                <div class="widget-toolbox padding-8 clearfix">
                                    <div class="row" data-bind="with: $root.UnderlyingGridProperties">
                                        <!-- pagination size start -->
                                        <div class="col-sm-6">
                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                Showing
                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-
placement="bottom" title="Showing Page Entries"></select>
                                                rows of <span data-bind="text: Total"></span>
                                                entries
                                            </div>
                                        </div>
                                        <!-- pagination size end -->
                                        <!-- pagination page jump start -->
                                        <div class="col-sm-3">
                                            <div class="dataTables_paginate paging_bootstrap">
                                                Page
                                                <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" 
data-placement="bottom" title="Jump to Page" />
                                                of <span data-bind="text: TotalPages"></span>
                                            </div>
                                        </div>
                                        <!-- pagination page jump end -->
                                        <!-- pagination navigation start -->
                                        <div class="col-sm-3">
                                            <div class="dataTables_paginate paging_bootstrap">
                                                <ul class="pagination">
                                                    <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                            <i class="icon-double-angle-left"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                            <i class="icon-angle-left"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                            <i class="icon-angle-right"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                            <i class="icon-double-angle-right"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- pagination navigation end -->

                                    </div>
                                </div>

                                <!-- widget footer end -->

                            </div>
                            <!-- widget main end -->
                        </div>
                        <!-- widget body end -->
                    </div>
                    <!-- widget box end -->


                </div>
                <!--- Added form custome Customer Underlying End -->
                <div class="space-10"></div>
                <div data-bind="if: Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR'">
                    <div class="form-horizontal" role="form">

                        <div class="form-group">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="underlying-code">Underlying Code</label>
                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <select class="col-sm-12" id="underlying-code" name="underlying-code" data-bind="options: $root.Parameter().UnderlyingDocs, optionsText: function(item) { if(item.ID != 
null) return item.Code + ' (' + item.Name + ')'} , optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().UnderlyingDoc, disable: !$root.IsEditable()" data-rule-
required="true"></select>

                                </div>
                            </div>
                        </div>

                        <div class="form-group" data-bind="visible: $root.isNewUnderlying()">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="underlying-desc">Other Underlying</label>

                            <div class="col-xs-9 align-left">
                                <div class="clearfix">
                                    <textarea class="col-lg-5" id="underlying-desc" rows="4" data-rule-required="true" data-bind="value: OtherUnderlyingDoc, enable: $root.isNewUnderlying, disable: !
$root.IsEditable()"></textarea>

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="utilization-amount">Utilization Amount</label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="text" autocomplete="off" class="col-sm-4 align-right" name="utilization-amount" id="utilization-amount" rule-required="true" disabled="disabled" data-rule-
number="true" data-bind="value: formatNumber($root.UtilizationAmounts())" />
                                </div>
                            </div>
                        </div>

                        <!--<div class="form-group">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="totalTrx">Total FX-IDR Transaction in USD</label>
                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="text" autocomplete="off" class="col-sm-4 align-right" name="totalTrx" id="totalTrx" disabled="disabled" data-bind="value:TotalTransFX" />
                                </div>
                            </div>
                        </div>-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="tz-number">TZ Number</label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="text" autocomplete="off" class="col-sm-6" name="tz-number" id="tz-number" data-bind="value: TZNumber, disable: !$root.IsEditable()" />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="space-4"></div>
                <div class="padding-4">
                    <h3 class="header smaller lighter dark">Instruction Form and Docs
                        <button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.UploadDocumentUnderlying, disable: false,visible:(Currency.Code != 'IDR' && $root.CurrencyDep().Code == 
'IDR')" onclick="return false">
                            <i class="icon-plus"></i>
                            Attach
                        </button>
                        <button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.UploadDocument, disable: false,visible:!(Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR')" 
onclick="return false">
                            <i class="icon-plus"></i>
                            Attach
                        </button>
                    </h3>
                    <!--data-bind="visible:!$root.IsUnderlyingMode()" -->
                    <div class="dataTables_wrapper" role="grid">
                        <table class="table table-striped table-bordered table-hover dataTable">
                            <thead>
                                <tr>
                                    <th style="width: 50px">No.</th>
                                    <th>File Name</th>
                                    <th>Purpose of Docs</th>
                                    <th>Type of Docs</th>
                                    <th>Modified</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody data-bind="foreach: $root.Documents, visible: $root.Documents().length > 0">
                                <tr>
                                    <!--data-bind="click: $root.EditDocument"-->
                                    <td><span data-bind="text: $index() +1"></span></td>
                                    <td><span data-bind="text: FileName"></span></td>
                                    <td><span data-bind="text: Purpose.Name"></span></td>
                                    <td><span data-bind="text: Type.Name"></span></td>
                                    <td><span data-bind="text: moment(DocumentPath.lastModifiedDate).format(config.format.dateTime)"></span></td>
                                    <!--$root.LocalDate(LastModifiedDate)-->
                                    <td><a href="#" data-bind="click: $root.RemoveDocument">Remove</a></td>
                                </tr>
                            </tbody>
                            <tbody data-bind="visible: $root.Documents().length == 0">
                                <tr>
                                    <td colspan="6" align="center">No entries available.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="space-10"></div>

                    <!--- Grid Underlying Proforma Start -->
                    <!-- widget header start -->
                    <div data-bind="if: Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR'">
                        <h3 class="header smaller lighter dark">Underlying Docs
                        </h3>
                        <div class="widget-header widget-hea1der-small header-color-dark">
                            <h6>Attach File Table</h6>
                            <div class="widget-toolbar">
                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                    <i class="blue icon-filter"></i>
                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: $root.AttachGridProperties().AllowFilter" />
                                    <span class="lbl"></span>
                                </label>
                                <a href="#" data-bind="click: $root.GetDataAttachFile" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                    <i class="blue icon-refresh"></i>
                                </a>
                            </div>
                        </div>
                        <!-- widget header end -->
                        <div class="widget-body">
                            <!-- widget main start -->
                            <div class="widget-main padding-0">
                                <!-- widget slim control start -->
                                <div class="slim-scroll" data-height="200">
                                    <!-- widget content start -->
                                    <div class="content">

                                        <!-- table responsive start -->
                                        <div class="table-responsive">
                                            <div class="dataTables_wrapper" role="grid">
                                                <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                    <thead>
                                                        <tr data-bind="with: $root.AttachGridProperties">
                                                            <th style="width: 50px">No.</th>
                                                            <th data-bind="click: function () { Sorting('FileName'); }, css: GetSortedColumn('FileName')">File Name</th>
                                                            <th data-bind="click: function () { Sorting('DocumentPurpose'); }, css: GetSortedColumn('DocumentPurpose')">Document Purpose</th>
                                                            <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Update</th>
                                                            <th data-bind="click: function () { Sorting('DocumentRefNumber'); }, css: GetSortedColumn('DocumentRefNumber')">Document Ref Number</th>
                                                            <th style="width: 0px; display: none"></th>
                                                            <th style="width: 50px">Delete</th>
                                                        </tr>
                                                    </thead>
                                                    <thead data-bind="visible: $root.AttachGridProperties().AllowFilter" class="table-filter">
                                                        <tr>
                                                            <th class="clear-filter">
                                                                <a href="#" data-bind="click: $root.AttachClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                    <i class="green icon-trash"></i>
                                                                </a>
                                                            </th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.AttachFilterFileName, event: { change: 
$root.AttachGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.AttachFilterDocumentPurpose, event: { change: 
$root.AttachGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.AttachFilterLastModifiedDate, event: { change: $root.AttachGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.AttachFilterDocumentRefNumber, event: { change: 
$root.AttachGridProperties().Filter }" /></th>
                                                            <th class="clear-filter" style="display: none"></th>
                                                            <th class="clear-filter"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody data-bind="foreach: $root.CustomerUnderlyingFiles, visible: $root.CustomerUnderlyingFiles().length > 0">
                                                        <tr data-bind="click: $root.GetAttachSelectedRow" data-toggle="modal">
                                                            <td><span data-bind="text: RowID"></span></td>
                                                            <td><span data-bind="text: FileName"></span></td>
                                                            <td><span data-bind="text: DocumentPurpose.Name"></span></td>
                                                            <td><span data-bind="text: $root.LocalDate(LastModifiedDate,false,false)"></span></td>
                                                            <td><span data-bind="text: DocumentRefNumber"></span></td>
                                                            <td style="width: 0px; display: none"><span data-bind="text: DocumentPath"></span></td>
                                                            <td align="center">
                                                                <!--<a href="#" data-bind="click:$root.delete_a" class="tooltip-info" data-rel="tooltip" data-placement="right">
                                                                    <i class="green icon-trash"></i>
                                                                </a>-->
                                                                <i class="gray icon-trash"></i>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                    <tbody data-bind="visible: $root.CustomerUnderlyingFiles().length == 0">
                                                        <tr>
                                                            <td colspan="10" class="text-center">No entries available.
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- table responsive end -->
                                    </div>
                                    <!-- widget content end -->
                                </div>
                                <!-- widget slim control end -->
                                <!-- widget footer start -->
                                <div class="widget-toolbox padding-8 clearfix">
                                    <div class="row" data-bind="with: $root.AttachGridProperties">
                                        <!-- pagination size start -->
                                        <div class="col-sm-6">
                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                Showing
                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-
placement="bottom" title="Showing Page Entries"></select>
                                                rows of <span data-bind="text: Total"></span>
                                                entries
                                            </div>
                                        </div>
                                        <!-- pagination size end -->
                                        <!-- pagination page jump start -->
                                        <div class="col-sm-3">
                                            <div class="dataTables_paginate paging_bootstrap">
                                                Page
                                                <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" 
data-placement="bottom" title="Jump to Page" />
                                                of <span data-bind="text: TotalPages"></span>
                                            </div>
                                        </div>
                                        <!-- pagination page jump end -->
                                        <!-- pagination navigation start -->
                                        <div class="col-sm-3">
                                            <div class="dataTables_paginate paging_bootstrap">
                                                <ul class="pagination">
                                                    <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                            <i class="icon-double-angle-left"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                            <i class="icon-angle-left"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                            <i class="icon-angle-right"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                            <i class="icon-double-angle-right"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- pagination navigation end -->

                                    </div>
                                </div>
                                <!-- widget footer end -->

                            </div>
                            <!-- widget main end -->
                        </div>
                    </div>
                    <!-- widget body end -->
                    <!--- Grid Underlying Proforma End -->

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 align-right">
            <button class="btn btn-sm btn-primary" data-bind="click: Submit, disable: $root.IsEditable()">
                <i class="icon-save"></i>
                Submit
            </button>

            <button class="btn btn-sm btn-success" data-bind="click: $root.cancel_detail" data-dismiss="modal">
                <i class="icon-remove"></i>
                Cancel
            </button>
        </div>
    </div>

    <!-- modal form start Attach Document Underlying Form start -->
    <div id="modal-form-Attach" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <!-- modal header start Attach FIle Form -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span>Added Attachment File</span>
                    </h4>
                </div>
                <!-- modal header end Attach file -->
                <!-- modal body start Attach File -->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div id="customer-form" class="form-horizontal" role="form">
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="Name">
                                        Purpose of Doc
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentPurpose" name="documentPurpose" data-bind="value:$root.DocumentPurpose_a().ID, options: ddlDocumentPurpose_a, optionsText: 'Name',optionsValue: 
'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="description">
                                        Type of Doc
                                    </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentType" name="documentType" data-bind="value:$root.DocumentType_a().ID, options: ddlDocumentType_a, optionsText: 'Name',optionsValue: 
'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-path">Document</label>

                                    <div class="col-sm-6">
                                        <div class="clearfix">
                                            <input type="file" id="document-path" name="document-path" data-bind="file: DocumentPath_a" class="col-xs-7" data-rule-required="true" data-rule-value="true" 
/><!---->
                                            <!--<span data-bind="text: DocumentPath"></span>-->
                                        </div>
                                    </div>
                                    <label class="control-label bolder text-danger">*</label>
                                </div>
                            </div>
                            <!-- modal body form Attach File End -->
                            <!-- widget box Underlying Attach start -->
                            <div id="widget-box" class="widget-box">
                                <!-- widget header Underlying Attach start -->
                                <div class="widget-header widget-hea1der-small header-color-dark">
                                    <h6>Underlying Form Table</h6>

                                    <div class="widget-toolbar">
                                        <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                            <i class="blue icon-filter"></i>
                                            <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingAttachGridProperties().AllowFilter" />
                                            <span class="lbl"></span>
                                        </label>
                                        <a href="#" data-bind="click: GetDataUnderlyingAttach" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                            <i class="blue icon-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                                <!-- widget header end -->
                                <!-- widget body underlying Attach start -->
                                <div class="widget-body">
                                    <!-- widget main start -->
                                    <div class="widget-main padding-0">
                                        <!-- widget slim control start -->
                                        <div class="slim-scroll" data-height="400">
                                            <!-- widget content start -->
                                            <div class="content">
                                                <!-- table responsive start -->
                                                <div class="table-responsive">
                                                    <div class="dataTables_wrapper" role="grid">
                                                        <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                            <thead>
                                                                <tr data-bind="with: $root.UnderlyingAttachGridProperties">
                                                                    <!-- <th style="width:50px">Select</th> -->
                                                                    <th style="width: 50px">No.</th>
                                                                    <th style="width: 50px">Select</th>
                                                                    <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                    <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                    <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                    <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                    <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                    <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                    <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                    <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th>
                                                                    <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                                </tr>
                                                            </thead>
                                                            <thead data-bind="visible: $root.UnderlyingAttachGridProperties().AllowFilter" class="table-filter">
                                                                <tr>
                                                                    <th class="clear-filter">
                                                                        <a href="#" data-bind="click: $root.UnderlyingAttachClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" 
title="Clear Filters">
                                                                            <i class="green icon-trash"></i>
                                                                        </a>
                                                                    </th>
                                                                    <th class="clear-filter"></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterStatementLetter, event: { 
    change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterUnderlyingDocument, 
    event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterDocumentType, event: { 
    change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterCurrency, event: { 
    change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterAmount, event: { change: 
UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.UnderlyingAttachFilterDateOfUnderlying, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterSupplierName, event: { 
    change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterReferenceNumber, event: { 
    change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.UnderlyingAttachFilterExpiredDate, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody data-bind="foreach: {data: $root.CustomerAttachUnderlyings, as: 'AttachData'}, visible: $root.CustomerAttachUnderlyings().length > 0">
                                                                <tr data-toggle="modal">
                                                                    <td><span data-bind="text: RowID"></span></td>
                                                                    <td align="center">
                                                                      <input type="checkbox" id="isSelected2" name="isSelect2" data-bind="checked:IsSelectedAttach,event:{change:function(data)
{$root.onSelectionAttach($index(),$data)}}, disable: !($root.SelectingUnderlying())" /></td>
                                                                    <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                    <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                    <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                    <td><span data-bind="text: Currency.Code"></span></td>
                                                                    <td><span data-bind="text: Amount"></span></td>
                                                                    <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                                    <td><span data-bind="text: SupplierName"></span></td>
                                                                    <td><span data-bind="text: ReferenceNumber"></span></td>
                                                                    <td><span data-bind="text: $root.LocalDate(ExpiredDate,true,false)"></span></td>
                                                                </tr>
                                                            </tbody>
                                                            <tbody data-bind="visible: $root.CustomerAttachUnderlyings().length == 0">
                                                                <tr>
                                                                    <td colspan="11" class="text-center">No entries available.
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- table responsive end -->
                                            </div>
                                            <!-- widget content end -->
                                        </div>
                                        <!-- widget slim control end -->
                                        <!-- widget footer start -->
                                        <div class="widget-toolbox padding-8 clearfix">
                                            <div class="row" data-bind="with: UnderlyingAttachGridProperties">
                                                <!-- pagination size start -->
                                                <div class="col-sm-6">
                                                    <div class="dataTables_paginate paging_bootstrap pull-left">
                                                        Showing
                                                        <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-
placement="bottom" title="Showing Page Entries"></select>
                                                        rows of <span data-bind="text: Total"></span>
                                                        entries
                                                    </div>
                                                </div>
                                                <!-- pagination size end -->
                                                <!-- pagination page jump start -->
                                                <div class="col-sm-3">
                                                    <div class="dataTables_paginate paging_bootstrap">
                                                        Page
                                                        <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-
rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                        of <span data-bind="text: TotalPages"></span>
                                                    </div>
                                                </div>
                                                <!-- pagination page jump end -->
                                                <!-- pagination navigation start -->
                                                <div class="col-sm-3">
                                                    <div class="dataTables_paginate paging_bootstrap">
                                                        <ul class="pagination">
                                                            <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                    <i class="icon-double-angle-left"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                    <i class="icon-angle-left"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                    <i class="icon-angle-right"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                    <i class="icon-double-angle-right"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- pagination navigation end -->

                                            </div>
                                        </div>
                                        <!-- widget footer end -->

                                    </div>
                                    <!-- widget main end -->
                                </div>
                                <!-- widget body end -->
                            </div>
                            <!-- widget box end -->



                        </div>
                    </div>
                </div>
                <!-- modal body end Attach File -->
                <!-- modal footer start Attach File-->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.save_a">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.cancel_a">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
                <!-- modal footer end attach file -->

            </div>
        </div>
    </div>
    <!-- modal form end Attach Document Underlying Form end -->
    <!-- Modal Double Transaction start -->
    <!-- Modal Double Transaction end -->
    <!-- Modal upload form start -->
    <div id="modal-form-upload" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="customer-form" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-type">Document Type</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="document-type" name="document-type" data-rule-required="true" data-rule-value="true" data-bind="options: Parameter().DocumentTypes, optionsText: 'Name', 
    optionsValue: 'ID', optionsCaption: 'Options..', value: Selected().DocumentType"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-purpose">Purpose of Docs</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="document-purpose" name="document-purpose" data-rule-required="true" data-rule-value="true" data-bind="options: Parameter().DocumentPurposes, 
    optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Options..', value: Selected().DocumentPurpose"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-path">Document</label>

                                    <div class="col-sm-8">
                                        <div class="clearfix">
                                            <input type="file" id="document-path" name="document-path" data-bind="file: DocumentPath" /><!---->
                                            <!--<span data-bind="text: DocumentPath"></span>-->
                                        </div>
                                    </div>
                                    <label class="control-label bolder text-danger">*</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.AddDocument">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.cancel_a">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal upload form end -->
    <!-- modal form start Underlying Form -->
    <div id="modal-form-Underlying" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <!-- modal header start Underlying Form -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span data-bind="if: $root.IsNewDataUnderlying()">Create a new Customer Underlying Parameter</span>
                        <span data-bind="if: !$root.IsNewDataUnderlying()">Modify a Customer Underlying Parameter</span>
                    </h4>
                </div>
                <!-- modal header end -->
                <!-- modal body start -->
                <div class="modal-body overflow-visible">
                    <div id="modalUnderlyingtest">
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- modal body form start -->

                                <div id="customerUnderlyingform" class="form-horizontal" role="form">
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="StatementLetterID">
                                            Statement Letter
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="statementLetter" name="statementLetter" data-bind="value:$root.StatementLetter_u().ID, options: ddlStatementLetter_u, optionsText: 
'Name',optionsValue: 'ID',optionsCaption: 'Please Select...',enable:$root.IsStatementA()" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="UnderlyingDocument">
                                            Underlying Document
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="underlyingDocument" name="underlyingDocument" data-bind="value:UnderlyingDocument_u().ID, options: ddlUnderlyingDocument_u, optionsText: 
'CodeName',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <!--    						<span aa-bind="text:UnderlyingDocument_u().Code()"></span>-->
                                    <div data-bind="visible:UnderlyingDocument_u().ID()=='50'" class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="otherUnderlying">
                                            Other Underlying
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="otherUnderlying" name="otherUnderlying" data-bind="value: $root.OtherUnderlying_u" class="col-xs-7" />
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="DocumentType">
                                            Type of Document
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="documentType" name="documentType" data-bind="value:$root.DocumentType_u().ID, options: ddlDocumentType_u, optionsText: 'Name',optionsValue: 
'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Currency">
                                            Transaction Currency
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="currency_u" name="currency_u" data-bind="value:$root.Currency_u().ID,disable:$root.StatementLetter_u().ID()==1, options:ddlCurrency_u, 
    optionsText:'Code',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-2"></select>
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Amount">
                                            Invoice Amount
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="Amount_u" name="Amount_u" data-bind="value: $root.Amount_u,disable:$root.StatementLetter_u().ID()==1,valueUpdate:
['afterkeydown','propertychange','input']" data-rule-required="true" data-rule-number="true" class="col-xs-4 align-right" />
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Rate">Rate</label>

                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" class="col-sm-4 align-right" name="rate_u" id="rate_u" disabled="disabled" data-rule-required="true" data-rule-
number="true" data-bind="value: Rate_u" />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Eqv.USD">Eqv. USD</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" class="col-sm-4 align-right" disabled="disabled" name="eqv-usd_u" id="eqv-usd_u" data-rule-required="true" data-rule-
number="true" data-bind="value: AmountUSD_u" />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="DateOfUnderlying">
                                            Date of Underlying
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <input type="text" autocomplete="off" id="DateOfUnderlying" data-date-format="dd-M-yyyy" name="DateOfUnderlying_u" data-bind="value:DateOfUnderlying_u" 
class="form-control date-picker col-xs-6" data-rule-required="true" />
                                                <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                <label class="control-label bolder text-danger starremove">*</label>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label no-padding-right" for="ExpiredDate">
                                            Expiry Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <input type="text" autocomplete="off" id="ExpiredDate" data-date-format="dd-M-yyyy" name="ExpiredDate" data-bind="value:ExpiredDate_u" class="form-control 
date-picker col-xs-6" data-rule-required="true" />
                                                <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                <label class="control-label bolder text-danger starremove">*</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="IsDeclarationOfException">
                                            Declaration of Exception
                                        </label>
                                        <div class="col-sm-9">
                                            <label>
                                                <input name="switch-field-1" id="IsDeclarationOfException" data-bind="checked: $root.IsDeclarationOfException_u" class="ace ace-switch ace-switch-6" 
type="checkbox" />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="StartDate">
                                            Start Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <input id="StartDate" name="StartDate" data-date-format="dd-M-yyyy" data-bind="value:StartDate_u" class="form-control date-picker col-xs-6" type="text" 
autocomplete="off" />
                                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                    <label class="control-label bolder text-danger starremove">*</label>
                                                </div>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label no-padding-right" for="EndDate">
                                            End Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <input type="text" autocomplete="off" id="EndDate" name="EndDate" data-date-format="dd-M-yyyy" data-bind="value:EndDate_u" class="form-control date-picker 
col-xs-6" />
                                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                    <label class="control-label bolder text-danger starremove">*</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="ReferenceNumber">
                                            Reference Number
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="ReferenceNumber" name="ReferenceNumber" data-bind="value: $root.ReferenceNumber_u" disabled="disabled" class="col-xs-
9" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="SupplierName">
                                            Supplier Name
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="SupplierName" name="SupplierName" data-bind="value: $root.SupplierName_u" class="col-xs-8" data-rule-required="true" 
/>
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="InvoiceNumber">
                                            Invoice Number
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="invoiceNumber" name="invoiceNumber" data-bind="value: $root.InvoiceNumber_u" class="col-xs-8" data-rule-
required="true" />
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--								<div class="space-2"></div>
                                                                <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="AttachmentNo">
                                                                Attachment No.</label>
                                                                <div class="col-sm-9">
                                                                        <div class="clearfix">
                                                                            <input type="text" autocomplete="off" id="AttachmentNo" name="AttachmentNo" data-bind="value: $root.AttachmentNo_u" class="col-xs-
8"/>
                                                                        </div>
                                                                    </div>
                                                                </div>-->
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="AttachmentNo">
                                            Is This doc Purposed to replace the proforma doc
                                        </label>
                                        <div class="col-sm-9">
                                            <label>
                                                <input name="switch-field-1" id="IsProforma" data-bind="checked:IsProforma_u,disable:DocumentType_u().ID()==2" class="ace ace-switch ace-switch-6" 
type="checkbox" />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <!-- grid proforma begin -- >

                                    <!-- widget box start -->
                                    <div id="widget-box" class="widget-box" data-bind="visible:IsProforma_u()">
                                        <!-- widget header start -->
                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                            <h6>Customer Underlying Proforma Table</h6>

                                            <div class="widget-toolbar">
                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                    <i class="blue icon-filter"></i>
                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingProformaGridProperties().AllowFilter" />
                                                    <span class="lbl"></span>
                                                </label>
                                                <a href="#" data-bind="click: GetDataUnderlyingProforma" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                    <i class="blue icon-refresh"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- widget header end -->
                                        <!-- widget body start -->
                                        <div class="widget-body">
                                            <!-- widget main start -->
                                            <div class="widget-main padding-0">
                                                <!-- widget slim control start -->
                                                <div class="slim-scroll" data-height="400">
                                                    <!-- widget content start -->
                                                    <div class="content">

                                                        <!-- table responsive start -->
                                                        <div class="table-responsive">
                                                            <div class="dataTables_wrapper" role="grid">
                                                                <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                                    <thead>
                                                                        <tr data-bind="with: UnderlyingProformaGridProperties">
                                                                            <th style="width: 50px">No.</th>
                                                                            <th style="width: 50px">Select</th>
                                                                            <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                            <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying 
Document</th>
                                                                            <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                            <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                            <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                            <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                            <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                            <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th>
                                                                            <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <thead data-bind="visible: UnderlyingProformaGridProperties().AllowFilter" class="table-filter">
                                                                        <tr>
                                                                            <th class="clear-filter">
                                                                                <a href="#" data-bind="click: ProformaClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear 
Filters">
                                                                                    <i class="green icon-trash"></i>
                                                                                </a>
                                                                            </th>
                                                                            <th class="clear-filter"></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterStatementLetter, event: { 
    change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterUnderlyingDocument, 
    event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterDocumentType, event: { 
    change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterCurrency, event: { 
    change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterAmount, event: { change: 
UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.ProformaFilterDateOfUnderlying, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterSupplierName, event: { 
    change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterReferenceNumber, event: { 
    change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.ProformaFilterExpiredDate, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody data-bind="foreach: {data: CustomerUnderlyingProformas, as: 'ProformasData'}, visible: CustomerUnderlyingProformas().length > 0">
                                                                        <tr data-toggle="modal">
                                                                            <td><span data-bind="text: RowID"></span></td>
                                                                            <td align="center">
                                                                                <input type="checkbox" id="isSelectedProforma" name="isSelect" data-bind="checked:ProformasData.IsSelectedProforma, event: 
{change:function(data){$root.onSelectionProforma($index(),$data)}}" /></td>
                                                                            <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                            <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                            <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                            <td><span data-bind="text: Currency.Code"></span></td>
                                                                            <td><span data-bind="text: Amount"></span></td>
                                                                            <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                                            <td><span data-bind="text: SupplierName"></span></td>
                                                                            <td><span data-bind="text: ReferenceNumber"></span></td>
                                                                            <td><span data-bind="text: $root.LocalDate(ExpiredDate,true,false)"></span></td>
                                                                        </tr>
                                                                    </tbody>
                                                                    <tbody data-bind="visible: CustomerUnderlyingProformas().length == 0">
                                                                        <tr>
                                                                            <td colspan="11" class="text-center">No entries available.
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <!-- table responsive end -->
                                                    </div>
                                                    <!-- widget content end -->
                                                </div>
                                                <!-- widget slim control end -->
                                                <!-- widget footer start -->
                                                <div class="widget-toolbox padding-8 clearfix">
                                                    <div class="row" data-bind="with: UnderlyingProformaGridProperties">
                                                        <!-- pagination size start -->
                                                        <div class="col-sm-6">
                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                Showing
                                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" 
data-placement="bottom" title="Showing Page Entries"></select>
                                                                rows of <span data-bind="text: Total"></span>
                                                                entries
                                                            </div>
                                                        </div>
                                                        <!-- pagination size end -->
                                                        <!-- pagination page jump start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                Page
                                                                <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-
rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                of <span data-bind="text: TotalPages"></span>
                                                            </div>
                                                        </div>
                                                        <!-- pagination page jump end -->
                                                        <!-- pagination navigation start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                <ul class="pagination">
                                                                    <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                            <i class="icon-double-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                            <i class="icon-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                            <i class="icon-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                            <i class="icon-double-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <!-- pagination navigation end -->

                                                    </div>
                                                </div>
                                                <!-- widget footer end -->

                                            </div>
                                            <!-- widget main end -->
                                        </div>
                                        <!-- widget body end -->

                                    </div>
                                    <!-- widget box end -->
                                    <!-- grid proforma end -- >
                                    </div>

                                    <!-- modal body form end -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal body end -->
                    <!-- modal footer start -->
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-primary" data-bind="click: $root.save_u, visible: $root.IsNewDataUnderlying()">
                            <i class="icon-save"></i>
                            Save
                        </button>
                        <button class="btn btn-sm btn-primary" data-bind="click: $root.update_u, visible: !$root.IsNewDataUnderlying() && !$root.IsUtilize_u()">
                            <i class="icon-edit"></i>
                            Update
                        </button>
                        <button class="btn btn-sm btn-warning" data-bind="click: $root.delete_u, visible: !$root.IsNewDataUnderlying() && !$root.IsUtilize_u()">
                            <i class="icon-trash"></i>
                            Delete
                        </button>
                        <button class="btn btn-sm" data-bind="click: $root.cancel_u" data-dismiss="modal">
                            <i class="icon-remove"></i>
                            Cancel
                        </button>
                    </div>
                    <!-- modal footer end -->

                </div>
            </div>
        </div>
        <!-- modal form end underlying form -->
    </div>

</div>

<%--IPE ADD UDIN--%>
<div id="transaction-child-ipe" style="display: none;">
    <h3 class="header smaller no-margin-top lighter dark">Transaction</h3>

    <div class="modal-body overflow-visible">
        <div class="row" data-bind="with: TransactionModel">
            <div class="col-xs-12">
                <div id="customer-form-ipe" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right">
                            <span data-bind="text: $root.LocalDate(CreateDate, true, true)"></span>
                        </label>

                        <label class="col-sm-4 pull-right">
                            <b>Application ID</b> : <span data-bind="text: ApplicationID"></span>
                            <br />
                            <b>User Name</b> : <span data-bind="text: $root.SPUser().DisplayName"></span>
                        </label>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right">Top Urgent</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsTopUrgent ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                        <label class="col-sm-2 control-label bolder no-padding-right">Top Urgent Chain</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsTopUrgentChain ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>

                        <label class="col-sm-2 control-label bolder no-padding-right">Normal</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsNormal? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="customer-name">Customer Name</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Customer.Name"></span>
                            <span class="label label-warning arrowed" data-bind="if: IsNewCustomer">
                                <i class="icon-warning-sign bigger-120"></i>
                                New Customer
                            </span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="cif">CIF</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Customer.CIF"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="product">Product</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Product.Code"></span>
                            (<span data-bind="text: Product.Name"></span>)
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="currency">Transaction Currency</label>

                        <label class="col-sm-8">
                            <span data-bind="text: Currency.Code"></span>
                            (<span data-bind="text: Currency.Description"></span>)
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="trxn-amount">Trxn Amount</label>

                        <label class="col-sm-9">
                            <span data-bind="text: formatNumber(Amount)"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="rate">IDR Mid Rate</label>

                        <label class="col-sm-9">
                            <span data-bind="text: formatNumber_r(Rate)"></span>
                        </label>
                    </div>
                    <!-- Permintaan User
                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="rate">Transaction Rate</label>

                        <label class="col-sm-9">
                            <span data-bind="text: formatNumber_r(Rate)"></span>
                        </label>
                    </div>
                    END-->
                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="eqv-usd">Eqv. USD</label>

                        <label class="col-sm-9">
                            <span data-bind="text: formatNumber_r(AmountUSD)"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="channel">Channel</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Channel.Name"></span>
                        </label>
                    </div>
                    <%-- start LLD Document Code by azam --%>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="rate">LLD Document Code</label>

                        <label class="col-sm-9">
                              <span data-bind="text:LLDDocumentCode"></span> 
                        </label>
                    </div>


                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="rate">LLD Underlying Amount</label>

                        <label class="col-sm-9">
                             <span data-bind="text:formatNumber(LLDUnderlyingAmount)"></span> 
                        </label>
                    </div>



                    <%-- end LLD Document Code --%>
                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="debit-acc-number">Debit Acc Number</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Account.AccountNumber"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="debit-acc-ccy">Debit Acc CCY</label>

                        <label class="col-sm-9">
                            <span name="debit-acc-ccy-ipe" id="debit-acc-ccy-code-ipe" data-bind="text: DebitCurrency.Code"></span>
                            (<span name="debit-acc-ccy-ipe" id="debit-acc-ccy-desc-ipe" data-bind="text: DebitCurrency.Description"></span>)
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="Debit Sundry">Debit Sundry</label>
                        <label data-bind="if: Sundry !=null" class="col-sm-8">
                            <span id="DebitSundry" data-bind="text: Sundry.OABAccNo"></span>
                        </label>
                    </div>
                    <!--Permintaan User
                    <div data-bind="visible: Product.Name == 'OTT' && Currency.Code !='IDR', attr: { class: 'form-group form-line no-margin-bottom no-padding-bottom' }">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="Nostro">Nostro</label>
                        <label data-bind="if:Nostro !=null" class="col-sm-8">
                            <span name="Nostro" id="Nostro" data-bind="text: Nostro.NostroUsed"></span>
                        </label>
                    </div>
                    END-->
                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="application-date">Application Date</label>

                        <label class="col-sm-9">
                            <span data-bind="text: $root.LocalDate(ApplicationDate, true)"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="biz-segment">Biz Segment</label>

                        <label class="col-sm-9">
                            <span data-bind="text: BizSegment.Description"></span>
                            (<span data-bind="text: BizSegment.Name"></span>)
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom" data-bind="if: Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR' && ProductType !=null">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="product">Product Type</label>

                        <label class="col-sm-9">
                            <span data-bind="text: ProductType.Code"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="bene-name">Bene Name</label>

                        <label class="col-sm-9">
                            <span data-bind="text: BeneName"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="bene-bank">Bene Bank</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Bank.Description"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="bank-code">Swift Code/Bank Code</label>

                        <label class="col-sm-1">
                            <span data-bind="text: Bank.SwiftCode"></span>
                        </label>

                        <label class="col-sm-1 control-label bolder no-padding-right" for="bank-code-branch">Branch Code</label>
                        <label data-bind="if:Branch !=null" class="col-sm-1">
                            <span name="bank-code-branch" id="bank-code-branch" data-bind="text: Branch.Code"></span>
                            (<span name="bank-code-branch" id="bank-code-branch" data-bind="text: Branch.Name"></span>)
                        </label>

                        <label class="col-sm-1 control-label bolder no-padding-right" for="bank-code-city">City Code</label>

                        <label data-bind="if:City !=null" class="col-sm-3">
                            <span name="bank-code-cityCode" id="bank-code-cityCode" data-bind="text: City.CityCode"></span>
                            (<span name="bank-code-cityDesc" id="bank-code-cityDesc" data-bind="text: City.Description"></span>)
                        </label>

                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="bene-acc-number">Bene Acc Number</label>

                        <label class="col-sm-9">
                            <span data-bind="text: BeneAccNumber"></span>
                        </label>
                    </div>

                    <div data-bind="if: BankCharges != null && (Product.Code == 'OT' || Product.Code == 'RT' || Product.Code == 'SK'), attr: { class: 'form-group form-line no-margin-bottom no-padding-bottom' 
}">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="bank-charges">Bank Charges</label>

                        <label class="col-sm-4">
                            <span data-bind="text: BankCharges.Code"></span>
                        </label>
                    </div>

                    <div data-bind="if: AgentCharges != null && Product.Code == 'RT', attr: { class: 'form-group form-line no-margin-bottom no-padding-bottom' }">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="agent-charges">Agent Charges</label>

                        <label class="col-sm-4">
                            <span data-bind="text: AgentCharges.Code"></span>
                        </label>
                    </div>

                    <div data-bind="if: Product.Code == 'SK', visible: Product.Code == 'SK', attr: { class: 'form-group form-line no-margin-bottom no-padding-bottom' }">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="type">Type</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Type"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Citizen</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsCitizen ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Resident</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsResident ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom" data-bind="visible:Product.Code == 'SK' || Product.Code == 'OT' || Product.Code == 'OV' || Product.Code == 'RT'">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="type">Beneficiary Resident</label>

                        <label class="col-sm-8">
                            <label data-bind=" visible: IsBeneficiaryResident == true">
                                <span id="IsbeneTrue">Resident</span>
                            </label>
                            <label data-bind="visible: IsBeneficiaryResident == false">
                                <span id="IsbeneFalse">Non Resident</span>
                            </label>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class=" col-sm-3 control-label bolder no-padding-right" for="type">
                            Beneficiary Country Code
                        </label>
                        <label class="col-sm-8" data-bind="if: BeneficiaryCountry != null">
                            <span id="beneficiary-country-code" data-bind="text: BeneficiaryCountry.Code"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="ChAccName">Charging Account Name</label>
                        <label class="col-sm-8">
                            <span id="ChAccName" data-bind="text: ChargingAccountName"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="ChAccBank">Charging Account Bank</label>
                        <label class="col-sm-8">
                            <span id="ChAccBank" data-bind="text: ChargingAccountBank"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="ChAccount">Charging Account</label>
                        <label class="col-sm-3">
                            <span id="ChAccount" data-bind="text: ChargingACCNumber "></span>
                        </label>
                        <label class="col-sm-3 control-label bolder no-padding-right">CCY</label>
                        <label class="col-sm-2" data-bind="if:ChargingAccountCurrency !=null && ChargingAccountCurrency !=0 ">
                            <span id="ChAccountCCY" data-bind="text: $root.ChargingAccCurrency().Code"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="beneficiary-code">Beneficiary Business Type</label>

                        <label class="col-sm-8" data-bind="if: BeneficiaryBusines != null">
                            <label class="col-sm-8">
                                <span id="beneficiary-code" data-bind="text: BeneficiaryBusines.Code"></span>
                                (<span id="beneficiary-desc" data-bind="text: BeneficiaryBusines.Description"></span>)
                            </label>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="beneficiary-addr">Beneficiary Address</label>
                        <div data-bind="ifnot: BeneficiaryAddress == '' ">
                            <div data-bind="if: BeneficiaryAddress != ''">
                                <label class="col-sm-8">
                                    <span id="beneficiary-addr" data-bind="text: BeneficiaryAddress"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class=" col-sm-3 control-label bolder no-padding-right" for="type">
                            Transaction Relationship
                        </label>
                        <label class="col-sm-8" data-bind="if: TransactionRelationship != null">
                            <span id="trrelation" data-bind="text: TransactionRelationship.Code"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="lld-code">LLD Code</label>

                        <label class="col-sm-9" data-bind="if:LLD != null">
                            <span data-bind="text: LLD.Code + ' (' + LLD.Description + ')'"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="underlying-code">Underlying Code</label>
                        <div data-bind="ifnot:UnderlyingDoc==null">
                            <label class="col-sm-8">
                                <span id="underlying-code" name="underlying-code" data-bind="text: UnderlyingDoc.Code"></span>
                                (<span id="underlying-code1" name="underlying-code" data-bind="text: UnderlyingDoc.Name"></span>)
                            </label>
                        </div>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="ComCode">Compliance Code</label>
                        <div data-bind="ifnot:Compliance==null">
                            <label class="col-sm-8">
                                <span id="ComCode-name" data-bind="text: Compliance.Name"></span>
                                <span id="ComCode-description" data-bind="text: ' - ' + Compliance.Description"></span>
                            </label>
                        </div>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom" data-bind="if: OtherUnderlyingDoc != null">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="underlying-desc">Other Underlying</label>

                        <div class="col-xs-8 align-left">
                            <div class="clearfix">
                                <textarea class="col-lg-5" id="underlying-desc" rows="4" data-rule-required="true" data-bind="value: OtherUnderlyingDoc"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Payment Details</label>

                        <label class="col-sm-9">
                            <span data-bind="text: PaymentDetails"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom" data-bind="visible: Product.Name == 'SKN' || Product.Name == 'RTGS' || Product.Name == 'OTT'">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="purpose-detail">Waive Charges Options</label>
                        <label class="col-sm-2 control-label bolder no-padding-right" for="purpose-detail">CBG Customer</label>
                        <label class="col-sm-2" data-bind="if: CBGCustomer !=null">
                            <span id="purpose-detail" data-bind="text: CBGCustomer.Code"></span>
                        </label>
                        <label class="col-sm-2 control-label bolder no-padding-right" for="purpose-detail">Transaction Using Debit Sundry</label>
                        <label class="col-sm-2" data-bind="if: TransactionUsingDebitSundry != null">
                            <span id=" purpose-detail" data-bind="text:TransactionUsingDebitSundry.Code"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="check-callback-required">Callback is Required</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsCallbackRequired ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Signature Verification</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsSignatureVerified ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Dormant Account</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsDormantAccount ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Freeze Account</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsFreezeAccount ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>

                    <%--<div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="check-statement-Letter-copy">Statement Letter Copy</label>

                        <label class="col-sm-1">
                            <i class="bigger-170" data-bind="css: IsStatementLetterCopy ? 'icon-check green' : 'icon-check-empty'"></i>
                        </label>
                    </div>--%>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Others Exceptional Handling</label>

                        <label class="col-sm-9">
                            <span data-bind="text: Others"></span>
                        </label>
                    </div>

                    <div class="form-group form-line no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="is-pending-documents">Documents Completeness</label>

                        <label class="col-sm-9">
                            <input name="is-pending-documents" class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: $root.UpdatePendingDocuments().IsCompleted , click: 
IsDocumentCompleteCheck()" /><!--, disable: !$root.IsEditable()-->
                            <span class="lbl"></span>
                        </label>
                    </div>
                </div>

                <div class="space-4"></div>

                <!--
                                if: Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR'
                -->
                <div class="padding-4" data-bind="if: Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR'">
                    <h3 class="header smaller lighter dark">Un-utilize Underlying Documents
                        <%--<button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.NewDataUnderlying, disable: !$root.IsEditable()">
                            <i class="icon-plus"></i>
                            Add Underlying
                        </button>--%>
                    </h3>
                    <!-- widget box start -->
                    <div id="widget-box-ipe" class="widget-box">
                        <!-- widget header start -->
                        <div class="widget-header widget-hea1der-small header-color-dark">
                            <h6>Underlying Table</h6>

                            <div class="widget-toolbar">
                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                    <i class="blue icon-filter"></i>
                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: $root.UnderlyingGridProperties().AllowFilter" />
                                    <span class="lbl"></span>
                                </label>
                                <a href="#" data-bind="click: $root.GetDataUnderlying" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                    <i class="blue icon-refresh"></i>
                                </a>
                            </div>
                        </div>
                        <!-- widget header end -->
                        <!-- widget body start -->
                        <div class="widget-body">
                            <!-- widget main start -->
                            <div class="widget-main padding-0">
                                <!-- widget slim control start -->
                                <div class="slim-scroll" data-height="200">
                                    <!-- widget content start -->
                                    <div class="content">

                                        <!-- table responsive start -->
                                        <div class="table-responsive">
                                            <div class="dataTables_wrapper" role="grid">
                                                <table id="Customer Underlying-table-ipe" class="table table-striped table-bordered table-hover dataTable">
                                                    <thead>
                                                        <tr data-bind="with: $root.UnderlyingGridProperties">
                                                            <th style="width: 50px">No.</th>
                                                            <th style="width: 50px">Utilize</th>
                                                            <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                            <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                            <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                            <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                            <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                            <th data-bind="click: function () { Sorting('AvailableAmount'); }, css: GetSortedColumn('AvailableAmount')">Available Amount in USD</th>
                                                            <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                            <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                            <th data-bind="click: function () { Sorting('AttachmentNo'); }, css: GetSortedColumn('AttachmentNo')">Attachment No</th>
                                                            <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th>
                                                            <th style="width: 50px">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <thead data-bind="visible: $root.UnderlyingGridProperties().AllowFilter" class="table-filter">
                                                        <tr>
                                                            <th class="clear-filter">
                                                                <a href="#" data-bind="click: $root.UnderlyingClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear 
Filters">
                                                                    <i class="green icon-trash"></i>
                                                                </a>
                                                            </th>
                                                            <th class="clear-filter"></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterStatementLetter, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterUnderlyingDocument, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterDocumentType, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterCurrency, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterAmount, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterAvailableAmount, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.UnderlyingFilterDateOfUnderlying, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterSupplierName, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterAttachmentNo, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterReferenceNumber, event: { change: 
$root.UnderlyingGridProperties().Filter }" /></th>
                                                            <th class="clear-filter"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody data-bind="foreach: $root.CustomerUnderlyings, visible: $root.CustomerUnderlyings().length > 0">
                                                        <tr>
                                                            <td><span data-bind="text: RowID"></span></td>
                                                            <td align="center">
                                                                <input type="checkbox" id="isUtilize-ipe" name="isUtilize" data-bind="checked:IsEnable,event: {change:function(data){$root.onSelectionUtilize($index(),$data)}}, disable: !$root.IsEditable()" /></td>
                                                            <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                            <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                            <td><span data-bind="text: DocumentType.Name"></span></td>
                                                            <td><span data-bind="text: Currency.Code"></span></td>
                                                            <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                                            <td align="right"><span data-bind="text: formatNumber(AvailableAmount)"></span></td>
                                                            <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                            <td><span data-bind="text: SupplierName"></span></td>
                                                            <td><span data-bind="text: AttachmentNo"></span></td>
                                                            <td><span data-bind="text: ReferenceNumber"></span></td>
                                                            <td><span class="label label-info arrowed-in-right arrowed" data-bind="click: $root.GetUnderlyingSelectedRow,disable: !$root.IsEditable()">view</span></td>
                                                        </tr>
                                                    </tbody>
                                                    <tbody data-bind="visible: $root.CustomerUnderlyings().length == 0">
                                                        <tr>
                                                            <td colspan="13" class="text-center">No entries available.
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- table responsive end -->
                                    </div>
                                    <!-- widget content end -->
                                </div>
                                <!-- widget slim control end -->
                                <!-- widget footer start -->

                                <div class="widget-toolbox padding-8 clearfix">
                                    <div class="row" data-bind="with: $root.UnderlyingGridProperties">
                                        <!-- pagination size start -->
                                        <div class="col-sm-6">
                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                Showing
                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                rows of <span data-bind="text: Total"></span>
                                                entries
                                            </div>
                                        </div>
                                        <!-- pagination size end -->
                                        <!-- pagination page jump start -->
                                        <div class="col-sm-3">
                                            <div class="dataTables_paginate paging_bootstrap">
                                                Page
                                                <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" /> of <span data-bind="text: TotalPages"></span>
                                            </div>
                                        </div>
                                        <!-- pagination page jump end -->
                                        <!-- pagination navigation start -->
                                        <div class="col-sm-3">
                                            <div class="dataTables_paginate paging_bootstrap">
                                                <ul class="pagination">
                                                    <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                            <i class="icon-double-angle-left"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                            <i class="icon-angle-left"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                            <i class="icon-angle-right"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                            <i class="icon-double-angle-right"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- pagination navigation end -->

                                    </div>
                                </div>

                                <!-- widget footer end -->

                            </div>
                            <!-- widget main end -->
                        </div>
                        <!-- widget body end -->
                    </div>
                    <!-- widget box end -->


                </div>
                <!--- Added form custome Customer Underlying End -->
                <div class="space-10"></div>
                <div data-bind="if: Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR'">
                    <div class="form-horizontal" role="form">

                        <div class="form-group">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="underlying-code">Underlying Code</label>
                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <select class="col-sm-12" id="underlying-code-ipe" name="underlying-code" data-bind="options: $root.Parameter().UnderlyingDocs, optionsText: function(item) { if(item.ID != null) return item.Code + ' (' + item.Name + ')'} , optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().UnderlyingDoc, disable: !$root.IsEditable()" data-rule-required="true"></select>

                                </div>
                            </div>
                        </div>

                        <div class="form-group" data-bind="visible: $root.isNewUnderlying()">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="underlying-desc">Other Underlying</label>

                            <div class="col-xs-9 align-left">
                                <div class="clearfix">
                                    <textarea class="col-lg-5" id="underlying-desc-ipe" rows="4" data-rule-required="true" data-bind="value: OtherUnderlyingDoc, enable: $root.isNewUnderlying, disable: !$root.IsEditable()"></textarea>

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="utilization-amount">Utilization Amount</label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="text" autocomplete="off" class="col-sm-4 align-right" name="utilization-amount" id="utilization-amount" rule-required="true" disabled="disabled" data-rule-number="true" data-bind="value: formatNumber($root.UtilizationAmounts())" />
                                </div>
                            </div>
                        </div>

                        <!--<div class="form-group">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="totalTrx">Total FX-IDR Transaction in USD</label>
                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="text" autocomplete="off" class="col-sm-4 align-right" name="totalTrx" id="totalTrx" disabled="disabled" data-bind="value:TotalTransFX" />
                                </div>
                            </div>
                        </div>-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="tz-number">TZ Number</label>

                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <input type="text" autocomplete="off" class="col-sm-6" name="tz-number" id="tz-number-ipe" data-bind="value: TZNumber, disable: !$root.IsEditable()" />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="space-4"></div>
                <div class="padding-4">
                    <h3 class="header smaller lighter dark">Instruction Form and Docs
                        <button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.UploadDocumentUnderlying, disable: false,visible:(Currency.Code != 'IDR' && $root.CurrencyDep().Code == 
'IDR')" onclick="return false">
                            <i class="icon-plus"></i>
                            Attach
                        </button>
                        <button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.UploadDocument, disable: false,visible:!(Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR')" 
onclick="return false">
                            <i class="icon-plus"></i>
                            Attach
                        </button>
                    </h3>
                    <!--data-bind="visible:!$root.IsUnderlyingMode()" -->
                    <div class="dataTables_wrapper" role="grid">
                        <table class="table table-striped table-bordered table-hover dataTable">
                            <thead>
                                <tr>
                                    <th style="width: 50px">No.</th>
                                    <th>File Name</th>
                                    <th>Purpose of Docs</th>
                                    <th>Type of Docs</th>
                                    <th>Modified</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody data-bind="foreach: $root.Documents, visible: $root.Documents().length > 0">
                                <tr>
                                    <!--data-bind="click: $root.EditDocument"-->
                                    <td><span data-bind="text: $index() +1"></span></td>
                                    <td><span data-bind="text: FileName"></span></td>
                                   <%-- <td><span data-bind="text: Purpose.Name"></span></td>
                                    <td><span data-bind="text: Type.Name"></span></td>--%>
                                    <td><span data-bind="text: Purpose.Name"></span></td>
                                    <td><span data-bind="text: Type.Name"></span></td>
                                    <td><span data-bind="text: moment(DocumentPath.lastModifiedDate).format(config.format.dateTime)"></span></td>
                                    <!--$root.LocalDate(LastModifiedDate)-->
                                    <td><a href="#" data-bind="click: $root.RemoveDocument">Remove</a></td>
                                </tr>
                            </tbody>
                            <tbody data-bind="visible: $root.Documents().length == 0">
                                <tr>
                                    <td colspan="6" align="center">No entries available.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="space-10"></div>

                    <!--- Grid Underlying Proforma Start -->
                    <!-- widget header start -->
                    <div data-bind="if: Currency.Code != 'IDR' && $root.CurrencyDep().Code == 'IDR'">
                        <h3 class="header smaller lighter dark">Underlying Docs
                        </h3>
                        <div class="widget-header widget-hea1der-small header-color-dark">
                            <h6>Attach File Table</h6>
                            <div class="widget-toolbar">
                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                    <i class="blue icon-filter"></i>
                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: $root.AttachGridProperties().AllowFilter" />
                                    <span class="lbl"></span>
                                </label>
                                <a href="#" data-bind="click: $root.GetDataAttachFile" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                    <i class="blue icon-refresh"></i>
                                </a>
                            </div>
                        </div>
                        <!-- widget header end -->
                        <div class="widget-body">
                            <!-- widget main start -->
                            <div class="widget-main padding-0">
                                <!-- widget slim control start -->
                                <div class="slim-scroll" data-height="200">
                                    <!-- widget content start -->
                                    <div class="content">

                                        <!-- table responsive start -->
                                        <div class="table-responsive">
                                            <div class="dataTables_wrapper" role="grid">
                                                <table id="Customer Underlying-table-ipe" class="table table-striped table-bordered table-hover dataTable">
                                                    <thead>
                                                        <tr data-bind="with: $root.AttachGridProperties">
                                                            <th style="width: 50px">No.</th>
                                                            <th data-bind="click: function () { Sorting('FileName'); }, css: GetSortedColumn('FileName')">File Name</th>
                                                            <th data-bind="click: function () { Sorting('DocumentPurpose'); }, css: GetSortedColumn('DocumentPurpose')">Document Purpose</th>
                                                            <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Update</th>
                                                            <th data-bind="click: function () { Sorting('DocumentRefNumber'); }, css: GetSortedColumn('DocumentRefNumber')">Document Ref Number</th>
                                                            <th style="width: 0px; display: none"></th>
                                                            <th style="width: 50px">Delete</th>
                                                        </tr>
                                                    </thead>
                                                    <thead data-bind="visible: $root.AttachGridProperties().AllowFilter" class="table-filter">
                                                        <tr>
                                                            <th class="clear-filter">
                                                                <a href="#" data-bind="click: $root.AttachClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                    <i class="green icon-trash"></i>
                                                                </a>
                                                            </th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.AttachFilterFileName, event: { change: 
$root.AttachGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.AttachFilterDocumentPurpose, event: { change: 
$root.AttachGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.AttachFilterLastModifiedDate, event: { change: $root.AttachGridProperties().Filter }" /></th>
                                                            <th>
                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.AttachFilterDocumentRefNumber, event: { change: 
$root.AttachGridProperties().Filter }" /></th>
                                                            <th class="clear-filter" style="display: none"></th>
                                                            <th class="clear-filter"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody data-bind="foreach: $root.CustomerUnderlyingFiles, visible: $root.CustomerUnderlyingFiles().length > 0">
                                                        <tr data-bind="click: $root.GetAttachSelectedRow" data-toggle="modal">
                                                            <td><span data-bind="text: RowID"></span></td>
                                                            <td><span data-bind="text: FileName"></span></td>
                                                            <td><span data-bind="text: DocumentPurpose.Name"></span></td>
                                                            <td><span data-bind="text: $root.LocalDate(LastModifiedDate,false,false)"></span></td>
                                                            <td><span data-bind="text: DocumentRefNumber"></span></td>
                                                            <td style="width: 0px; display: none"><span data-bind="text: DocumentPath"></span></td>
                                                            <td align="center">
                                                                <!--<a href="#" data-bind="click:$root.delete_a" class="tooltip-info" data-rel="tooltip" data-placement="right">
                                                                    <i class="green icon-trash"></i>
                                                                </a>-->
                                                                <i class="gray icon-trash"></i>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                    <tbody data-bind="visible: $root.CustomerUnderlyingFiles().length == 0">
                                                        <tr>
                                                            <td colspan="10" class="text-center">No entries available.
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- table responsive end -->
                                    </div>
                                    <!-- widget content end -->
                                </div>
                                <!-- widget slim control end -->
                                <!-- widget footer start -->
                                <div class="widget-toolbox padding-8 clearfix">
                                    <div class="row" data-bind="with: $root.AttachGridProperties">
                                        <!-- pagination size start -->
                                        <div class="col-sm-6">
                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                Showing
                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-
placement="bottom" title="Showing Page Entries"></select>
                                                rows of <span data-bind="text: Total"></span>
                                                entries
                                            </div>
                                        </div>
                                        <!-- pagination size end -->
                                        <!-- pagination page jump start -->
                                        <div class="col-sm-3">
                                            <div class="dataTables_paginate paging_bootstrap">
                                                Page
                                                <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" 
data-placement="bottom" title="Jump to Page" />
                                                of <span data-bind="text: TotalPages"></span>
                                            </div>
                                        </div>
                                        <!-- pagination page jump end -->
                                        <!-- pagination navigation start -->
                                        <div class="col-sm-3">
                                            <div class="dataTables_paginate paging_bootstrap">
                                                <ul class="pagination">
                                                    <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                            <i class="icon-double-angle-left"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                            <i class="icon-angle-left"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                            <i class="icon-angle-right"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                            <i class="icon-double-angle-right"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- pagination navigation end -->

                                    </div>
                                </div>
                                <!-- widget footer end -->

                            </div>
                            <!-- widget main end -->
                        </div>
                    </div>
                    <!-- widget body end -->
                    <!--- Grid Underlying Proforma End -->

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 align-right">
            <button class="btn btn-sm btn-primary" data-bind="click: Submit, disable: $root.IsEditable()">
                <i class="icon-save"></i>
                Submit
            </button>

            <button class="btn btn-sm btn-success" data-bind="click: $root.cancel_detail" data-dismiss="modal">
                <i class="icon-remove"></i>
                Cancel
            </button>
        </div>
    </div>

    <!-- modal form start Attach Document Underlying Form start -->
    <div id="modal-form-Attach-ipe" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <!-- modal header start Attach FIle Form -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span>Added Attachment File</span>
                    </h4>
                </div>
                <!-- modal header end Attach file -->
                <!-- modal body start Attach File -->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div id="customer-form" class="form-horizontal" role="form">
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="Name">
                                        Purpose of Doc
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentPurpose-ipe" name="documentPurpose" data-bind="value:$root.DocumentPurpose_a().ID, options: ddlDocumentPurpose_a, optionsText: 
'Name',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="description">
                                        Type of Doc
                                    </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentType-ipe" name="documentType" data-bind="value:$root.DocumentType_a().ID, options: ddlDocumentType_a, optionsText: 'Name',optionsValue: 
'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-path">Document</label>

                                    <div class="col-sm-6">
                                        <div class="clearfix">
                                            <input type="file" id="document-path-ipe" name="document-path" data-bind="file: DocumentPath_a" class="col-xs-7" data-rule-required="true" data-rule-value="true" 
/><!---->
                                            <!--<span data-bind="text: DocumentPath"></span>-->
                                        </div>
                                    </div>
                                    <label class="control-label bolder text-danger">*</label>
                                </div>
                            </div>
                            <!-- modal body form Attach File End -->
                            <!-- widget box Underlying Attach start -->
                            <div id="widget-box" class="widget-box">
                                <!-- widget header Underlying Attach start -->
                                <div class="widget-header widget-hea1der-small header-color-dark">
                                    <h6>Underlying Form Table</h6>

                                    <div class="widget-toolbar">
                                        <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                            <i class="blue icon-filter"></i>
                                            <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingAttachGridProperties().AllowFilter" />
                                            <span class="lbl"></span>
                                        </label>
                                        <a href="#" data-bind="click: GetDataUnderlyingAttach" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                            <i class="blue icon-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                                <!-- widget header end -->
                                <!-- widget body underlying Attach start -->
                                <div class="widget-body">
                                    <!-- widget main start -->
                                    <div class="widget-main padding-0">
                                        <!-- widget slim control start -->
                                        <div class="slim-scroll" data-height="400">
                                            <!-- widget content start -->
                                            <div class="content">
                                                <!-- table responsive start -->
                                                <div class="table-responsive">
                                                    <div class="dataTables_wrapper" role="grid">
                                                        <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                            <thead>
                                                                <tr data-bind="with: $root.UnderlyingAttachGridProperties">
                                                                    <!-- <th style="width:50px">Select</th> -->
                                                                    <th style="width: 50px">No.</th>
                                                                    <th style="width: 50px">Select</th>
                                                                    <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                    <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                    <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                    <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                    <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                    <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                    <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                    <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th>
                                                                    <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                                </tr>
                                                            </thead>
                                                            <thead data-bind="visible: $root.UnderlyingAttachGridProperties().AllowFilter" class="table-filter">
                                                                <tr>
                                                                    <th class="clear-filter">
                                                                        <a href="#" data-bind="click: $root.UnderlyingAttachClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" 
title="Clear Filters">
                                                                            <i class="green icon-trash"></i>
                                                                        </a>
                                                                    </th>
                                                                    <th class="clear-filter"></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterStatementLetter, event: { 
    change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterUnderlyingDocument, 
    event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterDocumentType, event: { 
    change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterCurrency, event: { 
    change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterAmount, event: { change: 
UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.UnderlyingAttachFilterDateOfUnderlying, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterSupplierName, event: { 
    change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterReferenceNumber, event: { 
    change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.UnderlyingAttachFilterExpiredDate, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody data-bind="foreach: {data: $root.CustomerAttachUnderlyings, as: 'AttachData'}, visible: $root.CustomerAttachUnderlyings().length > 0">
                                                                <tr data-toggle="modal">
                                                                    <td><span data-bind="text: RowID"></span></td>
                                                                    <td align="center">
                                                                        <input type="checkbox" id="isSelected2-ipe" name="isSelect2" data-bind="checked:IsSelectedAttach,event:{change:function(data)
{$root.onSelectionAttach($index(),$data)}}, disable: !($root.SelectingUnderlying())" /></td>
                                                                    <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                    <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                    <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                    <td><span data-bind="text: Currency.Code"></span></td>
                                                                    <td><span data-bind="text: Amount"></span></td>
                                                                    <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                                    <td><span data-bind="text: SupplierName"></span></td>
                                                                    <td><span data-bind="text: ReferenceNumber"></span></td>
                                                                    <td><span data-bind="text: $root.LocalDate(ExpiredDate,true,false)"></span></td>
                                                                </tr>
                                                            </tbody>
                                                            <tbody data-bind="visible: $root.CustomerAttachUnderlyings().length == 0">
                                                                <tr>
                                                                    <td colspan="11" class="text-center">No entries available.
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- table responsive end -->
                                            </div>
                                            <!-- widget content end -->
                                        </div>
                                        <!-- widget slim control end -->
                                        <!-- widget footer start -->
                                        <div class="widget-toolbox padding-8 clearfix">
                                            <div class="row" data-bind="with: UnderlyingAttachGridProperties">
                                                <!-- pagination size start -->
                                                <div class="col-sm-6">
                                                    <div class="dataTables_paginate paging_bootstrap pull-left">
                                                        Showing
                                                        <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-
placement="bottom" title="Showing Page Entries"></select>
                                                        rows of <span data-bind="text: Total"></span>
                                                        entries
                                                    </div>
                                                </div>
                                                <!-- pagination size end -->
                                                <!-- pagination page jump start -->
                                                <div class="col-sm-3">
                                                    <div class="dataTables_paginate paging_bootstrap">
                                                        Page
                                                        <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-
rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                        of <span data-bind="text: TotalPages"></span>
                                                    </div>
                                                </div>
                                                <!-- pagination page jump end -->
                                                <!-- pagination navigation start -->
                                                <div class="col-sm-3">
                                                    <div class="dataTables_paginate paging_bootstrap">
                                                        <ul class="pagination">
                                                            <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                    <i class="icon-double-angle-left"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                    <i class="icon-angle-left"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                    <i class="icon-angle-right"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                    <i class="icon-double-angle-right"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- pagination navigation end -->

                                            </div>
                                        </div>
                                        <!-- widget footer end -->

                                    </div>
                                    <!-- widget main end -->
                                </div>
                                <!-- widget body end -->
                            </div>
                            <!-- widget box end -->



                        </div>
                    </div>
                </div>
                <!-- modal body end Attach File -->
                <!-- modal footer start Attach File-->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.save_a">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.cancel_a">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
                <!-- modal footer end attach file -->

            </div>
        </div>
    </div>
    <!-- modal form end Attach Document Underlying Form end -->
    <!-- Modal Double Transaction start -->
    <!-- Modal Double Transaction end -->
    <!-- Modal upload form start -->
    <div id="modal-form-upload-ipe" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="customer-form" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-type">Document Type</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="document-type-ipe" name="document-type" data-rule-required="true" data-rule-value="true" data-bind="options:$root.ddlDocumentType_a(), optionsText: 
'Name', optionsValue: 'ID', optionsCaption: 'Please select...', value: DocumentTypeValue"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-purpose">Purpose of Docs</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="document-purpose-ipe" name="document-purpose" data-rule-required="true" data-rule-value="true" data-bind="options: $root.ddlDocumentPurpose_a(), 
    optionsText: 'Name', optionsValue: 'ID',  optionsCaption: 'Please Select...', value: DocumentPurposeValue"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-path">Document</label>

                                    <div class="col-sm-8">
                                        <div class="clearfix">
                                            <input type="file" id="document-path" name="document-path" data-bind="file: DocumentPath" /><!---->
                                            <!--<span data-bind="text: DocumentPath"></span>-->
                                        </div>
                                    </div>
                                    <label class="control-label bolder text-danger">*</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.AddDocument">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.cancel_a">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal upload form end -->
    <!-- modal form start Underlying Form -->
    <div id="modal-form-Underlying-ipe" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <!-- modal header start Underlying Form -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span data-bind="if: $root.IsNewDataUnderlying()">Create a new Customer Underlying Parameter</span>
                        <span data-bind="if: !$root.IsNewDataUnderlying()">Modify a Customer Underlying Parameter</span>
                    </h4>
                </div>
                <!-- modal header end -->
                <!-- modal body start -->
                <div class="modal-body overflow-visible">
                    <div id="modalUnderlyingtest-ipe">
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- modal body form start -->

                                <div id="customerUnderlyingform-ipe" class="form-horizontal" role="form">
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="StatementLetterID">
                                            Statement Letter
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="statementLetter-ipe" name="statementLetter" data-bind="value:$root.StatementLetter_u().ID, options: ddlStatementLetter_u, optionsText: 
'Name',optionsValue: 'ID',optionsCaption: 'Please Select...',enable:$root.IsStatementA()" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="UnderlyingDocument">
                                            Underlying Document
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="underlyingDocument-ipe" name="underlyingDocument" data-bind="value:UnderlyingDocument_u().ID, options: ddlUnderlyingDocument_u, optionsText: 
'CodeName',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <!--    						<span aa-bind="text:UnderlyingDocument_u().Code()"></span>-->
                                    <div data-bind="visible:UnderlyingDocument_u().ID()=='50'" class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="otherUnderlying">
                                            Other Underlying
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="otherUnderlying-ipe" name="otherUnderlying" data-bind="value: $root.OtherUnderlying_u" class="col-xs-7" />
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="DocumentType">
                                            Type of Document
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="documentType" name="documentType" data-bind="value:$root.DocumentType_u().ID, options: ddlDocumentType_u, optionsText: 'Name',optionsValue: 
'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Currency">
                                            Transaction Currency
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="currency_u-ipe" name="currency_u" data-bind="value:$root.Currency_u().ID,disable:$root.StatementLetter_u().ID()==1, options:ddlCurrency_u, 
    optionsText:'Code',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-2"></select>
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Amount">
                                            Invoice Amount
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="Amount_u-ipe" name="Amount_u-ipe" data-bind="value: $root.Amount_u,disable:$root.StatementLetter_u().ID
()==1,valueUpdate:['afterkeydown','propertychange','input']" data-rule-required="true" data-rule-number="true" class="col-xs-4 align-right" />
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Rate">Rate</label>

                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" class="col-sm-4 align-right" name="rate_u" id="rate_u-ipe" disabled="disabled" data-rule-required="true" data-rule-
number="true" data-bind="value: Rate_u" />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Eqv.USD">Eqv. USD</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" class="col-sm-4 align-right" disabled="disabled" name="eqv-usd_u" id="eqv-usd_u" data-rule-required="true" data-rule-
number="true" data-bind="value: AmountUSD_u" />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="DateOfUnderlying">
                                            Date of Underlying
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <input type="text" autocomplete="off" id="DateOfUnderlying-ipe" data-date-format="dd-M-yyyy" name="DateOfUnderlying_u" data-bind="value:DateOfUnderlying_u" 
class="form-control date-picker col-xs-6" data-rule-required="true" />
                                                <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                <label class="control-label bolder text-danger starremove">*</label>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label no-padding-right" for="ExpiredDate">
                                            Expiry Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <input type="text" autocomplete="off" id="ExpiredDate-ipe" data-date-format="dd-M-yyyy" name="ExpiredDate" data-bind="value:ExpiredDate_u" class="form-control 
date-picker col-xs-6" data-rule-required="true" />
                                                <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                <label class="control-label bolder text-danger starremove">*</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="IsDeclarationOfException">
                                            Declaration of Exception
                                        </label>
                                        <div class="col-sm-9">
                                            <label>
                                                <input name="switch-field-1" id="IsDeclarationOfException-ipe" data-bind="checked: $root.IsDeclarationOfException_u" class="ace ace-switch ace-switch-6" 
type="checkbox" />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="StartDate">
                                            Start Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <input id="StartDate-ipe" name="StartDate" data-date-format="dd-M-yyyy" data-bind="value:StartDate_u" class="form-control date-picker col-xs-6" type="text" 
autocomplete="off" />
                                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                    <label class="control-label bolder text-danger starremove">*</label>
                                                </div>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label no-padding-right" for="EndDate">
                                            End Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <input type="text" autocomplete="off" id="EndDate-ipe" name="EndDate" data-date-format="dd-M-yyyy" data-bind="value:EndDate_u" class="form-control date-
picker col-xs-6" />
                                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                    <label class="control-label bolder text-danger starremove">*</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="ReferenceNumber">
                                            Reference Number
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="ReferenceNumber-ipe" name="ReferenceNumber" data-bind="value: $root.ReferenceNumber_u" disabled="disabled" 
class="col-xs-9" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="SupplierName">
                                            Supplier Name
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="SupplierName-ipe" name="SupplierName" data-bind="value: $root.SupplierName_u" class="col-xs-8" data-rule-
required="true" />
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="InvoiceNumber">
                                            Invoice Number
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="invoiceNumber-ipe" name="invoiceNumber" data-bind="value: $root.InvoiceNumber_u" class="col-xs-8" data-rule-
required="true" />
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--								<div class="space-2"></div>
                                                                <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="AttachmentNo">
                                                                Attachment No.</label>
                                                                <div class="col-sm-9">
                                                                        <div class="clearfix">
                                                                            <input type="text" autocomplete="off" id="AttachmentNo" name="AttachmentNo" data-bind="value: $root.AttachmentNo_u" class="col-xs-
8"/>
                                                                        </div>
                                                                    </div>
                                                                </div>-->
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="AttachmentNo">
                                            Is This doc Purposed to replace the proforma doc
                                        </label>
                                        <div class="col-sm-9">
                                            <label>
                                                <input name="switch-field-1" id="IsProforma-ipe" data-bind="checked:IsProforma_u,disable:DocumentType_u().ID()==2" class="ace ace-switch ace-switch-6" 
type="checkbox" />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <!-- grid proforma begin -- >

                                    <!-- widget box start -->
                                    <div id="widget-box" class="widget-box" data-bind="visible:IsProforma_u()">
                                        <!-- widget header start -->
                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                            <h6>Customer Underlying Proforma Table</h6>

                                            <div class="widget-toolbar">
                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                    <i class="blue icon-filter"></i>
                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingProformaGridProperties().AllowFilter" />
                                                    <span class="lbl"></span>
                                                </label>
                                                <a href="#" data-bind="click: GetDataUnderlyingProforma" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                    <i class="blue icon-refresh"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- widget header end -->
                                        <!-- widget body start -->
                                        <div class="widget-body">
                                            <!-- widget main start -->
                                            <div class="widget-main padding-0">
                                                <!-- widget slim control start -->
                                                <div class="slim-scroll" data-height="400">
                                                    <!-- widget content start -->
                                                    <div class="content">

                                                        <!-- table responsive start -->
                                                        <div class="table-responsive">
                                                            <div class="dataTables_wrapper" role="grid">
                                                                <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                                    <thead>
                                                                        <tr data-bind="with: UnderlyingProformaGridProperties">
                                                                            <th style="width: 50px">No.</th>
                                                                            <th style="width: 50px">Select</th>
                                                                            <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                            <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying 
Document</th>
                                                                            <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                            <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                            <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                            <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                            <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                            <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th>
                                                                            <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <thead data-bind="visible: UnderlyingProformaGridProperties().AllowFilter" class="table-filter">
                                                                        <tr>
                                                                            <th class="clear-filter">
                                                                                <a href="#" data-bind="click: ProformaClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear 
Filters">
                                                                                    <i class="green icon-trash"></i>
                                                                                </a>
                                                                            </th>
                                                                            <th class="clear-filter"></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterStatementLetter, event: { 
    change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterUnderlyingDocument, 
    event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterDocumentType, event: { 
    change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterCurrency, event: { 
    change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterAmount, event: { change: 
UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.ProformaFilterDateOfUnderlying, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterSupplierName, event: { 
    change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterReferenceNumber, event: { 
    change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: 
$root.ProformaFilterExpiredDate, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody data-bind="foreach: {data: CustomerUnderlyingProformas, as: 'ProformasData'}, visible: CustomerUnderlyingProformas().length > 0">
                                                                        <tr data-toggle="modal">
                                                                            <td><span data-bind="text: RowID"></span></td>
                                                                            <td align="center">
                                                                                <input type="checkbox" id="isSelectedProforma-ipe" name="isSelect" data-bind="checked:ProformasData.IsSelectedProforma, event: 
{change:function(data){$root.onSelectionProforma($index(),$data)}}" /></td>
                                                                            <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                            <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                            <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                            <td><span data-bind="text: Currency.Code"></span></td>
                                                                            <td><span data-bind="text: Amount"></span></td>
                                                                            <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                                            <td><span data-bind="text: SupplierName"></span></td>
                                                                            <td><span data-bind="text: ReferenceNumber"></span></td>
                                                                            <td><span data-bind="text: $root.LocalDate(ExpiredDate,true,false)"></span></td>
                                                                        </tr>
                                                                    </tbody>
                                                                    <tbody data-bind="visible: CustomerUnderlyingProformas().length == 0">
                                                                        <tr>
                                                                            <td colspan="11" class="text-center">No entries available.
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <!-- table responsive end -->
                                                    </div>
                                                    <!-- widget content end -->
                                                </div>
                                                <!-- widget slim control end -->
                                                <!-- widget footer start -->
                                                <div class="widget-toolbox padding-8 clearfix">
                                                    <div class="row" data-bind="with: UnderlyingProformaGridProperties">
                                                        <!-- pagination size start -->
                                                        <div class="col-sm-6">
                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                Showing
                                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" 
data-placement="bottom" title="Showing Page Entries"></select>
                                                                rows of <span data-bind="text: Total"></span>
                                                                entries
                                                            </div>
                                                        </div>
                                                        <!-- pagination size end -->
                                                        <!-- pagination page jump start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                Page
                                                                <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-
rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                of <span data-bind="text: TotalPages"></span>
                                                            </div>
                                                        </div>
                                                        <!-- pagination page jump end -->
                                                        <!-- pagination navigation start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                <ul class="pagination">
                                                                    <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                            <i class="icon-double-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                            <i class="icon-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                            <i class="icon-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                            <i class="icon-double-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <!-- pagination navigation end -->

                                                    </div>
                                                </div>
                                                <!-- widget footer end -->

                                            </div>
                                            <!-- widget main end -->
                                        </div>
                                        <!-- widget body end -->

                                    </div>
                                    <!-- widget box end -->
                                    <!-- grid proforma end -- >
                                    </div>

                                    <!-- modal body form end -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal body end -->
                    <!-- modal footer start -->
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-primary" data-bind="click: $root.save_u, visible: $root.IsNewDataUnderlying()">
                            <i class="icon-save"></i>
                            Save
                        </button>
                        <button class="btn btn-sm btn-primary" data-bind="click: $root.update_u, visible: !$root.IsNewDataUnderlying() && !$root.IsUtilize_u()">
                            <i class="icon-edit"></i>
                            Update
                        </button>
                        <button class="btn btn-sm btn-warning" data-bind="click: $root.delete_u, visible: !$root.IsNewDataUnderlying() && !$root.IsUtilize_u()">
                            <i class="icon-trash"></i>
                            Delete
                        </button>
                        <button class="btn btn-sm" data-bind="click: $root.cancel_u" data-dismiss="modal">
                            <i class="icon-remove"></i>
                            Cancel
                        </button>
                    </div>
                    <!-- modal footer end -->

                </div>
            </div>
        </div>
        <!-- modal form end underlying form -->
    </div>

</div>


<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/Helper.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/PendingDocuments.js"></script>

