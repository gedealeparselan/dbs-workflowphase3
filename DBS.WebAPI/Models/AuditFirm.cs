﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Data.Entity.SqlServer;
using System.Web;


namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("AuditFirm")]
    public class AuditFirmModel
    {
        public int AuditFirmID { get; set; }
        public string CIF { get; set; }
        public string NPWP { get; set; }
        public string Address { get; set; }
        public virtual Customer Customer { get; set; }
        public DateTime? LastModifiedDate { get; set; }        
        public string LastModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
        public string Name { get; set; }
        
    }
    public class AuditFirmRow:AuditFirmModel
    {
        public int RowID { get; set; }

    }

    public class AuditFirmGrid : Grid
    {
        public IList<AuditFirmRow> Rows { get; set; }
    }

    #endregion
    #region Filter
    public class AuditFirmFilter : Filter { }
    #endregion
    #region Interface
    public interface IAuditFirmRepository : IDisposable
    {
        bool GetAuditFirmByID(int id, ref AuditFirmModel auditFirm, ref string message);
        bool GetAuditFirm(ref IList<AuditFirmModel> auditFirms, int limit, int index, ref string message);
        bool GetAuditFirm(ref IList<AuditFirmModel> auditFirms, ref string message);
        bool GetAuditFirm(int page, int size, IList<AuditFirmFilter> filters, string sortColumn, string sortOrder, ref AuditFirmGrid auditFirms, ref string message);
        bool GetAuditFirm(AuditFirmFilter filter, ref IList<AuditFirmModel> auditFirms, ref string message);
        bool GetAuditFirm(string key, int limit, ref IList<AuditFirmModel> auditFirms, ref string message);
        bool AddAuditFirm(AuditFirmModel auditFirms, ref string message);
        bool UpdateAuditFirm(int id, AuditFirmModel auditFirms, ref string message);
        bool DeleteAuditFirm(int id, ref string message);
    }
    #endregion
    #region Repository
    public class AuditFirmRepository : IAuditFirmRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetAuditFirmByID(int id, ref AuditFirmModel auditFirm, ref string message)
        {
            bool isSuccess = false;

            try
            {
                auditFirm = (from a in context.AuditFirms
                        //join b in context.Customers on a.CIF equals b.CIF
                        where a.IsDeleted.Equals(false) && a.AuditFirmID.Equals(id)
                        select new AuditFirmModel
                        {
                            AuditFirmID = a.AuditFirmID,
                            CIF = a.CIF,
                          //  Customer = a.Customer,
                            NPWP = a.NPWP,
                            Address = a.Address,                          
                            LastModifiedDate = a.CreateDate,
                            LastModifiedBy = a.CreateBy
                        }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetAuditFirm(ref IList<AuditFirmModel> auditFirms, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    auditFirms = (from a in context.AuditFirms
                             //join b in context.Customers on a.CIF equals b.CIF
                             where a.IsDeleted.Equals(false)
                             orderby new { a.CIF}
                             //orderby new { a.CIF, a.Customer, a.NPWP, a.Address}
                             select new AuditFirmModel
                             {
                                 AuditFirmID = a.AuditFirmID,
                                 CIF = a.CIF,
                                 //Customer = a.Customer,
                                 NPWP = a.NPWP,
                                 Address = a.Address,                          
                                 LastModifiedDate = a.CreateDate,
                                 LastModifiedBy = a.CreateBy                                 
                             }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetAuditFirm(ref IList<AuditFirmModel> auditFirms, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    auditFirms = (from a in context.AuditFirms
                             //join b in context.Customers on a.CIF equals b.CIF
                             where a.IsDeleted.Equals(false)
                             orderby new { a.CIF }
                             select new AuditFirmModel
                             {
                                 AuditFirmID = a.AuditFirmID,
                                 CIF = a.CIF,
                                 //Customer = a.Customer,
                                 NPWP = a.NPWP,
                                 Address = a.Address,                          
                                 LastModifiedDate = a.CreateDate,
                                 LastModifiedBy = a.CreateBy                                 
                             }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;

        }

        public bool GetAuditFirm(int page, int size, IList<AuditFirmFilter> filters, string sortColumn, string sortOrder, ref AuditFirmGrid AuditFirmGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();
                AuditFirmModel filter = new AuditFirmModel();

                //filter.Customer = new Customer { CustomerName = string.Empty };
                if (filters != null)
                {
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.Name = (string)filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.NPWP = (string)filters.Where(a => a.Field.Equals("NPWP")).Select(a => a.Value).SingleOrDefault();
                    filter.Address = (string)filters.Where(a => a.Field.Equals("Address")).Select(a => a.Value).SingleOrDefault();                 
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.AuditFirms
                                join b in context.Customers on a.CIF equals b.CIF                            
                                let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                let isCustomer = !string.IsNullOrEmpty(filter.Name)
                                let isNPWP = !string.IsNullOrEmpty(filter.NPWP)
                                let isAddress = !string.IsNullOrEmpty(filter.Address)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isCIF ? a.CIF.Contains(filter.CIF) : true) &&
                                    (isCustomer ? a.Customer.CustomerName.Contains(filter.Name) : true) &&
                                    (isNPWP ? a.NPWP.Contains(filter.NPWP) : true) &&
                                    (isAddress ? a.Address.Contains(filter.Address) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new AuditFirmModel
                                {
                                    AuditFirmID = a.AuditFirmID,
                                    CIF = a.CIF,
                                    Customer=a.Customer,//fandi
                                    NPWP = a.NPWP,
                                    Address = a.Address,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    AuditFirmGrid.Page = page;
                    AuditFirmGrid.Size = size;
                    AuditFirmGrid.Total = data.Count();
                    AuditFirmGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    AuditFirmGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new AuditFirmRow
                        {
                            RowID = i + 1,
                            AuditFirmID = a.AuditFirmID,
                            CIF = a.CIF,
                            //Customer = a.Customer,
                            Name= a.Customer.CustomerName,
                            NPWP = a.NPWP,
                            Address = a.Address,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetAuditFirm(AuditFirmFilter filter, ref IList<AuditFirmModel> auditFirms, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetAuditFirm(string key, int limit, ref IList<AuditFirmModel> auditFirms, ref string message)
        {
            bool isSuccess = false;

            try
            {
                auditFirms = (from a in context.AuditFirms
                         join b in context.Customers on a.CIF equals b.CIF
                         where (a.CIF.Contains(key) ||
                              a.Customer.CustomerName.Contains(key) || a.NPWP.Contains(key) || a.Address.Contains(key)
                              ) && a.IsDeleted.Equals(false)
                         orderby new { a.CIF }
                         select new AuditFirmModel
                         {
                             AuditFirmID = a.AuditFirmID,
                             CIF = a.CIF,
                             Customer = a.Customer,
                             NPWP = a.NPWP,
                             Address = a.Address,
                             IsDeleted = a.IsDeleted,
                             
                         }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddAuditFirm(AuditFirmModel auditFirm, ref string message)
        {
            bool isSuccess = false;

            if (auditFirm != null)
            {
                try
                {
                    if (!context.AuditFirms.Where(a => a.CIF.Equals(auditFirm.CIF) && a.IsDeleted.Equals(false)).Any())
                    {
                        Entity.AuditFirm data = new Entity.AuditFirm()
                        {
                            CIF = auditFirm.CIF,
                            //Customer = auditFirm.Customer,
                            NPWP = auditFirm.NPWP,
                            Address = auditFirm.Address,
                            IsDeleted = false,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().DisplayName
                        };
                        context.AuditFirms.Add(data);
                        context.SaveChanges();

                        isSuccess = true;
                    }
                    else
                    {
                        message = string.Format("Audit Firm data for CIF {0} is already exist.", auditFirm.CIF);
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return isSuccess;
        }

        public bool UpdateAuditFirm(int id, AuditFirmModel auditFirm, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.AuditFirm data = context.AuditFirms.Where(a => a.AuditFirmID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.AuditFirms.Where(a => a.AuditFirmID != auditFirm.AuditFirmID && a.CIF.Equals(auditFirm.CIF) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("CIF {0} is exist.", auditFirm.CIF);
                    }
                    else
                    {
                        data.CIF = auditFirm.CIF;
                        //data.Customer = auditFirm.Customer;
                        data.NPWP = auditFirm.NPWP;
                        data.Address = auditFirm.Address;                   
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Audit Firm data for Audit Firm ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteAuditFirm(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.AuditFirm data = context.AuditFirms.Where(a => a.AuditFirmID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Audit Firm data for CIF {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}