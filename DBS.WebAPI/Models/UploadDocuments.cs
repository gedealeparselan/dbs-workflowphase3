﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Data.SqlClient;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("UploadDocument")]
    public class UploadDocumentModel
    {
        public int ID { get; set; }
        public ParameterModel UploadType { get; set; }
        public string FileName { get; set; }
        public string DocumentPath { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }

    public class FailedUplaodModel
    {
        public int ID { get; set; }
        public string ItemName { get; set; }
        public string Status { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }

    public class FailedUplaodRow : FailedUplaodModel
    {
        public int RowID { get; set; }

    }

    public class FailedUplaodGrid : Grid
    {
        public IList<FailedUplaodRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class FailedUplaodFilter : Filter { }
    #endregion

    #region Interface
    public interface IUploadDocumentRepository : IDisposable
    {

        bool UploadDocumentUT(ref string message);
        bool GetFieldUpload(int page, int size, IList<FailedUplaodFilter> filters, string sortColumn, string sortOrder, ref FailedUplaodGrid customerType, ref string message);
    }
    #endregion

    #region Repository
    public class UploadDocumentRepository : IUploadDocumentRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool UploadDocumentUT(ref string message) {
            bool isStatus = false;
            try
            {
              /*  using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                {
                   
                    bulkCopy.DestinationTableName = "BulkLoadTable";
                    // How many records to send to the database in one go (all of them)
                    bulkCopy.BatchSize = myDataTable.Rows.Count;

                    // Load the data to the database
                    bulkCopy.WriteToServer(myDataTable);

                    // Close up          
                    bulkCopy.Close();
                }  */


                isStatus = true;
            }
            catch (Exception ex) {
                message = ex.Message;
            }
            return isStatus;
        }
       
  


        public bool GetFieldUpload(int page, int size, IList<FailedUplaodFilter> filters, string sortColumn, string sortOrder, ref FailedUplaodGrid failedUplaodGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

              /*  using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.TransactionContacts
                                 select new UploadDocumentModel
                                {
                                    ID = a.CustomerTypeID,
                                    Name = a.CustomerTypeName,
                                    Description = a.CustomerTypeDescription,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    failedUplaodGrid.Page = page;
                    failedUplaodGrid.Size = size;
                    failedUplaodGrid.Total = data.Count();
                    failedUplaodGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    failedUplaodGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new CustomerTypeRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Name = a.Name,
                            Description = a.Description,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                } */

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}