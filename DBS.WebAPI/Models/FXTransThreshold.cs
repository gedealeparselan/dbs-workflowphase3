﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("FXTransactionTreshold")]
    public class FXTransThresholdModel
    {
        /// <summary>
        /// FXTransThreshold ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// ThresholdAmount.
        /// </summary>
       // [MaxLength(3, ErrorMessage = "Threshold Amount Code is must be in {1} characters.")]
        public decimal ThresholdAmount { get; set; }

        /// <summary>
        /// DayOfTrx.
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public int DayOfTrx { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class FXTransThresholdRow : FXTransThresholdModel
    {
        public int RowID { get; set; }

    }

    public class FXTransThresholdGrid : Grid
    {
        public IList<FXTransThresholdRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class FXTransThresholdFilter : Filter {}
    #endregion

    #region Interface
    public interface IFXTransThresholdRepository : IDisposable
    {
        bool GetFXTransThresholdByID(int id, ref FXTransThresholdModel fXTransThreshold, ref string message);
        bool GetFXTransThreshold(ref IList<FXTransThresholdModel> fXTransThresholds, int limit, int index, ref string message);
        bool GetFXTransThreshold(int page, int size, IList<FXTransThresholdFilter> filters, string sortColumn, string sortOrder, ref FXTransThresholdGrid fXTransThreshold, ref string message);
        bool GetFXTransThreshold(FXTransThresholdFilter filter, ref IList<FXTransThresholdModel> fXTransThresholds, ref string message);
       // bool GetFXTransThreshold(string key, int limit, ref IList<FXTransThreshold> fXTransThresholds, ref string message);
        bool AddFXTransThreshold(FXTransThresholdModel fXTransThreshold, ref string message);
        bool UpdateFXTransThreshold(int id, FXTransThresholdModel fXTransThreshold, ref string message);
        bool DeleteFXTransThreshold(int id, ref string message);
    }
    #endregion

    #region Repository
    public class FXTransThresholdRepository : IFXTransThresholdRepository
    {
        private DBSEntities context = new DBSEntities();

        public bool GetFXTransThresholdByID(int id, ref FXTransThresholdModel fXTransThreshold, ref string message)
        {
            bool isSuccess = false;

            try
            {
                fXTransThreshold = (from a in context.FXTransactionThresholds
                            where a.IsDeleted.Equals(false) && a.ThresholdAmountID.Equals(id)
                            select new FXTransThresholdModel
                            {
                                ID = a.ThresholdAmountID,
                                ThresholdAmount  = a.ThresholdAmount,
                                DayOfTrx = a.DayOfTrx,
                                LastModifiedDate = a.CreateDate,
                                LastModifiedBy = a.CreateBy
                            }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetFXTransThreshold(ref IList<FXTransThresholdModel> fXTransThreshold, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    fXTransThreshold = (from a in context.FXTransactionThresholds
                                where a.IsDeleted.Equals(false)
                                orderby new { a.ThresholdAmount, a.DayOfTrx }
                                select new FXTransThresholdModel
                                {
                                     ID = a.ThresholdAmountID,
                                     ThresholdAmount = a.ThresholdAmount,
                                     DayOfTrx = a.DayOfTrx,
                                     LastModifiedBy = a.CreateBy,
                                     LastModifiedDate = a.CreateDate
                                }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetFXTransThreshold(int page, int size, IList<FXTransThresholdFilter> filters, string sortColumn, string sortOrder, ref FXTransThresholdGrid customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                FXTransThresholdModel filter = new FXTransThresholdModel();
                if (filters != null)
                {
                   // string test = (string)filters.Where(a => a.Field.Equals("ThresholdAmount")).Select().SingleOrDefault();
                   // string ThresholdAmount_ = (string)filters.Where(a => a.Field.Equals("ThresholdAmount")).Select(a => a == null ? "0" : a.Value).SingleOrDefault();
                  //  string DayOfTrx_ = (string)filters.Where(a => a.Field.Equals("DayOfTrx")).Select(a => a == null ? "0" : a.Value).SingleOrDefault();
                    //filter.ThresholdAmount = decimal.Parse(filters.Where(a => a.Field.Equals("ThresholdAmount")).Select(a => string.IsNullOrEmpty(a.Value) ? "0" : a.Value).SingleOrDefault());
                    //filter.DayOfTrx = int.Parse(filters.Where(a => a.Field.Equals("DayOfTrx")).Select(a => string.IsNullOrEmpty(a.Value) ? "0" : a.Value).SingleOrDefault());
                    //filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => string.IsNullOrEmpty(a.Value) ? "" : a.Value).SingleOrDefault();
                    
                    filter.ThresholdAmount = filters.Where(a => a.Field.Equals("ThresholdAmount")).Count() > 0 ? decimal.Parse(filters.Where(a => a.Field.Equals("ThresholdAmount")).Select(a => a.Value).SingleOrDefault()) : 0;
                    filter.DayOfTrx = filters.Where(a => a.Field.Equals("DayOfTrx")).Count() > 0 ? int.Parse(filters.Where(a => a.Field.Equals("DayOfTrx")).Select(a => a.Value).SingleOrDefault()) : 0;
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Count() > 0 ? filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault() : string.Empty;

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.FXTransactionThresholds
                                let isThresholdAmount = filter.ThresholdAmount == 0 ? false : true
                                let isDayOfTrx = filter.DayOfTrx== 0 ? false : true
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isThresholdAmount ? a.ThresholdAmount == filter.ThresholdAmount : true) &&
                                    (isDayOfTrx ? a.DayOfTrx== filter.DayOfTrx : true) &&
                                    (isCreateBy ? a.CreateBy.Contains(filter.LastModifiedBy) : true) &&
                                    (isCreateDate ? (a.CreateDate > filter.LastModifiedDate.Value && a.CreateDate < maxDate) : true)
                                select new FXTransThresholdModel
                                {
                                     ID = a.ThresholdAmountID,
                                     ThresholdAmount = a.ThresholdAmount,
                                     DayOfTrx = a.DayOfTrx,
                                    LastModifiedBy = a.CreateBy,
                                    LastModifiedDate = a.CreateDate
                                });

                    customer.Page = page;
                    customer.Size = size;
                    customer.Total = data.Count();
                    customer.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    customer.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new FXTransThresholdRow {
                            RowID = i + 1,
                            ID = a.ID,
                            ThresholdAmount = a.ThresholdAmount,
                            DayOfTrx = a.DayOfTrx,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate 
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetFXTransThreshold(FXTransThresholdFilter filter, ref IList<FXTransThresholdModel> customer, ref string message)
        {
            throw new NotImplementedException();
        }

     /*   public bool GetFXTransThreshold(string key, int limit, ref IList<FXTransThreshold> fXTransThresholds, ref string message)
        {
            bool isSuccess = false;

            try
            {
                fXTransThresholds = (from a in context.FXTransactionThresholds
                            where a.IsDeleted.Equals(false) &&
                                a.ThresholdAmount==key || a.DayOfTrx == key 
                            orderby new { a.ThresholdAmount, a.DayOfTrx }
                            select new FXTransThreshold
                            {
                                ThresholdAmount = a.ThresholdAmount,
                                DayOfTrx = a.DayOfTrx
                            }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        } */

        public bool AddFXTransThreshold(FXTransThresholdModel fXTransThreshold, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.FXTransactionThresholds.Where(a => a.ThresholdAmount.Equals(fXTransThreshold.ThresholdAmount) && a.IsDeleted.Equals(false)).Any())
                {
                    context.FXTransactionThresholds.Add(new Entity.FXTransactionThreshold()
                    {
                         ThresholdAmount = fXTransThreshold.ThresholdAmount,
                         DayOfTrx = fXTransThreshold.DayOfTrx,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = "Admin"
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Transaction Threshold data for Transaction Amount  {0} is already exist.", fXTransThreshold.ThresholdAmount);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateFXTransThreshold(int id, FXTransThresholdModel fXTransThreshold, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.FXTransactionThreshold data = context.FXTransactionThresholds.Where(a => a.ThresholdAmountID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.FXTransactionThresholds.Where(a => a.ThresholdAmountID != fXTransThreshold.ID && a.ThresholdAmount.Equals(fXTransThreshold.ThresholdAmount) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format(" Transaction Threshold Amount {0} is exist.", fXTransThreshold.ThresholdAmount);
                    }
                    else
                    {
                        data.ThresholdAmount = fXTransThreshold.ThresholdAmount;
                        data.DayOfTrx = fXTransThreshold.DayOfTrx;
                        data.UpdateDate = DateTime.UtcNow;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Transaction Threshold data for Transaction Threshold Amount ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteFXTransThreshold(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.FXTransactionThreshold data = context.FXTransactionThresholds.Where(a => a.ThresholdAmountID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format(" Transaction Threshold data for Transaction Threshold ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                        context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}