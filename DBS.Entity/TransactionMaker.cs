//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class TransactionMaker
    {
        public long TransactionMakerID { get; set; }
        public long TransactionID { get; set; }
        public Nullable<System.Guid> WorkflowInstanceID { get; set; }
        public Nullable<long> ApproverID { get; set; }
        public string ApplicationID { get; set; }
        public System.DateTime ApplicationDate { get; set; }
        public string CIF { get; set; }
        public int ProductID { get; set; }
        public int CurrencyID { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountUSD { get; set; }
        public decimal Rate { get; set; }
        public int ChannelID { get; set; }
        public string AccountNumber { get; set; }
        public string OtherAccountNumber { get; set; }
        public int DebitCurrencyID { get; set; }
        public Nullable<int> ProductTypeID { get; set; }
        public int BizSegmentID { get; set; }
        public string BeneName { get; set; }
        public string BeneAccNumber { get; set; }
        public int BankID { get; set; }
        public Nullable<int> BankChargesID { get; set; }
        public Nullable<int> AgentChargesID { get; set; }
        public Nullable<decimal> Type { get; set; }
        public bool IsResident { get; set; }
        public bool IsCitizen { get; set; }
        public string PaymentDetails { get; set; }
        public int IsTopUrgent { get; set; }
        public bool IsNewCustomer { get; set; }
        public bool IsSignatureVerified { get; set; }
        public bool IsDormantAccount { get; set; }
        public bool IsFrezeAccount { get; set; }
        public bool IsDocumentComplete { get; set; }
        public bool IsDraft { get; set; }
        public Nullable<bool> IsPendingDocumentCompleted { get; set; }
        public Nullable<bool> IsOtherAccountNumber { get; set; }
        public string Others { get; set; }
        public string DetailCompliance { get; set; }
        public string TZNumber { get; set; }
        public int StateID { get; set; }
        public Nullable<int> LLDID { get; set; }
        public string InitiatorGroup { get; set; }
        public string OtherBeneBankName { get; set; }
        public string OtherBeneBankAccNumber { get; set; }
        public string OtherBeneBankSwift { get; set; }
        public Nullable<int> UnderlyingDocID { get; set; }
        public string OtherUnderlyingDoc { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public Nullable<bool> IsExceptionHandling { get; set; }
        public Nullable<int> TransactionTypeID { get; set; }
        public Nullable<int> TransactionSubTypeID { get; set; }
        public string BranchName { get; set; }
        public string LocationName { get; set; }
        public Nullable<bool> IsFNACore { get; set; }
        public Nullable<int> FunctionType { get; set; }
        public Nullable<int> AccountType { get; set; }
        public string SolID { get; set; }
        public Nullable<System.DateTime> CRiskEfectiveDate { get; set; }
        public Nullable<int> RiskScore { get; set; }
        public Nullable<System.DateTime> RiskProfileExpiryDate { get; set; }
        public string OperativeAccount { get; set; }
        public string InvestmentID { get; set; }
        public string OtherInvestmentID { get; set; }
        public Nullable<System.DateTime> ValueDate { get; set; }
        public string DealNumber { get; set; }
        public string Remarks { get; set; }
        public string ReceiverName { get; set; }
        public string CreditAccountNumber { get; set; }
        public Nullable<decimal> InterestRate { get; set; }
        public Nullable<System.DateTime> MaturityDate { get; set; }
        public string FDBankName { get; set; }
        public string FDAccNumber { get; set; }
        public string Tenor { get; set; }
        public string DebitAccNumber { get; set; }
        public string CreditAccNumber { get; set; }
        public string LoanContractNo { get; set; }
        public string LoanTransID { get; set; }
        public Nullable<bool> IsLOI { get; set; }
        public Nullable<bool> IsPOI { get; set; }
        public Nullable<bool> IsPOA { get; set; }
        public Nullable<bool> IsChangeRM { get; set; }
        public Nullable<bool> IsSegment { get; set; }
        public Nullable<bool> IsSolID { get; set; }
        public string Transaction_Status { get; set; }
        public string LastUpdatePosition { get; set; }
        public Nullable<long> LastStatusID { get; set; }
        public Nullable<int> CBOMaintainID { get; set; }
        public string AttachmentRemarks { get; set; }
        public Nullable<bool> IsJointAccount { get; set; }
        public Nullable<bool> IsBeneficiaryResident { get; set; }
        public Nullable<int> BeneficiaryBusinessTypeID { get; set; }
        public Nullable<int> SundryID { get; set; }
        public string ChargingAccountName { get; set; }
        public string ChargingAccountBank { get; set; }
        public string BeneficiaryAddress { get; set; }
        public Nullable<int> BranchID { get; set; }
        public Nullable<int> CityID { get; set; }
        public Nullable<int> ComplianceCodeID { get; set; }
        public string ChargingAccount { get; set; }
        public string Mode { get; set; }
        public Nullable<System.DateTime> ExecutionDate { get; set; }
        public Nullable<int> BeneficiaryCountryID { get; set; }
        public Nullable<int> TransactionRelationshipID { get; set; }
        public Nullable<bool> CBGCustomer { get; set; }
        public Nullable<bool> TransactionUsingDebitSundry { get; set; }
        public Nullable<decimal> TransactionRate { get; set; }
        public Nullable<int> LLDDocumentID { get; set; }
        public Nullable<decimal> LLDUnderlyingAmount { get; set; }
        public Nullable<int> SourceID { get; set; }
    
        public virtual Customer Customer { get; set; }
        public virtual CustomerAccount CustomerAccount { get; set; }
        public virtual Bank Bank { get; set; }
        public virtual BeneficiaryBusinessType BeneficiaryBusinessType { get; set; }
        public virtual BizSegment BizSegment { get; set; }
        public virtual Channel Channel { get; set; }
        public virtual Charge Charge { get; set; }
        public virtual Charge Charge1 { get; set; }
        public virtual Currency Currency { get; set; }
        public virtual Currency Currency1 { get; set; }
        public virtual LLD LLD { get; set; }
        public virtual MaintenanceType MaintenanceType { get; set; }
        public virtual Product Product { get; set; }
        public virtual ProductType ProductType { get; set; }
        public virtual ProgressState ProgressState { get; set; }
        public virtual TransactionSUBType TransactionSUBType { get; set; }
        public virtual TransactionType TransactionType { get; set; }
        public virtual UnderlyingDocument UnderlyingDocument { get; set; }
        public virtual Transaction Transaction { get; set; }
    }
}
