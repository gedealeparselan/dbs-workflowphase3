﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Transactions;
using DBS.Entity;
using DBS.WebAPI.Helpers;
using System.IO;

namespace DBS.WebAPI.Models
{
    #region Property
    public class TransactionDetailLoanModel
    {
        /// <summary>
        /// Workflow Instance ID
        /// </summary>
        public Guid? WorkflowInstanceID { get; set; }

        /// <summary>
        /// Transaction ID.
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Application ID
        /// </summary>
        public string ApplicationID { get; set; }

        /// <summary>
        /// Top Urgent
        /// </summary>
        public int? IsTopUrgent { get; set; }
        /// <summary>
        /// Top Urgent
        /// </summary>
        public int? IsTopUrgentChain { get; set; }
        /// <summary>
        /// Customer Data Details
        /// </summary>
        public CustomerModel Customer { get; set; }

        /// <summary>
        /// New Customer
        /// </summary>
        public bool IsNewCustomer { get; set; }

        /// <summary>
        /// Product Details
        /// </summary>
        public ProductModel Product { get; set; }

        /// <summary>
        /// Product Type Details
        /// </summary>
        public ProductTypeModel ProductType { get; set; }

        /// <summary>
        /// LLD Details
        /// </summary>
        public LLDModel LLD { get; set; }

        /// <summary>
        /// LLD Details
        /// </summary>
        public UnderlyingDocModel UnderlyingDoc { get; set; }
        /// <summary>
        /// Other Underlying
        /// </summary>
        public string OtherUnderlyingDoc { get; set; }

        /// <summary>
        /// Currency Details
        /// </summary>
        public CurrencyModel Currency { get; set; }

        /// <summary>
        /// Transaction Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Rate
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Eqv.USD
        /// </summary>
        public decimal AmountUSD { get; set; }

        /// <summary>
        /// Channel Details
        /// </summary>
        public ChannelModel Channel { get; set; }

        /// <summary>
        /// Debit Acc Number
        /// </summary>
        public CustomerAccountModel Account { get; set; }

        /// <summary>
        /// Application Date
        /// </summary>
        public DateTime ApplicationDate { get; set; }

        /// <summary>
        /// Bene Segment Details
        /// </summary>
        public BizSegmentModel BizSegment { get; set; }

        /// <summary>
        /// Bank Details
        /// </summary>
        public BankModel Bank { get; set; }

        /// <summary>
        /// Bank Charges
        /// </summary>
        public ChargesTypeModel BankCharges { get; set; }

        /// <summary>
        /// Agent Charges
        /// </summary>
        public ChargesTypeModel AgentCharges { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        public decimal? Type { get; set; }

        /// <summary>
        /// Is Citizen
        /// </summary>
        public bool IsCitizen { get; set; }

        /// <summary>
        /// Is Resident
        /// </summary>
        public bool IsResident { get; set; }

        ///// <summary>
        ///// Lld Code
        ///// </summary>
        //public string LLDCode { get; set; }

        ///// <summary>
        ///// Lld Info
        ///// </summary>
        //public string LLDInfo { get; set; }

        /// <summary>
        /// Signature Verification
        /// </summary>
        public bool IsSignatureVerified { get; set; }

        /// <summary>
        /// Dormant Account
        /// </summary>
        public bool IsDormantAccount { get; set; }

        /// <summary>
        /// Freeze Account
        /// </summary>
        public bool IsFreezeAccount { get; set; }

        /// basri 30-09-2015
        /// <summary>
        /// Callback Required
        /// </summary>
        public bool IsCallbackRequired { get; set; }

        public bool? IsSyndication { get; set; }

        /// <summary>
        /// Letter of Indemnity Available
        /// </summary>
        public bool IsLOIAvailable { get; set; }
        ///end basri


        /// <summary>
        /// Others
        /// </summary>
        public string Others { get; set; }

        /// <summary>
        /// Is Draft
        /// </summary>
        public bool IsDraft { get; set; }

        /// <summary>
        /// Create Date
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// List Transaction Document Details
        /// </summary>
        public IList<TransactionDocumentModel> Documents { get; set; }

        /// <summary>
        /// Purpose detail of compliance
        /// </summary>
        public string DetailCompliance { get; set; }

        /// <summary>
        /// Bene Name
        /// </summary>
        public string BeneName { get; set; }

        /// <summary>
        /// Bene Name
        /// </summary>
        public string BeneAccNumber { get; set; }

        /// <summary>
        /// TZ Number
        /// </summary>
        public string TZNumber { get; set; }

        public string PaymentDetails { get; set; }

        /// <summary>
        /// Underlyings
        /// </summary> //add chandra
        public IList<TransUnderlyingModel> Underlyings { get; set; }

        /// <summary>
        /// Transaction Create By
        /// </summary> //add reizvan
        public string CreateBy { get; set; }

        /// <summary>
        /// Transaction Is Other Bene Bank
        /// </summary> //add reizvan
        public bool IsOtherBeneBank { get; set; }

        /// <summary>
        /// Transaction Other Bene Bank Name
        /// </summary> //add reizvan
        public string OtherBeneBankName { get; set; }

        /// <summary>
        /// Transaction Other Bene Bank Swift
        /// </summary> //add reizvan
        public string OtherBeneBankSwift { get; set; }


        public CustomerDraftModel CustomerDraft { get; set; }
        public bool IsDocumentComplete { get; set; }

        //approver ID
        public long? ApproverID { get; set; }
        public bool? IsOtherAccountNumber { get; set; }
        public string OtherAccountNumber { get; set; }

        public CurrencyModel DebitCurrency { get; set; }

        /// <summary>
        /// List Transaction All Document reference to underlying by cif
        /// </summary>
        public IList<ReviseMakerDocumentModel> ReviseMakerDocuments { get; set; }

    }

    public class TransactionTimelineLoanModel
    {
        public long ApproverID { get; set; }
        public string Activity { get; set; }
        public DateTime? Time { get; set; }
        public string Outcome { get; set; }
        public string UserOrGroup { get; set; }
        public bool IsGroup { get; set; }
        public string DisplayName { get; set; }
        public string Comment { get; set; }
    }
    
    public class TransactionCheckerDataLoanModel
    {
        public long ID { get; set; }
        //public string LLDCode { get; set; }
        //public string LLDInfo { get; set; }

        public string Others { get; set; }

        /// <summary>
        /// Signature Verification
        /// </summary>
        public bool IsSignatureVerified { get; set; }
        public bool? IsSyndication { get; set; }

        /// <summary>
        /// Dormant Account
        /// </summary>
        public bool IsDormantAccount { get; set; }

        /// <summary>
        /// Freeze Account
        /// </summary>
        public bool IsFreezeAccount { get; set; }

        public bool IsCallbackRequired { get; set; }
        public bool IsLOIAvailable { get; set; }
        public bool IsStatementLetterCopy { get; set; }
        /// <summary>
        /// LLD Details
        /// </summary>
        public LLDModel LLD { get; set; }
    }
    public class VerifyLoanModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool IsVerified { get; set; }
    }
    public class TransactionCheckerDetailLoanModel
    {
        public TransactionDetailLoanModel Transaction { get; set; }
        public IList<TransactionTimelineLoanModel> Timelines { get; set; }
        public TransactionCheckerDataLoanModel Checker { get; set; }
        public IList<VerifyLoanModel> Verify { get; set; }
    
    }
    public class TransactionMakerDetailLoanModel
    {
        public TransactionDetailLoanModel Transaction { get; set; }
        public IList<TransactionTimelineLoanModel> Timelines { get; set; }
        public IList<VerifyLoanModel> Verify { get; set; }
       
      
       
    }
    public class TransactionLoanModel
    {

        public long TrasnsactionID { get; set; }

        /// <summary>
        /// Workflow Instance ID
        /// </summary>
        public Guid? WorkflowInstanceID { get; set; }

        /// <summary>
        /// Transaction ID.
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Application ID
        /// </summary>
        public string ApplicationID { get; set; }
        public DateTime ApplicationDate { get; set; }

        /// <summary>
        /// Top Urgent
        /// </summary>
        public int? IsTopUrgent { get; set; }
        /// <summary>
        /// Top Urgent
        /// </summary>
        public int? IsTopUrgentChain { get; set; }
        /// <summary>
        /// Customer Data Details
        /// </summary>
        public CustomerModel Customer { get; set; }
        /// <summary>
        /// Others
        /// </summary>
        public string Others { get; set; }

        /// <summary>
        /// Product Details
        /// </summary>
        public ProductModel Product { get; set; }

        /// <summary>
        /// Currency Details
        /// </summary>
        public CurrencyModel Currency { get; set; }

        /// <summary>
        /// Transaction Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Rate
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Eqv.USD
        /// </summary>
        public decimal AmountUSD { get; set; }

        /// <summary>
        /// Channel Details
        /// </summary>
        public ChannelModel Channel { get; set; }


        /// <summary>
        /// Application Date
        /// </summary>
        public DateTime ValueDate { get; set; }

        public string LoanContractNo { get; set; }

        /// <summary>
        /// Bene Segment Details
        /// </summary>
        public BizSegmentModel BizSegment { get; set; }
        public bool? IsSignatureVerified { get; set; }

        /// <summary>
        /// Dormant Account
        /// </summary>
        public bool? IsDormantAccount { get; set; }

        /// <summary>
        /// Freeze Account
        /// </summary>
        public bool? IsFreezeAccount { get; set; }
        /// <summary>
        /// IsDocumentComplete
        /// </summary>
        public bool IsDocumentComplete { get; set; }
        /// <summary>
        /// IsCallbackRequired
        /// </summary>
        public bool IsCallbackRequired { get; set; }
        /// <summary>
        /// IsLOIAvailable
        /// </summary>
        public bool IsLOIAvailable { get; set; }
        /// <summary>
        /// ApproverID
        /// </summary>
        public long? ApproverID { get; set; }
        /// <summary>
        /// CreateDate
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// List Transaction Document Details
        /// </summary>
        public IList<TransactionDocumentModel> Documents { get; set; }
        /// <summary>
        /// FundingMemo
        /// </summary>
        //   public FundingMemoLTModel FundingMemo { get; set; }
        public FundingMemoLTModel FundingMemo { get; set; }

    }



    #endregion
    #region Filter
    public class TransactionCheckerLoanFilter : Filter
    { }
    


    #endregion
    #region interface
    public interface ITransactionLoanPPUCheckerRepository : IDisposable
    {
        bool GetTransactionDetails(Guid instanceID, ref TransactionDetailLoanModel transaction, ref string message);
        bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineLoanModel> timelines, ref string message);
        bool GetTransactionCheckerDetailsLoan(Guid workflowInstanceID, long approverID, ref TransactionCheckerDetailLoanModel output, ref string message);
        bool GetTransactionCheckerDetails(Guid workflowInstanceID, long approverID, ref LoanCheckerDetailLoanModel output, ref string message);
        bool UpdateTransactionCheckerDetails(Guid workflowInstanceID, long approverID, TransactionCheckerDetailLoanModel data, ref string message);
        bool AddTransactionCheckerDetails(Guid workflowInstanceID, long approverID, TransactionCheckerDetailLoanModel data, ref string message);
    

        //bool UpdateTransactionCheckerDetails(Guid workflowInstanceID, long approverID, TransactionCheckerDetailLoanModel data, ref string message);
    }
    #endregion

    #region Repository
    public class TransactionLoanRepository : ITransactionLoanPPUCheckerRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetTransactionDetails(Guid instanceID, ref TransactionDetailLoanModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                TransactionDetailLoanModel datatransaction = (from a in context.Transactions
                                                              where a.WorkflowInstanceID.Value.Equals(instanceID)
                                                              select new TransactionDetailLoanModel
                                                          {
                                                              ID = a.TransactionID,
                                                              WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                                              ApplicationID = a.ApplicationID,
                                                              ApplicationDate = a.ApplicationDate,
                                                              ApproverID = a.ApproverID != null ? a.ApproverID : 0,

                                                              Product = new ProductModel()
                                                              {
                                                                  ID = a.Product.ProductID,
                                                                  Code = a.Product.ProductCode,
                                                                  Name = a.Product.ProductName,
                                                                  WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                                                  Workflow = a.Product.ProductWorkflow.WorkflowName,
                                                                  LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                                                  LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                                              },
                                                              Currency = new CurrencyModel()
                                                              {
                                                                  ID = a.Currency.CurrencyID,
                                                                  Code = a.Currency.CurrencyCode,
                                                                  Description = a.Currency.CurrencyDescription,
                                                                  LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                                  LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                              },
                                                              Amount = a.Amount,
                                                              //AmountUSD = a.AmountUSD,
                                                              //Rate = a.Rate,
                                                              Channel = new ChannelModel()
                                                              {
                                                                  ID = a.Channel.ChannelID,
                                                                  Name = a.Channel.ChannelName,
                                                                  LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                                                  LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                                              },

                                                              BizSegment = new BizSegmentModel()
                                                              {
                                                                  ID = a.BizSegment.BizSegmentID,
                                                                  Name = a.BizSegment.BizSegmentName,
                                                                  Description = a.BizSegment.BizSegmentDescription,
                                                                  LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                                  LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                              },
                                                              
                                                              IsTopUrgent = a.IsTopUrgent,
                                                             
                                                              IsSignatureVerified = a.IsSignatureVerified,
                                                              IsFreezeAccount = a.IsFrezeAccount,
                                                              IsDormantAccount = a.IsDormantAccount,
                                                              
                                                              Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                                                              {
                                                                  ID = x.TransactionDocumentID,
                                                                  Type = new DocumentTypeModel()
                                                                  {
                                                                      ID = x.DocumentType.DocTypeID,
                                                                      Name = x.DocumentType.DocTypeName,
                                                                      Description = x.DocumentType.DocTypeDescription,
                                                                      LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                      LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                  },
                                                                  Purpose = new DocumentPurposeModel()
                                                                  {
                                                                      ID = x.DocumentPurpose.PurposeID,
                                                                      Name = x.DocumentPurpose.PurposeName,
                                                                      Description = x.DocumentPurpose.PurposeDescription,
                                                                      LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                      LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                                  },
                                                                  FileName = x.Filename,
                                                                  DocumentPath = x.DocumentPath,
                                                                  LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                  LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value,
                                                                  IsDormant = x.IsDormant == null ? false : x.IsDormant
                                                              }).ToList(),
                                                              ReviseMakerDocuments = (from p in a.TransactionDocuments
                                                                                       where p.PurposeID != 2 && p.IsDeleted.Equals(false)
                                                                                       select new ReviseMakerDocumentModel()
                                                                                                {
                                                                                                    UnderlyingID = 0,
                                                                                                    ID = p.TransactionDocumentID,
                                                                                                    Type = new DocumentTypeModel()
                                                                                                    {
                                                                                                        ID = p.DocumentType.DocTypeID,
                                                                                                        Name = p.DocumentType.DocTypeName,
                                                                                                        Description = p.DocumentType.DocTypeDescription,
                                                                                                        LastModifiedBy = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateBy : p.DocumentType.UpdateBy,
                                                                                                        LastModifiedDate = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateDate : p.DocumentType.UpdateDate.Value
                                                                                                    },
                                                                                                    Purpose = new DocumentPurposeModel()
                                                                                                    {
                                                                                                        ID = p.DocumentPurpose.PurposeID,
                                                                                                        Name = p.DocumentPurpose.PurposeName,
                                                                                                        Description = p.DocumentPurpose.PurposeDescription,
                                                                                                        LastModifiedBy = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateBy : p.DocumentPurpose.UpdateBy,
                                                                                                        LastModifiedDate = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateDate : p.DocumentPurpose.UpdateDate.Value
                                                                                                    },
                                                                                                    FileName = p.Filename,
                                                                                                    DocumentPath = p.DocumentPath,
                                                                                                    IsDormant = false,
                                                                                                    LastModifiedBy = p.UpdateDate == null ? p.CreateBy : p.UpdateBy,
                                                                                                    LastModifiedDate = p.UpdateDate == null ? p.CreateDate : p.UpdateDate.Value
                                                                                                }).ToList(),
                                                              CreateDate = a.CreateDate,
                                                              LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                              LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                                              IsDocumentComplete = a.IsDocumentComplete
                                                          }).SingleOrDefault();

                 if (datatransaction != null)
                {
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(instanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    // changed, chandra : 2015.03.23
                    if (customerRepo.GetCustomerByTransaction(datatransaction.ID, ref customer, ref message))
                    {
                        datatransaction.Customer = customer;
                    }

                    var data = context.Transactions.Where(x => x.TransactionID.Equals(datatransaction.ID)).Select(x => x).FirstOrDefault();


                    customerRepo.Dispose();
                    customer = null;

                    long IDTrns = datatransaction.ID;
                    var checker = context.TransactionCheckerDatas.Where(x => x.TransactionID.Equals(IDTrns)).Select(x => x).OrderByDescending(b => b.ApproverID).FirstOrDefault();
                    if (checker != null)
                    {
                        datatransaction.IsSignatureVerified = checker.IsSignatureVerified;
                        //basri 30-09-2015
                        datatransaction.IsDocumentComplete = datatransaction.IsDocumentComplete;
                        datatransaction.IsCallbackRequired = checker.IsCallbackRequired;
                        datatransaction.IsLOIAvailable = checker.IsLOIAvailable;
                        //end basri
                        datatransaction.IsDormantAccount = checker.IsDormantAccount;
                        datatransaction.IsFreezeAccount = checker.IsFreezeAccount;
                    }
                     
                    transaction = datatransaction;
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }

        public bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineLoanModel> timelines, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                //Remark Rizki 2015-04-21, ganti dari View ke SP
                /* timelines = (from a in context.V_NintexTaskTimeline.AsNoTracking()
                              where a.WorkflowInstanceID.Value.Equals(workflowInstanceID) && !a.Outcome.Equals("Cancelled")
                              orderby a.ActivityTime
                              select new TransactionTimelineModel
                              {
                                  ApproverID = a.WorkflowApproverID,
                                  Activity = a.ActivityTitle,
                                  Time = a.ActivityTime,
                                  UserOrGroup = a.UserOrGroup,
                                  IsGroup = a.IsSPGroup.Value,
                                  Outcome = a.Outcome,
                                  DisplayName = a.DisplayName,
                                  Comment = a.Comment
                              }).ToList(); */

                timelines = (from a in context.SP_GetNintexTaskTimeline(workflowInstanceID)
                             select new TransactionTimelineLoanModel
                             {
                                 ApproverID = a.WorkflowApproverID,
                                 Activity = a.ActivityTitle,
                                 Time = a.ActivityTime,
                                 UserOrGroup = a.UserOrGroup,
                                 IsGroup = a.IsSPGroup.Value,
                                 Outcome = a.Outcome,
                                 DisplayName = a.DisplayName,
                                 Comment = a.Comment
                             }).ToList();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        //public bool GetTransactionCheckerDetails(Guid workflowInstanceID, long approverID, ref TransactionCheckerDetailLoanModel LoanCheckerDetailLoanModel output, ref string message)
        public bool GetTransactionCheckerDetails(Guid workflowInstanceID, long approverID, ref LoanCheckerDetailLoanModel output, ref string message)
        {
            bool IsSuccess = false;

            var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

            try
            {
                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          //select new TransactionCheckerDetailLoanModel
                          select new LoanCheckerDetailLoanModel
                          {
                              Checker = a.TransactionCheckerDatas
                                .Where(x => x.TransactionID.Equals(transactionID))
                                  .OrderByDescending(x => x.TransactionCheckerDataID)
                                .Select(x => new TransactionCheckerDataLoanModel()
                                {
                                    ID = x.TransactionCheckerDataID,
                                    Others = x.Others,
                                    IsSyndication = x.IsSyndication,
                                    IsSignatureVerified = x.IsSignatureVerified,
                                    IsDormantAccount = x.IsDormantAccount,
                                    IsFreezeAccount = x.IsFreezeAccount,
                                    IsCallbackRequired = x.IsCallbackRequired,
                                    IsLOIAvailable = x.IsLOIAvailable,
                                    IsStatementLetterCopy = x.IsStatementLetterCopy

                                }).FirstOrDefault(),
                              Verify = a.TransactionCheckers
                                .Where(x => x.TransactionID.Equals(transactionID) && x.IsDeleted == false)
                                .Select(x => new VerifyLoanModel()
                                {
                                    ID = x.TransactionColumnID,
                                    Name = x.TransactionColumn.ColumnName,
                                    IsVerified = x.IsVerified
                                }).ToList()
                          }).SingleOrDefault();

                // replace output while Checker & Verify is empty
                if (output != null)
                {
                    // replace Checker while empty
                    if (output.Checker == null)
                    {
                        var trans = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).SingleOrDefault();

                        output.Checker = new TransactionCheckerDataLoanModel()
                        {
                            ID = 0,
                            Others = trans.Others,

                        };
                    }

                    // replace verify list while empty
                    if (!output.Verify.Any())
                    {
                        output.Verify = context.TransactionColumns
                            .Where(x => x.IsPPUCheckerLoan.Equals(true) && x.IsDeleted.Equals(false))
                            .Select(x => new VerifyLoanModel()
                            {
                                ID = x.TransactionColumnID,
                                Name = x.ColumnName,
                                IsVerified = false
                            })
                            .ToList();
                    }
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        public bool GetTransactionCheckerDetailsLoan(Guid workflowInstanceID, long approverID, ref TransactionCheckerDetailLoanModel output, ref string message)
        {
            bool IsSuccess = false;

            var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

            try
            {
                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          //select new TransactionCheckerDetailLoanModel
                          select new TransactionCheckerDetailLoanModel
                          {
                              Checker = a.TransactionCheckerDatas
                                .Where(x => x.TransactionID.Equals(transactionID))
                                  .OrderByDescending(x => x.TransactionCheckerDataID)
                                .Select(x => new TransactionCheckerDataLoanModel()
                                {
                                    ID = x.TransactionCheckerDataID,
                                    Others = x.Others,
                                    IsSyndication = x.IsSyndication,
                                    IsSignatureVerified = x.IsSignatureVerified,
                                    IsDormantAccount = x.IsDormantAccount,
                                    IsFreezeAccount = x.IsFreezeAccount,
                                    IsCallbackRequired = x.IsCallbackRequired,
                                    IsLOIAvailable = x.IsLOIAvailable,
                                    IsStatementLetterCopy = x.IsStatementLetterCopy

                                }).FirstOrDefault(),
                              Verify = a.TransactionCheckers
                                .Where(x => x.TransactionID.Equals(transactionID) && x.IsDeleted == false)
                                .Select(x => new VerifyLoanModel()
                                {
                                    ID = x.TransactionColumnID,
                                    Name = x.TransactionColumn.ColumnName,
                                    IsVerified = x.IsVerified
                                }).ToList()
                          }).SingleOrDefault();

                // replace output while Checker & Verify is empty
                if (output != null)
                {
                    // replace Checker while empty
                    if (output.Checker == null)
                    {
                        var trans = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).SingleOrDefault();

                        output.Checker = new TransactionCheckerDataLoanModel()
                        {
                            ID = 0,
                            Others = trans.Others,

                        };
                    }

                    // replace verify list while empty
                    if (!output.Verify.Any())
                    {
                        output.Verify = context.TransactionColumns
                            .Where(x => x.IsPPUCheckerLoan.Value.Equals(true) && x.IsDeleted.Equals(false))
                            .Select(x => new VerifyLoanModel()
                            {
                                ID = x.TransactionColumnID,
                                Name = x.ColumnName,
                                IsVerified = false
                            })
                            .ToList();
                    }
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }


        public bool AddTransactionCheckerDetails(Guid workflowInstanceID, long approverID, TransactionCheckerDetailLoanModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

                    if (transactionID > 0)
                    {
                        Entity.Transaction data_t = context.Transactions.Where(a => a.TransactionID.Equals(transactionID)).SingleOrDefault();
                        if (data_t != null)
                        {
                            //data_t.IsDocumentComplete = data.Verify.ToList().Where(a => a.Name.Equals("Document Completeness")).FirstOrDefault().IsVerified;
                            data_t.IsDocumentComplete = data.Transaction.IsDocumentComplete;
                            data_t.ApproverID = approverID;
                            context.SaveChanges();
                        }

                        if (!data.Transaction.IsDocumentComplete)
                        {

                        }

                        var checker = context.TransactionCheckers
                           .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                           .ToList();

                        var checkerData = context.TransactionCheckerDatas
                            .Where(a => a.TransactionID.Equals(transactionID))
                            .SingleOrDefault();

                        #region Transaction Checker Data
                        if (checkerData != null)
                        {
                            // update table: TransactionCheckerData
                            //checkerData.LLDCode = data.Checker.LLDCode;
                            //checkerData.LLDInfo = data.Checker.LLDInfo;
                            // newlld

                            if (data.Checker.LLD != null)
                            {
                                checkerData.LLDID = data.Checker.LLD.ID;
                            }

                            //checkerData.LLDID = (data.Checker.LLD != null) ? data.Checker.LLD.ID : 0;

                            checkerData.Others = data.Checker.Others;
                            checkerData.IsSignatureVerified = data.Checker.IsSignatureVerified;
                            checkerData.IsSyndication = data.Checker.IsSyndication;
                            checkerData.IsDormantAccount = data.Checker.IsDormantAccount;
                            checkerData.IsFreezeAccount = data.Checker.IsFreezeAccount;
                            checkerData.IsCallbackRequired = data.Checker.IsCallbackRequired;
                            checkerData.IsLOIAvailable = data.Checker.IsLOIAvailable;
                            checkerData.IsStatementLetterCopy = data.Checker.IsStatementLetterCopy;
                            checkerData.UpdateDate = DateTime.UtcNow;
                            checkerData.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                            context.SaveChanges();
                        }
                        else
                        {
                            int? lld = null;
                            // Insert
                            context.TransactionCheckerDatas.Add(new TransactionCheckerData()
                            {
                                ApproverID = approverID,
                                TransactionID = transactionID,
                                //LLDCode = data.Checker.LLDCode,
                                //LLDInfo = data.Checker.LLDInfo,
                                //LLDID = (data.Checker.LLD != null) ? data.Checker.LLD.ID : null,

                                LLDID = data.Checker.LLD != null ? data.Checker.LLD.ID : lld,
                                Others = data.Checker.Others,
                                IsSignatureVerified = data.Checker.IsSignatureVerified,
                                IsDormantAccount = data.Checker.IsDormantAccount,
                                IsFreezeAccount = data.Checker.IsFreezeAccount,
                                IsCallbackRequired = data.Checker.IsCallbackRequired,
                                IsLOIAvailable = data.Checker.IsLOIAvailable,
                                IsStatementLetterCopy = data.Checker.IsStatementLetterCopy,
                                CreatedDate = DateTime.UtcNow,
                                CreatedBy = currentUser.GetCurrentUser().DisplayName
                            });

                            context.SaveChanges();

                            if (data.Checker.LLD != null)
                            {
                                var updateLLD = context.TransactionCheckerDatas
                                    .Where(a => a.TransactionID.Equals(transactionID))
                                    .SingleOrDefault();
                                updateLLD.LLDID = data.Checker.LLD.ID;
                                context.SaveChanges();
                            }
                        }
                        #endregion

                        #region Transaction Checker
                        if (checker.Count > 0)
                        {
                            var checkerdata = context.TransactionCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                            foreach (var item in checkerdata)
                            {
                                context.TransactionCheckers.Remove(item);
                            }
                            context.SaveChanges();

                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });

                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });

                                context.SaveChanges();
                            }
                        }
                        #endregion

                        checker = null;
                        checkerData = null;
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }

        #region LoanCheckerSettlementUnschedule
        public bool AddCheckerSettlementUnschedule(Guid workflowInstanceID, long approverID, TransactionCheckerDetailLoanModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

                    if (transactionID > 0)
                    {
                        Entity.Transaction data_t = context.Transactions.Where(a => a.TransactionID.Equals(transactionID)).SingleOrDefault();
                        if (data_t != null)
                        {
                            //data_t.IsDocumentComplete = data.Verify.ToList().Where(a => a.Name.Equals("Document Completeness")).FirstOrDefault().IsVerified;
                            data_t.IsDocumentComplete = data.Transaction.IsDocumentComplete;
                            context.SaveChanges();
                        }

                        if (!data.Transaction.IsDocumentComplete)
                        {

                        }

                        var checker = context.TransactionCheckers
                           .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                           .ToList();

                        var checkerData = context.TransactionCheckerDatas
                            .Where(a => a.TransactionID.Equals(transactionID))
                            .SingleOrDefault();

                        #region Transaction Checker Data
                        if (checkerData != null)
                        {
                            // update table: TransactionCheckerData
                            //checkerData.LLDCode = data.Checker.LLDCode;
                            //checkerData.LLDInfo = data.Checker.LLDInfo;
                            // newlld

                            if (data.Checker.LLD != null)
                            {
                                checkerData.LLDID = data.Checker.LLD.ID;
                            }

                            //checkerData.LLDID = (data.Checker.LLD != null) ? data.Checker.LLD.ID : 0;

                            checkerData.Others = data.Checker.Others;
                            checkerData.IsSignatureVerified = data.Checker.IsSignatureVerified;
                            checkerData.IsSyndication = data.Checker.IsSyndication;
                            checkerData.IsDormantAccount = data.Checker.IsDormantAccount;
                            checkerData.IsFreezeAccount = data.Checker.IsFreezeAccount;
                            checkerData.IsCallbackRequired = data.Checker.IsCallbackRequired;
                            checkerData.IsLOIAvailable = data.Checker.IsLOIAvailable;
                            checkerData.IsStatementLetterCopy = data.Checker.IsStatementLetterCopy;
                            checkerData.UpdateDate = DateTime.UtcNow;
                            checkerData.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                            

                            context.SaveChanges();
                        }
                        else
                        {
                            int? lld = null;
                            // Insert
                            context.TransactionCheckerDatas.Add(new TransactionCheckerData()
                            {
                                ApproverID = approverID,
                                TransactionID = transactionID,
                                //LLDCode = data.Checker.LLDCode,
                                //LLDInfo = data.Checker.LLDInfo,
                                //LLDID = (data.Checker.LLD != null) ? data.Checker.LLD.ID : null,

                                LLDID = data.Checker.LLD != null ? data.Checker.LLD.ID : lld,
                                Others = data.Checker.Others,
                                IsSignatureVerified = data.Checker.IsSignatureVerified,
                                IsDormantAccount = data.Checker.IsDormantAccount,
                                IsFreezeAccount = data.Checker.IsFreezeAccount,
                                IsCallbackRequired = data.Checker.IsCallbackRequired,
                                IsLOIAvailable = data.Checker.IsLOIAvailable,
                                IsStatementLetterCopy = data.Checker.IsStatementLetterCopy,
                                CreatedDate = DateTime.UtcNow,
                                CreatedBy = currentUser.GetCurrentUser().DisplayName
                            });

                            context.SaveChanges();

                            if (data.Checker.LLD != null)
                            {
                                var updateLLD = context.TransactionCheckerDatas
                                    .Where(a => a.TransactionID.Equals(transactionID))
                                    .SingleOrDefault();
                                updateLLD.LLDID = data.Checker.LLD.ID;
                                context.SaveChanges();
                            }
                        }
                        #endregion

                        #region Transaction Checker
                        if (checker.Count > 0)
                        {
                            var checkerdata = context.TransactionCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                            foreach (var item in checkerdata)
                            {
                                context.TransactionCheckers.Remove(item);
                            }
                            context.SaveChanges();

                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });

                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });

                                context.SaveChanges();
                            }
                        }
                        #endregion

                        checker = null;
                        checkerData = null;
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }
        #endregion

        public bool UpdateTransactionCheckerDetails(Guid workflowInstanceID, long approverID, TransactionCheckerDetailLoanModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

                    if (transactionID > 0)
                    {
                        //update document completed


                        var checker = context.TransactionCheckers
                           .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                           .ToList();

                        if (checker.Count > 0)
                        {
                            var checkerdata = context.TransactionCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                            foreach (var item in checkerdata)
                            {
                                context.TransactionCheckers.Remove(item);
                            }
                            context.SaveChanges();

                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });

                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });

                                context.SaveChanges();
                            }
                        }

                        checker = null;
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }
       
        
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.Collect();
            GC.SuppressFinalize(this);
        }
 



    }
       #endregion
}