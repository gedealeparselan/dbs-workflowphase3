﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
      [Authorize]
    public class ForceCompleteController : ApiController
    {
           private string message = string.Empty;

        private readonly IForceCompleteRepo repository = null;

        public ForceCompleteController()
        {
            this.repository = new ForceCompleteRepository();
        }

        public ForceCompleteController(IForceCompleteRepo repo)
        {
            this.repository = repo;
        }


        /// Get all RejectCode

        [ResponseType(typeof(IList<ForceCompleteModel>))]
        public HttpResponseMessage Get()
        {
            IList<ForceCompleteModel> output = new List<ForceCompleteModel>();

            if (!repository.GetForceComplete(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ForceCompleteModel>>(HttpStatusCode.OK, output);
            }
        }


        /// Get Reject Code with maximum rows will be retreive and select page index to display.

        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<ForceCompleteModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<ForceCompleteModel> output = new List<ForceCompleteModel>();

            if (!repository.GetForceComplete(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<ForceCompleteModel>>(HttpStatusCode.OK, output);
            }
        }


        /// <summary>
        /// Get Reject Code for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(ForceCompleteModel))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<ForceCompleteFilter> filters)
        {
            ForceCompleteGrid output = new ForceCompleteGrid();

            if (!repository.GetForceComplete(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<ForceCompleteGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find Reject Code using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<FXComplianceModel>))]
        [HttpGet]
        [Route("api/ForceComplete/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<ForceCompleteModel> output = new List<ForceCompleteModel>();

            if (!repository.GetForceComplete(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ForceCompleteModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get FX Compliance by ID.
        /// </summary>
        /// <param name="id">FX Compliance ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(ForceCompleteModel))]
        [Route("api/RejectCode/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            ForceCompleteModel output = new ForceCompleteModel();

            if (!repository.GetForcedCompleteByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<ForceCompleteModel>(HttpStatusCode.OK, output);
            }
        }


        /// <summary>
        /// Create a new reject Code.
        /// </summary>
        /// <param name="FXCompliance">FX Compliance data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(ForceCompleteModel ForceComplete)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddForceComplete(ForceComplete, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New ForceComplete data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing Reject Code.
        /// </summary>
        /// <param name="id">ID of RejectCode to be modify.</param>
        /// <param name="FXCompliance">RejectCode data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/ForceComplete/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, ForceCompleteModel ForceComplete)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateForceComplete(id, ForceComplete, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "ForceComplete data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting Reject Code.
        /// </summary>
        /// <param name="id">ID of Reject Code to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/ForceComplete/{id}")]
        [Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteForceComplete(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "ForceComplete data has been deleted." });
                }
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }

    }
}
