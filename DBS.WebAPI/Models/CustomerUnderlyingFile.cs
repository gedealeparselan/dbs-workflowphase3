﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Transactions;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    public class CustomerUnderlyingFileModel
    {
        /// <summary>
        /// CustomerUnderlyingFile ID.
        /// </summary>
        public long ID { get; set; }

        public string CIF { get; set; }
        public string FileName { get; set; }

        public string DocumentRefNumber { get; set; }

        public string AttachmentReference { get; set; }

        public string DocumentPath { get; set; }
        public string AccountNumber { get; set; }
        public int StatusShowData { get; set; }
        public DocumentPurposeModel DocumentPurpose { get; set; }


        public DocumentTypeModel DocumentType { get; set; }



        public DateTime? LastModifiedDate { get; set; }


        public string LastModifiedBy { get; set; }


        public IList<CustomerUnderlyingMappingModel> CustomerUnderlyingMappings { get; set; }
    }

    public class CustomerUnderlyingMappingModel
    {
        public long ID { get; set; }

        public long UnderlyingID { get; set; }

        public long UnderlyingFileID { get; set; }

        public string AttachmentNo { get; set; }
    }

    public class CustomerUnderlyingFileRow : CustomerUnderlyingFileModel
    {
        public int RowID { get; set; }

    }

    public class CustomerUnderlyingFileGrid : Grid
    {
        public IList<CustomerUnderlyingFileRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class CustomerUnderlyingFileFilter : Filter { }
    #endregion

    #region Interface
    public interface ICustomerUnderlyingFileRepository : IDisposable
    {
        bool GetCustomerUnderlyingFileByID(int id, ref CustomerUnderlyingFileModel CustomerUnderlyingFile, ref string message);
        bool GetCustomerUnderlyingFile(ref IList<CustomerUnderlyingFileModel> CustomerUnderlyingFiles, int limit, int index, ref string message);
        bool GetCustomerUnderlyingFile(int page, int size, IList<CustomerUnderlyingFileFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingFileGrid CustomerUnderlyingFile, ref string message);
        bool GetCustomerUnderlyingFile(ref IList<CustomerUnderlyingFileModel> CustomerUnderlyingFiles, ref string message);
        bool GetCustomerUnderlyingFile(CustomerUnderlyingFileFilter filter, ref IList<CustomerUnderlyingFileModel> CustomerUnderlyingFiles, ref string message);
        bool GetCustomerUnderlyingFile(string key, int limit, ref IList<CustomerUnderlyingFileModel> CustomerUnderlyingFiles, ref string message);
        bool AddCustomerUnderlyingFile(CustomerUnderlyingFileModel CustomerUnderlyingFile, ref string message);
        bool UpdateCustomerUnderlyingFile(int id, CustomerUnderlyingFileModel CustomerUnderlyingFile, ref string message);
        bool DeleteCustomerUnderlyingFile(int id, ref string message);
        bool GetCustomerUnderlyingFile(long id, int page, int size, IList<CustomerUnderlyingFileFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingFileGrid CustomerUnderlyingFile, ref string message);
        bool GetDealCustomerUnderlyingFile(long id, int page, int size, IList<CustomerUnderlyingFileFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingFileGrid CustomerUnderlying, ref string message);
        bool AddCustomerUnderlyingFileReturnID(CustomerUnderlyingFileModel customerUnderlyingFile, ref string message, ref long UnderlyingFileID);
    }
    #endregion

    #region Repository
    public class CustomerUnderlyingFileRepository : ICustomerUnderlyingFileRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetCustomerUnderlyingFileByID(int id, ref CustomerUnderlyingFileModel CustomerUnderlyingFile, ref string message)
        {
            bool isSuccess = false;

            try
            {
                CustomerUnderlyingFile = (from a in context.CustomerUnderlyingFiles
                                          join b in context.CustomerUnderlyingMappings on a.UnderlyingFileID equals b.UnderlyingFileID
                                          join c in context.CustomerUnderlyings on b.UnderlyingID equals c.UnderlyingID
                                          where a.IsDeleted.Equals(false) && a.UnderlyingFileID.Equals(id) && b.IsDeleted.Equals(false) && c.IsDeleted.Equals(false)
                                          select new CustomerUnderlyingFileModel
                                          {
                                              ID = a.UnderlyingFileID,
                                              FileName = a.FileName,
                                              DocumentPath = a.DocumentPath,
                                              DocumentRefNumber = a.DocumentRefNumber,
                                              AttachmentReference = a.AttachmentReference,
                                              DocumentPurpose = new DocumentPurposeModel { ID = a.DocumentPurpose.PurposeID, Name = a.DocumentPurpose.PurposeName, Description = a.DocumentPurpose.PurposeDescription },
                                              DocumentType = new DocumentTypeModel { ID = a.DocumentType.DocTypeID, Name = a.DocumentType.DocTypeName, Description = a.DocumentType.DocTypeDescription },
                                              CustomerUnderlyingMappings = context.CustomerUnderlyingMappings.Where(e => e.UnderlyingFileID == e.UnderlyingFileID && e.IsDeleted.Equals(false)).Select(e => new CustomerUnderlyingMappingModel
                                              {
                                                  ID = e.UnderlyingMappingID,
                                                  UnderlyingID = e.UnderlyingID,
                                                  UnderlyingFileID = e.UnderlyingFileID
                                              }).ToList(),
                                              LastModifiedDate = a.CreateDate,
                                              LastModifiedBy = a.CreateBy
                                          }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerUnderlyingFile(ref IList<CustomerUnderlyingFileModel> CustomerUnderlyingFiles, ref string message)
        {
            bool isSuccess = false;

            try
            {

                using (DBSEntities context = new DBSEntities())
                {
                    CustomerUnderlyingFiles = (from a in context.CustomerUnderlyingFiles
                                               join b in context.CustomerUnderlyingMappings on a.UnderlyingFileID equals b.UnderlyingFileID
                                               join c in context.CustomerUnderlyings on b.UnderlyingID equals c.UnderlyingID
                                               where a.IsDeleted.Equals(false) && b.IsDeleted.Equals(false) && c.IsDeleted.Equals(false)

                                               orderby new { a.UnderlyingFileID }
                                               select new CustomerUnderlyingFileModel
                                               {
                                                   ID = a.UnderlyingFileID,
                                                   FileName = a.FileName,
                                                   DocumentPath = a.DocumentPath,
                                                   DocumentRefNumber = a.DocumentRefNumber,
                                                   AttachmentReference = a.AttachmentReference,
                                                   DocumentPurpose = new DocumentPurposeModel { ID = a.DocumentPurpose.PurposeID, Name = a.DocumentPurpose.PurposeName, Description = a.DocumentPurpose.PurposeDescription },
                                                   DocumentType = new DocumentTypeModel { ID = a.DocumentType.DocTypeID, Name = a.DocumentType.DocTypeName, Description = a.DocumentType.DocTypeDescription },
                                                   CustomerUnderlyingMappings = context.CustomerUnderlyingMappings.Where(e => e.UnderlyingFileID == e.UnderlyingFileID && e.IsDeleted.Equals(false)).Select(e => new CustomerUnderlyingMappingModel
                                                   {
                                                       ID = e.UnderlyingMappingID,
                                                       UnderlyingID = e.UnderlyingID,
                                                       UnderlyingFileID = e.UnderlyingFileID
                                                   }).ToList(),
                                                   LastModifiedDate = a.CreateDate,
                                                   LastModifiedBy = a.CreateBy
                                               }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetCustomerUnderlyingFile(ref IList<CustomerUnderlyingFileModel> CustomerUnderlyingFile, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    CustomerUnderlyingFile = (from a in context.CustomerUnderlyingFiles
                                              join b in context.CustomerUnderlyingMappings on a.UnderlyingFileID equals b.UnderlyingFileID
                                              join c in context.CustomerUnderlyings on b.UnderlyingID equals c.UnderlyingID
                                              where a.IsDeleted.Equals(false) && b.IsDeleted.Equals(false) && c.IsDeleted.Equals(false)
                                              select new CustomerUnderlyingFileModel
                                              {
                                                  ID = a.UnderlyingFileID,
                                                  DocumentPath = a.DocumentPath,
                                                  FileName = a.FileName,
                                                  DocumentRefNumber = a.DocumentRefNumber,
                                                  AttachmentReference = a.AttachmentReference,
                                                  DocumentPurpose = new DocumentPurposeModel { ID = a.DocumentPurpose.PurposeID, Name = a.DocumentPurpose.PurposeName, Description = a.DocumentPurpose.PurposeDescription },
                                                  DocumentType = new DocumentTypeModel { ID = a.DocumentType.DocTypeID, Name = a.DocumentType.DocTypeName, Description = a.DocumentType.DocTypeDescription },
                                                  CustomerUnderlyingMappings = context.CustomerUnderlyingMappings.Where(e => e.UnderlyingFileID == e.UnderlyingFileID && e.IsDeleted.Equals(false)).Select(e => new CustomerUnderlyingMappingModel
                                                  {
                                                      ID = e.UnderlyingMappingID,
                                                      UnderlyingID = e.UnderlyingID,
                                                      UnderlyingFileID = e.UnderlyingFileID
                                                  }).ToList(),
                                                  LastModifiedDate = a.CreateDate,
                                                  LastModifiedBy = a.CreateBy
                                              }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerUnderlyingFile(int page, int size, IList<CustomerUnderlyingFileFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingFileGrid customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                CustomerUnderlyingFileModel filter = new CustomerUnderlyingFileModel();
                filter.DocumentPurpose = new DocumentPurposeModel { Name = string.Empty };
                if (filters != null)
                {
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.DocumentPurpose.Name = (string)filters.Where(a => a.Field.Equals("DocumentPurpose")).Select(a => a.Value).SingleOrDefault();
                    filter.FileName = (string)filters.Where(a => a.Field.Equals("FileName")).Select(a => a.Value).SingleOrDefault();
                    filter.DocumentRefNumber = (string)filters.Where(a => a.Field.Equals("DocumentRefNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.AttachmentReference = (string)filters.Where(a => a.Field.Equals("AttachmentReference")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                    filter.AccountNumber = (string)filters.Where(a => a.Field.Equals("AccountNumber")).Select(a => a.Value).SingleOrDefault();
                    string statusData = (string)filters.Where(a => a.Field.Equals("StatusShowData")).Select(a => a.Value).SingleOrDefault();
                    filter.StatusShowData = statusData != null ? int.Parse(statusData) : 0;
                }
                bool isDocumentRefNumber = !string.IsNullOrEmpty(filter.DocumentRefNumber);
                using (DBSEntities context = new DBSEntities())
                {
                    IEnumerable<CustomerUnderlyingFileModel> data = (from a in context.CustomerUnderlyingFiles
                                                                     join b in context.CustomerUnderlyingMappings on a.UnderlyingFileID equals b.UnderlyingFileID
                                                                     join c in context.CustomerUnderlyings on b.UnderlyingID equals c.UnderlyingID
                                                                     let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                                                     let isDocPurpose = !string.IsNullOrEmpty(filter.DocumentPurpose.Name)
                                                                     let isFileName = !string.IsNullOrEmpty(filter.FileName)
                                                                     let isAttachmentReference = !string.IsNullOrEmpty(filter.AttachmentReference)
                                                                     let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                                                     let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                                                     let isAccountNumber = filter.AccountNumber != null ? true : false
                                                                     let isStatusShow = filter.StatusShowData != null ? true : false
                                                                     where a.IsDeleted.Equals(false) && b.IsDeleted.Equals(false) && c.IsDeleted.Equals(false) &&
                                                                         //(isCIF ? c.CIF.Equals(filter.CIF) : true) &&
                                                                         (isStatusShow ? filter.StatusShowData == 0 ? c.CIF.Equals(filter.CIF) || c.AccountNumber.Equals(filter.AccountNumber) : (filter.StatusShowData == 1 ? c.CIF.Equals(filter.CIF) : filter.StatusShowData == 2 ? c.AccountNumber.Equals(filter.AccountNumber) : true) : true) &&
                                                                         (isDocPurpose ? a.DocumentPurpose.PurposeName.Contains(filter.DocumentPurpose.Name) : true) &&
                                                                         (isFileName ? a.FileName.Contains(filter.FileName) : true) &&
                                                                         (isAttachmentReference ? a.AttachmentReference.Contains(filter.AttachmentReference) : true) &&
                                                                         (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                                                         (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                                                     select new CustomerUnderlyingFileModel
                                                                     {
                                                                         ID = a.UnderlyingFileID,
                                                                         FileName = a.FileName,
                                                                         DocumentPath = a.DocumentPath,
                                                                         DocumentRefNumber = c.ReferenceNumber == null ? "" : c.ReferenceNumber,
                                                                         AttachmentReference = a.AttachmentReference,
                                                                         DocumentPurpose = new DocumentPurposeModel { ID = a.DocumentPurpose.PurposeID, Name = a.DocumentPurpose.PurposeName, Description = a.DocumentPurpose.PurposeDescription },
                                                                         DocumentType = new DocumentTypeModel { ID = a.DocumentType.DocTypeID, Name = a.DocumentType.DocTypeName, Description = a.DocumentType.DocTypeDescription },
                                                                         LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                         LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                                     });
                    var groupData = (from a in data.ToList()
                                     group a by a.ID into groupFile
                                     select new CustomerUnderlyingFileModel
                                     {
                                         ID = groupFile.FirstOrDefault().ID,
                                         FileName = groupFile.FirstOrDefault().FileName,
                                         DocumentPath = groupFile.FirstOrDefault().DocumentPath,
                                         DocumentRefNumber = string.Join(", ", (groupFile.Select(x => x.DocumentRefNumber)).ToArray()),
                                         DocumentPurpose = groupFile.FirstOrDefault().DocumentPurpose,
                                         AttachmentReference = groupFile.FirstOrDefault().AttachmentReference,
                                         DocumentType = groupFile.FirstOrDefault().DocumentType,
                                         LastModifiedBy = groupFile.FirstOrDefault().LastModifiedBy,
                                         LastModifiedDate = groupFile.FirstOrDefault().LastModifiedDate
                                     }).AsQueryable().Where(x => x.DocumentRefNumber.ToLower().Contains(isDocumentRefNumber ? filter.DocumentRefNumber.ToLower() : ""));
                    customer.Page = page;
                    customer.Size = size;
                    customer.Total = groupData.Count();
                    customer.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    customer.Rows = groupData.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new CustomerUnderlyingFileRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            FileName = a.FileName,
                            DocumentPath = a.DocumentPath,
                            DocumentRefNumber = a.DocumentRefNumber,
                            AttachmentReference = a.AttachmentReference,
                            DocumentPurpose = a.DocumentPurpose,
                            DocumentType = a.DocumentType,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            CustomerUnderlyingMappings = context.CustomerUnderlyingMappings.Where(e => e.UnderlyingFileID == e.UnderlyingFileID && e.IsDeleted.Equals(false)).Select(e => new CustomerUnderlyingMappingModel
                            {
                                ID = e.UnderlyingMappingID,
                                UnderlyingID = e.UnderlyingID,
                                UnderlyingFileID = e.UnderlyingFileID
                            }).ToList()
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerUnderlyingFile(CustomerUnderlyingFileFilter filter, ref IList<CustomerUnderlyingFileModel> customer, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetCustomerUnderlyingFile(string key, int limit, ref IList<CustomerUnderlyingFileModel> CustomerUnderlyingFile, ref string message)
        {
            bool isSuccess = false;

            try
            {
                CustomerUnderlyingFile = (from a in context.CustomerUnderlyingFiles
                                          join b in context.CustomerUnderlyingMappings on a.UnderlyingFileID equals b.UnderlyingFileID
                                          join c in context.CustomerUnderlyings on b.UnderlyingID equals c.UnderlyingID
                                          where a.IsDeleted.Equals(false) && b.IsDeleted.Equals(false) && c.IsDeleted.Equals(false) &&
                                          a.DocumentPurpose.PurposeName.Contains(key) || a.DocumentType.DocTypeName.Contains(key)
                                          orderby new { a.DocumentPurpose.PurposeName, a.DocumentType.DocTypeName }
                                          select new CustomerUnderlyingFileModel
                                          {
                                              DocumentPurpose = new DocumentPurposeModel { ID = a.DocumentPurpose.PurposeID, Name = a.DocumentPurpose.PurposeName, Description = a.DocumentPurpose.PurposeDescription },
                                              DocumentType = new DocumentTypeModel { ID = a.DocumentType.DocTypeID, Name = a.DocumentType.DocTypeName, Description = a.DocumentType.DocTypeDescription },
                                          }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddCustomerUnderlyingFile(CustomerUnderlyingFileModel customerUnderlyingFile, ref string message)
        {
            bool isSuccess = false;
            string AttachmentRef = string.Empty;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.CustomerUnderlyingFile result = new Entity.CustomerUnderlyingFile()
                    {
                        FileName = customerUnderlyingFile.FileName,
                        DocumentPath = customerUnderlyingFile.DocumentPath,
                        DocumentRefNumber = customerUnderlyingFile.DocumentRefNumber,
                        AttachmentReference = customerUnderlyingFile.AttachmentReference,
                        PurposeID = customerUnderlyingFile.DocumentPurpose.ID,
                        DocTypeID = customerUnderlyingFile.DocumentType.ID,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    };
                    context.CustomerUnderlyingFiles.Add(result);
                    context.SaveChanges();

                    foreach (var data in customerUnderlyingFile.CustomerUnderlyingMappings)
                    {
                        Entity.CustomerUnderlyingMapping detail = new Entity.CustomerUnderlyingMapping()
                        {
                            UnderlyingFileID = result.UnderlyingFileID,
                            UnderlyingID = data.UnderlyingID,
                            IsDeleted = false,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().DisplayName
                        };
                        context.CustomerUnderlyingMappings.Add(detail);
                        context.SaveChanges();

                        Entity.CustomerUnderlying custUnderlying = context.CustomerUnderlyings.Where(x => x.UnderlyingID.Equals(data.UnderlyingID)).SingleOrDefault();
                        custUnderlying.IsAttachFile = true;
                        custUnderlying.AttachmentNo = data.AttachmentNo + result.UnderlyingFileID.ToString();
                        custUnderlying.UpdateDate = DateTime.UtcNow;
                        custUnderlying.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        context.SaveChanges();
                        AttachmentRef = data.AttachmentNo + result.UnderlyingFileID.ToString();
                    }

                    Entity.CustomerUnderlyingFile custUnderlyingFile = context.CustomerUnderlyingFiles.Where(z => z.UnderlyingFileID.Equals(result.UnderlyingFileID)).SingleOrDefault();
                    custUnderlyingFile.AttachmentReference = AttachmentRef;
                    context.SaveChanges();


                    isSuccess = true;
                    ts.Complete();
                }

                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }
            return isSuccess;
        }

        public bool AddCustomerUnderlyingFileReturnID(CustomerUnderlyingFileModel customerUnderlyingFile, ref string message, ref long UnderlyingFileID)
        {
            bool isSuccess = false;
            string AttachmentRef = string.Empty;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.CustomerUnderlyingFile result = new Entity.CustomerUnderlyingFile()
                    {
                        FileName = customerUnderlyingFile.FileName,
                        DocumentPath = customerUnderlyingFile.DocumentPath,
                        DocumentRefNumber = customerUnderlyingFile.DocumentRefNumber,
                        AttachmentReference = customerUnderlyingFile.AttachmentReference,
                        PurposeID = customerUnderlyingFile.DocumentPurpose.ID,
                        DocTypeID = customerUnderlyingFile.DocumentType.ID,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    };
                    context.CustomerUnderlyingFiles.Add(result);
                    context.SaveChanges();
                    UnderlyingFileID = result.UnderlyingFileID;
                    foreach (var data in customerUnderlyingFile.CustomerUnderlyingMappings)
                    {
                        Entity.CustomerUnderlyingMapping detail = new Entity.CustomerUnderlyingMapping()
                        {
                            UnderlyingFileID = result.UnderlyingFileID,
                            UnderlyingID = data.UnderlyingID,
                            IsDeleted = false,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().DisplayName
                        };
                        context.CustomerUnderlyingMappings.Add(detail);
                        context.SaveChanges();

                        Entity.CustomerUnderlying custUnderlying = context.CustomerUnderlyings.Where(x => x.UnderlyingID.Equals(data.UnderlyingID)).SingleOrDefault();
                        custUnderlying.IsAttachFile = true;
                        custUnderlying.AttachmentNo = data.AttachmentNo + result.UnderlyingFileID.ToString();
                        context.SaveChanges();
                        AttachmentRef = data.AttachmentNo + result.UnderlyingFileID.ToString();
                    }

                    Entity.CustomerUnderlyingFile custUnderlyingFile = context.CustomerUnderlyingFiles.Where(z => z.UnderlyingFileID.Equals(result.UnderlyingFileID)).SingleOrDefault();
                    custUnderlyingFile.AttachmentReference = AttachmentRef;
                    context.SaveChanges();

                    /*  Entity.TransactionDocument transactionDocument = new Entity.TransactionDocument()
                      {
                          TransactionID = customerUnderlyingFile.ID, //customerUnderlyingFile.ID representasi dari transactionID
                          DocumentPath = customerUnderlyingFile.DocumentPath,
                          Filename = customerUnderlyingFile.FileName,
                          PurposeID = customerUnderlyingFile.DocumentPurpose.ID,
                          DocTypeID = customerUnderlyingFile.DocumentType.ID,
                          IsDeleted = false,
                          CreateDate = DateTime.UtcNow,
                          CreateBy = currentUser.GetCurrentUser().DisplayName
                      };
                      context.TransactionDocuments.Add(transactionDocument);
                      context.SaveChanges(); */

                    isSuccess = true;
                    ts.Complete();
                }

                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }
            return isSuccess;
        }

        public bool UpdateCustomerUnderlyingFile(int id, CustomerUnderlyingFileModel customerUnderlyingFile, ref string message)
        {
            bool isSuccess = false;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.CustomerUnderlyingFile data = context.CustomerUnderlyingFiles.Where(a => a.UnderlyingFileID.Equals(id)).SingleOrDefault();

                    if (data != null)
                    {
                        data.FileName = customerUnderlyingFile.FileName;
                        data.DocumentPath = customerUnderlyingFile.DocumentPath;
                        data.DocumentRefNumber = customerUnderlyingFile.DocumentRefNumber;
                        data.PurposeID = customerUnderlyingFile.DocumentPurpose.ID;
                        data.DocTypeID = customerUnderlyingFile.DocumentType.ID;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        DeletedUnderlyingMapping(id);
                        foreach (var detail in customerUnderlyingFile.CustomerUnderlyingMappings)
                        {
                            Entity.CustomerUnderlyingMapping dataDetail = new Entity.CustomerUnderlyingMapping()
                            {
                                UnderlyingFileID = id,
                                UnderlyingID = customerUnderlyingFile.ID,
                                IsDeleted = false,
                                CreateDate = data.CreateDate,
                                CreateBy = data.CreateBy,
                                UpdateDate = DateTime.UtcNow,
                                UpdateBy = currentUser.GetCurrentUser().DisplayName
                            };
                            context.CustomerUnderlyingMappings.Add(dataDetail);
                            context.SaveChanges();
                        }
                        isSuccess = true;
                    }
                    else
                    {
                        message = string.Format("Customer Underlying File data for Customer Underlying File ID {0} is does not exist.", id);
                    }
                }

                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return isSuccess;
        }


        private void DeletedUnderlyingMapping(int id)
        {

            List<long> UnderlyingIDs = new List<long>();

            try
            {
                var datadetail = context.CustomerUnderlyingMappings.Where(a => a.UnderlyingFileID.Equals(id) && a.IsDeleted.Equals(false)).ToList();
                if (datadetail != null && datadetail.Count > 0)
                {
                    datadetail.ForEach(a =>
                    {
                        a.IsDeleted = true;
                        a.UpdateDate = DateTime.UtcNow;
                        a.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        UnderlyingIDs.Add(a.UnderlyingID);

                    });
                    context.SaveChanges();

                    foreach (int UnderlyingID in UnderlyingIDs)
                    {
                        RemoveUnderlyingAttach(UnderlyingID);
                    }


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void RemoveUnderlyingAttach(long id)
        {

            try
            {
                var datadetail = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(id)).FirstOrDefault();
                if (datadetail != null)
                {
                    datadetail.IsAttachFile = false;
                    datadetail.AttachmentNo = "";
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteCustomerUnderlyingFile(int id, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {

                    Entity.CustomerUnderlyingFile data = context.CustomerUnderlyingFiles.Where(a => a.UnderlyingFileID.Equals(id)).SingleOrDefault();
                    if (data != null)
                    {
                        data.IsDeleted = true;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        context.SaveChanges();
                        DeletedUnderlyingMapping(id);
                        isSuccess = true;
                        ts.Complete();
                    }
                    else
                    {
                        message = string.Format("Customer Underlying File data for Customer Underlying File ID {0} is does not exist.", id);
                    }

                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public bool GetCustomerUnderlyingFile(long id, int page, int size, IList<CustomerUnderlyingFileFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingFileGrid CustomerUnderlying, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();


                CustomerUnderlyingFileModel filter = new CustomerUnderlyingFileModel();
                filter.DocumentPurpose = new DocumentPurposeModel { Name = string.Empty };
                if (filters != null)
                {
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.DocumentPurpose.Name = (string)filters.Where(a => a.Field.Equals("DocumentPurpose")).Select(a => a.Value).SingleOrDefault();
                    filter.FileName = (string)filters.Where(a => a.Field.Equals("FileName")).Select(a => a.Value).SingleOrDefault();
                    filter.DocumentRefNumber = (string)filters.Where(a => a.Field.Equals("DocumentRefNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.AttachmentReference = (string)filters.Where(a => a.Field.Equals("AttachmentReference")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                bool isDocumentRefNumber = !string.IsNullOrEmpty(filter.DocumentRefNumber);
                using (DBSEntities context = new DBSEntities())
                {
                    IEnumerable<CustomerUnderlyingFileModel> data = (from a in context.CustomerUnderlyingFiles
                                                                     join b in context.CustomerUnderlyingMappings on a.UnderlyingFileID equals b.UnderlyingFileID
                                                                     join c in context.CustomerUnderlyings on b.UnderlyingID equals c.UnderlyingID
                                                                     join f in context.TransactionUnderlyings on c.UnderlyingID equals f.UnderlyingID
                                                                     let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                                                     let isDocPurpose = !string.IsNullOrEmpty(filter.DocumentPurpose.Name)
                                                                     let isFileName = !string.IsNullOrEmpty(filter.FileName)
                                                                     let isAttachmentReference = !string.IsNullOrEmpty(filter.AttachmentReference)
                                                                     let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                                                     let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                                                     where a.IsDeleted.Equals(false) && f.TransactionID.Equals(id)
                                                                     && b.IsDeleted.Equals(false) && c.IsDeleted.Equals(false) &&
                                                                         (isCIF ? c.CIF.Equals(filter.CIF) : true) &&
                                                                         (isDocPurpose ? a.DocumentPurpose.PurposeName.Contains(filter.DocumentPurpose.Name) : true) &&
                                                                         (isFileName ? a.FileName.Contains(filter.FileName) : true) &&
                                                                         (isAttachmentReference ? a.AttachmentReference.Contains(filter.AttachmentReference) : true) &&
                                                                         (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                                                         (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                                                     select new CustomerUnderlyingFileModel
                                                                     {
                                                                         ID = a.UnderlyingFileID,
                                                                         FileName = a.FileName,
                                                                         DocumentPath = a.DocumentPath,
                                                                         AttachmentReference = a.AttachmentReference,
                                                                         DocumentRefNumber = c.ReferenceNumber == null ? "" : c.ReferenceNumber,
                                                                         DocumentPurpose = new DocumentPurposeModel { ID = a.DocumentPurpose.PurposeID, Name = a.DocumentPurpose.PurposeName, Description = a.DocumentPurpose.PurposeDescription },
                                                                         DocumentType = new DocumentTypeModel { ID = a.DocumentType.DocTypeID, Name = a.DocumentType.DocTypeName, Description = a.DocumentType.DocTypeDescription },
                                                                         LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                         LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                                     });

                    var groupData = (from a in data.ToList()

                                     // where (isDocumentRefNumber ? a.DocumentRefNumber.Contains(filter.DocumentRefNumber) : true)
                                     group a by a.ID into groupFile
                                     select new CustomerUnderlyingFileModel
                                     {
                                         ID = groupFile.FirstOrDefault().ID,
                                         FileName = groupFile.FirstOrDefault().FileName,
                                         DocumentPath = groupFile.FirstOrDefault().DocumentPath,
                                         DocumentRefNumber = string.Join(", ", (groupFile.Select(x => x.DocumentRefNumber)).ToArray()),
                                         DocumentPurpose = groupFile.FirstOrDefault().DocumentPurpose,
                                         AttachmentReference = groupFile.FirstOrDefault().AttachmentReference,
                                         DocumentType = groupFile.FirstOrDefault().DocumentType,
                                         LastModifiedBy = groupFile.FirstOrDefault().LastModifiedBy,
                                         LastModifiedDate = groupFile.FirstOrDefault().LastModifiedDate
                                     }).AsQueryable();
                    CustomerUnderlying.Page = page;
                    CustomerUnderlying.Size = size;
                    CustomerUnderlying.Total = groupData.Count();
                    CustomerUnderlying.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    CustomerUnderlying.Rows = groupData.OrderBy(orderBy).Where(x => x.DocumentRefNumber.Contains(isDocumentRefNumber ? filter.DocumentRefNumber : "")).AsEnumerable()
                        .Select((a, i) => new CustomerUnderlyingFileRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            FileName = a.FileName,
                            DocumentPath = a.DocumentPath,
                            DocumentRefNumber = a.DocumentRefNumber,
                            AttachmentReference = a.AttachmentReference,
                            DocumentPurpose = a.DocumentPurpose,
                            DocumentType = a.DocumentType,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            CustomerUnderlyingMappings = context.CustomerUnderlyingMappings.Where(e => e.UnderlyingFileID == e.UnderlyingFileID && e.IsDeleted.Equals(false)).Select(e => new CustomerUnderlyingMappingModel
                            {
                                ID = e.UnderlyingMappingID,
                                UnderlyingID = e.UnderlyingID,
                                UnderlyingFileID = e.UnderlyingFileID
                            }).ToList()
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetDealCustomerUnderlyingFile(long id, int page, int size, IList<CustomerUnderlyingFileFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingFileGrid CustomerUnderlying, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                CustomerUnderlyingFileModel filter = new CustomerUnderlyingFileModel();
                filter.DocumentPurpose = new DocumentPurposeModel { Name = string.Empty };
                if (filters != null)
                {
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.DocumentPurpose.Name = (string)filters.Where(a => a.Field.Equals("DocumentPurpose")).Select(a => a.Value).SingleOrDefault();
                    filter.FileName = (string)filters.Where(a => a.Field.Equals("FileName")).Select(a => a.Value).SingleOrDefault();
                    filter.DocumentRefNumber = (string)filters.Where(a => a.Field.Equals("DocumentRefNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.AttachmentReference = (string)filters.Where(a => a.Field.Equals("AttachmentReference")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    IEnumerable<CustomerUnderlyingFileModel> data = (from a in context.CustomerUnderlyingFiles
                                                                     join b in context.CustomerUnderlyingMappings on a.UnderlyingFileID equals b.UnderlyingFileID
                                                                     join c in context.CustomerUnderlyings on b.UnderlyingID equals c.UnderlyingID
                                                                     join f in context.TransactionDealUnderlyings on c.UnderlyingID equals f.UnderlyingID
                                                                     let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                                                     let isDocPurpose = !string.IsNullOrEmpty(filter.DocumentPurpose.Name)
                                                                     let isFileName = !string.IsNullOrEmpty(filter.FileName)
                                                                     let isDocumentRefNumber = !string.IsNullOrEmpty(filter.DocumentRefNumber)
                                                                     let isAttachmentReference = !string.IsNullOrEmpty(filter.AttachmentReference)
                                                                     let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                                                     let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                                                     where a.IsDeleted.Equals(false) && f.TransactionDealID.Equals(id)
                                                                     && b.IsDeleted.Equals(false) && c.IsDeleted.Equals(false) &&
                                                                         (isCIF ? c.CIF.Equals(filter.CIF) : true) &&
                                                                         (isDocPurpose ? a.DocumentPurpose.PurposeName.Contains(filter.DocumentPurpose.Name) : true) &&
                                                                         (isFileName ? a.FileName.Contains(filter.FileName) : true) &&
                                                                         (isDocumentRefNumber ? a.DocumentRefNumber.Contains(filter.DocumentRefNumber) : true) &&
                                                                         (isAttachmentReference ? a.AttachmentReference.Contains(filter.AttachmentReference) : true) &&
                                                                         (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                                                         (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                                                     select new CustomerUnderlyingFileModel
                                                                     {
                                                                         ID = a.UnderlyingFileID,
                                                                         FileName = a.FileName,
                                                                         DocumentPath = a.DocumentPath,
                                                                         DocumentRefNumber = c.ReferenceNumber == null ? "" : c.ReferenceNumber,
                                                                         AttachmentReference = a.AttachmentReference,
                                                                         DocumentPurpose = new DocumentPurposeModel { ID = a.DocumentPurpose.PurposeID, Name = a.DocumentPurpose.PurposeName, Description = a.DocumentPurpose.PurposeDescription },
                                                                         DocumentType = new DocumentTypeModel { ID = a.DocumentType.DocTypeID, Name = a.DocumentType.DocTypeName, Description = a.DocumentType.DocTypeDescription },
                                                                         LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                         LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                                     });
                    var groupData = (from a in data.ToList()
                                     group a by a.ID into groupFile
                                     select new CustomerUnderlyingFileModel
                                     {
                                         ID = groupFile.FirstOrDefault().ID,
                                         FileName = groupFile.FirstOrDefault().FileName,
                                         DocumentPath = groupFile.FirstOrDefault().DocumentPath,
                                         DocumentRefNumber = string.Join(", ", (groupFile.Select(x => x.DocumentRefNumber)).ToArray()),
                                         AttachmentReference = groupFile.FirstOrDefault().AttachmentReference,
                                         DocumentPurpose = groupFile.FirstOrDefault().DocumentPurpose,
                                         DocumentType = groupFile.FirstOrDefault().DocumentType,
                                         LastModifiedBy = groupFile.FirstOrDefault().LastModifiedBy,
                                         LastModifiedDate = groupFile.FirstOrDefault().LastModifiedDate
                                     }).AsQueryable();
                    CustomerUnderlying.Page = page;
                    CustomerUnderlying.Size = size;
                    CustomerUnderlying.Total = groupData.Count();
                    CustomerUnderlying.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    CustomerUnderlying.Rows = groupData.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new CustomerUnderlyingFileRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            FileName = a.FileName,
                            DocumentPath = a.DocumentPath,
                            DocumentRefNumber = a.DocumentRefNumber,
                            AttachmentReference = a.AttachmentReference,
                            DocumentPurpose = a.DocumentPurpose,
                            DocumentType = a.DocumentType,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            CustomerUnderlyingMappings = context.CustomerUnderlyingMappings.Where(e => e.UnderlyingFileID == e.UnderlyingFileID && e.IsDeleted.Equals(false)).Select(e => new CustomerUnderlyingMappingModel
                            {
                                ID = e.UnderlyingMappingID,
                                UnderlyingID = e.UnderlyingID,
                                UnderlyingFileID = e.UnderlyingFileID
                            }).ToList()
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
    }
    #endregion
}