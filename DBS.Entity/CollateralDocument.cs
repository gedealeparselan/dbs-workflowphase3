//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class CollateralDocument
    {
        public long CLDocumentID { get; set; }
        public int CLDocCategoryID { get; set; }
        public string DocumentName { get; set; }
        public System.DateTime DateofDocument { get; set; }
        public string DocumentRefNo { get; set; }
        public Nullable<bool> IsSuporting { get; set; }
        public string FiduciaryType { get; set; }
        public string FiduciaryCoverage { get; set; }
        public string Number { get; set; }
        public string FiduciaryValue { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string GuaranteeCustType { get; set; }
        public string GuarantorName { get; set; }
        public string GuarantorType { get; set; }
        public string GuarantorCoverage { get; set; }
        public string Description { get; set; }
        public string IssuedBy { get; set; }
        public string Referencename { get; set; }
        public string PledgeType { get; set; }
        public string Object { get; set; }
        public string NumberDoc { get; set; }
        public Nullable<System.DateTime> MaturityDate { get; set; }
        public string PledgeID { get; set; }
        public string IssuingBank { get; set; }
        public string Applicant { get; set; }
        public string Beneficiary { get; set; }
        public string Type { get; set; }
        public string Term { get; set; }
        public string Maturity { get; set; }
        public string Owner { get; set; }
        public string AgreementNo { get; set; }
        public string Detail { get; set; }
        public string AgreementID { get; set; }
        public string Location { get; set; }
        public string RegisteredOwner { get; set; }
        public string Area { get; set; }
        public Nullable<System.DateTime> ExpiryDate { get; set; }
        public string CertificateNo { get; set; }
        public string Rank { get; set; }
        public string ObjectType { get; set; }
        public Nullable<int> DocTypePID { get; set; }
        public Nullable<int> CurrencyID { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<System.DateTime> DocReceiveDate { get; set; }
        public Nullable<int> DocStatusPID { get; set; }
        public Nullable<int> LoanStatusPID { get; set; }
        public string Remarks { get; set; }
        public string OtherRemarks { get; set; }
        public Nullable<bool> IsCCP { get; set; }
        public Nullable<bool> IsSC { get; set; }
        public Nullable<bool> IsReconsile { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    
        public virtual CLDocumentCategory CLDocumentCategory { get; set; }
    }
}
