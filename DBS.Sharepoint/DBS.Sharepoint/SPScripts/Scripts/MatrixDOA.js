var ddlInternalConditions = [{ ID: 1, Name: "AND" }, { ID: 2, Name: "OR" }];
var ddlExternalConditions = [{ ID: 1, Name: "AND" }, { ID: 2, Name: "OR" }];
var ddlValueTypes = [{ ID: 0, Name: "Value" }, { ID: 1, Name: "Parameter" }];
var ddlSubParameters;
var ddlSubConditions;
var ddlProduct = [{ ID: ConsProductID.LoanDisbursmentProductIDCons, Name: "Loan Disbursement" }, { ID: ConsProductID.LoanRolloverProductIDCons, Name: "Loan Rollover" }, { ID: ConsProductID.LoanIMProductIDCons, Name: "Loan Interest Maintenance" }, { ID: ConsProductID.LoanSettlementProductIDCons, Name: "Loan Settlement" }]; //modify adi 18-01-2017
var currLevel = 1;
var resultDOADetailModel = function (ID, matrixFieldID, valueType, operatorID, value1, orderID, internalRelation, levelRelation, isVisible, ddlInternalCondition, ddlExternalCondition, ddlValueTypes, valueText, ddlSubParameters, ddlSubConditions) {
    var self = this;
    self.DetailID = ko.observable(ID);
    self.MatrixFieldID = ko.observable(matrixFieldID);
    self.ValueType = ko.observable(valueType);
    self.OperatorID = ko.observable(operatorID);
    self.Value1 = ko.observable(value1);
    self.ValueText = ko.observable(valueText);
    //self.Relation = ko.observable(relation);
    self.OrderID = ko.observable(orderID);
    self.LevelRelation = ko.observable(levelRelation);
    self.Relation = ko.observable(internalRelation);
    self.IsVisible = ko.observable(isVisible);
    self.IsFieldComparer = ko.observable(valueType == "1");
    self.IsValueParameter = ko.observable(valueType == "1");
    self.IsValue = ko.observable(valueType == "0");
    self.ddlInternalCondition = ko.observableArray(ddlInternalCondition);
    self.ddlExternalCondition = ko.observableArray(ddlExternalCondition);
    self.ddlValueType = ko.observableArray(ddlValueTypes);
    self.ddlProduct = ko.observableArray(ddlProduct);
    self.ddlSubParameter = ko.observableArray(ddlSubParameters);
    self.ddlSubCondition = ko.observableArray(ddlSubConditions);

    self.RemoveDetail = function () {
        self.Details.remove(this);
        currLevel = self.Details[self.Details.length-1].LevelRelation;

    };

}

var resultDOAApproverModel = function (ID, approverPosition, approverCount, relation, ddlInternalCondition) {
    var self = this;
    self.ApproverID = ko.observable(ID);
    self.ApproverPosition = ko.observable(approverPosition);
    self.ApproverCount = ko.observable(approverCount);
    self.ApproverRelation = ko.observable(relation);
    self.ddlInternalCondition = ko.observableArray(ddlInternalCondition);
}

var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;
    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);

    //Declare observable which will be bind with UI 

    self.ID = ko.observable("");
    self.DOAName = ko.observable("");
    self.ProductName = ko.observable("");
    self.ApprovalDOAHeader = ko.observable("");
    self.Description = ko.observable("");
    self.MatrixCondition = ko.observable("");
    self.MatrixDetailDOA = ko.observableArray([]);
    self.ApproverDOA = ko.observableArray([]);
    self.ParamName = ko.observable("");
    self.OperatorName = ko.observable("");

    self.ValueName = ko.observable("");
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");
    self.IsddlValue = ko.observable(false);
    //Detail
    self.DetailID = ko.observable("");
    self.MatrixFieldID = ko.observable("");
    self.IsFieldComparer = ko.observable(false);
    self.ProductID = ko.observable("");
    self.OperatorID = ko.observable("");
    self.Value1 = ko.observable("");
    self.Value1ID = ko.observable("");
    self.Value2 = ko.observable("");
    self.Value2ID = ko.observable("");
    self.Relation = ko.observable("");
    self.OrderID = ko.observable("");
    self.ValueType = ko.observable("");
    self.LevelRelationInternal = ko.observable("");
    self.LevelRelationExternal = ko.observable("");
    self.RemoveDetail = function () {
        self.Details.remove(this);

    };
    //Approver
    self.ApproverID = ko.observable("");
    self.ApproverPosition = ko.observable("");
    self.ApproverCount = ko.observable("");
    self.ApproverRelation = ko.observable("");
    //Dropdown
    self.ddlMatrix = ko.observableArray([]);
    self.ddlSubParameters = ko.observableArray([]);
    self.ddlSubConditions = ko.observableArray([]);
    self.ddlSubValuetypes = ko.observableArray([]);
    self.ddlSubValues = ko.observableArray([]);
    self.ddlValue = ko.observableArray([]);
    self.ddlProduct = ko.observableArray(ddlProduct);

    // filter
    self.FilterProductName = ko.observable("");
    self.FilterDOAName = ko.observable("");
    self.FilterApprovalDOA = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");


    

    self.Details = ko.observableArray([new resultDOADetailModel('', '', '', '', '', '', '', '1', true, ddlInternalConditions, ddlExternalConditions, ddlValueTypes, '')]);
    self.Approvers = ko.observableArray([new resultDOAApproverModel('', '', '', '', ddlInternalConditions)]);

    // New Data flag
    self.IsNewData = ko.observable(false);
    self.IsValueTypeParameter = ko.observable(false);

    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    self.DOA = ko.observable("");
    self.MatrixDOA = ko.observableArray([]);
    self.MatrixDOADetail = ko.observableArray([new resultDOADetailModel(0, '', '', [])]);
    self.MatrixDOAApprover = ko.observableArray([new resultDOAApproverModel('', '', '', '', ddlInternalConditions)]);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    // set default sorting
    self.GridProperties().SortColumn("ApprovalDOAName");
    self.GridProperties().SortOrder("ASC");

    // bind clear filters
    self.ClearFilters = function () {
        self.FilterProductName("");
        self.FilterDOAName("");
        self.FilterApprovalDOA("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");

        GetData();
    };


    // bind get data function to view
    self.GetData = function () {
        GetData();
    };
    self.GetDropdown = function () {
        GetDropdown();
    };
    //The Object which stored data entered in the observables
    var MatrixDOA = {
        ID: self.ID,
        ProductID: self.ProductID,
        Name: self.DOAName,
        ProductName: self.ProductName,
        ApprovalDOAHeader: self.ApprovalDOAHeader,
        Description: self.Description,
        MatrixCondition: self.MatrixCondition,
        MatrixDetailDOA: self.MatrixDetailDOA,
        ApproverDOA: self.ApproverDOA,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
    };

    self.AddGroup = function () {
        self.Details.push(new resultDOADetailModel('', '', '', '', '', '', '', currLevel, true, ddlInternalConditions, ddlExternalConditions, ddlValueTypes, '', ddlSubParameters, ddlSubConditions));
    };
    self.AddLevel = function () {
        currLevel = currLevel + 1;
        self.Details.push(new resultDOADetailModel('', '', '', '', '', '', '', currLevel, true, ddlInternalConditions, ddlExternalConditions, ddlValueTypes, '', ddlSubParameters, ddlSubConditions));
    };

    self.AddApprover = function () {
        self.Approvers.push(new resultDOAApproverModel('', '', '', '', ddlInternalConditions));
    }

    self.RemoveApprover = function () {
        self.Approvers.remove(this);
    };

    self.RemoveGroup = function () {
        self.Details.remove(this);
        currLevel = self.Details()[self.Details().length - 1].LevelRelation();
    };

    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    console.log("spUser Roles : " + ko.toJSON(spUser.Roles));
                    console.log("spUser Permited : " + data);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, '', 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, '', 'gritter-error', true);
            }
        });
    }

    self.save = function () {
        // validation
        console.log(ddlProduct);
        ko.utils.arrayFilter(ddlProduct, function (item) {
            if (item.ID == MatrixDOA.ProductID()) {
                self.ProductName(item.Name);
                console.log(item.Name);
            }
        });
        var form = $("#aspnetForm");
        form.validate();
        changeObject(self.Details(), self.Approvers());

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    //console.log(ko.toJSON(ExceptionHandling));
                    //Ajax call to insert the ExceptionHandlings
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.matrixDOA,
                        data: ko.toJSON(MatrixDOA), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, '', 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, '', 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.update = function () {
        // validation

        //ExceptionHandling.MatrixName($('#matrix option:selected').text());
        console.log('update');
        var form = $("#aspnetForm");
        form.validate();
        changeObject(self.Details(), self.Approvers());
        if (form.valid()) {

            // hide current popup window
            //$("#modal-form").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.matrixDOA + "/" + MatrixDOA.ID(),
                        data: ko.toJSON(MatrixDOA),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, '', 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.delete = function () {

        changeObject(self.Details(), self.Approvers());
        //ExceptionHandling.MatrixName($('#matrix option:selected').text());
        // hide current popup window
        //$("#modal-form").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.matrixDOA + "/" + MatrixDOA.ID(),
                    data: ko.toJSON(MatrixDOA),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, '', 'gritter-error', true);
                    }
                });
            }
        });
    };

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        console.log(data);
        self.IsNewData(false);


        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');

        self.IsddlValue(false);
        self.ID(data.ID);
        //self.DOAName(data.Name);
        self.DOAName(data.Name);
        self.ApprovalDOAHeader(data.ApprovalDOAHeader);
        self.ProductID(data.ProductID);
        //self.Matrix(new MatrixModel(data.Matrix.ID, data.Matrix.Name, data.Matrix.Description));

        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);
        self.Details([]);
        self.Approvers([]);

        //console.log(data.ExceptionHandlingsDetail);
        //if (data.ID > 0) {
            var prevlevel = 0;
            var externalCondition = '';
            var internalCondition = '';
            for (var i = 0; i < data.ApproverDOA.length; i++) {
                var position = data.ApproverDOA[i].ApproverPosition;
                var count = data.ApproverDOA[i].ApproverCount;
                var id = data.ApproverDOA[i].ID;
                var matrixId = data.ApproverDOA[i].MatrixID;
                var relation = data.ApproverDOA[i].Rlation;
                self.Approvers.push(new resultDOAApproverModel(id, position, count, relation, ddlInternalConditions));
            }
            for (var i = 0; i < data.MatrixDetailDOA.length; i++) {
                var internalCondition = data.MatrixDetailDOA[i].Relation;
                var level = data.MatrixDetailDOA[i].LevelRelation;
                var matrixID = data.MatrixDetailDOA[i].ID;
                var valueType = data.MatrixDetailDOA[i].IsFieldComparer ? '1' : '0';
                var matrixFieldID = data.MatrixDetailDOA[i].MatrixFieldID;
                var operatorID = data.MatrixDetailDOA[i].OperatorID;
                var orderID = data.MatrixDetailDOA[i].OrderID;
                var value1 = data.MatrixDetailDOA[i].Value1;

                //if (valueType == 1) {
                //    var posStart = value1.indexOf('(') + 1;
                //    var posEnd = value1.length;
                //    value1 = (data.MatrixDetailDOA[i].Value1).substring(posStart, posEnd);
                //    value1 = value1.replace('(', '').replace(')', '');
                //} else {
                //    value1 = data.MatrixDetailDOA[i].Value1;
                //}

                var IsVisible = true;
                if (valueType == undefined || valueType == "") {

                    IsVisible = false;
                    valueTypeName = '';
                    value1 = '';

                    $('#lblvaluetype' + i + j).hide(); $('#lblvalue' + i + j).hide();
                    $('#ValueType' + i + j).hide(); $('#Value' + i + j).hide();

                }
                self.Details.push(new resultDOADetailModel(matrixID, matrixFieldID, valueType, operatorID, value1, orderID, internalCondition, level, true, ddlInternalConditions, ddlExternalConditions, ddlValueTypes, value1, ddlSubParameters, ddlSubConditions));
                currLevel = level;
                //console.log(ko.toJSON(self.Details()[i].Subdetails));
                var suspected = $('#lblParameter' + i + j).text().trim();

                if (data.MatrixDetailDOA[i].IsFieldComparer) {
                    $('#Value' + i + j).hide();
                    $('#ValueTypeParameter' + i + j).show();
                    $('#Value' + i + j).val('');
                } else {

                    var conditionOperator = $('#ConditionOpr' + i  + ' option:selected').text();
                    if ((conditionOperator.toLowerCase().trim() == 'is empty') ||
                        (conditionOperator.toLowerCase().trim() == 'is not empty')) {
                        $('#lblvaluetype' + i).hide(); $('#lblvalue' + i).hide();
                        $('#ValueType' + i).hide(); $('#Value' + i).hide();
                        $('#ValueTypeParameter' + i).hide();
                        $('#lblstarvaluetypename' + i).hide();
                        $('#lblstarvalueparameter' + i).hide();
                    } else {
                        $('#lblvaluetype' + i).show(); $('#lblvalue' + i).show();
                        $('#ValueType' + i).show(); $('#Value' + i).show();
                        $('#lblstarvaluetypename' + i).show();
                        $('#lblstarvalueparameter' + i).show();

                        if (ValueType.toLowerCase().trim() == 'parameter') {
                            $('#ValueTypeParameter' + i).show(); $('#Value' + i).hide();
                        } else {
                            $('#ValueTypeParameter' + i).hide(); $('#Value' + i).show();

                        }
                    }
                }
            }

        //}
        $(".lblforhidden").each(
	        function () {
	            var content = $(this).text().trim();

	            //Get the length of the content;
	            //var contentLength = $.trim( content ).length;
	            if ((content == ":")) {
	                $(this).hide();
	            }
	        }
    	);
    };
    function IsEmptyOrIsNotEmpty(index) {

        var Condition = $('#ConditionOpr' + index + ' option:selected').text();
        var ValueType = $('#ValueType' + index + ' option:selected').text();
        if ((Condition.toLowerCase().trim() == 'is empty') ||
			(Condition.toLowerCase().trim() == 'is not empty')) {

            $('#lblvaluetype' + index).hide(); $('#lblvalue' + index).hide();
            $('#ValueType' + index).hide(); $('#Value'+ index).hide();
            $('#ValueTypeParameter' + index).hide();
            $('#lblstarvaluetypename' + index).hide();
            $('#lblstarvalueparameter' + index).hide();


        } else {

            $('#lblvaluetype' + index).show(); $('#lblvalue' + index).show();
            $('#ValueType' + index).show(); $('#Value' + index).show();
            $('#lblstarvaluetypename' + index).show();
            $('#lblstarvalueparameter' + index).show();

            if (ValueType.toLowerCase().trim() == 'parameter') {
                $('#ValueTypeParameter' + index).show(); $('#Value' + index).hide();
            } else {
                $('#ValueTypeParameter' + index).hide(); $('#Value' + index).show();

            }



        }

    }
    self.onChangeCondition = function (index) {

        IsEmptyOrIsNotEmpty(index);

    };

    function IsValueTypeParameter(index) {

        var ValueType = $('#ValueType' + index + ' option:selected').text();
        if ((ValueType.toLowerCase().trim() == 'parameter')) {

            //self.IsValueTypeParameter(true);
            $('#ValueTypeParameter' + index).show();
            $('#Value' + index).hide();


        } else {
            //self.IsValueTypeParameter(false);
            $('#ValueTypeParameter' + index).hide();
            $('#Value' + index).show();

        }

    }

    self.onChangeValueType = function (index) {

        IsValueTypeParameter(index);

    };

    //insert new
    self.NewData = function () {
        // flag as new Data
        self.IsNewData(true);
        self.Readonly(false);
        self.ID(0);
        self.DOAName('');
        self.ApprovalDOAHeader('');

        self.Details.removeAll();
        self.Approvers.removeAll();
        currLevel = 1;
        self.Details.push(new resultDOADetailModel('', '', '', '', '', '', '', currLevel, true, ddlInternalConditions, ddlExternalConditions, ddlValueTypes, '', ddlSubParameters, ddlSubConditions));
        self.Approvers.push(new resultDOAApproverModel('', '', '', '', ddlInternalConditions));
        //console.log(ko.toJS(self.IsNewData));
    };

    // Function to get Biz Segment for Dropdownlist
    self.OnAfterGetDropdown = null;
    function GetDropdown() {
        $.ajax({

            type: "GET",
            url: api.server + api.url.parameter + '?select=MatrixDOAParameter,MatrixDOAOperator',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {

                if (jqXHR.status = 200) {
                    console.log("Tes");
                    self.ddlSubParameters(data['MatrixDOAParameter']);
                    ddlSubParameters = self.ddlSubParameters();
                    self.ddlSubConditions(data['MatrixDOAOperator']);
                    ddlSubConditions = self.ddlSubConditions();
                    if (self.OnAfterGetDropdown) self.OnAfterGetDropdown();
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, '', 'gritter-error', true);
            }
        });
    }

    //Function to Read All Employee
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.matrixDOA,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }


    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];
        if (self.FilterProductName() != "") filters.push({ Field: 'ProductName', Value: self.FilterProductName() });
        if (self.FilterDOAName() != "") filters.push({ Field: 'MatrixName', Value: self.FilterDOAName() });
        if (self.FilterApprovalDOA() != "") filters.push({ Field: 'ApprovalDOAHeader', Value: self.FilterApprovalDOA() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

        return filters;
    };

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        console.log(data);
        if (jqXHR.status = 200) {
            self.MatrixDOA(data.Rows);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, '', 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }


    // Custom Function
    function changeObject(Details, Approvers) {
        var value = "";
        self.MatrixDetailDOA([]);
        self.ApproverDOA([]);
        //ID: self.ID,
        //Name: self.DOAName,
        //ApprovalDOAHeader: self.ApprovalDOAHeader,
        //Description: self.Description,
        //MatrixCondition: self.MatrixCondition,
        //MatrixDetailDOA: self.MatrixDetailDOA,
        //ApproverDOA: self.ApproverDOA,
        //LastModifiedBy: self.LastModifiedBy,
        //LastModifiedDate: self.LastModifiedDate

        if (ko.toJSON(Details.length) > 0) {
            for (var i = 0; i < Details.length; i++) {
                self.MatrixDetailDOA.push(new resultDOADetailModel(Details[i].DetailID(), Details[i].MatrixFieldID(), Details[i].ValueType(), Details[i].OperatorID(), Details[i].ValueText(), Details[i].OrderID(), Details[i].Relation(), Details[i].LevelRelation(), Details[i].IsVisible(), ddlInternalConditions, ddlExternalConditions, ddlValueTypes, Details[i].ValueText()));
            }
        }

        if (ko.toJSON(Approvers.length) > 0) {
            for (var i = 0; i < Approvers.length; i++) {
                self.ApproverDOA.push(new resultDOAApproverModel(Approvers[i].ApproverID(), Approvers[i].ApproverPosition(), Approvers[i].ApproverCount(), Approvers[i].ApproverRelation(), ddlInternalConditions));
            }
        }
    }

    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });

    // text editor (Body Email from Exception Handling)
    function showErrorAlert(reason, detail) {
        var msg = '';
        if (reason === 'unsupported-file-type') { msg = "Unsupported format " + detail; }
        else {
            console.log("error uploading file", reason, detail);
        }
        $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
    }
};