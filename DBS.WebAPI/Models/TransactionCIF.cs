﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Linq.Dynamic;
using System.Transactions;
using System.Linq;
using DBS.Entity;
using DBS.WebAPI.Helpers;
using System.IO;

namespace DBS.WebAPI.Models
{
    #region Property
    public class TransactionCIFModel
    {
        public Guid? WorkflowInstanceID { get; set; }
        public long ID { get; set; }
        public string ApplicationID { get; set; }
        public CustomerModel Customer { get; set; }
        public bool IsNewCustomer { get; set; }
        public ProductModel Product { get; set; }
        public TransactionTypeParameterModel TransactionType { get; set; }
        public MaintenanceTypeModel MaintenanceType { get; set; }
        public TransactionSubTypeModel TransactionSubType { get; set; }
        public StaffTaggingModel StaffTagging { get; set; }
        public bool IsDraft { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public IList<TransactionDocumentCIFModel> DocumentsCIF { get; set; }
        public GroupCheckBoxModel GroupCheckBox { get; set; }
        public string CreateBy { get; set; }
        public string StaffID { get; set; }
        public string ATMNumber { get; set; }
        public DateTime ApplicationDate { get; set; }
        public TransactionRetailCIFCBOModel RetailCIFCBO { get; set; }
        public CustomerAccountModel AccountNumber { get; set; }
        public bool? IsLOI { get; set; }
        public bool? IsPOI { get; set; }
        public bool? IsPOA { get; set; }
        public List<AccountListPOAsModel> AccountListPOAs { get; set; }
        public List<CustomerJoinLoiPoisModel> CustomerJoinLoiPois { get; set; }
        public int AccountTypeLoiPoiID { get; set; }
        public BranchRiskRatingModel BrachRiskRating { get; set; }
        public IList<CustomerModalModel> AddJoinTableCustomerCIF { get; set; }
        public IList<FFDAccountModalModel> AddJoinTableFFDAcountCIF { get; set; }
        public IList<FFDAccountModalModel> AddJoinTableAccountCIF { get; set; }
        public IList<DormantAccountModel> AddJoinTableDormantCIF { get; set; }
        public IList<FreezeUnfreezeModel> AddJoinTableFreezeUnfreezeCIF { get; set; }
        public CurrencyModel Currency { get; set; }
        public CustomerDraftModel CustomerDraft { get; set; }
        public CustomerAccountModel Account { get; set; }
        public CurrencyModel DebitCurrency { get; set; }
        public string BranchName { get; set; }
        public bool IsBringupTask { get; set; }
        public IList<AccountNumberLienUnlienModel> AccountNumberLienUnlien { get; set; }

    }
    public class AccountListPOAsModel
    {
        public string CIF { get; set; }
        public string AccountNumber { get; set; }
        public string CustomerName { get; set; }
        public int? CurrencyID { get; set; }
        public string IsJointAccount { get; set; }
    }
    public class CustomerJoinLoiPoisModel
    {
        public string CIF { get; set; }
        public long CustomerJoinLoiPoiID { get; set; }
        public string CustomerName { get; set; }
        public bool? IsDeleted { get; set; }
        public long? TransactionID { get; set; }
    }
    public class FreezeUnfreezeModel
    {
        public string AccountNumber { get; set; }
        public bool? IsAddTblFreezeAccount { get; set; }
        public bool? IsFreezeAccount { get; set; }
        public string CurrencyCode { get; set; }
    }
    public class DormantAccountModel
    {
        public string AccountNumber { get; set; }
        public CurrencyModalModel Currency { get; set; }
        public bool? IsAddDormantAccount { get; set; }
    }
    public class BranchRiskRatingModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class CustomerModalModel
    {
        public string CIF { get; set; }
        public string Name { get; set; }
    }

    public class FFDAccountModalModel
    {
        public string AccountNumber { get; set; }
        public CurrencyModalModel Currency { get; set; }
        public bool? IsAddFFDAccount { get; set; }
    }
    public class CurrencyModalModel
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public class TransactionDocumentCIFModel
    {
        public long ID { get; set; }
        public DocumentTypeModel Type { get; set; }
        public DocumentPurposeModel Purpose { get; set; }
        public string FileName { get; set; }
        public string DocumentPath { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }
    public class TransactionRetailCIFCBOModel
    {
        public string AccountNumber { get; set; }
        public string AtmNumber { get; set; }
        public string CellPhone { get; set; }
        public string CorrespondenseAddress { get; set; }
        public string CorrespondenseCity { get; set; }
        public string CorrespondenseCountry { get; set; }
        public string CorrespondenseKecamatan { get; set; }
        public string CorrespondenseKelurahan { get; set; }
        public string CorrespondensePostalCode { get; set; }
        public string CorrespondenseProvince { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public int? DispatchModeTypeID { get; set; }
        public string EmailAddress { get; set; }
        public string Fax { get; set; }
        public string HomePhone { get; set; }

        public string IdentityAddress { get; set; }
        public string IdentityCity { get; set; }
        public string IdentityCountry { get; set; }
        public DateTime? IdentityEndDate { get; set; }
        public string IdentityKecamatan { get; set; }
        public string IdentityKelurahan { get; set; }
        public string IdentityNumber { get; set; }
        public string IdentityPostalCode { get; set; }
        public string IdentityProvince { get; set; }
        public DateTime? IdentityStartDate { get; set; }
        public string DispatchModeOther { get; set; }
        public string IdentityAddress2 { get; set; }
        public string IdentityCity2 { get; set; }
        public string IdentityCountry2 { get; set; }
        public DateTime? IdentityEndDate2 { get; set; }
        public string IdentityKecamatan2 { get; set; }
        public string IdentityKelurahan2 { get; set; }
        public string IdentityNumber2 { get; set; }
        public string IdentityPostalCode2 { get; set; }
        public string IdentityProvince2 { get; set; }
        public DateTime? IdentityStartDate2 { get; set; }

        public string IdentityAddress3 { get; set; }
        public string IdentityCity3 { get; set; }
        public string IdentityCountry3 { get; set; }
        public DateTime? IdentityEndDate3 { get; set; }
        public string IdentityKecamatan3 { get; set; }
        public string IdentityKelurahan3 { get; set; }
        public string IdentityNumber3 { get; set; }
        public string IdentityPostalCode3 { get; set; }
        public string IdentityProvince3 { get; set; }
        public DateTime? IdentityStartDate3 { get; set; }

        public string IdentityAddress4 { get; set; }
        public string IdentityCity4 { get; set; }
        public string IdentityCountry4 { get; set; }
        public DateTime? IdentityEndDate4 { get; set; }
        public string IdentityKecamatan4 { get; set; }
        public string IdentityKelurahan4 { get; set; }
        public string IdentityNumber4 { get; set; }
        public string IdentityPostalCode4 { get; set; }
        public string IdentityProvince4 { get; set; }
        public DateTime? IdentityStartDate4 { get; set; }

        public bool? IsNPWPReceived { get; set; }
        public bool? IsCorrespondenseToEmail { get; set; }
        public int? MaintenanceTypeID { get; set; }
        public string Name { get; set; }
        public string Nationality { get; set; }
        public string NPWPNumber { get; set; }
        public string OfficeAddress { get; set; }
        public string OfficeCity { get; set; }
        public string OfficeCountry { get; set; }
        public string OfficeKecamatan { get; set; }
        public string OfficeKelurahan { get; set; }
        public string OfficePhone { get; set; }
        public string OfficePostalCode { get; set; }
        public string OfficeProvince { get; set; }
        public string Remarks { get; set; }
        public int? RequestTypeID { get; set; }
        public virtual ICollection<RetailCIFCBOAccNo> RetailCIFCBOAccNoes { get; set; }
        public long RetailCIFCBOID { get; set; }
        public string SpouseName { get; set; }
        public int? SubType { get; set; }
        //public virtual Transaction Transaction { get; set; }       
        public long TransactionID { get; set; }
        public string UBOIdentityType { get; set; }
        public string UBOJob { get; set; }
        public string UBOName { get; set; }
        public string UBOPhone { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdatedCellPhone { get; set; }
        public string UpdatedFax { get; set; }
        public string UpdatedHomePhone { get; set; }
        public string UpdatedOfficePhone { get; set; }
        public GroupCheckBoxModel GroupCheckBox { get; set; }
        public DateTime? ReportDate { get; set; }

        public string CompanyName { get; set; }
        public string Position { get; set; }
        public string WorkPeriod { get; set; }
        public string IndustryType { get; set; }

        public DateTime? NextReviewDate { get; set; }

        //Parameter System
        public ParameterSystemCIFModel MaritalStatusID { get; set; }
        public ParameterSystemCIFModel CellPhoneMethodID { get; set; }
        public ParameterSystemCIFModel HomePhoneMethodID { get; set; }
        public ParameterSystemCIFModel OfficePhoneMethodID { get; set; }
        public ParameterSystemCIFModel FaxMethodID { get; set; }
        public ParameterSystemCIFModel IdentityTypeID { get; set; }
        public ParameterSystemCIFModel IdentityTypeID2 { get; set; }
        public ParameterSystemCIFModel IdentityTypeID3 { get; set; }
        public ParameterSystemCIFModel IdentityTypeID4 { get; set; }

        public ParameterSystemCIFModel FundSource { get; set; }
        public ParameterSystemCIFModel NetAsset { get; set; }
        public ParameterSystemCIFModel MonthlyIncome { get; set; }
        public ParameterSystemCIFModel MonthlyExtraIncome { get; set; }
        public ParameterSystemCIFModel Job { get; set; }
        public ParameterSystemCIFModel AccountPurpose { get; set; }
        public ParameterSystemCIFModel IncomeForecast { get; set; }
        public ParameterSystemCIFModel OutcomeForecast { get; set; }
        public ParameterSystemCIFModel TransactionForecast { get; set; }
        public ParameterSystemCIFModel RiskRatingResult { get; set; }
        public ParameterSystemCIFModel DispatchModeType { get; set; }
        public string HubunganNasabah { get; set; }

        //added by dani 12-apr-16 start
        public string PekerjaanProfesional { get; set; }
        public string PekerjaanLainnya { get; set; }
        public string TujuanBukaRekeningLainnya { get; set; }
        public string SumberDanaLainnya { get; set; }
        public decimal? AmountLienUnlien { get; set; }
        public int? Tier { get; set; }
        //end by dani
        //SU dayat
        public bool IsSignature { get; set; }
        public bool IsPenawaranProductPerbankan { get; set; }
        public bool IsStampDuty { get; set; }
        public bool IsOthers { get; set; }

        public IList<RCCBOContactModel> SelularPhoneNumbers { get; set; }
        public IList<RCCBOContactModel> OfficePhoneNumbers { get; set; }
        public IList<RCCBOContactModel> HomePhoneNumbers { get; set; }


        //endSU dayat
        //henggar 180417
        public string MotherMaiden { get; set; }
        public int Education { get; set; }
        public int Religion { get; set; }
        public int DataUBOCif { get; set; }
        public string DataUBOName { get; set; }
        public string DataUBORelationship { get; set; }
        public int GrosIncomeccy { get; set; }
        public CurrencyModel Currency { get; set; }
        public decimal GrossIncomeAmount { get; set; }
        public int GrossCifSpouse { get; set; }
        public string GrossSpouseName { get; set; }
        public string GrossSpouseRelation { get; set; }
        public bool ChangeSignature { get; set; }
        public int FatcaCountryCode { get; set; }
        public int FatcaReviewStatus { get; set; }
        public int FatcaCRSStatus { get; set; }
        public int FatcaTaxPayer { get; set; }
        public int FatcaWithHolding { get; set; }
        public DateTime? FatcaReviewStatusDate { get; set; }
        public DateTime? FatcaDateOnForm { get; set; }
        public string FatcaTaxPayerID { get; set; }
        public bool IsMotherMaiden { get; set; }
        public bool IsEducation { get; set; }
        public bool IsReligion { get; set; }
        public bool IsDataUBO { get; set; }
        public bool IsTransparansiDataPenawaranProduct { get; set; }
        public bool IsTransparansiPenggunaan { get; set; }
        public bool IsPenawaranProduct { get; set; }
        public bool IsChangeSignature { get; set; }
        public bool IsFatcaCRS { get; set; }
        public bool IsSLIK { get; set; }
        public string UBORelationship { get; set; }
        public int CIFUBO { get; set; }
        //end
    }

    public class CityCodeModel
    {
        public int CityID { get; set; }
        public string CityCode { get; set; }
        public string Description { get; set; }
        public int ProvinceID { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }

    public class CountryCodeModel
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string CountryCodeNumber { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }


    public class ParameterSystemCIFModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class GroupCheckBoxModel
    {
        public bool? IsAccountPurposeMaintenance { get; set; }
        public bool? IsCorrespondenceMaintenance { get; set; }
        public bool? IsCorrespondenseAddressMaintenance { get; set; }
        public bool? IsFundSourceMaintenance { get; set; }
        public bool? IsIdentityAddressMaintenance { get; set; }
        public bool? IsIdentityTypeMaintenance { get; set; }
        public bool? IsJobMaintenance { get; set; }
        public bool? IsMaritalStatusMaintenance { get; set; }
        public bool? IsMonthlyIncomeMaintenance { get; set; }
        public bool? IsMonthlyTransactionMaintenance { get; set; }
        public bool? IsNameMaintenance { get; set; }
        public bool? IsNationalityMaintenance { get; set; }
        public bool? IsNetAssetMaintenance { get; set; }
        public bool? IsNPWPMaintenance { get; set; }
        public bool? IsOfficeAddressMaintenance { get; set; }
        public bool? IsPhoneFaxEmailMaintenance { get; set; }
        public bool? IsTujuanBukaRekeningLainnya { get; set; }
        public bool? IsSumberDanaLainnya { get; set; }
        public bool? IsPekerjaanProfesional { get; set; }
        public bool? IsPekerjaanLainnya { get; set; }
        public bool? IsOthers { get; set; }

        //henggar 180417
        public bool IsMotherMaiden { get; set; }
        public bool IsEducation { get; set; }
        public bool IsReligion { get; set; }
        public bool IsDataUBO { get; set; }
        public bool IsTransparansiDataPenawaranProduct { get; set; }
        public bool IsTransparansiPenggunaan { get; set; }
        public bool IsPenawaranProduct { get; set; }
        public bool IsSignature { get; set; }
        public bool IsStampDuty { get; set; }
        public bool IsSLIK { get; set; }
        public bool IsFatcaCRS { get; set; }
        public bool IsChangeSignature { get; set; }
        public bool ChangeSignature { get; set; }
        //end
    }
    public class TransactionCIFDraftModel
    {
        public long ID { get; set; }
        public string ApplicationID { get; set; }
        public int? IsTopUrgent { get; set; }
        public CustomerModel Customer { get; set; }
        public ProductModel Product { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public string CIF { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool IsNewCustomer { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public string CreateBy { get; set; }
        public TransactionTypeModel TransactionType { get; set; }
        public MaintenanceTypeModel MaintenanceType { get; set; }
        public StaffTaggingModel StaffTagging { get; set; }
        public string StaffID { get; set; }
        public TransactionSubTypeModel TransactionSubTypeID { get; set; }
        public string ATMNumber { get; set; }
        public Guid? WorkflowInstanceID { get; set; }
        public TransactionSubTypeModel TransactionSubType { get; set; }
        public bool IsDraft { get; set; }
        public IList<TransactionDocumentCIFModel> DocumentsCIF { get; set; }
        public GroupCheckBoxModel GroupCheckBox { get; set; }
        public TransactionRetailCIFCBOModel RetailCIFCBO { get; set; }
        public bool? IsLOI { get; set; }
        public bool? IsPOI { get; set; }
        public bool? IsPOA { get; set; }
        public BranchRiskRatingModel BrachRiskRating { get; set; }
        public IList<CustomerModalModel> AddJoinTableCustomerCIF { get; set; }
        public IList<FFDAccountModalModel> AddJoinTableFFDAcountCIF { get; set; }
        public IList<FFDAccountModalModel> AddJoinTableAccountCIF { get; set; }
        public IList<DormantAccountModel> AddJoinTableDormantCIF { get; set; }
        public IList<FreezeUnfreezeModel> AddJoinTableFreezeUnfreezeCIF { get; set; }
        public CustomerAccountModel AccountNumber { get; set; }
        public IList<TransactionDocumentDraftModel> Documents { get; set; }
        public Currency Currency { get; set; }
        public IList<RCCBOContactDraftModel> SelularPhoneNumbers { get; set; }
        public IList<RCCBOContactDraftModel> OfficePhoneNumbers { get; set; }
        public IList<RCCBOContactDraftModel> HomePhoneNumbers { get; set; }
        public IList<AccountNumberLienUnlienModel> AccountNumberLienUnlien { get; set; }

    }

    public class TransactionChangeRMModel
    {
        public long ID { get; set; }
        public string ApplicationID { get; set; }
        public CustomerModel Customer { get; set; }
        public ProductModel Product { get; set; }
        public IList<ChangeRMModel> ChangeRM { get; set; }
        public CurrencyModel Currency { get; set; }
        public CustomerAccountModel AccountNumber { get; set; }
        public bool IsDraft { get; set; }
        public TransactionTypeModel TransactionType { get; set; }
        public MaintenanceTypeModel MaintenanceType { get; set; }
        public string BranchName { get; set; }
        public bool? IsChangeRM { get; set; }
        public bool? IsSegment { get; set; }
        public bool? IsSOLID { get; set; }
        public IList<TransactionDocumentModel> Documents { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public bool? IsBringupTask { get; set; }
    }

    public class TransactionChangeRMDraftModel
    {
        public long ID { get; set; }
        public string ApplicationID { get; set; }
        public CustomerModel Customer { get; set; }
        public ProductModel Product { get; set; }
        public IList<ChangeRMModel> ChangeRM { get; set; }
        public CurrencyModel Currency { get; set; }
        public CustomerAccountModel AccountNumber { get; set; }
        public bool? IsDraft { get; set; }
        public TransactionTypeModel TransactionType { get; set; }
        public MaintenanceTypeModel MaintenanceType { get; set; }
        public string BranchName { get; set; }
        public bool? IsChangeRM { get; set; }
        public bool? IsSegment { get; set; }
        public bool? IsSOLID { get; set; }
        public IList<TransactionDocumentDraftModel> Documents { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public bool? IsBringupTask { get; set; }
    }
    public class TransactionDispatchModeModel
    {
        public string CIF { get; set; }
        public int ProductID { get; set; }
        public int RequestTypeID { get; set; }
        public int MaintenanceTypeID { get; set; }
        public bool IsDraft { get; set; }
        public int DispatchModeTypeID { get; set; }
        public long TransactionDispatchModeID { set; get; }
        public long TransactionID { set; get; }
        public string ApplicationID { set; get; }
        public string DispatchModeOther { get; set; }
        public IList<DocumentModel> Documents { set; get; }
        public bool IsBringupTask { get; set; }
    }
    #endregion

    #region Filter
    public class TransactionCIFFilter : Filter { }

    //public class TransactionDocumentFilter : Filter { }
    #endregion

    #region Interface
    public interface ITransactionCIFRepository : IDisposable
    {
        bool AddTransaction(TransactionCIFModel transactionCIF, ref long transactionCIFID, ref string ApplicationID, ref string message);
        bool GetTransactionDraftByID(long id, ref TransactionCIFDraftModel transaction, ref string message);
        bool DeleteTransactionDraftByID(int id, ref string Message);
        bool UpdateTransactionDraft(long id, TransactionCIFModel transaction, ref string message);
        bool AddTransactionChangeRM(TransactionChangeRMModel transactionChangeRM, ref long transactionID, ref string ApplicationID, ref string message);
        bool DeleteTransactionChangeRMByID(long id, ref string message);
        bool GetTransactionChangeRMDraftByID(long id, ref TransactionChangeRMDraftModel transaction, ref string message);
        bool AddTransactionDispatchMode(TransactionDispatchModeModel data, ref long TransactionId, ref string ApplicationId, ref string message);

        bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineModel> timelines, ref string message);
        bool GetTransactionDispatchModeDraftByID(long id, ref TransactionDispatchModeModel transaction, ref string message);
        bool GetCustomerAccount(string CIF, ref IList<CustomerAccountModel> customerAccount, ref string message);
    }
    #endregion

    #region Repository
    public class TransactionCIFRepository : ITransactionCIFRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool AddTransaction(TransactionCIFModel transactionCIF, ref long transactionID, ref string ApplicationID, ref string message)
        {
            bool IsSuccess = false;
            long TrIDInserted = 0;
            using (DBSEntities context_ts = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        long RetailCIFCBOID = 0;

                        if (transactionCIF.IsDraft)
                        {
                            #region Save draft
                            if (transactionCIF.ID != null && transactionCIF.ID > 0)
                            {
                                /*
                                 * sebelumnya draft dan disimpan sebagai draft lagi
                                 */
                                #region drfat lama
                                Entity.TransactionDraft data = context.TransactionDrafts.Where(a => a.TransactionID.Equals(transactionCIF.ID)).SingleOrDefault();
                                if (data != null)
                                {
                                    #region Unused field but Not Null
                                    data.BeneName = "-";
                                    data.BankID = 1;
                                    data.IsResident = false;
                                    data.IsCitizen = false;
                                    data.AmountUSD = 0.00M;
                                    data.Rate = 0.00M;
                                    data.DebitCurrencyID = 1;
                                    data.BizSegmentID = 1;
                                    data.ChannelID = 1;
                                    #endregion

                                    #region Primary Field
                                    data.CIF = transactionCIF.Customer.CIF;
                                    data.ProductID = transactionCIF.Product.ID;
                                    data.TransactionID = transactionCIF.ID;
                                    data.ApplicationID = transactionCIF.ApplicationID;
                                    data.IsDraft = transactionCIF.IsDraft;
                                    data.IsNewCustomer = transactionCIF.IsNewCustomer;
                                    data.CreateDate = DateTime.UtcNow;
                                    data.CreateBy = currentUser.GetCurrentUser().LoginName;
                                    data.ApplicationDate = DateTime.UtcNow;
                                    data.CurrencyID = (transactionCIF.Currency.ID != 0 ? transactionCIF.Currency.ID : (int)CurrencyID.NullCurrency);
                                    data.Amount = 0;
                                    data.IsTopUrgent = 0;
                                    data.StateID = (int)StateID.OnProgress;
                                    data.StaffID = transactionCIF.StaffID;
                                    //Tambah StaffTagging
                                    data.StaffTaggingID = transactionCIF.StaffTagging.ID;
                                    //End
                                    data.ATMNumber = transactionCIF.ATMNumber;
                                    data.IsLOI = transactionCIF.IsLOI;
                                    data.IsPOI = transactionCIF.IsPOI;
                                    data.IsPOA = transactionCIF.IsPOA;
                                    data.LocationName = transactionCIF.BrachRiskRating.Name;
                                    data.AccountNumber = (transactionCIF.AccountNumber.AccountNumber == null ? null : transactionCIF.AccountNumber.AccountNumber);
                                    #endregion
                                    context_ts.SaveChanges();

                                    #region Customer Join
                                    if (transactionCIF.AddJoinTableCustomerCIF.Count() > 0)
                                    {

                                        IList<Entity.RetailCIFCBOAccountJoinDraft> ExistingJoinTableCustomer = context.RetailCIFCBOAccountJoinDrafts.Where(t => t.TransactionID == transactionCIF.ID).ToList();

                                        if (ExistingJoinTableCustomer != null && ExistingJoinTableCustomer.Count > 0)
                                        {
                                            foreach (var iJoinTableCustomer in ExistingJoinTableCustomer)
                                            {
                                                context.RetailCIFCBOAccountJoinDrafts.Remove(iJoinTableCustomer);
                                            }
                                            context_ts.SaveChanges();

                                            if (transactionCIF.AddJoinTableCustomerCIF != null)
                                            {
                                                if (transactionCIF.AddJoinTableCustomerCIF.Count() > 0)
                                                {
                                                    foreach (var iJoinTableCustomerNew in transactionCIF.AddJoinTableCustomerCIF)
                                                    {
                                                        Entity.RetailCIFCBOAccountJoinDraft dataRetailCIFCBOAccountJoin = new Entity.RetailCIFCBOAccountJoinDraft()
                                                        {
                                                            TransactionID = transactionID,
                                                            CIF = iJoinTableCustomerNew.CIF,
                                                            CreateDate = DateTime.UtcNow,
                                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                                        };
                                                        context_ts.RetailCIFCBOAccountJoinDrafts.Add(dataRetailCIFCBOAccountJoin);
                                                    }
                                                    context_ts.SaveChanges();
                                                }
                                            }
                                            context_ts.SaveChanges();
                                        }
                                    }
                                    #endregion

                                    #region CustomerAccount Join LOI POI Draft
                                    if (transactionCIF.CustomerJoinLoiPois != null)
                                    {
                                        IList<Entity.RetailCIFCBOCustomerJoinLoiPoiDraft> OldData = context_ts.RetailCIFCBOCustomerJoinLoiPoiDrafts.Where(t => t.TransactionID == transactionCIF.ID).ToList();
                                        foreach (var i in OldData)
                                        {
                                            i.IsDeleted = true;
                                        }
                                        context_ts.SaveChanges();
                                        foreach (var a in transactionCIF.CustomerJoinLoiPois)
                                        {
                                            Entity.RetailCIFCBOCustomerJoinLoiPoiDraft item = new Entity.RetailCIFCBOCustomerJoinLoiPoiDraft();
                                            item.CIF = a.CIF;
                                            item.CustomerName = a.CustomerName;
                                            item.TransactionID = transactionID;
                                            item.IsDeleted = false;
                                            context_ts.RetailCIFCBOCustomerJoinLoiPoiDrafts.Add(item);
                                        }
                                        context_ts.SaveChanges();
                                    }
                                    #endregion

                                    #region Account List Join POA Draft
                                    if (transactionCIF.AccountListPOAs != null)
                                    {
                                        IList<Entity.RetailCIFCBOAccountListPOADraft> OldData = context_ts.RetailCIFCBOAccountListPOADrafts.Where(t => t.TransactionID == transactionCIF.ID).ToList();
                                        foreach (var i in OldData)
                                        {
                                            i.IsDeleted = true;
                                        }
                                        context_ts.SaveChanges();
                                        foreach (var a in transactionCIF.AccountListPOAs)
                                        {
                                            Entity.RetailCIFCBOAccountListPOADraft item = new Entity.RetailCIFCBOAccountListPOADraft();
                                            item.AccountNumber = a.AccountNumber;
                                            item.CustomerName = a.CustomerName;
                                            item.CurrencyID = a.CurrencyID;
                                            item.CIF = a.CIF;
                                            item.IsJointAccount = a.IsJointAccount;
                                            item.TransactionID = transactionID;
                                            item.IsDeleted = false;
                                            context_ts.RetailCIFCBOAccountListPOADrafts.Add(item);
                                        }

                                        context_ts.SaveChanges();
                                    }
                                    #endregion

                                    #region FDD Account
                                    if (transactionCIF.AddJoinTableFFDAcountCIF.Count() > 0)
                                    {
                                        IList<Entity.RetailCIFCBOAccNoDraft> ExistingJoinTableFFDAccount = context.RetailCIFCBOAccNoDrafts.Where(t => t.RetailCIFCBODraftID == RetailCIFCBOID).ToList();
                                        if (ExistingJoinTableFFDAccount != null && ExistingJoinTableFFDAccount.Count > 0)
                                        {
                                            foreach (var iJoinTableFFDAccount in ExistingJoinTableFFDAccount)
                                            {
                                                context_ts.RetailCIFCBOAccNoDrafts.Remove(iJoinTableFFDAccount);
                                            }
                                            context_ts.SaveChanges();

                                            if (transactionCIF.AddJoinTableFFDAcountCIF != null)
                                            {
                                                if (transactionCIF.AddJoinTableFFDAcountCIF.Count() > 0)
                                                {
                                                    foreach (var iJoinTableFFDAccountNew in transactionCIF.AddJoinTableFFDAcountCIF)
                                                    {
                                                        Entity.RetailCIFCBOAccNoDraft dataRetailCIFCBOAccount = new Entity.RetailCIFCBOAccNoDraft()
                                                        {
                                                            RetailCIFCBODraftID = RetailCIFCBOID,
                                                            AccountNumber = iJoinTableFFDAccountNew.AccountNumber,
                                                            IsFFD = iJoinTableFFDAccountNew.IsAddFFDAccount,
                                                            CreateDate = DateTime.UtcNow,
                                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                                        };
                                                        context_ts.RetailCIFCBOAccNoDrafts.Add(dataRetailCIFCBOAccount);

                                                    }
                                                    context_ts.SaveChanges();
                                                }

                                            }
                                            context_ts.SaveChanges();
                                        }
                                    }

                                    #endregion

                                    #region Account Join
                                    if (transactionCIF.AddJoinTableAccountCIF.Count() > 0)
                                    {
                                        IList<Entity.RetailCIFCBOAccNoDraft> ExistingJoinTableAccount = context.RetailCIFCBOAccNoDrafts.Where(t => t.RetailCIFCBODraftID == RetailCIFCBOID).ToList();
                                        if (ExistingJoinTableAccount != null && ExistingJoinTableAccount.Count > 0)
                                        {
                                            foreach (var iJoinTableAccount in ExistingJoinTableAccount)
                                            {
                                                context.RetailCIFCBOAccNoDrafts.Remove(iJoinTableAccount);
                                            }
                                            context.SaveChanges();

                                            if (transactionCIF.AddJoinTableAccountCIF != null)
                                            {
                                                if (transactionCIF.AddJoinTableAccountCIF.Count() > 0)
                                                {
                                                    foreach (var iJoinTableAccountNew in transactionCIF.AddJoinTableAccountCIF)
                                                    {
                                                        Entity.RetailCIFCBOAccNoDraft dataRetailCIFCBOAccount = new Entity.RetailCIFCBOAccNoDraft()
                                                        {
                                                            RetailCIFCBODraftID = RetailCIFCBOID,
                                                            AccountNumber = iJoinTableAccountNew.AccountNumber,
                                                            CurrencyID = iJoinTableAccountNew.Currency.ID,
                                                            CreateDate = DateTime.UtcNow,
                                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                                        };
                                                        context_ts.RetailCIFCBOAccNoDrafts.Add(dataRetailCIFCBOAccount);

                                                    }
                                                    context_ts.SaveChanges();
                                                }

                                            }
                                            context_ts.SaveChanges();
                                        }
                                    }
                                    #endregion

                                    #region Dormant
                                    if (transactionCIF.AddJoinTableDormantCIF.Count() > 0)
                                    {
                                        IList<Entity.RetailCIFCBOAccNoDraft> ExistingJoinTableDormant = context.RetailCIFCBOAccNoDrafts.Where(t => t.RetailCIFCBODraftID == RetailCIFCBOID).ToList();
                                        if (ExistingJoinTableDormant != null && ExistingJoinTableDormant.Count > 0)
                                        {
                                            foreach (var iJoinTableDormant in ExistingJoinTableDormant)
                                            {
                                                context_ts.RetailCIFCBOAccNoDrafts.Remove(iJoinTableDormant);
                                            }
                                            context_ts.SaveChanges();
                                        }
                                        if (transactionCIF.AddJoinTableDormantCIF != null)
                                        {
                                            if (transactionCIF.AddJoinTableDormantCIF.Count() > 0)
                                            {
                                                var SelectedDormant = transactionCIF.AddJoinTableDormantCIF.Where(s => s.IsAddDormantAccount == true);
                                                if (SelectedDormant.Count() > 0)
                                                {
                                                    foreach (var iJoinDormantNew in SelectedDormant)
                                                    {
                                                        Entity.RetailCIFCBOAccNoDraft dataRetailCIFCBDormant = new Entity.RetailCIFCBOAccNoDraft()
                                                        {
                                                            RetailCIFCBODraftID = RetailCIFCBOID,
                                                            AccountNumber = iJoinDormantNew.AccountNumber,
                                                            CurrencyID = iJoinDormantNew.Currency.ID,
                                                            CreateDate = DateTime.UtcNow,
                                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                                        };
                                                        context_ts.RetailCIFCBOAccNoDrafts.Add(dataRetailCIFCBDormant);
                                                    }
                                                    context_ts.SaveChanges();
                                                }
                                            }
                                            context_ts.SaveChanges();
                                        }
                                    }
                                    #endregion

                                    #region Freeze Account
                                    if (transactionCIF.AddJoinTableFreezeUnfreezeCIF.Count() > 0)
                                    {
                                        IList<Entity.RetailCIFCBOFreezeAccountDraft> ExistingJoinFreezeUnfreeze = context.RetailCIFCBOFreezeAccountDrafts.Where(t => t.RetailCIFCBODraftID == RetailCIFCBOID).ToList();
                                        if (ExistingJoinFreezeUnfreeze != null && ExistingJoinFreezeUnfreeze.Count > 0)
                                        {
                                            foreach (var iJoinTableFreezeUnfreeze in ExistingJoinFreezeUnfreeze)
                                            {
                                                context_ts.RetailCIFCBOFreezeAccountDrafts.Remove(iJoinTableFreezeUnfreeze);
                                            }
                                            context_ts.SaveChanges();
                                        }
                                        if (transactionCIF.AddJoinTableFreezeUnfreezeCIF != null)
                                        {
                                            if (transactionCIF.AddJoinTableFreezeUnfreezeCIF.Count() > 0)
                                            {
                                                var SelectedFreeze = transactionCIF.AddJoinTableFreezeUnfreezeCIF.Where(s => s.IsAddTblFreezeAccount == true);
                                                foreach (var iJoinTableFreezeUnfreezeNew in SelectedFreeze)
                                                {
                                                    Entity.RetailCIFCBOFreezeAccountDraft dataRetailFreezeUnfreezeAccount = new Entity.RetailCIFCBOFreezeAccountDraft()
                                                    {
                                                        RetailCIFCBODraftID = RetailCIFCBOID,
                                                        AccountNumber = iJoinTableFreezeUnfreezeNew.AccountNumber,
                                                        IsFreeze = iJoinTableFreezeUnfreezeNew.IsFreezeAccount != null ? iJoinTableFreezeUnfreezeNew.IsFreezeAccount : null,
                                                        CreateDate = DateTime.UtcNow,
                                                        CreateBy = currentUser.GetCurrentUser().LoginName,
                                                    };
                                                    context_ts.RetailCIFCBOFreezeAccountDrafts.Add(dataRetailFreezeUnfreezeAccount);
                                                }
                                                context_ts.SaveChanges();
                                            }
                                            context_ts.SaveChanges();
                                        }
                                    }
                                    #endregion

                                    #region Save Document
                                    var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(transactionCIF.ID)).ToList();
                                    if (existingDocs != null)
                                    {
                                        if (existingDocs.Count() > 0)
                                        {
                                            foreach (var item in existingDocs)
                                            {
                                                var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                                deleteDoc.IsDeleted = true;
                                                context.SaveChanges();
                                            }
                                        }
                                    }
                                    if (transactionCIF.DocumentsCIF != null)
                                    {
                                        if (transactionCIF.DocumentsCIF.Count > 0)
                                        {
                                            foreach (var item in transactionCIF.DocumentsCIF)
                                            {
                                                Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                                                {
                                                    TransactionID = data.TransactionID,
                                                    DocTypeID = item.Type.ID,
                                                    PurposeID = item.Purpose.ID,
                                                    Filename = item.FileName,
                                                    DocumentPath = item.DocumentPath,
                                                    CreateDate = DateTime.UtcNow,
                                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                                };
                                                context_ts.TransactionDocumentDrafts.Add(document);
                                            }
                                            context_ts.SaveChanges();
                                        }
                                    }
                                    #endregion

                                    #region LienUnlien
                                    if (transactionCIF.AccountNumberLienUnlien != null)
                                    {
                                        if (transactionCIF.AccountNumberLienUnlien.Count > 0)
                                        {
                                            IList<AccountNumberLienUnlienModel> selectedLienUnlien = transactionCIF.AccountNumberLienUnlien.Where(d => d.IsSelected == true).ToList();
                                            IList<Entity.LienUnlienDraft> existingLienUnlien = context.LienUnlienDrafts.Where(x => x.TransactionId.Value == transactionCIF.ID).ToList();
                                            if (existingLienUnlien != null)
                                            {
                                                if (existingLienUnlien.Count() > 0)
                                                {
                                                    foreach (var item in existingLienUnlien)
                                                    {
                                                        var deleteLienUnlien = context.LienUnlienDrafts.Where(a => a.TransactionId.Value == item.TransactionId.Value).SingleOrDefault();
                                                        deleteLienUnlien.IsDeleted = true;
                                                        context.SaveChanges();
                                                    }
                                                }
                                            }
                                            if (selectedLienUnlien != null)
                                            {
                                                if (selectedLienUnlien.Count > 0)
                                                {
                                                    foreach (var item in selectedLienUnlien)
                                                    {
                                                        Entity.LienUnlienDraft dataInsert = new LienUnlienDraft();
                                                        dataInsert.CIF = transactionCIF.Customer.CIF;
                                                        dataInsert.TransactionId = transactionID;
                                                        dataInsert.AccountNumber = item.AccountNumber;
                                                        dataInsert.LienUnlienData = (item.LienUnlien.Trim().Equals("1") ? "Lien" : (item.LienUnlien.Trim().Equals("2") ? "Un-lien" : ""));
                                                        dataInsert.IsDeleted = false;
                                                        dataInsert.CreateBy = currentUser.GetCurrentUser().LoginName;
                                                        dataInsert.CreateDate = DateTime.UtcNow;

                                                        context_ts.LienUnlienDrafts.Add(dataInsert);
                                                    }
                                                    context_ts.SaveChanges();
                                                }
                                            }
                                            selectedLienUnlien = null;
                                        }
                                    }
                                    #endregion
                                }
                                #endregion
                            }
                            else
                            {
                                /*
                                 * draft baru
                                 */
                                #region draft baru
                                #region New Customer
                                if (transactionCIF.IsNewCustomer)
                                {
                                    context_ts.Customers.Add(new Customer()
                                    {
                                        CIF = transactionCIF.Customer.CIF,
                                        CustomerName = transactionCIF.Customer.Name,
                                        BizSegmentID = 1,
                                        LocationID = 1,
                                        CustomerTypeID = 1,
                                        SourceID = 1,
                                        IsDeleted = false,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName
                                    });
                                    context_ts.SaveChanges();
                                }
                                #endregion

                                bool isBringup = false;
                                if (transactionCIF.IsBringupTask == true) isBringup = true;

                                #region default value
                                DBS.Entity.TransactionDraft data = new DBS.Entity.TransactionDraft()
                                {
                                    BeneName = "-",
                                    BankID = 1,
                                    IsResident = false,
                                    IsCitizen = false,
                                    AmountUSD = 0.00M,
                                    Rate = 0.00M,
                                    DebitCurrencyID = 1,
                                    BizSegmentID = 1,
                                    ChannelID = 1,
                                    CurrencyID = (transactionCIF.Currency.ID != 0 ? transactionCIF.Currency.ID : (int)CurrencyID.NullCurrency),
                                    Amount = 0,
                                    IsTopUrgent = 0,
                                    StateID = (int)StateID.OnProgress,

                                    CIF = transactionCIF.Customer.CIF,
                                    ProductID = transactionCIF.Product.ID,
                                    TransactionID = transactionCIF.ID,
                                    ApplicationID = transactionCIF.ApplicationID,
                                    IsDraft = transactionCIF.IsDraft,
                                    IsNewCustomer = transactionCIF.IsNewCustomer,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName,
                                    ApplicationDate = DateTime.UtcNow,

                                    StaffID = transactionCIF.StaffID,
                                    //Tambah StaffTagging
                                    StaffTaggingID = transactionCIF.StaffTagging.ID,
                                    //End
                                    ATMNumber = transactionCIF.ATMNumber,
                                    IsLOI = transactionCIF.IsLOI,
                                    IsPOI = transactionCIF.IsPOI,
                                    IsPOA = transactionCIF.IsPOA,
                                    AccountNumber = (transactionCIF.AccountNumber == null ? null : transactionCIF.AccountNumber.AccountNumber),
                                    BranchName = transactionCIF.BranchName,
                                    LocationName = transactionCIF.BrachRiskRating.Name//added by dani 30-1-2016
                                };
                                #endregion
                                #region non default value
                                if (transactionCIF.TransactionType.TransTypeID == 0)
                                    data.TransactionTypeID = null;
                                else
                                    data.TransactionTypeID = transactionCIF.TransactionType.TransTypeID;

                                if (transactionCIF.MaintenanceType.ID == 0)
                                    data.CBOMaintainID = null;
                                else
                                    data.CBOMaintainID = (transactionCIF.MaintenanceType.ID == 0 ? int.Parse(null) : transactionCIF.MaintenanceType.ID);

                                if (transactionCIF.TransactionSubType.ID == 0)
                                    data.TransactionSubTypeID = null;
                                else
                                    data.TransactionSubTypeID = (transactionCIF.TransactionSubType.ID == 0 ? 1 : transactionCIF.TransactionSubType.ID);
                                data.IsBringUp = isBringup;

                                if (!transactionCIF.IsNewCustomer)
                                {
                                    data.CIF = transactionCIF.Customer.CIF;

                                    if (transactionCIF.Currency.Code != null && transactionCIF.Currency.Code != "")
                                    {
                                        data.CurrencyID = transactionCIF.Currency.ID;
                                        if (transactionCIF.Account != null)
                                        {
                                            data.AccountNumber = transactionCIF.Account.AccountNumber; // save draft if account number tidak diisi.
                                        }
                                    }
                                }
                                else
                                {
                                    data.DraftCIF = transactionCIF.Customer.CIF;
                                    data.DraftCustomerName = transactionCIF.Customer.Name;
                                }

                                context_ts.TransactionDrafts.Add(data);
                                context_ts.SaveChanges();
                                #endregion

                                transactionID = data.TransactionID;//transaction id baru yang terbentuk

                                #region CustomerAccount Join LOI POI Draft
                                if (transactionCIF.CustomerJoinLoiPois != null)
                                {

                                    foreach (var a in transactionCIF.CustomerJoinLoiPois)
                                    {
                                        Entity.RetailCIFCBOCustomerJoinLoiPoiDraft item = new Entity.RetailCIFCBOCustomerJoinLoiPoiDraft();
                                        item.CIF = a.CIF;
                                        item.CustomerName = a.CustomerName;
                                        item.TransactionID = transactionID;
                                        item.IsDeleted = false;
                                        context_ts.RetailCIFCBOCustomerJoinLoiPoiDrafts.Add(item);
                                    }

                                    context_ts.SaveChanges();
                                }
                                #endregion

                                #region Account List POA Draft
                                if (transactionCIF.AccountListPOAs != null)
                                {
                                    foreach (var a in transactionCIF.AccountListPOAs)
                                    {
                                        Entity.RetailCIFCBOAccountListPOADraft item = new Entity.RetailCIFCBOAccountListPOADraft();
                                        item.AccountNumber = a.AccountNumber;
                                        item.CustomerName = a.CustomerName;
                                        item.CurrencyID = a.CurrencyID;
                                        item.CIF = a.CIF;
                                        item.IsJointAccount = a.IsJointAccount;
                                        item.TransactionID = transactionID;
                                        item.IsDeleted = false;
                                        context_ts.RetailCIFCBOAccountListPOADrafts.Add(item);
                                    }

                                    context_ts.SaveChanges();
                                }
                                #endregion

                                #region RetailCIFCBODraft
                                DBS.Entity.RetailCIFCBODraft dataRetailCIFdraft = new DBS.Entity.RetailCIFCBODraft()
                                {
                                    TransactionID = transactionID,
                                    RequestTypeID = transactionCIF.TransactionType.TransTypeID,
                                    MaintenanceTypeID = transactionCIF.MaintenanceType.ID,

                                    //transactionCIF.RetailCIFCBO.GroupCheckBox
                                    IsNameMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsNameMaintenance,
                                    IsIdentityTypeMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsIdentityTypeMaintenance,
                                    IsNPWPMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsNPWPMaintenance,
                                    IsMaritalStatusMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsMaritalStatusMaintenance,
                                    IsCorrespondenceMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsCorrespondenceMaintenance,
                                    IsIdentityAddressMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsIdentityAddressMaintenance,
                                    IsOfficeAddressMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsOfficeAddressMaintenance,
                                    IsCorrespondenseAddressMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsCorrespondenseAddressMaintenance,
                                    IsPhoneFaxEmailMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsPhoneFaxEmailMaintenance,
                                    IsNationalityMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsNationalityMaintenance,
                                    IsFundSourceMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsFundSourceMaintenance,
                                    IsNetAssetMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsNetAssetMaintenance,
                                    IsMonthlyIncomeMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsMonthlyIncomeMaintenance,
                                    IsJobMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsJobMaintenance,
                                    IsAccountPurposeMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsAccountPurposeMaintenance,
                                    IsMonthlyTransactionMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsMonthlyTransactionMaintenance,
                                    IsPekerjaanProfesional = transactionCIF.RetailCIFCBO.GroupCheckBox.IsPekerjaanProfesional,
                                    IsPekerjaanLainnya = transactionCIF.RetailCIFCBO.GroupCheckBox.IsPekerjaanLainnya,
                                    IsSumberDanaLainnya = transactionCIF.RetailCIFCBO.GroupCheckBox.IsSumberDanaLainnya,
                                    IsTujuanBukaRekeningLainnya = transactionCIF.RetailCIFCBO.GroupCheckBox.IsTujuanBukaRekeningLainnya,
                                    IsOthers = transactionCIF.RetailCIFCBO.GroupCheckBox.IsOthers,

                                    //transactionCIF.RetailCIFCBO                              
                                    AccountNumber = transactionCIF.RetailCIFCBO.AccountNumber,
                                    Name = transactionCIF.RetailCIFCBO.Name,
                                    IdentityTypeID = transactionCIF.RetailCIFCBO.IdentityTypeID.ID,
                                    IdentityNumber = transactionCIF.RetailCIFCBO.IdentityNumber,
                                    IdentityStartDate = transactionCIF.RetailCIFCBO.IdentityStartDate,
                                    IdentityEndDate = transactionCIF.RetailCIFCBO.IdentityEndDate,
                                    IdentityAddress = transactionCIF.RetailCIFCBO.IdentityAddress,
                                    IdentityKelurahan = transactionCIF.RetailCIFCBO.IdentityKelurahan,
                                    IdentityKecamatan = transactionCIF.RetailCIFCBO.IdentityKecamatan,
                                    IdentityCity = transactionCIF.RetailCIFCBO.IdentityCity,
                                    IdentityProvince = transactionCIF.RetailCIFCBO.IdentityProvince,
                                    IdentityCountry = transactionCIF.RetailCIFCBO.IdentityCountry,
                                    IdentityPostalCode = transactionCIF.RetailCIFCBO.IdentityPostalCode,

                                    IdentityTypeID2 = transactionCIF.RetailCIFCBO.IdentityTypeID2.ID,
                                    IdentityNumber2 = transactionCIF.RetailCIFCBO.IdentityNumber2,
                                    IdentityStartDate2 = transactionCIF.RetailCIFCBO.IdentityStartDate2,
                                    IdentityEndDate2 = transactionCIF.RetailCIFCBO.IdentityEndDate2,
                                    IdentityAddress2 = transactionCIF.RetailCIFCBO.IdentityAddress2,
                                    IdentityKelurahan2 = transactionCIF.RetailCIFCBO.IdentityKelurahan2,
                                    IdentityKecamatan2 = transactionCIF.RetailCIFCBO.IdentityKecamatan2,
                                    IdentityCity2 = transactionCIF.RetailCIFCBO.IdentityCity2,
                                    IdentityProvince2 = transactionCIF.RetailCIFCBO.IdentityProvince2,
                                    IdentityCountry2 = transactionCIF.RetailCIFCBO.IdentityCountry2,
                                    IdentityPostalCode2 = transactionCIF.RetailCIFCBO.IdentityPostalCode2,

                                    IdentityTypeID3 = transactionCIF.RetailCIFCBO.IdentityTypeID3.ID,
                                    IdentityNumber3 = transactionCIF.RetailCIFCBO.IdentityNumber3,
                                    IdentityStartDate3 = transactionCIF.RetailCIFCBO.IdentityStartDate3,
                                    IdentityEndDate3 = transactionCIF.RetailCIFCBO.IdentityEndDate3,
                                    IdentityAddress3 = transactionCIF.RetailCIFCBO.IdentityAddress3,
                                    IdentityKelurahan3 = transactionCIF.RetailCIFCBO.IdentityKelurahan3,
                                    IdentityKecamatan3 = transactionCIF.RetailCIFCBO.IdentityKecamatan3,
                                    IdentityCity3 = transactionCIF.RetailCIFCBO.IdentityCity3,
                                    IdentityProvince3 = transactionCIF.RetailCIFCBO.IdentityProvince3,
                                    IdentityCountry3 = transactionCIF.RetailCIFCBO.IdentityCountry3,
                                    IdentityPostalCode3 = transactionCIF.RetailCIFCBO.IdentityPostalCode3,

                                    IdentityTypeID4 = transactionCIF.RetailCIFCBO.IdentityTypeID4.ID,
                                    IdentityNumber4 = transactionCIF.RetailCIFCBO.IdentityNumber4,
                                    IdentityStartDate4 = transactionCIF.RetailCIFCBO.IdentityStartDate4,
                                    IdentityEndDate4 = transactionCIF.RetailCIFCBO.IdentityEndDate4,
                                    IdentityAddress4 = transactionCIF.RetailCIFCBO.IdentityAddress4,
                                    IdentityKelurahan4 = transactionCIF.RetailCIFCBO.IdentityKelurahan4,
                                    IdentityKecamatan4 = transactionCIF.RetailCIFCBO.IdentityKecamatan4,
                                    IdentityCity4 = transactionCIF.RetailCIFCBO.IdentityCity4,
                                    IdentityProvince4 = transactionCIF.RetailCIFCBO.IdentityProvince4,
                                    IdentityCountry4 = transactionCIF.RetailCIFCBO.IdentityCountry4,
                                    IdentityPostalCode4 = transactionCIF.RetailCIFCBO.IdentityPostalCode4,

                                    NPWPNumber = transactionCIF.RetailCIFCBO.NPWPNumber,
                                    IsNPWPReceived = transactionCIF.RetailCIFCBO.IsNPWPReceived,
                                    SpouseName = transactionCIF.RetailCIFCBO.SpouseName,
                                    IsCorrespondenseToEmail = transactionCIF.RetailCIFCBO.IsCorrespondenseToEmail,
                                    CorrespondenseAddress = transactionCIF.RetailCIFCBO.CorrespondenseAddress,
                                    CorrespondenseKelurahan = transactionCIF.RetailCIFCBO.CorrespondenseKelurahan,
                                    CorrespondenseKecamatan = transactionCIF.RetailCIFCBO.CorrespondenseKecamatan,
                                    CorrespondenseCity = transactionCIF.RetailCIFCBO.CorrespondenseCity,
                                    CorrespondenseProvince = transactionCIF.RetailCIFCBO.CorrespondenseProvince,
                                    CorrespondenseCountry = transactionCIF.RetailCIFCBO.CorrespondenseCountry,
                                    CorrespondensePostalCode = transactionCIF.RetailCIFCBO.CorrespondensePostalCode,
                                    CellPhoneMethodID = transactionCIF.RetailCIFCBO.CellPhoneMethodID.ID,
                                    CellPhone = transactionCIF.RetailCIFCBO.CellPhone,
                                    UpdatedCellPhone = transactionCIF.RetailCIFCBO.UpdatedCellPhone,
                                    HomePhoneMethodID = transactionCIF.RetailCIFCBO.HomePhoneMethodID.ID,
                                    HomePhone = transactionCIF.RetailCIFCBO.HomePhone,
                                    UpdatedHomePhone = transactionCIF.RetailCIFCBO.UpdatedHomePhone,
                                    OfficePhoneMethodID = transactionCIF.RetailCIFCBO.OfficePhoneMethodID.ID,
                                    OfficePhone = transactionCIF.RetailCIFCBO.OfficePhone,
                                    UpdatedOfficePhone = transactionCIF.RetailCIFCBO.UpdatedOfficePhone,
                                    FaxMethodID = transactionCIF.RetailCIFCBO.FaxMethodID.ID,
                                    Fax = transactionCIF.RetailCIFCBO.Fax,
                                    UpdatedFax = transactionCIF.RetailCIFCBO.UpdatedFax,
                                    EmailAddress = transactionCIF.RetailCIFCBO.EmailAddress,
                                    OfficeAddress = transactionCIF.RetailCIFCBO.OfficeAddress,
                                    OfficeKelurahan = transactionCIF.RetailCIFCBO.OfficeKelurahan,
                                    OfficeKecamatan = transactionCIF.RetailCIFCBO.OfficeKecamatan,
                                    OfficeCity = transactionCIF.RetailCIFCBO.OfficeCity,
                                    OfficeProvince = transactionCIF.RetailCIFCBO.OfficeProvince,
                                    OfficeCountry = transactionCIF.RetailCIFCBO.OfficeCountry,
                                    OfficePostalCode = transactionCIF.RetailCIFCBO.OfficePostalCode,
                                    Nationality = transactionCIF.RetailCIFCBO.Nationality,
                                    UBOIdentityType = transactionCIF.RetailCIFCBO.UBOIdentityType,
                                    UBOPhone = transactionCIF.RetailCIFCBO.UBOPhone,
                                    UBOJob = transactionCIF.RetailCIFCBO.UBOJob,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName,
                                    AtmNumber = transactionCIF.RetailCIFCBO.AtmNumber,
                                    PekerjaanProfesional = string.IsNullOrEmpty(transactionCIF.RetailCIFCBO.PekerjaanProfesional) ? "" : transactionCIF.RetailCIFCBO.PekerjaanProfesional,
                                    PekerjaanLainnya = string.IsNullOrEmpty(transactionCIF.RetailCIFCBO.PekerjaanLainnya) ? "" : transactionCIF.RetailCIFCBO.PekerjaanLainnya,
                                    TujuanBukaRekeningLainnya = string.IsNullOrEmpty(transactionCIF.RetailCIFCBO.TujuanBukaRekeningLainnya) ? "" : transactionCIF.RetailCIFCBO.TujuanBukaRekeningLainnya,
                                    SumberDanaLainnya = string.IsNullOrEmpty(transactionCIF.RetailCIFCBO.SumberDanaLainnya) ? "" : transactionCIF.RetailCIFCBO.SumberDanaLainnya,


                                    //Parameter System for pengkinian data
                                    MaritalStatusID = transactionCIF.RetailCIFCBO.MaritalStatusID.ID,
                                    // FundSource = transactionCIF.RetailCIFCBO.FundSource.ID,
                                    NetAsset = transactionCIF.RetailCIFCBO.NetAsset.ID,
                                    MonthlyIncome = transactionCIF.RetailCIFCBO.MonthlyIncome.ID,
                                    MonthlyExtraIncome = transactionCIF.RetailCIFCBO.MonthlyExtraIncome.ID,
                                    Job = transactionCIF.RetailCIFCBO.Job.ID,
                                    AccountPurpose = transactionCIF.RetailCIFCBO.AccountPurpose.ID,
                                    IncomeForecast = transactionCIF.RetailCIFCBO.IncomeForecast.ID,
                                    OutcomeForecast = transactionCIF.RetailCIFCBO.OutcomeForecast.ID,
                                    TransactionForecast = transactionCIF.RetailCIFCBO.TransactionForecast.ID,
                                    DispatchModeTypeID = transactionCIF.RetailCIFCBO.DispatchModeType.ID,
                                    DispatchModeOther = transactionCIF.RetailCIFCBO.DispatchModeOther,
                                    IsPenawaranProductPerbankan = transactionCIF.RetailCIFCBO.IsPenawaranProductPerbankan,

                                    //Field-field feed from Risk Rating Form
                                    RiskRatingReportDate = transactionCIF.RetailCIFCBO.ReportDate,
                                    RiskRatingNextReviewDate = transactionCIF.RetailCIFCBO.NextReviewDate,
                                    RiskRatingResultID = transactionCIF.RetailCIFCBO.RiskRatingResult.ID,
                                    CompanyName = transactionCIF.RetailCIFCBO.CompanyName,
                                    Position = transactionCIF.RetailCIFCBO.Position,
                                    WorkPeriod = transactionCIF.RetailCIFCBO.WorkPeriod,
                                    IndustryType = transactionCIF.RetailCIFCBO.IndustryType,
                                    HubunganNasabah = transactionCIF.RetailCIFCBO.HubunganNasabah,
                                    FundSource = transactionCIF.RetailCIFCBO.FundSource.ID,

                                    IsMotherMaiden = transactionCIF.RetailCIFCBO.GroupCheckBox.IsMotherMaiden,
                                    IsEducation = transactionCIF.RetailCIFCBO.GroupCheckBox.IsEducation,
                                    IsReligion = transactionCIF.RetailCIFCBO.GroupCheckBox.IsReligion,
                                    IsDataUBO = transactionCIF.RetailCIFCBO.GroupCheckBox.IsDataUBO,
                                    IsTransparansiDataPenawaranProduct = transactionCIF.RetailCIFCBO.GroupCheckBox.IsTransparansiDataPenawaranProduct,
                                    IsTransparansiPenggunaan = transactionCIF.RetailCIFCBO.GroupCheckBox.IsTransparansiPenggunaan,
                                    IsPenawaranProduct = transactionCIF.RetailCIFCBO.GroupCheckBox.IsPenawaranProduct,
                                    IsStampDuty = transactionCIF.RetailCIFCBO.GroupCheckBox.IsStampDuty,
                                    IsSignature = transactionCIF.RetailCIFCBO.GroupCheckBox.IsSignature,
                                    Education = transactionCIF.RetailCIFCBO.Education,
                                    Religion = transactionCIF.RetailCIFCBO.Religion,
                                    MotherMaiden = transactionCIF.RetailCIFCBO.MotherMaiden,
                                    CIFUBO = transactionCIF.RetailCIFCBO.DataUBOCif,
                                    UBOName = transactionCIF.RetailCIFCBO.DataUBOName,
                                    UBORelationship = transactionCIF.RetailCIFCBO.DataUBORelationship,
                                    IsSLIK = transactionCIF.RetailCIFCBO.GroupCheckBox.IsSLIK,
                                    GrosIncomeccy = transactionCIF.RetailCIFCBO.GrosIncomeccy,
                                    GrossIncomeAmount = transactionCIF.RetailCIFCBO.GrossIncomeAmount,
                                    GrossCifSpouse = transactionCIF.RetailCIFCBO.GrossCifSpouse,
                                    GrossSpouseName = transactionCIF.RetailCIFCBO.GrossSpouseName,
                                    GrossSpouseRelation = transactionCIF.RetailCIFCBO.GrossSpouseRelation,
                                    IsChangeSignature = transactionCIF.RetailCIFCBO.GroupCheckBox.IsChangeSignature,
                                    ChangeSignature = transactionCIF.RetailCIFCBO.GroupCheckBox.ChangeSignature,
                                    FatcaCountryCode = transactionCIF.RetailCIFCBO.FatcaCountryCode,
                                    FatcaReviewStatus = transactionCIF.RetailCIFCBO.FatcaReviewStatus,
                                    FatcaCRSStatus = transactionCIF.RetailCIFCBO.FatcaCRSStatus,
                                    FatcaTaxPayer = transactionCIF.RetailCIFCBO.FatcaTaxPayer,
                                    FatcaWithHolding = transactionCIF.RetailCIFCBO.FatcaWithHolding,
                                    FatcaDateOnForm = transactionCIF.RetailCIFCBO.FatcaDateOnForm,
                                    FatcaReviewStatusDate = transactionCIF.RetailCIFCBO.FatcaReviewStatusDate,
                                    FatcaTaxPayerID = transactionCIF.RetailCIFCBO.FatcaTaxPayerID,
                                    IsFatcaCRS = transactionCIF.RetailCIFCBO.GroupCheckBox.IsFatcaCRS,
                                    AmountLienUnlien = transactionCIF.RetailCIFCBO.AmountLienUnlien,
                                    Tier = transactionCIF.RetailCIFCBO.Tier
                                };
                                context_ts.RetailCIFCBODrafts.Add(dataRetailCIFdraft);
                                context_ts.SaveChanges();
                                transactionID = data.TransactionID;
                                TrIDInserted = transactionID;
                                RetailCIFCBOID = dataRetailCIFdraft.RetailCIFCBODraftID;
                                #endregion

                                #region RetailCIFCBOContact dayat Draft
                                DBS.Entity.RetailCIFCBOContactDraft dataRetailCIFContactDraft = new DBS.Entity.RetailCIFCBOContactDraft();
                                //selular
                                #region Selular Phone Number
                                if (transactionCIF.RetailCIFCBO.SelularPhoneNumbers != null && transactionCIF.RetailCIFCBO.SelularPhoneNumbers.Count > 0)
                                {
                                    foreach (var item in transactionCIF.RetailCIFCBO.SelularPhoneNumbers)
                                    {
                                        if (item.ActionType != "" && item.PhoneNumber != "")
                                        {
                                            dataRetailCIFContactDraft.RetailCIFCBODraftID = dataRetailCIFdraft.RetailCIFCBODraftID;
                                            dataRetailCIFContactDraft.ActionType = item.ActionType;
                                            dataRetailCIFContactDraft.ContactType = item.ContactType;
                                            dataRetailCIFContactDraft.PhoneNumber = item.PhoneNumber;
                                            dataRetailCIFContactDraft.CityCode = item.CityCode;
                                            dataRetailCIFContactDraft.CountryCode = item.CountryCode;
                                            dataRetailCIFContactDraft.CreatedDate = DateTime.UtcNow;
                                            dataRetailCIFContactDraft.UpdateCountryCode = null;
                                            dataRetailCIFContactDraft.UpdateCityCode = null;
                                            dataRetailCIFContactDraft.UpdatePhoneNumber = null;
                                            dataRetailCIFContactDraft.RetailCIFCBOContactUpdateID = null;
                                            dataRetailCIFContactDraft.CreatedBy = currentUser.GetCurrentUser().LoginName;
                                            if (item.ActionType == "2")
                                            {
                                                dataRetailCIFContactDraft.UpdateCountryCode = item.UpdateCountryCode;
                                                dataRetailCIFContactDraft.UpdateCityCode = item.UpdateCityCode;
                                                dataRetailCIFContactDraft.UpdatePhoneNumber = item.UpdatePhoneNumber;
                                                dataRetailCIFContactDraft.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                            }
                                            if (item.ActionType == "3")
                                            {
                                                dataRetailCIFContactDraft.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                            }
                                            context_ts.RetailCIFCBOContactDrafts.Add(dataRetailCIFContactDraft);
                                            context_ts.SaveChanges();
                                        }
                                    }
                                }
                                #endregion
                                //oofice
                                #region Office Phone Number
                                if (transactionCIF.RetailCIFCBO.OfficePhoneNumbers != null && transactionCIF.RetailCIFCBO.OfficePhoneNumbers.Count > 0)
                                {
                                    foreach (var item in transactionCIF.RetailCIFCBO.OfficePhoneNumbers)
                                    {
                                        if (item.ActionType != "" && item.PhoneNumber != "")
                                        {
                                            dataRetailCIFContactDraft.RetailCIFCBODraftID = dataRetailCIFdraft.RetailCIFCBODraftID;
                                            dataRetailCIFContactDraft.ActionType = item.ActionType;
                                            dataRetailCIFContactDraft.ContactType = item.ContactType;
                                            dataRetailCIFContactDraft.PhoneNumber = item.PhoneNumber;
                                            dataRetailCIFContactDraft.CityCode = item.CityCode;
                                            dataRetailCIFContactDraft.CountryCode = item.CountryCode;
                                            dataRetailCIFContactDraft.CreatedDate = DateTime.UtcNow;
                                            dataRetailCIFContactDraft.UpdateCountryCode = null;
                                            dataRetailCIFContactDraft.UpdateCityCode = null;
                                            dataRetailCIFContactDraft.UpdatePhoneNumber = null;
                                            dataRetailCIFContactDraft.RetailCIFCBOContactUpdateID = null;
                                            dataRetailCIFContactDraft.CreatedBy = currentUser.GetCurrentUser().LoginName;
                                            if (item.ActionType == "2")
                                            {
                                                dataRetailCIFContactDraft.UpdateCountryCode = item.UpdateCountryCode;
                                                dataRetailCIFContactDraft.UpdateCityCode = item.UpdateCityCode;
                                                dataRetailCIFContactDraft.UpdatePhoneNumber = item.UpdatePhoneNumber;
                                                dataRetailCIFContactDraft.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                            }
                                            if (item.ActionType == "3")
                                            {
                                                dataRetailCIFContactDraft.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                            }
                                            context_ts.RetailCIFCBOContactDrafts.Add(dataRetailCIFContactDraft);
                                            context_ts.SaveChanges();
                                        }
                                    }
                                }
                                #endregion
                                //Home
                                #region Home Phone Numbers
                                if (transactionCIF.RetailCIFCBO.HomePhoneNumbers != null && transactionCIF.RetailCIFCBO.HomePhoneNumbers.Count > 0)
                                {
                                    foreach (var item in transactionCIF.RetailCIFCBO.HomePhoneNumbers)
                                    {
                                        if (item.ActionType != "" && item.PhoneNumber != "")
                                        {
                                            dataRetailCIFContactDraft.RetailCIFCBODraftID = dataRetailCIFdraft.RetailCIFCBODraftID;
                                            dataRetailCIFContactDraft.ActionType = item.ActionType;
                                            dataRetailCIFContactDraft.ContactType = item.ContactType;
                                            dataRetailCIFContactDraft.PhoneNumber = item.PhoneNumber;
                                            dataRetailCIFContactDraft.CityCode = item.CityCode;
                                            dataRetailCIFContactDraft.CountryCode = item.CountryCode;
                                            dataRetailCIFContactDraft.CreatedDate = DateTime.UtcNow;
                                            dataRetailCIFContactDraft.UpdateCountryCode = null;
                                            dataRetailCIFContactDraft.UpdateCityCode = null;
                                            dataRetailCIFContactDraft.UpdatePhoneNumber = null;
                                            dataRetailCIFContactDraft.RetailCIFCBOContactUpdateID = null;
                                            dataRetailCIFContactDraft.CreatedBy = currentUser.GetCurrentUser().LoginName;
                                            if (item.ActionType == "2")
                                            {
                                                dataRetailCIFContactDraft.UpdateCountryCode = item.UpdateCountryCode;
                                                dataRetailCIFContactDraft.UpdateCityCode = item.UpdateCityCode;
                                                dataRetailCIFContactDraft.UpdatePhoneNumber = item.UpdatePhoneNumber;
                                                dataRetailCIFContactDraft.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                            }
                                            if (item.ActionType == "3")
                                            {
                                                dataRetailCIFContactDraft.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                            }
                                            context_ts.RetailCIFCBOContactDrafts.Add(dataRetailCIFContactDraft);
                                            context_ts.SaveChanges();
                                        }
                                    }
                                }
                                #endregion
                                #endregion

                                #region Customer Join
                                //save into table RetailCIFCBOAccountJoin
                                if (transactionCIF.AddJoinTableCustomerCIF != null)
                                {
                                    if (transactionCIF.AddJoinTableCustomerCIF.Count > 0)
                                    {
                                        foreach (var item in transactionCIF.AddJoinTableCustomerCIF)
                                        {
                                            Entity.RetailCIFCBOAccountJoinDraft dataRetailCIFCBOAccountJoinDraft = new Entity.RetailCIFCBOAccountJoinDraft()
                                            {
                                                TransactionID = transactionID,
                                                CIF = item.CIF,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName
                                            };
                                            context_ts.RetailCIFCBOAccountJoinDrafts.Add(dataRetailCIFCBOAccountJoinDraft);
                                        }
                                        context_ts.SaveChanges();
                                    }
                                }
                                #endregion

                                #region FFD Account
                                //save into table RetailCIFCBOAccNo for link to FFD Account
                                if (transactionCIF.AddJoinTableFFDAcountCIF != null)
                                {
                                    if (transactionCIF.AddJoinTableFFDAcountCIF.Count > 0)
                                    {
                                        //var SelectedFFD = transactionCIF.AddJoinTableFFDAcountCIF.Where(s => s.IsAddFFDAccount == true);
                                        foreach (var item in transactionCIF.AddJoinTableFFDAcountCIF)//SelectedFFD)
                                        {
                                            Entity.RetailCIFCBOAccNoDraft dataRetailCIFCBOAccNoDraft = new Entity.RetailCIFCBOAccNoDraft()
                                            {
                                                RetailCIFCBODraftID = RetailCIFCBOID,
                                                AccountNumber = item.AccountNumber,
                                                IsFFD = item.IsAddFFDAccount,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName
                                            };
                                            context_ts.RetailCIFCBOAccNoDrafts.Add(dataRetailCIFCBOAccNoDraft);
                                        }
                                        context_ts.SaveChanges();
                                    }
                                }
                                #endregion

                                #region Account Join
                                //save into table RetailCIFCBOAccNo for Add Account
                                if (transactionCIF.AddJoinTableAccountCIF != null)
                                {
                                    if (transactionCIF.AddJoinTableAccountCIF.Count > 0)
                                    {
                                        //var SelectedFFD = transactionCIF.AddJoinTableAccountCIF.Where(s => s.IsAddFFDAccount == true);
                                        foreach (var item in transactionCIF.AddJoinTableAccountCIF) //SelectedFFD)
                                        {
                                            Entity.RetailCIFCBOAccNoDraft dataRetailCIFCBOAccNoDraft1 = new Entity.RetailCIFCBOAccNoDraft()
                                            {
                                                RetailCIFCBODraftID = RetailCIFCBOID,
                                                AccountNumber = item.AccountNumber,
                                                CurrencyID = item.Currency.ID,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName
                                            };
                                            context_ts.RetailCIFCBOAccNoDrafts.Add(dataRetailCIFCBOAccNoDraft1);
                                        }
                                        context_ts.SaveChanges();
                                    }
                                }
                                #endregion

                                #region Join Account
                                //save into table RetailCIFCBOAccNo for Dormant Account
                                if (transactionCIF.AddJoinTableDormantCIF != null)
                                {
                                    if (transactionCIF.AddJoinTableDormantCIF.Count > 0)
                                    {
                                        var SelectedDormant = transactionCIF.AddJoinTableDormantCIF.Where(s => s.IsAddDormantAccount == true);
                                        foreach (var item in SelectedDormant)
                                        {
                                            Entity.RetailCIFCBOAccNoDraft dataRetailCIFCBOAccNoDraft2 = new Entity.RetailCIFCBOAccNoDraft()
                                            {
                                                RetailCIFCBODraftID = RetailCIFCBOID,
                                                AccountNumber = item.AccountNumber,
                                                CurrencyID = item.Currency.ID,
                                                //IsFFD = item.IsAddFFDAccount,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName
                                            };
                                            context_ts.RetailCIFCBOAccNoDrafts.Add(dataRetailCIFCBOAccNoDraft2);
                                        }
                                        context_ts.SaveChanges();
                                    }
                                }
                                #endregion

                                #region Freeze Account
                                //save into table RetailCIFCBOFreezeAccount for Freeze Unfreeze Account
                                if (transactionCIF.AddJoinTableFreezeUnfreezeCIF != null)
                                {
                                    if (transactionCIF.AddJoinTableFreezeUnfreezeCIF.Count > 0)
                                    {
                                        var SelectedFreeze = transactionCIF.AddJoinTableFreezeUnfreezeCIF.Where(s => s.IsAddTblFreezeAccount == true);
                                        foreach (var item in SelectedFreeze)
                                        {
                                            Entity.RetailCIFCBOFreezeAccountDraft dataRetailCIFCBOFreezeDraft = new Entity.RetailCIFCBOFreezeAccountDraft()
                                            {
                                                RetailCIFCBODraftID = RetailCIFCBOID,
                                                AccountNumber = item.AccountNumber,
                                                IsFreeze = item.IsFreezeAccount,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName
                                            };
                                            context_ts.RetailCIFCBOFreezeAccountDrafts.Add(dataRetailCIFCBOFreezeDraft);
                                        }
                                        context_ts.SaveChanges();
                                    }
                                    context_ts.SaveChanges();
                                }
                                #endregion
                                #region Save Document
                                var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(transactionCIF.ID)).ToList();
                                if (existingDocs != null)
                                {
                                    if (existingDocs.Count() > 0)
                                    {
                                        foreach (var item in existingDocs)
                                        {
                                            var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                            deleteDoc.IsDeleted = true;
                                            context.SaveChanges();
                                        }
                                    }
                                }

                                if (transactionCIF.DocumentsCIF != null)
                                {
                                    if (transactionCIF.DocumentsCIF.Count > 0)
                                    {
                                        foreach (var item in transactionCIF.DocumentsCIF)
                                        {
                                            Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                                            {
                                                TransactionID = data.TransactionID,
                                                DocTypeID = item.Type.ID,
                                                PurposeID = item.Purpose.ID,
                                                Filename = item.FileName,
                                                DocumentPath = item.DocumentPath,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName
                                            };
                                            context_ts.TransactionDocumentDrafts.Add(document);
                                        }
                                        context_ts.SaveChanges();
                                    }
                                }
                                #endregion
                                #region LienUnlien
                                if (transactionCIF.AccountNumberLienUnlien != null)
                                {
                                    if (transactionCIF.AccountNumberLienUnlien.Count > 0)
                                    {
                                        IList<AccountNumberLienUnlienModel> selectedLienUnlien = transactionCIF.AccountNumberLienUnlien.Where(d => d.IsSelected == true).ToList();
                                        if (selectedLienUnlien != null)
                                        {
                                            if (selectedLienUnlien.Count > 0)
                                            {
                                                foreach (var item in selectedLienUnlien)
                                                {
                                                    Entity.LienUnlienDraft dataInsert = new LienUnlienDraft();
                                                    dataInsert.CIF = transactionCIF.Customer.CIF;
                                                    dataInsert.TransactionId = transactionID;
                                                    dataInsert.AccountNumber = item.AccountNumber;
                                                    dataInsert.LienUnlienData = (item.LienUnlien.Trim().Equals("1") ? "Lien" : (item.LienUnlien.Trim().Equals("2") ? "Un-lien" : ""));
                                                    dataInsert.IsDeleted = false;
                                                    dataInsert.CreateBy = currentUser.GetCurrentUser().LoginName;
                                                    dataInsert.CreateDate = DateTime.UtcNow;

                                                    context_ts.LienUnlienDrafts.Add(dataInsert);
                                                }
                                                context_ts.SaveChanges();
                                            }
                                        }
                                        selectedLienUnlien = null;
                                    }
                                }
                                #endregion

                                #region BringUp
                                if (transactionCIF.IsBringupTask == true)
                                {
                                    Entity.TransactionBringupTask Bringup = new Entity.TransactionBringupTask()
                                    {
                                        TransactionID = transactionID,
                                        TaskName = "Bring Up Retail CIF",
                                        AssignedTo = currentUser.GetCurrentUser().LoginName,
                                        AssignedFrom = currentUser.GetCurrentUser().LoginName,
                                        DateIn = DateTime.UtcNow,
                                        IsSubmitted = false,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName,
                                    };
                                    context_ts.TransactionBringupTasks.Add(Bringup);
                                    context_ts.SaveChanges();
                                }
                                #endregion
                                ts.Complete();
                                IsSuccess = true;
                                #endregion
                            }
                            #endregion
                        }
                        else
                        {

                            #region New Customer
                            if (transactionCIF.IsNewCustomer)
                            {
                                context_ts.Customers.Add(new Customer()
                                {
                                    CIF = transactionCIF.Customer.CIF,
                                    CustomerName = transactionCIF.Customer.Name,
                                    BizSegmentID = 1,
                                    LocationID = 1,
                                    CustomerTypeID = 1,
                                    SourceID = 1,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });
                                context_ts.SaveChanges();
                            }
                            #endregion

                            #region Submit Transaction
                            DBS.Entity.Transaction data = new DBS.Entity.Transaction()
                            {
                                #region Unused field but Not Null
                                IsSignatureVerified = false,
                                IsDormantAccount = false,
                                IsFrezeAccount = false,
                                BeneName = "-",
                                BankID = 1,
                                IsResident = false,
                                IsCitizen = false,
                                AmountUSD = 0.00M,
                                Rate = 0.00M,
                                DebitCurrencyID = 1,
                                BizSegmentID = 1,
                                ChannelID = 1,
                                #endregion

                                #region Primary Field
                                CIF = transactionCIF.Customer.CIF,
                                ProductID = transactionCIF.Product.ID,
                                TransactionID = transactionCIF.ID,
                                ApplicationID = transactionCIF.ApplicationID,
                                IsDraft = transactionCIF.IsDraft,
                                IsNewCustomer = transactionCIF.IsNewCustomer,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName,
                                ApplicationDate = DateTime.UtcNow,
                                CurrencyID = (transactionCIF.Currency.ID != 0 ? transactionCIF.Currency.ID : (int)CurrencyID.NullCurrency),
                                Amount = 0,
                                IsTopUrgent = 0,
                                IsDocumentComplete = false,
                                StateID = (int)StateID.OnProgress,
                                StaffID = transactionCIF.StaffID,
                                //Data And
                                StaffTaggingID = transactionCIF.StaffTagging.ID,
                                //End
                                ATMNumber = transactionCIF.ATMNumber,
                                IsLOI = transactionCIF.IsLOI,
                                IsPOI = transactionCIF.IsPOI,
                                IsPOA = transactionCIF.IsPOA,
                                AccountTypeLoiPoiID = transactionCIF.AccountTypeLoiPoiID,
                                LocationName = transactionCIF.BrachRiskRating.Name,
                                AccountNumber = (transactionCIF.AccountNumber == null ? null : transactionCIF.AccountNumber.AccountNumber),
                                BranchName = transactionCIF.BranchName,
                                TouchTimeStartDate = transactionCIF.CreateDate
                                #endregion
                            };
                            if (transactionCIF.TransactionType.TransTypeID == 0)
                                data.TransactionTypeID = null;
                            else
                                data.TransactionTypeID = transactionCIF.TransactionType.TransTypeID;

                            if (transactionCIF.MaintenanceType.ID == 0)
                                data.CBOMaintainID = null;
                            else
                                data.CBOMaintainID = (transactionCIF.MaintenanceType.ID == 0 ? int.Parse(null) : transactionCIF.MaintenanceType.ID);

                            if (transactionCIF.TransactionSubType.ID == 0)
                                data.TransactionSubTypeID = null;
                            else
                                data.TransactionSubTypeID = (transactionCIF.TransactionSubType.ID == 0 ? 1 : transactionCIF.TransactionSubType.ID);

                            context_ts.Transactions.Add(data);
                            context_ts.SaveChanges();
                            #endregion

                            transactionID = data.TransactionID;
                            TrIDInserted = transactionID;

                            #region CustomerAccount Join LOI POI
                            if (transactionCIF.CustomerJoinLoiPois != null)
                            {

                                foreach (var a in transactionCIF.CustomerJoinLoiPois)
                                {
                                    Entity.RetailCIFCBOCustomerJoinLoiPoi item = new Entity.RetailCIFCBOCustomerJoinLoiPoi();
                                    item.CIF = a.CIF;
                                    item.CustomerName = a.CustomerName;
                                    item.TransactionID = transactionID;
                                    item.IsDeleted = false;
                                    context_ts.RetailCIFCBOCustomerJoinLoiPois.Add(item);
                                }

                                context_ts.SaveChanges();
                            }
                            #endregion

                            #region Account List POA
                            if (transactionCIF.AccountListPOAs != null)
                            {
                                foreach (var a in transactionCIF.AccountListPOAs)
                                {
                                    Entity.RetailCIFCBOAccountListPOA item = new Entity.RetailCIFCBOAccountListPOA();
                                    item.AccountNumber = a.AccountNumber;
                                    item.CustomerName = a.CustomerName;
                                    item.CurrencyID = a.CurrencyID;
                                    item.CIF = a.CIF;
                                    item.IsJointAccount = a.IsJointAccount;
                                    item.TransactionID = transactionID;
                                    item.IsDeleted = false;
                                    context_ts.RetailCIFCBOAccountListPOAs.Add(item);
                                }

                                context_ts.SaveChanges();
                            }
                            #endregion

                            #region RetailCIFCBO
                            DBS.Entity.RetailCIFCBO dataRetailCIF = new DBS.Entity.RetailCIFCBO();

                            dataRetailCIF.TransactionID = transactionID;
                            dataRetailCIF.RequestTypeID = transactionCIF.TransactionType.TransTypeID;
                            dataRetailCIF.MaintenanceTypeID = transactionCIF.MaintenanceType.ID;

                            //transactionCIF.RetailCIFCBO.GroupCheckBox
                            dataRetailCIF.IsNameMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsNameMaintenance;
                            dataRetailCIF.IsIdentityTypeMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsIdentityTypeMaintenance;
                            dataRetailCIF.IsNPWPMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsNPWPMaintenance;
                            dataRetailCIF.IsMaritalStatusMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsMaritalStatusMaintenance;
                            dataRetailCIF.IsCorrespondenceMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsCorrespondenceMaintenance;
                            dataRetailCIF.IsIdentityAddressMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsIdentityAddressMaintenance;
                            dataRetailCIF.IsOfficeAddressMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsOfficeAddressMaintenance;
                            dataRetailCIF.IsCorrespondenseAddressMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsCorrespondenseAddressMaintenance;
                            dataRetailCIF.IsPhoneFaxEmailMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsPhoneFaxEmailMaintenance;
                            dataRetailCIF.IsNationalityMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsNationalityMaintenance;
                            dataRetailCIF.IsFundSourceMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsFundSourceMaintenance;
                            dataRetailCIF.IsNetAssetMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsNetAssetMaintenance;
                            dataRetailCIF.IsMonthlyIncomeMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsMonthlyIncomeMaintenance;
                            dataRetailCIF.IsJobMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsJobMaintenance;
                            dataRetailCIF.IsAccountPurposeMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsAccountPurposeMaintenance;
                            dataRetailCIF.IsMonthlyTransactionMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsMonthlyTransactionMaintenance;
                            dataRetailCIF.IsPekerjaanProfesional = transactionCIF.RetailCIFCBO.GroupCheckBox.IsPekerjaanProfesional;
                            dataRetailCIF.IsPekerjaanLainnya = transactionCIF.RetailCIFCBO.GroupCheckBox.IsPekerjaanLainnya;
                            dataRetailCIF.IsSumberDanaLainnya = transactionCIF.RetailCIFCBO.GroupCheckBox.IsSumberDanaLainnya;
                            dataRetailCIF.IsTujuanBukaRekeningLainnya = transactionCIF.RetailCIFCBO.GroupCheckBox.IsTujuanBukaRekeningLainnya;
                            dataRetailCIF.IsOthers = transactionCIF.RetailCIFCBO.GroupCheckBox.IsOthers;

                            //transactionCIF.RetailCIFCBO                              
                            dataRetailCIF.AccountNumber = transactionCIF.RetailCIFCBO.AccountNumber;
                            dataRetailCIF.Name = transactionCIF.RetailCIFCBO.Name;
                            dataRetailCIF.IdentityTypeID = transactionCIF.RetailCIFCBO.IdentityTypeID.ID;
                            dataRetailCIF.IdentityNumber = transactionCIF.RetailCIFCBO.IdentityNumber;
                            dataRetailCIF.IdentityStartDate = transactionCIF.RetailCIFCBO.IdentityStartDate;
                            dataRetailCIF.IdentityEndDate = transactionCIF.RetailCIFCBO.IdentityEndDate;
                            dataRetailCIF.IdentityAddress = transactionCIF.RetailCIFCBO.IdentityAddress;
                            dataRetailCIF.IdentityKelurahan = transactionCIF.RetailCIFCBO.IdentityKelurahan;
                            dataRetailCIF.IdentityKecamatan = transactionCIF.RetailCIFCBO.IdentityKecamatan;
                            dataRetailCIF.IdentityCity = transactionCIF.RetailCIFCBO.IdentityCity;
                            dataRetailCIF.IdentityProvince = transactionCIF.RetailCIFCBO.IdentityProvince;
                            dataRetailCIF.IdentityCountry = transactionCIF.RetailCIFCBO.IdentityCountry;
                            dataRetailCIF.IdentityPostalCode = transactionCIF.RetailCIFCBO.IdentityPostalCode;

                            dataRetailCIF.IdentityTypeID2 = transactionCIF.RetailCIFCBO.IdentityTypeID2.ID;
                            dataRetailCIF.IdentityNumber2 = transactionCIF.RetailCIFCBO.IdentityNumber2;
                            dataRetailCIF.IdentityStartDate2 = transactionCIF.RetailCIFCBO.IdentityStartDate2;
                            dataRetailCIF.IdentityEndDate2 = transactionCIF.RetailCIFCBO.IdentityEndDate2;
                            dataRetailCIF.IdentityAddress2 = transactionCIF.RetailCIFCBO.IdentityAddress2;
                            dataRetailCIF.IdentityKelurahan2 = transactionCIF.RetailCIFCBO.IdentityKelurahan2;
                            dataRetailCIF.IdentityKecamatan2 = transactionCIF.RetailCIFCBO.IdentityKecamatan2;
                            dataRetailCIF.IdentityCity2 = transactionCIF.RetailCIFCBO.IdentityCity2;
                            dataRetailCIF.IdentityProvince2 = transactionCIF.RetailCIFCBO.IdentityProvince2;
                            dataRetailCIF.IdentityCountry2 = transactionCIF.RetailCIFCBO.IdentityCountry2;
                            dataRetailCIF.IdentityPostalCode2 = transactionCIF.RetailCIFCBO.IdentityPostalCode2;

                            dataRetailCIF.IdentityTypeID3 = transactionCIF.RetailCIFCBO.IdentityTypeID3.ID;
                            dataRetailCIF.IdentityNumber3 = transactionCIF.RetailCIFCBO.IdentityNumber3;
                            dataRetailCIF.IdentityStartDate3 = transactionCIF.RetailCIFCBO.IdentityStartDate3;
                            dataRetailCIF.IdentityEndDate3 = transactionCIF.RetailCIFCBO.IdentityEndDate3;
                            dataRetailCIF.IdentityAddress3 = transactionCIF.RetailCIFCBO.IdentityAddress3;
                            dataRetailCIF.IdentityKelurahan3 = transactionCIF.RetailCIFCBO.IdentityKelurahan3;
                            dataRetailCIF.IdentityKecamatan3 = transactionCIF.RetailCIFCBO.IdentityKecamatan3;
                            dataRetailCIF.IdentityCity3 = transactionCIF.RetailCIFCBO.IdentityCity3;
                            dataRetailCIF.IdentityProvince3 = transactionCIF.RetailCIFCBO.IdentityProvince3;
                            dataRetailCIF.IdentityCountry3 = transactionCIF.RetailCIFCBO.IdentityCountry3;
                            dataRetailCIF.IdentityPostalCode3 = transactionCIF.RetailCIFCBO.IdentityPostalCode3;

                            dataRetailCIF.IdentityTypeID4 = transactionCIF.RetailCIFCBO.IdentityTypeID4.ID;
                            dataRetailCIF.IdentityNumber4 = transactionCIF.RetailCIFCBO.IdentityNumber4;
                            dataRetailCIF.IdentityStartDate4 = transactionCIF.RetailCIFCBO.IdentityStartDate4;
                            dataRetailCIF.IdentityEndDate4 = transactionCIF.RetailCIFCBO.IdentityEndDate4;
                            dataRetailCIF.IdentityAddress4 = transactionCIF.RetailCIFCBO.IdentityAddress4;
                            dataRetailCIF.IdentityKelurahan4 = transactionCIF.RetailCIFCBO.IdentityKelurahan4;
                            dataRetailCIF.IdentityKecamatan4 = transactionCIF.RetailCIFCBO.IdentityKecamatan4;
                            dataRetailCIF.IdentityCity4 = transactionCIF.RetailCIFCBO.IdentityCity4;
                            dataRetailCIF.IdentityProvince4 = transactionCIF.RetailCIFCBO.IdentityProvince4;
                            dataRetailCIF.IdentityCountry4 = transactionCIF.RetailCIFCBO.IdentityCountry4;
                            dataRetailCIF.IdentityPostalCode4 = transactionCIF.RetailCIFCBO.IdentityPostalCode4;

                            dataRetailCIF.NPWPNumber = transactionCIF.RetailCIFCBO.NPWPNumber;
                            dataRetailCIF.IsNPWPReceived = transactionCIF.RetailCIFCBO.IsNPWPReceived;
                            dataRetailCIF.SpouseName = transactionCIF.RetailCIFCBO.SpouseName;
                            dataRetailCIF.IsCorrespondenseToEmail = transactionCIF.RetailCIFCBO.IsCorrespondenseToEmail;
                            dataRetailCIF.CorrespondenseAddress = transactionCIF.RetailCIFCBO.CorrespondenseAddress;
                            dataRetailCIF.CorrespondenseKelurahan = transactionCIF.RetailCIFCBO.CorrespondenseKelurahan;
                            dataRetailCIF.CorrespondenseKecamatan = transactionCIF.RetailCIFCBO.CorrespondenseKecamatan;
                            dataRetailCIF.CorrespondenseCity = transactionCIF.RetailCIFCBO.CorrespondenseCity;
                            dataRetailCIF.CorrespondenseProvince = transactionCIF.RetailCIFCBO.CorrespondenseProvince;
                            dataRetailCIF.CorrespondenseCountry = transactionCIF.RetailCIFCBO.CorrespondenseCountry;
                            dataRetailCIF.CorrespondensePostalCode = transactionCIF.RetailCIFCBO.CorrespondensePostalCode;
                            if (transactionCIF.RetailCIFCBO.CellPhoneMethodID != null)
                            {
                                dataRetailCIF.CellPhoneMethodID = transactionCIF.RetailCIFCBO.CellPhoneMethodID.ID;
                                dataRetailCIF.CellPhone = transactionCIF.RetailCIFCBO.CellPhone;
                            }
                            dataRetailCIF.UpdatedCellPhone = transactionCIF.RetailCIFCBO.UpdatedCellPhone;
                            if (transactionCIF.RetailCIFCBO.HomePhoneMethodID != null)
                            {
                                dataRetailCIF.HomePhoneMethodID = transactionCIF.RetailCIFCBO.HomePhoneMethodID.ID;
                                dataRetailCIF.HomePhone = transactionCIF.RetailCIFCBO.HomePhone;
                            }
                            dataRetailCIF.UpdatedHomePhone = transactionCIF.RetailCIFCBO.UpdatedHomePhone;
                            if (transactionCIF.RetailCIFCBO.OfficePhoneMethodID != null)
                            {
                                dataRetailCIF.OfficePhoneMethodID = transactionCIF.RetailCIFCBO.OfficePhoneMethodID.ID;
                                dataRetailCIF.OfficePhone = transactionCIF.RetailCIFCBO.OfficePhone;
                            }
                            dataRetailCIF.UpdatedOfficePhone = transactionCIF.RetailCIFCBO.UpdatedOfficePhone;
                            if (transactionCIF.RetailCIFCBO.FaxMethodID != null)
                            {
                                dataRetailCIF.FaxMethodID = (!string.IsNullOrEmpty(transactionCIF.RetailCIFCBO.Fax) && transactionCIF.RetailCIFCBO.FaxMethodID.ID > 0) ? transactionCIF.RetailCIFCBO.FaxMethodID.ID : 0;
                                dataRetailCIF.Fax = transactionCIF.RetailCIFCBO.Fax;
                            }
                            dataRetailCIF.UpdatedFax = transactionCIF.RetailCIFCBO.UpdatedFax;
                            dataRetailCIF.EmailAddress = transactionCIF.RetailCIFCBO.EmailAddress;
                            dataRetailCIF.OfficeAddress = transactionCIF.RetailCIFCBO.OfficeAddress;
                            dataRetailCIF.OfficeKelurahan = transactionCIF.RetailCIFCBO.OfficeKelurahan;
                            dataRetailCIF.OfficeKecamatan = transactionCIF.RetailCIFCBO.OfficeKecamatan;
                            dataRetailCIF.OfficeCity = transactionCIF.RetailCIFCBO.OfficeCity;
                            dataRetailCIF.OfficeProvince = transactionCIF.RetailCIFCBO.OfficeProvince;
                            dataRetailCIF.OfficeCountry = transactionCIF.RetailCIFCBO.OfficeCountry;
                            dataRetailCIF.OfficePostalCode = transactionCIF.RetailCIFCBO.OfficePostalCode;
                            dataRetailCIF.Nationality = transactionCIF.RetailCIFCBO.Nationality;
                            dataRetailCIF.UBOName = transactionCIF.RetailCIFCBO.UBOName;
                            dataRetailCIF.UBOIdentityType = transactionCIF.RetailCIFCBO.UBOIdentityType;
                            dataRetailCIF.UBOPhone = transactionCIF.RetailCIFCBO.UBOPhone;
                            dataRetailCIF.UBOJob = transactionCIF.RetailCIFCBO.UBOJob;
                            dataRetailCIF.CreateDate = DateTime.UtcNow;
                            dataRetailCIF.CreateBy = currentUser.GetCurrentUser().LoginName;
                            dataRetailCIF.AtmNumber = transactionCIF.RetailCIFCBO.AtmNumber;
                            dataRetailCIF.PekerjaanProfesional = string.IsNullOrEmpty(transactionCIF.RetailCIFCBO.PekerjaanProfesional) ? "" : transactionCIF.RetailCIFCBO.PekerjaanProfesional;
                            dataRetailCIF.PekerjaanLainnya = string.IsNullOrEmpty(transactionCIF.RetailCIFCBO.PekerjaanLainnya) ? "" : transactionCIF.RetailCIFCBO.PekerjaanLainnya;
                            dataRetailCIF.TujuanBukaRekeningLainnya = string.IsNullOrEmpty(transactionCIF.RetailCIFCBO.TujuanBukaRekeningLainnya) ? "" : transactionCIF.RetailCIFCBO.TujuanBukaRekeningLainnya;
                            dataRetailCIF.SumberDanaLainnya = string.IsNullOrEmpty(transactionCIF.RetailCIFCBO.SumberDanaLainnya) ? "" : transactionCIF.RetailCIFCBO.SumberDanaLainnya;

                            //Parameter System for pengkinian data
                            dataRetailCIF.MaritalStatusID = transactionCIF.RetailCIFCBO.MaritalStatusID.ID;
                            dataRetailCIF.NetAsset = transactionCIF.RetailCIFCBO.NetAsset.ID;
                            dataRetailCIF.MonthlyIncome = transactionCIF.RetailCIFCBO.MonthlyIncome.ID;
                            dataRetailCIF.MonthlyExtraIncome = transactionCIF.RetailCIFCBO.MonthlyExtraIncome.ID;
                            dataRetailCIF.Job = transactionCIF.RetailCIFCBO.Job.ID;
                            dataRetailCIF.AccountPurpose = transactionCIF.RetailCIFCBO.AccountPurpose.ID;
                            dataRetailCIF.IncomeForecast = transactionCIF.RetailCIFCBO.IncomeForecast.ID;
                            dataRetailCIF.OutcomeForecast = transactionCIF.RetailCIFCBO.OutcomeForecast.ID;
                            dataRetailCIF.TransactionForecast = transactionCIF.RetailCIFCBO.TransactionForecast.ID;
                            dataRetailCIF.DispatchModeTypeID = transactionCIF.RetailCIFCBO.DispatchModeType.ID;
                            dataRetailCIF.DispatchModeOther = transactionCIF.RetailCIFCBO.DispatchModeOther;
                            //dayat
                            dataRetailCIF.IsStampDuty = transactionCIF.RetailCIFCBO.IsStampDuty;
                            dataRetailCIF.IsSignature = transactionCIF.RetailCIFCBO.IsSignature;
                            dataRetailCIF.IsPenawaranProductPerbankan = transactionCIF.RetailCIFCBO.IsPenawaranProductPerbankan;

                            //end dayat
                            //Field-field feed from Risk Rating Form
                            dataRetailCIF.RiskRatingReportDate = transactionCIF.RetailCIFCBO.ReportDate;
                            dataRetailCIF.RiskRatingNextReviewDate = transactionCIF.RetailCIFCBO.NextReviewDate;
                            dataRetailCIF.RiskRatingResultID = transactionCIF.RetailCIFCBO.RiskRatingResult.ID;
                            dataRetailCIF.CompanyName = transactionCIF.RetailCIFCBO.CompanyName;
                            dataRetailCIF.Position = transactionCIF.RetailCIFCBO.Position;
                            dataRetailCIF.WorkPeriod = transactionCIF.RetailCIFCBO.WorkPeriod;
                            dataRetailCIF.IndustryType = transactionCIF.RetailCIFCBO.IndustryType;
                            dataRetailCIF.HubunganNasabah = transactionCIF.RetailCIFCBO.HubunganNasabah;
                            dataRetailCIF.FundSource = transactionCIF.RetailCIFCBO.FundSource.ID;

                            //henggar 180417
                            dataRetailCIF.IsMotherMaiden = transactionCIF.RetailCIFCBO.GroupCheckBox.IsMotherMaiden;
                            dataRetailCIF.IsEducation = transactionCIF.RetailCIFCBO.GroupCheckBox.IsEducation;
                            dataRetailCIF.IsReligion = transactionCIF.RetailCIFCBO.GroupCheckBox.IsReligion;
                            dataRetailCIF.IsDataUBO = transactionCIF.RetailCIFCBO.GroupCheckBox.IsDataUBO;
                            dataRetailCIF.IsTransparansiDataPenawaranProduct = transactionCIF.RetailCIFCBO.GroupCheckBox.IsTransparansiDataPenawaranProduct;
                            dataRetailCIF.IsTransparansiPenggunaan = transactionCIF.RetailCIFCBO.GroupCheckBox.IsTransparansiPenggunaan;
                            dataRetailCIF.IsPenawaranProduct = transactionCIF.RetailCIFCBO.GroupCheckBox.IsPenawaranProduct;
                            dataRetailCIF.IsSignature = transactionCIF.RetailCIFCBO.GroupCheckBox.IsSignature;
                            dataRetailCIF.IsStampDuty = transactionCIF.RetailCIFCBO.GroupCheckBox.IsStampDuty;
                            dataRetailCIF.MotherMaiden = transactionCIF.RetailCIFCBO.MotherMaiden;
                            dataRetailCIF.Education = transactionCIF.RetailCIFCBO.Education;
                            dataRetailCIF.Religion = transactionCIF.RetailCIFCBO.Religion;
                            dataRetailCIF.CIFUBO = transactionCIF.RetailCIFCBO.DataUBOCif;
                            dataRetailCIF.UBOName = transactionCIF.RetailCIFCBO.DataUBOName;
                            dataRetailCIF.UBORelationship = transactionCIF.RetailCIFCBO.DataUBORelationship;
                            dataRetailCIF.IsSLIK = transactionCIF.RetailCIFCBO.GroupCheckBox.IsSLIK;
                            dataRetailCIF.GrosIncomeccy = transactionCIF.RetailCIFCBO.GrosIncomeccy;
                            dataRetailCIF.GrossIncomeAmount = transactionCIF.RetailCIFCBO.GrossIncomeAmount;
                            dataRetailCIF.GrossCifSpouse = transactionCIF.RetailCIFCBO.GrossCifSpouse;
                            dataRetailCIF.GrossSpouseName = transactionCIF.RetailCIFCBO.GrossSpouseName;
                            dataRetailCIF.GrossSpouseRelation = transactionCIF.RetailCIFCBO.GrossSpouseRelation;
                            dataRetailCIF.IsChangeSignature = transactionCIF.RetailCIFCBO.GroupCheckBox.IsChangeSignature;
                            dataRetailCIF.ChangeSignature = transactionCIF.RetailCIFCBO.GroupCheckBox.ChangeSignature;
                            dataRetailCIF.IsFatcaCRS = transactionCIF.RetailCIFCBO.GroupCheckBox.IsFatcaCRS;
                            dataRetailCIF.FatcaCountryCode = transactionCIF.RetailCIFCBO.FatcaCountryCode;
                            dataRetailCIF.FatcaReviewStatus = transactionCIF.RetailCIFCBO.FatcaReviewStatus;
                            dataRetailCIF.FatcaCRSStatus = transactionCIF.RetailCIFCBO.FatcaCRSStatus;
                            dataRetailCIF.FatcaTaxPayer = transactionCIF.RetailCIFCBO.FatcaTaxPayer;
                            dataRetailCIF.FatcaWithHolding = transactionCIF.RetailCIFCBO.FatcaWithHolding;
                            dataRetailCIF.FatcaDateOnForm = transactionCIF.RetailCIFCBO.FatcaDateOnForm;
                            dataRetailCIF.FatcaReviewStatusDate = transactionCIF.RetailCIFCBO.FatcaReviewStatusDate;
                            dataRetailCIF.FatcaTaxPayerID = transactionCIF.RetailCIFCBO.FatcaTaxPayerID;
                            dataRetailCIF.DispatchModeOther = transactionCIF.RetailCIFCBO.DispatchModeOther;
                            //end



                            //
                            dataRetailCIF.Tier = transactionCIF.RetailCIFCBO.Tier;
                            dataRetailCIF.AmountLienUnlien = transactionCIF.RetailCIFCBO.AmountLienUnlien;

                            context_ts.RetailCIFCBOes.Add(dataRetailCIF);
                            context_ts.SaveChanges();
                            #endregion
                            //Get RetailCIFCBOID
                            RetailCIFCBOID = dataRetailCIF.RetailCIFCBOID;

                            #region RetailCIFCBOContact dayat
                            DBS.Entity.RetailCIFCBOContact dataRetailCIFContact = new DBS.Entity.RetailCIFCBOContact();
                            //selular
                            if (transactionCIF.RetailCIFCBO.SelularPhoneNumbers != null)
                            {
                                foreach (var item in transactionCIF.RetailCIFCBO.SelularPhoneNumbers)
                                {
                                    if (item.ActionType != "" && item.PhoneNumber != "")
                                    {
                                        dataRetailCIFContact.RetailCIFCBOID = dataRetailCIF.RetailCIFCBOID;
                                        dataRetailCIFContact.ActionType = item.ActionType;
                                        dataRetailCIFContact.ContactType = item.ContactType;
                                        dataRetailCIFContact.PhoneNumber = item.PhoneNumber;
                                        dataRetailCIFContact.CityCode = item.CityCode;
                                        dataRetailCIFContact.CountryCode = item.CountryCode;
                                        dataRetailCIFContact.CreateDate = DateTime.UtcNow;
                                        dataRetailCIFContact.CreateBy = currentUser.GetCurrentUser().LoginName;
                                        dataRetailCIFContact.UpdateCountryCode = null;
                                        dataRetailCIFContact.UpdateCityCode = null;
                                        dataRetailCIFContact.UpdatePhoneNumber = null;
                                        dataRetailCIFContact.RetailCIFCBOContactUpdateID = null;
                                        if (item.ActionType == "2")
                                        {
                                            dataRetailCIFContact.UpdateCountryCode = item.UpdateCountryCode;
                                            dataRetailCIFContact.UpdateCityCode = item.UpdateCityCode;
                                            dataRetailCIFContact.UpdatePhoneNumber = item.UpdatePhoneNumber;
                                            dataRetailCIFContact.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                        }

                                        if (item.ActionType == "3")
                                        {
                                            dataRetailCIFContact.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                        }
                                        context_ts.RetailCIFCBOContacts.Add(dataRetailCIFContact);
                                        context_ts.SaveChanges();
                                    }
                                }
                            }
                            //oofice
                            if (transactionCIF.RetailCIFCBO.OfficePhoneNumbers != null)
                            {
                                foreach (var item in transactionCIF.RetailCIFCBO.OfficePhoneNumbers)
                                {
                                    if (item.ActionType != "" && item.PhoneNumber != "")
                                    {
                                        dataRetailCIFContact.RetailCIFCBOID = dataRetailCIF.RetailCIFCBOID;
                                        dataRetailCIFContact.ActionType = item.ActionType;
                                        dataRetailCIFContact.ContactType = item.ContactType;
                                        dataRetailCIFContact.PhoneNumber = item.PhoneNumber;
                                        dataRetailCIFContact.CityCode = item.CityCode;
                                        dataRetailCIFContact.CountryCode = item.CountryCode;
                                        dataRetailCIFContact.CreateDate = DateTime.UtcNow;
                                        dataRetailCIFContact.CreateBy = currentUser.GetCurrentUser().LoginName;
                                        dataRetailCIFContact.UpdateCountryCode = null;
                                        dataRetailCIFContact.UpdateCityCode = null;
                                        dataRetailCIFContact.UpdatePhoneNumber = null;
                                        dataRetailCIFContact.RetailCIFCBOContactUpdateID = null;
                                        if (item.ActionType == "2")
                                        {
                                            dataRetailCIFContact.UpdateCountryCode = item.UpdateCountryCode;
                                            dataRetailCIFContact.UpdateCityCode = item.UpdateCityCode;
                                            dataRetailCIFContact.UpdatePhoneNumber = item.UpdatePhoneNumber;
                                            dataRetailCIFContact.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                        }
                                        if (item.ActionType == "3")
                                        {
                                            dataRetailCIFContact.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                        }
                                        context_ts.RetailCIFCBOContacts.Add(dataRetailCIFContact);
                                        context_ts.SaveChanges();
                                    }
                                }

                            }
                            //Home
                            if (transactionCIF.RetailCIFCBO.HomePhoneNumbers != null)
                            {
                                foreach (var item in transactionCIF.RetailCIFCBO.HomePhoneNumbers)
                                {
                                    if (item.ActionType != "" && item.PhoneNumber != "")
                                    {
                                        dataRetailCIFContact.RetailCIFCBOID = dataRetailCIF.RetailCIFCBOID;
                                        dataRetailCIFContact.ActionType = item.ActionType;
                                        dataRetailCIFContact.ContactType = item.ContactType;
                                        dataRetailCIFContact.PhoneNumber = item.PhoneNumber;
                                        dataRetailCIFContact.CityCode = item.CityCode;
                                        dataRetailCIFContact.CountryCode = item.CountryCode;
                                        dataRetailCIFContact.CreateDate = DateTime.UtcNow;
                                        dataRetailCIFContact.CreateBy = currentUser.GetCurrentUser().LoginName;
                                        dataRetailCIFContact.UpdateCountryCode = null;
                                        dataRetailCIFContact.UpdateCityCode = null;
                                        dataRetailCIFContact.UpdatePhoneNumber = null;
                                        dataRetailCIFContact.RetailCIFCBOContactUpdateID = null;
                                        if (item.ActionType == "2")
                                        {
                                            dataRetailCIFContact.UpdateCountryCode = item.UpdateCountryCode;
                                            dataRetailCIFContact.UpdateCityCode = item.UpdateCityCode;
                                            dataRetailCIFContact.UpdatePhoneNumber = item.UpdatePhoneNumber;
                                            dataRetailCIFContact.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                        }
                                        if (item.ActionType == "3")
                                        {
                                            dataRetailCIFContact.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                        }
                                        context_ts.RetailCIFCBOContacts.Add(dataRetailCIFContact);
                                        context_ts.SaveChanges();
                                    }
                                }

                            }
                            #endregion

                            #region Customer Join
                            //save into table RetailCIFCBOAccountJoin
                            if (transactionCIF.AddJoinTableCustomerCIF != null)
                            {
                                if (transactionCIF.AddJoinTableCustomerCIF.Count > 0)
                                {
                                    foreach (var item in transactionCIF.AddJoinTableCustomerCIF)
                                    {
                                        Entity.RetailCIFCBOAccountJoin dataRetailCIFCBOAccountJoin = new Entity.RetailCIFCBOAccountJoin()
                                        {
                                            TransactionID = transactionID,
                                            CIF = item.CIF,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context_ts.RetailCIFCBOAccountJoins.Add(dataRetailCIFCBOAccountJoin);
                                    }
                                    context_ts.SaveChanges();
                                }
                            }
                            #endregion

                            #region FFD Account
                            //save into table RetailCIFCBOAccNo for link to FFD Account
                            if (transactionCIF.AddJoinTableFFDAcountCIF != null)
                            {
                                if (transactionCIF.AddJoinTableFFDAcountCIF.Count > 0)
                                {
                                    //var SelectedFFD = transactionCIF.AddJoinTableFFDAcountCIF.Where(s => s.IsAddFFDAccount == true);
                                    foreach (var item in transactionCIF.AddJoinTableFFDAcountCIF)//SelectedFFD)
                                    {
                                        Entity.RetailCIFCBOAccNo dataRetailCIFCBOAccNo = new Entity.RetailCIFCBOAccNo()
                                        {
                                            RetailCIFCBOID = RetailCIFCBOID,
                                            AccountNumber = item.AccountNumber,
                                            IsFFD = item.IsAddFFDAccount,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context_ts.RetailCIFCBOAccNoes.Add(dataRetailCIFCBOAccNo);
                                    }
                                    context_ts.SaveChanges();
                                }
                            }
                            #endregion

                            #region Account Join
                            //save into table RetailCIFCBOAccNo for Add Account
                            if (transactionCIF.AddJoinTableAccountCIF != null)
                            {
                                if (transactionCIF.AddJoinTableAccountCIF.Count > 0)
                                {
                                    //var SelectedFFD = transactionCIF.AddJoinTableAccountCIF.Where(s => s.IsAddFFDAccount == true);
                                    foreach (var item in transactionCIF.AddJoinTableAccountCIF) //SelectedFFD)
                                    {
                                        Entity.RetailCIFCBOAccNo dataRetailCIFCBOAccNo1 = new Entity.RetailCIFCBOAccNo()
                                        {
                                            RetailCIFCBOID = RetailCIFCBOID,
                                            AccountNumber = item.AccountNumber,
                                            CurrencyID = item.Currency.ID,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context_ts.RetailCIFCBOAccNoes.Add(dataRetailCIFCBOAccNo1);
                                    }
                                    context_ts.SaveChanges();
                                }
                            }
                            #endregion

                            #region Join Account
                            //save into table RetailCIFCBOAccNo for Dormant Account
                            if (transactionCIF.AddJoinTableDormantCIF != null)
                            {
                                if (transactionCIF.AddJoinTableDormantCIF.Count > 0)
                                {
                                    var SelectedDormant = transactionCIF.AddJoinTableDormantCIF.Where(s => s.IsAddDormantAccount == true);
                                    foreach (var item in SelectedDormant)
                                    {
                                        Entity.RetailCIFCBOAccNo dataRetailCIFCBOAccNo2 = new Entity.RetailCIFCBOAccNo()
                                        {
                                            RetailCIFCBOID = RetailCIFCBOID,
                                            AccountNumber = item.AccountNumber,
                                            CurrencyID = item.Currency.ID,
                                            //IsFFD = item.IsAddFFDAccount,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context_ts.RetailCIFCBOAccNoes.Add(dataRetailCIFCBOAccNo2);
                                    }
                                    context_ts.SaveChanges();
                                }
                            }
                            #endregion

                            #region Freeze Account
                            //save into table RetailCIFCBOFreezeAccount for Freeze Unfreeze Account
                            if (transactionCIF.AddJoinTableFreezeUnfreezeCIF != null)
                            {
                                if (transactionCIF.AddJoinTableFreezeUnfreezeCIF.Count > 0)
                                {
                                    var SelectedFreeze = transactionCIF.AddJoinTableFreezeUnfreezeCIF.Where(s => s.IsAddTblFreezeAccount == true);
                                    foreach (var item in SelectedFreeze)
                                    {
                                        Entity.RetailCIFCBOFreezeAccount dataRetailCIFCBOFreeze = new Entity.RetailCIFCBOFreezeAccount()
                                        {
                                            RetailCIFCBOID = RetailCIFCBOID,
                                            AccountNumber = item.AccountNumber,
                                            IsFreeze = item.IsFreezeAccount,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context_ts.RetailCIFCBOFreezeAccounts.Add(dataRetailCIFCBOFreeze);
                                    }
                                    context_ts.SaveChanges();
                                }
                            }
                            #endregion

                            #region Document
                            //save into table TransactionDocument
                            if (transactionCIF.DocumentsCIF != null)
                            {
                                if (transactionCIF.DocumentsCIF.Count > 0)
                                {
                                    foreach (var item in transactionCIF.DocumentsCIF)
                                    {
                                        Entity.TransactionDocument document = new Entity.TransactionDocument()
                                        {
                                            TransactionID = data.TransactionID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = 1,
                                            Filename = item.FileName,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };

                                        context_ts.TransactionDocuments.Add(document);
                                    }

                                    context_ts.SaveChanges();
                                }
                            }

                            #endregion

                            #region LienUnlien
                            if (transactionCIF.AccountNumberLienUnlien != null)
                            {
                                if (transactionCIF.AccountNumberLienUnlien.Count > 0)
                                {
                                    IList<AccountNumberLienUnlienModel> selectedLienUnlien = transactionCIF.AccountNumberLienUnlien.Where(d => d.IsSelected == true).ToList();
                                    if (selectedLienUnlien != null)
                                    {
                                        if (selectedLienUnlien.Count > 0)
                                        {
                                            foreach (var item in selectedLienUnlien)
                                            {
                                                Entity.LienUnlien dataInsert = new LienUnlien();
                                                dataInsert.CIF = transactionCIF.Customer.CIF;
                                                dataInsert.TransactionId = transactionID;
                                                dataInsert.AccountNumber = item.AccountNumber;
                                                dataInsert.LienUnlienData = (item.LienUnlien.Trim().Equals("1") ? "Lien" : (item.LienUnlien.Trim().Equals("2") ? "Un-lien" : ""));
                                                dataInsert.IsDeleted = false;
                                                dataInsert.CreateBy = currentUser.GetCurrentUser().LoginName;
                                                dataInsert.CreateDate = DateTime.UtcNow;
                                                dataInsert.IsBranchCheckerVerify = false;
                                                dataInsert.IsCBOAccountCheckerVerify = false;

                                                context_ts.LienUnliens.Add(dataInsert);
                                            }
                                            context_ts.SaveChanges();
                                        }
                                    }
                                    selectedLienUnlien = null;
                                }
                            }
                            #endregion


                            ts.Complete();
                            IsSuccess = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        ts.Dispose();
                        if (ex.InnerException != null && ex.InnerException.InnerException != null)
                            message = ex.InnerException.InnerException.Message;
                        else
                            message = ex.Message;
                    }

                }
            }
            if (!transactionCIF.IsDraft)
            {
                using (DBSEntities context = new DBSEntities())
                {
                    var getTR = (from TR in context.Transactions
                                 where TR.TransactionID == TrIDInserted
                                 select TR).SingleOrDefault();
                    ApplicationID = getTR.ApplicationID;
                    IsSuccess = true;
                }
            }
            return IsSuccess;
        }

        public bool GetTransactionDraftByID(long id, ref TransactionCIFDraftModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                TransactionCIFDraftModel datatransaction = (from a in context.TransactionDrafts
                                                            where a.TransactionID.Equals(id)
                                                            select new TransactionCIFDraftModel
                                                            {
                                                                ID = a.TransactionID,
                                                                ApplicationID = a.ApplicationID,
                                                                ApplicationDate = a.ApplicationDate,
                                                                IsTopUrgent = a.IsTopUrgent,
                                                                CreateDate = a.CreateDate,
                                                                LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                                                IsPOA = a.IsPOA,
                                                                IsLOI = a.IsLOI,
                                                                IsPOI = a.IsPOI
                                                            }).SingleOrDefault();

                // fill CIF Retail data
                if (datatransaction != null)
                {
                    Entity.TransactionDraft data = context.TransactionDrafts.Where(x => x.TransactionID.Equals(datatransaction.ID)).Select(x => x).FirstOrDefault();

                    if (datatransaction.IsNewCustomer != null && datatransaction.IsNewCustomer != true)
                    {
                        string cif = context.TransactionDrafts.Where(a => a.TransactionID.Equals(id)).Select(a => a.CIF).FirstOrDefault();
                        if (!string.IsNullOrEmpty(cif))
                        {
                            ICustomerRepository customerRepo = new CustomerRepository();
                            CustomerModel customer = new CustomerModel();
                            if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                            {
                                datatransaction.Customer = customer;
                            }

                            customerRepo.Dispose();
                            customer = null;
                        }
                    }
                    //added bu dani
                    if (data.Currency != null)
                    {
                        Currency currency = new Currency();
                        currency.CurrencyID = data.Currency.CurrencyID;
                        currency.CurrencyCode = data.Currency.CurrencyCode;
                        currency.CurrencyDescription = data.Currency.CurrencyDescription;
                        datatransaction.Currency = currency;
                    }
                    //added bu dani

                    if (data.Product != null)
                    {
                        ProductModel p = new ProductModel();

                        p.ID = data.Product.ProductID;
                        p.Code = data.Product.ProductCode;
                        p.Name = data.Product.ProductName;

                        datatransaction.Product = p;
                    }

                    if (data.TransactionType != null)
                    {
                        TransactionTypeModel t = new TransactionTypeModel();
                        t.ID = data.TransactionType.TransTypeID;
                        t.Name = data.TransactionType.TransactionType1;
                        datatransaction.TransactionType = t;
                    }

                    if (data.AccountNumber != null)
                    {
                        CustomerAccountModel Account = new CustomerAccountModel();
                        Account.AccountNumber = data.CustomerAccount.AccountNumber;
                        Account.LastModifiedBy = data.CustomerAccount.UpdateDate == null ? data.CustomerAccount.CreateBy : data.CustomerAccount.UpdateBy;
                        Account.LastModifiedDate = data.CustomerAccount.UpdateDate == null ? data.CustomerAccount.CreateDate : data.CustomerAccount.UpdateDate.Value;

                        datatransaction.AccountNumber = Account;

                    }

                    if (data.MaintenanceType != null)
                    {
                        MaintenanceTypeModel m = new MaintenanceTypeModel();
                        m.ID = data.MaintenanceType.CBOMaintainID;
                        m.Name = data.MaintenanceType.MaintenanceTypeName;
                        datatransaction.MaintenanceType = m;
                    }
                    //Tambah TagUntag
                    if (data.StaffTaggingID != null)
                    {
                        StaffTaggingModel st = new StaffTaggingModel();
                        st.ID = data.StaffTaggingID ?? 0;
                        datatransaction.StaffTagging = st;
                    }
                    //End

                    //added by dani 29-1-2016
                    if (data.TransactionSUBType != null)
                    {
                        TransactionSubTypeModel ts = new TransactionSubTypeModel();
                        ts.ID = data.TransactionSubTypeID ?? 0;
                        datatransaction.TransactionSubType = ts;
                    }
                    datatransaction.StaffID = string.IsNullOrEmpty(data.StaffID) ? "" : data.StaffID;//added by dani
                    datatransaction.ATMNumber = string.IsNullOrEmpty(data.ATMNumber) ? "" : data.ATMNumber;//added by dani
                    //added by dani end
                    //added by dani 30-1-2016
                    if (data.LocationName != null)
                    {
                        BranchRiskRatingModel br = new BranchRiskRatingModel();
                        br.Name = data.LocationName;
                        datatransaction.BrachRiskRating = br;
                    }
                    //added by dani 30-1-2016 end
                    var retailcifCBODraft = (from retail in context.RetailCIFCBODrafts
                                             where retail.TransactionID == id
                                             select new TransactionRetailCIFCBOModel
                                             {
                                                 TransactionID = datatransaction.ID,
                                                 RequestTypeID = retail.RequestTypeID,
                                                 MaintenanceTypeID = retail.MaintenanceTypeID,

                                                 AccountNumber = retail.AccountNumber,
                                                 Name = retail.Name,
                                                 RetailCIFCBOID = retail.RetailCIFCBODraftID,
                                                 IdentityNumber = retail.IdentityNumber,
                                                 IdentityStartDate = retail.IdentityStartDate,
                                                 IdentityEndDate = retail.IdentityEndDate,
                                                 IdentityAddress = retail.IdentityAddress,
                                                 IdentityKelurahan = retail.IdentityKelurahan,
                                                 IdentityKecamatan = retail.IdentityKecamatan,
                                                 IdentityCity = retail.IdentityCity,
                                                 IdentityProvince = retail.IdentityProvince,
                                                 IdentityCountry = retail.IdentityCountry,
                                                 IdentityPostalCode = retail.IdentityPostalCode,

                                                 IdentityNumber2 = retail.IdentityNumber2,
                                                 IdentityStartDate2 = retail.IdentityStartDate2,
                                                 IdentityEndDate2 = retail.IdentityEndDate2,
                                                 IdentityAddress2 = retail.IdentityAddress2,
                                                 IdentityKelurahan2 = retail.IdentityKelurahan2,
                                                 IdentityKecamatan2 = retail.IdentityKecamatan2,
                                                 IdentityCity2 = retail.IdentityCity2,
                                                 IdentityProvince2 = retail.IdentityProvince2,
                                                 IdentityCountry2 = retail.IdentityCountry2,
                                                 IdentityPostalCode2 = retail.IdentityPostalCode2,

                                                 IdentityNumber3 = retail.IdentityNumber3,
                                                 IdentityStartDate3 = retail.IdentityStartDate3,
                                                 IdentityEndDate3 = retail.IdentityEndDate3,
                                                 IdentityAddress3 = retail.IdentityAddress3,
                                                 IdentityKelurahan3 = retail.IdentityKelurahan3,
                                                 IdentityKecamatan3 = retail.IdentityKecamatan3,
                                                 IdentityCity3 = retail.IdentityCity3,
                                                 IdentityProvince3 = retail.IdentityProvince3,
                                                 IdentityCountry3 = retail.IdentityCountry3,
                                                 IdentityPostalCode3 = retail.IdentityPostalCode3,

                                                 IdentityNumber4 = retail.IdentityNumber4,
                                                 IdentityStartDate4 = retail.IdentityStartDate4,
                                                 IdentityEndDate4 = retail.IdentityEndDate4,
                                                 IdentityAddress4 = retail.IdentityAddress4,
                                                 IdentityKelurahan4 = retail.IdentityKelurahan4,
                                                 IdentityKecamatan4 = retail.IdentityKecamatan4,
                                                 IdentityCity4 = retail.IdentityCity4,
                                                 IdentityProvince4 = retail.IdentityProvince4,
                                                 IdentityCountry4 = retail.IdentityCountry4,
                                                 IdentityPostalCode4 = retail.IdentityPostalCode4,

                                                 NPWPNumber = retail.NPWPNumber,
                                                 IsNPWPReceived = retail.IsNPWPReceived,
                                                 SpouseName = retail.SpouseName,
                                                 IsCorrespondenseToEmail = retail.IsCorrespondenseToEmail,
                                                 CorrespondenseAddress = retail.CorrespondenseAddress,
                                                 CorrespondenseKelurahan = retail.CorrespondenseKelurahan,
                                                 CorrespondenseKecamatan = retail.CorrespondenseKecamatan,
                                                 CorrespondenseCity = retail.CorrespondenseCity,
                                                 CorrespondenseProvince = retail.CorrespondenseProvince,
                                                 CorrespondenseCountry = retail.CorrespondenseCountry,
                                                 CorrespondensePostalCode = retail.CorrespondensePostalCode,

                                                 CellPhone = retail.CellPhone,
                                                 UpdatedCellPhone = retail.UpdatedCellPhone,

                                                 HomePhone = retail.HomePhone,
                                                 UpdatedHomePhone = retail.UpdatedHomePhone,

                                                 OfficePhone = retail.OfficePhone,
                                                 UpdatedOfficePhone = retail.UpdatedOfficePhone,

                                                 Fax = retail.Fax,
                                                 UpdatedFax = retail.UpdatedFax,
                                                 EmailAddress = retail.EmailAddress,
                                                 OfficeAddress = retail.OfficeAddress,
                                                 OfficeKelurahan = retail.OfficeKelurahan,
                                                 OfficeKecamatan = retail.OfficeKecamatan,
                                                 OfficeCity = retail.OfficeCity,
                                                 OfficeProvince = retail.OfficeProvince,
                                                 OfficeCountry = retail.OfficeCountry,
                                                 OfficePostalCode = retail.OfficePostalCode,
                                                 Nationality = retail.Nationality,
                                                 UBOName = retail.UBOName,
                                                 UBOIdentityType = retail.UBOIdentityType,
                                                 UBOPhone = retail.UBOPhone,
                                                 UBOJob = retail.UBOJob,
                                                 CreateDate = retail.CreateDate,
                                                 CreateBy = retail.CreateBy,
                                                 AtmNumber = retail.AtmNumber,
                                                 CompanyName = retail.CompanyName,
                                                 Position = retail.Position,
                                                 WorkPeriod = retail.WorkPeriod,
                                                 IndustryType = retail.IndustryType,
                                                 //added by dani 30-1-2016
                                                 ReportDate = retail.RiskRatingReportDate,
                                                 NextReviewDate = retail.RiskRatingNextReviewDate,
                                                 RiskRatingResult = retail.RiskRatingResultID == null ? new ParameterSystemCIFModel { ID = 0, Name = "" } : new ParameterSystemCIFModel { ID = retail.RiskRatingResultID.Value, Name = "" },
                                                 //added by dani 30-1-2016 end
                                                 IdentityTypeID = retail.IdentityTypeID == null ? new ParameterSystemCIFModel { ID = 0, Name = "" } : new ParameterSystemCIFModel { ID = (int)retail.IdentityTypeID, Name = "" },
                                                 IdentityTypeID2 = retail.IdentityTypeID2 == null ? new ParameterSystemCIFModel { ID = 0, Name = "" } : new ParameterSystemCIFModel { ID = (int)retail.IdentityTypeID2, Name = "" },
                                                 IdentityTypeID3 = retail.IdentityTypeID3 == null ? new ParameterSystemCIFModel { ID = 0, Name = "" } : new ParameterSystemCIFModel { ID = (int)retail.IdentityTypeID3, Name = "" },
                                                 IdentityTypeID4 = retail.IdentityTypeID4 == null ? new ParameterSystemCIFModel { ID = 0, Name = "" } : new ParameterSystemCIFModel { ID = (int)retail.IdentityTypeID4, Name = "" },
                                                 CellPhoneMethodID = retail.CellPhoneMethodID == null ? new ParameterSystemCIFModel { ID = 0, Name = "" } : new ParameterSystemCIFModel { ID = (int)retail.CellPhoneMethodID, Name = "" },
                                                 HomePhoneMethodID = retail.HomePhoneMethodID == null ? new ParameterSystemCIFModel { ID = 0, Name = "" } : new ParameterSystemCIFModel { ID = (int)retail.HomePhoneMethodID, Name = "" },
                                                 OfficePhoneMethodID = retail.OfficePhoneMethodID == null ? new ParameterSystemCIFModel { ID = 0, Name = "" } : new ParameterSystemCIFModel { ID = (int)retail.OfficePhoneMethodID, Name = "" },
                                                 FaxMethodID = retail.FaxMethodID == null ? new ParameterSystemCIFModel { ID = 0, Name = "" } : new ParameterSystemCIFModel { ID = (int)retail.FaxMethodID, Name = "" },
                                                 MaritalStatusID = retail.MaritalStatusID == null ? new ParameterSystemCIFModel { ID = 0, Name = "" } : new ParameterSystemCIFModel { ID = (int)retail.MaritalStatusID, Name = "" },
                                                 FundSource = retail.FundSource == null ? new ParameterSystemCIFModel { ID = 0, Name = "" } : new ParameterSystemCIFModel { ID = (int)retail.FundSource, Name = "" },
                                                 NetAsset = retail.NetAsset == null ? new ParameterSystemCIFModel { ID = 0, Name = "" } : new ParameterSystemCIFModel { ID = (int)retail.NetAsset, Name = "" },
                                                 MonthlyIncome = retail.MonthlyIncome == null ? new ParameterSystemCIFModel { ID = 0, Name = "" } : new ParameterSystemCIFModel { ID = (int)retail.MonthlyIncome, Name = "" },
                                                 MonthlyExtraIncome = retail.MonthlyExtraIncome == null ? new ParameterSystemCIFModel { ID = 0, Name = "" } : new ParameterSystemCIFModel { ID = (int)retail.MonthlyExtraIncome, Name = "" },
                                                 Job = retail.Job == null ? new ParameterSystemCIFModel { ID = 0, Name = "" } : new ParameterSystemCIFModel { ID = (int)retail.Job, Name = "" },
                                                 AccountPurpose = retail.AccountPurpose == null ? new ParameterSystemCIFModel { ID = 0, Name = "" } : new ParameterSystemCIFModel { ID = (int)retail.AccountPurpose, Name = "" },
                                                 IncomeForecast = retail.IncomeForecast == null ? new ParameterSystemCIFModel { ID = 0, Name = "" } : new ParameterSystemCIFModel { ID = (int)retail.IncomeForecast, Name = "" },
                                                 OutcomeForecast = retail.OutcomeForecast == null ? new ParameterSystemCIFModel { ID = 0, Name = "" } : new ParameterSystemCIFModel { ID = (int)retail.OutcomeForecast, Name = "" },
                                                 TransactionForecast = retail.TransactionForecast == null ? new ParameterSystemCIFModel { ID = 0, Name = "" } : new ParameterSystemCIFModel { ID = (int)retail.TransactionForecast, Name = "" },
                                                 DispatchModeType = retail.DispatchModeTypeID == null ? new ParameterSystemCIFModel { ID = 0, Name = "" } : new ParameterSystemCIFModel { ID = (int)retail.DispatchModeTypeID, Name = "" },
                                                 DispatchModeOther = retail.DispatchModeOther == null ? "" : retail.DispatchModeOther,
                                                 PekerjaanLainnya = string.IsNullOrEmpty(retail.PekerjaanLainnya) ? "" : retail.PekerjaanLainnya,
                                                 PekerjaanProfesional = string.IsNullOrEmpty(retail.PekerjaanProfesional) ? "" : retail.PekerjaanProfesional,
                                                 SumberDanaLainnya = string.IsNullOrEmpty(retail.SumberDanaLainnya) ? "" : retail.SumberDanaLainnya,
                                                 TujuanBukaRekeningLainnya = string.IsNullOrEmpty(retail.TujuanBukaRekeningLainnya) ? "" : retail.TujuanBukaRekeningLainnya,
                                                 HubunganNasabah = retail.HubunganNasabah,
                                                 IsPenawaranProductPerbankan = retail.IsPenawaranProductPerbankan.Value,
                                                 IsSignature = retail.IsSignature.Value,
                                                 IsStampDuty = retail.IsStampDuty.Value,
                                                 IsOthers = retail.IsOthers.Value,
                                                 AmountLienUnlien = retail.AmountLienUnlien,

                                                 IsMotherMaiden = retail.IsMotherMaiden.Value,
                                                 IsEducation = retail.IsEducation.Value,
                                                 IsReligion = retail.IsReligion.Value,
                                                 IsDataUBO = retail.IsDataUBO.Value,
                                                 IsTransparansiDataPenawaranProduct = retail.IsTransparansiDataPenawaranProduct.Value,
                                                 IsTransparansiPenggunaan = retail.IsTransparansiPenggunaan.Value,
                                                 IsPenawaranProduct = retail.IsPenawaranProduct.Value,
                                                 Education = retail.Education.Value,
                                                 Religion = retail.Religion.Value,
                                                 MotherMaiden = retail.MotherMaiden,
                                                 CIFUBO = retail.CIFUBO.Value,
                                                 UBORelationship = retail.UBORelationship,
                                                 IsSLIK = retail.IsSLIK.Value,
                                                 GrosIncomeccy = retail.GrosIncomeccy.Value,
                                                 GrossIncomeAmount = retail.GrossIncomeAmount.Value,
                                                 GrossCifSpouse = retail.GrossCifSpouse.Value,
                                                 GrossSpouseName = retail.GrossSpouseName,
                                                 GrossSpouseRelation = retail.GrossSpouseRelation,
                                                 IsChangeSignature = retail.IsChangeSignature.Value,
                                                 ChangeSignature = retail.ChangeSignature.Value,
                                                 FatcaCountryCode = retail.FatcaCountryCode.Value,
                                                 FatcaReviewStatus = retail.FatcaReviewStatus.Value,
                                                 FatcaCRSStatus = retail.FatcaCRSStatus.Value,
                                                 FatcaTaxPayer = retail.FatcaTaxPayer.Value,
                                                 FatcaWithHolding = retail.FatcaWithHolding.Value,
                                                 FatcaDateOnForm = retail.FatcaDateOnForm.Value,
                                                 FatcaReviewStatusDate = retail.FatcaReviewStatusDate.Value,
                                                 FatcaTaxPayerID = retail.FatcaTaxPayerID,
                                                 IsFatcaCRS = retail.IsFatcaCRS.Value,
                                                 Tier = retail.Tier
                                             }).FirstOrDefault();
                    if (retailcifCBODraft != null)
                    {
                        datatransaction.RetailCIFCBO = retailcifCBODraft;
                    }
                    #region groupCexBOX
                    var retailcifCBODraftChk = (from retail in context.RetailCIFCBODrafts
                                                where retail.TransactionID == id
                                                select new GroupCheckBoxModel
                                                {
                                                    IsNameMaintenance = retail.IsNameMaintenance,
                                                    IsIdentityTypeMaintenance = retail.IsIdentityTypeMaintenance,
                                                    IsNPWPMaintenance = retail.IsNPWPMaintenance,
                                                    IsMaritalStatusMaintenance = retail.IsMaritalStatusMaintenance,
                                                    IsCorrespondenceMaintenance = retail.IsCorrespondenceMaintenance,
                                                    IsIdentityAddressMaintenance = retail.IsIdentityAddressMaintenance,
                                                    IsOfficeAddressMaintenance = retail.IsOfficeAddressMaintenance,
                                                    IsCorrespondenseAddressMaintenance = retail.IsCorrespondenseAddressMaintenance,
                                                    IsPhoneFaxEmailMaintenance = retail.IsPhoneFaxEmailMaintenance,
                                                    IsNationalityMaintenance = retail.IsNationalityMaintenance,
                                                    IsFundSourceMaintenance = retail.IsFundSourceMaintenance,
                                                    IsNetAssetMaintenance = retail.IsNetAssetMaintenance,
                                                    IsMonthlyIncomeMaintenance = retail.IsMonthlyIncomeMaintenance,
                                                    IsJobMaintenance = retail.IsJobMaintenance,
                                                    IsAccountPurposeMaintenance = retail.IsAccountPurposeMaintenance,
                                                    IsMonthlyTransactionMaintenance = retail.IsMonthlyTransactionMaintenance,
                                                    IsPekerjaanProfesional = retail.IsPekerjaanProfesional,
                                                    IsPekerjaanLainnya = retail.IsPekerjaanLainnya,
                                                    IsSumberDanaLainnya = retail.IsSumberDanaLainnya,
                                                    IsTujuanBukaRekeningLainnya = retail.IsTujuanBukaRekeningLainnya,
                                                    IsOthers = retail.IsOthers
                                                }).FirstOrDefault();
                    #endregion
                    if (retailcifCBODraftChk != null)
                    {
                        datatransaction.GroupCheckBox = retailcifCBODraftChk;
                    }

                    List<RetailCIFCBOAccountJoinDraft> AddJointCustomerCIF = context.RetailCIFCBOAccountJoinDrafts.Where(y => y.TransactionID == datatransaction.ID).ToList();
                    if (AddJointCustomerCIF != null)
                    {
                        datatransaction.AddJoinTableCustomerCIF = new List<CustomerModalModel>();
                        foreach (var item in AddJointCustomerCIF)
                        {
                            CustomerModalModel CustomerJoin = new CustomerModalModel();
                            CustomerJoin.CIF = item.CIF;
                            CustomerJoin.Name = item.Customer.CustomerName;
                            datatransaction.AddJoinTableCustomerCIF.Add(CustomerJoin);
                        }
                    }

                    long RetailCIFCBODraftID = context.RetailCIFCBODrafts.Where(y => y.TransactionID == datatransaction.ID).Select(y => y.RetailCIFCBODraftID).FirstOrDefault();
                    List<RetailCIFCBOAccNoDraft> AddJoinTableFFDAccountCIF = context.RetailCIFCBOAccNoDrafts.Where(y => y.RetailCIFCBODraftID == RetailCIFCBODraftID).ToList(); //&& y.IsFFD.Value == true).ToList();
                    if (AddJoinTableFFDAccountCIF != null)
                    {
                        datatransaction.AddJoinTableFFDAcountCIF = new List<FFDAccountModalModel>();
                        foreach (var item in AddJoinTableFFDAccountCIF)
                        {
                            FFDAccountModalModel FFDAccountJoin = new FFDAccountModalModel();

                            FFDAccountJoin.AccountNumber = item.AccountNumber;
                            FFDAccountJoin.IsAddFFDAccount = item.IsFFD;
                            datatransaction.AddJoinTableFFDAcountCIF.Add(FFDAccountJoin);
                        }

                    }

                    List<RetailCIFCBOAccNoDraft> AddJoinTableAccountCIF = context.RetailCIFCBOAccNoDrafts.Where(y => y.RetailCIFCBODraftID == RetailCIFCBODraftID && (y.IsFFD.Value == null || y.IsFFD.Value == false)).ToList();
                    if (AddJoinTableAccountCIF != null)
                    {
                        datatransaction.AddJoinTableAccountCIF = new List<FFDAccountModalModel>();
                        foreach (var item in AddJoinTableAccountCIF)
                        {
                            FFDAccountModalModel AccountJoin = new FFDAccountModalModel();
                            AccountJoin.AccountNumber = item.AccountNumber;
                            AccountJoin.Currency = item.Currency != null ? new CurrencyModalModel()
                            {
                                ID = item.Currency.CurrencyID,
                                Code = item.Currency.CurrencyCode,
                                Description = item.Currency.CurrencyDescription

                            } : new CurrencyModalModel()
                            {
                                ID = 13,
                                Code = "",
                                Description = ""
                            };
                            AccountJoin.IsAddFFDAccount = item.IsFFD;
                            datatransaction.AddJoinTableAccountCIF.Add(AccountJoin);
                        }
                    }

                    List<RetailCIFCBOAccNoDraft> AddJoinTableDormantCIF = context.RetailCIFCBOAccNoDrafts.Where(y => y.RetailCIFCBODraftID == RetailCIFCBODraftID && (y.IsFFD == null || y.IsFFD.Value == false)).ToList();
                    if (AddJoinTableDormantCIF != null)
                    {
                        datatransaction.AddJoinTableDormantCIF = new List<DormantAccountModel>();
                        foreach (var item in AddJoinTableDormantCIF)
                        {
                            DormantAccountModel DormantCIF = new DormantAccountModel();
                            DormantCIF.AccountNumber = item.AccountNumber;
                            DormantCIF.Currency = item.Currency != null ? new CurrencyModalModel()
                            {
                                ID = item.Currency.CurrencyID,
                                Code = item.Currency.CurrencyCode,
                                Description = item.Currency.CurrencyDescription
                            } : new CurrencyModalModel()
                            {
                                ID = 13,
                                Code = "",
                                Description = ""
                            };
                            DormantCIF.IsAddDormantAccount = true;
                            datatransaction.AddJoinTableDormantCIF.Add(DormantCIF);
                        }
                    }

                    List<RetailCIFCBOFreezeAccountDraft> AddJoinTableFreezeUnfreezeCIF = context.RetailCIFCBOFreezeAccountDrafts.Where(y => y.RetailCIFCBODraftID.Value == RetailCIFCBODraftID).ToList();
                    if (AddJoinTableFreezeUnfreezeCIF != null)
                    {
                        datatransaction.AddJoinTableFreezeUnfreezeCIF = new List<FreezeUnfreezeModel>();
                        foreach (var item in AddJoinTableFreezeUnfreezeCIF)
                        {
                            FreezeUnfreezeModel FreezeUnfreezeCIF = new FreezeUnfreezeModel();
                            FreezeUnfreezeCIF.AccountNumber = item.AccountNumber;
                            FreezeUnfreezeCIF.IsFreezeAccount = item.IsFreeze;
                            FreezeUnfreezeCIF.IsAddTblFreezeAccount = true;
                            datatransaction.AddJoinTableFreezeUnfreezeCIF.Add(FreezeUnfreezeCIF);
                        }
                    }

                    var docDraft = (from doc in context.TransactionDocumentDrafts
                                    where doc.TransactionID == id && doc.IsDeleted.Equals(false)
                                    select new TransactionDocumentDraftModel
                                    {
                                        ID = doc.TransactionDocumentID,
                                        IsDormant = doc.IsDormant,
                                        LastModifiedBy = doc.UpdateBy == null ? doc.CreateBy : doc.UpdateBy,
                                        LastModifiedDate = doc.UpdateDate == null ? doc.CreateDate : doc.CreateDate,
                                        Purpose = new DocumentPurposeModel { ID = doc.DocumentPurpose.PurposeID, Name = doc.DocumentPurpose.PurposeName },
                                        Type = new DocumentTypeModel { ID = doc.DocumentType.DocTypeID, Name = doc.DocumentType.DocTypeName },
                                        FileName = doc.Filename,
                                        DocumentPath = new DocumentPathModel { name = doc.Filename, lastModified = string.Empty, lastModifiedDate = DateTime.Now, DocPath = doc.DocumentPath, type = Util.ExistingDoctype }
                                    }).ToList();
                    if (docDraft != null)
                    {
                        datatransaction.Documents = docDraft;
                    }

                    IList<AccountNumberLienUnlienModel> dataLienUnlien = (from a in context.LienUnlienDrafts
                                                                          where a.TransactionId.Value == id && a.IsDeleted.Value == false
                                                                          select new AccountNumberLienUnlienModel
                                                                          {
                                                                              AccountNumber = a.AccountNumber,
                                                                              IsSelected = true,
                                                                              LienUnlien = a.LienUnlienData
                                                                          }).ToList();
                    if (dataLienUnlien != null && dataLienUnlien.Count > 0)
                    {
                        datatransaction.AccountNumberLienUnlien = dataLienUnlien;
                    }

                    transaction = datatransaction;

                    #region Get RCCBO Contact Draft
                    var RCCBOCD = context.RetailCIFCBOContactDrafts.Where(y => y.RetailCIFCBODraftID == retailcifCBODraft.RetailCIFCBOID).ToList();
                    TransactionRetailCIFCBOModel xxx = new TransactionRetailCIFCBOModel();
                    xxx.SelularPhoneNumbers = new List<RCCBOContactModel>();
                    xxx.HomePhoneNumbers = new List<RCCBOContactModel>();
                    xxx.OfficePhoneNumbers = new List<RCCBOContactModel>();
                    transaction.RetailCIFCBO.SelularPhoneNumbers = new List<RCCBOContactModel>();
                    transaction.RetailCIFCBO.OfficePhoneNumbers = new List<RCCBOContactModel>();
                    transaction.RetailCIFCBO.HomePhoneNumbers = new List<RCCBOContactModel>();

                    if (RCCBOCD != null)
                    {
                        foreach (var item in RCCBOCD)
                        {
                            RCCBOContactModel rccboc = new RCCBOContactModel();
                            rccboc.RetailCIFCBOID = item.RetailCIFCBODraftID;
                            rccboc.ActionType = item.ActionType;
                            rccboc.ContactType = item.ContactType;
                            rccboc.PhoneNumber = item.PhoneNumber;
                            rccboc.CountryCode = item.CountryCode;
                            rccboc.CityCode = item.CityCode;
                            rccboc.RetailCIFCBOContactUpdateID = null;
                            rccboc.UpdatePhoneNumber = null;
                            rccboc.UpdateCountryCode = null;
                            rccboc.UpdateCityCode = null;
                            if (item.ActionType == "2")
                            {
                                rccboc.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                rccboc.UpdatePhoneNumber = item.UpdatePhoneNumber;
                                rccboc.UpdateCountryCode = item.UpdateCountryCode;
                                rccboc.UpdateCityCode = item.UpdateCityCode;
                            }
                            if (item.ActionType == "3")
                            {
                                rccboc.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                            }
                            if (item.ContactType == "Selular" || item.ContactType == "selular")
                            {
                                xxx.SelularPhoneNumbers.Add(rccboc);
                            }
                            else if (item.ContactType == "Home")
                            {
                                xxx.HomePhoneNumbers.Add(rccboc);
                            }
                            else if (item.ContactType == "Office")
                            {
                                xxx.OfficePhoneNumbers.Add(rccboc);
                            }
                        }
                        if (xxx.SelularPhoneNumbers != null)
                        {
                            transaction.RetailCIFCBO.SelularPhoneNumbers = xxx.SelularPhoneNumbers;
                        }
                        if (xxx.HomePhoneNumbers != null)
                        {
                            transaction.RetailCIFCBO.HomePhoneNumbers = xxx.HomePhoneNumbers;
                        }
                        if (xxx.OfficePhoneNumbers != null)
                        {
                            transaction.RetailCIFCBO.OfficePhoneNumbers = xxx.OfficePhoneNumbers;
                        }
                    }
                    #endregion
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;

        }
        public bool GetTransactionChangeRMDraftByID(long id, ref TransactionChangeRMDraftModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                TransactionChangeRMDraftModel datatransaction = (from a in context.TransactionDrafts
                                                                 where a.TransactionID.Equals(id)
                                                                 select new TransactionChangeRMDraftModel
                                                                 {
                                                                     ID = a.TransactionID,
                                                                     ApplicationID = a.ApplicationID,
                                                                     CreateDate = a.CreateDate,
                                                                     LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                     LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                                                     IsBringupTask = a.IsBringUp,
                                                                     IsChangeRM = a.IsChangeRM,
                                                                     IsSegment = a.IsSegment,
                                                                     IsSOLID = a.IsSolID,
                                                                     IsDraft = a.IsDraft,
                                                                 }).SingleOrDefault();

                // fill CIF Retail data
                if (datatransaction != null)
                {
                    var data = context.TransactionDrafts.Where(x => x.TransactionID.Equals(datatransaction.ID)).Select(x => x).FirstOrDefault();

                    string cif = context.TransactionDrafts.Where(a => a.TransactionID.Equals(id)).Select(a => a.CIF).FirstOrDefault();
                    if (!string.IsNullOrEmpty(cif))
                    {
                        ICustomerRepository customerRepo = new CustomerRepository();
                        CustomerModel customer = new CustomerModel();
                        if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                        {
                            datatransaction.Customer = customer;
                        }

                        customerRepo.Dispose();
                        customer = null;
                    }

                    IList<ChangeRMModel> ChangeRMTransactionDraft = (from a in context.ChangeRMTransactionDrafts
                                                                     where a.TransactionID == datatransaction.ID
                                                                     select new ChangeRMModel
                                                                     {
                                                                         Selected = true,
                                                                         ChangeRMTransactionID = a.ChangeRMTransactionDraftID,
                                                                         TransactionID = a.TransactionID,
                                                                         CIF = a.CIF,
                                                                         CustomerName = a.Customer.CustomerName,
                                                                         ChangeSegmentFrom = a.ChangeSegmentFrom,
                                                                         ChangeSegmentTo = a.ChangeSegmentTo,
                                                                         ChangeFromBranchCode = a.ChangeFromBranchCode,
                                                                         ChangeFromRMCode = a.ChangeFromRMCode,
                                                                         ChangeFromBU = a.ChangeFromBU,
                                                                         ChangeToBranchCode = a.ChangeToBranchCode,
                                                                         ChangeToRMCode = a.ChangeToRMCode,
                                                                         ChangeToBU = a.ChangeToBU,
                                                                         Account = a.Account,
                                                                         PCCode = a.PCCode,
                                                                         EATPB = a.EATPB,
                                                                         Remarks = a.Remarks,
                                                                         CreateDate = a.CreateDate,
                                                                         CreateBy = a.CreateBy
                                                                     }).ToList();
                    if (ChangeRMTransactionDraft != null)
                    {
                        datatransaction.ChangeRM = ChangeRMTransactionDraft;
                    }

                    IList<TransactionDocumentDraftModel> DocDraft = (from doc in context.TransactionDocumentDrafts
                                                                     where doc.TransactionID == id && doc.IsDeleted.Equals(false)
                                                                     select new TransactionDocumentDraftModel
                                                                     {
                                                                         ID = doc.TransactionDocumentID,
                                                                         IsDormant = doc.IsDormant,
                                                                         LastModifiedBy = doc.UpdateBy == null ? doc.CreateBy : doc.UpdateBy,
                                                                         LastModifiedDate = doc.UpdateDate == null ? doc.CreateDate : doc.CreateDate,
                                                                         Purpose = new DocumentPurposeModel { ID = doc.DocumentPurpose.PurposeID, Name = doc.DocumentPurpose.PurposeName },
                                                                         Type = new DocumentTypeModel { ID = doc.DocumentType.DocTypeID, Name = doc.DocumentType.DocTypeName },
                                                                         FileName = doc.Filename,
                                                                         DocumentPath = new DocumentPathModel { name = doc.Filename, lastModified = string.Empty, lastModifiedDate = DateTime.Now, DocPath = doc.DocumentPath, type = Util.ExistingDoctype }
                                                                     }).ToList();
                    if (DocDraft != null)
                    {
                        datatransaction.Documents = DocDraft;
                    }

                    transaction = datatransaction;
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }

        public bool DeleteTransactionDraftByID(int id, ref string Message)
        {
            bool isDelete = false;
            using (DBSEntities context_ts = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        Entity.TransactionDraft data = context.TransactionDrafts.Where(a => a.TransactionID.Equals(id)).SingleOrDefault();
                        if (data != null)
                        {
                            Entity.TransactionBringupTask dataBringup = context.TransactionBringupTasks.Where(a => a.TransactionID.Equals(id)).FirstOrDefault();
                            if (dataBringup != null)
                            {
                                data.IsDeleted = true;
                                context.SaveChanges();

                                dataBringup.DateOut = DateTime.UtcNow;
                                dataBringup.IsSubmitted = true;
                                dataBringup.SubmittedBy = currentUser.GetCurrentUser().LoginName;
                                dataBringup.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                dataBringup.UpdateDate = DateTime.UtcNow;
                                context.SaveChanges();
                            }
                            else
                            {

                                var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(id)).ToList();
                                if (existingDocs != null)
                                {
                                    if (existingDocs.Count() > 0)
                                    {
                                        foreach (var item in existingDocs)
                                        {
                                            var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                            context.TransactionDocumentDrafts.Remove(deleteDoc);
                                            context.SaveChanges();
                                        }
                                    }
                                }
                                data.IsDeleted = true;
                                context.SaveChanges();
                            }
                            isDelete = true;
                        }
                        else
                        {
                            Message = string.Format("Draft ID {0} is does not exist.", id);
                            isDelete = false;
                        }
                        ts.Complete();
                    }
                    catch (Exception ex)
                    {
                        ts.Dispose();
                        Message = ex.Message;
                    }
                }
            }
            return isDelete;
        }
        public bool UpdateTransactionDraft(long id, TransactionCIFModel transactionCIF, ref string message)
        {
            bool isSuccess = false;
            long RetailCIFCBODraftID = 0;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.TransactionDraft data = context.TransactionDrafts.Where(a => a.TransactionID.Equals(id)).SingleOrDefault();
                    if (data != null)
                    {
                        #region Primary Field
                        data.CIF = transactionCIF.Customer.CIF;
                        data.ProductID = transactionCIF.Product.ID;
                        data.TransactionID = transactionCIF.ID;
                        data.ApplicationID = transactionCIF.ApplicationID;
                        data.IsDraft = transactionCIF.IsDraft;
                        data.IsNewCustomer = transactionCIF.IsNewCustomer;
                        data.CreateDate = DateTime.UtcNow;
                        data.CreateBy = currentUser.GetCurrentUser().LoginName;
                        data.ApplicationDate = DateTime.UtcNow;
                        data.CurrencyID = (int)CurrencyID.NullCurrency;
                        data.Amount = 0;
                        data.IsTopUrgent = 0;
                        data.StateID = (int)StateID.OnProgress;
                        data.StaffID = transactionCIF.StaffID;
                        data.StaffTaggingID = transactionCIF.StaffTagging.ID;
                        data.ATMNumber = transactionCIF.ATMNumber;
                        data.IsLOI = transactionCIF.IsLOI;
                        data.IsPOI = transactionCIF.IsPOI;
                        data.IsPOA = transactionCIF.IsPOA;
                        data.LocationName = transactionCIF.BrachRiskRating.Name;
                        data.AccountNumber = (transactionCIF.AccountNumber == null ? null : transactionCIF.AccountNumber.AccountNumber);
                        #endregion

                        if (transactionCIF.TransactionType.TransTypeID == 0)
                            data.TransactionTypeID = null;
                        else
                            data.TransactionTypeID = transactionCIF.TransactionType.TransTypeID;

                        if (transactionCIF.MaintenanceType.ID == 0)
                            data.CBOMaintainID = null;
                        else
                            data.CBOMaintainID = (transactionCIF.MaintenanceType.ID == 0 ? int.Parse(null) : transactionCIF.MaintenanceType.ID);

                        if (transactionCIF.TransactionSubType.ID == 0)
                            data.TransactionSubTypeID = null;
                        else
                            data.TransactionSubTypeID = (transactionCIF.TransactionSubType.ID == 0 ? 1 : transactionCIF.TransactionSubType.ID);

                    }
                    context.SaveChanges();

                    #region RetailCIFCBODraft
                    Entity.RetailCIFCBODraft dataRetailDraft = context.RetailCIFCBODrafts.Where(a => a.TransactionID.Equals(id)).FirstOrDefault();

                    if (dataRetailDraft != null)
                    {
                        dataRetailDraft.RequestTypeID = transactionCIF.TransactionType.TransTypeID;
                        dataRetailDraft.MaintenanceTypeID = transactionCIF.MaintenanceType.ID;

                        dataRetailDraft.IsNameMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsNameMaintenance;
                        dataRetailDraft.IsIdentityTypeMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsIdentityTypeMaintenance;
                        dataRetailDraft.IsNPWPMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsNPWPMaintenance;
                        dataRetailDraft.IsMaritalStatusMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsMaritalStatusMaintenance;
                        dataRetailDraft.IsCorrespondenceMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsCorrespondenceMaintenance;
                        dataRetailDraft.IsIdentityAddressMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsIdentityAddressMaintenance;
                        dataRetailDraft.IsOfficeAddressMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsOfficeAddressMaintenance;
                        dataRetailDraft.IsCorrespondenseAddressMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsCorrespondenseAddressMaintenance;
                        dataRetailDraft.IsPhoneFaxEmailMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsPhoneFaxEmailMaintenance;
                        dataRetailDraft.IsNationalityMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsNationalityMaintenance;
                        dataRetailDraft.IsFundSourceMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsFundSourceMaintenance;
                        dataRetailDraft.IsNetAssetMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsNetAssetMaintenance;
                        dataRetailDraft.IsMonthlyIncomeMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsMonthlyIncomeMaintenance;
                        dataRetailDraft.IsJobMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsJobMaintenance;
                        dataRetailDraft.IsAccountPurposeMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsAccountPurposeMaintenance;
                        dataRetailDraft.IsMonthlyTransactionMaintenance = transactionCIF.RetailCIFCBO.GroupCheckBox.IsMonthlyTransactionMaintenance;
                        dataRetailDraft.IsPekerjaanProfesional = transactionCIF.RetailCIFCBO.GroupCheckBox.IsPekerjaanProfesional;
                        dataRetailDraft.IsPekerjaanLainnya = transactionCIF.RetailCIFCBO.GroupCheckBox.IsPekerjaanLainnya;
                        dataRetailDraft.IsSumberDanaLainnya = transactionCIF.RetailCIFCBO.GroupCheckBox.IsSumberDanaLainnya;
                        dataRetailDraft.IsTujuanBukaRekeningLainnya = transactionCIF.RetailCIFCBO.GroupCheckBox.IsTujuanBukaRekeningLainnya;
                        dataRetailDraft.IsOthers = transactionCIF.RetailCIFCBO.GroupCheckBox.IsOthers;

                        //transactionCIF.RetailCIFCBO                              
                        dataRetailDraft.AccountNumber = transactionCIF.RetailCIFCBO.AccountNumber;
                        dataRetailDraft.Name = transactionCIF.RetailCIFCBO.Name;
                        dataRetailDraft.IdentityTypeID = transactionCIF.RetailCIFCBO.IdentityTypeID.ID;
                        dataRetailDraft.IdentityNumber = transactionCIF.RetailCIFCBO.IdentityNumber;
                        dataRetailDraft.IdentityStartDate = transactionCIF.RetailCIFCBO.IdentityStartDate;
                        dataRetailDraft.IdentityEndDate = transactionCIF.RetailCIFCBO.IdentityEndDate;
                        dataRetailDraft.IdentityAddress = transactionCIF.RetailCIFCBO.IdentityAddress;
                        dataRetailDraft.IdentityKelurahan = transactionCIF.RetailCIFCBO.IdentityKelurahan;
                        dataRetailDraft.IdentityKecamatan = transactionCIF.RetailCIFCBO.IdentityKecamatan;
                        dataRetailDraft.IdentityCity = transactionCIF.RetailCIFCBO.IdentityCity;
                        dataRetailDraft.IdentityProvince = transactionCIF.RetailCIFCBO.IdentityProvince;
                        dataRetailDraft.IdentityCountry = transactionCIF.RetailCIFCBO.IdentityCountry;
                        dataRetailDraft.IdentityPostalCode = transactionCIF.RetailCIFCBO.IdentityPostalCode;

                        dataRetailDraft.IdentityTypeID2 = transactionCIF.RetailCIFCBO.IdentityTypeID2.ID;
                        dataRetailDraft.IdentityNumber2 = transactionCIF.RetailCIFCBO.IdentityNumber2;
                        dataRetailDraft.IdentityStartDate2 = transactionCIF.RetailCIFCBO.IdentityStartDate2;
                        dataRetailDraft.IdentityEndDate2 = transactionCIF.RetailCIFCBO.IdentityEndDate2;
                        dataRetailDraft.IdentityAddress2 = transactionCIF.RetailCIFCBO.IdentityAddress2;
                        dataRetailDraft.IdentityKelurahan2 = transactionCIF.RetailCIFCBO.IdentityKelurahan2;
                        dataRetailDraft.IdentityKecamatan2 = transactionCIF.RetailCIFCBO.IdentityKecamatan2;
                        dataRetailDraft.IdentityCity2 = transactionCIF.RetailCIFCBO.IdentityCity2;
                        dataRetailDraft.IdentityProvince2 = transactionCIF.RetailCIFCBO.IdentityProvince2;
                        dataRetailDraft.IdentityCountry2 = transactionCIF.RetailCIFCBO.IdentityCountry2;
                        dataRetailDraft.IdentityPostalCode2 = transactionCIF.RetailCIFCBO.IdentityPostalCode2;

                        dataRetailDraft.IdentityTypeID3 = transactionCIF.RetailCIFCBO.IdentityTypeID3.ID;
                        dataRetailDraft.IdentityNumber3 = transactionCIF.RetailCIFCBO.IdentityNumber3;
                        dataRetailDraft.IdentityStartDate3 = transactionCIF.RetailCIFCBO.IdentityStartDate3;
                        dataRetailDraft.IdentityEndDate3 = transactionCIF.RetailCIFCBO.IdentityEndDate3;
                        dataRetailDraft.IdentityAddress3 = transactionCIF.RetailCIFCBO.IdentityAddress3;
                        dataRetailDraft.IdentityKelurahan3 = transactionCIF.RetailCIFCBO.IdentityKelurahan3;
                        dataRetailDraft.IdentityKecamatan3 = transactionCIF.RetailCIFCBO.IdentityKecamatan3;
                        dataRetailDraft.IdentityCity3 = transactionCIF.RetailCIFCBO.IdentityCity3;
                        dataRetailDraft.IdentityProvince3 = transactionCIF.RetailCIFCBO.IdentityProvince3;
                        dataRetailDraft.IdentityCountry3 = transactionCIF.RetailCIFCBO.IdentityCountry3;
                        dataRetailDraft.IdentityPostalCode3 = transactionCIF.RetailCIFCBO.IdentityPostalCode3;

                        dataRetailDraft.IdentityTypeID4 = transactionCIF.RetailCIFCBO.IdentityTypeID4.ID;
                        dataRetailDraft.IdentityNumber4 = transactionCIF.RetailCIFCBO.IdentityNumber4;
                        dataRetailDraft.IdentityStartDate4 = transactionCIF.RetailCIFCBO.IdentityStartDate4;
                        dataRetailDraft.IdentityEndDate4 = transactionCIF.RetailCIFCBO.IdentityEndDate4;
                        dataRetailDraft.IdentityAddress4 = transactionCIF.RetailCIFCBO.IdentityAddress4;
                        dataRetailDraft.IdentityKelurahan4 = transactionCIF.RetailCIFCBO.IdentityKelurahan4;
                        dataRetailDraft.IdentityKecamatan4 = transactionCIF.RetailCIFCBO.IdentityKecamatan4;
                        dataRetailDraft.IdentityCity4 = transactionCIF.RetailCIFCBO.IdentityCity4;
                        dataRetailDraft.IdentityProvince4 = transactionCIF.RetailCIFCBO.IdentityProvince4;
                        dataRetailDraft.IdentityCountry4 = transactionCIF.RetailCIFCBO.IdentityCountry4;
                        dataRetailDraft.IdentityPostalCode4 = transactionCIF.RetailCIFCBO.IdentityPostalCode4;


                        dataRetailDraft.NPWPNumber = transactionCIF.RetailCIFCBO.NPWPNumber;
                        dataRetailDraft.IsNPWPReceived = transactionCIF.RetailCIFCBO.IsNPWPReceived;
                        dataRetailDraft.SpouseName = transactionCIF.RetailCIFCBO.SpouseName;
                        dataRetailDraft.IsCorrespondenseToEmail = transactionCIF.RetailCIFCBO.IsCorrespondenseToEmail;
                        dataRetailDraft.CorrespondenseAddress = transactionCIF.RetailCIFCBO.CorrespondenseAddress;
                        dataRetailDraft.CorrespondenseKelurahan = transactionCIF.RetailCIFCBO.CorrespondenseKelurahan;
                        dataRetailDraft.CorrespondenseKecamatan = transactionCIF.RetailCIFCBO.CorrespondenseKecamatan;
                        dataRetailDraft.CorrespondenseCity = transactionCIF.RetailCIFCBO.CorrespondenseCity;
                        dataRetailDraft.CorrespondenseProvince = transactionCIF.RetailCIFCBO.CorrespondenseProvince;
                        dataRetailDraft.CorrespondenseCountry = transactionCIF.RetailCIFCBO.CorrespondenseCountry;
                        dataRetailDraft.CorrespondensePostalCode = transactionCIF.RetailCIFCBO.CorrespondensePostalCode;
                        dataRetailDraft.CellPhoneMethodID = transactionCIF.RetailCIFCBO.CellPhoneMethodID.ID;
                        dataRetailDraft.CellPhone = transactionCIF.RetailCIFCBO.CellPhone;
                        dataRetailDraft.UpdatedCellPhone = transactionCIF.RetailCIFCBO.UpdatedCellPhone;
                        dataRetailDraft.HomePhoneMethodID = transactionCIF.RetailCIFCBO.HomePhoneMethodID.ID;
                        dataRetailDraft.HomePhone = transactionCIF.RetailCIFCBO.HomePhone;
                        dataRetailDraft.UpdatedHomePhone = transactionCIF.RetailCIFCBO.UpdatedHomePhone;
                        dataRetailDraft.OfficePhoneMethodID = transactionCIF.RetailCIFCBO.OfficePhoneMethodID.ID;
                        dataRetailDraft.OfficePhone = transactionCIF.RetailCIFCBO.OfficePhone;
                        dataRetailDraft.UpdatedOfficePhone = transactionCIF.RetailCIFCBO.UpdatedOfficePhone;
                        dataRetailDraft.FaxMethodID = transactionCIF.RetailCIFCBO.FaxMethodID.ID;
                        dataRetailDraft.Fax = transactionCIF.RetailCIFCBO.Fax;
                        dataRetailDraft.UpdatedFax = transactionCIF.RetailCIFCBO.UpdatedFax;
                        dataRetailDraft.EmailAddress = transactionCIF.RetailCIFCBO.EmailAddress;
                        dataRetailDraft.OfficeAddress = transactionCIF.RetailCIFCBO.OfficeAddress;
                        dataRetailDraft.OfficeKelurahan = transactionCIF.RetailCIFCBO.OfficeKelurahan;
                        dataRetailDraft.OfficeKecamatan = transactionCIF.RetailCIFCBO.OfficeKecamatan;
                        dataRetailDraft.OfficeCity = transactionCIF.RetailCIFCBO.OfficeCity;
                        dataRetailDraft.OfficeProvince = transactionCIF.RetailCIFCBO.OfficeProvince;
                        dataRetailDraft.OfficeCountry = transactionCIF.RetailCIFCBO.OfficeCountry;
                        dataRetailDraft.OfficePostalCode = transactionCIF.RetailCIFCBO.OfficePostalCode;
                        dataRetailDraft.Nationality = transactionCIF.RetailCIFCBO.Nationality;
                        dataRetailDraft.UBOName = transactionCIF.RetailCIFCBO.UBOName;
                        dataRetailDraft.UBOIdentityType = transactionCIF.RetailCIFCBO.UBOIdentityType;
                        dataRetailDraft.UBOPhone = transactionCIF.RetailCIFCBO.UBOPhone;
                        dataRetailDraft.UBOJob = transactionCIF.RetailCIFCBO.UBOJob;
                        dataRetailDraft.CreateDate = DateTime.UtcNow;
                        dataRetailDraft.CreateBy = currentUser.GetCurrentUser().LoginName;
                        dataRetailDraft.AtmNumber = transactionCIF.RetailCIFCBO.AtmNumber;
                        dataRetailDraft.PekerjaanProfesional = string.IsNullOrEmpty(transactionCIF.RetailCIFCBO.PekerjaanProfesional) ? "" : transactionCIF.RetailCIFCBO.PekerjaanProfesional;
                        dataRetailDraft.PekerjaanLainnya = string.IsNullOrEmpty(transactionCIF.RetailCIFCBO.PekerjaanLainnya) ? "" : transactionCIF.RetailCIFCBO.PekerjaanLainnya;
                        dataRetailDraft.SumberDanaLainnya = string.IsNullOrEmpty(transactionCIF.RetailCIFCBO.SumberDanaLainnya) ? "" : transactionCIF.RetailCIFCBO.SumberDanaLainnya;
                        dataRetailDraft.TujuanBukaRekeningLainnya = string.IsNullOrEmpty(transactionCIF.RetailCIFCBO.TujuanBukaRekeningLainnya) ? "" : transactionCIF.RetailCIFCBO.TujuanBukaRekeningLainnya;

                        //Parameter System for pengkinian data
                        dataRetailDraft.MaritalStatusID = transactionCIF.RetailCIFCBO.MaritalStatusID.ID;
                        // FundSource = transactionCIF.RetailCIFCBO.FundSource.ID;
                        dataRetailDraft.NetAsset = transactionCIF.RetailCIFCBO.NetAsset.ID;
                        dataRetailDraft.MonthlyIncome = transactionCIF.RetailCIFCBO.MonthlyIncome.ID;
                        dataRetailDraft.MonthlyExtraIncome = transactionCIF.RetailCIFCBO.MonthlyExtraIncome.ID;
                        dataRetailDraft.Job = transactionCIF.RetailCIFCBO.Job.ID;
                        dataRetailDraft.AccountPurpose = transactionCIF.RetailCIFCBO.AccountPurpose.ID;
                        dataRetailDraft.IncomeForecast = transactionCIF.RetailCIFCBO.IncomeForecast.ID;
                        dataRetailDraft.OutcomeForecast = transactionCIF.RetailCIFCBO.OutcomeForecast.ID;
                        dataRetailDraft.TransactionForecast = transactionCIF.RetailCIFCBO.TransactionForecast.ID;
                        dataRetailDraft.DispatchModeTypeID = transactionCIF.RetailCIFCBO.DispatchModeType.ID;
                        dataRetailDraft.DispatchModeOther = transactionCIF.RetailCIFCBO.DispatchModeOther;
                        dataRetailDraft.IsPenawaranProductPerbankan = transactionCIF.RetailCIFCBO.IsPenawaranProductPerbankan;
                        dataRetailDraft.IsStampDuty = transactionCIF.RetailCIFCBO.IsStampDuty;
                        dataRetailDraft.IsSignature = transactionCIF.RetailCIFCBO.IsSignature;

                        //Field-field feed from Risk Rating Form
                        dataRetailDraft.RiskRatingReportDate = transactionCIF.RetailCIFCBO.ReportDate;
                        //RiskRatingNextReviewDate = transactionCIF.RetailCIFCBO.NextReviewDate;
                        dataRetailDraft.RiskRatingResultID = transactionCIF.RetailCIFCBO.RiskRatingResult.ID;
                        dataRetailDraft.CompanyName = transactionCIF.RetailCIFCBO.CompanyName;
                        dataRetailDraft.Position = transactionCIF.RetailCIFCBO.Position;
                        dataRetailDraft.WorkPeriod = transactionCIF.RetailCIFCBO.WorkPeriod;
                        dataRetailDraft.IndustryType = transactionCIF.RetailCIFCBO.IndustryType;
                        dataRetailDraft.HubunganNasabah = transactionCIF.RetailCIFCBO.HubunganNasabah;
                        dataRetailDraft.FundSource = transactionCIF.RetailCIFCBO.FundSource.ID;

                        dataRetailDraft.IsMotherMaiden = transactionCIF.RetailCIFCBO.GroupCheckBox.IsMotherMaiden;
                        dataRetailDraft.IsEducation = transactionCIF.RetailCIFCBO.GroupCheckBox.IsEducation;
                        dataRetailDraft.IsReligion = transactionCIF.RetailCIFCBO.GroupCheckBox.IsReligion;
                        dataRetailDraft.IsDataUBO = transactionCIF.RetailCIFCBO.GroupCheckBox.IsDataUBO;
                        dataRetailDraft.IsTransparansiDataPenawaranProduct = transactionCIF.RetailCIFCBO.GroupCheckBox.IsTransparansiDataPenawaranProduct;
                        dataRetailDraft.IsTransparansiPenggunaan = transactionCIF.RetailCIFCBO.GroupCheckBox.IsTransparansiPenggunaan;
                        dataRetailDraft.IsPenawaranProduct = transactionCIF.RetailCIFCBO.GroupCheckBox.IsPenawaranProduct;
                        dataRetailDraft.IsStampDuty = transactionCIF.RetailCIFCBO.GroupCheckBox.IsStampDuty;
                        dataRetailDraft.IsSignature = transactionCIF.RetailCIFCBO.GroupCheckBox.IsSignature;
                        dataRetailDraft.Education = transactionCIF.RetailCIFCBO.Education;
                        dataRetailDraft.Religion = transactionCIF.RetailCIFCBO.Religion;
                        dataRetailDraft.MotherMaiden = transactionCIF.RetailCIFCBO.MotherMaiden;
                        dataRetailDraft.CIFUBO = transactionCIF.RetailCIFCBO.DataUBOCif;
                        dataRetailDraft.UBOName = transactionCIF.RetailCIFCBO.DataUBOName;
                        dataRetailDraft.UBORelationship = transactionCIF.RetailCIFCBO.DataUBORelationship;
                        dataRetailDraft.IsSLIK = transactionCIF.RetailCIFCBO.GroupCheckBox.IsSLIK;
                        dataRetailDraft.GrosIncomeccy = transactionCIF.RetailCIFCBO.GrosIncomeccy;
                        dataRetailDraft.GrossIncomeAmount = transactionCIF.RetailCIFCBO.GrossIncomeAmount;
                        dataRetailDraft.GrossCifSpouse = transactionCIF.RetailCIFCBO.GrossCifSpouse;
                        dataRetailDraft.GrossSpouseName = transactionCIF.RetailCIFCBO.GrossSpouseName;
                        dataRetailDraft.GrossSpouseRelation = transactionCIF.RetailCIFCBO.GrossSpouseRelation;
                        dataRetailDraft.IsChangeSignature = transactionCIF.RetailCIFCBO.GroupCheckBox.IsChangeSignature;
                        dataRetailDraft.ChangeSignature = transactionCIF.RetailCIFCBO.GroupCheckBox.ChangeSignature;
                        dataRetailDraft.FatcaCountryCode = transactionCIF.RetailCIFCBO.FatcaCountryCode;
                        dataRetailDraft.FatcaReviewStatus = transactionCIF.RetailCIFCBO.FatcaReviewStatus;
                        dataRetailDraft.FatcaCRSStatus = transactionCIF.RetailCIFCBO.FatcaCRSStatus;
                        dataRetailDraft.FatcaTaxPayer = transactionCIF.RetailCIFCBO.FatcaTaxPayer;
                        dataRetailDraft.FatcaWithHolding = transactionCIF.RetailCIFCBO.FatcaWithHolding;
                        dataRetailDraft.FatcaDateOnForm = transactionCIF.RetailCIFCBO.FatcaDateOnForm;
                        dataRetailDraft.FatcaReviewStatusDate = transactionCIF.RetailCIFCBO.FatcaReviewStatusDate;
                        dataRetailDraft.FatcaTaxPayerID = transactionCIF.RetailCIFCBO.FatcaTaxPayerID;
                        dataRetailDraft.IsFatcaCRS = transactionCIF.RetailCIFCBO.GroupCheckBox.IsFatcaCRS;

                        dataRetailDraft.AmountLienUnlien = transactionCIF.RetailCIFCBO.AmountLienUnlien;
                        dataRetailDraft.Tier = transactionCIF.RetailCIFCBO.Tier;

                        context.SaveChanges();

                    }
                    #endregion
                    RetailCIFCBODraftID = dataRetailDraft.RetailCIFCBODraftID;
                    #region RetailCIFCBOContact dayat
                    context.RetailCIFCBOContactDrafts.RemoveRange(context.RetailCIFCBOContactDrafts.Where(xx => xx.RetailCIFCBODraftID == dataRetailDraft.RetailCIFCBODraftID));
                    context.SaveChanges();
                    DBS.Entity.RetailCIFCBOContactDraft dataRetailCIFContactDraft = new DBS.Entity.RetailCIFCBOContactDraft();
                    //selular
                    if (transactionCIF.RetailCIFCBO.SelularPhoneNumbers != null)
                    {
                        foreach (var item in transactionCIF.RetailCIFCBO.SelularPhoneNumbers)
                        {
                            if (item.ActionType != "")
                            {
                                dataRetailCIFContactDraft.RetailCIFCBODraftID = dataRetailDraft.RetailCIFCBODraftID;
                                dataRetailCIFContactDraft.ActionType = item.ActionType;
                                dataRetailCIFContactDraft.ContactType = item.ContactType;
                                dataRetailCIFContactDraft.PhoneNumber = item.PhoneNumber;
                                dataRetailCIFContactDraft.CityCode = item.CityCode;
                                dataRetailCIFContactDraft.CountryCode = item.CountryCode;
                                dataRetailCIFContactDraft.CreatedDate = DateTime.UtcNow;
                                dataRetailCIFContactDraft.CreatedBy = currentUser.GetCurrentUser().LoginName;
                                dataRetailCIFContactDraft.UpdateCountryCode = null;
                                dataRetailCIFContactDraft.UpdateCityCode = null;
                                dataRetailCIFContactDraft.UpdatePhoneNumber = null;
                                dataRetailCIFContactDraft.RetailCIFCBOContactUpdateID = null;
                                if (item.ActionType == "2")
                                {
                                    dataRetailCIFContactDraft.UpdateCountryCode = item.UpdateCountryCode;
                                    dataRetailCIFContactDraft.UpdateCityCode = item.UpdateCityCode;
                                    dataRetailCIFContactDraft.UpdatePhoneNumber = item.UpdatePhoneNumber;
                                    dataRetailCIFContactDraft.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                }
                                if (item.ActionType == "3")
                                {
                                    dataRetailCIFContactDraft.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                }
                                context.RetailCIFCBOContactDrafts.Add(dataRetailCIFContactDraft);
                                context.SaveChanges();
                            }
                        }
                    }
                    //oofice
                    if (transactionCIF.RetailCIFCBO.OfficePhoneNumbers != null)
                    {
                        foreach (var item in transactionCIF.RetailCIFCBO.OfficePhoneNumbers)
                        {
                            if (item.ActionType != "")
                            {
                                dataRetailCIFContactDraft.RetailCIFCBODraftID = dataRetailDraft.RetailCIFCBODraftID;
                                dataRetailCIFContactDraft.ActionType = item.ActionType;
                                dataRetailCIFContactDraft.ContactType = item.ContactType;
                                dataRetailCIFContactDraft.PhoneNumber = item.PhoneNumber;
                                dataRetailCIFContactDraft.CityCode = item.CityCode;
                                dataRetailCIFContactDraft.CountryCode = item.CountryCode;
                                dataRetailCIFContactDraft.CreatedDate = DateTime.UtcNow;
                                dataRetailCIFContactDraft.CreatedBy = currentUser.GetCurrentUser().LoginName;
                                dataRetailCIFContactDraft.UpdateCountryCode = null;
                                dataRetailCIFContactDraft.UpdateCityCode = null;
                                dataRetailCIFContactDraft.UpdatePhoneNumber = null;
                                dataRetailCIFContactDraft.RetailCIFCBOContactUpdateID = null;
                                if (item.ActionType == "2")
                                {
                                    dataRetailCIFContactDraft.UpdateCountryCode = item.UpdateCountryCode;
                                    dataRetailCIFContactDraft.UpdateCityCode = item.UpdateCityCode;
                                    dataRetailCIFContactDraft.UpdatePhoneNumber = item.UpdatePhoneNumber;
                                    dataRetailCIFContactDraft.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                }
                                if (item.ActionType == "3")
                                {
                                    dataRetailCIFContactDraft.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                }
                                context.RetailCIFCBOContactDrafts.Add(dataRetailCIFContactDraft);
                                context.SaveChanges();
                            }
                        }
                    }
                    //Home
                    if (transactionCIF.RetailCIFCBO.HomePhoneNumbers != null)
                    {
                        foreach (var item in transactionCIF.RetailCIFCBO.HomePhoneNumbers)
                        {
                            if (item.ActionType != "")
                            {
                                dataRetailCIFContactDraft.RetailCIFCBODraftID = dataRetailDraft.RetailCIFCBODraftID;
                                dataRetailCIFContactDraft.ActionType = item.ActionType;
                                dataRetailCIFContactDraft.ContactType = item.ContactType;
                                dataRetailCIFContactDraft.PhoneNumber = item.PhoneNumber;
                                dataRetailCIFContactDraft.CityCode = item.CityCode;
                                dataRetailCIFContactDraft.CountryCode = item.CountryCode;
                                dataRetailCIFContactDraft.CreatedDate = DateTime.UtcNow;
                                dataRetailCIFContactDraft.CreatedBy = currentUser.GetCurrentUser().LoginName;
                                dataRetailCIFContactDraft.UpdateCountryCode = null;
                                dataRetailCIFContactDraft.UpdateCityCode = null;
                                dataRetailCIFContactDraft.UpdatePhoneNumber = null;
                                dataRetailCIFContactDraft.RetailCIFCBOContactUpdateID = null;
                                if (item.ActionType == "2")
                                {
                                    dataRetailCIFContactDraft.UpdateCountryCode = item.UpdateCountryCode;
                                    dataRetailCIFContactDraft.UpdateCityCode = item.UpdateCityCode;
                                    dataRetailCIFContactDraft.UpdatePhoneNumber = item.UpdatePhoneNumber;
                                    dataRetailCIFContactDraft.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                }
                                if (item.ActionType == "3")
                                {
                                    dataRetailCIFContactDraft.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                }
                                context.RetailCIFCBOContactDrafts.Add(dataRetailCIFContactDraft);
                                context.SaveChanges();
                            }
                        }
                    }
                    #endregion

                    #region Customer Account Join
                    if (transactionCIF.AddJoinTableCustomerCIF.Count() > 0)
                    {

                        IList<Entity.RetailCIFCBOAccountJoinDraft> ExistingJoinTableCustomer = context.RetailCIFCBOAccountJoinDrafts.Where(t => t.TransactionID == transactionCIF.ID).ToList();

                        if (ExistingJoinTableCustomer != null)
                        {
                            foreach (var iJoinTableCustomer in ExistingJoinTableCustomer)
                            {
                                context.RetailCIFCBOAccountJoinDrafts.Remove(iJoinTableCustomer);
                            }
                            context.SaveChanges();

                            if (transactionCIF.AddJoinTableCustomerCIF != null)
                            {
                                if (transactionCIF.AddJoinTableCustomerCIF.Count() > 0)
                                {
                                    foreach (var iJoinTableCustomerNew in transactionCIF.AddJoinTableCustomerCIF)
                                    {
                                        Entity.RetailCIFCBOAccountJoinDraft dataRetailCIFCBOAccountJoin = new Entity.RetailCIFCBOAccountJoinDraft()
                                        {
                                            TransactionID = transactionCIF.ID,
                                            CIF = iJoinTableCustomerNew.CIF,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context.RetailCIFCBOAccountJoinDrafts.Add(dataRetailCIFCBOAccountJoin);
                                    }
                                    context.SaveChanges();
                                }
                            }
                        }

                    }
                    #endregion

                    #region FDD Account
                    if (transactionCIF.AddJoinTableFFDAcountCIF.Count() > 0)
                    {
                        IList<Entity.RetailCIFCBOAccNoDraft> ExistingJoinTableFFDAccount = context.RetailCIFCBOAccNoDrafts.Where(t => t.RetailCIFCBODraftID == RetailCIFCBODraftID).ToList();
                        if (ExistingJoinTableFFDAccount != null)
                        {
                            foreach (var iJoinTableFFDAccount in ExistingJoinTableFFDAccount)
                            {
                                context.RetailCIFCBOAccNoDrafts.Remove(iJoinTableFFDAccount);
                            }
                            context.SaveChanges();

                            if (transactionCIF.AddJoinTableFFDAcountCIF != null)
                            {
                                if (transactionCIF.AddJoinTableFFDAcountCIF.Count() > 0)
                                {
                                    foreach (var iJoinTableFFDAccountNew in transactionCIF.AddJoinTableFFDAcountCIF)
                                    {
                                        Entity.RetailCIFCBOAccNoDraft dataRetailCIFCBOAccount = new Entity.RetailCIFCBOAccNoDraft()
                                        {
                                            RetailCIFCBODraftID = RetailCIFCBODraftID,
                                            AccountNumber = iJoinTableFFDAccountNew.AccountNumber,
                                            IsFFD = iJoinTableFFDAccountNew.IsAddFFDAccount,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context.RetailCIFCBOAccNoDrafts.Add(dataRetailCIFCBOAccount);

                                    }
                                    context.SaveChanges();
                                }

                            }
                        }
                    }

                    #endregion

                    #region Account Join
                    if (transactionCIF.AddJoinTableAccountCIF.Count() > 0)
                    {
                        IList<Entity.RetailCIFCBOAccNoDraft> ExistingJoinTableAccount = context.RetailCIFCBOAccNoDrafts.Where(t => t.RetailCIFCBODraftID == RetailCIFCBODraftID).ToList();
                        if (ExistingJoinTableAccount != null)
                        {
                            foreach (var iJoinTableAccount in ExistingJoinTableAccount)
                            {
                                context.RetailCIFCBOAccNoDrafts.Remove(iJoinTableAccount);
                            }
                            context.SaveChanges();

                            if (transactionCIF.AddJoinTableAccountCIF != null)
                            {
                                if (transactionCIF.AddJoinTableAccountCIF.Count() > 0)
                                {
                                    foreach (var iJoinTableAccountNew in transactionCIF.AddJoinTableAccountCIF)
                                    {
                                        Entity.RetailCIFCBOAccNoDraft dataRetailCIFCBOAccount = new Entity.RetailCIFCBOAccNoDraft()
                                        {
                                            RetailCIFCBODraftID = RetailCIFCBODraftID,
                                            AccountNumber = iJoinTableAccountNew.AccountNumber,
                                            CurrencyID = iJoinTableAccountNew.Currency.ID,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context.RetailCIFCBOAccNoDrafts.Add(dataRetailCIFCBOAccount);

                                    }
                                    context.SaveChanges();
                                }

                            }
                        }

                    }
                    #endregion

                    #region Dormant
                    if (transactionCIF.AddJoinTableDormantCIF.Count() > 0)
                    {
                        IList<Entity.RetailCIFCBOAccNoDraft> ExistingJoinTableDormant = context.RetailCIFCBOAccNoDrafts.Where(t => t.RetailCIFCBODraftID == RetailCIFCBODraftID).ToList();
                        if (ExistingJoinTableDormant != null)
                        {
                            foreach (var iJoinTableDormant in ExistingJoinTableDormant)
                            {
                                context.RetailCIFCBOAccNoDrafts.Remove(iJoinTableDormant);
                            }
                            context.SaveChanges();
                        }
                        if (transactionCIF.AddJoinTableDormantCIF != null)
                        {
                            if (transactionCIF.AddJoinTableDormantCIF.Count() > 0)
                            {
                                var SelectedDormant = transactionCIF.AddJoinTableDormantCIF.Where(s => s.IsAddDormantAccount == true);
                                if (SelectedDormant.Count() > 0)
                                {
                                    foreach (var iJoinDormantNew in SelectedDormant)
                                    {
                                        Entity.RetailCIFCBOAccNoDraft dataRetailCIFCBDormant = new Entity.RetailCIFCBOAccNoDraft()
                                        {
                                            RetailCIFCBODraftID = RetailCIFCBODraftID,
                                            AccountNumber = iJoinDormantNew.AccountNumber,
                                            CurrencyID = iJoinDormantNew.Currency.ID,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context.RetailCIFCBOAccNoDrafts.Add(dataRetailCIFCBDormant);
                                    }
                                    context.SaveChanges();
                                }
                            }
                        }

                    }

                    #endregion

                    #region Freeze Account
                    if (transactionCIF.AddJoinTableFreezeUnfreezeCIF.Count() > 0)
                    {
                        IList<Entity.RetailCIFCBOFreezeAccountDraft> ExistingJoinFreezeUnfreeze = context.RetailCIFCBOFreezeAccountDrafts.Where(t => t.RetailCIFCBODraftID == RetailCIFCBODraftID).ToList();
                        if (ExistingJoinFreezeUnfreeze != null)
                        {
                            foreach (var iJoinTableFreezeUnfreeze in ExistingJoinFreezeUnfreeze)
                            {
                                context.RetailCIFCBOFreezeAccountDrafts.Remove(iJoinTableFreezeUnfreeze);
                            }
                            context.SaveChanges();
                        }
                        if (transactionCIF.AddJoinTableFreezeUnfreezeCIF != null)
                        {
                            if (transactionCIF.AddJoinTableFreezeUnfreezeCIF.Count() > 0)
                            {
                                var SelectedFreeze = transactionCIF.AddJoinTableFreezeUnfreezeCIF.Where(s => s.IsAddTblFreezeAccount == true);
                                foreach (var iJoinTableFreezeUnfreezeNew in SelectedFreeze)
                                {
                                    Entity.RetailCIFCBOFreezeAccountDraft dataRetailFreezeUnfreezeAccount = new Entity.RetailCIFCBOFreezeAccountDraft()
                                    {
                                        RetailCIFCBODraftID = RetailCIFCBODraftID,
                                        AccountNumber = iJoinTableFreezeUnfreezeNew.AccountNumber,
                                        IsFreeze = iJoinTableFreezeUnfreezeNew.IsFreezeAccount != null ? iJoinTableFreezeUnfreezeNew.IsFreezeAccount : null,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName,
                                    };
                                    context.RetailCIFCBOFreezeAccountDrafts.Add(dataRetailFreezeUnfreezeAccount);
                                }
                                context.SaveChanges();
                            }
                        }

                    }
                    #endregion

                    #region Save Document
                    var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(transactionCIF.ID)).ToList();
                    if (existingDocs != null)
                    {
                        if (existingDocs.Count() > 0)
                        {
                            foreach (var item in existingDocs)
                            {
                                var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                deleteDoc.IsDeleted = true;
                                context.SaveChanges();
                            }
                        }
                    }
                    if (transactionCIF.DocumentsCIF.Count > 0)
                    {
                        foreach (var item in transactionCIF.DocumentsCIF)
                        {
                            Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                            {
                                TransactionID = data.TransactionID,
                                DocTypeID = item.Type.ID,
                                PurposeID = item.Purpose.ID,
                                Filename = item.FileName,
                                DocumentPath = item.DocumentPath,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName
                            };
                            context.TransactionDocumentDrafts.Add(document);
                        }
                        context.SaveChanges();
                    }
                    #endregion
                    ts.Complete();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }

            return isSuccess;
        }

        public bool AddTransactionChangeRM(TransactionChangeRMModel transactionChangeRM, ref long transactionID, ref string ApplicationID, ref string message)
        {
            bool IsSuccess = false;
            long TrIDInserted = 0;
            int? ActivityHistoryIDSelected = 0;
            string CIFSelected = string.Empty;
            string CustomerNameSelected = string.Empty;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (transactionChangeRM.IsDraft)
                    {
                        #region resumbit draf transaction
                        if (transactionChangeRM.ID != null && transactionChangeRM.ID > 0)
                        {
                            Entity.TransactionDraft data = context.TransactionDrafts.Where(a => a.TransactionID.Equals(transactionChangeRM.ID)).SingleOrDefault();
                            if (data != null)
                            {
                                #region Unused field but Not Null
                                data.BeneName = "-";
                                data.BankID = 1;
                                data.IsResident = false;
                                data.IsCitizen = false;
                                data.AmountUSD = 0.00M;
                                data.Rate = 0.00M;
                                data.DebitCurrencyID = 1;
                                data.BizSegmentID = 1;
                                data.ChannelID = 1;
                                data.Amount = 0;
                                data.IsTopUrgent = 0;
                                data.StateID = (int)StateID.OnProgress;
                                #endregion

                                #region Primary Field
                                data.CIF = transactionChangeRM.Customer.CIF;
                                data.ProductID = transactionChangeRM.Product.ID;
                                data.TransactionID = transactionChangeRM.ID;
                                data.ApplicationID = transactionChangeRM.ApplicationID;
                                data.IsDraft = transactionChangeRM.IsDraft;
                                data.IsNewCustomer = false;
                                data.ApplicationDate = DateTime.Now;
                                data.CurrencyID = (transactionChangeRM.Currency.ID != null && transactionChangeRM.Currency.ID != 0 ? transactionChangeRM.Currency.ID : (int)CurrencyID.NullCurrency);
                                data.AccountNumber = (transactionChangeRM.AccountNumber.AccountNumber == null ? null : transactionChangeRM.AccountNumber.AccountNumber);
                                data.StateID = (int)StateID.OnProgress;
                                data.BranchName = transactionChangeRM.BranchName;
                                data.IsChangeRM = transactionChangeRM.IsChangeRM;
                                data.IsSegment = transactionChangeRM.IsSegment;
                                data.IsSolID = transactionChangeRM.IsSOLID;
                                data.IsChangeRMTransaction = true;
                                #endregion
                                context.SaveChanges();

                                IList<Entity.TransactionDocumentDraft> existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(transactionChangeRM.ID)).ToList();
                                if (existingDocs != null)
                                {
                                    if (existingDocs.Count() > 0)
                                    {
                                        foreach (Entity.TransactionDocumentDraft item in existingDocs)
                                        {
                                            Entity.TransactionDocumentDraft deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                            deleteDoc.IsDeleted = true;
                                            context.SaveChanges();
                                        }
                                    }

                                }
                                if (transactionChangeRM.Documents != null)
                                {
                                    if (transactionChangeRM.Documents.Count() > 0)
                                    {
                                        foreach (TransactionDocumentModel item in transactionChangeRM.Documents)
                                        {
                                            Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                                            {
                                                TransactionID = data.TransactionID,
                                                DocTypeID = item.Type.ID,
                                                PurposeID = item.Purpose.ID,
                                                Filename = item.FileName,
                                                DocumentPath = item.DocumentPath,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName
                                            };

                                            context.TransactionDocumentDrafts.Add(document);
                                        }
                                        context.SaveChanges();
                                    }
                                }
                            }
                        }
                        #endregion
                        else
                        {
                            bool isBringup = false;
                            if (transactionChangeRM.IsBringupTask == true)
                                isBringup = true;
                            #region first time submit draft transaction
                            DBS.Entity.TransactionDraft data = new DBS.Entity.TransactionDraft()
                            {
                                BeneName = "-",
                                BankID = 1,
                                IsResident = false,
                                IsCitizen = false,
                                AmountUSD = 0.00M,
                                Rate = 0.00M,
                                DebitCurrencyID = 1,
                                BizSegmentID = 1,
                                ChannelID = 1,
                                Amount = 0,
                                IsTopUrgent = 0,
                                StateID = (int)StateID.OnProgress,

                                CIF = transactionChangeRM.Customer.CIF,
                                ProductID = transactionChangeRM.Product.ID,
                                TransactionID = transactionChangeRM.ID,
                                ApplicationID = transactionChangeRM.ApplicationID,
                                IsDraft = transactionChangeRM.IsDraft,
                                IsNewCustomer = false,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName,
                                ApplicationDate = DateTime.Now,
                                CurrencyID = (transactionChangeRM.Currency.ID != null && transactionChangeRM.Currency.ID != 0 ? transactionChangeRM.Currency.ID : (int)CurrencyID.NullCurrency),
                                AccountNumber = (transactionChangeRM.AccountNumber == null ? null : transactionChangeRM.AccountNumber.AccountNumber),
                                BranchName = transactionChangeRM.BranchName,
                                IsChangeRM = transactionChangeRM.IsChangeRM,
                                IsSegment = transactionChangeRM.IsSegment,
                                IsSolID = transactionChangeRM.IsSOLID,
                                IsChangeRMTransaction = true

                            };
                            data.IsBringUp = isBringup;

                            #endregion
                            if (transactionChangeRM.TransactionType.ID == 0)
                                data.TransactionTypeID = null;
                            else
                                data.TransactionTypeID = transactionChangeRM.TransactionType.ID;

                            if (transactionChangeRM.MaintenanceType.ID == 0)
                                data.CBOMaintainID = null;
                            else
                                data.CBOMaintainID = (transactionChangeRM.MaintenanceType.ID == 0 ? int.Parse(null) : transactionChangeRM.MaintenanceType.ID);

                            context.TransactionDrafts.Add(data);
                            context.SaveChanges();

                            transactionID = data.TransactionID;
                            TrIDInserted = transactionID;

                            if (transactionChangeRM.ChangeRM != null)
                            {
                                if (transactionChangeRM.ChangeRM.Count() > 0)
                                {
                                    foreach (var item in transactionChangeRM.ChangeRM)
                                    {
                                        if (item.Selected == true && !string.IsNullOrEmpty(item.CIF))
                                        {
                                            DBS.Entity.ChangeRMTransactionDraft ChangeRMSegmentSOLIDDraft = new DBS.Entity.ChangeRMTransactionDraft()
                                            {
                                                TransactionID = transactionID,
                                                CIF = item.CIF,
                                                ChangeSegmentFrom = !string.IsNullOrEmpty(item.ChangeSegmentFrom) ? item.ChangeSegmentFrom : "",
                                                ChangeSegmentTo = !string.IsNullOrEmpty(item.ChangeSegmentTo) ? item.ChangeSegmentTo : "",
                                                ChangeFromBranchCode = !string.IsNullOrEmpty(item.ChangeFromBranchCode) ? item.ChangeFromBranchCode : "",
                                                ChangeFromRMCode = !string.IsNullOrEmpty(item.ChangeFromRMCode) ? item.ChangeFromRMCode : "",
                                                ChangeFromBU = !string.IsNullOrEmpty(item.ChangeFromBU) ? item.ChangeFromBU : "",
                                                ChangeToBranchCode = !string.IsNullOrEmpty(item.ChangeToBranchCode) ? item.ChangeToBranchCode : "",
                                                ChangeToRMCode = !string.IsNullOrEmpty(item.ChangeToRMCode) ? item.ChangeToRMCode : "",
                                                ChangeToBU = !string.IsNullOrEmpty(item.ChangeToBU) ? item.ChangeToBU : "",
                                                Account = !string.IsNullOrEmpty(item.Account) ? item.Account : "",
                                                PCCode = !string.IsNullOrEmpty(item.PCCode) ? item.PCCode : "",
                                                EATPB = !string.IsNullOrEmpty(item.EATPB) ? item.EATPB : "",
                                                Remarks = !string.IsNullOrEmpty(item.Remarks) ? item.Remarks : "",
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName
                                            };

                                            context.ChangeRMTransactionDrafts.Add(ChangeRMSegmentSOLIDDraft);

                                            ActivityHistoryIDSelected = item.ActivityHistoryID;
                                            CIFSelected = item.CIF;
                                            CustomerNameSelected = item.CustomerName;

                                            IList<Entity.ChangeRM> ChangeRMSelected = context.ChangeRMs.Where(a => a.ActivityHistoryID == ActivityHistoryIDSelected && a.CIF == CIFSelected && a.CUSTOMER_NAME == CustomerNameSelected).ToList();

                                            if (ChangeRMSelected != null)
                                            {
                                                foreach (var itemselected in ChangeRMSelected)
                                                {
                                                    itemselected.Selected = true;
                                                }
                                                context.SaveChanges();
                                            }
                                        }
                                    }
                                    context.SaveChanges();
                                }

                            }

                            if (transactionChangeRM.Documents != null)
                            {
                                if (transactionChangeRM.Documents.Count() > 0)
                                {
                                    foreach (TransactionDocumentModel item in transactionChangeRM.Documents)
                                    {
                                        Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                                        {
                                            TransactionID = data.TransactionID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };

                                        context.TransactionDocumentDrafts.Add(document);
                                    }
                                    context.SaveChanges();
                                }
                            }

                            #region BringUp
                            if (transactionChangeRM.IsBringupTask == true)
                            {
                                Entity.TransactionBringupTask Bringup = new Entity.TransactionBringupTask()
                                {
                                    TransactionID = TrIDInserted,
                                    TaskName = "Bring Up Retail CIF",
                                    AssignedTo = currentUser.GetCurrentUser().LoginName,
                                    AssignedFrom = currentUser.GetCurrentUser().LoginName,
                                    DateIn = DateTime.UtcNow,
                                    IsSubmitted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName,
                                };
                                context.TransactionBringupTasks.Add(Bringup);
                                context.SaveChanges();
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        #region submit Transaction changeRM
                        DBS.Entity.Transaction data = new DBS.Entity.Transaction()
                        {
                            #region Unused field but Not Null
                            IsSignatureVerified = false,
                            IsDormantAccount = false,
                            IsFrezeAccount = false,
                            BeneName = "-",
                            BankID = 1,
                            IsResident = false,
                            IsCitizen = false,
                            AmountUSD = 0.00M,
                            Rate = 0.00M,
                            DebitCurrencyID = 1,
                            BizSegmentID = 1,
                            ChannelID = 1,
                            #endregion

                            #region Primary Field
                            CIF = transactionChangeRM.Customer.CIF,
                            ProductID = transactionChangeRM.Product.ID,
                            TransactionID = transactionChangeRM.ID,
                            ApplicationID = transactionChangeRM.ApplicationID,
                            IsDraft = transactionChangeRM.IsDraft,
                            IsNewCustomer = false,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().LoginName,
                            ApplicationDate = DateTime.Now,
                            CurrencyID = (transactionChangeRM.Currency.ID != null && transactionChangeRM.Currency.ID != 0 ? transactionChangeRM.Currency.ID : (int)CurrencyID.NullCurrency),
                            Amount = 0,
                            IsTopUrgent = 0,
                            IsDocumentComplete = false,
                            StateID = (int)StateID.OnProgress,
                            AccountNumber = (transactionChangeRM.AccountNumber == null ? null : transactionChangeRM.AccountNumber.AccountNumber),
                            BranchName = transactionChangeRM.BranchName,
                            IsChangeRM = transactionChangeRM.IsChangeRM,
                            IsSegment = transactionChangeRM.IsSegment,
                            IsSolID = transactionChangeRM.IsSOLID
                            #endregion
                        };

                        if (transactionChangeRM.TransactionType.ID == 0)
                            data.TransactionTypeID = null;
                        else
                            data.TransactionTypeID = transactionChangeRM.TransactionType.ID;

                        if (transactionChangeRM.MaintenanceType.ID == 0)
                            data.CBOMaintainID = null;
                        else
                            data.CBOMaintainID = (transactionChangeRM.MaintenanceType.ID == 0 ? int.Parse(null) : transactionChangeRM.MaintenanceType.ID);

                        context.Transactions.Add(data);
                        context.SaveChanges();

                        transactionID = data.TransactionID;
                        TrIDInserted = transactionID;

                        if (transactionChangeRM.ChangeRM != null)
                        {
                            if (transactionChangeRM.ChangeRM.Count() > 0)
                            {
                                foreach (var item in transactionChangeRM.ChangeRM)
                                {
                                    if (item.Selected == true && !string.IsNullOrEmpty(item.CIF))
                                    {
                                        DBS.Entity.ChangeRMTransaction ChangeRMSegmentSOLID = new DBS.Entity.ChangeRMTransaction()
                                        {
                                            TransactionID = transactionID,
                                            CIF = item.CIF,
                                            ChangeSegmentFrom = !string.IsNullOrEmpty(item.ChangeSegmentFrom) ? item.ChangeSegmentFrom : "",
                                            ChangeSegmentTo = !string.IsNullOrEmpty(item.ChangeSegmentTo) ? item.ChangeSegmentTo : "",
                                            ChangeFromBranchCode = !string.IsNullOrEmpty(item.ChangeFromBranchCode) ? item.ChangeFromBranchCode : "",
                                            ChangeFromRMCode = !string.IsNullOrEmpty(item.ChangeFromRMCode) ? item.ChangeFromRMCode : "",
                                            ChangeFromBU = !string.IsNullOrEmpty(item.ChangeFromBU) ? item.ChangeFromBU : "",
                                            ChangeToBranchCode = !string.IsNullOrEmpty(item.ChangeToBranchCode) ? item.ChangeToBranchCode : "",
                                            ChangeToRMCode = !string.IsNullOrEmpty(item.ChangeToRMCode) ? item.ChangeToRMCode : "",
                                            ChangeToBU = !string.IsNullOrEmpty(item.ChangeToBU) ? item.ChangeToBU : "",
                                            Account = !string.IsNullOrEmpty(item.Account) ? item.Account : "",
                                            PCCode = !string.IsNullOrEmpty(item.PCCode) ? item.PCCode : "",
                                            EATPB = !string.IsNullOrEmpty(item.EATPB) ? item.EATPB : "",
                                            Remarks = !string.IsNullOrEmpty(item.Remarks) ? item.Remarks : "",
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };

                                        context.ChangeRMTransactions.Add(ChangeRMSegmentSOLID);

                                        ActivityHistoryIDSelected = item.ActivityHistoryID;
                                        CIFSelected = item.CIF;
                                        CustomerNameSelected = item.CustomerName;

                                        IList<Entity.ChangeRM> ChangeRMSelected = context.ChangeRMs.Where(a => a.ActivityHistoryID == ActivityHistoryIDSelected && a.CIF == CIFSelected && a.CUSTOMER_NAME == CustomerNameSelected).ToList();

                                        if (ChangeRMSelected != null)
                                        {
                                            foreach (var itemselected in ChangeRMSelected)
                                            {
                                                itemselected.Selected = true;
                                            }
                                            context.SaveChanges();
                                        }
                                    }
                                }
                                context.SaveChanges();
                            }

                        }


                        if (transactionChangeRM.Documents != null)
                        {
                            if (transactionChangeRM.Documents.Count() > 0)
                            {
                                foreach (var item in transactionChangeRM.Documents)
                                {
                                    Entity.TransactionDocument document = new Entity.TransactionDocument()
                                    {
                                        TransactionID = data.TransactionID,
                                        DocTypeID = item.Type.ID,
                                        PurposeID = item.Purpose.ID,
                                        Filename = item.FileName,
                                        DocumentPath = item.DocumentPath,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName
                                    };

                                    context.TransactionDocuments.Add(document);
                                }
                                context.SaveChanges();
                            }
                        }
                        #endregion

                    }
                    ts.Complete();
                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.InnerException.Message;
                }
            }

            if (!transactionChangeRM.IsDraft)
            {
                using (DBSEntities context = new DBSEntities())
                {
                    var getTR = context.Transactions.Where(x => x.TransactionID == TrIDInserted).SingleOrDefault();
                    ApplicationID = getTR.ApplicationID;
                    IsSuccess = true;
                }

            }
            return IsSuccess;
        }
        public bool DeleteTransactionChangeRMByID(long id, ref string message)
        {
            bool IsSuccess = false;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.TransactionDraft data = context.TransactionDrafts.Where(a => a.TransactionID.Equals(id)).SingleOrDefault();
                    if (data != null)
                    {
                        Entity.TransactionBringupTask dataBringup = context.TransactionBringupTasks.Where(a => a.TransactionID.Equals(id)).FirstOrDefault();
                        if (dataBringup != null)
                        {
                            data.IsDeleted = true;
                            context.SaveChanges();

                            dataBringup.DateOut = DateTime.UtcNow;
                            dataBringup.IsSubmitted = true;
                            dataBringup.SubmittedBy = currentUser.GetCurrentUser().LoginName;
                            dataBringup.UpdateBy = currentUser.GetCurrentUser().LoginName;
                            dataBringup.UpdateDate = DateTime.UtcNow;
                            context.SaveChanges();
                        }
                        else
                        {
                            var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(id)).ToList();
                            if (existingDocs != null)
                            {
                                if (existingDocs.Count() > 0)
                                {
                                    foreach (var item in existingDocs)
                                    {
                                        var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                        context.TransactionDocumentDrafts.Remove(deleteDoc);
                                        context.SaveChanges();
                                    }
                                }
                            }
                            context.TransactionDrafts.Remove(data);
                            context.SaveChanges();
                        }
                        IsSuccess = true;
                    }
                    else
                    {
                        message = string.Format("Draft ID {0} is does not exist.", id);
                        IsSuccess = false;
                    }
                    ts.Complete();
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }

            return IsSuccess;
        }

        #region Dispatch Mode
        public bool AddTransactionDispatchMode(TransactionDispatchModeModel data, ref long TransactionId, ref string ApplicationId, ref string message)
        {
            bool IsSuccess = false;
            long TrIDInserted = 0;
            var loginName = currentUser.GetCurrentUser().LoginName;
            using (DBSEntities context_ts = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        if (data.IsDraft)
                        {
                            #region Save draft
                            if (data.TransactionID != null && data.TransactionID > 0)//dulu draft sekarang draft lagi
                            {
                                Entity.TransactionDraft transDraft = context.TransactionDrafts.Where(a => a.TransactionID.Equals(data.TransactionID)).SingleOrDefault();
                                if (transDraft != null)
                                {
                                    #region Unused field but Not Null
                                    transDraft.BeneName = "-";
                                    transDraft.BankID = 1;
                                    transDraft.IsResident = false;
                                    transDraft.IsCitizen = false;
                                    transDraft.AmountUSD = 0.00M;
                                    transDraft.Rate = 0.00M;
                                    transDraft.DebitCurrencyID = 1;
                                    transDraft.BizSegmentID = 1;
                                    transDraft.ChannelID = 1;
                                    transDraft.CIF = "DUMMYCIF";
                                    #endregion

                                    #region Primary Field
                                    transDraft.ProductID = data.ProductID;
                                    transDraft.TransactionID = data.TransactionID;
                                    transDraft.ApplicationID = data.ApplicationID;
                                    transDraft.IsDraft = data.IsDraft;
                                    transDraft.IsNewCustomer = null;
                                    transDraft.CreateDate = DateTime.UtcNow;
                                    transDraft.CreateBy = loginName;
                                    transDraft.ApplicationDate = DateTime.UtcNow;
                                    transDraft.CurrencyID = (int)CurrencyID.NullCurrency;
                                    transDraft.Amount = 0;
                                    transDraft.IsTopUrgent = 0;
                                    transDraft.StateID = (int)StateID.OnProgress;
                                    transDraft.StaffID = null;
                                    transDraft.StaffTaggingID = null;
                                    transDraft.ATMNumber = null;
                                    transDraft.IsLOI = null;
                                    transDraft.IsPOI = null;
                                    transDraft.IsPOA = null;
                                    transDraft.LocationName = null;
                                    transDraft.AccountNumber = null;
                                    transDraft.TransactionTypeID = data.RequestTypeID;
                                    transDraft.CBOMaintainID = data.MaintenanceTypeID;
                                    #endregion
                                    context_ts.SaveChanges();

                                    #region Dispatch Mode draft
                                    Entity.RetailCIFCBODraft transDispatchModeDraft = context_ts.RetailCIFCBODrafts.Where(a => a.TransactionID.Equals(data.TransactionID)).SingleOrDefault();
                                    if (transDispatchModeDraft != null)
                                    {
                                        transDispatchModeDraft.TransactionID = data.TransactionID;
                                        transDispatchModeDraft.CreateBy = loginName;
                                        transDispatchModeDraft.CreateDate = DateTime.UtcNow;
                                        transDispatchModeDraft.DispatchModeTypeID = data.DispatchModeTypeID;

                                        transDispatchModeDraft.MaintenanceTypeID = data.MaintenanceTypeID;
                                        transDispatchModeDraft.DispatchModeOther = data.DispatchModeOther;

                                        context_ts.SaveChanges();
                                    }
                                    #endregion

                                    #region Save Document
                                    var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(data.TransactionID)).ToList();
                                    if (existingDocs != null)
                                    {
                                        if (existingDocs.Count() > 0)
                                        {
                                            foreach (var item in existingDocs)
                                            {
                                                var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                                deleteDoc.IsDeleted = true;
                                                context.SaveChanges();
                                            }
                                        }
                                    }
                                    if (data.Documents != null)
                                    {
                                        if (data.Documents.Count > 0)
                                        {
                                            foreach (var item in data.Documents)
                                            {
                                                Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                                                {
                                                    TransactionID = data.TransactionID,
                                                    DocTypeID = item.Type.ID,
                                                    PurposeID = 1,
                                                    Filename = item.FileName,
                                                    DocumentPath = item.DocumentPath.DocPath,
                                                    CreateDate = DateTime.UtcNow,
                                                    CreateBy = loginName
                                                };
                                                context_ts.TransactionDocumentDrafts.Add(document);
                                            }
                                            context_ts.SaveChanges();
                                        }
                                    }
                                    #endregion
                                }
                            }
                            else
                            {//draft baru
                                bool isBringup = false;
                                DBS.Entity.TransactionDraft transDraft = new DBS.Entity.TransactionDraft();
                                transDraft.BeneName = "-";
                                transDraft.BankID = 1;
                                transDraft.IsResident = false;
                                transDraft.IsCitizen = false;
                                transDraft.AmountUSD = 0.00M;
                                transDraft.Rate = 0.00M;
                                transDraft.DebitCurrencyID = 1;
                                transDraft.BizSegmentID = 1;
                                transDraft.ChannelID = 1;
                                transDraft.CIF = "DUMMYCIF";

                                transDraft.ProductID = data.ProductID;
                                transDraft.IsDraft = data.IsDraft;
                                transDraft.IsNewCustomer = null;
                                transDraft.CreateDate = DateTime.UtcNow;
                                transDraft.CreateBy = loginName;
                                transDraft.ApplicationDate = DateTime.UtcNow;
                                transDraft.CurrencyID = (int)CurrencyID.NullCurrency;
                                transDraft.Amount = 0;
                                transDraft.IsTopUrgent = 0;
                                transDraft.StateID = (int)StateID.OnProgress;
                                transDraft.StaffID = null;
                                transDraft.StaffTaggingID = null;
                                transDraft.ATMNumber = null;
                                transDraft.IsLOI = null;
                                transDraft.IsPOI = null;
                                transDraft.IsPOA = null;
                                transDraft.LocationName = null;
                                transDraft.AccountNumber = null;
                                transDraft.TransactionTypeID = data.RequestTypeID;
                                transDraft.CBOMaintainID = data.MaintenanceTypeID;

                                context_ts.TransactionDrafts.Add(transDraft);
                                context_ts.SaveChanges();

                                TransactionId = transDraft.TransactionID;

                                #region Submit Transaction Draft
                                Entity.RetailCIFCBODraft transDispatchModeDraft = new Entity.RetailCIFCBODraft();
                                transDispatchModeDraft.TransactionID = TransactionId;
                                transDispatchModeDraft.CreateBy = loginName;
                                transDispatchModeDraft.CreateDate = DateTime.UtcNow;
                                transDispatchModeDraft.MaintenanceTypeID = data.MaintenanceTypeID;
                                transDispatchModeDraft.RequestTypeID = data.RequestTypeID;
                                transDispatchModeDraft.DispatchModeTypeID = data.DispatchModeTypeID;
                                transDispatchModeDraft.DispatchModeOther = data.DispatchModeOther;
                                context_ts.RetailCIFCBODrafts.Add(transDispatchModeDraft);
                                context_ts.SaveChanges();

                                #endregion


                                #region Save Document
                                var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(data.TransactionID)).ToList();
                                if (existingDocs != null)
                                {
                                    if (existingDocs.Count() > 0)
                                    {
                                        foreach (var item in existingDocs)
                                        {
                                            var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                            deleteDoc.IsDeleted = true;
                                            context.SaveChanges();
                                        }
                                    }
                                }
                                if (data.Documents != null)
                                {
                                    if (data.Documents.Count > 0)
                                    {
                                        foreach (var item in data.Documents)
                                        {
                                            Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                                            {
                                                TransactionID = TransactionId,
                                                DocTypeID = item.Type.ID,
                                                PurposeID = 1,
                                                Filename = item.FileName,
                                                DocumentPath = item.DocumentPath.DocPath,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = loginName
                                            };
                                            context_ts.TransactionDocumentDrafts.Add(document);
                                        }
                                        context_ts.SaveChanges();
                                    }
                                }
                                #endregion
                                #region BringUp
                                if (data.IsBringupTask == true)
                                {
                                    Entity.TransactionBringupTask Bringup = new Entity.TransactionBringupTask()
                                    {
                                        TransactionID = TransactionId,
                                        TaskName = "Bring Up Retail CIF",
                                        AssignedTo = currentUser.GetCurrentUser().LoginName,
                                        AssignedFrom = currentUser.GetCurrentUser().LoginName,
                                        DateIn = DateTime.UtcNow,
                                        IsSubmitted = false,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName,
                                    };
                                    context_ts.TransactionBringupTasks.Add(Bringup);
                                    context_ts.SaveChanges();
                                }
                                #endregion
                                ts.Complete();
                                IsSuccess = true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region insert tabel transaction
                            bool isBringup = false;
                            Entity.Transaction trans = new Entity.Transaction();
                            trans.IsSignatureVerified = false;
                            trans.IsDormantAccount = false;
                            trans.IsFrezeAccount = false;
                            trans.BeneName = "-";
                            trans.BankID = 1;
                            trans.IsResident = false;
                            trans.IsCitizen = false;
                            trans.AmountUSD = 0.00M;
                            trans.Rate = 0.00M;
                            trans.DebitCurrencyID = 1;
                            trans.BizSegmentID = 1;
                            trans.ChannelID = 1;
                            trans.IsNewCustomer = false;
                            trans.CIF = "DUMMYCIF";
                            trans.CurrencyID = (int)CurrencyID.NullCurrency;
                            trans.Amount = 0;
                            trans.IsTopUrgent = 0;
                            trans.IsDocumentComplete = false;
                            trans.StaffID = null;
                            trans.LocationID = context_ts.Employees.Where(x => x.EmployeeUsername.Equals(loginName) && x.IsDeleted.Equals(false)).Select(x => x.LocationID).FirstOrDefault();
                            trans.ProductID = data.ProductID;
                            trans.IsDraft = data.IsDraft;
                            trans.CreateDate = DateTime.UtcNow;
                            trans.CreateBy = loginName;
                            trans.ApplicationDate = DateTime.UtcNow;
                            trans.StateID = (int)StateID.OnProgress;
                            trans.StaffTaggingID = null;
                            trans.ATMNumber = null;
                            trans.IsLOI = null;
                            trans.IsPOI = null;
                            trans.IsPOA = null;
                            trans.LocationName = null;
                            trans.AccountNumber = null;
                            trans.BranchName = null;
                            trans.TouchTimeStartDate = null;
                            trans.TransactionTypeID = data.RequestTypeID;
                            trans.CBOMaintainID = data.MaintenanceTypeID;
                            trans.ProductID = data.ProductID;
                            trans.TransactionSubTypeID = null;

                            context_ts.Transactions.Add(trans);
                            context_ts.SaveChanges();
                            #endregion

                            #region add transaction id
                            TransactionId = trans.TransactionID;
                            TrIDInserted = TransactionId;
                            #endregion

                            #region submit Dispatch Mode
                            Entity.RetailCIFCBO transDispatchMode = new Entity.RetailCIFCBO();
                            transDispatchMode.TransactionID = TransactionId;
                            transDispatchMode.CreateBy = loginName;
                            transDispatchMode.CreateDate = DateTime.UtcNow;
                            transDispatchMode.DispatchModeTypeID = data.DispatchModeTypeID;
                            transDispatchMode.DispatchModeOther = data.DispatchModeTypeID == null ? "" : data.DispatchModeOther;
                            transDispatchMode.RequestTypeID = data.RequestTypeID;
                            transDispatchMode.MaintenanceTypeID = data.MaintenanceTypeID;
                            context_ts.RetailCIFCBOes.Add(transDispatchMode);
                            context_ts.SaveChanges();
                            #endregion

                            #region Save Document
                            if (data.Documents != null)
                            {
                                if (data.Documents.Count > 0)
                                {
                                    foreach (var item in data.Documents)
                                    {
                                        Entity.TransactionDocument document = new Entity.TransactionDocument()
                                        {
                                            TransactionID = TrIDInserted,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = 1,
                                            Filename = item.FileName,
                                            DocumentPath = item.DocumentPath.DocPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = loginName
                                        };
                                        context_ts.TransactionDocuments.Add(document);
                                    }
                                    context_ts.SaveChanges();
                                }
                            }
                            #endregion
                        }
                        ts.Complete();
                        IsSuccess = true;
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException != null)
                            if (ex.InnerException.InnerException != null)
                                message = ex.InnerException.InnerException.Message;
                            else
                                message = ex.Message;
                        else
                            message = ex.Message;
                        ts.Dispose();

                    }
                }


            }
            if (!data.IsDraft)
            {
                using (DBSEntities context = new DBSEntities())
                {
                    var getTR = (from TR in context.Transactions
                                 where TR.TransactionID == TrIDInserted
                                 select TR).SingleOrDefault();
                    ApplicationId = getTR.ApplicationID;
                    IsSuccess = true;
                }
            }
            return IsSuccess;
        }
        public bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineModel> timelines, ref string message)
        {
            bool IsSuccess = false;

            try
            {

                timelines = (from a in context.SP_GetNintexTaskTimeline(workflowInstanceID)
                             select new TransactionTimelineModel
                             {
                                 ApproverID = a.WorkflowApproverID,
                                 Activity = a.ActivityTitle,
                                 Time = a.ActivityTime,
                                 UserOrGroup = a.UserOrGroup,
                                 IsGroup = a.IsSPGroup.Value,
                                 Outcome = a.Outcome,
                                 DisplayName = a.DisplayName,
                                 Comment = a.Comment
                             }).ToList();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        public bool GetTransactionDispatchModeDraftByID(long id, ref TransactionDispatchModeModel transaction, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                transaction = context.RetailCIFCBODrafts.Where(a => a.TransactionID == id).Select(a => new TransactionDispatchModeModel
                {
                    DispatchModeOther = a.DispatchModeOther,
                    DispatchModeTypeID = a.DispatchModeTypeID.HasValue ? a.DispatchModeTypeID.Value : 0,
                    MaintenanceTypeID = a.MaintenanceTypeID.HasValue ? a.MaintenanceTypeID.Value : 0,
                    ProductID = (int)DBSProductID.CIFProductIDCons,
                    RequestTypeID = 25,
                    TransactionID = id


                }).SingleOrDefault();

                #region Document
                var docDraft = (from doc in context.TransactionDocumentDrafts
                                where doc.TransactionID == id && doc.IsDeleted.Equals(false)
                                select new DocumentModel
                                {
                                    ID = doc.TransactionDocumentID,
                                    LastModifiedBy = doc.UpdateBy == null ? doc.CreateBy : doc.UpdateBy,
                                    LastModifiedDate = doc.UpdateDate == null ? doc.CreateDate : doc.CreateDate,
                                    Purpose = new DocumentPurposeModel { ID = doc.DocumentPurpose.PurposeID, Name = doc.DocumentPurpose.PurposeName },
                                    Type = new DocumentTypeModel { ID = doc.DocumentType.DocTypeID, Name = doc.DocumentType.DocTypeName },
                                    FileName = doc.Filename,
                                    DocumentPath = new DocumentPathModel { name = doc.Filename, lastModified = string.Empty, lastModifiedDate = DateTime.Now, DocPath = doc.DocumentPath, type = Util.ExistingDoctype }
                                }).ToList();
                if (docDraft != null)
                {
                    transaction.Documents = docDraft;
                }

                #endregion
                IsSuccess = true;
            }
            catch (Exception e)
            {
                message = e.Message;
            }


            return IsSuccess;
        }

        public bool GetCustomerAccount(string CIF, ref IList<CustomerAccountModel> customerAccount, ref string message)
        {
            bool isSuccess = false;

            try
            {
                customerAccount = (from j in context.SP_GETJointAccountLoiPoi(CIF)
                                   select new CustomerAccountModel
                                   {
                                       AccountNumber = j.AccountNumber,
                                       Currency = new CurrencyModel
                                       {
                                           ID = j.CurrencyID.Value,
                                           Code = j.CurrencyCode,
                                           Description = j.CurrencyDescription
                                       },
                                       CustomerName = j.CustomerName,
                                       IsJointAccount = j.IsJointAccount,
                                       CIF = j.CIF
                                   }).ToList();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        #endregion
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.Collect();
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}