﻿var ViewModel = function () {
    var self = this;

    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);
    //autocomplete declare
    var ProductID = {
        ProductID: ko.observable(""),
        ProductName: ko.observable()
    };
    var ProductModel = function (id, name) {
        var self = this;
        self.ID = ko.observable(id);
        self.Name = ko.observable(name);

    }
    //Declare observable which will be bind with UI
    self.ID = ko.observable("");
    self.TransTypeID = ko.observable("");
    self.TransactionType1 = ko.observable("");
    self.Name = ko.observable("");
    self.ProductID = ko.observable(ProductID);
    self.ProductDescription = ko.observable("");
    self.Product = ko.observable(new ProductModel('', ''));
    self.ddlProduct = ko.observableArray([]);
    //self.ProductID = ko.observable("");
    self.ProductName = ko.observable("");
    //self.CreateBy = ko.observable("");
    //self.CreateDate = ko.observable("");
    //self.UpdateBy = ko.observable("");
    //self.UpdateDate = ko.observable("");
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");
    self.ParameterTypeDisplay = ko.observable("");
    self.IsDeleted = ko.observable(false);

    // filter
    self.filterTransTypeID = ko.observable("");
    self.filterTransactionType1 = ko.observable("");
    self.filterProductID = ko.observable("");
    self.filterProductName = ko.observable("");
    self.filterCreateBy = ko.observable("");
    self.filterCreateDate = ko.observable("");
    self.filterUpdateBy = ko.observable("");
    self.filterUpdateDate = ko.observable("");
    self.filterName = ko.observable("");
    

    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    // New Data flag
    self.IsNewData = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.TransactionTypes = ko.observableArray([]);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    // set default sorting
    self.GridProperties().SortColumn("ID");
    self.GridProperties().SortOrder("ASC");

    // bind clear filters
    self.ClearFilters = function () {


        self.filterProductName("");
        self.filterName("");
        self.filterUpdateBy("");
        self.filterUpdateDate("");
        

        GetData();
    };

    self.GetDropdown = function () {

        GetDropdown();
    };

    // bind get data function to view
    self.GetData = function () {
        GetData();
    };

    //The Object which stored data entered in the observables
    var TransactionType = {
        ID: self.ID,
        ProductID: self.ProductID,
        ProductName: self.ProductDescription,
        Name: self.ProductName,
        ParameterTypeDisplay: self.ParameterTypeDisplay,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
        
    };
    //AutoCOmplete
    function OnSuccessAutoComplete(response, data, textStatus, jqXHR) {
        response($.map(data, function (item) {
            return {
                // default autocomplete object
                label: item.ID + " - " + item.Name,
                value: item.Name,

                // custom object binding
                data: {
                    ProductID: item.ID,
                    ProductName: item.Name
                }
            }
        }
        )
        );
    }
    //autocomplete II
    $("#ProductName").autocomplete({
        source: function (request, response) {
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.product + "/Search",
                data: {
                    query: request.term,
                    limit: 20
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
        },
        select: function (event, ui) {
            // set customer data model
            if (ui.item.data != undefined || ui.item.data != null) {
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                $("#ProductID").val(ui.item.data.ProductID);
            }
        }
    });

    //self.TransactionTypeModel().Customer().Name = "";
    //$("#product-name").val("");

    //// disabled autocomplete if this is a new customer
    //$("#product-name").autocomplete({ disabled: self.TransactionTypeModel().IsNewCustomer() });

    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    console.log("spUser Roles : " + ko.toJSON(spUser.Roles));
                    console.log("spUser Permited : " + data);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
    self.save = function () {
        // validation
        var form = $("#aspnetForm");
        var saveID = $("#Product").val();
        var productname = $('#Product option:selected')[0].label;
        var saveNameID = $("#Name").val();
        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    TransactionType.ProductID(saveID);
                    TransactionType.ProductName(productname);//description for productID
                    TransactionType.Name(saveNameID);//for Transactionname
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.transactiontype,
                        data: ko.toJSON(TransactionType), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.update = function () {
        // validation
        var form = $("#aspnetForm");
        var form = $("#aspnetForm");
        var saveID = $("#Product").val();
        var productname = $('#Product option:selected')[0].label;
        var saveNameID = $("#Name").val();
        form.validate();

        if (form.valid()) {

            //console.log(ko.toJSON(Bank));
            // hide current popup window

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    TransactionType.ProductID(saveID);
                    TransactionType.ProductName(productname);//description for productID
                    TransactionType.Name(saveNameID);//for Transactionname
                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.transactiontype + "/" + TransactionType.ID(),
                        data: ko.toJSON(TransactionType),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.delete = function () {


        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.transactiontype + "/" + TransactionType.ID(),
                    data: ko.toJSON(TransactionType),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };
    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-18
    };

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        self.IsNewData(false);

        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');
        
        self.ID(data.ID);
        self.Product(new ProductModel(data.ProductID, data.ProductName));
        self.TransTypeID(data.TransTypeID);
        self.ProductID(data.ProductID);
        self.ProductName(data.ProductName);
        self.TransactionType1(data.TransactionType1);
        self.Name(data.Name);
        //self.UpdateBy(data.UpdateBy);
        //self.UpdateDate(data.UpdateDate);
        //self.CreateBy(data.CreateBy);
        //self.CreateDate(data.CreateDate);
    };

    //insert new
    self.NewData = function () {
        // flag as new Data
        self.IsNewData(true);
        self.Readonly(false);
        // bind empty data
        //self.TransTypeID(0);
        //self.TransactionType1('');
        self.ID(0);
        self.ProductID('');
        self.ProductName('');
        self.ParameterTypeDisplay('');
        self.Name('');
        //self.IsDeleted(false);
        //self.UpdateBy('');
        //self.UpdateDate('');
        //self.CreateBy('');
        //self.CreateDate('');
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    //Function to Read All Customers
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.transactiontype,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }


    //Function to validation dynamic field
    function IsvalidField() { 
     
        return true;
    }
    //getdropdown
    function GetDropdown() {
        $.ajax({
            async: false,
            type: "GET",
            url: api.server + api.url.parameter + '?select=Product',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    if (data['Product'] != null) { self.ddlProduct(data['Product']); }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }

        });
    };



    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.filterName() != "") filters.push({ Field: 'Name', Value: self.filterName() });
        if (self.filterProductID() != "") filters.push({ Field: 'ProductID', Value: self.filterProductID() });
        if (self.filterProductName() != "") filters.push({ Field: 'ProductName', Value: self.filterProductName() });
        if (self.filterCreateBy() != "") filters.push({ Field: 'CreateBy', Value: self.filterCreateBy() });
        if (self.filterCreateDate() != "") filters.push({ Field: 'CreateDate', Value: self.filterCreateDate() });
        if (self.filterUpdateBy() != "") filters.push({ Field: 'UpdateBy', Value: self.filterUpdateBy() });
        if (self.filterUpdateDate() != "") filters.push({ Field: 'UpdateDate', Value: self.filterUpdateDate() });

        return filters;
    };

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.TransactionTypes(data.Rows);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }



    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });
};
