//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class CustomerFunction
    {
        public long CustomerFunctionID { get; set; }
        public string CIF { get; set; }
        public int POAFunctionID { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    
        public virtual Customer Customer { get; set; }
        public virtual POAFunction POAFunction { get; set; }
    }
}
