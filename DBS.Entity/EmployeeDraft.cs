//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class EmployeeDraft
    {
        public EmployeeDraft()
        {
            this.EmployeeRoleMappingDrafts = new HashSet<EmployeeRoleMappingDraft>();
        }
    
        public long EmployeeDraftID { get; set; }
        public Nullable<System.Guid> WorkflowInstanceID { get; set; }
        public string EmployeeUsername { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeEmail { get; set; }
        public Nullable<int> RankID { get; set; }
        public Nullable<int> BizSegmentID { get; set; }
        public int LocationID { get; set; }
        public Nullable<int> FunctionRoleID { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public string UserCategoryCode { get; set; }
    
        public virtual FunctionRole FunctionRole { get; set; }
        public virtual Location Location { get; set; }
        public virtual ICollection<EmployeeRoleMappingDraft> EmployeeRoleMappingDrafts { get; set; }
    }
}
