﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

//=======================================================================================//
///FOR ALL DEVELOPER...
///PLEASE ADD NEW REGION BY YOUR NAME TO KEEP CLEAN !!! DONT BE A DIRTY PROGRAMMER!!!
///ANDIWIJAYA
//=======================================================================================//

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class WorkflowLoanController : ApiController
    {
        private string message = string.Empty;
        /// <summary>
        /// Define repo here
        /// </summary>
        private readonly IWorkflowLoanRepository repoLoan = null;

        public WorkflowLoanController()
        {
            this.repoLoan = new WorkflowLoanRepository();

        }
        /// <summary>
        /// Add Controller below...
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (repoLoan != null)
                repoLoan.Dispose();

            base.Dispose(disposing);
        }

        #region Fandi
        #region CSOLOAN
        //get detail transaction/by fandi
        [ResponseType(typeof(TransactionPPUCheckerCSODetailModel))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/Checker/{approverID}")]
        public HttpResponseMessage GetTransactionCSODetails(Guid workflowInstanceID, long approverID)
        {
            //TransactionDetailModel transaction = new TransactionDetailModel();
            TransactionCSOModel transaction = new TransactionCSOModel();
            IList<TransactionTimelineLoanModel> timelines = new List<TransactionTimelineLoanModel>();
            TransactionPPUCheckerCSODetailModel output = new TransactionPPUCheckerCSODetailModel();

            try
            {
                // #1 Get Transaction Details
                if (!repoLoan.GetTransactionCSODetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoLoan.GetTransactionLoanTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        //#3 Get Verify
                        if (!repoLoan.GetCheckerCSOLoan(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }

                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }
        [ResponseType(typeof(TransactionPPUCheckerCSODetailModel))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/CSO/{approverID}")]
        public HttpResponseMessage GetTransactionCSO(Guid workflowInstanceID, long approverID)
        {
            //TransactionDetailModel transaction = new TransactionDetailModel();
            TransactionCSOModel transaction = new TransactionCSOModel();
            IList<TransactionTimelineLoanModel> timelines = new List<TransactionTimelineLoanModel>();
            TransactionPPUCheckerCSODetailModel output = new TransactionPPUCheckerCSODetailModel();

            try
            {
                // #1 Get Transaction Details
                if (!repoLoan.GetTransactionCSODetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoLoan.GetTransactionLoanTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        //#3 Get Verify
                        if (!repoLoan.GetCheckerCSOLoan(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }

                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/Checker/{approverID}")]
        public HttpResponseMessage AddTransactionCheckerDetails(Guid workflowInstanceID, long approverID, TransactionPPUCheckerCSODetailModel transactionChecker)
        {

            if (!repoLoan.AddTransactionCSODetails(workflowInstanceID, approverID, transactionChecker, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
            }

        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/cso/update/{approverID}")]
        public HttpResponseMessage UpdateBranchMakerCSO(Guid workflowInstanceID, long approverID, TransactionPPUCheckerCSODetailModel transactionChecker)
        {

            if (!repoLoan.UpdateReviseCSOTask(workflowInstanceID, approverID, transactionChecker, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
            }

        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/CSO/Add/{approverID}")]
        public HttpResponseMessage AddTransactionChecker(Guid workflowInstanceID, long approverID, TransactionPPUCheckerCSODetailModel transactionChecker)
        {

            if (!repoLoan.AddTransactionCSODetails(workflowInstanceID, approverID, transactionChecker, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
            }

        }
        #endregion
        #endregion

        #region AndriYanto
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/CSORevise/{approverID}")]
        public HttpResponseMessage AddCSOReviseTask(Guid workflowInstanceID, long approverID, TransactionPPUCheckerCSODetailModel transactionChecker)
        {

            if (!repoLoan.AddCSOReviseTask(workflowInstanceID, approverID, transactionChecker, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
            }
            else if (!repoLoan.AddReviseCSOChecker(workflowInstanceID, approverID, transactionChecker, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
            }

            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
            }

        }
        #region PPUCheckerLoan
        //get detail transaction/by fandi
        [ResponseType(typeof(LoanPPUCheckerModel))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/PPUCheckerLoan/{approverID}")]
        public HttpResponseMessage GetTransactionCheckerDetails(Guid workflowInstanceID, long approverID)
        {
            //TransactionDetailModel transaction = new TransactionDetailModel();
            TransactionLoanModel transaction = new TransactionLoanModel();
            IList<TransactionTimelineLoanModel> timelines = new List<TransactionTimelineLoanModel>();
            LoanPPUCheckerModel output = new LoanPPUCheckerModel();

            try
            {
                // #1 Get Transaction Details
                if (!repoLoan.GetTransactionLoanPPUChecker(workflowInstanceID, approverID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoLoan.GetTransactionLoanTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repoLoan.GetCheckerLoanPPUchecker(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;


                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        /// <summary>
        /// Save Transaction Payment from Payment Checker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionUpcountryBranchCheckerModel))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/UpcountryBranchChecker/{approverID}")]
        public HttpResponseMessage GetLoanUpcountryBranchCheckerDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionDisbursementModel Disbursement = new TransactionDisbursementModel();
            IList<TransactionRolloverModel> rollover = new List<TransactionRolloverModel>();
            IList<TransactionInterestMaintenanceModel> im = new List<TransactionInterestMaintenanceModel>();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionLoanModel transaction = new TransactionLoanModel();
            TransactionUpcountryBranchCheckerModel output = new TransactionUpcountryBranchCheckerModel();
            TransactionCallbackDetailLoanModel callback = new TransactionCallbackDetailLoanModel();
            IList<VerifyLoanModel> verify = new List<VerifyLoanModel>();
            output.Disbursement = Disbursement;
            output.LoanRollover = rollover;
            output.LoanIM = im;

            if (!repoLoan.GetLoanUpcountryBranchCheckerDetails(workflowInstanceID, approverID, ref transaction, ref verify, ref Disbursement, ref rollover, ref im, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (!repoLoan.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    if (!repoLoan.GetTransactionCallbackDetails(workflowInstanceID, approverID, ref callback, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }

                    else
                    {
                        output.Transaction = transaction;
                        output.Disbursement = Disbursement;
                        output.LoanRollover = rollover;
                        output.LoanIM = im;
                        output.Timelines = timelines;
                        output.Callbacks = callback;
                        output.Verify = verify;
                        return Request.CreateResponse(HttpStatusCode.OK, output);
                    }
                }
            }
        }

        /// <summary>
        /// Get transaction details by Application ID
        /// </summary>
        /// <param name="ApplicationID">Application ID</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionDisbursementModel))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/Loan/{approverID}")]
        public HttpResponseMessage GetTransactionLoanDisbursement(Guid workflowInstanceID, long approverID)
        {
            TransactionDisbursementModel output = new TransactionDisbursementModel();
            if (!repoLoan.GetLoanDisburseDetails(workflowInstanceID, approverID, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<TransactionDisbursementModel>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/PPUCheckerLoan/Update/{approverID}")]
        public HttpResponseMessage UpdateTransactionCheckerDetails(Guid workflowInstanceID, long approverID, LoanPPUCheckerModel output)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoLoan.UpdateTransactionCheckerDetails(workflowInstanceID, approverID, output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been updated." });
                }
            }
        }

        #region Revise Excel

        [ResponseType(typeof(LoanCheckerInterestMaintenance))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Loan/MakerExcel/GetDataIMCSO/{approverID}")]
        public HttpResponseMessage GetDataExcelImMCSO(Guid workflowInstanceID, long approverID)
        {
            {
                TransactionLoanScheduledSettlementModel transaction = new TransactionLoanScheduledSettlementModel();
                IList<TransactionTimelineLoanModel> timelines = new List<TransactionTimelineLoanModel>();
                LoanCheckerInterestMaintenance output = new LoanCheckerInterestMaintenance();
                try
                {
                    // #1 Get Transaction Details
                    if (!repoLoan.GetTransactionExcel(workflowInstanceID, ref transaction, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #2 Get Transaction Timeline
                        if (!repoLoan.GetTransactionLoanTimeline(workflowInstanceID, ref timelines, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            // #3 Get Transaction Checker Details
                            if (!repoLoan.GetDataIMCSO(workflowInstanceID, approverID, ref output, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                            }
                            else
                            {
                                output.Transaction = transaction;
                                output.Timelines = timelines;

                                return Request.CreateResponse(HttpStatusCode.OK, output);
                            }
                        }
                    }
                }
                finally
                {

                    transaction = null;
                    timelines = null;
                }
            }
        }

        [ResponseType(typeof(LoanCheckerRollover))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Loan/MakerExcel/DataRolloverCSO/{approverID}")]
        public HttpResponseMessage DataRolleverCSO(Guid workflowInstanceID, long approverID)
        {
            TransactionLoanScheduledSettlementModel transaction = new TransactionLoanScheduledSettlementModel();
            IList<TransactionTimelineLoanModel> timelines = new List<TransactionTimelineLoanModel>();
            LoanCheckerRollover output = new LoanCheckerRollover();
            try
            {
                // #1 Get Transaction Details
                if (!repoLoan.GetTransactionExcel(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoLoan.GetTransactionLoanTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repoLoan.GetDataRolloverCSO(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }


        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/CSO/MakerExcel/AddRollover/{approverID}")]
        public HttpResponseMessage AddCSORollover(Guid workflowInstanceID, LoanCheckerRollover output, long approverID)
        {

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoLoan.AddCSORollover(workflowInstanceID, ref output, approverID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }


        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/CSO/MakerExcel/AddIM/{approverID}")]
        public HttpResponseMessage AddCSOIM(Guid workflowInstanceID, LoanCheckerInterestMaintenance output, long approverID)
        {

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoLoan.AddDataIMCSO(workflowInstanceID, ref output, approverID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }

        #region Loan Maker
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/LoanMaker/ReviseIM/{approverID}")]
        public HttpResponseMessage UpdateLoanMakerReviseIM(Guid workflowInstanceID, long approverID, LoanCheckerInterestMaintenance output)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoLoan.UpdateTransactionIM(workflowInstanceID, approverID, output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been updated." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/LoanMaker/ReviseRollover/{approverID}")]
        public HttpResponseMessage UpdateLoanMakerReviseRollover(Guid workflowInstanceID, long approverID, LoanCheckerRollover output)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoLoan.UpdateTransactionRollover(workflowInstanceID, approverID, output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been updated." });
                }
            }
        }
        #endregion

        #region Revise Loan Checker
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/LoanChecker/ReviseIM/{approverID}")]
        public HttpResponseMessage UpdateLoanCheckerReviseIM(Guid workflowInstanceID, long approverID, InterestMaintenanceExcelCheckerLoan output)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoLoan.ReviseTransactionIMChecker(workflowInstanceID, approverID, output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been updated." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/LoanChecker/ReviseRollover/{approverID}")]
        public HttpResponseMessage UpdateLoanCheckerReviseRollover(Guid workflowInstanceID, long approverID, DataCheckerRollover output)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoLoan.ReviseTransactionRolloverChecker(workflowInstanceID, approverID, output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been updated." });
                }
            }
        }
        #endregion
        #endregion


        #endregion

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/UpcountryBranchCheckerLoan/Update/{approverID}")]

        public HttpResponseMessage UpdateTransactionUpcountryCheckerDetails(Guid workflowInstanceID, long approverID, IList<VerifyLoanModel> output)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoLoan.UpdateTransactionUpcountryCheckerDetails(workflowInstanceID, approverID, output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been updated." });
                }
            }
        }
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/PPUCheckerLoan/Add/{approverID}")]
        public HttpResponseMessage AddTransactionCheckerDetails(Guid workflowInstanceID, long approverID, LoanPPUCheckerModel output)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoLoan.AddPPUCheckerLoan(workflowInstanceID, approverID, output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been updated." });
                }
            }
        }
        #endregion

        #region PPUMakerLoan
        [ResponseType(typeof(LoanPPUCheckerModel))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/PPUMakerLoan/{approverID}")]
        public HttpResponseMessage GetDataPPUMakerLoan(Guid workflowInstanceID, long approverID)
        {
            //TransactionDetailModel transaction = new TransactionDetailModel();
            TransactionLoanModel transaction = new TransactionLoanModel();
            IList<TransactionTimelineLoanModel> timelines = new List<TransactionTimelineLoanModel>();
            LoanPPUCheckerModel output = new LoanPPUCheckerModel();

            try
            {
                // #1 Get Transaction Details
                if (!repoLoan.GetTransactionLoanPPUMaker(workflowInstanceID, approverID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoLoan.GetTransactionLoanTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repoLoan.GetCheckerPPULoanMaker(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;


                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/Maker/Add/{approverID}")]
        public HttpResponseMessage AddTransactionLoanMakerDetails(Guid workflowInstanceID, long approverID, TransactionLoanModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoLoan.AddTransactionLoanMaker(workflowInstanceID, approverID, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction maker data revision has been saved." });
                }
            }
        }

        #endregion

        #region LoanCaller

        [ResponseType(typeof(TransactionCheckerAfterCallbackDetailLoanModel))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/caller/{approverID}")]
        public HttpResponseMessage GetTransactionCheckCallbackDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionLoanModel transaction = new TransactionLoanModel();
            IList<TransactionTimelineLoanModel> timelines = new List<TransactionTimelineLoanModel>();
            TransactionCallbackDetailLoanModel output = new TransactionCallbackDetailLoanModel();
            TransactionLoanModel output2 = new TransactionLoanModel();
            try
            {

                // #2 Get Transaction Timeline
                if (!repoLoan.GetTransactionLoanTimeline(workflowInstanceID, ref timelines, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    if (!repoLoan.GetTransactionLoanCaller(workflowInstanceID, approverID, ref output2, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        if (!repoLoan.GetTransactionCallbackDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }


                        else
                        {
                            output.Transaction = output2;
                            output.Timelines = timelines;
                            output.Callbacks = output.Callbacks;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }

            finally
            {

                transaction = null;
                timelines = null;
            }
        }



        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/Caller/Add/{approverID}")]
        public HttpResponseMessage AddTransactionCallbackLoanDetails(Guid workflowInstanceID, long approverID, LoanPPUCheckerModel data)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoLoan.AddTransactionCallback(workflowInstanceID, approverID, data, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "transaction callback data has been saved." });
                }
            }
        }

        #endregion

        #region Excel
        [ResponseType(typeof(LoanCheckerRolloverDetails))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Get/MakerExcel/{approverID}/Detail/{csorolloverID}")]
        public HttpResponseMessage GetDetailRolloverMaker(Guid workflowInstanceID, long approverID, long csorolloverID)
        {
            TransactionLoanScheduledSettlementModel transaction = new TransactionLoanScheduledSettlementModel();
            LoanCheckerRolloverDetails output = new LoanCheckerRolloverDetails();
            try
            {
                // #1 Get Transaction Details
                if (!repoLoan.GetTransactionDetailsScheduledSettlement(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Checker Details
                    if (!repoLoan.GetScheduledSettlementDetails(workflowInstanceID, csorolloverID, ref output, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        output.Transaction = transaction;

                        return Request.CreateResponse(HttpStatusCode.OK, output);
                    }

                }
            }
            finally
            {

                transaction = null;

            }
        }

        [ResponseType(typeof(DataCheckerRolloverDetail))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Get/MakerExcel/{approverID}/DetailRollover/{LoanRolloverID}")]
        public HttpResponseMessage GetDetailRolloverChecker(Guid workflowInstanceID, long approverID, long LoanRolloverID)
        {
            TransactionLoanScheduledSettlementModel transaction = new TransactionLoanScheduledSettlementModel();
            DataCheckerRolloverDetail output = new DataCheckerRolloverDetail();
            try
            {
                // #1 Get Transaction Details
                if (!repoLoan.GetTransactionDetailsScheduledSettlement(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Checker Details
                    if (!repoLoan.GetDataRolloverDetail(workflowInstanceID, LoanRolloverID, ref output, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        output.Transaction = transaction;

                        return Request.CreateResponse(HttpStatusCode.OK, output);
                    }

                }
            }
            finally
            {

                transaction = null;

            }
        }

        [ResponseType(typeof(DataMakerIMDetail))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Get/MakerExcel/{approverID}/Detailimmaker/{LoanRolloverID}")]
        public HttpResponseMessage GetDetailImmaker(Guid workflowInstanceID, long approverID, long LoanRolloverID)
        {
            TransactionLoanScheduledSettlementModel transaction = new TransactionLoanScheduledSettlementModel();
            DataMakerIMDetail output = new DataMakerIMDetail();
            try
            {
                // #1 Get Transaction Details
                if (!repoLoan.GetTransactionDetailsScheduledSettlement(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Checker Details
                    if (!repoLoan.GetDataImDetail(workflowInstanceID, LoanRolloverID, ref output, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        output.Transaction = transaction;

                        return Request.CreateResponse(HttpStatusCode.OK, output);
                    }

                }
            }
            finally
            {

                transaction = null;

            }
        }


        [ResponseType(typeof(LoanMakerDetailModel))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Loan/MakerExcel/{approverID}")]
        public HttpResponseMessage GetLoanMakerExcelDetails(Guid workflowInstanceID, long approverID)
        {
            //TransactionDetailModel transaction = new TransactionDetailModel();
            TransactionCSOModel transaction = new TransactionCSOModel();
            IList<TransactionTimelineLoanModel> timelines = new List<TransactionTimelineLoanModel>();
            TransactionCSODetailModel output = new TransactionCSODetailModel();

            try
            {
                // #1 Get UT Details
                if (!repoLoan.GetLoanDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoLoan.GetTransactionLoanTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        output.Transaction = transaction;
                        output.Timelines = timelines;

                        return Request.CreateResponse(HttpStatusCode.OK, output);
                    }
                }
            }
            finally
            {
                transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(LoanCheckerRollover))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Loan/MakerExcel/DataRollover/{approverID}")]
        public HttpResponseMessage DataRollever(Guid workflowInstanceID, long approverID)
        {
            TransactionLoanScheduledSettlementModel transaction = new TransactionLoanScheduledSettlementModel();
            IList<TransactionTimelineLoanModel> timelines = new List<TransactionTimelineLoanModel>();
            LoanCheckerRollover output = new LoanCheckerRollover();
            try
            {
                // #1 Get Transaction Details
                if (!repoLoan.GetTransactionExcel(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoLoan.GetTransactionLoanTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repoLoan.GetDataRollover(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(LoanCheckerRollover))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Loan/MakerExcel/DataSettlementScheduled/{approverID}")]
        public HttpResponseMessage DataSettlementSchedule(Guid workflowInstanceID, long approverID, long csorolloverID)
        {
            {
                TransactionLoanScheduledSettlementModel transaction = new TransactionLoanScheduledSettlementModel();
                IList<TransactionTimelineLoanModel> timelines = new List<TransactionTimelineLoanModel>();
                LoanCheckerRollover output = new LoanCheckerRollover();
                try
                {
                    // #1 Get Transaction Details
                    if (!repoLoan.GetTransactionDetailsScheduledSettlement(workflowInstanceID, ref transaction, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #2 Get Transaction Timeline
                        if (!repoLoan.GetTransactionLoanTimeline(workflowInstanceID, ref timelines, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            // #3 Get Transaction Checker Details
                            if (!repoLoan.GetDataRollover(workflowInstanceID, approverID, ref output, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                            }
                            else
                            {
                                output.Transaction = transaction;
                                output.Timelines = timelines;

                                return Request.CreateResponse(HttpStatusCode.OK, output);
                            }
                        }
                    }
                }
                finally
                {

                    transaction = null;
                    timelines = null;
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Loan/CheckerExcel/AddRollover/{approverID}")]
        public HttpResponseMessage AddLoanRolloverChecker(Guid workflowInstanceID, DataCheckerRollover output, long approverID)
        {

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoLoan.AddDataRolloverChecker(workflowInstanceID, ref output, approverID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Loan/MakerExcel/AddRollover/{approverID}")]
        public HttpResponseMessage AddLoanRollover(Guid workflowInstanceID, LoanCheckerRollover output, long approverID)
        {

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoLoan.AddDataRollover(workflowInstanceID, ref output, approverID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Loan/MakerExcel/AddIM/{approverID}")]
        public HttpResponseMessage AddLoanIM(Guid workflowInstanceID, LoanCheckerInterestMaintenance output, long approverID)
        {

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoLoan.AddDataIMMaker(workflowInstanceID, ref output, approverID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Loan/CheckerExcel/AddIM/{approverID}")]
        public HttpResponseMessage AddLoanCheckerIM(Guid workflowInstanceID, InterestMaintenanceExcelCheckerLoan output, long approverID)
        {

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoLoan.AddDataIMChecker(workflowInstanceID, ref output, approverID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }

        [ResponseType(typeof(LoanCheckerInterestMaintenance))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Loan/MakerExcel/GetDataIM/{approverID}")]
        public HttpResponseMessage GetDataExcelImMaker(Guid workflowInstanceID, long approverID)
        {
            {
                TransactionLoanScheduledSettlementModel transaction = new TransactionLoanScheduledSettlementModel();
                IList<TransactionTimelineLoanModel> timelines = new List<TransactionTimelineLoanModel>();
                LoanCheckerInterestMaintenance output = new LoanCheckerInterestMaintenance();
                try
                {
                    // #1 Get Transaction Details
                    if (!repoLoan.GetTransactionExcel(workflowInstanceID, ref transaction, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #2 Get Transaction Timeline
                        if (!repoLoan.GetTransactionLoanTimeline(workflowInstanceID, ref timelines, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            // #3 Get Transaction Checker Details
                            if (!repoLoan.GetDataIM(workflowInstanceID, approverID, ref output, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                            }
                            else
                            {
                                output.Transaction = transaction;
                                output.Timelines = timelines;

                                return Request.CreateResponse(HttpStatusCode.OK, output);
                            }
                        }
                    }
                }
                finally
                {

                    transaction = null;
                    timelines = null;
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/Loan/MakerExcel/AddDataSettlement")]
        public HttpResponseMessage AddExcelMakerSettlement(LoanCheckerDetailLoanModel output)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            { //AddDataRollover(ref LoanCheckerDetailLoanModel output, ref string message)
                if (!repoLoan.AddDataSettlement(output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been updated." });
                }
            }
        }
        #region LoanChecker
        [ResponseType(typeof(DataCheckerRollover))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Loan/MakerExcel/DataRolleverLoanChecker/{approverID}")]
        public HttpResponseMessage DataRolleverLoanChecker(Guid workflowInstanceID, long approverID)
        {
            TransactionLoanScheduledSettlementModel transaction = new TransactionLoanScheduledSettlementModel();
            IList<TransactionTimelineLoanModel> timelines = new List<TransactionTimelineLoanModel>();
            DataCheckerRollover output = new DataCheckerRollover();
            try
            {
                // #1 Get Transaction Details
                if (!repoLoan.GetTransactionExcel(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoLoan.GetTransactionLoanTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repoLoan.GetDataRolloverLoanChecker(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }

        }

        [ResponseType(typeof(InterestMaintenanceExcelCheckerLoan))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Loan/MakerExcel/DataRolloverIMChecker/{approverID}")]
        public HttpResponseMessage DataInterestMaintenanceLoanChecker(Guid workflowInstanceID, long approverID)
        {
            TransactionLoanScheduledSettlementModel transaction = new TransactionLoanScheduledSettlementModel();
            IList<TransactionTimelineLoanModel> timelines = new List<TransactionTimelineLoanModel>();
            InterestMaintenanceExcelCheckerLoan output = new InterestMaintenanceExcelCheckerLoan();
            try
            {
                // #1 Get Transaction Details
                if (!repoLoan.GetTransactionExcel(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoLoan.GetTransactionLoanTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repoLoan.GetDataIMLoanChecker(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(RolloverLoanCheckerModel))]
        [HttpGet]
        [Route("api/WorkflowLoan/Loan/MakerExcel/DataSettlementScheduledLoanChecker")]
        public HttpResponseMessage DataSettlementScheduledLoanChecker()
        {
            IList<RolloverLoanCheckerModel> output = new List<RolloverLoanCheckerModel>();

            // #1 Get Transaction Details
            if (!repoLoan.GetDataSettlementScheduledLoanChecker(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<RolloverLoanCheckerModel>>(HttpStatusCode.OK, output);
            }
        }

        #endregion


        #endregion
        #region LoanMaker
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Loan/Maker/Add/{approverID}")]
        public HttpResponseMessage AddLoanMakerdetails(Guid workflowInstanceID, long approverID, LoanCheckerDetailLoanModel Transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoLoan.AddLoanChecker(workflowInstanceID, approverID, ref Transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else if (!repoLoan.AddLoanMakerTask(workflowInstanceID, approverID, Transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }
        #endregion

        #region Loan Checker

        [ResponseType(typeof(TransactionPPUCheckerCSODetailModel))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Loan/Checker/{approverID}")]
        public HttpResponseMessage GetLoancheckerDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionLoanModel transaction = new TransactionLoanModel();
            IList<TransactionTimelineLoanModel> timelines = new List<TransactionTimelineLoanModel>();
            LoanCheckerDetailLoanModel output = new LoanCheckerDetailLoanModel();
            IList<VerifyLoanModel> verifies = new List<VerifyLoanModel>();
            //TransactionCheckerDetailLoanModel output = new TransactionCheckerDetailLoanModel();
            try
            {
                // #1 Get Payment Details
                if (!repoLoan.GetTransactionLoanDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoLoan.GetTransactionLoanTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        //#3 Get Verify
                        if (!repoLoan.GetCheckerLoanMaker(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Verify = output.Verify;
                            output.Transaction = transaction;
                            output.Timelines = timelines;
                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {
                timelines = null;
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowUT/{workflowInstanceID}/Loan/Checker/Add/{approverID}")]
        public HttpResponseMessage AddLoanCheckerDetails(Guid workflowInstanceID, long approverID, LoanCheckerDetailLoanModel Transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoLoan.AddLoanChecker(workflowInstanceID, approverID, ref Transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }
        #endregion


        #region dani dp
        [ResponseType(typeof(LoanMakerSettlementUnscheduledDetailModel))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/LoanMakerSU/{approverID}")]
        public HttpResponseMessage GetTransactionLoanMakerSettlementUnscheduled(Guid workflowInstanceID, long approverID)
        {
            LoanMakerSettlementUnscheduledModel transaction = new LoanMakerSettlementUnscheduledModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            LoanMakerSettlementUnscheduledDetailModel output = new LoanMakerSettlementUnscheduledDetailModel();

            try
            {
                // #1 Get Transaction Details
                if (!repoLoan.GetTransactionLoanMakerSettlementUnscheduledDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoLoan.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        if (!repoLoan.GetCheckerUnsettlement(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;
                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {
                transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/Transaction/LoanMaker/SettlementUnscheduled")]
        public HttpResponseMessage AddTransactionLoanMakerSettlementUnscheduled(LoanMakerSettlementUnscheduledDetailModel transaction)
        {
            if (!repoLoan.AddTransactionLoanMakerSettlementUnscheduledDetails(transaction, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Data has been saved to Transaction Loan." });
            }
        }

        #endregion

        #region basri
        //Basri 28.10.2015        
        [ResponseType(typeof(TransactionLoanScheduledSettlementModel))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/Checker/ScheduledSettlement/{approverID}")]
        public HttpResponseMessage GetTransactionCheckerDetailsScheduledSettlement(Guid workflowInstanceID, long approverID)
        {
            TransactionLoanScheduledSettlementModel transaction = new TransactionLoanScheduledSettlementModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionCheckerLoanScheduledSettlementModel output = new TransactionCheckerLoanScheduledSettlementModel();

            try
            {
                // #1 Get Transaction Details
                if (!repoLoan.GetTransactionDetailsScheduledSettlement(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoLoan.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repoLoan.GetTransactionCheckerDetailsScheduledSettlement(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {
                transaction = null;
                timelines = null;
            }
        }
        [ResponseType(typeof(TransactionLoanScheduledSettlementModel))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/Checker/ScheduledSettlement/{approverID}/Detail/{csorolloverID}")]
        public HttpResponseMessage GetTransactionCheckerDetailsScheduledSettlement(Guid workflowInstanceID, long approverID, long csorolloverID)
        {
            TransactionLoanScheduledSettlementModel transaction = new TransactionLoanScheduledSettlementModel();
            TransactionCheckerLoanScheduledSettlementDetailModel output = new TransactionCheckerLoanScheduledSettlementDetailModel();

            try
            {
                // #1 Get Transaction Details
                if (!repoLoan.GetTransactionDetailsScheduledSettlement(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Checker Details
                    if (!repoLoan.GetScheduledSettlementDetails(workflowInstanceID, csorolloverID, ref output, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        output.Transaction = transaction;
                        return Request.CreateResponse(HttpStatusCode.OK, output);
                    }
                }
            }
            finally
            {
                transaction = null;
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowLoan/{workflowInstanceID}/Transaction/Checker/ScheduledSettlement/{approverID}")]
        public HttpResponseMessage AddTransactionCheckerDetailsScheduledSettlement(Guid workflowInstanceID, long approverID, TransactionCheckerLoanScheduledSettlementModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoLoan.AddTransactionCheckerDetailsScheduledSettlement(workflowInstanceID, approverID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }
        //end basri
        #endregion
    }
}

//=======================================================================================//
///FOR ALL DEVELOPER...
///PLEASE ADD NEW REGION BY YOUR NAME TO KEEP CLEAN !!! DONT BE A DIRTY PROGRAMMER!!!
///ANDIWIJAYA
//=======================================================================================//