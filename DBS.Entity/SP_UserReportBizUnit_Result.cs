//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    
    public partial class SP_UserReportBizUnit_Result
    {
        public string UserDisplayName { get; set; }
        public Nullable<int> Tat { get; set; }
        public Nullable<int> TransactionVolume { get; set; }
    }
}
