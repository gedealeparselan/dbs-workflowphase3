﻿var ProvinsiModel = function(id, description) {
    ProvinceID = ko.observable(id),
    Description = ko.observable(description)

}
var ViewModel = function () {
    var self = this;

    self.updateCallback = function () { };

    self.SetUpdateCallback = function (callback) {
        self.updateCallback = callback;
    };

    self.Provinsi = ko.observable(new ProvinsiModel('',''));
    //gridProperties
    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray([10, 25, 50, 75, 100]);
    self.Page = ko.observable(1);
    self.Size = ko.observableArray([10]);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    //bind with ui
    self.CityID = ko.observable();
    self.CityCode = ko.observable();
    self.Description = ko.observable();
    self.ProvinceID = ko.observable();
    self.LastModifiedBy = ko.observable();
    self.LastModifiedDate = ko.observable();

    //filter
    self.FilterCityCode = ko.observable();
    self.FilterDescription = ko.observable();
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");

    // New Data flag
    self.IsNewData = ko.observable(false);
    self.HeaderTitle = ko.observable('');
    self.UpdatedID = ko.observable(0);
    self.ID = ko.observable(0);
    self.isNew = ko.observable(false);
    self.isDeleted = ko.observable(false);
    self.BarLoad = ko.observable(false);
    self.Readonly = ko.observable(false);

    // storing for json respon
    self.Cities = ko.observableArray([]);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));
    self.GridProperties().SortColumn("Description");
    self.GridProperties().SortOrder("ASC");

    //bind clear filter
    self.ClearFilters = function () {
        self.FilterCityCode("");
        self.FilterDescription("");
        self.FilterModifiedBy = ko.observable("");
        self.FilterModifiedDate = ko.observable("");
        GetData();
    };

    // bind get data function to view
    self.GetData = function () {
        GetData();
    };

    //self.GetDropdown = function () {
    //    GetDropdown();
    //};

    //task properties
    self.ApproverId = ko.observable("");
    self.WorkflowInstanceID = ko.observable("");
    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);
    self.IsWorkflow = ko.observable(false);

    // Task status from Nintex custom REST API
    self.IsPendingNintex = ko.observable();
    self.IsAuthorizedNintex = ko.observable();
    self.MessageNintex = ko.observable();

    var city = {
        CityID: self.CityID,
        CityCode: self.CityCode,
       // ProvinceID: self.ProvinceID,
        Provinsi : self.Provinsi,
        Description: self.Description,
        isDeleted: self.isDeleted,
        CreateDate: self.CreateDate,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
    };

    self.save = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    //Ajax call to insert 
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.city,
                        //url: api.server + api.url.customercontact + '/Update/Workflow/' + self.WorkflowInstanceID() + '/ApproverID/' + self.ApproverId(),
                        data: ko.toJSON(city),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
                        }
                    });
                }
            });
        }
    };
    self.update = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.city + '/' + city.CityID(),
                        data: ko.toJSON(city),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status == 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            })
        }

    };
    self.delete = function () {
        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.city + "/" + city.CityID(),
                    data: ko.toJSON(city),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // hide current popup window
                            $("#modal-form").modal('hide');

                            // refresh data
                            GetData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    self.NewData = function (data) {
        // flag as new Data
        self.IsNewData(true);
        self.Readonly(false);

        //bind empty data
        self.CityCode('');
        self.Description('');
        //self.ProvinceID('');
        self.Provinsi(new ProvinsiModel('',''));
        //self.CreateDate('');

        $(':checkbox').prop('checked', false);
    }

    //Function to Read All Data
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.city,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);


            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }

    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    console.log("spUser Roles : " + ko.toJSON(spUser.Roles));
                    console.log("spUser Permited : " + data);
                    //compare user role to page permission to get checker validation
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(true);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }


    //untuk autocomplete   
    $("#provinsi-name").autocomplete({
        source: function (request, response) {
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.city + "/Search",
                data: {
                    query: request.term,
                    limit: 20
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
        },
        minLength: 2,
        select: function (event, ui) {
            // set CityModel
            if (ui.item.data != undefined || ui.item.data != null) {
                //self.ProvinceID(ui.item.data.ProvinceID);
                self.Provinsi(ko.mapping.toJS(ui.item.data, mapping));
               // self.Provinsi(new ProvinsiModel(ui.item.data.ProvinceID, ui.item.data.description));
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                // viewModel.TransactionModel().Customer(ko.mapping.toJS(ui.item.data, mapping));
                //self.ProvinceID(ko.mapping.toJS(ui.item.data, mapping));
                // customerNameData = ui.item.data.Name;


            }
            else
                self.Provinsi(new ProvinsiModel(null ,null));
            //  viewModel.TransactionModel().Customer(null);
        }
    });
    //  }

    // Autocomplete
    function OnSuccessAutoComplete(response, data, textStatus, jqXHR) {
        response($.map(data, function (item) {
            return {
                // default autocomplete object  
                label: item.Description,
                value: item.Description,

                // custom object binding
                data: {
                    ProvinceID: item.ProvinceID,
                    Description: item.Description                    
                }
            }
        })
        );
    }





    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];
        if (self.FilterCityCode() != "") filters.push({ Field: 'CityCode', Value: self.FilterCityCode() });
        if (self.FilterDescription() != "") filters.push({ Field: 'Description', Value: self.FilterDescription() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

        return filters;
    };

    self.GetSelectedRow = function (data) {

        self.IsNewData(false);

        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');

        self.UpdatedID(data.CityID);
        self.CityID(data.CityID);
        self.CityCode(data.CityCode);
        //self.ProvinceID(data.ProvinceID);
        self.Provinsi(data.Provinsi);
        self.Description(data.Description);
        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);
        self.HeaderTitle(data.Location);
        console.log(self.Provinsi());
    };

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.Cities(data.Rows);
            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));

            return self.isNew = false;
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }

    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });
}