﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class CurrencyController : ApiController
    {
        private string message = string.Empty;

        private readonly ICurrencyRepository repository = null;

        public CurrencyController()
        {
            this.repository = new CurrencyRepository();
        }

        public CurrencyController(ICurrencyRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all currencies.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<CurrencyModel>))]
        public HttpResponseMessage Get()
        {
            IList<CurrencyModel> output = new List<CurrencyModel>();

            if (!repository.GetCurrency(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CurrencyModel>>(HttpStatusCode.OK, output);
            }
        }

        #region IPE
        /// <summary>
        /// Get all currencies by currency.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<CurrencyModel>))]
        [HttpGet]
        [Route("api/Currency/IPE")]
        public HttpResponseMessage GetCurrencyBYID(int id)
        {
            IList<CurrencyModel> output = new List<CurrencyModel>();

            if (!repository.GetCurrencybyCurrency(ref output, id, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CurrencyModel>>(HttpStatusCode.OK, output);
            }
        }
        #endregion

        /// <summary>
        /// Get currencies with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<CurrencyRateModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<CurrencyRateModel> output = new List<CurrencyRateModel>();

            if (!repository.GetCurrency(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<CurrencyRateModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get currencies for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(CurrencyGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<CurrencyFilter> filters)
        {
            CurrencyGrid output = new CurrencyGrid();

            if (!repository.GetCurrency(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CurrencyGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find currencies using criterias Code or Description.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<CurrencyRateModel>))]
        [HttpGet]
        [Route("api/Currency/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<CurrencyRateModel> output = new List<CurrencyRateModel>();

            if (!repository.GetCurrency(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CurrencyRateModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get currency by ID.
        /// </summary>
        /// <param name="id">Currency ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(CurrencyRateModel))]
        [Route("api/Currency/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            CurrencyRateModel output = new CurrencyRateModel();

            if (!repository.GetCurrencyByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<CurrencyRateModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get currencyrate by ID.
        /// </summary>
        /// <param name="id">Currency ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(CurrencyRateModel))]
        [Route("api/Currency/CurrencyRate/{id}")]
        [HttpGet]
        public HttpResponseMessage GetCurrencyRate(int id)
        {
            CurrencyRateModel output = new CurrencyRateModel();

            if (!repository.GetCurrencyRateByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<CurrencyRateModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new currency.
        /// </summary>
        /// <param name="currency">Currency data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(CurrencyRateModel currency)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddCurrency(currency, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New currency data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing currency.
        /// </summary>
        /// <param name="id">ID of currency to be modify.</param>
        /// <param name="currency">Currency data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/Currency/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, CurrencyRateModel currency)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateCurrency(id, currency, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Currency data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting currency.
        /// </summary>
        /// <param name="id">ID of currency to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/Currency/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteCurrency(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Currency data has been deleted." });
                }
            }
        }
    }
}