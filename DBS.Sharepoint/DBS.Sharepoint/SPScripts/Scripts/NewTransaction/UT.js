﻿ChildModels["UT"] = function () {
    var self = this;
    console.log(this);
    self.Init = function (callback) {
        self.SetFundAutoCompleteFD();
        self.SetFundAutoCompleteFrom();
        self.SetFundAutoCompleteTo();
        self.SetInvestmentAutoComplete();
        self.SetCustomerAutoCompleteUTFNA();
        self.SetCustomerAutoCompleteUTNonFNA();
        callback();
    };

    self.Load = function (id, callback) {
        callback();
    }

    //function call when load pages
    self.Mutual = function () {
        console.log("debug");
        viewModel.IsSubscription(false);
        viewModel.IsRedemption(false);
        viewModel.IsSwitching(false);
        viewModel.IsSP(false);
        viewModel.TransactionUTModel().MutualFundCode(null);
        if (viewModel.ProductID() == ConsProductID.SavingPlanProductIDCons) {
            viewModel.IsSP(true);
            return;
        }
        var utSubc = ConsTransactionType.utSubcription;
        var utSwitch = ConsTransactionType.utSwitching;
        var utRedemp = ConsTransactionType.utRedemption;
        var sel = viewModel.Selected().Transaction_Type();
        if (utSubc.indexOf(sel) > -1) { viewModel.IsSubscription(true); }
        if (utSwitch.indexOf(sel) > -1) { viewModel.IsSwitching(true); }
        if (utRedemp.indexOf(sel) > -1) { viewModel.IsRedemption(true); }
    }

    self.MutualFundAdd = function () {
        viewModel.IsNewDataUT(true);
        viewModel.TransactionUTModel().MutualFund(null);
        viewModel.TransactionUTModel().MutualFundCode(null);
        viewModel.TransactionUTModel().MutualCurrency(null);
        viewModel.TransactionUTModel().MutualAmount(null);
        viewModel.TransactionUTModel().MutualFundSwitchFrom([]);
        viewModel.TransactionUTModel().MutualFundSwitchTo([]);
        viewModel.TransactionUTModel().MutualUnitNumber(null);
        viewModel.TransactionUTModel().MutualPartial(null);
        viewModel.TransactionUTModel().MutualFundList([]);
        self.Mutual();
        self.SetFundAutoCompleteFD();
        self.SetFundAutoCompleteFrom();
        self.SetFundAutoCompleteTo();
        $("#mutualfund-form").modal('show');
        $('.remove').click();
    }

    self.BindInvestmentSP = function () {
        var Invest = {
            url: api.server + api.url.transactionutsp + "/investIDSP/" + viewModel.TransactionUTModel().Investment().trim(),//updated by dani 23-6-2016
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(Invest, OnSuccessBindInvestmentSP, Helper.OnError, Helper.OnAlways);
    }

    self.SetFundAutoCompleteFD = function () {
            $("#MFund").autocomplete({
                source: function (request, response) {
                    // declare options variable for ajax get request
                    var options = {
                        url: api.server + api.url.cbofund + "/ParameterFund",
                        data: {
                            query: request.term,
                            limit: 20,
                            pid: GetIDFund()
                        },
                        token: accessToken
                    };

                    // exec ajax request
                    Helper.Ajax.AutoComplete(options, response, OnSuccessFundAutoComplete, viewModel.OnError);
                },
                minLength: 2,
                select: function (event, ui) {
                    // set customer data model
                    if (ui.item.data != undefined || ui.item.data != null) {
                        var mapping = {
                            'ignore': ["LastModifiedDate", "LastModifiedBy"]
                        };
                        viewModel.TransactionUTModel().MutualFundList(ko.mapping.toJS(ui.item.data, mapping));
                    }
                    else
                        viewModel.TransactionUTModel().MutualFundList(null);
                }
            });
    }

    self.SetFundAutoCompleteFrom = function () {
            $("#MFundFrom").autocomplete({
                source: function (request, response) {
                    // declare options variable for ajax get request
                    var options = {
                        url: api.server + api.url.cbofund + "/ParameterFund",
                        data: {
                            query: request.term,
                            limit: 20,
                            pid: GetIDFund()
                        },
                        token: accessToken
                    };

                    // exec ajax request
                    Helper.Ajax.AutoComplete(options, response, OnSuccessFundAutoComplete, Helper.OnError);
                },
                minLength: 2,
                select: function (event, ui) {
                    // set customer data model
                    if (ui.item.data != undefined || ui.item.data != null) {
                        var mapping = {
                            'ignore': ["LastModifiedDate", "LastModifiedBy"]
                        };
                        viewModel.TransactionUTModel().MutualFundSwitchFrom(ko.mapping.toJS(ui.item.data, mapping));
                    }
                    else
                        viewModel.TransactionUTModel().MutualFundSwitchFrom(null);
                }
            });
        }

    self.SetFundAutoCompleteTo = function () {
            $("#MFundTo").autocomplete({
                source: function (request, response) {
                    // declare options variable for ajax get request
                    var options = {
                        url: api.server + api.url.cbofund + "/ParameterFund",
                        data: {
                            query: request.term,
                            limit: 20,
                            pid: GetIDFund()
                        },
                        token: accessToken
                    };

                    // exec ajax request
                    Helper.Ajax.AutoComplete(options, response, OnSuccessFundAutoComplete, Helper.OnError);
                },
                minLength: 2,
                select: function (event, ui) {
                    // set customer data model
                    if (ui.item.data != undefined || ui.item.data != null) {
                        var mapping = {
                            'ignore': ["LastModifiedDate", "LastModifiedBy"]
                        };
                        viewModel.TransactionUTModel().MutualFundSwitchTo(ko.mapping.toJS(ui.item.data, mapping));
                    }
                    else
                        viewModel.TransactionUTModel().MutualFundSwitchTo(null);
                }
            });
        }

    self.SetInvestmentAutoComplete = function () {
        console.log("debug");
            $("#investmentid").autocomplete({
                source: function (request, response) {
                    // declare options variable for ajax get request
                    var options = {
                        url: api.server + api.url.transactionutin + "/Investment",
                        data: {
                            query: request.term,
                            limit: 20,
                            cif: viewModel.TransactionUTModel().Customer().CIF
                        },
                        token: accessToken
                    };

                    // exec ajax request
                    Helper.Ajax.AutoComplete(options, response, OnSuccessInvestmentAutoComplete, Helper.OnError);
                },
                minLength: 2,
                select: function (event, ui) {
                    // set customer data model
                    if (ui.item.data != undefined || ui.item.data != null) {
                        var mapping = {
                            'ignore': ["LastModifiedDate", "LastModifiedBy"]
                        };
                        viewModel.TransactionUTModel().InvestmentList(ko.mapping.toJS(ui.item.data, mapping));
                        viewModel.TransactionUTModel().Investment(ui.item.data.Investment);
                    }
                    else {
                        viewModel.TransactionUTModel().InvestmentList(null);
                        viewModel.TransactionUTModel().Investment(null);
                    }
                }
            });
        }

    self.SetCustomerAutoCompleteUTFNA = function () {
            $("#customer-nameJoinFna").autocomplete({
                source: function (request, response) {
                    // declare options variable for ajax get request
                    var options = {
                        url: api.server + api.url.customer + "/Search",
                        data: {
                            query: request.term,
                            limit: 20
                        },
                        token: accessToken
                    };

                    // exec ajax request
                    Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
                },
                minLength: 2,
                select: function (event, ui) {
                    // set customer data model
                    if (ui.item.data != undefined || ui.item.data != null) {
                        var mapping = {
                            'ignore': ["LastModifiedDate", "LastModifiedBy"]
                        };
                        viewModel.TransactionUTModel().CustomerJoin(ko.mapping.toJS(ui.item.data, mapping));
                    }
                    else
                        viewModel.TransactionUTModel().CustomerJoin(null);
                }
            });
        }

    self.SetCustomerAutoCompleteUTNonFNA = function () {
            $("#customer-nameJoinNonFna").autocomplete({
                source: function (request, response) {
                    // declare options variable for ajax get request
                    var options = {
                        url: api.server + api.url.customer + "/Search",
                        data: {
                            query: request.term,
                            limit: 20
                        },
                        token: accessToken
                    };

                    // exec ajax request
                    Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
                },
                minLength: 2,
                select: function (event, ui) {
                    // set customer data model
                    if (ui.item.data != undefined || ui.item.data != null) {
                        var mapping = {
                            'ignore': ["LastModifiedDate", "LastModifiedBy"]
                        };
                        viewModel.TransactionUTModel().CustomerJoin(ko.mapping.toJS(ui.item.data, mapping));
                    }
                    else
                        viewModel.TransactionUTModel().CustomerJoin(null);
                }
            });
        }

    function GetIDFund() {
        var ret = 0;
        switch (viewModel.ProductID()) {
            case ConsProductID.UTOffshoreProductIDCons:
                ret = 1;
                break;
            case ConsProductID.UTOnshoreproductIDCons:
                ret = 2;
                break;
            case ConsProductID.UTCPFProductIDCons:
                ret = 3;
                break;
            default:
                break
        }
        return ret;
    }

    function OnSuccessFundAutoComplete(response, data, textStatus, jqXHR) {
        response($.map(data, function (item) {
            return {
                label: item.FundCode + " - " + item.FundName,
                value: item.FundName,
                data: {
                    ID: item.ID,
                    OffOnshoreModel: item.OffOnshoreModel,
                    FundCode: item.FundCode,
                    FundName: item.FundName,
                    AccountNo: item.AccountNo,
                    SavingPlanDate: item.SavingPlanDate,
                    IsSwitching: item.IsSwitching,
                    IsSaving: item.IsSaving,
                    IsDeleted: item.IsDeleted,
                    LastModifiedDate: item.LastModifiedDate,
                    LastModifiedBy: item.LastModifiedBy
                }
            }
        })
        );
    }

    function OnSuccessInvestmentAutoComplete(response, data, textStatus, jqXHR) {
        response($.map(data, function (item) {
            return {
                label: item.Investment,
                value: item.Investment,
                data: {
                    InvestmentID: item.InvestmentID,
                    Investment: item.Investment,
                    CIF: item.CIF,
                    IsUT: item.IsUT,
                    IsBond: item.IsBond,
                    STNumber: item.STNumber,
                    CheckIN: item.CheckIN,
                    CheckCIF: item.CheckCIF,
                    CheckName: item.CheckName,
                    CheckST: item.CheckST
                }
            }
        })
        );
    }

    function OnSuccessBindInvestmentSP(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            if (data != null) {
                var IsSP = viewModel.IsSP();
                var IsSubscription = viewModel.IsSubscription();
                var IsRedemption = viewModel.IsRedemption();
                var IsSwitching = viewModel.IsSwitching();

                if (IsSP == true) {
                    viewModel.MutualFundColl(data);
                }
                else if (IsSubscription == true) {
                    viewModel.SubcriptionColl(data);
                }
                else if (IsRedemption == true) {
                    viewModel.RedemptionColl(data);
                }
                else if (IsSwitching == true) {
                    viewModel.SwitchingColl(data);
                }
            }
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    //end

    //function save draft & save trx UT
    self.SaveUT = function() {
        var form = $("#aspnetForm");
        form.validate();
        if (form.valid() && viewModel.TransactionUTModel().Customer().CIF != "") {
            viewModel.TransactionUTModel().IsDraft(false);
            viewModel.IsEditable(false);
            var data = {
                ApplicationID: viewModel.TransactionUTModel().ApplicationID(),
                CIF: viewModel.TransactionUTModel().Customer().CIF,
                Name: viewModel.TransactionUTModel().Customer().Name
            };
            if (viewModel.Documents().length > 0) {//dani dp S
                //for (var i = 0; i < viewModel.Documents().length; i++) {
                //    UploadFile(data, viewModel.Documents()[i], SaveTransactionUT);
                UploadFileRecuresive(data, viewModel.Documents(), SaveTransactionUT, viewModel.Documents().length);
                //}
            } else {
                SaveTransactionUT();
            }
        }
        else {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        }
    }

    self.SaveDraftUT = function() {
        var data = {
            ApplicationID: viewModel.TransactionUTModel().ApplicationID(),
            CIF: viewModel.TransactionUTModel().Customer().CIF,
            Name: viewModel.TransactionUTModel().Customer().Name
        };

        if (viewModel.Documents().length > 0) {
            //for (var i = 0; i < viewModel.Documents().length; i++) {
            //    UploadFile(data, viewModel.Documents()[i], SaveTransactionUT);
            UploadFileRecuresive(data, viewModel.Documents(), SaveTransactionUT, viewModel.Documents().length);
            //}
        } else {
            SaveTransactionUT();
        }
    }

    function SaveTransactionUT() {
        if (viewModel.TransactionUTModel().IsDraft() == true) {
            if (viewModel.TransactionUTModel().ID() == null) {
                switch (viewModel.ProductID()) {
                    case ConsProductID.IDInvestmentProductIDCons:
                        var selFNA = viewModel.Selected().FNACore();
                        var selFunc = viewModel.Selected().FunctionType();
                        var isJoin = viewModel.IsUTJoin();
                        if (isJoin == true) {
                            if (selFNA == ConsUTPar.fnaYes) {
                                viewModel.TransactionUTModel().UTJoin(viewModel.UTJoinFNA());
                            }
                            else if (selFNA == ConsUTPar.fnaNo) {
                                viewModel.TransactionUTModel().UTJoin(viewModel.UTJoinNonFNA());
                            }
                            else
                                viewModel.TransactionUTModel().UTJoin(null);
                        }
                        else
                            viewModel.TransactionUTModel().UTJoin(null);

                        var options = {
                            url: api.server + api.url.transactionutin,
                            token: accessToken,
                            data: ko.toJSON(viewModel.TransactionUTModel())
                        };
                        break;
                    case ConsProductID.UTCPFProductIDCons:
                    case ConsProductID.SavingPlanProductIDCons:
                    case ConsProductID.UTOffshoreProductIDCons:
                    case ConsProductID.UTOnshoreproductIDCons:
                        var IsSP = viewModel.IsSP();
                        var IsSubscription = viewModel.IsSubscription();
                        var IsRedemption = viewModel.IsRedemption();
                        var IsSwitching = viewModel.IsSwitching();

                        if (IsSP == true) {
                            viewModel.TransactionUTModel().MutualFundForms(viewModel.MutualFundColl());
                        }
                        else if (IsSubscription == true) {
                            var ret = viewModel.SubcriptionColl();
                            var sendVal = [];
                            for (var i = 0; i < ret.length; i++) {
                                var mf = {
                                    MutualFundList: ret[i].MutualFundList,
                                    MutualCurrency: null,
                                    MutualAmount: ret[i].MutualAmount,
                                    MutualFundSwitchFrom: null,
                                    MutualFundSwitchTo: null,
                                    MutualPartial: null,
                                    MutualUnitNumber: null,
                                    MutualSelected: null
                                };
                                sendVal.push(mf);
                            }

                            if (sendVal.length > 0) {
                                viewModel.TransactionUTModel().MutualFundForms(sendVal);
                            }
                        }
                        else if (IsRedemption == true) {
                            var ret = viewModel.RedemptionColl();
                            var sendVal = [];
                            for (var i = 0; i < ret.length; i++) {
                                if (ret[i].MutualSelected == true) {
                                    var mf = {
                                        MutualFundList: ret[i].MutualFundList,
                                        MutualCurrency: null,
                                        MutualAmount: null,
                                        MutualFundSwitchFrom: null,
                                        MutualFundSwitchTo: null,
                                        MutualPartial: ret[i].MutualPartial,
                                        MutualUnitNumber: ret[i].MutualUnitNumber,
                                        MutualSelected: null
                                    };
                                    sendVal.push(mf);
                                }
                            }
                            if (sendVal.length > 0) {
                                viewModel.TransactionUTModel().MutualFundForms(sendVal);
                            }
                        }
                        else if (IsSwitching == true) {
                            var ret = viewModel.SwitchingColl();
                            var sendVal = [];
                            for (var i = 0; i < ret.length; i++) {
                                if (ret[i].MutualSelected == true) {
                                    var mf = {
                                        MutualFundList: ret[i].MutualFundList,
                                        MutualCurrency: null,
                                        MutualAmount: null,
                                        MutualFundSwitchFrom: ret[i].MutualFundSwitchFrom,
                                        MutualFundSwitchTo: ret[i].MutualFundSwitchTo,
                                        MutualPartial: ret[i].MutualPartial,
                                        MutualUnitNumber: ret[i].MutualUnitNumber,
                                        MutualSelected: null
                                    };
                                    sendVal.push(mf);
                                }
                            }
                            if (sendVal.length > 0) {
                                viewModel.TransactionUTModel().MutualFundForms(sendVal);
                            }
                        }

                        var options = {
                            url: api.server + api.url.transactionutsp,
                            token: accessToken,
                            data: ko.toJSON(viewModel.TransactionUTModel())
                        };
                        break;
                }
                if (viewModel.TransactionUTModel().Documents().length == viewModel.Documents().length)
                    Helper.Ajax.Post(options, OnSuccessSaveDraftUT, viewModel.OnError, viewModel.OnAlways);
            }
            else {
                switch (viewModel.ProductID()) {
                    case ConsProductID.IDInvestmentProductIDCons:
                        var options = {
                            url: api.server + api.url.transactionutin + "/Draft/" + viewModel.TransactionUTModel().ID(),
                            token: accessToken,
                            data: ko.toJSON(viewModel.TransactionUTModel())
                        };
                        break;
                    case ConsProductID.UTCPFProductIDCons:
                    case ConsProductID.SavingPlanProductIDCons:
                    case ConsProductID.UTOffshoreProductIDCons:
                    case ConsProductID.UTOnshoreproductIDCons:
                        //added by dani dp 19-2-2016
                        var IsSP = viewModel.IsSP();
                        var IsSubscription = viewModel.IsSubscription();
                        var IsRedemption = viewModel.IsRedemption();
                        var IsSwitching = viewModel.IsSwitching();

                        if (IsSP == true) {
                            viewModel.TransactionUTModel().MutualFundForms(viewModel.MutualFundColl());
                        }
                        else if (IsSubscription == true) {
                            var ret = viewModel.SubcriptionColl();
                            var sendVal = [];
                            for (var i = 0; i < ret.length; i++) {
                                var mf = {
                                    MutualFundList: ret[i].MutualFundList,
                                    MutualCurrency: null,
                                    MutualAmount: ret[i].MutualAmount,
                                    MutualFundSwitchFrom: null,
                                    MutualFundSwitchTo: null,
                                    MutualPartial: null,
                                    MutualUnitNumber: null,
                                    MutualSelected: null
                                };
                                sendVal.push(mf);
                            }

                            if (sendVal.length > 0) {
                                viewModel.TransactionUTModel().MutualFundForms(sendVal);
                            }
                        }
                        else if (IsRedemption == true) {
                            var ret = viewModel.RedemptionColl();
                            var sendVal = [];
                            for (var i = 0; i < ret.length; i++) {
                                if (ret[i].MutualSelected == true) {
                                    var mf = {
                                        MutualFundList: ret[i].MutualFundList,
                                        MutualCurrency: null,
                                        MutualAmount: null,
                                        MutualFundSwitchFrom: null,
                                        MutualFundSwitchTo: null,
                                        MutualPartial: ret[i].MutualPartial,
                                        MutualUnitNumber: ret[i].MutualUnitNumber,
                                        MutualSelected: null
                                    };
                                    sendVal.push(mf);
                                }
                            }
                            if (sendVal.length > 0) {
                                viewModel.TransactionUTModel().MutualFundForms(sendVal);
                            }
                        }
                        else if (IsSwitching == true) {
                            var ret = viewModel.SwitchingColl();
                            var sendVal = [];
                            for (var i = 0; i < ret.length; i++) {
                                if (ret[i].MutualSelected == true) {
                                    var mf = {
                                        MutualFundList: ret[i].MutualFundList,
                                        MutualCurrency: null,
                                        MutualAmount: null,
                                        MutualFundSwitchFrom: ret[i].MutualFundSwitchFrom,
                                        MutualFundSwitchTo: ret[i].MutualFundSwitchTo,
                                        MutualPartial: ret[i].MutualPartial,
                                        MutualUnitNumber: ret[i].MutualUnitNumber,
                                        MutualSelected: null
                                    };
                                    sendVal.push(mf);
                                }
                            }
                            if (sendVal.length > 0) {
                                viewModel.TransactionUTModel().MutualFundForms(sendVal);
                            }
                        }
                        //added by dani dp 19-2-2016 end
                        var options = {
                            url: api.server + api.url.transactionutsp + "/Draft/" + viewModel.TransactionUTModel().ID(),
                            token: accessToken,
                            data: ko.toJSON(viewModel.TransactionUTModel())
                        };
                        break;
                }
                if (viewModel.TransactionUTModel().Documents().length == viewModel.Documents().length)
                    Helper.Ajax.Put(options, OnSuccessSaveDraftUT, viewModel.OnError, viewModel.OnAlways);
            }
            return;
        }
        if (viewModel.TransactionUTModel().Documents().length == viewModel.Documents().length) {
            switch (viewModel.ProductID()) {
                case ConsProductID.IDInvestmentProductIDCons:
                    var selFNA = viewModel.Selected().FNACore();
                    var selFunc = viewModel.Selected().FunctionType();
                    var isJoin = viewModel.IsUTJoin();
                    if (isJoin == true) {
                        if (selFNA == ConsUTPar.fnaYes) {
                            viewModel.TransactionUTModel().UTJoin(viewModel.UTJoinFNA());
                        }
                        else if (selFNA == ConsUTPar.fnaNo) {
                            viewModel.TransactionUTModel().UTJoin(viewModel.UTJoinNonFNA());
                        }
                        else
                            viewModel.TransactionUTModel().UTJoin(null);
                    }
                    else
                        viewModel.TransactionUTModel().UTJoin(null);

                    var options = {
                        url: api.server + api.url.transactionutin,
                        token: accessToken,
                        data: ko.toJSON(viewModel.TransactionUTModel())
                    };

                    break;
                case ConsProductID.UTCPFProductIDCons:
                case ConsProductID.SavingPlanProductIDCons:
                case ConsProductID.UTOffshoreProductIDCons:
                case ConsProductID.UTOnshoreproductIDCons:
                    var IsSP = viewModel.IsSP();
                    var IsSubscription = viewModel.IsSubscription();
                    var IsRedemption = viewModel.IsRedemption();
                    var IsSwitching = viewModel.IsSwitching();

                    if (IsSP == true) {
                        viewModel.TransactionUTModel().MutualFundForms(viewModel.MutualFundColl());
                    }
                    else if (IsSubscription == true) {
                        var ret = viewModel.SubcriptionColl();
                        var sendVal = [];
                        for (var i = 0; i < ret.length; i++) {
                            var mf = {
                                MutualFundList: ret[i].MutualFundList,
                                MutualCurrency: null,
                                MutualAmount: ret[i].MutualAmount,
                                MutualFundSwitchFrom: null,
                                MutualFundSwitchTo: null,
                                MutualPartial: null,
                                MutualUnitNumber: null,
                                MutualSelected: null
                            };
                            sendVal.push(mf);
                        }

                        if (sendVal.length > 0) {
                            viewModel.TransactionUTModel().MutualFundForms(sendVal);
                        }
                    }
                    else if (IsRedemption == true) {
                        var ret = viewModel.RedemptionColl();
                        var sendVal = [];
                        for (var i = 0; i < ret.length; i++) {
                            if (ret[i].MutualSelected == true) {
                                var mf = {
                                    MutualFundList: ret[i].MutualFundList,
                                    MutualCurrency: null,
                                    MutualAmount: null,
                                    MutualFundSwitchFrom: null,
                                    MutualFundSwitchTo: null,
                                    MutualPartial: ret[i].MutualPartial,
                                    MutualUnitNumber: ret[i].MutualUnitNumber,
                                    MutualSelected: null
                                };
                                sendVal.push(mf);
                            }
                        }
                        if (sendVal.length > 0) {
                            viewModel.TransactionUTModel().MutualFundForms(sendVal);
                        }
                    }
                    else if (IsSwitching == true) {
                        var ret = viewModel.SwitchingColl();
                        var sendVal = [];
                        for (var i = 0; i < ret.length; i++) {
                            if (ret[i].MutualSelected == true) {
                                var mf = {
                                    MutualFundList: ret[i].MutualFundList,
                                    MutualCurrency: null,
                                    MutualAmount: null,
                                    MutualFundSwitchFrom: ret[i].MutualFundSwitchFrom,
                                    MutualFundSwitchTo: ret[i].MutualFundSwitchTo,
                                    MutualPartial: ret[i].MutualPartial,
                                    MutualUnitNumber: ret[i].MutualUnitNumber,
                                    MutualSelected: null
                                };
                                sendVal.push(mf);
                            }
                        }
                        if (sendVal.length > 0) {
                            viewModel.TransactionUTModel().MutualFundForms(sendVal);
                        }
                    }

                    var options = {
                        url: api.server + api.url.transactionutsp,
                        token: accessToken,
                        data: ko.toJSON(viewModel.TransactionUTModel())
                    };
                    break;
            }

            if (viewModel.TransactionUTModel().ID() == null) {
                Helper.Ajax.Post(options, OnSuccessSaveUTAPI, viewModel.OnError, viewModel.OnAlways);
            } else {
                Helper.Ajax.Post(options, OnSuccessSaveUTAPI, viewModel.OnError, viewModel.OnAlways);
            }
        }
    }

    function OnSuccessSaveDraftUT(data, textStatus, jqXHR) {
        if (data.ID != null || data.ID != undefined) {
            viewModel.TransactionUTModel().ID(data.ID);
        }
        if (viewModel.IsCuttOff() == true) {
            viewModel.NotifHeader("Cut Off " + viewModel.strCuttOffTime());
            viewModel.NotifTitle("Attention");
            viewModel.NotifMessage("Transaction has exceeded cut off time, you need to attach email approval to continue cut off transaction. Please open Bring up file menu.");
            $("#modal-form-Notif").modal('show');
        }
        else {
            ShowNotification("Transaction Draft Success", "Transaction draft save attachments and underlying", "gritter-success", true);
            window.location = "/home/draft-transactions";
        }
    }

    function OnSuccessSaveUTAPI(data, textStatus, jqXHR) {
        // Get transaction id from api result
        if (data.ID != null || data.ID != undefined) {
            if (viewModel.TransactionUTModel().ID() != null) {
                var DraftID = viewModel.TransactionUTModel().ID();
                var AppID = {
                    TransactionID: data.ID,
                    ApplicationID: data.AppID
                };
                viewModel.RetIDColl([]);
                viewModel.RetIDColl.push(AppID);
                viewModel.TransactionUTModel().ApplicationID(data.AppID);
                viewModel.TransactionUTModel().ID(data.ID);
                viewModel.DeleteDraftTr(ProductName.ut, DraftID, data.ID);
            }
            else {
                var AppID = {
                    TransactionID: data.ID,
                    ApplicationID: data.AppID
                };
                viewModel.RetIDColl([]);
                viewModel.RetIDColl.push(AppID);
                viewModel.TransactionUTModel().ApplicationID(data.AppID);
                viewModel.TransactionUTModel().ID(data.ID);
                viewModel.AddListItem();
            }
        }
    }

    //end
}