﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Net.Http;
using System.Web.Script.Serialization;
using System.Transactions;
using System.Text.RegularExpressions;

//=======================================================================================//
///FOR ALL DEVELOPER...
///PLEASE ADD NEW REGION BY YOUR NAME TO KEEP CLEAN !!! DONT BE A DIRTY PROGRAMMER!!!
///ANDIWIJAYA
//=======================================================================================//

namespace DBS.WebAPI.Models
{
    #region Property
    #region Agung
    public class TransactionFDCheckerDetailModel
    {
        public FDDetailModel Transaction { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public TransactionFDCheckerDataModel Checker { get; set; }
        public IList<VerifyModel> Verify { get; set; }
    }
    public class TransactionFDCheckerDataModel
    {
        public long ID { get; set; }


        public string Others { get; set; }

        /// <summary>
        /// Signature Verification
        /// </summary>
        public bool IsSignatureVerified { get; set; }

        /// <summary>
        /// Dormant Account
        /// </summary>
        public bool IsDormantAccount { get; set; }

        /// <summary>
        /// Freeze Account
        /// </summary>
        public bool IsFreezeAccount { get; set; }

        public bool IsCallbackRequired { get; set; }



        public bool IsLOIAvailable { get; set; }



        public bool IsStatementLetterCopy { get; set; }
        /// <summary>
        /// LLD Details
        /// </summary>
        public LLDModel LLD { get; set; }
    }

    public class FDInterestMaintenanceModel
    {
        public long ID { get; set; }
        public CurrencyModel Currency { get; set; }
        public decimal? OriginalAmount { get; set; }
        public DateTime? RolloverDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public long? EmployeeID { get; set; }
        public string BranchCode { get; set; }
        public string IntRateCode { get; set; }
        public decimal? InterestRate { get; set; }
        public decimal? AcPrefIntCR { get; set; }
        public decimal? NetIntRate { get; set; }
        public decimal? InterestMargin { get; set; }
        public decimal? NewIntRate { get; set; }
        public string Tenor { get; set; }
        public decimal? FTPRate { get; set; }
        public decimal? SpreadRate { get; set; }
        public decimal? NetIntAmount { get; set; }
        public DateTime? MaturityDateInstruction { get; set; }
        public string ValueDateInstruction { get; set; }
        public DateTime? Date { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public bool? IsSelected { get; set; }
        public bool? SpecialFTP { get; set; }
        public string ApprovedBy { get; set; }
        public long FDTransactionInterestMaintenanceID { get; set; }

        public string CIF { get; set; }

        public string AccountNo { get; set; }

        public string CustomerName { get; set; }
        //Add by ady
        public string RenewalOption { get; set; }
        public string RepaymentAccount { get; set; }
        //end
    }

    /*public class FDDetailModel
    {
        public string DebitAccNumber;
        public string FDAccNumber;
        public string AttachmentRemarks;
        public TransactionTypeParameterModel TransactionType;
        public CurrencyModel Currency;
        /// <summary>
        /// Workflow Instance ID
        /// </summary>
        public Guid WorkflowInstanceID { get; set; }

        /// <summary>
        /// Transaction ID.
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Application ID
        /// </summary>
        public string ApplicationID { get; set; }


        /// <summary>
        /// Customer Data Details
        /// </summary>
        public CustomerModel Customer { get; set; }


        /// <summary>
        /// Product Details
        /// </summary>
        public ProductModel Product { get; set; }

        /// <summary>
        /// Channel Details
        /// </summary>
        public ChannelModel Channel { get; set; }

        /// <summary>
        /// Remakrs
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Create Date
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// List Transaction Document Details
        /// </summary>
        public IList<TransactionDocumentModel> Documents { get; set; }

        public long? ApproverID { get; set; }


    }
    */
    #endregion

    #region Dani
    public class FDDetailModel
    {
        public Guid? WorkflowInstanceID { get; set; }
        public long ID { get; set; }
        public long TransactionID { get; set; }
        public string ApplicationID { get; set; }
        public bool IsTopUrgent { get; set; }
        public bool IsTopUrgentChain { get; set; }
        public CustomerModel Customer { get; set; }
        public bool IsNewCustomer { get; set; }
        public ProductModel Product { get; set; }
        public CurrencyModel Currency { get; set; }
        public decimal Amount { get; set; }
        public decimal Rate { get; set; }
        public DateTime ApplicationDate { get; set; }
        public bool IsDraft { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public IList<TransactionDocumentModel> Documents { get; set; }
        public string CreateBy { get; set; }
        public TransactionTypeParameterModel TransactionType { get; set; }
        public int TransactionTypeID { get; set; }
        public string FDAccNumber { get; set; }
        public string FDBankName { get; set; }
        public string CreditAccNumber { get; set; }
        public string DebitAccNumber { get; set; }
        public decimal? InterestRate { get; set; }
        public string Tenor { get; set; }
        public DateTime? ValueDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public ParsysModel Remarks { get; set; }
        //public string Remarks { get; set; }
        public long? ApproverID { get; set; }
        public ChannelModel Channel { get; set; }
        public int SourceID { get; set; }
        public bool IsFrezeAccount { get; set; }
        public bool IsDocumentComplete { get; set; }
        public bool IsDormantAccount { get; set; }
        public bool IsSignatureVerified { get; set; }
        public string BankNameAccNumber { get; set; }
        public string AttachmentRemarks { get; set; }
        public string TransactionRemarks { get; set; }

        public bool? DocsComplete { set; get; }
        public decimal? AllInRate { set; get; }
        public string FTPRate { set; get; }
        public int? ApprovalName {set; get;}


    }
    public class TransactionPPUCheckerFDDetailModel
    {
        public FDDetailModel Transaction { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public TransactionCheckerDataModel Checker { get; set; }
        public IList<VerifyModel> Verify { get; set; }
    }

    public class FTPRateModel
    {
        public string Tenor { get; set; }
        public decimal IDR { get; set; }
        public decimal USD { set; get; }
        public decimal SGD { set; get; }
        public decimal JPY { set; get; }
        public decimal AUD { set; get; }
        public decimal NZD { set; get; }
        public decimal GBP { set; get; }
        public decimal EUR { set; get; }
        public decimal CHF { set; get; }
        public decimal CNH { set; get; }

    }
    public class MatrixFDDoa{
        public int ID {get; set;}
        public string Remarks {get; set;}
        public string Region {get; set;}
        public string Segment {get; set;}
        public string Role {get; set;}
        public string Rank {get; set;}
        public string Name {get; set;}
    }
    #endregion
    #region basri
    public class TransactionFDCallbackModel
    {
        public long? ApproverID { get; set; }
        public long ID { get; set; }

        /// <summary>
        /// Product Details
        /// </summary>
        public ProductModel Product { get; set; }

        /// <summary>
        /// Channel Details
        /// </summary>
        public ChannelModel Channel { get; set; }

        /// <summary>
        /// Others information
        /// </summary>
        public string Others { get; set; }

        /// <summary>
        /// Remarks
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        public bool IsSignatureVerified { get; set; }

        public bool IsDormantAccount { get; set; }

        public bool IsFreezeAccount { get; set; }

        public bool IsLOIAvailable { get; set; }

    }
    public class TransactionFDCallbackTimeModel
    {
        public long ID { get; set; }
        public long ApproverID { get; set; }
        public CustomerContactModel Contact { get; set; }
        public DateTime Time { get; set; }
        public bool? IsUTC { get; set; }
        public string Remark { get; set; }

    }
    public class TransactionFDCallbackDetailModel
    {
        public TransactionFDDetailModel Transaction { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public IList<TransactionFDCallbackTimeModel> Callbacks { get; set; }
        public IList<VerifyModel> Verify { get; set; }
    }
    public class TransactionFDCheckerAfterCallbackDetailModel
    {
        public TransactionFDDetailModel Transaction { get; set; }
        public TransactionFDCallbackModel Callback { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public TransactionFDCheckerDataModel Checker { get; set; }
        public IList<VerifyModel> Verify { get; set; }
    }
    #endregion

    #region Aridya
    public class FDMakerUserModel
    {
        public long TransactionID { get; set; }
        public long ApproverID { get; set; }
        public string LoginName { get; set; }
        public string DisplayName { get; set; }
        public string TransactionStatus { get; set; }
    }

    public class FDMakerHistoryModel
    {
        public string LoginName { get; set; }
        public string DisplayName { get; set; }
    }
    #endregion
    #endregion

    #region Interface
    public interface IWorkflowFDRepository : IDisposable
    {

        #region Agung
        bool UpdateTransactionPPUMakerFD(TransactionPPUCheckerFDDetailModel fd, ref string message);//*dani
        bool AddTransactionFDMaker(Guid workflowInstanceID, long approverID, TransactionFDCheckerDetailModel fd, ref string message);
        bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineModel> timelines, ref string message);
        bool GetFDCheckerDetails(Guid workflowInstanceID, long approverID, ref TransactionFDCheckerDetailModel output, ref string message);
        bool GetFDDetails(Guid workflowInstanceId, ref FDDetailModel output, ref string message);
        bool FDAddTransactionChecker(Guid workflowInstanceID, long approverID, TransactionFDCheckerDetailModel data, ref string message);
        bool FDInterestMaintenance(string logname, int searchBy, DateTime? date, ref IList<FDInterestMaintenanceModel> output, ref string message);
        bool FDCSOInterestMaintenance(IList<FDInterestMaintenanceModel> output, ref string message);
        bool AddTransactionFDCSO(Guid workflowInstanceID, long approverID, FDDetailModel fd, ref string message);
        bool GetTenorData(ref IList<string> tenor, ref string message);
        bool GetFTPRate(ref IList<FTPRateModel> output, ref string message);
        #endregion

        #region Dani
        bool GetTransactionFDPPUCheckerDetails(Guid workflowInstanceId, long approverID, ref TransactionPPUCheckerFDDetailModel output, ref string message);
        bool GetTransactionFDDetails(Guid workflowInstanceId, ref FDDetailModel output, ref string message);
        bool UpdateTransactionChecker(Guid workflowInstanceID, long approverID, TransactionPPUCheckerFDDetailModel data, ref string message);
        bool GetTransactionFDMakerReviewDetails(Guid workflowInstanceId, ref FDDetailModel output, ref string message);
        bool UpdateTransactionFDMakerReview(Guid workflowInstanceID, long approverID, TransactionPPUCheckerFDDetailModel data, ref string message);
        bool GetMatrixFdDOA(ref IList<MatrixFDDoa> output, ref string message);
        #endregion

        #region Basri
        bool AddTransactionCallback(Guid workflowInstanceID, long approverID, TransactionFDCallbackDetailModel data, ref string message);
        bool AddTransactionChecker(Guid workflowInstanceID, long approverID, TransactionFDCheckerDetailModel data, ref string message);
        bool GetTransactionDetails(Guid instanceID, ref TransactionFDDetailModel transaction, ref string message);
        bool GetTransactionCallbackDetails(Guid workflowInstanceID, long approverID, ref TransactionFDCallbackDetailModel output, ref string message);
        bool GetTransactionCheckerAfterCallbackDetails(Guid workflowInstanceID, long approverID, ref TransactionFDCheckerAfterCallbackDetailModel output, ref string message);
        bool GetTransactionMakerReviseAfterCheckerDetails(Guid instanceID, ref TransactionFDDetailModel transaction, ref string message);
        bool AddTransactionMakerAfterReviseCallerDetails(Guid workflowInstanceID, long approverID, TransactionFDDetailModel transaction, ref string message);
        #endregion

        #region Aridya
        bool GetTransactionFDMakerHistory(long transactionId, long approverId, FDMakerUserModel fdMakerUserModel, ref IList<FDMakerHistoryModel> output, ref string message);
        #endregion

    }
    #endregion

    #region Repository
    public class WorkflowFDRepository : IWorkflowFDRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();


        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #region Andi
        public bool AddTransactionFDCSO(Guid workflowInstanceID, long approverID, FDDetailModel fd, ref string message)
        {
            bool isSuccess = false;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    #region fd transaction
                    var Transaction = context.Transactions.Where(a=>a.TransactionID == fd.ID).SingleOrDefault();
                    if(Transaction != null) {
                        Transaction.FdMatrixDoaID = fd.ApprovalName;
                        Transaction.Tenor = fd.Tenor;
                        Transaction.FTPRate = fd.FTPRate;
                        context.SaveChanges();
                    }
                    #endregion
                    #region Document
                    if (fd.Documents.Count > 0)
                    {
                        var existingDocs = context.TransactionDocuments.Where(a => a.TransactionID.Equals(fd.ID)).ToList();
                        if (existingDocs != null)
                        {
                            if (existingDocs.Count() > 0)
                            {
                                foreach (var item in existingDocs)
                                {
                                    //var deleteDoc = context.TransactionDocuments.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                    //deleteDoc.IsDeleted = true;
                                    item.IsDeleted = true;
                                    context.SaveChanges();
                                }
                            }
                        }
                        foreach (var item in fd.Documents)
                        {
                            //if (item.ID == 0)//temporary
                            //{
                            Entity.TransactionDocument document = new Entity.TransactionDocument()
                            {
                                TransactionID = fd.ID,
                                DocTypeID = item.Type.ID,
                                PurposeID = item.Purpose.ID,
                                Filename = item.FileName,
                                DocumentPath = item.DocumentPath,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName
                            };
                            context.TransactionDocuments.Add(document);
                            //}
                        }
                        context.SaveChanges();
                    }
                    #endregion

                    ts.Complete();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.InnerException.ToString();
                    ts.Dispose();
                }
            }

            return isSuccess;
        }
        #endregion

        #region Agung
        public bool GetFTPRate(ref IList<FTPRateModel> output, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    output = (from a in context.IQuotes
                              select new FTPRateModel
                              {
                                  Tenor = a.Tenor,
                                  AUD = a.AUD ?? 0.0M,
                                  CHF = a.CHF ?? 0.0M,
                                  CNH = a.CNH ?? 0.0M,
                                  EUR = a.EUR ?? 0.0M,
                                  GBP = a.GBP ?? 0.0M,
                                  IDR = a.IDR ?? 0.0M,
                                  JPY = a.JPY ?? 0.0M,
                                  NZD = a.NZD ?? 0.0M,
                                  SGD = a.SGD ?? 0.0M,
                                  USD = a.USD ?? 0.0M
                              }).ToList<FTPRateModel>();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTenorData(ref IList<string> tenor, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    tenor = (from a in context.IQuotes
                             select a.Tenor).ToList<string>();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool FDInterestMaintenance(string logname, int searchBy, DateTime? date, ref IList<FDInterestMaintenanceModel> fdInterest, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    var Interest = (from a in context.FDMasterInterestMaintenances.AsEnumerable()
                                    join b in context.CSOes on a.CIF equals b.CIF
                                    join c in context.Employees on b.EmployeeID equals c.EmployeeID
                                    join d in context.Customers on a.CIF equals d.CIF
                                    where c.EmployeeUsername.Contains(logname)
                                    && (a.ValueDateInstruction.Equals("-") || a.ValueDateInstruction.Equals("") || (!a.ValueDateInstruction.ToLower().Contains("rollover")
                                    && Regex.IsMatch(a.ValueDateInstruction, @"([\W]+(ro|aro)[\W]+)|((ro|aro)[\W]+)|([\W]+(ro|aro))", RegexOptions.IgnoreCase)))
                                    && a.IsSelected != true
                                    //&& a.MaturityDate.Value.Year == date.Value.Year
                                    //&& a.MaturityDate.Value.Month == date.Value.Month
                                    //&& a.MaturityDate.Value.Day == date.Value.Day
                                    select new FDInterestMaintenanceModel
                                    {
                                        ID = a.FDInterestMaintenanceID,
                                        CIF = a.CIF,
                                        CustomerName = d.CustomerName,
                                        AccountNo = a.AccountNo,
                                        Currency = new CurrencyModel
                                        {
                                            ID = a.Currency.CurrencyID,
                                            Code = a.Currency.CurrencyCode,
                                            LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                            LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                        },
                                        OriginalAmount = a.OriginalAmount,
                                        RolloverDate = a.RolloverDate,
                                        MaturityDate = a.MaturityDate,
                                        EmployeeID = a.EmployeeID,
                                        BranchCode = a.BranchCode,
                                        IntRateCode = a.IntRateCode,
                                        InterestRate = a.InterestRate,
                                        AcPrefIntCR = a.AcPrefIntCR,
                                        NetIntRate = a.NetIntRate,
                                        InterestMargin = a.InterestMargin,
                                        NewIntRate = a.NewIntRate,
                                        FTPRate = a.FTPRate,
                                        SpreadRate = a.SpreadRate,
                                        NetIntAmount = a.NetIntAmount,
                                        MaturityDateInstruction = a.MaturityDateInstruction,
                                        ValueDateInstruction = a.ValueDateInstruction,
                                        Date = a.Date,
                                        RenewalOption = a.RenewalOption,
                                        RepaymentAccount = a.RepaymentAccount,
                                        CreateDate = a.CreateDate,
                                        CreateBy = a.CreateBy,
                                        IsSelected = a.IsSelected,
                                        SpecialFTP = false,
                                        ApprovedBy = ""
                                    }).ToList();
                    if (date != null)
                    {
                        if (searchBy == 1)
                        {
                            fdInterest = Interest.Where(w => w.CreateDate.Year == date.Value.Year && w.CreateDate.Month == date.Value.Month && w.CreateDate.Day == date.Value.Day).ToList<FDInterestMaintenanceModel>();
                        }
                        else
                        {
                            fdInterest = Interest.Where(w => w.RolloverDate.Value.Year == date.Value.Year && w.RolloverDate.Value.Month == date.Value.Month && w.RolloverDate.Value.Day == date.Value.Day).ToList<FDInterestMaintenanceModel>();
                        }
                    }
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool FDCSOInterestMaintenance(IList<FDInterestMaintenanceModel> fdInterest, ref string message)
        {
            bool isSuccess = false;

            try
            {
                context.Database.CommandTimeout = 600;
                foreach (FDInterestMaintenanceModel item in fdInterest)
                {
                    if (item.IsSelected == true || (item.ID == 0 && item.CIF != null))
                    {
                        DBS.Entity.Transaction data = new DBS.Entity.Transaction();
                        
                        #region Non Used Field Not Null
                        data.ApplicationDate = DateTime.Now;
                        data.CIF = item.CIF;
                        data.ProductID = 17;
                        data.CurrencyID = 1;
                        data.Amount = 0.00M;
                        data.AmountUSD = 0.00M;
                        data.Rate = 0.00M;
                        data.ChannelID = 1;

                        data.DebitCurrencyID = 1;
                        data.BizSegmentID = 1;
                        data.BeneName = "-";

                        data.BankID = 1;

                        data.IsResident = false;
                        data.IsCitizen = false;

                        data.IsTopUrgent = 0;
                        data.IsNewCustomer = false;
                        data.IsSignatureVerified = false;
                        data.IsDormantAccount = false;
                        data.IsFrezeAccount = false;
                        data.IsDocumentComplete = false;
                        data.IsDraft = false;

                        data.StateID = 1; // 1: On Progress, 2: Completed, 3: Canceled 
                        data.CreateDate = DateTime.UtcNow;
                        data.CreateBy = currentUser.GetCurrentUser().LoginName;
                        #endregion
                        
                        context.Transactions.Add(data);
                        context.SaveChanges();

                        // get inserted transactionid
                        var transactionID = data.TransactionID;

                        if (item.ID > 0)
                        {
                            DBS.Entity.FDTransactionInterestMaintenance FDIM = new DBS.Entity.FDTransactionInterestMaintenance();
                            
                            FDIM.TransactionID = transactionID;
                            FDIM.FDInterestMaintenanceID = item.ID;
                            FDIM.CreateDate = DateTime.UtcNow;
                            FDIM.CreateBy = currentUser.GetCurrentUser().LoginName;
                            
                            context.FDTransactionInterestMaintenances.Add(FDIM);
                            context.SaveChanges();
                            var FDTransactionInterestMaintenanceID = FDIM.FDTransactionInterestMaintenanceID;

                            Entity.FDMasterInterestMaintenance data_u = context.FDMasterInterestMaintenances.Where(a => a.FDInterestMaintenanceID.Equals(item.ID)).SingleOrDefault();

                            if (data_u != null)
                            {
                                data_u.IsSelected = true;

                                context.SaveChanges();
                            }

                            DBS.Entity.CSOFDInterestMaintenance CSOFD = new DBS.Entity.CSOFDInterestMaintenance();
                            
                            CSOFD.FDTransactionInterestMaintenanceID = FDTransactionInterestMaintenanceID;
                            CSOFD.CIF = item.CIF;
                            CSOFD.AccountNo = item.AccountNo;
                            CSOFD.CurrencyID = item.Currency.ID;
                            CSOFD.OriginalAmount = item.OriginalAmount;
                            CSOFD.RolloverDate = item.RolloverDate;
                            CSOFD.MaturityDate = item.MaturityDate;
                            CSOFD.EmployeeID = item.EmployeeID;
                            CSOFD.BranchCode = item.BranchCode;
                            CSOFD.IntRateCode = item.IntRateCode;
                            CSOFD.InterestRate = item.InterestRate;
                            CSOFD.AcPrefIntCR = item.AcPrefIntCR;
                            CSOFD.NetIntRate = item.NetIntRate;
                            CSOFD.InterestMargin = item.InterestMargin;
                            CSOFD.NewIntRate = item.NewIntRate;
                            CSOFD.FTPRate = item.FTPRate;
                            CSOFD.SpreadRate = item.SpreadRate;
                            CSOFD.NetIntAmount = item.NetIntAmount;
                            CSOFD.MaturityDateInstruction = item.MaturityDateInstruction;
                            CSOFD.ValueDateInstruction = item.ValueDateInstruction;
                            CSOFD.Date = item.Date;
                            CSOFD.RenewalOption = item.RenewalOption;
                            CSOFD.RepaymentAccount = item.RepaymentAccount;
                            CSOFD.Tenor = item.Tenor;
                            CSOFD.SpecialFTP = item.SpecialFTP;
                            CSOFD.ApprovedBy = item.ApprovedBy;
                            CSOFD.CreateDate = DateTime.UtcNow;
                            CSOFD.CreateBy = currentUser.GetCurrentUser().LoginName;
                            
                            context.CSOFDInterestMaintenances.Add(CSOFD);
                            context.SaveChanges();
                        }
                        else
                        {
                            Entity.FDMasterInterestMaintenance FMIM = new Entity.FDMasterInterestMaintenance();
                            FMIM.CIF = item.CIF;
                            FMIM.IsSelected = true;
                            FMIM.AccountNo = item.AccountNo;
                            FMIM.AcPrefIntCR = item.AcPrefIntCR;
                            FMIM.BranchCode = item.BranchCode;
                            FMIM.CreateBy = currentUser.GetCurrentUser().LoginName;
                            FMIM.CreateDate = DateTime.UtcNow;
                            FMIM.CurrencyID = item.Currency.ID > 0 ? item.Currency.ID : 1;
                            FMIM.Date = item.Date ?? DateTime.UtcNow;
                            FMIM.EmployeeID = item.EmployeeID;
                            FMIM.FTPRate = item.FTPRate;
                            FMIM.InterestMargin = item.InterestMargin;
                            FMIM.InterestRate = item.InterestRate;
                            FMIM.IntRateCode = item.IntRateCode;
                            FMIM.MaturityDate = item.MaturityDate;
                            FMIM.MaturityDateInstruction = item.MaturityDateInstruction;
                            FMIM.NetIntAmount = item.NetIntAmount;
                            FMIM.NetIntRate = item.NetIntRate;
                            FMIM.OriginalAmount = item.OriginalAmount;
                            FMIM.RenewalOption = item.RenewalOption;
                            FMIM.RepaymentAccount = item.RepaymentAccount;
                            FMIM.RolloverDate = item.RolloverDate;
                            FMIM.SpreadRate = item.SpreadRate;
                            FMIM.ValueDateInstruction = item.ValueDateInstruction;
                            FMIM.Tenor = item.Tenor;
                            FMIM.SpecialFTP = item.SpecialFTP;
                            FMIM.ApprovedBy = item.ApprovedBy;
                            context.FDMasterInterestMaintenances.Add(FMIM);
                            context.SaveChanges();

                            var FMIM_ID = FMIM.FDInterestMaintenanceID;

                            DBS.Entity.FDTransactionInterestMaintenance FDIM = new DBS.Entity.FDTransactionInterestMaintenance()
                            {
                                TransactionID = transactionID,
                                FDInterestMaintenanceID = FMIM_ID,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName
                            };
                            context.FDTransactionInterestMaintenances.Add(FDIM);
                            context.SaveChanges();
                            var FDTransactionInterestMaintenanceID = FDIM.FDTransactionInterestMaintenanceID;

                            DBS.Entity.CSOFDInterestMaintenance CSOFD = new DBS.Entity.CSOFDInterestMaintenance()
                            {
                                FDTransactionInterestMaintenanceID = FDTransactionInterestMaintenanceID,
                                CIF = item.CIF,
                                AccountNo = item.AccountNo,
                                CurrencyID = item.Currency.ID > 0 ? item.Currency.ID : 1,
                                OriginalAmount = item.OriginalAmount,
                                RolloverDate = item.RolloverDate,
                                MaturityDate = item.MaturityDate,
                                EmployeeID = item.EmployeeID,
                                BranchCode = item.BranchCode,
                                IntRateCode = item.IntRateCode,
                                InterestRate = item.InterestRate,
                                AcPrefIntCR = item.AcPrefIntCR,
                                NetIntRate = item.NetIntRate,
                                InterestMargin = item.InterestMargin,
                                NewIntRate = item.NewIntRate,
                                FTPRate = item.FTPRate,
                                SpreadRate = item.SpreadRate,
                                NetIntAmount = item.NetIntAmount,
                                MaturityDateInstruction = item.MaturityDateInstruction,
                                ValueDateInstruction = item.ValueDateInstruction,
                                Date = item.Date,
                                RenewalOption = item.RenewalOption,
                                RepaymentAccount = item.RepaymentAccount,
                                Tenor = item.Tenor,
                                SpecialFTP = item.SpecialFTP,
                                ApprovedBy = item.ApprovedBy,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName
                            };
                            context.CSOFDInterestMaintenances.Add(CSOFD);
                            context.SaveChanges();
                        }
                    }
                }
                isSuccess = true;

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineModel> timelines, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                //Remark Rizki 2015-04-21, ganti dari View ke SP             
                timelines = (from a in context.SP_GetNintexTaskTimeline(workflowInstanceID)
                             select new TransactionTimelineModel
                             {
                                 ApproverID = a.WorkflowApproverID,
                                 Activity = a.ActivityTitle,
                                 Time = a.ActivityTime,
                                 UserOrGroup = a.UserOrGroup,
                                 IsGroup = a.IsSPGroup.Value,
                                 Outcome = a.Outcome,
                                 DisplayName = a.DisplayName,
                                 Comment = a.Comment
                             }).ToList();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetFDCheckerDetails(Guid workflowInstanceID, long approverID, ref TransactionFDCheckerDetailModel output, ref string message)
        {
            bool IsSuccess = false;

            var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

            try
            {
                //output = (from a in context.Transactions.AsNoTracking()
                //          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                //          select new TransactionFDCheckerDetailModel
                //          {
                //              Verify = a.TransactionFDCheckers
                //                .Where(x => x.TransactionID.Equals(transactionID) && x.IsDeleted == false)
                //                .Select(x => new VerifyModel()
                //                {
                //                    ID = x.TransactionColumnID,
                //                    Name = x.TransactionColumn.ColumnName,
                //                    IsVerified = x.IsVerified
                //                }).ToList()
                //          }).SingleOrDefault();

                //// replace output while Checker & Verify is empty
                //if (output != null)
                //{
                //    // replace Checker while empty
                //    if (output.Checker == null)
                //    {
                //        var trans = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).SingleOrDefault();

                //        output.Checker = new TransactionFDCheckerDataModel()
                //        {
                //            ID = 0,
                //            Others = trans.Others,

                //        };
                //    }

                //    // replace verify list while empty
                //    //change isppucheckerloan by andry
                //    if (!output.Verify.Any())
                //    {
                //        output.Verify = context.TransactionColumns
                //            //.Where(x => x.IsPPUCheckerLoan.Equals(true) && x.IsDeleted.Equals(false))
                //            //.Where(x => x.IsPPUChecker.Equals(true) && x.IsDeleted.Equals(false))
                //            .Where(x => x.IsFDChecker.Equals(true) && x.IsDeleted.Equals(false))
                //            .Select(x => new VerifyModel()
                //            {
                //                ID = x.TransactionColumnID,
                //                Name = x.ColumnName,
                //                IsVerified = false
                //            })
                //            .ToList();
                //    }
                //}
                output = (from trans in context.Transactions.AsNoTracking()
                          where trans.WorkflowInstanceID.Value == workflowInstanceID
                          select new TransactionFDCheckerDetailModel
                          {
                              Verify = trans.TransactionFDCheckers
                                  .Where(x => x.TransactionID.Equals(trans.TransactionID) && x.IsDeleted == false)
                                  .Select(x => new VerifyModel()
                                  {
                                      ID = x.TransactionColumnID,
                                      Name = x.TransactionColumn.ColumnName,
                                      IsVerified = x.IsVerified
                                  }).ToList()
                          }).SingleOrDefault();
                if (output != null)
                {
                    if (!output.Verify.Any())
                    {
                        output.Verify = context.TransactionColumns
                            .Where(x => x.IsFDChecker.Value.Equals(true))
                            .Where(x => x.IsDeleted.Equals(false))
                            .Select(x => new VerifyModel()
                            {
                                ID = x.TransactionColumnID,
                                Name = x.ColumnName,
                                IsVerified = false
                            }).ToList();
                    }
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool FDAddTransactionChecker(Guid workflowInstanceID, long approverID, TransactionFDCheckerDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

                    if (transactionID > 0)
                    {
                        //Entity.Transaction data_t = context.Transactions.Where(a => a.TransactionID.Equals(transactionID)).SingleOrDefault();

                        var checker = context.TransactionFDCheckers
                           .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                           .ToList();


                        #region TransactionFDChecker
                        if (checker.Count > 0)
                        {
                           // var checkerdata = context.TransactionFDCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                            foreach (var item in checker)
                            {
                                context.TransactionFDCheckers.Remove(item);
                            }
                            context.SaveChanges();

                            foreach (var item in data.Verify)
                            {
                                context.TransactionFDCheckers.Add(new TransactionFDChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });

                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            foreach (var item in data.Verify)
                            {
                                context.TransactionFDCheckers.Add(new TransactionFDChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });

                                context.SaveChanges();
                            }
                        }
                        #endregion

                        checker = null;
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }
        public bool GetFDDetails(Guid workflowInstanceID, ref FDDetailModel output, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();
                // get main transaction data

                output = (from a in context.Transactions
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new FDDetailModel
                          {
                              ID = a.TransactionID,
                              WorkflowInstanceID = a.WorkflowInstanceID.Value,
                              ApplicationID = a.ApplicationID,
                              ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                              Amount = a.Amount,
                              DebitAccNumber = a.DebitAccNumber,
                              FDAccNumber = a.FDAccNumber,
                              AttachmentRemarks = a.AttachmentRemarks,//dani for agung
                              TransactionType = new TransactionTypeParameterModel()
                              {
                                  TransTypeID = a.TransactionType.TransTypeID,
                                  TransactionTypeName = a.TransactionType.TransactionType1,
                                  ProductID = a.TransactionType.ProductID
                              },
                              Currency = new CurrencyModel()
                              {
                                  ID = a.Currency.CurrencyID,//*dani
                                  Code = a.Currency.CurrencyCode,
                                  Description = a.Currency.CurrencyDescription,//dani for agung
                                  CodeDescription = a.Currency.CurrencyCode + " (" + a.Currency.CurrencyDescription + ")" //dani for agung
                              },
                              Product = new ProductModel()
                              {
                                  ID = a.Product.ProductID,
                                  Code = a.Product.ProductCode,
                                  Name = a.Product.ProductName,
                                  WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                  Workflow = a.Product.ProductWorkflow.WorkflowName,
                                  LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                  LastModifiedDate = a.Product.UpdateDate ?? a.Product.CreateDate
                              },
                              Channel = new ChannelModel()
                              {
                                  ID = a.Channel.ChannelID,
                                  Name = a.Channel.ChannelName,
                                  LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                  LastModifiedDate = a.Channel.UpdateDate ?? a.Channel.CreateDate
                              },
                              Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                              {
                                  ID = x.TransactionDocumentID,
                                  Type = new DocumentTypeModel()
                                  {
                                      ID = x.DocumentType.DocTypeID,
                                      Name = x.DocumentType.DocTypeName,
                                      Description = x.DocumentType.DocTypeDescription,
                                      LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                      LastModifiedDate = x.DocumentType.UpdateDate ?? x.DocumentType.CreateDate
                                  },
                                  Purpose = new DocumentPurposeModel()
                                  {
                                      ID = x.DocumentPurpose.PurposeID,
                                      Name = x.DocumentPurpose.PurposeName,
                                      Description = x.DocumentPurpose.PurposeDescription,
                                      LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                      LastModifiedDate = x.DocumentPurpose.UpdateDate ?? x.DocumentPurpose.CreateDate
                                  },
                                  FileName = x.Filename,
                                  DocumentPath = x.DocumentPath,
                                  LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                  LastModifiedDate = x.UpdateDate ?? x.CreateDate
                              }).ToList(),
                              SourceID = a.SourceID.HasValue ? a.SourceID.Value : 0,
                              CreateDate = a.CreateDate,
                              LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                              LastModifiedDate = a.UpdateDate ?? a.CreateDate,
                              IsTopUrgent = a.IsTopUrgent == 1 ? true : false,
                              IsTopUrgentChain = a.IsTopUrgent == 2 ? true : false,
                              InterestRate = a.InterestRate ?? null,
                              Tenor = a.Tenor,
                              ValueDate = a.ValueDate ?? null,
                              MaturityDate = a.MaturityDate ?? null,
                              BankNameAccNumber = a.FDBankName,
                              CreditAccNumber = a.CreditAccNumber,
                              DocsComplete = a.DocsComplete,
                              //AllInRate = a.AllInRate,
                              FTPRate = a.FTPRate,
                              ApprovalName = a.FdMatrixDoaID
                          }).SingleOrDefault();

                if (output != null)
                {
                    //dani for agung start
                    int IdRemarks = 0;
                    string IdRemarkString = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a.Remarks).SingleOrDefault();
                    bool IdRemarksBool = string.IsNullOrEmpty(IdRemarkString) ? false : int.TryParse(IdRemarkString.Trim(), out IdRemarks);
                    if (IdRemarksBool)
                    {
                        output.TransactionRemarks = context.ParameterSystems.Where(a => a.ParsysID.Equals(IdRemarks)).Select(a => a.ParameterValue).SingleOrDefault();
                        output.Remarks = new ParsysModel() { ID = IdRemarks, Name = context.ParameterSystems.Where(a => a.ParsysID.Equals(IdRemarks)).Select(a => a.ParameterValue).SingleOrDefault() };
                    }
                    else
                    {
                        output.TransactionRemarks = "";
                        output.Remarks = new ParsysModel() { ID = IdRemarks, Name = "" };
                    }
                    //dani for agung end
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    long TransactionID = output.ID;

                    bool isUnderlying = context.TransactionUnderlyings.Any(x => x.TransactionID.Equals(TransactionID));
                    if (isUnderlying)
                    {
                        if (customerRepo.GetCustomerByTransaction(TransactionID, ref customer, ref message))
                        {
                            output.Customer = customer;
                        }
                    }
                    else
                    {
                        if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                        {
                            output.Customer = customer;
                        }
                    }
                    customerRepo.Dispose();
                    customer = null;
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        //*dani
        public bool UpdateTransactionPPUMakerFD(TransactionPPUCheckerFDDetailModel fd, ref string message)
        {
            bool isSuccess = false;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.Transaction data = context.Transactions.Where(a => a.TransactionID.Equals(fd.Transaction.ID)).SingleOrDefault();
                    if (data != null)
                    {
                        //Update                    
                        data.ApplicationID = string.IsNullOrEmpty(fd.Transaction.ApplicationID) ? "" : fd.Transaction.ApplicationID;
                        data.WorkflowInstanceID = fd.Transaction.WorkflowInstanceID;
                        data.TransactionID = fd.Transaction.ID;
                        data.ApproverID = fd.Transaction.ApproverID ?? null;
                        data.ChannelID = fd.Transaction.Channel.ID;
                        data.SourceID = fd.Transaction.SourceID;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        //data.ApplicationDate = DateTime.Now;
                        data.IsTopUrgent = (fd.Transaction.IsTopUrgent == false && fd.Transaction.IsTopUrgentChain == false) ? 0 : (fd.Transaction.IsTopUrgent == true ? 1 : 2);
                        data.IsNewCustomer = fd.Transaction.IsNewCustomer;
                        data.CIF = fd.Transaction.Customer.CIF;
                        data.ProductID = fd.Transaction.Product.ID;
                        data.IsDocumentComplete = fd.Transaction.IsDocumentComplete;
                        data.IsDraft = fd.Transaction.IsDraft;
                        data.StateID = (int)StateID.OnProgress;
                        data.Remarks = fd.Transaction.Remarks.ID > 0 ? fd.Transaction.Remarks.ID.ToString() : "0";
                        data.ValueDate = fd.Transaction.ValueDate ?? null;
                        data.Amount = fd.Transaction.Amount > 0 ? fd.Transaction.Amount : 0;
                        data.CurrencyID = fd.Transaction.Currency.ID;
                        data.TransactionTypeID = fd.Transaction.TransactionType.TransTypeID;
                        data.FDAccNumber = string.IsNullOrEmpty(fd.Transaction.FDAccNumber) ? "" : fd.Transaction.FDAccNumber;
                        data.CreditAccNumber = string.IsNullOrEmpty(fd.Transaction.CreditAccNumber) ? "" : fd.Transaction.CreditAccNumber;
                        data.DebitAccNumber = string.IsNullOrEmpty(fd.Transaction.DebitAccNumber) ? "" : fd.Transaction.DebitAccNumber;
                        data.InterestRate = fd.Transaction.InterestRate;
                        data.Tenor = fd.Transaction.Tenor;
                        data.MaturityDate = fd.Transaction.MaturityDate;
                        data.ValueDate = fd.Transaction.ValueDate;
                        data.FDBankName = string.IsNullOrEmpty(fd.Transaction.BankNameAccNumber) ? "" : fd.Transaction.BankNameAccNumber;
                        data.AttachmentRemarks = string.IsNullOrEmpty(fd.Transaction.AttachmentRemarks) ? "" : fd.Transaction.AttachmentRemarks;
                        data.DocsComplete = fd.Transaction.DocsComplete;
                        //data.AllInRate = fd.Transaction.AllInRate;
                        data.FTPRate = fd.Transaction.FTPRate;
                        context.SaveChanges();
                    }
                    #region Save Document
                    var existingDocs = context.TransactionDocuments.Where(a => a.TransactionID.Equals(fd.Transaction.ID)).ToList();
                    if (existingDocs != null)
                    {
                        if (existingDocs.Count() > 0)
                        {
                            foreach (var item in existingDocs)
                            {
                                var deleteDoc = context.TransactionDocuments.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                deleteDoc.IsDeleted = true;
                                context.SaveChanges();
                            }
                        }
                    }
                    if (fd.Transaction.Documents.Count > 0)
                    {
                        foreach (var item in fd.Transaction.Documents)
                        {
                            Entity.TransactionDocument document = new Entity.TransactionDocument()
                            {
                                TransactionID = fd.Transaction.ID,
                                DocTypeID = item.Type.ID,
                                PurposeID = item.Purpose.ID,
                                Filename = item.FileName,
                                DocumentPath = item.DocumentPath,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName
                            };
                            context.TransactionDocuments.Add(document);
                        }
                        context.SaveChanges();
                    }
                    #endregion

                    ts.Complete();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.InnerException.ToString();
                    ts.Dispose();
                }
            }
            return isSuccess;
        }
        public bool AddTransactionFDMaker(Guid workflowInstanceID, long approverID, TransactionFDCheckerDetailModel fd, ref string message)
        {
            bool isSuccess = false;
            //dani for agung
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.TransactionFD data = context.TransactionFDs.Where(a => a.TransactionID.Equals(fd.Transaction.ID)).SingleOrDefault();
                    Entity.Transaction dataTrans = context.Transactions.Where(a => a.TransactionID.Equals(fd.Transaction.ID)).SingleOrDefault();

                    DBS.Entity.TransactionFD dataFD = new DBS.Entity.TransactionFD()
                    {
                        TransactionID = fd.Transaction.ID,
                        ApproverID = approverID,
                        //ChannelID = fd.Channel.ID,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().LoginName,
                        Amount = fd.Transaction.Amount > 0 ? fd.Transaction.Amount : 0,
                        CreditAccNumber = string.IsNullOrEmpty(fd.Transaction.CreditAccNumber) ? "" : fd.Transaction.CreditAccNumber,
                        DebitAccNumber = string.IsNullOrEmpty(fd.Transaction.DebitAccNumber) ? "" : fd.Transaction.DebitAccNumber,
                        FDAccNumber = string.IsNullOrEmpty(fd.Transaction.FDAccNumber) ? "" : fd.Transaction.FDAccNumber,
                        FDBankName = string.IsNullOrEmpty(fd.Transaction.BankNameAccNumber) ? "" : fd.Transaction.BankNameAccNumber,
                        InterestRate = fd.Transaction.InterestRate ?? null,
                        MaturityDate = fd.Transaction.MaturityDate ?? null,
                        WorkflowInstanceID = fd.Transaction.WorkflowInstanceID ?? null,
                        ValueDate = fd.Transaction.ValueDate ?? null,
                        TransactionTypeID = fd.Transaction.TransactionTypeID > 0 ? fd.Transaction.TransactionTypeID : fd.Transaction.TransactionType.TransTypeID,
                        Tenor = fd.Transaction.Tenor,
                        Rate = fd.Transaction.Rate > 0 ? fd.Transaction.Rate : 0,
                        DocsComplete = fd.Transaction.DocsComplete,
                        //AllInRate = fd.Transaction.AllInRate,
                        FTPRate = fd.Transaction.FTPRate
                    };
                    context.TransactionFDs.Add(dataFD);

                    if (dataTrans != null)
                    {
                        dataTrans.FDAccNumber = string.IsNullOrEmpty(fd.Transaction.FDAccNumber) ? "" : fd.Transaction.FDAccNumber;
                        dataTrans.SourceID = fd.Transaction.SourceID;
                    }
                    //}
                    context.SaveChanges();
                    ts.Complete();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.InnerException.ToString();
                    ts.Dispose();
                    //dani for agung
                }
            }

            return isSuccess;
        }

        #endregion

        #region Dani
        public bool GetTransactionFDPPUCheckerDetails(Guid workflowInstanceId, long approverID, ref TransactionPPUCheckerFDDetailModel output, ref string message)
        {
            bool IsSuccess = false;
            var transactionID = context.Transactions.Where(a => a.WorkflowInstanceID.Value == workflowInstanceId)
                                                    .Select(a => a.TransactionID)
                                                    .SingleOrDefault();
            try
            {
                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value == workflowInstanceId
                          select new TransactionPPUCheckerFDDetailModel
                          {
                              Checker = a.TransactionCheckerDatas.Where(x => x.TransactionID == transactionID).OrderByDescending(x => x.TransactionCheckerDataID)
                              .Select(x => new TransactionCheckerDataModel()
                              {
                                  ID = x.TransactionCheckerDataID,
                                  Others = x.Others,

                                  IsSignatureVerified = x.IsSignatureVerified,
                                  IsDormantAccount = x.IsDormantAccount,
                                  IsFreezeAccount = x.IsFreezeAccount,
                                  IsCallbackRequired = x.IsCallbackRequired,
                                  IsLOIAvailable = x.IsLOIAvailable
                              }).FirstOrDefault(),
                              Verify = a.TransactionCheckers
                                        .Where(x => x.TransactionID == transactionID && x.IsDeleted == false)
                                        .Select(x => new VerifyModel()
                                        {
                                            ID = x.TransactionColumnID,
                                            Name = x.TransactionColumn.ColumnName,
                                            IsVerified = x.IsVerified
                                        }).ToList(),
                          }).SingleOrDefault();

                if (output != null)
                {
                    if (output.Checker == null)
                    {
                        var trans = context.Transactions.Where(a => a.WorkflowInstanceID.Value == workflowInstanceId).SingleOrDefault();
                        output.Checker = new TransactionCheckerDataModel()
                        {
                            ID = 0,
                            Others = trans.Others
                        };
                    }
                    if (!output.Verify.Any())
                    {
                        output.Verify = context.TransactionColumns
                            .Where(x => x.IsPPUCheckerFD.Value == true && x.IsDeleted == false)
                           .Select(x => new VerifyModel()
                           {
                               ID = x.TransactionColumnID,
                               Name = x.ColumnName,
                               IsVerified = false
                           }).ToList();
                    }
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return IsSuccess;
        }
        public bool GetTransactionFDDetails(Guid workflowInstanceId, ref FDDetailModel output, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                context.Transactions.AsNoTracking();
                output = (from a in context.Transactions
                          where a.WorkflowInstanceID.Value == workflowInstanceId
                          select new FDDetailModel
                          {
                              ID = a.TransactionID,
                              WorkflowInstanceID = a.WorkflowInstanceID.Value,
                              ApplicationID = a.ApplicationID,
                              ApproverID = a.ApproverID != null ? a.ApproverID : null,
                              Product = new ProductModel()
                              {
                                  ID = a.Product.ProductID,
                                  Code = a.Product.ProductCode,
                                  Name = a.Product.ProductName,
                                  WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                  Workflow = a.Product.ProductWorkflow.WorkflowName,
                                  LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                  LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                              },
                              Channel = new ChannelModel()
                              {
                                  ID = a.Channel.ChannelID,
                                  Name = a.Channel.ChannelName,
                                  LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                  LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                              },
                              Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                              {
                                  ID = x.TransactionDocumentID,
                                  Type = new DocumentTypeModel()
                                  {
                                      ID = x.DocumentType.DocTypeID,
                                      Name = x.DocumentType.DocTypeName,
                                      Description = x.DocumentType.DocTypeDescription,
                                      LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                      LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                  },
                                  Purpose = new DocumentPurposeModel()
                                  {
                                      ID = x.DocumentPurpose.PurposeID,
                                      Name = x.DocumentPurpose.PurposeName,
                                      Description = x.DocumentPurpose.PurposeDescription,
                                      LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                      LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                  },
                                  FileName = x.Filename,
                                  DocumentPath = x.DocumentPath,
                                  LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                  LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                              }).ToList(),
                              Amount = a.Amount,
                              Currency = new CurrencyModel()
                              {
                                  ID = a.Currency.CurrencyID,
                                  Code = a.Currency.CurrencyCode,
                                  Description = a.Currency.CurrencyDescription,
                                  CodeDescription = a.Currency.CurrencyCode + "(" + a.Currency.CurrencyDescription + ")",
                                  LastModifiedBy = a.Currency.UpdateDate.HasValue ? a.Currency.UpdateBy : a.Currency.CreateBy,
                                  LastModifiedDate = a.Currency.UpdateDate.HasValue ? a.Currency.UpdateDate.Value : a.Currency.CreateDate
                              },
                              TransactionType = new TransactionTypeParameterModel()
                              {
                                  TransactionTypeName = a.TransactionType.TransactionType1,
                                  TransTypeID = a.TransactionType.TransTypeID,
                                  ProductID = a.TransactionType.ProductID
                              },
                              SourceID = a.SourceID.HasValue ? a.SourceID.Value : 0,
                              FDAccNumber = a.FDAccNumber,
                              DebitAccNumber = a.DebitAccNumber,
                              CreditAccNumber = a.CreditAccNumber,
                              InterestRate = a.InterestRate ?? null,
                              Tenor = a.Tenor,
                              ValueDate = a.ValueDate ?? null,
                              MaturityDate = a.MaturityDate ?? null,
                              FDBankName = a.FDBankName,
                              IsFrezeAccount = a.IsFrezeAccount,
                              IsDocumentComplete = a.IsDocumentComplete,
                              IsDormantAccount = a.IsDormantAccount,
                              IsSignatureVerified = a.IsSignatureVerified,
                              IsTopUrgent = a.IsTopUrgent == 1 ? true : false,
                              IsTopUrgentChain = a.IsTopUrgent == 2 ? true : false,
                              BankNameAccNumber = a.FDBankName,
                              AttachmentRemarks = a.AttachmentRemarks,
                              DocsComplete = a.DocsComplete,
                              //AllInRate = a.AllInRate,
                              CreateDate = a.CreateDate,
                              FTPRate = a.FTPRate,
                              LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                              LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                              ApprovalName = a.FdMatrixDoaID
                          }).SingleOrDefault();
                if (output != null)
                {
                    //baru 16-11-15
                    int IdRemarks = 0;
                    string IdRemarkString = context.Transactions.Where(a => a.WorkflowInstanceID.Value == workflowInstanceId).Select(a => a.Remarks).SingleOrDefault();
                    bool IdRemarksBool = string.IsNullOrEmpty(IdRemarkString) ? false : int.TryParse(IdRemarkString.Trim(), out IdRemarks);
                    if (IdRemarksBool)
                    {
                        output.TransactionRemarks = context.ParameterSystems.Where(a => a.ParsysID.Equals(IdRemarks)).Select(a => a.ParameterValue).SingleOrDefault();
                        output.Remarks = new ParsysModel() { ID = IdRemarks, Name = context.ParameterSystems.Where(a => a.ParsysID.Equals(IdRemarks)).Select(a => a.ParameterValue).SingleOrDefault() };
                    }
                    //baru
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value == workflowInstanceId).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    long TransactionID = output.ID;

                    bool isUnderlying = context.TransactionUnderlyings.Any(x => x.TransactionID.Equals(TransactionID));
                    if (isUnderlying)
                    {
                        if (customerRepo.GetCustomerByTransaction(TransactionID, ref customer, ref message))
                        {
                            output.Customer = customer;
                        }
                    }
                    else
                    {
                        if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                        {
                            output.Customer = customer;
                        }
                    }
                    customerRepo.Dispose();
                    customer = null;
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return IsSuccess;
        }

        public bool UpdateTransactionChecker(Guid workflowInstanceID, long approverID, TransactionPPUCheckerFDDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value == workflowInstanceID)
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

                    if (transactionID > 0)
                    {
                        Entity.Transaction data_t = context.Transactions.Where(a => a.TransactionID.Equals(transactionID)).SingleOrDefault();
                        if (data_t != null)
                        {
                            data_t.IsDocumentComplete = data.Transaction.IsDocumentComplete;
                            context.SaveChanges();
                        }

                        var checker = context.TransactionCheckers
                           .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                           .ToList();

                        var checkerData = context.TransactionCheckerDatas
                            .Where(a => a.TransactionID.Equals(transactionID))
                            .SingleOrDefault();

                        #region Transaction Checker Data
                        if (checkerData != null)
                        {
                            checkerData.Others = data.Checker.Others;
                            checkerData.IsSignatureVerified = data.Checker.IsSignatureVerified;
                            checkerData.IsDormantAccount = data.Checker.IsDormantAccount;
                            checkerData.IsFreezeAccount = data.Checker.IsFreezeAccount;
                            checkerData.IsCallbackRequired = data.Checker.IsCallbackRequired;
                            checkerData.IsLOIAvailable = data.Checker.IsLOIAvailable;
                            checkerData.UpdateDate = DateTime.UtcNow;
                            checkerData.UpdateBy = currentUser.GetCurrentUser().LoginName;

                            context.SaveChanges();
                        }
                        else
                        {

                            context.TransactionCheckerDatas.Add(new TransactionCheckerData()
                            {
                                ApproverID = approverID,
                                TransactionID = transactionID,
                                Others = data.Checker.Others,
                                IsSignatureVerified = data.Checker.IsSignatureVerified,
                                IsDormantAccount = data.Checker.IsDormantAccount,
                                IsFreezeAccount = data.Checker.IsFreezeAccount,
                                IsCallbackRequired = data.Checker.IsCallbackRequired,
                                IsLOIAvailable = data.Checker.IsLOIAvailable,
                                CreatedDate = DateTime.UtcNow,
                                CreatedBy = currentUser.GetCurrentUser().LoginName
                            });

                            context.SaveChanges();

                        }
                        #endregion

                        #region Transaction Checker
                        if (checker.Count > 0)
                        {
                            var checkerdata = context.TransactionCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                            foreach (var item in checkerdata)
                            {
                                context.TransactionCheckers.Remove(item);
                            }
                            context.SaveChanges();

                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });

                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });

                                context.SaveChanges();
                            }
                        }
                        #endregion

                        checker = null;
                        checkerData = null;
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }

        public bool GetTransactionFDMakerReviewDetails(Guid workflowInstanceId, ref FDDetailModel output, ref string message)
        {
            bool isSuccess = false;
            try
            {
                context.Transactions.AsNoTracking();
                //get transactionID 
                long transactionID = context.Transactions
                       .Where(a => a.WorkflowInstanceID.Value == workflowInstanceId)
                       .Select(a => a.TransactionID)
                       .SingleOrDefault();
                //jika transactionID ada
                if (transactionID > 0)
                {
                    long transFD = (from transfd in context.TransactionFDs
                                    where transfd.TransactionID.Equals(transactionID)
                                    orderby transfd.UpdateDate ?? transfd.CreateDate descending
                                    select transfd.TransactionFDID).FirstOrDefault();
                    //jika ada data pada tabel transaction FD
                    if (transFD > 0)
                    {
                        output = (from trans in context.Transactions
                                  join transFd in context.TransactionFDs
                                  on trans.TransactionID equals transFd.TransactionID
                                  where trans.TransactionID == transactionID
                                  orderby transFd.UpdateDate ?? transFd.CreateDate descending
                                  select new FDDetailModel
                                  {
                                      WorkflowInstanceID = trans.WorkflowInstanceID,
                                      Amount = transFd.Amount ?? 0,
                                      ApplicationDate = trans.ApplicationDate,
                                      ApplicationID = trans.ApplicationID,
                                      ApproverID = transFd.ApproverID,
                                      AttachmentRemarks = trans.AttachmentRemarks,
                                      BankNameAccNumber = transFd.FDBankName,
                                      Channel = new ChannelModel()
                                      {
                                          ID = trans.Channel.ChannelID,
                                          Name = trans.Channel.ChannelName,
                                          LastModifiedBy = trans.Channel.UpdateDate == null ? trans.Channel.CreateBy : trans.Channel.UpdateBy,
                                          LastModifiedDate = trans.Channel.UpdateDate ?? trans.Channel.CreateDate
                                      },
                                      CreditAccNumber = transFd.CreditAccNumber,
                                      Currency = new CurrencyModel()
                                      {
                                          ID = trans.Currency.CurrencyID,
                                          Code = trans.Currency.CurrencyCode,
                                          Description = trans.Currency.CurrencyDescription,
                                          CodeDescription = trans.Currency.CurrencyCode + "(" + trans.Currency.CurrencyDescription + ")",
                                          LastModifiedBy = trans.Currency.UpdateDate.HasValue ? trans.Currency.UpdateBy : trans.Currency.CreateBy,
                                          LastModifiedDate = trans.Currency.UpdateDate ?? trans.Currency.CreateDate
                                      },
                                      DebitAccNumber = transFd.DebitAccNumber,
                                      Documents = trans.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                                      {
                                          ID = x.TransactionDocumentID,
                                          Type = new DocumentTypeModel()
                                          {
                                              ID = x.DocumentType.DocTypeID,
                                              Name = x.DocumentType.DocTypeName,
                                              Description = x.DocumentType.DocTypeDescription,
                                              LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                              LastModifiedDate = x.DocumentType.UpdateDate ?? x.DocumentType.CreateDate
                                          },
                                          Purpose = new DocumentPurposeModel()
                                          {
                                              ID = x.DocumentPurpose.PurposeID,
                                              Name = x.DocumentPurpose.PurposeName,
                                              Description = x.DocumentPurpose.PurposeDescription,
                                              LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                              LastModifiedDate = x.DocumentPurpose.UpdateDate ?? x.DocumentPurpose.CreateDate
                                          },
                                          FileName = x.Filename,
                                          DocumentPath = x.DocumentPath,
                                          LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                          LastModifiedDate = x.UpdateDate ?? x.CreateDate
                                      }).ToList(),
                                      SourceID = trans.SourceID.HasValue ? trans.SourceID.Value : 0,
                                      FDAccNumber = transFd.FDAccNumber,
                                      FDBankName = transFd.FDBankName,
                                      ID = trans.TransactionID,
                                      InterestRate = transFd.InterestRate,
                                      IsDocumentComplete = trans.IsDocumentComplete,
                                      IsDormantAccount = trans.IsDormantAccount,
                                      IsDraft = trans.IsDraft,
                                      IsFrezeAccount = trans.IsFrezeAccount,
                                      IsNewCustomer = trans.IsNewCustomer,
                                      IsSignatureVerified = trans.IsSignatureVerified,
                                      IsTopUrgent = trans.IsTopUrgent == 1 ? true : false,
                                      IsTopUrgentChain = trans.IsTopUrgent == 2 ? true : false,
                                      LastModifiedBy = transFd.UpdateDate.HasValue ? transFd.UpdateBy : transFd.CreateBy,
                                      LastModifiedDate = transFd.UpdateDate ?? transFd.CreateDate,
                                      MaturityDate = transFd.MaturityDate,
                                      Product = new ProductModel()
                                      {
                                          ID = trans.Product.ProductID,
                                          Code = trans.Product.ProductCode,
                                          Name = trans.Product.ProductName,
                                          WorkflowID = trans.Product.ProductWorkflow.WorkflowID,
                                          Workflow = trans.Product.ProductWorkflow.WorkflowName,
                                          LastModifiedBy = trans.Product.UpdateDate == null ? trans.Product.CreateBy : trans.Product.UpdateBy,
                                          LastModifiedDate = trans.Product.UpdateDate ?? trans.Product.CreateDate
                                      },
                                      Rate = transFd.Rate ?? 0,
                                      Tenor = transFd.Tenor,
                                      TransactionRemarks = trans.Remarks,
                                      TransactionType = new TransactionTypeParameterModel()
                                      {
                                          TransactionTypeName = trans.TransactionType.TransactionType1,
                                          TransTypeID = trans.TransactionTypeID ?? trans.TransactionType.TransTypeID,
                                          ProductID = trans.ProductID > 0 ? trans.ProductID : trans.TransactionType.ProductID,
                                      },
                                      ValueDate = transFd.ValueDate,
                                      TransactionTypeID = trans.TransactionTypeID ?? trans.TransactionType.TransTypeID,
                                      DocsComplete = trans.DocsComplete,
                                      //AllInRate = trans.AllInRate,
                                      FTPRate = trans.FTPRate,
                                      //Remarks =  new ParsysModel {}
                                      ApprovalName = trans.FdMatrixDoaID
                                  }).FirstOrDefault();
                        isSuccess = true;
                    }
                    else
                    {
                        output = (from trans in context.Transactions
                                  where trans.TransactionID == transactionID
                                  select new FDDetailModel
                                  {
                                      ID = trans.TransactionID,
                                      WorkflowInstanceID = trans.WorkflowInstanceID.Value,
                                      ApplicationID = trans.ApplicationID,
                                      ApproverID = trans.ApproverID ?? null,
                                      Product = new ProductModel()
                                      {
                                          ID = trans.Product.ProductID,
                                          Code = trans.Product.ProductCode,
                                          Name = trans.Product.ProductName,
                                          WorkflowID = trans.Product.ProductWorkflow.WorkflowID,
                                          Workflow = trans.Product.ProductWorkflow.WorkflowName,
                                          LastModifiedBy = trans.Product.UpdateDate == null ? trans.Product.CreateBy : trans.Product.UpdateBy,
                                          LastModifiedDate = trans.Product.UpdateDate ?? trans.Product.CreateDate
                                      },
                                      Channel = new ChannelModel()
                                      {
                                          ID = trans.Channel.ChannelID,
                                          Name = trans.Channel.ChannelName,
                                          LastModifiedBy = trans.Channel.UpdateDate == null ? trans.Channel.CreateBy : trans.Channel.UpdateBy,
                                          LastModifiedDate = trans.Channel.UpdateDate ?? trans.Channel.CreateDate
                                      },
                                      Documents = trans.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                                      {
                                          ID = x.TransactionDocumentID,
                                          Type = new DocumentTypeModel()
                                          {
                                              ID = x.DocumentType.DocTypeID,
                                              Name = x.DocumentType.DocTypeName,
                                              Description = x.DocumentType.DocTypeDescription,
                                              LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                              LastModifiedDate = x.DocumentType.UpdateDate ?? x.DocumentType.CreateDate
                                          },
                                          Purpose = new DocumentPurposeModel()
                                          {
                                              ID = x.DocumentPurpose.PurposeID,
                                              Name = x.DocumentPurpose.PurposeName,
                                              Description = x.DocumentPurpose.PurposeDescription,
                                              LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                              LastModifiedDate = x.DocumentPurpose.UpdateDate ?? x.DocumentPurpose.CreateDate
                                          },
                                          FileName = x.Filename,
                                          DocumentPath = x.DocumentPath,
                                          LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                          LastModifiedDate = x.UpdateDate ?? x.CreateDate
                                      }).ToList(),
                                      Amount = trans.Amount,
                                      Currency = new CurrencyModel()
                                      {
                                          ID = trans.Currency.CurrencyID,
                                          Code = trans.Currency.CurrencyCode,
                                          Description = trans.Currency.CurrencyDescription,
                                          CodeDescription = trans.Currency.CurrencyCode + "(" + trans.Currency.CurrencyDescription + ")",
                                          LastModifiedBy = trans.Currency.UpdateDate.HasValue ? trans.Currency.UpdateBy : trans.Currency.CreateBy,
                                          LastModifiedDate = trans.Currency.UpdateDate ?? trans.Currency.CreateDate
                                      },
                                      TransactionType = new TransactionTypeParameterModel()
                                      {
                                          TransactionTypeName = trans.TransactionType.TransactionType1,
                                          TransTypeID = trans.TransactionTypeID ?? trans.TransactionType.TransTypeID,
                                          ProductID = trans.ProductID > 0 ? trans.ProductID : trans.TransactionType.ProductID,
                                      },
                                      SourceID = trans.SourceID.HasValue ? trans.SourceID.Value : 0,
                                      TransactionTypeID = trans.TransactionTypeID ?? trans.TransactionType.TransTypeID,
                                      FDAccNumber = trans.FDAccNumber,
                                      DebitAccNumber = trans.DebitAccNumber,
                                      CreditAccNumber = trans.CreditAccNumber,
                                      InterestRate = trans.InterestRate ?? null,
                                      Tenor = trans.Tenor,
                                      ValueDate = trans.ValueDate ?? null,
                                      MaturityDate = trans.MaturityDate ?? null,
                                      FDBankName = trans.FDBankName,
                                      IsFrezeAccount = trans.IsFrezeAccount,
                                      IsDocumentComplete = trans.IsDocumentComplete,
                                      IsDormantAccount = trans.IsDormantAccount,
                                      IsSignatureVerified = trans.IsSignatureVerified,
                                      IsTopUrgent = trans.IsTopUrgent == 1 ? true : false,
                                      IsTopUrgentChain = trans.IsTopUrgent == 2 ? true : false,
                                      BankNameAccNumber = trans.FDBankName,
                                      TransactionRemarks = trans.Remarks,
                                      AttachmentRemarks = trans.AttachmentRemarks,
                                      CreateDate = trans.CreateDate,
                                      LastModifiedBy = trans.UpdateDate == null ? trans.CreateBy : trans.UpdateBy,
                                      LastModifiedDate = trans.UpdateDate ?? trans.CreateDate,
                                      DocsComplete = trans.DocsComplete,
                                      //AllInRate = trans.AllInRate
                                      //Remarks =  new ParsysModel { }
                                      ApprovalName = trans.FdMatrixDoaID
                                  }).SingleOrDefault();
                        isSuccess = true;
                    }
                    if (output != null)
                    {
                        string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value == workflowInstanceId).Select(a => a.Customer.CIF).FirstOrDefault();
                        ICustomerRepository customerRepo = new CustomerRepository();
                        CustomerModel customer = new CustomerModel();
                        long TransactionID = output.ID;

                        bool isUnderlying = context.TransactionUnderlyings.Any(x => x.TransactionID.Equals(TransactionID));
                        if (isUnderlying)
                        {
                            if (customerRepo.GetCustomerByTransaction(TransactionID, ref customer, ref message))
                            {
                                output.Customer = customer;
                            }
                        }
                        else
                        {
                            if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                            {
                                output.Customer = customer;
                            }
                        }
                        customerRepo.Dispose();
                        customer = null;
                        //updated by dani start 25-1-2016
                        ParsysModel transRemarks = new ParsysModel();
                        int parseTemp = 0;
                        if (!string.IsNullOrEmpty(output.TransactionRemarks))
                        {
                            if (int.TryParse(output.TransactionRemarks, out parseTemp))
                            {
                                transRemarks.ID = parseTemp;
                                transRemarks.Name = "";
                            }
                            else
                            {
                                transRemarks.ID = 0;
                                transRemarks.Name = "";
                            }
                            output.Remarks = transRemarks;
                        }
                        //updated by dani end
                    }
                    isSuccess = true;
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool UpdateTransactionFDMakerReview(Guid workflowInstanceID, long approverID, TransactionPPUCheckerFDDetailModel data, ref string message)
        {
            bool isSuccess = false;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value == workflowInstanceID)
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

                    if (transactionID > 0)
                    {
                        Entity.Transaction data_t = context.Transactions.Where(a => a.TransactionID.Equals(transactionID)).SingleOrDefault();
                        if (data_t != null)
                        {
                            data_t.FDAccNumber = data.Transaction.FDAccNumber;
                            data_t.IsDocumentComplete = data.Transaction.IsDocumentComplete;
                            data_t.AttachmentRemarks = string.IsNullOrEmpty(data.Transaction.AttachmentRemarks) ? "" : data.Transaction.AttachmentRemarks;

                            //Tambah
                            data_t.ChannelID = data.Transaction.Channel.ID;
                            data_t.SourceID = data.Transaction.SourceID;
                            data_t.CurrencyID = data.Transaction.Currency.ID;
                            data_t.Amount = data.Transaction.Amount;
                            data_t.CreditAccNumber = data.Transaction.CreditAccNumber;
                            data_t.InterestRate = data.Transaction.InterestRate;
                            data_t.ValueDate = data.Transaction.ValueDate;
                            data_t.MaturityDate = data.Transaction.MaturityDate;
                            if (data.Transaction.Remarks != null)
                            {
                                data_t.Remarks = data.Transaction.Remarks.ID.ToString();
                            }
                            data_t.FDBankName = data.Transaction.BankNameAccNumber;
                            data_t.Tenor = data.Transaction.Tenor;
                            data_t.DocsComplete = data.Transaction.DocsComplete;
                            //data_t.AllInRate = data.Transaction.AllInRate;
                            data_t.FTPRate = data.Transaction.FTPRate;
                            context.SaveChanges();
                        }

                        context.TransactionFDs.Add(new TransactionFD()
                        {
                            Amount = data.Transaction.Amount,
                            ApproverID = data.Transaction.ApproverID,
                            CreateBy = currentUser.GetCurrentUser().LoginName,
                            CreateDate = DateTime.UtcNow,
                            CreditAccNumber = data.Transaction.CreditAccNumber,
                            DebitAccNumber = data.Transaction.DebitAccNumber,
                            FDAccNumber = data.Transaction.FDAccNumber,
                            FDBankName = data.Transaction.FDBankName,
                            InterestRate = data.Transaction.InterestRate,
                            MaturityDate = data.Transaction.MaturityDate,
                            Remarks = data.Transaction.TransactionRemarks,
                            Tenor = data.Transaction.Tenor,
                            TransactionID = data.Transaction.ID,
                            WorkflowInstanceID = data.Transaction.WorkflowInstanceID,
                            ValueDate = data.Transaction.ValueDate,
                            TransactionTypeID = data.Transaction.TransactionType.TransTypeID,
                            Rate = data.Transaction.Rate,
                            DocsComplete = data.Transaction.DocsComplete,
                            //AllInRate = data.Transaction.AllInRate
                        });
                        context.SaveChanges();
                        isSuccess = true;
                        ts.Complete();
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }
            return isSuccess;
        }
        public bool GetMatrixFdDOA(ref IList<MatrixFDDoa> output, ref string message)
        {
             bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    output = (from a in context.FDMatrixDOAs
                              select new MatrixFDDoa
                              {
                                  ID = a.FDMatrixDOAID,
                                  Name = a.Name,
                                  Rank = a.Rank,
                                  Region = a.Region,
                                  Remarks = a.Remarks,
                                  Role = a.Role,
                                  Segment = a.Segment
                              }).ToList<MatrixFDDoa>();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        #endregion

        #region Basri
        public bool AddTransactionMakerAfterReviseCallerDetails(Guid workflowInstanceID, long approverID, TransactionFDDetailModel transaction, ref string message)
        {
            bool isSuccess = false;
            try
            {
                var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();
                if (transactionID != null && transactionID > 0)
                {
                    DBS.Entity.Transaction data = context.Transactions.Where(x => x.TransactionID.Equals(transactionID)).SingleOrDefault();

                    #region Primary Field
                    data.CIF = transaction.Customer.CIF;
                    data.ProductID = transaction.Product.ID;
                    data.CreateDate = DateTime.UtcNow;
                    data.CreateBy = currentUser.GetCurrentUser().LoginName;
                    data.ValueDate = transaction.ValueDate;
                    data.Amount = transaction.Amount;
                    data.CurrencyID = transaction.Currency.ID;
                    data.TransactionTypeID = transaction.TransactionType.TransTypeID;
                    data.FDAccNumber = transaction.FDAccNumber;
                    data.CreditAccNumber = transaction.CreditAccNumber;
                    data.DebitAccNumber = transaction.DebitAccNumber;
                    data.InterestRate = transaction.InterestRate;
                    data.Tenor = transaction.Tenor;
                    data.MaturityDate = transaction.MaturityDate;
                    data.FDBankName = transaction.FDBankName;
                    data.AttachmentRemarks = transaction.AttachmentRemarks;
                    //data.DocsComplete = transaction.DocsComplete;
                    //data.AllInRate = transaction.AllInRate;
                    //data.FTPRate = transaction.FTPRate;

                    context.SaveChanges();

                    isSuccess = true;
                    #endregion

                }
            }
            catch (Exception Ex)
            {
                message = Ex.InnerException.Message;
            }
            return isSuccess;

        }
        public bool AddTransactionChecker(Guid workflowInstanceID, long approverID, TransactionFDCheckerDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

                    if (transactionID > 0)
                    {
                        Entity.Transaction data_t = context.Transactions.Where(a => a.TransactionID.Equals(transactionID)).SingleOrDefault();
                        if (data_t != null)
                        {
                            data_t.IsDocumentComplete = data.Transaction.IsDocumentComplete;
                            context.SaveChanges();
                        }

                        var checker = context.TransactionCheckers
                           .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                           .ToList();

                        var checkerData = context.TransactionCheckerDatas
                            .Where(a => a.TransactionID.Equals(transactionID))
                            .SingleOrDefault();

                        #region Transaction Checker Data
                        if (checkerData != null)
                        {


                            checkerData.Others = data.Checker.Others;
                            checkerData.IsSignatureVerified = data.Checker.IsSignatureVerified;
                            checkerData.IsDormantAccount = data.Checker.IsDormantAccount;
                            checkerData.IsFreezeAccount = data.Checker.IsFreezeAccount;
                            checkerData.IsCallbackRequired = data.Checker.IsCallbackRequired;
                            checkerData.IsLOIAvailable = data.Checker.IsLOIAvailable;
                            checkerData.UpdateDate = DateTime.UtcNow;
                            checkerData.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                            context.SaveChanges();
                        }
                        else
                        {

                            context.TransactionCheckerDatas.Add(new TransactionCheckerData()
                            {
                                ApproverID = approverID,
                                TransactionID = transactionID,
                                Others = data.Checker.Others,
                                IsSignatureVerified = data.Checker.IsSignatureVerified,
                                IsDormantAccount = data.Checker.IsDormantAccount,
                                IsFreezeAccount = data.Checker.IsFreezeAccount,
                                IsCallbackRequired = data.Checker.IsCallbackRequired,
                                IsLOIAvailable = data.Checker.IsLOIAvailable,
                                CreatedDate = DateTime.UtcNow,
                                CreatedBy = currentUser.GetCurrentUser().DisplayName
                            });

                            context.SaveChanges();

                        }
                        #endregion

                        #region Transaction Checker
                        if (checker.Count > 0)
                        {
                            var checkerdata = context.TransactionCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                            foreach (var item in checkerdata)
                            {
                                context.TransactionCheckers.Remove(item);
                            }
                            context.SaveChanges();

                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });

                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });

                                context.SaveChanges();
                            }
                        }
                        #endregion

                        checker = null;
                        checkerData = null;
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }
        public bool AddTransactionCallback(Guid workflowInstanceID, long approverID, TransactionFDCallbackDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            //feedbackcaller

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {

                    // insert trasaction FD callback data
                    DBS.Entity.TransactionCallbackData dataCallback = new DBS.Entity.TransactionCallbackData()
                    {
                        TransactionID = data.Transaction.ID,
                        ApproverID = approverID,
                        ChannelID = data.Transaction.Channel.ID,
                        SourceID = data.Transaction.SourceID,
                        IsDormantAccount = data.Transaction.IsDormantAccount,
                        IsFreezeAccount = data.Transaction.IsFreezeAccount,
                        IsSignatureVerified = data.Transaction.IsSignatureVerified,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName,
                    };
                    //hardcode for prevent error while saving, due to different model between payment product and TMO product
                    dataCallback.Amount = 0;
                    dataCallback.AmountUSD = 0;
                    dataCallback.Rate = 0;
                    dataCallback.ApplicationDate = DateTime.UtcNow;
                    dataCallback.CurrencyID = 1;
                    dataCallback.DebitCurrencyID = 1;

                    context.TransactionCallbackDatas.Add(dataCallback);
                    context.SaveChanges();

                    // insert callback
                    foreach (var item in data.Callbacks)
                    {
                        if (item.ID == 0 && item.Time.Hour > 0)
                        {
                            // Insert transaction callback
                            TransactionCallback callback = new TransactionCallback()
                            {
                                ApproverID = approverID,
                                TransactionID = data.Transaction.ID,
                                Time = item.Time.ToUniversalTime(),
                                IsUTC = item.IsUTC.Value,
                                Remark = item.Remark,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName
                            };

                            context.TransactionCallbacks.Add(callback);
                            context.SaveChanges();

                            // Insert transaction callback contact
                            if (item.Contact != null)
                            {
                                context.TransactionCallbackContacts.Add(new TransactionCallbackContact()
                                {
                                    TransactionCallbackID = callback.TransactionCallbackID,
                                    ContactID = item.Contact.ID,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });
                            }
                            context.SaveChanges();

                            callback = null;
                        }
                    }

                    //flag callback draft as deleted
                    List<Entity.TransactionCallbackDraft> callbackDraft = context.TransactionCallbackDrafts.Where(a => a.TransactionID == data.Transaction.ID && a.IsDeleted != true).ToList();
                    if (callbackDraft.Count > 0)
                    {
                        foreach (var item in callbackDraft)
                        {
                            item.IsDeleted = true;
                            item.UpdateDate = DateTime.UtcNow;
                            item.UpdateBy = currentUser.GetCurrentUser().LoginName;
                            context.SaveChanges();
                        }
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }
        public bool GetTransactionDetails(Guid instanceID, ref TransactionFDDetailModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                TransactionFDDetailModel datatransaction = (from a in context.Transactions
                                                            where a.WorkflowInstanceID.Value.Equals(instanceID)
                                                            select new TransactionFDDetailModel
                                                            {
                                                                ID = a.TransactionID,
                                                                WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                                                ApplicationID = a.ApplicationID,
                                                                ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                                                                IsTopUrgent = a.IsTopUrgent == 1 ? true : false,
                                                                IsTopUrgentChain = a.IsTopUrgent == 2 ? true : false,
                                                                Product = new ProductModel()
                                                                {
                                                                    ID = a.Product.ProductID,
                                                                    Code = a.Product.ProductCode,
                                                                    Name = a.Product.ProductName,
                                                                    WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                                                    Workflow = a.Product.ProductWorkflow.WorkflowName,
                                                                    LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                                                    LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                                                },
                                                                Channel = new ChannelModel()
                                                                {
                                                                    ID = a.Channel.ChannelID,
                                                                    Name = a.Channel.ChannelName,
                                                                    LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                                                    LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                                                },
                                                                Currency = new CurrencyModel()
                                                                {
                                                                    ID = a.Currency.CurrencyID,
                                                                    Code = a.Currency.CurrencyCode,
                                                                    Description = a.Currency.CurrencyDescription,
                                                                    CodeDescription = a.Currency.CurrencyCode + "(" + a.Currency.CurrencyDescription + ")",
                                                                    LastModifiedBy = a.Currency.UpdateDate.HasValue ? a.Currency.UpdateBy : a.Currency.CreateBy,
                                                                    LastModifiedDate = a.Currency.UpdateDate.HasValue ? a.Currency.UpdateDate.Value : a.Currency.CreateDate
                                                                },
                                                                TransactionType = a.TransactionType != null ? new TransactionTypeParameterModel()
                                                                {
                                                                    TransactionTypeName = a.TransactionType.TransactionType1,
                                                                    TransTypeID = a.TransactionType.TransTypeID,
                                                                    ProductID = a.TransactionType.ProductID
                                                                } : new TransactionTypeParameterModel()
                                                                {
                                                                    TransactionTypeName = "",
                                                                    TransTypeID = 0,
                                                                    ProductID = 0
                                                                },
                                                                SourceID = a.SourceID.HasValue ? a.SourceID.Value : 0,
                                                                Amount = a.Amount,
                                                                FDAccNumber = a.FDAccNumber,
                                                                DebitAccNumber = a.DebitAccNumber,
                                                                CreditAccNumber = a.CreditAccNumber,
                                                                InterestRate = a.InterestRate ?? null,
                                                                Tenor = a.Tenor ?? null,
                                                                ValueDate = a.ValueDate ?? null,
                                                                MaturityDate = a.MaturityDate ?? null,
                                                                BankNameAccNumber = a.FDBankName,
                                                                AttachmentRemarks = a.AttachmentRemarks,
                                                                IsSignatureVerified = a.IsSignatureVerified,
                                                                IsFreezeAccount = a.IsFrezeAccount,
                                                                IsDormantAccount = a.IsDormantAccount,
                                                                IsDocumentComplete = a.IsDocumentComplete,
                                                                IsLOIAvailable = a.IsLOI == null ? false : a.IsLOI == false ? false : true,
                                                                Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                                                                {
                                                                    ID = x.TransactionDocumentID,
                                                                    Type = new DocumentTypeModel()
                                                                    {
                                                                        ID = x.DocumentType.DocTypeID,
                                                                        Name = x.DocumentType.DocTypeName,
                                                                        Description = x.DocumentType.DocTypeDescription,
                                                                        LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                        LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                    },
                                                                    Purpose = new DocumentPurposeModel()
                                                                    {
                                                                        ID = x.DocumentPurpose.PurposeID,
                                                                        Name = x.DocumentPurpose.PurposeName,
                                                                        Description = x.DocumentPurpose.PurposeDescription,
                                                                        LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                        LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                                    },
                                                                    FileName = x.Filename,
                                                                    DocumentPath = x.DocumentPath,
                                                                    LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                    LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value,
                                                                    IsDormant = x.IsDormant == null ? false : x.IsDormant
                                                                }).ToList(),
                                                                CreateDate = a.CreateDate,
                                                                LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                                                DocsComplete = a.DocsComplete,
                                                                //AllInRate = a.AllInRate,
                                                                FTPRate = a.FTPRate
                                                            }).SingleOrDefault();

                // fill payment data
                if (datatransaction != null)
                {
                    int IdRemarks = 0;
                    string IdRemarkString = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(instanceID)).Select(a => a.Remarks).SingleOrDefault();
                    bool IdRemarksBool = string.IsNullOrEmpty(IdRemarkString) ? false : int.TryParse(IdRemarkString.Trim(), out IdRemarks);
                    if (IdRemarksBool)
                    {
                        datatransaction.TransactionRemarks = context.ParameterSystems.Where(a => a.ParsysID.Equals(IdRemarks)).Select(a => a.ParameterValue).SingleOrDefault();
                    }

                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(instanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();

                    if (customerRepo.GetCustomerByTransaction(datatransaction.ID, ref customer, ref message))
                    {
                        datatransaction.Customer = customer;
                    }

                    long IDTrns = datatransaction.ID;
                    var checker = context.TransactionCheckerDatas.Where(x => x.TransactionID.Equals(IDTrns)).Select(x => x).OrderByDescending(b => b.ApproverID).FirstOrDefault();
                    if (checker != null)
                    {
                        datatransaction.IsSignatureVerified = checker.IsSignatureVerified;
                        datatransaction.IsDocumentComplete = datatransaction.IsDocumentComplete;
                        datatransaction.IsCallbackRequired = checker.IsCallbackRequired;
                        datatransaction.IsLOIAvailable = checker.IsLOIAvailable;
                        datatransaction.IsDormantAccount = checker.IsDormantAccount;
                        datatransaction.IsFreezeAccount = checker.IsFreezeAccount;
                    }
                    transaction = datatransaction;
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionCallbackDetails(Guid instanceID, long approverID, ref TransactionFDCallbackDetailModel output, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                var transactionID = context.Transactions
                       .Where(a => a.WorkflowInstanceID.Value.Equals(instanceID))
                       .Select(a => a.TransactionID)
                       .SingleOrDefault();

                var callbackID = context.TransactionCallbacks
                    .Where(a => a.IsUTC == false && a.TransactionID.Equals(transactionID))
                    .OrderByDescending(a => a.TransactionCallbackID)
                    .Select(a => a.TransactionCallbackID)
                    .FirstOrDefault();

                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(instanceID)
                          select new TransactionFDCallbackDetailModel
                          {
                              Callbacks = (from b in context.TransactionCallbacks
                                           where b.TransactionID.Equals(transactionID)
                                           select new TransactionFDCallbackTimeModel
                                           {
                                               ID = b.TransactionCallbackID,
                                               ApproverID = b.ApproverID,
                                               Contact = b.TransactionCallbackContacts.Select(y => new CustomerContactModel()
                                               {
                                                   ID = y.CustomerContact.ContactID,
                                                   Name = y.CustomerContact.ContactName,
                                                   PhoneNumber = y.CustomerContact.PhoneNumber,
                                                   DateOfBirth = y.CustomerContact.DateOfBirth,
                                                   IDNumber = y.CustomerContact.IdentificationNumber,
                                               }).FirstOrDefault(),
                                               Time = b.Time,
                                               IsUTC = b.IsUTC,
                                               Remark = b.Remark
                                           }).ToList(),
                              Verify = a.TransactionCallbackCheckers
                                .Select(z => new VerifyModel()
                                {
                                    ID = z.TransactionColumnID,
                                    Name = z.TransactionColumn.ColumnName,
                                    IsVerified = z.IsVerified
                                }).ToList()
                          }).SingleOrDefault();

                // replace verify list while empty
                if (output != null)
                {

                    output.Verify = (from a in context.TransactionColumns
                                     where a.IsDeleted.Equals(false) && a.IsCallBack.Equals(true)
                                     select new VerifyModel()
                                     {
                                         ID = a.TransactionColumnID,
                                         Name = a.ColumnName,
                                         IsVerified = false
                                     }).ToList();


                    var documentCompleteness = (from a in context.TransactionCheckers
                                                join b in context.TransactionColumns
                                                on a.TransactionColumnID equals b.TransactionColumnID
                                                where a.TransactionID.Equals(transactionID) && b.ColumnName.Equals("Document Completeness") && a.IsDeleted == false
                                                select new VerifyModel()
                                                {
                                                    ID = a.TransactionColumnID,
                                                    Name = b.ColumnName,
                                                    IsVerified = a.IsVerified
                                                }).SingleOrDefault();

                    if (documentCompleteness != null)
                    {
                        output.Verify.Add(documentCompleteness);
                    }
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionCheckerAfterCallbackDetails(Guid workflowInstanceID, long approverID, ref TransactionFDCheckerAfterCallbackDetailModel output, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                var transactionID = context.Transactions
                       .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                       .Select(a => a.TransactionID)
                       .SingleOrDefault();

                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new TransactionFDCheckerAfterCallbackDetailModel
                          {
                              Callback = a.TransactionCallbackDatas
                                .OrderByDescending(x => x.ApproverID)
                                .Select(x => new TransactionFDCallbackModel()
                                {
                                    ID = x.TransactionCallbackDataID,
                                    Product = new ProductModel()
                                    {
                                        ID = a.Product.ProductID,
                                        Code = a.Product.ProductCode,
                                        Name = a.Product.ProductName,
                                        WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                        Workflow = a.Product.ProductWorkflow.WorkflowName,
                                        LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                        LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                    },
                                    Channel = new ChannelModel()
                                    {
                                        ID = x.Channel.ChannelID,
                                        Name = x.Channel.ChannelName,
                                        LastModifiedBy = x.Channel.UpdateDate == null ? x.Channel.CreateBy : x.Channel.UpdateBy,
                                        LastModifiedDate = x.Channel.UpdateDate == null ? x.Channel.CreateDate : x.Channel.UpdateDate.Value
                                    },

                                    IsSignatureVerified = x.IsSignatureVerified,
                                    IsDormantAccount = x.IsDormantAccount,
                                    IsFreezeAccount = x.IsFreezeAccount,
                                    Others = x.Other,
                                    LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                    LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                }).FirstOrDefault(),
                              Checker = a.TransactionCheckerDatas
                                .Where(x => x.TransactionID.Equals(transactionID))
                                .Select(x => new TransactionFDCheckerDataModel()
                                {
                                    ID = x.TransactionCheckerDataID,
                                    Others = x.Others,
                                    IsSignatureVerified = x.IsSignatureVerified,
                                    IsDormantAccount = x.IsDormantAccount,
                                    IsFreezeAccount = x.IsFreezeAccount,
                                    IsLOIAvailable = x.IsLOIAvailable,
                                    IsCallbackRequired = x.IsCallbackRequired
                                }).FirstOrDefault(),
                              Verify = a.TransactionCallbackCheckers
                                .Where(x => x.IsDeleted == false)
                                .Select(x => new VerifyModel()
                                {
                                    ID = x.TransactionColumnID,
                                    Name = x.TransactionColumn.ColumnName,
                                    IsVerified = x.IsVerified
                                }).ToList()
                          }).SingleOrDefault();

                // replace output while Checker & Verify is empty
                if (output != null)
                {

                    // replace checker data while empty with data from caller
                    if (output.Checker == null)
                    {
                        output.Checker = new TransactionFDCheckerDataModel()
                        {
                            ID = 0,
                            Others = output.Callback.Others,
                            IsSignatureVerified = output.Callback.IsSignatureVerified,
                            IsDormantAccount = output.Callback.IsDormantAccount,
                            IsFreezeAccount = output.Callback.IsFreezeAccount,
                            IsLOIAvailable = output.Checker.IsLOIAvailable

                        };
                    }

                    // replace verify list while empty
                    if (!output.Verify.Any())
                    {
                        output.Verify = context.TransactionColumns
                            .Where(x => x.IsPPUCheckerAfterCallbackFD.Value.Equals(true) && x.IsDeleted.Equals(false))
                            .Select(x => new VerifyModel()
                            {
                                ID = x.TransactionColumnID,
                                Name = x.ColumnName,
                                IsVerified = false
                            })
                            .ToList();
                    }

                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionMakerReviseAfterCheckerDetails(Guid instanceID, ref TransactionFDDetailModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                TransactionFDDetailModel datatransaction = (from a in context.Transactions
                                                            where a.WorkflowInstanceID.Value.Equals(instanceID)
                                                            select new TransactionFDDetailModel
                                                            {
                                                                ID = a.TransactionID,
                                                                WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                                                ApplicationID = a.ApplicationID,
                                                                ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                                                                IsTopUrgent = a.IsTopUrgent == 1 ? true : false,
                                                                IsTopUrgentChain = a.IsTopUrgent == 2 ? true : false,
                                                                Product = new ProductModel()
                                                                {
                                                                    ID = a.Product.ProductID,
                                                                    Code = a.Product.ProductCode,
                                                                    Name = a.Product.ProductName,
                                                                    WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                                                    Workflow = a.Product.ProductWorkflow.WorkflowName,
                                                                    LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                                                    LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                                                },
                                                                Channel = new ChannelModel()
                                                                {
                                                                    ID = a.Channel.ChannelID,
                                                                    Name = a.Channel.ChannelName,
                                                                    LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                                                    LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                                                },
                                                                Currency = new CurrencyModel()
                                                                {
                                                                    ID = a.Currency.CurrencyID,
                                                                    Code = a.Currency.CurrencyCode,
                                                                    Description = a.Currency.CurrencyDescription,
                                                                    CodeDescription = a.Currency.CurrencyCode + "(" + a.Currency.CurrencyDescription + ")",
                                                                    LastModifiedBy = a.Currency.UpdateDate.HasValue ? a.Currency.UpdateBy : a.Currency.CreateBy,
                                                                    LastModifiedDate = a.Currency.UpdateDate.HasValue ? a.Currency.UpdateDate.Value : a.Currency.CreateDate
                                                                },
                                                                TransactionType = a.TransactionType != null ? new TransactionTypeParameterModel()
                                                                {
                                                                    TransactionTypeName = a.TransactionType.TransactionType1,
                                                                    TransTypeID = a.TransactionType.TransTypeID,
                                                                    ProductID = a.TransactionType.ProductID
                                                                } : new TransactionTypeParameterModel()
                                                                {
                                                                    TransactionTypeName = "",
                                                                    TransTypeID = 0,
                                                                    ProductID = 0
                                                                },
                                                                SourceID = a.SourceID.Value,
                                                                Amount = a.Amount,
                                                                FDAccNumber = a.FDAccNumber,
                                                                DebitAccNumber = a.DebitAccNumber,
                                                                CreditAccNumber = a.CreditAccNumber,
                                                                InterestRate = a.InterestRate ?? null,
                                                                Tenor = a.Tenor ?? null,
                                                                ValueDate = a.ValueDate ?? null,
                                                                MaturityDate = a.MaturityDate ?? null,
                                                                BankNameAccNumber = a.FDBankName,
                                                                AttachmentRemarks = a.AttachmentRemarks,
                                                                IsSignatureVerified = a.IsSignatureVerified,
                                                                IsFreezeAccount = a.IsFrezeAccount,
                                                                IsDormantAccount = a.IsDormantAccount,
                                                                IsDocumentComplete = a.IsDocumentComplete,
                                                                IsLOIAvailable = a.IsLOI == null ? false : a.IsLOI == false ? false : true,
                                                                Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                                                                {
                                                                    ID = x.TransactionDocumentID,
                                                                    Type = new DocumentTypeModel()
                                                                    {
                                                                        ID = x.DocumentType.DocTypeID,
                                                                        Name = x.DocumentType.DocTypeName,
                                                                        Description = x.DocumentType.DocTypeDescription,
                                                                        LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                        LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                    },
                                                                    Purpose = new DocumentPurposeModel()
                                                                    {
                                                                        ID = x.DocumentPurpose.PurposeID,
                                                                        Name = x.DocumentPurpose.PurposeName,
                                                                        Description = x.DocumentPurpose.PurposeDescription,
                                                                        LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                        LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                                    },
                                                                    FileName = x.Filename,
                                                                    DocumentPath = x.DocumentPath,
                                                                    LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                    LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value,
                                                                    IsDormant = x.IsDormant == null ? false : x.IsDormant
                                                                }).ToList(),
                                                                CreateDate = a.CreateDate,
                                                                LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                                                DocsComplete = a.DocsComplete,
                                                                //AllInRate = a.AllInRate,
                                                                FTPRate = a.FTPRate
                                                            }).SingleOrDefault();

                // fill payment data
                if (datatransaction != null)
                {
                    int IdRemarks = 0;
                    string IdRemarkString = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(instanceID)).Select(a => a.Remarks).SingleOrDefault();
                    bool IdRemarksBool = string.IsNullOrEmpty(IdRemarkString) ? false : int.TryParse(IdRemarkString.Trim(), out IdRemarks);
                    if (IdRemarksBool)
                    {
                        datatransaction.TransactionRemarks = context.ParameterSystems.Where(a => a.ParsysID.Equals(IdRemarks)).Select(a => a.ParameterValue).SingleOrDefault();
                    }

                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(instanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();

                    if (customerRepo.GetCustomerByTransaction(datatransaction.ID, ref customer, ref message))
                    {
                        datatransaction.Customer = customer;
                    }

                    long IDTrns = datatransaction.ID;
                    var checker = context.TransactionCheckerDatas.Where(x => x.TransactionID.Equals(IDTrns)).Select(x => x).OrderByDescending(b => b.ApproverID).FirstOrDefault();
                    if (checker != null)
                    {
                        datatransaction.IsSignatureVerified = checker.IsSignatureVerified;
                        datatransaction.IsDocumentComplete = datatransaction.IsDocumentComplete;
                        datatransaction.IsCallbackRequired = checker.IsCallbackRequired;
                        datatransaction.IsLOIAvailable = checker.IsLOIAvailable;
                        datatransaction.IsDormantAccount = checker.IsDormantAccount;
                        datatransaction.IsFreezeAccount = checker.IsFreezeAccount;
                    }
                    transaction = datatransaction;
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        #endregion

        #region Aridya
        //20161221 aridya add for get fd maker history
        public bool GetTransactionFDMakerHistory(long transactionId, long approverId, FDMakerUserModel fdMakerUserModel, ref IList<FDMakerHistoryModel> output, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                //insert new data or update access time and access count if data is count
                List<TransactionFDMakerHistory> data = context.TransactionFDMakerHistories.Where(t => t.TransactionID == fdMakerUserModel.TransactionID && t.ApproverID == fdMakerUserModel.ApproverID).ToList(); //&& t.EmployeeUsername == fdMakerUserModel.LoginName
                TransactionFDMakerHistory initial = data.Where(d => d.EmployeeUsername == fdMakerUserModel.LoginName).FirstOrDefault();
                if (initial != null)
                {
                    initial.AccessTime = DateTime.UtcNow;
                    initial.AccessCount += 1;
                }
                else
                {
                    TransactionFDMakerHistory newHistory = new TransactionFDMakerHistory
                    {
                        TransactionID = fdMakerUserModel.TransactionID,
                        ApproverID = fdMakerUserModel.ApproverID,
                        EmployeeUsername = fdMakerUserModel.LoginName,
                        EmployeeName = fdMakerUserModel.DisplayName,
                        TransactionStatus = fdMakerUserModel.TransactionStatus,
                        AccessTime = DateTime.UtcNow,
                        AccessCount = 1
                    };
                    context.TransactionFDMakerHistories.Add(newHistory);
                }
                context.SaveChanges();

                //get data
                List<TransactionFDMakerHistory> accessor = data.Where(d => d.EmployeeUsername != fdMakerUserModel.LoginName).ToList();

                if (accessor.Count > 0)
                {
                    foreach (TransactionFDMakerHistory item in accessor)
                    {
                        output.Add(new FDMakerHistoryModel { DisplayName = item.EmployeeName, LoginName = item.EmployeeUsername });
                    }
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return IsSuccess;
        }
        #endregion

    }
    #endregion
}

//=======================================================================================//
///FOR ALL DEVELOPER...
///PLEASE ADD NEW REGION BY YOUR NAME TO KEEP CLEAN !!! DONT BE A DIRTY PROGRAMMER!!!
///ANDIWIJAYA
//=======================================================================================//