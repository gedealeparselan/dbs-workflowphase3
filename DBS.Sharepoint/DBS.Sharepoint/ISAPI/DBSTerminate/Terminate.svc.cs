﻿using DBS.Sharepoint.Helpers;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Workflow;
using Nintex.Workflow.HumanApproval;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading.Tasks;

namespace DBS.Sharepoint
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class Terminate : ITerminate
    {
        public string TerminateWF(ItemProperties itemProp)
        {
            string hasilTerminate = "";
            try
            {
                var curSite = SPContext.Current.Site;
                SPSecurity.RunWithElevatedPrivileges(delegate
                {
                    using (SPSite spSite = new SPSite(curSite.Url))
                    {
                        using (SPWeb spWeb = spSite.OpenWeb())
                        {

                            spWeb.AllowUnsafeUpdates = true;
                            SPListItem spItem;
                            SPList list = spWeb.Lists[itemProp.namaList];
                            spItem = list.Items.GetItemById(itemProp.ID);
                            Guid guid = Guid.Empty;
                            try
                            {
                                guid = new Guid(itemProp.WFInstanceID);
                            }
                            catch (Exception ex)
                            {
                                hasilTerminate = "Error GUID = " + ex.Message;
                            }
                            SPWorkflow spWorkflow = new SPWorkflow(spItem, guid);
                            SPWorkflowManager.CancelWorkflow(spWorkflow);
                            hasilTerminate = "Status = " + spWorkflow.StatusText + " - " + spWorkflow.StatusValue;
                            spWeb.AllowUnsafeUpdates = false;

                        }
                    }
                });
            }
            catch (Exception ex)
            {
                hasilTerminate = "Error = " + ex.Message;
                //throw new Exception(ex.Message);
            }
            return hasilTerminate;
        }
    }
}
