﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class CustomerController : ApiController
    {
        private string message = string.Empty;

        private readonly ICustomerRepository repository = null;
        
        public CustomerController()
        {
            this.repository = new CustomerRepository();
        }

        public CustomerController(ICustomerRepository repository)
        {
            this.repository = repository;
        }
        
        
        /// <summary>
        /// Get all customers.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<CustomerModel>))]
        public HttpResponseMessage GetAll()
        {
            IList<CustomerModel> output = new List<CustomerModel>();

            if (!repository.GetCustomer(ref output, 10, 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get customers with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<CustomerModel>))]
        public HttpResponseMessage GetAll(int? limit, int? index)
        {
            IList<CustomerModel> output = new List<CustomerModel>();

            if (!repository.GetCustomer(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get customers for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(CustomerGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<CustomerFilter> filters)
        {
            CustomerGrid output = new CustomerGrid();

            //if (!repository.GetCustomer(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, string.IsNullOrEmpty(criteria) ? "1=1" : HttpUtility.UrlDecode(criteria), string.IsNullOrEmpty(sort_column) ? "CIF" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            if (!repository.GetCustomer(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "CIF" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<CustomerGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find customers using criterias CIF or Customer Name.
        /// </summary>
        /// <param name="query">CIF or Customer Name.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<CustomerModel>))]
        [HttpGet]
        [Route("api/Customer/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<CustomerModel> output = new List<CustomerModel>();

            if (!repository.GetCustomer(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get customer by CIF number.
        /// </summary>
        /// <param name="cif">CIF number.</param>
        /// <returns></returns>
        [ResponseType(typeof(CustomerModel))]
        [Route("api/Customer/{cif}")]
        [HttpGet]
        public HttpResponseMessage GetByCIF(string cif)
        {
            CustomerModel output = new CustomerModel();

            if (!repository.GetCustomerByCIF(cif, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<CustomerModel>(HttpStatusCode.OK, output);
            }
        }
        /// <summary>
        /// Get customer and Underlyings by CIF number.
        /// </summary>
        /// <param name="cif">CIF number.</param>
        /// <returns></returns>
        [ResponseType(typeof(CustomerModel))]
        [Route("api/Customer/Underlyings/{transactionDealID}")]
        [HttpGet]
        public HttpResponseMessage GetCustomerUnderlyingByCIF(long transactionDealID)
        {
            CustomerModel output = new CustomerModel();

            if (!repository.GetCustomerUnderlyingsByCIF(transactionDealID, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<CustomerModel>(HttpStatusCode.OK, output);
            }
        }
        /// <summary>
        /// Create a new customer.
        /// </summary>
        /// <param name="customer">Customer data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS CBO Maker")]
        //[Route("api/Customer/{cif}")]
        public HttpResponseMessage Post(CustomerModel customer)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddCustomer(customer, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.Created, "New customer data has been created.");
                }
            }
        }

        /// <summary>
        /// Modify existing customer.
        /// </summary>
        /// <param name="cif">CIF number to be modify.</param>
        /// <param name="customer">Customer data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/Customer/{cif}")]
       // [Authorize(Roles = "DBS CBO Maker")]
        public HttpResponseMessage Put(string cif, CustomerModel customer)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateCustomer(cif, customer, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Customer data has been updated.");
                }
            }
        }

        /// <summary>
        /// Remove exisiting customer.
        /// </summary>
        /// <param name="cif">CIF number to be modify.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/Customer/{cif}")]
        [ResponseType(typeof(string))]
       // [Authorize(Roles = "DBS CBO Maker")]
        public HttpResponseMessage Delete(string cif)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteCustomer(cif, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "Customer data has been deleted.");
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}