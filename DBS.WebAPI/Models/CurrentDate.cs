﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace DBS.WebAPI.Models
{
    [DataContract]
    [ModelName("CurrentDate")]
    public class CurrentDateModel
    {
        
        public DateTime clientDateTimeNow { get; set; }

        [DataMember(Name="clientDateTimeNow")]
        public string strClientDateTimeNow { get; set; }

        [OnSerializing]
        void OnSerializing(StreamingContext context)
        {
            this.strClientDateTimeNow = this.clientDateTimeNow.ToString();
        }

    }
    public interface ICurrentDate : IDisposable
    {
        bool GetCurrentDate(DateTime clientDateTime, ref IList<CurrentDateModel> resultOutput, ref string message);

    }
    public class CurrentDateRepository : ICurrentDate
    {
        public bool GetCurrentDate(DateTime clientDateTime, ref IList<CurrentDateModel> resultOutput, ref string message)
        {
            bool isSuccess = false;
            
            try
            {   
                
                resultOutput = new List<CurrentDateModel> { new CurrentDateModel{ clientDateTimeNow = clientDateTime }}.ToList();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    }

}