﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class TBODocsSLAController : ApiController
    {
         private string message = string.Empty;
         private readonly ITBODocsSLARepository repository = null;
        public TBODocsSLAController()
        {
            this.repository = new TBODocsSLARepository();
        }
        public TBODocsSLAController(ITBODocsSLARepository repository)
        {
            this.repository = repository;
        }
        [ResponseType(typeof(IList<TBODocsSLAModel>))]
        public HttpResponseMessage Get()
        {
            IList<TBODocsSLAModel> output = new List<TBODocsSLAModel>();

            if (!repository.GetTBODocsSLA(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TBODocsSLAModel>>(HttpStatusCode.OK, output);
            }
        }
        [ResponseType(typeof(IList<TBODocsSLAModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<TBODocsSLAModel> output = new List<TBODocsSLAModel>();
            if (!repository.GetTBODocsSLA(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<TBODocsSLAModel>>(HttpStatusCode.OK, output);
            }
        }
        [ResponseType(typeof(TBODocsSLAGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/TBODocsSLA")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<TBODocsSLAFilter> filters)
        {
            TBODocsSLAGrid output = new TBODocsSLAGrid();

            if (!repository.GetTBODocsSLA(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<TBODocsSLAGrid>(HttpStatusCode.OK, output);
            }
        }
        [ResponseType(typeof(IList<TBODocsSLAModel>))]
        [HttpGet]
        [Route("api/TBODocsSLA/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<TBODocsSLAModel> output = new List<TBODocsSLAModel>();
            if (!repository.GetTBODocsSLA(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TBODocsSLAModel>>(HttpStatusCode.OK, output);
            }
        }
        [ResponseType(typeof(TBODocsSLAModel))]
        [Route("api/TBODocsSLA/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            TBODocsSLAModel output = new TBODocsSLAModel();
            if (!repository.GetTBODocsSLAByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<TBODocsSLAModel>(HttpStatusCode.OK, output);
            }
        }
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(TBODocsSLAModel tbodocssla)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTBODocsSLA(tbodocssla, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New tbodocssla data has been created." });
                }
            }
        }
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/TBODocsSLA/{id}")]
        public HttpResponseMessage Put(int id, TBODocsSLAModel tbodocssla)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTBODocsSLA(id, tbodocssla, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "TBODocsSLA data has been updated." });
                }
            }
        }
        [HttpDelete]
        [Route("api/TBODocsSLA/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteTBODocsSLA(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "TBODocsSLA data has been deleted." });
                }
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}