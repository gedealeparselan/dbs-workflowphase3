﻿using DBS.Entity;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Dynamic;
using System.Transactions;
using System.Data.Entity.SqlServer;

namespace DBS.WebAPI.Models
{
    #region Property
    public class EmployeeModel
    {
        public long EmployeeID { get; set; }
        public Guid? WorkflowInstanceID { get; set; }
        public string EmployeeUsername { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeEmail { get; set; }
        public RankModel Rank { get; set; }
        public int RankID { get; set; }
        public String RankCode { get; set; }
        public string RankDescription { get; set; }

        public BizSegmentModel Segment { get; set; }
        public int SegmentID { get; set; }
        public String SegmentName { get; set; }
        public string SegmentDescription { get; set; }
        public BranchModel Branch { get; set; }
        public IList<RoleModel> Roles { get; set; }
        public FunctionRoleModel FunctionRole { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public string UserCategoryCode { get; set; }

    }

    public class EmployeeModelRow : EmployeeModel
    {
        public int RowID { get; set; }
    }

    public class EmployeeModelGrid : Grid
    {
        public IList<EmployeeModelRow> Rows { get; set; }
    }
    #endregion

    #region Filter
    public class EmployeeModelFilter : Filter { }
    #endregion

    #region Interface
    public interface IEmployeeModelRepository : IDisposable
    {
        bool GetUserApprovalByID(string id, ref EmployeeModel Employee, ref string message);
        bool GetUserApprovalDraftByID(Guid id, ref EmployeeModel Employee, ref string message);
        bool GetUserApproval(ref IList<EmployeeModel> Employees, ref string message);
        bool GetUserApprovalDraft(ref IList<EmployeeModel> Employees, ref string message);
        bool GetUserApproval(ref IList<EmployeeModel> Employees, int limit, int index, ref string message);
        bool GetUserApproval(int page, int size, IList<EmployeeModelFilter> filters, string sortColumn, string sortOrder, ref EmployeeModelGrid Employee, ref string message);
        bool GetUserApprovalDraft(int page, int size, IList<EmployeeModelFilter> filters, string sortColumn, string sortOrder, ref EmployeeModelGrid Employee, ref string message);
        bool GetUserApproval(EmployeeModelFilter filter, ref IList<EmployeeModel> Employee, ref string message);
        bool GetUserApproval(string key, int limit, ref IList<EmployeeModel> Employee, ref string message);
        bool AddUserApproval(EmployeeModel Employee, ref long EmployeeID, ref string message);
        bool UpdateUserApproval(EmployeeModel Employee, ref long EmployeeID, ref string message);
        bool GetEmployeeLocation(string username, ref BranchModel branch, ref string message);

        bool DeleteEmployeeDraft(int id, ref string message);
    }
    #endregion

    #region Repository
    public class EmployeeModelRepository : IEmployeeModelRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetUserApprovalByID(string id, ref EmployeeModel Employee, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Employee = (from a in context.Employees
                            join x in context.Ranks on new { Rank = a.RankID } equals new { Rank = (int?)x.RankID }
                            into ax
                            from grax in ax.DefaultIfEmpty()
                            join y in context.BizSegments on new { Segment = a.BizSegmentID } equals new { Segment = (int?)y.BizSegmentID }
                            into axy
                            from graxy in axy.DefaultIfEmpty()
                            where a.IsDeleted.Equals(false) && a.EmployeeID.Equals(id)
                            select new EmployeeModel
                            {
                                EmployeeID = a.EmployeeID,
                                EmployeeUsername = a.EmployeeUsername,
                                EmployeeName = a.EmployeeName,
                                EmployeeEmail = a.EmployeeEmail,
                                //Rank = new RankModel { ID = a.Rank.RankID, Code = a.Rank.RankCode, Description = a.Rank.RankDescription },
                                //Segment = new BizSegmentModel { ID = a.BizSegment.BizSegmentID, Name = a.BizSegment.BizSegmentName, Description = a.BizSegment.BizSegmentDescription },
                                Branch = new BranchModel { ID = a.Location.LocationID, Name = a.Location.LocationName },

                                RankID = (grax.RankID != null) ? grax.RankID : 0,
                                RankCode = (grax.RankID != null) ? grax.RankCode : "-",
                                RankDescription = (grax.RankID != null) ? grax.RankDescription : "-",

                                SegmentID = (graxy.BizSegmentID != null) ? graxy.BizSegmentID : 0,
                                SegmentName = (graxy.BizSegmentID != null) ? graxy.BizSegmentName : "-",
                                SegmentDescription = (graxy.BizSegmentID != null) ? graxy.BizSegmentDescription : "-",
                                UserCategoryCode = a.UserCategoryCode,

                                FunctionRole = new FunctionRoleModel()
                                {
                                    ID = (a.FunctionRole != null) ? a.FunctionRole.FunctionRoleID : 0,
                                    Name = (a.FunctionRole != null) ? a.FunctionRole.FunctionRoleName : "-"
                                },
                                Roles = (from b in context.EmployeeRoleMappings
                                         where b.EmployeeID.Equals(a.EmployeeID)
                                         select new RoleModel
                                         {
                                             ID = b.Role.RoleID,
                                             Name = b.Role.RoleName,
                                             Description = b.Role.RoleDescription
                                         }).ToList(),

                                LastModifiedDate = a.CreateDate,
                                LastModifiedBy = a.CreateBy
                            }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetUserApprovalDraftByID(Guid id, ref EmployeeModel Employee, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Employee = (from a in context.EmployeeDrafts
                            join x in context.Ranks on new { Rank = a.RankID } equals new { Rank = (int?)x.RankID }
                            into ax
                            from grax in ax.DefaultIfEmpty()
                            join y in context.BizSegments on new { Segment = a.BizSegmentID } equals new { Segment = (int?)y.BizSegmentID }
                            into axy
                            from graxy in axy.DefaultIfEmpty()
                            where a.IsDeleted.Equals(false) && a.WorkflowInstanceID.Value.Equals(id)
                            select new EmployeeModel
                            {
                                EmployeeID = a.EmployeeDraftID,
                                WorkflowInstanceID = a.WorkflowInstanceID,
                                EmployeeUsername = a.EmployeeUsername,
                                EmployeeName = a.EmployeeName,
                                EmployeeEmail = a.EmployeeEmail,
                                //Rank = new RankModel { ID = a.Rank.RankID, Code = a.Rank.RankCode, Description = a.Rank.RankDescription },
                                //Segment = new BizSegmentModel { ID = a.BizSegment.BizSegmentID, Name = a.BizSegment.BizSegmentName, Description = a.BizSegment.BizSegmentDescription },
                                RankID = (grax.RankID != null) ? grax.RankID : 0,
                                RankCode = (grax.RankID != null) ? grax.RankCode : "-",
                                RankDescription = (grax.RankID != null) ? grax.RankDescription : "-",
                                UserCategoryCode = a.UserCategoryCode,
                                SegmentID = (graxy.BizSegmentID != null) ? graxy.BizSegmentID : 0,
                                SegmentName = (graxy.BizSegmentID != null) ? graxy.BizSegmentName : "-",
                                SegmentDescription = (graxy.BizSegmentID != null) ? graxy.BizSegmentDescription : "-",


                                Branch = new BranchModel { ID = a.Location.LocationID, Name = a.Location.LocationName },
                                FunctionRole = new FunctionRoleModel()
                                {
                                    ID = (a.FunctionRole != null) ? a.FunctionRole.FunctionRoleID : 0,
                                    Name = (a.FunctionRole != null) ? a.FunctionRole.FunctionRoleName : "-"
                                },
                                Roles = (from b in context.EmployeeRoleMappingDrafts
                                         where b.EmployeeDraftID.Equals(a.EmployeeDraftID)
                                         select new RoleModel
                                         {
                                             ID = b.Role.RoleID,
                                             Name = b.Role.RoleName,
                                             Description = b.Role.RoleDescription
                                         }).ToList(),

                                LastModifiedDate = a.CreateDate,
                                LastModifiedBy = a.CreateBy
                            }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetUserApproval(ref IList<EmployeeModel> Employees, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    Employees = (from a in context.Employees
                                 join x in context.Ranks on new { Rank = a.RankID } equals new { Rank = (int?)x.RankID }
                                 into ax
                                 from grax in ax.DefaultIfEmpty()
                                 join y in context.BizSegments on new { Segment = a.BizSegmentID } equals new { Segment = (int?)y.BizSegmentID }
                                 into axy
                                 from graxy in axy.DefaultIfEmpty()
                                 where a.IsDeleted.Equals(false)
                                 orderby new { a.EmployeeID, a.EmployeeName }
                                 select new EmployeeModel
                                 {
                                     EmployeeID = a.EmployeeID,
                                     EmployeeUsername = a.EmployeeUsername,
                                     EmployeeName = a.EmployeeName,
                                     EmployeeEmail = a.EmployeeEmail,
                                     //Rank = new RankModel { ID = a.Rank.RankID, Code = a.Rank.RankCode, Description = a.Rank.RankDescription },
                                     //Segment = new BizSegmentModel { ID = a.BizSegment.BizSegmentID, Name = a.BizSegment.BizSegmentName, Description = a.BizSegment.BizSegmentDescription },
                                     RankID = (grax.RankID != null) ? grax.RankID : 0,
                                     RankCode = (grax.RankID != null) ? grax.RankCode : "-",
                                     RankDescription = (grax.RankID != null) ? grax.RankDescription : "-",

                                     SegmentID = (graxy.BizSegmentID != null) ? graxy.BizSegmentID : 0,
                                     SegmentName = (graxy.BizSegmentID != null) ? graxy.BizSegmentName : "-",
                                     SegmentDescription = (graxy.BizSegmentID != null) ? graxy.BizSegmentDescription : "-",
                                     UserCategoryCode = a.UserCategoryCode,
                                     Branch = new BranchModel { ID = a.Location.LocationID, Name = a.Location.LocationName },
                                     FunctionRole = new FunctionRoleModel()
                                     {
                                         ID = (a.FunctionRole != null) ? a.FunctionRole.FunctionRoleID : 0,
                                         Name = (a.FunctionRole != null) ? a.FunctionRole.FunctionRoleName : "-"
                                     },
                                     Roles = (from b in context.EmployeeRoleMappings
                                              where b.EmployeeID.Equals(a.EmployeeID)
                                              select new RoleModel
                                              {
                                                  ID = b.Role.RoleID,
                                                  Name = b.Role.RoleName,
                                                  Description = b.Role.RoleDescription
                                              }).ToList(),

                                     LastModifiedDate = a.CreateDate,
                                     LastModifiedBy = a.CreateBy
                                 }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetUserApprovalDraft(ref IList<EmployeeModel> Employees, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    Employees = (from a in context.EmployeeDrafts
                                 join x in context.Ranks on new { Rank = a.RankID } equals new { Rank = (int?)x.RankID }
                                 into ax
                                 from grax in ax.DefaultIfEmpty()
                                 join y in context.BizSegments on new { Segment = a.BizSegmentID } equals new { Segment = (int?)y.BizSegmentID }
                                 into axy
                                 from graxy in axy.DefaultIfEmpty()
                                 where a.IsDeleted.Equals(false)
                                 orderby new { a.EmployeeDraftID, a.EmployeeName }
                                 select new EmployeeModel
                                 {
                                     EmployeeID = a.EmployeeDraftID,
                                     EmployeeUsername = a.EmployeeUsername,
                                     EmployeeName = a.EmployeeName,
                                     EmployeeEmail = a.EmployeeEmail,
                                     RankID = (grax.RankID != null) ? grax.RankID : 0,
                                     RankCode = (grax.RankID != null) ? grax.RankCode : "-",
                                     RankDescription = (grax.RankID != null) ? grax.RankDescription : "-",

                                     SegmentID = (graxy.BizSegmentID != null) ? graxy.BizSegmentID : 0,
                                     SegmentName = (graxy.BizSegmentID != null) ? graxy.BizSegmentName : "-",
                                     SegmentDescription = (graxy.BizSegmentID != null) ? graxy.BizSegmentDescription : "-",
                                     //Rank = new RankModel { ID = a.Rank.RankID, Code = a.Rank.RankCode, Description = a.Rank.RankDescription },
                                     //Segment = new BizSegmentModel { ID = a.BizSegment.BizSegmentID, Name = a.BizSegment.BizSegmentName, Description = a.BizSegment.BizSegmentDescription },
                                     Branch = new BranchModel { ID = a.Location.LocationID, Name = a.Location.LocationName },
                                     UserCategoryCode = a.UserCategoryCode,
                                     FunctionRole = new FunctionRoleModel()
                                     {
                                         ID = (a.FunctionRole != null) ? a.FunctionRole.FunctionRoleID : 0,
                                         Name = (a.FunctionRole != null) ? a.FunctionRole.FunctionRoleName : "-"
                                     },
                                     Roles = (from b in context.EmployeeRoleMappings
                                              where b.EmployeeID.Equals(a.EmployeeDraftID)
                                              select new RoleModel
                                              {
                                                  ID = b.Role.RoleID,
                                                  Name = b.Role.RoleName,
                                                  Description = b.Role.RoleDescription
                                              }).ToList(),

                                     LastModifiedDate = a.CreateDate,
                                     LastModifiedBy = a.CreateBy
                                 }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetUserApproval(ref IList<EmployeeModel> Employees, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    Employees = (from a in context.Employees
                                 join x in context.Ranks on new { Rank = a.RankID } equals new { Rank = (int?)x.RankID }
                                 into ax
                                 from grax in ax.DefaultIfEmpty()
                                 join y in context.BizSegments on new { Segment = a.BizSegmentID } equals new { Segment = (int?)y.BizSegmentID }
                                 into axy
                                 from graxy in axy.DefaultIfEmpty()
                                 orderby new { a.EmployeeID, a.EmployeeName }
                                 select new EmployeeModel
                                 {
                                     EmployeeID = a.EmployeeID,
                                     EmployeeUsername = a.EmployeeUsername,
                                     EmployeeName = a.EmployeeName,
                                     EmployeeEmail = a.EmployeeEmail,
                                     RankID = (grax.RankID != null) ? grax.RankID : 0,
                                     RankCode = (grax.RankID != null) ? grax.RankCode : "-",
                                     RankDescription = (grax.RankID != null) ? grax.RankDescription : "-",

                                     SegmentID = (graxy.BizSegmentID != null) ? graxy.BizSegmentID : 0,
                                     SegmentName = (graxy.BizSegmentID != null) ? graxy.BizSegmentName : "-",
                                     SegmentDescription = (graxy.BizSegmentID != null) ? graxy.BizSegmentDescription : "-",
                                     UserCategoryCode = a.UserCategoryCode,
                                     //Rank = new RankModel { ID = a.Rank.RankID, Code = a.Rank.RankCode, Description = a.Rank.RankDescription },
                                     //Segment = new BizSegmentModel { ID = a.BizSegment.BizSegmentID, Name = a.BizSegment.BizSegmentName, Description = a.BizSegment.BizSegmentDescription },
                                     Branch = new BranchModel { ID = a.Location.LocationID, Name = a.Location.LocationName },
                                     FunctionRole = new FunctionRoleModel()
                                     {
                                         ID = (a.FunctionRole != null) ? a.FunctionRole.FunctionRoleID : 0,
                                         Name = (a.FunctionRole != null) ? a.FunctionRole.FunctionRoleName : "-"
                                     },
                                     Roles = (from b in context.EmployeeRoleMappings
                                              where b.EmployeeID.Equals(a.EmployeeID)
                                              select new RoleModel
                                              {
                                                  ID = b.Role.RoleID,
                                                  Name = b.Role.RoleName,
                                                  Description = b.Role.RoleDescription
                                              }).ToList(),

                                     LastModifiedDate = a.CreateDate,
                                     LastModifiedBy = a.CreateBy
                                 }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetUserApproval(int page, int size, IList<EmployeeModelFilter> filters, string sortColumn, string sortOrder, ref EmployeeModelGrid Employee, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();
                EmployeeModel filter = new EmployeeModel();

                filter.Branch = new BranchModel { Name = string.Empty };
                filter.FunctionRole = new FunctionRoleModel { Name = string.Empty };


                //filter.Role = new RoleModel { Name = string.Empty };

                if (filters != null)
                {
                    filter.EmployeeUsername = filters.Where(a => a.Field.Equals("EmployeeUsername")).Select(a => a.Value).SingleOrDefault();
                    filter.EmployeeName = filters.Where(a => a.Field.Equals("EmployeeName")).Select(a => a.Value).SingleOrDefault();
                    filter.EmployeeEmail = filters.Where(a => a.Field.Equals("EmployeeEmail")).Select(a => a.Value).SingleOrDefault();
                    filter.RankCode = (string)filters.Where(a => a.Field.Equals("RankCode")).Select(a => a.Value).SingleOrDefault();
                    filter.SegmentName = (string)filters.Where(a => a.Field.Equals("SegmentName")).Select(a => a.Value).SingleOrDefault();
                    filter.Branch = new BranchModel { Name = (string)filters.Where(a => a.Field.Equals("Branch")).Select(a => a.Value).SingleOrDefault() };
                    filter.FunctionRole = new FunctionRoleModel { Name = (string)filters.Where(a => a.Field.Equals("FunctionRole")).Select(a => a.Value).SingleOrDefault() };
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    filter.UserCategoryCode = filters.Where(a => a.Field.Equals("UserCategoryCode")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    List<string> draft = context.EmployeeDrafts.Where(x => x.IsDeleted.Equals(false)).Select(x => x.EmployeeUsername).ToList<string>();

                    var data = (from a in context.Employees
                                join x in context.Ranks on new { Rank = a.RankID } equals new { Rank = (int?)x.RankID }
                                into ax
                                from grax in ax.DefaultIfEmpty()
                                join y in context.BizSegments on new { Segment = a.BizSegmentID } equals new { Segment = (int?)y.BizSegmentID }
                                into axy
                                from graxy in axy.DefaultIfEmpty()
                                let isUsername = !string.IsNullOrEmpty(filter.EmployeeUsername)
                                let isName = !string.IsNullOrEmpty(filter.EmployeeName)
                                let isEmail = !string.IsNullOrEmpty(filter.EmployeeEmail)
                                let isRank = !string.IsNullOrEmpty(filter.RankCode)
                                let isSegment = !string.IsNullOrEmpty(filter.SegmentName)
                                let isBranch = !string.IsNullOrEmpty(filter.Branch.Name)
                                let isFunctionRole = !string.IsNullOrEmpty(filter.FunctionRole.Name)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isUserCategoryCode = !string.IsNullOrEmpty(filter.UserCategoryCode)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false

                                where a.IsDeleted.Equals(false) &&
                                    //(from x in context.EmployeeDrafts select x.EmployeeID).Contains(a.EmployeeID) &&
                                    //!(from x in draft select x).Any(z => z.Contains(a.EmployeeUsername)) &&
                                    //(from x in draft select x).Contains(a.EmployeeID) &&
                                    (isUsername ? a.EmployeeUsername.Contains(filter.EmployeeUsername) : true) &&
                                    (isName ? a.EmployeeName.Contains(filter.EmployeeName) : true) &&
                                    (isEmail ? a.EmployeeEmail.Contains(filter.EmployeeEmail) : true) &&
                                    (isRank ? grax.RankCode.Contains(filter.RankCode) : true) &&
                                    (isSegment ? graxy.BizSegmentName.Contains(filter.SegmentName) : true) &&
                                    (isBranch ? a.Location.LocationName.Contains(filter.Branch.Name) : true) &&
                                    (isFunctionRole ? a.FunctionRole.FunctionRoleName.Contains(filter.FunctionRole.Name) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isUserCategoryCode ? a.UserCategoryCode.Contains(filter.UserCategoryCode) : true) &&
                                    (isCreateDate ? (((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) >= filter.LastModifiedDate.Value) && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true) //
                                select new EmployeeModel
                                {
                                    EmployeeID = a.EmployeeID,
                                    EmployeeUsername = a.EmployeeUsername,
                                    EmployeeName = a.EmployeeName,
                                    EmployeeEmail = a.EmployeeEmail,
                                    RankID = (grax.RankID != null) ? grax.RankID : 0,
                                    RankCode = (grax.RankID != null) ? grax.RankCode : "-",
                                    RankDescription = (grax.RankID != null) ? grax.RankDescription : "-",

                                    SegmentID = (graxy.BizSegmentID != null) ? graxy.BizSegmentID : 0,
                                    SegmentName = (graxy.BizSegmentID != null) ? graxy.BizSegmentName : "-",
                                    SegmentDescription = (graxy.BizSegmentID != null) ? graxy.BizSegmentDescription : "-",
                                    UserCategoryCode = a.UserCategoryCode,
                                    Branch = new BranchModel() { ID = a.Location.LocationID, Name = a.Location.LocationName },
                                    FunctionRole = new FunctionRoleModel()
                                    {
                                        ID = (a.FunctionRole != null) ? a.FunctionRole.FunctionRoleID : 0,
                                        Name = (a.FunctionRole != null) ? a.FunctionRole.FunctionRoleName : "-"
                                    },
                                    Roles = (from b in context.EmployeeRoleMappings
                                             where b.EmployeeID.Equals(a.EmployeeID)
                                             select new RoleModel
                                             {
                                                 ID = b.Role.RoleID,
                                                 Name = b.Role.RoleName,
                                                 Description = b.Role.RoleDescription
                                             }).ToList(),
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    Employee.Page = page;
                    Employee.Size = size;
                    Employee.Total = data.Count();
                    Employee.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    Employee.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new EmployeeModelRow
                        {
                            RowID = i + 1,
                            EmployeeUsername = a.EmployeeUsername,
                            EmployeeName = a.EmployeeName,
                            EmployeeEmail = a.EmployeeEmail,
                            Rank = a.Rank,
                            RankID = a.RankID,
                            RankCode = a.RankCode,
                            RankDescription = a.RankDescription,
                            Segment = a.Segment,
                            SegmentID = a.SegmentID,
                            SegmentName = a.SegmentName,
                            SegmentDescription = a.SegmentDescription,
                            Branch = a.Branch,
                            UserCategoryCode = a.UserCategoryCode,
                            Roles = a.Roles,
                            FunctionRole = a.FunctionRole,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetUserApprovalDraft(int page, int size, IList<EmployeeModelFilter> filters, string sortColumn, string sortOrder, ref EmployeeModelGrid Employee, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();
                EmployeeModel filter = new EmployeeModel();

                filter.Branch = new BranchModel { Name = string.Empty };
                filter.FunctionRole = new FunctionRoleModel { Name = string.Empty };
                //filter.Role = new RoleModel { Name = string.Empty };

                if (filters != null)
                {
                    filter.EmployeeUsername = filters.Where(a => a.Field.Equals("EmployeeUsername")).Select(a => a.Value).SingleOrDefault();
                    filter.EmployeeName = filters.Where(a => a.Field.Equals("EmployeeName")).Select(a => a.Value).SingleOrDefault();
                    filter.EmployeeEmail = filters.Where(a => a.Field.Equals("EmployeeEmail")).Select(a => a.Value).SingleOrDefault();
                    filter.RankCode = (string)filters.Where(a => a.Field.Equals("RankCode")).Select(a => a.Value).SingleOrDefault();
                    filter.SegmentName = (string)filters.Where(a => a.Field.Equals("SegmentName")).Select(a => a.Value).SingleOrDefault();
                    filter.Branch = new BranchModel { Name = (string)filters.Where(a => a.Field.Equals("Branch")).Select(a => a.Value).SingleOrDefault() };
                    filter.FunctionRole = new FunctionRoleModel { Name = (string)filters.Where(a => a.Field.Equals("FunctionRole")).Select(a => a.Value).SingleOrDefault() };
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    filter.UserCategoryCode = filters.Where(a => a.Field.Equals("UserCategoryCode")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    List<string> draft = context.EmployeeDrafts.Where(x => x.IsDeleted.Equals(false)).Select(x => x.EmployeeUsername).ToList<string>();

                    var data = (from a in context.EmployeeDrafts
                                join x in context.Ranks on new { Rank = a.RankID } equals new { Rank = (int?)x.RankID }
                                into ax
                                from grax in ax.DefaultIfEmpty()
                                join y in context.BizSegments on new { Segment = a.BizSegmentID } equals new { Segment = (int?)y.BizSegmentID }
                                into axy
                                from graxy in axy.DefaultIfEmpty()
                                let isUsername = !string.IsNullOrEmpty(filter.EmployeeUsername)
                                let isName = !string.IsNullOrEmpty(filter.EmployeeName)
                                let isEmail = !string.IsNullOrEmpty(filter.EmployeeEmail)
                                let isRank = !string.IsNullOrEmpty(filter.RankCode)
                                let isSegment = !string.IsNullOrEmpty(filter.SegmentName)
                                let isBranch = !string.IsNullOrEmpty(filter.Branch.Name)
                                let isFunctionRole = !string.IsNullOrEmpty(filter.FunctionRole.Name)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isUserCategoryCode = !string.IsNullOrEmpty(filter.UserCategoryCode)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false

                                where a.IsDeleted.Equals(false) &&
                                    //(from x in context.EmployeeDrafts select x.EmployeeID).Contains(a.EmployeeID) &&
                                    //(from x in draft select x).Any(z => z.Contains(a.EmployeeUsername)) &&
                                    //(from x in draft select x).Contains(a.EmployeeID) &&                                    
                                    (isUsername ? a.EmployeeUsername.Contains(filter.EmployeeUsername) : true) &&
                                    (isName ? a.EmployeeName.Contains(filter.EmployeeName) : true) &&
                                    (isEmail ? a.EmployeeEmail.Contains(filter.EmployeeEmail) : true) &&
                                    (isRank ? grax.RankCode.Contains(filter.RankCode) : true) &&
                                    (isSegment ? graxy.BizSegmentName.Contains(filter.SegmentName) : true) &&
                                    (isBranch ? a.Location.LocationName.Contains(filter.Branch.Name) : true) &&
                                    (isFunctionRole ? a.FunctionRole.FunctionRoleName.Contains(filter.FunctionRole.Name) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isUserCategoryCode ? a.UserCategoryCode.Contains(filter.UserCategoryCode) : true) &&
                                    (isCreateDate ? (((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) >= filter.LastModifiedDate.Value) && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new EmployeeModel
                                {
                                    EmployeeID = a.EmployeeDraftID,
                                    EmployeeUsername = a.EmployeeUsername,
                                    EmployeeName = a.EmployeeName,
                                    EmployeeEmail = a.EmployeeEmail,
                                    RankID = (grax.RankID != null) ? grax.RankID : 0,
                                    RankCode = (grax.RankID != null) ? grax.RankCode : "-",
                                    RankDescription = (grax.RankID != null) ? grax.RankDescription : "-",

                                    SegmentID = (graxy.BizSegmentID != null) ? graxy.BizSegmentID : 0,
                                    SegmentName = (graxy.BizSegmentID != null) ? graxy.BizSegmentName : "-",
                                    SegmentDescription = (graxy.BizSegmentID != null) ? graxy.BizSegmentDescription : "-",
                                    Branch = new BranchModel() { ID = a.Location.LocationID, Name = a.Location.LocationName },
                                    UserCategoryCode = a.UserCategoryCode,
                                    FunctionRole = new FunctionRoleModel()
                                    {
                                        ID = (a.FunctionRole != null) ? a.FunctionRole.FunctionRoleID : 0,
                                        Name = (a.FunctionRole != null) ? a.FunctionRole.FunctionRoleName : "-"
                                    },

                                    Roles = (from b in context.EmployeeRoleMappingDrafts
                                             where b.EmployeeDraftID.Equals(a.EmployeeDraftID)
                                             select new RoleModel
                                             {
                                                 ID = b.Role.RoleID,
                                                 Name = b.Role.RoleName,
                                                 Description = b.Role.RoleDescription
                                             }).ToList(),
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    Employee.Page = page;
                    Employee.Size = size;
                    Employee.Total = data.Count();
                    Employee.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    Employee.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new EmployeeModelRow
                        {
                            RowID = i + 1,
                            EmployeeUsername = a.EmployeeUsername,
                            EmployeeName = a.EmployeeName,
                            EmployeeEmail = a.EmployeeEmail,
                            Rank = a.Rank,
                            RankID = a.RankID,
                            RankCode = a.RankCode,
                            RankDescription = a.RankDescription,
                            Segment = a.Segment,
                            SegmentID = a.SegmentID,
                            SegmentName = a.SegmentName,
                            SegmentDescription = a.SegmentDescription,
                            Branch = a.Branch,
                            Roles = a.Roles,
                            FunctionRole = a.FunctionRole,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            UserCategoryCode = a.UserCategoryCode
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetUserApproval(EmployeeModelFilter filter, ref IList<EmployeeModel> Employees, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetUserApproval(string key, int limit, ref IList<EmployeeModel> Employees, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Employees = (from a in context.Employees
                             join x in context.Ranks on new { Rank = a.RankID } equals new { Rank = (int?)x.RankID }
                                 into ax
                             from grax in ax.DefaultIfEmpty()
                             join y in context.BizSegments on new { Segment = a.BizSegmentID } equals new { Segment = (int?)y.BizSegmentID }
                             into axy
                             from graxy in axy.DefaultIfEmpty()
                             where a.IsDeleted.Equals(false) &&
                                  (a.EmployeeUsername.Contains(key) || a.EmployeeName.Contains(key) || a.EmployeeEmail.Contains(key))
                             select new EmployeeModel
                             {
                                 EmployeeID = a.EmployeeID,
                                 EmployeeUsername = a.EmployeeUsername,
                                 EmployeeName = a.EmployeeName,
                                 EmployeeEmail = a.EmployeeEmail,
                                 RankID = (grax.RankID != null) ? grax.RankID : 0,
                                 RankCode = (grax.RankID != null) ? grax.RankCode : "-",
                                 RankDescription = (grax.RankID != null) ? grax.RankDescription : "-",

                                 SegmentID = (graxy.BizSegmentID != null) ? graxy.BizSegmentID : 0,
                                 SegmentName = (graxy.BizSegmentID != null) ? graxy.BizSegmentName : "-",
                                 SegmentDescription = (graxy.BizSegmentID != null) ? graxy.BizSegmentDescription : "-",
                                 UserCategoryCode = a.UserCategoryCode,
                                 Branch = new BranchModel { ID = a.Location.LocationID, Name = a.Location.LocationName },
                                 FunctionRole = new FunctionRoleModel()
                                 {
                                     ID = (a.FunctionRole != null) ? a.FunctionRole.FunctionRoleID : 0,
                                     Name = (a.FunctionRole != null) ? a.FunctionRole.FunctionRoleName : "-"
                                 },
                                 Roles = (from b in context.EmployeeRoleMappings
                                          where b.EmployeeID.Equals(a.EmployeeID)
                                          select new RoleModel
                                          {
                                              ID = b.Role.RoleID,
                                              Name = b.Role.RoleName,
                                              Description = b.Role.RoleDescription
                                          }).ToList(),

                                 LastModifiedDate = a.CreateDate,
                                 LastModifiedBy = a.CreateBy
                             }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddUserApproval(EmployeeModel Employee, ref long EmployeeID, ref string message)
        {
            bool isSuccess = false;

            try
            {
                var EmployeeUsername = "i:0#.f|dbsmembership|" + Employee.EmployeeUsername;


                if (!context.Employees.Where(a => a.EmployeeUsername.Equals(EmployeeUsername) && a.IsDeleted.Equals(false)).Any() &&
                    !context.EmployeeDrafts.Where(a => a.EmployeeUsername.Equals(EmployeeUsername) && a.IsDeleted.Equals(false)).Any())
                {
                    using (TransactionScope ts = new TransactionScope())
                    {
                        //insert to draft
                        EmployeeDraft employee = new EmployeeDraft();
                        employee.EmployeeUsername = EmployeeUsername;
                        employee.EmployeeName = Employee.EmployeeName;
                        employee.EmployeeEmail = Employee.EmployeeEmail;
                        employee.RankID = Employee.Rank.ID;
                        employee.BizSegmentID = Employee.Segment.ID;


                        if (!string.IsNullOrEmpty(Employee.FunctionRole.Name))
                        {
                            employee.FunctionRoleID = Employee.FunctionRole.ID;
                        }
                        employee.LocationID = Employee.Branch.ID;
                        employee.IsDeleted = false;
                        employee.CreateDate = DateTime.UtcNow;
                        employee.CreateBy = currentUser.GetCurrentUser().DisplayName;
                        employee.UserCategoryCode = Employee.UserCategoryCode;
                        //DBS.Entity.EmployeeDraft data = new DBS.Entity.EmployeeDraft();
                        //{
                        //    EmployeeUsername = EmployeeUsername,
                        //    EmployeeName = Employee.EmployeeName,
                        //    EmployeeEmail = Employee.EmployeeEmail,
                        //    RankID = Employee.Rank.ID,
                        //    BizSegmentID = Employee.Segment.ID,
                        //    LocationID = Employee.Branch.ID,
                        //    IsDeleted = false,
                        //    CreateDate = DateTime.UtcNow,                            
                        //    CreateBy = currentUser.GetCurrentUser().DisplayName
                        //};

                        context.EmployeeDrafts.Add(employee);
                        context.SaveChanges();

                        EmployeeID = employee.EmployeeDraftID;

                        if (Employee.Roles != null)
                        {
                            foreach (var item in Employee.Roles)
                            {
                                context.EmployeeRoleMappingDrafts.Add(new Entity.EmployeeRoleMappingDraft()
                                {
                                    EmployeeDraftID = employee.EmployeeDraftID,
                                    RoleID = item.ID,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });
                                context.SaveChanges();
                            }
                        }



                        ts.Complete();
                    }

                    isSuccess = true;
                    message = "New User Approval data has been created.";
                }
                else
                {
                    isSuccess = true;
                    message = string.Format("User Approval data for Employee ID {0} is already exist or waiting for approval.", Employee.EmployeeUsername);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateUserApproval(EmployeeModel Employee, ref long EmployeeID, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.EmployeeDrafts.Where(a => a.EmployeeUsername.Equals(Employee.EmployeeUsername) && a.IsDeleted.Equals(false)).Any())
                {
                    using (TransactionScope ts = new TransactionScope())
                    {
                        //insert to draft
                        DBS.Entity.EmployeeDraft data = new DBS.Entity.EmployeeDraft()
                        {
                            EmployeeUsername = Employee.EmployeeUsername,
                            EmployeeName = Employee.EmployeeName,
                            EmployeeEmail = Employee.EmployeeEmail,
                            //RankID = Employee.Rank.ID,
                            //BizSegmentID = Employee.Segment.ID,                            
                            LocationID = Employee.Branch.ID,
                            IsDeleted = false,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().DisplayName,
                            UserCategoryCode = Employee.UserCategoryCode,
                        };
                        context.EmployeeDrafts.Add(data);
                        context.SaveChanges();

                        EmployeeID = data.EmployeeDraftID;

                        if (Employee.Roles != null)
                        {
                            foreach (var item in Employee.Roles)
                            {
                                context.EmployeeRoleMappingDrafts.Add(new Entity.EmployeeRoleMappingDraft()
                                {
                                    EmployeeDraftID = data.EmployeeDraftID,
                                    RoleID = item.ID,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });
                                context.SaveChanges();
                            }
                        }

                        data.RankID = Employee.Rank.ID;
                        data.BizSegmentID = Employee.Segment.ID;

                        if (Employee.FunctionRole.ID != null && !string.IsNullOrEmpty(Employee.FunctionRole.Name))
                        {
                            data.FunctionRoleID = Employee.FunctionRole.ID;
                        }

                        context.SaveChanges();

                        ts.Complete();
                    }

                    isSuccess = true;
                    message = "New User Approval data has been created.";
                }
                else
                {
                    message = string.Format("Data for Employee ID {0} is waiting for approval proccess.", Employee.EmployeeUsername.Split('|')[2]);
                    isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetEmployeeLocation(string username, ref BranchModel branch, ref string message)
        {
            bool isSuccess = false;

            try
            {
                branch = (from emp in context.Employees
                          join loc in context.Locations on emp.LocationID equals loc.LocationID
                          join reg in context.Regions on loc.RegionID equals reg.RegionID
                          where emp.EmployeeUsername.ToLower().EndsWith("|" + username.ToLower()) && emp.IsDeleted.Equals(false)
                          select new BranchModel
                          {
                              ID = loc.LocationID,
                              Code = loc.LocationCode,
                              Name = loc.LocationName,
                              Region = loc.RegionID != null ? new RegionModel { ID = reg.RegionID, Name = reg.RegionName } : new RegionModel { ID = 0, Name = "" },
                              LastModifiedDate = loc.UpdateDate == null ? loc.CreateDate : loc.UpdateDate.Value,
                              LastModifiedBy = loc.UpdateBy == null ? loc.CreateBy : loc.UpdateBy,
                              SolID = loc.SolID,
                              IsHO = loc.IsHO != null ? loc.IsHO : false,
                              IsJakartaBranch = loc.IsJakartaBranch != null ? loc.IsJakartaBranch : false,
                              IsUpcountryBranch = loc.IsUpcountryBranch != null ? loc.IsUpcountryBranch : false,
                          }).FirstOrDefault();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool DeleteEmployeeDraft(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.EmployeeDraft data = context.EmployeeDrafts.Where(a => a.EmployeeDraftID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().LoginName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Employee Draft data for Employee Draft ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}