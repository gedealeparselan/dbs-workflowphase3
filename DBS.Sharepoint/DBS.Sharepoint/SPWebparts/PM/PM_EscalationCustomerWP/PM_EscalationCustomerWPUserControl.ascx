﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PM_EscalationCustomerWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.PM.PM_EscalationCustomerWP.PM_EscalationCustomerWPUserControl" %>

<h1 class="header smaller lighter dark">Escalation Customer</h1>
<div class="modal-body overflow-visible">
    <div class="row">
        <div class="col-xs-12">
            <div id="customer-form" class="form-horizontal" role="form">
                <div class="form-group no-margin-bottom no-padding-bottom">
                    <div class="row">
                        <div class="row">
                            <div class="col-sm-5">                         
                                     
                                    <div id="availablecustomer" class="widget-box">
	                                <!-- widget header start -->
	                                <div class="widget-header widget-hea1der-small header-color-dark">
		                                <h6>Available Customer</h6>

		                                <div class="widget-toolbar">
			                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
				                                <i class="blue icon-filter"></i>
				                                <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridPropertiesAvailableCustomer().AllowFilter" />
				                                <span class="lbl"></span>
			                                </label>
			                                <a href="#" data-bind="click: GetDataAvailableCustomer" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
				                                <i class="blue icon-refresh"></i>
			                                </a>
		                                </div>
	                                </div>
	                                <!-- widget header end -->

	                                <!-- widget body start -->
	                                <div class="widget-body">
		                                <!-- widget main start -->
		                                <div class="widget-main padding-0">
			                                <!-- widget slim control start -->
			                                <div class="slim-scroll" data-height="400">
				                                <!-- widget content start -->
				                                <div class="content">

					                                <!-- table responsive start -->
					                                <div class="table-responsive">
						                                <div class="dataTables_wrapper" role="grid">
							                                <table id="Matrix Approval-table" class="table table-striped table-bordered table-hover dataTable">
                                                                <thead>
                                                                    <tr data-bind="with: GridPropertiesAvailableCustomer">
                                                                        <th style="width:50px">No.</th>
                                                                        <th style="width:50px">Select</th>
                                                                        <th data-bind="click: function () { Sorting('CIF'); }, css: GetSortedColumn('CIF')">CIF</th>
                                                                        <th data-bind="click: function () { Sorting('Name'); }, css: GetSortedColumn('Name')">Customer Name</th>                                                                            
                                                                    </tr>
                                                                </thead>
                                                                <thead data-bind="visible: GridPropertiesAvailableCustomer().AllowFilter" class="table-filter">
                                                                    <tr>
                                                                        <th class="clear-filter">
                                                                            <a href="#" data-bind="click: ClearFiltersAvailableCustomer" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                <i class="green icon-trash"></i>
                                                                            </a>
                                                                        </th>
                                                                        <th></th>
                                                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCIFAvailableCustomer, event: { change: GridPropertiesAvailableCustomer().Filter }" /></th>
                                                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCustomerNameAvailableCustomer, event: { change: GridPropertiesAvailableCustomer().Filter }" /></th>                                                                            
                                                                    </tr>
                                                                </thead>
                                                                <tbody data-bind="foreach: AvailableCustomers, visible: AvailableCustomers().length > 0">
                                                                    <tr>
                                                                        <td><span  data-bind="attr: { id: 'AvailableCustomerRowID' + $index() },text: RowID"></span></td>
                                                                        <td class="center">
                                                                            <label>
                                                                                <input type="checkbox" data-bind="attr: { id: 'chkboxavailable' + $index() }, event: { change: $root.SelectAvailableCustomer }" />
                                                                                <span class="lbl"></span>
                                                                            </label>
                                                                        </td>
                                                                        <td><span data-bind="text: CIF"></span></td>
                                                                        <td><span data-bind="text: Name"></span></td>                                                                            
                                                                    </tr>
                                                                </tbody>
                                                                <tbody data-bind="visible: AvailableCustomers().length == 0">
                                                                    <tr>
                                                                        <td colspan="6" class="text-center">
                                                                            No entries available.
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
						                                </div>
					                                </div>
					                                <!-- table responsive end -->
				                                </div>
				                                <!-- widget content end -->
			                                </div>
			                                <!-- widget slim control end -->

			                                <!-- widget footer start -->
			                                <div class="widget-toolbox padding-8 clearfix">
				                                <div class="row" data-bind="with: GridPropertiesAvailableCustomer">
					                                <!-- pagination size start -->
					                                <div class="col-sm-7" style="padding-right:0px;">
						                                <div class="dataTables_paginate paging_bootstrap pull-left" style="text-align:left;">
							                                Showing <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
							                                rows of <span data-bind="text: Total"></span>
							                                entries
						                                </div>
					                                </div>
					                                <!-- pagination size end -->

					                                <!-- pagination navigation start -->
					                                <div class="col-sm-4" style="width:180px;margin-left:0px;margin-right:0px;">
						                                <div class="dataTables_paginate paging_bootstrap" style="text-align:left;">
							                                <ul class="pagination">
								                                <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
									                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
										                                <i class="icon-double-angle-left"></i>
									                                </a>
								                                </li>
								                                <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
									                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
										                                <i class="icon-angle-left"></i>
									                                </a>
								                                </li>
								                                <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
									                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
										                                <i class="icon-angle-right"></i>
									                                </a>
								                                </li>
								                                <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
									                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
										                                <i class="icon-double-angle-right"></i>
									                                </a>
								                                </li>
							                                </ul>
						                                </div>
					                                </div>
					                                <!-- pagination navigation end -->

				                                </div>
                                                <div class="row" data-bind="with: GridPropertiesAvailableCustomer">
                                                    <!-- pagination page jump start -->
					                                <div class="col-sm-6">
						                                <div class="dataTables_paginate paging_bootstrap" style="text-align:left;">
							                                Page <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width:50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page"/>
							                                of <span data-bind="text: TotalPages"></span>
						                                </div>
					                                </div>
					                                <!-- pagination page jump end -->
                                                </div>
			                                </div>
			                                <!-- widget footer end -->

		                                </div>
		                                <!-- widget main end -->
	                                </div>
	                                <!-- widget body end -->
                                </div>
                                <!-- widget box end --> 
                           
                            </div>
                            <div class="col-sm-1">
                                <div class="space-30"></div>   
                                <div class="space-30"></div>
                                <div class="space-30"></div>
                                <div class="space-30"></div>
                                <div class="space-30"></div>
                                <div class="space-30"></div>
                                <div class="space-30"></div>
                                <div class="space-10"></div>
                                <button id="btnPullLeft" type="button" style="margin-left:30%;" class="btn btn-sm btn-primary" data-bind="click: pullLeft, visible: $root.IsRoleMaker()">                                    
                                        <
                                </button>
                                <div class="space-2"></div>
                                <button id="btnPullRight" type="button" style="margin-left:30%;" class="btn btn-sm btn-primary" data-bind="click: pullRight, visible: $root.IsRoleMaker()">                                    
                                        >
                                </button>
                                <div class="space-2"></div>
                                <button id="btnUpdate" type="button" class="btn btn-sm btn-primary" data-bind="click: UpdateEscalation, visible: $root.IsRoleMaker()">
                                    <i class="icon-save"></i>
                                    Save
                                </button>                           
                            </div>
                            <div class="col-sm-5">
                                    <div id="escalatedcustomer" class="widget-box">
	                                <!-- widget header start -->
	                                <div class="widget-header widget-hea1der-small header-color-dark">
		                                <h6>Escalated Customer</h6>

		                                <div class="widget-toolbar">
			                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
				                                <i class="blue icon-filter"></i>
				                                <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridPropertiesEscalatedCustomer().AllowFilter" />
				                                <span class="lbl"></span>
			                                </label>
			                                <a href="#" data-bind="click: GetDataEscalatedCustomer" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
				                                <i class="blue icon-refresh"></i>
			                                </a>
		                                </div>
	                                </div>
	                                <!-- widget header end -->

	                                <!-- widget body start -->
	                                <div class="widget-body">
		                                <!-- widget main start -->
		                                <div class="widget-main padding-0">
			                                <!-- widget slim control start -->
			                                <div class="slim-scroll" data-height="400">
				                                <!-- widget content start -->
				                                <div class="content">

					                                <!-- table responsive start -->
					                                <div class="table-responsive">
						                                <div class="dataTables_wrapper" role="grid">
							                                <table id="Matrix Approval-table" class="table table-striped table-bordered table-hover dataTable">
                                                                <thead>
                                                                    <tr data-bind="with: GridPropertiesEscalatedCustomer">
                                                                        <th style="width:50px">No.</th>
                                                                        <th style="width:50px">Select</th>
                                                                        <th data-bind="click: function () { Sorting('CIF'); }, css: GetSortedColumn('CIF')">CIF</th>
                                                                        <th data-bind="click: function () { Sorting('Name'); }, css: GetSortedColumn('Name')">Customer Name</th>                                                                            
                                                                    </tr>
                                                                </thead>
                                                                <thead data-bind="visible: GridPropertiesEscalatedCustomer().AllowFilter" class="table-filter">
                                                                    <tr>
                                                                        <th class="clear-filter">
                                                                            <a href="#" data-bind="click: ClearFiltersEscalatedCustomer" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                <i class="green icon-trash"></i>
                                                                            </a>
                                                                        </th>
                                                                        <th></th>
                                                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCIFEscalatedCustomer, event: { change: GridPropertiesEscalatedCustomer().Filter }" /></th>
                                                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCustomerNameEscalatedCustomer, event: { change: GridPropertiesEscalatedCustomer().Filter }" /></th>                                                                            
                                                                    </tr>
                                                                </thead>
                                                                <tbody data-bind="foreach: EscalatedCustomers, visible: EscalatedCustomers().length > 0">
                                                                    <tr>
                                                                        <td><span data-bind="attr: { id: 'EscalatedCustomerRowID' + $index() },text: RowID"></span></td>
                                                                        <td class="center">
                                                                            <label>
                                                                                <input type="checkbox" data-bind="attr: { id: 'chkboxescalated' + $index() }, event: { change: $root.SelectEscalatedCustomer }" />
                                                                                <span class="lbl"></span>
                                                                            </label>
                                                                        </td>
                                                                        <td><span data-bind="text: CIF"></span></td>
                                                                        <td><span data-bind="text: Name"></span></td>                                                                            
                                                                    </tr>
                                                                </tbody>
                                                                <tbody data-bind="visible: EscalatedCustomers().length == 0">
                                                                    <tr>
                                                                        <td colspan="6" class="text-center">
                                                                            No entries available.
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
						                                </div>
					                                </div>
					                                <!-- table responsive end -->
				                                </div>
				                                <!-- widget content end -->
			                                </div>
			                                <!-- widget slim control end -->

			                                <!-- widget footer start -->
			                                <div class="widget-toolbox padding-8 clearfix">
				                                <div class="row" data-bind="with: GridPropertiesEscalatedCustomer">
					                                <!-- pagination size start -->
					                                <div class="col-sm-7">
						                                <div class="dataTables_paginate paging_bootstrap pull-left">
							                                Showing <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
							                                rows of <span data-bind="text: Total"></span>
							                                entries
						                                </div>
					                                </div>
					                                <!-- pagination size end -->

					                                <!-- pagination navigation start -->
					                                <div class="col-sm-5">
						                                <div class="dataTables_paginate paging_bootstrap">
							                                <ul class="pagination">
								                                <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
									                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
										                                <i class="icon-double-angle-left"></i>
									                                </a>
								                                </li>
								                                <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
									                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
										                                <i class="icon-angle-left"></i>
									                                </a>
								                                </li>
								                                <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
									                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
										                                <i class="icon-angle-right"></i>
									                                </a>
								                                </li>
								                                <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
									                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
										                                <i class="icon-double-angle-right"></i>
									                                </a>
								                                </li>
							                                </ul>
						                                </div>
					                                </div>
					                                <!-- pagination navigation end -->

				                                </div>
                                                <div class="row" data-bind="with: GridPropertiesEscalatedCustomer">
                                                    <!-- pagination page jump start -->
					                                <div class="col-sm-6">
						                                <div class="dataTables_paginate paging_bootstrap" style="text-align:left;">
							                                Page <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width:50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page"/>
							                                of <span data-bind="text: TotalPages"></span>
						                                </div>
					                                </div>
					                                <!-- pagination page jump end -->
                                                </div>
			                                </div>
			                                <!-- widget footer end -->

		                                </div>
		                                <!-- widget main end -->
	                                </div>
	                                <!-- widget body end -->
                                </div>
                                <!-- widget box end --> 
                           
                            </div>
                        </div>                            
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.hotkeys.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/bootstrap-wysiwyg.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>