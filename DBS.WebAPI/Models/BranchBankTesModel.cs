﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

using System.Data.Entity.SqlServer;

namespace DBS.WebAPI.Models
{
    #region Property
    public class BranchBankTesModel
    {
        public int BranchID { get; set; }
        public int? BankID { get; set; }
        public string BankDescription { get; set; }
        public string BranchCode { get; set; }
        public string BankAccount { get; set; }
        public string CommonName { get; set; }
        public int? CurrencyID { get; set; }
        public string PGSL { get; set; }
        public int? CityID { get; set; }
        public string CityDescription { get; set; }
        public bool IsDelete { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public BankModel Bank { get; set; }
        public CityModel City { get; set; }
    }
    public class BranchBankTesRow : BranchBankTesModel
    {
        public int RowID { get; set; }
    }
    public class BranchBankTesGrid : Grid
    {
        public IList<BranchBankTesRow> Rows { get; set; }
    }
    #endregion

    #region filter
    public class BranchBankTesFilter : Filter { }
    #endregion

    #region Interface
    public interface IBranchBankTes : IDisposable
    {
        bool GetBranchBank(ref IList<BranchBankTesModel> BranchBank, ref string message);
        bool GetBranchBankByID(int id, ref BranchBankTesModel BranchBank, ref string message);
        bool GetBranchBankGrid(int page, int size, IList<BranchBankTesFilter> filters, string sortColumn, string sortOrder, ref BranchBankTesGrid branchbank, ref string message);
        bool addBranchBank(BranchBankTesModel BranchBank, ref string message);
        bool UpdateBranchBank(int id, BranchBankTesModel BranchBank, ref string message);
        bool DeleteBranchBank(int id, ref string message);
        bool GetBranchBankDetail(int BankID, int page, int size, IList<BranchBankTesFilter> filters, string sortColumn, string sortOrder, ref BranchBankTesGrid branchbank, ref string message);
    }
    #endregion

    #region Repository
    public class BranchBankTesRepository : IBranchBankTes
    {

        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetBranchBank(ref IList<BranchBankTesModel> BranchBank, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    BranchBank = (from a in context.Branches
                                  where a.IsDeleted.Equals(false)
                                  orderby new { a.BranchCode, a.CommonName }
                                  select new BranchBankTesModel
                                  {
                                      BranchCode = a.BranchCode,
                                      BankID = a.BankID,
                                      BankAccount = a.BankAccount,
                                      CommonName = a.CommonName,
                                      CurrencyID = a.CurrencyID,
                                      PGSL = a.PGSL,
                                      CityID = a.CityID,
                                      LastModifiedBy = a.CreateBy,
                                      LastModifiedDate = a.CreateDate
                                  }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetBranchBankByID(int id, ref BranchBankTesModel BranchBank, ref string message)
        {
            bool isSuccess = false;
            try
            {
                BranchBank = (from a in context.Branches
                              select new BranchBankTesModel
                              {
                                  BranchCode = a.BranchCode,
                                  BankID = a.BankID,
                                  BankAccount = a.BankAccount,
                                  CommonName = a.CommonName,
                                  CurrencyID = a.CurrencyID,
                                  PGSL = a.PGSL,
                                  CityID = a.CityID,
                                  LastModifiedBy = a.CreateBy,
                                  LastModifiedDate = a.CreateDate
                              }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetBranchBankGrid(int page, int size, IList<BranchBankTesFilter> filters, string sortColumn, string sortOrder, ref BranchBankTesGrid branchbank, ref string message)
        {
            bool isSucces = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                BranchBankTesModel filter = new BranchBankTesModel();
                if (filters != null)
                {
                    filter.BranchCode = filters.Where(a => a.Field.Equals("BranchCode")).Select(a => a.Value).SingleOrDefault();
                    filter.BankDescription = filters.Where(a => a.Field.Equals("BankDescription")).Select(a => a.Value).SingleOrDefault();
                    filter.CommonName = filters.Where(a => a.Field.Equals("CommonName")).Select(a => a.Value).SingleOrDefault();
                    filter.PGSL = filters.Where(a => a.Field.Equals("PGSL")).Select(a => a.Value).SingleOrDefault();
                    filter.CityDescription = filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                 if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.Branches
                                join b in context.Banks on a.BankID equals b.BankID
                                join d in context.Cities on a.CityID equals d.CityID

                                let isBranchCode = !string.IsNullOrEmpty(filter.BranchCode)
                                let isBankDescription = !string.IsNullOrEmpty(filter.BankDescription)
                                let isCommonName = !string.IsNullOrEmpty(filter.CommonName)
                                let isPGSL = !string.IsNullOrEmpty(filter.PGSL)
                                let isCityDescription = !string.IsNullOrEmpty(filter.CityDescription)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false

                                where a.IsDeleted == false &&
                                  (isBranchCode ? a.BranchCode.Contains(filter.BranchCode) : true) &&
                                  (isBankDescription ? b.BankDescription.Contains(filter.BankDescription) :true) &&
                                  (isCommonName ? a.CommonName.Contains(filter.CommonName) : true) &&
                                  (isPGSL ? a.PGSL.Contains(filter.PGSL) : true) &&
                                  (isCityDescription ? d.Description.Contains(filter.CityDescription) : true) &&
                                  (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                  (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true) &&
                                  b.IsDeleted == false &&
                                  d.IsDeleted == false 
                                                                                                 
                                select new BranchBankTesModel
                                {
                                    BranchID = a.BranchID,
                                    BranchCode = a.BranchCode,
                                    BankID = b.BankID,
                                    BankDescription = b.BankDescription,
                                    BankAccount = a.BankAccount,
                                    CommonName = a.CommonName,
                                    PGSL = a.PGSL,
                                    CityID = d.CityID,
                                    CityDescription = d.Description,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    branchbank.Page = page;
                    branchbank.Size = size;
                    branchbank.Total = data.Count();
                    branchbank.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    branchbank.Rows = data.OrderBy(orderBy).AsEnumerable().Select((a, i) => new BranchBankTesRow
                    {
                        RowID = i + 1,
                        BranchID = a.BranchID,
                        BankID = a.BankID,
                        BranchCode = a.BranchCode,
                        BankDescription = a.BankDescription,
                        BankAccount = a.BankAccount,
                        CommonName = a.CommonName,
                        PGSL = a.PGSL,
                        CityID = a.CityID,
                        CityDescription = a.CityDescription,
                        LastModifiedBy = a.LastModifiedBy,
                        LastModifiedDate = a.LastModifiedDate
                    }).Skip(skip).Take(size).ToList();
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;

        }

        public bool addBranchBank(BranchBankTesModel BranchBank, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.Branches.Where(a => a.BranchID == BranchBank.BranchID  && a.IsDeleted == false).Any())
                {
                    context.Branches.Add(new Entity.Branch()
                    {
                        BranchCode = BranchBank.BranchCode,
                        BankID = BranchBank.BankID,
                        BankAccount = BranchBank.BankAccount,
                        CommonName = BranchBank.CommonName,
                        PGSL = BranchBank.PGSL,
                        CityID = BranchBank.CityID,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().LoginName
                    });
                    context.SaveChanges();
                    isSuccess = true;

                }
                else
                {
                    message = string.Format("Branch Bank data for Product code {0} is already exist.", BranchBank.BranchCode);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateBranchBank(int id, BranchBankTesModel BranchBank, ref string message)
        {
            bool isSucces = false;
            try
            {
                Entity.Branch data = context.Branches.Where(a => a.BranchID.Equals(id)).SingleOrDefault();
                if (data != null)
                {
                    //if (context.Branches.Where(a => a.BranchID != BranchBank.BranchID && a.BranchCode.Equals(BranchBank.BranchCode) && a.IsDeleted.Equals(false)).Count() >= 1)
                    //{
                    //    message = string.Format("Branch Code {0} does not exist.", BranchBank.BranchCode);
                    //}
                    //else
                    //{
                        data.BranchCode = BranchBank.BranchCode;
                        data.BankID = BranchBank.BankID;
                        data.BankAccount = BranchBank.BankAccount;
                        data.CommonName = BranchBank.CommonName;
                        data.PGSL = BranchBank.PGSL;
                        data.CityID = BranchBank.CityID;
                        data.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        data.UpdateDate = DateTime.UtcNow;
                        context.SaveChanges();
                        isSucces = true;
                   // }
                }
                else
                {
                    message = string.Format("Data Branch Bank {0} is does not exist.", id);
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool DeleteBranchBank(int id, ref string message)
        {
            bool isSucces = false;
            try
            {
                Entity.Branch data = context.Branches.Where(a => a.BranchID.Equals(id)).SingleOrDefault();
                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                    context.SaveChanges();
                    isSucces = true;
                }
                else
                {
                    message = string.Format("Branch Bank data for Branch Bank {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        currentUser = null;
                        context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public bool GetBranchBankDetail(int BankID, int page, int size, IList<BranchBankTesFilter> filters, string sortColumn, string sortOrder, ref BranchBankTesGrid branchbank, ref string message)
        {
            //throw new NotImplementedException();
            bool isSuccess = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                BranchBankTesModel filter = new BranchBankTesModel();
                if (filters != null)
                {

                    filter.BranchCode = (string)filters.Where(a => a.Field.Equals("BranchCode")).Select(a => a.Value).SingleOrDefault();
                    filter.BankAccount = (string)filters.Where(a => a.Field.Equals("BankAccount")).Select(a => a.Value).SingleOrDefault();
                    filter.CommonName = (string)filters.Where(a => a.Field.Equals("CommonName")).Select(a => a.Value).SingleOrDefault();
                    // filter.Currency = filters.Where(a => a.Field.Equals("Currency")).Select(a => a.Value).SingleOrDefault();

                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.Branches
                                join b in context.Banks on a.BankID equals b.BankID
                                join c in context.Cities on a.CityID equals c.CityID
                                // let isBranchCode = !string.IsNullOrEmpty(filter.BranchCode)
                                let isBranchCode = !string.IsNullOrEmpty(filter.BranchCode)
                                let isBankAccount = !string.IsNullOrEmpty(filter.BankAccount)
                                let isCommonName = !string.IsNullOrEmpty(filter.CommonName)
                                let isCurrencyID = filter.CurrencyID
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false

                                where a.IsDeleted == false && (a.BankID == BankID) &&
                                (isBranchCode ? a.BranchCode.Contains(filter.BranchCode) : true) &&
                                (isBankAccount ? a.BankAccount.Contains(filter.BankAccount) : true) &&
                                (isCommonName ? a.CommonName.Contains(filter.CommonName) : true)


                                select new BranchBankTesModel
                                {
                                    BranchCode = a.BranchCode,
                                    BankDescription = b.BankDescription,
                                    BankAccount = a.BankAccount,
                                    CommonName = a.CommonName,
                                    //CurrencyDescription =c.CurrencyDescription,
                                    PGSL = a.PGSL,
                                    CityDescription = c.Description,
                                    LastModifiedBy = a.CreateBy,
                                    LastModifiedDate = a.CreateDate
                                });

                    branchbank.Page = page;
                    branchbank.Size = size;
                    branchbank.Total = data.Count();
                    branchbank.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    branchbank.Rows = data.OrderBy(orderBy).AsEnumerable().Select((a, i) => new BranchBankTesRow
                    {
                        RowID = i + 1,
                        BranchCode = a.BranchCode,
                        BankDescription = a.BankDescription,
                        BankAccount = a.BankAccount,
                        CommonName = a.CommonName,
                        // CurrencyDescription = a.CurrencyDescription,
                        PGSL = a.PGSL,
                        CityDescription = a.CityDescription,
                        LastModifiedBy = a.CreateBy,
                        LastModifiedDate = a.CreateDate
                    }).Skip(skip).Take(size).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
    }
    #endregion
}