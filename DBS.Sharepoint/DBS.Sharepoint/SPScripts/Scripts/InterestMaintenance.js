﻿$(document).ready(function () {
    var InterestMaintenance = {};
    var InterestMaintenanceList = [];
    $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });
    GetDataInterestMaintenance()
});

function Hide() {
    $('td:nth-child(2),th:nth-child(2)').hide();
}

function GetDataInterestMaintenance() {
    var selectedDate = $("#IMDate").val();
    var options = {
        url: api.server + api.url.workflow.transactionLoanCheck,
        token: accessToken,
        params: {
            interestDate: selectedDate
        }
    };

    Helper.Ajax.Get(options, function (result, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            var $container = $("#dataTable");
            $container.handsontable({
                data: result,
                minSpareCols: 1,
                fixedColumnsLeft: 3,
                rowHeaders: true,
                colHeaders: ["SolID", "CIF", "CustomerName", "LoanContractNo", "DueDate", "CurrencyID","CCY", "PreviousInterestRate", "CurrentInterestRate", "BizSegmentID","BizSegment", "EmployeeID","RM", "CSO",
                            "IsSBLSecured", "IsHeavyEquipment", "ValueDate", "NextPrincipalDate", "NextInterestDate", "ApprovedMarginPerCM",
                            "InterestRateCodeID","InterestRateCode", "BaseRate", "AccountPreferecial", "SpecialFTP", "SpecialRate", "AllInRate", "ApprovalDOA", "ApprovedBy","Remark"],
                contextMenu: true,
                columns: [
                            {
                                data: 'SolID'
                            },
                            {
                                data: 'CIF'
                            },
                            {
                                data: 'CustomerName'
                            },
                            {
                                data: 'LoanContractNo'
                            },
                            {
                                data: 'DueDate'
                            },
                            {
                                data: 'CurrencyID'
                            },
                            {
                                data: 'CurrencyCode'
                            },
                            {
                                data: 'PreviousInterestRate'
                            },
                            {
                                data: 'CurrentInterestRate'
                            },
                            {
                                data: 'BizSegmentID'
                            },
                            {
                                data: 'BizSegmentName'
                            },
                            {
                                data: 'EmployeeID'
                            },
                            {
                                data: 'RM'
                            },
                            {
                                data: 'CSO'
                            },
                            {
                                data: 'IsSBLSecured'
                            },
                            {
                                data: 'IsHeavyEquipment'
                            },
                            {
                                data: 'ValueDate'
                            },
                            {
                                data: 'NextPrincipalDate'
                            },
                            {
                                data: 'NextInterestDate'
                            },
                            {
                                data: 'ApprovedMarginPerCM'
                            },
                            {
                                data: 'InterestRateCodeID'
                            },
                            {
                                data: 'InterestRateCodeName'
                            },
                            {
                                data: 'BaseRate'
                            },
                            {
                                data: 'AccountPreferecial'
                            },
                            {
                                data: 'SpecialFTP'
                            },
                            {
                                data: 'Margin'
                            },
                            {
                                data: 'AllInRate'
                            },
                            {
                                data: 'ApprovalDOA'
                            },
                            {
                                data: 'ApprovedBy'
                            },
                            {
                                data: 'Remarks'
                            }
                ]
            });
            var handsontable = $container.data('handsontable');
            Hide();
        }
    }, OnError);
    
    //console.log(handsontable.getData());

};