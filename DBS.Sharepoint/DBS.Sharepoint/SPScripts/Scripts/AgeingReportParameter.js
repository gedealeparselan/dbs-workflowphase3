function Day(id, name) {
    this.id = id;
    this.name = name;
}

var DaysInWeek = [
    new Day(1, 'Sunday'),
    new Day(2, 'Monday'),
    new Day(3, 'Tuesday'),
    new Day(4, 'Wednesday'),
    new Day(5, 'Thursday'),
    new Day(6, 'Friday'),
    new Day(7, 'Saturday')
];



var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;
    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);
    //Declare observable which will be bind with UI
    self.ID = ko.observable("");
    self.Name = ko.observable("");
    self.Days = ko.observable("");
    self.Times = ko.observable('00:00');
    self.AgeingDays = ko.observable(DaysModel);
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");
    //self.vDays = ko.observableArray([new DaysModel('')]);
    self.vTimes = ko.observableArray(['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']);
    self.selectedTimes = ko.observable('00:00');
    self.tmpDays = ko.observableArray([]);

    self.DaysInWeek = ko.observableArray(DaysInWeek);
    self.selectedDays = ko.observableArray([]);

    // filter
    self.FilterName = ko.observable("");
    self.FilterTimes = ko.observable("");
    self.FilterDays = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");

    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    // New Data flag
    self.IsNewData = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.AgeingReportParameters = ko.observableArray([]);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    // set default sorting
    self.GridProperties().SortColumn("Name");
    self.GridProperties().SortOrder("ASC");

    // bind clear filters
    self.ClearFilters = function () {
        self.FilterName("");
        self.FilterTimes("");
        self.FilterDays("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");

        GetData();
    };

    // bind get data function to view
    self.GetData = function () {
        GetData();
    };

    //The Object which stored data entered in the observables
    var AgeingReportParameter = {
        ID: self.ID,
        Name: self.Name,
        Times: self.Times,
        Days: self.Days,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
    };

    /*self.onChange = function () {
        //removed
        if (this.vDays().length > this.selectedTimes()) {
            for (var i = this.vDays().length; i > this.selectedTimes(); i--) {
                self.vDays.pop(new DaysModel(''));
            }
        }
        else {
            //added
            for (var i = this.vDays().length + 1; i <= this.selectedTimes(); i++) {
                self.vDays.push(new DaysModel(''));
            }
        }
    };*/

    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    self.save = function () {
        // validation
        IsvalidField();
        var form = $("#aspnetForm");
        form.validate();

        if (self.selectedDays().length == 0) {
            alert('Days of Ageing must be selected');
            return;
        }

        self.Days(self.selectedDays().join(","));

        if (form.valid()) {
            //Ajax call to insert the AgeingReportParameter
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {

                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.ageingreport,
                        data: ko.toJSON(AgeingReportParameter), //Convert the Observable Data into JSON
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        contentType: "application/json",
                        success: function (data, textStatus, jqXHR) {
                            // send notification
                            ShowNotification('Create Success', jqXHR.responseText, 'gritter-success', false);

                            // hide current popup window
                            $("#modal-form").modal('hide');

                            // refresh data
                            GetData();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification('An Error Occured', jqXHR.responseText, 'gritter-error', false);
                        }
                    });
                }
            });
        }
    };

    self.update = function () {
        // validation
        IsvalidField();
        var form = $("#aspnetForm");
        form.validate();

        if (self.selectedDays().length == 0) {
            alert('Days of Ageing must be selected');
            return;
        }

        self.Days(self.selectedDays().join(","));

        if (form.valid()) {

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    //Ajax call to update the AgeingReportParameter
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.ageingreport + "/" + AgeingReportParameter.ID(),
                        data: ko.toJSON(AgeingReportParameter),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            // send notification
                            if (jqXHR.status = 200) {
                                // refresh data
                                GetData();

                                ShowNotification('Update Success', jqXHR.responseText, 'gritter-success', false);
                            } else {
                                ShowNotification('An Error Occured', jqXHR.responseText, 'gritter-error', false);

                                $("#modal-form").modal('show');
                            }


                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification('An Error Occured', jqXHR.responseText, 'gritter-error', false);
                        }
                    });
                }
            });
        }
    };

    self.delete = function () {

        bootbox.confirm("Are you sure want to delete?", function (result) {

            if (!result) {
                $("#modal-form").modal('show');
            } else {
                //Ajax call to delete the AgeingReportParameter
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.ageingreport + "/" + AgeingReportParameter.ID(),
                    data: ko.toJSON(AgeingReportParameter), //Convert the Observable Data into JSON
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        ShowNotification('Delete Success', jqXHR.responseText, 'gritter-success', false);

                        // refresh data
                        GetData();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification('An Error Occured', jqXHR.responseText, 'gritter-error', false);
                    }
                });
            }
        });
    };

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return ""
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        self.IsNewData(false);

        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');

        self.ID(data.ID);
        self.Name(data.Name);
        self.Times(data.Times.substring(0, 5));

        self.selectedTimes(data.Times);
        self.selectedDays.removeAll();

        var arr = data.Days.split(',');
        for (var i = 0; i < arr.length; i++) {
            self.selectedDays.push(arr[i]);
        }

        //self.Days(data.Days);
        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);

        //lock off time picker on approval page    
        if ($('#TimePicker').is(':disabled')) {
            DisabledTimePicker();
        }


    };

    function DisabledTimePicker() {
        $('div.bootstrap-timepicker-widget.dropdown-menu.open').removeClass('open');
        var time = $('#TimePicker').val();
        $(document).mousedown(function (event) {
            switch (event.which) {
                case 1:
                case 2:
                case 3:
                    if ($('#TimePicker').is(':disabled')) {
                        $('#TimePicker').val(time);
                    }
                    break;

            }

        });

    }
    //insert new
    self.NewData = function () {
        // flag as new Data
        self.IsNewData(true);
        self.Readonly(false);
        // bind empty data
        self.ID(0);
        self.Name('');
        self.Times('00:00');
        self.selectedDays.removeAll();
        self.selectedTimes('00:00');
    };

    //Function to Read All Customers
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.ageingreport,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }

    //Function to validation dynamic field
    function IsvalidField() {
        $("[name^=days]").each(function () {
            $(this).rules('add', {
                required: true,
                maxlength: 2,
                number: true,
                messages: {
                    required: "Please provide a valid Day.",
                    maxlength: "Please provide maximum 2 number valid day value.",
                    number: "Please provide number"
                }
            });
        });
    }

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterName() != "") filters.push({ Field: 'Name', Value: self.FilterName() });
        if (self.FilterTimes() != "") filters.push({ Field: 'Times', Value: self.FilterTimes() });
        if (self.FilterDays() != "") filters.push({ Field: 'Days', Value: self.FilterDays() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

        return filters;
    };

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.AgeingReportParameters(data.Rows);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });

    //set selected contact
    $('#TimePicker').val('');
    $('.bootstrap-timepicker-hour').val("00");
    $('.bootstrap-timepicker-minute').val("00");

    $('.time-picker').timepicker({
        defaultTime: "",
        minuteStep: 1,
        showSeconds: false,
        showMeridian: false
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });
    var browser = navigator.userAgent.toLowerCase();
    var onScrolling = 'mousewheel';

    if ((browser.indexOf("chrome") > -1) || (browser.indexOf("safari") > -1)
       || (browser.indexOf("opera") > -1)) {
        onScrolling = 'mousewheel';
        hideDatePicker(onScrolling);
        removeStarInsideDatePickerChrome();

    } else if ((browser.indexOf("msie") > -1)) {
        onScrolling = 'mousewheel';
        hideDatePicker(onScrolling);
        removeStarInsideDatePickerIE();

    } else {
        onScrolling = 'DOMMouseScroll';
        hideDatePicker(onScrolling);

    }

    function removeStarInsideDatePickerChrome() {
        var star = jQuery(".control-label.bolder.text-danger.starremove");
        var starFirefox = jQuery(".control-label.bolder.text-danger.starremove.starremovefirefox");

        star.removeClass("starremove");
        starFirefox.removeClass("starremovefirefox");
    }
    function removeStarInsideDatePickerIE() {
        var star = jQuery(".control-label.bolder.text-danger.starremove.starremovefirefox");

        star.removeClass("starremovefirefox");
    }

    function hideDatePicker(Scrolling) {

        jQuery(window).on(onScrolling, function () {

            jQuery('.datepicker.datepicker-dropdown.dropdown-menu').hide();

        });

    }
};
