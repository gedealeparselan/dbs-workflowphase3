﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;


namespace DBS.WebAPI.Controllers
{
    //[Authorize]
    public class VolumeMatrixChannelController : ApiController
    {

        private string message = string.Empty;
        private readonly IVolumeMatrixChannel repositoryChannel = null;
        public VolumeMatrixChannelController()
        {
            this.repositoryChannel = new VolumeMatrixDashboardChannelRepository();
        }
        public VolumeMatrixChannelController(IVolumeMatrixChannel repositoryChannel)
        {
            this.repositoryChannel = repositoryChannel;

        }
        [ResponseType(typeof(IList<VolumeMatrixChannelModel>))]
        public HttpResponseMessage Get(string productTypeDetailID, DateTime startDate, DateTime endDate, DateTime clientDateTime, string paymentMode)
        {
            IList<VolumeMatrixChannelModel> output = new List<VolumeMatrixChannelModel>();
            if (!repositoryChannel.GetVolumeMatrixDashboardChannel(productTypeDetailID, startDate, endDate, clientDateTime, paymentMode, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<VolumeMatrixChannelModel>>(HttpStatusCode.OK, output);
            }
        }
    }

}
