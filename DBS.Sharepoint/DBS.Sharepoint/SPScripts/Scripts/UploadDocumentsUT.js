var accessToken;
var DocumentModel = ko.observable({ FileName: '', DocumentPath: '' });
var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};
var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};
var ParameterModel = {
    ID: ko.observable(),
    CatagoryName: ko.observable(),
    Name: ko.observable(),
    Itemvalue: ko.observable()
}
var Parameter = {
    UploadTypes: ko.observableArray([ParameterModel])
}
var SelectedModel = {
    UploadTypes: ko.observable()
}
var ViewModel = function () {
    var self = this;
    self.SPUser = ko.observable(SPUser);
    self.ID = ko.observable("");
    self.UploadType = ko.observable(Parameter);
    self.Document = ko.observable("");
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");

    // grid properties
    self.availableSizes = ko.observableArray([10, 25, 50, 75, 100]);
    self.Page = ko.observable(1);
    self.Size = ko.observableArray([10]);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.resultMessage = ko.observable("");
    self.resultHeader = ko.observable("");
    self.RowFailed = ko.observableArray([]);
    self.IsUploading = ko.observable(false);
    self.Column1Name = ko.observable("");
    self.Column2Name = ko.observable("");
    self.CloseProgres = function () {
        $("#modal-form-Upload").modal('hide');
    }
    // sorting
    self.SortColumn = ko.observable("Name");
    self.SortOrder = ko.observable("ASC");

    // Options selected value
    self.Selected = ko.observable(SelectedModel);

    // Parameters
    self.Parameter = ko.observable(Parameter);

    //Declare an ObservableArray for Storing the JSON Response
    self.FieldUploadItems = ko.observableArray([]);
    self.FailedUploads = ko.observableArray([]);
    self.DocumentPath = ko.observable();
    var UploadUTModel = {
        ID: self.ID,
        UploadType: self.UploadType,
        FileName: DocumentModel().FileName,
        DocumentPath: DocumentModel().DocumentPath,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
    }
    self.UploadUT = ko.observable(UploadUTModel);

    self.GetParameters = function () {
        GetParameters();
    }
    self.Modul = function () {
        var url = window.location.href;
        var urlmodul = url.substring(url.lastIndexOf("/") + 1);
        var arrmodul = urlmodul.split("-");
        var modul = null;
        switch (arrmodul[1]) {
            case "fd":
                modul = "fd";
                break;
            case "tmo":
                modul = "tmo";
                break;
            case "payment":
                modul = "payment";
                break;
            case "ut":
                modul = "ut";
                break;
            case "collateral":
                modul = "collateral";
                break;
            case "loan":
                modul = "loan";
                break;
            case "cif":
                modul = "cif";
                break;
            case "ibg":
                modul = "ibg";
                break;
            case "cso":
                modul = "cso";
                break;
        }
        return modul;
    }
    self.btnUpload = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        var UploadFileModel = {
            Path: $('input[type=file]')[0].files[0] != null ? $('input[type=file]')[0].files[0].name : "",
            Type: $('input[type=file]')[0].files[0] != null ? $('input[type=file]')[0].files[0].type : ""
        }
        if (form.valid()) {
            self.resultHeader('');
            self.resultMessage('');
            self.RowFailed([]);
            viewModel.IsUploading(true);
            $("#modal-form-Upload").modal('show');
            $("#transaction-progress").show();
            $("#transaction-result").hide();
            UploadFile(UploadFileModel, UploadedProcess);
        }
    }

    function UploadFile(document, callback) {
        var serverRelativeUrlToFolder = '/UploadedDocuments';
        var parts = document.Path.split('.');
        var fileExtension = parts[parts.length - 1];
        var timeStamp = new Date().getTime();
        var fileName = timeStamp + "." + fileExtension;

        var serverUrl = _spPageContextInfo.webAbsoluteUrl;

        var output;

        var getFile = getFileBuffer();
        getFile.done(function (arrayBuffer) {

            var addFile = addFileToFolder(arrayBuffer);
            addFile.done(function (file, status, xhr) {
                output = file.d;

                var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
                getItem.done(function (listItem, status, xhr) {
                    var UploadFileModel = {
                        Path: output.ServerRelativeUrl,
                        Type: document.Type,
                        ID: listItem.d.ID,
                        UploadType: viewModel.UploadUT().UploadType().Itemvalue
                    }
                    callback(UploadFileModel);
                });
                getItem.fail(OnError);
            });
            addFile.fail(OnError);
        });
        getFile.fail(OnError);

        // Get the local file as an array buffer.
        function getFileBuffer() {
            var deferred = $.Deferred();
            var reader = new FileReader();
            reader.onloadend = function (e) {
                deferred.resolve(e.target.result);
            }
            reader.onerror = function (e) {
                deferred.reject(e.target.error);
            }
            reader.readAsArrayBuffer($('input[type=file]')[0].files[0]);
            return deferred.promise();
        }

        // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
        function getListItem(fileListItemUri) {

            // Send the request and return the response.
            return $.ajax({
                url: fileListItemUri,
                type: "GET",
                headers: { "accept": "application/json;odata=verbose" }
            });
        }

        function addFileToFolder(arrayBuffer) {

            var fileCollectionEndpoint = String.format(
                    "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                    "/add(overwrite=false, url='{2}')",
                serverUrl, serverRelativeUrlToFolder, fileName);
            return $.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val()
                }
            });
        }
    }

    function UploadedProcess(UploadFileModel) {
        var options = {
            url: "/_vti_bin/DBSUploadFile/UploadFileService.svc/UploadFile",
            data: ko.toJSON(UploadFileModel)
        };
        Helper.Sharepoint.Nintex.Post(options, OnSuccessReadFile, OnError, OnAlways);
    }

    function OnSuccessReadFile(data, textStatus, jqXHR) {
        viewModel.IsUploading(false);
        if (data.Message == "Upload File Success.") {
            if (data.FailedRows == 0) {
                var ResultHeader = '';
                switch (viewModel.UploadUT().UploadType().Itemvalue) {
                    case "TMO_MUREX":
                        ResultHeader = "Uploaded Murex Data";
                        Col1Name = "NB";
                        Col2Name = "Counter Party";
                        self.resultHeader(ResultHeader);
                        self.resultMessage('Rows Affected:' + data.RowsAffected);
                        self.Column1Name(Col1Name);
                        self.Column2Name(Col2Name);
                        self.RowFailed(data.ReturnRows);
                        $("#transaction-progress").hide();
                        $("#transaction-result").show();
                        $("#transaction-grid").show();
                        break;
                    default:
                        self.resultHeader(data.Message);
                        self.resultMessage('Rows Affected:' + data.RowsAffected);
                        $("#transaction-progress").hide();
                        $("#transaction-result").show();
                        break;
                }
            }
            else {
                var IsNeedReturn = true;
                var ResultHeader = '';
                var ResultMsg = 'Rows Failed:' + data.FailedRows;
                var Col1Name = '';
                var Col2Name = '';
                switch (viewModel.UploadUT().UploadType().Itemvalue) {
                    case "TMO_MUREX":
                        ResultHeader = "Failed to mapping customer in Murex";
                        Col1Name = "NB";
                        Col2Name = "Counter Party";
                        break;
                    case "TMO_MUREXNAME":
                        ResultHeader = "Failed to mapping customer in Murex Mapping";
                        Col1Name = "Murex Name";
                        Col2Name = "CIF";
                        break;
                    case "FD_UPLOAD_INTEREST":
                        ResultHeader = "Failed to upload";
                        Col1Name = "CIF";
                        Col2Name = "Customer Name";
                        break;
                    case "LOAN_UPLOAD_ROLLOVER":
                        ResultHeader = "Failed to upload";
                        Col1Name = "CIF";
                        Col2Name = "Loan Contract No.";
                        break;
                    case "LOAN_UPLOAD_INTEREST":
                        ResultHeader = "Failed to upload";
                        Col1Name = "CIF";
                        Col2Name = "Loan Contract No.";
                        break;
                    case "LOAN_UPLOAD_SAM":
                        ResultHeader = "Failed to upload";
                        Col1Name = "CIF";
                        Col2Name = "Customer Name";
                        break;
                    case "LOAN_UPLOAD_EOD":
                        ResultHeader = "Failed to upload";
                        Col1Name = "SOL ID";
                        Col2Name = "Account No.";
                        break;
                    case "LOAN_UPLOAD_SG":
                        ResultHeader = "Failed to upload";
                        Col1Name = "SOL ID";
                        Col2Name = "Account No.";
                        break;
                    case "LOAN_UPLOAD_DUE":
                        ResultHeader = "Failed to upload";
                        Col1Name = "Cust No";
                        Col2Name = "Customer Name";
                        break;
                    case "LOAN_UPLOAD_ADHOC":
                        ResultHeader = "Failed to upload";
                        Col1Name = "CIF";
                        Col2Name = "Branch Code";
                        break;
                    case "IBG_ANNUAL":
                        ResultHeader = "Failed to upload";
                        Col1Name = "CIF";
                        Col2Name = "Customer Name";
                        break;
                    case "CSO_UPLOAD":
                        ResultHeader = "Failed to upload (Can't find CIF or CSO Name)";
                        Col1Name = "CIF";
                        Col2Name = "CSO Name";
                        break;
                    default:
                        IsNeedReturn = false;
                        break;
                }

                if (IsNeedReturn) {
                    self.resultHeader(ResultHeader);
                    self.resultMessage(ResultMsg);
                    self.Column1Name(Col1Name);
                    self.Column2Name(Col2Name);
                    self.RowFailed(data.ReturnRows);
                    $("#transaction-progress").hide();
                    $("#transaction-result").show();
                    $("#transaction-grid").show();
                }
                else {
                    self.resultHeader(ResultHeader);
                    self.resultMessage(ResultMsg);
                    $("#transaction-progress").hide();
                    $("#transaction-result").show();
                    $("#transaction-grid").hide();
                }
            }
        } else {
            self.resultHeader(data.Message);
            self.resultMessage('Rows Affected:' + 0);
            $("#transaction-progress").hide();
            $("#transaction-result").show();
            $("#transaction-grid").hide();
        }
    }

    function GetParameters() {
        var options = {
            url: api.server + api.url.helper + "/Dropdownlists/UPLOAD_TYPE/" + viewModel.Modul(),
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
    }
    function OnSuccessGetParameters(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            var mapping = {
                'ignore': ["LastModifiedDate", "LastModifiedBy"]
            };

            viewModel.Parameter().UploadTypes(ko.mapping.toJS(data.UPLOAD_TYPE, mapping));

        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    // Function to display notification. 
    // class name : gritter-success, gritter-warning, gritter-error, gritter-info
    function ShowNotification(title, text, className, isSticky) {
        $.gritter.add({
            title: title,
            text: text,
            class_name: className,
            sticky: isSticky,
        });
    }

    self.onPageSizeChange = function () {
        self.Page(1);

        GetData();
    };

    self.onPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }
        GetData();
    };

    self.nextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);
            GetData();
        }
    }

    self.previousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);
            GetData();
        }
    }

    self.firstPage = function () {
        self.Page(1);
        GetData();
    }

    self.lastPage = function () {
        self.Page(self.TotalPages());
        GetData();
    }

    self.filter = function () {
        self.Page(1);
        GetData();
    }

    self.sorting = function (column) {
        //alert(column)

        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }
        self.SortColumn(column);
        self.Page(1);
        GetData();
    }


    function Confirm(text) {
        bootbox.confirm(text, function (result) {
            return result;
        });
    }

    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    function OnAlways() {
        //$box.trigger('reloaded.ace.widget');
    }
    // Event handlers declaration end
};
// View Model
var viewModel = new ViewModel();
$(document).ready(function () {
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(OnSuccessToken, OnError);
    } else {
        accessToken = $.cookie(api.cookie.name);
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);
            viewModel.SPUser(ko.mapping.toJS(spUser));
        }
        viewModel.GetParameters();
    }


    $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    //$("#aspnetForm").validate({onsubmit: false});
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });
    // ace file upload
    $('#document-path').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        thumbnail: false, //| true | large
        blacklist: 'exe|dll'
    });
    // Knockout custom file handler
    ko.bindingHandlers.file = {
        init: function (element, valueAccessor) {
            $(element).change(function () {
                var file = this.files[0];

                if (ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };
    // Dependant Observable for dropdown auto fill
    ko.dependentObservable(function () {

        var uploadType = ko.utils.arrayFirst(viewModel.Parameter().UploadTypes(), function (item) {
            return item.ID == viewModel.Selected().UploadTypes();
        });

        if (uploadType != null) viewModel.UploadUT().UploadType(uploadType);
    });
    ko.applyBindings(new ViewModel());
});