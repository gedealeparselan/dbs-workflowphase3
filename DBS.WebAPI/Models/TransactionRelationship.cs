﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{

    #region Property
    [ModelName("TransactionRelationship")]
    public class TransactionRelationshipModel
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }        
    }

    public class TransactionRelationshipRow : TransactionRelationshipModel
    {
        public int RowID { get; set; }

    }

    public class TransactionRelationshipGrid : Grid
    {
        public IList<TransactionRelationshipRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class TransactionRelationshipFilter : Filter { }
    #endregion

    #region Interface
    public interface ITransactionRelationshipRepository : IDisposable
    {
        bool GetTransactionRelationshipByID(int id, ref TransactionRelationshipModel TransactionRelationshipModel, ref string message);
        bool GetTransactionRelationship(ref IList<TransactionRelationshipModel> TransactionRelationshipModel, int limit, int index, ref string message);
        bool GetTransactionRelationship(TransactionRelationshipFilter filter, ref IList<TransactionRelationshipModel> TransactionRelationshipModel, ref string message);
        bool GetTransactionRelationship(ref IList<TransactionRelationshipModel> TransactionRelationshipModel, ref string message);
        bool GetTransactionRelationship(string key, int limit, ref IList<TransactionRelationshipModel> TransactionRelationshipModel, ref string message);
        bool GetTransactionRelationship(int page, int size, IList<TransactionRelationshipFilter> filters, string sortColumn, string sortOrder, ref TransactionRelationshipGrid TransactionRelationshipGrid, ref string message);
        bool AddTransactionRelationship(TransactionRelationshipModel TransactionRelationshipModel, ref string message);
        bool UpdateTransactionRelationship(int id, TransactionRelationshipModel TransactionRelationshipModel, ref string message);
        bool DeleteTransactionRelationship(int id, ref string message);
    }
    #endregion

    #region Repository
    public class TransactionRelationshipRepository : ITransactionRelationshipRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetTransactionRelationshipByID(int id, ref TransactionRelationshipModel TransactionRelationshipModel, ref string message)
        {
            bool isSuccess = false;
            try
            {
                TransactionRelationshipModel = (from a in context.TransactionRelationships
                                           where a.IsDeleted == false && a.TransactionRelationshipID == id
                                           select new TransactionRelationshipModel
                                           {
                                               ID = a.TransactionRelationshipID,
                                               Code = a.TransactionRelationshipCode,
                                               Description = a.Description,
                                               CreateDate = a.CreateDate,
                                               CreateBy = a.CreateBy,
                                               LastModifiedDate = a.UpdateDate,
                                               LastModifiedBy = a.UpdateBy, 
                                           }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetTransactionRelationship(ref IList<TransactionRelationshipModel> TransactionRelationshipModel, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    TransactionRelationshipModel = (from a in context.TransactionRelationships
                                               where a.IsDeleted == false
                                               orderby new { a.TransactionRelationshipCode }
                                               select new TransactionRelationshipModel
                                               {
                                                   ID = a.TransactionRelationshipID,
                                                   Code = a.TransactionRelationshipCode,
                                                   Description = a.Description,
                                                   CreateDate = a.CreateDate,
                                                   CreateBy = a.CreateBy,
                                                   LastModifiedDate = a.UpdateDate,
                                                   LastModifiedBy = a.UpdateBy,  
                                               }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetTransactionRelationship(ref IList<TransactionRelationshipModel> TransactionRelationshipModel, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    TransactionRelationshipModel = (from a in context.TransactionRelationships
                                               where a.IsDeleted == false
                                               orderby new { a.TransactionRelationshipCode }
                                               select new TransactionRelationshipModel
                                               {
                                                   ID = a.TransactionRelationshipID,
                                                   Code = a.TransactionRelationshipCode,
                                                   Description = a.Description,
                                                   CreateDate = a.CreateDate,
                                                   CreateBy = a.CreateBy,
                                                   LastModifiedDate = a.UpdateDate,
                                                   LastModifiedBy = a.UpdateBy,                                                 
                                               }
                                         ).ToList();
                    TransactionRelationshipModel = TransactionRelationshipModel.GroupBy(p => p.Code).Select(g => g.First()).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetTransactionRelationship(TransactionRelationshipFilter filter, ref IList<TransactionRelationshipModel> TransactionRelationshipModel, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetTransactionRelationship(string key, int limit, ref IList<TransactionRelationshipModel> TransactionRelationshipModel, ref string message)
        {
            bool isSuccess = false;

            try
            {
                TransactionRelationshipModel = (from a in context.TransactionRelationships
                                           where a.IsDeleted == false &&
                                           a.TransactionRelationshipCode.Contains(key)                                       
                                           orderby new { a.TransactionRelationshipCode }
                                           select new TransactionRelationshipModel
                                           {
                                               ID = a.TransactionRelationshipID,
                                               Code = a.TransactionRelationshipCode,
                                               Description = a.Description,
                                               CreateDate = a.CreateDate,
                                               CreateBy = a.CreateBy,
                                               LastModifiedDate = a.UpdateDate,
                                               LastModifiedBy = a.UpdateBy,    
                                           }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        
        public bool GetTransactionRelationship(int page, int size, IList<TransactionRelationshipFilter> filters, string sortColumn, string sortOrder, ref TransactionRelationshipGrid TransactionRelationshipGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                TransactionRelationshipModel filter = new TransactionRelationshipModel();
                if (filters != null)
                {
                    filter.Code = (string)filters.Where(a => a.Field.Equals("Code")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.TransactionRelationships
                                let isTransactionRelationshipCode = !string.IsNullOrEmpty(filter.Code)
                                let isTransactionRelationshipDescription = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted == false &&
                                    (isTransactionRelationshipCode ? a.TransactionRelationshipCode.Contains(filter.Code) : true) &&
                                    (isTransactionRelationshipDescription ? a.Description.Contains(filter.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new RejectCodeModel
                                {
                                    ID = a.TransactionRelationshipID,
                                    Code = a.TransactionRelationshipCode,
                                    Description = a.Description,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    TransactionRelationshipGrid.Page = page;
                    TransactionRelationshipGrid.Size = size;
                    TransactionRelationshipGrid.Total = data.Count();
                    TransactionRelationshipGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    TransactionRelationshipGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new TransactionRelationshipRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Code = a.Code,
                            Description = a.Description,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddTransactionRelationship(TransactionRelationshipModel TransactionRelationshipModel, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.TransactionRelationships.Where(a => a.TransactionRelationshipCode.Trim() == TransactionRelationshipModel.Code.Trim() && a.IsDeleted == false).Any())
                {
                    context.TransactionRelationships.Add(new Entity.TransactionRelationship()
                    {
                        TransactionRelationshipCode = TransactionRelationshipModel.Code,
                        Description = TransactionRelationshipModel.Description,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Transaction Relationship data for Transaction Relationship Code {0} already exist.", TransactionRelationshipModel.Code);
                }
            }
            catch(Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateTransactionRelationship(int id, TransactionRelationshipModel TransactionRelationshipModel, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.TransactionRelationship data = context.TransactionRelationships.Where(a => a.TransactionRelationshipID == id).SingleOrDefault();

                if (data != null)
                {
                    if (context.TransactionRelationships.Where(a => a.TransactionRelationshipID != TransactionRelationshipModel.ID && a.TransactionRelationshipCode == TransactionRelationshipModel.Code && a.IsDeleted == false).Count() >= 1)
                    {
                        message = string.Format("Transaction Relationship {0} is exist.", TransactionRelationshipModel.Code);
                    }
                    else
                    {
                        data.TransactionRelationshipCode = TransactionRelationshipModel.Code;
                        data.Description = TransactionRelationshipModel.Description;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Transaction Relationship with id {0} does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteTransactionRelationship(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.TransactionRelationship data = context.TransactionRelationships.Where(a => a.TransactionRelationshipID == id).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Transaction Relationship data for Transaction Relationship ID {0} does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion

}