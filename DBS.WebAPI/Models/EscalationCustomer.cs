﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Net.Http;
using System.Web.Script.Serialization;
using System.Transactions;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("EscalationCustomer")]
    public class EscalationCustomerModel
    {
         /// <summary>
        /// EcalationCustomer ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// EcalationCustomer Name.
        /// </summary>        
        public string CIF { get; set; }

        /// <summary>
        /// EcalationCustomer.
        /// </summary> 
        public string Name { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }    
    public class EscalationCustomerRow : EscalationCustomerModel
    {
        public int RowID { get; set; }

    }

    public class EscalationCustomerGrid : Grid
    {
        public IList<EscalationCustomerRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class EscalationCustomerFilter : Filter { }
    #endregion 

    #region Interface
    public interface IEscalationCustomerRepository : IDisposable
    {
        bool GetEscalationCustomer(int page, int size, IList<EscalationCustomerFilter> filters, string sortColumn, string sortOrder, ref EscalationCustomerGrid escalationCustomersGrid, ref string message);
        bool AddEscalationCustomer(IList<EscalationCustomerModel> escalationCustomers, ref string message);

        //bool GetEscalationCustomerID(int id, ref EscalationCustomerModel escalationCustomers, ref string message);
        //bool GetEscalationCustomer(ref IList<EscalationCustomerModel> escalationCustomers, int limit, int index, ref string message);
        //bool GetEscalationCustomer(int page, int size, IList<EscalationCustomerFilter> filters, string sortColumn, string sortOrder, ref EscalationCustomerGrid ageingReportGrid, ref string message);
        //bool GetEscalationCustomer(EscalationCustomerFilter filter, ref IList<EscalationCustomerModel> escalationCustomers, ref string mssage);
        //bool GetEscalationCustomer(string key, int limit, ref IList<EscalationCustomerModel> escalationCustomers, ref string message);        

    }
    #endregion

    #region Repository
    public class EscalationCustomerRepository:IEscalationCustomerRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetEscalationCustomer(int page, int size, IList<EscalationCustomerFilter> filters, string sortColumn, string sortOrder, ref EscalationCustomerGrid escalationCustomersGrid, ref string message)
        {
            bool isSuccess = false;
            bool typeCustomer = true;
            
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                EscalationCustomerModel filter = new EscalationCustomerModel();                
                if (filters != null)
                {
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("CIFEscalatedCustomer")).Select(a => a.Value).SingleOrDefault();
                    filter.Name = (string)filters.Where(a => a.Field.Equals("CustomerNameEscalatedCustomer")).Select(a => a.Value).SingleOrDefault();

                }
                string CIF = filter.CIF;
                string CustomerName = filter.Name;
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.SP_GetEscalationCustomer(CIF, CustomerName, typeCustomer)                                
                                select new EscalationCustomerModel
                                {
                                    CIF = a.CIF,
                                    Name = a.CustomerName                                   
                                }).ToList();

                    escalationCustomersGrid.Page = page;
                    escalationCustomersGrid.Size = size;
                    escalationCustomersGrid.Total = data.Count();
                    escalationCustomersGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    escalationCustomersGrid.Rows = data.AsQueryable().OrderBy(orderBy)
                        .Select((a, i) => new EscalationCustomerRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            CIF = a.CIF,
                            Name = a.Name,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
                        
        public bool AddEscalationCustomer(IList<EscalationCustomerModel> escalationCustomers, ref string message)
        {
            bool isSuccess = false;
            using(DBSEntities context = new DBSEntities())
            {
                if (escalationCustomers != null) 
                {
                    using(TransactionScope transScope = new TransactionScope())
                    {
                        try
                        {
                            var escalatedcustomer = context.Escalations.ToList();
                            if (escalatedcustomer != null && escalatedcustomer.Count > 0)
                            {
                                escalatedcustomer.ForEach(a =>
                                {
                                    a.IsDeleted = true;
                                });
                            }
                            foreach(var data in escalationCustomers)
                            {
                                context.Escalations.Add(new Entity.Escalation()
                                {
                                    CIF = data.CIF,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });
                                context.SaveChanges();
                            }
                            transScope.Complete();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            transScope.Dispose();
                            message = ex.Message;
                        }
                    }
                }
            }
            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                        context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    
    #endregion
}