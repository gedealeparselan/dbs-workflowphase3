﻿var accessToken;
var $box;
var $remove = false;
var DocumentModels = ko.observable({ FileName: '', DocumentPath: '' });

var cifData = '0';
var DealID = '0';
var idrrate = 0;
var fromName = '';
var CurrentAmount = 0;
var FXModel = { cif: cifData, token: accessToken, currentAmount: CurrentAmount }
var AmountModel = {
    TotalPPUAmountsUSD: ko.observable(0),
    TotalDealAmountsUSD: ko.observable(0),
    RemainingBalance: ko.observable(0),
    TotalPPUUtilization: ko.observable(0),
    TotalDealUtilization: ko.observable(0)
}

/*var AmountModel = {
 TotalAmounts: ko.observable(0),
 TotalAmountsStatementB: ko.observable(0),
 TotalDealAmountsUSD: ko.observable(0),
 TreshHold: ko.observable(0),
 TotalRemainigByCIF: ko.observable(0),
 TempAmount: ko.observable(0),
 RemainingBalance: ko.observable(0),
 FCYIDRTreshold : ko.observable(0)
 } */



/* dodit@2014.11.08:add format function */
var formatNumber = function (num) {
    if (num != null) {
        //num = num.toString().replace(/,/g, '');
        //num = parseFloat(num).toFixed(2);
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    } else { return 0; }
}

var formatNumber_r = function (num) {
    if (num != null) {
        num = num.toString().replace(/,/g, '');
        num = parseFloat(num).toFixed(2);
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    } else { return 0; }
}

var resetNumber = function (num) {
    if (num != null) {
        num = num.toString().replace(/,/g, '');
        return parseFloat(num).toFixed(2);
    } else { return 0; }
}

var formatAccount = function (num) {

    num = num.replace(/-/g, '');

    sheet_a = (num.length > 0) ? num.substr(0, 4) : '';
    sheet_b = (num.length > 4) ? '-' + num.substr(4, 4) : '';
    sheet_c = (num.length > 8) ? '-' + num.substr(8) : '';

    return sheet_a + sheet_b + sheet_c;
}

var formatMakerAccount = function (task) {
    //Transaction.BeneAccNumber
    var num = document.getElementById("bene-acc-number").value;
    num = num.replace(/-/g, '');

    switch (task) {
        case "PaymentMaker":
            viewModel.PaymentMaker().Payment.BeneAccNumber = num;
            break;
        case "TransactionMaker":
            viewModel.TransactionMaker().Transaction.BeneAccNumber = num;
            break;
        case "TransactionMakerAfterCaller":
            viewModel.TransactionMaker().Transaction.BeneAccNumber = num;
            break;
        case "TransactionCaller":
            viewModel.TransactionCallback().Transaction.BeneAccNumber = num;
            break;
    }

    sheet_a = (num.length > 0) ? num.substr(0, 4) : '';
    sheet_b = (num.length > 4) ? '-' + num.substr(4, 4) : '';
    sheet_c = (num.length > 8) ? '-' + num.substr(8) : '';

    viewModel.BeneAccNumberMask(sheet_a + sheet_b + sheet_c);
}

var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};

var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};


var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};

var TZModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable(),
    Class: ko.observable()
}

var BizSegmentModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var BranchModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var TypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ProductModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    WorkflowID: ko.observable(),
    Workflow: ko.observable()
};

var ProductTypeModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    //IsFlowValas:ko.observable(),
    Description: ko.observable()
};

var LLDModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var ChannelModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var BankModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    BranchCode: ko.observable(),
    SwiftCode: ko.observable(),
    BankAccount: ko.observable(),
    Description: ko.observable(),
    CommonName: ko.observable(),
    Currency: ko.observable(),
    PGSL: ko.observable()
};

var RMModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    RankID: ko.observable(),
    Rank: ko.observable(),
    SegmentID: ko.observable(),
    Segment: ko.observable(),
    BranchID: ko.observable(),
    Branch: ko.observable(),
    Email: ko.observable()
};

var ContactModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    PhoneNumber: ko.observable(),
    DateOfBirth: ko.observable(),
    Address: ko.observable(),
    IDNumber: ko.observable()
};

var FunctionModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var CurrencyModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var DealUnderlyingModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var StatementLetterModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var RateTypeModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var AccountModel = {
    AccountNumber: ko.observable(),
    //Currency: ko.observable(CurrencyModel)
};

var UnderlyingDocumentModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable()
};

var DocumentTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var FXComplianceModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ChargesModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var DocumentPurposeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var UnderlyingModel = {
    ID: ko.observable(),
    UnderlyingDocument: ko.observable(UnderlyingDocumentModel),
    DocumentType: ko.observable(DocumentTypeModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    AttachmentNo: ko.observable(),
    IsStatementLetter: ko.observable(false),
    IsDeclarationOfException: ko.observable(false),
    StartDate: ko.observable(),
    EndDate: ko.observable(),
    DateOfUnderlying: ko.observable(),
    ExpiredDate: ko.observable(),
    ReferenceNumber: ko.observable(),
    SupplierName: ko.observable(),
    InvoiceNumber: ko.observable()
};

var DocumentModel = {
    ID: ko.observable(),
    Type: ko.observable(DocumentTypeModel),
    Purpose: ko.observable(DocumentPurposeModel),
    DocumentPath: ko.observable(),
    IsNewDocument: ko.observable(false),
    LastModifiedDate: ko.observable(new Date),
    LastModifiedBy: ko.observable(),
    IsDormant: ko.observable(false)
};

var CustomerModel = {
    CIF: ko.observable(),
    Name: ko.observable(),
    POAName: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Branch: ko.observable(BranchModel),
    Type: ko.observable(TypeModel),
    RM: ko.observable(RMModel),
    Contacts: ko.observableArray([ContactModel]),
    Functions: ko.observableArray([FunctionModel]),
    Accounts: ko.observableArray([AccountModel]),
    Underlyings: ko.observableArray([UnderlyingModel])
};

var TransactionModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    IsNewCustomer: ko.observable(false),
    Product: ko.observable(ProductModel),
    ProductType: ko.observable(ProductTypeModel),
    DebitCurrency: ko.observable(CurrencyModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    Channel: ko.observable(ChannelModel),
    Account: ko.observable(AccountModel),
    ApplicationDate: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Bank: ko.observable(BankModel),
    IsSignatureVerified: ko.observable(false),
    IsDormantAccount: ko.observable(false),
    IsFreezeAccount: ko.observable(false),
    Others: ko.observable(),
    IsDraft: ko.observable(false),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]), //DocumentModel
    DetailCompliance: ko.observable(),
    BeneName: ko.observable(),
    TZNumber: ko.observable(),
    LLD: ko.observable(LLDModel),
    IsOtherBeneBank: ko.observable(false),
    OtherBeneBankName: ko.observable(),
    OtherBeneBankSwift: ko.observable(),
    IsDocumentComplete: ko.observable(false),
    IsOtherAccountNumber: ko.observable(false),
    OtherAccountNumber: ko.observable(),

};

var PaymentModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    IsNewCustomer: ko.observable(false),
    Product: ko.observable(ProductModel),
    ProductType: ko.observable(ProductTypeModel),
    Currency: ko.observable(CurrencyModel),
    DebitCurrency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    Channel: ko.observable(ChannelModel),
    Account: ko.observable(AccountModel),
    ApplicationDate: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Bank: ko.observable(BankModel),
    BankCharges: ko.observable(ChargesModel),
    AgentCharges: ko.observable(ChargesModel),
    Type: ko.observable(),
    IsCitizen: ko.observable(false),
    IsResident: ko.observable(false),
    /*LLDCode: ko.observable(),
     LLDInfo: ko.observable(),*/
    PaymentDetails: ko.observable(),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]), //DocumentModel
    DetailCompliance: ko.observable(),
    BeneName: ko.observable(),
    TZNumber: ko.observable(),
    FXCompliance: ko.observable(FXComplianceModel),
    LLD: ko.observable(LLDModel),
    IsOtherBeneBank: ko.observable(false)

};

var CheckerModel = {
    ID: ko.observable(0),
    LLD: ko.observable(LLDModel)
    /*LLDCode: ko.observable(""),
     LLDInfo: ko.observable("")*/
};

//bangkit 20141119
var CallBackModel = {
    ID: ko.observable(0),
    ApproverID: ko.observable(""),
    Contact: ko.observable(ContactModel),
    Time: ko.observable(new Date),
    Remark: ko.observable("")
};
//Chandra 2015.02.24
var DocumentPurposeModel = {
    ID: ko.observable(0),
    Name: ko.observable(""),
    Description: ko.observable("")
}


var VerifyModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    IsVerified: ko.observable()
};

var TimelineModel = {
    ApproverID: ko.observable(),
    Activity: ko.observable(),
    Time: ko.observable(),
    Outcome: ko.observable(),
    UserOrGroup: ko.observable(),
    IsGroup: ko.observable(),
    DisplayName: ko.observable(),
    Comment: ko.observable()
};

var TransactionDealModel = function () {
    this.Transaction = ko.observable(TransactionDealDetailModel);
    this.Timelines = ko.observableArray([TimelineModel]);
    this.Verify = ko.observableArray([VerifyModel]);
};

var TransactionMakerModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var TransactionCheckerModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var TransactionCheckerCallbackModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    this.Calback = ko.observable();
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

//bangkit 20141119
var TransactionCallbackModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    //this.Checker = ko.observable(CheckerModel);
    this.Callbacks = ko.observableArray([CallBackModel]);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var TransactionContactModel = function () {
    this.Contacts = ko.observableArray([ContactModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var PaymentMakerModel = function () {
    this.Payment = ko.observable(PaymentModel);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var PaymentCheckerModel = function () {
    this.Payment = ko.observable(PaymentModel);
    this.Timelines = ko.observableArray([TimelineModel]);
    this.Verify = ko.observableArray([VerifyModel]);
};

/*var TransactionCheckerModel = function () {
 this.Transaction = ko.observable(TransactionDealDetailModel);
 this.Checker = ko.observable(CheckerModel);
 this.Verify = ko.observableArray([VerifyModel]);
 this.Timelines = ko.observableArray([TimelineModel]);
 };*/

var Documents = {
    Documents: self.Documents,
    DocumentPath: self.DocumentPath_a,
    Type: self.DocumentType_a,
    Purpose: self.DocumentPurpose_a
}


var CustomerUnderlyingMappingModel = {
    ID: ko.observable(0),
    UnderlyingID: ko.observable(0),
    UnderlyingFileID: ko.observable(0),
    AttachmentNo: ko.observable("")
}

var ProductTypeModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    IsFlowValas: ko.observable(),
    Description: ko.observable()
};

var SelectUtillizeModel = function (id, enable, uSDAmount, statementLetter) {
    var self = this;
    self.ID = ko.observable(id);
    self.Enable = ko.observable(enable);
    self.USDAmount = ko.observable(uSDAmount);
    self.StatementLetter = ko.observable(statementLetter);
};

var TransactionDealDetailModel = {
    ValueDate: ko.observable(),
    AccountNumber: ko.observable(),
    StatementLetter: ko.observable(StatementLetterModel),
    ProductType: ko.observable(ProductTypeModel),
    TZNumber: ko.observable(),
    RateTypeID: ko.observable(),
    Rate: ko.observable(),
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Currency: ko.observable(CurrencyModel),
    DebitCurrency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    AmountUSD: ko.observable(),
    Account: ko.observable(AccountModel),
    ValueDate: ko.observable(),
    Type: ko.observable(20),
    CreateDate: ko.observable(new Date),
    //CreateBy: ko.observable(spUser.DisplayName),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]), //DocumentModel
    DetailCompliance: ko.observable(),
    BeneName: ko.observable(),
    BeneAccNumber: ko.observable(),
    TZRef: ko.observable(),
    utilizationAmount: ko.observable(0.00),
    underlyings: ko.observableArray([]),
    TZReference: ko.observable(),
    IsBookDeal: ko.observable(false),
    bookunderlyingamount: ko.observable(),
    bookunderlyingcurrency: ko.observable(),
    bookunderlyingcode: ko.observable(),
    CreateBy: ko.observable(),
    AccountCurrencyCode: ko.observable(),
    IsFxTransaction: ko.observable(false),
    OtherUnderlying: ko.observable(),
    bookunderlyingdoccode: ko.observable()

    //TZRef: ko.observable()
    //UnderlyingGridProperties : self.UnderlyingGridProperties(new GridPropertiesModel(GetDataUnderlying))

};

var SelectedModel = {
    Product: ko.observable(),
    Currency: ko.observable(),
    Channel: ko.observable(),
    Account: ko.observable(),
    BizSegment: ko.observable(),
    Bank: ko.observable(),
    DocumentType: ko.observable(),
    DocumentPurpose: ko.observable(),
    NewCustomer: ko.observable({
        Currency: ko.observable(),
        BizSegment: ko.observable()
    }),
    BankCharges: ko.observable(2),
    AgentCharges: ko.observable(2),
    FXCompliaance: ko.observable(),
    LLD: ko.observable(),
    POAFunction: ko.observable(),
    ProductType: ko.observable(),
    UnderlyingDoc: ko.observable(),
    DocumentPurpose_a: ko.observable(),
    DocumentType_a: ko.observable(),
    DebitCurrency: ko.observable()
};

var statusValueModel = function (value, status) {
    var self = this;
    self.Value = ko.observable(value);
    self.Status = ko.observable(status);
}

// Original Value PPU Maker -- chandra a

var OriginalValueModel = function (amountOri, amountUSDOri, rateOri, beneNameOri, banksOri, beneAccNumberOri, swiftCodeOri, bankChargesOri, agentChargesOri, isCitizenOri, isResidentOri) {
    var self = this;
    self.AmountOri = ko.observable(amountOri);
    self.AmountUSDOri = ko.observable(amountUSDOri);
    self.RateOri = ko.observable(rateOri);
    self.BeneNameOri = ko.observable(beneNameOri);
    self.BanksOri = ko.observable(banksOri);
    self.BeneAccNumberOri = ko.observable(beneAccNumberOri);
    //self.AccountOri = ko.observable(accountOri);
    self.SwiftCodeOri = ko.observable(swiftCodeOri);
    self.BankChargesOri = ko.observable(bankChargesOri);
    self.AgentChargesOri = ko.observable(agentChargesOri);
    self.IsCitizenOri = ko.observable(isCitizenOri);
    self.IsResidentOri = ko.observable(isResidentOri);
}

var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;

    // Sharepoint User
    self.SPUser = ko.observable();

    // rate param
    self.Rate = ko.observable(0);
    self.Amount = ko.observable(0);
    self.BeneAccNumberMask = ko.observable();
    self.AmountUSD_r = ko.observable(0);

    //check tz number exist
    self.IsSearchTZ = ko.observable(false);
    self.TZModel = ko.observable(TZModel);
    // count of document
    self.CountDoc = 0;
    // Workflow task config
    self.WorkflowConfig = ko.observable();

    //aailable timelines tab
    self.IsTimelines = ko.observable(false);

    // utilize amount from underlying
    self.DealUnderlying = ko.observable(DealUnderlyingModel);
    self.UtilizationAmounts = ko.observable(0);

    //other account number selected
    self.IsOtherAccountNumber = ko.observable(false);
    // counter upload file ppu maker
    self.counterUpload = ko.observable(0);
    //underlying declare
    self.AmountModel = ko.observable(AmountModel);
    //file attachment edit
    self.SelectingUnderlying = ko.observable(true);
    //is fx transaction
    self.t_IsFxTransaction = ko.observable(false);
    self.t_IsFxTransactionToIDR = ko.observable(false);
    self.t_IsFxTransactionToIDRB = ko.observable(false);

    self.IsTZCanceled = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerAttachUnderlyings = ko.observableArray([]);

    // parameters
    self.Products = ko.observableArray([]);
    self.Currencies = ko.observableArray([]);
    self.DebitCurrencies = ko.observableArray([]);
    self.Channels = ko.observableArray([]);
    self.BizSegments = ko.observableArray([]);
    self.ProductTypes = ko.observableArray([]);
    self.LLDs = ko.observableArray([]);
    self.Banks = ko.observableArray([]);
    self.BankCharges = ko.observableArray([]);
    self.AgentCharges = ko.observableArray([]);
    self.FXCompliances = ko.observableArray([]);
    self.UnderlyingDocs = ko.observableArray([]);

    self.isNewUnderlying = ko.observable(false);
    self.IsOtherBank = ko.observable(false);

    self.POAFunctions = ko.observableArray([]);
    self.DocumentTypes = ko.observableArray([]);
    self.DocumentPurposes = ko.observableArray([]);

    // task properties
    self.ApproverId = ko.observable();
    self.SPWebID = ko.observable();
    self.SPSiteID = ko.observable();
    self.SPListID = ko.observable();
    self.SPListItemID = ko.observable();
    self.SPTaskListID = ko.observable();
    self.SPTaskListItemID = ko.observable();
    self.Initiator = ko.observable();
    self.WorkflowInstanceID = ko.observable();
    self.WorkflowID = ko.observable();
    self.WorkflowName = ko.observable();
    self.StartTime = ko.observable();
    self.StateID = ko.observable();
    self.StateDescription = ko.observable();
    self.IsAuthorized = ko.observable();

    self.TaskTypeID = ko.observable();
    self.TaskTypeDescription = ko.observable();
    self.Title = ko.observable();
    self.ActivityTitle = ko.observable();
    self.User = ko.observable();
    self.IsSPGroup = ko.observable();
    self.EntryTime = ko.observable();
    self.ExitTime = ko.observable();
    self.OutcomeID = ko.observable();
    self.OutcomeDescription = ko.observable();
    self.CustomOutcomeID = ko.observable();
    self.CustomOutcomeDescription = ko.observable();
    self.Comments = ko.observable();

    // value CIF underlying
    self.Treshold = ko.observable(0);
    self.FCYIDRTreshold = ko.observable(0);
    self.TotalRemainigByCIF = ko.observable(0);
    self.TotalTransFX = ko.observable(0);
    self.TotalUtilization = ko.observable(0);
    // Transaction Details
    self.TransactionMaker = ko.observable(TransactionMakerModel);
    self.TransactionChecker = ko.observable(TransactionCheckerModel);
    self.TransactionCheckerCallback = ko.observable(TransactionCheckerCallbackModel);
    self.TransactionContact = ko.observable(TransactionContactModel);
    self.TransactionCallback = ko.observable(TransactionCallbackModel);
    self.PaymentMaker = ko.observable(PaymentMakerModel);
    self.PaymentChecker = ko.observable(PaymentCheckerModel);
    self.TransactionDeal = ko.observable(TransactionDealModel);

    // filter Attach Underlying File
    self.AttachFilterFileName = ko.observable("");
    self.AttachFilterDocumentPurpose = ko.observable("");
    self.AttachFilterModifiedDate = ko.observable("");
    self.AttachFilterDocumentRefNumber = ko.observable("");

    self.UnderlyingFilterCIF = ko.observable("");
    self.UnderlyingFilterParentID = ko.observable("");
    self.UnderlyingFilterIsSelected = ko.observable(false);
    self.UnderlyingFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingFilterDocumentType = ko.observable("");
    self.UnderlyingFilterCurrency = ko.observable("");
    self.UnderlyingFilterStatementLetter = ko.observable("");
    self.UnderlyingFilterAmount = ko.observable("");
    self.UnderlyingFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingFilterExpiredDate = ko.observable("");
    self.UnderlyingFilterReferenceNumber = ko.observable("");
    self.UnderlyingFilterSupplierName = ko.observable("");
    self.UnderlyingFilterIsProforma = ko.observable("");

    // Task status from Nintex custom REST API
    self.IsPendingNintex = ko.observable();
    self.IsAuthorizedNintex = ko.observable();
    self.MessageNintex = ko.observable();

    // Task activity properties
    self.PPUChecker = ko.observable();

    //// start Checked Calculate for Attach Grid and Underlying Grid
    self.TempSelectedAttachUnderlying = ko.observableArray([]);
    //// strat Checker Calculate for Underlying Grid
    self.TempSelectedUnderlying = ko.observableArray([]);

    self.IsUploading = ko.observable(false);
    //self.PPUChecker(new PPUCheckerModel());
    //alert(self.PPUChecker().data());

    //is fx transaction
    self.t_IsFxTransaction = ko.observable(false);
    self.t_IsFxTransactionToIDR = ko.observable(false);
    self.t_IsFxTransactionToIDRB = ko.observable(false);
    // add : chandra 2015.03.23
    self.UtilizationAmount = ko.observable(0);

    //properties
    self.LLDCode = ko.observable();
    // New Data flag
    self.IsNewData = ko.observable(false);

    // check all data
    self.CheckAll = ko.observable(false);
    // temp payment maker confirm
    self.BeneBanktmp = ko.observable('');
    self.SwiftCodetmp = ko.observable('');
    self.AccNumbertmp = ko.observable('');

    self.IsResidenttmp = ko.observable('');
    self.IsCitizentmp = ko.observable('');

    self.agentChargestmp = ko.observable('');
    self.bankChargestmp = ko.observable('');

    self.CargerBankOrig = ko.observable('');
    self.AgentChargesOrig = ko.observable('');

    // customer contact
    self.ID_c = ko.observable();
    self.CIF_c = ko.observable("");
    self.SourceID_c = ko.observable("2");
    self.Name_c = ko.observable("");
    self.PhoneNumber_c = ko.observable("");
    self.DateOfBirth_c = ko.observable('');
    self.Address_c = ko.observable("");
    self.IDNumber_c = ko.observable("");
    self.POAFunction_c = ko.observable();
    self.OccupationInID_c = ko.observable("");
    self.PlaceOfBirth_c = ko.observable("");
    self.EffectiveDate_c = ko.observable("");
    self.CancellationDate_c = ko.observable("");
    self.POAFunctionOther_c = ko.observable("");
    self.LastModifiedBy_c = ko.observable("");
    self.LastModifiedDate_c = ko.observable('');

    // customer underlying
    self.ID_u = ko.observable("");
    self.UnderlyingDocument_u = ko.observable('');
    self.DocumentType_u = ko.observable('');
    self.AmountUSD_u = ko.observable(0);
    self.Currency_u = ko.observable('');
    self.Rate_u = ko.observable(0);
    self.StatementLetter_u = ko.observable('');
    self.Amount_u = ko.observable(0);
    self.AttachmentNo_u = ko.observable("");
    self.IsDeclarationOfException_u = ko.observable(false);
    self.StartDate_u = ko.observable('');
    self.EndDate_u = ko.observable('');
    self.DateOfUnderlying_u = ko.observable("");
    self.ExpiredDate_u = ko.observable("");
    self.ReferenceNumber_u = ko.observable("");
    self.SupplierName_u = ko.observable("");
    self.InvoiceNumber_u = ko.observable("");
    self.IsProforma_u = ko.observable(false);
    self.IsBulkUnderlying_u = ko.observable(false);
    self.Proformas = ko.observableArray([]);
    self.BulkUnderlyings = ko.observableArray([]);
    self.ProformaDetails = ko.observableArray([]);
    self.BulkDetails = ko.observableArray([]);

    // Temporary documents
    self.Documents = ko.observableArray([]);
    self.DocumentPath = ko.observable();
    self.DocumentType = ko.observable();
    self.DocumentPurpose = ko.observable();
    self.DocumentFileName = ko.observable();

    self.CurrencyCode = ko.observable("");
    self.CurrencyAccCode = ko.observable("");

    // Original value chandra
    self.OriginalValue = ko.observable((new OriginalValueModel(new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false)

    )));

    // Chandra 2015.02.24 attachment properties for maker checker
    // Attach Variable
    self.ID_a = ko.observable(0);
    self.ddlDocumentPurpose_a = ko.observableArray([]);
    self.tempddlDocumentPurpose_a = ko.observableArray([]);
    self.ddlDocumentType_a = ko.observableArray([]);
    self.DocumentPurpose_a = ko.observable();
    self.DocumentPath_a = ko.observable();
    self.DocumentType_a = ko.observable();
    self.CustomerUnderlyingMappings_a = ko.observableArray([CustomerUnderlyingMappingModel]);
    self.MakerDocuments = ko.observableArray([]);
    self.MakerAllDocumentsTemp = ko.observableArray([]);
    self.MakerUnderlyings = ko.observableArray([]);
    //self.CustomerUnderlyings
    self.NewDocuments = ko.observableArray([]);
    /* self.ID_a = ko.observable(0);
     self.FileName_a = ko.observable('');
     self.DocumentPurpose_a = ko.observable(DocumentPurposeModel);
     self.ddlDocumentPurpose_a = ko.observableArray([]);
     self.DocumentType_a = ko.observable(DocumentTypeModel);
     self.ddlDocumentType_a = ko.observableArray([]);
     self.DocumentRefNumber_a = ko.observable("");
     self.LastModifiedBy_a = ko.observable("");
     self.LastModifiedDate_a = ko.observable(new Date());
     self.CustomerUnderlyingMappings_a = ko.observableArray([CustomerUnderlyingMappingModel]);
     self.DocumentPath_a = ko.observable(); */

    var AttachDocuments = {
        Documents: self.Documents,
        DocumentPath: self.DocumentPath_a,
        Type: self.DocumentType_a,
        Purpose: self.DocumentPurpose_a
    }

    var CustomerUnderlyingFile = {
        ID: self.ID_a,
        FileName: DocumentModels().FileName,
        DocumentPath: DocumentModels().DocumentPath,
        DocumentPurpose: self.DocumentPurpose_a,
        DocumentType: self.DocumentType_a,
        DocumentRefNumber: self.DocumentRefNumber_a,
        LastModifiedBy: self.LastModifiedBy_a,
        LastModifiedDate: self.LastModifiedDate_a,
        CustomerUnderlyingMappings: self.CustomerUnderlyingMappings_a
    };

    //Bangkit 20141119
    self.Contact = ko.observable();
    self.Contacts = ko.observableArray([]);
    self.CalledContacts = ko.observableArray([]);

    self.OnCloseApproval = function () {
        self.TransactionMaker(TransactionMakerModel);
        self.TransactionChecker(TransactionCheckerModel);
        self.TransactionCheckerCallback(TransactionCheckerCallbackModel);
        self.TransactionContact(TransactionContactModel);
        self.TransactionCallback(TransactionCallbackModel);
        self.PaymentMaker(PaymentMakerModel);
        self.PaymentChecker(PaymentCheckerModel);
        self.TransactionDeal(TransactionDealModel);
        //self.TransactionMaker = ko.observable(TransactionMakerModel);
        //self.TransactionChecker = ko.observable(TransactionCheckerModel);
        //self.TransactionCheckerCallback = ko.observable(TransactionCheckerCallbackModel);
        //self.TransactionContact = ko.observable(TransactionContactModel);
        //self.TransactionCallback = ko.observable(TransactionCallbackModel);
        //self.PaymentMaker = ko.observable(PaymentMakerModel);
        //self.PaymentChecker = ko.observable(PaymentCheckerModel);
        //self.TransactionDeal = ko.observable(TransactionDealModel);
        self.IsOtherAccountNumber(false);
    }

    self.SelectedContacts = function (contact) {
        if (self.TransactionCallback().Callbacks.length == self.UtcAttemp()) {
            ShowNotification("Warning", "You can only tick " + self.UtcAttemp() + " contacts.", "gritter-warning", false);
            //document.getElementById('checkbox' + selectedIndex).checked = false;
            return;
        }

        $("#modal-form-callbacktime").modal('show');

        //set selected contact
        var tanggal = new Date();
        var jam = tanggal.getHours() > 9 ? tanggal.getHours() : '0' + tanggal.getHours();
        var menit = tanggal.getMinutes() > 9 ? tanggal.getMinutes() : '0' + tanggal.getMinutes();
        $('#TimePickerCallBack').val(jam + ':' + menit);
        $('#NameCallback').val(contact.Name);
        $('.bootstrap-timepicker-hour').val("00");
        $('.bootstrap-timepicker-minute').val("00");
        document.getElementById('IsUtc').checked = false;

        $('#backDrop').show();

        $('.time-picker').timepicker({
            defaultTime: "",
            minuteStep: 1,
            showSeconds: false,
            showMeridian: false
        }).next().on(ace.click_event, function () {
            $(this).prev().focus();
        });

        // validation
        $('#aspnetForm').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: true,

            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },

            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                $(e).remove();
            },

            errorPlacement: function (error, element) {
                if (element.is(':checkbox') || element.is(':radio')) {
                    var controls = element.closest('div[class*="col-"]');
                    if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if (element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if (element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            }
        });

        self.Contact(contact);
    }

    self.TempCallBackTime = ko.observableArray([]);

    self.SaveCallBackTime = function () {

        var form = $("#aspnetForm");
        form.validate();

        //if(form.valid()){
        self.TransactionCallback().Callbacks.push({
            ApproverID: 0,
            Contact: self.Contact(),
            ID: 0,
            Remark: "",
            Time: document.getElementById('TimePickerCallBack').value,
            IsUTC: document.getElementById('IsUtc').checked
        });

        self.TempCallBackTime().push({
            ApproverID: 0,
            Contact: self.Contact(),
            ID: 0,
            Remark: "",
            Time: document.getElementById('TimePickerCallBack').value,
            IsUTC: document.getElementById('IsUtc').checked
        });

        self.UpdateTemplateUI("TransactionCaller");

        $("#modal-form-callbacktime").modal('hide');

        $('#backDrop').hide();
        //console.log(self.TransactionCallback().Callbacks);
        //}
    }

    self.CloseCallBackTime = function () {
        $("#modal-form-callbacktime").modal('hide');

        $('#backDrop').hide();
    }

    self.TimeFormat = function (data) {
        if (data.length > 5) {
            var tanggal = new Date(data);
            return tanggal.format('HH:mm');
            //var jam = tanggal.getHours() > 9 ? tanggal.getHours() : '0' + tanggal.getHours();
            //var menit = tanggal.getMinutes() > 9 ? tanggal.getMinutes() : '0' + tanggal.getMinutes();
            /* var jam = tanggal.getUTCHours() > 9 ? tanggal.getUTCHours()  : '0' + tanggal.getUTCHours();
             var menit = tanggal.getUTCMinutes() > 9 ? tanggal.getUTCMinutes() : '0' + tanggal.getUTCMinutes();
             var result = jam + ':' + menit;
             return result; */
        } else {
            return data;
        }
    }

    self.SearchTZ = function () {
        viewModel.IsSearchTZ(true);
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckAvailbaleTZ/" + viewModel.PaymentMaker().Payment.TZNumber,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                viewModel.TZModel(ko.mapping.toJS(data));
                viewModel.IsSearchTZ(false);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                viewModel.IsSearchTZ(false);
            }
        });
    }

    /*
     self.SelectedContacts = function (contact) {
     //get index selected contact
     var selectedIndex = self.TransactionCallback().Transaction.Customer.Contacts.indexOf(contact);

     //get time now
     var dateNow = new Date();
     var hour = dateNow.getHours();
     var minute = dateNow.getMinutes();
     var timeNow = hour + ":" + minute;

     //find is contact selected?
     var isSelected = ko.utils.arrayFirst(self.Contacts(), function (data) {
     if (data.ID == contact.ID) {
     return true;
     } else {
     return false;
     }
     });

     if (isSelected) {
     //remove selected contact
     self.Contacts.remove(contact);
     self.Contact("");
     } else {
     if (self.Contacts().length == 4) {
     ShowNotification("Warning", "You can only tick " + self.TransactionCallback().Callbacks.length + " contacts.", "gritter-warning", false);
     document.getElementById('checkbox' + selectedIndex).checked = false;
     return;
     }

     $("#modal-form-callbacktime").modal('show');

     $('#backDrop').show();

     //set selected contact
     $('#timepickercallback').val('');
     $('#NameCallback').val(contact.Name);

     $('.time-picker').timepicker({
     defaultTime: "",
     minuteStep: 1,
     showSeconds: false,
     showMeridian: false
     }).next().on(ace.click_event, function () {
     $(this).prev().focus();
     });

     // validation
     $('#aspnetForm').validate({
     errorElement: 'div',
     errorClass: 'help-block',
     focusInvalid: true,

     highlight: function (e) {
     $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
     },

     success: function (e) {
     $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
     $(e).remove();
     },

     errorPlacement: function (error, element) {
     if (element.is(':checkbox') || element.is(':radio')) {
     var controls = element.closest('div[class*="col-"]');
     if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
     else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
     }
     else if (element.is('.select2')) {
     error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
     }
     else if (element.is('.chosen-select')) {
     error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
     }
     else error.insertAfter(element.parent());
     }
     });

     self.Contacts.push(contact);
     self.Contact(contact);
     }

     //re-updating data
     //self.UpdateTemplateUI("TransactionCallback");
     }
     */

    /*self.SelectedContacts = function (contact) {
     //get index selected contact
     var selectedIndex = self.TransactionCallback().Transaction.Customer.Contacts.indexOf(contact);

     //get time now
     var dateNow = new Date();
     var hour = dateNow.getHours();
     var minute = dateNow.getMinutes();
     var timeNow = hour + ":" + minute;

     //find is contact selected?
     var isSelected = ko.utils.arrayFirst(self.Contacts(), function (data) {
     if (data.ID == contact.ID) {
     return true;
     } else {
     return false;
     }
     });

     if (isSelected) {
     //remove selected contact
     self.Contacts.remove(contact);
     self.TransactionCallback().Callbacks[selectedIndex].Contact = null;
     self.TransactionCallback().Callbacks[self.Contacts().length].Time = "00:00";
     document.getElementById('timepicker' + self.Contacts().length).value = "00:00";
     self.Contacts.remove(contact);
     } else {
     if (self.Contacts().length == self.TransactionCallback().Callbacks.length) {
     ShowNotification("Warning", "You can only tick " + self.TransactionCallback().Callbacks.length + " contacts.", "gritter-warning", false);
     document.getElementById('checkbox' + selectedIndex).checked = false;
     return;
     }

     //set selected contact
     self.TransactionCallback().Callbacks[selectedIndex].Contact = contact;
     self.TransactionCallback().Callbacks[self.Contacts().length].Time = timeNow;
     document.getElementById('timepicker' + self.Contacts().length).value = timeNow;
     self.Contacts.push(contact);
     }

     //re-updating data
     //self.UpdateTemplateUI("TransactionCallback");
     }*/

    self.ApprovalButton = function (name) {
        if (name == 'Continue') {
            return 'Submit';
        } else {
            return name;
        }
    }

    // bind clear filters
    self.UnderlyingClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.UnderlyingFilterIsSelected(false);
        self.UnderlyingFilterUnderlyingDocument("");
        self.UnderlyingFilterDocumentType("");
        self.UnderlyingFilterCurrency("");
        self.UnderlyingFilterStatementLetter("");
        self.UnderlyingFilterAmount("");
        self.UnderlyingFilterDateOfUnderlying("");
        self.UnderlyingFilterExpiredDate("");
        self.UnderlyingFilterReferenceNumber("");
        self.UnderlyingFilterSupplierName("");
        self.UnderlyingFilterIsProforma("");

        GetDataUnderlying();
    };

    self.PendingDocumentsClearFilters = function () {
        self.FilterApplicationID("");
        self.FilterSignatureVerified("");
        self.FilterDocument("");
        self.FilterCustomerName("");
        self.FilterProductName("");
        self.FilterCurrencyDesc("");
        self.FilterTransactionAmount("");
        self.FilterEqvUSD("");
        self.FilterFxTransaction("");
        self.FilterActivityTitle("");
        self.FilterLastModifiedBy("");
        self.FilterLastModifiedDate("");

        GetPendingDocuments();
    }

    self.OnRemarkChanges = function () {
        //for (var i = 0; i <= self.TransactionCallback().Callbacks.length - 1; i++) {
        if ((self.TransactionCallback().Callbacks.length > 0) && (self.TransactionCallback().Callbacks[self.TransactionCallback().Callbacks.length - 1].ApproverID == 0)) {
            self.TransactionCallback().Callbacks[self.TransactionCallback().Callbacks.length - 1].Remark = document.getElementById('remarks').value;
            self.UpdateTemplateUI("TransactionCaller");
        }
        //}
    }

    /*self.TransactionChecker().Transaction.LLD.ID = ko.computed({
     read: function () {
     alert('new');
     },
     write: function () {
     }
     });*/

    //init transaction callback ( set data if exist )
    self.initTransactionCallback = function () {
        for (var i = 0; i <= self.TransactionCallback().Callbacks.length - 1; i++) {

            //set grid contact
            var isSelected = ko.utils.arrayFirst(self.TransactionCallback().Transaction.Customer.Contacts, function (data) {

                var indexOfContact = self.TransactionCallback().Transaction.Customer.Contacts.indexOf(self.TransactionCallback().Callbacks[i].Contact);

                if (data.ID == self.TransactionCallback().Callbacks[i].Contact.ID) {
                    document.getElementById('checkbox' + indexOfContact).checked = true;
                } else {
                    document.getElementById('checkbox' + indexOfContact).checked = false;
                }
            });

            //set time picker
            /*if(self.TransactionCallback().Callbacks[i].Time != '00:00'){
             document.getElementById('timepicker' + selectedIndex).value = "00:00";
             }*/
        }
    }

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyings = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyingFiles = ko.observableArray([]);

    // grid properties Underlying
    self.UnderlyingGridProperties = ko.observable();
    self.UnderlyingGridProperties(new GridPropertiesModel(GetDataUnderlying));

    // grid properties Attach Grid
    self.AttachGridProperties = ko.observable();
    self.AttachGridProperties(new GridPropertiesModel(GetDataAttachFile));

    // set default sorting for Underlying Grid
    self.UnderlyingGridProperties().SortColumn("ID");
    self.UnderlyingGridProperties().SortOrder("ASC");

    // set default sorting for Underlying File Grid
    self.AttachGridProperties().SortColumn("FileName");
    self.AttachGridProperties().SortOrder("ASC");

    // Get filtered columns value
    function GetUnderlyingFilteredColumns() {
        var filters = [];

        //if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingFilterIsSelected() });
        if (self.UnderlyingFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingFilterUnderlyingDocument() });
        if (self.UnderlyingFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingFilterDocumentType() });
        if (self.UnderlyingFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingFilterCurrency() });
        if (self.UnderlyingFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingFilterStatementLetter() });
        if (self.UnderlyingFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingFilterAmount() });
        if (self.UnderlyingFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingFilterDateOfUnderlying() });
        if (self.UnderlyingFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingFilterExpiredDate() });
        if (self.UnderlyingFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingFilterReferenceNumber() });
        if (self.UnderlyingFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingFilterSupplierName() });

        return filters;
    };

    self.GetCalculateFX = function (amountUSD) {
        GetCalculateFX(amountUSD);
    }

    self.GetDataUnderlying = function () {
        GetDataUnderlying();
    };

    self.GetDataAttachFile = function () {
        GetDataAttachFile();
    }

    self.GetTotalAmount = function (CIFData, AmountUSD) {
        GetTotalAmount(CIFData, AmountUSD);
    }

    self.GetTotalAmountFX = function (CIFData, currentAmount) {
        GetTotalAmountFX(CIFData, currentAmount);
    }

    self.GetFCYIDRTreshold = function () {
        GetFCYIDRTreshold();
    }

    function ResetDataAttachment(index, isSelect) {
        self.CustomerAttachUnderlyings()[index].IsSelectedAttach = isSelect;
        var dataRows = self.CustomerAttachUnderlyings().slice(0);
        self.CustomerAttachUnderlyings([]);
        self.CustomerAttachUnderlyings(dataRows);
    }
    // add : chandra 2015.03.23
    function ResetDataUnderlying(index, isSelect) {
        self.CustomerUnderlyings()[index].IsEnable = isSelect;
        var dataRows = self.CustomerUnderlyings().slice(0);
        self.CustomerUnderlyings([]);
        self.CustomerUnderlyings(dataRows);
    }
    // add :chandra 2015.03.23
    function setTotalUtilize() {
        var total = 0;
        ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (item) {
            total += parseFloat(ko.utils.unwrapObservable(item.USDAmount()));
        });
        viewModel.UtilizationAmount(parseFloat(total).toFixed(2));
        // viewModel.TransactionModel().utilizationAmount(parseFloat(total).toFixed(2));
    }
    function GetCalculateFX(amountUSD) {
        AmountModel.TotalPPUAmountsUSD(TotalPPUModel.Total.IDRFCYPPU);
        AmountModel.TotalDealAmountsUSD(TotalDealModel.Total.IDRFCYDeal);
        AmountModel.RemainingBalance(TotalDealModel.Total.RemainingBalance.toFixed(2));
        AmountModel.TotalPPUUtilization(TotalPPUModel.Total.UtilizationPPU);
        AmountModel.TotalDealUtilization(TotalDealModel.Total.UtilizationDeal);
    }

    self.DealCalculate = function () {

        var t_IsFxTransaction = (viewModel.TransactionDeal().Transaction.Currency.Code != 'IDR' && viewModel.TransactionDeal().Transaction.AccountCurrencyCode == 'IDR') ? true : false;
        var t_IsFxTransactionToIDR = (viewModel.TransactionDeal().Transaction.Currency.Code == 'IDR' && viewModel.TransactionDeal().Transaction.AccountCurrencyCode != 'IDR') ? true : false;
        var t_IsFxTransactionToIDRB = (viewModel.TransactionDeal().Transaction.Currency.Code == 'IDR' && viewModel.TransactionDeal().Transaction.AccountCurrencyCode != 'IDR' && viewModel.TransactionDeal().Transaction.ProductType.IsFlowValas == true) ? true : false;

        viewModel.t_IsFxTransaction(t_IsFxTransaction);
        viewModel.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);
        viewModel.t_IsFxTransactionToIDRB(t_IsFXTransactionToIDRB);

        var t_TotalAmountsUSD = 0;
        var t_TotalAmountsUSD_stB = 0;

        var t_RemainingBalance = 0;

        var t_TotalDealTransaction = parseFloat(viewModel.AmountModel().TotalAmounts());
        var t_TotalDealTransactionB = isNaN(parseFloat(viewModel.AmountModel().TotalAmountsStatementB())) ? 0 : parseFloat(viewModel.AmountModel().TotalAmountsStatementB()); //avoid NaN

        var t_FcyAmount = parseFloat(viewModel.TransactionDeal().Transaction.AmountUSD);
        var t_IsFxTransaction = viewModel.t_IsFxTransaction();
        var t_IsFxTransactionToIDR = viewModel.t_IsFxTransactionToIDR();
        var t_IsFXTransactionToIDRB = viewModel.t_IsFxTransactionToIDRB();

        var t_Treshold = parseFloat(viewModel.AmountModel().TreshHold());
        var t_RemainingBalanceByCIF = viewModel.AmountModel().TotalRemainigByCIF();

        /* START get Amount USD Trx Total */
        if (t_IsFxTransaction) {
            //t_TotalAmountsUSD = t_TotalDealTransaction + t_FcyAmount;
            //t_TotalAmountsUSD_stB = t_TotalDealTransactionB + t_FcyAmount;
            t_TotalAmountsUSD = t_TotalDealTransaction;
            t_TotalAmountsUSD_stB = t_TotalDealTransactionB;
        }
        else if (t_IsFxTransactionToIDR) {
            //t_TotalAmountsUSD = t_FcyAmount;
            //t_TotalAmountsUSD_stB = t_FcyAmount;
            t_TotalAmountsUSD = t_TotalDealTransaction;
            t_TotalAmountsUSD_stB = t_TotalDealTransactionB;
        }
        else {
            t_TotalAmountsUSD = t_TotalDealTransaction;
            t_TotalAmountsUSD_stB = t_TotalDealTransactionB;
        }
        viewModel.AmountModel().TotalDealAmountsUSD(t_TotalAmountsUSD);
        /* END OF get Amount USD Trx Total */

        /* START get Remaining Underlying Balance */
        if (viewModel.TransactionDeal().Transaction.StatementLetter != undefined) {
            if (viewModel.TransactionDeal().Transaction.StatementLetter.ID == 1) {

                // begin if statement A
                if (t_TotalAmountsUSD > t_Treshold) {
                    //t_RemainingBalance = t_Treshold - t_TotalAmountsUSD;
                    t_RemainingBalance = t_RemainingBalanceByCIF - t_TotalAmountsUSD_stB;
                }
                else {
                    t_RemainingBalance = 0.00;
                }
            }
            else {
                // begin if statement B
                t_RemainingBalance = t_RemainingBalanceByCIF - t_TotalAmountsUSD_stB;
            }
            if (t_IsFxTransactionToIDR) {
                if (t_FcyAmount >= self.FCYIDRTreshold() && t_IsFxTransactionToIDRB == true) {
                    t_RemainingBalance = t_RemainingBalance - t_FcyAmount;
                } else {
                    t_RemainingBalance = t_RemainingBalance;
                }
            }
			
            viewModel.AmountModel().RemainingBalance(t_RemainingBalance);
        }
    }

    // Get filtered columns value
    function GetAttachFilteredColumns() {
        var filters = [];

        if (self.AttachFilterFileName() != "") filters.push({ Field: 'FileName', Value: self.AttachFilterFileName() });
        if (self.AttachFilterDocumentPurpose() != "") filters.push({ Field: 'DocumentPurpose', Value: self.AttachFilterDocumentPurpose() });
        if (self.AttachFilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.AttachFilterModifiedDate() });
        if (self.AttachFilterDocumentRefNumber() != "") filters.push({ Field: 'DocumentRefNumber', Value: self.AttachFilterDocumentRefNumber() });

        return filters;
    };

    function GetCustomerUnderlyingFilters() {
        var filters = [];
        var _cif = viewModel.TransactionMaker().Transaction.Customer.CIF;
        filters.push({ Field: 'CIF', Value: _cif });
        return filters;
    }

    function GetTreshold(cif_d) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.singlevalueparameter + "/FX_TRANSACTION_TRESHOLD",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }, success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.Treshold(data);
                    GetAvailableStatementB(cif_d)

                }
            }
        });
    }

    function GetFCYIDRTreshold() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.singlevalueparameter + "/FCY_IDR_TRANSACTION_TRESHOLD",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }, success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.FCYIDRTreshold(data);
                }
            }
        });
    }

    function GetTotalAmountFX(cif, currentAmountUSD) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.transaction + "/GetAmountTransaction=" + cif,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }, success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if (data == undefined) {
                        viewModel.AmountModel().TotalDealAmountsUSD(0);
                    } else {
                        //self.TransactionModel().TotalTransFX(parseFloat(data));
                        viewModel.AmountModel().TotalDealAmountsUSD(parseFloat(data) - parseFloat(currentAmountUSD));
                    }
                }
            }
        });

    }

    function GetTotalAmount(CIFData, AmountUSD) {
        var totalValue1 = $.ajax({
            type: "GET",
            url: api.server + api.url.transactiondeal + "/GetAmountTransaction=" + CIFData + "?isStatementB=false",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }
        });
        var totalValue2 = $.ajax({
            type: "GET",
            url: api.server + api.url.transactiondeal + "/GetAmountTransaction=" + CIFData + "?isStatementB=true",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }
        });
        $.when(totalValue1, totalValue2).done(function (data1, data2) {
            var t_TotalAmounts = 0;
            if (data1 != null || data1 != undefined) {
                var t_TotalAmounts1 = parseFloat(data1);
                var t_TotalAmounts2 = parseFloat(data2);

                self.AmountModel().TotalAmounts(t_TotalAmounts2);
                self.AmountModel().TotalAmountsStatementB(t_TotalAmounts1);
                self.DealCalculate();
            }
            else {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }


    function GetTotalAmount(CIFData, IsStatementB) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.transactiondeal + "/GetAmountTransaction=" + CIFData + "?isStatementB=" + IsStatementB,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {

                var t_TotalAmounts = 0;
                if (jqXHR.status = 200) {
                    var t_TotalAmounts = parseFloat(data);
                    if (IsStatementB == true) {
                        self.AmountModel().TotalAmounts(t_TotalAmounts);
                    }
                    else {
                        self.AmountModel().TotalAmountsStatementB(t_TotalAmounts);
                    }
                }
                else if (jqXHR.status = 204) {
                    if (IsStatementB == true) {
                        self.AmountModel().TotalAmounts(t_TotalAmounts);
                    }
                    else {
                        self.AmountModel().TotalAmountsStatementB(t_TotalAmounts);
                    }
                }
                else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetAvailableStatementB(cif_d) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetAvailableStatementB/" + cif_d,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                var t_TotalRemainig = 0;
                if (jqXHR.status = 200) {
                    t_TotalRemainig = data;
                    viewModel.AmountModel().TotalRemainigByCIF(t_TotalRemainig);

                } else {

                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetUnderlyingFile() {
        var options = {
            url: api.server + api.url.customerunderlyingfile + "/TransactionDeal/" + DealID,
            token: accessToken
        };

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataAttachFile, OnError, OnAlways);

        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        }
    }

    function GetDataAttachFile() {
        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlyingfile + "/TransactionDeal/" + DealID,
            params: {
                page: self.AttachGridProperties().Page(),
                size: self.AttachGridProperties().Size(),
                sort_column: self.AttachGridProperties().SortColumn(),
                sort_order: self.AttachGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataAttachFile, OnError, OnAlways);

        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        }
    }

    // On success GetData callback
    function OnSuccessGetDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerUnderlyingFiles(data.Rows);

            self.AttachGridProperties().Page(data['Page']);
            self.AttachGridProperties().Size(data['Size']);
            self.AttachGridProperties().Total(data['Total']);
            self.AttachGridProperties().TotalPages(Math.ceil(self.AttachGridProperties().Total() / self.AttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    function GetDataUnderlying() {
        // widget reloader function start
        var options = {
            url: api.server + api.url.customerunderlying + "/TransactionDeal/" + DealID,
            params: {
                page: self.UnderlyingGridProperties().Page(),
                size: self.UnderlyingGridProperties().Size(),
                sort_column: self.UnderlyingGridProperties().SortColumn(),
                sort_order: self.UnderlyingGridProperties().SortOrder()
            },
            token: accessToken
        };



        // get filtered columns
        var filters = GetUnderlyingFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        }
    }

    // On success GetData callback 1
    function OnSuccessGetDataUnderlying(data, textStatus, jqXHR) {

        if (jqXHR.status = 200) {

            var UtilizationAmount = 0;
            for (i = 0; i < data['Total']; i++) {
                UtilizationAmount = UtilizationAmount + data.Rows[i].UtilizationAmount;

            }

            self.CustomerUnderlyings(data.Rows);
            self.UnderlyingGridProperties().Page(data['Page']);
            self.UnderlyingGridProperties().Size(data['Size']);
            self.UnderlyingGridProperties().Total(data['Total']);
            self.UnderlyingGridProperties().TotalPages(Math.ceil(self.UnderlyingGridProperties().Total() / self.UnderlyingGridProperties().Size()));

            self.UtilizationAmounts(UtilizationAmount);
            GetDataAttachFile();
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    //added chandra
    self.GetUnderlyingSelectedRow = function (data) {

        $("#modal-form-Underlying").modal('show');

        self.ID_u(data.ID);
        self.Amount_u(data.Amount);
        self.UnderlyingDocument_u(data.UnderlyingDocument.Code + " (" + data.UnderlyingDocument.Name + ")");
        //self.OtherUnderlying_u(data.OtherUnderlying);
        self.DocumentType_u(data.DocumentType.Name);

        //self.Currency_u(data.Currency);
        self.Currency_u(data.Currency.Code + " (" + data.Currency.Description + ")");


        self.Rate_u(data.Rate);
        self.AmountUSD_u(data.AmountUSD);
        self.AttachmentNo_u(data.AttachmentNo);

        //self.StatementLetter_u(data.StatementLetter);
        self.StatementLetter_u(data.StatementLetter.Name);

        self.IsDeclarationOfException_u(data.IsDeclarationOfException);
        self.StartDate_u(self.LocalDate(data.StartDate, true, false));
        self.EndDate_u(self.LocalDate(data.EndDate, true, false));
        self.DateOfUnderlying_u(self.LocalDate(data.DateOfUnderlying, true, false));
        self.ExpiredDate_u(self.LocalDate(data.ExpiredDate, true, false));
        self.ReferenceNumber_u(data.ReferenceNumber);
        self.SupplierName_u(data.SupplierName);
        self.InvoiceNumber_u(data.InvoiceNumber);
        self.IsProforma_u(data.IsProforma);
        self.IsBulkUnderlying_u(data.IsBulkUnderlying);
        self.Proformas(data.Proformas);
        self.BulkUnderlyings(data.BulkUnderlyings);
        self.ProformaDetails(data.ProformaDetails);
        self.BulkDetails(data.BulkDetails);
        $('#backDrop').show();
    }


    self.ReplaceBoolean = function (value) {
        if (value == true) return "yes";
        else return "no";
    }

    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    self.GridProperties().SortColumn("EntryTime");
    self.GridProperties().SortOrder("DESC");

    // Options selected value
    self.Selected = ko.observable(SelectedModel);

    // filters
    self.FilterWorkflowName = ko.observable("");
    self.FilterInitiator = ko.observable("");
    self.FilterStateDescription = ko.observable("");
    self.FilterStartTime = ko.observable("");
    self.FilterTitle = ko.observable("");
    self.FilterActivityTitle = ko.observable("");
    self.FilterUser = ko.observable("");
    self.FilterUserApprover = ko.observable("");
    self.FilterEntryTime = ko.observable("");
    self.FilterExitTime = ko.observable("");
    self.FilterOutcomeDescription = ko.observable("");
    self.FilterCustomOutcomeDescription = ko.observable("");
    self.FilterComments = ko.observable("");

    // filters transaction
    self.FilterApplicationID = ko.observable("");
    self.FilterCustomer = ko.observable("");
    self.FilterProduct = ko.observable("");
    self.FilterCurrency = ko.observable("");
    self.FilterAmount = ko.observable("");
    self.FilterAmountUSD = ko.observable("");
    self.FilterDebitAccNumber = ko.observable("");
    self.FilterFXTransaction = ko.observable("");
    self.FilterTopUrgent = ko.observable("");

    //Declare an ObservableArray for Storing the JSON Response
    self.Tasks = ko.observableArray([]);
    self.Outcomes = ko.observableArray([]);

    // customer contact
    var CustomerContact = {
        ID: self.ID_c,
        CIF: self.CIF_c,
        SourceID: self.SourceID_c,
        Name: self.Name_c,
        PhoneNumber: self.PhoneNumber_c,
        DateOfBirth: self.DateOfBirth_c,
        Address: self.Address_c,
        IDNumber: self.IDNumber_c,
        POAFunction: self.POAFunction_c,
        OccupationInID: self.OccupationInID_c,
        PlaceOfBirth: self.PlaceOfBirth_c,
        EffectiveDate: self.EffectiveDate_c,
        CancellationDate: self.CancellationDate_c,
        POAFunctionOther: self.POAFunctionOther_c,
        LastModifiedBy: self.LastModifiedBy_c,
        LastModifiedDate: self.LastModifiedDate_c
    };

    self.AddContact = function () {
        self.IsNewData(true);
        self.ClearData();

        $("#modal-form-child").modal('show');

        $('#backDrop').show();

        // datepicker
        $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
            $(this).prev().focus();
        });

        // validation
        $('#aspnetForm').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: true,

            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },

            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                $(e).remove();
            },

            errorPlacement: function (error, element) {
                if (element.is(':checkbox') || element.is(':radio')) {
                    var controls = element.closest('div[class*="col-"]');
                    if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if (element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if (element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            }
        });

    }

    self.ClearData = function () {
        self.ID_c(0);
        self.Name_c('');
        self.PhoneNumber_c('');
        self.DateOfBirth_c('');
        self.Address_c('');
        self.IDNumber_c('');
        self.Selected().POAFunction('');
        self.OccupationInID_c('');
        self.PlaceOfBirth_c('');
        self.EffectiveDate_c('');
        self.CancellationDate_c('');
        self.POAFunctionOther_c('');
    }

    self.Close = function () {
        $("#modal-form-child").modal('hide');
        $("#modal-form-Attach1").modal('hide');
        $('#backDrop').hide();
    }
    self.CloseUnderlying = function () {
        $("#modal-form-Underlying").modal('hide');
        //$("#modal-form-upload").modal('hide');
        $('#backDrop').hide();
    }
    self.CloseDocument = function () {
        $("#modal-form-Attach1").modal('hide');
        $('#backDrop').hide();
        $('.remove').click();
    }

    self.GetSelectedContactRow = function (contact) {
        self.IsNewData(false);

        self.ID_c(contact.ID);
        self.SourceID_c('2');
        self.Name_c(contact.Name);
        self.PhoneNumber_c(contact.PhoneNumber);

        self.Address_c(contact.Address);
        self.IDNumber_c(contact.IDNumber);
        self.Selected().POAFunction(contact.POAFunction.ID);
        self.POAFunction_c(contact.POAFunction);
        self.OccupationInID_c(contact.OccupationInID);
        self.PlaceOfBirth_c(contact.PlaceOfBirth);
        self.DateOfBirth_c(self.LocalDate(contact.DateOfBirth,true,false));
        self.EffectiveDate_c(self.LocalDate(contact.EffectiveDate,true,false));
        self.CancellationDate_c(self.LocalDate(contact.CancellationDate,true,false));

        self.POAFunctionOther_c(contact.POAFunctionOther);
        //self.DateOfBirth_c(moment(contact.DateOfBirth).format('MM-DD-YYYY'));
        //self.EffectiveDate_c(moment(contact.EffectiveDate).format('MM-DD-YYYY'));
        //self.CancellationDate_c(moment(contact.CancellationDate).format('MM-DD-YYYY'));

        //var zind = getStyle("modal-form-child", "zIndex");
        //alert(zind);

        $("#modal-form-child").modal('show');
        $('#backDrop').show();
        //$("#modal-form-child").modal({backdrop: 'fixed'});
    }

    function getStyle(el, prop) {
        var x = document.getElementById(el);

        if (window.getComputedStyle) {
            var y = document.defaultView.getComputedStyle(x, null).getPropertyValue(prop);
        } else if (x.currentStyle) {
            var y = x.currentStyle[styleProp];
        }

        return y;
    }

    self.save_c = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();
        SetPOAFunctionOther();
        if (form.valid()) {
            //Ajax call to insert the CustomerContacts
            $.ajax({
                type: "POST",
                url: api.server + api.url.customercontact + '/Add/Workflow/' + self.WorkflowInstanceID() + '/ApproverID/' + self.ApproverId(),
                data: ko.toJSON(CustomerContact), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        // hide current popup window
                        $("#modal-form-child").modal('hide');
                        $('#backDrop').hide();

                        // refresh data
                        GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }
    };

    self.update_c = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();
        SetPOAFunctionOther();
        if (form.valid()) {
            //Ajax call to insert the CustomerContacts
            $.ajax({
                type: "PUT",
                url: api.server + api.url.customercontact + '/Update/Workflow/' + self.WorkflowInstanceID() + '/ApproverID/' + self.ApproverId(),
                data: ko.toJSON(CustomerContact), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        // hide current popup window
                        $("#modal-form-child").modal('hide');
                        $('#backDrop').hide();

                        // refresh data
                        GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }
    };

    self.delete_c = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            //Ajax call to insert the CustomerContacts
            $.ajax({
                type: "PUT",
                url: api.server + api.url.customercontact + '/Delete/Workflow/' + self.WorkflowInstanceID() + '/ApproverID/' + self.ApproverId(),
                data: ko.toJSON(CustomerContact), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        // hide current popup window
                        $("#modal-form-child").modal('hide');
                        $('#backDrop').hide();

                        // refresh data
                        GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }
    };

    //set POA Function Other Other
    function SetPOAFunctionOther() {
        if (self.Selected().POAFunction() != 9) {
            self.POAFunctionOther_c('');
        }
    }
    // upload document
    self.UploadDocument = function () {
        self.IsUploading(false);
        $("#modal-form-Attach1").modal('show');
        $('.remove').click();
        var item = ko.utils.arrayFilter(self.DocumentPurposes(), function (dta) { return dta.ID != 2 });
        self.ddlDocumentPurpose_a(item);
        self.ID_a(0);
        self.DocumentPurpose_a(null);
        self.Selected().DocumentPurpose_a(null);
        self.DocumentType_a(null);
        self.Selected().DocumentType_a(null);
        self.DocumentPath_a(null);

        $('#backDrop').show();
    }

    // Add new document handler
    self.AddDocument = function () {
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            var vDocumentType = ko.utils.arrayFirst(self.DocumentTypes(), function (item) {
                return item.ID == self.Selected().DocumentType();
            });

            var vDocumentPurpose = ko.utils.arrayFirst(self.DocumentPurposes(), function (item) {
                return item.ID == self.Selected().DocumentPurpose();
            });

            var file = ko.toJS(self.DocumentPath())

            var doc = {
                ID: 0,
                Type: vDocumentType,
                Purpose: vDocumentPurpose,
                FileName: file.name,
                DocumentPath: self.DocumentPath(),
                LastModifiedDate: new Date(),
                LastModifiedBy: null
            };

            var isDocExist = ko.utils.arrayFirst(self.TransactionChecker().Transaction.Documents, function (item) {
                return item.FileName == doc.FileName && item.Purpose.Name == doc.Purpose.Name && item.Type.Name == doc.Type.Name;
            });

            if (isDocExist) {
                ShowNotification('Warning', 'Data already exist.', 'gritter-warning', false);
            } else {
                self.TransactionChecker().Transaction.Documents.push(doc);

                var update = viewModel.TransactionChecker();
                viewModel.TransactionChecker(ko.mapping.toJS(update));

                // hide upload dialog
                $("#modal-form-Attach1").modal('hide');
                $('#backDrop').hide();
            }
        }

        $('.remove').click();
    };

    //is deleted document
    self.IsDeleteAllowed = function (id) {
        /*var isAllowed = ko.utils.arrayFilter(self.TransactionChecker().Transaction.Documents, function (item) {
         return item.ID != 0;
         });*/

        if (id != 0) {
            return false;
        } else {
            return true;
        }
    };

    //delete document
    self.RemoveDocument = function (data) {
        /*var data = ko.utils.arrayFilter(self.TransactionChecker().Transaction.Documents, function (item) {
         return ((item.FileName == data.FileName) && (item.Purpose.Name == data.Purpose.Name) && (item.Type.Name == data.Type.Name));
         });*/

        var result = ko.utils.arrayFilter(self.TransactionChecker().Transaction.Documents, function (item) {
            return ((item.DocumentPath != data.DocumentPath));
        });

        self.TransactionChecker().Transaction.Documents = [];

        for (var i = 0; i <= result.length - 1; i++) {
            self.TransactionChecker().Transaction.Documents.push(result[i]);
        }

        var update = viewModel.TransactionChecker();
        viewModel.TransactionChecker(ko.mapping.toJS(update));
        //console.log(self.TransactionChecker().Transaction.Documents);
    };
    // delete document on ppumaker
    self.RemoveDocumentData = function (data) {
        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (result) {
                if (data.Purpose.ID == 2) {
                    // delete underlying document
                    DeleteUnderlyingAttachment(data);

                    switch (self.ActivityTitle()) {
                        case "PPU Maker Task":
                        case "PPU Maker After Checker and Caller Task":
                            var item = ko.utils.arrayFilter(viewModel.TransactionMaker().Transaction.Documents, function (dta) { return dta.DocumentPath != data.DocumentPath; });
                            if (item != null) {
                                viewModel.TransactionMaker().Transaction.Documents = item;
                            }
                            break;
                        default:
                            var item = ko.utils.arrayFilter(viewModel.TransactionChecker().Transaction.Documents, function (dta) { return dta.DocumentPath != data.DocumentPath; });
                            if (item != null) {
                                viewModel.TransactionChecker().Transaction.Documents = item;
                            }
                    }
                } else {
                    if (data.IsNewDocument == false) {

                        switch (self.ActivityTitle()) {
                            case "PPU Maker Task":
                            case "PPU Maker After Checker and Caller Task":
                                var item = ko.utils.arrayFilter(viewModel.TransactionMaker().Transaction.Documents, function (dta) { return dta.ID != data.ID; });
                                if (item != null) {
                                    viewModel.TransactionMaker().Transaction.Documents = item;
                                }
                                break;
                            default:
                                var item = ko.utils.arrayFilter(viewModel.TransactionChecker().Transaction.Documents, function (dta) { return dta.ID != data.ID; });
                                if (item != null) {
                                    viewModel.TransactionChecker().Transaction.Documents = item;
                                }
                        }
                    }
                }
                self.MakerAllDocumentsTemp.remove(data);
                self.MakerDocuments.remove(data);

            }
        });
    }

    function DeleteUnderlyingAttachment(item) {
        //Ajax call to delete the Customer

        $.ajax({
            type: "DELETE",
            url: api.server + api.url.customerunderlyingfile + "/" + item.ID,
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                // send notification
                if (jqXHR.status = 200) {
                    //DeleteFile(item.DocumentPath); deleted file underlying
                    //if(!item.IsNewDocument) {
                    // DeleteTransactionDocumentUnderlying(item);
                    //}
                    ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                    // refresh data
                    //GetDataAttachFile();
                } else
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
    function DeleteTransactionDocumentUnderlying(item) {
        //Ajax call to delete the Customer
        var pathdocs = item.DocumentPath.split('/');
        var file = pathdocs[pathdocs.length - 1].split('.');
        var fileName = file[0];
        $.ajax({
            type: "DELETE",
            url: api.server + api.url.transaction + "/DocumentPath/Delete/" + fileName,
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                // send notification
                if (jqXHR.status = 200) {
                    //DeleteFile(item.DocumentPath);
                    //ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                    // refresh data
                    //GetDataAttachFile();
                } else
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
    function DeleteFile(documentPath) {
        // Get the server URL.
        var serverUrl = _spPageContextInfo.webAbsoluteUrl;
        // output variable
        var output;

        // Delete the file to the SharePoint folder.
        var deleteFile = deleteFileToFolder();
        deleteFile.done(function (file, status, xhr) {
            output = file.d;
        });
        deleteFile.fail(OnError);
        // Call function delete File Shrepoint Folder
        function deleteFileToFolder() {
            return $.ajax({
                url: serverUrl + "/_api/web/GetFileByServerRelativeUrl('" + documentPath + "')",
                type: "POST",
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                    "X-HTTP-Method": "DELETE",
                    "IF-MATCH": "*"
                    //"content-length": arrayBuffer.byteLength
                }
            });
        }
    }

    // bind get data function to view
    self.GetData = function () { GetData(); };
    self.GetParameters = function () { GetParameters(); };
    self.GetCustomerUnderlying = function () { GetCustomerUnderlying(); }
    // bind clear filters
    self.ClearFilters = function () {
        self.FilterWorkflowName("");
        self.FilterInitiator("");
        self.FilterStateDescription("");
        self.FilterStartTime("");
        self.FilterTitle("");
        self.FilterActivityTitle("");
        self.FilterUser("");
        self.FilterUserApprover("");
        self.FilterEntryTime("");
        self.FilterExitTime("");
        self.FilterOutcomeDescription("");
        self.FilterCustomOutcomeDescription("");
        self.FilterComments("");

        self.FilterApplicationID("");
        self.FilterCustomer("");
        self.FilterProduct("");
        self.FilterCurrency("");
        self.FilterAmount("");
        self.FilterAmountUSD("");
        self.FilterDebitAccNumber("");
        self.FilterFXTransaction("");
        self.FilterTopUrgent("");

        GetData();
    };

    // Color Status
    // Update Rizki 2015-03-06
    self.SetColorStatus = function (id, applicationID, outcomeDescription, customOutcomeDescription) {
        if (outcomeDescription == null) {
            outcomeDescription = '';
        }

        if (customOutcomeDescription == null) {
            customOutcomeDescription = '';
        }

        if (id == 2) {
            return "active";
        }
        else if (id == 4) {
            //Untuk handle FX Checker reject dan PPU Checker Approve Cancellation
            if ((applicationID.toLowerCase().startsWith('fx') && outcomeDescription.toLowerCase() == 'rejected') || (customOutcomeDescription.toLowerCase() == 'approve cancellation')) {
                return "warning";
            }
            else {
                return "success";
            }
        }
        else if (id == 8) {
            return "warning";
        }
        else if (id == 64) {
            return "danger";
        }
        else {
            return "";
        }
    };

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);
        if (moment(localDate).isValid()) {
            if (date == '1970/01/01 00:00:00' || date == '1970-01-01T00:00:00' || date == null) {
                return "";
            }
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return ""
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    self.FormatNumber = function (num) {
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    }

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        self.ApproverId(data.WorkflowContext.ApproverID);
        self.SPWebID(data.WorkflowContext.SPWebID);
        self.SPSiteID(data.WorkflowContext.SPSiteID);
        self.SPListID(data.WorkflowContext.SPListID);
        self.SPListItemID(data.WorkflowContext.SPListItemID);
        self.SPTaskListID(data.WorkflowContext.SPTaskListID);
        self.SPTaskListItemID(data.WorkflowContext.SPTaskListItemID);
        self.Initiator(data.WorkflowContext.Initiator);
        self.WorkflowInstanceID(data.WorkflowContext.WorkflowInstanceID);
        self.WorkflowID(data.WorkflowContext.WorkflowID);
        self.WorkflowName(data.WorkflowContext.WorkflowName);
        self.StartTime(data.WorkflowContext.StartTime);
        self.StateID(data.WorkflowContext.StateID);
        self.StateDescription(data.WorkflowContext.StateDescription);
        self.IsAuthorized(data.WorkflowContext.IsAuthorized);

        self.TaskTypeID(data.CurrentTask.TaskTypeID);
        self.TaskTypeDescription(data.CurrentTask.TaskTypeDescription);
        self.Title(data.CurrentTask.Title);
        self.ActivityTitle(data.CurrentTask.ActivityTitle);
        self.User(data.CurrentTask.User);
        self.IsSPGroup(data.CurrentTask.IsSPGroup);
        self.EntryTime(data.CurrentTask.EntryTime);
        self.ExitTime(data.CurrentTask.ExitTime);
        self.OutcomeID(data.CurrentTask.OutcomeID);
        self.OutcomeDescription(data.CurrentTask.OutcomeDescription);
        self.CustomOutcomeID(data.CurrentTask.CustomOutcomeID);
        self.CustomOutcomeDescription(data.CurrentTask.CustomOutcomeDescription);
        self.Comments(data.CurrentTask.Comments);

        self.Outcomes.removeAll();
        // add chandra set original value

        // show the dialog task form
        $("#modal-form").modal('show');

        // while is pending task, we need to get outcomes
        if (data.CurrentTask.OutcomeID == 2) {
            // lock current task for current user only
            //LockAndUnlockTaskHub("Lock", self.WorkflowInstanceID(), self.ApproverId(), self.SPTaskListItemID(), self.ActivityTitle());

            // get nintex outcomes
            //GetTaskOutcomes(data.WorkflowContext.SPTaskListID, data.WorkflowContext.SPTaskListItemID);
        }

        // ruddy : by pass to check pending outcome
        // get nintex outcomes


        GetTaskOutcomes(data.WorkflowContext.SPTaskListID, data.WorkflowContext.SPTaskListItemID);


        // test only
        //LockAndUnlockTaskHub("Lock", self.WorkflowInstanceID(), self.ApproverId(), self.SPTaskListItemID(), self.ActivityTitle());

        // get transaction data
        //if(self.TransactionMaker() == undefined || self.TransactionMaker().WorkflowInstanceID != self.WorkflowInstanceID()){
        /*$("#transaction-progress").show();

         setTimeout(function(){
         GetTransactionData(self.WorkflowInstanceID());
         }, 2000);*/

        //GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());
        //}
        //GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());
    };

    self.OpenTask = function (data) {

    };

    // set approval ui template
    self.ApprovalTemplate = function () {
        var output;

        switch (self.ActivityTitle()) {
            case "PPU Maker Task":
                output = "TaskPPUMaker";
                break;
            case "PPU Checker Cancellation Task":
                output = "TaskPPUCheckerCanceled";
                break;
            case "PPU Maker After Checker and Caller Task":
                output = "TaskPPUMakerAfterCheckerandCaller";
                break;
            case "PPU Checker Task":
                output = "TaskPPUChecker";
                break;
            case "PPU Checker After PPU Caller Task":
                output = "TaskPPUCheckerAfterPPUCaller";
                break;
            case "PPU Caller Task":
                GetUtcAttemp();
                output = "TaskPPUCaller";
                break;
            case "CBO Maker Task":
                output = "TaskCBOMaker";
                break;
            case "CBO Checker Task":
                output = "TaskCBOChecker";
                break;
            case "Branch Maker Reactivation Task":
                output = "TaskBranchMakerReactivation";
                break;
            case "Branch Checker Reactivation Task":
                output = "TaskBranchCheckerReactivation";
                break;
            case "CBO Maker Reactivation Task":
                output = "TaskCBOMakerReactivation";
                break;
            case "CBO Checker Reactivation Task":
                output = "TaskCBOCheckerReactivation";
                break;
            case "Payment Maker Task":
                output = "TaskPaymentMaker";
                break;
            case "Payment Maker Revise Task":
                output = "TaskPaymentMaker";
                break;
            case "Payment Checker Task":
                output = "TaskPaymentChecker";
                break;
            case "Lazy Approval Task for Exceptional Handling Case":
                output = "TaskLazyApproval";
                break;
            case "FX Deal Checker Task":
                output = "TaskFXDealChecker";
                break;
            case "Pending Documents Checker Task":
                output = "TaskPendingDocumentsChecker";
                break;
            case "Lazy App fo Except":
                output = "TaskLazyApproval";
                break;
            case "Lazy Approval Task for UTC Case":
                output = "TaskLazyApproval";
                break;

        }

        return output;
    };

    self.UtcAttemp = ko.observable();
    function GetUtcAttemp() {
        var options = {
            url: api.server + api.url.utcattemps,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetUtcAttemp, OnError, OnAlways);
    }

    function OnSuccessGetUtcAttemp(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            self.UtcAttemp(data);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    // set approval data template
    self.ApprovalData = function () {
        var output;

        switch (self.ActivityTitle()) {
            case "PPU Maker Task":
                output = self.TransactionMaker();
                break;
            case "PPU Maker After Checker and Caller Task":
                output = self.TransactionMaker();
                break;
            case "PPU Checker Task":
                output = self.TransactionChecker();
                break;
            case "PPU Checker After PPU Caller Task":
                output = self.TransactionCheckerCallback();
                break;
            case "PPU Caller Task":
                output = self.TransactionCallback();
                break;
            case "PPU Checker Cancellation Task":
                output = self.TransactionChecker();
                break;
            case "CBO Maker Task":
                output = self.TransactionContact();
                break;
            case "CBO Checker Task":
                output = self.TransactionContact();
                break;
            case "Branch Maker Reactivation Task":
                output = self.TransactionChecker();
                break;
            case "Branch Checker Reactivation Task":
                output = self.TransactionChecker();
                break;
            case "CBO Maker Reactivation Task":
                output = self.TransactionChecker();
                break;
            case "CBO Checker Reactivation Task":
                output = self.TransactionChecker();
                break;
            case "Payment Maker Task":
                output = self.PaymentMaker();
                break;
            case "Payment Maker Revise Task":
                output = self.PaymentMaker();
                break;
            case "Payment Checker Task":
                output = self.PaymentChecker();
                break;
            case "Lazy Approval Task for Exceptional Handling Case":
                output = self.TransactionChecker();
                break;
            case "FX Deal Checker Task":
                output = self.TransactionDeal(); //ganti di sini
                break;
            case "Pending Documents Checker Task":
                output = self.TransactionChecker();
                break;
            case "Lazy App fo Except":
                output = self.TransactionChecker();
                break;
            case "Lazy Approval Task for UTC Case":
                output = self.TransactionChecker();
                break;
        }

        return output;
    };
    function EnableMessageApprover() {
        self.OriginalValue().BeneAccNumberOri().Status(false);
        self.OriginalValue().AmountOri().Status(false);
        self.OriginalValue().RateOri().Status(false);
        self.OriginalValue().AmountUSDOri().Status(false);
        self.OriginalValue().BeneNameOri().Status(false);
        self.OriginalValue().BanksOri().Status(false);
        //self.OriginalValue().AccountOri().Status(false);

        self.OriginalValue().SwiftCodeOri().Status(false);
        self.OriginalValue().BankChargesOri().Status(false);
        self.OriginalValue().AgentChargesOri().Status(false);
        self.OriginalValue().IsCitizenOri().Status(false);
        self.OriginalValue().IsResidentOri().Status(false);
    }
    //chandra SetDisable Message Approver
    function SetDisableMessageApprover() {
        var isStatus = false;
        EnableMessageApprover();
        var amount = self.PaymentMaker().Payment.Amount != undefined ? self.PaymentMaker().Payment.Amount : '';
        var AmountOri = self.OriginalValue().AmountOri().Value() != undefined ? self.OriginalValue().AmountOri().Value() : 0;
        if (amount != AmountOri) {
            self.OriginalValue().AmountOri().Status(true);
            self.OriginalValue().RateOri().Status(true);
            self.OriginalValue().AmountUSDOri().Status(true);
            isStatus = true;
        }
        var beneAccNumber = self.PaymentMaker().Payment.BeneAccNumber != undefined ? self.PaymentMaker().Payment.BeneAccNumber.trim().toLowerCase() : '';
        var beneAccNumberOri = self.OriginalValue().BeneAccNumberOri().Value() != undefined ? self.OriginalValue().BeneAccNumberOri().Value().trim().toLowerCase() : '';
        if (beneAccNumber != beneAccNumberOri) {
            self.OriginalValue().BeneAccNumberOri().Status(true);
            isStatus = true;
        }

        var beneName = self.PaymentMaker().Payment.BeneName != undefined ? self.PaymentMaker().Payment.BeneName.toLowerCase().trim().toLowerCase() : '';
        var benenameOri = self.OriginalValue().BeneNameOri().Value() != undefined ? self.OriginalValue().BeneNameOri().Value().trim().toLowerCase() : '';
        if (beneName != benenameOri) {
            self.OriginalValue().BeneNameOri().Status(true);
            isStatus = true;
        }
        var bankName = self.PaymentMaker().Payment.Bank.Description != undefined ? self.PaymentMaker().Payment.Bank.Description.trim().toLowerCase() : '';
        var bankNameOri = self.OriginalValue().BanksOri().Value() != undefined ? self.OriginalValue().BanksOri().Value().trim().toLowerCase() : '';
        if (bankName != bankNameOri) {
            self.OriginalValue().BanksOri().Status(true);
            isStatus = true;
        }

        var swiftCode = self.PaymentMaker().Payment.Bank.SwiftCode != undefined ? self.PaymentMaker().Payment.Bank.SwiftCode.trim().toLowerCase() : '';
        var swiftCodeOri = self.OriginalValue().SwiftCodeOri().Value() != undefined ? self.OriginalValue().SwiftCodeOri().Value().trim().toLowerCase() : '';
        if (swiftCode != swiftCodeOri) {
            self.OriginalValue().SwiftCodeOri().Status(true);
            isStatus = true;
        }

        /*       var bankChargesOri = self.OriginalValue().BankChargesOri().Value() != undefined ? self.OriginalValue().BankChargesOri().Value().trim().toLowerCase() : '';
         if (bankCharges != bankChargesOri) {
         self.OriginalValue().BankChargesOri().Status(true);
         isStatus = true;
         } */

        /*       var agentChargesOri = self.OriginalValue().AgentChargesOri().Value() != undefined ? self.OriginalValue().AgentChargesOri().Value().trim().toLowerCase() : '';
         if (agentCharges != agentChargesOri) {
         self.OriginalValue().AgentChargesOri().Status(true);
         isStatus = true;
         }
         */


        var agentCharges = self.PaymentMaker().Payment.AgentCharges.Code != undefined ? self.PaymentMaker().Payment.AgentCharges.Code.trim().toLowerCase() : '';
        var agentChargesOri = self.AgentChargesOrig() != undefined ? self.AgentChargesOrig().trim().toLowerCase() : '';
        if (agentCharges != agentChargesOri) {
            self.OriginalValue().AgentChargesOri().Status(true);
            isStatus = true;
        }

        var bankCharges = self.PaymentMaker().Payment.BankCharges.Code != undefined ? self.PaymentMaker().Payment.BankCharges.Code.trim().toLowerCase() : '';
        var bankChargesOri = self.CargerBankOrig() != undefined ? self.CargerBankOrig().trim().toLowerCase() : '';
        if (bankCharges != bankChargesOri) {
            self.OriginalValue().BankChargesOri().Status(true);
            isStatus = true;
        }

        var isCitizen = self.PaymentMaker().Payment.IsCitizen != undefined ? self.PaymentMaker().Payment.IsCitizen : '';
        var isCitizenOri = self.OriginalValue().IsCitizenOri().Value() != undefined ? self.OriginalValue().IsCitizenOri().Value() : '';
        if (isCitizen != isCitizenOri) {
            self.OriginalValue().IsCitizenOri().Status(true);
            isStatus = true;
        }

        var isResident = self.PaymentMaker().Payment.IsResident != undefined ? self.PaymentMaker().Payment.IsResident : '';
        var isResidentOri = self.OriginalValue().IsResidentOri().Value() != undefined ? self.OriginalValue().IsResidentOri().Value() : '';
        if (isResident != isResidentOri) {
            self.OriginalValue().IsResidentOri().Status(true);
            isStatus = true;
        }
        self.BeneBanktmp(self.PaymentMaker().Payment.Bank.Description);
        self.SwiftCodetmp(self.PaymentMaker().Payment.Bank.SwiftCode);
        self.AccNumbertmp(self.PaymentMaker().Payment.BeneAccNumber);

        self.IsResidenttmp(self.PaymentMaker().Payment.IsResident);
        self.IsCitizentmp(self.PaymentMaker().Payment.IsCitizen);

        self.agentChargestmp(self.PaymentMaker().Payment.AgentCharges.Code);
        self.bankChargestmp(self.PaymentMaker().Payment.BankCharges.Code);

        return isStatus;
    }
    // workflow approval process & get selected outcome to send into custom Nintex REST Api
    self.ApprovalProcess = function (data) {

        /*if (data.Name == 'Approve' && (self.ActivityTitle() == "PPU Checker Task" || self.ActivityTitle() == "PPU Checker Cancellation Task" )) {
         if(!self.TransactionChecker().Checker.IsSignatureVerified)
         {
         ShowNotification("Task Validation Warning", "Signature Verification required", "gritter-warning", false);
         return;
         }
         }*/

        var text = String.format("{0}<br/><br/>Do you want to <b>{1}</b> this task?", data.Description, data.Name);
        var outcomes = config.nintex.outcome.uncompleted;

        if (self.ActivityTitle() == "Payment Maker Task") {
            if (viewModel.TZModel().ID == 1) {
                ShowNotification("Task Validation Warning", String.format("{0}.", viewModel.TZModel().Message), "gritter-warning", false);
                return;
            }
        }

        if (self.ActivityTitle() == "CBO Maker Reactivation Task") {
            if (viewModel.TransactionChecker().Checker.IsDormantAccount == true || viewModel.TransactionChecker().Checker.IsFreezeAccount == true) {
                ShowNotification("Task Validation Warning", "Dormant Account and Freeze Account muct be UNTICK", "gritter-warning", false);
                return;
            }
        }

        if (self.ActivityTitle() == "Payment Checker Task") {
            if (data.Name == 'Revise to PPU Maker') {
                var form = $("#aspnetForm");
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        UpdateApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                        return;
                    }
                });
            } else if (data.Name == 'Approve') {

                if (!self.IsAllChecked()) {
                    ShowNotification("Task Validation Warning", String.format("Please tick all checkbox to {0} this task.", data.Name), "gritter-warning", false);
                } else {
                    bootbox.confirm(text, function (result) {
                        if (result) {
                            // call nintex api
                            CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                            return;
                        }
                    });
                }
            } else if (data.Name = 'Revise to Payment Maker') {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                        return;
                    }
                });
            }
        } else {

            // while user reject, revise or cancel the task, don't validate the form and the form data will not stored on database
            if (outcomes.indexOf(data.Name) > -1 || self.ActivityTitle() == "FX Deal Checker Task" || self.ActivityTitle() == "Pending Documents Checker Task") {
                if (data.ID != 1) // ID reject
                {
                        var form = $("#aspnetForm");
                        bootbox.confirm(text, function (result) {
                            if (result) {
                                // call nintex api
                                if (self.ActivityTitle() == "PPU Checker Task" || self.ActivityTitle() == "PPU Checker Task" || self.ActivityTitle() == "Payment Checker Task" || self.ActivityTitle() == "PPU Checker After PPU Caller Task") {
                                    UpdateApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                                } else {
                                    CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                                }
                            }
                        });
                } else {
                    bootbox.confirm(text, function (result) {
                        if (result) {
                            RejectData(self.WorkflowInstanceID(), data.ID);
                        }
                    });
                }
            } else {
                // validation
                var form = $("#aspnetForm");
                form.validate();

                if (form.valid()) {
                    // Ruddy 29.12.2014 : While current task as PPU Checker, user must checked all checkbox to approve task.
                    if (self.ActivityTitle() == "PPU Checker Cancellation Task") {
                        self.IsAllChecked = ko.observable(true);
                    }

                    if (self.ActivityTitle() == "PPU Caller Task") {
                        if (self.TransactionCallback().Callbacks.length == 0) {
                            ShowNotification("Task Validation Warning", String.format("Callback time does not exist.", data.Name), "gritter-warning", false);
                            return;
                        }
                    }

                    if (!self.IsAllChecked()) {
                        ShowNotification("Task Validation Warning", String.format("Please tick all checkbox to {0} this task.", data.Name), "gritter-warning", false);
                    } else {
                        if (self.ActivityTitle() == "Payment Maker Task") {
                            if (SetDisableMessageApprover()) {
                                text = $('#messageApprover').html();
                            }
                        }
                        if (self.ActivityTitle() == "PPU Maker Task" || self.ActivityTitle() == "PPU Maker After Checker and Caller Task") {
                            // fx validation submit
                            if (viewModel.t_IsFxTransaction() || viewModel.t_IsFxTransactionToIDRB()) {
                                var amountUSD = viewModel.TransactionMaker().Transaction.AmountUSD;
                                var totalUnderlying = viewModel.UtilizationAmount();
                                if (amountUSD <= parseFloat(totalUnderlying)) {
                                    var totalUtilization = viewModel.TotalUtilization();
                                    var TreshHold = viewModel.Treshold();
                                    if (totalUtilization > TreshHold && viewModel.TempSelectedUnderlying()[0].StatementLetter() == 1) {
                                        ShowNotification("Form Underlying Warning", "Total Utilization greater than " + TotalPPUModel.TreshHold + " (USD), Please add statement B underlying", 'gritter-warning', false);
                                        return false;
                                    }
                                } else {
                                    ShowNotification("Form Underlying Warning", "Total Utilization Amount must be equal greater than Transaction Amount", 'gritter-warning', false);
                                    return false;
                                }
                                viewModel.TransactionMaker().Transaction.Underlyings = viewModel.TempSelectedUnderlying();
                            } else {
                                viewModel.TransactionMaker().Transaction.Underlyings = [];
                            }
                        }
                        if (self.ActivityTitle() == "PPU Checker Task") {
                            var transactionId = viewModel.TransactionChecker().Transaction.ID;
                            var checkerData = viewModel.TransactionChecker().Checker;
                            CheckExceptionHandling(transactionId, checkerData, SaveApprovalData, text, data.ID);
                            return false;
                        }
                        bootbox.confirm(text, function (result) {
                            if (result) {
                                // store data to db
                                if ((self.ActivityTitle() == "Branch Maker Reactivation Task") || (self.ActivityTitle() == "CBO Maker Reactivation Task")) {
                                    var param = {
                                        WFID: self.WorkflowInstanceID(),
                                        ApproverID: self.ApproverId(),
                                        DataID: data.ID
                                    }
                                    UploadReactivationDoc(SaveApprovalData, self.WorkflowInstanceID(), self.ApproverId(), data.ID, param);
                                } else {
                                    if ((self.ActivityTitle() == "PPU Maker Task" || self.ActivityTitle() == "PPU Maker After Checker and Caller Task") && self.MakerDocuments().length > 0) {
                                        var param = {
                                            WFID: self.WorkflowInstanceID(),
                                            ApproverID: self.ApproverId(),
                                            DataID: data.ID
                                        }
                                        UploadDocuments(SaveApprovalData, param);
                                    } else {
                                        SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                                    }
                                }
                            }
                        });
                    }
                }

            }
        }
    };

    self.VerifyColumn = function (colName) {
        //var data =  ko.utils.arrayFirst(self.TransactionDeal().Verify(), function (item){ return item.Name == colName; });
        var data = ko.utils.arrayFirst(self.ApprovalData().Verify, function (item) { return item.Name == colName; });
        return ko.observable(data);
    };

    /*self.VerifyOnChange = function(data){
     var verify =  ko.utils.arrayFirst(self.TransactionChecker().Verify(), function (item){ return item.ID == data.ID; });
     var index = self.TransactionChecker().Verify.indexOf(verify);
     //var x = self.TransactionChecker().Verify()[index];
     //alert(JSON.stringify(x))
     if(index > -1){
     self.TransactionChecker().Verify()[index].IsVerified = data.IsVerified;
     }

     alert(JSON.stringify(self.TransactionChecker().Verify()[index]))
     };*/

    self.IsAllChecked = function () {
        var isAllChecked = true;

        if (self.TransactionChecker().Verify != undefined) {
            var skipTerms = ["Document Completeness"]; // set verify name to exclude from validation checkbox

            for (var i = 0; i < self.TransactionChecker().Verify.length; i++) {
                // skip to check Document Completeness
                if (skipTerms.indexOf(self.TransactionChecker().Verify[i].Name) == -1) {
                    if (!self.TransactionChecker().Verify[i].IsVerified) {
                        isAllChecked = false;
                        break;
                    }
                }
            }
        }

        if (self.TransactionCheckerCallback().Verify != undefined) {
            var skipTerms = ["Document Completeness"]; // set verify name to exclude from validation checkbox

            for (var i = 0; i < self.TransactionCheckerCallback().Verify.length; i++) {
                // skip to check Document Completeness
                if (skipTerms.indexOf(self.TransactionCheckerCallback().Verify[i].Name) == -1) {
                    if (!self.TransactionCheckerCallback().Verify[i].IsVerified) {
                        isAllChecked = false;
                        break;
                    }
                }
            }
        }

        if (self.TransactionCallback().Verify != undefined) {
            var skipTerms = ["Document Completeness"];

            if (self.TransactionCallback().Callbacks.length > 0) {
                // changed chandra : check all thick validation
                //if(self.TransactionCallback().Callbacks[self.TransactionCallback().Callbacks.length - 1].IsUTC != true) {
                for (var i = 0; i < self.TransactionCallback().Verify.length; i++) {
                    if (skipTerms.indexOf(self.TransactionCallback().Verify[i].Name) == -1) {
                        if (!self.TransactionCallback().Verify[i].IsVerified) {
                            isAllChecked = false;
                            break;
                        }
                    }
                }
                // }
            } else {

                for (var i = 0; i < self.TransactionCallback().Verify.length; i++) {
                    if (skipTerms.indexOf(self.TransactionCallback().Verify[i].Name) == -1) {
                        if (!self.TransactionCallback().Verify[i].IsVerified) {
                            isAllChecked = false;
                            break;
                        }
                    }
                }
            }
        }

        if (self.PaymentChecker().Verify != undefined) {
            var skipTerms = ["Charges Type"];
            var skipTerms_2 = ["Document Completeness"]; // set verify name to exclude from validation checkbox

            for (var i = 0; i < self.PaymentChecker().Verify.length; i++) {
                if (skipTerms.indexOf(self.PaymentChecker().Verify[i].Name) == -1 && skipTerms_2.indexOf(self.PaymentChecker().Verify[i].Name) == -1) {
                    if (!self.PaymentChecker().Verify[i].IsVerified) {
                        isAllChecked = false;
                        break;
                    }
                }
            }
        }

        return isAllChecked;
    };

    // rename status fx transaction & top urgent
    self.RenameStatus = function (type, value) {
        if (type == "FX Transaction") {
            if (value) {
                return "Yes";
            } else {
                return "No";
            }
        } else if (type == "Urgency") {
            if (value) {
                return "Top Urgent";
            } else {
                return "Normal";
            }
        } else {
            return "Unknown";
        }
    };

    // rename status filter fx Transaction & top urgent : add chandra
    self.RenameStatusFilter = function (type, value) {
        if (type == "FX Transaction") {
            if(value!=""){
                if (("yes").indexOf(value.toLowerCase()) > -1) {
                    return true;
                } else if (("no").indexOf(value.toLowerCase()) > -1) {
                    return false;
                } else {
                    return "-";
                }
            }else{return ""};
        } else if (type == "Urgency") {
            if(value!=""){
                if (("top urgent").indexOf(value.toLowerCase()) > -1) {
                    return true;
                } else if (("normal").indexOf(value.toLowerCase()) > -1) {
                    return false;
                } else {
                    return "-";
                }}else{ return ""};
        } else {
            // unknown parameter
            return false;
        }
    }

    // rename status fx transaction & top urgent
    self.CallbackStatus = function (type, value) {
        if (type == "FX Transaction") {
            if (value == "Yes") {
                return true;
            } else {
                return false;
            }
        } else if (type == "Urgency") {
            if (value == "Top Urgent") {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    };

    // Auto populate data from select
    self.PopulateSelected = function (form, element) {
        switch (form) {
            case "PaymentMaker":
                switch (element) {
                    case "Currency":
                        var data = ko.utils.arrayFirst(self.Currencies(),
                            function (item) {
                                return item.ID == self.Selected().Currency();
                            });

                        if (data != null) {
                            //bangkit
                            GetRateAmount("PaymentMaker", data.ID);
                            self.PaymentMaker().Payment.Currency = data;
                        }
                        //alert(JSON.stringify(self.PaymentMaker().Payment.Currency))
                        break;
                    case "FXCompliance":
                        var data = ko.utils.arrayFirst(self.FXCompliances(),
                            function (item) {
                                return item.ID == self.Selected().FXCompliance;
                            });

                        if (data != null) {
                            self.PaymentMaker().Payment.FXCompliance = data;
                        }
                        break;
                    case "AccountNumber":
                        var data = ko.utils.arrayFirst(self.PaymentMaker().Payment.Customer.Accounts,
                            function (item) {
                                return item.AccountNumber == self.Selected().Account();
                            });

                        if (data != null) {
                            self.PaymentMaker().Payment.Account = data;
                        }
                        //alert(JSON.stringify(self.PaymentMaker().Payment.Account))
                        break;
                    case "Bank":
                        var data = ko.utils.arrayFirst(self.Banks(),
                            function (item) {
                                return item.ID == self.Selected().Bank();
                            });

                        if (data != null) {
                            self.PaymentMaker().Payment.Bank = data;
                        }
                        //alert(JSON.stringify(self.PaymentMaker().Payment.Account))
                        break;
                    case "ProductType":
                        var data = ko.utils.arrayFirst(self.ProductTypes(),
                            function (item) {
                                return item.ID == self.Selected().ProductType();
                            });

                        if (data != null) {
                            self.PaymentMaker().Payment.ProductType = data;
                        }
                        break;
                    case "LLD":
                        var data = ko.utils.arrayFirst(self.LLDs(),
                            function (item) {
                                return item.ID == self.Selected().LLD();
                            });

                        if (data != null) {
                            self.PaymentMaker().Payment.LLD = data;
                            self.LLDCode(data.Description);
                        }
                        else {
                            self.LLDCode('');
                        }
                        break;
                    case "UnderlyingDoc":
                        var data = ko.utils.arrayFirst(self.UnderlyingDocs(),
                            function (item) {
                                return item.ID == self.Selected().UnderlyingDoc();
                            });

                        if (data != null) {
                            if (data.Code == '999') {
                                self.isNewUnderlying(true);
                                self.PaymentMaker().Payment.UnderlyingDoc.Name = self.PaymentMaker().Payment.OtherUnderlyingDoc;
                            }
                            else {
                                self.isNewUnderlying(false);
                            }
                            self.PaymentMaker().Payment.UnderlyingDoc = data;
                        }
                        break;
                    case "BankCharges":
                        var data = ko.utils.arrayFirst(self.BankCharges(),
                            function (item) {
                                return item.ID == self.Selected().BankCharges();
                            });

                        if (data != null) {
                            self.PaymentMaker().Payment.BankCharges = data;
                        }
                        break;
                    case "AgentCharges":
                        var data = ko.utils.arrayFirst(self.AgentCharges(),
                            function (item) {
                                return item.ID == self.Selected().AgentCharges();
                            });

                        if (data != null) {
                            self.PaymentMaker().Payment.AgentCharges = data;
                        }
                        console.log(ko.toJSON(data));
                        break;
                }
                break;
            case "TransactionMaker":
                switch (element) {
                    case "CustomerName":
                        self.TransactionMaker().Transaction.BizSegment = self.TransactionMaker().Transaction.Customer.BizSegment;
                        break;
                    case "Currency":
                        var data = ko.utils.arrayFirst(self.Currencies(),
                            function (item) {
                                return item.ID == self.Selected().Currency();
                            });

                        if (data != null) {
                            //bangkit
                            GetRateAmount("TransactionMaker", data.ID);
                            self.TransactionMaker().Transaction.Currency = data;
                        }
                        break;
                    case "DebitCurrency":
                        var data = ko.utils.arrayFirst(self.DebitCurrencies(),
                            function (item) {
                                return item.ID == self.Selected().DebitCurrency();
                            });

                        if (data != null) {
                            self.TransactionMaker().Transaction.DebitCurrency = data;
                        }
                        break;
                    case "AccountNumber":
                        var account = typeof (self.Selected().Account) == 'function' ? self.Selected().Account() : self.Selected().Account;
                        var data = ko.utils.arrayFirst(self.TransactionMaker().Transaction.Customer.Accounts,
                            function (item) {
                                return item.AccountNumber == account;
                            });

                        if (data != null) {
                            if (account != '-') {
                                self.IsOtherAccountNumber(false);
                            }

                            self.TransactionMaker().Transaction.Account = data;

                            if (self.IsOtherAccountNumber() || self.TransactionMaker().Transaction.Account.AccountNumber == '-') {
                                //self.TransactionMaker().Transaction.Account.AccountNumber = null;
                                self.IsOtherAccountNumber(true);
                                self.TransactionMaker().Transaction.IsOtherAccountNumber = true;
                                $('#other-debit-acc-number').focus();
                            }
                            else {
                                //self.TransactionMaker().Transaction.OtherAccountNumber = null;
                                var DebitCurrency = self.TransactionMaker().Transaction.Account.Currency;
                                $('#debit-acc-ccy').val(DebitCurrency.Code);
                                self.TransactionMaker().Transaction.DebitCurrency = DebitCurrency;
                                self.IsOtherAccountNumber(false);
                                self.TransactionMaker().Transaction.IsOtherAccountNumber = false;
                            }
                        }
                        break;
                    case "Bank":
                        var data = ko.utils.arrayFirst(self.Banks(),
                            function (item) {
                                return item.ID == self.Selected().Bank();
                            });

                        if (data != null) {
                            self.TransactionMaker().Transaction.Bank = data;
                        }
                        break;
                    case "ProductType":
                        var data = ko.utils.arrayFirst(self.ProductTypes(),
                            function (item) {
                                return item.ID == self.Selected().ProductType();
                            });

                        if (data != null) {
                            self.TransactionMaker().Transaction.ProductType = data;
                        }
                        break;
                    case "LLD":
                        var data = ko.utils.arrayFirst(self.LLDs(),
                            function (item) {
                                return item.ID == self.Selected().LLD();
                            });

                        if (data != null) {
                            self.TransactionMaker().Transaction.LLD = data;
                            self.LLDCode(data.Description);
                        }
                        else {
                            self.LLDCode('');
                        }
                        break;
                    case "UnderlyingDoc":
                        var data = ko.utils.arrayFirst(self.UnderlyingDocs(),
                            function (item) {
                                return item.ID == self.Selected().UnderlyingDoc();
                            });

                        if (data != null) {
                            if (data.Code == '999') {
                                self.isNewUnderlying(true);
                                self.TransactionMaker().Transaction.UnderlyingDoc.Name = self.TransactionMaker().Transaction.OtherUnderlyingDoc;
                            }
                            else {
                                self.isNewUnderlying(false);
                            }
                            self.TransactionMaker().Transaction.UnderlyingDoc = data;
                        }
                        break;
                }
                break;
            case "TransactionMakerAfterCaller":
                switch (element) {
                    case "CustomerName":
                        self.TransactionMaker().Transaction.BizSegment = self.TransactionMaker().Transaction.Customer.BizSegment;
                        break;
                    case "Currency":
                        var data = ko.utils.arrayFirst(self.Currencies(),
                            function (item) {
                                return item.ID == self.Selected().Currency();
                            });

                        if (data != null) {
                            //bangkit
                            GetRateAmount("TransactionMakerAfterCaller", data.ID);
                            self.TransactionMaker().Transaction.Currency = data;
                        }
                        break;
                    case "AccountNumber":
                        var account = typeof (self.Selected().Account) == 'function' ? self.Selected().Account() : self.Selected().Account;
                        var data = ko.utils.arrayFirst(self.TransactionMaker().Transaction.Customer.Accounts,
                            function (item) {
                                return item.AccountNumber == account;
                            });

                        if (data != null) {
                            if (account != '-') {
                                self.IsOtherAccountNumber(false);
                            }

                            self.TransactionMaker().Transaction.Account = data;

                            if (self.IsOtherAccountNumber() || self.TransactionMaker().Transaction.Account.AccountNumber == '-') {
                                //self.TransactionMaker().Transaction.Account.AccountNumber = null;
                                self.IsOtherAccountNumber(true);
                                self.TransactionMaker().Transaction.IsOtherAccountNumber = true;
                                $('#other-debit-acc-number').focus();
                            }
                            else {
                                //self.TransactionMaker().Transaction.OtherAccountNumber = null;
                                var DebitCurrency = self.TransactionMaker().Transaction.Account.Currency;
                                $('#debit-acc-ccy').val(DebitCurrency.Code);
                                self.TransactionMaker().Transaction.DebitCurrency = DebitCurrency;
                                self.IsOtherAccountNumber(false);
                                self.TransactionMaker().Transaction.IsOtherAccountNumber = false;
                            }
                        }
                        break;
                    case "Bank":
                        var data = ko.utils.arrayFirst(self.Banks(),
                            function (item) {
                                return item.ID == self.Selected().Bank();
                            });

                        if (data != null) {
                            self.TransactionMaker().Transaction.Bank = data;
                        }
                        break;
                    case "ProductType":
                        var data = ko.utils.arrayFirst(self.ProductTypes(),
                            function (item) {
                                return item.ID == self.Selected().ProductType();
                            });

                        if (data != null) {
                            self.TransactionMaker().Transaction.ProductType = data;
                        }
                        break;
                    case "LLD":
                        var data = ko.utils.arrayFirst(self.LLDs(),
                            function (item) {
                                return item.ID == self.Selected().LLD();
                            });

                        if (data != null) {
                            self.TransactionMaker().Transaction.LLD = data;
                            self.LLDCode(data.Description);
                        }
                        else {
                            self.LLDCode('');
                        }
                        break;
                    case "UnderlyingDoc":
                        var data = ko.utils.arrayFirst(self.UnderlyingDocs(),
                            function (item) {
                                return item.ID == self.Selected().UnderlyingDoc();
                            });

                        if (data != null) {
                            if (data.Code == '999') {
                                self.isNewUnderlying(true);
                                self.TransactionMaker().Transaction.UnderlyingDoc.Name = self.TransactionMaker().Transaction.OtherUnderlyingDoc;
                            }
                            else {
                                self.isNewUnderlying(false);
                            }
                            self.TransactionMaker().Transaction.UnderlyingDoc = data;
                        }
                        break;
                }
                break;
            case "TransactionChecker":
                switch (element) {
                    case "LLD":
                        var data = ko.utils.arrayFirst(self.LLDs(),
                            function (item) {
                                return item.ID == self.Selected().LLD();
                            });

                        if (data != null) {
                            self.TransactionChecker().Checker.LLD = data;
                            self.TransactionChecker().Transaction.LLD = data;
                            self.LLDCode(data.Description);
                        }
                        else {
                            self.LLDCode('');
                        }
                        break;
                }
                break;
            case "TransactionCaller":
                switch (element) {
                    case "UnderlyingDoc":
                        var data = ko.utils.arrayFirst(self.UnderlyingDocs(),
                            function (item) {
                                return item.ID == self.Selected().UnderlyingDoc();
                            });

                        if (data != null) {
                            if (data.Code == '999') {
                                self.isNewUnderlying(true);
                                self.TransactionCallback().Transaction.UnderlyingDoc.Name = self.TransactionCallback().Transaction.OtherUnderlyingDoc;
                            }
                            else {
                                self.isNewUnderlying(false);
                            }
                            self.TransactionCallback().Transaction.UnderlyingDoc = data;
                        }
                        break;
                    case "LLD":
                        var data = ko.utils.arrayFirst(self.LLDs(),
                            function (item) {
                                return item.ID == self.Selected().LLD();
                            });

                        if (data != null) {
                            self.TransactionCallback().Transaction.LLD = data;
                            self.LLDCode(data.Description);
                        }
                        else {
                            self.LLDCode('');
                        }
                        break;
                    case "AccountNumber":
                        var account = typeof (self.Selected().Account) == 'function' ? self.Selected().Account() : self.Selected().Account;
                        var data = ko.utils.arrayFirst(self.TransactionCallback().Transaction.Customer.Accounts,
                            function (item) {
                                return item.AccountNumber == account;
                            });

                        if (data != null) {
                            if (account != '-') {
                                self.IsOtherAccountNumber(false);
                            }

                            self.TransactionCallback().Transaction.Account = data;

                            if (self.IsOtherAccountNumber() || self.TransactionCallback().Transaction.Account.AccountNumber == '-') {
                                //self.TransactionCallback().Transaction.Account.AccountNumber = null;
                                self.IsOtherAccountNumber(true);
                                self.TransactionCallback().Transaction.IsOtherAccountNumber = true;
                                $('#other-debit-acc-number').focus();
                            }
                            else {
                                //self.TransactionCallback().Transaction.OtherAccountNumber = null;
                                var DebitCurrency = self.TransactionCallback().Transaction.Account.Currency;
                                $('#debit-acc-ccy').val(DebitCurrency.Code);
                                self.TransactionCallback().Transaction.DebitCurrency = DebitCurrency;
                                self.IsOtherAccountNumber(false);
                                self.TransactionCallback().Transaction.IsOtherAccountNumber = false;
                            }
                        }
                        break;
                    case "DebitCurrency":
                        var data = ko.utils.arrayFirst(self.Currencies(),
                            function (item) {
                                return item.ID == self.Selected().DebitCurrency();
                            });

                        if (data != null) {
                            self.TransactionCallback().Transaction.DebitCurrency = data;
                        }
                        break;
                }
                break;
            case "TransactionCheckerAfterCaller":
                switch (element) {
                    case "UnderlyingDoc":
                        var data = ko.utils.arrayFirst(self.UnderlyingDocs(),
                            function (item) {
                                return item.ID == self.Selected().UnderlyingDoc();
                            });

                        if (data != null) {
                            if (data.Code == '999') {
                                self.isNewUnderlying(true);
                                self.TransactionCheckerCallback().Transaction.UnderlyingDoc.Name = self.TransactionCallback().Transaction.OtherUnderlyingDoc;
                            }
                            else {
                                self.isNewUnderlying(false);
                            }
                            self.TransactionCheckerCallback().Transaction.UnderlyingDoc = data;
                        }
                        break;
                    case "LLD":
                        var data = ko.utils.arrayFirst(self.LLDs(),
                            function (item) {
                                return item.ID == self.Selected().LLD();
                            });

                        if (data != null) {
                            self.TransactionCheckerCallback().Transaction.LLD = data;
                            self.LLDCode(data.Description);
                        }
                        else {
                            self.LLDCode('');
                        }
                        break;
                }
                break;
        }

        // re-updating observable. This method will updating the UI (view)
        self.UpdateTemplateUI(form);

        //bind auto complete
        viewModel.SetBankAutoComplete('');
    };

    self.IsChecked = function (transactioncode, callbackcode, isverified, name) {
        var verify = ko.utils.arrayFilter(self.TransactionCheckerCallback().Verify, function (item) { return item.Name == name; });
        var index = self.TransactionCheckerCallback().Verify.indexOf(verify[0]);

        if (verify != null) {
            var index = self.TransactionCheckerCallback().Verify.indexOf(verify[0]);

            if (transactioncode == callbackcode) {
                self.TransactionCheckerCallback().Verify[index].IsVerified = true;
                return true;
            } else {
                self.TransactionCheckerCallback().Verify[index].IsVerified = isverified;
                return isverified;
            }
        }
    }

    self.IsVerifiedChecked = function (data) {
        var verify = ko.utils.arrayFilter(self.TransactionCheckerCallback().Verify, function (item) { return item.Name == data.Name; });

        if (verify != null) {
            var index = self.TransactionCheckerCallback().Verify.indexOf(verify[0]);

            if (verify[0].IsVerified) {
                self.TransactionCheckerCallback().Verify[index].IsVerified = false;
            } else {
                self.TransactionCheckerCallback().Verify[index].IsVerified = true;
            }
        }
        self.UpdateTemplateUI("TransactionCheckerAfterCaller");
    }

    self.UpdateTemplateUI = function (form) {
        // re-updating observable. This method will updating the UI (view)
        switch (form) {
            case "PaymentMaker":
                self.Rate = formatNumber(self.PaymentMaker().Payment.Rate);
                self.Amount = formatNumber(self.PaymentMaker().Payment.Amount);
                var update = self.PaymentMaker();
                self.PaymentMaker(ko.mapping.toJS(update));
                break;
            case "TransactionMaker":
                //var AccountCurrency = self.TransactionMaker().Transaction.DebitCurrency.Code;
                self.Rate = formatNumber(self.TransactionMaker().Transaction.Rate);
                self.Amount = formatNumber(self.TransactionMaker().Transaction.Amount);
                var update = self.TransactionMaker();
                self.TransactionMaker(ko.mapping.toJS(update));

                var DebitAccountNumber = null;

                if (!self.TransactionMaker().Transaction.IsOtherAccountNumber) {
                    DebitAccountNumber = self.TransactionMaker().Transaction.Account.AccountNumber;
                }

                if (DebitAccountNumber == null || DebitAccountNumber == '-' || self.TransactionMaker().Transaction.IsOtherAccountNumber == true) {

                    self.IsOtherAccountNumber(true);
                    self.TransactionMaker().Transaction.IsOtherAccountNumber = true;
                    if (self.TransactionMaker().Transaction.DebitCurrency != null)
                        self.Selected().DebitCurrency(self.TransactionMaker().Transaction.DebitCurrency.ID);
                    $('#other-debit-acc-number').focus();
                    $('#debit-acc-number').val('-');
                }
                else {
                    var DebitCurrency = self.TransactionMaker().Transaction.Account.Currency;
                    $('#debit-acc-ccy').val(self.TransactionMaker().Transaction.DebitCurrency.Code);
                    self.TransactionMaker().Transaction.DebitCurrency = DebitCurrency;
                    self.IsOtherAccountNumber(false);
                    self.TransactionMaker().Transaction.IsOtherAccountNumber = false;
                    if (self.TransactionMaker().Transaction.Account != null)
                        self.Selected().Account(self.TransactionMaker().Transaction.Account.AccountNumber);
                }

                //self.Selected().Account(self.TransactionMaker().Transaction.Account.AccountNumber);
                //self.TransactionMaker().Transaction.DebitCurrency.Code = AccountCurrency;

                break;
            case "TransactionMakerAfterCaller":
                /*self.Rate = formatNumber(self.TransactionMaker().Transaction.Rate);
                 self.Amount = formatNumber(self.TransactionMaker().Transaction.Amount);
                 var update = self.TransactionMaker();
                 self.TransactionMaker(ko.mapping.toJS(update));
                 self.Selected().Account(self.TransactionMaker().Transaction.Account.AccountNumber);
                 break;*/
                //var AccountCurrency = self.TransactionMaker().Transaction.DebitCurrency.Code;
                self.Rate = formatNumber(self.TransactionMaker().Transaction.Rate);
                self.Amount = formatNumber(self.TransactionMaker().Transaction.Amount);
                var update = self.TransactionMaker();
                self.TransactionMaker(ko.mapping.toJS(update));

                var DebitAccountNumber = null;

                if (!self.TransactionMaker().Transaction.IsOtherAccountNumber) {
                    DebitAccountNumber = self.TransactionMaker().Transaction.Account.AccountNumber;
                }

                if (DebitAccountNumber == null || DebitAccountNumber == '-' || self.TransactionMaker().Transaction.IsOtherAccountNumber == true) {

                    self.IsOtherAccountNumber(true);
                    self.TransactionMaker().Transaction.IsOtherAccountNumber = true;
                    self.Selected().Account('-');
                    if (self.TransactionMaker().Transaction.DebitCurrency != null)
                        self.Selected().DebitCurrency(self.TransactionMaker().Transaction.DebitCurrency.ID);
                    $('#other-debit-acc-number').focus();
                }
                else {
                    var DebitCurrency = self.TransactionMaker().Transaction.Account.Currency;
                    $('#debit-acc-ccy').val(self.TransactionMaker().Transaction.DebitCurrency.Code);
                    self.TransactionMaker().Transaction.DebitCurrency = DebitCurrency;
                    self.IsOtherAccountNumber(false);
                    self.TransactionMaker().Transaction.IsOtherAccountNumber = false;
                    if (self.TransactionMaker().Transaction.Account != null)
                        self.Selected().Account(self.TransactionMaker().Transaction.Account.AccountNumber);
                }

                //self.Selected().Account(self.TransactionMaker().Transaction.Account.AccountNumber);
                //self.TransactionMaker().Transaction.DebitCurrency.Code = AccountCurrency;
                //$('#debit-acc-ccy').val(AccountCurrency);
                break;
            case "TransactionCheckerAfterCaller":
                //self.Rate = formatNumber(self.TransactionMaker().Transaction.Rate);
                var update = self.TransactionCheckerCallback();
                self.TransactionCheckerCallback(ko.mapping.toJS(update));
                break;
            case "TransactionContactMaker":
                //self.Rate = formatNumber(self.TransactionMaker().Transaction.Rate);
                var update = self.TransactionContact();
                self.TransactionContact(ko.mapping.toJS(update));
                break;
            case "PaymentChecker":
                var update = self.PaymentChecker();
                self.PaymentChecker(ko.mapping.toJS(update));
                break;
            case "TransactionChecker":
                var update = self.TransactionChecker();
                self.TransactionChecker(ko.mapping.toJS(update));
                break;
            case "TransactionCaller":

                //var AccountCurrency = self.TransactionCallback().Transaction.DebitCurrency.Code;
                var update = self.TransactionCallback();
                self.TransactionCallback(ko.mapping.toJS(update));

                var DebitAccountNumber = null;

                if (!self.TransactionCallback().Transaction.IsOtherAccountNumber) {
                    DebitAccountNumber = self.TransactionCallback().Transaction.Account.AccountNumber;
                }

                if (DebitAccountNumber == null || DebitAccountNumber == '-' || self.TransactionCallback().Transaction.IsOtherAccountNumber == true) {
                    self.IsOtherAccountNumber(true);
                    self.TransactionCallback().Transaction.IsOtherAccountNumber = true;
                    self.Selected().Account('-');
                    if (self.TransactionCallback().Transaction.DebitCurrency != null)
                        self.Selected().DebitCurrency(self.TransactionCallback().Transaction.DebitCurrency.ID);
                    $('#other-debit-acc-number').focus();
                }
                else {
                    var DebitCurrency = self.TransactionCallback().Transaction.DebitCurrency;
                    $('#debit-acc-ccy').val(self.TransactionCallback().Transaction.DebitCurrency.Code);
                    self.TransactionCallback().Transaction.DebitCurrency = DebitCurrency;
                    self.IsOtherAccountNumber(false);
                    self.TransactionCallback().Transaction.IsOtherAccountNumber = false;
                    if (self.TransactionCallback().Transaction.Account != null)
                        self.Selected().Account(self.TransactionCallback().Transaction.Account.AccountNumber);
                }
                //$('#debit-acc-ccy').val(AccountCurrency);
                break;

        }

        SetAutoComplete(form);
    };

    // Ruddy 2014.11.09 : Generate Transaction Status
    // Update Rizki 2015-03-06
    self.FormatTransactionStatus = function (workflowStateDesc, activityTitle, applicationID, outcomeDescription, customOutcomeDescription) {
        var output;

        if (outcomeDescription == null) {
            outcomeDescription = '';
        }

        if (customOutcomeDescription == null) {
            customOutcomeDescription = '';
        }

        if (workflowStateDesc.toLowerCase() == 'running') {
            output = activityTitle;
        }
        else if (workflowStateDesc.toLowerCase() == 'complete') {
            //Untuk handle FX Checker reject dan PPU Checker Approve Cancellation
            if ((applicationID.toLowerCase().startsWith('fx') && outcomeDescription.toLowerCase() == 'rejected') || (customOutcomeDescription.toLowerCase() == 'approve cancellation')) {
                output = 'Cancelled'
            }
            else {
                output = 'Completed';
            }
        }
        else {
            output = workflowStateDesc;
        }

        //if (outcome != 'Custom') {
        //output = String.format("{0} by {1}", outcome, user);
        //} else {
        //output = String.format("{0} by {1}", customOutcome, user);
        //}

        return output;
    };


    // Chandra 2015.02.24 : Showing edit underlying on ppu maker
    // Uploading document fx document
    self.UploadDocumentUnderlying = function () {
        // show the dialog task form
        if (self.MakerUnderlyings().length > 0) {
            self.IsUploading(false);
            $("#modal-form-Attach").modal('show');
            $('.remove').click();
            self.ID_a(0);
            self.ddlDocumentPurpose_a(self.DocumentPurposes());
            self.DocumentPurpose_a(null);
            self.Selected().DocumentPurpose_a(null);
            self.DocumentType_a(null);
            self.Selected().DocumentType_a(null);
            self.DocumentPath_a(null);
            $('#backDrop').show();
            self.TempSelectedAttachUnderlying([]);
            GetCustomerUnderlying();
        } else {
            ShowNotification("", "Please select utilize underlying form the table.", 'gritter-warning', true);
        }
    }

    self.onSelectionUtilize = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
            return x.ID() == item.ID;
        });
        if (data != null) { //uncheck condition
            self.TempSelectedUnderlying.remove(data);
            self.MakerUnderlyings.remove(item.ID); //remove underlying for maker
            ResetDataUnderlying(index, false);
            setTotalUtilize();
            setMakerDocuments();
        } else {
            var statementA = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                return x.StatementLetter() == 1;
            });
            var statementB = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                return x.StatementLetter() == 2;
            });
            var amount = document.getElementById("eqv-usd").value.replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '');
            if (((statementA == null && item.StatementLetter.ID == 2) || (statementB == null && item.StatementLetter.ID == 1))
            //&& parseFloat(self.TransactionModel().utilizationAmount()) < parseFloat(amount)
                ) {
                self.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, false, item.AvailableAmount, item.StatementLetter.ID));
                self.MakerUnderlyings.push(item.ID);
                ResetDataUnderlying(index, true);
                setTotalUtilize();
                setMakerDocuments();
            } else {
                ResetDataUnderlying(index, false);
            }
        }

    };
    function setMakerDocuments() {
        // remove all underlying documents
        ko.utils.arrayForEach(self.MakerAllDocumentsTemp(), function (item) {
            if (item.Purpose.ID == 2) {
                self.MakerDocuments.remove(item);
                self.TransactionMaker().Transaction.Documents = jQuery.grep(self.TransactionMaker().Transaction.Documents, function (value) {
                    return value.ID != item.ID;
                });
            }
        });
        // push underlying by makerunderlyings
        ko.utils.arrayForEach(self.MakerAllDocumentsTemp(), function (item) {
            ko.utils.arrayForEach(self.MakerUnderlyings(), function (item2) {
                if (item2 == item.UnderlyingID && item.Purpose.ID == 2) {
                    self.MakerDocuments.push(item);
                    self.TransactionMaker().Transaction.Documents.push(item);
                }
            });
        });
    }
    self.onSelectionAttach = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            self.TempSelectedAttachUnderlying.remove(data);
            ResetDataAttachment(index, false);

        } else {
            self.TempSelectedAttachUnderlying.push({
                ID: item.ID
            });
            ResetDataAttachment(index, true);
        }
    }
    self.OnChangePropose = function () {
        if (self.Selected().DocumentPurpose_a() == 2) {
            self.SelectingUnderlying(true);
        } else {
            self.SelectingUnderlying(false)
        }
    }
    // Chandra 2015.02.24 : Get Underlying data for attach files
    function GetCustomerUnderlying() {
        var _cif = viewModel.TransactionMaker().Transaction.Customer.CIF;
        var options = {
            url: api.server + api.url.customerunderlying + "/Attachment",
            params: { cif: _cif },
            token: accessToken
        };
        // get filtered columns
        var filters = self.MakerUnderlyings();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataAttachFile1, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataAttachFile1, OnError, OnAlways);
        }
    }

    // On success GetData Underlying for AttachFile
    function OnSuccessGetDataAttachFile1(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            for (var i = 0 ; i < data.length; i++) {
                var selected = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (item) {
                    return item.ID == data[i].ID;
                });
                if (selected != null) {
                    data[i].IsSelectedAttach = true;
                }
                else {
                    data[i].IsSelectedAttach = false;
                }
            }
            self.CustomerAttachUnderlyings(data);

        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }
    // chandra 2015.02.26 : upload attachment of ppu maker task
    self.save_a = function () {
        /* var form = $("#frmUnderlying");
         form.validate();
         if (form.valid()) {*/
        self.IsUploading(true);
        var doc = {
            ID: 0,
            FileName: AttachDocuments.DocumentPath() != null ? AttachDocuments.DocumentPath().name : null,
            DocumentPath: self.DocumentPath_a(),
            Type: ko.utils.arrayFirst(self.ddlDocumentType_a(), function (item) {
                return item.ID == self.Selected().DocumentType_a();
            }),
            Purpose: ko.utils.arrayFirst(self.ddlDocumentPurpose_a(), function (item) {
                return item.ID == self.Selected().DocumentPurpose_a();
            }),
            IsNewDocument: true,
            LastModifiedDate: new Date(),
            LastModifiedBy: null
        };
        if (doc.Type == null || doc.Purpose == null || doc.DocumentPath == null || doc.FileName == null) {
            ShowNotification("", "Please complete the upload form fields.", 'gritter-warning', true);
            self.IsUploading(false);
        } else {
            var FxWithUnderlying = (self.TempSelectedAttachUnderlying().length > 0);
            var FxWithNoUnderlying = (self.Selected().DocumentPurpose_a() != 2);
            if (FxWithUnderlying || FxWithNoUnderlying) {
                var dataCIF = null;
                var dataName = null;
                switch (self.ActivityTitle()) {
                    case "PPU Maker Task":
                    case "PPU Maker After Checker and Caller Task":
                        dataCIF = viewModel.TransactionMaker().Transaction.Customer.CIF;
                        dataName = viewModel.TransactionMaker().Transaction.Customer.Name;
                        break;
                    default:
                        dataCIF = viewModel.TransactionChecker().Transaction.Customer.CIF;
                        dataName = viewModel.TransactionChecker().Transaction.Customer.Name;
                }
                var data = {
                    CIF: dataCIF,
                    Name: dataName,
                    Type: ko.utils.arrayFirst(self.ddlDocumentType_a(), function (item) {
                        return item.ID == self.Selected().DocumentType_a();
                    }),
                    Purpose: ko.utils.arrayFirst(self.ddlDocumentPurpose_a(), function (item) {
                        return item.ID == self.Selected().DocumentPurpose_a();
                    })
                };
                if (FxWithUnderlying && self.Selected().DocumentPurpose_a() == 2) {
                    UploadFileUnderlying(data, doc, SaveDataFile);
                }
                if (FxWithNoUnderlying) {
                    self.MakerDocuments.push(doc);
                }
                $("#modal-form-Attach").modal('hide');
                $("#modal-form-Attach1").modal('hide');
                $('#backDrop').hide();
            } else {
                ShowNotification("", "Please select underlying form the table.", 'gritter-warning', true);
                self.IsUploading(false);
            }
        }
        //}
    }
    self.cancel_a = function () {
        $("#modal-form-Attach").modal('hide');
        $('#backDrop').hide();
    }


    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterWorkflowName() != "") filters.push({ Field: 'WorkflowName', Value: self.FilterWorkflowName() });
        if (self.FilterInitiator() != "") filters.push({ Field: 'Initiator', Value: self.FilterInitiator() });
        if (self.FilterStateDescription() != "") filters.push({ Field: 'StateDescription', Value: self.FilterStateDescription() });
        if (self.FilterStartTime() != "") filters.push({ Field: 'StartTime', Value: self.FilterStartTime() });
        if (self.FilterTitle() != "") filters.push({ Field: 'Title', Value: self.FilterTitle() });
        if (self.FilterActivityTitle() != "") filters.push({ Field: 'ActivityTitle', Value: self.FilterActivityTitle() });
        if (self.FilterUser() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterUser() });
        if (self.FilterUserApprover() != "") filters.push({ Field: 'UserApprover', Value: self.FilterUserApprover() });
        if (self.FilterEntryTime() != "") filters.push({ Field: 'EntryTime', Value: self.FilterEntryTime() });
        if (self.FilterExitTime() != "") filters.push({ Field: 'ExitTime', Value: self.FilterExitTime() });
        if (self.FilterOutcomeDescription() != "") filters.push({ Field: 'OutcomeDescription', Value: self.FilterOutcomeDescription() });
        if (self.FilterCustomOutcomeDescription() != "") filters.push({ Field: 'CustomOutcomeDescription', Value: self.FilterCustomOutcomeDescription() });
        if (self.FilterComments() != "") filters.push({ Field: 'Comments', Value: self.FilterComments() });


        if (self.FilterApplicationID() != "") filters.push({ Field: 'ApplicationID', Value: self.FilterApplicationID() });
        if (self.FilterCustomer() != "") filters.push({ Field: 'Customer', Value: self.FilterCustomer() });
        if (self.FilterProduct() != "") filters.push({ Field: 'Product', Value: self.FilterProduct() });
        if (self.FilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.FilterCurrency() });
        if (self.FilterAmount() != "") filters.push({ Field: 'Amount', Value: self.FilterAmount() });
        if (self.FilterAmountUSD() != "") filters.push({ Field: 'AmountUSD', Value: self.FilterAmountUSD() });
        if (self.FilterDebitAccNumber() != "") filters.push({ Field: 'DebitAccNumber', Value: self.FilterDebitAccNumber() });
        if (self.FilterFXTransaction() != "") filters.push({ Field: 'IsFXTransaction', Value: self.RenameStatusFilter('FX Transaction', self.FilterFXTransaction()) });
        if (self.FilterTopUrgent() != "") filters.push({ Field: 'IsTopUrgent', Value: self.RenameStatusFilter('Urgency', self.FilterTopUrgent()) });
        //if (self.FilterFXTransaction() != "") filters.push({ Field: 'IsFXTransaction', Value: self.CallbackStatus('FX Transaction', self.FilterFXTransaction()) });
        //if (self.FilterTopUrgent() != "") filters.push({ Field: 'IsTopUrgent', Value: self.CallbackStatus('Urgency', self.FilterTopUrgent()) });

        return filters;
    };

    // get all parameters need
    function GetParameters() {
        var options = {
            url: api.server + api.url.parameter,
            params: {
                select: "Product,Currency,Channel,BizSegment,Bank,BankCharge,AgentCharge,FXCompliance,ChargesType,ProductType,lld,UnderlyingDoc,POAFunction,DocType,PurposeDoc"
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
    }

    // Get data / refresh data
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') { $remove = true; $box.addClass('position-relative'); }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        // widget reloader function end

        // declare options variable for ajax get request
        var options = null;
        options = {
            url: api.server + api.url.mytask,
            params: {
                webid: config.sharepoint.webId,
                siteid: config.sharepoint.siteId,
                workflowids: config.sharepoint.workflowId,
                state: self.WorkflowConfig().State,
                outcome: self.WorkflowConfig().Outcome,
                //customOutcome:  "1,2,3,4,5,6,7,8",
                customOutcome: self.WorkflowConfig().CustomOutcome,
                showContribute: self.WorkflowConfig().ShowContribute,
                showActiveTask: self.WorkflowConfig().ShowActiveTask,
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        /*if(self.WokflowConfig().CustomOutcome != undefined){
         options.params.customOutcome = self.WokflowConfig().CustomOutcome;
         }*/

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }

    // get Transaction data
    function GetTransactionData(instanceId, approverId) {
        var endPointURL;

        switch (self.ActivityTitle()) {
            case "PPU Maker Task":
                //endPointURL = api.server + api.url.workflow.transactionChecker(instanceId, approverId);
                endPointURL = api.server + api.url.workflow.transactionMakerReviseAfterPPU(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "PPU Checker Cancellation Task":
                endPointURL = api.server + api.url.workflow.transactionChecker(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "PPU Maker After Checker and Caller Task":
                //endPointURL = api.server + api.url.workflow.transactionCheckerCallback(instanceId, approverId);
                endPointURL = api.server + api.url.workflow.transactionMakerReviseAfterCaller(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "PPU Checker Task":
                endPointURL = api.server + api.url.workflow.transactionChecker(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "PPU Checker After PPU Caller Task":
                endPointURL = api.server + api.url.workflow.transactionCheckerCallback(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "PPU Caller Task":
                endPointURL = api.server + api.url.workflow.transactionCallback(instanceId, approverId);
                self.IsTimelines(true);
                break;
            //case "PPU Checker Canceled Task":
            case "CBO Maker Task":
                endPointURL = api.server + api.url.workflow.transactionContact(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "CBO Checker Task":
                endPointURL = api.server + api.url.workflow.transactionContact(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "Branch Maker Reactivation Task":
                endPointURL = api.server + api.url.workflow.transactionChecker(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "Branch Checker Reactivation Task":
                endPointURL = api.server + api.url.workflow.transactionChecker(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "CBO Maker Reactivation Task":
                endPointURL = api.server + api.url.workflow.transactionChecker(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "CBO Checker Reactivation Task":
                endPointURL = api.server + api.url.workflow.transactionChecker(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "Payment Maker Task":
                endPointURL = api.server + api.url.workflow.paymentMaker(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "Payment Checker Task":
                endPointURL = api.server + api.url.workflow.paymentChecker(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "Lazy Approval Task for Exceptional Handling Case":
                endPointURL = api.server + api.url.workflow.transactionChecker(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "FX Deal Checker Task":
                endPointURL = api.server + api.url.workflow.transactionDeal(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "Pending Documents Checker Task":
                endPointURL = api.server + api.url.workflow.transactionDocuments(instanceId, approverId);
                self.IsTimelines(false);
                break;
            case "Lazy App fo Except":
                endPointURL = api.server + api.url.workflow.transactionChecker(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "Lazy Approval Task for UTC Case":
                endPointURL = api.server + api.url.workflow.transactionChecker(instanceId, approverId);
                self.IsTimelines(true);
                break;
        }

        // declare options variable for ajax get request
        var options = {
            url: endPointURL,
            token: accessToken
        };

        // show progress bar
        $("#transaction-progress").show();

        // hide transaction data
        $("#transaction-data").hide();

        // call ajax
        Helper.Ajax.Get(options, OnSuccessGetTransactionData, OnError, function () {
            // hide progress bar
            $("#transaction-progress").hide();

            // show transaction data
            $("#transaction-data").show();
        });
    }

    //bangkit
    function UpdateApprovalData(instanceId, approverId, outcomeId) {
        var endPointURL;
        var body;

        switch (self.ActivityTitle()) {
            case "PPU Checker Task":
                endPointURL = api.server + api.url.workflow.transactionCheckerUpdate(instanceId, approverId);
                body = ko.toJS(self.TransactionChecker());
                break;
            case "Payment Checker Task":
                endPointURL = api.server + api.url.workflow.transactionCheckerUpdate(instanceId, approverId);
                body = ko.toJS(self.PaymentChecker());
                break;
            case "PPU Checker After PPU Caller Task":
                endPointURL = api.server + api.url.workflow.transactionCheckerAfterCallbackUpdate(instanceId, approverId);
                body = ko.toJS(self.TransactionCheckerCallback());
                //return true;
                break;
        }

        // declare options variable for ajax get request
        var options = {
            url: endPointURL,
            token: accessToken,
            data: ko.toJSON(body)
        };

        //Helper.Ajax.Post(options, OnSuccessSaveApprovalData(outcomeId), OnError, OnAlways);
        Helper.Ajax.Post(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
            }
        }, OnError, OnAlways);
    }

    function SaveApprovalData(instanceId, approverId, outcomeId) {
        var endPointURL;
        var body;

        switch (self.ActivityTitle()) {
            case "PPU Maker Task":
                endPointURL = api.server + api.url.workflow.transactionMaker(instanceId, approverId);
                body = ko.toJS(self.TransactionMaker().Transaction);
                break;
            case "PPU Maker After Checker and Caller Task":
                endPointURL = api.server + api.url.workflow.transactionMaker(instanceId, approverId);
                body = ko.toJS(self.TransactionMaker().Transaction);
                break;
            case "PPU Checker Task":
                endPointURL = api.server + api.url.workflow.transactionChecker(instanceId, approverId);
                body = ko.toJS(self.TransactionChecker());
                break;
            case "PPU Checker After PPU Caller Task":
                endPointURL = api.server + api.url.workflow.transactionChecker(instanceId, approverId);
                body = ko.toJS(self.TransactionCheckerCallback());
                break;
            case "PPU Caller Task":
                endPointURL = api.server + api.url.workflow.transactionCallback(instanceId, approverId);
                self.TransactionCallback().Callbacks = self.TempCallBackTime();
                body = ko.toJS(self.TransactionCallback());
                break;
            case "PPU Checker Cancellation Task":
                CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
                return true;
                break;
            case "CBO Maker Task":
                //endPointURL = api.server + api.url.workflow.transactionContact(instanceId, approverId);
                //body = ko.toJS(self.TransactionContact().Contacts);
                CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
                return true;
                break;
            case "CBO Checker Task":
                //endPointURL = api.server + api.url.workflow.transactionContact(instanceId, approverId);
                //body = ko.toJS(self.TransactionContact().Contacts);
                CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
                return true;
                break;
            case "Branch Maker Reactivation Task":
                endPointURL = api.server + api.url.workflow.transactionReactivationDormant(instanceId, approverId);
                body = ko.toJS(self.TransactionChecker());
                break;
            case "Branch Checker Reactivation Task":
                CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
                return true;
                break;
            case "CBO Maker Reactivation Task":
                endPointURL = api.server + api.url.workflow.transactionReactivationDormant(instanceId, approverId);
                body = ko.toJS(self.TransactionChecker());
                break;
            case "CBO Checker Reactivation Task":
                CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
                return true;
                //endPointURL = api.server + api.url.workflow.transactionReactivationDormant(instanceId, approverId);
                //body = ko.toJS(self.TransactionChecker());
                break;
            case "Payment Maker Task":
                endPointURL = api.server + api.url.workflow.paymentMaker(instanceId, approverId);
                body = ko.toJS(self.PaymentMaker().Payment);
                break;
            case "Payment Maker Revise Task":
                endPointURL = api.server + api.url.workflow.paymentMaker(instanceId, approverId);
                body = ko.toJS(self.PaymentMaker().Payment);
                break;
            case "Payment Checker Task":
                endPointURL = api.server + api.url.workflow.paymentChecker(instanceId, approverId);
                body = ko.toJS({ Verify: self.PaymentChecker().Verify });
                break;
            case "Lazy Approval Task for Exceptional Handling Case":
                CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
                return true;
                break;
            case "Lazy App fo Except":
                CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
                return true;
                break;
            case "Lazy Approval Task for UTC Case":
                CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
                return true;
                break;
            /*case "FX Deal Checker Task" :
             endPointURL = api.server + api.url.workflow.bizunitChecker(instanceId, approverId);
             body = ko.toJS({ Verify: self.TransactionDeal().Verify });
             break;*/
        }

        // declare options variable for ajax get request
        var options = {
            url: endPointURL,
            token: accessToken,
            data: ko.toJSON(body)
        };

        //Helper.Ajax.Post(options, OnSuccessSaveApprovalData(outcomeId), OnError, OnAlways);
        Helper.Ajax.Post(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {

                CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
            }
        }, OnError, OnAlways);
    }

    function RejectData(instanceId, dataID) {
        var endPointURL;
        var body;

        switch (self.ActivityTitle()) {
            case "FX Deal Checker Task":
                endPointURL = api.server + api.url.workflow.transactionRejectDeal(instanceId);
                body = ko.toJS(self.TransactionDeal().Transaction);
                break;
        }

        // declare options variable for ajax get request
        var options = {
            url: endPointURL,
            token: accessToken,
            data: ko.toJSON(body)
        };

        //Helper.Ajax.Post(options, OnSuccessSaveApprovalData(outcomeId), OnError, OnAlways);
        Helper.Ajax.Put(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {

                CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), dataID, self.Comments());
            }
        }, OnError, OnAlways);
    }

    // Get Nintex task outcomes
    function GetTaskOutcomes(taskListID, taskListItemID) {
        var task = {
            TaskListID: taskListID,
            TaskListItemID: taskListItemID
        };

        var options = {
            url: "/_vti_bin/DBSNintex/Services.svc/GetTaskOutcomesByListID",
            data: ko.toJSON(task)
        };

        Helper.Sharepoint.Nintex.Post(options, OnSuccessTaskOutcomes, OnError, OnAlways);
    }

    // Completing Nintex task
    function CompletingTask(taskListID, taskListItemID, taskTypeID, outcomeID, comments) {
        var task = {
            TaskListID: taskListID,
            TaskListItemID: taskListItemID,
            OutcomeID: outcomeID,
            Comment: comments
        };

        var endPointURL;
        switch (taskTypeID) {
            // Request Approval
            case 0: endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondApprovalTask";
                break;

            // Request Review
            case 1: endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondReviewTask";
                break;

            // Flexi Task
            case 4: endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondFlexiTask";
                break;
        }

        var options = {
            url: endPointURL,
            data: ko.toJSON(task)
        };

        Helper.Sharepoint.Nintex.Post(options, OnSuccessCompletingTask, OnError, OnAlways);
    }

    // Event handlers declaration start
    function OnSuccessGetParameters(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            self.Products(data.Product);
            self.Channels(data.Channel);
            self.Currencies(data.Currency);
            self.DebitCurrencies(data.Currency);
            self.BizSegments(data.BizSegment);
            self.Banks(data.Bank);
            self.FXCompliances(data.FXCompliance);
            self.BankCharges(data.ChargesType);
            self.AgentCharges(data.ChargesType);
            self.ProductTypes(data.ProductType);
            self.LLDs(data.LLD);
            self.UnderlyingDocs(data.UnderltyingDoc);
            self.POAFunctions(data.POAFunction);
            self.DocumentTypes(data.DocType);
            self.DocumentPurposes(data.PurposeDoc);
            self.ddlDocumentPurpose_a(data.PurposeDoc);

            self.ddlDocumentType_a(data.DocType);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.Tasks(data.Rows); //Put the response in ObservableArray

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));

            //$("#transaction-progress").hide();
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    function GetIsFlowValas(FlowValas) {
        return (FlowValas == null ? false : FlowValas.IsFlowValas);
    }

    self.LLDFlag = ko.observable();

    function UploadDocuments(callBack, param) {
        var data = {
            ApplicationID: viewModel.TransactionMaker().Transaction.ApplicationID,
            CIF: viewModel.TransactionMaker().Transaction.Customer.CIF,
            Name: viewModel.TransactionMaker().Transaction.Customer.Name
        };

        // upload hanya document selain underlying
        var items = ko.utils.arrayFilter(self.MakerDocuments(), function (item) {
            return item.Purpose.ID != 2 && item.IsNewDocument == true;
        });

        if (items != null && items.length > 0) {
            self.counterUpload(0);
            for (var i = 0; items.length > i; i++) {
                UploadFile(data, items[i], callBack, items.length, param);
            }
        } else {
            callBack(param.WFID, param.ApproverID, param.DataID);
        }
    }

    function OnSuccessGetTransactionData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            var mapping = {
                //'ignore': ["LastModifiedDate", "LastModifiedBy"]
            };
            // Chandra (get Total IDR_FCY & utilization Amount)
            SetFXTotalTransaction(self.ActivityTitle(), data);
            //GetTransactionTZDetails(self.ActivityTitle(),data)
            switch (self.ActivityTitle()) {
                case "PPU Maker Task":

                    self.BeneAccNumberMask((data.Transaction.BeneAccNumber != null) ? data.Transaction.BeneAccNumber : '');

                    if (data.Transaction.IsOtherAccountNumber == null) {
                        data.Transaction.IsOtherAccountNumber = false;
                    }

                    //data.Transaction.Customer.Accounts

                    viewModel.Rate = formatNumber(data.Transaction.Rate);
                    viewModel.Amount = formatNumber(data.Transaction.Amount);
                    viewModel.AmountUSD_r(formatNumber_r(data.Transaction.AmountUSD));

                    var t_IsFxTransaction = (data.Transaction.Currency.Code != 'IDR' && data.Transaction.DebitCurrency.Code == 'IDR');
                    var t_IsFxTransactionToIDRB = (data.Transaction.Currency.Code == 'IDR' &&
                        data.Transaction.DebitCurrency.Code != 'IDR' && GetIsFlowValas(data.Transaction.ProductType) == true && data.Transaction.AmountUSD >= self.FCYIDRTreshold());

                    self.t_IsFxTransaction(t_IsFxTransaction);
                    self.t_IsFxTransactionToIDRB(t_IsFxTransactionToIDRB);

                    if (data.Transaction.Bank.Code == '999') {
                        data.Transaction.Bank.Description = data.Transaction.OtherBeneBankName;
                        data.Transaction.Bank.SwiftCode = data.Transaction.OtherBeneBankSwift;
                        data.Transaction.IsOtherBeneBank = true;

                        /*if(self.VerifyColumn('Bene Bank')().IsVerified)
                         {
                         $('#bank-code').prop('disabled', false);
                         }*/
                    }
                    else {
                        $('#bank-code').prop('disabled', true);
                    }

                    if (data.Transaction.UnderlyingDoc != null) {
                        if (data.Transaction.UnderlyingDoc.Code == '999') {
                            self.Selected().UnderlyingDoc(data.Transaction.UnderlyingDoc.ID);
                            data.Transaction.UnderlyingDoc.Name = data.Transaction.OtherUnderlyingDoc;
                            viewModel.isNewUnderlying(true);
                        }
                        else {
                            self.Selected().UnderlyingDoc(data.Transaction.UnderlyingDoc.ID);
                            //data.Transaction.UnderlyingDoc.Name = data.Transaction.OtherUnderlyingDoc;
                            viewModel.isNewUnderlying(false);
                        }
                    }

                    if (data.Transaction.LLD != null) {
                        self.Selected().LLD(data.Transaction.LLD.ID);
                        self.LLDCode(data.Transaction.LLD.Description);
                    }
                    else {
                        self.Selected().LLD(null);
                    }

                    var tempdata = {
                        Timelines: data.Timelines,
                        Transaction: data.Transaction,
                        Verify: data.Verify
                    };

                    self.TransactionMaker(ko.mapping.toJS(tempdata, mapping));

                    if (data.Transaction.ProductType != null) {
                        self.Selected().ProductType(data.Transaction.ProductType.ID);
                    }
                    else {
                        self.Selected().ProductType(null);
                    }

                    // fill selected id for auto populate field
                    self.Selected().Currency(data.Transaction.Currency.ID);
                    typeof (self.Selected().Account) == 'function' ? (self.Selected().Account(data.Transaction.Account.AccountNumber)) : (self.Selected().Account = data.Transaction.Account.AccountNumber);
                    //self.Selected().Account(data.Transaction.Account.AccountNumber);
                    //$('debit-acc-number').Value(data.Transaction.Account.AccountNumber);
                    self.Selected().Bank(data.Transaction.Bank.ID);

                    // chandra 2015.05.03 : set underlying document
                    self.MakerUnderlyings([]);
                    self.CustomerUnderlyings([]);
                    self.TempSelectedUnderlying([]);
                    if (data.Transaction.Customer.Underlyings != null) {
                        ko.utils.arrayForEach(data.Transaction.Customer.Underlyings, function (item) {//for(var i=0;data.Transaction.Customer.Underlyings.length>i;i++){
                            if (item.IsEnable == true) {
                                self.MakerUnderlyings.push(item.ID);
                                self.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, false, item.AvailableAmount, item.StatementLetter.ID));
                            }
                            self.CustomerUnderlyings.push(item);
                        });
                        setTotalUtilize();
                    }

                    // chandra 2015.05.03 : set attachment document

                    self.MakerDocuments([]);
                    self.TransactionMaker().Transaction.Documents = [];
                    self.MakerAllDocumentsTemp([]);
                    //if(data.Transaction.Documents != null){
                    if (data.Transaction.ReviseMakerDocuments != null) {
                        ko.utils.arrayForEach(data.Transaction.ReviseMakerDocuments, function (item) {
                            item.IsNewDocument = false;
                            self.MakerAllDocumentsTemp.push(item);
                            ko.utils.arrayForEach(self.MakerUnderlyings(), function (item2) {
                                if (item2 == item.UnderlyingID) {
                                    self.MakerDocuments.push(item);
                                    self.TransactionMaker().Transaction.Documents.push(item);
                                }
                            });
                            if (item.Purpose.ID != 2) {
                                self.MakerDocuments.push(item);
                                self.TransactionMaker().Transaction.Documents.push(item);
                            }
                        });
                    }

                    self.Selected().DebitCurrency(data.Transaction.DebitCurrency.ID);
                    //self.PopulateSelected("TransactionMaker", 'DebitCurrency');
                    //self.TransactionMaker().Transaction.IsOtherAccountNumber
                    //bind auto complete
                    self.SetBankAutoComplete('TransactionMaker');

                    //bind auto complete
                    SetAutoComplete('TransactionMaker');
                    break;
                case "PPU Checker Cancellation Task":
                    if (data.Transaction.IsOtherAccountNumber) {
                        data.Transaction.Account.AccountNumber = data.Transaction.OtherAccountNumber;
                    }

                    var t_IsFxTransaction = (data.Transaction.Currency.Code != 'IDR' && data.Transaction.DebitCurrency.Code == 'IDR');
                    var t_IsFxTransactionToIDRB = (data.Transaction.Currency.Code == 'IDR' &&
                        data.Transaction.DebitCurrency.Code != 'IDR' && GetIsFlowValas(data.Transaction.ProductType) == true && data.Transaction.AmountUSD >= self.FCYIDRTreshold());

                    self.t_IsFxTransaction(t_IsFxTransaction);
                    self.t_IsFxTransactionToIDRB(t_IsFxTransactionToIDRB);

                    self.BeneAccNumberMask((data.Transaction.BeneAccNumber != null) ? data.Transaction.BeneAccNumber : '');

                    viewModel.Rate = formatNumber(data.Transaction.Rate);
                    viewModel.Amount = formatNumber(data.Transaction.Amount);
                    viewModel.AmountUSD_r(formatNumber_r(data.Transaction.AmountUSD));

                    if (data.Transaction.Bank.Code == '999') {
                        data.Transaction.Bank.Description = data.Transaction.OtherBeneBankName;
                        data.Transaction.Bank.SwiftCode = data.Transaction.OtherBeneBankSwift;

                        /*if(self.VerifyColumn('Bene Bank')().IsVerified)
                         {
                         $('#bank-code').prop('disabled', false);
                         }*/
                    }
                    else {
                        $('#bank-code').prop('disabled', true);
                    }

                    if (data.Transaction.UnderlyingDoc != null) {
                        if (data.Transaction.UnderlyingDoc.Code == '999') {
                            self.Selected().UnderlyingDoc(data.Transaction.UnderlyingDoc.ID);
                            data.Transaction.UnderlyingDoc.Name = data.Transaction.OtherUnderlyingDoc;
                            viewModel.isNewUnderlying(true);
                        }
                        else {
                            self.Selected().UnderlyingDoc(data.Transaction.UnderlyingDoc.ID);
                            //data.Transaction.UnderlyingDoc.Name = data.Transaction.OtherUnderlyingDoc;
                            viewModel.isNewUnderlying(false);
                        }
                    }

                    if (data.Transaction.LLD != null) {
                        self.Selected().LLD(data.Transaction.LLD.ID);
                        self.LLDCode(data.Transaction.LLD.Description);
                    }
                    else {
                        self.Selected().LLD(null);
                    }



                    /*var tempdata = {

                     Timelines: data.Timelines,
                     Transaction: data.Transaction,
                     Verify: data.Verify
                     };*/

                    self.TransactionChecker(ko.mapping.toJS(data, mapping));

                    if (data.Transaction.ProductType != null) {
                        self.Selected().ProductType(data.Transaction.ProductType.ID);
                    }
                    else {
                        self.Selected().ProductType(null);
                    }

                    // fill selected id for auto populate field
                    self.Selected().Currency(data.Transaction.Currency.ID);
                    typeof (self.Selected().Account) == 'function' ? (self.Selected().Account(data.Transaction.Account.AccountNumber)) : (self.Selected().Account = data.Transaction.Account.AccountNumber);
                    //self.Selected().Account(data.Transaction.Account.AccountNumber);
                    //$('debit-acc-number').Value(data.Transaction.Account.AccountNumber);
                    self.Selected().Bank(data.Transaction.Bank.ID);

                    //bind auto complete
                    self.SetBankAutoComplete('TransactionMaker');

                    //bind auto complete
                    SetAutoComplete('TransactionMaker');
                    break;
                case "PPU Maker After Checker and Caller Task":
                    if (data.Transaction.IsOtherAccountNumber) {
                        data.Transaction.Account.AccountNumber = data.Transaction.OtherAccountNumber;
                    }

                    if (data.Callback.IsOtherAccountNumber) {
                        data.Callback.Account.AccountNumber = data.Callback.OtherAccountNumber;
                    }

                    if (data.Transaction.IsOtherAccountNumber == null) {
                        data.Transaction.IsOtherAccountNumber = false;
                    }

                    self.BeneAccNumberMask(data.Transaction.BeneAccNumber);
                    if (data.Transaction.Bank.Code == '999') {
                        data.Transaction.Bank.Description = data.Transaction.OtherBeneBankName;
                        data.Transaction.Bank.SwiftCode = data.Transaction.OtherBeneBankSwift;

                        /*if(self.VerifyColumn('Bene Bank')().IsVerified)
                         {
                         $('#bank-code').prop('disabled', false);
                         }*/
                    }
                    else {
                        $('#bank-code').prop('disabled', true);
                    }
                    if (data.Transaction.BankCharges != null) {
                        self.Selected().BankCharges(data.Transaction.BankCharges.ID);
                    }
                    if (data.Transaction.AgentCharges != null) {
                        self.Selected().AgentCharges(data.Transaction.AgentCharges.ID);
                    }

                    if (data.Transaction.UnderlyingDoc != null) {
                        if (data.Transaction.UnderlyingDoc.Code == '999') {
                            self.Selected().UnderlyingDoc(data.Transaction.UnderlyingDoc.ID);
                            data.Transaction.UnderlyingDoc.Name = data.Transaction.OtherUnderlyingDoc;
                            viewModel.isNewUnderlying(true);
                        }
                        else {
                            self.Selected().UnderlyingDoc(data.Transaction.UnderlyingDoc.ID);
                            //data.Transaction.UnderlyingDoc.Name = data.Transaction.OtherUnderlyingDoc;
                            viewModel.isNewUnderlying(false);
                        }
                    }

                    if (data.Transaction.LLD != null) {
                        self.Selected().LLD(data.Transaction.LLD.ID);
                        self.LLDCode(data.Transaction.LLD.Description);
                    }
                    else {
                        self.Selected().LLD(null);
                    }

                    if (data.Transaction.ProductType != null) {
                        self.Selected().ProductType(data.Transaction.ProductType.ID);
                    }

                    var tempdata = {
                        Callback: data.Callback,
                        Checker: data.Checker,
                        Timelines: data.Timelines,
                        Transaction: data.Transaction,
                        Verify: data.Verify
                    };

                    self.TransactionMaker(ko.mapping.toJS(data, mapping));

                    self.Selected().DebitCurrency(data.Transaction.DebitCurrency.ID);

                    // chandra 2015.05.03 : set underlying document
                    self.MakerUnderlyings([]);
                    self.CustomerUnderlyings([]);
                    self.TempSelectedUnderlying([]);
                    if (data.Transaction.Customer.Underlyings != null) {
                        ko.utils.arrayForEach(data.Transaction.Customer.Underlyings, function (item) {//for(var i=0;data.Transaction.Customer.Underlyings.length>i;i++){
                            if (item.IsEnable == true) {
                                self.MakerUnderlyings.push(item.ID);
                                self.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, false, item.AvailableAmount, item.StatementLetter.ID));
                            }
                            self.CustomerUnderlyings.push(item);
                        });
                        setTotalUtilize();
                    }

                    // chandra 2015.05.03 : set attachment document
                    self.MakerDocuments([]);
                    self.TransactionMaker().Transaction.Documents = [];
                    self.MakerAllDocumentsTemp([]);
                    //if(data.Transaction.Documents != null){
                    if (data.Transaction.ReviseMakerDocuments != null) {
                        ko.utils.arrayForEach(data.Transaction.ReviseMakerDocuments, function (item) {
                            item.IsNewDocument = false;
                            self.MakerAllDocumentsTemp.push(item);
                            ko.utils.arrayForEach(self.MakerUnderlyings(), function (item2) {
                                if (item2 == item.UnderlyingID) {
                                    self.MakerDocuments.push(item);
                                    self.TransactionMaker().Transaction.Documents.push(item);
                                }
                            });
                            if (item.Purpose.ID != 2) {
                                self.MakerDocuments.push(item);
                                self.TransactionMaker().Transaction.Documents.push(item);
                            }
                        });
                    }

                    // fill selected id for auto populate field
                    self.Selected().Currency(data.Transaction.Currency.ID);
                    typeof (self.Selected().Account) == 'function' ? (self.Selected().Account(data.Transaction.Account.AccountNumber)) : (self.Selected().Account = data.Transaction.Account.AccountNumber);
                    self.Selected().Bank(data.Transaction.Bank.ID);

                    //bind auto complete
                    self.SetBankAutoComplete('TransactionMaker');

                    //bind auto complete
                    SetAutoComplete('TransactionMaker');
                    break;
                case "PPU Checker Task":

                    if (data.Transaction.IsOtherAccountNumber) {
                        data.Transaction.Account.AccountNumber = data.Transaction.OtherAccountNumber;
                    }

                    if (data.Transaction.LLD != null) {
                        self.Selected().LLD(data.Transaction.LLD.ID);
                        self.LLDCode(data.Transaction.LLD.Description);
                    }
                    else {
                        self.Selected().LLD(null);
                    }

                    if (data.Transaction.Bank.Code == '999') {
                        data.Transaction.Bank.Description = data.Transaction.OtherBeneBankName;
                        data.Transaction.Bank.SwiftCode = data.Transaction.OtherBeneBankSwift;
                    }
                    else {
                        $('#bank-code').prop('disabled', true);
                    }

                    self.TransactionChecker(ko.mapping.toJS(data, mapping));

                    if (!self.IsAllChecked()) {
                        self.CheckAll(false);
                    } else {
                        self.CheckAll(true);
                    }

                    break;
                case "Pending Documents Checker Task":

                    if (data.Transaction.Bank.Code == '999') {
                        data.Transaction.Bank.Description = data.Transaction.OtherBeneBankName;
                        data.Transaction.Bank.SwiftCode = data.Transaction.OtherBeneBankSwift;
                    }
                    else {
                        $('#bank-code').prop('disabled', true);
                    }

                    self.TransactionChecker(ko.mapping.toJS(data, mapping));

                    if (!self.IsAllChecked()) {
                        self.CheckAll(false);
                    } else {
                        self.CheckAll(true);
                    }

                    break;
                case "PPU Checker After PPU Caller Task":

                    if (data.Transaction.IsOtherAccountNumber) {
                        data.Transaction.Account.AccountNumber = data.Transaction.OtherAccountNumber;
                    }

                    if (data.Callback.IsOtherAccountNumber) {
                        data.Callback.Account.AccountNumber = data.Callback.OtherAccountNumber;
                    }

                    if ((data.Transaction.LLD != null) && (data.Callback.LLD != null)) {
                        self.LLDFlag(data.Transaction.LLD);
                    } else {
                        self.LLDFlag(null);
                    }

                    if (data.Transaction.LLD != null) {
                        self.Selected().LLD(data.Transaction.LLD.ID);
                        self.LLDCode(data.Transaction.LLD.Description);
                    }
                    else {
                        self.Selected().LLD(null);
                    }

                    if (data.Transaction.Bank.Code == '999') {
                        data.Transaction.Bank.Description = data.Transaction.OtherBeneBankName;
                        data.Transaction.Bank.SwiftCode = data.Transaction.OtherBeneBankSwift;
                    }

                    if (data.Callback.Bank.Code == '999') {
                        data.Callback.Bank.Description = data.Callback.OtherBeneBankName;
                        data.Callback.Bank.SwiftCode = data.Callback.OtherBeneBankSwift;
                    }

                    self.TransactionCheckerCallback(ko.mapping.toJS(data, mapping));
                    break;
                case "PPU Caller Task":
                    self.BeneAccNumberMask(data.Transaction.BeneAccNumber);
                    if (data.Transaction.UnderlyingDoc != null) {
                        if (data.Transaction.UnderlyingDoc.Code == '999') {
                            self.Selected().UnderlyingDoc(data.Transaction.UnderlyingDoc.ID);
                            data.Transaction.UnderlyingDoc.Name = data.Transaction.OtherUnderlyingDoc;
                            viewModel.isNewUnderlying(true);
                        }
                        else {
                            self.Selected().UnderlyingDoc(data.Transaction.UnderlyingDoc.ID);
                            //data.Transaction.UnderlyingDoc.Name = data.Transaction.OtherUnderlyingDoc;
                            viewModel.isNewUnderlying(false);
                        }
                    }

                    if (data.Transaction.LLD != null) {
                        self.Selected().LLD(data.Transaction.LLD.ID);
                        self.LLDCode(data.Transaction.LLD.Description);
                    }
                    else {
                        self.Selected().LLD(null);
                    }

                    if (data.Transaction.Bank.Code == '999') {
                        data.Transaction.Bank.Description = data.Transaction.OtherBeneBankName;
                        data.Transaction.Bank.SwiftCode = data.Transaction.OtherBeneBankSwift;
                    }
                    else {
                        $('#bank-code').prop('disabled', true);
                    }

                    // Filter Account Number if Fx Transaction,
                    if (self.t_IsFxTransaction() || self.t_IsFxTransactionToIDRB()) {
                        if (data.Transaction.Customer.Accounts != undefined && data.Transaction.Customer.Accounts.length > 0) {
                            var tmpArry = [];
                            $.each(data.Transaction.Customer.Accounts, function (index, item) {
                                if (item.Currency.ID == data.Transaction.DebitCurrency.ID || item.Currency.Code == '-') {
                                    tmpArry.push(item);
                                    //data.Transaction.Customer.Accounts.remove(item);
                                    //console.log(item.Currency.Code);
                                }
                            });
                            data.Transaction.Customer.Accounts = [];
                            data.Transaction.Customer.Accounts = tmpArry;
                        }
                    }

                    self.TransactionCallback(ko.mapping.toJS(data, mapping));
                    typeof (self.Selected().Account) == 'function' ? (self.Selected().Account(data.Transaction.Account.AccountNumber)) : (self.Selected().Account = data.Transaction.Account.AccountNumber);

                    self.SetBankAutoComplete('PaymentMaker');
                    self.Contact(undefined);
                    self.TempCallBackTime([]);
                    break;
                case "CBO Maker Task":

                    self.TransactionContact(ko.mapping.toJS(data, mapping));
                    self.CIF_c(self.TransactionContact().Transaction.Customer.CIF);
                    break;
                case "CBO Checker Task":
                    self.TransactionContact(ko.mapping.toJS(data, mapping));
                    break;
                case "Branch Maker Reactivation Task":
                    if (data.Transaction.IsOtherAccountNumber) {
                        data.Transaction.Account.AccountNumber = data.Transaction.OtherAccountNumber;
                    }

                    if (data.Transaction.LLD != null) {
                        self.Selected().LLD(data.Transaction.LLD.ID);
                        self.LLDCode(data.Transaction.LLD.Description);
                    }
                    else {
                        self.Selected().LLD(null);
                    }

                    if (data.Transaction.Bank.Code == '999') {
                        data.Transaction.Bank.Description = data.Transaction.OtherBeneBankName;
                        data.Transaction.Bank.SwiftCode = data.Transaction.OtherBeneBankSwift;
                    }
                    else {
                        $('#bank-code').prop('disabled', true);
                    }

                    self.MakerDocuments([]);
                    if (data.Transaction.Documents != null) {
                        for (var i = 0; data.Transaction.Documents.length > i; i++) {
                            data.Transaction.Documents[i].IsNewDocument = false;
                            self.MakerDocuments.push(data.Transaction.Documents[i]);
                        }
                    }
                    self.MakerUnderlyings([]);
                    if (data.Transaction.Customer.Underlyings != null) {
                        for (var i = 0; data.Transaction.Customer.Underlyings.length > i; i++) {
                            self.MakerUnderlyings.push(data.Transaction.Customer.Underlyings[i].ID);
                        }
                    }

                    self.TransactionChecker(ko.mapping.toJS(data, mapping));
                    break;
                case "Branch Checker Reactivation Task":
                    if (data.Transaction.IsOtherAccountNumber) {
                        data.Transaction.Account.AccountNumber = data.Transaction.OtherAccountNumber;
                    }

                    if (data.Transaction.LLD != null) {
                        self.Selected().LLD(data.Transaction.LLD.ID);
                        self.LLDCode(data.Transaction.LLD.Description);
                    }
                    else {
                        self.Selected().LLD(null);
                    }

                    if (data.Transaction.Bank.Code == '999') {
                        data.Transaction.Bank.Description = data.Transaction.OtherBeneBankName;
                        data.Transaction.Bank.SwiftCode = data.Transaction.OtherBeneBankSwift;
                    }
                    else {
                        $('#bank-code').prop('disabled', true);
                    }

                    self.MakerDocuments([]);
                    if (data.Transaction.Documents != null) {
                        for (var i = 0; data.Transaction.Documents.length > i; i++) {
                            data.Transaction.Documents[i].IsNewDocument = false;
                            self.MakerDocuments.push(data.Transaction.Documents[i]);
                        }
                    }
                    self.MakerUnderlyings([]);
                    if (data.Transaction.Customer.Underlyings != null) {
                        for (var i = 0; data.Transaction.Customer.Underlyings.length > i; i++) {
                            self.MakerUnderlyings.push(data.Transaction.Customer.Underlyings[i].ID);
                        }
                    }

                    self.TransactionChecker(ko.mapping.toJS(data, mapping));
                    break;
                case "CBO Maker Reactivation Task":
                    if (data.Transaction.IsOtherAccountNumber) {
                        data.Transaction.Account.AccountNumber = data.Transaction.OtherAccountNumber;
                    }

                    if (data.Transaction.LLD != null) {
                        self.Selected().LLD(data.Transaction.LLD.ID);
                        self.LLDCode(data.Transaction.LLD.Description);
                    }
                    else {
                        self.Selected().LLD(null);
                    }

                    if (data.Transaction.Bank.Code == '999') {
                        data.Transaction.Bank.Description = data.Transaction.OtherBeneBankName;
                        data.Transaction.Bank.SwiftCode = data.Transaction.OtherBeneBankSwift;
                    }
                    else {
                        $('#bank-code').prop('disabled', true);
                    }

                    self.MakerDocuments([]);
                    if (data.Transaction.Documents != null) {
                        for (var i = 0; data.Transaction.Documents.length > i; i++) {
                            data.Transaction.Documents[i].IsNewDocument = false;
                            self.MakerDocuments.push(data.Transaction.Documents[i]);
                        }
                    }
                    self.MakerUnderlyings([]);
                    if (data.Transaction.Customer.Underlyings != null) {
                        for (var i = 0; data.Transaction.Customer.Underlyings.length > i; i++) {
                            self.MakerUnderlyings.push(data.Transaction.Customer.Underlyings[i].ID);
                        }
                    }
                    self.TransactionChecker(ko.mapping.toJS(data, mapping));
                    break;
                case "CBO Checker Reactivation Task":
                    if (data.Transaction.IsOtherAccountNumber) {
                        data.Transaction.Account.AccountNumber = data.Transaction.OtherAccountNumber;
                    }

                    if (data.Transaction.LLD != null) {
                        self.Selected().LLD(data.Transaction.LLD.ID);
                        self.LLDCode(data.Transaction.LLD.Description);
                    }
                    else {
                        self.Selected().LLD(null);
                    }

                    if (data.Transaction.Bank.Code == '999') {
                        data.Transaction.Bank.Description = data.Transaction.OtherBeneBankName;
                        data.Transaction.Bank.SwiftCode = data.Transaction.OtherBeneBankSwift;
                    }
                    else {
                        $('#bank-code').prop('disabled', true);
                    }

                    self.MakerDocuments([]);
                    if (data.Transaction.Documents != null) {
                        for (var i = 0; data.Transaction.Documents.length > i; i++) {
                            data.Transaction.Documents[i].IsNewDocument = false;
                            self.MakerDocuments.push(data.Transaction.Documents[i]);
                        }
                    }
                    self.MakerUnderlyings([]);
                    if (data.Transaction.Customer.Underlyings != null) {
                        for (var i = 0; data.Transaction.Customer.Underlyings.length > i; i++) {
                            self.MakerUnderlyings.push(data.Transaction.Customer.Underlyings[i].ID);
                        }
                    }
                    self.TransactionChecker(ko.mapping.toJS(data, mapping));
                    break;
                case "Payment Maker Task":
         
                    self.BeneAccNumberMask('');
                    if (t_IsFxTransaction || t_IsFxTransactionToIDRB) {
                        self.GetTotalAmountFX(data.Payment.Customer.CIF, data.Payment.AmountUSD);
                    }
                    if (data.Payment.LLD != null) {
                        self.Selected().LLD(data.Payment.LLD.ID);
                        self.LLDCode(data.Payment.LLD.Description);
                    }
                    else {
                        self.Selected().LLD(null);
                    }

                    if (data.Payment.Bank.Code == '999') {
                        data.Payment.Bank.Description = data.Payment.OtherBeneBankName;
                        data.Payment.Bank.SwiftCode = data.Payment.OtherBeneBankSwift;
                    }

                    self.OriginalValue(new OriginalValueModel(new statusValueModel(data.Payment.Amount, false),
                        new statusValueModel(data.Payment.AmountUSD, false),
                        new statusValueModel(data.Payment.Rate, false),
                        new statusValueModel(data.Payment.BeneName, false),
                        new statusValueModel(data.Payment.Bank.Description, false),
                        new statusValueModel(data.Payment.BeneAccNumber, false),
                        //new statusValueModel(data.Payment.BankCharges, false),
                        //new statusValueModel(data.Payment.Account.AccountNumber + ' (' + data.Payment.DebitCurrency.Code + ')'), false)
                        new statusValueModel(data.Payment.Bank.SwiftCode, false),
                        new statusValueModel(data.Payment.BankCharges.Code, false),
                        new statusValueModel(data.Payment.AgentCharges.Code, false),
                        new statusValueModel(data.Payment.IsCitizen, false),
                        new statusValueModel(data.Payment.IsResident, false)
                        //Payment.IsCitizen
                        //Payment.IsResident
                        //viewModel.Selected().BankCharges
                    ));

                    //reset bene bank mapped data
                    data.Payment.Bank.SwiftCode = '';
                    data.Payment.Bank.BankAccount = '';
                    data.Payment.Bank.Code = '999';
                    data.Payment.IsOtherBeneBank = true;

                    self.CargerBankOrig(data.Payment.BankCharges.Code);
                    self.AgentChargesOrig(data.Payment.AgentCharges.Code);
                    self.Selected().Account('');
                    self.Selected().Bank('');
                    self.Selected().BankCharges('2');
                    self.Selected().AgentCharges('2');
                    var CargerBank = ko.utils.arrayFirst(self.BankCharges(), function (item) {
                        return item.ID == 2;
                    });
                    if (CargerBank != null) {
                        data.Payment.BankCharges = CargerBank;
                    }

                    var AgentCharges = ko.utils.arrayFirst(self.AgentCharges(), function (item) {
                        return item.ID == 2;
                    });

                    if (AgentCharges != null) {
                        data.Payment.AgentCharges = AgentCharges;
                    }

                    viewModel.Amount = '';
                    data.Payment.Amount = '';
                    data.Payment.AmountUSD = 0;
                    data.Payment.BeneName = '';
                    data.Payment.Bank.Description = '';
                    data.Payment.BeneAccNumber = '';

                    data.Payment.Bank.SwiftCode = '';
                    //data.Payment.BankCharges.Code = '';
                    data.Payment.IsCitizen = true;
                    data.Payment.IsResident = true;

                    self.PaymentMaker(ko.mapping.toJS(data, mapping));

                    if (data.Payment.LLD != null) {
                        self.Selected().LLD(data.Payment.LLD.ID);
                    }
                    else {
                        self.Selected().LLD('0');
                    }

                    if (data.Payment.UnderlyingDoc != null) {
                        if (data.Payment.UnderlyingDoc.Code == '999') {
                            self.Selected().UnderlyingDoc(data.Payment.UnderlyingDoc.ID);
                            data.Payment.UnderlyingDoc.Name = data.Payment.OtherUnderlyingDoc;
                            viewModel.isNewUnderlying(true);
                        }
                        else {
                            self.Selected().UnderlyingDoc(data.Payment.UnderlyingDoc.ID);
                            //data.Payment.UnderlyingDoc.Name = data.Payment.OtherUnderlyingDoc;
                            viewModel.isNewUnderlying(false);
                        }
                    }

                    if (data.Payment.ProductType != null) {
                        self.Selected().ProductType(data.Payment.ProductType.ID);
                    }

                    //self.PaymentMaker(ko.mapping.toJS(data, mapping));

                    /*GetRateAmount("PaymentMaker", data.Payment.Currency.ID);
                     self.PaymentMaker().Payment.Currency = data;*/

                    self.Rate = data.Payment.Rate;
                    $('#rate').val(data.Payment.Rate);

                    // fill selected id for auto populate field
                    self.Selected().Account(data.Payment.Account.AccountNumber);

                    //bind auto complete
                    self.SetBankAutoComplete('PaymentMaker');
                    self.SearchTZ();



                    break;
                case "Payment Maker Revise Task":
                    self.PaymentMaker(ko.mapping.toJS(data, mapping));

                    // fill selected id for auto populate field
                    self.Selected().Account(data.Payment.Account.AccountNumber);
                    self.Selected().Bank(data.Payment.Bank.ID);

                    break;
                case "Payment Checker Task":


                    if (data.Payment.LLD != null) {
                        self.Selected().LLD(data.Payment.LLD.ID);
                        self.LLDCode(data.Payment.LLD.Description);
                    }
                    else {
                        self.Selected().LLD(null);
                    }

                    if (data.Payment.Bank.Code == '999') {
                        data.Payment.Bank.Description = data.Payment.OtherBeneBankName;
                        data.Payment.Bank.SwiftCode = data.Payment.OtherBeneBankSwift;
                    }

                    if (self.IsPendingNintex()) {
                        SetVerifiedData(data.Verify, false);
                    }

                    self.PaymentChecker(ko.mapping.toJS(data, mapping));

                    if (!self.IsAllChecked()) {
                        self.CheckAll(false);
                    } else {
                        self.CheckAll(true);
                    }

                    break;
                case "Lazy Approval Task for Exceptional Handling Case":
                    if (data.Transaction.IsOtherAccountNumber) {
                        data.Transaction.Account.AccountNumber = data.Transaction.OtherAccountNumber;
                    }

                    if (data.Transaction.LLD != null) {
                        self.Selected().LLD(data.Transaction.LLD.ID);
                        self.LLDCode(data.Transaction.LLD.Description);
                    }
                    else {
                        self.Selected().LLD(null);
                    }

                    if (data.Transaction.Bank.Code == '999') {
                        data.Transaction.Bank.Description = data.Transaction.OtherBeneBankName;
                        data.Transaction.Bank.SwiftCode = data.Transaction.OtherBeneBankSwift;
                    }
                    else {
                        $('#bank-code').prop('disabled', true);
                    }

                    self.TransactionChecker(ko.mapping.toJS(data, mapping));
                    break;
                case "FX Deal Checker Task":

                    if (data.Transaction.IsOtherAccountNumber) {
                        data.Transaction.Account.AccountNumber = data.Transaction.OtherAccountNumber;
                    }

                    if (data.Transaction.DealUnderlying == null) {
                        self.DealUnderlying.ID = '-';
                        self.DealUnderlying.Description = '-';
                    }
                    else {
                        self.DealUnderlying.ID = data.Transaction.DealUnderlying.ID;
                        self.DealUnderlying.Description = data.Transaction.DealUnderlying.Description;
                    }

                    self.TransactionDeal(ko.mapping.toJS(data, mapping));

                    DealID = data.Transaction.ID;

                    //cifData = data.Transaction.Customer.CIF;
                    //GetDataUnderlying();


                    break;
                case "Lazy App fo Except":

                    if (data.Transaction.IsOtherAccountNumber) {
                        data.Transaction.Account.AccountNumber = data.Transaction.OtherAccountNumber;
                    }

                    if (data.Transaction.LLD != null) {
                        self.Selected().LLD(data.Transaction.LLD.ID);
                        self.LLDCode(data.Transaction.LLD.Description);
                    }
                    else {
                        self.Selected().LLD(null);
                    }

                    if (data.Transaction.Bank.Code == '999') {
                        data.Transaction.Bank.Description = data.Transaction.OtherBeneBankName;
                        data.Transaction.Bank.SwiftCode = data.Transaction.OtherBeneBankSwift;
                    }
                    else {
                        $('#bank-code').prop('disabled', true);
                    }

                    self.TransactionChecker(ko.mapping.toJS(data, mapping));

                   /* if (!self.IsAllChecked()) {
                        self.CheckAll(false);
                    } else {
                        self.CheckAll(true);
                    } */

                    break;
                case "Lazy Approval Task for UTC Case":

                    if (data.Transaction.IsOtherAccountNumber) {
                        data.Transaction.Account.AccountNumber = data.Transaction.OtherAccountNumber;
                    }

                    if (data.Transaction.LLD != null) {
                        self.Selected().LLD(data.Transaction.LLD.ID);
                        self.LLDCode(data.Transaction.LLD.Description);
                    }
                    else {
                        self.Selected().LLD(null);
                    }

                    if (data.Transaction.Bank.Code == '999') {
                        data.Transaction.Bank.Description = data.Transaction.OtherBeneBankName;
                        data.Transaction.Bank.SwiftCode = data.Transaction.OtherBeneBankSwift;
                    }
                    else {
                        $('#bank-code').prop('disabled', true);
                    }

                    self.TransactionChecker(ko.mapping.toJS(data, mapping));

                    /* if (!self.IsAllChecked()) {
                         self.CheckAll(false);
                     } else {
                         self.CheckAll(true);
                     } */

                    break;
            }

            // disable form if this task is not authorized
            if (self.IsPendingNintex()) {
                if (!self.IsAuthorizedNintex()) {
                    DisableForm();
                }
            } else {
                DisableForm();
            }
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    function SetMappingAttachment(customerName) {
        CustomerUnderlyingFile.CustomerUnderlyingMappings([]);
        for (var i = 0 ; i < self.TempSelectedAttachUnderlying().length; i++) {
            //CustomerUnderlyingFile.CustomerUnderlyingMappings.push(new CustomerUnderlyingMappingModel(0, self.TempSelectedAttachUnderlying()[i].ID, customerName + '-(A)'));
            var sValue = {
                ID: 0,
                UnderlyingID: self.TempSelectedAttachUnderlying()[i].ID,
                UnderlyingFileID: 0,
                AttachmentNo: customerName + '-(A)'
            }
            CustomerUnderlyingFile.CustomerUnderlyingMappings.push(sValue);
        }
    }

    function CheckExceptionHandling(transactionID, checkerDatas, callback, text, dataID) {

        var options = {
            url: api.server + api.url.helper + '/CheckExceptionalHandling/' + transactionID,
            params: {
            },
            token: accessToken
        };
        options.data = ko.toJSON(checkerDatas);

        Helper.Ajax.Post(options, function OnSuccessCheckExceptionHandling(data, textStatus, jqXHR) {
            var headerText = '';
            var dataText = '';
            if (jqXHR.status = 200) {
                if (data != null && data.length > 0) {
                    headerText = '<h3><font color="red">Exception Handling Occured!</font></h3></br>' +
                        'This Transaction is Part of Exception Handling : </br>';
                    for (var i = 0; data.length > i; i++) {
                        if (dataText == '') {
                            dataText = dataText + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + (parseInt(i) + 1) + '. ' + data[i] + '</br>';
                        } else {
                            dataText = dataText + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + (parseInt(i) + 1) + '. ' + data[i] + '</br>';
                        }
                    }
                    text = headerText + String.format("<b>{1}</b> </br></br> Do you want to <b>{0}</b> this task  ?", 'Approve', dataText);
                }
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        callback(self.WorkflowInstanceID(), self.ApproverId(), dataID);
                    }
                });
            } else {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
            }
        }, OnError, OnAlways);
    }




    function SetFXTotalTransaction(title, data) {
        switch (title) {
            case "PPU Maker Task":
            case "PPU Maker After Checker and Caller Task":
                $.ajax({
                    type: "GET",
                    url: api.server + api.url.transaction + "/TZ/" + data.Transaction.ApplicationID,
                    contentType: "application/json",
                    headers: {
                        "Authorization": "Bearer " + $.cookie(api.cookie.name)
                    },
                    success: function (data, textStatus, jqXHR) {

                        viewModel.IsTZCanceled(ko.mapping.toJS(data));
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
                cifData = data.Transaction.Customer.CIF;
                var t_IsFxTransaction = (data.Transaction.Currency.Code != 'IDR' && data.Transaction.DebitCurrency.Code == 'IDR');
                var t_IsFxTransactionToIDRB = (data.Transaction.Currency.Code == 'IDR' &&
                    data.Transaction.DebitCurrency.Code != 'IDR' && GetIsFlowValas(data.Transaction.ProductType) == true && data.Transaction.AmountUSD >= self.FCYIDRTreshold());
                self.t_IsFxTransaction(t_IsFxTransaction);
                self.t_IsFxTransactionToIDRB(t_IsFxTransactionToIDRB);
                FXModel.currentAmount = data.Transaction.AmountUSD;
                break;
            case "PPU Checker Task":
            case "Pending Documents Checker Task":
            case "PPU Checker After PPU Caller Task":
            case "PPU Checker Cancellation Task":
            case "PPU Caller Task":
            case "CBO Maker Task":
            case "CBO Checker Task":
            case "Branch Maker Reactivation Task":
            case "CBO Maker Reactivation Task":
            case "CBO Checker Reactivation Task":
            case "Branch Checker Reactivation Task":
            case "Lazy Approval Task for Exceptional Handling Case":
            case "Lazy App fo Except":
            case "Lazy Approval Task for UTC Case":
            case "FX Deal Checker Task":

                cifData = data.Transaction.Customer.CIF;
                var t_IsFxTransaction = (data.Transaction.Currency.Code != 'IDR' && data.Transaction.DebitCurrency.Code == 'IDR');
                var t_IsFxTransactionToIDRB = (data.Transaction.Currency.Code == 'IDR' &&
                    data.Transaction.DebitCurrency.Code != 'IDR' && GetIsFlowValas(data.Transaction.ProductType) == true && data.Transaction.AmountUSD >= self.FCYIDRTreshold());

                self.t_IsFxTransaction(t_IsFxTransaction);
                self.t_IsFxTransactionToIDRB(t_IsFxTransactionToIDRB);
                break;
            case "Payment Checker Task":
            case "Payment Maker Task":
            case "Payment Maker Revise Task":
                cifData = data.Payment.Customer.CIF;
                var t_IsFxTransaction = (data.Payment.Currency.Code != 'IDR' && data.Payment.DebitCurrency.Code == 'IDR');
                var t_IsFxTransactionToIDRB = (data.Payment.Currency.Code == 'IDR' &&
                    data.Payment.DebitCurrency.Code != 'IDR' && GetIsFlowValas(data.Payment.ProductType) == true && data.Payment.AmountUSD >= self.FCYIDRTreshold());
                
                self.t_IsFxTransaction(t_IsFxTransaction);
                self.t_IsFxTransactionToIDRB(t_IsFxTransactionToIDRB);
                break;
            case "FX Deal Checker Task":

                // set value of transaction 'IS FX TRANSACTION'
                var t_IsFxTransaction = (data.Transaction.Currency.Code != 'IDR' && data.Transaction.AccountCurrencyCode == 'IDR') ? true : false;
                var t_IsFxTransactionToIDR = (data.Transaction.Currency.Code == 'IDR' && data.Transaction.AccountCurrencyCode != 'IDR') ? true : false;
                var t_IsFxTransactionToIDRB = (data.Transaction.Currency.Code == 'IDR' && data.Transaction.AccountCurrencyCode != 'IDR' && GetIsFlowValas(data.Transaction.ProductType) == true) ? true : false;
                self.t_IsFxTransaction(t_IsFxTransaction);
                self.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);
                self.t_IsFxTransactionToIDRB(t_IsFxTransactionToIDRB);
                break;
        }

        FXModel.cif = cifData;
        FXModel.token = accessToken;
        GetTotalDeal(FXModel, OnSuccessTotal, OnError);
        GetTotalPPU(FXModel, OnSuccessTotal, OnError);
    }

    self.CheckAllData = function (form) {
        // re-updating observable. This method will updating the UI (view)
        var data = null;
        var isVerified = document.getElementById('check-all').checked;
        self.CheckAll(isVerified);

        switch (form) {
            case "PaymentChecker":
                data = self.PaymentChecker().Verify;
                break;
            case "TransactionChecker":
                data = self.TransactionChecker().Verify;
                break;
        }

        for (i = 0; i <= data.length - 1; i++) {
            data[i].IsVerified = isVerified;
        }

        self.UpdateTemplateUI(form);
    }

    function SetVerifiedData(data, isVerified) {
        for (i = 0; i <= data.length - 1; i++) {
            data[i].IsVerified = isVerified;
        }

        self.Callback = function () {
            data;
        };
        self.Callback();
    }

    function OnSuccessSaveApprovalData(outcomeId) {
        // completing task
        CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
    }

    function DisableForm() {
        $("#transaction-form").find(" input, select, textarea, button").prop("disabled", true);
    }

    function OnSuccessTaskOutcomes(data, textStatus, jqXHR) {
        if (data.IsSuccess) {
            self.Outcomes(data.Outcomes);

            self.IsPendingNintex(data.IsPending);
            self.IsAuthorizedNintex(data.IsAuthorized);
            self.MessageNintex(data.Message);

            // Get transaction Data
            GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());

            // lock current task only if user had permission
            if (self.IsPendingNintex() == true && self.IsAuthorizedNintex() == true) {
                LockAndUnlockTaskHub("Lock", self.WorkflowInstanceID(), self.ApproverId(), self.SPTaskListItemID(), self.ActivityTitle());
            }
        } else {
            if (data.Outcomes == null) {
                //self.Outcomes(data.Outcomes);
                //self.IsPendingNintex(data.IsPending);
                //self.IsAuthorizedNintex(data.IsAuthorized);
                self.MessageNintex(data.Message);
                GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());
            }
            else {
                ShowNotification("An error Occured", jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        }
    }

    function OnSuccessCompletingTask(data, textStatus, jqXHR) {
        if (data.IsSuccess) {
            // send notification
            ShowNotification('Completing Task Success', jqXHR.responseJSON.Message, 'gritter-success', false);

            // close dialog task
            $("#modal-form").modal('hide');

            // reload tasks & show progress bar
            $("#transaction-progress").show();

            self.IsOtherAccountNumber(false);

            self.GetData();
        } else {
            ShowNotification('Completing Task Failed', jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    //bangkit
    function GetRateAmount(form, CurrencyID) {
        var options = {
            url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, function OnSuccessGetRateAmount(data, textStatus, jqXHR) {
            if (jqXHR.status = 200) {
                switch (form) {
                    case "TransactionMaker":
                        self.Rate = formatNumber(data.RupiahRate);
                        $('#rate').val(self.Rate);
                        OnKeyUp('TransactionMaker');

                        //self.Amount = formatNumber(self.TransactionMaker().Transaction.Amount);
                        self.TransactionMaker().Transaction.Rate = data.RupiahRate;
                        self.Rate = formatNumber(data.RupiahRate);

                        break;
                    case "TransactionMakerAfterCaller":
                        self.Rate = formatNumber(data.RupiahRate);
                        $('#rate').val(self.Rate);
                        OnKeyUp('TransactionMaker');

                        self.TransactionMaker().Transaction.Rate = data.RupiahRate;
                        self.Rate = formatNumber(data.RupiahRate);

                        break;
                    case "PaymentMaker":
                        self.Rate = formatNumber(data.RupiahRate);
                        $('#rate').val(self.Rate);
                        OnKeyUp('PaymentMaker');

                        self.PaymentMaker().Payment.Rate = data.RupiahRate;
                        self.Rate = formatNumber(data.RupiahRate);

                        break;
                    case "TransactionCaller":
                        self.Rate = formatNumber(data.RupiahRate);
                        $('#rate').val(self.Rate);
                        OnKeyUp('TransactionCaller');

                        self.TransactionCallback().Transaction.Rate = data.RupiahRate;
                        self.Rate = formatNumber(data.RupiahRate);

                        break;
                }
                self.UpdateTemplateUI(form);
            } else {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
            }
        }, OnError, OnAlways);
    }

    self.idrrate = ko.observable();

    self.GetRateIDR = function () { GetRateIDR() };

    function GetRateIDR() {
        var options = {
            url: api.server + api.url.currency + "/CurrencyRate/" + "1",
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetRateIDR, OnError, OnAlways);
    }


    function OnSuccessGetRateIDR(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            self.idrrate(data.RupiahRate);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    //Apply bank auto complete
    self.SetBankAutoComplete = function (form) {
        console.log('autocomplete loaded');
        // autocomplete
        $("#bank-bank").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.bank + "/Search",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessBankAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set customer data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };

                    switch (self.ActivityTitle()) {
                        case "PPU Maker Task":
                            self.TransactionMaker().Transaction.Bank = ko.mapping.toJS(ui.item.data, mapping);
                            self.IsOtherBank(true);
                            self.TransactionMaker().Transaction.IsOtherBeneBank = false;

                            $("#bank-code").val(self.TransactionMaker().Transaction.Bank.SwiftCode);
                            $('#bank-code').prop('disabled', true);
                            break;
                        case "PPU Maker After Checker and Caller Task":
                            self.TransactionMaker().Transaction.Bank = ko.mapping.toJS(ui.item.data, mapping);
                            self.IsOtherBank(true);
                            self.TransactionMaker().Transaction.IsOtherBeneBank = false;

                            $("#bank-code").val(self.TransactionMaker().Transaction.Bank.SwiftCode);
                            $('#bank-code').prop('disabled', true);
                            break;
                        case "Payment Maker Task":
                            self.PaymentMaker().Payment.Bank = ko.mapping.toJS(ui.item.data, mapping);
                            self.IsOtherBank(true);
                            self.PaymentMaker().Payment.IsOtherBeneBank = false;

                            $("#bank-code").val(self.PaymentMaker().Payment.Bank.SwiftCode);
                            $('#bank-code').prop('disabled', true);
                            break;
                        case "PPU Caller Task":
                            self.TransactionCallback().Transaction.Bank = ko.mapping.toJS(ui.item.data, mapping);
                            self.IsOtherBank(true);
                            self.TransactionCallback().Transaction.IsOtherBeneBank = false;

                            $("#bank-code").val(self.TransactionCallback().Transaction.Bank.SwiftCode);
                            $('#bank-code').prop('disabled', true);
                            break;

                    }
                }
                else {
                    $('#bank-code').prop('disabled', false);
                }
            }
        });
    }

    //Apply auto complete
    function SetAutoComplete(formdata) {
        console.log('autocomplete loaded');
        // autocomplete
        $("#customer-name").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.customer + "/Search",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set customer data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    self.TransactionMaker().Transaction.Customer = ko.mapping.toJS(ui.item.data, mapping);

                    self.PopulateSelected(formdata, 'CustomerName');

                    //var dtSegment = ko.utils.arrayFirst(self.BizSegments(), function(data) { if(data.ID == ui.item.data.BizSegment.ID) return data; });
                    //var iSegment = self.BizSegments().indexOf(dtSegment);
                    //$("#biz-segment").prop('selectedIndex', iSegment + 1);

                    //cifData = ui.item.data.CIF;
                    //customerNameData = ui.item.data.Name;

                }
                else
                    viewModel.TransactionModel().Customer(null);
            }
        });
    }

    self.UploadReactivationDoc = ko.observable();

    function UploadReactivationDoc(SaveApprovalData, instanceId, approverId, outcomeId, param) {
        // uploading documents. after upload completed, see SaveTransaction()
        var data = {
            ApplicationID: self.TransactionChecker().Transaction.ApplicationID,
            CIF: self.TransactionChecker().Transaction.Customer.CIF,
            Name: self.TransactionChecker().Transaction.Customer.Name
        };

        //bangkit
        /*if(self.TransactionChecker().Transaction.Documents.length > 0 ){
         for(var i =0; i < self.TransactionChecker().Transaction.Documents.length; i++)
         {
         UploadFile(i, data, self.TransactionChecker().Transaction.Documents[i], SaveApprovalData, instanceId, approverId, outcomeId);
         }
         } else {
         SaveApprovalData(instanceId, approverId, outcomeId);
         }*/

        // upload hanya document selain underlying
        var items = ko.utils.arrayFilter(self.MakerDocuments(), function (item) {
            return item.Purpose.ID != 2 && item.IsNewDocument == true;
        });

        if (items != null && items.length > 0) {
            self.counterUpload(0);
            for (var i = 0; items.length > i; i++) {
                if (items[i].IsNewDocument)
                    UploadFile(data, items[i], SaveApprovalData, items.length, param);
            }
        } else {
            //ShowNotification("Task Validation Warning", "File attachment required", 'gritter-warning', true);
            SaveApprovalData(instanceId, approverId, outcomeId);
        }
    }

    // Upload the file
    function UploadFile(index, context, document, SaveApprovalData, instanceId, approverId, outcomeId) {

        // Define the folder path for this example.
        var serverRelativeUrlToFolder = '/Attachment Document';

        // Get test values from the file input and text input page controls.
        //var fileInput = $('#document-path');

        var parts = document.FileName.split('.');
        var fileExtension = parts[parts.length - 1];
        var timeStamp = new Date().getTime();
        var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

        // Get the server URL.
        var serverUrl = _spPageContextInfo.webAbsoluteUrl;

        // output variable
        var output;

        // Initiate method calls using jQuery promises.
        // Get the local file as an array buffer.
        var getFile = getFileBuffer();
        getFile.done(function (arrayBuffer) {

            // Add the file to the SharePoint folder.
            var addFile = addFileToFolder(arrayBuffer);
            addFile.done(function (file, status, xhr) {
                output = file.d;

                // Get the list item that corresponds to the uploaded file.
                var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
                getItem.done(function (listItem, status, xhr) {

                    // Change the display name and title of the list item.
                    var changeItem = updateListItem(listItem.d.__metadata);
                    changeItem.done(function (data, status, xhr) {
                        //alert('file uploaded and updated');
                        //return output;
                        var kodok = {
                            ID: 0,
                            Type: document.Type,
                            Purpose: document.Purpose,
                            LastModifiedDate: null,
                            LastModifiedBy: null,
                            DocumentPath: output.ServerRelativeUrl,
                            FileName: document.DocumentPath.name
                        };

                        self.TransactionChecker().Transaction.Documents[index].DocumentPath = output.ServerRelativeUrl;

                        if (self.TransactionChecker().Transaction.Documents.length - 1 == index) {
                            SaveApprovalData(instanceId, approverId, outcomeId);
                        }
                    });
                    changeItem.fail(OnError);
                });
                getItem.fail(OnError);
            });
            addFile.fail(OnError);
        });
        getFile.fail(OnError);

        // Get the local file as an array buffer.
        function getFileBuffer() {
            var deferred = $.Deferred();
            var reader = new FileReader();
            reader.onloadend = function (e) {
                deferred.resolve(e.target.result);
            }
            reader.onerror = function (e) {
                deferred.reject(e.target.error);
            }
            //bangkit
            var f = new File([""], document.DocumentPath.name)
            reader.readAsArrayBuffer(f);
            //reader.readAsDataURL(document.DocumentPath);
            return deferred.promise();
        }

        // Add the file to the file collection in the Shared Documents folder.
        function addFileToFolder(arrayBuffer) {

            // Get the file name from the file input control on the page.
            //var parts = fileInput[0].value.split('\\');
            //var fileName = parts[parts.length - 1];

            // Construct the endpoint.
            var fileCollectionEndpoint = String.format(
                    "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                    "/add(overwrite=false, url='{2}')",
                serverUrl, serverRelativeUrlToFolder, fileName);

            // Send the request and return the response.
            // This call returns the SharePoint file.
            return $.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val()
                    //"content-length": arrayBuffer.byteLength
                }
            });
        }

        // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
        function getListItem(fileListItemUri) {

            // Send the request and return the response.
            return $.ajax({
                url: fileListItemUri,
                type: "GET",
                headers: { "accept": "application/json;odata=verbose" }
            });
        }

        // Change the display name and title of the list item.
        function updateListItem(itemMetadata) {

            // Define the list item changes. Use the FileLeafRef property to change the display name.
            // For simplicity, also use the name as the title.
            // The example gets the list item type from the item's metadata, but you can also get it from the
            // ListItemEntityTypeFullName property of the list.
            //var body = String.format("{{'__metadata':{{'type':'{0}'}},'FileLeafRef':'{1}','Title':'{2}'}}", itemMetadata.type, fileName, fileName);
            var body = {
                Title: document.DocumentPath.name,
                Application_x0020_ID: context.ApplicationID,
                CIF: context.CIF,
                Customer_x0020_Name: context.Name,
                Document_x0020_Type: document.Type.Name,
                Document_x0020_Purpose: document.Purpose.Name,
                __metadata: {
                    type: itemMetadata.type,
                    FileLeafRef: fileName,
                    Title: fileName
                }
            };

            // Send the request and return the promise.
            // This call does not return response content from the server.
            return $.ajax({
                url: itemMetadata.uri,
                type: "POST",
                data: ko.toJSON(body),
                headers: {
                    "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                    "content-type": "application/json;odata=verbose",
                    //"content-length": body.length,
                    "IF-MATCH": itemMetadata.etag,
                    "X-HTTP-Method": "MERGE"
                }
            });
        }
    }
    // upload underlying file
    function UploadFileUnderlying(context, document, callBack) {

        // Define the folder path for this example.
        var serverRelativeUrlToFolder = '/Underlying Documents';


        // Get the file name from the file input control on the page.
        if (document.DocumentPath.name != undefined) {
            var parts = document.DocumentPath.name.split('.');
        } else {
            //self.DocumentPath_a('');
            self.IsEditable(true);
            ShowNotification("", "Please select a file", 'gritter-warning', true);
        }

        var fileExtension = parts[parts.length - 1];
        var timeStamp = new Date().getTime();
        var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

        // Get the server URL.
        var serverUrl = _spPageContextInfo.webAbsoluteUrl;

        // output variable
        var output;

        // Initiate method calls using jQuery promises.
        // Get the local file as an array buffer.
        var getFile = getFileBufferUnderlying();
        getFile.done(function (arrayBuffer) {

            // Add the file to the SharePoint folder.
            var addFile = addFileToFolderUnderlying(arrayBuffer);
            addFile.done(function (file, status, xhr) {
                output = file.d;

                // Get the list item that corresponds to the uploaded file.
                var getItem = getListItemUnderlying(file.d.ListItemAllFields.__deferred.uri);
                getItem.done(function (listItem, status, xhr) {
                    //return output;
                    var documentData = {
                        ID: 0,
                        Type: document.Type,
                        Purpose: document.Purpose,
                        LastModifiedDate: null,
                        LastModifiedBy: null,
                        DocumentPath: output.ServerRelativeUrl,
                        FileName: document.DocumentPath.name,
                        IsDormant: document.IsNewDocument
                    };

                    switch (self.ActivityTitle()) {
                        case "PPU Maker Task":
                        case "PPU Maker After Checker and Caller Task":
                            viewModel.TransactionMaker().Transaction.Documents.push(documentData);
                            break;
                        default:
                            viewModel.TransactionChecker().Transaction.Documents.push(documentData);
                    }
                    // Change the display name and title of the list item.
                    var changeItem = updateListItemUnderlying(listItem.d.__metadata);
                    changeItem.done(function (data, status, xhr) {
                        DocumentModels({ "FileName": document.DocumentPath.name, "DocumentPath": output.ServerRelativeUrl });
                        callBack();
                    });
                    changeItem.fail(OnError);
                });
                getItem.fail(OnError);
            });
            addFile.fail(OnError);
        });
        getFile.fail(OnError);

        // Get the local file as an array buffer.
        function getFileBufferUnderlying() {
            var deferred = $.Deferred();
            var reader = new FileReader();
            reader.onloadend = function (e) {
                deferred.resolve(e.target.result);
            }
            reader.onerror = function (e) {
                deferred.reject(e.target.error);
            }
            //reader.readAsArrayBuffer(fileInput[0].files[0]);
            //reader.readAsDataURL(document.DocumentPath());
            reader.readAsArrayBuffer(document.DocumentPath);
            return deferred.promise();
        }

        // Add the file to the file collection in the Shared Documents folder.
        function addFileToFolderUnderlying(arrayBuffer) {

            // Get the file name from the file input control on the page.
            //var parts = fileInput[0].value.split('\\');
            //var fileName = parts[parts.length - 1];

            // Construct the endpoint.
            var fileCollectionEndpoint = String.format(
                    "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                    "/add(overwrite=false, url='{2}')",
                serverUrl, serverRelativeUrlToFolder, fileName);

            // Send the request and return the response.
            // This call returns the SharePoint file.
            return $.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val()
                    //"content-length": arrayBuffer.byteLength
                }
            });
        }

        // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
        function getListItemUnderlying(fileListItemUri) {

            // Send the request and return the response.
            return $.ajax({
                url: fileListItemUri,
                type: "GET",
                headers: { "accept": "application/json;odata=verbose" }
            });
        }

        // Change the display name and title of the list item.
        function updateListItemUnderlying(itemMetadata) {

            // Define the list item changes. Use the FileLeafRef property to change the display name.
            // For simplicity, also use the name as the title.
            // The example gets the list item type from the item's metadata, but you can also get it from the
            // ListItemEntityTypeFullName property of the list.
            //var body = String.format("{{'__metadata':{{'type':'{0}'}},'FileLeafRef':'{1}','Title':'{2}'}}", itemMetadata.type, fileName, fileName);
            var body = {
                Title: document.DocumentPath.name,
                Application_x0020_ID: context.ApplicationID,
                CIF: context.CIF,
                Customer_x0020_Name: context.Name,
                Document_x0020_Type: context.Type.DocTypeName,
                Document_x0020_Purpose: context.Purpose.Name,
                __metadata: {
                    type: itemMetadata.type,
                    FileLeafRef: fileName,
                    Title: fileName
                }
            };

            // Send the request and return the promise.
            // This call does not return response content from the server.
            return $.ajax({
                url: itemMetadata.uri,
                type: "POST",
                data: ko.toJSON(body),
                headers: {
                    "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                    "content-type": "application/json;odata=verbose",
                    //"content-length": body.length,
                    "IF-MATCH": itemMetadata.etag,
                    "X-HTTP-Method": "MERGE"
                }
            });
        }
    }

    // Upload the file
    function UploadFile(context, document, callBack, countItem, param) {
        // Define the folder path for this example.

        var serverRelativeUrlToFolder = '/Instruction Documents';

        var parts = document.DocumentPath.name.split('.');

        var fileExtension = parts[parts.length - 1];
        var timeStamp = new Date().getTime();
        var fileName;
        if (parts[parts.length - 2] != null) {
            fileName = parts[parts.length - 2] + timeStamp + "." + fileExtension; //document.DocumentPath.name;
        } else {
            fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;
        }


        // Get the server URL.
        var serverUrl = _spPageContextInfo.webAbsoluteUrl;

        // output variable
        var output;

        // Initiate method calls using jQuery promises.
        // Get the local file as an array buffer.
        var getFile = getFileBuffer();
        getFile.done(function (arrayBuffer) {

            // Add the file to the SharePoint folder.
            var addFile = addFileToFolder(arrayBuffer);
            addFile.done(function (file, status, xhr) {
                output = file.d;

                // Get the list item that corresponds to the uploaded file.
                var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
                getItem.done(function (listItem, status, xhr) {

                    // Change the display name and title of the list item.
                    var changeItem = updateListItem(listItem.d.__metadata);
                    changeItem.done(function (data, status, xhr) {
                        //return output;
                        var documentData = {
                            ID: 0,
                            Type: document.Type,
                            Purpose: document.Purpose,
                            LastModifiedDate: null,
                            LastModifiedBy: null,
                            DocumentPath: output.ServerRelativeUrl,
                            FileName: document.DocumentPath.name,
                            IsDormant: document.IsNewDocument
                        };

                        switch (self.ActivityTitle()) {
                            case "PPU Maker Task":
                            case "PPU Maker After Checker and Caller Task":
                                viewModel.TransactionMaker().Transaction.Documents.push(documentData);
                                break;
                            default:
                                viewModel.TransactionChecker().Transaction.Documents.push(documentData);
                        }

                        self.counterUpload(self.counterUpload() + 1);
                        if (countItem == self.counterUpload()) {
                            callBack(param.WFID, param.ApproverID, param.DataID);
                        }
                        return 1;
                    });
                    changeItem.fail(OnError);
                });
                getItem.fail(OnError);
            });
            addFile.fail(OnError);
        });
        getFile.fail(OnError);

        // Get the local file as an array buffer.
        function getFileBuffer() {
            var deferred = $.Deferred();
            var reader = new FileReader();
            reader.onloadend = function (e) {
                deferred.resolve(e.target.result);
            }
            reader.onerror = function (e) {
                deferred.reject(e.target.error);
            }
            //bangkit
            reader.readAsArrayBuffer(document.DocumentPath);
            //reader.readAsDataURL(document.DocumentPath);
            return deferred.promise();
        }

        // Add the file to the file collection in the Shared Documents folder.
        function addFileToFolder(arrayBuffer) {

            // Construct the endpoint.
            var fileCollectionEndpoint = String.format(
                    "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                    "/add(overwrite=false, url='{2}')",
                serverUrl, serverRelativeUrlToFolder, fileName);

            // Send the request and return the response.
            // This call returns the SharePoint file.
            return $.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val()
                    //"content-length": arrayBuffer.byteLength
                }
            });
        }

        // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
        function getListItem(fileListItemUri) {

            // Send the request and return the response.
            return $.ajax({
                url: fileListItemUri,
                type: "GET",
                headers: { "accept": "application/json;odata=verbose" }
            });
        }

        // Change the display name and title of the list item.
        function updateListItem(itemMetadata) {
            var body = {
                Title: document.DocumentPath.name,
                Application_x0020_ID: context.ApplicationID,
                CIF: context.CIF,
                Customer_x0020_Name: context.Name,
                Document_x0020_Type: document.Type.Name,
                Document_x0020_Purpose: document.Purpose.Name,
                __metadata: {
                    type: itemMetadata.type,
                    FileLeafRef: fileName,
                    Title: fileName
                }
            };

            // Send the request and return the promise.
            // This call does not return response content from the server.
            return $.ajax({
                url: itemMetadata.uri,
                type: "POST",
                data: ko.toJSON(body),
                headers: {
                    "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                    "content-type": "application/json;odata=verbose",
                    //"content-length": body.length,
                    "IF-MATCH": itemMetadata.etag,
                    "X-HTTP-Method": "MERGE"
                }
            });
        }
    }

    // save underlying file
    function SaveDataFile() {

        SetMappingAttachment(viewModel.TransactionMaker().Transaction.Customer.Name);
        //Ajax call to insert the CustomerUnderlyings
        var transactionID = viewModel.TransactionMaker().Transaction.ID;
        CustomerUnderlyingFile.ID = transactionID;
        CustomerUnderlyingFile.FileName = DocumentModels().FileName;
        CustomerUnderlyingFile.DocumentPath = DocumentModels().DocumentPath;
        CustomerUnderlyingFile.DocumentType = ko.utils.arrayFirst(self.ddlDocumentType_a(), function (item) {
            return item.ID == self.Selected().DocumentType_a();
        });
        CustomerUnderlyingFile.DocumentPurpose = ko.utils.arrayFirst(self.ddlDocumentPurpose_a(), function (item) {
            return item.ID == self.Selected().DocumentPurpose_a();
        });
        var transactionID = viewModel.TransactionMaker().Transaction.ID;
        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlyingfile + "/AddFile",
            data: ko.toJSON(CustomerUnderlyingFile), //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    // send notification
                    ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                    // hide current popup window
                    $("#modal-form-Attach").modal('hide');
                    viewModel.IsUploading(false);
                    var doc = {
                        ID: data,
                        Type: CustomerUnderlyingFile.DocumentType,
                        Purpose: CustomerUnderlyingFile.DocumentPurpose,
                        FileName: CustomerUnderlyingFile.FileName,
                        DocumentPath: CustomerUnderlyingFile.DocumentPath,
                        IsNewDocument: true,
                        LastModifiedDate: new Date(),
                        LastModifiedBy: null
                    };
                    self.MakerDocuments.push(doc);
                    // refresh data
                    //GetDataAttachFile();
                } else {
                    viewModel.IsUploading(false);
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                viewModel.IsUploading(false);
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });


    }

    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
    // Event handlers declaration end
};

// View Model
var viewModel = new ViewModel();

// jQuery Init
$(document).ready(function () {
    /*
     Nintex 2013 DB

     WorkflowState Enumeration :
     Running: 2
     Complete: 4
     Cancelled: 8
     Error: 64

     Outcome Enumeration :
     None: -1 (Represents no outcome.)
     Approved: 0 (Represents the 'approved' response of a 'Request Approval' task.)
     Rejected: 1 (Represents the 'rejected' response of a 'Request Approval' task.)
     Pending: 2 (Represent a task that is waiting for a response.)
     Cancelled: 3 (Represents a task that was cancelled because of a cancelled workflow.)
     NotRequired: 4 (Represents a task that no longer requires a response as a result of the business rules of the task action.)
     Continue: 5 (Represents a completed 'Assign todo task' or 'Request data' task.)
     Delegated: 6 (Represents a task that was delegated to another user.)
     Custom: 7 (Represents a completed 'Assign Flexi task' action.)
     OverrideApproved: 8 (Represents a task that was force approved by a 'Complete workflow task' action.)
     OverrideRejected: 9 (Represents a task that was force rejected by a 'Complete workflow task' action.)
     OverrideContinue: 10 (Represents a task that was force completed by a 'Complete workflow task' action.)
     */





    // get table parameter
    var workflow = {
        State: $("#workflow-task-table").attr("workflow-state"),
        Outcome: $("#workflow-task-table").attr("workflow-outcome"),
        CustomOutcome: $("#workflow-task-table").attr("workflow-custom-outcome"),
        ShowContribute: $("#workflow-task-table").attr("workflow-show-contribute"),
        ShowActiveTask: $("#workflow-task-table").attr("workflow-show-active-task")
    };

    // set workflow inside vm
    viewModel.WorkflowConfig(workflow);
    // $('#aspnetForm').after('<form id="frmUnderlying"></form>');
    // $("#modal-form-Attach").appendTo("#frmUnderlying");
    // $("#modal-form-Attach").appendTo("#frmUnderlying");
    // widget loader
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    // block enter key from user to prevent submitted data.
    $(this).keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

            return false;
        }
    });

    // Load all templates
    $('script[src][type="text/html"]').each(function () {
        //alert($(this).attr("src"))
        var src = $(this).attr("src");
        if (src != undefined) {
            $(this).load(src);
        }
    });

    // scrollables
    $('.slim-scroll').each(function () {
        var $this = $(this);
        $this.slimscroll({
            height: $this.data('height') || 100,
            railVisible: true,
            alwaysVisible: true,
            color: '#D15B47'
        });
    });

    // tooltip
    $('[data-rel=tooltip]').tooltip();

    // datepicker
    $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });


    $('.time-picker').timepicker({
        defaultTime: "",
        minuteStep: 1,
        showSeconds: false,
        showMeridian: false
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });
    // ace file upload
    $('#document-path-upload').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });
    // ace file upload
    $('#document-path-upload1').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });
    // ace file upload
    $('#document-path').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });

    // Knockout custom file handler
    ko.bindingHandlers.file = {
        init: function (element, valueAccessor) {
            $(element).change(function () {
                var file = this.files[0];

                if (ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };

    // validation
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,

        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });

    // Knockout Datepicker custom binding handler
    ko.bindingHandlers.datepicker = {
        init: function (element) {
            $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).datepicker("refresh");
        }
    };

    // Knockout Timepicker custom binding handler
    ko.bindingHandlers.timepicker = {
        init: function (element) {
            $(element).timepicker({
                defaultTime: "",
                minuteStep: 1,
                showSeconds: false,
                showMeridian: false
            }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).timepicker('showWidget');
        }
    };

    // Knockout Bindings
    //bangkit
    //ko.applyBindings(viewModel);
    ko.applyBindings(viewModel, document.getElementById('home-transaction'));
    // -- test error login
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(TokenOnSuccess, TokenOnError);
        StartTaskHub();
    } else {
        accessToken = $.cookie(api.cookie.name);
        StartTaskHub();
        GetCurrentUser(viewModel);
        FXModel.token = accessToken;
        GetParameterData(FXModel, OnSuccessGetTotal, OnError);
        viewModel.GetParameters();
        viewModel.GetData();
        viewModel.GetRateIDR();
    }

    // -- end test

    /*
     // Token Validation
     if ($.cookie(api.cookie.name) == undefined) {
     Helper.Token.Request(TokenOnSuccess, TokenOnError);
     console.log("log undefined tokn");
     // bangkit
     myFunction();
     } else {
     // read token from cookie
     accessToken = $.cookie(api.cookie.name);
     //bangkit
     StartTaskHub();

     // call spuser function
     GetCurrentUser(viewModel);

     // call get data inside view model
     FXModel.token = accessToken;
     GetParameterData(FXModel,OnSuccessGetTotal,OnError);
     viewModel.GetParameters();
     viewModel.GetData();
     viewModel.GetRateIDR();
     }

     function myFunction() {
     myVar = setInterval(alertFunction, 2000);
     }

     function alertFunction() {
     if($.cookie(api.cookie.spUser) != undefined){
     GetCurrentUser(viewModel);

     viewModel.GetParameters();
     viewModel.GetData();
     viewModel.GetRateIDR();

     StartTaskHub();

     //alert("Stop!");
     clearInterval(myVar);
     } else {
     //alert("Hello!");
     }
     }
     */
    // Modal form on close handler
    $("#modal-form").on('hidden.bs.modal', function () {
        //alert("close")

        LockAndUnlockTaskHub("unlock", viewModel.WorkflowInstanceID(), viewModel.ApproverId(), viewModel.SPTaskListItemID(), viewModel.ActivityTitle());
    });
});

function ResetTZNumber() {
    viewModel.TZModel().Message = '';
    $('#tz-message').text('');
}

function enforceMaxlength(data, event) {
    switch (viewModel.ActivityTitle()) {
        case "PPU Maker Task":
            if (viewModel.TransactionMaker().Transaction.Product.Name != 'OTT') {
                if (event.target.value.length >= 35) {
                    return false;
                }
            }
            else {
                if (event.target.value.length >= 255) {
                    return false;
                }
            }
            return true;
            break;
        case "PPU Maker After Checker and Caller Task":
            if (viewModel.TransactionMaker().Transaction.Product.Name != 'OTT') {
                if (event.target.value.length >= 35) {
                    return false;
                }
            }
            else {
                if (event.target.value.length >= 255) {
                    return false;
                }
            }
            return true;
            break;
        case "PPU Caller Task":
            if (viewModel.TransactionCallback().Transaction.Product.Name != 'OTT') {
                if (event.target.value.length >= 35) {
                    return false;
                }
            }
            else {
                if (event.target.value.length >= 255) {
                    return false;
                }
            }
            return true;
            break;
        case "Payment Maker Task":
            if (viewModel.PaymentMaker().Payment.Product.Name != 'OTT') {
                if (event.target.value.length >= 35) {
                    return false;
                }
            }
            else {
                if (event.target.value.length >= 255) {
                    return false;
                }
            }
            return true;
            break;

            return true;
    }
}

function IsDocumentCompleteCheck(data) {
    //IsDocumentCompleteCheck

    if (viewModel.TransactionChecker().Verify != undefined) {

        var verify = ko.utils.arrayFilter(viewModel.TransactionChecker().Verify, function (item) {
            return item.Name == "Document Completeness";
        });

        if (verify != null) {
            var index = viewModel.TransactionChecker().Verify.indexOf(verify[0]);

            if (verify[0].IsVerified) {
                viewModel.TransactionChecker().Transaction.IsDocumentComplete = true;
            } else {
                viewModel.TransactionChecker().Transaction.IsDocumentComplete = false;
            }
        }
    }
}


function OnKeyUp(form, element) {
    //================================= Eqv Calculation ====================================
    //bangkit
    var x = document.getElementById("trxn-amount");
    var y = document.getElementById("rate");
    var d = document.getElementById("eqv-usd");
    var xstored = x.getAttribute("data-in");
    var ystored = y.getAttribute("data-in");
    setInterval(function () {
        if (x == document.activeElement) {
            var temp = x.value;
            if (xstored != temp) {
                xstored = temp;
                x.setAttribute("data-in", temp);
                calculate();
            }
        }
        if (y == document.activeElement) {
            var temp = y.value;
            if (ystored != temp) {
                ystored = temp;
                y.setAttribute("data-in", temp);
                calculate();
            }
        }
    }, 50);

    function calculate() {
        fromName = form;
        switch (form) {
            case "PaymentMaker":

                x.value = x.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '');
                y.value = y.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '');
                viewModel.PaymentMaker().Payment.Amount = x.value;
                viewModel.PaymentMaker().Payment.Rate = y.value;

                var trx = x.value;
                var ret = y.value;

                x.value = formatNumber(x.value);
                y.value = formatNumber(y.value);

                var res = (trx * ret) / viewModel.idrrate();
                // res = res.replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $("#eqv-usd").val(res);

                viewModel.PaymentMaker().Payment.AmountUSD = res;
                var tmp = viewModel.AmountModel().TotalDealAmountsUSD();
                viewModel.AmountModel().TotalDealAmountsUSD(parseFloat(tmp) + parseFloat(res));

                break;
            case "TransactionMaker":

                x.value = x.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '');
                y.value = y.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '');
                viewModel.TransactionMaker().Transaction.Amount = x.value;
                viewModel.TransactionMaker().Transaction.Rate = y.value;

                var trx = x.value;
                var ret = y.value;

                x.value = formatNumber(x.value);
                y.value = formatNumber(y.value);

                var res = (trx * ret) / viewModel.idrrate();
                //res = res.replace(/\d(?=(\d{3})+\.)/g, '$&,');
                //$("#eqv-usd").val(res);

                viewModel.AmountUSD_r(formatNumber_r(res));
                viewModel.TransactionMaker().Transaction.AmountUSD = res;
                viewModel.TotalUtilization(TotalPPUModel.Total.UtilizationPPU + res);
                break;
            case "TransactionMakerAfterCaller":

                x.value = x.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '');
                y.value = y.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '');
                viewModel.TransactionMaker().Transaction.Amount = x.value;
                viewModel.TransactionMaker().Transaction.Rate = y.value;

                var trx = x.value;
                var ret = y.value;

                x.value = formatNumber(x.value);
                y.value = formatNumber(y.value);

                var res = (trx * ret) / viewModel.idrrate();
                //res = res.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $("#eqv-usd").val(res);

                viewModel.TransactionMaker().Transaction.AmountUSD = res;
                viewModel.TotalUtilization(TotalPPUModel.Total.UtilizationPPU + res);
                break;
            case "TransactionCaller":
                x.value = x.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '');
                y.value = y.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '');
                viewModel.TransactionCallback().Transaction.Amount = x.value;
                viewModel.TransactionCallback().Transaction.Rate = y.value;

                var trx = x.value;
                var ret = y.value;

                x.value = formatNumber(x.value);
                y.value = formatNumber(y.value);

                var res = (trx * ret) / viewModel.idrrate();
                //res = res.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $("#eqv-usd").val(res);

                viewModel.TransactionCallback().Transaction.AmountUSD = res;
                /*var res = (x.value * y.value) / viewModel.idrrate();
                 res = res.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                 $("#eqv-usd").val(res);
                 viewModel.TransactionCallback().Transaction.Amount = x.value;
                 viewModel.TransactionCallback().Transaction.AmountUSD = res;*/
                break;
        }
    }

    x.onblur = ReloadDataAfterCalculate; //calculate;
    calculate();

    // re-updating observable. This method will updating the UI (view)
    // ReloadData(form);
    //================================= End Eqv Calculation ====================================
};

function ReloadData(form) {
    switch (form) {
        case "PaymentMaker":
            viewModel.Rate = formatNumber(viewModel.PaymentMaker().Payment.Rate);
            viewModel.Amount = formatNumber(viewModel.PaymentMaker().Payment.Amount);

            // var update = viewModel.PaymentMaker();
            // viewModel.PaymentMaker(ko.mapping.toJS(update));
            document.getElementById("other-debit-acc-number").focus();
            //bind auto complete
            //viewModel.SetBankAutoComplete('PaymentMaker');
            break;
        case "TransactionMaker":
            viewModel.Rate = formatNumber(viewModel.TransactionMaker().Transaction.Rate);
            viewModel.Amount = formatNumber(viewModel.TransactionMaker().Transaction.Amount);

            var update = viewModel.TransactionMaker();
            viewModel.TransactionMaker(ko.mapping.toJS(update));
            document.getElementById("trxn-amount").focus();
            //bind auto complete
            viewModel.SetBankAutoComplete('PaymentMaker');
            break;
        case "TransactionMakerAfterCaller":
            viewModel.Rate = formatNumber(viewModel.TransactionMaker().Transaction.Rate);
            viewModel.Amount = formatNumber(viewModel.TransactionMaker().Transaction.Amount);

            var update = viewModel.TransactionMaker();
            viewModel.TransactionMaker(ko.mapping.toJS(update));
            document.getElementById("trxn-amount").focus();
            //bind auto complete
            viewModel.SetBankAutoComplete('PaymentMaker');
            break;
        case "TransactionCaller":
            viewModel.Rate = formatNumber(viewModel.TransactionCallback().Transaction.Rate);
            viewModel.Amount = formatNumber(viewModel.TransactionCallback().Transaction.Amount);

            var update = viewModel.TransactionCallback();
            viewModel.TransactionCallback(ko.mapping.toJS(update));
            document.getElementById("trxn-amount").focus();
            //bind auto complete
            viewModel.SetBankAutoComplete('PaymentMaker');
            /*var update = viewModel.TransactionCallback();
             viewModel.TransactionCallback(ko.mapping.toJS(update));
             document.getElementById("trxn-amount").focus();
             //bind auto complete
             viewModel.SetBankAutoComplete('PaymentMaker');*/
            break;
    }
}


function ReloadDataAfterCalculate() {
    switch (fromName) {
        case "PaymentMaker":
            viewModel.Rate = formatNumber(viewModel.PaymentMaker().Payment.Rate);
            viewModel.Amount = formatNumber(viewModel.PaymentMaker().Payment.Amount);

            var update = viewModel.PaymentMaker();
            viewModel.PaymentMaker(ko.mapping.toJS(update));
            document.getElementById("trxn-amount").focus();
            //bind auto complete
            viewModel.SetBankAutoComplete('PaymentMaker');
            break;
        case "TransactionMaker":
            viewModel.Rate = formatNumber(viewModel.TransactionMaker().Transaction.Rate);
            viewModel.Amount = formatNumber(viewModel.TransactionMaker().Transaction.Amount);

            var update = viewModel.TransactionMaker();
            viewModel.TransactionMaker(ko.mapping.toJS(update));
            document.getElementById("trxn-amount").focus();
            //bind auto complete
            viewModel.SetBankAutoComplete('PaymentMaker');
            break;
        case "TransactionMakerAfterCaller":
            viewModel.Rate = formatNumber(viewModel.TransactionMaker().Transaction.Rate);
            viewModel.Amount = formatNumber(viewModel.TransactionMaker().Transaction.Amount);

            var update = viewModel.TransactionMaker();
            viewModel.TransactionMaker(ko.mapping.toJS(update));
            document.getElementById("trxn-amount").focus();
            //bind auto complete
            viewModel.SetBankAutoComplete('PaymentMaker');
            break;
        case "TransactionCaller":
            viewModel.Rate = formatNumber(viewModel.TransactionCallback().Transaction.Rate);
            viewModel.Amount = formatNumber(viewModel.TransactionCallback().Transaction.Amount);

            var update = viewModel.TransactionCallback();
            viewModel.TransactionCallback(ko.mapping.toJS(update));
            document.getElementById("trxn-amount").focus();
            //bind auto complete
            viewModel.SetBankAutoComplete('PaymentMaker');
            /*var update = viewModel.TransactionCallback();
             viewModel.TransactionCallback(ko.mapping.toJS(update));
             document.getElementById("trxn-amount").focus();
             //bind auto complete
             viewModel.SetBankAutoComplete('PaymentMaker');*/
            break;
    }
}

// Autocomplete
function OnSuccessAutoComplete(response, data, textStatus, jqXHR) {
    response($.map(data, function (item) {
            return {
                // default autocomplete object
                label: item.CIF + " - " + item.Name,
                value: item.Name,

                // custom object binding
                data: {
                    CIF: item.CIF,
                    Name: item.Name,
                    POAName: item.POAName,
                    Accounts: item.Accounts,
                    BizSegment: item.BizSegment,
                    Branch: item.Branch,
                    Contacts: item.Contacts,
                    Functions: item.Functions,
                    RM: item.RM,
                    Type: item.Type,
                    Underlyings: item.Underlyings,
                    LastModifiedBy: item.LastModifiedBy,
                    LastModifiedDate: item.LastModifiedDate
                }
            }
        })
    );
}

// Function to display notification.
// class name : gritter-success, gritter-warning, gritter-error, gritter-info
function ShowNotification(title, text, className, isSticky) {
    $.gritter.add({
        title: title,
        text: text,
        class_name: className,
        sticky: isSticky
    });
}
// chandra
function UpdatePaymentModel(data) {
    switch (data.name) {
        case "bene-name":
            viewModel.PaymentMaker().Payment.BeneName = data.value;
            break;
        case "bene-acc-number":
            viewModel.PaymentMaker().Payment.BeneAccNumber = data.value;
            break;
        default:
            break;
    }
    var update = viewModel.PaymentMaker();
    viewModel.PaymentMaker(ko.mapping.toJS(update));
    //bind auto complete
    viewModel.SetBankAutoComplete('PaymentMaker');
}

// Get SPUser from cookies
function GetCurrentUser() {
    //if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
    if ($.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);

        viewModel.SPUser = spUser;

        //alert(JSON.stringify(vm.SPUser));

        //Helper.Signal.Connect(OnSuccessSignal, OnErrorSignal, OnReceivedSignal);

        // load hub script
        //$.getScript(config.signal.server +"Scripts/hubs.js");

    }
}

// Token validation On Success function
function TokenOnSuccess(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // call spuser function
    GetCurrentUser(viewModel);

    // call get data inside view model
    FXModel.token = accessToken;
    GetParameterData(FXModel, OnSuccessGetTotal, OnError);
    viewModel.GetParameters();
    viewModel.GetData();
    viewModel.GetRateIDR();
    viewModel.GetFCYIDRTreshold();
}

// Token validation On Error function
function TokenOnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

//
function OnSuccessSignal() {
    //ShowNotification("Connection Success", "Connected to signal server", "gritter-success", false);
    /*    var hub = $.connection.task;
     $.connection.hub.url = config.signal.server+ "task";

     $.connection.hub.start()
     .done(function () {
     alert("Connected");
     })
     .fail(function() {
     alert("Connection failed!");
     });*/
}

function OnErrorSignal() {
    ShowNotification("Connection Failed", "Failed connected to signal server", "gritter-error", true);
}

function OnReceivedSignal(data) {
    ShowNotification(data.Sender, data.Message, "gritter-info", false);
}

// SignalR ----------------------
var taskHub = $.hubConnection(config.signal.server + "hub");
var taskProxy = taskHub.createHubProxy("TaskService");
var IsTaskHubConnected = false;

function StartTaskHub() {
    taskHub.start()
        .done(function () {
            //alert("Connected");
            /*var msg = { From: spUser.DisplayName, Message: "Hahaha"};
             taskProxy.invoke("Send", msg)
             .done(function(result) {
             //alert("ok: "+ result);
             })
             .fail(function(err) {
             alert("error: "+err);
             });*/

            /*var checkData = {
             WorkflowInstanceID: "dsadas sadsad",
             TaskID: 99
             };
             // check current task status
             taskProxy.invoke("CheckTask", checkData)
             .fail(function(err) {
             ShowNotification("Checking Task Failed", err, "gritter-error", true);
             }
             );*/

            /*var data = {
             WorkflowInstanceID: "dsadas sadsad",
             TaskID: 99,
             LoginName: spUser.LoginName,
             DisplayName: spUser.DisplayName
             };

             // try lock current task
             taskProxy.invoke("LockTask", data)
             .fail(function(err) {
             ShowNotification("Lock Task Failed", err, "gritter-error", true);
             }
             );*/
            //alert('success');
            IsTaskHubConnected = true;
        })
        .fail(function () {
            ShowNotification("Hub Connection Failed", "Fail while try connecting to hub server", "gritter-error", true);
        }
    );
}

function LockAndUnlockTaskHub(lockType, instanceId, approverID, taskId, activityTitle) {
    if (IsTaskHubConnected) {
        var data = {
            WorkflowInstanceID: instanceId,
            ApproverID: approverID,
            TaskID: taskId,
            ActivityTitle: activityTitle,
            LoginName: spUser.LoginName,
            DisplayName: spUser.DisplayName
        };

        var LockType = lockType.toLowerCase() == "lock" ? "LockTask" : "UnlockTask";

        // try lock current task
        taskProxy.invoke(LockType, data)
            .fail(function (err) {
                ShowNotification("LockType Task Failed", err, "gritter-error", true);
            }
        );
    } else {
        ShowNotification("Task Manager", "There is no active connection to task server manager.", "gritter-error", true);
    }
}

function OnSuccessBankAutoComplete(response, data, textStatus, jqXHR) {

    switch (viewModel.ActivityTitle()) {
        case "PPU Maker Task":
        case "PPU Maker After Checker and Caller Task":
            //reset bene bank mapped data
            viewModel.TransactionMaker().Transaction.Bank.SwiftCode = '';
            viewModel.TransactionMaker().Transaction.Bank.BankAccount = '';
            viewModel.TransactionMaker().Transaction.Bank.Code = '999';
            viewModel.TransactionMaker().Transaction.IsOtherBeneBank = true;
            break;
        case "Payment Maker Task":
            //reset bene bank mapped data
            viewModel.PaymentMaker().Payment.Bank.SwiftCode = '';
            viewModel.PaymentMaker().Payment.Bank.BankAccount = '';
            viewModel.PaymentMaker().Payment.Bank.Code = '999';
            viewModel.PaymentMaker().Payment.IsOtherBeneBank = true;
            break;
        case "PPU Caller Task":
            viewModel.TransactionCallback().Transaction.Bank.SwiftCode = '';
            viewModel.TransactionCallback().Transaction.Bank.BankAccount = '';
            viewModel.TransactionCallback().Transaction.Bank.Code = '999';
            viewModel.TransactionCallback().Transaction.IsOtherBeneBank = true;
            break;
    }

    viewModel.IsOtherBank(false);
    $('#bank-code').val('');
    $('#bank-code').prop('disabled', false);
    //$('#bene-acc-number').val('');

    response($.map(data, function (item) {
            return {
                // default autocomplete object
                label: item.Description,
                value: item.Description,

                // custom object binding
                data: {
                    BankAccount: item.BankAccount,
                    BranchCode: item.BranchCode,
                    Code: item.Code,
                    CommonName: item.CommonName,
                    Currency: item.Currency,
                    Description: item.Description,
                    ID: item.ID,
                    LastModifiedBy: item.LastModifiedBy,
                    LastModifiedDate: item.LastModifiedDate,
                    PGSL: item.PGSL,
                    SwiftCode: item.SwiftCode
                }
            }
        })
    );
}



taskProxy.on("Message", function (data) {
    ShowNotification(data.From, data.Message, "gritter-info", false);
});

taskProxy.on("NotifyInfo", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-info", false);
});

taskProxy.on("NotifyWarning", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-warning", false);
});

taskProxy.on("NotifyError", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-error", true);
});

function OnSuccessGetTotal(dataCall) {
    idrrate = dataCall.RateIDR;
    viewModel.FCYIDRTreshold(dataCall.FCYIDRTrashHold);
    viewModel.Treshold(dataCall.TreshHold);
}
function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}
function OnSuccessTotal(totalData, currentAmount) {
    viewModel.GetCalculateFX(0);
    if (totalData != null) {
        if (totalData.Total != null) {
            viewModel.TotalTransFX(totalData.Total.IDRFCYPPU);
            if (currentAmount != null) {
                TotalPPUModel.Total.UtilizationPPU -= currentAmount;
                var totalRemainingTransaction = TotalPPUModel.Total.UtilizationPPU;
                viewModel.TotalUtilization(totalRemainingTransaction + currentAmount);
            } else {
                viewModel.TotalUtilization(totalData.Total.UtilizationPPU);
            }
        }
    }
}
