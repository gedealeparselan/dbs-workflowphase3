﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property

    [ModelName("HomeDraft")]
    public class HomeDraftModel
    {

        public Guid ID { get; set; }

        public string WorkflowTaskTitle { get; set; }

        public string WorkflowTaskActivityTitle { get; set; }


        public DateTime WorkflowTaskEntryTime { get; set; }

        public string WorkflowInitiator { get; set; }

        public long WorkflowApproverID { get; set; }

        public int? WorkflowTaskType { get; set; }

        public Guid SPTaskListID { get; set; }
        public int SPTaskListItemID { get; set; }

    }

    public class HomeDraftRow : HomeDraftModel
    {
        public int RowID { get; set; }

    }

    public class HomeDraftGrid : Grid
    {
        public IList<HomeDraftRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class HomeDraftFilter : Filter { }
    #endregion

    #region Interface
    public interface IHomeDraftRepository : IDisposable
    {
        bool GetByID(Guid id, ref HomeDraftModel rec, ref string message);
        bool Get(ref IList<HomeDraftModel> roles, int limit, int index, ref string message);
        bool Get(ref IList<HomeDraftModel> roles, ref string message);
        bool Get(int page, int size, IList<HomeDraftFilter> filters, string sortColumn, string sortOrder, ref HomeDraftGrid grid, ref string message);
        bool Get(HomeDraftFilter filter, ref IList<HomeDraftModel> roles, ref string message);
        bool Get(string key, int limit, ref IList<HomeDraftModel> roles, ref string message);
        //bool Add(HomeDraftModel role, ref string message);
        //bool Update(Guid id, HomeDraftModel role, ref string message);
        //bool Delete(Guid id, ref string message);
    }
    #endregion

    #region Repository
    public class HomeDraftRepository : IHomeDraftRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetByID(Guid id, ref HomeDraftModel rec, ref string message)
        {
            bool isSuccess = false;

            try
            {
                rec = (from a in context.V_NintexWorkflowTasks
                       where a.WorkflowInstanceID == id
                       select new HomeDraftModel
                              {
                                  ID = (Guid)a.WorkflowInstanceID,
                                  WorkflowTaskTitle = a.WorkflowTaskTitle,
                                  WorkflowTaskActivityTitle = a.WorkflowTaskActivityTitle,
                                  WorkflowTaskEntryTime = a.WorkflowTaskEntryTime,
                                  WorkflowInitiator = a.WorkflowInitiator,
                                  WorkflowApproverID = a.WorkflowApproverID,
                                  WorkflowTaskType = a.WorkflowTaskType,
                                  SPTaskListID = (Guid)(a.SPTaskListID == null ? Guid.Empty : a.SPTaskListID),
                                  SPTaskListItemID = (int)(a.SPTaskListItemID == null ? 0 : a.SPTaskListItemID)
                              }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool Get(ref IList<HomeDraftModel> rec, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    rec = (from a in context.V_NintexWorkflowTasks
                           where a.WorkflowOutcomeDescription.Equals("Pending")
                           orderby new { a.WorkflowTaskEntryTime }
                           select new HomeDraftModel
                                    {
                                        ID = (Guid)a.WorkflowInstanceID,
                                        WorkflowTaskTitle = a.WorkflowTaskTitle,
                                        WorkflowTaskActivityTitle = a.WorkflowTaskActivityTitle,
                                        WorkflowTaskEntryTime = a.WorkflowTaskEntryTime,
                                        WorkflowInitiator = a.WorkflowInitiator,
                                        WorkflowApproverID = a.WorkflowApproverID,
                                        WorkflowTaskType = a.WorkflowTaskType,
                                        SPTaskListID = (Guid)(a.SPTaskListID == null ? Guid.Empty : a.SPTaskListID),
                                        SPTaskListItemID = (int)(a.SPTaskListItemID == null ? 0 : a.SPTaskListItemID)
                                    }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool Get(ref IList<HomeDraftModel> recs, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    recs = (from a in context.V_NintexWorkflowTasks
                            where a.WorkflowOutcomeDescription.Equals("Pending")
                            orderby new { a.WorkflowTaskEntryTime }
                            select new HomeDraftModel
                                    {
                                        ID = (Guid)a.WorkflowInstanceID,
                                        WorkflowTaskTitle = a.WorkflowTaskTitle,
                                        WorkflowTaskActivityTitle = a.WorkflowTaskActivityTitle,
                                        WorkflowTaskEntryTime = a.WorkflowTaskEntryTime,
                                        WorkflowInitiator = a.WorkflowInitiator,
                                        WorkflowApproverID = a.WorkflowApproverID,
                                        WorkflowTaskType = a.WorkflowTaskType,
                                        SPTaskListID = (Guid)(a.SPTaskListID == null ? Guid.Empty : a.SPTaskListID),
                                        SPTaskListItemID = (int)(a.SPTaskListItemID == null ? 0 : a.SPTaskListItemID)
                                    }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool Get(int page, int size, IList<HomeDraftFilter> filters, string sortColumn, string sortOrder, ref HomeDraftGrid grid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                HomeDraftModel filter = new HomeDraftModel();
                if (filters != null)
                {
                    filter.WorkflowTaskTitle = (string)filters.Where(a => a.Field.Equals("WorkflowTaskTitle")).Select(a => a.Value).SingleOrDefault();
                    filter.WorkflowTaskActivityTitle = (string)filters.Where(a => a.Field.Equals("WorkflowTaskActivityTitle")).Select(a => a.Value).SingleOrDefault();
                    //filter.WorkflowTaskEntryTime = filters.Where(a => a.Field.Equals("WorkflowTaskEntryTime")).Select(a => a.Value).SingleOrDefault();
                    filter.WorkflowInitiator = (string)filters.Where(a => a.Field.Equals("WorkflowInitiator")).Select(a => a.Value).SingleOrDefault();


                    if (filters.Where(a => a.Field.Equals("WorkflowTaskEntryTime")).Any())
                    {
                        filter.WorkflowTaskEntryTime = DateTime.Parse((string)filters.Where(a => a.Field.Equals("WorkflowTaskEntryTime")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.WorkflowTaskEntryTime.AddDays(1);
                    }
                }

                string[] roles = currentUser.GetCurrentUser().Roles.Select(r => r.Name).ToArray();
                DateTime defaultDate = Convert.ToDateTime("1/1/0001 12:00:00 AM");
                //Guid aaa = Guid.Parse("6f2d8205-0000-0000-0000-a75ad3c3aa0e");

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.V_NintexWorkflowTasks
                                let isWorkflowTaskTitle = !string.IsNullOrEmpty(filter.WorkflowTaskTitle)
                                let isWorkflowTaskActivityTitle = !string.IsNullOrEmpty(filter.WorkflowTaskActivityTitle)
                                let isWorkflowInitiator = !string.IsNullOrEmpty(filter.WorkflowInitiator)
                                let isWorkflowTaskEntryTime = filter.WorkflowTaskEntryTime.Date != defaultDate.Date ? true : false
                                where
                                    roles.Any(x => a.WorkflowTaskUserContribute.Contains(x)) &&
                                    a.WorkflowOutcomeDescription.Equals("Pending") &&
                                    (a.WorkflowTaskActivityTitle.Equals("Master Role Approval Task") ||
                                    a.WorkflowTaskActivityTitle.Equals("Master User Approval Task") ||
                                    a.WorkflowTaskActivityTitle.Equals("Master Underlying Approval Task")) &&
                                    (isWorkflowTaskTitle ? a.WorkflowTaskTitle.Contains(filter.WorkflowTaskTitle) : true) &&
                                    (isWorkflowTaskActivityTitle ? a.WorkflowTaskActivityTitle.Contains(filter.WorkflowTaskActivityTitle) : true) &&
                                    (isWorkflowInitiator ? a.WorkflowInitiator.Contains(filter.WorkflowInitiator) : true) &&
                                    (isWorkflowTaskEntryTime ? (a.WorkflowTaskEntryTime > filter.WorkflowTaskEntryTime && a.WorkflowTaskEntryTime < maxDate) : true)

                                //(isWorkflowInitiator ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.WorkflowInitiator) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                //(isWorkflowTaskEntryTime ? ((a.WorkflowTaskEntryTime == null ? a.WorkflowTaskEntryTime : a.WorkflowTaskEntryTime) > filter.WorkflowTaskEntryTime.Value && ((a.WorkflowTaskEntryTime == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new HomeDraftModel
                                {
                                    ID = (Guid)a.WorkflowInstanceID,
                                    WorkflowTaskTitle = a.WorkflowTaskTitle,
                                    WorkflowTaskActivityTitle = a.WorkflowTaskActivityTitle,
                                    WorkflowTaskEntryTime = a.WorkflowTaskEntryTime,
                                    WorkflowInitiator = a.WorkflowInitiator,
                                    WorkflowApproverID = a.WorkflowApproverID,
                                    WorkflowTaskType = a.WorkflowTaskType,
                                    SPTaskListID = (Guid)(a.SPTaskListID == null ? Guid.Empty : a.SPTaskListID),
                                    SPTaskListItemID = (int)(a.SPTaskListItemID == null ? 0 : a.SPTaskListItemID)
                                });

                    grid.Page = page;
                    grid.Size = size;
                    grid.Total = data.Count();
                    grid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    grid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new HomeDraftRow
                        {
                            RowID = i + 1,
                            ID = (Guid)a.ID,
                            WorkflowTaskTitle = a.WorkflowTaskTitle,
                            WorkflowTaskActivityTitle = a.WorkflowTaskActivityTitle,
                            WorkflowTaskEntryTime = a.WorkflowTaskEntryTime,
                            WorkflowInitiator = a.WorkflowInitiator,
                            WorkflowApproverID = a.WorkflowApproverID,
                            WorkflowTaskType = a.WorkflowTaskType,
                            SPTaskListID = (Guid)(a.SPTaskListID == null ? Guid.Empty : a.SPTaskListID),
                            SPTaskListItemID = (int)(a.SPTaskListItemID == null ? 0 : a.SPTaskListItemID)
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool Get(HomeDraftFilter filter, ref IList<HomeDraftModel> rec, ref string message)
        {
            throw new NotImplementedException();
        }

        /*    public bool Get(string key, int limit, ref IList<HomeDraftModel> rec, ref string message)
            {
                bool isSuccess = false;

                try
                {
                    List<Guid> wfInstance = (from a in context.V)

                    rec = (from a in context.V_NintexWorkflowTasks
                           where a.WorkflowOutcomeDescription.Equals("Pending")
                           && a.WorkflowTaskTitle.Contains(key)
                           || a.WorkflowTaskActivityTitle.Contains(key)
                           || a.WorkflowInitiator.Contains(key)
                           orderby new { a.WorkflowTaskTitle, a.WorkflowTaskActivityTitle, a.WorkflowTaskEntryTime, a.WorkflowInitiator }
                           select new HomeDraftModel
                           {
                               WorkflowTaskTitle = a.WorkflowTaskTitle,
                               WorkflowTaskActivityTitle = a.WorkflowTaskActivityTitle,
                               WorkflowTaskEntryTime = a.WorkflowTaskEntryTime,
                               WorkflowInitiator = a.WorkflowInitiator,
                               WorkflowApproverID = a.WorkflowApproverID,
                               WorkflowTaskType = a.WorkflowTaskType,
                               SPTaskListID = (Guid)(a.SPTaskListID == null ? Guid.Empty : a.SPTaskListID),
                               SPTaskListItemID = (int)(a.SPTaskListItemID == null ? 0 : a.SPTaskListItemID)
                           }).Take(limit).ToList();

                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }

                return isSuccess;
            } */
        /*
        public bool Add(HomeDraftModel rec, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.V_NintexWorkflowTasks.Where(a => a.WorkflowLastActivityTitle.Equals(rec.WorkflowLastActivityTitle) && a.WorkflowOutcomeDescription.Equals("Pending")).Any())
                {
                    Entity.V_NintexWorkflowTasks item = new Entity.V_NintexWorkflowTasks()
                    {
                        WorkflowTaskTitle = a.WorkflowTaskTitle,
                        WorkflowTaskActivityTitle = a.WorkflowTaskActivityTitle,
                        WorkflowTaskEntryTime = a.WorkflowTaskEntryTime,
                        WorkflowInitiator = a.WorkflowInitiator

                    };
                    context.V_NintexWorkflowTasks.Add(item);

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Role data for Role Name {0} is already exist.", role.Name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }*/
        /*
        public bool Update(Guid id, HomeDraftModel role, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.V_NintexWorkflowTasks data = context.V_NintexWorkflowTasks.Where(a => a.RoleID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.V_NintexWorkflowTasks.Where(a => a.RoleID != role.ID && a.RoleName.Equals(role.Name) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Role Name {0} is exist.", role.Name);
                    }
                    else
                    {
                        data.RoleName = role.Name;
                        data.RoleDescription = role.Description;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Role data for Role ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }*/
        /*
        public bool Delete(Guid id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.V_NintexWorkflowTasks data = context.V_NintexWorkflowTasks.Where(a => a.WorkflowInstanceID.Equals(id)).SingleOrDefault();

                if (data != null)
                {

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Workflow Task data for Workflow ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }*/

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public bool Get(string key, int limit, ref IList<HomeDraftModel> roles, ref string message)
        {
            throw new NotImplementedException();
        }
    }
    #endregion
}