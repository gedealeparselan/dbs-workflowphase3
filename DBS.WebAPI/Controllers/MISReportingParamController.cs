﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class MISReportingParamController : ApiController
    {
        private string message = string.Empty;


        private readonly IMISTransactionType repoTransactionType = null;
        private readonly IMISSegmentation repoSegmentation = null;
        private readonly IMISChannel repoChannel = null;
        private readonly IMISBranch repoBranch = null;

        public MISReportingParamController()
        {
            this.repoTransactionType = new MISTransactionTypeRepository();
            this.repoSegmentation = new MISSegmentationRepository();
            this.repoChannel = new MISChannelRepository();
            this.repoBranch = new MISBranchRepository();
        }

        public HttpResponseMessage Get(string select = "TransactionType,Segmentation,Channel,Branch")
        {
            IDictionary<string, object> output = new Dictionary<string, object>();
            string messages = string.Empty;

            if (!string.IsNullOrEmpty(select))
            {
                string[] val = select.Split(',');

                foreach (string str in val)
                {
                    switch (str.ToLower())
                    {
                        case "transactiontype" :
                            IList<MISReportingTransactionTypeModel> transactionTypes = new List<MISReportingTransactionTypeModel>();
                            if (!repoTransactionType.GetMISReportingTransactionType(ref transactionTypes, ref message))
                            {
                                messages += string.Concat("Error Transaction Type : ", message);
                            }
                            else
                            {
                                output.Add("TransactionType", transactionTypes); 
                            }
                            transactionTypes = null;
                            break;

                        case "segmentation" :
                            IList<MISReportingSegmentationModel> segmentations = new List<MISReportingSegmentationModel>();
                            if (!repoSegmentation.GetMISReportingSegmentation(ref segmentations, ref message))
                            {
                                messages += string.Concat("Error Segmentation : ", message);
                            }
                            else
                            {
                                output.Add("Segmentation", segmentations); 
                            }
                            segmentations = null;
                            break;

                        case "channel" :
                            IList<MISReportingChannelModel> channels = new List<MISReportingChannelModel>();
                            if (!repoChannel.GetMISReportingChannel(ref channels, ref message))
                            {
                                messages += string.Concat("Error Channel : ", message);
                            }
                            else
                            {
                                output.Add("Channel", channels); 
                            }
                            channels = null;
                            break;

                        case "branch" :
                            IList<MISReportingBranchModel> branchs = new List<MISReportingBranchModel>();
                            if (!repoBranch.GetMISReportingBranch(ref branchs, ref message))
                            {
                                messages += string.Concat("Error Branch : ", message);
                            }
                            else
                            {
                                output.Add("Branch", branchs); 
                            }
                            branchs = null;
                            break;
                    }
                }
            }

            if (output == null)
                return Request.CreateResponse(HttpStatusCode.NoContent);
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repoTransactionType != null)
                repoTransactionType.Dispose();
            if (repoSegmentation != null)
                repoSegmentation.Dispose();
            if (repoChannel != null)
                repoChannel.Dispose();
            if (repoBranch != null)
                repoBranch.Dispose();
            
            base.Dispose(disposing);
        }

    }
}
