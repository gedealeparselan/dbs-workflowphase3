﻿using DBS.Entity;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq.Dynamic;
using System.Linq;
using System.Web;

namespace DBS.WebAPI.Models
{
    

    #region Model
    public class Loan
    {
        public long FDMaturityID { get; set; }
        public bool FDIMSelect { get; set; }
        public string SchemeType { get; set; }
        public string AccNumber { get; set; }
        public string IntTableCode { get; set; }
        public string Version { get; set; }
        public string IntCr { get; set; }
        public decimal IntDr { get; set; }
        public string IntPegged { get; set; }
        public string PegReviewDate { get; set; }
        public string PegFreq { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal IntMargin { get; set; }
        
    }


    public class LoanRow : Loan
    {
        public int RowID { get; set; }
    }

    public class LoanRowModel
    {
        public int RowID { get; set; }
        public long FDMaturityID { get; set; }
        public bool FDIMSelect { get; set; }
        public string SchemeType { get; set; }
        public string AccNumber { get; set; }
        public string IntTableCode { get; set; }
        public string Version { get; set; }
        public string IntCr { get; set; }
        public string IntDr { get; set; }
        public string IntPegged { get; set; }
        public string PegReviewDate { get; set; }
        public string PegFreq { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string IntMargin { get; set; }
    }

    public class LoanGrid : Grid
    {
        public IList<LoanRowModel> Rows { get; set; }
    }
    #endregion

    #region Filter
    public class LoanFilter : Filter { }
    #endregion

    #region Interface
    public interface ILoanGenerateRepository : IDisposable
    {
        bool GetLoanInterestMaintenance(DateTime? date, ref IList<Loan> output, ref string message);
        bool GetLoanInterestMaintenance(DateTime? date, int page, int size, IList<LoanFilter> filters, string sortColumn, string sortOrder, ref LoanGrid transaction, ref string message);
        bool GetLoanRollover(DateTime? date, ref IList<Loan> output, ref string message);
        bool GetLoanRollover(DateTime? date, int page, int size, IList<LoanFilter> filters, string sortColumn, string sortOrder, ref LoanGrid transaction, ref string message);
    }
    #endregion

    #region Repository
    public class LoanGenerateRepository : ILoanGenerateRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);

            GC.Collect();
            GC.SuppressFinalize(this);
        }

        public bool GetLoanInterestMaintenance(DateTime? date, ref IList<Loan> output, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    output = (from a in context.V_GenerateLoanInterestMaintenance
                              where a.Start_Date.Value.Year == date.Value.Year
                              && a.Start_Date.Value.Month == date.Value.Month
                              && a.Start_Date.Value.Day == date.Value.Day
                              select new Loan
                              {
                                  IntCr = a.A_c__Pref__Int___Cr__,
                                  IntDr = a.A_c__Pref__Int___Dr__ ?? 0,
                                  AccNumber = a.Account_Number,
                                  EndDate = a.End_Date,
                                  IntPegged = a.Int__Pegged,
                                  IntTableCode = a.Int__Table_Code,
                                  IntMargin = a.Interest_Margin ?? 0,
                                  PegReviewDate = a.Peg_Review_Date,
                                  PegFreq = a.Peggin_Freq___Month_Days_,
                                  SchemeType = a.Scheme_Type,
                                  StartDate = a.Start_Date,
                                  Version = a.Version,
                                  FDIMSelect = false,
                                  FDMaturityID = a.CSOInterestMaintenanceID
                              }).ToList(); ;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetLoanInterestMaintenance(DateTime? date, int page, int size, IList<LoanFilter> filters, string sortColumn, string sortOrder, ref LoanGrid transaction, ref string message)
        {
            bool isSuccess = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();
                Loan filter = new Loan();
                if (filters != null)
                {
                    //filter.IntCr = (string)filters.Where(a => a.Field.Equals("IntCr")).Select(a => string.IsNullOrEmpty(a.Value) ? "0" : a.Value).SingleOrDefault();
                    filter.IntCr = (string)filters.Where(a => a.Field.Equals("IntCr")).Select(a => a.Value).SingleOrDefault();
                    filter.IntDr = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("IntDr")).Select(a => string.IsNullOrEmpty(a.Value) ? "0" : a.Value).SingleOrDefault());
                    filter.AccNumber = (string)filters.Where(a => a.Field.Equals("AccountNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.Version = (string)filters.Where(a => a.Field.Equals("Version")).Select(a => a.Value).SingleOrDefault();
                    filter.IntPegged = (string)filters.Where(a => a.Field.Equals("IntPeg")).Select(a => a.Value).SingleOrDefault();
                    filter.IntTableCode = (string)filters.Where(a => a.Field.Equals("IntTableCode")).Select(a => a.Value).SingleOrDefault();
                    filter.IntMargin = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("InterestMargin")).Select(a => a.Value).SingleOrDefault());
                    filter.PegReviewDate = (string)filters.Where(a => a.Field.Equals("PegReviewDate")).Select(a => a.Value == null ? "0" : a.Value).SingleOrDefault();
                    filter.PegFreq = (string)filters.Where(a => a.Field.Equals("PegginFreq")).Select(a => a.Value).SingleOrDefault();
                    filter.SchemeType = (string)filters.Where(a => a.Field.Equals("SchemeType")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("StartDate")).Any())
                    {
                        filter.StartDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("StartDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("EndDate")).Any())
                    {
                        filter.EndDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EndDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.EndDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.V_GenerateLoanInterestMaintenance

                                let isSchemeType = !string.IsNullOrEmpty(filter.SchemeType)
                                let isAccNumber = !string.IsNullOrEmpty(filter.AccNumber)
                                let isIntTableCode = !string.IsNullOrEmpty(filter.IntTableCode)
                                let isVersion = !string.IsNullOrEmpty(filter.Version)
                                let isIntCr = !string.IsNullOrEmpty(filter.IntCr)
                                let isIntDr = filter.IntDr > 0 ? true : false
                                let isIntPegged = !string.IsNullOrEmpty(filter.IntPegged)
                                let isPegFreq = !string.IsNullOrEmpty(filter.PegFreq)
                                let isPegReviewDate = !string.IsNullOrEmpty(filter.PegReviewDate)
                                let isIntMargin = filter.IntMargin > 0 ? true : false
                                let isStartDate = filter.StartDate.HasValue ? true : false
                                let isEndDate = filter.EndDate.HasValue ? true : false

                                where
                                    a.Start_Date.Value.Year == date.Value.Year
                                    && a.Start_Date.Value.Month == date.Value.Month
                                    && a.Start_Date.Value.Day == date.Value.Day &&
                                    (isSchemeType ? a.Scheme_Type.Contains(filter.SchemeType) : true) &&
                                    (isAccNumber ? a.Account_Number.Contains(filter.AccNumber) : true) &&
                                    (isIntTableCode ? a.Int__Table_Code.Contains(filter.IntTableCode) : true) &&
                                    (isVersion ? a.Version.Contains(filter.Version) : true) &&
                                    (isIntCr ? a.A_c__Pref__Int___Cr__ == filter.IntCr : true) &&
                                    (isIntDr ? a.A_c__Pref__Int___Dr__.Value == filter.IntDr : true) &&
                                    (isIntPegged ? a.Int__Pegged.Contains(filter.IntPegged) : true) &&
                                    (isPegFreq ? a.Peggin_Freq___Month_Days_.Contains(filter.PegFreq) : true) &&
                                    (isPegReviewDate ? a.Peg_Review_Date.Contains(filter.PegReviewDate) : true) &&
                                    (isIntMargin ? a.Interest_Margin.Value == filter.IntMargin : true) &&
                                    (isStartDate ? a.Start_Date.Value == filter.StartDate.Value : true) &&
                                    (isEndDate ? a.End_Date.Value == filter.EndDate.Value : true)

                                select new Loan
                                {
                                    Version = a.Version,
                                    StartDate = a.Start_Date,
                                    SchemeType = a.Scheme_Type,
                                    PegFreq = a.Peggin_Freq___Month_Days_,
                                    PegReviewDate = a.Peg_Review_Date,
                                    IntMargin = a.Interest_Margin.Value,
                                    IntTableCode = a.Int__Table_Code,
                                    IntPegged = a.Int__Pegged,
                                    EndDate = a.End_Date,
                                    AccNumber = a.Account_Number,
                                    IntDr = a.A_c__Pref__Int___Dr__.Value,
                                    IntCr = a.A_c__Pref__Int___Cr__,
                                    FDIMSelect = false,
                                    FDMaturityID = a.CSOInterestMaintenanceID
                                });

                    transaction.Page = page;
                    transaction.Size = size;
                    transaction.Total = data.Count();
                    transaction.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    transaction.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new LoanRowModel
                        {
                            RowID = i + 1,
                            IntCr = a.IntCr,
                            IntDr = String.Format("{0:N6}",a.IntDr),
                            AccNumber = a.AccNumber,
                            EndDate = a.EndDate,
                            IntPegged = a.IntPegged,
                            Version = a.Version,
                            IntTableCode = a.IntTableCode,
                            IntMargin = String.Format("{0:N6}", a.IntMargin),
                            PegReviewDate = a.PegReviewDate,
                            PegFreq = a.PegFreq,
                            SchemeType = a.SchemeType,
                            StartDate = a.StartDate,
                            FDIMSelect = a.FDIMSelect,
                            FDMaturityID = a.FDMaturityID
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }


        public bool GetLoanRollover(DateTime? date, ref IList<Loan> output, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    output = (from a in context.V_GenerateLoanRollover
                              where a.Start_Date.Value.Year == date.Value.Year
                              && a.Start_Date.Value.Month == date.Value.Month
                              && a.Start_Date.Value.Day == date.Value.Day
                              select new Loan
                              {
                                  IntCr = a.A_c__Pref__Int___Cr__,
                                  IntDr = a.A_c__Pref__Int___Dr__ ?? 0,
                                  AccNumber = a.Account_Number,
                                  EndDate = a.End_Date,
                                  IntPegged = a.Int__Pegged,
                                  IntTableCode = a.Int__Table_Code,
                                  IntMargin = a.Interest_Margin ?? 0,
                                  PegReviewDate = a.Peg_Review_Date,
                                  PegFreq = a.Peggin_Freq___Month_Days_,
                                  SchemeType = a.Scheme_Type,
                                  StartDate = a.Start_Date,
                                  Version = a.Version,
                                  FDIMSelect = false,
                                  FDMaturityID = a.CSORolloverID
                              }).ToList(); ;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetLoanRollover(DateTime? date, int page, int size, IList<LoanFilter> filters, string sortColumn, string sortOrder, ref LoanGrid transaction, ref string message)
        {
            bool isSuccess = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();
                 Loan filter = new Loan();
                 if (filters != null)
                 {
                     //filter.IntCr = (string)filters.Where(a => a.Field.Equals("IntCr")).Select(a => string.IsNullOrEmpty(a.Value) ? "0" : a.Value).SingleOrDefault();
                     filter.IntCr = (string)filters.Where(a => a.Field.Equals("IntCr")).Select(a => a.Value).SingleOrDefault();
                     filter.IntDr = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("IntDr")).Select(a => string.IsNullOrEmpty(a.Value) ? "0" : a.Value).SingleOrDefault());
                     filter.AccNumber = (string)filters.Where(a => a.Field.Equals("AccountNumber")).Select(a => a.Value).SingleOrDefault();
                     filter.Version = (string)filters.Where(a => a.Field.Equals("Version")).Select(a => a.Value).SingleOrDefault();
                     filter.IntPegged = (string)filters.Where(a => a.Field.Equals("IntPeg")).Select(a => a.Value).SingleOrDefault();
                     filter.IntTableCode = (string)filters.Where(a => a.Field.Equals("IntTableCode")).Select(a => a.Value).SingleOrDefault();
                     filter.IntMargin = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("InterestMargin")).Select(a => string.IsNullOrEmpty(a.Value) ? "0" : a.Value).SingleOrDefault());
                     filter.PegReviewDate = (string)filters.Where(a => a.Field.Equals("PegReviewDate")).Select(a => a.Value).SingleOrDefault();
                     filter.PegFreq = (string)filters.Where(a => a.Field.Equals("PegginFreq")).Select(a => a.Value).SingleOrDefault();
                     filter.SchemeType = (string)filters.Where(a => a.Field.Equals("SchemeType")).Select(a => a.Value).SingleOrDefault();
                     if (filters.Where(a => a.Field.Equals("StartDate")).Any())
                     {
                         filter.StartDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("StartDate")).Select(a => a.Value).SingleOrDefault());
                     }
                     if (filters.Where(a => a.Field.Equals("EndDate")).Any())
                     {
                         filter.EndDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EndDate")).Select(a => a.Value).SingleOrDefault());
                         maxDate = filter.EndDate.Value.AddDays(1);
                     }
                 }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.V_GenerateLoanRollover

                                let isSchemeType = !string.IsNullOrEmpty(filter.SchemeType)
                                let isAccNumber = !string.IsNullOrEmpty(filter.AccNumber)
                                let isIntTableCode = !string.IsNullOrEmpty(filter.IntTableCode)
                                let isVersion = !string.IsNullOrEmpty(filter.Version)
                                let isIntCr = !string.IsNullOrEmpty(filter.IntCr)
                                let isIntDr = filter.IntDr > 0 ? true : false
                                let isIntPegged = !string.IsNullOrEmpty(filter.IntPegged)
                                let isPegFreq = !string.IsNullOrEmpty(filter.PegFreq)
                                let isPegReviewDate = !string.IsNullOrEmpty(filter.PegReviewDate)
                                let isIntMargin = filter.IntMargin > 0 ? true : false
                                let isStartDate = filter.StartDate.HasValue ? true : false
                                let isEndDate = filter.EndDate.HasValue ? true : false

                                where
                                    a.Start_Date.Value.Year == date.Value.Year
                                    && a.Start_Date.Value.Month == date.Value.Month
                                    && a.Start_Date.Value.Day == date.Value.Day &&
                                    (isSchemeType ? a.Scheme_Type.Contains(filter.SchemeType) : true) &&
                                    (isAccNumber ? a.Account_Number.Contains(filter.AccNumber) : true) &&
                                    (isIntTableCode ? a.Int__Table_Code.Contains(filter.IntTableCode) : true) &&
                                    (isVersion ? a.Version.Contains(filter.Version) : true) &&
                                    (isIntCr ? a.A_c__Pref__Int___Cr__ == filter.IntCr : true) &&
                                    (isIntDr ? a.A_c__Pref__Int___Dr__.Value == filter.IntDr : true) &&
                                    (isIntPegged ? a.Int__Pegged.Contains(filter.IntPegged) : true) &&
                                    (isPegFreq ? a.Peggin_Freq___Month_Days_.Contains(filter.PegFreq) : true) &&
                                    (isPegReviewDate ? a.Peg_Review_Date.Contains(filter.PegReviewDate) : true) &&
                                    (isIntMargin ? a.Interest_Margin.Value == filter.IntMargin : true) &&
                                    (isStartDate ? a.Start_Date.Value == filter.StartDate.Value : true) &&
                                    (isEndDate ? a.End_Date.Value == filter.EndDate.Value : true)

                                select new Loan
                                {
                                    Version = a.Version,
                                    StartDate = a.Start_Date,
                                    SchemeType = a.Scheme_Type,
                                    PegFreq = a.Peggin_Freq___Month_Days_,
                                    PegReviewDate = a.Peg_Review_Date,
                                    IntMargin = a.Interest_Margin.Value,
                                    IntTableCode = a.Int__Table_Code,
                                    IntPegged = a.Int__Pegged,
                                    EndDate = a.End_Date,
                                    AccNumber = a.Account_Number,
                                    IntDr = a.A_c__Pref__Int___Dr__.Value,
                                    IntCr = a.A_c__Pref__Int___Cr__,
                                    FDIMSelect = false,
                                    FDMaturityID = a.CSORolloverID
                                });

                    transaction.Page = page;
                    transaction.Size = size;
                    transaction.Total = data.Count();
                    transaction.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    transaction.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new LoanRowModel
                        {
                            RowID = i + 1,
                            IntCr = a.IntCr,
                            IntDr = String.Format("{0:N6}",a.IntDr),
                            AccNumber = a.AccNumber,
                            EndDate = a.EndDate,
                            IntPegged = a.IntPegged,
                            Version = a.Version,
                            IntTableCode = a.IntTableCode,
                            IntMargin = String.Format("{0:N6}", a.IntMargin),
                            PegReviewDate = a.PegReviewDate,
                            PegFreq = a.PegFreq,
                            SchemeType = a.SchemeType,
                            StartDate = a.StartDate,
                            FDIMSelect = a.FDIMSelect,
                            FDMaturityID = a.FDMaturityID
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
    }
    #endregion
}