﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Models;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using System.Runtime.Serialization;
using System.Data.Objects.SqlClient;
using System.Data.Entity;
using System.Data;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.SqlClient;

namespace DBS.WebAPI.Models
{
    [DataContract]
    [ModelName("MISReportingTableSummary")]
    public class MISReportingTableSummaryModel
    {
        /// <summary>
        /// Date for Report Period
        /// </summary>
        public DateTime datePeriod { get; set; }

        /// <summary>
        /// Total Exceptional Transactions
        /// </summary>
        public int exceptionalTransactions { get; set; }

        /// <summary>
        /// Total SLA Transactions
        /// </summary>
        public int slaTransactions { get; set; }

        /// <summary>
        /// Breach Transactions
        /// </summary>
        public int breachTransactions { get; set; }

        /// <summary>
        /// TAT Time
        /// </summary>
        public int TATTime { get; set; }

        /// <summary>
        /// String for buffering Date 
        /// </summary>
        [DataMember(Name = "datePeriod")]
        public string strDatePeriod { get; set; }

        /// <summary>
        /// String for buffering Total Transactions
        /// </summary>
        [DataMember(Name = "totalTransactions")]
        public string strTotalTransactions { get; set; }

        /// <summary>
        /// String for buffering total Exceptional Transactions
        /// </summary>
        [DataMember(Name = "exceptionalTransactions")]
        public string strExceptionalTransactions { get; set; }

        /// <summary>
        /// String for buffering total Exceptional Transactions
        /// </summary>
        [DataMember(Name = "slaTransactions")]
        public string strSLATransactions { get; set; }

        /// <summary>
        /// String for buffering total Breach Transactions
        /// </summary>
        [DataMember(Name = "breachTransactions")]
        public string strBreachTransactions { get; set; }

        /// <summary>
        ///  String for buffering TAT Time
        /// </summary>
        [DataMember(Name = "TATTime")]
        public string strTATTime { get; set; }

        [OnSerializing]
        void OnSerializing(StreamingContext context)
        {
            this.strDatePeriod = this.datePeriod.ToString();
            this.strExceptionalTransactions = this.exceptionalTransactions.ToString();
            this.strSLATransactions = this.slaTransactions.ToString();
            this.strBreachTransactions = this.breachTransactions.ToString();
            this.strTATTime = this.TATTime.ToString();
        }
    }

    public interface IMISReportingTableSummary : IDisposable
    {
        bool GetMISReportingTableSummary(int transactionType, int segmentation, int channel, int branch, DateTime startDate, DateTime endDate, DateTime clientDateTime, ref IList<MISReportingTableSummaryModel> resultOutput, ref string message);

    }

    public class MISReportingTableSummaryRepository : IMISReportingTableSummary
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetMISReportingTableSummary(int transactionType, int segmentation, int channel, int branch, DateTime startDate, DateTime endDate, DateTime clientDateTime, ref IList<MISReportingTableSummaryModel> resultOutput, ref string message)
        {
            bool isSuccess = false;
            DateTime serverUTC = DateTime.UtcNow;
            int hourDiff, oneDayHour = 24;
            if (clientDateTime > serverUTC)
            {
                hourDiff = clientDateTime.Hour - serverUTC.Hour;
            }
            else if (clientDateTime < serverUTC)
            {
                hourDiff = oneDayHour + clientDateTime.Hour - serverUTC.Hour;
            }
            else
            {
                hourDiff = 0;
            }
            try
            {
                using (DBSEntities context = new DBSEntities())
                {

                    var ProductCount = (from a in context.Products select new { a.ProductID }).Count();
                    var BizSegmentCount = (from a in context.BizSegments select new { a.BizSegmentID }).Count();
                    var ChannelCount = (from a in context.Channels select new { a.ChannelID }).Count();
                    var BranchCount = (from a in context.Locations select new { a.LocationID }).Count();


                    bool isAllProduct = false; bool isAllSegment = false;
                    bool isAllChannel = false; bool isAllBranch = false;

                    bool isStartDateNull = false; bool isEndDateNull = false;

                    if (transactionType == ProductCount + 1)
                    {
                        isAllProduct = true;
                    }
                    if (segmentation == BizSegmentCount + 1)
                    {
                        isAllSegment = true;
                    }
                    if (channel == ChannelCount + 1)
                    {
                        isAllChannel = true;
                    }
                    if (branch == BranchCount + 1)
                    {
                        isAllBranch = true;
                    }
                    if (startDate == null)
                    {
                        isStartDateNull = true;
                    }
                    if (endDate == null)
                    {
                        isEndDateNull = true;
                    }

                    if ((startDate != null) && (endDate != null))
                    {

                        resultOutput = (from a in context.SP_GetMISReportingTableSummary(transactionType, segmentation, channel, branch, startDate, endDate, isAllProduct, isAllSegment, isAllChannel, isAllBranch)                                                                            
                                        select new
                                        {

                                            transactionsID = a.TransactionID.Value,
                                            createdDate = a.CreateDate.Value,
                                            slaTime = a.SLATime != null ? a.SLATime.Value : 0,
                                            TATTime = a.TATTime != null ? a.TATTime.Value : 0,
                                            isExceptionHandling = a.IsExceptionHandling != null ? a.IsExceptionHandling.Value : false,
                                            StateID = a.StateID != null ? a.StateID.Value : 0
                                        }
                                            into aa
                                            group aa by new
                                            {
                                                TransactionID = aa.transactionsID,
                                                CreatedDate = aa.createdDate,
                                                SLATime = aa.slaTime,
                                                TATTime = aa.TATTime,
                                                ExceptionalHandling = aa.isExceptionHandling,
                                                StateID = aa.StateID
                                            } into bb
                                            orderby bb.Key.CreatedDate
                                            select new MISReportingTableSummaryModel
                                            {
                                                datePeriod = (from bbb in bb select bbb.createdDate).FirstOrDefault(),

                                                slaTransactions = (from bbb in bb
                                                                   let TATTime = bbb.TATTime
                                                                   where ((TATTime <= (bbb.slaTime * 3600)) && (TATTime >= 0))
                                                                         && (bbb.isExceptionHandling.Equals(false) || bbb.isExceptionHandling == null)
                                                                   select bbb).Count(),

                                                exceptionalTransactions = (from bbb in bb
                                                                           where (bbb.isExceptionHandling.Equals(true))
                                                                           select bbb).Count(),

                                                breachTransactions = (from bbb in bb
                                                                      let TATTime = bbb.TATTime
                                                                      where ((TATTime > (bbb.slaTime * 3600)) && (TATTime >= 0))
                                                                                && (bbb.isExceptionHandling.Equals(false) || bbb.isExceptionHandling == null)
                                                                      select bbb).Count(),

                                                TATTime = (from bbb in bb where (bbb.StateID == 2) && (bbb.isExceptionHandling.Equals(false) || bbb.isExceptionHandling == null) select bbb.TATTime).FirstOrDefault()

                                            }).ToList();

                    }

                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

}