﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailNotificationWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.Others.EmailNotificationWP.EmailNotificationWPUserControl" %>


<h3 class="header smaller lighter dark">Email Notification</h3>

    <div id="notification-form" class="form-horizontal" role="form">
        <div class="modal-body overflow-visible">
            <div class="form-group" data-bind="with: NotificationModel">
                <label class="col-sm-2 bolder" for="customer-name">Application ID</label>
                <div class="col-sm-6">
                    <div class="clearfix">
                        <input type="text" class="col-sm-7" name="application-name" id="application-name" data-rule-required="true" data-bind="value: Application().ApplicationID" placeholder=" Search..." />
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- widget box start -->
    <div id="widget-box" class="widget-box" data-bind="with: NotificationModel">
        <!-- widget header start -->
        <div class="widget-header widget-hea1der-small header-color-dark">
            <h6>
                <span id="resultfor">Result for  : </span>
                <span data-bind="text: Application().AppID"></span>
            </h6>
        </div>
        <!-- widget header end -->
        <!-- widget body start -->
        <div class="widget-body">
            <!-- widget main start -->
            <div class="widget-main padding-0">
                <!-- widget slim control start -->
                <div class="slim-scroll" data-height="400">
                    <!-- widget content start -->
                    <div class="content">
                        <!-- table responsive start -->
                        <div class="table-responsive">
                            <div class="dataTables_wrapper" role="grid">
                                <table id="customer-table" class="table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>Customer Name</th>
                                            <th>Product</th>
                                            <th>Currency</th>
                                            <th>Trx Amount</th>
                                            <th>Eqv. USD</th>
                                            <th>FX Transaction</th>
                                            <th>Urgency</th>
                                            <th>Transtaction Status</th>
                                            <th>Last Sent By</th>
                                            <th>Last Sent Date/Time</th>
                                        </tr>
                                    </thead>
                                    <!--Iterate through an observableArray using foreach-->
                                    <tbody id="isentries">
                                        <tr>
                                            <td><span data-bind="text: Application().CustomerName"></span></td>
                                            <td><span data-bind="text: Application().Product"></span></td>
                                            <td><span data-bind="text: Application().Currency"></span></td>
                                            <td><span data-bind="text: Application().TransactionAmount"></span></td>
                                            <td><span data-bind="text: Application().EqvUsd"></span></td>
                                            <td><span data-bind="text: Application().FXTransaction"></span></td>
                                            <td><span data-bind="text: Application().IsTopUrgent"></span></td>
                                            <td><span data-bind="text: Application().TranstactionStatus"></span></td>
                                            <td><span data-bind="text: Application().LastSentBy"></span></td>
                                            <td><span data-bind="text: $root.LocalDate(Application().LastSentDate)"></span></td>
                                        </tr>
                                    </tbody>
                                    <tbody id="noentries">
                                        <tr>
                                            <td colspan="12" class="text-center">
                                                No entries available.
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- table responsive end -->
                    </div>
                    <!-- widget content end -->
                </div>
                <!-- widget slim control end -->
            </div>
            <!-- widget main end -->
        </div>
        <!-- widget body end -->
    </div>
    <!-- widget box end -->

    <div class="space-12"></div>
    <!-- ddl matrix -->
    <div class="form-group">
        <label class="col-sm-2 bolder" for="Matrix">
            Matrix Cases
        </label>
        <div class="col-sm-6">
            <div class="clearfix">
                <select id="matrix" name="matrix" data-rule-required="true" data-rule-value="true" data-bind="value: '', options: ddlMatrix, optionsText: 'Name', optionsValue: 'ID', value: selectedChoice, optionsCaption: 'Choose...'" class="col-xs-7"></select>
            </div>
        </div>
        <div style="float:right;">
            <button class="btn btn-sm btn-primary" id="btnSubmit" data-bind="click: ValidationProcess, enable: selectedChoice">
                <i class="icon-save"></i>
                Send Email
            </button>
        </div>
    </div>
    <!-- end of ddl matrix -->

    <script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
    <script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
    <script type="text/javascript" src="/SiteAssets/Scripts/Helper.js"></script>
    <script type="text/javascript" src="/SiteAssets/Scripts/EmailNotification.js"></script>