﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBS.Sharepoint.Entity;

namespace DBS.Sharepoint.Repository
{
    public class UserRoleModel
    {
        public long EmployeeID { get; set; }
        public string EmployeeUsername { get; set; }
        public string RoleName { get; set; }
    }

    public class UserRoleModelCCU
    {
        public long EmployeeID { get; set; }
        public string EmployeeUsername { get; set; }
        public string RoleName { get; set; }
        public long RoleID { get; set; }
    }
    public class UserValidationServices
    {
        public IList<UserRoleModel> GetUserRoleModel(string sUserName)
        {
            try
            {   
                using (DBSSharePointEntities context = new DBSSharePointEntities())
                {
                    var data =
                        (from EM in context.Employees
                         join ER in context.EmployeeRoleMappings on EM.EmployeeID equals ER.EmployeeID
                         join RO in context.Roles on ER.RoleID equals RO.RoleID
                         where
                           EM.EmployeeUsername.Equals(sUserName)

                         select new UserRoleModel
                         {
                             EmployeeID = EM.EmployeeID,
                             EmployeeUsername = EM.EmployeeUsername,
                             RoleName = RO.RoleName
                         }).ToList();
                    if (data.Count > 0)
                    {
                        return data;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public string GetUserRoleModel2(string sUserName)
        {
            try
            {
                using (DBSSharePointEntities context = new DBSSharePointEntities())
                {
                    var data =
                        (from EM in context.Employees
                        join ER in context.EmployeeRoleMappings on EM.EmployeeID equals ER.EmployeeID
                        join RO in context.Roles on ER.RoleID equals RO.RoleID
                        where
                          EM.EmployeeUsername.Equals(sUserName)

                        select new UserRoleModel
                        {
                            EmployeeID = EM.EmployeeID,
                            EmployeeUsername = EM.EmployeeUsername,
                            RoleName = RO.RoleName
                        }).ToList();
                    if (data.Count>0)
                    {
                       //return context.Roles.Select(x=> x.RoleName).FirstOrDefault(); //
                        return data.Select(x => x.RoleName).FirstOrDefault();
                        //return "data ada = " + data[0].RoleName + " - " + sUserName;
                    }
                    else
                    {
                        return "data bener2 kosong";
                    }

                    
                }
            }
            catch (Exception ex)
            {
                return "get user kosong - " + sUserName + " - errorMessage = " + ex.Message;
            }
        }


        public IList<UserRoleModelCCU> GetUserRoleReleaseModel(string sUserName)
        {
            try
            {
                using (DBSSharePointEntities context = new DBSSharePointEntities())
                {
                    var data =
                        (from EM in context.Employees
                         join ER in context.EmployeeRoleMappings on EM.EmployeeID equals ER.EmployeeID
                         join RO in context.Roles on ER.RoleID equals RO.RoleID
                         where
                           EM.EmployeeUsername.Equals(sUserName)

                         select new UserRoleModelCCU
                         {
                             EmployeeID = EM.EmployeeID,
                             EmployeeUsername = EM.EmployeeUsername,
                             RoleName = RO.RoleName,
                             RoleID = RO.RoleID
                         }).ToList();
                    if (data.Count > 0)
                    {
                        return data;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
