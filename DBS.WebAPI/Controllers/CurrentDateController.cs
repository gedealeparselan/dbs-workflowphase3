﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;
using DBS.Entity;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;

namespace DBS.WebAPI.Controllers
{
    public class CurrentDateController : ApiController
    {
        private string message = string.Empty;
        private readonly ICurrentDate repositoryCurrentDate = null;

        public CurrentDateController()
        {
            this.repositoryCurrentDate = new CurrentDateRepository();
        }

        public CurrentDateController(ICurrentDate repositoryCurrentDate)
        {
            this.repositoryCurrentDate = repositoryCurrentDate;
        }

        [ResponseType(typeof(CurrentDateModel))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/CurrentDate")]
        public HttpResponseMessage Get(DateTime clientDateTime)
        {

            IList<CurrentDateModel> Output = new List<CurrentDateModel>();
            if (!repositoryCurrentDate.GetCurrentDate(clientDateTime, ref Output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<CurrentDateModel>>(HttpStatusCode.OK, Output);
            }

        }
    }
}
