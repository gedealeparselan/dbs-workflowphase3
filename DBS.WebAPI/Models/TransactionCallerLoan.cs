﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Transactions;
using DBS.Entity;
using DBS.WebAPI.Helpers;
using System.IO;

namespace DBS.WebAPI.Models
{
    #region property
    public class TransactionCallbackLoanModel
    {
        public long? ApproverID { get; set; }
        public long ID { get; set; }
        /// <summary>
        /// Bank Details
        /// </summary>
        public BankModel Bank { get; set; }

        /// <summary>
        /// Product Details
        /// </summary>
        public ProductModel Product { get; set; }

        /// <summary>
        /// Currency Details
        /// </summary>
        public CurrencyModel Currency { get; set; }

        /// <summary>
        /// Transaction Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Rate
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Eqv.USD
        /// </summary>
        public decimal AmountUSD { get; set; }

        /// <summary>
        /// Channel Details
        /// </summary>
        public ChannelModel Channel { get; set; }

        /// <summary>
        /// Debit Acc Number
        /// </summary>
        public CustomerAccountModel Account { get; set; }

        /// <summary>
        /// Application Date
        /// </summary>
        public DateTime ApplicationDate { get; set; }

        ///// <summary>
        ///// LLD Code
        ///// </summary>
        //public string LLDCode { get; set; }

        ///// <summary>
        ///// LLD Info
        ///// </summary>
        //public string LLDInfo { get; set; }

        /// <summary>
        /// LLD Details
        /// </summary>
        public LLDModel LLD { get; set; }

        /// <summary>
        /// Others information
        /// </summary>
        public string Others { get; set; }

        /// <summary>
        /// Purpose detail of compliance
        /// </summary>
        public string DetailCompliance { get; set; }

        /// <summary>
        /// Payment details information
        /// </summary>
        public string PaymentDetails { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        public string BeneAccNumber { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        public bool? Issyndiciation { get; set; }

        public bool IsSignatureVerified { get; set; }

        public bool IsDormantAccount { get; set; }

        public bool IsFreezeAccount { get; set; }

        /// <summary>
        /// Transaction Is Other Bene Bank
        /// </summary> //add reizvan
        public bool IsOtherBeneBank { get; set; }

        /// <summary>
        /// Transaction Other Bene Bank Name
        /// </summary> //add reizvan
        public string OtherBeneBankName { get; set; }

        /// <summary>
        /// Transaction Other Bene Bank Swift
        /// </summary> //add reizvan
        public string OtherBeneBankSwift { get; set; }

        public bool? IsOtherAccountNumber { get; set; }
        public string OtherAccountNumber { get; set; }

        public CurrencyModel DebitCurrency { get; set; }
    }
    
    public class TransactionCheckerAfterCallbackDetailLoanModel
    {
        public TransactionDetailLoanModel Transaction { get; set; }
        public TransactionCallbackLoanModel Callback { get; set; }
        public IList<TransactionTimelineLoanModel> Timelines { get; set; }
        public TransactionCheckerDataLoanModel Checker { get; set; }
        public IList<VerifyLoanModel> Verify { get; set; }
    }
    public class TransactionCallbackTimeLoanModel
    {
        public long ID { get; set; }
        public long ApproverID { get; set; }
        public CustomerContactModel Contact { get; set; }
        public DateTime Time { get; set; }
        public bool? IsUTC { get; set; }
        public string Remark { get; set; }
        //public DateTime ModifiedDate { get; set; }
        //public string ModifiedBy { get; set; }
    }

    public class TransactionCallbackDetailLoanModel
    {
        public TransactionDetailLoanModel Transaction { get; set; }
        public IList<TransactionTimelineLoanModel> Timelines { get; set; }
        public IList<TransactionCallbackTimeLoanModel> Callbacks { get; set; }
        public IList<VerifyLoanModel> Verify { get; set; }
    }

    #endregion
    #region filter
    #endregion
    #region interface
    public interface IWorflowcallerLoanRepository : IDisposable
    {
        bool GetTransactionCheckerAfterCallbackDetails(Guid workflowInstanceID, long approverID, ref TransactionCheckerAfterCallbackDetailLoanModel output, ref string message);
        bool GetTransactionCallbackDetails(Guid workflowInstanceID, long approverID, ref TransactionCallbackDetailLoanModel output, ref string message);
        bool AddTransactionCallback(Guid workflowInstanceID, long approverID, TransactionCallbackDetailLoanModel data, ref string message);
    }
    #endregion
    #region Repository
    public class WorkflowCallerLoanRepository : IWorflowcallerLoanRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();


        public bool GetTransactionCheckerAfterCallbackDetails(Guid workflowInstanceID, long approverID, ref TransactionCheckerAfterCallbackDetailLoanModel output, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                var transactionID = context.Transactions
                       .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                       .Select(a => a.TransactionID)
                       .SingleOrDefault();

                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new TransactionCheckerAfterCallbackDetailLoanModel
                          {
                              Callback = a.TransactionCallbackDatas
                                .OrderByDescending(x => x.ApproverID)
                                .Select(x => new TransactionCallbackLoanModel()
                                {
                                    ID = x.TransactionCallbackDataID,
                                    Product = new ProductModel()
                                    {
                                        ID = a.Product.ProductID,
                                        Code = a.Product.ProductCode,
                                        Name = a.Product.ProductName,
                                        WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                        Workflow = a.Product.ProductWorkflow.WorkflowName,
                                        LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                        LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                    },
                                    Currency = new CurrencyModel()
                                    {
                                        ID = x.Currency.CurrencyID,
                                        Code = x.Currency.CurrencyCode,
                                        Description = x.Currency.CurrencyDescription,
                                        LastModifiedBy = x.Currency.UpdateDate == null ? x.Currency.CreateBy : x.Currency.UpdateBy,
                                        LastModifiedDate = x.Currency.UpdateDate == null ? x.Currency.CreateDate : x.Currency.UpdateDate.Value
                                    },


                                    Amount = x.Amount,
                                    AmountUSD = x.AmountUSD,
                                    Rate = x.Rate,
                                    OtherBeneBankName = x.OtherBeneBankName,
                                    OtherBeneBankSwift = x.OtherBeneBankSwift,

                                    Channel = new ChannelModel()
                                    {
                                        ID = x.Channel.ChannelID,
                                        Name = x.Channel.ChannelName,
                                        LastModifiedBy = x.Channel.UpdateDate == null ? x.Channel.CreateBy : x.Channel.UpdateBy,
                                        LastModifiedDate = x.Channel.UpdateDate == null ? x.Channel.CreateDate : x.Channel.UpdateDate.Value
                                    },
                                    ApplicationDate = x.ApplicationDate,
                                    BeneAccNumber = x.BeneACCNumber,
                                    IsSignatureVerified = x.IsSignatureVerified,
                                    Issyndiciation = x.IsSyndication,
                                    IsDormantAccount = x.IsDormantAccount,
                                    IsFreezeAccount = x.IsFreezeAccount,
                                    //DetailCompliance = x.DetailCompliance,
                                    PaymentDetails = x.PaymentDetails,
                                    Others = x.Other,
                                    IsOtherAccountNumber = x.IsOtherAccountNumber,
                                    LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                    LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                }).FirstOrDefault(),
                              Checker = a.TransactionCheckerDatas
                                .Where(x => x.TransactionID.Equals(transactionID))
                                .Select(x => new TransactionCheckerDataLoanModel()
                                {
                                    ID = x.TransactionCheckerDataID,
                                    Others = x.Others,
                                    IsSyndication = x.IsSyndication,
                                    IsSignatureVerified = x.IsSignatureVerified,
                                    IsDormantAccount = x.IsDormantAccount,
                                    IsFreezeAccount = x.IsFreezeAccount,
                                    IsLOIAvailable = x.IsLOIAvailable,
                                    IsCallbackRequired = x.IsCallbackRequired,
                                    IsStatementLetterCopy = x.IsStatementLetterCopy
                                }).FirstOrDefault(),
                              Verify = a.TransactionCallbackCheckers
                                  //.Where(x => x.ApproverID.Equals(approverID) && x.IsDeleted == false)
                                .Where(x => x.IsDeleted == false)
                                .Select(x => new VerifyLoanModel()
                                {
                                    ID = x.TransactionColumnID,
                                    Name = x.TransactionColumn.ColumnName,
                                    IsVerified = x.IsVerified
                                }).ToList()
                          }).SingleOrDefault();

                // replace output while Checker & Verify is empty
                if (output != null)
                {

                    long CallBackID = output.Callback.ID;
                    var callbackLLD = context.TransactionCallbackDatas.Where(m => m.TransactionCallbackDataID.Equals(CallBackID)).Select(x => x.LLD).FirstOrDefault();
                    if (callbackLLD != null)
                    {
                        output.Callback.LLD = new LLDModel()
                        {
                            ID = callbackLLD.LLDID,
                            Code = callbackLLD.LLDCode,
                            Description = callbackLLD.LLDDescription
                        };
                    }

                    var callbackBank = context.TransactionCallbackDatas.Where(m => m.TransactionCallbackDataID.Equals(CallBackID)).Select(x => x.Bank).FirstOrDefault();
                    var callbackAccount = context.TransactionCallbackDatas.Where(m => m.TransactionCallbackDataID.Equals(CallBackID)).Select(x => x).FirstOrDefault();
                    if (callbackBank != null)
                    {
                        if (callbackAccount.IsOtherAccountNumber != null && callbackAccount.IsOtherAccountNumber == true)
                        {

                            CustomerAccountModel Account = new CustomerAccountModel();
                            output.Callback.Account = Account;

                            output.Callback.OtherAccountNumber = callbackAccount.OtherAccountNumber;

                            CurrencyModel AccountCurrency = new CurrencyModel();

                            AccountCurrency.ID = callbackAccount.Currency1.CurrencyID;
                            AccountCurrency.Code = callbackAccount.Currency1.CurrencyCode;
                            AccountCurrency.Description = callbackAccount.Currency1.CurrencyDescription;


                            output.Callback.DebitCurrency = AccountCurrency;

                        }
                        else
                        {
                            CustomerAccountModel Account = new CustomerAccountModel();
                            Account.AccountNumber = callbackAccount.CustomerAccount.AccountNumber;
                            Account.LastModifiedBy = callbackAccount.CustomerAccount.UpdateDate == null ? callbackAccount.CustomerAccount.CreateBy : callbackAccount.CustomerAccount.UpdateBy;
                            Account.LastModifiedDate = callbackAccount.CustomerAccount.UpdateDate == null ? callbackAccount.CustomerAccount.CreateDate : callbackAccount.CustomerAccount.UpdateDate.Value;

                            output.Callback.Account = Account;

                            CurrencyModel AccountCurrency = new CurrencyModel();
                            AccountCurrency.ID = callbackAccount.Currency1.CurrencyID;
                            AccountCurrency.Code = callbackAccount.Currency1.CurrencyCode;
                            AccountCurrency.Description = callbackAccount.Currency1.CurrencyDescription;

                            output.Callback.DebitCurrency = AccountCurrency;
                        }

                        output.Callback.Bank = new BankModel()
                        {
                            ID = callbackBank.BankID,
                            Code = callbackBank.BankCode,
                            BankAccount = callbackBank.BankAccount,
                            BranchCode = callbackBank.BranchCode,
                            CommonName = callbackBank.CommonName,
                            Currency = callbackBank.Currency,
                            PGSL = callbackBank.Currency,
                            SwiftCode = callbackBank.SwiftCode,
                            Description = callbackBank.BankDescription,
                            LastModifiedBy = callbackBank.UpdateDate == null ? callbackBank.CreateBy : callbackBank.UpdateBy,
                            LastModifiedDate = callbackBank.UpdateDate == null ? callbackBank.CreateDate : callbackBank.UpdateDate.Value
                        };

                    }

                    // replace checker data while empty with data from caller
                    if (output.Checker == null)
                    {


                        output.Checker = new TransactionCheckerDataLoanModel()
                        {
                            ID = 0,
                            //LLDCode = output.Callback.LLDCode,
                            //LLDInfo = output.Callback.LLDInfo,
                            // newlld
                            Others = output.Callback.Others,
                            IsSyndication = output.Callback.Issyndiciation,
                            IsSignatureVerified = output.Callback.IsSignatureVerified,
                            IsDormantAccount = output.Callback.IsDormantAccount,
                            IsFreezeAccount = output.Callback.IsFreezeAccount,
                            //IsLOIAvailable = output.Checker.IsLOIAvailable,
                            //IsCallbackRequired = output.Checker.IsCallbackRequired,
                            //IsStatementLetterCopy = output.Checker.IsStatementLetterCopy
                        };
                    }

                    // replace verify list while empty
                    if (!output.Verify.Any())
                    {
                        output.Verify = context.TransactionColumns
                            .Where(x => x.IsPPUCheckerAfterCallback.Equals(true) && x.IsDeleted.Equals(false))
                            .Select(x => new VerifyLoanModel()
                            {
                                ID = x.TransactionColumnID,
                                Name = x.ColumnName,
                                IsVerified = false
                            })
                            .ToList();
                    }

                    var documentCompleteness = (from a in context.TransactionCheckers
                                                join b in context.TransactionColumns
                                                on a.TransactionColumnID equals b.TransactionColumnID
                                                where a.TransactionID.Equals(transactionID) && b.ColumnName.Equals("Document Completeness") && a.IsDeleted == false
                                                select new VerifyLoanModel()
                                                {
                                                    ID = a.TransactionColumnID,
                                                    Name = b.ColumnName,
                                                    IsVerified = a.IsVerified
                                                }).SingleOrDefault();

                    if (documentCompleteness != null)
                    {
                        output.Verify.Add(documentCompleteness);
                    }
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionCallbackDetails(Guid workflowInstanceID, long approverID, ref TransactionCallbackDetailLoanModel output, ref string message)
        {
            bool IsSuccess = false; //is

            try
            {
                var transactionID = context.Transactions
                       .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                       .Select(a => a.TransactionID)
                       .SingleOrDefault();

                var callbackID = context.TransactionCallbacks
                    .Where(a => a.IsUTC == false && a.TransactionID.Equals(transactionID))
                    .OrderByDescending(a => a.TransactionCallbackID)
                    .Select(a => a.TransactionCallbackID)
                    .FirstOrDefault();

                //where a.IsUTC.Equals(false) && a.TransactionID.Equals(transactionID)
                //select a.TransactionCallbackID).so;

                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new TransactionCallbackDetailLoanModel
                          {

                              /*Callbacks = a.TransactionCallbacks
                                  //.Where(x => x.ApproverID.Equals(approverID))
                                  .Select(x => new TransactionCallbackTimeModel()
                                  {
                                      ID = x.TransactionCallbackID,
                                      ApproverID = x.ApproverID,
                                      Contact = x.TransactionCallbackContacts.Select(y => new CustomerContactModel()
                                      {
                                          ID = y.CustomerContact.ContactID,
                                          Name = y.CustomerContact.ContactName,
                                          PhoneNumber = y.CustomerContact.PhoneNumber,
                                          DateOfBirth = y.CustomerContact.DateOfBirth,
                                          Address = y.CustomerContact.Address,
                                          IDNumber = y.CustomerContact.IdentificationNumber,
                                      }).FirstOrDefault(),
                                      Time = x.Time,
                                      IsUTC = x.IsUTC,
                                      Remark = x.Remark
                                  }).ToList()*/
                              Callbacks = (from b in context.TransactionCallbacks
                                           //let isCallbackId = callbackID != null ? true : false
                                           where b.TransactionID.Equals(transactionID)
                                           //&& (isCallbackId ? b.TransactionCallbackID > callbackID : true)
                                           select new TransactionCallbackTimeLoanModel
                                           {
                                               ID = b.TransactionCallbackID,
                                               ApproverID = b.ApproverID,
                                               Contact = b.TransactionCallbackContacts.Select(y => new CustomerContactModel()
                                               {
                                                   ID = y.CustomerContact.ContactID,
                                                   Name = y.CustomerContact.ContactName,
                                                   PhoneNumber = y.CustomerContact.PhoneNumber,
                                                   DateOfBirth = y.CustomerContact.DateOfBirth,
                                                   //Address = y.CustomerContact.Address,
                                                   IDNumber = y.CustomerContact.IdentificationNumber,
                                               }).FirstOrDefault(),
                                               Time = b.Time,
                                               IsUTC = b.IsUTC,
                                               Remark = b.Remark
                                           }).ToList(),
                              Verify = a.TransactionCallbackCheckers
                                  //.Where(z => z.ApproverID.Equals(approverID))
                                .Select(z => new VerifyLoanModel()
                                {
                                    ID = z.TransactionColumnID,
                                    Name = z.TransactionColumn.ColumnName,
                                    IsVerified = z.IsVerified
                                }).ToList()
                          }).SingleOrDefault();

                // replace verify list while empty
                if (output != null)
                {
                    /*if (!output.Callbacks.Any())
                    {
                        var utcAttempts = context.Configs.Where(a => a.Parameter.Equals("UTC_ATTEMPS") & a.IsDelete.Equals(false))
                            .Select(a => a.Value).SingleOrDefault();

                        for (int i = 0; i < Convert.ToInt32(utcAttempts); i++)
                        {
                            output.Callbacks.Add(new TransactionCallbackTimeModel());
                        }
                    }*/


                    output.Verify = (from a in context.TransactionColumns
                                     where a.IsDeleted.Equals(false) && a.IsCallBack.Equals(true)
                                     select new VerifyLoanModel()
                                     {
                                         ID = a.TransactionColumnID,
                                         Name = a.ColumnName,
                                         IsVerified = false
                                     }).ToList();


                    var documentCompleteness = (from a in context.TransactionCheckers
                                                join b in context.TransactionColumns
                                                on a.TransactionColumnID equals b.TransactionColumnID
                                                where a.TransactionID.Equals(transactionID) && b.ColumnName.Equals("Document Completeness") && a.IsDeleted == false
                                                select new VerifyLoanModel()
                                                {
                                                    ID = a.TransactionColumnID,
                                                    Name = b.ColumnName,
                                                    IsVerified = a.IsVerified
                                                }).SingleOrDefault();

                    if (documentCompleteness != null)
                    {
                        output.Verify.Add(documentCompleteness);
                    }
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool AddTransactionCallback(Guid workflowInstanceID, long approverID, TransactionCallbackDetailLoanModel data, ref string message)
        {
            bool IsSuccess = false;

            //feedbackcaller

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    int bankID = context.Banks.Where(x => x.BankCode.Equals(data.Transaction.Bank.Code)).Select(y => y.BankID).FirstOrDefault();
                    //context.TransactionCallbackDatas.Add(new TransactionCallbackData()

                    // insert trasaction callback data
                    DBS.Entity.TransactionCallbackData dataCallback = new DBS.Entity.TransactionCallbackData()
                    {
                        TransactionID = data.Transaction.ID,

                        //AccountNumber = data.Transaction.Account.AccountNumber,
                       
                        Amount = data.Transaction.Amount,
                        AmountUSD = data.Transaction.AmountUSD,
                        Rate = data.Transaction.Rate,
                        ApplicationDate = data.Transaction.ApplicationDate,
                        ApproverID = approverID,
                        ChannelID = data.Transaction.Channel.ID,
                        CurrencyID = data.Transaction.Currency.ID,
                        DebitCurrencyID = data.Transaction.DebitCurrency.ID,
                        //LLDCode = data.Transaction.LLDCode,
                        //LLDInfo = data.Transaction.LLDInfo,
                        //LLDID = data.Transaction.LLD.ID, // newlld
                        //DetailCompliance = data.Transaction.DetailCompliance,
                        PaymentDetails = data.Transaction.PaymentDetails,
                        Other = data.Transaction.Others,
                        IsDormantAccount = data.Transaction.IsDormantAccount,
                        IsFreezeAccount = data.Transaction.IsFreezeAccount,
                        IsSyndication = data.Transaction.IsSyndication,
                        IsSignatureVerified = data.Transaction.IsSignatureVerified,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    };

                    if (data.Transaction.IsOtherAccountNumber == true)
                    {
                        dataCallback.AccountNumber = null;
                        dataCallback.OtherAccountNumber = data.Transaction.OtherAccountNumber;
                        dataCallback.IsOtherAccountNumber = true;
                    }
                    else
                    {
                        dataCallback.OtherAccountNumber = null;
                        dataCallback.AccountNumber = data.Transaction.Account.AccountNumber;
                        dataCallback.IsOtherAccountNumber = false;
                    }

                    if (data.Transaction.LLD != null)
                    {
                        dataCallback.LLDID = data.Transaction.LLD.ID; // newlld
                    }

                    dataCallback.BeneACCNumber = data.Transaction.BeneAccNumber; //add chandra

                    // if bene bank is not from data bank
                    if (data.Transaction.IsOtherBeneBank)
                    {
                        dataCallback.BankID = bankID;
                        dataCallback.OtherBeneBankName = data.Transaction.Bank.Description;
                        dataCallback.OtherBeneBankSwift = data.Transaction.Bank.SwiftCode;
                    }
                    else
                    {
                        dataCallback.BankID = data.Transaction.Bank.ID;
                        dataCallback.OtherBeneBankName = null;
                        dataCallback.OtherBeneBankSwift = null;
                    }

                    context.TransactionCallbackDatas.Add(dataCallback);
                    context.SaveChanges();

                    // insert callback
                    foreach (var item in data.Callbacks)
                    {
                        if (item.Time.Hour > 0)
                        {
                            // Insert transaction callback
                            TransactionCallback callback = new TransactionCallback()
                            {
                                ApproverID = approverID,
                                TransactionID = data.Transaction.ID,
                                Time = item.Time.ToUniversalTime(),
                                IsUTC = item.IsUTC,
                                Remark = item.Remark,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().DisplayName
                            };

                            context.TransactionCallbacks.Add(callback);
                            context.SaveChanges();

                            // Insert transaction callback contact
                            if (item.Contact != null)
                            {
                                context.TransactionCallbackContacts.Add(new TransactionCallbackContact()
                                {
                                    TransactionCallbackID = callback.TransactionCallbackID,
                                    ContactID = item.Contact.ID,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });
                            }
                            context.SaveChanges();

                            callback = null;
                        }
                    }

                    //update payment detail from transaction
                    //Entity.Transaction dataPPU = context.Transactions.Where(a => a.TransactionID.Equals(data.Transaction.ID)).SingleOrDefault();
                    //if (dataPPU != null)
                    //{
                    //    dataPPU.PaymentDetails = data.Transaction.DetailCompliance;
                    //    dataPPU.DetailCompliance = data.Transaction.DetailCompliance;
                    //    context.SaveChanges();
                    //}
                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.Collect();
            GC.SuppressFinalize(this);
        }
 




    #endregion


    }
}