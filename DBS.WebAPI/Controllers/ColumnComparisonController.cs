﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    public class ColumnComparisonController : ApiController
    {
        private string message = string.Empty;

        private readonly IColumnComparisonModel repository = null;

        public ColumnComparisonController()
        {
            this.repository = new ColumnComparisonRepository();
        }

        public ColumnComparisonController(IColumnComparisonModel repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all City.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<ColumnComparisonModel>))]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            IList<ColumnComparisonModel> output = new List<ColumnComparisonModel>();

            if (!repository.GetColumnComparison(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ColumnComparisonModel>>(HttpStatusCode.OK, output);
            }
        }      


        /// <summary>
        /// Get ColumnComparison for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(ColumnComparisonModelGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/columncomparison")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<ColumnComparisonModelFilter> filters)
        {
            ColumnComparisonModelGrid output = new ColumnComparisonModelGrid();

            if (!repository.GetColumnComparison(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Group Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<ColumnComparisonModelGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get City by ID.
        /// </summary>
        /// <param name="id">ProductMappingID</param>
        /// <returns></returns>
        [ResponseType(typeof(ColumnComparisonModel))]
        [Route("api/columncomparison/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            ColumnComparisonModel output = new ColumnComparisonModel();

            if (!repository.GetColumnComparisonByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<ColumnComparisonModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new city
        //Add new
        /// </summary>
        /// <param name="ProductTypeCode">Product Type Code</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/columncomparison")]
        public HttpResponseMessage Post(ColumnComparisonModel colum)
        {

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddColumnComparison(colum, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Colum Comparison has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing product type threshold mapping.
        /// </summary>
        /// <param name="id">ID of product type mapping to be modify.</param>
        /// <param name="threshold">Product type mapping data to be updated.</param>
        /// <returns></returns>
        /// Update Data ThresholdGroup
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/columncomparison/{ID}")]
        public HttpResponseMessage Update(int id, ref ColumnComparisonModel colum)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateColumnComparison(id, ref colum, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "colum comparison data has been updated." });
                }
            }
        }


        //Delete Data
        [HttpDelete]
        [Route("api/columncomparison/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteColumnComparison(id, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "Column  Comparison data has been deleted.");
                }
            }
        }

        //string tablename
        [ResponseType(typeof(IList<ColumnComparisonModel>))]
        [HttpGet]
        [Route("api/columncomparison/GetColumnByTableName")]
        public HttpResponseMessage GetColumnByTableName(string tableName)
        {
            IList<ColumnComparisonModel> output = new List<ColumnComparisonModel>();

            if (!repository.GetColumnComparisonByTableName(ref output, tableName, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ColumnComparisonModel>>(HttpStatusCode.OK, output);
            }
        }
    }
}
