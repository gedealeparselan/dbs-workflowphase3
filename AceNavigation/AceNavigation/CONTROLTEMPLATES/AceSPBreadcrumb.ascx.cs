﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Navigation;
using Microsoft.SharePoint.Publishing.Navigation;
using Microsoft.SharePoint.Taxonomy;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AceNavigation.Repository;

namespace AceNavigation.CONTROLTEMPLATES
{
    public partial class AceSPBreadcrumb : UserControl
    {
        private bool IsSuccess { get; set; }
        private string ErrorMessage { get; set; }

        private class Breadcrumb
        {
            public int ID { get; set; }
            public string Title { get; set; }
            public string Url { get; set; }
        }

        private IList<Breadcrumb> Model = new List<Breadcrumb>();

        #region Properties
        public string RootName { get; set; }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            using (SPSite site = new SPSite(SPContext.Current.Site.Url))
            {
                ReadTaxonomy(site);
            }
        }

        private void ReadTaxonomy(SPSite site)
        {
            TaxonomySession taxonomy;
            TaxonomyNavigationContext context;

            try
            {
                taxonomy = new TaxonomySession(site);
                context = TaxonomyNavigationContext.Current;

                if (context.NavigationTerm != null)
                {
                    int i = 0;
                    Model.Add(new Breadcrumb()
                    {
                        ID = i,
                        Title = context.FriendlyUrlTerm.Title.Value,
                        Url = context.NavigationTerm.GetWebRelativeFriendlyUrl()
                    });

                    // get parent
                    GetParent(context.FriendlyUrlTerm, i);
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                ErrorMessage = ex.Message + " - " + ex.InnerException;
            }
            finally
            {
                taxonomy = null;
                context = null;
            }
        }

        private void GetParent(NavigationTerm term, int index)
        {
            NavigationTerm parentTerm = term.Parent;
            if (parentTerm != null)
            {
                Model.Add(new Breadcrumb()
                {
                    ID = index + 1,
                    Title = parentTerm.Title.Value,
                    Url = parentTerm.GetWebRelativeFriendlyUrl()
                });

                // get parent
                NavigationTerm curParentTerm = parentTerm.Parent;
                if (curParentTerm != null)
                {
                    Model.Add(new Breadcrumb()
                    {
                        ID = index + 2,
                        Title = curParentTerm.Title.Value,
                        Url = curParentTerm.GetWebRelativeFriendlyUrl()
                    });

                    GetParent(curParentTerm, index + 2);
                }
                curParentTerm = null;
            }
            parentTerm = null;
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (IsSuccess)
            {
                writer.Write("<ul class='breadcrumb'>");
                writer.Write("<li>");
                writer.Write("<i class='icon-home home-icon'></i>");
                writer.Write("<a href='/'>" + RootName + "</a>");
                writer.Write("</li>");

                if (Model.Any())
                {
                    var data = from a in Model orderby a.ID descending select a;
                    foreach (var item in data)
                    {
                        if (item.Title.Equals(RootName)) return;

                        writer.Write("<li " + (item.ID == 0 ? "class='active'" : "") + ">");
                        writer.Write("<a href='" + (item.ID == 0 ? "#" : item.Url) + "'>" + item.Title + "</a>");
                        writer.Write("</li>");
                    }
                }

                writer.Write("</ul>");
            }
            else
            {
                writer.Write(ErrorMessage);
            }
        }
    }
}
