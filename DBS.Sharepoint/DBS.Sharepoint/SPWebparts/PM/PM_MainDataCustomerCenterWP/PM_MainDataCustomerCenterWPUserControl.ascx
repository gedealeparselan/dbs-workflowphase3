﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PM_MainDataCustomerCenterWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.PM.PM_MainDataCustomerCenterWP.PM_MainDataCustomerCenterWPUserControl" %>

<h1 class="header smaller lighter dark">Main Data Customer Center</h1>
	
<div class="row" data-bind="ifnot: $data == null">

    <div class="col-xs-12">
        <!-- modal body form start -->
        <div id="customer-form" class="form-horizontal" role="form">
            <div class="space-4"></div>           
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="times">
                    Information Type
                </label>
                <div class="col-sm-9">
                    <div class="clearfix">
                        <select id="MainDataCustomerCenter" name="MainDataCustomerCenter" class="valid" data-bind="value: InformationTypeModel().CBOMaintainSubID, options: ddlInformationType, optionsText: 'SubTypeName', optionsValue: 'CBOMaintainSubID', optionsCaption: 'Please Select...', disable: $root.Readonly, visible: $root.Readonly() == false"></select>
                        <label class="col-xs-1 col-sm-1" data-bind="visible: $root.Readonly() == true">:</label><label class="col-xs-11" data-bind="    text: InformationTypeModel().SubTypeName, visible: $root.Readonly() == true"></label>
                    </div>                  
                </div>
            </div>

        </div>
        <!-- modal body form end -->
    </div>
</div>

<!-- modal form end -->
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>