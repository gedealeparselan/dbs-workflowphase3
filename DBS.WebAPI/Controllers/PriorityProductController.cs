﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    public class PriorityProductController : ApiController
    {
        private string message = string.Empty;

        private readonly IPriorityProductModelInterface repository = null;

        public PriorityProductController()
        {
            this.repository = new PriorityProductModelRepository();
        }

        public PriorityProductController(IPriorityProductModelInterface repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all priority.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<PriorityProductModel>))]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            IList<PriorityProductModel> output = new List<PriorityProductModel>();

            if (!repository.GetPriority(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<PriorityProductModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get priority with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<PriorityProductModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<PriorityProductModel> output = new List<PriorityProductModel>();

            if (!repository.GetPriority(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<PriorityProductModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get priority for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(PriorityProductModelGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/PriorityProduct")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<PriorityProductModelFilter> filters)
        {
            PriorityProductModelGrid output = new PriorityProductModelGrid();

            if (!repository.GetPriority(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Type Location" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<PriorityProductModelGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Priority by ID.
        /// </summary>
        /// <param name="id">Priority ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(PriorityProductModel))]
        [Route("api/priorityproduct/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            PriorityProductModel output = new PriorityProductModel();

            if (!repository.GetPriorityByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<PriorityProductModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new Priority
        /// </summary>
        /// <param name="Location">Type Location</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/PriorityProduct")]
        public HttpResponseMessage Post(PriorityProductModel Priority)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddPriorityProduct(Priority, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Priority product has been created." });
                }
            }
        }


        /// <summary>
        /// Modify existing priority.
        /// </summary>
        /// <param name="id">ID of Priority to be modify.</param>
        /// <param name="priority">Priority data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/priorityproduct/{id}")]
        public HttpResponseMessage Put(int id, PriorityProductModel Priority)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdatePriorityProduct(id, Priority, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Priority data has been updated." });
                }
                }
            }

        /// <summary>
        /// Remove exisiting Biz Segment.
        /// </summary>
        /// <param name="id">ID of Biz Segment to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/priorityproduct/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeletePriorityProduct(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Priority data has been deleted." });
                }
            }
        }

        }
    }

