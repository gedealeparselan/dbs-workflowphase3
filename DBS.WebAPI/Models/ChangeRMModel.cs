﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Validation;
using System.Transactions;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity.Core.Objects;
using System.Text;
using DBS.Entity;
using DBS.WebAPI.Helpers;

namespace DBS.WebAPI.Models
{
    #region property
    public class TransactionChangeRM
    {
        //public IList<ChangeRMTransactionModel> ChangeRM { get; set; }
        public IList<ChangeRM> ChangeRM { get; set; }
        public TransactionCBOContactDetailModel TransactionModel { get; set; }
    }
    public class ChangeRMTransactionModel
    {
        public long TransactionID { get; set; }
        public string CIF { get; set; }
        public string ChangeSegmentFrom { get; set; }
        public string ChangeSegmentTo { get; set; }
        public string ChangeFromBranchCode { get; set; }
        public string ChangeFromRMCode { get; set; }
        public string ChangeFromBU { get; set; }
        public string ChangeToBranchCode { get; set; }
        public string ChangeToRMCode { get; set; }
        public string ChangeToBU { get; set; }
        public string Account { get; set; }
        public string PCCode { get; set; }
        public string EATPB { get; set; }
        public string Remarks { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public bool Selected { get; set; }
        public TransactionCBODetailModel TransactionModel { get; set; }
    }
    public class ChangeRM
    {
        public string NO { get; set; }
        public string CIF { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string CHANGE_SEGMENT_FROM { get; set; }
        public string CHANGE_SEGMENT_TO { get; set; }
        public string CHANGE_FROM_BRANCH_CODE { get; set; }
        public string CHANGE_FROM_RM_CODE { get; set; }
        public string CHANGE_FROM_BU { get; set; }
        public string CHANGE_TO_BRANCH_CODE { get; set; }
        public string CHANGE_TO_RM_CODE { get; set; }
        public string CHANGE_TO_BU { get; set; }
        public string ACCOUNT { get; set; }
        public string PC_CODE { get; set; }
        public string FREE_TEXT_5__EA_TPB_EA_TPB_ { get; set; }
        public string REMARKS_REASON { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public bool Selected { get; set; }

    }
    #endregion

    #region Interface
    public interface IChangeRMTask : IDisposable
    {
        //bool GetRM(ref IList<ChangeRMTransactionModel> ChangeRM, DateTime _date, ref string message);
        //bool AddTransactionDraft(IList<ChangeRMTransactionModel> transaction);
        bool GetRM(ref IList<ChangeRM> ChangeRM, DateTime _date, ref string message);
        bool AddTransaction(TransactionChangeRM transaction, ref long transactionid, ref string aplicationid, ref string message);
        bool AddTransactionDraft(TransactionChangeRM transaction);
        bool addChangeRM(ref IList<ChangeRMTransactionModel> ChangeRM, ref string message);
    }
    #endregion
    public class ChangeRMModelModal : IChangeRMTask
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        //public bool GetRM(ref IList<ChangeRMTransactionModel> ChangeRM, DateTime _date, ref string message)
        //{
        //    bool IsSuccess = false;
        //    try
        //    {
        //        context.ChangeRMTransactions.AsNoTracking();
        //        ChangeRM = (from a in context.ChangeRMTransactions
        //                    join b in context.Transactions on a.TransactionID equals b.TransactionID
        //                    select new ChangeRMTransactionModel
        //                    {
        //                        Selected = false,
        //                        CIF = a.CIF,
        //                        ChangeSegmentFrom = a.ChangeSegmentFrom,
        //                        ChangeSegmentTo = a.ChangeSegmentTo,
        //                        ChangeFromBranchCode = a.ChangeFromBranchCode,
        //                        ChangeFromRMCode = a.ChangeFromRMCode,
        //                        ChangeFromBU = a.ChangeFromBU,
        //                        ChangeToBranchCode = a.ChangeToBranchCode,
        //                        ChangeToRMCode = a.ChangeToRMCode,
        //                        ChangeToBU = a.ChangeToBU,
        //                        Account = a.Account,
        //                        PCCode = a.PCCode,
        //                        EATPB = a.EATPB,
        //                        Remarks = a.Remarks,
        //                        TransactionModel = new TransactionCBODetailModel()
        //                        {
        //                            ID = b.TransactionID,
        //                            WorkflowInstanceID = b.WorkflowInstanceID.Value,
        //                            ApplicationID = b.ApplicationID,
        //                            ApplicationDate = b.ApplicationDate,
        //                            ApproverID = b.ApproverID != null ? b.ApproverID : 0,
        //                            Product = new ProductModel()
        //                            {
        //                                ID = b.Product.ProductID,
        //                                Code = b.Product.ProductCode,
        //                                Name = b.Product.ProductName,
        //                                WorkflowID = b.Product.ProductWorkflow.WorkflowID,
        //                                Workflow = b.Product.ProductWorkflow.WorkflowName,
        //                                LastModifiedBy = b.Product.UpdateDate == null ? b.Product.CreateBy : b.Product.UpdateBy,
        //                                LastModifiedDate = b.Product.UpdateDate == null ? b.Product.CreateDate : b.Product.UpdateDate.Value
        //                            },
        //                            Customer = new CustomerModel()
        //                            {
        //                                CIF = b.Customer.CIF,
        //                                //CustomerTypeID = b.Customer.CustomerTypeID,
        //                                //CustomerCategoryID=b.Customer.
        //                                //LastModifiedDate = b.Customer.UpdateDate == null ? b.Customer.CreateDate : b.Customer.UpdateDate.Value
        //                            },
        //                            Currency = new CurrencyModel()
        //                            {
        //                                ID = b.Currency.CurrencyID,
        //                                Code = b.Currency.CurrencyCode,
        //                                Description = b.Currency.CurrencyDescription,
        //                                LastModifiedBy = b.Currency.UpdateDate == null ? b.Currency.CreateBy : b.Currency.UpdateBy,
        //                                LastModifiedDate = b.Currency.UpdateDate == null ? b.Currency.CreateDate : b.Currency.UpdateDate.Value
        //                            },
        //                            Amount = b.Amount,
        //                            AmountUSD = b.AmountUSD,
        //                            Rate = b.Rate,
        //                            Channel = new ChannelModel()
        //                            {
        //                                ID = b.Channel.ChannelID,
        //                                Name = b.Channel.ChannelName,
        //                                LastModifiedBy = b.Channel.UpdateDate == null ? b.Channel.CreateBy : b.Channel.UpdateBy,
        //                                LastModifiedDate = b.Channel.UpdateDate == null ? b.Channel.CreateDate : b.Channel.UpdateDate.Value
        //                            },

        //                            BizSegment = new BizSegmentModel()
        //                            {
        //                                ID = b.BizSegment.BizSegmentID,
        //                                Name = b.BizSegment.BizSegmentName,
        //                                Description = b.BizSegment.BizSegmentDescription,
        //                                LastModifiedBy = b.BizSegment.UpdateDate == null ? b.BizSegment.CreateBy : b.BizSegment.UpdateBy,
        //                                LastModifiedDate = b.BizSegment.UpdateDate == null ? b.BizSegment.CreateDate : b.BizSegment.UpdateDate.Value
        //                            },
        //                            BeneName = b.BeneName,
        //                            BeneAccNumber = b.BeneAccNumber,

        //                            Bank = new BankModel()
        //                            {
        //                                ID = b.Bank.BankID,
        //                                Code = b.Bank.BankCode,
        //                                BankAccount = b.Bank.BankAccount,
        //                                BranchCode = b.Bank.BranchCode,
        //                                CommonName = b.Bank.CommonName,
        //                                Currency = b.Bank.Currency,
        //                                PGSL = b.Bank.Currency,
        //                                SwiftCode = b.Bank.SwiftCode,
        //                                Description = b.Bank.BankDescription,
        //                                LastModifiedBy = b.Bank.UpdateDate == null ? b.Bank.CreateBy : b.Bank.UpdateBy,
        //                                LastModifiedDate = b.Bank.UpdateDate == null ? b.Bank.CreateDate : b.Bank.UpdateDate.Value
        //                            },
        //                            TransactionType = new TransactionTypeModel()
        //                            {
        //                                ID = b.TransactionType.TransTypeID,
        //                                Name = b.TransactionType.Product.ProductName
        //                            },
        //                            MaintenanceType = new MaintenanceTypeModel()
        //                            {
        //                                ID = b.MaintenanceType.CBOMaintainID,
        //                                Name = b.MaintenanceType.MaintenanceTypeName
        //                            },

        //                            IsTopUrgent = b.IsTopUrgent,
        //                            IsSignatureVerified = b.IsSignatureVerified,
        //                            IsFreezeAccount = b.IsFrezeAccount,
        //                            IsDormantAccount = b.IsDormantAccount,
        //                            IsDraft = b.IsDraft,
        //                            IsNewCustomer = b.IsNewCustomer,
        //                            IsOtherAccountNumber = b.IsOtherAccountNumber,
        //                            IsDocumentComplete = b.IsDocumentComplete,
        //                            //DetailCompliance = a.DetailCompliance,                                                              
        //                            Others = b.Others,
        //                            TZNumber = b.TZNumber,
        //                        }

        //                    }).ToList();

        //        IsSuccess = true;
        //    }

        //    catch (Exception ex)
        //    {
        //        message = ex.InnerException.Message;
        //    }
        //    return IsSuccess;
        //}
        public bool GetRM(ref IList<ChangeRM> ChangeRM, DateTime _date, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                context.ChangeRMs.AsNoTracking();
                ChangeRM = (from a in context.ChangeRMs
                            select new ChangeRM
                            {
                                Selected = false,
                                NO = a.NO,
                                CIF = a.CIF,
                                CUSTOMER_NAME = a.CUSTOMER_NAME,
                                CHANGE_SEGMENT_FROM = a.CHANGE_SEGMENT_FROM,
                                CHANGE_SEGMENT_TO = a.CHANGE_SEGMENT_TO,
                                CHANGE_FROM_BRANCH_CODE = a.CHANGE_FROM_BRANCH_CODE,
                                CHANGE_FROM_RM_CODE = a.CHANGE_FROM_RM_CODE,
                                CHANGE_FROM_BU = a.CHANGE_FROM_BU,
                                CHANGE_TO_BRANCH_CODE = a.CHANGE_TO_BRANCH_CODE,
                                CHANGE_TO_RM_CODE = a.CHANGE_TO_RM_CODE,
                                CHANGE_TO_BU = a.CHANGE_TO_BU,
                                ACCOUNT = a.ACCOUNT__,
                                PC_CODE = a.PC_CODE,
                                FREE_TEXT_5__EA_TPB_EA_TPB_ = a.FREE_TEXT_5__EA_TPB_EA_TPB_,
                                REMARKS_REASON=a.REMARKS___REASON,
                                CreateDate=a.CreateDate,
                                CreateBy=a.CreateBy

                            }).ToList();

                IsSuccess = true;
            }

            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return IsSuccess;
        }
        public bool AddTransaction(TransactionChangeRM transaction, ref long transactionid, ref string aplicationid, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.Add(new Entity.Transaction
                {
                    IsDraft =false, //transaction.TransactionModel.Transaction.IsDraft,
                    TZNumber = "TZ333",
                    //TransactionID = item.TransactionModel.ID,
                    //ApplicationID = transaction.TransactionModel.Transaction.ApplicationID,
                    IsTopUrgent = 1,//(item.TransactionModel.IsTopUrgentChain == 1 ? 2 : item.TransactionModel.IsTopUrgent),
                    Amount =0, //transaction.TransactionModel.Transaction.Amount,
                    Rate =0, //transaction.TransactionModel.Transaction.Rate,
                    AmountUSD =0, //transaction.TransactionModel.Transaction.AmountUSD,
                    BeneName ="DBS", //transaction.TransactionModel.Transaction.BeneName,
                    BeneAccNumber ="222", //transaction.TransactionModel.Transaction.BeneAccNumber,
                    //IsOtherAccountNumber = transaction.TransactionModel.Transaction.IsOtherAccountNumber,
                    IsNewCustomer =false, //transaction.TransactionModel.Transaction.IsNewCustomer,
                    StateID = 1,
                    CreateDate = DateTime.UtcNow,
                    CreateBy = currentUser.GetCurrentUser().DisplayName,
                    Type = transaction.TransactionModel.Transaction.Type.HasValue ? transaction.TransactionModel.Transaction.Type : null,
                    IsResident = true,//transaction.TransactionModel.Transaction.IsResident,
                    IsCitizen = true,//transaction.TransactionModel.Transaction.IsResident,
                    PaymentDetails = transaction.TransactionModel.Transaction.PaymentDetails,
                    CIF = transaction.ChangeRM[0].CIF,//transaction.TransactionModel.Transaction.Customer.CIF,
                    ProductID = 19,//transaction.TransactionModel.Transaction.Product.ID,
                    CurrencyID =5, //transaction.TransactionModel.Transaction.Currency.ID,
                    ChannelID =1, //transaction.TransactionModel.Transaction.Channel.ID,
                    DebitCurrencyID = 13,//item.TransactionModel.DebitCurrency.ID,
                    ProductTypeID = 5,
                    BankChargesID = 2,
                    UnderlyingDocID = 19,
                    TransactionTypeID = 6,
                    CBOMaintainID = 1006,
                    //bangkit
                    BankID =1167, //transaction.TransactionModel.Transaction.Bank.ID,
                    ApplicationDate = DateTime.UtcNow,//transaction.TransactionModel.Transaction.ApplicationDate,
                    BizSegmentID =3, //transaction.TransactionModel.Transaction.BizSegment.ID,
                    //LLDCode = item.TransactionModel.LLDCode,
                    //LLDInfo = transaction.LLDInfo,
                    IsChangeRM = transaction.TransactionModel.Transaction.IsChangeRM,
                    IsSegment = transaction.TransactionModel.Transaction.IsSegment,
                    IsSolID = transaction.TransactionModel.Transaction.IsSolID,
                    IsSignatureVerified = false,//transaction.TransactionModel.Transaction.IsSignatureVerified,
                    IsDormantAccount = false,//transaction.TransactionModel.Transaction.IsDormantAccount,
                    IsFrezeAccount = false,//transaction.TransactionModel.Transaction.IsFreezeAccount,
                    IsDocumentComplete = false,//transaction.TransactionModel.Transaction.IsDocumentComplete,
                    Others = "Test"
                });
                context.SaveChanges();
                IsSuccess = true;

                var TransactionID = context.Transactions.OrderByDescending(x => x.TransactionID).First();
                //return value
                transactionid = TransactionID.TransactionID;
                aplicationid = TransactionID.ApplicationID;
                if (aplicationid == null)
                {
                    var AppID = context.Transactions.Where(c => c.TransactionID.Equals(TransactionID.TransactionID)).Select(c => c.ApplicationID).FirstOrDefault();
                    //var AppID = context.Transactions.Where(y=>y.TransactionID.Equals(transactionid))
                    aplicationid = AppID;
                }

                //Add transaction document
                if (transaction.TransactionModel.Transaction.Documents.Count > 0)
                {
                    foreach (var item in transaction.TransactionModel.Transaction.Documents)
                    {
                        Entity.TransactionDocument document = new Entity.TransactionDocument()
                        {
                            TransactionID = transactionid,
                            DocTypeID = item.Type.ID,
                            PurposeID = item.Purpose.ID,
                            Filename = item.FileName,
                            DocumentPath = item.DocumentPath,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().DisplayName
                        };

                        context.TransactionDocuments.Add(document);
                    }

                    context.SaveChanges();
                }



                if (TransactionID != null)//save change rm
                {
                    if (transaction.ChangeRM.Count > 0)
                    {
                        foreach (var RM in transaction.ChangeRM)
                        {
                            if (RM.Selected == true)
                            {

                                if (!context.ChangeRMTransactions.Where(a => a.TransactionID.Equals(TransactionID.TransactionID)).Any())
                                {
                                    context.ChangeRMTransactions.Add(new Entity.ChangeRMTransaction
                                    {
                                        TransactionID = TransactionID.TransactionID,
                                        CIF = RM.CIF,
                                        ChangeSegmentFrom = RM.CHANGE_SEGMENT_FROM,
                                        ChangeSegmentTo = RM.CHANGE_SEGMENT_TO,
                                        ChangeFromBranchCode = RM.CHANGE_FROM_BRANCH_CODE,
                                        ChangeFromRMCode = RM.CHANGE_FROM_RM_CODE,
                                        ChangeFromBU = RM.CHANGE_FROM_BU,
                                        ChangeToBranchCode = RM.CHANGE_TO_BRANCH_CODE,
                                        ChangeToRMCode = RM.CHANGE_TO_RM_CODE,
                                        ChangeToBU = RM.CHANGE_TO_BU,
                                        Account = RM.ACCOUNT,
                                        PCCode = RM.PC_CODE,
                                        EATPB = RM.FREE_TEXT_5__EA_TPB_EA_TPB_,
                                        Remarks = RM.REMARKS_REASON,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().DisplayName
                                    });
                                    context.SaveChanges();
                                    IsSuccess = true;
                                }
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                message = ex.Message;
                throw;
            }


            return IsSuccess;
        }
        public bool AddTransactionDraft(TransactionChangeRM transaction)
        {
            bool isSuccess = false;
            var message = "";
            try
            {
                        context.TransactionDrafts.Add(new Entity.TransactionDraft
                        {
                            IsDraft = transaction.TransactionModel.Transaction.IsDraft,
                            TZNumber = transaction.TransactionModel.Transaction.TZNumber,
                            //TransactionID = item.TransactionModel.ID,
                            ApplicationID = transaction.TransactionModel.Transaction.ApplicationID,
                            IsTopUrgent = 1,//(item.TransactionModel.IsTopUrgentChain == 1 ? 2 : item.TransactionModel.IsTopUrgent),
                            Amount = transaction.TransactionModel.Transaction.Amount,
                            Rate = transaction.TransactionModel.Transaction.Rate,
                            AmountUSD = transaction.TransactionModel.Transaction.AmountUSD,
                            BeneName = "OLD BENE NAME",//item.TransactionModel.BeneName,
                            BeneAccNumber = transaction.TransactionModel.Transaction.BeneAccNumber,
                            IsOtherAccountNumber = transaction.TransactionModel.Transaction.IsOtherAccountNumber,
                            IsNewCustomer = transaction.TransactionModel.Transaction.IsNewCustomer,
                            StateID = 1,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().DisplayName,
                            Type = transaction.TransactionModel.Transaction.Type.HasValue ? transaction.TransactionModel.Transaction.Type : null,
                            IsResident = transaction.TransactionModel.Transaction.IsResident,
                            IsCitizen = transaction.TransactionModel.Transaction.IsResident,
                            PaymentDetails = transaction.TransactionModel.Transaction.PaymentDetails,
                            CIF = transaction.ChangeRM[0].CIF,//transaction.TransactionModel.Transaction.Customer.CIF,
                            ProductID = transaction.TransactionModel.Transaction.Product.ID,
                            CurrencyID = transaction.TransactionModel.Transaction.Currency.ID,
                            ChannelID = transaction.TransactionModel.Transaction.Channel.ID,
                            DebitCurrencyID = 13,//item.TransactionModel.DebitCurrency.ID,
                            ProductTypeID = 5,
                            BankChargesID = 2,
                            CBOMaintainID = 1006,
                            //bangkit
                            BankID =1167, //item.TransactionModel.Bank.ID,
                            ApplicationDate =DateTime.UtcNow, //item.TransactionModel.ApplicationDate,
                            BizSegmentID = transaction.TransactionModel.Transaction.BizSegment.ID
                           
                        });
                        context.SaveChanges();
                        isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                throw;
            }
            return isSuccess;
        }
        public bool addChangeRM(ref IList<ChangeRMTransactionModel> ChangeRM, ref string message)
        {
            bool isSuccess = true;

            try
            {
                if (ChangeRM.Count > 0)
                {
                    foreach (var item in ChangeRM)
                    {
                        if (!context.ChangeRMTransactions.Where(a => a.TransactionID.Equals(item.TransactionID)).Any())
                        {
                            context.ChangeRMTransactions.Add(new Entity.ChangeRMTransaction
                            {
                                TransactionID = item.TransactionID,
                                CIF = item.CIF,
                                ChangeSegmentFrom = item.ChangeSegmentFrom,
                                ChangeSegmentTo = item.ChangeSegmentTo,
                                ChangeFromBranchCode = item.ChangeFromBranchCode,
                                ChangeFromRMCode = item.ChangeFromRMCode,
                                ChangeFromBU = item.ChangeFromBU,
                                ChangeToBranchCode = item.ChangeToBranchCode,
                                ChangeToRMCode = item.ChangeToRMCode,
                                ChangeToBU = item.ChangeToBU,
                                Account = item.Account,
                                PCCode = item.PCCode,
                                EATPB = item.EATPB,
                                Remarks = item.Remarks
                            });
                            context.SaveChanges();
                            isSuccess = true;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public void Dispose()
        {
            throw new NotImplementedException();
        }

    }
}