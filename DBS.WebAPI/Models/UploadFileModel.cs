﻿using DBS.Entity;
using DBS.WebAPI.Helpers;
using Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace DBS.WebAPI.Models
{
    #region Property
    public class UploadFileModel
    {
        public string Path { get; set; }
        public string Type { get; set; }
    }
    #endregion

    #region Interface
    public interface IUploadFileRepository
    {
        bool UploadFile(UploadFileModel path, ref string message);
    }
    #endregion

    #region Repository
    public class UploadFileRepository : IUploadFileRepository
    {
        private CurrentUser currentUser = new CurrentUser();

        public bool UploadFile(UploadFileModel model, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!string.IsNullOrEmpty(model.Path))
                {
                    switch (model.Type)
                    {
                        // Read txt file
                        case "text/plain":
                            Text(model, ref message);
                            break;

                        // Read xlsx format
                        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                            Excel(model, ref message);
                            break;

                        // Read xls format
                        case "application/vnd.ms-excel":
                            Excel(model, ref message);
                            break;

                        default:
                            message = "File type does not support for upload function.";
                            break;
                    }
                    if (string.IsNullOrEmpty(message))
                    { isSuccess = true; }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        //Read Data From excel
        public void Excel(UploadFileModel model, ref string message)
        {
            DataSet result = new DataSet();

            if (ReadStreamToDataSet(model, ref result, ref message))
            {
                DBSEntities context = new DBSEntities();

                //get customers
                var customer = (from a in context.Customers select a).ToList();

                //get bank
                var bank = (from a in context.Banks select a).ToList();

                for (int a = 0; a < result.Tables[0].Rows.Count - 1; a++)
                {
                    //for (int b = 0; b < result.Tables[0].Columns.Count; b++)
                    //{
                    //    Console.WriteLine(b + ". " + result.Tables[0].Rows[a][b].ToString());
                    //}

                }
            }
        }

        public bool ReadStreamToDataSet(UploadFileModel model, ref DataSet result, ref string message)
        {
            bool isSuccess = false;

            try
            {
                var path = System.IO.Path.GetFullPath(Directory.GetCurrentDirectory() + @"\" + model.Path);

                FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read);

                IExcelDataReader excelReader = null;

                if (!string.IsNullOrEmpty(stream.Name))
                {
                    switch (model.Type)
                    {
                        case "application/vnd.ms-excel":
                            //Reading from a binary Excel file ('97-2003 format; *.xls)
                            excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                            break;

                        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                            //Reading from a OpenXml Excel file (2007 format; *.xlsx)
                            excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                            break;
                    }
                }

                if (string.IsNullOrEmpty(message))
                {
                    excelReader.IsFirstRowAsColumnNames = true;
                    result = excelReader.AsDataSet();
                    excelReader.Close();
                    isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public void Text(UploadFileModel model, ref string message)
        {
            if (!string.IsNullOrEmpty(model.Path))
            {
                var dir = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                
                //var a = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
                //UriBuilder uri = new UriBuilder(a);
                //var p = Uri.UnescapeDataString(uri.Path);
                //var path = System.IO.Path.GetFullPath(Directory.GetCurrentDirectory() + @"\" + model.Path);
                var path = dir + @"\" + model.Path;

                string[] splitName = path.Split('\\');

                string[] file = splitName[splitName.Length - 1].Split('.');

                if ((file[0] != null) && (file[1] == "txt"))
                {
                    string[] type = file[0].Split('_');

                    switch (type[type.Length - 1])
                    {
                        case "RT":
                            ReadRTGS(path, ref message);
                            break;
                        case "SK":
                            ReadSKN(path, ref message);
                            break;
                        default:
                            message = "Product type does not exist.";
                            break;
                    }
                }
            }
        }

        //Read Data From txt file SKN
        public void ReadSKN(string path, ref string message)
        {
            try
            {
                string line;
                StringBuilder sb = new StringBuilder();

                //Read data from txt
                System.IO.StreamReader file = new System.IO.StreamReader(path);

                string header = string.Empty;

                while ((line = file.ReadLine()) != null)
                {
                    string code = line.Substring(0, 1);

                    switch (code)
                    {
                        case "0":
                            header = line;

                            break;
                        case "1":

                            //break down line header
                            #region Header
                            string var1 = header.Substring(1, 10).TrimEnd();
                            DateTime ValueDate = DateTime.ParseExact(var1, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);

                            string DebtAccNo = header.Substring(11, 35).TrimEnd();

                            string var2 = header.Substring(46, 35).TrimEnd();
                            string sqlCustomer = "SELECT * FROM [Customer].[Customer] WHERE CustomerName = '" + var2 + "'";
                            DataTable dtCustomer = new DataTable();
                            dtCustomer = DataHelper.ExecuteDataTable(sqlCustomer);
                            var Cif = dtCustomer != null ? dtCustomer.Rows[0][0].ToString() : null;

                            string IsNewCustomer = Cif == null ? "1" : "0";
                            #endregion

                            //break down content
                            #region Content
                            string var3 = line.Substring(21, 4).TrimEnd();
                            string sqlBank = "SELECT * FROM [Master].[Bank] WHERE BankCode = '" + var3 + "'";
                            DataTable dtBank = new DataTable();
                            dtBank = DataHelper.ExecuteDataTable(sqlBank);
                            var BankId = dtBank != null ? dtBank.Rows[0][0].ToString() : null;

                            string BranchCode = line.Substring(25, 4).TrimEnd();

                            string BenefAccNo = line.Substring(40, 35).TrimEnd();

                            string BenefName = line.Substring(75, 50).TrimEnd();

                            string Resident = line.Substring(264, 1).TrimEnd() == "1" ? "1" : "0";

                            string Citizen = line.Substring(265, 1).TrimEnd() == "0" ? "1" : "0";

                            string TrxType = line.Substring(269, 2).TrimEnd();

                            string PaymentAmount = line.Substring(271, 17).TrimStart('0');

                            string var4 = line.Substring(290, 16).TrimEnd();
                            string sqlBankCharges = "SELECT * FROM [Master].[Charges] WHERE ChargesCode = '" + var4 + "'";
                            DataTable dtBankCharges = new DataTable();
                            dtBankCharges = DataHelper.ExecuteDataTable(sqlBankCharges);
                            var Charges = dtBankCharges != null ? dtBankCharges.Rows[0][0].ToString() : null;

                            string PaymentDetails = line.Substring(309, 210).TrimEnd();
                            #endregion

                            //query
                            #region Query
                            sb.Append("INSERT [Data].[TransactionDraft] ( "
                            + "[CIF], "
                                //+ "[BizSegmentID], "
                            + "[AccountNumber], "
                            + "[ApplicationDate], "
                            + "[Amount], "
                            + "[BeneName], "
                            + "[BeneAccNumber], "
                            + "[BankID], "
                            + "[PaymentDetails], "
                            + "[IsResident], "
                            + "[IsCitizen], "
                            + "[BankChargesID], "
                                //+ "[AgentChargesID], "
                            + "[Type], "
                            + "[IsTopUrgent], "
                            + "[IsNewCustomer], "
                                //+ "[IsSignatureVerified], "
                                //+ "[IsDormantAccount], "
                                //+ "[IsFrezeAccount], "
                                //+ "[IsDocumentComplete], "
                            + "[IsDraft], "
                            + "[CreateDate], "
                            + "[CreateBy]"
                            + ") VALUES (");

                            sb.Append(Cif + ", " +
                                      DebtAccNo + ", '" +
                                      ValueDate + "', " +
                                      PaymentAmount + ", '" +
                                      BenefName + "', " +
                                      BenefAccNo + ", " +
                                      BankId + ", '" +
                                      PaymentDetails + "', " +
                                      Resident + ", " +
                                      Citizen + ", " +
                                      Charges + ", " +
                                      TrxType + ", " +
                                      0 + ", " +
                                      IsNewCustomer + ", " +
                                //0 + ", " +
                                //0 + ", " +
                                //0 + ", " +
                                //0 + ", " +
                                      1 + ", '" +
                                      DateTime.UtcNow + "', '" +
                                      "" + currentUser.GetCurrentUser().DisplayName + "')"
                                );
                            #endregion

                            break;
                        case "9":

                            break;
                    }
                }

                //Close file
                file.Close();

                //Execute string and insert data into database
                DataHelper.ExecuteNonQuery(sb.ToString());
                //Console.WriteLine(sb.ToString());
                //Console.ReadLine();
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
        }

        //Read Data From txt file RTGS
        public void ReadRTGS(string path, ref string message)
        {
            try
            {
                string line;
                StringBuilder sb = new StringBuilder();

                //Read data from txt
                System.IO.StreamReader file = new System.IO.StreamReader(path);

                //Initialize index of data
                string[] dataindex = { "3", "4", "5", "6", "7", "9", "10", "17", "32", "33", "34", "35" };

                //Iterate data and build sql string
                while ((line = file.ReadLine()) != null)
                {
                    sb.Append("INSERT [Data].[TransactionDraft] ( "
                            + "[CIF], "
                            + "[BizSegmentID], "
                            + "[AccountNumber], "
                            + "[ApplicationDate], "
                            + "[Amount], "
                            + "[BeneName], "
                            + "[BeneAccNumber], "
                            + "[BankID], "
                            + "[PaymentDetails], "
                            + "[IsResident], "
                            + "[IsCitizen], "
                            + "[BankChargesID], "
                            + "[AgentChargesID], "
                            + "[Type], "
                            + "[IsTopUrgent], "
                            + "[IsNewCustomer], "
                        //+ "[IsSignatureVerified], "
                        //+ "[IsDormantAccount], "
                        //+ "[IsFrezeAccount], "
                        //+ "[IsDocumentComplete], "
                            + "[IsDraft], "
                            + "[CreateDate], "
                            + "[CreateBy]"
                            + ") VALUES (");

                    //Split data each line
                    string[] splitline = line.Split('^');

                    //Iterate each column in a row
                    for (int i = 0; i < splitline.Length; i++)
                    {
                        if (Array.IndexOf(dataindex, i.ToString()) >= 0)
                        {
                            switch (i)
                            {
                                case 3:
                                    string sqlCustomer = "SELECT * FROM [Customer].[Customer] WHERE CIF = '" + splitline[i] + "'";
                                    DataTable dtCustomer = new DataTable();
                                    dtCustomer = DataHelper.ExecuteDataTable(sqlCustomer);
                                    sb.Append("" + dtCustomer.Rows[0][0] + ", " + dtCustomer.Rows[0][3] + ", ");
                                    break;
                                case 5:
                                    sb.Append("CAST('" + splitline[i] + "' AS DateTime), ");
                                    break;
                                case 6:
                                    sb.Append("" + splitline[i] + ", ");
                                    break;
                                case 10:
                                    string sqlBank = "SELECT * FROM [Master].[Bank] WHERE BankCode = '" + splitline[i] + "'";
                                    DataTable dtBank = new DataTable();
                                    dtBank = DataHelper.ExecuteDataTable(sqlBank);
                                    sb.Append("" + dtBank.Rows[0][0] + ", ");
                                    break;
                                case 32:
                                    string isResident = splitline[i].ToLower().Equals("true") ? "1" : "0";
                                    sb.Append(isResident + ", ");
                                    break;
                                case 33:
                                    string isCitizen = splitline[i].ToLower().Equals("true") ? "1" : "0";
                                    sb.Append(isCitizen + ", ");
                                    break;
                                case 34:
                                    string sqlBankCharges = "SELECT * FROM [Master].[Charges] WHERE ChargesCode = '" + splitline[i] + "'";
                                    DataTable dtBankCharges = new DataTable();
                                    dtBankCharges = DataHelper.ExecuteDataTable(sqlBankCharges);
                                    sb.Append("" + dtBankCharges.Rows[0][0] + ", ");
                                    break;
                                case 35:
                                    string sqlAgentCharges = "SELECT * FROM [Master].[Charges] WHERE ChargesCode = '" + splitline[i] + "'";
                                    DataTable dtAgentCharges = new DataTable();
                                    dtAgentCharges = DataHelper.ExecuteDataTable(sqlAgentCharges);
                                    sb.Append("" + dtAgentCharges.Rows[0][0] + ", ");
                                    break;
                                default:
                                    sb.Append("'" + splitline[i] + "', ");
                                    break;
                            }
                        }
                    }

                    sb.Append("20, 0, 0, 1, CAST('" + DateTime.UtcNow + "' AS DateTime), '" + currentUser.GetCurrentUser().DisplayName + "')");
                    sb.AppendLine(Environment.NewLine);
                }

                //Close file
                file.Close();

                //Execute string and insert data into database
                DataHelper.ExecuteNonQuery(sb.ToString());
                //Console.WriteLine(sb.ToString());
                //Console.ReadLine();
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
        }
    }
    #endregion
}