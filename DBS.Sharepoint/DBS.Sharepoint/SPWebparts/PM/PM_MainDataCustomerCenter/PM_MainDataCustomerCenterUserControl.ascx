﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PM_MainDataCustomerCenterUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.PM.PM_MainDataCustomerCenter.PM_MainDataCustomerCenterUserControl" %>
<h1 class="header smaller lighter dark">Escalation Customer</h1>
<div class="modal-body overflow-visible">

                            <div class="row">
                                <div class="col-sm-5">
                          
                                    <div class="row">
                                        <label class="col-sm-8" for="SingleValue">
                                            InformationType
                                        </label>                       
                                    </div>
                       
                                     <div class="row">                           
	                                    <div class="col-sm-9">
									        <div class="clearfix">
										        <select class="col-xs-12" id="InformationType" name="InformationType" data-bind="options: InformationTypeDrop, optionsText: function (item) { if (item.CBOMaintainSubID != null) return item.SubTypeName }, optionsValue: 'CBOMaintainSubID', selectedOptions: SelectedInformationType, visible: $root.Readonly() == false" size="10" multiple="multiple">
										        </select>
									        </div>
								        </div>                             
                                     </div>
                               
                                 </div>
                                <div class="col-sm-2">
                                    <div class="space-30"></div>   
                                    <div class="space-30"></div>
                                    <div class="space-10"></div>
                                    
                                
                                  
                             
                                    <button id="btnPullLeft" type="button" style="margin-left:10%;" class="btn btn-sm btn-primary" data-bind="click: pullLeft, visible: $root.IsRoleMaker()">                                    
                                           <
                                    </button>
                                   
                                    <div class="space-2"></div>
                                    <button id="btnPullRight" type="button" style="margin-left:10%;" class="btn btn-sm btn-primary" data-bind="click: pullRight, visible: $root.IsRoleMaker()">                                    
                                           >
                                    </button>

                  
                                    
                                    <div class="space-2"></div>
                                    &nbsp &nbsp <button id="save" type="button" class="btn btn-sm btn-primary" data-bind="click: save, visible: $root.IsRoleMaker()">
                                        <i class="icon-save"></i>
                                        Save
                                    </button>                           
                                </div>
  
    
                                 
                                <div class="col-sm-5">
                                    <div class="row">
                                        <label class="col-sm-8" for="SingleValue">
                                            Maintenance CC
                                        </label>                       
                                    </div>
   
                                    <div class="row">
                            
	                                    <div class="col-sm-9">
									        <div class="clearfix">
										        <select class="col-xs-12" id="MaintenanceCC" name="MaintenanceCC" data-bind="options: MaintenanceCCDrop, optionsText: function (item) { if (item.CBOMaintainSubID != null) return item.SubTypeName }, optionsValue: 'CBOMaintainSubID', selectedOptions: SelectedMaintenanceCC, visible: $root.Readonly() == false" size="10" multiple="multiple">
										        </select>
									        </div>
								        </div>                                
                                    </div>    
                           
                                </div>
                            </div>
                    </div>
             

<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.hotkeys.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/bootstrap-wysiwyg.min.js"></script>