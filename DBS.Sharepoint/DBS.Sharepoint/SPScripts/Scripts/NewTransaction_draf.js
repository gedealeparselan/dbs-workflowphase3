/**
 * Created by ruddycahyadi on 9/29/2014.
 */

var accessToken;

var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};

var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};

var BizSegmentModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var BranchModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var TypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ProductModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    WorkflowID: ko.observable(),
    Workflow: ko.observable()
};

var ChannelModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var BankModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    BranchCode: ko.observable(),
    SwiftCode: ko.observable(),
    BankAccount: ko.observable(),
    Description: ko.observable(),
    CommonName: ko.observable(),
    Currency: ko.observable(),
    PGSL: ko.observable()
};

var RMModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    RankID: ko.observable(),
    Rank: ko.observable(),
    SegmentID: ko.observable(),
    Segment: ko.observable(),
    BranchID: ko.observable(),
    Branch: ko.observable(),
    Email: ko.observable()
};

var ContactModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    PhoneNumber: ko.observable(),
    DateOfBirth: ko.observable(),
    Address: ko.observable(),
    IDNumber: ko.observable()
};

var FunctionModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var CurrencyModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var AccountModel = {
    AccountNumber: ko.observable(),
    Currency: ko.observable(CurrencyModel)
};

var UnderlyingDocumentModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var DocumentTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var DocumentPurposeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var FXComplianceModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ChargesModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var UnderlyingModel = {
    ID: ko.observable(),
    UnderlyingDocument: ko.observable(UnderlyingDocumentModel),
    DocumentType: ko.observable(DocumentTypeModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    AttachmentNo: ko.observable(),
    IsStatementLetter: ko.observable(),
    IsDeclarationOfException: ko.observable(),
    StartDate: ko.observable(),
    EndDate: ko.observable(),
    DateOfUnderlying: ko.observable(),
    ExpiredDate: ko.observable(),
    ReferenceNumber: ko.observable(),
    SupplierName: ko.observable(),
    InvoiceNumber: ko.observable()
};

var DocumentModel = {
    ID: ko.observable(),
    Type: ko.observable(DocumentTypeModel),
    Purpose: ko.observable(DocumentPurposeModel),
    FileName: ko.observable(),
    DocumentPath: ko.observable(),
    LastModifiedDate: ko.observable(new Date),
    LastModifiedBy: ko.observable()
};

var CustomerModel = {
    CIF: ko.observable(),
    Name: ko.observable(),
    POAName: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Branch: ko.observable(BranchModel),
    Type: ko.observable(TypeModel),
    RM: ko.observable(RMModel),
    Contacts: ko.observableArray([ContactModel]),
    Functions: ko.observableArray([FunctionModel]),
    Accounts: ko.observableArray([AccountModel]),
    Underlyings: ko.observableArray([UnderlyingModel])
};

var TransactionModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    IsNewCustomer: ko.observable(false),
    Product: ko.observable(ProductModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    Channel: ko.observable(ChannelModel),
    Account: ko.observable(AccountModel),
    ApplicationDate: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Bank: ko.observable(BankModel),
    BankCharges: ko.observable(ChargesModel),
    AgentCharges: ko.observable(ChargesModel),
    Type: ko.observable(20),
    IsResident: ko.observable(true),
    IsCitizen: ko.observable(true),
    PaymentDetails: ko.observable(null),
    LLDCode: ko.observable(null),
    LLDInfo: ko.observable(null),
    IsSignatureVerified: ko.observable(false),
    IsDormantAccount: ko.observable(false),
    IsFreezeAccount: ko.observable(false),
    Others: ko.observable(null),
    IsDraft: ko.observable(false),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]), //DocumentModel
    DetailCompliance: ko.observable(),
    BeneName: ko.observable(),
    BeneAccNumber: ko.observable(),
    TZNumber: ko.observable()
};

var Parameter = {
    Products: ko.observableArray([ProductModel]),
    Currencies: ko.observableArray([CurrencyModel]),
    Channels: ko.observableArray([ChannelModel]),
    BizSegments: ko.observableArray([BizSegmentModel]),
    Banks: ko.observableArray([BankModel]),
    BankCharges: ko.observableArray([ChargesModel]),
    AgentCharges: ko.observableArray([ChargesModel]),
    FXCompliances: ko.observableArray([FXComplianceModel]),
    DocumentTypes: ko.observableArray([DocumentTypeModel]),
    DocumentPurposes: ko.observableArray([DocumentPurposeModel])
};

var SelectedModel = {
    Product: ko.observable(),
    Currency: ko.observable(),
    Channel: ko.observable(),
    Account: ko.observable(),
    BizSegment: ko.observable(),
    Bank: ko.observable(),
    DocumentType: ko.observable(),
    DocumentPurpose: ko.observable(),
    NewCustomer: ko.observable({
        Currency: ko.observable(),
        BizSegment: ko.observable()
    }),
    BankCharges: ko.observable(3),
    AgentCharges: ko.observable(3)
};

var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;

    // Sharepoint User
    self.SPUser = ko.observable(SPUser);

    // input form controls
    self.IsEditable = ko.observable(false);

    // Parameters
    self.Parameter = ko.observable(Parameter);

    // Main Model
    self.TransactionModel = ko.observable(TransactionModel);

    // Double Transaction
    self.DoubleTransactions = ko.observableArray([]);

    // Validation User
    self.UserValidation = ko.observable({
        UserID: ko.observable(),
        Password: ko.observable(),
        Error: ko.observable()
    });

    // Options selected value
    self.Selected = ko.observable(SelectedModel);

    // Temporary documents
    self.Documents = ko.observableArray([]);
    self.DocumentPath = ko.observable();
    self.DocumentType = ko.observable();
    self.DocumentPurpose = ko.observable();
    self.DocumentFileName = ko.observable();

    // Local Date
    self.LocalDate = function(date, isDateOnly, isDateLong){
        /*
        var localDate = new Date(date);

        if(moment(localDate).isValid()){
            if(isDateOnly != undefined || isDateOnly == true){
                if(isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            }else{
                return moment(localDate).format(config.format.dateTime);
            }
        }else{
            return ""
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    // New Customer on change handler
    self.IsNewCustomerOnChange = function(){
        // disabled autocomplete if this is a new customer
        $("#customer-name").autocomplete({ disabled: self.TransactionModel().IsNewCustomer() });

        return true;
    };

    // Product on change handler
    self.OnProductChange = function(){
        var options = {
            url: api.server + api.url.transaction +"/GetApplicationID",
            params: {
                ProductID: self.TransactionModel().Product().ID
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetApplicationID, OnError, OnAlways);
    };

    // Uploading document
    self.UploadDocument = function(){
        self.DocumentPath(null);
        self.Selected().DocumentType(null);
        self.Selected().DocumentPurpose(null);
        self.DocumentType(null);
        self.DocumentPurpose(null);

        // show upload dialog
        $("#modal-form-upload").modal('show');
    }

    // Add new document handler
    self.AddDocument = function () {
        var doc = {
            ID: 0,
            Type: self.DocumentType(),
            Purpose: self.DocumentPurpose(),
            FileName: self.DocumentFileName(),
            DocumentPath: self.DocumentPath(),
            LastModifiedDate: new Date(),
            LastModifiedBy: null
        };
        //alert(JSON.stringify(self.DocumentPath()))
        //UploadFile(self.DocumentPath());

        //alert(JSON.stringify(self.DocumentPath()))
        if(doc.Type == null || doc.Purpose == null || doc.DocumentPath == null){
            alert("Please complete the upload form fields.")
        }else{
            self.Documents.push(doc);

            // hide upload dialog
            $("#modal-form-upload").modal('hide');
        }


        // test upload
        /*var options = {
            url: "/_api/web/GetFolderByServerRelativeUrl('/sites/fileuploadtest/Documents/Communities)/Files/Add(url='"+ file.name + "',overwrite=true)"
        };
        Helper.Ajax.Post(options, OnSuccessUpload, OnError);*/
    };

    self.EditDocument = function(data){
        self.DocumentPath(data.DocumentPath);
        self.Selected().DocumentType(data.Type.ID);
        self.Selected().DocumentPurpose(data.Purpose.ID);

        // show upload dialog
        $("#modal-form-upload").modal('show');
    };

    self.RemoveDocument = function(data){
      //alert(JSON.stringify(data))
        self.Documents.remove(data);
    };

    // Save as draft handler
    self.SaveAsDraft = function () {
        var data = {
            ApplicationID: self.TransactionModel().ApplicationID(),
            CIF: self.TransactionModel().Customer().CIF,
            Name: self.TransactionModel().Customer().Name
        };

        /*if(self.TransactionModel().Documents().length > 0){
            for(var i =0; i < self.TransactionModel().Documents().length; i++)
            {
                //var upload = UploadFile(data, self.TransactionModel().Documents()[i]); //["DocumentPath"]
                //alert(JSON.stringify(self.TransactionModel().Documents()[i]["DocumentPath"]));
                alert(JSON.stringify(UploadFile(data, self.TransactionModel().Documents()[i])))
            }
        }*/

        // validation
        var form = $("#aspnetForm");
        form.validate();

        if(form.valid()) {
            // set as draft
            self.TransactionModel().IsDraft(true);

            // disabling form input controls
            self.IsEditable(false);

            /*var options = {
                url: api.server + api.url.transaction,
                token: accessToken,
                data: ko.toJSON(self.TransactionModel())
            };

            // Save or Update
            if(self.TransactionModel().ID() == null){
                // if id is null = Save
                Helper.Ajax.Post(options, OnSuccessSaveAsDraft, OnError, OnAlways);
            }else{
                // if id is not null = Update
                options.url += "/"+ self.TransactionModel().ID();

                Helper.Ajax.Put(options, OnSuccessSaveAsDraft, OnError, OnAlways);
            }*/

            // start upload docs and save the transaction
            UploadDocuments();
        }
    };

    // Save as draft handler
    self.Submit = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if(form.valid()) {
            // set as not draft
            self.TransactionModel().IsDraft(false);

            // disabling form input controls
            self.IsEditable(false);

            // clear double transactions
            self.DoubleTransactions().removeAll;

            // Validation #1 : Check "Others" field
            if(self.TransactionModel().Others() == undefined || self.TransactionModel().Others() == ""){
                // Validation #2 : Check new transaction status
                ValidateTransaction();
            }else{
                var text = "Are you sure to make this transaction as an <b>Exceptional Handling</b>?";

                bootbox.confirm(text, function (result) {
                    if (result) {
                        // Validation #2 : Check new transaction status
                        ValidateTransaction();
                    }else{
                        // enable form input controls
                        self.IsEditable(true);
                    }
                });
            }

            /*var options = {
                url: api.server + api.url.transaction,
                token: accessToken,
                data: ko.toJSON(self.TransactionModel())
            };

            // Save to api
            Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);*/

            /*setTimeout(function(){
                //ko.unwrapObservable(TransactionModel);
                //self.TransactionModel(TransactionModel);
                //ResetModel();
                self.IsEditable(true);
            }, 3000);*/
        }
		else {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        }
    };

    // User Validation process
    self.ValidationProcess = function(){
        // Ruddy 21.10.2014 : Pending task. Still can't validate user & password to sharepoint.
        /*var options = {
            url: "/_vti_bin/DBSUserValidation/UserValidationServices.svc/Validate",
            data: ko.toJSON(self.UserValidation())
        };

        Helper.Sharepoint.Validate.User(options, function (data, textStatus, jqXHR){
            if(jqXHR.status == 200) {
                if (data.IsAuthorized == true && data.IsCheckerGroup == true) {
                    // hide Double Transaction dialog
                    $("#modal-double-transaction").modal('hide');

                    // start upload docs and save the transaction
                    UploadDocuments();
                } else {
                    // show error
                    self.UserValidation().Error(data.Message);
                }
            }
        }, OnError, OnAlways);*/

        // remove this when pending task (user validation has been completed).
        UploadDocuments(); // start upload docs and save the transaction
    };

    // Continue editing
    self.ContinueEditing = function(){
        // enable form input controls
        self.IsEditable(true);
    };
};

// Knockout View Model
var viewModel = new ViewModel();

// jQuery Init
$(document).ready(function () {
    // datepicker
    $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
        $(this).prev().focus();
    });

    // validation
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,

        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if(element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if(element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if(element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });

    // autocomplete
    $("#customer-name").autocomplete({
        source: function (request, response) {
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.customer + "/Search",
                data: {
                    query: request.term,
                    limit: 20
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
        },
        minLength: 2,
        select: function (event, ui) {
            // set customer data model
            if(ui.item.data != undefined || ui.item.data != null) {
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                viewModel.TransactionModel().Customer(ko.mapping.toJS(ui.item.data, mapping));
            }
            else
                viewModel.TransactionModel().Customer(null);
        }
    });

    // ace file upload
    $('#document-path').ace_file_input({
        no_file:'No File ...',
        btn_choose:'Choose',
        btn_change:'Change',
        droppable:false,
        //onchange:null,
        thumbnail:false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist:'exe|dll'
        //onchange:''
        //
    });

    // Knockout Datepicker custom binding handler
    ko.bindingHandlers.datepicker = {
        init: function(element){
            $(element).datepicker({autoclose:true}).next().on(ace.click_event, function(){
                $(this).prev().focus();
            });
        },
        update: function(element){
            $(element).datepicker("refresh");
        }
    };

    // Knockout custom file handler
    ko.bindingHandlers.file = {
        init: function(element, valueAccessor){
            $(element).change(function(){
                var file = this.files[0];

                if(ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };

    // Dependant Observable for dropdown auto fill
    ko.dependentObservable(function () {
        var product = ko.utils.arrayFirst(viewModel.Parameter().Products(), function (item){ return item.ID == viewModel.Selected().Product(); });
        var currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item){ return item.ID == viewModel.Selected().Currency(); });
        var channel = ko.utils.arrayFirst(viewModel.Parameter().Channels(), function (item){ return item.ID == viewModel.Selected().Channel(); });
        var bizSegment = ko.utils.arrayFirst(viewModel.Parameter().BizSegments(), function (item){ return item.ID == viewModel.Selected().BizSegment(); });
        var bank = ko.utils.arrayFirst(viewModel.Parameter().Banks(), function (item){ return item.ID == viewModel.Selected().Bank(); });
        var account = ko.utils.arrayFirst(viewModel.TransactionModel().Customer().Accounts, function (item){ return item.AccountNumber == viewModel.Selected().Account(); });
        var docPurpose = ko.utils.arrayFirst(viewModel.Parameter().DocumentPurposes(), function (item){ return item.ID == viewModel.Selected().DocumentPurpose(); });
        var docType = ko.utils.arrayFirst(viewModel.Parameter().DocumentTypes(), function (item){ return item.ID == viewModel.Selected().DocumentType(); });
        var bankCharges = ko.utils.arrayFirst(viewModel.Parameter().BankCharges(), function (item){ return item.ID == viewModel.Selected().BankCharges(); });
        var agentCharges = ko.utils.arrayFirst(viewModel.Parameter().AgentCharges(), function (item){ return item.ID == viewModel.Selected().AgentCharges(); });

        if(product != null) viewModel.TransactionModel().Product(product);
        if(currency != null) {
            viewModel.TransactionModel().Currency(currency);
            GetRateAmount(currency.ID);
        }
        if(channel != null) viewModel.TransactionModel().Channel(channel);
        if(bizSegment != null) viewModel.TransactionModel().BizSegment(bizSegment);
        if(bank != null) viewModel.TransactionModel().Bank(bank);
        if(account != null) viewModel.TransactionModel().Account(account);
        if(docType != null) viewModel.DocumentType(docType);
        if(docPurpose != null) viewModel.DocumentPurpose(docPurpose);
        if(bankCharges != null) viewModel.TransactionModel().BankCharges(bankCharges);
        if(agentCharges != null) viewModel.TransactionModel().AgentCharges(agentCharges);

        if(viewModel.TransactionModel().IsNewCustomer()){
            var new_currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item){ return item.ID == viewModel.Selected().NewCustomer().Currency(); });

            if(new_currency != null) viewModel.TransactionModel().Account().Currency(new_currency);
            if(bizSegment != null) viewModel.TransactionModel().Customer().BizSegment(bizSegment);
        }
    });
    
    // Token Validation
    if($.cookie(api.cookie.name) == undefined){
        Helper.Token.Request(OnSuccessToken, OnError);
    }else{
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);

            viewModel.SPUser(ko.mapping.toJS(spUser));
        }

        // call get data inside view model
        GetParameters();
    }

    // Knockout Bindings
    ko.applyBindings(viewModel);

    function GetRateAmount(CurrencyID) {
            var options = {
                url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
                params: {
                },
                token: accessToken
            };

            Helper.Ajax.Get(options, OnSuccessGetRateAmount, OnError, OnAlways);
    }

    function OnSuccessGetRateAmount(data, textStatus, jqXHR){
        if(jqXHR.status = 200){
            // bind result to observable array
            viewModel.TransactionModel().Rate(data.RupiahRate);
        }else{
            ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    var idrrate = 0;

    GetRateIDR();

    function GetRateIDR() {
        var options = {
            url: api.server + api.url.currency + "/CurrencyRate/" + "/1",
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetRateIDR, OnError, OnAlways);
    }

    function OnSuccessGetRateIDR(data, textStatus, jqXHR){
        if(jqXHR.status = 200){
            // bind result to observable array
            idrrate = data.RupiahRate;
        }else{
            ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    //================================= Eqv Calculation ====================================

    var x = document.getElementById("trxn-amount");
    var y = document.getElementById("rate");
    var d = document.getElementById("eqv-usd");
    var xstored = x.getAttribute("data-in");
    var ystored = y.getAttribute("data-in");
    setInterval(function(){
        if( x == document.activeElement ){
            var temp = x.value;
            if( xstored != temp ){
                xstored = temp;
                x.setAttribute("data-in",temp);
                calculate();
            }
        }
        if( y == document.activeElement ){
            var temp = y.value;
            if( ystored != temp ){
                ystored = temp;
                y.setAttribute("data-in",temp);
                calculate();
            }
        }
    },50);

    function calculate(){
        var res = (x.value * y.value) / idrrate;
        //$("#eqv-usd").val(res);
        viewModel.TransactionModel().AmountUSD(res);
    }
    x.onblur = calculate;
    calculate();

    //================================= End Eqv Calculation ====================================
});

// reset model
function ResetModel(){
    /*for(var index in viewModel){
        if(ko.isObservable(viewModel[index]))
            viewModel[index](null);
    }*/
    viewModel = new ViewModel();
}

// get all parameters need
function GetParameters(){
    var options = {
        url: api.server + api.url.parameter,
        params: {
            select: "Product,Currency,Channel,BizSegment,Bank,BankCharge,AgentCharge,FXCompliance,ChargesType,DocType,PurposeDoc"
        },
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
}

function SaveTransaction() {
    if(viewModel.TransactionModel().Documents().length == viewModel.Documents().length){
        var options = {
            url: api.server + api.url.transaction,
            token: accessToken,
            data: ko.toJSON(viewModel.TransactionModel())
        };

        // Save to api
        //Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);

        // Save or Update
        if(viewModel.TransactionModel().ID() == null){
            // if id is null = Save
            Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
        }else{
            // if id is not null = Update
            options.url += "/"+ viewModel.TransactionModel().ID();

            Helper.Ajax.Put(options, OnSuccessSaveAsDraft, OnError, OnAlways);
        }
    }
}

function AddListItem() {
    var body = {
        Title: viewModel.TransactionModel().ApplicationID() + " - " + viewModel.TransactionModel().Customer().CIF,
        Application_x0020_Date: viewModel.TransactionModel().ApplicationDate(),
        Application_x0020_ID: viewModel.TransactionModel().ApplicationID(),
        Top_x0020_Urgent: viewModel.TransactionModel().IsTopUrgent(),
        CIF: viewModel.TransactionModel().Customer().CIF,
        Customer_x0020_Name: viewModel.TransactionModel().Customer().Name,
        New_x0020_Customer: viewModel.TransactionModel().IsNewCustomer(),
        Product: viewModel.TransactionModel().Product().Name,
        Currency: viewModel.TransactionModel().Currency().Code,
        Amount: viewModel.TransactionModel().Amount(),
        Rate: viewModel.TransactionModel().Rate(),
        Amount_x0020_USD: viewModel.TransactionModel().AmountUSD(),
        Channel: viewModel.TransactionModel().Channel().Name,
        Account_x0020_Number: viewModel.TransactionModel().Account().AccountNumber,
        Account_x0020_Currency: viewModel.TransactionModel().Account().Currency.Code,
        Bene_x0020_Name: viewModel.TransactionModel().BeneName(),
        BeneAccNumber: viewModel.TransactionModel().BeneAccNumber(),
        Bank: viewModel.TransactionModel().Bank().Description,
        Swift_x0020_Code: viewModel.TransactionModel().Bank().SwiftCode,
        Bene_x0020_Account: viewModel.TransactionModel().Bank().BankAccount,
        Biz_x0020_Segment: viewModel.TransactionModel().BizSegment().Name,
        LLD_x0020_Code: viewModel.TransactionModel().LLDCode(),
        LLD_x0020_Info: viewModel.TransactionModel().LLDInfo(),
        Signature_x0020_Verified: viewModel.TransactionModel().IsSignatureVerified(),
        Dormant_x0020_Account: viewModel.TransactionModel().IsDormantAccount(),
        Freeze_x0020_Account: viewModel.TransactionModel().IsFreezeAccount(),
        Detail_x0020_Compliance: viewModel.TransactionModel().DetailCompliance(),
        Others: viewModel.TransactionModel().Others(),
        Transaction_x0020_ID: viewModel.TransactionModel().ID(),
        Require_x0020_Callback: false, // New callback column
        Initiator_x0020_GroupId: viewModel.SPUser().Roles[0].ID,
        __metadata: {
            type: config.sharepoint.metadata.list
        }
    };

    var options = {
        url: config.sharepoint.url.api + "/Lists(guid'" + config.sharepoint.listId + "')/Items",
        data: JSON.stringify(body),
        digest: jQuery("#__REQUESTDIGEST").val()
    };

    Helper.Sharepoint.List.Add(options, OnSuccessAddListItem, OnError, OnAlways);
}

function ValidateTransaction() {
    // Ruddy : new exception to validate double transaction/underlying
    var options = {
        url: api.server + api.url.workflow.transactionCheck,
        token: accessToken,
        params: {
            cif: viewModel.TransactionModel().Customer().CIF,
            productID: viewModel.TransactionModel().Product().ID,
            currencyID: viewModel.TransactionModel().Currency().ID,
            amountUSD: viewModel.TransactionModel().AmountUSD(),
            applicationDate: viewModel.TransactionModel().ApplicationDate()
        }
    };

    Helper.Ajax.Get(options, function(data, textStatus, jqXHR){
        if(jqXHR.status == 200){
            // Double transaction handling
            if(data.IsDoubleTransaction){
                // fil double transactions
                viewModel.DoubleTransactions(data.Transactions);

                // Show Double Transaction dialog
                $("#modal-double-transaction").modal('show');
            }else{
                // start upload docs and save the transaction
                UploadDocuments();
            }
        }
    }, OnError);
}

function UploadDocuments() {
    // uploading documents. after upload completed, see SaveTransaction()
    var data = {
        ApplicationID: viewModel.TransactionModel().ApplicationID(),
        CIF: viewModel.TransactionModel().Customer().CIF,
        Name: viewModel.TransactionModel().Customer().Name
    };

	//bangkit
    if(viewModel.Documents().length > 0){
        for(var i =0; i < viewModel.Documents().length; i++)
        {
            UploadFile(data, viewModel.Documents()[i], SaveTransaction);
        }
    }
	else{
		SaveTransaction();
	}
}

// Event handlers declaration start
function OnSuccessUpload(data, textStatus, jqXHR){
    ShowNotification("Upload Success", JSON.stringify(data), "gritter-success", true);
}

function OnSuccessGetApplicationID(data, textStatus, jqXHR){
    // set ApplicationID
    if(data != null){
        viewModel.TransactionModel().ApplicationID(data.ApplicationID);
    }
}

function OnSuccessGetParameters(data, textStatus, jqXHR){
    if(jqXHR.status = 200){
        // bind result to observable array
        //self.Products(ko.toJS(data.Product));
        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };

        viewModel.Parameter().Products(ko.mapping.toJS(data.Product, mapping));
        viewModel.Parameter().Channels(ko.mapping.toJS(data.Channel, mapping));
        viewModel.Parameter().Currencies(ko.mapping.toJS(data.Currency, mapping));
        viewModel.Parameter().BizSegments(ko.mapping.toJS(data.BizSegment, mapping));
        viewModel.Parameter().Banks(ko.mapping.toJS(data.Bank, mapping));
        viewModel.Parameter().FXCompliances(ko.mapping.toJS(data.FXCompliance, mapping));
        viewModel.Parameter().BankCharges(ko.mapping.toJS(data.ChargesType, mapping));
        viewModel.Parameter().AgentCharges(ko.mapping.toJS(data.ChargesType, mapping));
        viewModel.Parameter().DocumentTypes(ko.mapping.toJS(data.DocType, mapping));
        viewModel.Parameter().DocumentPurposes(ko.mapping.toJS(data.PurposeDoc, mapping));

        // enabling form input controls
        viewModel.IsEditable(true);
    }else{
        ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function OnSuccessSaveAsDraft(data, textStatus, jqXHR) {
    if(data.ID != null || data.ID != undefined){
        viewModel.TransactionModel().ID(data.ID);
    }

    // send notification
    ShowNotification(self.TransactionModel().IsDraft() ? 'Saving Success': 'Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);
}

function OnSuccessAddListItem(data, textStatus, jqXHR) {
    // clear transaction model
    //viewModel.TransactionModel(null);

    // send notification
    //ShowNotification('Submit Success', 'New transaction has been submitted at '+ viewModel.LocalDate(data.d.Created), 'gritter-success', false);

    // redirect to all transaction
    //window.location = "/home/all-transaction";
    //bangkit
    window.location = "/home";
}

function OnSuccessSaveAPI(data, textStatus, jqXHR){
    // Get transaction id from api result
    if(data.ID != null || data.ID != undefined){
        viewModel.TransactionModel().ID(data.ID);
        
        // insert to sharepoint list item
        AddListItem();
    }
}
// Event handlers declaration end

// Autocomplete
function OnSuccessAutoComplete(response, data, textStatus, jqXHR) {
    response($.map(data, function (item) {
            return {
                // default autocomplete object
                label: item.CIF + " - " + item.Name,
                value: item.Name,

                // custom object binding
                data: {
                    CIF: item.CIF,
                    Name: item.Name,
                    POAName: item.POAName,
                    Accounts: item.Accounts,
                    BizSegment: item.BizSegment,
                    Branch: item.Branch,
                    Contacts: item.Contacts,
                    Functions: item.Functions,
                    RM: item.RM,
                    Type: item.Type,
                    Underlyings: item.Underlyings,
                    LastModifiedBy: item.LastModifiedBy,
                    LastModifiedDate: item.LastModifiedDate
                }
            }
        })
    );
}

// Token validation On Success function
function OnSuccessToken(data, textStatus, jqXHR){
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // read spuser from cookie
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);

        viewModel.SPUser(ko.mapping.toJS(spUser));
    }

    // get parameter values
    GetParameters();
}

// Upload the file
function UploadFile(context, document, callBack) {

    // Define the folder path for this example.
    var serverRelativeUrlToFolder = '/Instruction Documents';

    // Get test values from the file input and text input page controls.
    //var fileInput = $('#document-path');

    // Get the file name from the file input control on the page.
    //var parts = fileInput[0].value.split('\\');
    //var parts = fileInput.split('\\');
    //var fileName = parts[parts.length - 1];
    //var parts = viewModel.DocumentPath.name.split('.');
    var parts = document.DocumentPath.name.split('.');
    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp +"."+ fileExtension; //document.DocumentPath.name;

    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBuffer();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolder(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                // Change the display name and title of the list item.
                var changeItem = updateListItem(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    //alert('file uploaded and updated');
                    //return output;
                    var kodok = {
                        ID: 0,
                        Type: document.Type,
                        Purpose: document.Purpose,
                        LastModifiedDate: null,
                        LastModifiedBy: null,
                        DocumentPath: output.ServerRelativeUrl,
                        FileName: document.DocumentPath.name
                    };
                    viewModel.TransactionModel().Documents.push(kodok);

                    callBack();
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBuffer() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }
        //bangkit
        reader.readAsArrayBuffer(document.DocumentPath);
        //reader.readAsDataURL(document.DocumentPath);
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(arrayBuffer) {

        // Get the file name from the file input control on the page.
        //var parts = fileInput[0].value.split('\\');
        //var fileName = parts[parts.length - 1];

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                //"content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItem(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItem(itemMetadata) {

        // Define the list item changes. Use the FileLeafRef property to change the display name.
        // For simplicity, also use the name as the title.
        // The example gets the list item type from the item's metadata, but you can also get it from the
        // ListItemEntityTypeFullName property of the list.
        //var body = String.format("{{'__metadata':{{'type':'{0}'}},'FileLeafRef':'{1}','Title':'{2}'}}", itemMetadata.type, fileName, fileName);
        var body = {
            Title: document.DocumentPath.name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: document.Type.Name,
            Document_x0020_Purpose: document.Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}

// AJAX On Error Callback
function OnError(jqXHR, textStatus, errorThrown){
    ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

// AJAX On Alway Callback
function OnAlways(){
    //$box.trigger('reloaded.ace.widget');

    // enabling form input controls
    viewModel.IsEditable(true);
}