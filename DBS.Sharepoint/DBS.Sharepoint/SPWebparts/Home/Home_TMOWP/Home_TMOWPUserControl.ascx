﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Home_TMOWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.Home.Home_TMOWP.Home_TMOWPUserControl" %>

<style>
    #transaction-data > .row > .col-xs-10 {
        margin-left: 60px;
    }
</style>
<link rel="stylesheet" href="/_catalogs/masterpage/Ace/assets/css/bootstrap-timepicker.css" />
<h1 class="header smaller no-margin-top lighter dark">TMO Home</h1>

<div id="home-transaction-tmo">

    <!-- widget box start -->
    <div id="widget-box" class="widget-box">
        <!-- widget header start -->
        <div class="widget-header widget-hea1der-small header-color-dark">
            <h6>Incoming Tasks</h6>

            <div class="widget-toolbar">
                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                    <i class="blue icon-filter"></i>
                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridProperties().AllowFilter" />
                    <span class="lbl"></span>
                </label>
                <a href="#" data-bind="click: GetData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                    <i class="blue icon-refresh"></i>
                </a>
            </div>
        </div>
        <!-- widget header end -->
        <!-- widget body start -->
        <div class="widget-body">
            <!-- widget main start -->
            <div class="widget-main padding-0">
                <!-- widget slim control start -->
                <div class="slim-scroll" data-height="400">
                    <!-- widget content start -->
                    <div class="content">

                        <!-- table responsive start -->
                        <div class="table-responsive" id="form-menu-custom" form="Home-TMO">
                            <div class="dataTables_wrapper" role="grid">
                                <table id="workflow-task-table" class="table table-striped table-bordered table-hover dataTable"
                                       workflow-state="running"
                                       workflow-outcome="pending"
                                       workflow-custom-outcome=""
                                       workflow-show-active-task="true"
                                       workflow-show-contribute="false">
                                    <thead>
                                        <tr data-bind="with: GridProperties">
                                            <th>No.</th>
                                            <th class="center">Action</th>  <%--<th class="center" data-bind="visible: $root.IsTMOChecker">Action</th>  --%>                                         
                                            <th data-bind="click: function () { Sorting('ApplicationID') }, css: GetSortedColumn('ApplicationID')">Application ID</th>
                                            <th data-bind="click: function () { Sorting('Customer') }, css: GetSortedColumn('Customer')">Customer Name</th>
                                            <th data-bind="click: function () { Sorting('CIF') }, css: GetSortedColumn('CIF')">CIF</th>
                                            <th data-bind="click: function () { Sorting('Product') }, css: GetSortedColumn('Product')">Product</th>
                                            <th data-bind="click: function () { Sorting('Channel') }, css: GetSortedColumn('Channel')">Channel</th>
                                            <th data-bind="click: function () { Sorting('LastModifiedBy') }, css: GetSortedColumn('LastModifiedBy')">Modified By</th>
                                            <th data-bind="click: function () { Sorting('LastModifiedDate') }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
                                        </tr>
                                    </thead>
                                    <thead data-bind="visible: GridProperties().AllowFilter" class="table-filter">
                                        <tr>
                                            <th class="clear-filter">
                                                <a href="#" data-bind="click: ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                    <i class="green icon-trash"></i>
                                                </a>
                                            </th>
                                            <th></th>
                                            <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterApplicationID, event: { change: GridProperties().Filter }" /></th>
                                            <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCustomer, event: { change: GridProperties().Filter }" /></th>
                                            <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCIF, event: { change: GridProperties().Filter }" /></th>
                                            <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterProduct, event: { change: GridProperties().Filter }" /></th>
                                            <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterChannel, event: { change: GridProperties().Filter }" /></th>
                                            <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterUserApprover, event: { change: GridProperties().Filter }" /></th>
                                            <th><input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterEntryTime, event: { change: GridProperties().Filter }" /></th>
                                        </tr>
                                    </thead>
                                    <tbody data-bind="foreach: Tasks">
                                        <tr data-bind="click: $root.GetSelectedRow, css: $root.SetColorStatus(WorkflowContext.StateID)" data-toggle="modal">
                                            <td><span data-bind="text: RowID"></span></td>
                                            <td style="text-align:center; white-space: nowrap">
                                                <div class="btnContainer" data-bind="visible: WorkflowContext.WorkflowName.toLowerCase() =='tmo product workflow'">       
                                                    <!-- dynamic outcomes button start -->
                                                        <span data-bind="foreach: $root.HomeOutcomesRow, attr: { id: 'homeApprovalContainer' + $index() }, visible: $root.IsAuthorizedNintexHome()">
                                                            <button type="button" data-bind="click: $root.HomeApprovalProcess, enable: $root.IsPendingNintexHome(), stopBubble: 'click', attr: { id: 'btnHomeApproval' + $parentContext.$index() + $index(), SPTaskListID: '', SPTaskListItemID: 0, TaskTypeID: 0, ActivityTitle: '', WorkflowInstanceID: '', ApproverID: 0, OutcomeID: 0 }"  class="btn btn-xs btn-primary">
                                                                <i class="icon-approve"></i>
                                                                <span class=""></span>
                                                            </button>
                                                        </span>
                                                        <!-- dynamic outcomes button end -->
                                                    <!-- dynamic outcomes button end -->
                                                </div>
                                            </td>
                                            <td style="white-space: nowrap"><span data-bind="text: Transaction.ApplicationID"></span></td>
                                            <td><span data-bind="text: Transaction.Customer"></span></td>
                                            <td><span data-bind="text: Transaction.CIF"></span></td>
                                            <td><span data-bind="text: Transaction.Product"></span></td>                                            
                                            <td><span data-bind="text: Transaction.Channel"></span></td>
                                            <td><span data-bind="text: Transaction.LastModifiedBy"></span></td>
                                            <td><span data-bind="text: $root.LocalDate(Transaction.LastModifiedDate)"></span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- table responsive end -->
                    </div>
                    <!-- widget content end -->
                </div>
                <!-- widget slim control end -->
                <!-- widget footer start -->
                <div class="widget-toolbox padding-8 clearfix">
                    <div class="row" data-bind="with: GridProperties">
                        <!-- pagination size start -->
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                Showing <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                rows of <span data-bind="text: Total"></span>
                                entries
                            </div>
                        </div>
                        <!-- pagination size end -->
                        <!-- pagination page jump start -->
                        <div class="col-sm-3">
                            <div class="dataTables_paginate paging_bootstrap">
                                Page <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width:50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                of <span data-bind="text: TotalPages"></span>
                            </div>
                        </div>
                        <!-- pagination page jump end -->
                        <!-- pagination navigation start -->
                        <div class="col-sm-3">
                            <div class="dataTables_paginate paging_bootstrap">
                                <ul class="pagination">
                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                            <i class="icon-double-angle-left"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                            <i class="icon-angle-left"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                            <i class="icon-angle-right"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                            <i class="icon-double-angle-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- pagination navigation end -->

                    </div>
                </div>
                <!-- widget footer end -->

            </div>
            <!-- widget main end -->
        </div>
        <!-- widget body end -->
    </div>
    <!-- widget box end -->
    <!-- modal form start -->
    <div id="modal-form" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width:100%;">
            <div id="backDrop" class="absCustomBackDrop topCustomBackDrop darkCustomBackDrop hideCustomBackDrop fullHeightCustomBackDrop fullWidthCustomBackDrop centerCustomBackDrop"></div>
            <div class="modal-content">
                <!-- modal header start
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="blue bigger">
                        <span data-bind="text: $root.Title"></span>
                    </h4>
                </div> -->
                <div class="alert alert-danger" data-bind="visible: !$root.IsAuthorizedNintex()">
                    <!-- <button type="button" class="close" data-dismiss="alert">
                        <i class="icon-remove"></i>
                    </button> -->
                    <strong>
                        <i class="icon-remove"></i>
                        Unauthorized!
                    </strong>
                    <span data-bind="text: $root.MessageNintex"></span>
                </div>
                <!-- modal header end -->
                <!-- modal body start -->
                <div class="modal-body overflow-visible">
                    <button type="button" class="bootbox-close-button close" data-dismiss="modal" style="margin-top: -10px;font-size:30px;margin-right: -8px;" data-bind:"$root.OnCloseApproval">×</button>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div class="tabbable">
                                <ul class="nav nav-tabs" id="myTab">
                                    <li class="active">
                                        <a data-toggle="tab" href="#transaction">
                                            <i class="blue icon-credit-card bigger-110"></i>
                                            <span data-bind="text: ActivityTitle"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#history">
                                            <i class="blue icon-comments bigger-110"></i>
                                            History
                                            <!--<span class="badge badge-danger">4</span>-->
                                        </a>
                                    </li>
                                    <!--<li>
                                        <a data-toggle="tab" href="#context">
                                            <i class="blue icon-cogs bigger-110"></i>
                                            Context
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#ko-model">
                                            <i class="blue icon-cogs bigger-110"></i>
                                            Knockout Model
                                        </a>
                                    </li>-->
                                </ul>

                                <div class="tab-content">
                                    <div id="transaction" class="tab-pane in active">
                                        <div id="transaction-progress" class="col-sm-4 col-sm-offset-4" style="display: none">
                                            <h4 class="smaller blue">Please wait...</h4>
                                            <span class="space-4"></span>
                                            <div class="progress progress-small progress-striped active">
                                                <div class="progress-bar progress-bar-info" style="width:100%"></div>
                                            </div>
                                        </div>

                                        <div id="transaction-data" data-bind="template: { name: ApprovalTemplate(), data: ApprovalData() }"></div>
                                    </div>

                                    <div id="history" class="tab-pane">
                                        <div id="timeline-2" data-bind="with: ApprovalData()">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                                                    <div class="timeline-container timeline-style2">
                                                        <!--<span class="timeline-label">
                                                            <b>Today</b>
                                                        </span>-->

                                                        <div data-bind="if: $root.IsTimelines">

                                                            <div class="timeline-items" data-bind="foreach: Timelines">
                                                                <div class="timeline-item clearfix">
                                                                    <div class="timeline-info">
                                                                        <span class="timeline-date" data-bind="text: $root.LocalDate(Time)"></span>

                                                                        <i class="timeline-indicator btn btn-info no-hover"></i>
                                                                    </div>

                                                                    <div class="widget-box transparent">
                                                                        <div class="widget-body">
                                                                            <div class="widget-main no-padding">
                                                                                <span class="bigger-110">
                                                                                    <span class="blue bolder" data-bind="text: DisplayName"></span>
                                                                                    <!-- ko if: IsGroup -->
                                                                                    (<span data-bind="text: UserOrGroup"></span>)
                                                                                    <!-- /ko -->
                                                                                </span>

                                                                                <span class="bolder" data-bind="text: Outcome"></span>
                                                                                <span data-bind="text: Activity"></span>

                                                                                <!-- ko ifnot: ApproverID == 0 -->
                                                                                <br />
                                                                                <i class="icon-comments-alt bigger-110 blue"></i>
                                                                                <span data-bind="text: Comment"></span>
                                                                                <!-- /ko -->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="context" class="tab-pane">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Parameter</th>
                                                    <th>Value</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Web ID</td>
                                                    <td><span data-bind="text: SPWebID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Site ID</td>
                                                    <td><span data-bind="text: SPSiteID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>List ID</td>
                                                    <td><span data-bind="text: SPListID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>List Item ID</td>
                                                    <td><span data-bind="text: SPListItemID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Task List ID</td>
                                                    <td><span data-bind="text: SPTaskListID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Task List Item ID</td>
                                                    <td><span data-bind="text: SPTaskListItemID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Initiator</td>
                                                    <td><span data-bind="text: Initiator"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Workflow Instance ID</td>
                                                    <td><span data-bind="text: WorkflowInstanceID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Workflow ID</td>
                                                    <td><span data-bind="text: WorkflowID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Workflow Name</td>
                                                    <td><span data-bind="text: WorkflowName"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Start Time</td>
                                                    <td><span data-bind="text: LocalDate(StartTime())"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>State ID </td>
                                                    <td><span data-bind="text: StateID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>State Description</td>
                                                    <td><span data-bind="text: StateDescription"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Is Authorized</td>
                                                    <td><span data-bind="text: IsAuthorized"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Task Type</td>
                                                    <td><span data-bind="text: TaskTypeDescription"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Title</td>
                                                    <td><span data-bind="text: Title"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Activity Title</td>
                                                    <td><span data-bind="text: ActivityTitle"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>User</td>
                                                    <td><span data-bind="text: User"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Is Sharepoint Group</td>
                                                    <td><span data-bind="text: IsSPGroup"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Entry Time</td>
                                                    <td><span data-bind="text: LocalDate(EntryTime())"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Exit Time</td>
                                                    <td><span data-bind="text: LocalDate(ExitTime())"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Outcome ID</td>
                                                    <td><span data-bind="text: OutcomeID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Outcome Description</td>
                                                    <td><span data-bind="text: OutcomeDescription"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Custom Outcome ID</td>
                                                    <td><span data-bind="text: CustomOutcomeID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Custom Outcom Description</td>
                                                    <td><span data-bind="text: CustomOutcomeDescription"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Comments</td>
                                                    <td><span data-bind="text: Comments"></span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div id="ko-model" class="tab-pane">
                                        <pre data-bind="text: ko.toJSON($root.ApprovalData, null, 2)"></pre>
                                        <!--<pre data-bind="text: JSON.stringify(ko.toJS(ApprovalData), null, 2)"></pre>-->
                                    </div>
                                </div>
                            </div>
                            <!-- modal body form end -->
                        </div>
                    </div>
                </div>
                <!-- modal body end -->
                <!-- modal footer start -->
                <div class="modal-footer">
                    <div class="row" data-bind="visible: $root.IsAuthorizedNintex()">
                        <div class="form-group">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="workflow-comments">Comments</label>

                            <div class="col-xs-9 align-left">
                                <div class="clearfix">
                                    <textarea class="col-lg-12" id="workflow-comments" name="workflow-comments" rows="4" data-bind="value: $root.Comments, enable: $root.IsPendingNintex()" placeholder="Type any comment here..."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="space-8"></div>

                    <!-- dynamic outcomes button start -->
                    <span data-bind="foreach: Outcomes, visible: $root.IsAuthorizedNintex()">
                        <button data-bind="click: $root.ApprovalProcess, enable: $root.IsPendingNintex()" class="btn btn-sm btn-primary">
                            <i class="icon-approve"></i>
                            <span data-bind="text: $root.ApprovalButton(Name)"></span>
                        </button>
                    </span>
                    <!-- dynamic outcomes button end -->

                    <button class="btn btn-sm btn-success" data-dismiss="modal" data-bind="click: $root.OnCloseApproval">
                        <i class="icon-remove"></i>
                        Close
                    </button>
                </div>
                <!-- modal footer end -->

            </div>
        </div>
    </div>
    <!-- modal form end -->
    
        <!-- modal form start Underlying Form -->
    <div id="modal-form-Underlying" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
                <!-- modal header start Underlying Form -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span data-bind="if: $root.IsNewDataUnderlying()">Create a new Customer Underlying Parameter</span>
                        <span data-bind="if: !$root.IsNewDataUnderlying()">Modify a Customer Underlying Parameter</span>
                    </h4>
                </div>
                <!-- modal header end -->
                <!-- modal body start -->
                <div class="modal-body overflow-visible">
                    <div id="modalUnderlyingtest">
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- modal body form start -->

                                <div id="customerUnderlyingform" class="form-horizontal" role="form">
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="JointAccountID">
                                            Joint Account</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="book-joinAcc" name="book-statement" data-bind="value: $root.IsJointAccount_u,
    options: $root.CIFJointAccounts,
    optionsText: 'Name',
    optionsValue: 'ID' "
                                                    class="col-xs-3" disabled="disabled">
                                                </select>&nbsp;
                                                <input type="text" id="accountNumber" name="documentType" data-bind="value: $root.AccountNumber_u, visible: $root.IsJointAccount_u" class="col-xs-3" disabled="disabled" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="StatementLetterID">
                                            Statement Letter
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="statementLetter" name="statementLetter" data-bind="value:$root.StatementLetter_u().ID, options: $root.ddlStatementLetter_u, optionsText: 'Name',optionsValue: 'ID',optionsCaption: 'Please Select...',enable:$root.IsStatementA(),event:{change:OnChangeStatementLetterUnderlying}" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger" for="StatementLetter_u">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="UnderlyingDocument">
                                            Underlying Document
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="underlyingDocument" name="underlyingDocument" data-bind="value:UnderlyingDocument_u().ID, options: $root.ddlUnderlyingDocument_u, optionsText: 'CodeName',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger" for="UnderlyingDocument_u">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <!--    						<span aa-bind="text:UnderlyingDocument_u().Code()"></span>-->
                                    <div data-bind="visible:UnderlyingDocument_u().ID()=='50'" class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="otherUnderlying">
                                            Other Underlying
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="otherUnderlying" name="otherUnderlying" data-bind="value: $root.OtherUnderlying_u" class="col-xs-7" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="DocumentType">
                                            Type of Document
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="documentType" name="documentType" data-bind="value:$root.DocumentType_u().ID, options: $root.ddlDocumentType_u, optionsText: 'Name',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger" for="DocumentType_u">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Currency">
                                            Transaction Currency
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="currency_u" name="currency_u" data-bind="value:$root.Currency_u().ID,disable:$root.StatementLetter_u().ID()==1, options: $root.ddlCurrency_u, optionsText:'Code',optionsValue: 'ID',optionsCaption: 'Please Select...',event: {change:function(data){OnCurrencyChange(Currency_u().ID())}}" data-rule-required="true" data-rule-value="true" class="col-xs-2"></select>
                                                <label class="control-label bolder text-danger" for="Currency_u">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Amount">
                                            Invoice Amount
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="Amount_u" name="Amount_u" data-bind="value: $root.Amount_u(),disable:$root.StatementLetter_u().ID()==1,valueUpdate:['afterkeydown','propertychange','input']" onkeyup="read_u();" data-rule-required="true" data-rule-number="true" class="col-xs-4 align-right" />
                                                <label class="control-label bolder text-danger" for="Amount_u">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Rate">Rate</label>

                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" class="col-sm-4 align-right" name="rate_u" id="rate_u" disabled="disabled" data-rule-required="true" data-rule-number="true" data-bind="value: formatNumber($root.Rate_u())" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Eqv.USD">Eqv. USD</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" class="col-sm-4 align-right" disabled="disabled" name="eqv-usd_u" id="eqv-usd_u" data-rule-required="true" data-rule-number="true" data-bind="value: formatNumber($root.AmountUSD_u())" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="DateOfUnderlying">
                                            Date of Underlying
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <input type="text" autocomplete="off" id="DateOfUnderlying" data-date-format="dd-M-yyyy" name="DateOfUnderlying_u" data-bind="value:DateOfUnderlying_u, event: { change:$root.onChangeDateOfUnderlying}" class="form-control date-picker col-xs-6" data-rule-required="true" />
                                                <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                <label class="control-label bolder text-danger starremove" for="DateOfUnderlying_u" style="position: absolute;">*</label>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label no-padding-right" for="ExpiredDate">
                                            Expiry Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <input type="text" autocomplete="off" id="ExpiredDate" data-date-format="dd-M-yyyy" name="ExpiredDate" data-bind="value:ExpiredDate_u" class="form-control date-picker col-xs-6" data-rule-required="true" />
                                                <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                <label class="control-label bolder text-danger starremove" for="ExpiredDate_u" style="position: absolute;">*</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="space-2"></div>
                                    <div class="form-group" data-bind="visible:false">
                                        <label class="col-sm-3 control-label no-padding-right" for="IsDeclarationOfException">
                                            Declaration of Exception
                                        </label>
                                        <div class="col-sm-9">
                                            <label>
                                                <input name="switch-field-1" id="IsDeclarationOfException" data-bind="checked: $root.IsDeclarationOfException_u" class="ace ace-switch ace-switch-6" type="checkbox" />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="space-2" data-bind="visible:false"></div>
                                    <div class="form-group" data-bind="visible:false">
                                        <label class="col-sm-3 control-label no-padding-right" for="StartDate">
                                            Start Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <input id="StartDate" name="StartDate" data-date-format="dd-M-yyyy" data-bind="value:StartDate_u" class="form-control date-picker col-xs-6" type="text" autocomplete="off" />
                                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label no-padding-right" for="EndDate">
                                            End Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <input type="text" autocomplete="off" id="EndDate" name="EndDate" data-date-format="dd-M-yyyy" data-bind="value:EndDate_u" class="form-control date-picker col-xs-6" />
                                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="ReferenceNumber">
                                            Reference Number
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="ReferenceNumber" name="ReferenceNumber" data-bind="value: $root.ReferenceNumber_u" disabled="disabled" class="col-xs-9" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="SupplierName">
                                            Supplier Name
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="SupplierName" name="SupplierName" data-bind="value: $root.SupplierName_u" class="col-xs-8" data-rule-required="true" />
                                                <label class="control-label bolder text-danger" for="SupplierName_u">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="InvoiceNumber">
                                            Invoice Number
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="invoiceNumber" name="invoiceNumber" data-bind="value: $root.InvoiceNumber_u" class="col-xs-8" data-rule-required="true" />
                                                <label class="control-label bolder text-danger" for="InvoiceNumber_u">*</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="space-2"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="AttachmentNo">
                                            Is This doc Purposed to replace the proforma doc
                                        </label>
                                        <div class="col-sm-9">
                                            <label>
                                                <input name="switch-field-1" id="IsProforma" data-bind="checked:IsProforma_u,disable:DocumentType_u().ID()==2" class="ace ace-switch ace-switch-6" type="checkbox" />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <!-- grid proforma begin -->
                                    <!-- widget box start -->
                                    <div id="widget-box" class="widget-box" data-bind="visible:IsProforma_u()">
                                        <!-- widget header start -->
                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                            <h6>Customer Underlying Proforma Table</h6>

                                            <div class="widget-toolbar">
                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                    <i class="blue icon-filter"></i>
                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingProformaGridProperties().AllowFilter" />
                                                    <span class="lbl"></span>
                                                </label>
                                                <a href="#" data-bind="click: GetDataUnderlyingProforma" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                    <i class="blue icon-refresh"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- widget header end -->
                                        <!-- widget body start -->
                                        <div class="widget-body">
                                            <!-- widget main start -->
                                            <div class="widget-main padding-0">
                                                <!-- widget slim control start -->
                                                <div class="slim-scroll" data-height="400">
                                                    <!-- widget content start -->
                                                    <div class="content">

                                                        <!-- table responsive start -->
                                                        <div class="table-responsive">
                                                            <div class="dataTables_wrapper" role="grid">
                                                                <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                                    <thead>
                                                                        <tr data-bind="with: UnderlyingProformaGridProperties">
                                                                            <th style="width: 50px">No.</th>
                                                                            <th style="width: 50px">Select</th>
                                                                            <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                            <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                            <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                            <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                            <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                            <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                            <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th>
                                                                            <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                            <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <thead data-bind="visible: UnderlyingProformaGridProperties().AllowFilter" class="table-filter">
                                                                        <tr>
                                                                            <th class="clear-filter">
                                                                                <a href="#" data-bind="click: ProformaClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                    <i class="green icon-trash"></i>
                                                                                </a>
                                                                            </th>
                                                                            <th class="clear-filter"></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterStatementLetter, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterUnderlyingDocument, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterDocumentType, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterCurrency, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterAmount, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterSupplierName, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterReferenceNumber, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.ProformaFilterDateOfUnderlying, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.ProformaFilterExpiredDate, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody data-bind="foreach: {data: CustomerUnderlyingProformas, as: 'ProformasData'}, visible: CustomerUnderlyingProformas().length > 0">
                                                                        <tr>
                                                                            <td><span data-bind="text: RowID"></span></td>
                                                                            <td align="center">
                                                                                <input type="checkbox" id="isSelectedProforma" name="isSelect" data-bind="checked:ProformasData.IsSelectedProforma, event: {change:function(data){$root.onSelectionProforma($index(),$data)}}" /></td>
                                                                            <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                            <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                            <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                            <td><span data-bind="text: Currency.Code"></span></td>
                                                                            <td><span data-bind="text: Amount"></span></td>
                                                                            <td><span data-bind="text: SupplierName"></span></td>
                                                                            <td><span data-bind="text: ReferenceNumber"></span></td>
                                                                            <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                                            <td><span data-bind="text: $root.LocalDate(ExpiredDate,true,false)"></span></td>
                                                                        </tr>
                                                                    </tbody>
                                                                    <tbody data-bind="visible: CustomerUnderlyingProformas().length == 0">
                                                                        <tr>
                                                                            <td colspan="11" class="text-center">No entries available.
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <!-- table responsive end -->
                                                    </div>
                                                    <!-- widget content end -->
                                                </div>
                                                <!-- widget slim control end -->
                                                <!-- widget footer start -->
                                                <div class="widget-toolbox padding-8 clearfix">
                                                    <div class="row" data-bind="with: UnderlyingProformaGridProperties">
                                                        <!-- pagination size start -->
                                                        <div class="col-sm-6">
                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                Showing
                                                            <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                rows of <span data-bind="text: Total"></span>
                                                                entries
                                                            </div>
                                                        </div>
                                                        <!-- pagination size end -->
                                                        <!-- pagination page jump start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                Page
                                                            <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                of <span data-bind="text: TotalPages"></span>
                                                            </div>
                                                        </div>
                                                        <!-- pagination page jump end -->
                                                        <!-- pagination navigation start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                <ul class="pagination">
                                                                    <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                            <i class="icon-double-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                            <i class="icon-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                            <i class="icon-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                            <i class="icon-double-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <!-- pagination navigation end -->

                                                    </div>
                                                </div>
                                                <!-- widget footer end -->

                                            </div>
                                            <!-- widget main end -->
                                        </div>
                                        <!-- widget body end -->

                                    </div>
                                    <!-- widget box end -->
                                    <!-- grid proforma end -->


                                    <div class="space-2"></div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="IsBulkUnderlying">
                                            Is This Bulk Underlying
                                        </label>
                                        <div class="col-sm-9">
                                            <label>
                                                <input name="switch-field-1" id="IsBulkUnderlying" data-bind="checked:IsBulkUnderlying_u" class="ace ace-switch ace-switch-6" type="checkbox" />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <!-- grid Bulk begin -->
                                    <!-- widget box start -->
                                    <div class="widget-box" data-bind="visible:IsBulkUnderlying_u()">
                                        <!-- widget header start -->
                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                            <h6>Customer Bulk Underlying Table</h6>

                                            <div class="widget-toolbar">
                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                    <i class="blue icon-filter"></i>
                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingBulkGridProperties().AllowFilter" />
                                                    <span class="lbl"></span>
                                                </label>
                                                <a href="#" data-bind="click: GetDataBulkUnderlying" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                    <i class="blue icon-refresh"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- widget header end -->
                                        <!-- widget body start -->
                                        <div class="widget-body">
                                            <!-- widget main start -->
                                            <div class="widget-main padding-0">
                                                <!-- widget slim control start -->
                                                <div class="slim-scroll" data-height="400">
                                                    <!-- widget content start -->
                                                    <div class="content">

                                                        <!-- table responsive start -->
                                                        <div class="table-responsive">
                                                            <div class="dataTables_wrapper" role="grid">
                                                                <table id="Customer-BulkUnderlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                                    <thead>
                                                                        <tr data-bind="with: UnderlyingBulkGridProperties">
                                                                            <th style="width: 50px">No.</th>
                                                                            <th style="width: 50px">Select</th>
                                                                            <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                            <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                            <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                            <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                            <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                            <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                            <th data-bind="click: function () { Sorting('InvoiceNumber'); }, css: GetSortedColumn('InvoiceNumber')">Invoice Number</th>
                                                                            <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                            <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <thead data-bind="visible: UnderlyingBulkGridProperties().AllowFilter" class="table-filter">
                                                                        <tr>
                                                                            <th class="clear-filter">
                                                                                <a href="#" data-bind="click: BulkClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                    <i class="green icon-trash"></i>
                                                                                </a>
                                                                            </th>
                                                                            <th class="clear-filter"></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterStatementLetter, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterUnderlyingDocument, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterDocumentType, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterCurrency, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterSupplierName, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterAmount, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterInvoiceNumber, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.BulkFilterDateOfUnderlying, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.BulkFilterExpiredDate, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody data-bind="foreach: {data: CustomerBulkUnderlyings, as: 'BulkDatas'}, visible: CustomerBulkUnderlyings().length > 0">
                                                                        <tr>
                                                                            <td><span data-bind="text: RowID"></span></td>
                                                                            <td align="center">
                                                                                <input type="checkbox" id="isSelectedBulk" name="isSelect" data-bind="checked:BulkDatas.IsSelectedBulk, event: {change:function(data){$root.onSelectionBulk($index(),$data)}}" /></td>
                                                                            <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                            <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                            <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                            <td><span data-bind="text: Currency.Code"></span></td>
                                                                            <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                                                            <td><span data-bind="text: SupplierName"></span></td>
                                                                            <td><span data-bind="text: InvoiceNumber"></span></td>
                                                                            <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                                            <td><span data-bind="text: $root.LocalDate(ExpiredDate,true,false)"></span></td>
                                                                        </tr>
                                                                    </tbody>
                                                                    <tbody data-bind="visible: CustomerBulkUnderlyings().length == 0">
                                                                        <tr>
                                                                            <td colspan="11" class="text-center">No entries available.
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <!-- table responsive end -->
                                                    </div>
                                                    <!-- widget content end -->
                                                </div>
                                                <!-- widget slim control end -->
                                                <!-- widget footer start -->
                                                <div class="widget-toolbox padding-8 clearfix">
                                                    <div class="row" data-bind="with: UnderlyingBulkGridProperties">
                                                        <!-- pagination size start -->
                                                        <div class="col-sm-6">
                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                Showing
                                                            <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                rows of <span data-bind="text: Total"></span>
                                                                entries
                                                            </div>
                                                        </div>
                                                        <!-- pagination size end -->
                                                        <!-- pagination page jump start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                Page
                                                            <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                of <span data-bind="text: TotalPages"></span>
                                                            </div>
                                                        </div>
                                                        <!-- pagination page jump end -->
                                                        <!-- pagination navigation start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                <ul class="pagination">
                                                                    <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                            <i class="icon-double-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                            <i class="icon-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                            <i class="icon-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                            <i class="icon-double-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <!-- pagination navigation end -->

                                                    </div>
                                                </div>
                                                <!-- widget footer end -->

                                            </div>
                                            <!-- widget main end -->
                                        </div>
                                        <!-- widget body end -->

                                    </div>
                                    <!-- widget box end -->
                                    <!-- grid Bulk end -->

                                </div>

                                <!-- modal body form end -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- modal body end -->
                <!-- modal footer start -->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.save_u, visible: $root.IsNewDataUnderlying(), disable:!$root.IsEditTableUnderlying()">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.update_u, visible: !$root.IsNewDataUnderlying() && !$root.IsUtilize_u()">
                        <i class="icon-edit"></i>
                        Update
                    </button>
                    <button class="btn btn-sm btn-warning" data-bind="click: $root.delete_u, visible: !$root.IsNewDataUnderlying() && !$root.IsUtilize_u()">
                        <i class="icon-trash"></i>
                        Delete
                    </button>
                    <button class="btn btn-sm" data-bind="click: $root.cancel_u" data-dismiss="modal">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
                <!-- modal footer end -->

            </div>
        </div>
    </div>
    <!-- modal form end Underlying Form -->
    <!-- modal form callbacktime -->
    <div id="modal-form-callbacktime" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="0" role="dialog">
        <div class="modal-dialog" style="width: 50%">
            <div class="modal-content">

                <!-- modal header start -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span>Call Back Time</span>
                    </h4>
                </div>
                <!-- modal header end -->
                <!-- modal body start -->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div id="customer-form" class="form-horizontal" role="form">

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="NameCallback">
                                        Contact Name
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="NameCallback" name="NameCallback" class="col-xs-10 col-sm-5" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="TimePickerCallBack">
                                        Call back time
                                    </label>
                                    <div class="col-sm-4">
                                        <div class="clearfix">
                                            <div class="input-group bootstrap-timepicker">
                                                <input type="text" id="TimePickerCallBack" class="form-control time-picker" data-bind="timepicker: true" data-rule-required="true" data-rule-value="true" />
                                                <span class="input-group-addon">
                                                    <i class="icon-time bigger-110"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="IsUtc">
                                        Is UTC
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input id="IsUtc" class="ace ace-switch ace-switch-6" type="checkbox" />
                                            <span class="lbl"></span>
                                        </div>
                                    </div>
                                </div>

                                <!--<div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Remarks
                                    </label>

                                    <div class="col-sm-9">
                                        <textarea name="RemarksCallBack" id="remarks" class="col-sm-12" rows="5"></textarea>
                                    </div>
                                </div>-->

                            </div>
                            <!-- modal body form end -->
                        </div>
                    </div>
                </div>
                <!-- modal body end -->
                <!-- modal footer start -->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.SaveCallBackTime">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-bind="click: $root.CloseCallBackTime">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
                <!-- modal footer end -->

            </div>
        </div>
    </div>
    <!-- modal form callbacktime end-->
    <!-- modal form start Attach Document Underlying Form start -->
    <div id="modal-form-Attach" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
                <!-- modal header start Attach FIle Form -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span>Added Attachment File</span>
                    </h4>
                </div>
                <!-- modal header end Attach file -->
                <!-- modal body start Attach File -->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div id="customer-form1" class="form-horizontal" role="form">
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="Name">
                                        Purpose of Doc
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentPurpose" name="documentPurpose" data-bind="value:$root.DocumentPurpose_a().ID, options: ddlDocumentPurpose_a, optionsText: 'Name',optionsValue: 'ID',optionsCaption: 'Please Select...', event: { change: OnChangePropose }" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="description">
                                        Type of Doc
                                    </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentType1" name="documentType" data-bind="value:$root.DocumentType_a().ID, options: ddlDocumentType_a, optionsText: 'Name',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-path-upload">
                                        Document</label>

                                    <div class="col-sm-8">
                                        <div class="clearfix">
                                            <input type="file" id="document-path-upload" class="col-xs-8" name="document-path-upload" data-bind="file: DocumentPath_a" /><!---->
                                        </div>
                                    </div>
                                    <label class="control-label bolder text-danger" style="position: absolute;">*</label>
                                </div>
                            </div>
                            <!-- modal body form Attach File End -->
                            <!-- widget box Underlying Attach start -->
                            <div id="widget-box1" class="widget-box">
                                <!-- widget header Underlying Attach start -->
                                <div class="widget-header widget-hea1der-small header-color-dark">
                                    <h6>Underlying Form Table</h6>

                                    <div class="widget-toolbar">
                                        <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                            <i class="blue icon-filter"></i>
                                            <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingAttachGridProperties().AllowFilter" />
                                            <span class="lbl"></span>
                                        </label>
                                        <a href="#" data-bind="click: GetDataUnderlyingAttach" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                            <i class="blue icon-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                                <!-- widget header end -->
                                <!-- widget body underlying Attach start -->
                                <div class="widget-body">
                                    <!-- widget main start -->
                                    <div class="widget-main padding-0">
                                        <!-- widget slim control start -->
                                        <div class="slim-scroll" data-height="400">
                                            <!-- widget content start -->
                                            <div class="content">
                                                <!-- table responsive start -->
                                                <div class="table-responsive">
                                                    <div class="dataTables_wrapper" role="grid">
                                                        <table id="Customer Underlying-table1" class="table table-striped table-bordered table-hover dataTable">
                                                            <thead>
                                                                <tr data-bind="with: $root.UnderlyingAttachGridProperties">
                                                                    <!-- <th style="width:50px">Select</th> -->
                                                                    <th style="width: 50px">No.</th>
                                                                    <th style="width: 50px">Select</th>
                                                                    <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement
																			Letter</th>
                                                                    <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying
																			Document</th>
                                                                    <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type
																			of
																			Doc</th>
                                                                    <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                    <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                    <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                    <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier
																			Name</th>
                                                                    <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference
																			Number</th>
                                                                    <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry
																			Date</th>
                                                                </tr>
                                                            </thead>
                                                            <thead data-bind="visible: $root.UnderlyingAttachGridProperties().AllowFilter" class="table-filter">
                                                                <tr>
                                                                    <th class="clear-filter">
                                                                        <a href="#" data-bind="click: $root.UnderlyingAttachClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                            <i class="green icon-trash"></i>
                                                                        </a>
                                                                    </th>
                                                                    <th class="clear-filter"></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterStatementLetter, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterUnderlyingDocument, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterDocumentType, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterCurrency, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12 input-numericonly" data-bind="value: $root.UnderlyingAttachFilterAmount, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.UnderlyingAttachFilterDateOfUnderlying, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterSupplierName, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterReferenceNumber, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.UnderlyingAttachFilterExpiredDate, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody data-bind="foreach: {data: $root.CustomerAttachUnderlyings, as: 'AttachData'}, visible: $root.CustomerAttachUnderlyings().length > 0">
                                                                <tr>
                                                                    <td><span data-bind="text: RowID"></span></td>
                                                                    <td align="center">
                                                                        <input type="checkbox" id="isSelected2" name="isSelect2" data-bind="checked:IsSelectedAttach,event:{change:function(data){$root.onSelectionAttach($index(),$data)}}, enable: $root.SelectingUnderlying()" /></td>
                                                                    <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                    <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                    <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                    <td><span data-bind="text: Currency.Code"></span></td>
                                                                    <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                                                    <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                                    <td><span data-bind="text: SupplierName"></span></td>
                                                                    <td><span data-bind="text: ReferenceNumber"></span></td>
                                                                    <td><span data-bind="text: $root.LocalDate(ExpiredDate,true,false)"></span></td>
                                                                </tr>
                                                            </tbody>
                                                            <tbody data-bind="visible: $root.CustomerAttachUnderlyings().length == 0">
                                                                <tr>
                                                                    <td colspan="11" class="text-center">No
																				entries
																				available.
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- table responsive end -->
                                            </div>
                                            <!-- widget content end -->
                                        </div>
                                        <!-- widget slim control end -->
                                        <!-- widget footer start -->
                                        <div class="widget-toolbox padding-8 clearfix">
                                            <div class="row" data-bind="with: UnderlyingAttachGridProperties">
                                                <!-- pagination size start -->
                                                <div class="col-sm-6">
                                                    <div class="dataTables_paginate paging_bootstrap pull-left">
                                                        Showing
                                                        <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                        rows of <span data-bind="text: Total"></span>
                                                        entries
                                                    </div>
                                                </div>
                                                <!-- pagination size end -->
                                                <!-- pagination page jump start -->
                                                <div class="col-sm-3">
                                                    <div class="dataTables_paginate paging_bootstrap">
                                                        Page
                                                        <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                        of <span data-bind="text: TotalPages"></span>
                                                    </div>
                                                </div>
                                                <!-- pagination page jump end -->
                                                <!-- pagination navigation start -->
                                                <div class="col-sm-3">
                                                    <div class="dataTables_paginate paging_bootstrap">
                                                        <ul class="pagination">
                                                            <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                    <i class="icon-double-angle-left"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                    <i class="icon-angle-left"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                    <i class="icon-angle-right"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                    <i class="icon-double-angle-right"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- pagination navigation end -->

                                            </div>
                                        </div>
                                        <!-- widget footer end -->

                                    </div>
                                    <!-- widget main end -->
                                </div>
                                <!-- widget body end -->
                            </div>
                            <!-- widget box end -->



                        </div>
                    </div>
                </div>
                <!-- modal body end Attach File -->
                <!-- modal footer start Attach File-->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.save_a, disable: $root.IsUploading()">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.cancel_a">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
                <!-- modal footer end attach file -->

            </div>
        </div>
    </div>
    <!-- modal form end Attach Document Underlying Form end -->
    <!-- Modal upload form start -->
        <div id="modal-form-Attach1" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="0" role="dialog">
        <div class="modal-dialog" style="width: 80%">
            <div class="modal-content">
                <!-- modal header start Attach FIle Form -->
                <div class="modal-header">
                    <h5 class="blue bigger">
                        <span>Add Attachment File</span>
                    </h5>
                </div>
                <!-- modal header end Attach file -->
                <!-- modal body start Attach File -->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div id="customer-form1" class="form-horizontal" role="form">
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="Name">
                                        Purpose of Doc
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentPurpose1" name="documentPurpose" data-bind="value:$root.DocumentPurpose_a().ID, options: ddlDocumentPurpose_a, optionsText: 'Name',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="description">
                                        Type of Doc
                                    </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentType12" name="documentType" data-bind="value:$root.DocumentType_a().ID, options: ddlDocumentType_a, optionsText: 'Name',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-path-upload">
                                        Document</label>

                                    <div class="col-sm-8">
                                        <div class="clearfix">
                                            <input type="file" id="document-path-upload1" class="col-xs-8" name="document-path-upload" data-bind="file: DocumentPath_a" /><!---->
                                        </div>
                                    </div>
                                    <label class="control-label bolder text-danger" style="position: absolute;">*</label>
                                </div>
                            </div>
                            <!-- modal body form Attach File End -->

                        </div>
                    </div>
                </div>
                <!-- modal body end Attach File -->
                <!-- modal footer start Attach File-->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.save_a, disable: $root.IsUploading()">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.cancel_a">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
                <!-- modal footer end attach file -->
            </div>
        </div>
    </div>
    <!-- Modal upload form end -->

</div>

<!-- approval templates start -->

<!-- TMO Document Tracking Start-->
<script type="text/html" id="TaskTMOMaker" src="/Pages/Approval/TMOMaker.html"></script>
<script type="text/html" id="TaskTMOChecker" src="/Pages/Approval/TMOChecker.html"></script>
<script type="text/html" id="TransactionTimeline" src="/Pages/Approval/Timeline.html"></script>
<script type="text/html" id="TaskTMOCheckerCanceled" src="/Pages/Approval/TransactionCheckerCanceledTMO.html"></script>
<!-- TMO Document Traking End -->

<!-- TMO Deal templates start -->
<script type="text/html" id="TaskTrackingTMOChecker" src="/Pages/Approval/TransactionCheckerTrackingTMO.html"></script>
<script type="text/html" id="TaskTrackingTMOMaker" src="/Pages/Approval/TransactionMakerTrackingTMO.html"></script>
<script type="text/html" id="TaskTrackingTMOMakerAfterSubmit" src="/Pages/Approval/TransactionMakerTrackingTMOAfterSubmit.html"></script>
<script type="text/html" id="TaskTrackingTMORequestCancel" src="/Pages/Approval/TransactionCheckerTrackingCancel.html"></script>
<!-- TMO Deal templates End -->

<!-- TMO Netting templates start -->
<script type="text/html" id="TaskFXNettingMaker" src="/Pages/Approval/TransactionMakerNetting.html"></script>
<script type="text/html" id="TaskFXNettingChecker" src="/Pages/Approval/TransactionCheckerNetting.html"></script>
<script type="text/html" id="TaskFXNettingCheckerApproveCancellation" src="/Pages/Approval/TransactionCheckerNettingApproveCancellation.html"></script>
<!-- TMO Netting templates End -->

<!-- approval templates end -->

<!-- additional page scripts start -->
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/date-time/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/CalculatedFX.js"></script>
<script src="/SiteAssets/Scripts/HomeTMO.js"></script>
<!-- additional page scripts end -->
