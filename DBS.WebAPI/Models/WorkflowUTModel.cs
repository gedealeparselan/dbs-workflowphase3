﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Net.Http;
using System.Web.Script.Serialization;
using System.Transactions;

//=======================================================================================//
///FOR ALL DEVELOPER...
///PLEASE ADD NEW REGION BY YOUR NAME TO KEEP CLEAN !!! DONT BE A DIRTY PROGRAMMER!!!
///ANDIWIJAYA
//=======================================================================================//

namespace DBS.WebAPI.Models
{
    #region Property
    #region Andri
    public class TransactionTypeParameterUTModel
    {
        public int? TransTypeID { get; set; }

        public string TransactionTypeName { get; set; }
    }
    public class JoinAccountBranchCheckerModel
    {

        public long? ApproverID { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsVerified { get; set; }
        public bool VerifiedJoinAccount { get; set; }
        public long JoinAccountBranchCheckerID { get; set; }
        public long JoinAccountID { get; set; }
        public long TransactionID { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string CustomerName { get; set; }
        public string CIF { get; set; }
        public DateTime? CriskEfDate { get; set; }
        public int? RiskScore { get; set; }
        public DateTime? RiskProfexpDate { get; set; }
        public int JoinOrder { get; set; }
        public JoinModel Join { get; set; }

        public string SolIDJoin { get; set; }
    }

    public class TransactionMutualFundModel
    {
        public decimal? Amount { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public CurrencyModel Currency { get; set; }
        public int FundID { get; set; }
        public string FundName { get; set; }
        public string FundCode { get; set; }
        public bool? IsPartial { get; set; }
        public long MutualFundTransactionID { get; set; }
        public decimal? NumberofUnit { get; set; }
        public int? SwitchToFundID { get; set; }
        public string SwitchToFundName { get; set; }
        public string SwitchToFundCode { get; set; }
        public long TransactionID { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }

    }
    public class MutualFundCBOCheckerModel
    {
        public decimal? Amount { get; set; }
        public string Currency { get; set; }
        public long? ApproverID { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsVerified { get; set; }
        public long MutualFundTransactionCBOCheckerID { get; set; }
        public long MutualFundTransactionID { get; set; }
        public bool? partial { get; set; }
        public decimal? NumberofUnit { get; set; }//updated by dani
        public String MutualFundName { get; set; }
        public string MutualFundCode { get; set; }
        public long? TransactionID { get; set; }
        public string UpdateBy { get; set; }
        public string FromStrswitchmutualFund { get; set; }
        public string FromCodeswitchmutualfund { get; set; }
        public string TOStrswitchmutualFund { get; set; }
        public string TOCodeswitchmutualfund { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool VerifiedMutualFund { get; set; }
        public string DBNumber { get; set; }
        public string UTNumber { get; set; }
    }
    public class UTBranchCheckerDetailModel
    {
        public UTDetailModel Transaction { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public IList<VerifyModelUT> Verify { set; get; }
        public TransactionCheckerDataModel Checker { get; set; }
        public IList<MutualFundCBOCheckerModel> MutualFund { get; set; }
        public IList<JoinAccountBranchCheckerModel> JoinAccount { get; set; }
    }

    public class VerifyModelUT
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool IsVerified { get; set; }
        public bool IsVerifiedJoin { get; set; }
        public bool ISVerifiedMutualFund { get; set; }
    }
    public class UTCBOChecker
    {
        public TransactionLoanModel Transaction { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public IList<VerifyModel> Verify { set; get; }
        public TransactionCheckerDataModel Checker { get; set; }
        public IList<MutualFundCBOCheckerModel> MutualFund { get; set; }
        public IList<JoinAccountBranchCheckerModel> JoinAccount { get; set; }
    }

    #endregion
    // Agung 01.10.2015 : Add UT Detail Model
    public class UTDetailModel
    {
        /// <summary>
        /// Workflow Instance ID
        /// </summary>
        public Guid WorkflowInstanceID { get; set; }

        /// <summary>
        /// Transaction ID.
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Application ID
        /// </summary>
        public string ApplicationID { get; set; }

        /// <summary>
        /// Top Urgent
        /// </summary>
        public int? IsTopUrgent { get; set; }

        /// <summary>
        /// Customer Data Details
        /// </summary>
        public CustomerModel Customer { get; set; }

        /// <summary>
        /// New Customer
        /// </summary>
        public bool IsNewCustomer { get; set; }

        /// <summary>
        /// Product Details
        /// </summary>
        public ProductModel Product { get; set; }
        /// <summary>
        /// Product Details
        /// </summary>
        public ProductTypeModel ProductType { get; set; }

        /// <summary>
        /// Underlying Doc Details
        /// </summary>
        public UnderlyingDocModel UnderlyingDoc { get; set; }
        /// <summary>
        /// Other Underlying
        /// </summary>
        public string OtherUnderlyingDoc { get; set; }

        /// <summary>
        /// LLD Details
        /// </summary>
        public LLDModel LLD { get; set; }

        /// <summary>
        /// Currency Details
        /// </summary>
        public CurrencyModel Currency { get; set; }

        /// <summary>
        /// Transaction Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Rate
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Eqv.USD
        /// </summary>
        public decimal AmountUSD { get; set; }

        /// <summary>
        /// Channel Details
        /// </summary>
        public ChannelModel Channel { get; set; }

        /// <summary>
        /// Debit Acc Number
        /// </summary>
        public CustomerAccountModel Account { get; set; }

        /// <summary>
        /// Application Date
        /// </summary>
        public DateTime ApplicationDate { get; set; }

        /// <summary>
        /// Bene Segment Details
        /// </summary>
        public BizSegmentModel BizSegment { get; set; }

        /// <summary>
        /// Bank Details
        /// </summary>
        public BankModel Bank { get; set; }

        /// <summary>
        /// Bank Charges
        /// </summary>
        public ChargesTypeModel BankCharges { get; set; }

        /// <summary>
        /// Agent Charges
        /// </summary>
        public ChargesTypeModel AgentCharges { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        public decimal? Type { get; set; }

        /// <summary>
        /// Is Citizen
        /// </summary>
        public bool IsCitizen { get; set; }

        /// <summary>
        /// Is Resident
        /// </summary>
        public bool IsResident { get; set; }

        /// <summary>
        /// BeneACCNumber
        /// </summary>
        public string BeneAccNumber { get; set; }

        /// <summary>
        /// Payment Details
        /// </summary>
        public string PaymentDetails { get; set; }


        ///// <summary>
        ///// Lld Code
        ///// </summary>
        //public string LLDCode { get; set; }

        ///// <summary>
        ///// Lld Info
        ///// </summary>
        //public string LLDInfo { get; set; }

        /// <summary>
        /// Create Date
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
        public string strTransactiontype { get; set; }

        /// <summary>
        /// List Transaction Document Details
        /// </summary>
        public IList<TransactionDocumentModel> Documents { get; set; }

        /// <summary>
        /// Purpose detail of compliance
        /// </summary>
        public string DetailCompliance { get; set; }

        /// <summary>
        /// Bene Name
        /// </summary>
        public string BeneName { get; set; }

        /// <summary>
        /// TZ Number
        /// </summary>
        public string TZNumber { get; set; }

        /// <summary>
        /// FX Compliance
        /// </summary>
        //public FXComplianceModel FXCompliance { get; set; }

        public long? ApproverID { get; set; }

        /// <summary>
        /// Transaction Is Other Bene Bank
        /// </summary> //add reizvan
        public bool IsOtherBeneBank { get; set; }

        /// <summary>
        /// Transaction Other Bene Bank Name
        /// </summary> //add reizvan
        public string OtherBeneBankName { get; set; }

        /// <summary>
        /// Transaction Other Bene Bank Swift
        /// </summary> //add reizvan
        public string OtherBeneBankSwift { get; set; }

        public bool? IsOtherAccountNumber { get; set; }
        public string OtherAccountNumber { get; set; }
        public CurrencyModel DebitCurrency { get; set; }
        /// <summary>
        /// Debit Acc Number
        /// </summary>
        public string debitAccNumber { get; set; }
        //Andriyanto 16 nov 2015
        public string SolID { get; set; }
        public bool? IsFNACore { get; set; }
        public string strFNACore { get; set; }
        public int? FunctionType { get; set; }
        public string strfunctionType { get; set; }
        public string strAccountType { get; set; }
        public int? RiskScore { get; set; }
        public int? AccountType { get; set; }
        public DateTime? RiskProfileExpiryDate { get; set; }
        public string OperativeAccount { get; set; }
        public DateTime? CRiskEfectiveDate { get; set; }
        public VerifyModel Verify { get; set; }
        public string InvestmentID { get; set; }
        public string InvestmenName { get; set; }
        public InvestmentIDModel Investmen { get; set; }
        public TransactionTypeParameterModel Transactiontype { get; set; }
        public int? transactiontypeID { get; set; }
        public bool verifyjoinAccount { get; set; }
        //end
        //added by dani start
        public TransactionTypeParameterModel TransactionType { get; set; }
        //added by dani end
        //add by henggar
        public string DBNumber { get; set; }
        public string UTNumber { get; set; }
        //end
        //added by adi
        public string Remarks { get; set; }

        public string AttachmentRemarks { get; set; }
    }

    //end Agung
    // Agung 01.10.2015 : Add UT Maker Detail Model

    public class UTCheckerDetailModel
    {
        public UTDetailModel Transaction { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
    }
    //end Agung
    // Agung 01.10.2015 : Add UT Maker Detail Model

    //Afif
    public class UTCBOAccountMakerModel : UTDetailModel
    {
        public ParameterSystemUTModel ParameterSystemUT { get; set; }
        public string SOLID { get; set; }
        public int? RiskScore { get; set; }
        public DateTime CustomerRiskEffectiveDate { get; set; }
        public DateTime RiskProfileExpiryDate { get; set; }
        public string Remark { get; set; }
        public string InvestmentID { get; set; }
        public IList<JoinAccountModel> JoinAccounts { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }

    }

    public class UTCBOUTMakerModel : UTDetailModel
    {
        public ParameterSystemUTModel ParameterSystemUT { get; set; }
        public string SOLID { get; set; }
        public int? RiskScore { get; set; }
        public DateTime CustomerRiskEffectiveDate { get; set; }
        public DateTime RiskProfileExpiryDate { get; set; }
        public string Remark { get; set; }
        public string InvestmentID { get; set; }
        public IList<JoinAccountModel> JoinAccounts { get; set; }
        public IList<TransactionMutualFundModel> MutualFunds { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }

        public string AttacmentRemarks { get; set; }

    }

    public class ParameterSystemUTModel
    {
        public UTFNACore FNACore { get; set; }
        public UTFunctionType FunctionType { get; set; }
        public UTAccountType AccountType { get; set; }
    }

    public class UTFNACore
    {
        public int? FNACoreID { get; set; }
        public string FNACoreName { get; set; }
    }

    public class UTFunctionType
    {
        public int? FunctionTypeID { get; set; }
        public string FunctionTypeName { get; set; }
    }

    public class UTAccountType
    {
        public int? AccountTypeID { get; set; }
        public string AccountTypeName { get; set; }
    }

    //end Afif

    #region dani
    public class JoinAccountBranchMakerModel
    {
        public int? JoinOrder { get; set; }
        public CustomerModel CustomerJoin { get; set; }
        public DateTime? CustomerRiskEffectiveDateJoin { get; set; }
        public int? RiskScoreJoin { get; set; }
        public DateTime? RiskProfileExpiryDateJoin { get; set; }
        public string SolIDJoin { get; set; }
        public long? ApproverID { get; set; }
        public bool IsVerified { get; set; }
        public long TransactionID { get; set; }
        public long JoinAccountBranchCheckerID { get; set; }
        public long JoinAccountID { get; set; }
        public int Flag { get; set; } //1. add, 2 update, 3 delete
        public JoinModel Join { get; set; }
    }
    public class JoinAccountCBOUTMakerModel
    {
        public int? JoinOrder { get; set; }
        public CustomerModel CustomerJoin { get; set; }
        public DateTime? CustomerRiskEffectiveDateJoin { get; set; }
        public int? RiskScoreJoin { get; set; }
        public DateTime? RiskProfileExpiryDateJoin { get; set; }
        public string SolIDJoin { get; set; }
        public long? ApproverID { get; set; }
        public bool IsVerified { get; set; }
        public long TransactionID { get; set; }
        public long JoinAccountCBOCheckerID { get; set; }
        public long JoinAccountID { get; set; }
        public int Flag { get; set; } //1. add, 2 update, 3 delete
        public JoinModel Join { get; set; }
    }
    public class MutualFundBranchMakerModel
    {
        public CBOFundModel MutualFundList { get; set; }
        public CBOFundModel MutualFundSwitchFrom { get; set; }
        public CBOFundModel MutualFundSwitchTo { get; set; }
        public CurrencyModel MutualCurrency { get; set; }
        public decimal? MutualAmount { get; set; }
        public bool? MutualPartial { get; set; }
        public bool? MutualSelected { get; set; }
        public decimal? MutualUnitNumber { get; set; }
        public long? TransactionID { get; set; }
        public String MutualFundName { get; set; }
        public long? ApproverID { get; set; }
        public bool IsVerified { get; set; }
        public long? MutualFundTransactionBranchCheckerID { get; set; }
        public long? MutualFundTransactionID { get; set; }
        public int Flag { get; set; } //1. add, 2 update, 3 delete
        public string DBNumber { get; set; }
        public string UTNumber { get; set; }
    }
    public class MutualFundCBOUTMakerModel
    {
        public CBOFundModel MutualFundList { get; set; }
        public CBOFundModel MutualFundSwitchFrom { get; set; }
        public CBOFundModel MutualFundSwitchTo { get; set; }
        public CurrencyModel MutualCurrency { get; set; }
        public decimal? Amount { get; set; }
        public string Currency { get; set; }
        public decimal? MutualAmount { get; set; }
        public bool? MutualPartial { get; set; }
        public bool? partial { get; set; }  
        public bool? MutualSelected { get; set; }
        public decimal? MutualUnitNumber { get; set; }//updated by dani
        public decimal? NumberofUnit { get; set; }
        public long? TransactionID { get; set; }
        public String MutualFundName { get; set; }
        public string MutualFundCode { get; set; }
        public long? ApproverID { get; set; }
        public bool IsVerified { get; set; }
        public long? MutualFundTransactionCBOCheckerID { get; set; }
        public long? MutualFundTransactionID { get; set; }
        public int Flag { get; set; } //1. add, 2 update, 3 delete
        public string FromStrswitchmutualFund { get; set; }
        public string FromCodeswitchmutualfund { get; set; }
        public string TOStrswitchmutualFund { get; set; }
        public string TOCodeswitchmutualfund { get; set; }
        public string DBNumber { get; set; }
        public string UTNumber { get; set; }
        public bool VerifiedMutualFund { get; set; }
    }
    public class ReSubmitBranchMakerDetailModel : UTCheckerDetailModel
    {
        public IList<VerifyModel> Verify { set; get; }
        public TransactionCheckerDataModel Checker { get; set; }
        public IList<MutualFundBranchMakerModel> MutualFund { get; set; }
        public IList<JoinAccountBranchMakerModel> JoinAccount { get; set; }
    }

    public class ResubmitCBOUTMakerModel : ReSubmitBranchMakerDetailModel
    {
        public IList<MutualFundCBOUTMakerModel> MutualFund { get; set; }
        public IList<JoinAccountBranchMakerModel> JoinAccount { get; set; }
    }
    #endregion
    #endregion
    #region Interface
    public interface IWorkflowUTRepository : IDisposable
    {
        #region Agung
        //Agung 01.10.2015
        bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineModel> timelines, ref string message);
        bool GetUTDetails(Guid workflowInstanceId, ref UTDetailModel output, ref string message);
        #endregion
        #region Andri
        bool AddUTCBOAccountChecker(Guid workflowInstanceID, long approverID, ref UTBranchCheckerDetailModel output, ref string message);
        bool UpdateTransactionCheckerDetails(Guid workflowInstanceID, long approverID, UTBranchCheckerDetailModel data, ref string message);
        bool AddUTBranchChecker(Guid workflowInstanceID, long approverID, ref UTBranchCheckerDetailModel output, ref string message);
        bool GetTransactionCheckerDetails(Guid workflowInstanceID, long approverID, ref UTBranchCheckerDetailModel output, ref string message);
        bool GetTransactionDetails(Guid workflowInstanceID, ref UTDetailModel transaction, ref string message);
        bool GetTransactionMutualFund(Guid workflowInstanceID, ref IList<MutualFundCBOCheckerModel> transaction, ref string message);
        bool GetTransactionMutualFundBranch(Guid workflowInstanceID, ref IList<MutualFundCBOCheckerModel> transaction, ref string message);
        bool GetTransactionJoinAccountBranch(Guid workflowInstanceID, ref IList<JoinAccountBranchCheckerModel> JoinAccount, ref string message);
        bool GetTransactionJoinAccountUTChecker(Guid workflowInstanceID, ref IList<JoinAccountBranchCheckerModel> JoinAccount, ref string message);
        bool AddUTChecker(Guid workflowInstanceID, long approverID, ref UTBranchCheckerDetailModel output, ref string message);
        bool UpdateUTCheckerData(Guid workflowInstanceID, long approverID, ref UTBranchCheckerDetailModel output, ref string message);
        bool CboAccountChecker(Guid workflowInstanceID, long approverID, ref UTBranchCheckerDetailModel output, ref string message);
        bool GetTransactionUTCheckerDetails(Guid workflowInstanceID, long approverID, ref UTBranchCheckerDetailModel output, ref string message);

        #endregion
        #region Afif
        bool AddTransactionCBOAccountMaker(Guid workflowInstanceId, long approverID, UTCBOAccountMakerModel transaction, ref string message);
        bool AddTransactionCBOUTMaker(Guid workflowInstanceId, long approverID, UTBranchCheckerDetailModel transaction, ref string message);
        bool GetUTCBOAccountMaker(Guid workflowInstanceId, ref UTCBOAccountMakerModel output, ref string message);
        bool GetUTCBOUTMaker(Guid workflowInstanceId, ref UTCBOUTMakerModel output, ref string message);
        #endregion
        #region Team
        #region Dani
        bool GetTransactionCheckerDetailForBranchMaker(Guid workflowInstanceID, long approverID, ref ReSubmitBranchMakerDetailModel output, ref string message);
        bool GetTransactionMutualFundBranchMaker(Guid workflowInstanceID, ref IList<MutualFundBranchMakerModel> MutualFund, ref string message);
        bool GetTransactionJoinAccountBranchMaker(Guid workflowInstanceID, ref IList<JoinAccountBranchMakerModel> JoinAccount, ref string message);
        bool UpdateTransactionResubmitByBranchMaker(ReSubmitBranchMakerDetailModel ut, ref string message);
        bool GetMutualFundTransactionForCBOMaker(Guid workflowInstanceID, ref IList<MutualFundCBOUTMakerModel> MutualFund, ref string message);
        bool GetJoinAccountTransactionForCBOMaker(Guid workflowInstanceID, ref IList<JoinAccountCBOUTMakerModel> JoinAccount, ref string message);
        bool GetTransactionCheckerDetailForCBOUTMaker(Guid workflowInstanceID, long approverID, ref ResubmitCBOUTMakerModel output, ref string message);
        bool UpdateTransactionResubmitByCBOUTMaker(ResubmitCBOUTMakerModel ut, ref string message);
        #endregion

        #region Basri
        #endregion

        #region Chandra
        #endregion

        #region Andi
        #endregion
        #endregion

    }
    #endregion
    #region Repository
    public class WorkflowUTRepository : IWorkflowUTRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        #region Andri

        public bool UpdateTransactionCheckerDetails(Guid workflowInstanceID, long approverID, UTBranchCheckerDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

                    if (transactionID > 0)
                    {
                        var checker = context.TransactionCheckers
                           .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                           .ToList();

                        if (checker.Count > 0)
                        {
                            var checkerdata = context.TransactionCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                            foreach (var item in checkerdata)
                            {
                                context.TransactionCheckers.Remove(item);
                            }
                            context.SaveChanges();

                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });

                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });

                                context.SaveChanges();
                            }
                        }

                        checker = null;
                    }

                    #region Mutual Fund
                    if (data.MutualFund != null && data.MutualFund.Count > 0)
                    {

                        var dataMutualFund = context.MutualFundTransactionBranchCheckers.Where(x => x.TransactionID.Equals(transactionID)).ToList();

                        if (dataMutualFund.Count > 0)
                        {
                            foreach (var item in data.MutualFund)
                            {
                                var updateMutual = context.MutualFundTransactionBranchCheckers.Where(a => a.MutualFundTransactionID.Equals(item.MutualFundTransactionID)).SingleOrDefault();
                                updateMutual.IsVerified = item.VerifiedMutualFund;
                            }
                            context.SaveChanges();

                        }
                        else
                        {
                            IList<DBS.Entity.MutualFundTransactionBranchChecker> MutualFund = (from item in data.MutualFund
                                                                                               select new DBS.Entity.MutualFundTransactionBranchChecker
                                                                                               {
                                                                                                   ApproverID = approverID,
                                                                                                   TransactionID = item.TransactionID.Value,
                                                                                                   IsVerified = item.VerifiedMutualFund,
                                                                                                   CreateDate = DateTime.UtcNow,
                                                                                                   CreateBy = currentUser.GetCurrentUser().LoginName,
                                                                                                   UpdateBy = null,
                                                                                                   UpdateDate = null,
                                                                                                   MutualFundTransactionID = item.MutualFundTransactionID
                                                                                               }).ToList();

                            context.MutualFundTransactionBranchCheckers.AddRange(MutualFund);
                            context.SaveChanges();
                        }
                    }
                    #endregion

                    #region Join Account
                    if (data.JoinAccount != null && data.JoinAccount.Count > 0)
                    {
                        var dataJoin = context.JoinAccountBranchCheckers.Where(x => x.TransactionID.Equals(transactionID)).ToList();

                        if (dataJoin.Count > 0)
                        {
                            foreach (var item in data.JoinAccount)
                            {
                                var updateJoin = context.JoinAccountBranchCheckers.Where(a => a.JoinAccountID.Equals(item.JoinAccountID)).SingleOrDefault();
                                updateJoin.IsVerified = item.VerifiedJoinAccount;
                            }
                            context.SaveChanges();
                        }
                        else
                        {
                            IList<DBS.Entity.JoinAccountBranchChecker> JoinAccount = (from item in data.JoinAccount
                                                                                      select new DBS.Entity.JoinAccountBranchChecker
                                                                                      {
                                                                                          ApproverID = approverID,
                                                                                          TransactionID = item.TransactionID,
                                                                                          JoinAccountID = item.JoinAccountID,
                                                                                          IsVerified = item.VerifiedJoinAccount,
                                                                                          CreateDate = DateTime.UtcNow,
                                                                                          CreateBy = currentUser.GetCurrentUser().LoginName,
                                                                                          UpdateBy = null,
                                                                                          UpdateDate = null,

                                                                                      }).ToList();
                            context.JoinAccountBranchCheckers.AddRange(JoinAccount);
                            context.SaveChanges();
                        }
                    }
                    #endregion

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }
        public bool AddUTBranchChecker(Guid workflowInstanceID, long approverID, ref UTBranchCheckerDetailModel output, ref string message)
        {
            var transactionID = context.Transactions
                   .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                   .Select(a => a.TransactionID)
                   .SingleOrDefault();
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (transactionID > 0)
                    {

                        var checker = context.TransactionCheckers
                           .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                           .ToList();

                        if (checker.Count > 0)
                        {
                            var checkerdata = context.TransactionCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                            foreach (var item in checkerdata)
                            {
                                context.TransactionCheckers.Remove(item);
                            }
                            context.SaveChanges();

                            foreach (var item in output.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });

                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            foreach (var item in output.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });
                                context.SaveChanges();
                            }
                        }
                        checker = null;
                    }

                    //IList<DBS.Entity.TransactionChecker> selectedModel = (from item in output.Verify
                    //                                                      select new DBS.Entity.TransactionChecker
                    //                                                      {
                    //                                                          ApproverID = approverID,
                    //                                                          TransactionColumnID = item.ID,
                    //                                                          TransactionID = transactionID,
                    //                                                          IsVerified = item.IsVerifiedJoin,
                    //                                                          IsDeleted = false,
                    //                                                          CreateDate = DateTime.UtcNow,
                    //                                                          CreateBy = currentUser.GetCurrentUser().LoginName,
                    //                                                          UpdateBy = null,
                    //                                                          UpdateDate = null,

                    //                                                      }).ToList();
                    //context.TransactionCheckers.AddRange(selectedModel);
                    //context.SaveChanges();

                    #region Mutual
                    if (output.MutualFund != null && output.MutualFund.Count > 0)
                    {
                        var dataMutualFund = context.MutualFundTransactionBranchCheckers.Where(x => x.TransactionID.Equals(transactionID)).ToList();

                        if (dataMutualFund.Count > 0)
                        {
                            foreach (var item in output.MutualFund)
                            {
                                var updateMutual = context.MutualFundTransactionBranchCheckers.Where(a => a.MutualFundTransactionID.Equals(item.MutualFundTransactionID)).SingleOrDefault();
                                updateMutual.IsVerified = item.VerifiedMutualFund;
                            }
                            context.SaveChanges();

                        }
                        else
                        {
                            IList<DBS.Entity.MutualFundTransactionBranchChecker> MutualFund = (from item in output.MutualFund
                                                                                               select new DBS.Entity.MutualFundTransactionBranchChecker
                                                                                               {
                                                                                                   ApproverID = approverID,
                                                                                                   TransactionID = item.TransactionID.Value,
                                                                                                   IsVerified = item.VerifiedMutualFund,
                                                                                                   CreateDate = DateTime.UtcNow,
                                                                                                   CreateBy = currentUser.GetCurrentUser().LoginName,
                                                                                                   UpdateBy = null,
                                                                                                   UpdateDate = null,
                                                                                                   MutualFundTransactionID = item.MutualFundTransactionID,
                                                                                               }).ToList();

                            context.MutualFundTransactionBranchCheckers.AddRange(MutualFund);
                            context.SaveChanges();
                        }
                    }
                    #endregion

                    #region Join
                    if (output.JoinAccount != null && output.JoinAccount.Count > 0)
                    {
                        var dataJoinAccount = context.JoinAccountBranchCheckers.Where(x => x.TransactionID.Equals(transactionID)).ToList();

                        if (dataJoinAccount.Count > 0)
                        {
                            foreach (var item in output.JoinAccount)
                            {
                                var updateJoin = context.JoinAccountBranchCheckers.Where(a => a.JoinAccountID.Equals(item.JoinAccountID)).SingleOrDefault();
                                updateJoin.IsVerified = item.VerifiedJoinAccount;
                            }
                            context.SaveChanges();
                        }
                        else
                        {
                            IList<DBS.Entity.JoinAccountBranchChecker> JoinAccount = (from item in output.JoinAccount
                                                                                      select new DBS.Entity.JoinAccountBranchChecker
                                                                                      {
                                                                                          ApproverID = approverID,
                                                                                          TransactionID = item.TransactionID,
                                                                                          JoinAccountID = item.JoinAccountID,
                                                                                          IsVerified = item.VerifiedJoinAccount,
                                                                                          CreateDate = DateTime.UtcNow,
                                                                                          CreateBy = currentUser.GetCurrentUser().LoginName,
                                                                                          UpdateBy = null,
                                                                                          UpdateDate = null,

                                                                                      }).ToList();
                            context.JoinAccountBranchCheckers.AddRange(JoinAccount);
                            context.SaveChanges();
                        }
                    }

                    #endregion

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string _message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            raise = new InvalidOperationException(_message, raise);
                        }
                    }
                    throw raise;
                }

                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();

                }
            }
            return IsSuccess;
        }
        public bool AddUTCBOAccountChecker(Guid workflowInstanceID, long approverID, ref UTBranchCheckerDetailModel output, ref string message)
        {
            var transactionID = context.Transactions
                 .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                 .Select(a => a.TransactionID)
                 .SingleOrDefault();
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (transactionID > 0)
                    {
                        //update document completed


                        var checker = context.TransactionCheckers
                           .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                           .ToList();

                        if (checker.Count > 0)
                        {
                            var checkerdata = context.TransactionCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                            foreach (var item in checkerdata)
                            {
                                context.TransactionCheckers.Remove(item);
                            }
                            context.SaveChanges();

                            foreach (var item in output.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });

                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            foreach (var item in output.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });

                                context.SaveChanges();


                            }
                        }

                        checker = null;
                    }
                    ts.Complete();

                    IsSuccess = true;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string _message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(_message, raise);
                        }
                    }
                    throw raise;
                }

                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();

                }
            }
            return IsSuccess;
        }
        public bool CboAccountChecker(Guid workflowInstanceID, long approverID, ref UTBranchCheckerDetailModel output, ref string message)
        {
            bool IsSuccess = false;

            var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

            try
            {

                output.Verify = context.TransactionColumns
                    .Where(x => x.IsDeleted.Equals(false) && x.IsCBOAccountCheckerIDInvestment == true)
                    .Select(x => new VerifyModelUT()
                    {
                        ID = x.TransactionColumnID,
                        Name = x.ColumnName,
                        IsVerified = false
                    })
                    .ToList();

                IsSuccess = true;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string _message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(_message, raise);
                    }
                }
                throw raise;
            }

            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionUTCheckerDetails(Guid workflowInstanceID, long approverID, ref UTBranchCheckerDetailModel output, ref string message)
        {
            bool IsSuccess = false;

            var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

            try
            {
                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new UTBranchCheckerDetailModel
                          {

                              Verify = a.TransactionCBOUTCheckers
                                .Where(x => x.TransactionID == transactionID && x.IsDeleted == false)
                                .Select(x => new VerifyModelUT()
                                {
                                    ID = x.TransactionColumnID,
                                    Name = x.TransactionColumn.ColumnName,
                                    IsVerified = x.IsVerified
                                }).ToList()
                          }).SingleOrDefault();
                if (output != null)
                {
                    if (!output.Verify.Any())
                    {

                        output.Verify = context.TransactionColumns
                                     .Where(x => x.IsDeleted.Equals(false) && x.IsCBOUTCheckerIDInvestment == true)
                                     .Select(x => new VerifyModelUT()
                                     {
                                         ID = x.TransactionColumnID,
                                         Name = x.ColumnName,
                                         IsVerified = false
                                     })
                                     .ToList();
                    }
                }
                IsSuccess = true;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string _message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(_message, raise);
                    }
                }
                throw raise;
            }

            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionCheckerDetails(Guid workflowInstanceID, long approverID, ref UTBranchCheckerDetailModel output, ref string message)
        {
            bool IsSuccess = false;

            var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

            try
            {
                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new UTBranchCheckerDetailModel
                          {

                              Verify = a.TransactionCheckers
                                .Where(x => x.TransactionID == transactionID && x.IsDeleted == false)
                                .Select(x => new VerifyModelUT()
                                {
                                    ID = x.TransactionColumnID,
                                    Name = x.TransactionColumn.ColumnName,
                                    IsVerified = x.IsVerified
                                }).ToList()
                          }).SingleOrDefault();

                // replace output while Checker & Verify is empty
                if (output != null)
                {
                    if (!output.Verify.Any())
                    {
                        output.Verify = context.TransactionColumns
                            .Where(x => x.IsBranchCheckerUTProduct == true && x.IsDeleted == false)
                            //.Where(x => x.IsDeleted.Equals(false))
                            .Select(x => new VerifyModelUT()
                            {
                                ID = x.TransactionColumnID,
                                Name = x.ColumnName,
                                IsVerified = false
                            })
                            .ToList();
                    }
                }

                IsSuccess = true;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string _message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(_message, raise);
                    }
                }
                throw raise;
            }

            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionDetails(Guid instanceID, ref UTDetailModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                UTDetailModel datatransaction = (from a in context.Transactions
                                                 where a.WorkflowInstanceID.Value.Equals(instanceID)
                                                 select new UTDetailModel
                                                 {
                                                     ID = a.TransactionID,
                                                     WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                                     ApplicationID = a.ApplicationID,
                                                     //ApplicationDate = a.ApplicationDate,
                                                     FunctionType = a.FunctionType != null ? a.FunctionType : 0,
                                                     ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                                                     //BankCharges = new cha
                                                     SolID = a.SolID,
                                                     IsFNACore = a.IsFNACore,
                                                     RiskScore = a.RiskScore,
                                                     RiskProfileExpiryDate = a.RiskProfileExpiryDate,
                                                     OperativeAccount = a.OperativeAccount,
                                                     CRiskEfectiveDate = a.CRiskEfectiveDate,
                                                     InvestmenName = a.InvestmentID != null ? a.InvestmentID : " ",
                                                     InvestmentID = a.InvestmentID != null ? a.InvestmentID : " ",
                                                     LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                                     AccountType = a.AccountType,
                                                     DBNumber = a.DBNumber,
                                                     UTNumber = a.UTNumber,
                                                     Remarks = a.Remarks,
                                                     AttachmentRemarks = a.AttachmentRemarks,
                                                     Product = new ProductModel()
                                                     {
                                                         ID = a.Product.ProductID,
                                                         Code = a.Product.ProductCode,
                                                         Name = a.Product.ProductName,
                                                         WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                                         Workflow = a.Product.ProductWorkflow.WorkflowName,
                                                         LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                                         LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                                     },
                                                     Currency = new CurrencyModel()
                                                     {
                                                         ID = a.Currency.CurrencyID,
                                                         Code = a.Currency.CurrencyCode,
                                                         Description = a.Currency.CurrencyDescription,
                                                         LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                         LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                     },
                                                     Amount = a.Amount,
                                                     AmountUSD = a.AmountUSD,
                                                     Rate = a.Rate,
                                                     Channel = new ChannelModel()
                                                     {
                                                         ID = a.Channel.ChannelID,
                                                         Name = a.Channel.ChannelName,
                                                         LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                                         LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                                     },

                                                     BizSegment = new BizSegmentModel()
                                                     {
                                                         ID = a.BizSegment.BizSegmentID,
                                                         Name = a.BizSegment.BizSegmentName,
                                                         Description = a.BizSegment.BizSegmentDescription,
                                                         LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                         LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                     },

                                                     transactiontypeID = a.TransactionTypeID != null ? a.TransactionTypeID : 0,
                                                     IsTopUrgent = a.IsTopUrgent,

                                                     Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                                                     {
                                                         ID = x.TransactionDocumentID,
                                                         Type = new DocumentTypeModel()
                                                         {
                                                             ID = x.DocumentType.DocTypeID,
                                                             Name = x.DocumentType.DocTypeName,
                                                             Description = x.DocumentType.DocTypeDescription,
                                                             LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                             LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                         },
                                                         Purpose = new DocumentPurposeModel()
                                                         {
                                                             ID = x.DocumentPurpose.PurposeID,
                                                             Name = x.DocumentPurpose.PurposeName,
                                                             Description = x.DocumentPurpose.PurposeDescription,
                                                             LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                             LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                         },
                                                         FileName = x.Filename,
                                                         DocumentPath = x.DocumentPath,
                                                         LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                         LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value,
                                                         IsDormant = x.IsDormant == null ? false : x.IsDormant
                                                     }).ToList(),
                                                     CreateDate = a.CreateDate,
                                                     LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy
                                                 }).SingleOrDefault();

                if (datatransaction != null)
                {
                    //datatransaction.IsOtherBeneBank = datatransaction.Bank.Code != "999" ? false : true;
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(instanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    // changed, chandra : 2015.03.23
                    if (customerRepo.GetCustomerByTransaction(datatransaction.ID, ref customer, ref message))
                    {
                        datatransaction.Customer = customer;

                    }
                    if (datatransaction.FunctionType != 0)
                    {
                        string functiontypename = context.ParameterSystems.Where(a => a.ParsysID == datatransaction.FunctionType).Select(a => a.ParameterValue).FirstOrDefault();
                        datatransaction.strfunctionType = functiontypename;
                    }
                    if (datatransaction.FunctionType == null)
                    {
                        datatransaction.FunctionType = 0;
                    }
                    if (datatransaction.AccountType != null)
                    {
                        string AccountTypeName = context.ParameterSystems.Where(a => a.ParsysID == datatransaction.AccountType).Select(a => a.ParameterValue).FirstOrDefault();
                        datatransaction.strAccountType = AccountTypeName;
                    }
                    if (datatransaction.FunctionType == null)
                    {
                        datatransaction.FunctionType = 0;
                    }
                    if (datatransaction.IsFNACore == true)
                    {
                        datatransaction.strFNACore = "YES";
                    }
                    if (datatransaction.IsFNACore == false)
                    {
                        datatransaction.strFNACore = "NO";
                    }
                    if (datatransaction.transactiontypeID != null)
                    {
                        string TransactionTypeName = context.TransactionTypes.Where(a => a.TransTypeID == datatransaction.transactiontypeID).Select(a => a.TransactionType1).FirstOrDefault();
                        datatransaction.strTransactiontype = TransactionTypeName;
                    }
                    if (datatransaction.transactiontypeID == 0)
                    {
                        datatransaction.strTransactiontype = " ";
                    }
                    customerRepo.Dispose();
                    customer = null;
                }

                transaction = datatransaction;

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionMutualFund(Guid workflowInstanceID, ref IList<MutualFundCBOCheckerModel> MutualFund, ref string message)
        {
            var TransactionTypeID = (from a in context.Transactions
                                     where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                                     select a.TransactionTypeID).FirstOrDefault();
            bool IsSuccess = false;
            var isSwitching = false;
            try 
            {
                context.Transactions.AsNoTracking();
                if (TransactionTypeID != null)
                {
                    if (TransactionTypeID == (int)UTSwitching.onShore || TransactionTypeID == (int)UTSwitching.ofShore || TransactionTypeID == (int)UTSwitching.cpf)
                    {
                        isSwitching = true;
                    }
                }
                if (isSwitching)
                {
                    MutualFund = (from Trans in context.Transactions
                                  join CBOTrans in context.MutualFundTransactions on Trans.TransactionID equals CBOTrans.TransactionID
                                  join fundname in context.Funds on CBOTrans.FundID equals fundname.FundID
                                  join fundto in context.Funds on CBOTrans.SwitchToFundID equals fundto.FundID
                                  join CBOChecker in context.MutualFundTransactionCBOCheckers on CBOTrans.MutualFundTransactionID equals CBOChecker.MutualFundTransactionID
                                  into temp from J in temp.DefaultIfEmpty()
                                  where Trans.WorkflowInstanceID == workflowInstanceID

                                  select new MutualFundCBOCheckerModel
                                  {
                                      TransactionID = Trans.TransactionID,
                                      ApproverID = Trans.ApproverID,
                                      MutualFundName = string.IsNullOrEmpty(fundname.FundName) ? "-" : fundname.FundName,
                                      MutualFundCode = string.IsNullOrEmpty(fundname.FundCode) ? "-" : fundname.FundCode,
                                      Currency = string.IsNullOrEmpty(CBOTrans.Currency.CurrencyCode) ? "-" : CBOTrans.Currency.CurrencyCode,//Trans.Currency.CurrencyCode,
                                      Amount = CBOTrans.Amount ?? 0,
                                      MutualFundTransactionID = CBOTrans.MutualFundTransactionID != null ? CBOTrans.MutualFundTransactionID : 0,
                                      partial = CBOTrans.IsPartial != null ? CBOTrans.IsPartial : null,
                                      NumberofUnit = CBOTrans.NumberofUnit != null ? CBOTrans.NumberofUnit : 0,
                                      FromCodeswitchmutualfund = string.IsNullOrEmpty(fundname.FundCode) ? "-" : fundname.FundCode,
                                      FromStrswitchmutualFund = string.IsNullOrEmpty(fundname.FundName) ? "-" : fundname.FundName,
                                      TOCodeswitchmutualfund = CBOTrans.SwitchToFundID != null ? fundto.FundCode : string.Empty,
                                      TOStrswitchmutualFund = fundto.FundName,
                                      IsVerified = J.IsVerified != null ? J.IsVerified : false,
                                      VerifiedMutualFund = J.IsVerified != null ? J.IsVerified : false,
                                      DBNumber = CBOTrans.DBNumber,
                                      UTNumber = CBOTrans.UTNumber
                                  }).ToList();
                }
                else
                {
                    MutualFund = (from Trans in context.Transactions
                                  join CBOTrans in context.MutualFundTransactions on Trans.TransactionID equals CBOTrans.TransactionID
                                  join fundname in context.Funds on CBOTrans.FundID equals fundname.FundID
                                  join CBOChecker in context.MutualFundTransactionCBOCheckers on CBOTrans.MutualFundTransactionID equals CBOChecker.MutualFundTransactionID
                                  into temp
                                  from J in temp.DefaultIfEmpty()
                                  where Trans.WorkflowInstanceID == workflowInstanceID

                                  select new MutualFundCBOCheckerModel
                                  {
                                      TransactionID = Trans.TransactionID,
                                      ApproverID = Trans.ApproverID,
                                      MutualFundName = string.IsNullOrEmpty(fundname.FundName) ? "-" : fundname.FundName,
                                      MutualFundCode = string.IsNullOrEmpty(fundname.FundCode) ? "-" : fundname.FundCode,
                                      Currency = string.IsNullOrEmpty(CBOTrans.Currency.CurrencyCode) ? "-" : CBOTrans.Currency.CurrencyCode,//Trans.Currency.CurrencyCode,
                                      Amount = CBOTrans.Amount ?? 0,
                                      MutualFundTransactionID = CBOTrans.MutualFundTransactionID != null ? CBOTrans.MutualFundTransactionID : 0,
                                      partial = CBOTrans.IsPartial != null ? CBOTrans.IsPartial : null,
                                      NumberofUnit = CBOTrans.NumberofUnit != null ? CBOTrans.NumberofUnit : 0,
                                      FromCodeswitchmutualfund = string.IsNullOrEmpty(fundname.FundCode) ? "-" : fundname.FundCode,
                                      FromStrswitchmutualFund = string.IsNullOrEmpty(fundname.FundName) ? "-" : fundname.FundName,
                                      TOCodeswitchmutualfund = string.Empty,
                                      TOStrswitchmutualFund = string.Empty,
                                      IsVerified = J.IsVerified != null ? J.IsVerified : false,
                                      VerifiedMutualFund = J.IsVerified != null ? J.IsVerified : false,
                                      DBNumber = CBOTrans.DBNumber,
                                      UTNumber = CBOTrans.UTNumber
                                  }).ToList();
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return IsSuccess;
        }
        public bool GetTransactionJoinAccountUTChecker(Guid workflowInstanceID, ref IList<JoinAccountBranchCheckerModel> JoinAccount, ref string message)
        {
            long TransactionID = 0;

            bool IsSuccess = false;
            try
            {
                context.Transactions.AsNoTracking();

                JoinAccount = (from Trans in context.Transactions
                               join TransJoin in context.JoinAccounts on Trans.TransactionID equals TransJoin.TransactionID
                               where Trans.WorkflowInstanceID == workflowInstanceID
                               select new JoinAccountBranchCheckerModel
                               {
                                   TransactionID = Trans.TransactionID,
                                   ApproverID = Trans.ApproverID,
                                   JoinAccountID = TransJoin.JoinAccountID,
                                   JoinOrder = TransJoin.JoinOrder,
                                   Join = new JoinModel
                                   {
                                       ID = TransJoin.JoinType,
                                       Name = TransJoin.JoinType == false ? "And" : "Or"
                                   },
                                   CreateDate = TransJoin.CreateDate,
                                   CreateBy = TransJoin.CreateBy,
                                   UpdateDate = TransJoin.UpdateDate,
                                   UpdateBy = TransJoin.UpdateBy,
                                   CustomerName = TransJoin.Customer.CustomerName,
                                   CIF = TransJoin.CIF,
                                   CriskEfDate = TransJoin.RiskEffectiveDate,
                                   RiskScore = TransJoin.RiskScore,
                                   RiskProfexpDate = TransJoin.RiskProfileExpiryDate,
                                   SolIDJoin = TransJoin.SolID,
                               }).ToList();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return IsSuccess;
        }
        public bool GetTransactionMutualFundBranch(Guid workflowInstanceID, ref IList<MutualFundCBOCheckerModel> MutualFund, ref string message)
        {
            long TransactionID = 0;

            bool IsSuccess = false;
            try
            {
                context.Transactions.AsNoTracking();

                MutualFund = (from Trans in context.Transactions
                              join CBOTrans in context.MutualFundTransactionBranchCheckers on Trans.TransactionID equals CBOTrans.TransactionID
                              where Trans.WorkflowInstanceID == workflowInstanceID
                              select new MutualFundCBOCheckerModel
                              {
                                  TransactionID = Trans.TransactionID,
                              }).ToList();

                if (MutualFund.Count != 0 && MutualFund != null)
                {
                    MutualFund = (from Trans in context.Transactions
                                  join BranchChecker in context.MutualFundTransactionBranchCheckers on Trans.TransactionID equals BranchChecker.TransactionID
                                  join FundTrans in context.MutualFundTransactions on BranchChecker.MutualFundTransactionID equals FundTrans.MutualFundTransactionID
                                  join fundname in context.Funds on FundTrans.FundID equals fundname.FundID
                                  join fundto in context.Funds on FundTrans.SwitchToFundID equals fundto.FundID into ft
                                  from fundto in ft.DefaultIfEmpty()
                                  where Trans.WorkflowInstanceID == workflowInstanceID
                                  select new MutualFundCBOCheckerModel
                                  {
                                      TransactionID = Trans.TransactionID,
                                      ApproverID = Trans.ApproverID,
                                      MutualFundName = fundname.FundName,
                                      Currency = FundTrans.Currency.CurrencyCode,
                                      Amount = FundTrans.Amount,
                                      MutualFundTransactionID = BranchChecker.MutualFundTransactionID,
                                      FromCodeswitchmutualfund = fundname.FundCode,
                                      FromStrswitchmutualFund = fundname.FundName,
                                      TOCodeswitchmutualfund = FundTrans.SwitchToFundID != null ? fundto.FundCode : string.Empty,
                                      TOStrswitchmutualFund = fundto.FundName,
                                      partial = FundTrans.IsPartial != null ? FundTrans.IsPartial : null,
                                      NumberofUnit = FundTrans.NumberofUnit != null ? FundTrans.NumberofUnit : 0,
                                      VerifiedMutualFund = BranchChecker.IsVerified,
                                      UTNumber = FundTrans.UTNumber,
                                      DBNumber = FundTrans.DBNumber
                                  }).ToList();
                }
                else
                {
                    MutualFund = (from Trans in context.Transactions
                                  join CBOTrans in context.MutualFundTransactions on Trans.TransactionID equals CBOTrans.TransactionID
                                  join fundname in context.Funds on CBOTrans.FundID equals fundname.FundID
                                  join fundto in context.Funds on CBOTrans.SwitchToFundID equals fundto.FundID into ft
                                  from fundto in ft.DefaultIfEmpty()
                                  where Trans.WorkflowInstanceID == workflowInstanceID
                                  select new MutualFundCBOCheckerModel
                                  {
                                      TransactionID = Trans.TransactionID,
                                      ApproverID = Trans.ApproverID,
                                      MutualFundName = fundname.FundName,
                                      Currency = CBOTrans.Currency.CurrencyCode,
                                      Amount = CBOTrans.Amount,
                                      MutualFundTransactionID = CBOTrans.MutualFundTransactionID,
                                      FromCodeswitchmutualfund = fundname.FundCode,
                                      FromStrswitchmutualFund = fundname.FundName,
                                      TOCodeswitchmutualfund = CBOTrans.SwitchToFundID != null ? fundto.FundCode : string.Empty,
                                      TOStrswitchmutualFund = fundto.FundName,
                                      partial = CBOTrans.IsPartial != null ? CBOTrans.IsPartial : null,
                                      NumberofUnit = CBOTrans.NumberofUnit != null ? CBOTrans.NumberofUnit : 0,
                                      VerifiedMutualFund = false,
                                      UTNumber = CBOTrans.UTNumber,
                                      DBNumber = CBOTrans.DBNumber
                                  }).ToList();
                }


                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return IsSuccess;
        }
        public bool GetTransactionJoinAccountBranch(Guid workflowInstanceID, ref IList<JoinAccountBranchCheckerModel> JoinAccount, ref string message)
        {
            long TransactionID = 0;

            bool IsSuccess = false;
            try
            {
                context.Transactions.AsNoTracking();

                JoinAccount = (from Trans in context.Transactions
                               join TransJoin in context.JoinAccountBranchCheckers on Trans.TransactionID equals TransJoin.TransactionID
                               where Trans.WorkflowInstanceID == workflowInstanceID
                               select new JoinAccountBranchCheckerModel
                               {
                                   TransactionID = Trans.TransactionID,
                               }).ToList();

                if (JoinAccount.Count != 0 && JoinAccount != null)
                {
                    JoinAccount = (from Trans in context.Transactions
                                   join TransJoin in context.JoinAccounts on Trans.TransactionID equals TransJoin.TransactionID
                                   join JoinChecker in context.JoinAccountBranchCheckers on TransJoin.JoinAccountID equals JoinChecker.JoinAccountID
                                   where Trans.WorkflowInstanceID == workflowInstanceID
                                   select new JoinAccountBranchCheckerModel
                                   {
                                       TransactionID = Trans.TransactionID,
                                       ApproverID = Trans.ApproverID,
                                       JoinAccountID = TransJoin.JoinAccountID,
                                       JoinOrder = TransJoin.JoinOrder,
                                       Join = new JoinModel
                                       {
                                           ID = TransJoin.JoinType,
                                           Name = TransJoin.JoinType == false ? "And" : "Or"
                                       },
                                       CreateDate = TransJoin.CreateDate,
                                       CreateBy = TransJoin.CreateBy,
                                       UpdateDate = TransJoin.UpdateDate,
                                       UpdateBy = TransJoin.UpdateBy,
                                       CustomerName = TransJoin.Customer.CustomerName,
                                       CIF = TransJoin.CIF,
                                       CriskEfDate = TransJoin.RiskEffectiveDate,
                                       RiskScore = TransJoin.RiskScore,
                                       RiskProfexpDate = TransJoin.RiskProfileExpiryDate,
                                       SolIDJoin = TransJoin.SolID,
                                       VerifiedJoinAccount = JoinChecker.IsVerified

                                   }).ToList();
                }
                else
                {
                    JoinAccount = (from Trans in context.Transactions
                                   join TransJoin in context.JoinAccounts on Trans.TransactionID equals TransJoin.TransactionID
                                   where Trans.WorkflowInstanceID == workflowInstanceID
                                   select new JoinAccountBranchCheckerModel
                                   {
                                       TransactionID = Trans.TransactionID,
                                       ApproverID = Trans.ApproverID,
                                       JoinAccountID = TransJoin.JoinAccountID,
                                       JoinOrder = TransJoin.JoinOrder,
                                       Join = new JoinModel
                                       {
                                           ID = TransJoin.JoinType,
                                           Name = TransJoin.JoinType == false ? "And" : "Or"
                                       },
                                       CreateDate = TransJoin.CreateDate,
                                       CreateBy = TransJoin.CreateBy,
                                       UpdateDate = TransJoin.UpdateDate,
                                       UpdateBy = TransJoin.UpdateBy,
                                       CustomerName = TransJoin.Customer.CustomerName,
                                       CIF = TransJoin.CIF,
                                       CriskEfDate = TransJoin.RiskEffectiveDate,
                                       RiskScore = TransJoin.RiskScore,
                                       RiskProfexpDate = TransJoin.RiskProfileExpiryDate,
                                       SolIDJoin = TransJoin.SolID,
                                   }).ToList();
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return IsSuccess;
        }
        public bool AddUTChecker(Guid workflowInstanceID, long approverID, ref UTBranchCheckerDetailModel output, ref string message)
        {
            var transactionID = context.Transactions
                               .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                               .Select(a => a.TransactionID)
                               .SingleOrDefault();
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (transactionID > 0)
                    {
                        var checker = context.TransactionCBOUTCheckers
                           .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                           .ToList();

                        if (checker.Count > 0)
                        {
                            var checkerdata = context.TransactionCBOUTCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                            foreach (var item in checkerdata)
                            {
                                context.TransactionCBOUTCheckers.Remove(item);
                            }
                            context.SaveChanges();

                            foreach (var item in output.Verify)
                            {
                                context.TransactionCBOUTCheckers.Add(new TransactionCBOUTChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });

                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            foreach (var item in output.Verify)
                            {
                                context.TransactionCBOUTCheckers.Add(new TransactionCBOUTChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });
                                context.SaveChanges();
                            }
                        }

                        checker = null;
                    }

                    #region Mutual Fund
                    if (output.MutualFund != null && output.MutualFund.Count > 0)
                    {

                        var dataMutualFund = context.MutualFundTransactionCBOCheckers.Where(x => x.TransactionID.Equals(transactionID)).ToList();

                        if (dataMutualFund.Count > 0)
                        {
                            foreach (var item in output.MutualFund)
                            {
                                var updateMutual = context.MutualFundTransactionCBOCheckers.Where(a => a.MutualFundTransactionID.Equals(item.MutualFundTransactionID)).SingleOrDefault();
                                if (updateMutual != null)
                                {
                                    updateMutual.IsVerified = item.VerifiedMutualFund;
                                    context.SaveChanges();
                                }
                            }
                        }
                        else
                        {
                            IList<DBS.Entity.MutualFundTransactionCBOChecker> MutualFund = (from item in output.MutualFund
                                                                                            select new DBS.Entity.MutualFundTransactionCBOChecker
                                                                                            {
                                                                                                ApproverID = approverID,
                                                                                                TransactionID = item.TransactionID.Value,
                                                                                                IsVerified = item.VerifiedMutualFund,
                                                                                                CreateDate = DateTime.UtcNow,
                                                                                                CreateBy = currentUser.GetCurrentUser().LoginName,
                                                                                                UpdateBy = null,
                                                                                                UpdateDate = null,
                                                                                                MutualFundTransactionID = item.MutualFundTransactionID
                                                                                            }).ToList();

                            context.MutualFundTransactionCBOCheckers.AddRange(MutualFund);
                            context.SaveChanges();
                        }

                        #region for table master.mutuallist
                        var InvestmentID = output.Transaction.InvestmentID;
                        // var SelectInvestID = from xy in context.MutualLists where xy.InvestmentID.Equals(InvestmentID) select xy;
                        //Entity.MutualList DataMutualist = context.MutualLists.Where(a => a.InvestmentID.Equals(InvestmentID)).SingleOrDefault();
                        if (output.MutualFund != null)
                        {
                            foreach (var xy in output.MutualFund)
                            {
                                int idFund = context.Funds.Where(a => a.FundName == xy.MutualFundName).Select(a => a.FundID).FirstOrDefault();
                                var DataMutualList = context.MutualLists.Where(a => a.FundID.Value.Equals(idFund) && a.InvestmentID.Equals(InvestmentID)).FirstOrDefault();

                                if (DataMutualList != null)
                                {
                                    DataMutualList.ConfirmedUnit = xy.NumberofUnit;
                                    context.SaveChanges();
                                }
                            }

                        }

                        #endregion
                    }
                    #endregion

                    #region Join Account
                    if (output.JoinAccount != null && output.JoinAccount.Count > 0)
                    {
                        var dataJoinAccount = context.JoinAccountCBOCheckers.Where(x => x.TransactionID.Equals(transactionID)).ToList();

                        if (dataJoinAccount.Count > 0)
                        {
                            foreach (var item in output.JoinAccount)
                            {
                                //var updateJoin = context.JoinAccountCBOCheckers.Where(a => a.JoinAccountCBOCheckerID.Equals(item.JoinAccountBranchCheckerID)).SingleOrDefault();
                                var updateJoin = context.JoinAccountCBOCheckers.Where(a => a.JoinAccountID.Equals(item.JoinAccountID)).SingleOrDefault();
                                if (updateJoin != null)
                                {
                                    updateJoin.IsVerified = item.VerifiedJoinAccount;
                                    context.SaveChanges();
                                }
                            }
                        }
                    }
                    else
                    {
                        IList<DBS.Entity.JoinAccountCBOChecker> JoinAccount = (from item in output.JoinAccount
                                                                               select new DBS.Entity.JoinAccountCBOChecker
                                                                               {
                                                                                   ApproverID = approverID,
                                                                                   TransactionID = item.TransactionID,
                                                                                   JoinAccountID = item.JoinAccountID,
                                                                                   IsVerified = item.VerifiedJoinAccount,
                                                                                   CreateDate = DateTime.UtcNow,
                                                                                   CreateBy = currentUser.GetCurrentUser().LoginName,
                                                                                   UpdateBy = null,
                                                                                   UpdateDate = null,

                                                                               }).ToList();
                        context.JoinAccountCBOCheckers.AddRange(JoinAccount);
                        context.SaveChanges();
                    }
                    #endregion

                    InvestmentID invest = new InvestmentID()
                    {
                        CIF = output.Transaction.Customer.CIF,
                        IsUT = true,
                        IsBond = false,
                        STNumber = null,
                        CheckIN = null,
                        CheckCIF = null,
                        CheckName = null,
                        CheckST = null,
                        IsDelete = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().LoginName,
                        Investment = output.Transaction.InvestmenName,

                    };

                    context.InvestmentIDs.Add(invest);
                    context.SaveChanges();

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string _message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(_message, raise);
                        }
                    }
                    throw raise;
                }

                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();

                }
            }
            return IsSuccess;
        }

        public bool UpdateUTCheckerData(Guid workflowInstanceID, long approverID, ref UTBranchCheckerDetailModel output, ref string message)
        {
            var transactionID = context.Transactions
                               .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                               .Select(a => a.TransactionID)
                               .SingleOrDefault();
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {

                    #region Transaction UT Checker
                    if (transactionID > 0)
                    {
                        var checker = context.TransactionCBOUTCheckers
                           .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                           .ToList();

                        if (checker.Count > 0)
                        {
                            var checkerdata = context.TransactionCBOUTCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                            foreach (var item in checkerdata)
                            {
                                context.TransactionCBOUTCheckers.Remove(item);
                            }
                            context.SaveChanges();

                            foreach (var item in output.Verify)
                            {
                                context.TransactionCBOUTCheckers.Add(new TransactionCBOUTChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });

                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            foreach (var item in output.Verify)
                            {
                                context.TransactionCBOUTCheckers.Add(new TransactionCBOUTChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });
                                context.SaveChanges();
                            }
                        }
                        checker = null;
                    }

                    #endregion

                    #region Mutual Fund
                    if (output.MutualFund != null && output.MutualFund.Count > 0)
                    {

                        var dataMutualFund = context.MutualFundTransactionCBOCheckers.Where(x => x.TransactionID.Equals(transactionID)).ToList();

                        if (dataMutualFund.Count > 0)
                        {
                            foreach (var item in output.MutualFund)
                            {
                                var updateMutual = context.MutualFundTransactionCBOCheckers.Where(a => a.MutualFundTransactionID.Equals(item.MutualFundTransactionID)).SingleOrDefault();
                                if (updateMutual == null)
                                {
                                    DBS.Entity.MutualFundTransactionCBOChecker MF = new DBS.Entity.MutualFundTransactionCBOChecker()
                                    {
                                        ApproverID = approverID,
                                        TransactionID = item.TransactionID.Value,
                                        IsVerified = item.VerifiedMutualFund,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName,
                                        UpdateBy = null,
                                        UpdateDate = null,
                                        MutualFundTransactionID = item.MutualFundTransactionID
                                    };
                                    context.MutualFundTransactionCBOCheckers.Add(MF);
                                    context.SaveChanges();
                                }
                                else {
                                    updateMutual.IsVerified = item.VerifiedMutualFund;
                                    context.SaveChanges();
                                }
                            }
                        }
                        else
                        {
                            IList<DBS.Entity.MutualFundTransactionCBOChecker> MutualFund = (from item in output.MutualFund
                                                                                            select new DBS.Entity.MutualFundTransactionCBOChecker
                                                                                            {
                                                                                                ApproverID = approverID,
                                                                                                TransactionID = item.TransactionID.Value,
                                                                                                IsVerified = item.VerifiedMutualFund,
                                                                                                CreateDate = DateTime.UtcNow,
                                                                                                CreateBy = currentUser.GetCurrentUser().LoginName,
                                                                                                UpdateBy = null,
                                                                                                UpdateDate = null,
                                                                                                MutualFundTransactionID = item.MutualFundTransactionID
                                                                                            }).ToList();

                            context.MutualFundTransactionCBOCheckers.AddRange(MutualFund);
                            context.SaveChanges();
                        }

                    }
                    #endregion

                    #region Join
                    if (output.JoinAccount != null && output.JoinAccount.Count > 0)
                    {
                        var dataJoinAccount = context.JoinAccountCBOCheckers.Where(x => x.TransactionID.Equals(transactionID)).ToList();

                        if (dataJoinAccount.Count > 0)
                        {
                            foreach (var item in output.JoinAccount)
                            {
                                var updateJoin = context.JoinAccountCBOCheckers.Where(a => a.JoinAccountID.Equals(item.JoinAccountID)).SingleOrDefault();
                                if (updateJoin != null)
                                {
                                    updateJoin.IsVerified = item.VerifiedJoinAccount;
                                }
                            }
                            context.SaveChanges();
                        }
                        else
                        {
                            IList<DBS.Entity.JoinAccountCBOChecker> JoinAccount = (from item in output.JoinAccount
                                                                                   select new DBS.Entity.JoinAccountCBOChecker
                                                                                   {
                                                                                       ApproverID = approverID,
                                                                                       TransactionID = item.TransactionID,
                                                                                       JoinAccountID = item.JoinAccountID,
                                                                                       IsVerified = item.VerifiedJoinAccount,
                                                                                       CreateDate = DateTime.UtcNow,
                                                                                       CreateBy = currentUser.GetCurrentUser().LoginName,
                                                                                       UpdateBy = null,
                                                                                       UpdateDate = null,

                                                                                   }).ToList();
                            context.JoinAccountCBOCheckers.AddRange(JoinAccount);
                            context.SaveChanges();
                        }
                    }
                    #endregion

                    ts.Complete();
                    IsSuccess = true;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string _message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(_message, raise);
                        }
                    }
                    throw raise;
                }

                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();

                }
            }
            return IsSuccess;
        }

        #endregion
        #region UT-Agung

        /// <summary>
        /// Add method below...
        /// </summary>
        public bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineModel> timelines, ref string message)
        {
            bool IsSuccess = false;

            try
            {

                timelines = (from a in context.SP_GetNintexTaskTimeline(workflowInstanceID)
                             select new TransactionTimelineModel
                             {
                                 ApproverID = a.WorkflowApproverID,
                                 Activity = a.ActivityTitle,
                                 Time = a.ActivityTime,
                                 UserOrGroup = a.UserOrGroup,
                                 IsGroup = a.IsSPGroup.Value,
                                 Outcome = a.Outcome,
                                 DisplayName = a.DisplayName,
                                 Comment = a.Comment
                             }).ToList();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetUTDetails(Guid workflowInstanceID, ref UTDetailModel output, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();
                // get main transaction data

                UTDetailModel datatransaction = (from a in context.Transactions
                                                 where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                                                 select new UTDetailModel
                                                 {
                                                     ID = a.TransactionID,
                                                     WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                                     ApplicationID = a.ApplicationID,
                                                     //ApplicationDate = a.ApplicationDate,
                                                     FunctionType = a.FunctionType != null ? a.FunctionType : 0,
                                                     ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                                                     //BankCharges = new cha
                                                     SolID = a.SolID,
                                                     IsFNACore = a.IsFNACore,
                                                     RiskScore = a.RiskScore,
                                                     RiskProfileExpiryDate = a.RiskProfileExpiryDate,
                                                     OperativeAccount = a.OperativeAccount,
                                                     CRiskEfectiveDate = a.CRiskEfectiveDate,
                                                     InvestmenName = a.InvestmentID != null ? a.InvestmentID : " ",
                                                     InvestmentID = a.InvestmentID != null ? a.InvestmentID : " ",
                                                     LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                                     AccountType = a.AccountType,
                                                     DBNumber = a.DBNumber,
                                                     UTNumber = a.UTNumber,
                                                     AttachmentRemarks = a.AttachmentRemarks,// add by adi

                                                     Product = new ProductModel()
                                                     {
                                                         ID = a.Product.ProductID,
                                                         Code = a.Product.ProductCode,
                                                         Name = a.Product.ProductName,
                                                         WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                                         Workflow = a.Product.ProductWorkflow.WorkflowName,
                                                         LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                                         LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                                     },
                                                     Currency = new CurrencyModel()
                                                     {
                                                         ID = a.Currency.CurrencyID,
                                                         Code = a.Currency.CurrencyCode,
                                                         Description = a.Currency.CurrencyDescription,
                                                         LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                         LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                     },
                                                     Amount = a.Amount,
                                                     AmountUSD = a.AmountUSD,
                                                     Rate = a.Rate,
                                                     Channel = new ChannelModel()
                                                     {
                                                         ID = a.Channel.ChannelID,
                                                         Name = a.Channel.ChannelName,
                                                         LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                                         LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                                     },
                                                     BizSegment = new BizSegmentModel()
                                                     {
                                                         ID = a.BizSegment.BizSegmentID,
                                                         Name = a.BizSegment.BizSegmentName,
                                                         Description = a.BizSegment.BizSegmentDescription,
                                                         LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                         LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                     },

                                                     transactiontypeID = a.TransactionTypeID != null ? a.TransactionTypeID : 0,
                                                     IsTopUrgent = a.IsTopUrgent,

                                                     Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                                                     {
                                                         ID = x.TransactionDocumentID,
                                                         Type = new DocumentTypeModel()
                                                         {
                                                             ID = x.DocumentType.DocTypeID,
                                                             Name = x.DocumentType.DocTypeName,
                                                             Description = x.DocumentType.DocTypeDescription,
                                                             LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                             LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                         },
                                                         Purpose = new DocumentPurposeModel()
                                                         {
                                                             ID = x.DocumentPurpose.PurposeID,
                                                             Name = x.DocumentPurpose.PurposeName,
                                                             Description = x.DocumentPurpose.PurposeDescription,
                                                             LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                             LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                         },
                                                         FileName = x.Filename,
                                                         DocumentPath = x.DocumentPath,
                                                         LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                         LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value,
                                                         IsDormant = x.IsDormant == null ? false : x.IsDormant
                                                     }).ToList(),
                                                     CreateDate = a.CreateDate,
                                                     LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy
                                                 }).SingleOrDefault();

                if (datatransaction != null)
                {
                    //datatransaction.IsOtherBeneBank = datatransaction.Bank.Code != "999" ? false : true;
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    // changed, chandra : 2015.03.23
                    if (customerRepo.GetCustomerByTransaction(datatransaction.ID, ref customer, ref message))
                    {
                        datatransaction.Customer = customer;

                    }
                    if (datatransaction.FunctionType != 0)
                    {
                        string functiontypename = context.ParameterSystems.Where(a => a.ParsysID == datatransaction.FunctionType).Select(a => a.ParameterValue).FirstOrDefault();
                        datatransaction.strfunctionType = functiontypename;
                    }
                    if (datatransaction.FunctionType == null)
                    {
                        datatransaction.FunctionType = 0;
                    }
                    if (datatransaction.AccountType != null)
                    {
                        string AccountTypeName = context.ParameterSystems.Where(a => a.ParsysID == datatransaction.AccountType).Select(a => a.ParameterValue).FirstOrDefault();
                        datatransaction.strAccountType = AccountTypeName;
                    }
                    if (datatransaction.IsFNACore == true)
                    {
                        datatransaction.strFNACore = "YES";
                    }
                    if (datatransaction.IsFNACore == false)
                    {
                        datatransaction.strFNACore = "NO";
                    }
                    if (datatransaction.transactiontypeID != null)
                    {
                        TransactionTypeParameterModel TransactionTypeData = (from tt in context.TransactionTypes
                                                                             where tt.TransTypeID.Equals(datatransaction.transactiontypeID.Value)
                                                                             select new TransactionTypeParameterModel
                                                                             {
                                                                                 TransTypeID = tt.TransTypeID,
                                                                                 TransactionTypeName = tt.TransactionType1
                                                                             }).SingleOrDefault();

                        datatransaction.TransactionType = TransactionTypeData;

                    }
                    if (datatransaction.transactiontypeID != null)
                    {
                        string TransactionTypeName = context.TransactionTypes.Where(a => a.TransTypeID == datatransaction.transactiontypeID).Select(a => a.TransactionType1).FirstOrDefault();
                        datatransaction.strTransactiontype = TransactionTypeName;
                    }
                    if (datatransaction.transactiontypeID == 0)
                    {
                        datatransaction.strTransactiontype = " ";
                    }
                    customerRepo.Dispose();
                    customer = null;
                }

                output = datatransaction;

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        #endregion

        #region Dani
        public bool GetTransactionCheckerDetailForBranchMaker(Guid workflowInstanceID, long approverID, ref ReSubmitBranchMakerDetailModel output, ref string message)
        {
            bool isSuccess = false;
            try
            {
                output = (from trans in context.Transactions.AsNoTracking()
                          where trans.WorkflowInstanceID.Value == workflowInstanceID
                          select new ReSubmitBranchMakerDetailModel
                          {
                              Verify = trans.TransactionCheckers
                                  .Where(x => x.TransactionID.Equals(trans.TransactionID) && x.IsDeleted == false)
                                  .Select(x => new VerifyModel()
                                  {
                                      ID = x.TransactionColumnID,
                                      Name = x.TransactionColumn.ColumnName,
                                      IsVerified = x.IsVerified
                                  }).ToList()
                          }).SingleOrDefault();
                if (output != null)
                {
                    if (!output.Verify.Any())
                    {
                        output.Verify = context.TransactionColumns
                            .Where(x => x.IsBranchCheckerUTProduct.Value.Equals(true))
                            .Where(x => x.IsDeleted.Equals(false))
                            .Select(x => new VerifyModel()
                            {
                                ID = x.TransactionColumnID,
                                Name = x.ColumnName,
                                IsVerified = x.IsBranchCheckerUTProduct.Value
                            }).ToList();
                    }
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTransactionCheckerDetailForCBOUTMaker(Guid workflowInstanceID, long approverID, ref ResubmitCBOUTMakerModel output, ref string message)
        {
            bool isSuccess = false;
            try
            {
                output = (from trans in context.Transactions.AsNoTracking()
                          where trans.WorkflowInstanceID.Value == workflowInstanceID
                          select new ResubmitCBOUTMakerModel
                          {
                              Verify = trans.TransactionCBOUTCheckers
                                  .Where(x => x.TransactionID.Equals(trans.TransactionID) && x.IsDeleted == false)
                                  .Select(x => new VerifyModel()
                                  {
                                      ID = x.TransactionColumnID,
                                      Name = x.TransactionColumn.ColumnName,
                                      IsVerified = x.IsVerified
                                  }).ToList()
                          }).SingleOrDefault();
                if (output != null)
                {
                    if (!output.Verify.Any())
                    {
                        output.Verify = context.TransactionColumns
                            .Where(x => x.IsCBOUTCheckerIDInvestment.Value.Equals(true))
                            .Where(x => x.IsDeleted.Equals(false))
                            .Select(x => new VerifyModel()
                            {
                                ID = x.TransactionColumnID,
                                Name = x.ColumnName,
                                IsVerified = false
                            }).ToList();
                    }
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTransactionMutualFundBranchMaker(Guid workflowInstanceID, ref IList<MutualFundBranchMakerModel> MutualFund, ref string message)
        {
            var TransactionTypeID = (from a in context.Transactions
                                     where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                                     select a.TransactionTypeID).FirstOrDefault();
            bool IsSuccess = false;
            var isSwitching = false;
            try
            {
                context.Transactions.AsNoTracking();
                if (TransactionTypeID != null)
                {
                    if (TransactionTypeID == (int)UTSwitching.onShore || TransactionTypeID == (int)UTSwitching.ofShore || TransactionTypeID == (int)UTSwitching.cpf)
                    {
                        isSwitching = true;
                    }
                }
                if (isSwitching)
                {
                    MutualFund = (from Trans in context.Transactions
                                  join Checker in context.MutualFundTransactionBranchCheckers on Trans.TransactionID equals Checker.TransactionID
                                  join FundTrans in context.MutualFundTransactions on Checker.MutualFundTransactionID equals FundTrans.MutualFundTransactionID
                                  join Funds in context.Funds on FundTrans.FundID equals Funds.FundID
                                  join Fundto in context.Funds on FundTrans.SwitchToFundID equals Fundto.FundID
                                  where Trans.WorkflowInstanceID == workflowInstanceID
                                  select new MutualFundBranchMakerModel
                                  {
                                      TransactionID = Trans.TransactionID,
                                      ApproverID = Trans.ApproverID ?? null,
                                      IsVerified = Checker.IsVerified != null ? Checker.IsVerified : false,
                                      MutualAmount = FundTrans.Amount ?? 0,
                                      MutualCurrency = new CurrencyModel()
                                      {
                                          ID = FundTrans.Currency.CurrencyID != null ? FundTrans.Currency.CurrencyID : 0,
                                          Code = string.IsNullOrEmpty(FundTrans.Currency.CurrencyCode) ? "-" : FundTrans.Currency.CurrencyCode,
                                          Description = string.IsNullOrEmpty(FundTrans.Currency.CurrencyDescription) ? "-" : FundTrans.Currency.CurrencyDescription
                                      },
                                      MutualFundList = new CBOFundModel()
                                      {
                                          ID = Funds.FundID != null ? Funds.FundID : 0,
                                          FundCode = string.IsNullOrEmpty(Funds.FundCode) ? "-" : Funds.FundCode,
                                          FundName = string.IsNullOrEmpty(Funds.FundName) ? "" : Funds.FundName,
                                          AccountNo = string.IsNullOrEmpty(Funds.AccountNumber) ? "-" : Funds.AccountNumber,
                                          IsSaving = Funds.IsSaving ?? null,
                                          IsSwitching = Funds.IsSwitching ?? null,
                                          SavingPlanDate = Funds.SavingPlanDate ?? null,
                                      },
                                      MutualFundName = string.IsNullOrEmpty(Funds.FundName) ? "-" : Funds.FundName,
                                      MutualUnitNumber = FundTrans.NumberofUnit ?? 0,
                                      MutualPartial = FundTrans.IsPartial ?? null,
                                      MutualFundSwitchTo = new CBOFundModel()
                                      {
                                          ID = FundTrans.SwitchToFundID ?? 0,
                                          FundCode = string.IsNullOrEmpty(Fundto.FundCode) ? "-" : Fundto.FundCode,
                                          FundName = string.IsNullOrEmpty(Fundto.FundName) ? "-" : Fundto.FundName,
                                          AccountNo = string.IsNullOrEmpty(Fundto.AccountNumber) ? "-" : Fundto.AccountNumber,
                                          IsSaving = Fundto.IsSaving ?? null,
                                          IsSwitching = Fundto.IsSwitching ?? null,
                                          SavingPlanDate = Fundto.SavingPlanDate ?? null,
                                      },
                                      MutualFundSwitchFrom = new CBOFundModel()
                                      {
                                          ID = FundTrans.FundID != null ? FundTrans.FundID : 0,
                                          FundCode = string.IsNullOrEmpty(Funds.FundCode) ? "-" : Funds.FundCode,
                                          FundName = string.IsNullOrEmpty(Funds.FundName) ? "-" : Funds.FundName,
                                          AccountNo = string.IsNullOrEmpty(Funds.AccountNumber) ? "-" : Funds.AccountNumber,
                                          IsSaving = Funds.IsSaving ?? null,
                                          IsSwitching = Funds.IsSwitching ?? null,
                                          SavingPlanDate = Funds.SavingPlanDate ?? null,
                                      },
                                      MutualFundTransactionBranchCheckerID = Checker.MutualFundTransactionBranchCheckerID != null ? Checker.MutualFundTransactionBranchCheckerID : 0,
                                      MutualFundTransactionID = FundTrans.MutualFundTransactionID != null ? FundTrans.MutualFundTransactionID : 0,
                                      UTNumber = FundTrans.UTNumber ?? null,
                                      DBNumber = FundTrans.DBNumber ?? null
                                  }).ToList();
                }
                else
                {
                    MutualFund = (from Trans in context.Transactions
                                  join Checker in context.MutualFundTransactionBranchCheckers on Trans.TransactionID equals Checker.TransactionID
                                  join FundTrans in context.MutualFundTransactions on Checker.MutualFundTransactionID equals FundTrans.MutualFundTransactionID
                                  join Funds in context.Funds on FundTrans.FundID equals Funds.FundID
                                  where Trans.WorkflowInstanceID == workflowInstanceID
                                  select new MutualFundBranchMakerModel
                                  {
                                      TransactionID = Trans.TransactionID,
                                      ApproverID = Trans.ApproverID ?? null,
                                      IsVerified = Checker.IsVerified != null ? Checker.IsVerified : false,
                                      MutualAmount = FundTrans.Amount ?? 0,
                                      MutualCurrency = new CurrencyModel()
                                      {
                                          ID = FundTrans.Currency.CurrencyID != null ? FundTrans.Currency.CurrencyID : 0,
                                          Code = string.IsNullOrEmpty(FundTrans.Currency.CurrencyCode) ? "-" : FundTrans.Currency.CurrencyCode,
                                          Description = string.IsNullOrEmpty(FundTrans.Currency.CurrencyDescription) ? "-" : FundTrans.Currency.CurrencyDescription
                                      },
                                      MutualFundList = new CBOFundModel()
                                      {
                                          ID = Funds.FundID != null ? Funds.FundID : 0,
                                          FundCode = string.IsNullOrEmpty(Funds.FundCode) ? "-" : Funds.FundCode,
                                          FundName = string.IsNullOrEmpty(Funds.FundName) ? "" : Funds.FundName,
                                          AccountNo = string.IsNullOrEmpty(Funds.AccountNumber) ? "-" : Funds.AccountNumber,
                                          IsSaving = Funds.IsSaving ?? null,
                                          IsSwitching = Funds.IsSwitching ?? null,
                                          SavingPlanDate = Funds.SavingPlanDate ?? null,
                                      },
                                      MutualFundName = string.IsNullOrEmpty(Funds.FundName) ? "-" : Funds.FundName,
                                      MutualUnitNumber = FundTrans.NumberofUnit ?? 0,
                                      MutualPartial = FundTrans.IsPartial ?? null,
                                      MutualFundSwitchTo = new CBOFundModel()
                                      {
                                          ID = FundTrans.SwitchToFundID ?? 0,
                                          FundCode = string.IsNullOrEmpty(Funds.FundCode) ? "-" : Funds.FundCode,
                                          FundName = string.IsNullOrEmpty(Funds.FundName) ? "-" : Funds.FundName,
                                          AccountNo = string.IsNullOrEmpty(Funds.AccountNumber) ? "-" : Funds.AccountNumber,
                                          IsSaving = Funds.IsSaving ?? null,
                                          IsSwitching = Funds.IsSwitching ?? null,
                                          SavingPlanDate = Funds.SavingPlanDate ?? null,
                                      },
                                      MutualFundSwitchFrom = new CBOFundModel()
                                      {
                                          ID = FundTrans.FundID != null ? FundTrans.FundID : 0,
                                          FundCode = string.IsNullOrEmpty(Funds.FundCode) ? "-" : Funds.FundCode,
                                          FundName = string.IsNullOrEmpty(Funds.FundName) ? "-" : Funds.FundName,
                                          AccountNo = string.IsNullOrEmpty(Funds.AccountNumber) ? "-" : Funds.AccountNumber,
                                          IsSaving = Funds.IsSaving ?? null,
                                          IsSwitching = Funds.IsSwitching ?? null,
                                          SavingPlanDate = Funds.SavingPlanDate ?? null,
                                      },
                                      MutualFundTransactionBranchCheckerID = Checker.MutualFundTransactionBranchCheckerID != null ? Checker.MutualFundTransactionBranchCheckerID : 0,
                                      MutualFundTransactionID = FundTrans.MutualFundTransactionID != null ? FundTrans.MutualFundTransactionID : 0,
                                      UTNumber = FundTrans.UTNumber ?? null,
                                      DBNumber = FundTrans.DBNumber ?? null
                                  }).ToList();
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return IsSuccess;
        }
        public bool GetTransactionJoinAccountBranchMaker(Guid workflowInstanceID, ref IList<JoinAccountBranchMakerModel> JoinAccount, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                context.Transactions.AsNoTracking();
                JoinAccount = (from Trans in context.Transactions
                               join TransJoin in context.JoinAccounts on Trans.TransactionID equals TransJoin.TransactionID
                               join Checker in context.JoinAccountBranchCheckers on TransJoin.JoinAccountID equals Checker.JoinAccountID
                               where Trans.WorkflowInstanceID == workflowInstanceID
                               select new JoinAccountBranchMakerModel
                               {
                                   TransactionID = Trans.TransactionID,
                                   ApproverID = Trans.ApproverID ?? null,
                                   JoinAccountID = TransJoin.JoinAccountID != null ? TransJoin.JoinAccountID : 0,
                                   CustomerJoin = new CustomerModel()
                                   {
                                       CIF = string.IsNullOrEmpty(TransJoin.Customer.CIF) ? "-" : TransJoin.Customer.CIF,
                                       Name = string.IsNullOrEmpty(TransJoin.Customer.CustomerName) ? "-" : TransJoin.Customer.CustomerName
                                   },
                                   CustomerRiskEffectiveDateJoin = TransJoin.RiskEffectiveDate ?? null,
                                   IsVerified = Checker.IsVerified != null ? Checker.IsVerified : false,
                                   JoinAccountBranchCheckerID = Checker.JoinAccountBranchCheckerID != null ? Checker.JoinAccountBranchCheckerID : 0,
                                   JoinOrder = TransJoin.JoinOrder,
                                   Join = new JoinModel
                                   {
                                       ID = TransJoin.JoinType,
                                       Name = TransJoin.JoinType == false ? "And" : "Or"
                                   },
                                   RiskProfileExpiryDateJoin = TransJoin.RiskProfileExpiryDate ?? null,
                                   RiskScoreJoin = TransJoin.RiskScore ?? 0,
                                   SolIDJoin = string.IsNullOrEmpty(TransJoin.SolID) ? "" : TransJoin.SolID
                               }).ToList();
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return IsSuccess;
        }
        public bool UpdateTransactionResubmitByBranchMaker(ReSubmitBranchMakerDetailModel ut, ref string message)
        {
            bool isSuccess = false;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.Transaction data = context.Transactions.Where(a => a.TransactionID.Equals(ut.Transaction.ID)).SingleOrDefault();
                    if (data != null)
                    {
                        // Remark Product ID Gak Boleh Diupdate                   
                        //data.ApplicationID = string.IsNullOrEmpty(ut.Transaction.ApplicationID) ? "" : ut.Transaction.ApplicationID;
                        //data.WorkflowInstanceID = ut.Transaction.WorkflowInstanceID;
                        //data.TransactionID = ut.Transaction.ID;
                        data.ApproverID = ut.Transaction.ApproverID ?? null;
                        data.ChannelID = ut.Transaction.Channel.ID;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        //data.ApplicationDate = DateTime.Now;
                        data.IsNewCustomer = ut.Transaction.IsNewCustomer;
                        //data.CIF = string.IsNullOrEmpty(ut.Transaction.Customer.CIF) ? "" : ut.Transaction.Customer.CIF;
                        //data.ProductID = ut.Transaction.Product.ID > 0 ? ut.Transaction.Product.ID : 1;
                        data.StateID = (int)StateID.OnProgress;
                        data.Amount = ut.Transaction.Amount > 0 ? ut.Transaction.Amount : 0;
                        if (ut.Transaction.TransactionType != null)
                        {
                            data.TransactionTypeID = ut.Transaction.TransactionType.TransTypeID > 0 ? ut.Transaction.TransactionType.TransTypeID : 1;
                        }
                        //Data Investment
                        data.IsFNACore = ut.Transaction.IsFNACore;
                        data.InvestmentID = ut.Transaction.InvestmentID;
                        data.FunctionType = ut.Transaction.FunctionType;
                        data.AccountType = ut.Transaction.AccountType;
                        data.SolID = ut.Transaction.SolID;
                        data.CRiskEfectiveDate = ut.Transaction.CRiskEfectiveDate;
                        data.RiskScore = ut.Transaction.RiskScore;
                        data.RiskProfileExpiryDate = ut.Transaction.RiskProfileExpiryDate;
                        data.OperativeAccount = ut.Transaction.OperativeAccount;
                        data.AttachmentRemarks = ut.Transaction.AttachmentRemarks;// add by adi

                        context.SaveChanges();
                    }

                    #region Joint Account 2
                    if (ut.JoinAccount.Count > 0)
                    {
                        var DeleteJoinBranchChecker = (from x in context.JoinAccountBranchCheckers
                                                       where x.TransactionID == ut.Transaction.ID
                                                       select x);
                        if (DeleteJoinBranchChecker != null && DeleteJoinBranchChecker.ToList().Count > 0)
                        {
                            foreach (var itemjoinbranch in DeleteJoinBranchChecker.ToList())
                            {
                                context.JoinAccountBranchCheckers.Remove(itemjoinbranch);

                            }
                            context.SaveChanges();
                        }

                        var DeleteJoinCBOChecker = (from x in context.JoinAccountCBOCheckers
                                                    where x.TransactionID == ut.Transaction.ID
                                                    select x);
                        if (DeleteJoinCBOChecker != null && DeleteJoinCBOChecker.ToList().Count > 0)
                        {
                            foreach (var itemjoincbo in DeleteJoinCBOChecker.ToList())
                            {
                                context.JoinAccountCBOCheckers.Remove(itemjoincbo);

                            }
                            context.SaveChanges();
                        }

                        var DeleteJoin = (from x in context.JoinAccounts
                                          where x.TransactionID == ut.Transaction.ID
                                          select x);
                        if (DeleteJoin != null && DeleteJoin.ToList().Count > 0)
                        {
                            foreach (var itemjoin2 in DeleteJoin.ToList())
                            {
                                context.JoinAccounts.Remove(itemjoin2);

                            }
                            context.SaveChanges();
                        }

                        foreach (var itemjoin in ut.JoinAccount)
                        {
                            //if (itemjoin.Flag == 1)
                            //{
                            DBS.Entity.JoinAccount JA = new DBS.Entity.JoinAccount()
                            {
                                TransactionID = ut.Transaction.ID,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName,
                                SolID = string.IsNullOrEmpty(itemjoin.SolIDJoin) ? "" : itemjoin.SolIDJoin,
                                RiskScore = itemjoin.RiskScoreJoin ?? null,
                                RiskProfileExpiryDate = itemjoin.RiskProfileExpiryDateJoin ?? null,
                                RiskEffectiveDate = itemjoin.CustomerRiskEffectiveDateJoin ?? null,
                                JoinOrder = itemjoin.JoinOrder ?? 0,
                                JoinType = itemjoin.Join.ID,
                                CIF = string.IsNullOrEmpty(itemjoin.CustomerJoin.CIF) ? "" : itemjoin.CustomerJoin.CIF
                            };
                            context.JoinAccounts.Add(JA);
                            context.SaveChanges();

                            DBS.Entity.JoinAccountBranchChecker JB = new DBS.Entity.JoinAccountBranchChecker()
                            {
                                TransactionID = ut.Transaction.ID,
                                ApproverID = ut.Transaction.ApproverID.Value,
                                JoinAccountID = JA.JoinAccountID,
                                IsVerified = false,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName,
                            };
                            context.JoinAccountBranchCheckers.Add(JB);
                            context.SaveChanges();
                            //}
                            //if (itemjoin.Flag == 2)
                            //{
                            //    var updatejoin = context.JoinAccounts.Where(a => a.JoinAccountID.Equals(itemjoin.JoinAccountID)).SingleOrDefault();
                            //    updatejoin.TransactionID = ut.Transaction.ID;
                            //    updatejoin.CreateDate = DateTime.UtcNow;
                            //    updatejoin.CreateBy = currentUser.GetCurrentUser().LoginName;
                            //    updatejoin.SolID = string.IsNullOrEmpty(itemjoin.SolIDJoin) ? "" : itemjoin.SolIDJoin;
                            //    updatejoin.RiskScore = itemjoin.RiskScoreJoin ?? null;
                            //    updatejoin.RiskProfileExpiryDate = itemjoin.RiskProfileExpiryDateJoin ?? null;
                            //    updatejoin.RiskEffectiveDate = itemjoin.CustomerRiskEffectiveDateJoin ?? null;
                            //    updatejoin.JoinOrder = itemjoin.JoinOrder ?? 0;
                            //    updatejoin.JoinType = itemjoin.Join.ID;
                            //    updatejoin.CIF = string.IsNullOrEmpty(itemjoin.CustomerJoin.CIF) ? "" : itemjoin.CustomerJoin.CIF;
                            //    context.SaveChanges();
                            //}
                        }
                    }
                    #endregion

                    #region Save Document
                    var existingDocs = context.TransactionDocuments.Where(a => a.TransactionID.Equals(ut.Transaction.ID) && a.IsDeleted == false).ToList();
                    if (existingDocs != null)
                    {
                        if (existingDocs.Count() > 0)
                        {
                            foreach (var item in existingDocs)
                            {
                                var deleteDoc = context.TransactionDocuments.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                deleteDoc.IsDeleted = true;
                                context.SaveChanges();
                            }
                        }
                    }
                    if (ut.Transaction.Documents.Count > 0)
                    {
                        foreach (var item in ut.Transaction.Documents)
                        {
                            Entity.TransactionDocument document = new Entity.TransactionDocument()
                            {
                                TransactionID = data.TransactionID,
                                DocTypeID = item.Type.ID,
                                PurposeID = item.Purpose.ID,
                                Filename = item.FileName,
                                DocumentPath = item.DocumentPath,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName
                            };
                            context.TransactionDocuments.Add(document);
                        }
                        context.SaveChanges();
                    }
                    #endregion


                    if (ut.MutualFund.Count > 0)
                    {
                        foreach (var item in ut.MutualFund)
                        {
                            if (item.Flag == 1)//tambah baru
                            {
                                if (item.MutualCurrency.ID != 0)
                                {
                                    DBS.Entity.MutualFundTransaction MF = new DBS.Entity.MutualFundTransaction()
                                    {
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName,
                                        TransactionID = ut.Transaction.ID,//item.TransactionID ?? 0,
                                        SwitchToFundID = item.MutualFundSwitchTo.ID != null ? item.MutualFundSwitchTo.ID : 0,
                                        NumberofUnit = item.MutualUnitNumber ?? null,
                                        IsPartial = item.MutualPartial ?? null,
                                        FundID = item.MutualFundList.ID != null ? item.MutualFundList.ID : 0,
                                        CurrencyID = item.MutualCurrency.ID,
                                        Amount = item.MutualAmount ?? 0
                                    };
                                    context.MutualFundTransactions.Add(MF);
                                    context.SaveChanges();

                                    context.MutualFundTransactionBranchCheckers.Add(new MutualFundTransactionBranchChecker()
                                    {
                                        ApproverID = null,
                                        TransactionID = ut.Transaction.ID,
                                        IsVerified = false,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName,
                                        UpdateBy = null,
                                        UpdateDate = null,
                                        MutualFundTransactionID = MF.MutualFundTransactionID
                                    });
                                }
                                else
                                {
                                    DBS.Entity.MutualFundTransaction MF = new DBS.Entity.MutualFundTransaction()
                                    {
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName,
                                        TransactionID = ut.Transaction.ID,//item.TransactionID ?? 0,
                                        SwitchToFundID = item.MutualFundSwitchTo.ID != null ? item.MutualFundSwitchTo.ID : 0,
                                        NumberofUnit = item.MutualUnitNumber ?? null,
                                        IsPartial = item.MutualPartial ?? null,
                                        FundID = item.MutualFundList.ID != null ? item.MutualFundList.ID : 0,
                                        CurrencyID = null,
                                        Amount = item.MutualAmount ?? 0
                                    };
                                    context.MutualFundTransactions.Add(MF);
                                    context.SaveChanges();

                                    context.MutualFundTransactionBranchCheckers.Add(new MutualFundTransactionBranchChecker()
                                    {
                                        ApproverID = null,
                                        TransactionID = ut.Transaction.ID,
                                        IsVerified = false,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName,
                                        UpdateBy = null,
                                        UpdateDate = null,
                                        MutualFundTransactionID = MF.MutualFundTransactionID
                                    });
                                }

                                context.SaveChanges();
                            }
                            if (item.Flag == 2)//update
                            {
                                if (item.MutualCurrency.ID != 0)
                                {
                                    if (item.MutualFundTransactionID != null)
                                    {
                                        var updateMutual = context.MutualFundTransactions.Where(a => a.MutualFundTransactionID.Equals(item.MutualFundTransactionID.Value)).SingleOrDefault();
                                        if (updateMutual == null)
                                        {
                                            DBS.Entity.MutualFundTransaction MF = new DBS.Entity.MutualFundTransaction()
                                            {
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName,
                                                TransactionID = ut.Transaction.ID,//item.TransactionID ?? 0,
                                                SwitchToFundID = item.MutualFundSwitchTo.ID != null ? item.MutualFundSwitchTo.ID : 0,
                                                NumberofUnit = item.MutualUnitNumber ?? null,
                                                IsPartial = item.MutualPartial ?? null,
                                                FundID = item.MutualFundList.ID != null ? item.MutualFundList.ID : 0,
                                                CurrencyID = item.MutualCurrency.ID,
                                                Amount = item.MutualAmount ?? 0
                                            };
                                            context.MutualFundTransactions.Add(MF);
                                            context.SaveChanges();

                                            context.MutualFundTransactionBranchCheckers.Add(new MutualFundTransactionBranchChecker()
                                            {
                                                ApproverID = null,
                                                TransactionID = ut.Transaction.ID,
                                                IsVerified = false,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName,
                                                UpdateBy = null,
                                                UpdateDate = null,
                                                MutualFundTransactionID = MF.MutualFundTransactionID
                                            });
                                        }
                                        else
                                        {
                                            updateMutual.UpdateDate = DateTime.UtcNow;
                                            updateMutual.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                            updateMutual.SwitchToFundID = item.MutualFundSwitchTo.ID != null ? item.MutualFundSwitchTo.ID : 0;
                                            updateMutual.NumberofUnit = item.MutualUnitNumber ?? null;
                                            updateMutual.IsPartial = item.MutualPartial ?? null;
                                            if (item.MutualFundSwitchFrom.ID != 0)
                                            {
                                                updateMutual.FundID = item.MutualFundSwitchFrom.ID != null ? item.MutualFundSwitchFrom.ID : 0;
                                            }
                                            else
                                            {
                                                updateMutual.FundID = item.MutualFundList.ID != null ? item.MutualFundList.ID : 0;
                                            }
                                            updateMutual.CurrencyID = item.MutualCurrency.ID;
                                            updateMutual.Amount = item.MutualAmount ?? 0;
                                            context.SaveChanges();

                                            var updateMutualChecker = context.MutualFundTransactionBranchCheckers.Where(a => a.MutualFundTransactionBranchCheckerID.Equals(item.MutualFundTransactionBranchCheckerID.Value)).SingleOrDefault();
                                            updateMutualChecker.ApproverID = null;
                                            updateMutualChecker.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                            updateMutualChecker.UpdateDate = DateTime.UtcNow;
                                            context.SaveChanges();
                                        }
                                    }
                                    
                                }
                                else
                                {
                                    if (item.MutualCurrency.ID != 0)
                                    {
                                        DBS.Entity.MutualFundTransaction MF = new DBS.Entity.MutualFundTransaction()
                                        {
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName,
                                            TransactionID = ut.Transaction.ID,//item.TransactionID ?? 0,
                                            SwitchToFundID = item.MutualFundSwitchTo.ID != null ? item.MutualFundSwitchTo.ID : 0,
                                            NumberofUnit = item.MutualUnitNumber ?? null,
                                            IsPartial = item.MutualPartial ?? null,
                                            FundID = item.MutualFundList.ID != null ? item.MutualFundList.ID : 0,
                                            CurrencyID = item.MutualCurrency.ID,
                                            Amount = item.MutualAmount ?? 0
                                        };
                                        context.MutualFundTransactions.Add(MF);
                                        context.SaveChanges();

                                        context.MutualFundTransactionBranchCheckers.Add(new MutualFundTransactionBranchChecker()
                                        {
                                            ApproverID = null,
                                            TransactionID = ut.Transaction.ID,
                                            IsVerified = false,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName,
                                            UpdateBy = null,
                                            UpdateDate = null,
                                            MutualFundTransactionID = MF.MutualFundTransactionID
                                        });
                                        context.SaveChanges();
                                    }
                                    else
                                    {
                                        if (item.MutualFundTransactionID != null)
                                        {
                                            var updateMutual = context.MutualFundTransactions.Where(a => a.MutualFundTransactionID.Equals(item.MutualFundTransactionID.Value)).SingleOrDefault();

                                            if (updateMutual != null)
                                            {
                                                updateMutual.UpdateDate = DateTime.UtcNow;
                                                updateMutual.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                                updateMutual.SwitchToFundID = item.MutualFundSwitchTo.ID != null ? item.MutualFundSwitchTo.ID : 0;
                                                updateMutual.NumberofUnit = item.MutualUnitNumber ?? null;
                                                updateMutual.IsPartial = item.MutualPartial ?? null;
                                                if (item.MutualFundSwitchFrom.ID != 0)
                                                {
                                                    updateMutual.FundID = item.MutualFundSwitchFrom.ID != null ? item.MutualFundSwitchFrom.ID : 0;
                                                }
                                                else
                                                {
                                                    updateMutual.FundID = item.MutualFundList.ID != null ? item.MutualFundList.ID : 0;
                                                }
                                                // updateMutual.CurrencyID = item.MutualCurrency.ID;
                                                updateMutual.Amount = item.MutualAmount ?? 0;
                                                context.SaveChanges();

                                                var updateMutualChecker = context.MutualFundTransactionBranchCheckers.Where(a => a.MutualFundTransactionBranchCheckerID.Equals(item.MutualFundTransactionBranchCheckerID.Value)).SingleOrDefault();
                                                updateMutualChecker.ApproverID = null;
                                                updateMutualChecker.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                                updateMutualChecker.UpdateDate = DateTime.UtcNow;
                                                context.SaveChanges();
                                            }
                                            else
                                            {
                                                DBS.Entity.MutualFundTransaction MF = new DBS.Entity.MutualFundTransaction()
                                                {
                                                    CreateDate = DateTime.UtcNow,
                                                    CreateBy = currentUser.GetCurrentUser().LoginName,
                                                    TransactionID = ut.Transaction.ID,//item.TransactionID ?? 0,
                                                    SwitchToFundID = item.MutualFundSwitchTo.ID != null ? item.MutualFundSwitchTo.ID : 0,
                                                    NumberofUnit = item.MutualUnitNumber ?? null,
                                                    IsPartial = item.MutualPartial ?? null,
                                                    FundID = item.MutualFundList.ID != null ? item.MutualFundList.ID : 0,
                                                    CurrencyID = item.MutualCurrency.ID,
                                                    Amount = item.MutualAmount ?? 0
                                                };
                                                context.MutualFundTransactions.Add(MF);
                                                context.SaveChanges();

                                                context.MutualFundTransactionBranchCheckers.Add(new MutualFundTransactionBranchChecker()
                                                {
                                                    ApproverID = null,
                                                    TransactionID = ut.Transaction.ID,
                                                    IsVerified = false,
                                                    CreateDate = DateTime.UtcNow,
                                                    CreateBy = currentUser.GetCurrentUser().LoginName,
                                                    UpdateBy = null,
                                                    UpdateDate = null,
                                                    MutualFundTransactionID = MF.MutualFundTransactionID
                                                });
                                                context.SaveChanges();
                                            }
                                        }
                                        else { //handle if add new mutual fund and then edit data. behavior like add new

                                            DBS.Entity.MutualFundTransaction MF = new DBS.Entity.MutualFundTransaction()
                                            {
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName,
                                                TransactionID = ut.Transaction.ID,//item.TransactionID ?? 0,
                                                SwitchToFundID = item.MutualFundSwitchTo.ID != null ? item.MutualFundSwitchTo.ID : 0,
                                                NumberofUnit = item.MutualUnitNumber ?? null,
                                                IsPartial = item.MutualPartial ?? null,
                                                FundID = item.MutualFundList.ID != null ? item.MutualFundList.ID : 0,
                                                Amount = item.MutualAmount ?? 0
                                            };
                                            if (item.MutualCurrency.ID != 0) {
                                                MF.CurrencyID = item.MutualCurrency.ID;
                                            }
                                            context.MutualFundTransactions.Add(MF);
                                            context.SaveChanges();

                                            context.MutualFundTransactionBranchCheckers.Add(new MutualFundTransactionBranchChecker()
                                            {
                                                ApproverID = null,
                                                TransactionID = ut.Transaction.ID,
                                                IsVerified = false,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName,
                                                UpdateBy = null,
                                                UpdateDate = null,
                                                MutualFundTransactionID = MF.MutualFundTransactionID
                                            });
                                            context.SaveChanges();
                                        }                                         
                                    }
                                }
                            }
                        }
                    }

                    ts.Complete();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }
            return isSuccess;
        }
        public bool GetMutualFundTransactionForCBOMaker(Guid workflowInstanceID, ref IList<MutualFundCBOUTMakerModel> MutualFund, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                context.Transactions.AsNoTracking();
                MutualFund = (from Trans in context.Transactions
                              join CBOChecker in context.MutualFundTransactionCBOCheckers on Trans.TransactionID equals CBOChecker.TransactionID
                              join FundTrans in context.MutualFundTransactions on CBOChecker.MutualFundTransactionID equals FundTrans.MutualFundTransactionID
                              join Funds in context.Funds on FundTrans.FundID equals Funds.FundID
                              where Trans.WorkflowInstanceID == workflowInstanceID
                              select new MutualFundCBOUTMakerModel 
                              {
                                  TransactionID = Trans.TransactionID,
                                  ApproverID = Trans.ApproverID ?? null,
                                  Amount = FundTrans.Amount ?? 0,
                                  IsVerified = CBOChecker.IsVerified != null ? CBOChecker.IsVerified : false,
                                  VerifiedMutualFund = CBOChecker.IsVerified != null ? CBOChecker.IsVerified: false,
                                  MutualAmount = FundTrans.Amount ?? 0,
                                  MutualCurrency = new CurrencyModel()
                                  {
                                      ID = FundTrans.Currency.CurrencyID != null ? FundTrans.Currency.CurrencyID : 0,
                                      Code = string.IsNullOrEmpty(FundTrans.Currency.CurrencyCode) ? "-" : FundTrans.Currency.CurrencyCode,
                                      Description = string.IsNullOrEmpty(FundTrans.Currency.CurrencyDescription) ? "-" : FundTrans.Currency.CurrencyDescription
                                  },
                                  MutualFundList = new CBOFundModel()
                                  {
                                      ID = Funds.FundID != null ? Funds.FundID : 0,
                                      FundCode = string.IsNullOrEmpty(Funds.FundCode) ? "-" : Funds.FundCode,
                                      FundName = string.IsNullOrEmpty(Funds.FundName) ? "" : Funds.FundName,
                                      AccountNo = string.IsNullOrEmpty(Funds.AccountNumber) ? "-" : Funds.AccountNumber,
                                      IsSaving = Funds.IsSaving ?? null,
                                      IsSwitching = Funds.IsSwitching ?? null,
                                      SavingPlanDate = Funds.SavingPlanDate ?? null,
                                  },
                                  MutualFundName = string.IsNullOrEmpty(Funds.FundName) ? "-" : Funds.FundName,
                                  MutualFundCode = string.IsNullOrEmpty(Funds.FundCode) ? "-" : Funds.FundCode,
                                  MutualUnitNumber = FundTrans.NumberofUnit ?? 0,
                                  NumberofUnit = FundTrans.NumberofUnit ?? 0,
                                  MutualPartial = FundTrans.IsPartial ?? null,
                                  partial = FundTrans.IsPartial ?? null,
                                  MutualFundSwitchTo = new CBOFundModel()
                                  {
                                      ID = FundTrans.SwitchToFundID ?? 0,
                                      FundCode = string.IsNullOrEmpty(Funds.FundCode) ? "-" : Funds.FundCode,
                                      FundName = string.IsNullOrEmpty(Funds.FundName) ? "-" : Funds.FundName,
                                      AccountNo = string.IsNullOrEmpty(Funds.AccountNumber) ? "-" : Funds.AccountNumber,
                                      IsSaving = Funds.IsSaving ?? null,
                                      IsSwitching = Funds.IsSwitching ?? null,
                                      SavingPlanDate = Funds.SavingPlanDate ?? null,
                                  },
                                  MutualFundSwitchFrom = new CBOFundModel()
                                  {
                                      ID = FundTrans.FundID != null ? FundTrans.FundID : 0,
                                      FundCode = string.IsNullOrEmpty(Funds.FundCode) ? "-" : Funds.FundCode,
                                      FundName = string.IsNullOrEmpty(Funds.FundName) ? "-" : Funds.FundName,
                                      AccountNo = string.IsNullOrEmpty(Funds.AccountNumber) ? "-" : Funds.AccountNumber,
                                      IsSaving = Funds.IsSaving ?? null,
                                      IsSwitching = Funds.IsSwitching ?? null,
                                      SavingPlanDate = Funds.SavingPlanDate ?? null,
                                  },
                                  MutualFundTransactionCBOCheckerID = CBOChecker.MutualFundTransactionCBOCheckerID != null ? CBOChecker.MutualFundTransactionCBOCheckerID : 0,
                                  MutualFundTransactionID = FundTrans.MutualFundTransactionID != null ? FundTrans.MutualFundTransactionID : 0,
                                  Currency = string.IsNullOrEmpty(FundTrans.Currency.CurrencyCode) ? "-" : FundTrans.Currency.CurrencyCode,
                                  FromStrswitchmutualFund = string.IsNullOrEmpty(Funds.FundName) ? "-" : Funds.FundName,
                                  FromCodeswitchmutualfund = string.IsNullOrEmpty(Funds.FundCode) ? "-" : Funds.FundCode,
                                  TOStrswitchmutualFund = string.IsNullOrEmpty(Funds.FundName) ? "-" : Funds.FundName,
                                  TOCodeswitchmutualfund = string.IsNullOrEmpty(Funds.FundCode) ? "-" : Funds.FundCode,
                                  UTNumber = FundTrans.UTNumber ?? null,
                                  DBNumber = FundTrans.DBNumber ?? null
                              }).ToList();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return IsSuccess;
        }
        public bool GetJoinAccountTransactionForCBOMaker(Guid workflowInstanceID, ref IList<JoinAccountCBOUTMakerModel> JoinAccount, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                context.Transactions.AsNoTracking();
                JoinAccount = (from Trans in context.Transactions
                               join CBOChecker in context.JoinAccountCBOCheckers on Trans.TransactionID equals CBOChecker.TransactionID
                               join TransJoin in context.JoinAccounts on Trans.TransactionID equals TransJoin.TransactionID
                               where Trans.WorkflowInstanceID == workflowInstanceID
                               select new JoinAccountCBOUTMakerModel
                               {
                                   TransactionID = Trans.TransactionID,
                                   ApproverID = Trans.ApproverID ?? null,
                                   JoinAccountID = TransJoin.JoinAccountID != null ? TransJoin.JoinAccountID : 0,
                                   CustomerJoin = new CustomerModel()
                                   {
                                       CIF = string.IsNullOrEmpty(TransJoin.Customer.CIF) ? "-" : TransJoin.Customer.CIF,
                                       Name = string.IsNullOrEmpty(TransJoin.Customer.CustomerName) ? "-" : TransJoin.Customer.CustomerName
                                   },
                                   CustomerRiskEffectiveDateJoin = TransJoin.RiskEffectiveDate ?? null,
                                   IsVerified = CBOChecker.IsVerified != null ? CBOChecker.IsVerified : false,
                                   JoinAccountCBOCheckerID = CBOChecker.JoinAccountCBOCheckerID != null ? CBOChecker.JoinAccountCBOCheckerID : 0,
                                   JoinOrder = TransJoin.JoinOrder,
                                   Join = new JoinModel
                                   {
                                       ID = TransJoin.JoinType,
                                       Name = TransJoin.JoinType == false ? "And" : "Or"
                                   },
                                   RiskProfileExpiryDateJoin = TransJoin.RiskProfileExpiryDate ?? null,
                                   RiskScoreJoin = TransJoin.RiskScore ?? 0,
                                   SolIDJoin = string.IsNullOrEmpty(TransJoin.SolID) ? "" : TransJoin.SolID
                               }).ToList();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return IsSuccess;
        }
        public bool UpdateTransactionResubmitByCBOUTMaker(ResubmitCBOUTMakerModel ut, ref string message)
        {
            bool isSuccess = false;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.Transaction data = context.Transactions.Where(a => a.TransactionID.Equals(ut.Transaction.ID)).SingleOrDefault();
                    if (data != null)
                    {
                        //Update     
                        data.InvestmentID = string.IsNullOrEmpty(ut.Transaction.InvestmentID) ? "" : ut.Transaction.InvestmentID;
                        data.UTNumber = string.IsNullOrEmpty(ut.Transaction.UTNumber) ? "" : ut.Transaction.UTNumber;
                        data.DBNumber = string.IsNullOrEmpty(ut.Transaction.DBNumber) ? "" : ut.Transaction.DBNumber;
                        data.AttachmentRemarks = string.IsNullOrEmpty(ut.Transaction.AttachmentRemarks) ? "" : ut.Transaction.AttachmentRemarks;
                        context.SaveChanges();
                    }

                    #region save mutual 
                    if (ut.MutualFund != null && ut.MutualFund.Count > 0)
                    {
                        var dataMutualFund = ut.MutualFund;

                        if (ut.Transaction.Product.Code == "UC" || ut.Transaction.Product.Code == "UO" || ut.Transaction.Product.Code == "UF")
                        {
                            if (dataMutualFund.Count > 0)
                            {
                                foreach (var item in dataMutualFund)
                                {
                                    var updateMutual = context.MutualFundTransactions.Where(a => a.MutualFundTransactionID.Equals(item.MutualFundTransactionID.Value)).SingleOrDefault();
                                    updateMutual.UTNumber = item.UTNumber;
                                }
                            }
                        }
                        else
                        {
                            if (ut.Transaction.Product.Code == "SP")
                            {
                                if (dataMutualFund.Count > 0)
                                {
                                    foreach (var item in dataMutualFund)
                                    {
                                        var updateMutualSP = context.MutualFundTransactions.Where(a => a.MutualFundTransactionID.Equals(item.MutualFundTransactionID.Value)).SingleOrDefault();
                                        updateMutualSP.DBNumber = item.DBNumber;
                                    }
                                }
                            }
                        }
                        context.SaveChanges();
                    }
                    #endregion 

                    ts.Complete();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }
            return isSuccess;
        }

        #endregion

        #region Basri
        #endregion

        #region Chandra
        #endregion

        #region Andi
        #endregion

        #region Afif
        public bool GetUTCBOAccountMaker(Guid workflowInstanceId, ref UTCBOAccountMakerModel output, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();
                // get main transaction data
                IList<ParameterSystem> parSys = context.ParameterSystems.ToList();
                output = (from a in context.Transactions
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceId)
                          select new UTCBOAccountMakerModel
                          {
                              ID = a.TransactionID,
                              WorkflowInstanceID = a.WorkflowInstanceID.Value,
                              ApplicationID = a.ApplicationID,
                              ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                              Product = new ProductModel()
                              {
                                  ID = a.Product.ProductID,
                                  Code = a.Product.ProductCode,
                                  Name = a.Product.ProductName,
                                  WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                  Workflow = a.Product.ProductWorkflow.WorkflowName,
                                  LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                  LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                              },
                              Channel = new ChannelModel()
                              {
                                  ID = a.Channel.ChannelID,
                                  Name = a.Channel.ChannelName,
                                  LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                  LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                              },
                              Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                              {
                                  ID = x.TransactionDocumentID,
                                  Type = new DocumentTypeModel()
                                  {
                                      ID = x.DocumentType.DocTypeID,
                                      Name = x.DocumentType.DocTypeName,
                                      Description = x.DocumentType.DocTypeDescription,
                                      LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                      LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                  },
                                  Purpose = new DocumentPurposeModel()
                                  {
                                      ID = x.DocumentPurpose.PurposeID,
                                      Name = x.DocumentPurpose.PurposeName,
                                      Description = x.DocumentPurpose.PurposeDescription,
                                      LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                      LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                  },
                                  FileName = x.Filename,
                                  DocumentPath = x.DocumentPath,
                                  LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                  LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                              }).ToList(),
                              ParameterSystemUT = new ParameterSystemUTModel()
                              {
                                  FNACore = new UTFNACore() { FNACoreID = a.IsFNACore == true ? 17 : 18 },
                                  AccountType = new UTAccountType() { AccountTypeID = a.AccountType.Value },
                                  FunctionType = new UTFunctionType() { FunctionTypeID = a.FunctionType.Value }
                              },
                              SOLID = a.SolID,
                              RiskScore = a.RiskScore.Value,
                              CustomerRiskEffectiveDate = a.CRiskEfectiveDate.HasValue ? a.CRiskEfectiveDate.Value : default(DateTime),
                              RiskProfileExpiryDate = a.RiskProfileExpiryDate.HasValue ? a.RiskProfileExpiryDate.Value : default(DateTime),
                              Remark = a.Remarks,
                              InvestmentID = a.InvestmentID,
                              AttachmentRemarks = a.AttachmentRemarks,
                              OperativeAccount = a.OperativeAccount,
                              CreateDate = a.CreateDate,
                              LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                              LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                          }).SingleOrDefault();

                if (output != null)
                {
                    int _fnaCoreId = output.ParameterSystemUT.FNACore.FNACoreID.Value;
                    output.ParameterSystemUT.FNACore.FNACoreName = (from f in parSys
                                                                    where f.ParsysID == _fnaCoreId
                                                                    select f.ParameterValue).FirstOrDefault();
                    int _acctType = output.ParameterSystemUT.AccountType.AccountTypeID.Value;
                    output.ParameterSystemUT.AccountType.AccountTypeName = (from f in parSys
                                                                            where f.ParsysID == _acctType
                                                                            select f.ParameterValue).FirstOrDefault();
                    int _funcType = output.ParameterSystemUT.FunctionType.FunctionTypeID.Value;
                    output.ParameterSystemUT.FunctionType.FunctionTypeName = (from f in parSys
                                                                              where f.ParsysID == _funcType
                                                                              select f.ParameterValue).FirstOrDefault();
                    long TRid = output.ID;
                    IList<JoinAccountModel> UTJoin = (from uj in context.JoinAccounts
                                                      where uj.TransactionID == TRid
                                                      select new JoinAccountModel
                                                      {
                                                          CustomerJoin = new CustomerModel { CIF = uj.Customer.CIF, Name = uj.Customer.CustomerName },
                                                          CustomerRiskEffectiveDateJoin = uj.RiskEffectiveDate,
                                                          RiskProfileExpiryDateJoin = uj.RiskProfileExpiryDate,
                                                          RiskScoreJoin = uj.RiskScore,
                                                          SolIDJoin = uj.SolID,
                                                          JoinOrder = uj.JoinOrder,
                                                          Join = new JoinModel
                                                          {
                                                              ID = uj.JoinType,
                                                              Name = uj.JoinType == false ? "And" : "Or"
                                                          }

                                                      }).ToList<JoinAccountModel>();
                    output.JoinAccounts = UTJoin;
                    //datatransaction.IsOtherBeneBank = datatransaction.Bank.Code != "999" ? false : true;
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceId)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    // changed, chandra : 2015.03.23
                    if (customerRepo.GetCustomerByTransaction(output.ID, ref customer, ref message))
                    {
                        output.Customer = customer;
                    }
                    else
                    {
                        if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                        {
                            output.Customer = customer;
                        }
                    }
                    //output.Customer = customer;
                    customerRepo.Dispose();
                    customer = null;
                }

                IsSuccess = true;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string _message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(_message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        public bool GetUTCBOUTMaker(Guid workflowInstanceId, ref UTCBOUTMakerModel output, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();
                // get main transaction data
                IList<ParameterSystem> parSys = context.ParameterSystems.ToList();
                output = (from a in context.Transactions
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceId)
                          select new UTCBOUTMakerModel
                          {
                              ID = a.TransactionID,
                              WorkflowInstanceID = a.WorkflowInstanceID.Value,
                              ApplicationID = a.ApplicationID,
                              ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                              Product = new ProductModel()
                              {
                                  ID = a.Product.ProductID,
                                  Code = a.Product.ProductCode,
                                  Name = a.Product.ProductName,
                                  WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                  Workflow = a.Product.ProductWorkflow.WorkflowName,
                                  LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                  LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                              },
                              Channel = new ChannelModel()
                              {
                                  ID = a.Channel.ChannelID,
                                  Name = a.Channel.ChannelName,
                                  LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                  LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                              },
                              Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                              {
                                  ID = x.TransactionDocumentID,
                                  Type = new DocumentTypeModel()
                                  {
                                      ID = x.DocumentType.DocTypeID,
                                      Name = x.DocumentType.DocTypeName,
                                      Description = x.DocumentType.DocTypeDescription,
                                      LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                      LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                  },
                                  Purpose = new DocumentPurposeModel()
                                  {
                                      ID = x.DocumentPurpose.PurposeID,
                                      Name = x.DocumentPurpose.PurposeName,
                                      Description = x.DocumentPurpose.PurposeDescription,
                                      LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                      LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                  },
                                  FileName = x.Filename,
                                  DocumentPath = x.DocumentPath,
                                  LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                  LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                              }).ToList(),
                              ParameterSystemUT = new ParameterSystemUTModel()
                              {
                                  FNACore = new UTFNACore() { FNACoreID = a.IsFNACore == true ? 17 : 18 },
                                  AccountType = new UTAccountType() { AccountTypeID = a.AccountType.Value },
                                  FunctionType = new UTFunctionType() { FunctionTypeID = a.FunctionType.Value }
                              },
                              SOLID = a.SolID,
                              RiskScore = a.RiskScore.Value,
                              CustomerRiskEffectiveDate = a.CRiskEfectiveDate.Value,
                              RiskProfileExpiryDate = a.RiskProfileExpiryDate.Value,
                              Remark = a.Remarks,
                              InvestmentID = a.InvestmentID,
                              AttachmentRemarks = a.AttachmentRemarks,
                              CreateDate = a.CreateDate,
                              LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                              LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                          }).SingleOrDefault();

                if (output != null)
                {
                    int _fnaCoreId = output.ParameterSystemUT.FNACore.FNACoreID.Value;
                    output.ParameterSystemUT.FNACore.FNACoreName = (from f in parSys
                                                                    where f.ParsysID == _fnaCoreId
                                                                    select f.ParameterValue).FirstOrDefault();
                    int _acctType = output.ParameterSystemUT.AccountType.AccountTypeID.Value;
                    output.ParameterSystemUT.AccountType.AccountTypeName = (from f in parSys
                                                                            where f.ParsysID == _acctType
                                                                            select f.ParameterValue).FirstOrDefault();
                    int _funcType = output.ParameterSystemUT.FunctionType.FunctionTypeID.Value;
                    output.ParameterSystemUT.FunctionType.FunctionTypeName = (from f in parSys
                                                                              where f.ParsysID == _funcType
                                                                              select f.ParameterValue).FirstOrDefault();
                    long TRid = output.ID;
                    IList<JoinAccountModel> UTJoin = (from uj in context.JoinAccounts
                                                      where uj.TransactionID == TRid
                                                      select new JoinAccountModel
                                                      {
                                                          CustomerJoin = new CustomerModel { CIF = uj.Customer.CIF, Name = uj.Customer.CustomerName },
                                                          CustomerRiskEffectiveDateJoin = uj.RiskEffectiveDate,
                                                          RiskProfileExpiryDateJoin = uj.RiskProfileExpiryDate,
                                                          RiskScoreJoin = uj.RiskScore,
                                                          SolIDJoin = uj.SolID,
                                                          JoinOrder = uj.JoinOrder,
                                                          Join = new JoinModel
                                                          {
                                                              ID = uj.JoinType,
                                                              Name = uj.JoinType == false ? "And" : "Or"
                                                          },
                                                      }).ToList<JoinAccountModel>();
                    IList<TransactionMutualFundModel> MutuaFund = (from mt in context.MutualFundTransactions
                                                                   join ff in context.Funds on mt.FundID equals ff.FundID
                                                                   join ft in context.Funds on mt.FundID equals ft.FundID
                                                                   where mt.TransactionID == TRid
                                                                   select new TransactionMutualFundModel
                                                                   {
                                                                       FundID = mt.FundID,
                                                                       FundName = ff.FundName,
                                                                       FundCode = ff.FundCode,
                                                                       Amount = mt.Amount,
                                                                       IsPartial = mt.IsPartial,
                                                                       NumberofUnit = mt.NumberofUnit,
                                                                       SwitchToFundID = mt.SwitchToFundID,
                                                                       SwitchToFundName = ft.FundName,
                                                                       SwitchToFundCode = ft.FundCode
                                                                   }).ToList<TransactionMutualFundModel>();
                    output.JoinAccounts = UTJoin;
                    output.MutualFunds = MutuaFund;
                    //datatransaction.IsOtherBeneBank = datatransaction.Bank.Code != "999" ? false : true;
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceId)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    // changed, chandra : 2015.03.23
                    if (customerRepo.GetCustomerByTransaction(output.ID, ref customer, ref message))
                    {
                        output.Customer = customer;
                    }
                    else
                    {
                        if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                        {
                            output.Customer = customer;
                        }
                    }
                    //output.Customer = customer;
                    customerRepo.Dispose();
                    customer = null;
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        public bool AddTransactionCBOAccountMaker(Guid workflowInstanceId, long approverID, UTCBOAccountMakerModel transaction, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var data = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceId)).SingleOrDefault();
                    if (data != null)
                    {
                        TransactionCBOAccount cboAccount = new TransactionCBOAccount()
                        {
                            TransactionID = data.TransactionID,
                            WorkflowInstanceID = workflowInstanceId,
                            ApproverID = approverID,
                            InvestmentID = transaction.InvestmentID,
                            TransactionTypeID = transaction.transactiontypeID,
                            OtherInvestmentID = null,
                            Remarks = transaction.Remark,

                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().LoginName
                        };
                        context.TransactionCBOAccounts.Add(cboAccount);
                        context.SaveChanges();
                    }
                    ts.Complete();
                    isSuccess = true;
                }

                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }
            return isSuccess;
        }

        public bool AddTransactionCBOUTMaker(Guid workflowInstanceId, long approverID, UTBranchCheckerDetailModel transaction, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    #region UTNumber & DBNumber in Mutual Fund for UT UC || UO || UF
                    if (transaction.MutualFund != null && transaction.MutualFund.Count > 0)
                    {
                        var dataMutualFund = transaction.MutualFund;

                        if (transaction.Transaction.Product.Code == "UC" || transaction.Transaction.Product.Code == "UO" || transaction.Transaction.Product.Code == "UF")
                        {
                            if (dataMutualFund.Count > 0)
                            {
                                foreach (var item in dataMutualFund)
                                {
                                    var updateMutual = context.MutualFundTransactions.Where(a => a.MutualFundTransactionID.Equals(item.MutualFundTransactionID)).SingleOrDefault();
                                    updateMutual.UTNumber = item.UTNumber;
                                }
                            }
                        }else {
                            if (transaction.Transaction.Product.Code == "SP")
                            {
                                if (dataMutualFund.Count > 0)
                                {
                                    foreach (var item in dataMutualFund)
                                    {
                                        var updateMutualSP = context.MutualFundTransactions.Where(a => a.MutualFundTransactionID.Equals(item.MutualFundTransactionID)).SingleOrDefault();
                                        updateMutualSP.DBNumber = item.DBNumber;
                                    }
                                }
                            }
                        }
                        context.SaveChanges();
                    }
                    #endregion

                    #region ID Investment for UT IN 
                    if (transaction.Transaction.Product.Code == "IN"){
                        var updateINID = context.Transactions.Where(a => a.TransactionID.Equals(transaction.Transaction.ID)).SingleOrDefault();
                        updateINID.InvestmentID = transaction.Transaction.InvestmentID;

                        context.SaveChanges();
                    }
                    #endregion

                    ts.Complete();
                    isSuccess = true;
                }

                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }
            return isSuccess;
        }
        #endregion

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}
//=======================================================================================//
///FOR ALL DEVELOPER...
///PLEASE ADD NEW REGION BY YOUR NAME TO KEEP CLEAN !!! DONT BE A DIRTY PROGRAMMER!!!
///ANDIWIJAYA
//=======================================================================================//