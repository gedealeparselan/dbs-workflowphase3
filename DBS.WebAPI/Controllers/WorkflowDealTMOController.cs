﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class WorkflowDealTMOController : ApiController
    {
        private string message = string.Empty;
        private readonly IWorkflowDealTMORepository repoDealTMO = null;
        public WorkflowDealTMOController()
        {
            this.repoDealTMO = new WorkflowDealTMORepository();
        }

        /// <summary>
        /// Get transaction details for checker by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approve ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionNettingMakerDetailModel))]
        [HttpGet]
        [Route("api/WorkflowDealTMO/{workflowInstanceID}/Transaction/Maker/{approverID}")]
        public HttpResponseMessage GetTransactionMakerDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionNettingDetailModel transaction = new TransactionNettingDetailModel();          
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionNettingCheckerDetailModel verify = new TransactionNettingCheckerDetailModel();
            TransactionNettingMakerDetailModel output = new TransactionNettingMakerDetailModel();

            try
            {
                if (!repoDealTMO.GetTransactionDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {                    
                    // #2 Get Transaction Timeline
                    if (!repoDealTMO.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker
                        if (!repoDealTMO.GetTransactionCheckerDetails(workflowInstanceID, approverID, ref verify, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;
                            output.Verify = verify.Verify;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }                        
                       
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }       

        /// <summary>
        /// Get transaction details for checker by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approve ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpGet]
        [Route("api/WorkflowDealTMO/{workflowInstanceID}/Transaction/Checker/{approverID}")]
        public HttpResponseMessage GetTransactionCheckerDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionNettingDetailModel transaction = new TransactionNettingDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionNettingCheckerDetailModel output = new TransactionNettingCheckerDetailModel();

            try
            {
                // #1 Get Transaction Details
                if (!repoDealTMO.GetTransactionDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoDealTMO.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        if (!repoDealTMO.GetTransactionCheckerDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else                   
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);                        
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        /// <summary>
        /// Save approval data from checker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="transactionChecker">Transaction checker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowDealTMO/{workflowInstanceID}/Transaction/Checker/{approverID}")]
        public HttpResponseMessage AddTransactionCheckerDetails(Guid workflowInstanceID, long approverID, TransactionNettingCheckerDetailModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoDealTMO.AddTransactionChecker(workflowInstanceID, approverID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }

        /// <summary>
        /// Save approval data from checker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="transactionChecker">Transaction checker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowDealTMO/{workflowInstanceID}/Transaction/Maker/{approverID}")]
        public HttpResponseMessage UpdateTransactionMakerDetails(Guid workflowInstanceID, long approverID, TransactionNettingMakerDetailModel transactionMaker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoDealTMO.UpdateTransactionMaker(workflowInstanceID, approverID, transactionMaker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction maker has been saved." });
                }
            }
        }
        /// <summary>
        /// Save approval data from checker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="transactionChecker">Transaction checker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpGet]
        [Route("api/WorkflowDealTMO/Transaction/Maker/Underlying/{Id}")]
        public HttpResponseMessage GetUnderlyingFromDeals(long Id)
        {
            TransactionDealDetailModel transaction = new TransactionDealDetailModel();           

            try
            {
                // #1 Get Transaction Details
                if (!repoDealTMO.GetTransactionDealDetails(Id, ref transaction, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
                }
                else
                {                    
                    return Request.CreateResponse<TransactionDealDetailModel>(HttpStatusCode.OK, transaction);
                                       
                }
            }
            finally
            {

                transaction = null;
               
            }
        }
        /// <summary>
        /// Update Transaction Deal Maker after Revise from Payment Checker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowDealTMO/{workflowInstanceID}/TransactionRevise/Deal/")]
        public HttpResponseMessage UpdateTransactionDealRevise(Guid workflowInstanceID, TransactionNettingCheckerDetailModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoDealTMO.UpdateTransactionDealRevise(workflowInstanceID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "revise transaction deal complete." });
                }
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (repoDealTMO != null)
                repoDealTMO.Dispose();
            base.Dispose(disposing);
        }        

    }
}
