﻿using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;

using System.Net.Http;
using System.Web.Script.Serialization;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("CustomerCSO")]
    public class CustomerCSOModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public string Data { get; set; } // azam
        public int ID { get; set; }
        public int CustCSOID { get; set; }
        /// <summary>
        /// CIF
        /// </summary>
        public string CIF { get; set; }

        /// <summary>
        /// Employee Name
        /// </summary>
        public EmployeeModel Employee { get; set; }

        /// <summary>
        /// Last modified Date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class CustomerCSORow: CustomerCSOModel
    {
        public int RowID { get; set; }
    }

    public class CustomerCSOGrid : Grid
    {
        public IList<CustomerCSORow> Rows { get; set; }
    }
    #endregion

    #region Filter
    public class CustomerCSOFilter : Filter { }
    #endregion

    #region Interface
    public interface ICustomerCSORepository : IDisposable
    {
        bool GetCustomerCSOByID(string cif, int id, ref CustomerCSOModel CustomerCSO, ref string message);
        bool GetCustomerCSO(string cif, ref IList<CustomerCSOModel> CustomerCSOs, int limit, int index, ref string message);
        bool GetCustomerCSO(string cif, ref IList<CustomerCSOModel> CustomerCSOs, ref string message);
        bool GetCustomerCSO(string cif, int page, int size, IList<CustomerCSOFilter> filters, string sortColumn, string sortOrder, ref CustomerCSOGrid CustomerCSO, ref string message);
        bool GetCustomerCSO(string cif, string key, int limit, ref IList<CustomerCSOModel> CustomerCSOs, ref string message);
        bool AddCustomerCSO(string cif, CustomerCSOModel CustomerCSO, ref string message);
        bool UpdateCustomerCSO(string cif, int id, CustomerCSOModel CustomerCSO, ref string message);
        bool DeleteCustomerCSO(string cif, int id, ref string message);

        //bool GetCustomerCSODraftByID(string id, ref CustomerCSOModel role, ref string message);
        //bool GetCustomerCSODraft(string cif, int page, int size, IList<CustomerCSOFilter> filters, string sortColumn, string sortOrder, ref CustomerCSOGrid CustomerCSO, ref string message);
        bool GetSP(string cif,int page, int size, IList<WorkflowEntityFilter> filters, string sortColumn, string sortOrder, ref WorkflowEntityGrid roleGrid, ref string message);
    }
    #endregion

    #region Repository
    public class CustomerCSORepository : ICustomerCSORepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetCustomerCSOByID(string cif, int id, ref CustomerCSOModel CustomerCSO, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using(DBSEntities context = new DBSEntities())
                {
                    CustomerCSO = (from a in context.CSOes
                                   where a.IsDeleted.Equals(false) && a.CustCSOID.Equals(id) && a.CIF.Equals(cif)
                                   select new CustomerCSOModel
                                   {
                                       ID = a.CustCSOID,
                                       CIF = a.CIF,
                                       Employee = new EmployeeModel()
                                       {
                                           EmployeeID = a.Employee.EmployeeID,
                                           EmployeeName = a.Employee.EmployeeName,
                                           EmployeeEmail = a.Employee.EmployeeEmail                                           
                                       },
                                       LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                       LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                   }).SingleOrDefault();
                }
                isSuccess = true;
            }
            catch(Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return isSuccess;
        }
        
        public bool GetCustomerCSO(string cif, ref IList<CustomerCSOModel> CustomerCSOs, int limit, int index, ref string message)
        {
            bool isSuccess = false;
            try
            {
                int skip = limit * index;
                using(DBSEntities context = new DBSEntities())
                {
                    CustomerCSOs = (from a in context.CSOes
                                    where a.IsDeleted.Equals(false) && a.CIF.Equals(cif)
                                    orderby new { a.Employee.EmployeeName, a.CustCSOID }
                                    select new CustomerCSOModel
                                    {
                                        ID = a.CustCSOID,
                                        CIF = a.CIF,
                                        Employee = new EmployeeModel()
                                        {
                                            EmployeeID = a.Employee.EmployeeID,
                                            EmployeeName = a.Employee.EmployeeName,
                                            EmployeeEmail = a.Employee.EmployeeEmail,
                                            Branch = new BranchModel
                                            {
                                                ID = a.Employee.Location.LocationID,
                                                Name = a.Employee.Location.LocationName
                                            }
                                        },
                                        LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                        LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                    }).Skip(skip).Take(limit).ToList();
                }
                isSuccess = true;
            }
            catch(Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerCSO(string cif, ref IList<CustomerCSOModel> CustomerCSOs, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    CustomerCSOs = (from a in context.CSOes
                                    where a.IsDeleted.Equals(false) && a.CIF.Equals(cif)
                                    orderby new { a.Employee.EmployeeName, a.CustCSOID }
                                    select new CustomerCSOModel
                                    {
                                        ID = a.CustCSOID,
                                        CIF = a.CIF,
                                        Employee = new EmployeeModel()
                                        {
                                            EmployeeID = a.Employee.EmployeeID,
                                            EmployeeName = a.Employee.EmployeeName,
                                            EmployeeEmail = a.Employee.EmployeeEmail,
                                            Branch = new BranchModel
                                            {
                                                ID = a.Employee.Location.LocationID,
                                                Name = a.Employee.Location.LocationName
                                            }
                                        },
                                        LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                        LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                    }).ToList();
                }
                isSuccess = true;
            }
            catch(Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return isSuccess;
        }

        public bool GetCustomerCSO(string cif, int page, int size, IList<CustomerCSOFilter> filters, string sortColumn, string sortOrder, ref CustomerCSOGrid CustomerCSO, ref string message)
        {
            bool isSuccess = false;
            try
            {                
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                CustomerCSOModel filter = new CustomerCSOModel();
                filter.Employee = new EmployeeModel { EmployeeName = string.Empty,
                                                      EmployeeEmail = string.Empty,
                                                      Branch = new BranchModel { Name = string.Empty }
                                                    };

                if (filters != null)
                {                    
                    filter.Employee.EmployeeName = (string)filters.Where(a => a.Field.Equals("EmployeeName")).Select(a => a.Value).SingleOrDefault();
                    filter.Employee.EmployeeEmail = (string)filters.Where(a => a.Field.Equals("EmployeeEmail")).Select(a => a.Value).SingleOrDefault();
                    filter.Employee.Branch.Name= (string)filters.Where(a => a.Field.Equals("LocationName")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                
                using(DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.CSOes                           
                                let isEmployeeName = !string.IsNullOrEmpty(filter.Employee.EmployeeName)
                                let isEmployeeEmail = !string.IsNullOrEmpty(filter.Employee.EmployeeEmail)
                                let isLocationName = !string.IsNullOrEmpty(filter.Employee.Branch.Name)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) && a.CIF.Equals(cif) &&                                 
                                    (isEmployeeName ? a.Employee.EmployeeName.Contains(filter.Employee.EmployeeName) : true) &&
                                    (isEmployeeEmail ? a.Employee.EmployeeEmail.Contains(filter.Employee.EmployeeEmail) : true) &&
                                    (isLocationName ? a.Employee.Location.LocationName.Contains(filter.Employee.Branch.Name) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new CustomerCSOModel
                                {
                                    ID = a.CustCSOID,
                                    CIF = a.CIF,
                                    Employee = new EmployeeModel()
                                    {
                                        EmployeeID = a.Employee.EmployeeID,
                                        EmployeeName = a.Employee.EmployeeName,
                                        EmployeeEmail = a.Employee.EmployeeEmail,
                                        Branch = new BranchModel
                                        {
                                            ID = a.Employee.Location.LocationID,
                                            Name = a.Employee.Location.LocationName
                                        }
                                    },
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });
                    CustomerCSO.Page = page;
                    CustomerCSO.Size = size;
                    CustomerCSO.Total = data.Count();
                    CustomerCSO.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    CustomerCSO.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new CustomerCSORow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            CIF = a.CIF,
                            Employee = a.Employee,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }
                 isSuccess = true;
            }
            catch(Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        
        public bool GetCustomerCSO(string cif, string key, int limit, ref IList<CustomerCSOModel> CustomerCSOs, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    CustomerCSOs = (from a in context.CSOes
                                    where a.IsDeleted.Equals(false) && a.Employee.EmployeeName.Contains(key)
                                    select new CustomerCSOModel
                                    {
                                        ID = a.CustCSOID,
                                        CIF = a.CIF,
                                        Employee = new EmployeeModel()
                                        {
                                            EmployeeID = a.Employee.EmployeeID,
                                            EmployeeName = a.Employee.EmployeeName,
                                            EmployeeUsername = a.Employee.EmployeeUsername,
                                            EmployeeEmail = a.Employee.EmployeeEmail,
                                            Branch = new BranchModel
                                            {
                                                ID = a.Employee.Location.LocationID,
                                                Name = a.Employee.Location.LocationName
                                            }
                                        },
                                        LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                        LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                    }).Take(limit).ToList();
                }
                isSuccess = true;
            }
            catch(Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return isSuccess;
        }

        public bool AddCustomerCSO(string cif, CustomerCSOModel CustomerCSO, ref string message)
        {
            bool isSuccess = false;
            try
            {
                var EmployeeUsername = "i:0#.f|dbsmembership|" + CustomerCSO.Employee.EmployeeUsername;
                if (!context.CSOes.Where(a => a.Employee.EmployeeUsername.Equals(EmployeeUsername) && a.CIF.Equals(CustomerCSO.CIF) && a.IsDeleted.Equals(false)).Any())
                {
                    context.CSOes.Add(new Entity.CSO
                    {
                        CIF = CustomerCSO.CIF,
                        EmployeeID  = CustomerCSO.Employee.EmployeeID,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });
                    context.SaveChanges();
                    isSuccess = true;
                }
                else
                {
                    message = string.Format("CSO For Customer, Name {0} is already exist.", CustomerCSO.Employee.EmployeeName);
                }
                
            }
            catch(Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return isSuccess;
        }

        public bool UpdateCustomerCSO(string cif, int id, CustomerCSOModel CustomerCSO, ref string message)
        {
            bool isSuccess = false;
            try
            {
                DBS.Entity.CSO data = context.CSOes.Where(a => a.CustCSOID.Equals(id) && a.CIF.Equals(cif)).SingleOrDefault();
                if (data != null)
                {
                    if (context.CSOes.Where(a => a.CustCSOID != CustomerCSO.ID && a.EmployeeID.Equals(CustomerCSO.Employee.EmployeeID) && a.CIF.Equals(CustomerCSO.CIF) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("CSO For Customer, Name {0} is already exist.", CustomerCSO.Employee.EmployeeName);
                    }
                    else
                    {
                        data.CIF = CustomerCSO.CIF;
                        data.EmployeeID = CustomerCSO.Employee.EmployeeID;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
            }
            catch(Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return isSuccess;
        }

        public bool DeleteCustomerCSO(string cif, int id, ref string message)
        {
            bool isSuccess = false;
            try
            {
                DBS.Entity.CSO data = context.CSOes.Where(a => a.CustCSOID.Equals(id) && a.CIF.Equals(cif)).SingleOrDefault();
                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("CSO for Customer, ID {0} is does not exist.", id);
                }
            }
            catch(Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return isSuccess;
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                        context.Dispose();
                }
            }
            this.disposed = true;
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }        

        //start add  azam
        private IQueryable<WorkflowEntityModel> GetSPAllJoin(DBSEntities context, string EntityName = null, string filterCIF = null)
        {            
            List<WorkflowEntity> filterEntity = new List<WorkflowEntity>();
            string cifValue = null;
            if (EntityName != null && EntityName.Equals("CustomerCSO"))
            {
                if (filterCIF != null)
                {
                    cifValue = filterCIF;
                }
            }
            string userName = currentUser.GetCurrentUser().LoginName;
            var data = (from a in context.SP_GetTaskWorkflow(EntityName, cifValue, null, null, null, null)
                        select new WorkflowEntityModel
                        {
                            ID = a.ID,
                            EntityName = a.EntityName,
                            TaskName = a.TaskName,
                            ActionType = a.ActionType,
                            //Initiator = a.,
                            //SPListItemID = a.SPListItemID,
                            //SPTaskListID = (Guid)a.SPTaskListID,
                            //SPTaskListItemID = (int)a.SPTaskListItemID,
                            WorkflowID = (Guid)a.WorkflowID,
                            LastModifiedDate = a.CreateDate,
                            LastModifiedBy = a.CreateBy,
                            Data = a.Data
                        }).ToList();

            return data.AsQueryable();
        }
        public bool GetSP(string cif,int page, int size, IList<WorkflowEntityFilter> filters, string sortColumn, string sortOrder, ref WorkflowEntityGrid roleGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                WorkflowEntityModel filter = new WorkflowEntityModel();
                if (filters != null)
                {
                    filter.EntityName = (string)filters.Where(a => a.Field.Equals("EntityName")).Select(a => a.Value).SingleOrDefault();
                    filter.TaskName = (string)filters.Where(a => a.Field.Equals("TaskName")).Select(a => a.Value).SingleOrDefault();
                    filter.ActionType = (string)filters.Where(a => a.Field.Equals("ActionType")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    filter.FilterValue = (string)filters.Where(a => a.Field.Equals("FilterValue")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    //var data = from a in GetSPAllJoin(context, filter.EntityName, filter.FilterValue)
                    var data = from a in GetSPAllJoin(context, "CustomerCSO", cif)
                               select a;
                    roleGrid.Page = page;
                    roleGrid.Size = size;
                    roleGrid.Total = data.Count();
                    roleGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    roleGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new WorkflowEntityRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            EntityName = a.EntityName,
                            TaskName = a.TaskName,
                            ActionType = a.ActionType,
                            Data = a.Data,
                            SPTaskListID = a.SPTaskListID,
                            SPTaskListItemID = a.SPTaskListItemID,
                            Initiator = a.Initiator,
                            WorkflowID = (Guid)a.WorkflowID == null ? Guid.Empty : (Guid)a.WorkflowID,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

       //end add zam
    }

    #endregion
}