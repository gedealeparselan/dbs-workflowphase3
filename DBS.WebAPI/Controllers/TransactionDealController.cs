﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class TransactionDealController : ApiController
    {
        private string message = string.Empty;

        private readonly ITransactionDealRepository repository = null;

        public TransactionDealController()
        {
            this.repository = new TransactionDealRepository();
        }

        public TransactionDealController(ITransactionDealRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all Get Transaction Deal.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<TransactionDealModel>))]
        public HttpResponseMessage GetAllTransactionDeal()
        {
            IList<TransactionDealModel> output = new List<TransactionDealModel>();

            if (!repository.GetTransactionDeal(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TransactionDealModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new Transaction Deal.
        /// </summary>
        /// <param name="data">Transaction Deal data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Post(TransactionDealDetailModel data)
        {
            long transactionDealID = 0;

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionDeal(data, ref transactionDealID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new
                    {
                        ID = transactionDealID,
                        Message = "New Transaction has been submitted."
                    });
                }
            }
        }
        /// <summary>
        /// Get Transaction Deal for data grid.
        /// </summary>
        /// <param name="page">Page.</param>
        /// <param name="size">Size.</param>
        /// <param name="sort_column">Sort Column.</param>
        /// <param name="sort_order">Sort Order.</param>
        /// <param name="filters">Filters.</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionDealGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<TransactionDealFilter> filters)
        {
            TransactionDealGrid output = new TransactionDealGrid();

            if (!repository.GetTransactionDeal(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<TransactionDealGrid>(HttpStatusCode.OK, output);
            }
        }

        #region Tambah Agung
        [ResponseType(typeof(TransactionDraftModel))]
        [Route("api/Transaction/DraftTMOIPE/{id}")]
        [HttpGet]
        public HttpResponseMessage GetDraftTMOIPE(Int64 id)
        {
            TransactionDraftModel output = new TransactionDraftModel();

            if (!repository.GetTransactionDraftByIDTMOIPE(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<TransactionDraftModel>(HttpStatusCode.OK, output);
            }
        }


        /// <summary>
        /// Get Transaction Deal for data grid.
        /// </summary>
        /// <param name="page">Page.</param>
        /// <param name="size">Size.</param>
        /// <param name="sort_column">Sort Column.</param>
        /// <param name="sort_order">Sort Order.</param>
        /// <param name="filters">Filters.</param>
        /// <returns></returns>
        [ResponseType(typeof(InstructionDocumentGrid))]
        [Route("api/InstructionDocumentTMO/{cif}")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetInstruction(int? page, int? size, string sort_column, string sort_order, IList<InstructionDocumentFilter> filters, string cif)
        {
            InstructionDocumentGrid output = new InstructionDocumentGrid();

            if (!repository.GetDataInstructionDocument(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, cif, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<InstructionDocumentGrid>(HttpStatusCode.OK, output);
            }
        }
        #endregion

        /// <summary>
        /// Get Transaction TZ Information for data grid.
        /// </summary>
        /// <param name="page">Page.</param>
        /// <param name="size">Size.</param>
        /// <param name="size">Size.</param>
        /// <returns></returns>
        [ResponseType(typeof(TzInformationGrid))]
        [Route("api/TransactionDeal/TzInformation/{cif}")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetTzInformation(int? page, int? size, string cif)
        {
            TzInformationGrid output = new TzInformationGrid();

            if (!repository.GetTzInformation(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, cif, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<TzInformationGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Modify existing transaction deal.
        /// </summary>
        /// <param name="id">ID of transaction to be modify.</param>
        /// <param name="data">Transaction data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/TransactionDeal/deal/update/{id}")]
        public HttpResponseMessage Put(long id, TransactionDealDetailModel data)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTransactionDealById(id, data, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        //Message = "Transaction deal data has been updated." 
                        Message = message
                    });
                }
            }
        }
        /// <summary>
        /// Modify existing transaction deal.
        /// </summary>
        /// <param name="id">ID of transaction to be modify.</param>
        /// <param name="data">Transaction data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/TransactionDeal/TMO/update/{id}")]
        public HttpResponseMessage Put(long id, TransactionTMOModel data)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTransactionTMOById(id, data, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        Message = message
                    });
                }
            }
        }

        #region tambah Agung
        /// <summary>
        /// Modify existing transaction deal.
        /// </summary>
        /// <param name="id">ID of transaction to be modify.</param>
        /// <param name="data">Transaction data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/TransactionDeal/TMO/updatecorrection/{id}")]
        public HttpResponseMessage UpdateTMO(long id, TransactionTMOModel data)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTransactionCorrectionTMOById(id, data, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        Message = message
                    });
                }
            }
        }
        #endregion

        [ResponseType(typeof(TransactionMakerDetailModel))]
        [HttpGet]
        [Route("api/TransactionDeal/{workflowInstanceID}")]
        public HttpResponseMessage GetTransactionDetails(Guid workflowInstanceID)
        {
            TransactionDealDetailModel transaction = new TransactionDealDetailModel();

            TransactionDealCheckerDetailModel output = new TransactionDealCheckerDetailModel();

            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionDealDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    output.Transaction = transaction;
                    return Request.CreateResponse(HttpStatusCode.OK, output);

                }
            }
            finally
            {
                transaction = null;
            }
        }

        /// <summary>
        /// Get Total Amount Transaction Deal Underlying by Cif
        /// </summary>
        /// <param name="Cif">CIF.</param>
        /// <param name="isStatementB">is Statement B.</param>
        /// <returns></returns>
        [ResponseType(typeof(decimal))]
        [Route("api/TransactionDeal/GetAmountTransaction={Cif}")]
        [HttpGet]
        public HttpResponseMessage GetAmountTransaction(string Cif, bool isStatementB)
        {
            decimal output = new decimal();

            if (!repository.GetAmountTransactionDeal(Cif, isStatementB, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == 0)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<decimal>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Transaction Deal by ID.
        /// </summary>
        /// <param name="id">Transaction Deal ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionDealDetailModel))]
        [Route("api/TransactionDeal/update/{id}")]
        [HttpGet]
        public HttpResponseMessage GetDetail(long id)
        {
            TransactionDealDetailModel output = new TransactionDealDetailModel();

            if (!repository.GetTransactionDealById(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<TransactionDealDetailModel>(HttpStatusCode.OK, output);
            }
        }
        /// <summary>
        /// Get Transaction Deal by ID.
        /// </summary>
        /// <param name="id">Transaction Deal ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionTMOModel))]
        [Route("api/TransactionDeal/TMOupdate/{id}")]
        [HttpGet]
        public HttpResponseMessage GetTMODetail(long id)
        {
            TransactionTMOModel output = new TransactionTMOModel();

            if (!repository.GetTransactionTMOById(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<TransactionTMOModel>(HttpStatusCode.OK, output);
            }
        }
        /// <summary>
        /// Set Application ID
        /// </summary>
        /// <param name="applicationID">Application ID</param>
        /// <param name="transactionID">Transaction ID</param>
        /// <returns></returns>
        [ResponseType(typeof(string))]
        [Route("api/TransactionDeal/Set/ApplicationID/")]
        [HttpGet]
        public HttpResponseMessage Get(string applicationID, long transactionID)
        {
            bool IsSucceess = repository.SetApplicationID(applicationID, transactionID, ref message);
            if (!IsSucceess)
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { ApplicationID = applicationID });
            }
        }

        [ResponseType(typeof(string))]
        [Route("api/TransactionDeal/TransactionStatus/{TZNumber}")]
        [HttpGet]
        public HttpResponseMessage UpdateTransactionStatus(string TZNumber)
        {
            if (!repository.UpdateTransactionStatusOTTFXTransaction(TZNumber, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Message = message });
            }


        }
    }
}