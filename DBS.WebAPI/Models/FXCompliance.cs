﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    public class FXComplianceModel
    {
        /// <summary>
        /// FXCompliance ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// FXComplianceCodes Name.
        /// </summary>
        //[MaxLength(50, ErrorMessage = "FX Compliance Name is must be in {1} characters.")]
        public string Name { get; set; }

        /// <summary>
        /// FXCompliance Description.
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class FXComplianceRow : FXComplianceModel
    {
        public int RowID { get; set; }

    }

    public class FXComplianceGrid : Grid
    {
        public IList<FXComplianceRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class FXComplianceFilter : Filter { }
    #endregion

    #region Interface
    public interface IFXComplianceRepository : IDisposable
    {
        bool GetFXComplianceByID(int id, ref FXComplianceModel fXCompliance, ref string message);
        bool GetFXCompliance(ref IList<FXComplianceModel> FXComplianceCodes, int limit, int index, ref string message);
        bool GetFXCompliance(ref IList<FXComplianceModel> FXComplianceCodes, ref string message);
        bool GetFXCompliance(int page, int size, IList<FXComplianceFilter> filters, string sortColumn, string sortOrder, ref FXComplianceGrid fXCompliance, ref string message);
        bool GetFXCompliance(FXComplianceFilter filter, ref IList<FXComplianceModel> FXComplianceCodes, ref string message);
        bool GetFXCompliance(string key, int limit, ref IList<FXComplianceModel> FXComplianceCodes, ref string message);
        bool AddFXCompliance(FXComplianceModel fXCompliance, ref string message);
        bool UpdateFXCompliance(int id, FXComplianceModel fXCompliance, ref string message);
        bool DeleteFXCompliance(int id, ref string message);
    }
    #endregion

    #region Repository
    public class FXComplianceRepository : IFXComplianceRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetFXComplianceByID(int id, ref FXComplianceModel fXCompliance, ref string message)
        {
            bool isSuccess = false;

            try
            {
                fXCompliance = (from a in context.FXComplianceCodes
                               where a.IsDeleted.Equals(false) && a.ComplianceCodeID.Equals(id)
                               select new FXComplianceModel
                               {
                                   ID = a.ComplianceCodeID,
                                   Name = a.ComplianceCodeName,
                                   Description = a.ComplianceCodeDescription,
                                   LastModifiedDate = a.CreateDate,
                                   LastModifiedBy = a.CreateBy
                               }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetFXCompliance(ref IList<FXComplianceModel> FXComplianceCodes, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    FXComplianceCodes = (from a in context.FXComplianceCodes
                                    where a.IsDeleted.Equals(false)
                                    orderby new { a.ComplianceCodeName, a.ComplianceCodeDescription }
                                    select new FXComplianceModel
                                    {
                                        ID = a.ComplianceCodeID,
                                        Name = a.ComplianceCodeName,
                                        Description = a.ComplianceCodeDescription,
                                        LastModifiedBy = a.CreateBy,
                                        LastModifiedDate = a.CreateDate
                                    }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetFXCompliance(ref IList<FXComplianceModel> FXComplianceCodes, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    FXComplianceCodes = (from a in context.FXComplianceCodes
                                    where a.IsDeleted.Equals(false)
                                    orderby new { a.ComplianceCodeName, a.ComplianceCodeDescription }
                                    select new FXComplianceModel
                                    {
                                        ID = a.ComplianceCodeID,
                                        Name = a.ComplianceCodeName,
                                        Description = a.ComplianceCodeDescription,
                                        LastModifiedBy = a.CreateBy,
                                        LastModifiedDate = a.CreateDate
                                    }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetFXCompliance(int page, int size, IList<FXComplianceFilter> filters, string sortColumn, string sortOrder, ref FXComplianceGrid fXComplianceGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                FXComplianceModel filter = new FXComplianceModel();
                if (filters != null)
                {
                    filter.Name = (string)filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.FXComplianceCodes
                                let isComplianceCodeName = !string.IsNullOrEmpty(filter.Name)
                                let isComplianceCodeDescription = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isComplianceCodeName ? a.ComplianceCodeName.Contains(filter.Name) : true) &&
                                    (isComplianceCodeDescription ? a.ComplianceCodeDescription.Contains(filter.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new FXComplianceModel
                                {
                                    ID = a.ComplianceCodeID,
                                    Name = a.ComplianceCodeName,
                                    Description = a.ComplianceCodeDescription,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    fXComplianceGrid.Page = page;
                    fXComplianceGrid.Size = size;
                    fXComplianceGrid.Total = data.Count();
                    fXComplianceGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    fXComplianceGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new FXComplianceRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Name = a.Name,
                            Description = a.Description,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetFXCompliance(FXComplianceFilter filter, ref IList<FXComplianceModel> FXComplianceCodes, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetFXCompliance(string key, int limit, ref IList<FXComplianceModel> FXComplianceCodes, ref string message)
        {
            bool isSuccess = false;

            try
            {
                FXComplianceCodes = (from a in context.FXComplianceCodes
                                where a.IsDeleted.Equals(false) &&
                                    a.ComplianceCodeName.Contains(key) || a.ComplianceCodeDescription.Contains(key)
                                orderby new { a.ComplianceCodeName, a.ComplianceCodeDescription }
                                select new FXComplianceModel
                                {
                                    Name = a.ComplianceCodeName,
                                    Description = a.ComplianceCodeDescription
                                }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddFXCompliance(FXComplianceModel fXCompliance, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.FXComplianceCodes.Where(a => a.ComplianceCodeName.Equals(fXCompliance.Name) && a.IsDeleted.Equals(false)).Any())
                {
                    context.FXComplianceCodes.Add(new Entity.FXComplianceCode()
                    {
                        ComplianceCodeName = fXCompliance.Name,
                        ComplianceCodeDescription = fXCompliance.Description,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("FX Compliance data for FX Compliance Name {0} is already exist.", fXCompliance.Name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateFXCompliance(int id, FXComplianceModel fXCompliance, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.FXComplianceCode data = context.FXComplianceCodes.Where(a => a.ComplianceCodeID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.FXComplianceCodes.Where(a => a.ComplianceCodeID != fXCompliance.ID && a.ComplianceCodeName.Equals(fXCompliance.Name) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("FX Compliance Name {0} is exist.", fXCompliance.Name);
                    }
                    else
                    {
                        data.ComplianceCodeName = fXCompliance.Name;
                        data.ComplianceCodeDescription = fXCompliance.Description;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("FX Compliance data for FX Compliance ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteFXCompliance(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.FXComplianceCode data = context.FXComplianceCodes.Where(a => a.ComplianceCodeID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("FX Compliance data for FX Compliance ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}