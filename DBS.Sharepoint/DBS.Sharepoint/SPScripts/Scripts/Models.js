var DaysModel = function (value) {
    var self = this;
    self.vDay = ko.observable(value);
}

var SingleValueParameter = function (data) {
    var self = this;

    self.ID = ko.observable(data.ID);
    self.Name = ko.observable(data.Name);
    self.Value = ko.observable(data.Value);
    self.LastModifiedDate = ko.observable(data.LastModifiedDate);
    self.LastModifiedBy = ko.observable(data.LastModifiedBy);
}

var DetailModel = function (subDetailModel, selInternalCondition, selExternalCondition, ddlInternalCondition, ddlExternalCondition) {
    var self = this;
    self.Subdetails = ko.observableArray(subDetailModel);

    self.selectInternalCondition = ko.observable(selInternalCondition);
    self.ddlInternalCondition = ko.observableArray(ddlInternalCondition);

    self.selectExternalCondition = ko.observable(selExternalCondition);
    self.ddlExternalCondition = ko.observableArray(ddlExternalCondition);

    self.AddSubdetail = function () {
        self.Subdetails.push(new SubDetailModel('', '', '', '', '', '', '', ddlValueTypes, true));

    };

    self.RemoveSubdetail = function () {
        self.Subdetails.remove(this);

    };

}

var SubDetailModel = function (selSubParameter, selSubParameterName, selSubCondition, selSubConditionName, selValueType, selValueTypeName, selValue, ddlValueTypes, isVisible) {
    var self = this;
    var selValueParameter = selValue;
    self.selectParameter = ko.observable(selSubParameter);
    self.selectParameterName = ko.observable(": " + selSubParameterName);
    self.selectCondition = ko.observable(selSubCondition);
    self.selectConditionName = ko.observable(": " + selSubConditionName);
    self.selectValueType = ko.observable(selValueType);
    self.selectValueTypeName = ko.observable(': ' + selValueTypeName);
    self.selectValue = ko.observable(selValue);
    self.selectValueParameter = ko.observable(selValueParameter);
    self.selectValueName = ko.observable(': ' + selValue);
    self.ddlValueType = ko.observable(ddlValueTypes);
    self.IsddlValue = ko.observable(false);
    self.IsVisible = ko.observable(isVisible);

}

var resultDetailModel = function (ID, internalCondition, externalCondition, resultSubdetailModel) {
    var self = this;
    self.ID = ko.observable(ID);
    self.InternalCondition = ko.observable(internalCondition);
    self.ExternalCondition = ko.observable(externalCondition);
    self.ExceptionHandlingsSubDetail = ko.observableArray(resultSubdetailModel);
}

var resultSubdetailModel = function (ID, parameterID, parameterName, conditionID, conditionName, valueType, valueTypeName, value) {
    var self = this;
    self.ID = ko.observable(ID);
    self.ParamID = ko.observable(parameterID);
    self.ParamName = ko.observable(parameterName);
    self.OperatorID = ko.observable(conditionID);
    self.OperatorName = ko.observable(conditionName);
    self.ValueType = ko.observable(valueType);
    self.ValueTypeName = ko.observable(valueTypeName);
    self.Value = ko.observable(value);

}
var CurrencyModel = function (id, code) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(code);
}

var ProductTypeModel = function (id, code) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
}

var MatrixModel = function (id, name, desc) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(desc);
}

var MatrixFieldDetailModel = function (id, name, matrixFieldSubDetails) {
    var self = this;

    self.ID = ko.observable(id);
    self.FieldID = ko.observable(id);
    self.Name = ko.observable(name);
    self.MatrixSubDetails = ko.observableArray(matrixFieldSubDetails);
}

var MatrixFieldSubDetailModel = function (id, name, desc) {
    var self = this;
    self.ID = ko.observable(id);
    self.FieldValueID = ko.observable(id);
    self.RankID = ko.observable(id);
    self.FunctionRoleID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(desc);
}

var SegmentModel = function (id, name, desc) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(desc);
}

var BranchModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}


// Matrix dropdown Model
var MatrixsModel = function (selDetail, subMatrixModel) {
    var self = this;
    self.selectDetail = ko.observable(selDetail);
    self.ddlDetails = ko.observableArray(ddlDetailModel);
    self.Subdetails = ko.observableArray(subMatrixModel);
}

var SubMatrixModel = function (selSubdetail, ddlSubdetailsModel_) {
    var self = this;
    self.selectSubdetail = ko.observable(selSubdetail);
    self.ddlSubdetails = ko.observableArray(ddlSubdetailsModel_);
}

var ddlDetailModel = [{ ID: 1, Name: "Rank" }, { ID: 2, Name: "Biz Segment" }, { ID: 3, Name: "Role" }, { ID: 4, Name: "Branch" }];

var MatrixModels = function (id, name, description, matrixs) {
    this.ID = id;
    this.Name = name;
    this.Description = description;
    this.LastModifiedDate = null;
    this.LastModifiedBy = null;
    this.Matrixs = matrixs;
}

var Matrixs = function (fieldId, subMatrix) {
    this.ID = 0;
    this.FieldID = fieldId;
    this.SubMatrix = subMatrix;
}

var SubMatrix = function (fieldID, SubdetailID) {
    switch (fieldID) {
        case 1:
            this.ID = 0;
            this.MatrixApprovalID = 0;
            this.RankID = SubdetailID;
            this.LocationID = 0;
            this.RoleID = 0;
            break;
        case 2:
            this.ID = 0;
            this.MatrixApprovalID = SubdetailID;
            this.RankID = 0;
            this.LocationID = 0;
            this.RoleID = 0;
            break;
        case 3:
            this.ID = 0;
            this.MatrixApprovalID = 0;
            this.RankID = 0;
            this.LocationID = 0;
            this.RoleID = SubdetailID;
            break;
        case 4:
            this.ID = 0;
            this.MatrixApprovalID = 0;
            this.RankID = 0;
            this.LocationID = SubdetailID;
            this.RoleID = 0;
            break;
        default:
            this.ID = 0;
            this.MatrixApprovalID = 0;
            this.RankID = 0;
            this.LocationID = 0;
            this.RoleID = 0;
            break;
    }
}

var RankModel = function (id, name) {
    var self = this;
    self.ID = id;
    self.Name = name;
}

var optionModel = function (id, name) {
    var self = this;
    self.id = id;
    self.name = name;
}

var ProductModel = {
    ID: ko.observable(0),
    Code: ko.observable(""),
    Name: ko.observable("")
};

var CustomerTypeModel = {
    ID: ko.observable(0),
    Name: ko.observable(""),
    Description: ko.observable("")
};

var BizSegmentModel = {
    ID: ko.observable(0),
    Name: ko.observable(""),
    Description: ko.observable("")
};

var SelectedModel = {
    Product: ko.observable(),
    CustomerType: ko.observable(),
    BizSegment: ko.observable(),
    Branch: ko.observable(),
    ProductID: ko.observable(),
    CustomerTypeID: ko.observable(),
    BizSegmentID: ko.observable(),
    BranchID: ko.observable()
};

var TypeofDocsModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable(),
    LastModifiedDate: ko.observable(new Date()),
    LastModifiedBy: "Unknown"
};

var ExistingRoleModel = function (id, name) {

    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);

}

var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};