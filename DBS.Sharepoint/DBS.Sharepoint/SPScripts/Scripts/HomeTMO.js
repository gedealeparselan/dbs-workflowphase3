﻿var accessToken;
var $box;
var cifData = '0';
var customerNameData = '';
var DocumentModels = ko.observable({ FileName: '', DocumentPath: '' });
var $remove = false;
var IsOutcomesComplete = false;
var IsRenameButtonSuccess = false;
var idrrate = 0;
var DealModel = { cif: cifData, token: accessToken }
//#region Constanta Variable

var CONST_STATEMENT = {
    StatementA_ID: 1,
    StatementB_ID: 2,
    Dash: 3,
    AnnualStatement: 4
}
var CONST_MSG = {
    NotCompleteUpload: 'Please complete the upload form fields.',
    ActivityTitleNotFound: 'Something went worng, the activity title task not found.',
    ValidationFields: 'Mandatory field must be filled.',
    ValidationAnnualSL: 'Customer does not have annual statement.',
    ValidationEnter: 'Enter key is disabled for this form.',
    SuccessSave: 'Transaction data has been saved.',
    ValidationMinTotal: 'The Total Underlying Amount must be greater than Transaction Amount',
    ValidationUnderlyingInstraction: 'Please Select Underlying or upload Instruction Document.',
}
//#endregion

/* dodit@2014.11.08:add format function */
var formatNumber = function (num) {
    if (num != null) {
        //num = num.toString().replace(/,/g, '');
        //num = parseFloat(num).toFixed(2);
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    } else { return 0; }
}

var formatNumber_r = function (num) {
    if (num != null) {
        num = num.toString().replace(/,/g, '');
        num = parseFloat(num).toFixed(2);
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    } else { return 0; }
}
var DBS = {
    AcivityTitle: {
        TMOCSOTask: "CSO Task",
        TMOPPUMakerTask: "PPU Maker Task",
        TMOPPUMakerAfterCheckerAndCallerTask: "PPU Maker After Checker and Caller Task",
        TMOPPUCheckerTask: "PPU Checker Task",
        TMOPPUCheckerAfterPPUCallerTask: "PPU Checker After PPU Caller Task",
        TMOPPUCallerTask: "PPU Caller Task",
        TMOMakerTask: "TMO Maker Task",
        TMOCheckerTask: "TMO Checker Task",
        TMOCheckerCancellationTask: "TMO Checker Cancellation Task",

        FXDealTMOCheckerTask: "FX Deal TMO Checker Task",
        FXTMOMakerAfterReviseTask: " FX TMO Maker After Revise Task",
        FXTMOMakerAfterSubmitTask: " FX Deal TMO Maker After Submit Task",
        FXTMOCheckerRequestCancelTask: "FX Deal TMO Checker Request Cancel Task",

        FXDealTMOCheckerReopenTask: "FX Deal TMO Checker Reopen Task",
        FXTMOMakerAfterReopenTask: "FX TMO Maker Reopen Task",
        FXTMOCheckerReopenCancelTask: "FX Deal TMO Checker Reopen Cancel Task",

        FXDealTMOMakerNettingTask: "FX Deal TMO Netting Maker Task",
        FXDealTMOCheckerNettingTask: "FX Deal TMO Netting Checker Task",
        FXDealTMOMakerNettingCancellationTask: "FX Deal TMO Netting Checker Cancellation Task"
    }
}
var NettingPurposeModel = {
    ID: ko.observable(),
    name: ko.observable()
};

var TransactionMakerNettingModel = function () {
    this.Transaction = ko.observable(TransactionNettingModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var TransactionCheckerNettingModel = function () {
    this.Transaction = ko.observable(TransactionNettingModel);
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};


var TransactionNettingModel = {
    AccountNumber: ko.observable(),
    StatementLetter: ko.observable(StatementModel),
    Product: ko.observable(ProductModel),
    TZReference: ko.observable(),
    IDTZReference: ko.observable(),
    MurexNumber: ko.observable(),
    SwapDealNumber: ko.observable(),
    NettingTZReference: ko.observable(),
    IDNettingTZReference: ko.observable(),
    ExpectedDateStatementLetter: ko.observable(),
    ExpectedDateUnderlying: ko.observable(),
    ActualDateStatementLetter: ko.observable(),
    ActualDateUnderlying: ko.observable(),
    RateType: ko.observable(),
    ProductType: ko.observable(ProductTypeModel),
    Rate: ko.observable(),
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Currency: ko.observable(CurrencyModel),
    AmountUSD: ko.observable(0),
    Account: ko.observable(AccountModel),
    Type: ko.observable(20),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]),
    utilizationAmount: ko.observable(0.00),
    underlyings: ko.observableArray([]),
    IsDraft: ko.observable(false),
    OtherUnderlying: ko.observable(),
    bookunderlyingamount: ko.observable(),
    bookunderlyingcurrency: ko.observable(),
    bookunderlyingcode: ko.observable(),
    bookunderlyingdesc: ko.observable(),
    Remarks: ko.observable(),
    IsTMO: ko.observable(true),
    IsNettingTransaction: ko.observable(true),
    NettingPurpose: ko.observable(NettingPurposeModel)
};

var resetNumber = function (num) {
    if (num != null) {
        num = num.toString().replace(/,/g, '');
        return parseFloat(num).toFixed(2);
    } else { return 0; }
}

var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};

var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};


var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};
// add chandra : 20151123
var StatementModel = {
    ID: ko.observable(0),
    Name: ko.observable("")
};

var AmountModel = {
    TotalAmountsUSD: ko.observable(0),
    RemainingBalance: ko.observable(0),
    TotalUtilization: ko.observable(0),
    TotalDealUtilization: ko.observable(0),
    TotalDealAmountsUSD: ko.observable(0)
};

var BizSegmentModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var BranchModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var TypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ProductModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    WorkflowID: ko.observable(),
    Workflow: ko.observable()
};
var ProductTypeModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};
var ChannelModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var RMModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    RankID: ko.observable(),
    Rank: ko.observable(),
    SegmentID: ko.observable(),
    Segment: ko.observable(),
    BranchID: ko.observable(),
    Branch: ko.observable(),
    Email: ko.observable()
};

var ContactModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    PhoneNumber: ko.observable(),
    DateOfBirth: ko.observable(),
    Address: ko.observable(),
    IDNumber: ko.observable()
};

var FunctionModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var CurrencyModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var AccountModel = {
    AccountNumber: ko.observable()
};

var UnderlyingDocumentModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable()
};

var DocumentTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var DocumentPurposeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var SelectedModel = {
    Product: ko.observable(),
    Currency: ko.observable(),
    Channel: ko.observable(),
    Account: ko.observable(),
    BizSegment: ko.observable(),
    Bank: ko.observable(),
    DocumentType: ko.observable(),
    DocumentPurpose: ko.observable(),
    NewCustomer: ko.observable({
        Currency: ko.observable(),
        BizSegment: ko.observable()
    }),
    BankCharges: ko.observable(2),
    AgentCharges: ko.observable(2),
    FXCompliaance: ko.observable(),
    LLD: ko.observable(),
    POAFunction: ko.observable(),
    ProductType: ko.observable(),
    UnderlyingDoc: ko.observable(),
    DocumentPurpose_a: ko.observable(),
    DocumentType_a: ko.observable(),
    DebitCurrency: ko.observable(),
    NettingPurpose: ko.observable(),
};

var UnderlyingModel = {
    ID: ko.observable(),
    UnderlyingDocument: ko.observable(UnderlyingDocumentModel),
    DocumentType: ko.observable(DocumentTypeModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    AttachmentNo: ko.observable(),
    IsStatementLetter: ko.observable(false),
    IsDeclarationOfException: ko.observable(false),
    StartDate: ko.observable(),
    EndDate: ko.observable(),
    DateOfUnderlying: ko.observable(),
    ExpiredDate: ko.observable(),
    ReferenceNumber: ko.observable(),
    SupplierName: ko.observable(),
    InvoiceNumber: ko.observable()
};

var DocumentModel = {
    ID: ko.observable(),
    Type: ko.observable(DocumentTypeModel),
    Purpose: ko.observable(DocumentPurposeModel),
    DocumentPath: ko.observable(),
    IsNewDocument: ko.observable(false),
    LastModifiedDate: ko.observable(new Date),
    LastModifiedBy: ko.observable(),
    IsDormant: ko.observable(false)
};

var CustomerModel = {
    CIF: ko.observable(),
    Name: ko.observable(),
    POAName: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Branch: ko.observable(BranchModel),
    Type: ko.observable(TypeModel),
    RM: ko.observable(RMModel),
    Contacts: ko.observableArray([ContactModel]),
    Functions: ko.observableArray([FunctionModel]),
    Accounts: ko.observableArray([AccountModel]),
    Underlyings: ko.observableArray([UnderlyingModel])
};

var TMOModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    Channel: ko.observable(ChannelModel),
    TMODetails: ko.observable(),
    Remarks: ko.observable(),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]),
};

//add chandra : 20151123
var TransactionTMOModel = {
    TransactionDealID: ko.observable(),
    WorkflowInstanceID: ko.observable(),
    IsDraft: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    StatementLetter: ko.observable(StatementModel),
    BuyCurrency: ko.observable(CurrencyModel),
    SellCurrency: ko.observable(CurrencyModel),
    BuyAmount: ko.observable(),
    SellAmount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    TradeDate: ko.observable(),
    ValueDate: ko.observable(),
    Remarks: ko.observable(),
    TZReference: ko.observable(),
    SubmissionDateSL: ko.observable(),
    SubmissionDateInstruction: ko.observable(),
    SubmissionDateUnderlying: ko.observable(),
    IsUnderlyingCompleted: ko.observable(false),
    OtherUnderlying: ko.observable(),
    UnderlyingCodeID: ko.observable(),
    TransactionStatus: ko.observable(),
    Documents: ko.observableArray([]),
    AccountCurrencyCode: ko.observable(),
    AccountNumber: ko.observable(),
    ProductType: ProductTypeModel,
    IsOtherAccountNumber: ko.observable(false),
    OtherAccountNumber: ko.observable(),
    DebitCurrency: CurrencyModel,
    IsJointAccount: ko.observable(false),
    IsResident: ko.observable(),
    NB: ko.observable(),
    SwapType: ko.observable(),
    SwapTZDealNo: ko.observable(),
    SwapTransactionID: ko.observable(),
    IsNettingTransaction: ko.observable(false),
    Underlyings: ko.observableArray([]),
    CreateDate: ko.observable(),
    CreateBy: ko.observable(),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable()
};

var TMOMakerModel = function () {
    this.Transaction = ko.observable(TMOModel);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var TMOCheckerModel = function () {
    this.Transaction = ko.observable(TMOModel);
    this.Timelines = ko.observableArray([TimelineModel]);
    this.Verify = ko.observableArray([VerifyModel]);
};

var TrackingTMOModel = function () {
    this.Transaction = ko.observable(TransactionTMOModel);
    this.Timelines = ko.observableArray([TimelineModel]);
    this.Verify = ko.observableArray([VerifyModel]);
};

var CheckerModel = {
    ID: ko.observable(0),
    LLD: ko.observable(LLDModel)
};
var LLDModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var VerifyModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    IsVerified: ko.observable()
};

var TimelineModel = {
    ApproverID: ko.observable(),
    Activity: ko.observable(),
    Time: ko.observable(),
    Outcome: ko.observable(),
    UserOrGroup: ko.observable(),
    IsGroup: ko.observable(),
    DisplayName: ko.observable(),
    Comment: ko.observable()
};


var Documents = {
    Documents: self.Documents,
    DocumentPath: self.DocumentPath_a,
    Type: self.DocumentType_a,
    Purpose: self.DocumentPurpose_a
}


var CustomerUnderlyingMappingModel = {
    ID: ko.observable(0),
    UnderlyingID: ko.observable(0),
    UnderlyingFileID: ko.observable(0),
    AttachmentNo: ko.observable("")
}
//#region Function Model

var SelectModel = function (cif, id) {
    var self = this;
    self.ID = ko.observable(0);
    self.CIF = ko.observable(cif);
    self.UnderlyingID = ko.observable(id);
    self.ParentID = ko.observable(0);
}
var SelectBulkModel = function (cif, id) {
    var self = this;
    self.ID = ko.observable(0);
    self.CIF = ko.observable(cif);
    self.UnderlyingID = ko.observable(id);
    self.ParentID = ko.observable(0);
}
var UnderlyingDocumentModel2 = function (id, name, code) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Name = ko.observable(name);
    self.CodeName = ko.observable(code + ' (' + name + ')');
}
var DocumentTypeModel2 = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);

}
var StatementLetterModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}
var CurrencyModel2 = function (id, code, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Description = ko.observable(description);
}
var RateTypeModel2 = function (id, code, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Description = ko.observable(description);
}
var DocumentPurposeModel2 = function (id, name, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(description);
}
var CustomerUnderlyingMappingModel = function (FileID, UnderlyingID, attachNo) {
    var self = this;
    self.ID = ko.observable(0);
    self.UnderlyingID = ko.observable(UnderlyingID);
    self.UnderlyingFileID = ko.observable(FileID);
    self.AttachmentNo = ko.observable(attachNo);
}
var SelectUtillizeModel = function (id, enable, uSDAmount, statementLetter) {
    var self = this;
    self.ID = ko.observable(id);
    self.Enable = ko.observable(enable);
    self.USDAmount = ko.observable(uSDAmount);
    self.StatementLetter = ko.observable(statementLetter);
}

//#endregion
var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;

    // Sharepoint User
    self.SPUser = ko.observable();

    // count of document
    self.CountDoc = 0;

    // Custom Form for Menu Home
    self.FormMenuCustom = ko.observable();

    // Workflow task config
    self.WorkflowConfig = ko.observable();

    //aailable timelines tab
    self.IsTimelines = ko.observable(false);
    //global var
    self.TransactionDealID = ko.observable();
    self.CIF = ko.observable();

    // counter upload file ppu maker
    self.counterUpload = ko.observable(0);

    self.TransactionCheckerNetting = ko.observable(TransactionCheckerNettingModel);
    self.TransactionMakerNetting = ko.observable(TransactionMakerNettingModel);

    // parameters
    self.Products = ko.observableArray([]);
    self.Channels = ko.observableArray([]);
    self.DocumentTypes = ko.observableArray([]);
    self.DocumentPurposes = ko.observableArray([]);
    self.StatementLetter = ko.observableArray([]);
    self.Currencies = ko.observableArray([]);
    self.POAFunctions = ko.observableArray([]);
    self.UnderlyingDocumens = ko.observableArray([]);
    self.ProductTypes = ko.observableArray([]);

    //tbo
    self.IsToBeObtained = ko.observable();
    self.ToBeObtained = ko.observable();
    //end

    // task properties
    self.ApproverId = ko.observable();
    self.SPWebID = ko.observable();
    self.SPSiteID = ko.observable();
    self.SPListID = ko.observable();
    self.SPListItemID = ko.observable();
    self.SPTaskListID = ko.observable();
    self.SPTaskListItemID = ko.observable();
    self.Initiator = ko.observable();
    self.WorkflowInstanceID = ko.observable();
    self.WorkflowID = ko.observable();
    self.WorkflowName = ko.observable();
    self.StartTime = ko.observable();
    self.StateID = ko.observable();
    self.StateDescription = ko.observable();
    self.IsAuthorized = ko.observable();
    self.IsCSOInitiator = ko.observable(false);
    self.TaskTypeID = ko.observable();
    self.TaskTypeDescription = ko.observable();
    self.Title = ko.observable();
    self.ActivityTitle = ko.observable();
    self.User = ko.observable();
    self.IsSPGroup = ko.observable();
    self.EntryTime = ko.observable();
    self.ExitTime = ko.observable();
    self.OutcomeID = ko.observable();
    self.OutcomeDescription = ko.observable();
    self.CustomOutcomeID = ko.observable();
    self.CustomOutcomeDescription = ko.observable();
    self.Comments = ko.observable();
    self.FCYIDRTresHold = ko.observable(0);
    //basri 28-09-2015  
    self.InitialProduct = ko.observable();
    //end basri
    self.NettingPurpose = ko.observableArray([]);

    //TMO Transaction Details
    self.TMOMaker = ko.observable(TMOMakerModel);
    self.TMOChecker = ko.observable(TMOCheckerModel);
    //#region Add Chandra A (variable)
    self.TrackingTMOChecker = ko.observable(TrackingTMOModel); // add chandra : 20151123
    self.TrackingTMOMaker = ko.observable(TrackingTMOModel); // add chandra : 20151123
    self.AmountModel = ko.observable(AmountModel);
    self.IsEditTableUnderlying = ko.observable(true);
    self.tempAmountBulk = ko.observable(0);
    self.taskName = ko.observable();
    //#endregion

    // filter Attach Underlying File
    self.AttachFilterFileName = ko.observable("");
    self.AttachFilterDocumentPurpose = ko.observable("");
    self.AttachFilterModifiedDate = ko.observable("");
    self.AttachFilterDocumentRefNumber = ko.observable("");

    // Task status from Nintex custom REST API
    self.IsPendingNintex = ko.observable();
    self.IsAuthorizedNintex = ko.observable();
    self.MessageNintex = ko.observable();
    // Add Chandra : 20151123
    self.IsTMOChecker = ko.observable();
    self.isNewUnderlying = ko.observable(false);
    // Approval definition at tmo home page
    self.IsPendingNintexHome = ko.observable();
    self.IsAuthorizedNintexHome = ko.observable();
    self.MessageNintexHome = ko.observable();
    self.SPTaskListIDHome = ko.observableArray([]);
    self.SPTaskListItemIDHome = ko.observableArray([]);
    self.TaskTypeIDHome = ko.observableArray([]);
    self.ActivityTitleHome = ko.observableArray([]);
    self.WorkflowInstanceIDHome = ko.observableArray([]);
    self.ApproverIDHome = ko.observableArray([]);
    self.OutcomeIDHome = ko.observableArray([]);

    self.RowCount = ko.observable();

    // Task activity properties
    self.PPUChecker = ko.observable();

    //is fx transaction
    self.t_IsFxTransaction = ko.observable(false);
    self.t_IsFxTransactionToIDR = ko.observable(false);
    //#region Joint Account
    self.CIFJointAccounts = ko.observableArray([{ ID: false, Name: "Single" }, { ID: true, Name: "Join" }]);
    self.DynamicAccounts = ko.observableArray([]);
    self.IsJointAccount = ko.observable(false);
    self.IsHitThreshold = ko.observable(false);
    self.ThresholdValue = ko.observable(0);
    self.IsNoThresholdValue = ko.observable(false)
    self.IsEmptyAccountNumber = ko.observable(false);
    self.IsCanEditUnderlying = ko.observable(false);
    //self.ThresholdValue = ko.observable(ThresholdModel);
    self.ThresholdType = ko.observable();
    //#endregion
    //// strat Checker Calculate for Underlying Grid
    self.TempSelectedUnderlying = ko.observableArray([]);
    self.IsUploading = ko.observable(false);
    self.SelectingUnderlying = ko.observable(false);
    self.IsShowUnderlyingDocuments = ko.observable(true);
    self.IsAnnualStatement = ko.observable(false);
    self.IsUnderlyingCompletedEmpty = ko.observable(false);
    self.IsSubmissionDateSLEmpty = ko.observable(false);
    self.IsSwapTransaction = ko.observable(false);
    self.IsNettingTransaction = ko.observable(false);
    self.MessageTransactionType = ko.observable(false);
    self.Swap_NettingDFNumber = ko.observable();
    //self.PPUChecker(new PPUCheckerModel());
    //alert(self.PPUChecker().data());
    // New Data flag
    self.IsNewDataUnderlying = ko.observable(false);
    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyings = ko.observableArray([]);
    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyingProformas = ko.observableArray([]);
    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyingFiles = ko.observableArray([]);
    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerAttachUnderlyings = ko.observableArray([]);
    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerBulkUnderlyings = ko.observableArray([]); // add 2015.03.09
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));
    // grid properties Underlying
    self.UnderlyingGridProperties = ko.observable();
    self.UnderlyingGridProperties(new GridPropertiesModel(GetDataUnderlying));

    // grid properties Attach Grid
    self.AttachGridProperties = ko.observable();
    self.AttachGridProperties(new GridPropertiesModel(GetDataAttachFile));

    // grid properties Underlying File
    self.UnderlyingAttachGridProperties = ko.observable();
    self.UnderlyingAttachGridProperties(new GridPropertiesModel(GetDataUnderlyingAttach));

    // grid properties Proforma
    self.UnderlyingProformaGridProperties = ko.observable();
    self.UnderlyingProformaGridProperties(new GridPropertiesModel(GetDataUnderlyingProforma));

    // grid properties Bulk // 2015.03.09
    self.UnderlyingBulkGridProperties = ko.observable();
    self.UnderlyingBulkGridProperties(new GridPropertiesModel(GetDataBulkUnderlying));

    // set default sorting for Underlying Grid
    self.UnderlyingGridProperties().SortColumn("ID");
    self.UnderlyingGridProperties().SortOrder("ASC");

    // set default sorting for Underlying File Grid
    self.AttachGridProperties().SortColumn("FileName");
    self.AttachGridProperties().SortOrder("ASC");

    // set default sorting for Attach Underlying Grid
    self.UnderlyingAttachGridProperties().SortColumn("ID");
    self.UnderlyingAttachGridProperties().SortOrder("DESC");
    // set default sorting for Proforma grid
    self.UnderlyingProformaGridProperties().SortColumn("IsSelectedProforma");
    self.UnderlyingProformaGridProperties().SortOrder("DESC");
    // set default sorting for Bulk grid //add 2015.03.09
    self.UnderlyingBulkGridProperties().SortColumn("IsSelectedBulk");
    self.UnderlyingBulkGridProperties().SortOrder("DESC");

    self.GridProperties().SortColumn("LastModifiedDate");
    self.GridProperties().SortOrder("DESC");
    // set default sorting for Underlying Grid
    self.UnderlyingGridProperties().SortColumn("ID");
    self.UnderlyingGridProperties().SortOrder("ASC");

    // Options selected value
    self.Selected = ko.observable(SelectedModel);
    //#region Variable Filtering for Table
    // filters transaction

    self.FilterWorkflowName = ko.observable("");
    self.FilterInitiator = ko.observable("");
    self.FilterStateDescription = ko.observable("");
    self.FilterStartTime = ko.observable("");
    self.FilterTitle = ko.observable("");
    self.FilterTransactionStatus = ko.observable("");
    self.FilterUser = ko.observable("");
    self.FilterUserApprover = ko.observable("");
    self.FilterEntryTime = ko.observable("");
    self.FilterExitTime = ko.observable("");
    self.FilterOutcomeDescription = ko.observable("");
    self.FilterCustomOutcomeDescription = ko.observable("");
    self.FilterComments = ko.observable("");
    // filter Underlying
    self.UnderlyingFilterCIF = ko.observable("");
    self.UnderlyingFilterParentID = ko.observable("");
    self.UnderlyingFilterIsSelected = ko.observable(false);
    self.UnderlyingFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingFilterDocumentType = ko.observable("");
    self.UnderlyingFilterCurrency = ko.observable("");
    self.UnderlyingFilterStatementLetter = ko.observable("");
    self.UnderlyingFilterAmount = ko.observable("");
    self.UnderlyingFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingFilterExpiredDate = ko.observable("");
    self.UnderlyingFilterReferenceNumber = ko.observable("");
    self.UnderlyingFilterInvoiceNumber = ko.observable("");
    self.UnderlyingFilterSupplierName = ko.observable("");
    self.UnderlyingFilterIsProforma = ko.observable("");
    self.UnderlyingFilterIsAvailable = ko.observable(false);
    self.UnderlyingFilterIsExpiredDate = ko.observable(false);
    self.UnderlyingFilterIsSelectedBulk = ko.observable(false);
    self.UnderlyingFilterAvailableAmount = ko.observable("");
    self.UnderlyingFilterAttachmentNo = ko.observable("");
    self.UnderlyingFilterIsTMO = ko.observable(false);
    self.UnderlyingFilterAccountNumber = ko.observable("");
    self.UnderlyingFilterShow = ko.observable(1);
    // filter Attach Underlying File
    self.AttachFilterFileName = ko.observable("");
    self.AttachFilterDocumentPurpose = ko.observable("");
    self.AttachFilterModifiedDate = ko.observable("");
    self.AttachFilterDocumentRefNumber = ko.observable("");
    self.AttachFilterAttachmentReference = ko.observable("");
    // filter Underlying for Attach
    self.UnderlyingAttachFilterIsSelected = ko.observable("");
    self.UnderlyingAttachFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingAttachFilterDocumentType = ko.observable("");
    self.UnderlyingAttachFilterCurrency = ko.observable("");
    self.UnderlyingAttachFilterStatementLetter = ko.observable("");
    self.UnderlyingAttachFilterAmount = ko.observable("");
    self.UnderlyingAttachFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingAttachFilterExpiredDate = ko.observable("");
    self.UnderlyingAttachFilterReferenceNumber = ko.observable("");
    self.UnderlyingAttachFilterSupplierName = ko.observable("");
    self.UnderlyingAttachFilterAccountNumber = ko.observable("");
    self.UnderlyingAttachFilterIsTMO = ko.observable(true);
    // filter Underlying for Attach
    self.ProformaFilterIsSelected = ko.observable(false);
    self.ProformaFilterUnderlyingDocument = ko.observable("");
    self.ProformaFilterDocumentType = ko.observable("Proforma");
    self.ProformaFilterCurrency = ko.observable("");
    self.ProformaFilterStatementLetter = ko.observable("");
    self.ProformaFilterAmount = ko.observable("");
    self.ProformaFilterDateOfUnderlying = ko.observable("");
    self.ProformaFilterExpiredDate = ko.observable("");
    self.ProformaFilterReferenceNumber = ko.observable("");
    self.ProformaFilterInvoiceNumber = ko.observable("");
    self.ProformaFilterSupplierName = ko.observable("");
    self.ProformaFilterIsProforma = ko.observable(false);
    // filter Underlying for Attach // add 2015.03.09
    self.BulkFilterIsSelected = ko.observable(false);
    self.BulkFilterUnderlyingDocument = ko.observable("");
    self.BulkFilterDocumentType = ko.observable("");
    self.BulkFilterCurrency = ko.observable("");
    self.BulkFilterStatementLetter = ko.observable("");
    self.BulkFilterAmount = ko.observable("");
    self.BulkFilterDateOfUnderlying = ko.observable("");
    self.BulkFilterExpiredDate = ko.observable("");
    self.BulkFilterReferenceNumber = ko.observable("");
    self.BulkFilterSupplierName = ko.observable("");
    self.BulkFilterInvoiceNumber = ko.observable("");
    self.BulkFilterIsBulkUnderlying = ko.observable(false);
    self.utilizationAmount = ko.observable(0.00);
    self.Rounding = ko.observable(0.00);
    self.ParameterRounding = ko.observable(0);
    //#endregion
    self.FilterApplicationID = ko.observable("");
    self.FilterCustomer = ko.observable("");
    self.FilterCIF = ko.observable("");
    self.FilterProduct = ko.observable("");
    self.FilterChannel = ko.observable("");

    // New Data flag
    self.IsNewData = ko.observable(false);

    // check all data
    self.CheckAll = ko.observable(false);

    //Declare an ObservableArray for Storing the JSON Response
    self.Tasks = ko.observableArray([]);
    self.Outcomes = ko.observableArray([]);
    self.HomeOutcomes = ko.observableArray([]);
    self.HomeOutcomesRow = ko.observableArray([]);

    self.IsJointAccount_u = ko.observable(false);
    //#region Bind clear filters
    self.ClearFilters = function () {
        self.FilterWorkflowName("");
        self.FilterInitiator("");
        self.FilterStateDescription("");
        self.FilterStartTime("");
        self.FilterTitle("");
        self.FilterTransactionStatus("");
        self.FilterUser("");
        self.FilterUserApprover("");
        self.FilterEntryTime("");
        self.FilterExitTime("");
        self.FilterOutcomeDescription("");
        self.FilterCustomOutcomeDescription("");
        self.FilterComments("");

        self.FilterApplicationID("");
        self.FilterCustomer("");
        self.FilterCIF("");
        self.FilterProduct("");
        self.FilterChannel("");

        GetData();
    };
    self.UnderlyingClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.UnderlyingFilterIsSelected(false);
        self.UnderlyingFilterUnderlyingDocument("");
        self.UnderlyingFilterDocumentType("");
        self.UnderlyingFilterCurrency("");
        self.UnderlyingFilterStatementLetter("");
        self.UnderlyingFilterAmount("");
        self.UnderlyingFilterDateOfUnderlying("");
        self.UnderlyingFilterExpiredDate("");
        self.UnderlyingFilterReferenceNumber("");
        self.UnderlyingFilterInvoiceNumber("");
        self.UnderlyingFilterSupplierName("");
        self.UnderlyingFilterIsProforma("");
        self.UnderlyingFilterIsAvailable(false);
        self.UnderlyingFilterIsExpiredDate(false);

        GetDataUnderlying();
    };
    self.ProformaClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.ProformaFilterIsSelected(false);
        self.ProformaFilterUnderlyingDocument("");
        self.ProformaFilterDocumentType("Proforma");
        self.ProformaFilterCurrency("");
        self.ProformaFilterStatementLetter("");
        self.ProformaFilterAmount("");
        self.ProformaFilterDateOfUnderlying("");
        self.ProformaFilterExpiredDate("");
        self.ProformaFilterReferenceNumber("");
        self.ProformaFilterInvoiceNumber("");
        self.ProformaFilterSupplierName("");
        self.ProformaFilterIsProforma("");

        GetDataUnderlyingProforma();
    };
    self.BulkClearFilters = function () {
        self.BulkFilterIsSelected(false);
        self.BulkFilterUnderlyingDocument("");
        self.BulkFilterDocumentType("");
        self.BulkFilterCurrency("");
        self.BulkFilterStatementLetter("");
        self.BulkFilterAmount("");
        self.BulkFilterDateOfUnderlying("");
        self.BulkFilterExpiredDate("");
        self.BulkFilterReferenceNumber("");
        self.BulkFilterInvoiceNumber("");
        self.BulkFilterSupplierName("");
        self.BulkFilterIsBulkUnderlying("");
        GetDataBulkUnderlying();
    };
    self.AttachClearFilters = function () {
        self.AttachFilterFileName("");
        self.AttachFilterDocumentPurpose("");
        self.AttachFilterModifiedDate("");
        self.AttachFilterDocumentRefNumber("");
        self.AttachFilterAttachmentReference("");
        GetDataAttachFile();
    };
    self.UnderlyingAttachClearFilters = function () {
        self.UnderlyingAttachFilterIsSelected(false);
        self.UnderlyingAttachFilterUnderlyingDocument("");
        self.UnderlyingAttachFilterDocumentType("");
        self.UnderlyingAttachFilterCurrency("");
        self.UnderlyingAttachFilterStatementLetter("");
        self.UnderlyingAttachFilterAmount("");
        self.UnderlyingAttachFilterDateOfUnderlying("");
        self.UnderlyingAttachFilterExpiredDate("");
        self.UnderlyingAttachFilterReferenceNumber("");
        self.UnderlyingAttachFilterSupplierName("");
        GetDataUnderlyingAttach();
    };
    //#endregion
    self.ValidationUtilize = function () {
        var isStatus = false;
        var t_TreshHold = TotalDealModel.TreshHold;
        var t_TotalAmountsUSD = parseFloat(viewModel.AmountModel().TotalAmountsUSD());
        var t_AmountUSD = viewModel.TrackingTMOMaker().Transaction.AmountUSD();
        var t_UtilizeAmount = parseFloat(viewModel.utilizationAmount());

        if (t_AmountUSD <= t_UtilizeAmount) {
            if ((t_TotalAmountsUSD > t_TreshHold && viewModel.TempSelectedUnderlying()[0].StatementLetter() == CONST_STATEMENT.StatementA_ID &&
                viewModel.TempSelectedUnderlying()[0].StatementLetter() == CONST_STATEMENT.StatementA_ID)) {
                ShowNotification('Form Underlying Warning", "Total Transaction greater than ' + t_TreshHold + ' (USD), Please add statement B underlying', 'gritter-warning', false);
                isStatus = false;
            } else {
                isStatus = true;
            }
        } else {
            ShowNotification("Form Underlying Warning", CONST_MSG.ValidationMinTotal, 'gritter-warning', false);
            isStatus = false;
        }
        return isStatus;
    }


    self.GetSelectedContactRow = function (contact) {
        self.IsNewData(false);

        self.ID_c(contact.ID);
        self.SourceID_c('2');
        self.Name_c(contact.Name);
        self.PhoneNumber_c(contact.PhoneNumber);

        self.Address_c(contact.Address);
        self.IDNumber_c(contact.IDNumber);
        self.Selected().POAFunction(contact.POAFunction.ID);
        self.POAFunction_c(contact.POAFunction);
        self.OccupationInID_c(contact.OccupationInID);
        self.PlaceOfBirth_c(contact.PlaceOfBirth);
        self.DateOfBirth_c(self.LocalDate(contact.DateOfBirth, true, false));
        self.EffectiveDate_c(self.LocalDate(contact.EffectiveDate, true, false));
        self.CancellationDate_c(self.LocalDate(contact.CancellationDate, true, false));
        self.POAFunctionOther_c(contact.POAFunctionOther);
        //self.DateOfBirth_c(moment(contact.DateOfBirth).format('MM-DD-YYYY'));
        //self.EffectiveDate_c(moment(contact.EffectiveDate).format('MM-DD-YYYY'));
        //self.CancellationDate_c(moment(contact.CancellationDate).format('MM-DD-YYYY'));

        //var zind = getStyle("modal-form-child", "zIndex");
        //alert(zind);

        $("#modal-form-child").modal('show');
        $('#backDrop').show();
        //$("#modal-form-child").modal({backdrop: 'fixed'});
    }
    // customer contact
    self.ID_c = ko.observable();
    self.CIF_c = ko.observable("");
    self.SourceID_c = ko.observable("2");
    self.Name_c = ko.observable("");
    self.PhoneNumber_c = ko.observable("");
    self.DateOfBirth_c = ko.observable('');
    self.Address_c = ko.observable("");
    self.IDNumber_c = ko.observable("");
    self.POAFunction_c = ko.observable();
    self.OccupationInID_c = ko.observable("");
    self.PlaceOfBirth_c = ko.observable("");
    self.EffectiveDate_c = ko.observable("");
    self.CancellationDate_c = ko.observable("");
    self.POAFunctionOther_c = ko.observable("");
    self.LastModifiedBy_c = ko.observable("");
    self.LastModifiedDate_c = ko.observable('');

    //--------------------------- set Underlying function & variable start
    self.IsViewTransaction = ko.observable(false);
    self.IsUnderlyingMode = ko.observable(true);
    self.ID_u = ko.observable("");
    self.CIF_u = ko.observable(cifData);
    self.CustomerName_u = ko.observable(customerNameData);
    self.UnderlyingDocument_u = ko.observable(new UnderlyingDocumentModel2('', '', ''));
    self.ddlUnderlyingDocument_u = ko.observableArray([]);
    self.DocumentType_u = ko.observable(new DocumentTypeModel2('', ''));
    self.ddlDocumentType_u = ko.observableArray([]);
    self.AmountUSD_u = ko.observable(0.00);
    self.Currency_u = ko.observable(new CurrencyModel2('', '', ''));
    self.Rate_u = ko.observable(0);
    self.ddlCurrency_u = ko.observableArray([]);
    self.StatementLetter_u = ko.observable(new StatementLetterModel('', ''));
    self.ddlStatementLetter = ko.observableArray([]);
    self.ddlStatementLetter_u = ko.observableArray([]);
    self.Amount_u = ko.observable(0);
    self.AttachmentNo_u = ko.observable("");
    self.SelectUnderlyingDocument_u = ko.observable("");
    self.IsDeclarationOfException_u = ko.observable(false);
    self.StartDate_u = ko.observable('');
    self.EndDate_u = ko.observable('');
    self.DateOfUnderlying_u = ko.observable("");
    self.ExpiredDate_u = ko.observable("");
    self.ReferenceNumber_u = ko.observable("");
    self.SupplierName_u = ko.observable("");
    self.InvoiceNumber_u = ko.observable("");
    self.IsStatementA = ko.observable(true);
    self.IsProforma_u = ko.observable(false);
    self.IsSelectedProforma = ko.observable(false);
    self.IsUtilize_u = ko.observable(false);
    self.IsTMO_u = ko.observable(true);
    self.AccountNumber_u = ko.observable();
    // add bulkUnderlying // add 2015.03.09
    self.IsBulkUnderlying_u = ko.observable(false);
    self.IsSelectedBulk = ko.observable(false);
    /*self.Amounttmp_u = ko.computed({
        read: function () {
            if (document.getElementById("Amount_u") != null) {
                var fromFormat = document.getElementById("Amount_u").value;
                fromFormat = fromFormat.toString().replace(/,/g, '');
                if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementA_ID) {
                    self.Currency_u().ID(1);
                    self.Amount_u(formatNumber(DataModel.ThresholdBuy));
                    var date = new Date();
                    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                    var day = lastDay.getDate();
                    var month = lastDay.getMonth() + 1;
                    var year = lastDay.getFullYear();
                    var fixLastDay = year + "/" + month + "/" + day;
                    self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
                    SetDefaultValueStatementA();
                } else if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementB_ID && self.DateOfUnderlying_u() != "" && self.IsNewDataUnderlying()) {
                    if (self.LocalDate(self.DateOfUnderlying_u()) != "") {
                        var date = new Date(self.DateOfUnderlying_u());
                        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                        var day = date.getDate();
                        var month = lastDay.getMonth() + 13;
                        var year = lastDay.getFullYear();
                        if (month > 12) {
                            month -= 12;
                            year += 1;
                        }
                        var fixLastDay = year + "/" + month + "/" + day;
                        if (!moment(fixLastDay, 'YYYY/MM/DD').isValid()) {
                            var LastDay_ = new Date(year, month, 0);
                            day = LastDay_.getDate();
                        }
                        self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
                    }
                }

                var e = document.getElementById("Amount_u").value;
                e = e.toString().replace(/,/g, '');

                var res = parseFloat(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
                res = Math.round(res * 100) / 100;
                res = isNaN(res) ? 0 : res; //avoid NaN
                self.AmountUSD_u(parseFloat(res).toFixed(2));
                self.Amount_u(formatNumber(e));
            }
        },
        write: function (data) {
            var fromFormat = document.getElementById("Amount_u").value;
            fromFormat = fromFormat.toString().replace(/,/g, '');

            var res = parseInt(fromFormat) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res).toFixed(2));
            self.Amount_u(formatNumber(fromFormat));

        }
    }, this); */
    self.CaluclateRate_u = ko.computed({
        read: function () {
            var CurrencyID = self.Currency_u().ID();
            var AccountID = self.Selected().Account;
            if (CurrencyID != '' && CurrencyID != undefined) {
                GetRateIDRAmount(CurrencyID);
            } else {
                self.Rate_u(0);
            }
            if (AccountID == '' && AccountID != undefined) {
                // alert('test');
            }
        },
        write: function () { }
    });
    self.Proformas = ko.observableArray([new SelectModel('', '')]);
    self.BulkUnderlyings = ko.observableArray([new SelectBulkModel('', '')]); // add 2015.03.09
    self.CodeUnderlying = ko.computed({
        read: function () {
            if (self.UnderlyingDocument_u().ID() != undefined && self.UnderlyingDocument_u().ID() != '') {
                var item = ko.utils.arrayFirst(self.ddlUnderlyingDocument_u(), function (x) {
                    return self.UnderlyingDocument_u().ID() == x.ID();
                }
                )
                if (item != null) {
                    return item.Code();
                }
                else {
                    return ''
                }
            } else { return '' }
        },
        write: function (value) {
            return value;
        }
    });
    self.OtherUnderlying_u = ko.observable('');
    self.tmpOtherUnderlying = ko.observable('');
    self.IsSelected_u = ko.observable(false);
    self.LastModifiedBy_u = ko.observable("");
    self.LastModifiedDate_u = ko.observable("");
    // Temporary documents
    self.Documents = ko.observableArray([]);
    self.DocumentPath = ko.observable();
    self.DocumentType = ko.observable();
    self.DocumentPurpose = ko.observable();
    self.DocumentFileName = ko.observable();
    // temp amount_u for bulk underlying // add 2015.03.09
    self.tempAmountBulk = ko.observable(0);
    //// start Checked Calculate for Attach Grid and Underlying Grid
    self.TempSelectedAttachUnderlying = ko.observableArray([]);
    //// start Checked Calculate for Underlying Proforma Grid
    self.TempSelectedUnderlyingProforma = ko.observableArray([]);
    //// start Checked Calculate for Underlying Bulk Grid // add 2015.03.09
    self.TempSelectedUnderlyingBulk = ko.observableArray([]);
    //// start Checked Calculate for Underlying Grid
    self.TempSelectedUnderlying = ko.observableArray([]);
    // Attach Variable
    self.ID_a = ko.observable(0);
    self.ddlDocumentPurpose_a = ko.observableArray([]);
    self.tempddlDocumentPurpose_a = ko.observableArray([]);
    self.ddlDocumentType_a = ko.observableArray([]);
    self.DocumentPurpose_a = ko.observable(new DocumentPurposeModel2('', '', ''));
    self.DocumentPath_a = ko.observable();
    self.DocumentType_a = ko.observable(new DocumentTypeModel2('', ''));
    self.CustomerUnderlyingMappings_a = ko.observableArray([CustomerUnderlyingMappingModel]);
    self.MakerDocuments = ko.observableArray([]);
    self.MakerAllDocumentsTemp = ko.observableArray([]);
    self.MakerUnderlyings = ko.observableArray([]);
    self.NewDocuments = ko.observableArray([]);
    self.ReviseDealDocuments = ko.observableArray([]);
    // Color Status 
    self.SetColorStatus = function (id, applicationID, outcomeDescription, customOutcomeDescription) {
        if (outcomeDescription == null) {
            outcomeDescription = '';
        }

        if (customOutcomeDescription == null) {
            customOutcomeDescription = '';
        }

        if (id == 2) {
            return "active";
        }
        else if (id == 4) {
            //Untuk handle FX Checker reject dan PPU Checker Approve Cancellation
            if ((applicationID.toLowerCase().startsWith('fx') && outcomeDescription.toLowerCase() == 'rejected') || (customOutcomeDescription.toLowerCase() == 'approve cancellation')) {
                return "warning";
            }
            else {
                return "success";
            }
        }
        else if (id == 8) {
            return "warning";
        }
        else if (id == 64) {
            return "danger";
        }
        else {
            return "";
        }
    };

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        if (ko.isObservable(date)) {
            date = date();
        }

        var utcDate = moment.utc(date);
        if (utcDate.isValid()) {
            if (utcDate == '1970/01/01 00:00:00' || utcDate == '1970-01-01T00:00:00' || utcDate == null) {
                return null;
            }
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(utcDate).local().format(config.format.dateLong);
                else
                    return moment(utcDate).local().format(config.format.date);
            }
            else {
                return moment(utcDate).local().format(config.format.dateTime);
            }
        }
        else {
            return null;
        }
    };

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        //console.log(data)
        self.ApproverId(data.WorkflowContext.ApproverID);
        self.SPWebID(data.WorkflowContext.SPWebID);
        self.SPSiteID(data.WorkflowContext.SPSiteID);
        self.SPListID(data.WorkflowContext.SPListID);
        self.SPListItemID(data.WorkflowContext.SPListItemID);
        self.SPTaskListID(data.WorkflowContext.SPTaskListID);
        self.SPTaskListItemID(data.WorkflowContext.SPTaskListItemID);
        self.Initiator(data.WorkflowContext.Initiator);
        self.WorkflowInstanceID(data.WorkflowContext.WorkflowInstanceID);
        self.WorkflowID(data.WorkflowContext.WorkflowID);
        self.WorkflowName(data.WorkflowContext.WorkflowName);
        self.StartTime(data.WorkflowContext.StartTime);
        self.StateID(data.WorkflowContext.StateID);
        self.StateDescription(data.WorkflowContext.StateDescription);
        self.IsAuthorized(data.WorkflowContext.IsAuthorized);
        self.IsCSOInitiator(data.WorkflowContext.IsCSOInitiator);

        self.TaskTypeID(data.CurrentTask.TaskTypeID);
        self.TaskTypeDescription(data.CurrentTask.TaskTypeDescription);
        self.Title(data.CurrentTask.Title);
        self.ActivityTitle(data.CurrentTask.ActivityTitle);
        self.User(data.CurrentTask.User);
        self.IsSPGroup(data.CurrentTask.IsSPGroup);
        self.EntryTime(data.CurrentTask.EntryTime);
        self.ExitTime(data.CurrentTask.ExitTime);
        self.OutcomeID(data.CurrentTask.OutcomeID);
        self.OutcomeDescription(data.CurrentTask.OutcomeDescription);
        self.CustomOutcomeID(data.CurrentTask.CustomOutcomeID);
        self.CustomOutcomeDescription(data.CurrentTask.CustomOutcomeDescription);
        self.Comments(data.CurrentTask.Comments);
        //basri 28-09-2015        
        self.InitialProduct(data.Transaction.ApplicationID.substring(0, 2).toLowerCase());
        //end basri

        self.Outcomes.removeAll();
        // add chandra set original value

        // show the dialog task form
        $("#modal-form").modal('show');

        GetTaskOutcomes(data.WorkflowContext.SPTaskListID, data.WorkflowContext.SPTaskListItemID);

    };
    //15-2-2016 dani
    self.IsCSO = ko.observable();
    function GetInitiatorRole(username) {
        var un = username.lastIndexOf("|");
        var UN = username.substring(un + 1)
        var options = {
            url: api.server + api.url.workflow.isCso(UN),
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetIsCSO, OnError, OnAlways);
    }
    function OnSuccessGetIsCSO(data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            viewModel.IsCSO(data);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }
    //15-2-2016 end dani
    self.onSelectionAttach = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            self.TempSelectedAttachUnderlying.remove(data);
            ResetDataAttachment(index, false);

        } else {
            self.TempSelectedAttachUnderlying.push({
                ID: item.ID
            });
            ResetDataAttachment(index, true);
        }
    }

    self.OnChangeStatementLetterUnderlying = function () {
        var fromFormat = document.getElementById("Amount_u").value;
        fromFormat = fromFormat.toString().replace(/,/g, '');
        if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementA_ID) {
            self.Currency_u().ID(1);
            self.Amount_u(formatNumber(DataModel.ThresholdBuy));
            var date = new Date();
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var day = lastDay.getDate();
            var month = lastDay.getMonth() + 1;
            var year = lastDay.getFullYear();
            var fixLastDay = year + "/" + month + "/" + day;
            self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
            SetDefaultValueStatementA();
        } else if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementB_ID && self.DateOfUnderlying_u() != "" && self.IsNewDataUnderlying()) {
            if (self.LocalDate(self.DateOfUnderlying_u()) != "") {
                var date = new Date(self.DateOfUnderlying_u());
                var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                var day = date.getDate();
                var month = lastDay.getMonth() + 13;
                var year = lastDay.getFullYear();
                if (month > 12) {
                    month -= 12;
                    year += 1;
                }
                var fixLastDay = year + "/" + month + "/" + day;
                if (!moment(fixLastDay, 'YYYY/MM/DD').isValid()) {
                    var LastDay_ = new Date(year, month, 0);
                    day = LastDay_.getDate();
                }
                self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
            }
        }

        var e = document.getElementById("Amount_u").value;
        e = e.toString().replace(/,/g, '');

        var res = parseFloat(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
        res = Math.round(res * 100) / 100;
        res = isNaN(res) ? 0 : res; //avoid NaN
        self.AmountUSD_u(parseFloat(res).toFixed(2));
        self.Amount_u(formatNumber(e));
        /* if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementA_ID) {
             self.Currency_u().ID(1);
             self.Amount_u(formatNumber(DataModel.ThresholdBuy));
             // set default last day of month
             var date = new Date();
             var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
             var day = lastDay.getDate();
             var month = lastDay.getMonth() + 1;
             var year = lastDay.getFullYear()
             var fixLastDay = year + "/" + month + "/" + day;
             self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
             SetDefaultValueStatementA();
         } else if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementB_ID && self.IsNewDataUnderlying()) {
             self.ClearUnderlyingData();
             if (self.LocalDate(self.DateOfUnderlying_u()) != "") {
                 var date = new Date(self.DateOfUnderlying_u());
                 var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                 var day = date.getDate();
                 var month = lastDay.getMonth() + 13;
                 var year = lastDay.getFullYear();
                 if (month > 12) {
                     month -= 12;
                     year += 1;
                 }
                 var fixLastDay = year + "/" + month + "/" + day;
                 if (!moment(fixLastDay, 'YYYY/MM/DD').isValid()) {
                     var LastDay_ = new Date(year, month, 0);
                     day = LastDay_.getDate();
                 }
                 self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
             }
         }
 
         var e = document.getElementById("Amount_u").value;
         e = e.toString().replace(/,/g, '');
 
         var res = parseFloat(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
         res = Math.round(res * 100) / 100;
         res = isNaN(res) ? 0 : res; //avoid NaN
         self.AmountUSD_u(parseFloat(res).toFixed(2));
         self.Amount_u(formatNumber(e)); */

    }
    self.onChangeDateOfUnderlying = function () {
        if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementA_ID) {
            self.Currency_u().ID(1);
            self.Amount_u(formatNumber(DataModel.ThresholdBuy));
            var date = new Date();
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var day = lastDay.getDate();
            var month = lastDay.getMonth() + 1;
            var year = lastDay.getFullYear();
            var fixLastDay = year + "/" + month + "/" + day;
            self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
            SetDefaultValueStatementA();
        } else if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementB_ID && self.DateOfUnderlying_u() != "" && self.IsNewDataUnderlying()) {
            if (self.LocalDate(self.DateOfUnderlying_u()) != "") {
                var date = new Date(self.DateOfUnderlying_u());
                var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                var day = date.getDate();
                var month = lastDay.getMonth() + 13;
                var year = lastDay.getFullYear();
                if (month > 12) {
                    month -= 12;
                    year += 1;
                }
                var fixLastDay = year + "/" + month + "/" + day;
                if (!moment(fixLastDay, 'YYYY/MM/DD').isValid()) {
                    var LastDay_ = new Date(year, month, 0);
                    day = LastDay_.getDate();
                }
                self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
            }
        }
    }
    self.OnChangePropose = function () {
        if (self.DocumentPurpose_a().ID() == 2) {
            self.SelectingUnderlying(true);
        } else {
            self.SelectingUnderlying(false)
        }
    }

    self.onSelectionProforma = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlyingProforma(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            self.TempSelectedUnderlyingProforma.remove(data);
            ResetDataProforma(index, false);
        } else {
            self.TempSelectedUnderlyingProforma.push({
                ID: item.ID
            });
            ResetDataProforma(index, true);
        }
    }
    // set approval ui template
    self.ApprovalTemplate = function () {
        var output;

        switch (self.ActivityTitle()) {
            case DBS.AcivityTitle.TMOMakerTask:
                output = "TaskTMOMaker";
                break;
            case DBS.AcivityTitle.TMOCheckerTask:
                output = "TaskTMOChecker";
                break;
            case DBS.AcivityTitle.TMOCheckerCancellationTask:
                output = "TaskTMOCheckerCanceled";
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerTask:
            case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
                output = "TaskTrackingTMOChecker"
                break;
            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                output = "TaskTrackingTMOMakerReopen"
                break;
            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
                output = "TaskTrackingTMOMaker"
                break;
            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
                output = "TaskTrackingTMOMakerAfterSubmit"
                break;
            case DBS.AcivityTitle.FXTMOCheckerRequestCancelTask:
                output = "TaskTrackingTMORequestCancel";
                break;
            case DBS.AcivityTitle.FXDealTMOMakerNettingTask:
                //output = "TaskFXNettingChecker";
                output = "TaskFXNettingMaker";
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerNettingTask:
                output = "TaskFXNettingChecker";
                break;
            case DBS.AcivityTitle.FXDealTMOMakerNettingCancellationTask:
                output = "TaskFXNettingCheckerApproveCancellation";
                break;
        }
        return output;
    };
    // set approval data template
    self.ApprovalData = function () {
        var output;

        switch (self.ActivityTitle()) {

            case DBS.AcivityTitle.TMOMakerTask:
                output = self.TMOMaker();
                break;
            case DBS.AcivityTitle.TMOCheckerTask:
                output = self.TMOChecker();
                break;
            case DBS.AcivityTitle.TMOCheckerCancellationTask:
                output = self.TMOChecker();
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerTask:
            case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
            case DBS.AcivityTitle.FXTMOCheckerRequestCancelTask:
                output = self.TrackingTMOChecker();
                break;
            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                output = self.TrackingTMOMaker();
                break;
            case DBS.AcivityTitle.FXDealTMOMakerNettingTask:
                output = self.TransactionMakerNetting();
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerNettingTask:
                output = self.TransactionCheckerNetting();
                break;
            case DBS.AcivityTitle.FXDealTMOMakerNettingCancellationTask:
                output = self.TransactionCheckerNetting();
                break;
        }
        return output;
    };
    self.onSelectionUtilize = function (index, item) {
        var statementLetterID;
        var statementLetterName;
        var itemdeal = $('#Position' + index).val();
        switch (viewModel.ActivityTitle()) {
            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                statementLetterID = self.TrackingTMOMaker().Transaction.StatementLetter.ID();
                statementLetterName = self.TrackingTMOMaker().Transaction.StatementLetter.Name();
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerTask:
            case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
                statementLetterID = self.TrackingTMOChecker().Transaction.StatementLetter.ID();
                statementLetterName = self.TrackingTMOChecker().Transaction.StatementLetter.Name();
                break;
            case DBS.AcivityTitle.FXDealTMOMakerNettingTask:
                statementLetterID = 2;
                statementLetterName = 'Underlying';
                break;
            default:
                ShowMesageDefault();
                break;
        }
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
            return x.ID() == item.ID;
        });

        if (data != null) { //uncheck condition
            //self.TempSelectedUnderlying.remove(data);
            self.MakerUnderlyings.remove(item.ID); //remove underlying for maker
            self.TempSelectedUnderlying.remove(data);
            ResetDataUnderlying(index, false);
            setTotalUtilize();
            switch (viewModel.ActivityTitle()) {
                case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
                case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
                case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                    SetTMODocuments();
                    break;
                case DBS.AcivityTitle.FXDealTMOMakerNettingTask:
                    SetTMONettingDocuments();
                    break;
            }
        } else {
            if (viewModel.ActivityTitle() == DBS.AcivityTitle.FXDealTMOMakerNettingTask) {
                self.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, false, parseFloat(itemdeal.replace(/,/g, '')), item.StatementLetter.ID));
                self.MakerUnderlyings.push(item.ID);
                ResetDataUnderlying(index, true);
                setTotalUtilize();
                switch (viewModel.ActivityTitle()) {
                    case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
                    case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
                    case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                        SetTMODocuments();
                        break;
                    case DBS.AcivityTitle.FXDealTMOMakerNettingTask:
                        SetTMONettingDocuments();
                        break;
                }
            } else {
                var IsSelectedUnderlying = (self.TempSelectedUnderlying() != null && self.TempSelectedUnderlying().length > 0);
                var dataUnderlyingStatementLetterID = IsSelectedUnderlying && self.TempSelectedUnderlying()[0].StatementLetter();
                if (IsSelectedUnderlying && dataUnderlyingStatementLetterID == item.StatementLetter.ID ||
                    (statementLetterID == item.StatementLetter.ID && !IsSelectedUnderlying)) {
                    self.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, false, parseFloat(itemdeal.replace(/,/g, '')), item.StatementLetter.ID));
                    self.MakerUnderlyings.push(item.ID);
                    ResetDataUnderlying(index, true);
                    setTotalUtilize();
                    switch (viewModel.ActivityTitle()) {
                        case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
                        case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
                        case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                            SetTMODocuments();
                            break;
                        case DBS.AcivityTitle.FXDealTMOMakerNettingTask:
                            SetTMONettingDocuments();
                            break;
                    }
                } else {
                    ResetDataUnderlying(index, false);
                    var msg = 'Please select ' + statementLetterName + '.';
                    ShowNotification('Form Validation Warning', msg, 'gritter-error', true);
                }
            }
        }

    }
    self.FormatNumber = function (num) {
        if (ko.isObservable(num)) {
            num = num();
        }
        if (num == null) {
            return 0;
        }
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    }
    self.ApprovalButton = function (name) {
        if (name == 'Continue') {
            return 'Submit';
        } else {
            return name;
        }
    }
    self.HomeApprovalButton = function (name) {

        if (name == 'Continue') {
            return 'Submit';
        } else {
            return name;
        }
        return name;
    }

    self.utilizationAmount = ko.observable(0.00);
    // delete document on ppumaker
    self.RemoveDocumentData = function (data) {
        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (result) {
                if (data.Purpose.ID == 2) {
                    // delete underlying document
                    DeleteUnderlyingAttachment(data);

                    switch (self.ActivityTitle()) {
                        case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
                        case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
                        case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                            var item = ko.utils.arrayFilter(self.TrackingTMOMaker().Transaction.Documents(), function (dta) { return dta.DocumentPath != data.DocumentPath; });
                            if (item != null) {
                                self.TrackingTMOMaker().Transaction.Documents(item);
                            }
                            break;
                    }
                } else {
                    if (data.IsNewDocument == false) {
                        switch (self.ActivityTitle()) {
                            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
                            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
                            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                                var item = ko.utils.arrayFilter(self.TrackingTMOMaker().Transaction.Documents(), function (dta) { return dta.ID != data.ID; });
                                if (item != null) {
                                    self.TrackingTMOMaker().Transaction.Documents(item);
                                }
                                break;
                        }
                    }
                }
                self.MakerAllDocumentsTemp.remove(data);
                self.MakerDocuments.remove(data);
            }
        });
    }

    self.GetData = function () {
        GetData();
    };
    self.GetParameters = function () { GetParameters(); };
    self.GetDataUnderlying = function () { GetDataUnderlying(); };
    self.GetDataBulkUnderlying = function () { GetDataBulkUnderlying(); }
    self.GetDataAttachFile = function () { GetDataAttachFile(); }
    self.GetDataUnderlyingAttach = function () { GetDataUnderlyingAttach(); }
    self.GetDataUnderlyingProforma = function () { GetDataUnderlyingProforma(); }
    self.GetDataBulkUnderlying = function () { GetDataBulkUnderlying(); }
    self.GetUnderlyingParameters = function (data) { GetUnderlyingParameters(data); }
    self.CalculateFX = function (amountUSD) { CalculateFX(amountUSD); }
    //#region Joint Account Payment
    self.OnChangeJointAcc = function (obj, event) {
        OnChangeJointAcc(obj);
    }
    self.CheckEmptyAccountNumber = function (obj, evnt) {
        OnChangeDebitAccount();
        GetTotalTransaction();
    };
    function AddOtherAccounts(accounts) {
        if (accounts != null) {
            var accountData = {
                AccountNumber: ko.observable("-"),
                CustomerName: ko.observable(),
                IsJointAccount: ko.observable(),
                Currency: null
            };
            accounts.push(accountData);
        }
        return accounts;
    }
    //#endregion
    //insert new
    self.NewDataUnderlying = function () {
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            //Remove required filed if show
            $('.form-group').removeClass('has-error').addClass('has-info');
            $('.help-block').remove();

            $("#modal-form-Underlying").modal('show');
            $('#backDrop').show();
            // flag as new Data
            self.IsNewDataUnderlying(true);
            // bind empty data
            self.ID_u(0);
            self.UnderlyingDocument_u(new UnderlyingDocumentModel2('', '', ''));
            self.OtherUnderlying_u('');
            self.DocumentType_u(new DocumentTypeModel2('', ''));
            self.StatementLetter_u(new StatementLetterModel('', ''));
            self.Currency_u(new CurrencyModel2('', '', ''));
            self.Amount_u('');
            self.AttachmentNo_u();
            self.IsDeclarationOfException_u(false);
            self.StartDate_u('');
            self.EndDate_u('');
            self.DateOfUnderlying_u('');
            self.ExpiredDate_u('');
            self.ReferenceNumber_u('');
            self.SupplierName_u('');
            self.InvoiceNumber_u('');
            self.IsProforma_u(false);
            self.IsSelectedProforma(false);
            self.IsUtilize_u(false);
            self.IsBulkUnderlying_u(false); //add 2015.03.09
            self.IsSelectedBulk(false); //add 2015.03.09
            self.tempAmountBulk(0);//add 2015.03.09
            self.IsJointAccount_u(GetModelTransaction().Transaction.IsJointAccount());
            if (self.IsJointAccount_u()) {
                var dataAccN = viewModel.Selected().Account();
                self.AccountNumber_u(dataAccN);
            }
            //Call Grid Underlying for Check Proforma
            GetDataUnderlyingProforma();
            GetDataBulkUnderlying();
            self.TempSelectedUnderlyingProforma([]);
            self.TempSelectedUnderlyingBulk([]);
            //setStatementA();
            setRefID();

            //underlyingCodeSave = $("#new-underlying-code option:selected").val();
            //viewModel.TransactionDealDetailModel().bookunderlyingcode(parseInt(underlyingCodeSave, 10));
            //console.log(underlyingCodeSave);
        } else {
            ShowNotification("Form Validation Warning", CONST_MSG.ValidationFields, 'gritter-warning', false);
        }

    };
    //Function to Display record to be updated
    self.GetUnderlyingSelectedRow = function (data) {
        data = ko.toJS(data);
        self.IsNewDataUnderlying(false);

        $("#modal-form-Underlying").modal('show');
        $('#backDrop').show();
        self.ID_u(data.ID);
        self.CustomerName_u(data.CustomerName);
        self.UnderlyingDocument_u(new UnderlyingDocumentModel2(data.UnderlyingDocument.ID, data.UnderlyingDocument.Name, data.UnderlyingDocument.Code));
        self.OtherUnderlying_u(data.OtherUnderlying);
        self.DocumentType_u(new DocumentTypeModel2(data.DocumentType.ID, data.DocumentType.Name));

        //self.Currency_u(data.Currency);
        self.Currency_u(new CurrencyModel2(data.Currency.ID, data.Currency.Code, data.Currency.Description));

        self.Amount_u(data.Amount);
        self.AttachmentNo_u(data.AttachmentNo);

        //self.StatementLetter_u(data.StatementLetter);
        self.StatementLetter_u(new StatementLetterModel(data.StatementLetter.ID, data.StatementLetter.Name));
        self.IsJointAccount_u(data.IsJointAccount);
        self.IsDeclarationOfException_u(data.IsDeclarationOfException);
        self.StartDate_u(self.LocalDate(data.StartDate, true, false));
        self.EndDate_u(self.LocalDate(data.EndDate, true, false));
        self.DateOfUnderlying_u(self.LocalDate(data.DateOfUnderlying, true, false));
        self.ExpiredDate_u(self.LocalDate(data.ExpiredDate, true, false));
        self.ReferenceNumber_u(data.ReferenceNumber);
        self.SupplierName_u(data.SupplierName);
        self.InvoiceNumber_u(data.InvoiceNumber);
        self.IsProforma_u(data.IsProforma);
        self.IsUtilize_u(data.IsUtilize);
        self.IsSelectedProforma(data.IsSelectedProforma);
        self.LastModifiedBy_u(data.LastModifiedBy);
        self.LastModifiedDate_u(data.LastModifiedDate);
        self.IsBulkUnderlying_u(data.IsBulkUnderlying); // add 2015.03.09
        self.IsSelectedBulk(data.IsSelectedBulk); // add 2015.03.09
        self.tempAmountBulk(data.Amount);// add 2015.03.09
        //Call Grid Underlying for Check Proforma
        //GetSelectedProformaID(GetDataUnderlyingProforma);

        //Call Grid Underlying for Check Bullk // add 2015.03.09
        //GetSelectedBulkID(GetDataBulkUnderlying);


    };
    self.CloseUnderlying = function () {
        $("#modal-form-Underlying").modal('hide');
        //$("#modal-form-upload").modal('hide');
        $('#backDrop').hide();
    }
    self.Close = function () {
        $("#modal-form-Attach").modal('hide');
        $('#backDrop').hide();
    }
    self.IsAllChecked = function () {
        var isAllChecked = true;
        switch (self.ActivityTitle()) {
            case DBS.AcivityTitle.FXDealTMOCheckerTask:
            case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
                if (self.TrackingTMOChecker().Verify() != undefined) {
                    var skipTerms = []; // set verify name to exclude from validation checkbox
                    for (var i = 0; i < self.TrackingTMOChecker().Verify().length; i++) {
                        if (skipTerms.indexOf(self.TrackingTMOChecker().Verify()[i].Name()) == -1) {
                            if (!self.TrackingTMOChecker().Verify()[i].IsVerified()) {
                                isAllChecked = false;
                                break;
                            }
                        }
                    }
                }
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerNettingTask:
                if (self.TransactionCheckerNetting().Verify != undefined) {
                    for (var i = 0; i < self.TransactionCheckerNetting().Verify.length; i++) {
                        if (!self.TransactionCheckerNetting().Verify[i].IsVerified) {
                            isAllChecked = false;
                            break;
                        }

                    }

                }
                break;
            case DBS.AcivityTitle.TMOPPUCheckerTask:
                if (self.TransactionCheckerTMO().Verify != undefined) {
                    var skipTerms = ["Document Completeness"];
                    for (var i = 0; i < self.TransactionCheckerTMO().Verify.length; i++) {
                        if (skipTerms.indexOf(self.TransactionCheckerTMO().Verify[i].Name) == -1) {
                            if (!self.TransactionCheckerTMO().Verify[i].IsVerified) {
                                isAllChecked = false;
                                break;
                            }
                        }
                    }
                }

                break;
            case DBS.AcivityTitle.TMOCheckerTask:
                if (self.TMOChecker().Verify != undefined) {
                    for (var i = 0; i < self.TMOChecker().Verify.length; i++) {
                        if (!self.TMOChecker().Verify[i].IsVerified) {
                            isAllChecked = false;
                            break;
                        }

                    }
                }
                break;
        }
        return isAllChecked;
    };
    self.onSelectionOther = function (underlyingID) {
        /*if (underlyingID() > 0) {
            //GetUnderlyingDocName(underlyingID());
            var underlyingCode = $("#book-underlying-code option:selected").val();
            viewModel.TransactionDealDetailModel().bookunderlyingcode(parseInt(underlyingCode, 10));
            $("#new-underlying-code").val(underlyingCode);
        }*/
    };

    // Uploading document
    self.UploadDocumentNetting = function () {

        if (self.MakerUnderlyings().length > 0) {
            self.IsUploading(false);
            $("#modal-form-Attach").modal('show');
            $('.remove').click();
            self.ID_a(0);
            self.ddlDocumentPurpose_a(self.DocumentPurposes());
            self.DocumentPurpose_a(null);
            self.Selected().DocumentPurpose_a(null);
            self.DocumentType_a(null);
            self.Selected().DocumentType_a(null);
            self.DocumentPath_a(null);
            $('#backDrop').show();
            self.TempSelectedAttachUnderlying([]);
            GetCustomerUnderlying();
        } else {
            ShowNotification("", "Please select utilize underlying form the table.", 'gritter-warning', true);
        }
    }

    self.UploadDocumentUnderlying = function () {
        self.IsUploading(false);
        // show the dialog task form
        $("#modal-form-Attach").modal('show');
        $('#backDrop').show();
        self.TempSelectedAttachUnderlying([]);
        self.ID_a(0);
        self.DocumentPurpose_a(new DocumentPurposeModel2('', '', ''));
        self.DocumentType_a(new DocumentTypeModel2('', ''));
        self.DocumentPath_a('');
        self.TempSelectedAttachUnderlying([]);
        GetDataUnderlyingAttach();
        $('.remove').click();
    }
    self.UploadDocument = function () {
        self.IsUploading(false);
        $("#modal-form-Attach1").modal('show');
        $('.remove').click();
        var item = ko.utils.arrayFilter(self.DocumentPurposes(), function (dta) { return dta.ID() != 2 });
        self.ddlDocumentPurpose_a(item);
        self.ID_a(0);
        self.DocumentPurpose_a(new DocumentPurposeModel2('', '', ''));
        self.DocumentType_a(new DocumentTypeModel2('', ''));
        self.DocumentPath_a('');

        $('#backDrop').show();
    }
    self.OnChangeStatement = function (obj1, event) {
        if (obj1.StatementLetter.ID() == 1 || obj1.StatementLetter.ID() == 2) {
            self.IsAnnualStatement(true);
            self.IsShowUnderlyingDocuments(true);
        } else {
            self.IsAnnualStatement(false);
            self.IsShowUnderlyingDocuments(false);
        }
    }

    self.OnChangeUndrlyingDoc = function (obj1, event) {
        if (obj1.UnderlyingCodeID() == 37) {
            self.isNewUnderlying(true);
        } else {
            self.isNewUnderlying(false);
        }
    }
    self.onChangeUtilizeAmountDeal = function (item) {
        var index = item.RowID - 1;
        var itemdeal = $('#Position' + index).val();
        if (parseFloat($('#Position' + index).val().replace(/,/g, '')) > item.AvailableAmount) {
            if (itemdeal == "NaN" || itemdeal == "") {
                $('#Position' + index).val(formatNumber(parseFloat(item.AvailableAmountDeal)));
            } else {
                $('#Position' + index).val(formatNumber(parseFloat(item.UtilizeAmountDeal.replace(/,/g, ''))));
            }
            ShowNotification("Form Validation Warning", "Available Amount Underlying must be greater and lesh than Utilize Amount Deal", 'gritter-warning', true);
            return false;
        } else {
            if (itemdeal == "NaN" || itemdeal == "") {
                $('#Position' + index).val(formatNumber(parseFloat(item.AvailableAmountDeal)));
            } else {
                $('#Position' + index).val(formatNumber(parseFloat(itemdeal.replace(/,/g, ''))));
            }
            if (item.IsEnable == true) {
                //item.IsEnable = false;
                if (itemdeal == "NaN" || itemdeal == "") {
                    item.UtilizeAmountDeal = formatNumber(parseFloat(item.AvailableAmountDeal));
                } else {
                    item.UtilizeAmountDeal = formatNumber(parseFloat(itemdeal.replace(/,/g, '')));
                }
                ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (itemtemp) {
                    if (itemtemp.ID() == item.ID) {
                        self.TempSelectedUnderlying.remove(itemtemp);
                        itemtemp.USDAmount(parseFloat(item.UtilizeAmountDeal.replace(/,/g, '')));
                        self.TempSelectedUnderlying.push(itemtemp);
                        setTotalUtilize();
                    }
                });

                //var datad = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                //    return x.ID() == item.ID;
                //});
                //if (datad != null) {
                //    self.TempSelectedUnderlying.remove(datad);
                //    setTotalUtilize();
                //}
                //self.MakerUnderlyings.remove(item.ID);
                //SetTMODocuments();
                //ResetDataUnderlying(index, false);
            }
        }
        //viewModel.CustomerUnderlyings()[item.RowID - 1].UtilizeAmountDeal = formatNumber(parseFloat(itemdeal));
    }
    self.OnCurrencyChange = function (CurrencyID) {
        if (CurrencyID != null) {
            var item = ko.utils.arrayFirst(self.ddlCurrency_u(), function (x) {
                return CurrencyID == x.ID();
            });
            if (item != null) {
                self.Currency_u(new CurrencyModel2(item.ID(), item.Code(), item.Description()));
            }
            if (!self.IsNewDataUnderlying()) {
                GetSelectedBulkID(GetDataBulkUnderlying);
            } else {
                self.TempSelectedUnderlyingBulk([]);
                GetDataBulkUnderlying();
            }
        }
    }

    self.save_u = function () {
        // validation
        self.IsEditTableUnderlying(false);
        var form = $("#frmUnderlying");
        form.validate();

        if (form.valid()) {
            viewModel.Amount_u(viewModel.Amount_u().toString().replace(/,/g, ''));
            CustomerUnderlying.OtherUnderlying = self.CodeUnderlying() == '999' ? self.OtherUnderlying_u : '';
            SetMappingProformas();
            SetMappingBulks(); // add 2015.03.09
            CustomerUnderlying.AvailableAmountUSD = self.AmountUSD_u();
            //Ajax call to insert the CustomerUnderlyings
            $.ajax({
                type: "POST",
                url: api.server + api.url.customerunderlying + "/Save",
                data: ko.toJSON(CustomerUnderlying), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        $("#modal-form-Underlying").modal('hide');
                        $('#backDrop').hide();
                        self.IsEditTableUnderlying(true);
                        // refresh data
                        GetDataUnderlying();
                        GetTotalTransaction();
                        //$("#new-underlying-code").val(underlyingCodeSave);
                    } else {

                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                        self.IsEditTableUnderlying(true);

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    if (jqXHR.responseJSON.Message.toLowerCase().startsWith("double underlying")) {
                        ShowNotification("", jqXHR.responseJSON.Message, 'gritter-warning', true);
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    } self.IsEditTableUnderlying(true);
                }
            });
        } else {
            self.IsEditTableUnderlying(true);
        }
    };
    self.save_a = function () {
        self.IsUploading(true);
        var doc = {
            ID: 0,
            FileName: Documents.DocumentPath() != null ? Documents.DocumentPath().name : null,
            DocumentPath: self.DocumentPath_a(),
            Type: ko.utils.arrayFirst(self.ddlDocumentType_a(), function (item) {
                return item.ID == self.DocumentType_a().ID();
            }),
            Purpose: ko.utils.arrayFirst(ko.toJS(self.ddlDocumentPurpose_a()), function (item) {
                return item.ID == self.DocumentPurpose_a().ID();
            }),
            IsNewDocument: true,
            LastModifiedDate: new Date(),
            LastModifiedBy: null
        };
        if (doc.Type == null || doc.Purpose == null || doc.DocumentPath == null || doc.FileName == null) {
            ShowNotification("Information", CONST_MSG.NotCompleteUpload, 'gritter-warning', true);
            self.IsUploading(false);
        } else {
            var FxWithUnderlying = (self.TempSelectedAttachUnderlying().length > 0);
            var FxWithNoUnderlying = (doc.Purpose.ID != 2);
            if (FxWithUnderlying || FxWithNoUnderlying) {
                var dataCIF = null;
                var dataName = null;
                switch (self.ActivityTitle()) {
                    case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
                    case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
                    case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                        dataCIF = viewModel.TrackingTMOMaker().Transaction.Customer.CIF();
                        dataName = viewModel.TrackingTMOMaker().Transaction.Customer.Name();
                        break;
                }
                var data = {
                    CIF: dataCIF,
                    Name: dataName,
                    Type: ko.utils.arrayFirst(self.ddlDocumentType_a(), function (item) {
                        return item.ID == self.DocumentType_a().ID();
                    }),
                    Purpose: ko.utils.arrayFirst(ko.toJS(self.ddlDocumentPurpose_a()), function (item) {
                        return item.ID == self.DocumentPurpose_a().ID();
                    })
                };
                if (FxWithUnderlying && data.Purpose.ID == 2) {
                    UploadFileUnderlying(data, doc, SaveDataFile);
                }
                if (FxWithNoUnderlying) {
                    self.MakerDocuments.push(doc);
                }
                $("#modal-form-Attach").modal('hide');
                $("#modal-form-Attach1").modal('hide');
                $('#backDrop').hide();
            } else {
                ShowNotification("Information", CONST_MSG.NotCompleteUpload, 'gritter-warning', true);
                self.IsUploading(false);
            }
        }
    }
    self.update_u = function () {
        var form = $("#frmUnderlying");
        form.validate();

        if (form.valid()) {
            // hide current popup window
            viewModel.Amount_u(viewModel.Amount_u().toString().replace(/,/g, ''));
            $("#modal-form-Underlying").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form-Underlying").modal('show');
                    $('#backDrop').show();
                } else {
                    CustomerUnderlying.OtherUnderlying = self.CodeUnderlying() == '999' ? self.OtherUnderlying_u : '';
                    SetMappingProformas();
                    SetMappingBulks(); // add 2015.03.09
                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.customerunderlying + "/" + CustomerUnderlying.ID(),
                        data: ko.toJSON(CustomerUnderlying),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {

                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                $("#modal-form-Underlying").modal('hide');
                                $('#backDrop').hide();
                                // refresh data
                                GetDataUnderlying();
                                GetTotalTransaction();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            if (jqXHR.responseJSON.Message.toLowerCase().startsWith("double underlying")) {
                                ShowNotification("", jqXHR.responseJSON.Message, 'gritter-warning', true);
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                            }
                        }
                    });
                }
            });
        }
    };
    self.delete_u = function () {
        // hide current popup window
        $("#modal-form-Underlying").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form-Underlying").modal('show');
                $('#backDrop').show();
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customerunderlying + "/remove/" + CustomerUnderlying.ID(),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {

                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                            $("#modal-form-Underlying").modal('hide');
                            $('#backDrop').hide();
                            GetDataUnderlying();
                            GetTotalTransaction();
                            var data = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                                return x.ID() == CustomerUnderlying.ID();
                            });
                            if (data != null) { //uncheck condition
                                //self.TempSelectedUnderlying.remove(data);
                                self.MakerUnderlyings.remove(CustomerUnderlying.ID()); //remove underlying for maker
                                self.TempSelectedUnderlying.remove(data);
                                //ResetDataUnderlying(index, false);
                                setTotalUtilize();
                                switch (viewModel.ActivityTitle()) {
                                    case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
                                    case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
                                    case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                                        SetTMODocuments();
                                        break;
                                }
                            }
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };
    self.cancel_u = function () {
        $('#backDrop').hide();
    }
    self.cancel_a = function () {
        $('#backDrop').hide();
    }

    // add function underlying | chandra a
    // Function to Read parameter Underlying
    function GetUnderlyingParameters(data) {

        var underlying = data['UnderltyingDoc'];
        for (var i = 0; i < underlying.length; i++) {
            self.ddlUnderlyingDocument_u.push(new UnderlyingDocumentModel2(underlying[i].ID, underlying[i].Name, underlying[i].Code));
        }

        self.ddlDocumentType_u(data['DocType']);
        self.ddlDocumentType_a(data['DocType']);
        ko.utils.arrayForEach(data['Currency'], function (item) {
            self.ddlCurrency_u.push(new CurrencyModel2(item.ID, item.Code, item.Description));
        });
        self.ddlDocumentPurpose_a(data['PurposeDoc']);
        self.ddlStatementLetter(data['StatementLetter']);
        ko.utils.arrayForEach(data['StatementLetter'], function (item) {
            if (item.ID == CONST_STATEMENT.StatementA_ID || item.ID == CONST_STATEMENT.StatementB_ID) {
                self.ddlStatementLetter_u.push(item);
            }
        });
    }
    //Function to Read All Customers Underlying
    function GetDataUnderlying() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        var transactionDealID;
        switch (self.ActivityTitle()) {
            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                transactionDealID = viewModel.TrackingTMOMaker().Transaction.ID();
                break;
        }
        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Underlying",
            //url: api.server + api.url.customerunderlying + "/UnionTransactionDeal/" + transactionDealID,
            params: {
                page: self.UnderlyingGridProperties().Page(),
                size: self.UnderlyingGridProperties().Size(),
                sort_column: self.UnderlyingGridProperties().SortColumn(),
                sort_order: self.UnderlyingGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        self.UnderlyingFilterIsAvailable(true);
        self.UnderlyingFilterIsExpiredDate(true);
        var filters = GetUnderlyingFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        }
    }
    //Function to Read All Customers Underlying File
    function GetDataAttachFile() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlyingfile,
            params: {
                page: self.AttachGridProperties().Page(),
                size: self.AttachGridProperties().Size(),
                sort_column: self.AttachGridProperties().SortColumn(),
                sort_order: self.AttachGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        }
    }
    //Function to Read All Customers Underlying for Attach File
    function GetDataUnderlyingAttach() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Attach",
            params: {
                page: self.UnderlyingAttachGridProperties().Page(),
                size: self.UnderlyingAttachGridProperties().Size(),
                sort_column: self.UnderlyingAttachGridProperties().SortColumn(),
                sort_order: self.UnderlyingAttachGridProperties().SortOrder()
            },
            token: accessToken
        };
        self.UnderlyingAttachFilterAccountNumber(self.UnderlyingFilterAccountNumber());
        // get filtered columns
        var filters = GetUnderlyingAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingDataAttachFile, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingDataAttachFile, OnError, OnAlways);
        }
    }
    //Function to Read All Customers Underlying Proforma
    function GetDataUnderlyingProforma() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Proforma",
            params: {
                page: self.UnderlyingProformaGridProperties().Page(),
                size: self.UnderlyingProformaGridProperties().Size(),
                sort_column: self.UnderlyingProformaGridProperties().SortColumn(),
                sort_order: self.UnderlyingProformaGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetUnderlyingProformaFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingProforma, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingProforma, OnError, OnAlways);
        }
    }
    //Function to Read All Customers Bulk Underlying // add 2015.03.09
    function GetDataBulkUnderlying() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Bulk",
            params: {
                page: self.UnderlyingBulkGridProperties().Page(),
                size: self.UnderlyingBulkGridProperties().Size(),
                sort_column: self.UnderlyingBulkGridProperties().SortColumn(),
                sort_order: self.UnderlyingBulkGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns

        var filters = GetBulkUnderlyingFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingBulk, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingBulk, OnError, OnAlways);
        }
    }
    // On success GetData callback
    /* function OnSuccessGetDataUnderlying(data, textStatus, jqXHR) {
         if (jqXHR.status = 200) {
             //self.CustomerUnderlyings(data.Rows);
             //self.TempSelectedUnderlying([]);
             self.MakerAllDocumentsTemp([]);
 
             var dataunderlying = SetSelectedUnderlying(data.Rows);
             self.CustomerUnderlyings([]);
             self.CustomerUnderlyings(dataunderlying);
             self.UnderlyingGridProperties().Page(data['Page']);
             self.UnderlyingGridProperties().Size(data['Size']);
             self.UnderlyingGridProperties().Total(data['Total']);
             self.UnderlyingGridProperties().TotalPages(Math.ceil(self.UnderlyingGridProperties().Total() / self.UnderlyingGridProperties().Size()));
         } else {
             ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
         }
     } */

    // On success GetData callback
    function OnSuccessGetUnderlyingDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {

            for (var i = 0 ; i < data.Rows.length; i++) {
                var selected = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (item) {
                    return item.ID == data.Rows[i].ID;
                });
                if (selected != null) {
                    data.Rows[i].IsSelectedAttach = true;
                }
                else {
                    data.Rows[i].IsSelectedAttach = false;
                }
            }
            self.CustomerAttachUnderlyings(data.Rows);

            self.UnderlyingAttachGridProperties().Page(data['Page']);
            self.UnderlyingAttachGridProperties().Size(data['Size']);
            self.UnderlyingAttachGridProperties().Total(data['Total']);
            self.UnderlyingAttachGridProperties().TotalPages(Math.ceil(self.UnderlyingAttachGridProperties().Total() / self.UnderlyingAttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerUnderlyingFiles(data.Rows);
            self.AttachGridProperties().Page(data['Page']);
            self.AttachGridProperties().Size(data['Size']);
            self.AttachGridProperties().Total(data['Total']);
            self.AttachGridProperties().TotalPages(Math.ceil(self.AttachGridProperties().Total() / self.AttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetUnderlyingProforma(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            SetSelectedProforma(data.Rows);

            self.CustomerUnderlyingProformas(data.Rows);

            self.UnderlyingProformaGridProperties().Page(data['Page']);
            self.UnderlyingProformaGridProperties().Size(data['Size']);
            self.UnderlyingProformaGridProperties().Total(data['Total']);
            self.UnderlyingProformaGridProperties().TotalPages(Math.ceil(self.UnderlyingProformaGridProperties().Total() / self.UnderlyingProformaGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback // Add 2015.03.09
    function OnSuccessGetUnderlyingBulk(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            SetSelectedBulk(data.Rows);

            self.CustomerBulkUnderlyings(data.Rows);

            self.UnderlyingBulkGridProperties().Page(data['Page']);
            self.UnderlyingBulkGridProperties().Size(data['Size']);
            self.UnderlyingBulkGridProperties().Total(data['Total']);
            self.UnderlyingBulkGridProperties().TotalPages(Math.ceil(self.UnderlyingBulkGridProperties().Total() / self.UnderlyingBulkGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }
    //The Object which stored data entered in the observables
    var CustomerUnderlying = {
        ID: self.ID_u,
        CIF: self.CIF_u,
        CustomerName: self.CustomerName_u,
        UnderlyingDocument: self.UnderlyingDocument_u,
        DocumentType: self.DocumentType_u,
        Currency: self.Currency_u,
        Amount: self.Amount_u,
        Rate: self.Rate_u,
        AmountUSD: self.AmountUSD_u,
        AvailableAmountUSD: self.AvailableAmount_u,
        AttachmentNo: self.AttachmentNo_u,
        StatementLetter: self.StatementLetter_u,
        IsDeclarationOfException: self.IsDeclarationOfException_u,
        StartDate: self.StartDate_u,
        EndDate: self.EndDate_u,
        DateOfUnderlying: self.DateOfUnderlying_u,
        ExpiredDate: self.ExpiredDate_u,
        ReferenceNumber: self.ReferenceNumber_u,
        SupplierName: self.SupplierName_u,
        InvoiceNumber: self.InvoiceNumber_u,
        IsSelectedProforma: self.IsSelectedProforma,
        IsProforma: self.IsProforma_u,
        IsSelectedBulk: self.IsSelectedBulk,
        IsBulkUnderlying: self.IsBulkUnderlying_u,
        BulkUnderlyings: self.BulkUnderlyings,
        IsUtilize: self.IsUtilize_u,
        IsEnable: self.IsEnable_u,
        IsTMO: self.IsTMO_u,
        Proformas: self.Proformas,
        OtherUnderlying: self.OtherUnderlying_u,
        LastModifiedBy: self.LastModifiedBy_u,
        LastModifiedDate: self.LastModifiedDate_u
    };

    var Documents = {
        Documents: self.Documents,
        DocumentPath: self.DocumentPath_a,
        Type: self.DocumentType_a,
        Purpose: self.DocumentPurpose_a
    }

    var CustomerUnderlyingFile = {
        ID: self.ID_a,
        FileName: DocumentModels().FileName,
        DocumentPath: DocumentModels().DocumentPath,
        DocumentPurpose: self.DocumentPurpose_a,
        DocumentType: self.DocumentType_a,
        DocumentRefNumber: self.DocumentRefNumber_a,
        LastModifiedBy: self.LastModifiedBy_a,
        LastModifiedDate: self.LastModifiedDate_a,
        CustomerUnderlyingMappings: self.CustomerUnderlyingMappings_a
    };
    //#region Function Switch Case TMO and FX Model
    function GetIsFlowValas(FlowValas) {
        return (FlowValas == null ? false : FlowValas.IsFlowValas());
    }
    function GetProductTypeID(FlowValas) {
        return (FlowValas == null ? null : FlowValas.ID);
    }
    function GetStatementLetterID() {
        switch (viewModel.ActivityTitle()) {
            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                return viewModel.TrackingTMOMaker().Transaction.StatementLetter.ID();
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerTask:
            case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
                return viewModel.TrackingTMOChecker().Transaction.StatementLetter.ID();
                break;
            default:
                ShowMesageDefault();
                break;
        }
    }
    function GetFlowValas() {
        switch (viewModel.ActivityTitle()) {
            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                return GetIsFlowValas(viewModel.TrackingTMOMaker().ProductType);
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerTask:
            case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
                isFlowValas = true;
                break;
            default:
                ShowMesageDefault();
                break;
        }
    }
    function GetAmountUSD() {
        switch (viewModel.ActivityTitle()) {
            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                return viewModel.TrackingTMOMaker().Transaction.AmountUSD();
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerTask:
            case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
                return viewModel.TrackingTMOChecker().Transaction.AmountUSD();
                break;
            default:
                ShowMesageDefault();
                break;
        }
    }

    //#endregion
    function ResetDataAttachment(index, isSelect) {
        self.CustomerAttachUnderlyings()[index].IsSelectedAttach = isSelect;
        var dataRows = self.CustomerAttachUnderlyings().slice(0);
        self.CustomerAttachUnderlyings([]);
        self.CustomerAttachUnderlyings(dataRows);
    }
    function ResetDataProforma(index, isSelect) {
        self.CustomerUnderlyingProformas()[index].IsSelectedProforma = isSelect;
        var dataRows = self.CustomerUnderlyingProformas().slice(0);
        self.CustomerUnderlyingProformas([]);
        self.CustomerUnderlyingProformas(dataRows);
    }
    function ResetBulkData(index, isSelect) { //add 2015.03.09
        self.CustomerBulkUnderlyings()[index].IsSelectedBulk = isSelect;
        var dataRows = self.CustomerBulkUnderlyings().slice(0);
        self.CustomerBulkUnderlyings([]);
        self.CustomerBulkUnderlyings(dataRows);
    }
    function ResetDataUnderlying(index, isSelect) {
        self.CustomerUnderlyings()[index].IsEnable = isSelect;
        var itemdeal = $('#Position' + index).val();
        if (itemdeal != undefined) {
            self.CustomerUnderlyings()[index].UtilizeAmountDeal = itemdeal;
        }
        var dataRows = self.CustomerUnderlyings().slice(0);
        self.CustomerUnderlyings([]);
        if (dataRows[0].UtilizeAmountDeal == "NaN") {
            dataRows[0].UtilizeAmountDeal = formatNumber(dataRows[0].AvailableAmountDeal);
        }
        self.CustomerUnderlyings(dataRows);
    }
    function GetUnderlyingDocName(underlyingID) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.underlyingdoc + "/" + underlyingID,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if (data != null) {

                        self.isNewUnderlying(false); //return default disabled

                        // available type to input for new underlying name if code is 999
                        if (data.Code == '999') {
                            self.TransactionDealDetailModel().bookunderlyingdesc('');
                            self.isNewUnderlying(true);
                        }
                        else {
                            self.isNewUnderlying(false);
                            self.TransactionDealDetailModel().OtherUnderlying('');
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
    function SetSelectedProforma(data) {
        // if(self.TempSelectedUnderlyingProforma().length>0) {
        for (var i = 0 ; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlyingProforma(), function (item) {
                return item.ID == data[i].ID;
            });
            if (selected != null) {
                data[i].IsSelectedProforma = true;
            }
            else {
                data[i].IsSelectedProforma = false;
            }
        }
    }
    function SetSelectedBulk(data) { //add 2015.03.09

        for (var i = 0 ; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlyingBulk(), function (item) {
                return item.ID == data[i].ID;
            });
            if (selected != null) {
                data[i].IsSelectedBulk = true;
            }
            else {
                data[i].IsSelectedBulk = false;
            }
        }
    }
    function SetDefaultValueStatementA() {
        var today = Date.now();
        var documentType = ko.utils.arrayFirst(self.ddlDocumentType_u(), function (item) { return item.Name == '-' });
        var underlyingDocument = ko.utils.arrayFirst(self.ddlUnderlyingDocument_u(), function (item) { return item.Code() == 998 });

        if (documentType != null) {
            self.DocumentType_u(new DocumentTypeModel2(documentType.ID, documentType.Name));
        }

        if (underlyingDocument != null) {
            self.UnderlyingDocument_u(new UnderlyingDocumentModel2(underlyingDocument.ID(), underlyingDocument.Name(), underlyingDocument.Code()));
        }

        self.DateOfUnderlying_u(self.LocalDate(today, true, false));
        self.SupplierName_u('-');
        self.InvoiceNumber_u('-');
    }
    function SetSelectedUnderlying(data) {
        for (var i = 0; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (item) {
                return item.ID() == data[i].ID;
            });
            if (selected != null) {
                data[i].IsEnable = true;
                data[i].UtilizeAmountDeal = formatNumber(selected.USDAmount());
            }
            else {
                data[i].IsEnable = false;
            }
        }
        return data;
    }
    function TotalUtilize() {
        var total = 0;
        ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (item) {
            if (item.Enable() == false)
                total += parseFloat(ko.utils.unwrapObservable(item.USDAmount()));
        });
        var fixTotal = parseFloat(total).toFixed(2);
        return fixTotal;
    }
    function GetRateIDRAmount(CurrencyID) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
            params: {
            },
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    self.Rate_u(data.RupiahRate);
                    //if(CurrencyID!=1){ // if not USD

                    var fromFormat = document.getElementById("Amount_u").value;
                    fromFormat = fromFormat.toString().replace(/,/g, '');
                    res = parseFloat(fromFormat) * data.RupiahRate / idrrate;
                    res = Math.round(res * 100) / 100;
                    res = isNaN(res) ? 0 : res; //avoid NaN
                    self.AmountUSD_u(parseFloat(res).toFixed(2));
                    self.Amount_u(formatNumber(fromFormat));
                }
            }
        });
    }
    function GetUSDAmount(currencyID, amount, IdUnderlying) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetAmountUSD/" + currencyID + "/" + amount,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlying.push(new SelectUtillizeModel(IdUnderlying, false, data));
                    var total = TotalUtilize();
                    viewModel.utilizationAmount(total);
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
    function SetTMODocuments() {
        ko.utils.arrayForEach(self.MakerAllDocumentsTemp(), function (item) {
            if (item.Purpose.ID == 2) {
                self.MakerDocuments.remove(item);
                var dataDoc = jQuery.grep(self.TrackingTMOMaker().Transaction.Documents(), function (value) {
                    return value.ID != item.ID;
                });
                self.TrackingTMOMaker().Transaction.Documents(dataDoc);
            }
        });
        ko.utils.arrayForEach(self.MakerAllDocumentsTemp(), function (item) {
            ko.utils.arrayForEach(self.MakerUnderlyings(), function (item2) {
                if (item2 == item.UnderlyingID && item.Purpose.ID == 2) {
                    self.MakerDocuments.push(item);
                    self.TrackingTMOMaker().Transaction.Documents.push(item);
                }
            });
        });
    }

    function SetTMONettingDocuments() {
        ko.utils.arrayForEach(self.CustomerUnderlyingFiles(), function (item) {
            if (item.DocumentPurpose.ID == 2) {
                self.MakerDocuments.remove(item);
                var dataDoc = jQuery.grep(self.TransactionMakerNetting().Transaction.Documents, function (value) {
                    return value.ID != item.ID;
                });
                self.TransactionMakerNetting().Transaction.Documents = dataDoc;
            }
        });
        ko.utils.arrayForEach(self.CustomerUnderlyingFiles(), function (item) {
            ko.utils.arrayForEach(self.MakerUnderlyings(), function (item2) {
                if (item2 == item.UnderlyingID && item.DocumentPurpose.ID == 2) {
                    self.MakerDocuments.push(item);
                    self.TransactionMakerNetting().Transaction.Documents.push(item);
                }
            });
        });
    }

    function SetFxStatus() {
        self.t_IsFxTransaction(GetIsFxTransaction());
        self.t_IsFxTransactionToIDR(GetIsFxTransactiontoIDR());
        var statementLetterID = GetModelTransaction().Transaction.StatementLetter.ID();
        self.IsAnnualStatement(statementLetterID !== null && statementLetterID == 4 ? true : false);
    }
    function GetIsFxTransaction() {
        var output;
        switch (self.ActivityTitle()) {
            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                output = self.TrackingTMOMaker().Transaction.BuyCurrency.Code() != 'IDR' && self.TrackingTMOMaker().Transaction.SellCurrency.Code() == 'IDR';
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerTask:
            case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
            case DBS.AcivityTitle.FXTMOCheckerRequestCancelTask:
                output = self.TrackingTMOChecker().Transaction.BuyCurrency.Code() != 'IDR' && self.TrackingTMOChecker().Transaction.SellCurrency.Code() == 'IDR';
                break;
        }
        return output;
    }
    function GetIsFxTransactiontoIDR() {
        var output;
        switch (self.ActivityTitle()) {
            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                output = self.TrackingTMOMaker().Transaction.BuyCurrency.Code() == 'IDR' && self.TrackingTMOMaker().Transaction.SellCurrency.Code() != 'IDR';
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerTask:
            case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
            case DBS.AcivityTitle.FXTMOCheckerRequestCancelTask:
                output = self.TrackingTMOChecker().Transaction.BuyCurrency.Code() == 'IDR' && self.TrackingTMOChecker().Transaction.SellCurrency.Code() != 'IDR';
                break;
        }
        return output;
    }

    function CalculateFX(amountUSD) {
        var statementLetterID = GetStatementLetterID();
        var isFlowValas = GetFlowValas();
        var t_TotalAmountsUSD = TotalDealModel.Total.IDRFCYDeal;
        var t_RemainingBalance = TotalDealModel.Total.RemainingBalance;
        viewModel.AmountModel().TotalUtilization(TotalDealModel.Total.UtilizationDeal);
        viewModel.FCYIDRTresHold(TotalDealModel.FCYIDRTrashHold);
        var t_Treshold = TotalDealModel.TreshHold;
        var t_FCYTreshold = TotalDealModel.FCYIDRTrashHold;
        var t_FcyAmount = parseFloat(amountUSD);
        var t_IsFxTransaction = viewModel.t_IsFxTransaction();
        var t_IsFxTransactionToIDR = viewModel.t_IsFxTransactionToIDR();
        if (t_IsFxTransactionToIDR) {
            if (t_FcyAmount >= t_FCYTreshold && isFlowValas) {
                viewModel.Selected().StatementLetter(CONST_STATEMENT.StatementB_ID);
                t_TotalAmountsUSD = parseFloat(t_TotalAmountsUSD);
                t_TotalAmountsUSD = t_TotalAmountsUSD.toFixed(2);

            } else {
                viewModel.Selected().StatementLetter(CONST_STATEMENT.Dash);
            }
        }
        if (t_IsFxTransaction) {
            t_TotalAmountsUSD = parseFloat(t_TotalAmountsUSD);
            t_TotalAmountsUSD = t_TotalAmountsUSD.toFixed(2);

        }
        if (viewModel.statementLetterID != undefined) {
            if (viewModel.statementLetterID == CONST_STATEMENT.StatementA_ID) {
                if (t_TotalAmountsUSD <= t_Treshold) {
                    t_RemainingBalance = 0.00;
                }
            } else {
                if (amountUSD != null) {
                    t_RemainingBalance = parseFloat(t_RemainingBalance);
                } else {
                    t_RemainingBalance = t_RemainingBalance
                }
            }
            if (t_IsFxTransactionToIDR) {
                if (t_FcyAmount >= t_FCYTreshold && isFlowValas) {
                    if (t_RemainingBalance == 0.00) {
                        t_RemainingBalance = t_RemainingBalance;
                    }
                }
            }
            t_RemainingBalance = parseFloat(t_RemainingBalance.toFixed(2));
        }
        viewModel.AmountModel().TotalAmountsUSD(t_TotalAmountsUSD);
        viewModel.AmountModel().RemainingBalance(t_RemainingBalance);
    }
    function onSuccessRemainingBlnc() {
        var amountUSD = GetAmountUSD();
        CalculateFX(amountUSD);
    }
    function SetMappingAttachment(customerName) {
        CustomerUnderlyingFile.CustomerUnderlyingMappings([]);
        for (var i = 0 ; i < self.TempSelectedAttachUnderlying().length; i++) {
            var sValue = {
                ID: 0,
                UnderlyingID: self.TempSelectedAttachUnderlying()[i].ID,
                UnderlyingFileID: 0,
                AttachmentNo: customerName + '-(A)'
            }
            CustomerUnderlyingFile.CustomerUnderlyingMappings.push(sValue);
        }

    }
    function SetMappingProformas() {
        CustomerUnderlying.Proformas([]);
        for (var i = 0 ; i < self.TempSelectedUnderlyingProforma().length; i++) {
            CustomerUnderlying.Proformas.push(new SelectModel(cifData, self.TempSelectedUnderlyingProforma()[i].ID));
        }
    }
    function SetMappingBulks() { //add 2015.03.09
        console.log(CustomerUnderlying.BulkUnderlyings());
        CustomerUnderlying.BulkUnderlyings([]);
        for (var i = 0 ; i < self.TempSelectedUnderlyingBulk().length; i++) {
            CustomerUnderlying.BulkUnderlyings.push(new SelectBulkModel(cifData, self.TempSelectedUnderlyingBulk()[i].ID));
        }
    }
    function setTotalUtilize() {
        var total = 0;
        ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (item) {
            total += parseFloat(ko.utils.unwrapObservable(item.USDAmount()));
        });
        self.utilizationAmount(parseFloat(total).toFixed(2));
        var sRounding = GetRounding(parseFloat(total), self.ParameterRounding());
        if (sRounding == "NaN") {
            viewModel.Rounding(0);
        } else {
            viewModel.Rounding(sRounding);
        }
    }
    function GetRounding(totalUnderlying, parameterRounding) {
        if (totalUnderlying != null && parameterRounding != null) {
            var rounding = totalUnderlying % parameterRounding;
            if (rounding != null && rounding != 0) {
                return rounding = parseFloat(parameterRounding - rounding).toFixed(2);
            }
        }
        return 0;
    }
    function setStatementA() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetStatementA/" + cifData,
            data: null, //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            }, success: function (data) {
                if (data.IsStatementA) {
                    self.StatementLetter_u(new StatementLetterModel(CONST_STATEMENT.StatementB_ID, 'Statement B'));
                    self.IsStatementA(false);
                } else { self.IsStatementA(true); }
            }
        });
    }
    function setRefID() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetRefID/" + cifData,
            data: null, //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            }, success: function (data) {
                self.ReferenceNumber_u(customerNameData + '-' + data.ID);
            }
        });

    }
    function SaveDataFile() {
        if (self.ActivityTitle() == DBS.AcivityTitle.FXDealTMOMakerNettingTask) {
            var transactionID = self.TransactionMakerNetting().Transaction.ID;
            var customerName = self.TransactionMakerNetting().Transaction.Customer.Name;
        } else {
            var transactionID = self.TrackingTMOMaker().Transaction.ID();
            var customerName = self.TrackingTMOMaker().Transaction.Customer.Name();
        }
        SetMappingAttachment(customerName);
        CustomerUnderlyingFile.ID = transactionID;
        CustomerUnderlyingFile.FileName = DocumentModels().FileName;
        CustomerUnderlyingFile.DocumentPath = DocumentModels().DocumentPath;
        CustomerUnderlyingFile.DocumentType = ko.utils.arrayFirst(self.ddlDocumentType_a(), function (item) {
            return item.ID == self.DocumentType_a().ID();
        });
        CustomerUnderlyingFile.DocumentPurpose = ko.utils.arrayFirst(self.ddlDocumentPurpose_a(), function (item) {
            return item.ID == self.DocumentPurpose_a().ID();
        });
        $.ajax({
            type: "POST",
            //url: api.server + api.url.customerunderlyingfile,
            url: api.server + api.url.customerunderlyingfile + "/AddFile",
            data: ko.toJSON(CustomerUnderlyingFile), //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //viewModel.IsEditable(true);
                    // send notification
                    ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                    // hide current popup window
                    $("#modal-form-Attach").modal('hide');
                    $('#backDrop').hide();
                    viewModel.IsUploading(false);
                    var doc = {
                        ID: data,
                        Type: CustomerUnderlyingFile.DocumentType,
                        Purpose: CustomerUnderlyingFile.DocumentPurpose,
                        FileName: CustomerUnderlyingFile.FileName,
                        DocumentPath: CustomerUnderlyingFile.DocumentPath,
                        IsNewDocument: true,
                        LastModifiedDate: new Date(),
                        LastModifiedBy: null,
                        UnderlyingID: 0
                    };
                    if (doc.Purpose.ID == 2) { // data underlying
                        // Set All Document Transaction
                        self.TempSelectedUnderlying([]);
                        self.MakerUnderlyings([]);
                        // remove all underlying documents
                        ko.utils.arrayForEach(self.MakerAllDocumentsTemp(), function (item) {
                            if (item.Purpose.ID == 2) {
                                self.MakerDocuments.remove(item);
                                self.TrackingTMOMaker().Transaction.Documents(jQuery.grep(self.TrackingTMOMaker().Transaction.Documents(), function (value) {
                                    return value.ID != item.ID;
                                }));
                            }
                        });
                        ko.utils.arrayForEach(self.TempSelectedAttachUnderlying(), function (itemTemp) {
                            var docUnderlying = {
                                ID: data,
                                Type: CustomerUnderlyingFile.DocumentType,
                                Purpose: CustomerUnderlyingFile.DocumentPurpose,
                                FileName: CustomerUnderlyingFile.FileName,
                                DocumentPath: CustomerUnderlyingFile.DocumentPath,
                                IsNewDocument: false,
                                LastModifiedDate: new Date(),
                                LastModifiedBy: null,
                                UnderlyingID: itemTemp.ID
                            };
                            self.MakerAllDocumentsTemp.push(docUnderlying);
                            var IsdocumentAlready = ko.utils.arrayFirst(self.MakerDocuments(), function (document) { return document.ID == docUnderlying.ID });
                            if (IsdocumentAlready == null) {
                                self.MakerDocuments.push(docUnderlying);
                                self.TrackingTMOMaker().Transaction.Documents.push(docUnderlying);
                                // set checked underlying
                                ko.utils.arrayForEach(self.CustomerUnderlyings(), function (underlyingItem) {
                                    if (underlyingItem.ID == itemTemp.ID) {
                                        self.TempSelectedUnderlying.push(new SelectUtillizeModel(underlyingItem.ID, true, underlyingItem.AvailableAmount, underlyingItem.StatementLetter.ID));
                                        self.MakerUnderlyings.push(underlyingItem.ID);
                                    }
                                });
                                //self.TransactionDealDetailModel().Documents.push(doc);
                            }
                        });
                        setTotalUtilize();
                    } else {
                        self.MakerDocuments.push(doc);
                        self.MakerAllDocumentsTemp.push(doc);

                    }
                    self.GetDataUnderlying();
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    //viewModel.IsEditable(true);
                    viewModel.IsUploading(false);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                //viewModel.IsEditable(true);
                viewModel.IsUploading(false);
            }
        });
    }
    function SetCalculateFXNetting(amountUSD) {
        if (GetModelTransaction().StatementLetter != null && (GetModelTransaction().StatementLetter.ID == 1 || GetModelTransaction().StatementLetter.ID == 4)) {
            AmountModel.RemainingBalance(0.00);
        } else if (GetModelTransaction().StatementLetter != null && GetModelTransaction().StatementLetter.ID != 4) {
            AmountModel.RemainingBalance(TotalDealModel.Total.RemainingBalance);
        }
        AmountModel.TotalDealAmountsUSD = TotalDealModel.Total.IDRFCYDeal;
        AmountModel.TotalDealUtilization(TotalDealModel.Total.UtilizationDeal);
    }
    function SetCalculateFXCheckerNetting(amountUSD) {
        if (GetModelTransaction().StatementLetter != null && (GetModelTransaction().StatementLetter.ID == 1 || GetModelTransaction().StatementLetter.ID == 4)) {
            AmountModel.RemainingBalance(0.00);
        } else if (GetModelTransaction().StatementLetter != null && GetModelTransaction().StatementLetter.ID != 4) {
            AmountModel.RemainingBalance(TotalDealModel.Total.RemainingBalance + parseFloat(amountUSD));
        }
        AmountModel.TotalDealAmountsUSD(TotalDealModel.Total.IDRFCYDeal);
        AmountModel.TotalDealUtilization(TotalDealModel.Total.UtilizationDeal);
    }

    function GetTotalTransactionNetting() {

        paramhreshold = {
            ProductTypeID: GetModelTransaction().Transaction.ProductType.ID,
            DebitCurrencyCode: GetModelTransaction().Transaction.DebitCurrency.Code,
            TransactionCurrencyCode: GetModelTransaction().Transaction.Currency.Code,
            IsResident: GetModelTransaction().Transaction.IsResident,
            AmountUSD: parseFloat(GetModelTransaction().Transaction.AmountUSD),
            CIF: GetModelTransaction().Transaction.Customer.CIF,
            AccountNumber: GetModelTransaction().Transaction.IsOtherAccountNumber ? null : GetModelTransaction().Transaction.Account.AccountNumber,
            IsJointAccount: GetModelTransaction().Transaction.IsJointAccount,
            StatementLetterID: GetModelTransaction().Transaction.StatementLetter.ID,
            TZReference: null
        }
        console.log(paramhreshold);
        var options = {
            url: api.server + api.url.helper + "/GetTotalTransactionIDRFCY",
            token: accessToken,
            params: {},
            data: ko.toJSON(paramhreshold)
        };
        Helper.Ajax.Post(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                TotalPPUModel.Total.IDRFCYPPU = data.TotalTransaction;
                TotalDealModel.Total.IDRFCYDeal = data.TotalTransaction;
                self.AmountModel().TotalAmountsUSD(data.TotalTransaction);
                TotalPPUModel.Total.UtilizationPPU = data.TotalUtilizationAmount;
                TotalDealModel.Total.UtilizationDeal = data.TotalUtilizationAmount;
                self.AmountModel().TotalUtilization(data.TotalUtilizationAmount);
                TotalDealModel.Total.RemainingBalance = data.AvailableUnderlyingAmount;
                self.AmountModel().RemainingBalance(data.AvailableUnderlyingAmount);
                //self.Rounding(data.RoundingValue);
                self.ParameterRounding(data.RoundingValue);
                self.ThresholdType(data.ThresholdType);
                self.ThresholdValue(data.ThresholdValue);
                self.IsNoThresholdValue(data.ThresholdValue == 0 ? true : false);
                if (self.IsCanEditUnderlying()) {
                    SetCalculateFXNetting(paramhreshold.AmountUSD);
                } else {
                    SetCalculateFXCheckerNetting(paramhreshold.AmountUSD);
                }
            }
        }, OnError);

    }

    function DeleteUnderlyingAttachment(item) {
        //Ajax call to delete the Customer

        $.ajax({
            type: "DELETE",
            url: api.server + api.url.customerunderlyingfile + "/" + item.ID,
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                // send notification
                if (jqXHR.status = 200) {
                    //DeleteFile(item.DocumentPath); deleted file underlying
                    //if(!item.IsNewDocument) {
                    // DeleteTransactionDocumentUnderlying(item);
                    //}
                    ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                    // refresh data
                    //GetDataAttachFile();
                } else
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
    function ApproveData(SaveApprovalData, param, text, callback) {
        if (self.MakerDocuments().length > 0) {
            callback(SaveApprovalData, param);
        } else {
            SaveApprovalData(param.WFID, param.ApproverID, param.DataID);
        }
    }
    //#region Threshold & Validation
    function GetThreshold(SaveApprovalData, param, text, callback) {
        var dataTransaction = ko.toJS(viewModel);

        var paramhreshold = GetValueParamterThresholdbyViewModel();
        var options = {
            url: api.server + api.url.helper + "/GetThresholdFXValue",
            token: accessToken,
            params: {},
            data: ko.toJSON(paramhreshold)
        };
        Helper.Ajax.Post(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                //self.ThresholdValue(ko.mapping.fromJS(data));
                if (IsValidationFX(data, paramhreshold.AmountUSD)) {
                    bootbox.confirm(text, function (result) {
                        if (result) {
                            if (self.MakerDocuments().length > 0) {
                                callback(SaveApprovalData, param);
                            } else {
                                SaveApprovalData(param.WFID, param.ApproverID, param.DataID);
                            }
                        }
                    });
                }
            }
        }, OnError);
    }
    function GetTotalTransaction() {

        paramhreshold = {
            ProductTypeID: GetModelTransaction().Transaction.ProductType.ID(),
            DebitCurrencyCode: GetModelTransaction().Transaction.SellCurrency.Code(),
            TransactionCurrencyCode: GetModelTransaction().Transaction.BuyCurrency.Code(),
            IsResident: GetModelTransaction().Transaction.IsResident(),
            AmountUSD: parseFloat(GetModelTransaction().Transaction.AmountUSD()),
            CIF: GetModelTransaction().Transaction.Customer.CIF(),
            AccountNumber: GetModelTransaction().Transaction.IsOtherAccountNumber() ? null : GetModelTransaction().Transaction.Account.AccountNumber(),
            IsJointAccount: GetModelTransaction().Transaction.IsJointAccount(),
            StatementLetterID: GetModelTransaction().Transaction.StatementLetter.ID(),
            TZReference: null
        }
        var options = {
            url: api.server + api.url.helper + "/GetTotalTransactionIDRFCY",
            token: accessToken,
            params: {},
            data: ko.toJSON(paramhreshold)
        };
        Helper.Ajax.Post(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                TotalPPUModel.Total.IDRFCYPPU = data.TotalTransaction;
                TotalDealModel.Total.IDRFCYDeal = data.TotalTransaction;
                self.AmountModel().TotalAmountsUSD(data.TotalTransaction);
                TotalPPUModel.Total.UtilizationPPU = data.TotalUtilizationAmount;
                TotalDealModel.Total.UtilizationDeal = data.TotalUtilizationAmount;
                self.AmountModel().TotalUtilization(data.TotalUtilizationAmount);
                TotalDealModel.Total.RemainingBalance = data.AvailableUnderlyingAmount;
                self.AmountModel().RemainingBalance(data.AvailableUnderlyingAmount);
                //self.Rounding(data.RoundingValue);
                self.ParameterRounding(data.RoundingValue);
                self.ThresholdType(data.ThresholdType);
                self.ThresholdValue(data.ThresholdValue);
                self.IsNoThresholdValue(data.ThresholdValue == 0 ? true : false);
                /*if (self.IsCanEditUnderlying()) {
                    SetCalculateFX(paramhreshold.AmountUSD);
                } else {
                    SetCalculateFXChecker(paramhreshold.AmountUSD);
                }*/
            }
        }, OnError);

    }

    /* function GetValueParamterThreshold(transaction) {
         var paramhreshold;
         var Transaction;
         switch (self.ActivityTitle()) {
             case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
                 Transaction = transaction.TMO;
                 break;
             case DBS.AcivityTitle.FXDealTMOCheckerTask:
                 Transaction = transaction.TMO;
                 break;
             default:
                 break;
         }
         if (Transaction != null) {
             paramhreshold = {
                 ProductTypeID: GetProductTypeID(Transaction.ProductType),
                 DebitCurrencyCode: Transaction.DebitCurrency.Code,
                 TransactionCurrencyCode: Transaction.Currency.Code,
                 IsResident: Transaction.IsResident,
                 AmountUSD: parseFloat(Transaction.AmountUSD),
                 CIF: Transaction.Customer.CIF,
                 AccountNumber: Transaction.IsOtherAccountNumber ? null : Transaction.Account.AccountNumber,
                 IsJointAccount: Transaction.IsJointAccount
             }
         }
         return paramhreshold;
     } */
    function GetValueParamterThresholdbyViewModel() {
        // untuk submit edit underlying
        var paramhreshold;
        var Transaction;
        switch (self.ActivityTitle()) {
            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                Transaction = self.TrackingTMOMaker().Transaction;
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerTask:
            case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
            case DBS.AcivityTitle.FXTMOCheckerRequestCancelTask:
                Transaction = self.TrackingTMOChecker().Transaction;
                break;
            default:
                break;
        }
        if (Transaction != null) {
            paramhreshold = {
                TzRef: Transaction.TZReference(),
                StatementLetterID: Transaction.StatementLetter.ID(),
                ProductTypeID: GetProductTypeID(ko.toJS(Transaction.ProductType)),
                DebitCurrencyCode: Transaction.SellCurrency.Code(),
                TransactionCurrencyCode: Transaction.BuyCurrency.Code(),
                IsResident: Transaction.IsResident(),
                AmountUSD: parseFloat(Transaction.AmountUSD()),
                CIF: Transaction.Customer.CIF(),
                AccountNumber: Transaction.IsOtherAccountNumber() ? null : Transaction.Account.AccountNumber(),
                IsJointAccount: Transaction.IsJointAccount()
            }
        }
        return paramhreshold;
    }
    function SetHitThreshold(amountUSD) {
        if (self.t_IsFxTransactionToIDR()) {
            self.IsHitThreshold(false);
            if (self.ThresholdValue() != null && self.ThresholdValue() != 0) {
                if (self.ThresholdType() == 'T') {
                    if (amountUSD > self.ThresholdValue()) {
                        self.IsHitThreshold(true);
                        if (viewModel.Selected().StatementLetter != null) {
                            viewModel.Selected().StatementLetter(CONST_STATEMENT.StatementB_ID);
                        }
                    } else {
                        if (viewModel.Selected().StatementLetter != null) {
                            viewModel.Selected().StatementLetter(CONST_STATEMENT.Dash);
                        }
                        self.AmountModel().TotalAmountsUSD(0);
                    }
                } else {
                    if (TotalDealModel.Total.IDRFCYDeal > self.ThresholdValue()) {
                        self.IsHitThreshold(true);
                        if (viewModel.Selected().StatementLetter != null) {
                            viewModel.Selected().StatementLetter(CONST_STATEMENT.StatementB_ID);
                        }
                    } else {
                        if (viewModel.Selected().StatementLetter != null) {
                            viewModel.Selected().StatementLetter(CONST_STATEMENT.Dash);
                        }
                        self.AmountModel().TotalAmountsUSD(0);
                    }
                }
            }
        }
    }
    function SetCalculateFX(amountUSD) {
        if (GetModelTransaction().StatementLetter != null && (GetModelTransaction().StatementLetter.ID == 1 || GetModelTransaction().StatementLetter.ID == 4)) {
            AmountModel().RemainingBalance(0.00);
        } else if (GetModelTransaction().StatementLetter != null && GetModelTransaction().StatementLetter.ID != 4) {
            AmountModel().RemainingBalance(TotalDealModel.Total.RemainingBalance);
        }
        AmountModel().TotalDealAmountsUSD(TotalDealModel.Total.IDRFCYDeal);
        AmountModel().TotalDealUtilization(TotalDealModel.Total.UtilizationDeal);
    }
    function SetCalculateFXChecker(amountUSD) {
        if (GetModelTransaction().StatementLetter != null && (GetModelTransaction().StatementLetter.ID == 1 || GetModelTransaction().StatementLetter.ID == 4)) {
            AmountModel().RemainingBalance(0.00);
        } else if (GetModelTransaction().StatementLetter != null && GetModelTransaction().StatementLetter.ID != 4) {
            AmountModel().RemainingBalance(TotalDealModel.Total.RemainingBalance + parseFloat(amountUSD));
        }
        AmountModel().TotalDealAmountsUSD(TotalDealModel.Total.IDRFCYDeal);
        AmountModel().TotalDealUtilization(TotalDealModel.Total.UtilizationDeal);
    }

    function GetAmountUSD() {
        var output = 0
        switch (self.ActivityTitle()) {
            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                output = self.TrackingTMOMaker().Transaction.AmountUSD();
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerTask:
            case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
            case DBS.AcivityTitle.FXTMOCheckerRequestCancelTask:
                output = self.TrackingTMOChecker().Transaction.AmountUSD();
                break;
            default:
                break;
        }
        return output;
    }
    function GetTotalUnderlyingRounding(rounding) {
        var totalUnderlying;
        if (self.TempSelectedUnderlying() != null && self.TempSelectedUnderlying().length > 0) {
            switch (self.TempSelectedUnderlying()[0].StatementLetter()) {
                case 2:
                    totalUnderlying = parseFloat(GetUtilizationAmount()) + parseFloat(rounding);
                    break;
                default:
                    totalUnderlying = parseFloat(GetUtilizationAmount());
                    break;
            }
        } else {
            totalUnderlying = parseFloat(GetUtilizationAmount());
        }
        return totalUnderlying;
    }

    function IsValidationFX(data, amountUSD) {
        var isStatus = true;
        var IsSelectedUnderlying = (self.TempSelectedUnderlying() != null && self.TempSelectedUnderlying().length > 0);
        var document = ko.utils.arrayFilter(self.MakerDocuments(), function (item) {
            return item.Purpose.ID == 1; // Instruction Document
        });
        var documentUnderlying = ko.utils.arrayFilter(viewModel.MakerDocuments(), function (item) {
            return item.Purpose.ID == 2; // Underlying Document
        });

        var documentStatementLetter = ko.utils.arrayFilter(viewModel.MakerDocuments(), function (item) {
            return item.Purpose.Name == 'Statement Letter'; // Statement Letter Document
        });
        var isNeedUnderlying;
        switch (self.ActivityTitle()) {
            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                isNeedUnderlying = (self.t_IsFxTransaction() || (self.t_IsFxTransactionToIDR() && !self.IsNoThresholdValue()));
                break;
            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
                isNeedUnderlying = (self.t_IsFxTransaction() || (self.t_IsFxTransactionToIDR() && !self.IsNoThresholdValue())) && self.IsUnderlyingCompletedEmpty();
                break;
        }
        if (isNeedUnderlying) {
            if (!IsSelectedUnderlying && (document != null && document.length == 0)) { //validastion submit underlyin or statement               
                ShowNotification("Form Validation Warning", CONST_MSG.ValidationUnderlyingInstraction, 'gritter-warning', false);
                return false;
            }

            if (!IsSelectedUnderlying && self.TrackingTMOMaker().Transaction.IsUnderlyingCompleted()) {
                var msg = 'To submit this transaction, Please untick Is Underlying Completed field or select utilize underlying document.';
                ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
                return false;
            }
            if (IsSelectedUnderlying && self.TrackingTMOMaker().Transaction.IsUnderlyingCompleted() && (documentUnderlying != null && documentUnderlying.length == 0)) {
                var msg = 'Transaction does not have underlying document, Please upload document underlying file.';
                ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
                return false;
            }
            if (IsSelectedUnderlying && self.TempSelectedUnderlying()[0].StatementLetter() != GetModelTransaction().Transaction.StatementLetter.ID()) {
                var errorMessage;
                switch (GetModelTransaction().Transaction.StatementLetter.ID()) {
                    case CONST_STATEMENT.StatementA_ID:
                        errorMessage = "Please select utilize underlying Statement A.";
                        break;
                    case CONST_STATEMENT.StatementB_ID:
                        errorMessage = "Please select utilize underlying Statement B.";
                        break;
                    case CONST_STATEMENT.AnnualStatement_ID:
                        errorMessage = "Please select utilize underlying Annual Statement Letter.";
                        break;
                    case CONST_STATEMENT.Dash_ID:
                        errorMessage = "Please select statement letter or untick utilization underlying.";
                        break;
                }
                ShowNotification("Form Underlying Warning", errorMessage, 'gritter-warning', false);
                return false;
            }
        }
        var IsSubmissionDateStatementLetter = self.TrackingTMOMaker().Transaction.SubmissionDateSL() == '' || self.TrackingTMOMaker().Transaction.SubmissionDateSL() == null;
        if (IsSubmissionDateStatementLetter == false && !(documentStatementLetter != null && documentStatementLetter.length > 0)) {
            var msg = 'Please upload Underlying document or remove Actual Submission Date Statement Letter field.';
            ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
            IsStatus = false;
            return IsStatus;
        }
        if (IsSubmissionDateStatementLetter == true && (documentStatementLetter != null && documentStatementLetter.length > 0)) {
            var msg = 'Please entry Actual Submission Date Statement Letter field.';
            ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
            IsStatus = false;
            return IsStatus;
        }

        var IsSubmissionDateInstruction = self.TrackingTMOMaker().Transaction.SubmissionDateInstruction() == '' || self.TrackingTMOMaker().Transaction.SubmissionDateInstruction() == null;
        if (IsSubmissionDateInstruction == false && !(document != null && document.length > 0)) {
            var msg = 'Please upload Instruction Document or remove Actual Submission Date Instruction field.';
            ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
            IsStatus = false;
            return IsStatus;
        }
        if (IsSubmissionDateInstruction == true && (document != null && document.length > 0)) {
            var msg = 'Please entry Actual Submission Date Instruction field.';
            ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
            IsStatus = false;
            return IsStatus;
        }

        var IsSubmissionDateUnderlying = self.TrackingTMOMaker().Transaction.SubmissionDateUnderlying() == '' || self.TrackingTMOMaker().Transaction.SubmissionDateUnderlying() == null;
        if (IsSubmissionDateUnderlying == false && !(documentUnderlying != null && documentUnderlying.length > 0)) {
            var msg = 'Please upload Underlying document or remove Actual Submission Date Underlying field.';
            ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
            IsStatus = false;
            return IsStatus;
        }
        if (IsSubmissionDateUnderlying == true && (documentUnderlying != null && documentUnderlying.length > 0)) {
            var msg = 'Please entry Actual Submission Date Underlying field.';
            ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
            IsStatus = false;
            return IsStatus;
        }

        if ((IsSelectedUnderlying && isNeedUnderlying) && self.TempSelectedUnderlying()[0].StatementLetter() != 4) {
            //var totalUnderlying = GetTotalUnderlyingRounding(data.RoundingValue);
            var totalUnderlying = GetTotalUnderlyingRounding(self.Rounding());
            if (amountUSD <= totalUnderlying) {
                if ((self.TempSelectedUnderlying() != null && self.TempSelectedUnderlying().length > 0)) {
                    if ((self.TempSelectedUnderlying() != null && self.TempSelectedUnderlying().length > 0) && data.IsHitThreshold && (self.TempSelectedUnderlying()[0].StatementLetter() == 1 || self.TempSelectedUnderlying()[0].StatementLetter() == 3)) {
                        ShowNotification("Form Underlying Warning", "Total IDR->FCY Amount greater than " + data.ThresholdValue + " (USD), Please add statement B underlying", 'gritter-warning', false);
                        isStatus = false;
                    }
                }
            } else {
                ShowNotification("Form Underlying Warning", "Total Underlying Amount must be equal greater than Transaction Amount", 'gritter-warning', false);
                isStatus = false;
            }
        }
        if (!self.IsUnderlyingCompletedEmpty() && self.ActivityTitle() == DBS.AcivityTitle.FXTMOMakerAfterSubmitTask) {
            if (IsSelectedUnderlying && self.TrackingTMOMaker().Transaction.IsUnderlyingCompleted() && (documentUnderlying != null && documentUnderlying.length == 0)) {
                var msg = 'Transaction does not have underlying document, Please upload document underlying file.';
                ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
                return false;
            }
            GetModelTransaction().Transaction.Underlyings([]);
        }
        return isStatus;
    }
    function GetUtilizationAmount() {
        var utilization = 0;
        switch (self.ActivityTitle()) {
            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
                utilization = self.utilizationAmount();
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerTask:
            case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
            case DBS.AcivityTitle.FXTMOCheckerRequestCancelTask:
                utilization = self.utilizationAmount();
                break;
            default:
                break;
        }
        return utilization;
    }
    function GetCalculateFX() {
        AmountModel().TotalPPUAmountsUSD(TotalPPUModel.Total.IDRFCYPPU);
        AmountModel().TotalDealAmountsUSD(TotalDealModel.Total.IDRFCYDeal);
        AmountModel().RemainingBalance(TotalDealModel.Total.RemainingBalance);
        AmountModel().TotalPPUUtilization(TotalPPUModel.Total.UtilizationPPU);
        AmountModel().TotalDealUtilization(TotalDealModel.Total.UtilizationDeal);
    }
    //#endregion
    function OnChangeDebitAccount() {
        switch (self.ActivityTitle()) {
            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
                var AccountNumberSelected = $('#debit-acc-number option:selected').text().trim();
                if (AccountNumberSelected == '-') {
                    self.IsEmptyAccountNumber(true);
                    self.TransactionMaker().Transaction.IsOtherAccountNumber(true);
                } else {
                    self.IsEmptyAccountNumber(false);
                    self.TrackingTMOMaker().Transaction.OtherAccountNumber(null);
                    self.TrackingTMOMaker().Transaction.IsOtherAccountNumber(false);
                    var account = ko.utils.arrayFirst(self.TrackingTMOMaker().Transaction.Customer.Accounts(), function (item) { return item.AccountNumber() == self.Selected().Account(); });
                    if (account != null) {
                        self.TrackingTMOMaker().Transaction.Account = account;
                        self.TrackingTMOMaker().Transaction.AccountNumber = account.AccountNumber;
                    }
                    var update1 = self.TrackingTMOMaker();
                    self.TrackingTMOMaker(update1);
                }
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerTask:
            case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
            case DBS.AcivityTitle.FXTMOCheckerRequestCancelTask:
                var AccountNumberSelected = $('#debit-acc-number option:selected').text().trim();
                if (AccountNumberSelected == '-') {
                    self.IsEmptyAccountNumber(true);
                    self.TrackingTMOChecker().Transaction.IsOtherAccountNumber(true);
                } else {
                    self.IsEmptyAccountNumber(false);
                    self.TrackingTMOChecker().Transaction.OtherAccountNumber(null);
                    self.TrackingTMOChecker().Transaction.IsOtherAccountNumber(false);
                    var account = ko.utils.arrayFirst(self.TrackingTMOChecker().Transaction.Customer.Accounts, function (item) { return item.AccountNumber() == self.Selected().Account(); });
                    if (account != null) {
                        self.TrackingTMOChecker().Transaction.Account = account;
                        self.TrackingTMOChecker().Transaction.AccountNumber = account.AccountNumber;
                    }

                    var update1 = self.TrackingTMOChecker();
                    self.TrackingTMOChecker(update1);
                }
                break;
        }
    }
    function GetJointAccount(obj) {
        var isJointAcc;
        switch (viewModel.ActivityTitle()) {
            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
                isJointAcc = obj.IsJointAccount();
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerTask:
            case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
                isJointAcc = obj.IsJointAccount();
                break;
            default:
                ShowMesageDefault();
                break;
        }
        return isJointAcc;
    }
    function OnChangeJointAcc(obj) {
        var isJointacc = GetJointAccount(obj);
        if (isJointacc != null) {
            var dataAccounts = ko.utils.arrayFilter(GetDataCustomerAccounts(), function (dta) {
                var sAccount = dta.IsJointAccount() == null ? false : dta.IsJointAccount();
                var debitCurrency = GetDataDebitCurrency();
                return sAccount == isJointacc && (debitCurrency !== null ? debitCurrency.Code() : "");
            })
            if (dataAccounts != null && dataAccounts.length > 0) {
                self.DynamicAccounts([]);
                if (isJointacc == false) {
                    dataAccounts = AddOtherAccounts(dataAccounts);
                }
                self.DynamicAccounts(dataAccounts);
            }
        }
    }
    function GetDataCustomerAccounts() {
        var accounts = [];
        switch (self.ActivityTitle()) {
            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
                accounts = self.TrackingTMOMaker().Transaction.Customer.Accounts();
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerTask:
            case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
            case DBS.AcivityTitle.FXTMOCheckerRequestCancelTask:
                accounts = self.TrackingTMOChecker().Transaction.Customer.Accounts();
                break;
        }
        return accounts;
    }
    function GetDataDebitCurrency() {
        var accounts = [];
        switch (self.ActivityTitle()) {
            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
                accounts = self.TrackingTMOMaker().Transaction.DebitCurrency;
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerTask:
            case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
            case DBS.AcivityTitle.FXTMOCheckerRequestCancelTask:
                accounts = self.TrackingTMOChecker().Transaction.DebitCurrency;
                break;
        }
        return accounts;
    }
    function GetTaskOutcomes(taskListID, taskListItemID) {
        var task = {
            TaskListID: taskListID,
            TaskListItemID: taskListItemID
        };

        var options = {
            url: "/_vti_bin/DBSNintex/Services.svc/GetTaskOutcomesByListID",
            data: ko.toJSON(task)
        };

        Helper.Sharepoint.Nintex.Post(options, OnSuccessTaskOutcomes, OnError, OnAlways);
    }
    function GetHomeTaskOutcomes(taskListID, taskListItemID, error, always) {
        var task = {
            TaskListID: taskListID,
            TaskListItemID: taskListItemID
        };

        var options = {
            url: "/_vti_bin/DBSNintex/Services.svc/GetTaskOutcomesByListID",
            data: ko.toJSON(task)
        };

        var isValid = true;
        var Url = options.url;
        var method = "POST";
        // validate & decode params
        if (options.params != null || options.params != undefined) {
            Url += DecodeParams(options.params);
        }

        // declare ajax options
        var ajaxOptions = {
            async: false,
            type: method,
            url: Url,
            contentType: "application/json; odata=verbose",
            dataType: "json",
            headers: {
                "accept": "application/json; odata=verbose",
                "X-RequestDigest": options.digest
            },
            success: function (data, textStatus, jqXHR) {
                OnSuccessHomeTaskOutcomes(data, textStatus, jqXHR);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                error(jqXHR, textStatus, errorThrown);
            }
        };

        // set body request
        if (method != "GET") {
            // validate request body on POST
            if (options.data == null || options.data == undefined) {
                alert(method + " method does not contains request body");

                isValid = false;
            } else {
                ajaxOptions.data = options.data;
            }
        }

        // execute ajax request
        if (isValid) {
            $.ajax(ajaxOptions).always(function () {
                if (always != null || always != undefined)
                    always();
            });
        } else {
            if (always != null || always != undefined)
                always();
        }
        //Helper.Sharepoint.Nintex.Post(options, OnSuccessHomeTaskOutcomes, OnError, OnAlways);

    }
    // Completing Nintex task
    function CompletingTask(taskListID, taskListItemID, taskTypeID, outcomeID, comments) {
        var task = {
            TaskListID: taskListID,
            TaskListItemID: taskListItemID,
            OutcomeID: outcomeID,
            Comment: comments
        };

        var endPointURL;
        switch (taskTypeID) {
            // Request Approval
            case 0: endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondApprovalTask";
                break;

                // Request Review
            case 1: endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondReviewTask";
                break;

                // Flexi Task
            case 4: endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondFlexiTask";
                break;
        }

        var options = {
            url: endPointURL,
            data: ko.toJSON(task)
        };

        Helper.Sharepoint.Nintex.Post(options, OnSuccessCompletingTask, OnError, OnAlways);
    }
    function CompletingTaskHome(taskListID, taskListItemID, taskTypeID, outcomeID, comments) {
        var task = {
            TaskListID: taskListID,
            TaskListItemID: taskListItemID,
            OutcomeID: outcomeID,
            Comment: comments
        };

        var endPointURL;
        switch (taskTypeID) {
            // Request Approval
            case 0: endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondApprovalTask";
                break;

                // Request Review
            case 1: endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondReviewTask";
                break;

                // Flexi Task
            case 4: endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondFlexiTask";
                break;
        }

        var options = {
            url: endPointURL,
            data: ko.toJSON(task)
        };
        //console.log(options.data);
        Helper.Sharepoint.Nintex.Post(options, OnSuccessCompletingTask, DoNothing, OnAlways);
    }
    function DisableForm() {
        $("#transaction-form").find(" input, select, textarea, button").prop("disabled", true);
    }
    function OnSuccessTaskOutcomes(data, textStatus, jqXHR) {
        if (data.IsSuccess) {

            self.Outcomes(data.Outcomes);
            self.IsPendingNintex(data.IsPending);
            self.IsAuthorizedNintex(data.IsAuthorized);
            self.MessageNintex(data.Message);

            // Get transaction Data            
            GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());

            // lock current task only if user had permission
            if (self.IsPendingNintex() == true && self.IsAuthorizedNintex() == true) {
                LockAndUnlockTaskHub("Lock", self.WorkflowInstanceID(), self.ApproverId(), self.SPTaskListItemID(), self.ActivityTitle());
            }
        } else {
            if (data.Outcomes == null) {
                self.Outcomes(data.Outcomes);
                self.IsPendingNintex(data.IsPending);
                self.IsAuthorizedNintex(data.IsAuthorized);
                self.MessageNintex(data.Message);
                GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());
            }
            else {
                ShowNotification("An error Occured", jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        }
    }
    function OnSuccessHomeTaskOutcomes(data, textStatus, jqXHR, workflowInstanceID, approverID) {

        if (data.IsSuccess) {
            self.HomeOutcomes.push(data.Outcomes);
            self.HomeOutcomesRow.push(data.Outcomes);
            self.IsPendingNintexHome(data.IsPending);
            self.IsAuthorizedNintexHome(data.IsAuthorized);
            self.MessageNintexHome(data.Message);

        } else {
            if (data.Outcomes == null) {
                self.HomeOutcomes.push(data.Outcomes);
                self.HomeOutcomesRow.push(data.Outcomes);
                self.IsPendingNintexHome(data.IsPending);
                self.IsAuthorizedNintexHome(data.IsAuthorized);
                self.MessageNintexHome(data.Message);


            }
            else {
                ShowNotification("An error Occured", jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        }
        if (!IsRenameButtonSuccess) {
            var CounterRename = setInterval(function () {
                Counter(CounterRename)
            }, 100);
            var Counter = function (CounterRename) {
                if (IsRenameButtonSuccess) {
                    clearInterval(CounterRename);
                    $('[id*="btnHomeApproval"]>span').each(function () {
                        if (!$(this).text().trim().length) {
                            $(this).parent().hide();
                        }

                    });

                } else {
                    //console.log(data.Outcomes);
                    if (ko.toJS(self.HomeOutcomes().length) == ko.toJS(self.RowCount)) {
                        IsRenameButtonSuccess = true;
                        for (var i = 0; i < ko.toJS(self.HomeOutcomes().length) ; i++) {
                            for (var j = 0; j < ko.toJS(self.HomeOutcomes()[i].length) ; j++) {
                                if (ko.toJS(self.HomeOutcomes()[i][j].Name) == 'Continue') {
                                    self.HomeOutcomes()[i][j].Name = 'Submit';
                                }

                                $('#btnHomeApproval' + i + j).text(ko.toJS(self.HomeOutcomes()[i][j].Name));
                                $('#btnHomeApproval' + i + j).attr('SPTaskListID', ko.toJS(self.SPTaskListIDHome()[i]));
                                $('#btnHomeApproval' + i + j).attr('SPTaskListItemID', ko.toJS(self.SPTaskListItemIDHome()[i]));
                                $('#btnHomeApproval' + i + j).attr('TaskTypeID', ko.toJS(self.TaskTypeIDHome()[i]));
                                $('#btnHomeApproval' + i + j).attr('ActivityTitle', ko.toJS(self.ActivityTitleHome()[i]));
                                $('#btnHomeApproval' + i + j).attr('WorkflowInstanceID', ko.toJS(self.WorkflowInstanceIDHome()[i]));
                                $('#btnHomeApproval' + i + j).attr('ApproverID', ko.toJS(self.ApproverIDHome()[i]));
                                $('#btnHomeApproval' + i + j).attr('OutcomeID', ko.toJS(self.HomeOutcomes()[i][j].ID));

                            }
                        }

                    }

                }
            };
        }

    }
    function OnSuccessCompletingTask(data, textStatus, jqXHR) {
        if (data.IsSuccess) {
            // send notification            
            ShowNotification('Completing Task Success', jqXHR.responseJSON.Message, 'gritter-success', false);

            // close dialog task
            $("#modal-form").modal('hide');

            // reload tasks & show progress bar
            $("#transaction-progress").show();

            self.GetData();

        } else {
            ShowNotification('Completing Task Failed', jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }
    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterWorkflowName() != "") filters.push({ Field: 'WorkflowName', Value: self.FilterWorkflowName() });
        if (self.FilterInitiator() != "") filters.push({ Field: 'Initiator', Value: self.FilterInitiator() });
        if (self.FilterStateDescription() != "") filters.push({ Field: 'StateDescription', Value: self.FilterStateDescription() });
        if (self.FilterStartTime() != "") filters.push({ Field: 'StartTime', Value: self.FilterStartTime() });
        if (self.FilterTitle() != "") filters.push({ Field: 'Title', Value: self.FilterTitle() });
        if (self.FilterTransactionStatus() != "") filters.push({ Field: 'TransactionStatus', Value: self.FilterTransactionStatus() });
        if (self.FilterUser() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterUser() });
        if (self.FilterUserApprover() != "") filters.push({ Field: 'UserApprover', Value: self.FilterUserApprover() });
        if (self.FilterEntryTime() != "") filters.push({ Field: 'EntryTime', Value: self.FilterEntryTime() });
        if (self.FilterExitTime() != "") filters.push({ Field: 'ExitTime', Value: self.FilterExitTime() });
        if (self.FilterOutcomeDescription() != "") filters.push({ Field: 'OutcomeDescription', Value: self.FilterOutcomeDescription() });
        if (self.FilterCustomOutcomeDescription() != "") filters.push({ Field: 'CustomOutcomeDescription', Value: self.FilterCustomOutcomeDescription() });
        if (self.FilterComments() != "") filters.push({ Field: 'Comments', Value: self.FilterComments() });


        if (self.FilterApplicationID() != "") filters.push({ Field: 'ApplicationID', Value: self.FilterApplicationID() });
        if (self.FilterCustomer() != "") filters.push({ Field: 'Customer', Value: self.FilterCustomer() });
        if (self.FilterCIF() != "") filters.push({ Field: 'CIF', Value: self.FilterCIF() });
        if (self.FilterProduct() != "") filters.push({ Field: 'Product', Value: self.FilterProduct() });
        if (self.FilterChannel() != "") filters.push({ Field: 'Channel', Value: self.FilterChannel() });

        return filters;
    };

    // Get filtered columns value
    function GetUnderlyingFilteredColumns() {
        // define filter

        var filters = [];
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingFilterIsSelected() });
        if (self.UnderlyingFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingFilterUnderlyingDocument() });
        if (self.UnderlyingFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingFilterDocumentType() });
        if (self.UnderlyingFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingFilterCurrency() });
        if (self.UnderlyingFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingFilterStatementLetter() });
        if (self.UnderlyingFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingFilterAmount() });
        if (self.UnderlyingFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingFilterDateOfUnderlying() });
        if (self.UnderlyingFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingFilterExpiredDate() });
        if (self.UnderlyingFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingFilterReferenceNumber() });
        if (self.UnderlyingFilterInvoiceNumber() != "") filters.push({ Field: 'InvoiceNumber', Value: self.UnderlyingFilterInvoiceNumber() });
        if (self.UnderlyingFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingFilterSupplierName() });
        if (self.UnderlyingFilterIsAvailable() != "") filters.push({ Field: 'IsAvailable', Value: self.UnderlyingFilterIsAvailable() });
        if (self.UnderlyingFilterIsExpiredDate() != "") filters.push({ Field: 'IsExpiredDate', Value: self.UnderlyingFilterIsExpiredDate() });
        if (self.UnderlyingFilterIsSelectedBulk() == false) filters.push({ Field: 'IsSelectedBulk', Value: self.UnderlyingFilterIsSelectedBulk() });
        if (self.UnderlyingFilterAvailableAmount() != "") filters.push({ Field: 'AvailableAmount', Value: self.UnderlyingFilterAvailableAmount() });
        if (self.UnderlyingFilterAttachmentNo() != "") filters.push({ Field: 'AttachmentNo', Value: self.UnderlyingFilterAttachmentNo() });
        if (self.UnderlyingFilterIsTMO() != "") filters.push({ Field: 'IsTMO', Value: self.UnderlyingFilterIsTMO() });
        if (self.UnderlyingFilterAccountNumber() != "") filters.push({ Field: 'AccountNumber', Value: self.UnderlyingFilterAccountNumber() });
        filters.push({ Field: 'StatusShowData', Value: self.UnderlyingFilterShow() });

        return filters;
    };

    // Get filtered columns value
    function GetUnderlyingAttachFilteredColumns() {
        var filters = [];
        self.UnderlyingFilterIsSelected(true); // flag for get all datas
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingAttachFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingAttachFilterIsSelected() });
        if (self.UnderlyingAttachFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingAttachFilterUnderlyingDocument() });
        if (self.UnderlyingAttachFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingAttachFilterDocumentType() });
        if (self.UnderlyingAttachFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingAttachFilterCurrency() });
        if (self.UnderlyingAttachFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingAttachFilterStatementLetter() });
        if (self.UnderlyingAttachFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingAttachFilterAmount() });
        if (self.UnderlyingAttachFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingAttachFilterDateOfUnderlying() });
        if (self.UnderlyingAttachFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingAttachFilterExpiredDate() });
        if (self.UnderlyingAttachFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingAttachFilterReferenceNumber() });
        if (self.UnderlyingAttachFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingAttachFilterSupplierName() });
        if (self.UnderlyingAttachFilterAccountNumber() != "") filters.push({ Field: 'AccountNumber', Value: self.UnderlyingAttachFilterAccountNumber() });
        if (self.UnderlyingAttachFilterIsTMO() != "") filters.push({ Field: 'IsTMO', Value: self.UnderlyingAttachFilterIsTMO() });
        filters.push({ Field: 'StatusShowData', Value: self.UnderlyingFilterShow() });
        return filters;
    }

    // Get filtered columns value
    function GetAttachFilteredColumns() {
        // define filter

        var filters = [];
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.AttachFilterFileName() != "") filters.push({ Field: 'FileName', Value: self.AttachFilterFileName() });
        if (self.AttachFilterDocumentPurpose() != "") filters.push({ Field: 'DocumentPurpose', Value: self.AttachFilterDocumentPurpose() });
        if (self.AttachFilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.AttachFilterModifiedDate() });
        if (self.AttachFilterDocumentRefNumber() != "") filters.push({ Field: 'DocumentRefNumber', Value: self.AttachFilterDocumentRefNumber() });
        if (self.AttachFilterAttachmentReference() != "") filters.push({ Field: 'AttachmentReference', Value: self.AttachFilterAttachmentReference() });
        return filters;
    };

    // Get filtered columns value
    function GetUnderlyingProformaFilteredColumns() {
        // define filter
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.ProformaFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.ProformaFilterIsSelected() });
        if (self.ProformaFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.ProformaFilterUnderlyingDocument() });
        if (self.ProformaFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.ProformaFilterDocumentType() });
        if (self.ProformaFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.ProformaFilterCurrency() });
        if (self.ProformaFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.ProformaFilterStatementLetter() });
        if (self.ProformaFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.ProformaFilterAmount() });
        if (self.ProformaFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.ProformaFilterDateOfUnderlying() });
        if (self.ProformaFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.ProformaFilterExpiredDate() });
        if (self.ProformaFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.ProformaFilterReferenceNumber() });
        if (self.ProformaFilterInvoiceNumber() != "") filters.push({ Field: 'InvoiceNumber', Value: self.ProformaFilterInvoiceNumber() });

        if (self.ProformaFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.ProformaFilterSupplierName() });

        return filters;
    };

    // Get filtered columns value // add 2015.03.09
    function GetBulkUnderlyingFilteredColumns() {
        // define filter
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.BulkFilterCurrency(self.Currency_u().Code() != "" && self.Currency_u().Code() != null ? self.Currency_u().Code() : "xyz");
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: self.CIF_u() });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.BulkFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.BulkFilterIsSelected() });
        if (self.BulkFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.BulkFilterUnderlyingDocument() });
        if (self.BulkFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.BulkFilterDocumentType() });
        if (self.BulkFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.BulkFilterCurrency() });
        if (self.BulkFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.BulkFilterStatementLetter() });
        if (self.BulkFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.BulkFilterAmount() });
        if (self.BulkFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.BulkFilterDateOfUnderlying() });
        if (self.BulkFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.BulkFilterExpiredDate() });
        if (self.BulkFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.BulkFilterReferenceNumber() });
        if (self.BulkFilterInvoiceNumber() != "") filters.push({ Field: 'InvoiceNumber', Value: self.BulkFilterInvoiceNumber() });
        if (self.BulkFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.BulkFilterSupplierName() });

        return filters;
    };


    // get all parameters need
    function GetParameters() {
        var options = {
            url: api.server + api.url.parameter,
            params: {
                select: "Currency,AgentCharge,FXCompliance,ChargesType,DocType,PurposeDoc,statementletter,underlyingdoc,customer,ratetype,producttype"
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
    }
    function OnSuccessGetParameters(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            var mapping = {
                'ignore': ["LastModifiedDate", "LastModifiedBy"]
            };
            self.Currencies = ko.mapping.fromJS(data.Currency, mapping);
            self.StatementLetter = ko.mapping.fromJS(data.StatementLetter, mapping);
            self.UnderlyingDocumens = ko.mapping.fromJS(data.UnderltyingDoc, mapping);
            self.ProductTypes = ko.mapping.fromJS(data.ProductType, mapping);
            self.DocumentPurposes = ko.mapping.fromJS(data.PurposeDoc, mapping);
            // underlying parameter
            viewModel.GetUnderlyingParameters(data);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') { $remove = true; $box.addClass('position-relative'); }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        var urlTuning = GetUrl(self.FormMenuCustom());

        // declare options variable for ajax get request
        var options = null;
        options = {
            //url: api.server + api.url.task,
            url: api.server + urlTuning,
            params: {
                webid: config.sharepoint.webId,
                siteid: config.sharepoint.siteId,
                workflowids: config.sharepoint.workflowId,
                state: self.WorkflowConfig().State,
                outcome: self.WorkflowConfig().Outcome,
                //customOutcome:  "1,2,3,4,5,6,7,8",
                customOutcome: self.WorkflowConfig().CustomOutcome,
                showContribute: self.WorkflowConfig().ShowContribute,
                showActiveTask: self.WorkflowConfig().ShowActiveTask,
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }

    }

    //Function to Read All Customers Underlying
    /* function GetDataUnderlying() {
         // widget reloader function start
         if ($box.css('position') == 'static') {
             $remove = true;
             $box.addClass('position-relative');
         }
         $box.append(config.spinner);
 
         $box.one('reloaded.ace.widget', function () {
             $box.find('.widget-box-overlay').remove();
             if ($remove) $box.removeClass('position-relative');
         });
         var transactionDealID = self.TransactionDealID();
         // declare options variable for ajax get request
         var options = {
             //url: api.server + api.url.customerunderlying + "/Underlying",
             url: api.server + api.url.customerunderlying + "/UnionTransactionDeal/" + transactionDealID,
             params: {
                 page: self.UnderlyingGridProperties().Page(),
                 size: self.UnderlyingGridProperties().Size(),
                 sort_column: self.UnderlyingGridProperties().SortColumn(),
                 sort_order: self.UnderlyingGridProperties().SortOrder()
             },
             token: accessToken
         };
 
         // get filtered columns
         self.UnderlyingFilterIsAvailable(true);
         self.UnderlyingFilterIsExpiredDate(true);
         var filters = GetUnderlyingFilteredColumns();
 
         if (filters.length > 0) {
             // POST
             // add request body on POST
             options.data = ko.toJSON(filters);
 
             Helper.Ajax.Post(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
         } else {
             // GET
             Helper.Ajax.Get(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
         }
     } */
    // On success GetData callback
    function OnSuccessGetDataUnderlying(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            ko.utils.arrayForEach(data.Rows, function (itemd) {
                itemd.UtilizeAmountDeal = formatNumber(itemd.UtilizeAmountDeal);
            });

            var dataunderlying = SetSelectedUnderlying(data.Rows);
            self.CustomerUnderlyings([]);
            self.CustomerUnderlyings(dataunderlying);
            self.UnderlyingGridProperties().Page(data['Page']);
            self.UnderlyingGridProperties().Size(data['Size']);
            self.UnderlyingGridProperties().Total(data['Total']);
            self.UnderlyingGridProperties().TotalPages(Math.ceil(self.UnderlyingGridProperties().Total() / self.UnderlyingGridProperties().Size()));
            //LUPA UNTUK APA
            //if (self.CustomerUnderlyings() != null && self.CustomerUnderlyings().length > 0) {
            //    self.TempSelectedUnderlying([]);
            //    ko.utils.arrayForEach(self.CustomerUnderlyings(), function (underlyingItem) {
            //        if (underlyingItem.IsEnable == true) {
            //            self.TempSelectedUnderlying.push(new SelectUtillizeModel(underlyingItem.ID, false, underlyingItem.AvailableAmount, underlyingItem.StatementLetter.ID));
            //        }
            //    });
            //    setTotalUtilize();
            //    //var total = TotalUtilize();
            //    //self.utilizationAmount(total);
            //}
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }
    //function SetSelectedUnderlying(data) {
    //    for (var i = 0; i < data.length; i++) {
    //        var selected = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (item) {
    //            return item.ID() == data[i].ID;
    //        });
    //        if (selected != null) {
    //            data[i].IsEnable = true;
    //            data[i].UtilizeAmountDeal = formatNumber(selected.UtilizeAmountDeal());
    //        }
    //        else {
    //            data[i].IsEnable = false;
    //        }
    //    }
    //    return data;
    //}
    function SetSwapTypeTransaction() {
        var swap = GetModelTransaction().Transaction.SwapType();
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetSwapDataTransaction/" + GetModelTransaction().Transaction.NB() + "/" + GetModelTransaction().Transaction.TZReference(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if (data != null) {
                        self.IsSwapTransaction(true);
                        self.MessageTransactionType('Swap transaction');
                        self.Swap_NettingDFNumber(data.DFNumber);
                        GetModelTransaction().Transaction.SwapTransactionID(data.SwapTransactionID);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });

    }


    function SetNettingTypeTransaction() {
        var isNetting = GetModelTransaction().Transaction.IsNettingTransaction();
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetNettingDataTransaction/" + GetModelTransaction().Transaction.TZReference(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if (data != null) {
                        self.IsNettingTransaction(true);
                        self.MessageTransactionType('Netting transaction');
                        self.Swap_NettingDFNumber(data.DFNumber);
                        //GetModelTransaction().Transaction.SwapTransactionID(data.SwapTransactionID);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });

    }


    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            IsRenameButtonSuccess = false;

            self.Tasks(data.Rows);
            //console.log(ko.toJS(self.Tasks)); 
            self.HomeOutcomes([]);
            self.SPTaskListIDHome([]);
            self.SPTaskListItemIDHome([]);
            self.TaskTypeIDHome([]);
            self.ActivityTitleHome([]);
            self.WorkflowInstanceIDHome([]);
            self.ApproverIDHome([]);
            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
            self.RowCount(data.Rows.length);
            ko.utils.arrayForEach(self.Tasks(), function (item, index, OnError, OnAlways) {
                self.SPTaskListIDHome.push(item.WorkflowContext.SPTaskListID);
                self.SPTaskListItemIDHome.push(item.WorkflowContext.SPTaskListItemID);
                self.TaskTypeIDHome.push(item.CurrentTask.TaskTypeID);
                self.ActivityTitleHome.push(item.CurrentTask.ActivityTitle);
                self.WorkflowInstanceIDHome.push(item.WorkflowContext.WorkflowInstanceID);
                self.ApproverIDHome.push(item.WorkflowContext.ApproverID);
            });

            var TaskCount = ko.toJS(self.Tasks().length);
            for (var i = 0; i < TaskCount; i++) {
                GetHomeTaskOutcomes(self.SPTaskListIDHome()[i], self.SPTaskListItemIDHome()[i], OnError, OnAlways);
            }

            $('[id*="btnHomeApproval"]>span').text('');
            //$("#transaction-progress").hide();
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // Get Url for Menu TMO Home
    function GetUrl(CustomMenu) {
        var urlTuning;
        if (CustomMenu != null) {
            switch (CustomMenu.toLowerCase()) {
                case "home-tmo":
                    urlTuning = api.url.hometmo;
                    break;

            }
        }
        return urlTuning;
    }
    // add chandra : 201511023
    function GetPermisssion() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    viewModel.isDealBUGroup(false);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].toLowerCase().startsWith("dbs tmo") && Const_RoleName[spUser.Roles[i].ID].toLowerCase().endsWith("checker")) { //if (spUser.Roles[i].Name.toLowerCase().startsWith("dbs tmo") && spUser.Roles[i].Name.toLowerCase().endsWith("checker")) {
                                    viewModel.IsTMOChecker(true);
                                    return;
                                }
                            }
                        }
                    }
                    viewModel.isTMOChecker(false);
                    viewModel.GetTransactionData();
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
    // get Transaction data
    function GetTransactionData(instanceId, approverId) {
        var endPointURL;

        switch (self.ActivityTitle()) {
            case DBS.AcivityTitle.TMOMakerTask:
                endPointURL = api.server + api.url.workflow.tmoMaker(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case DBS.AcivityTitle.TMOCheckerTask:
                endPointURL = api.server + api.url.workflow.tmoChecker(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case DBS.AcivityTitle.TMOCheckerCancellationTask:
                endPointURL = api.server + api.url.workflow.tmoChecker(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerTask:
            case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
            case DBS.AcivityTitle.FXTMOCheckerRequestCancelTask:
                endPointURL = api.server + api.url.workflow.trackingTmoChecker(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                endPointURL = api.server + api.url.workflow.trackingTmoMaker(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
                endPointURL = api.server + api.url.workflow.trackingTmoMaker(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case DBS.AcivityTitle.FXDealTMOMakerNettingTask:
                endPointURL = api.server + api.url.workflow.TMONettingDetail(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerNettingTask:
                endPointURL = api.server + api.url.workflow.TMONettingDetail(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case DBS.AcivityTitle.FXDealTMOMakerNettingCancellationTask:
                endPointURL = api.server + api.url.workflow.TMONettingDetail(instanceId, approverId);
                self.IsTimelines(true);
                break;

        }

        // declare options variable for ajax get request
        var options = {
            url: endPointURL,
            token: accessToken
        };

        // show progress bar
        $("#transaction-progress").show();

        // hide transaction data
        $("#transaction-data").hide();

        // call ajax
        Helper.Ajax.Get(options, OnSuccessGetTransactionData, OnError, function () {
            // hide progress bar
            $("#transaction-progress").hide();
            // show transaction data
            $("#transaction-data").show();
        });
    }
    // upload underlying file
    function UploadFileUnderlying(context, document, callBack) {

        // Define the folder path for this example.
        var serverRelativeUrlToFolder = '/Underlying Documents';


        // Get the file name from the file input control on the page.
        if (document.DocumentPath.name != undefined) {
            var parts = document.DocumentPath.name.split('.');
        } else {
            //self.DocumentPath_a('');
            //self.IsEditable(true);
            ShowNotification("", "Please select a file", 'gritter-warning', true);
        }

        var fileExtension = parts[parts.length - 1];
        var timeStamp = new Date().getTime();
        var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

        // Get the server URL.
        var serverUrl = _spPageContextInfo.webAbsoluteUrl;

        // output variable
        var output;

        // Initiate method calls using jQuery promises.
        // Get the local file as an array buffer.
        var getFile = getFileBufferUnderlying();
        getFile.done(function (arrayBuffer) {

            // Add the file to the SharePoint folder.
            var addFile = addFileToFolderUnderlying(arrayBuffer);
            addFile.done(function (file, status, xhr) {
                output = file.d;

                // Get the list item that corresponds to the uploaded file.
                var getItem = getListItemUnderlying(file.d.ListItemAllFields.__deferred.uri);
                getItem.done(function (listItem, status, xhr) {
                    //return output;
                    var documentData = {
                        ID: 0,
                        Type: document.Type,
                        Purpose: document.Purpose,
                        LastModifiedDate: null,
                        LastModifiedBy: null,
                        DocumentPath: output.ServerRelativeUrl,
                        FileName: document.DocumentPath.name,
                        IsDormant: document.IsNewDocument
                    };
                    //self.TrackingTMOMaker().Transaction.Documents.push(documentData);
                    // Change the display name and title of the list item.
                    var changeItem = updateListItemUnderlying(listItem.d.__metadata);
                    changeItem.done(function (data, status, xhr) {
                        DocumentModels({ "FileName": document.DocumentPath.name, "DocumentPath": output.ServerRelativeUrl });
                        callBack();
                    });
                    changeItem.fail(OnError);
                });
                getItem.fail(OnError);
            });
            addFile.fail(OnError);
        });
        getFile.fail(OnError);

        // Get the local file as an array buffer.
        function getFileBufferUnderlying() {
            var deferred = $.Deferred();
            var reader = new FileReader();
            reader.onloadend = function (e) {
                deferred.resolve(e.target.result);
            }
            reader.onerror = function (e) {
                deferred.reject(e.target.error);
            }
            //reader.readAsArrayBuffer(fileInput[0].files[0]);
            //reader.readAsDataURL(document.DocumentPath());
            reader.readAsArrayBuffer(document.DocumentPath);
            return deferred.promise();
        }

        // Add the file to the file collection in the Shared Documents folder.
        function addFileToFolderUnderlying(arrayBuffer) {

            // Get the file name from the file input control on the page.
            //var parts = fileInput[0].value.split('\\');
            //var fileName = parts[parts.length - 1];

            // Construct the endpoint.
            var fileCollectionEndpoint = String.format(
                    "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                    "/add(overwrite=false, url='{2}')",
                serverUrl, serverRelativeUrlToFolder, fileName);
            UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval);
            // Send the request and return the response.
            // This call returns the SharePoint file.
            return $.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val()
                    //"content-length": arrayBuffer.byteLength
                }
            });
        }

        // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
        function getListItemUnderlying(fileListItemUri) {

            // Send the request and return the response.
            return $.ajax({
                url: fileListItemUri,
                type: "GET",
                headers: { "accept": "application/json;odata=verbose" }
            });
        }

        // Change the display name and title of the list item.
        function updateListItemUnderlying(itemMetadata) {

            var body = {
                Title: document.DocumentPath.name,
                Application_x0020_ID: context.ApplicationID,
                CIF: context.CIF,
                Customer_x0020_Name: context.Name,
                Document_x0020_Type: context.Type.DocTypeName,
                Document_x0020_Purpose: context.Purpose.Name,
                __metadata: {
                    type: itemMetadata.type,
                    FileLeafRef: fileName,
                    Title: fileName
                }
            };
            UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval);
            // Send the request and return the promise.
            // This call does not return response content from the server.
            return $.ajax({
                url: itemMetadata.uri,
                type: "POST",
                data: ko.toJSON(body),
                headers: {
                    "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                    "content-type": "application/json;odata=verbose",
                    //"content-length": body.length,
                    "IF-MATCH": itemMetadata.etag,
                    "X-HTTP-Method": "MERGE"
                }
            });
        }
    }

    function GetTransactionDataHome(activitytitle, instanceId, approverId, error, always) {
        var endPointURL;

        switch (activitytitle) {
            case DBS.AcivityTitle.TMOMakerTask:
                endPointURL = api.server + api.url.workflow.tmoMaker(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case DBS.AcivityTitle.TMOCheckerTask:
                endPointURL = api.server + api.url.workflow.tmoChecker(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case DBS.AcivityTitle.TMOCheckerCancellationTask:
                endPointURL = api.server + api.url.workflow.tmoChecker(instanceId, approverId);
                self.IsTimelines(true);
                break;
        }

        // declare options variable for ajax get request
        var options = {
            url: endPointURL,
            token: accessToken
        };
        // call ajax
        //Helper.Ajax.Get(options, OnSuccessGetTransactinDataHome, DoNothing, OnAlways);
        var isValid = true;
        var Url = options.url;
        var method = "GET";
        // validate & decode params
        if (options.params != null || options.params != undefined) {
            Url += DecodeParams(options.params);
        }

        // declare ajax options
        var ajaxOptions = {
            async: false,
            type: method,
            url: Url,
            contentType: "application/json; charset=utf-8", //; odata=verbose;
            dataType: "json",
            //data: ko.toJSON(options.data),
            headers: {
                "Authorization": "Bearer " + options.token
            },
            success: function (data, textStatus, jqXHR) {
                OnSuccessGetTransactionDataHome(data, textStatus, jqXHR, activitytitle);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                error(jqXHR, textStatus, errorThrown);
            }
        };

        // set body request
        if (method != "GET") {
            // validate request body on POST
            if (options.data == null || options.data == undefined) {
                alert(method + " method does not contains request body");

                isValid = false;
            } else {
                ajaxOptions.data = options.data;
            }
        }

        // execute ajax request
        if (isValid) {
            $.ajax(ajaxOptions).always(function () {
                if (always != null || always != undefined)
                    always();
            });
        } else {
            if (always != null || always != undefined)
                always();
        }

    }
    function OnSuccessGetTransactionData(data, textStatus, jqXHR) {

        if (jqXHR.status = 200) {
            var mapping = {};
            switch (self.ActivityTitle()) {
                case DBS.AcivityTitle.TMOMakerTask:
                    GetInitiatorRole(viewModel.Initiator());//15-2-2016 dani
                    self.TMOMaker(ko.mapping.toJS(data, mapping));
                    break;
                case DBS.AcivityTitle.TMOCheckerTask:
                    GetInitiatorRole(viewModel.Initiator());//15-2-2016 dani
                    self.TMOChecker(ko.mapping.toJS(data, mapping));
                    if (!self.IsAllChecked()) {
                        self.CheckAll(false);
                    } else {
                        self.CheckAll(true);
                    }
                    break;
                case DBS.AcivityTitle.TMOCheckerCancellationTask:
                    self.TMOChecker(ko.mapping.toJS(data, mapping));
                    break;
                case DBS.AcivityTitle.FXDealTMOCheckerTask:
                case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
                case DBS.AcivityTitle.FXTMOCheckerRequestCancelTask:
                    self.isNewUnderlying(false);
                    if (data != null && data.Transaction.UnderlyingCodeID == 37) {
                        self.isNewUnderlying(true);
                    }
                    self.CheckAll(false);
                    data.Transaction.UnderlyingCodeID = data.Transaction.UnderlyingCodeID;
                    self.TrackingTMOChecker(ko.mapping.fromJS(data, mapping));

                    if (data.Transaction.DocumentTBO.length > 0) {
                        self.IsToBeObtained(true);
                        self.ToBeObtained(true);
                    } else {
                        self.IsToBeObtained(false);
                        self.ToBeObtained(false);
                    }

                    if (data.Transaction.IsOtherAccountNumber) {
                        data.Transaction.Account.AccountNumber = data.Transaction.OtherAccountNumber;
                    }
                    if (!self.IsAllChecked()) {
                        self.CheckAll(false);
                    } else {
                        self.CheckAll(true);
                    }
                    SetFxStatus();
                    GetModelTransaction();
                    GetTotalTransaction();
                    self.IsSwapTransaction(false);
                    self.IsNettingTransaction(false);
                    self.Swap_NettingDFNumber('')
                    if (data.Transaction.SwapType != null && data.Transaction.SwaptType != 0) {
                        SetSwapTypeTransaction();
                    }
                    if (data.Transaction.IsNettingTransaction != null && data.Transaction.IsNettingTransaction == true) {
                        SetNettingTypeTransaction();
                    }
                    //if (data.Transaction.ReviseDealDocuments != null) {
                    //    ko.utils.arrayForEach(data.Transaction.ReviseDealDocuments, function (item) {
                    //        item.IsNewDocument = false;
                    //        self.MakerAllDocumentsTemp.push(item);
                    //        ko.utils.arrayForEach(data.Transaction.Customer.Underlyings, function (item2) {
                    //            if (item2.ID == item.UnderlyingID) {
                    //                self.MakerDocuments.push(item);
                    //                self.TrackingTMOChecker().Transaction.Documents.push(item);
                    //            }
                    //        });
                    //    });
                    //}
                    break;
                case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
                    self.IsUnderlyingCompletedEmpty(!data.Transaction.IsUnderlyingCompleted);
                    self.IsSubmissionDateSLEmpty(data.Transaction.SubmissionDateSL == null || data.Transaction.SubmissionDateSL == '');
                    self.utilizationAmount('0.00');
                    //self.GetDataUnderlying();                    
                case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
                case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                    self.isNewUnderlying(false);
                    if (data != null && data.Transaction.UnderlyingCodeID == 37) {
                        self.isNewUnderlying(true);
                    }
                    self.UnderlyingFilterCurrency(data.Transaction.BuyCurrency.Code);
                    self.TransactionDealID(data.Transaction.ID);
                    cifData = data.Transaction.Customer.CIF;
                    customerNameData = data.Transaction.Customer.Name;
                    data.Transaction.SubmissionDateSL = viewModel.LocalDate(data.Transaction.SubmissionDateSL, true, false)
                    data.Transaction.SubmissionDateInstruction = viewModel.LocalDate(data.Transaction.SubmissionDateInstruction, true, false)
                    data.Transaction.SubmissionDateUnderlying = viewModel.LocalDate(data.Transaction.SubmissionDateUnderlying, true, false)
                    self.TrackingTMOMaker(ko.mapping.fromJS(data, mapping));
                    //#region jointAccount
                    self.IsJointAccount(true);
                    var dataAccount = ko.utils.arrayFilter(self.TrackingTMOMaker().Transaction.Customer.Accounts(), function (item) {
                        var sAccount = item.IsJointAccount() == null ? false : item.IsJointAccount();
                        var debitCurrency = self.TrackingTMOMaker().Transaction.DebitCurrency != null ? self.TrackingTMOMaker().Transaction.DebitCurrency.Code() : "";
                        return sAccount == data.Transaction.IsJointAccount && debitCurrency == item.Currency.Code();
                    });
                    if (!data.Transaction.IsJointAccount) {
                        dataAccount = AddOtherAccounts(dataAccount);
                    }
                    self.DynamicAccounts(dataAccount);

                    //check customer have joint account
                    var isJointAcc = ko.utils.arrayFilter(data.Transaction.Customer.Accounts, function (item) {
                        return true == item.IsJointAccount;
                    });
                    if (isJointAcc != null & isJointAcc.length == 0) {
                        self.IsJointAccount(false);
                    }
                    if (data.Transaction.Account != null) {
                        if (!data.Transaction.IsOtherAccountNumber) {
                            self.Selected().Account(data.Transaction.Account.AccountNumber);
                            self.IsEmptyAccountNumber(false);
                            if (data.Transaction.DebitCurrency != null) {
                                self.TrackingTMOMaker().Transaction.Account.Currency.Code(data.Transaction.DebitCurrency.Code);
                                //self.TransactionMaker().Transaction.Account(tempdata.Transaction.Account);
                            }
                            if (data.Transaction.Account != null) {
                                self.Selected().Account(data.Transaction.Account.AccountNumber);
                            }
                        }
                        else {
                            self.Selected().Account('-');
                            self.IsEmptyAccountNumber(true);
                            if (data.Transaction.DebitCurrency != null) {
                                self.TrackingTMOMaker().Transaction.DebitCurrency.Code(data.Transaction.DebitCurrency.Code);
                                self.Selected().DebitCurrency(data.Transaction.DebitCurrency.ID);
                            }
                            self.TrackingTMOMaker().Transaction.OtherAccountNumber(data.Transaction.OtherAccountNumber);
                        }
                    }
                    //#endregion


                    // chandra 2015.05.03 : set underlying document
                    self.MakerUnderlyings([]);
                    self.CustomerUnderlyings([]);
                    self.TempSelectedUnderlying([]);
                    if (data.Transaction.Customer.Underlyings != null) {
                        ko.utils.arrayForEach(data.Transaction.Customer.Underlyings, function (item) {
                            if (item.IsEnable == true) {
                                self.MakerUnderlyings.push(item.ID);
                                self.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, false, item.UtilizeAmountDeal, item.StatementLetter.ID));
                            }
                        });
                        setTotalUtilize();
                    }
                    // chandra 2015.05.03 : set attachment document
                    self.MakerDocuments([]);
                    self.MakerAllDocumentsTemp([]);
                    self.TrackingTMOMaker().Transaction.Documents([]);
                    if (data.Transaction.ReviseDealDocuments != null) {
                        ko.utils.arrayForEach(data.Transaction.ReviseDealDocuments, function (item) {
                            item.IsNewDocument = false;
                            self.MakerAllDocumentsTemp.push(item);
                            ko.utils.arrayForEach(data.Transaction.Customer.Underlyings, function (item2) {
                                if (item2.ID == item.UnderlyingID) {
                                    self.MakerDocuments.push(item);
                                    self.TrackingTMOMaker().Transaction.Documents.push(item);
                                }
                            });
                            //if (item.Purpose.ID != 2) {
                            //    self.MakerDocuments.push(item);
                            //    self.TrackingTMOMaker().Transaction.Documents.push(item);
                            //}
                        });
                    }
                    if (data.Transaction.Documents != null) {
                        ko.utils.arrayForEach(data.Transaction.Documents, function (item) {
                            item.IsNewDocument = false;
                            if (item.Purpose.ID != 2) {
                                self.MakerAllDocumentsTemp.push(item);
                                self.MakerDocuments.push(item);
                                self.TrackingTMOMaker().Transaction.Documents.push(item);
                            }
                        });
                    }
                    self.GetDataUnderlying();
                    SetFxStatus();
                    GetTotalTransaction();
                    self.IsSwapTransaction(false);
                    self.IsNettingTransaction(false);
                    self.Swap_NettingDFNumber('')
                    if (data.Transaction.SwapType != null && data.Transaction.SwaptType != 0) {
                        SetSwapTypeTransaction();
                    }
                    if (data.Transaction.IsNettingTransaction != null && data.Transaction.IsNettingTransaction == true) {
                        SetNettingTypeTransaction();
                    }
                    $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                        $(this).prev().focus();
                    });
                    break;
                case DBS.AcivityTitle.FXDealTMOMakerNettingTask:
                    if (data.Transaction.Customer.Underlyings == null) {
                        data.Transaction.Customer.Underlyings = [];
                    }
                    GetNettingPurpose();
                    self.Selected().NettingPurpose(data.Transaction.NettingPurpose.ID);
                    self.TransactionMakerNetting(ko.mapping.toJS(data, mapping));
                    self.TransactionMakerNetting().Transaction.ActualDateStatementLetter = self.LocalDate(data.Transaction.ActualDateStatementLetter, true, false);
                    self.TransactionMakerNetting().Transaction.ActualDateUnderlying = self.LocalDate(data.Transaction.ActualDateUnderlying, true, false);
                    self.TransactionMakerNetting().Transaction.ExpectedDateStatementLetter = self.LocalDate(data.Transaction.ExpectedDateStatementLetter, true, false);
                    self.TransactionMakerNetting().Transaction.ExpectedDateUnderlying = self.LocalDate(data.Transaction.ExpectedDateUnderlying, true, false);
                    self.TransactionMakerNetting().Transaction.Customer.Underlyings = [];
                    self.TransactionMakerNetting().Transaction.Documents = [];

                    if (data.Transaction.Customer.Underlyings != null) {
                        $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                            $(this).prev().focus();
                        });

                        ko.utils.arrayForEach(data.Transaction.Customer.Underlyings, function (item) {
                            self.TransactionMakerNetting().Transaction.Customer.Underlyings.push(item);
                        });

                    }
                    self.MakerDocuments([]);
                    if (data.Transaction.Documents != null) {
                        ko.utils.arrayForEach(data.Transaction.Documents, function (item) {
                            self.TransactionMakerNetting().Transaction.Documents.push(item);
                            self.MakerDocuments.push(item);
                        });
                    }
                    cifData = data.Transaction.Customer.CIF;
                    self.GetDataUnderlying();
                    self.GetDataAttachFile();
                    //var t_IsFxTransaction = (data.Transaction.Currency.Code != 'IDR' && data.Transaction.AccountCurrencyCode == 'IDR');
                    //var t_IsFxTransactionToIDR = (data.Transaction.Currency.Code == 'IDR' && data.Transaction.AccountCurrencyCode != 'IDR');

                    //self.t_IsFxTransaction(t_IsFxTransaction);
                    //self.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);

                    //if (self.t_IsFxTransaction() || self.t_IsFxTransactionToIDR()) {
                    //    console.log(data);
                    //    GetTotalTransactionNetting(data);// For Get Total IDR - FCY & Utilization
                    //}

                    break;
                case DBS.AcivityTitle.FXDealTMOCheckerNettingTask:
                    if (data.Transaction.Customer.Underlyings == null) {
                        data.Transaction.Customer.Underlyings = [];
                    }
                    GetNettingPurpose();
                    self.Selected().NettingPurpose(data.Transaction.NettingPurpose.ID);
                    self.TransactionCheckerNetting(ko.mapping.toJS(data, mapping));
                    self.TransactionCheckerNetting().Transaction.Customer.Underlyings = [];
                    self.TransactionCheckerNetting().Transaction.Documents = [];
                    if (data.Transaction.Customer.Underlyings != null) {
                        $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                            $(this).prev().focus();
                        });

                        ko.utils.arrayForEach(data.Transaction.Customer.Underlyings, function (item) {
                            self.TransactionCheckerNetting().Transaction.Customer.Underlyings.push(item);
                        });

                    }


                    if (data.Transaction.Documents != null) {
                        ko.utils.arrayForEach(data.Transaction.Documents, function (item) {
                            self.TransactionCheckerNetting().Transaction.Documents.push(item);
                        });
                    }


                    //var t_IsFxTransaction = (data.Transaction.Currency.Code != 'IDR' && data.Transaction.AccountCurrencyCode == 'IDR');
                    //var t_IsFxTransactionToIDR = (data.Transaction.Currency.Code == 'IDR' && data.Transaction.AccountCurrencyCode != 'IDR');

                    //self.t_IsFxTransaction(t_IsFxTransaction);
                    //self.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);

                    //if (self.t_IsFxTransaction() || self.t_IsFxTransactionToIDR()) {
                    //    console.log(data);
                    //    GetTotalTransactionNetting(data);// For Get Total IDR - FCY & Utilization
                    //}

                    if (!self.IsAllChecked()) {
                        self.CheckAll(false);
                    } else {
                        self.CheckAll(true);
                    }
                    break;
                case DBS.AcivityTitle.FXDealTMOMakerNettingCancellationTask:
                    if (data.Transaction.Customer.Underlyings == null) {
                        data.Transaction.Customer.Underlyings = [];
                    }
                    GetNettingPurpose();
                    self.Selected().NettingPurpose(data.Transaction.NettingPurpose.ID);
                    self.TransactionCheckerNetting(ko.mapping.toJS(data, mapping));
                    self.TransactionCheckerNetting().Transaction.Customer.Underlyings = [];
                    self.TransactionCheckerNetting().Transaction.Documents = [];
                    if (data.Transaction.Customer.Underlyings != null) {
                        $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                            $(this).prev().focus();
                        });

                        ko.utils.arrayForEach(data.Transaction.Customer.Underlyings, function (item) {
                            self.TransactionCheckerNetting().Transaction.Customer.Underlyings.push(item);
                        });

                    }


                    if (data.Transaction.Documents != null) {
                        ko.utils.arrayForEach(data.Transaction.Documents, function (item) {
                            self.TransactionCheckerNetting().Transaction.Documents.push(item);
                        });
                    }


                    //var t_IsFxTransaction = (data.Transaction.Currency.Code != 'IDR' && data.Transaction.AccountCurrencyCode == 'IDR');
                    //var t_IsFxTransactionToIDR = (data.Transaction.Currency.Code == 'IDR' && data.Transaction.AccountCurrencyCode != 'IDR');

                    //self.t_IsFxTransaction(t_IsFxTransaction);
                    //self.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);

                    //if (self.t_IsFxTransaction() || self.t_IsFxTransactionToIDR()) {
                    //    console.log(data);
                    //    GetTotalTransactionNetting(data);// For Get Total IDR - FCY & Utilization
                    //}

                    break;

            }

            // disable form if this task is not authorized
            if (self.IsPendingNintex()) {
                if (!self.IsAuthorizedNintex()) {
                    DisableForm();
                }
            } else {
                DisableForm();
            }

        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }
    self.VerifyColumn = function (colName) {
        var approvalData = ko.isObservable(self.ApprovalData().Verify) ? self.ApprovalData().Verify() : self.ApprovalData().Verify;
        var data = ko.utils.arrayFirst(approvalData, function (item) {
            var itemName;
            if (ko.isObservable(item.Name)) {
                itemName = item.Name();
            } else {
                itemName = item.Name;
            }
            return itemName == colName;
        });
        return ko.observable(data);
    };

    self.CheckAllData = function (form) {
        // re-updating observable. This method will updating the UI (view)
        var data = null;
        var isVerified = document.getElementById('check-all').checked;
        self.CheckAll(isVerified);

        switch (form) {
            case "TMOChecker":
                data = self.TMOChecker().Verify;
                break;
            case "TrackingTMOChecker":
                data = self.TrackingTMOChecker().Verify();
                break;
            case "TransactionCheckerNetting":
                data = self.TransactionCheckerNetting().Verify;
                break;

        }

        for (i = 0; i <= data.length - 1; i++) {
            switch (form) {
                case "TMOChecker":
                    data[i].IsVerified = isVerified;
                    break;
                case "TrackingTMOChecker":
                    data[i].IsVerified(isVerified);
                    break;
                case "TransactionCheckerNetting":
                    data[i].IsVerified = isVerified;
                    break;

            }
        }
        self.UpdateTemplateUI(form);
    }
    self.UpdateTemplateUI = function (form) {
        // re-updating observable. This method will updating the UI (view)
        switch (form.toLowerCase()) {
            //basri 16.10.2015          
            case "tmochecker":
                var update = self.TMOChecker();
                self.TMOChecker(ko.mapping.toJS(update));
                break; //end basri
            case "trackingtmochecker":
                var update = self.TrackingTMOChecker();
                self.TrackingTMOChecker(ko.mapping.fromJS(update));
                break;
            case "transactioncheckernetting":
                var update = self.TransactionCheckerNetting();
                self.TransactionCheckerNetting(ko.mapping.toJS(update));
                break;
        }

    };

    function OnSuccessGetTransactionDataHome(data, textStatus, jqXHR, activitytitle) {
        if (jqXHR.status = 200) {
            var mapping = {};

            switch (activitytitle) {
                case "TMO Maker Task":
                    self.IsCanEditUnderlying(true);
                    self.TMOMaker(ko.mapping.toJS(data, mapping));
                    break;
                case "TMO Checker Task":
                    self.IsCanEditUnderlying(false);
                    self.TMOChecker(ko.mapping.toJS(data, mapping));
                    break;
                case "TMO Checker Cancellation Task":
                    self.IsCanEditUnderlying(false);
                    self.TMOChecker(ko.mapping.toJS(data, mapping));
                    break;
            }

        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    function GetModelTransaction() {
        var model;
        switch (self.ActivityTitle()) {
            case DBS.AcivityTitle.FXDealTMOCheckerTask:
            case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
            case DBS.AcivityTitle.FXTMOCheckerRequestCancelTask:
                model = self.TrackingTMOChecker();
                break;
            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                model = self.TrackingTMOMaker();
                break;
            case DBS.AcivityTitle.TMOCheckerCancellationTask:
                model = self.TrackingTMOChecker();
                break;
            case DBS.AcivityTitle.FXDealTMOMakerNettingTask:
                model = self.TransactionMakerNetting();
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerNettingTask:
                model = self.TransactionCheckerNetting();
                break;
            case DBS.AcivityTitle.FXDealTMOMakerNettingCancellationTask:
                model = self.TransactionCheckerNetting();
                break;
            default:
                break;
        }
        return model;
    }

    function SaveApprovalData(instanceId, approverId, outcomeId) {
        var endPointURL;
        var body;

        switch (self.ActivityTitle()) {

            case "TMO Maker Task":
                endPointURL = api.server + api.url.workflow.tmoMaker(instanceId, approverId);
                body = ko.toJS(self.TMOMaker().Transaction);
                break;
            case "TMO Checker Task":
                endPointURL = api.server + api.url.workflow.tmoChecker(instanceId, approverId);
                body = ko.toJS(self.TMOChecker().Transaction);
                break;
            case "TMO Checker Cancellation Task":
                endPointURL = api.server + api.url.workflow.tmoChecker(instanceId, approverId);
                body = ko.toJS(self.TMOChecker().Transaction);
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerTask:
            case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
                endPointURL = api.server + api.url.workflow.tmoCheckerSubmit(instanceId, approverId);
                body = ko.toJS(self.TrackingTMOChecker());
                break;
            case DBS.AcivityTitle.FXTMOCheckerRequestCancelTask:
                endPointURL = api.server + api.url.workflow.tmoCheckerCancel(instanceId);
                body = ko.toJS(self.TrackingTMOChecker());
                break;
            case DBS.AcivityTitle.FXDealTMOMakerNettingTask:
                endPointURL = api.server + api.url.workflow.SubmitNetting(instanceId, approverId);
                body = ko.toJS(self.TransactionMakerNetting().Transaction);
                break;
            case DBS.AcivityTitle.FXDealTMOCheckerNettingTask:
                endPointURL = api.server + api.url.workflow.transactionCheckerNettingApproveOrRevise(instanceId, approverId, outcomeId);
                body = ko.toJS(self.TransactionCheckerNetting());
                break;
        }
        if (endPointURL == null) {
            CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
        } else {
            // declare options variable for ajax get request
            var options = {
                url: endPointURL,
                token: accessToken,
                data: ko.toJSON(body)
            };

            //Helper.Ajax.Post(options, OnSuccessSaveApprovalData(outcomeId), OnError, OnAlways);
            Helper.Ajax.Post(options, function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
                }
            }, OnError, OnAlways);
        }
    }
    function SaveApprovalDataHome(sptasklistid, sptasklistitemid, tasktypeid, activitytitle, instanceId, approverId, outcomeId) {
        var endPointURL;
        var body;

        switch (activitytitle) {
            case "TMO Maker Task":
                endPointURL = api.server + api.url.workflow.tmoMaker(instanceId, approverId);
                body = ko.toJS(self.TMOMaker().Transaction);
                break;
            case "TMO Checker Task":
                endPointURL = api.server + api.url.workflow.tmoChecker(instanceId, approverId);
                body = ko.toJS(self.TMOChecker().Transaction);
                break;
            case "TMO Checker Cancellation Task":
                endPointURL = api.server + api.url.workflow.tmoChecker(instanceId, approverId);
                body = ko.toJS(self.TMOChecker().Transaction);
                break;
        }

        // declare options variable for ajax get request
        var options = {
            url: endPointURL,
            token: accessToken,
            data: ko.toJSON(body)
        };
        //console.log(sptasklistid + ' ' + sptasklistitemid + ' ' + tasktypeid + ' ' + activitytitle + ' ' + instanceId + ' ' + approverId + ' ' + outcomeId);
        //Helper.Ajax.Post(options, OnSuccessSaveApprovalData(outcomeId), OnError, OnAlways);
        console.log(options.data);
        Helper.Ajax.Post(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                CompletingTaskHome(sptasklistid, parseInt(sptasklistitemid), parseInt(tasktypeid), parseInt(outcomeId), '');
            }
        }, OnError, OnAlways);
    }

    // workflow approval process & get selected outcome to send into custom Nintex REST Api
    self.ApprovalProcess = function (data) {
        var text = String.format("{0}<br/><br/>Do you want to <b>{1}</b> this task?", data.Description, data.Name);
        var outcomes = config.nintex.outcome.uncompleted;
        if (self.ActivityTitle() == DBS.AcivityTitle.FXDealTMOCheckerTask || self.ActivityTitle() == DBS.AcivityTitle.FXDealTMOCheckerReopenTask) {
            if (data.Name == 'Complete') {
                if (self.ActivityTitle() != DBS.AcivityTitle.FXDealTMOCheckerReopenTask) {
                    if (!self.IsAllChecked()) {
                        ShowNotification("Task Validation Warning", String.format("Please tick all checkbox to {0} this task.", data.Name), "gritter-warning", false);
                        return;
                    }
                    //if (viewModel.TrackingTMOChecker().Transaction.SubmissionDateSL() == null || viewModel.TrackingTMOChecker().Transaction.SubmissionDateUnderlying() == null || self.IsNoThresholdValue()) {
                    //    ShowNotification("Task Validation Warning", String.format("To complete this transaction, Please complete a statement letter and underlying.", data.Name), "gritter-warning", false);
                    //    return;
                    //}
                    //if (self.TrackingTMOChecker().Transaction.SubmissionDateSL() == null || self.TrackingTMOChecker().Transaction.IsUnderlyingCompleted() != true || self.IsNoThresholdValue()) {
                    //    ShowNotification("Task Validation Warning", String.format("To complete this transaction, Please complete a statement letter and underlying document.", data.Name), "gritter-warning", false);
                    //    return;
                    //}
                    if (viewModel.TrackingTMOChecker().Transaction.SubmissionDateSL() == null || viewModel.TrackingTMOChecker().Transaction.SubmissionDateSL() == null || viewModel.TrackingTMOChecker().Transaction.SubmissionDateSL() == null) {
                        text = String.format("{0}<br/><br/>To complete this transaction, Please complete a statement letter, instruction and underlying document", data.Description);
                    }
                }
                bootbox.confirm(text, function (result) {
                    if (result) { CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments()); }
                });
            } else if (data.Name == 'Revise') {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        //if (self.ActivityTitle() == DBS.AcivityTitle.FXDealTMOCheckerReopenTask) {
                        //    CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                        //} else {
                        UpdateDataTMO(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                        //}
                        return;
                    }
                });
            } else if (data.Name == 'Submit to Maker') {
                if (!self.IsAllChecked()) {
                    ShowNotification("Task Validation Warning", String.format("Please tick all checkbox to {0} this task.", data.Name), "gritter-warning", false);
                    return;
                }
                bootbox.confirm(text, function (result) {
                    viewModel.taskName(data.Name);
                    if (result) { SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID); }//CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments()); }
                });
            }
        }
        else if (self.ActivityTitle() == DBS.AcivityTitle.FXTMOMakerAfterReviseTask || self.ActivityTitle() == DBS.AcivityTitle.FXTMOMakerAfterReopenTask) {
            if (data.Name == 'Submit') {
                var param = {
                    WFID: self.WorkflowInstanceID(),
                    ApproverID: self.ApproverId(),
                    DataID: data.ID
                }
                viewModel.TrackingTMOMaker().Transaction.Underlyings(self.TempSelectedUnderlying());
                GetThreshold(UpdateDataTMO, param, text, UploadDocuments);
                return;
            }
            if (data.Name == "Cancel") {
                bootbox.confirm(text, function (result) {
                    if (result) { CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments()); }
                });
            }
        }
        else if (self.ActivityTitle() == DBS.AcivityTitle.FXTMOMakerAfterSubmitTask) {
            if (data.Name == 'Submit') {
                if (viewModel.IsNoThresholdValue()) {
                    $('#ActualSubmissionDateSL').data("ruleRequired", false)
                }
                var param = {
                    WFID: self.WorkflowInstanceID(),
                    ApproverID: self.ApproverId(),
                    DataID: data.ID
                }
                var form = $("#aspnetForm");
                form.validate();
                if (form.valid() || viewModel.IsNoThresholdValue()) {
                    viewModel.TrackingTMOMaker().Transaction.Underlyings(self.TempSelectedUnderlying());
                    GetThreshold(UpdateDataTMO, param, text, UploadDocuments);
                } else {
                    ShowNotification("Form Validation Warning", 'Mandatory field must be filled.', 'gritter-warning', false);
                    return;
                }

            }
            if (data.Name == "Cancel") {
                bootbox.confirm(text, function (result) {
                    if (result) { CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments()); }
                });
            }
        }
        else if (self.ActivityTitle() == DBS.AcivityTitle.FXTMOCheckerRequestCancelTask) {
            if (data.Name == "Approve Cancellation") {
                bootbox.confirm(text, function (result) {
                    viewModel.taskName(data.Name);
                    if (result) { SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID); }// CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments()); }
                });
            }
            if (data.Name == "Revise") {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        UpdateDataTMO(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                        return;
                    }
                });
            }
        }
        else if (self.ActivityTitle() == DBS.AcivityTitle.FXDealTMOMakerNettingTask) {
            if (data.Name == 'Cancel') {
                var form = $("#aspnetForm");
                bootbox.confirm(text, function (result) {
                    if (result) {
                        CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                        return;
                    }
                });
            }
            else if (data.Name == 'Submit') {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        viewModel.TransactionMakerNetting().Transaction.Underlyings = self.TempSelectedUnderlying();

                        var param = {
                            WFID: self.WorkflowInstanceID(),
                            ApproverID: self.ApproverId(),
                            DataID: data.ID
                        }
                        ApproveData(SaveApprovalData, param, text, UploadDocuments);
                        return;
                    }
                });
            }

        }
        else if (self.ActivityTitle() == DBS.AcivityTitle.FXDealTMOCheckerNettingTask) {
            if (data.Name == 'Revise') {
                var form = $("#aspnetForm");
                bootbox.confirm(text, function (result) {
                    if (result) {
                        SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                        return;
                    }
                });
            }
            else if (data.Name == 'Approve') {
                if (!self.IsAllChecked()) {
                    ShowNotification("Task Validation Warning", String.format("Please tick all checkbox to {0} this task.", data.Name), "gritter-warning", false);
                }
                else {
                    bootbox.confirm(text, function (result) {
                        if (result) {
                            SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                            return;
                        }
                    });
                }
            }

        }
        else if (self.ActivityTitle() == DBS.AcivityTitle.FXDealTMOMakerNettingCancellationTask) {
            if (data.Name == 'Revise') {
                var form = $("#aspnetForm");
                bootbox.confirm(text, function (result) {
                    if (result) {
                        CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                        return;
                    }
                });
            }
            else if (data.Name == 'Approve Cancellation') {

                bootbox.confirm(text, function (result) {
                    if (result) {
                        CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                        return;
                    }
                });

            }

        }
        else if (self.ActivityTitle() == DBS.AcivityTitle.TMOCSOTask) {
            if (data.Name == 'Cancel') {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                        return;
                    }
                });
            } else if (data.Name == 'Submit Revise') {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                        return;
                    }
                });
            }
        }
        else if (self.ActivityTitle() == DBS.AcivityTitle.TMOPPUMakerTask) {
            if (data.Name == 'Cancel') {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                        return;
                    }
                });
            } else if (data.Name == 'Submit Revise') {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                        return;
                    }
                });

            }
        }
        else if (self.ActivityTitle() == DBS.AcivityTitle.TMOPPUMakerAfterCheckerAndCallerTask) {
            if (data.Name == 'Cancel') {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                        return;
                    }
                });
            } else if (data.Name == 'Submit Revise') {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                        return;
                    }
                });

            }
        }
        else if (self.ActivityTitle() == DBS.AcivityTitle.TMOPPUCheckerTask) {
            if (data.Name == 'Approve') {
                if (!self.IsAllChecked()) {
                    ShowNotification("Task Validation Warning", String.format("Please tick all checkbox to {0} this task.", data.Name), "gritter-warning", false);
                } else {
                    bootbox.confirm(text, function (result) {
                        if (result) {
                            // call nintex api
                            SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                            return;
                        }
                    });
                }
            } else if (data.Name == 'Revise') {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                        return;
                    }
                });

            }
        }
        else if (self.ActivityTitle() == DBS.AcivityTitle.TMOPPUCheckerAfterPPUCallerTask) {
            if (data.Name == 'Approve') {
                if (!self.IsAllChecked()) {
                    ShowNotification("Task Validation Warning", String.format("Please tick all checkbox to {0} this task.", data.Name), "gritter-warning", false);
                } else {
                    bootbox.confirm(text, function (result) {
                        if (result) {
                            // call nintex api
                            SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                            return;
                        }
                    });
                }
            } else if (data.Name == 'Revise') {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                        return;
                    }
                });

            }

        }
        else if (self.ActivityTitle() == DBS.AcivityTitle.TMOPPUCallerTask) {
            if (self.TransactionCallbackTMO().Callbacks.length == 0) {
                ShowNotification("Task Validation Warning", String.format("Callback time does not exist.", data.Name), "gritter-warning", false);
                return;
            } else if (ko.toJS(self.TransactionCallbackTMO().Callbacks[self.TransactionCallbackTMO().Callbacks.length - 1].IsUTC) && self.TransactionCallbackTMO().Callbacks.length != self.UtcAttemp()) {
                ShowNotification("Task Validation Warning", String.format("Callback is UTC, this transaction cannot submitted", data.Name), "gritter-warning", false);
                return;
            }
            bootbox.confirm(text, function (result) {
                if (result) {
                    // call nintex api
                    SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                    return;
                }
            });


        }
        else if (self.ActivityTitle() == DBS.AcivityTitle.TMOCheckerCancellationTask) {
            if (data.Name == 'Approve Cancellation') {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                        return;
                    }
                });
            } else if (data.Name == 'Reject Cancellation') {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                        return;
                    }
                });

            }
        } else if (self.ActivityTitle() == DBS.AcivityTitle.TMOCheckerTask) {
            if (data.Name == 'Approve') {
                if (!self.IsAllChecked()) {
                    ShowNotification("Task Validation Warning", String.format("Please tick all checkbox to {0} this task.", data.Name), "gritter-warning", false);
                } else {
                    bootbox.confirm(text, function (result) {
                        if (result) {
                            // call nintex api
                            SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                            return;
                        }
                    });
                }
            } else if (data.Name = 'Revise to TMO Maker') {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                        return;
                    }
                });
            }
        } else if (self.ActivityTitle() == DBS.AcivityTitle.TMOMakerTask) {
            if (data.Name == 'Approve') {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                        return;
                    }
                });
            } else if (data.Name = 'Revise to PPU Maker') {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                        return;
                    }
                });
            } else if (data.Name = 'Revise to CSO') {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                        return;
                    }
                });
            } else if (data.Name = 'Cancel') {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                        return;
                    }
                });

            }

        }
        else {
            bootbox.confirm(text, function (result) {
                if (result) {
                    SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);
                }
            });

        }

    };
    // workflow approval process & get selected outcome to send into custom Nintex REST Api
    self.HomeApprovalProcess = function (data, event) {
        var Element = event.target;
        var Approval = {
            SPTaskListID: Element.getAttribute('SPTaskListID'),
            SPTaskListItemID: Element.getAttribute('SPTaskListItemID'),
            TaskTypeID: Element.getAttribute('TaskTypeID'),
            ActivityTitle: Element.getAttribute('ActivityTitle'),
            WorkflowInstanceID: Element.getAttribute('WorkflowInstanceID'),
            ApproverID: Element.getAttribute('ApproverID'),
            OutcomeID: Element.getAttribute('OutcomeID'),
        };
        GetTransactionDataHome(Approval.ActivityTitle, Approval.WorkflowInstanceID, Approval.ApproverID, DoNothing, OnAlways);

        // store data to db
        if ((Approval.ActivityTitle == "Branch Maker Reactivation Task") || (Approval.ActivityTitle == "CBO Maker Reactivation Task")) {
            var param = {
                WFID: self.WorkflowInstanceID(),
                ApproverID: self.ApproverId(),
                DataID: data.ID
            }
            UploadReactivationDoc(SaveApprovalDataHome, self.WorkflowInstanceID(), self.ApproverId(), data.ID, param);
        } else {
            if ((Approval.ActivityTitle == "PPU Maker Task" || Approval.ActivityTitle == "PPU Maker After Checker and Caller Task") && self.MakerDocuments().length > 0) {
                var param = {
                    WFID: self.WorkflowInstanceID(),
                    ApproverID: self.ApproverId(),
                    DataID: data.ID
                }
                UploadDocuments(SaveApprovalDataHome, param);
            } else {
                SaveApprovalDataHome(Approval.SPTaskListID, Approval.SPTaskListItemID, Approval.TaskTypeID, Approval.ActivityTitle, Approval.WorkflowInstanceID, Approval.ApproverID, Approval.OutcomeID);
            }
        }

    };

    function UpdateDataTMO(instanceId, approverId, outcomeId) {
        var endPointURL;
        var body;
        switch (self.ActivityTitle()) {
            case DBS.AcivityTitle.FXDealTMOCheckerTask:
            case DBS.AcivityTitle.FXDealTMOCheckerReopenTask:
            case DBS.AcivityTitle.FXTMOCheckerRequestCancelTask:
                endPointURL = api.server + api.url.workflow.trackingTmoChecker(instanceId, approverId);
                body = ko.toJS(self.TrackingTMOChecker());
                break;
            case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
                endPointURL = api.server + api.url.workflow.trackingTmoSubmitMaker(instanceId, approverId);
                body = ko.toJS(self.TrackingTMOMaker().Transaction);
                break;
            case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
                endPointURL = api.server + api.url.workflow.trackingTmoMaker(instanceId, approverId);
                body = ko.toJS(self.TrackingTMOMaker().Transaction);
                break;
            case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                endPointURL = api.server + api.url.workflow.trackingTmoReopenMaker(instanceId, approverId);
                body = ko.toJS(self.TrackingTMOMaker().Transaction);
                break;
        }
        if (endPointURL != null) {
            var options = {
                url: endPointURL,
                token: accessToken,
                data: ko.toJSON(body)
            };

            Helper.Ajax.Post(options, function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
                }
            }, OnError, OnAlways);
        }
    }

    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
    function DoNothing(jqXHR, textStatus, errorThrown) {

    }

};
function GetNettingPurpose() {
    $.ajax({
        async: false,
        type: "GET",
        url: api.server + api.url.parametersystem + "/partype/" + ConsPARSYS.tmoNettingPurpose,
        contentType: "application/json; charset=utf-8; odata=verbose;",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + accessToken
        },
        success: function (data, textStatus, jqXHR) {
            viewModel.NettingPurpose(ko.mapping.toJS(data.Parsys));
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });

}

var viewModel = new ViewModel();

$.holdReady(true);
var intervalRoleName = setInterval(function () {
    if (Object.keys(Const_RoleName).length != 0) {
        $.holdReady(false);
        clearInterval(intervalRoleName);
    }
}, 100);

//$(document).on("RoleNameReady", function() {
$(document).ready(function () {

    // get table attribute
    var workflow = {
        State: $("#workflow-task-table").attr("workflow-state"),
        Outcome: $("#workflow-task-table").attr("workflow-outcome"),
        CustomOutcome: $("#workflow-task-table").attr("workflow-custom-outcome"),
        ShowContribute: $("#workflow-task-table").attr("workflow-show-contribute"),
        ShowActiveTask: $("#workflow-task-table").attr("workflow-show-active-task")
    };
    // get custom form for menu home
    var FormMenuCustom = {
        form: $("#form-menu-custom").attr("form")
    };
    // set workflow inside vm
    viewModel.WorkflowConfig(workflow);
    // $('#aspnetForm').after('<form id="frmUnderlying"></form>');
    // $("#modal-form-Attach").appendTo("#frmUnderlying");
    // $("#modal-form-Attach").appendTo("#frmUnderlying");
    // widget loader

    //set formCustom for home menu
    viewModel.FormMenuCustom(FormMenuCustom.form);
    $('#aspnetForm').after('<form id="frmUnderlying"></form>');
    $("#modal-form-Underlying").appendTo("#frmUnderlying");
    $("#modal-form-Attach").appendTo("#frmUnderlying");
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();
    $('#document-path-upload').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });
    $('#document-path-upload1').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });
    // block enter key from user to prevent submitted data.
    $(this).keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

            return false;
        }
    });
    // Load all templates
    $('script[src][type="text/html"]').each(function () {
        //alert($(this).attr("src"))
        var src = $(this).attr("src");
        if (src != undefined) {
            $(this).load(src);
        }
    });

    // scrollables
    $('.slim-scroll').each(function () {
        var $this = $(this);
        $this.slimscroll({
            height: $this.data('height') || 100,
            railVisible: true,
            alwaysVisible: true,
            color: '#D15B47'
        });
    });

    // tooltip
    $('[data-rel=tooltip]').tooltip();

    // datepicker
    $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    $('.time-picker').timepicker({
        defaultTime: "",
        minuteStep: 1,
        showSeconds: false,
        showMeridian: false
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    // Knockout Datepicker custom binding handler
    ko.bindingHandlers.datepicker = {
        init: function (element) {
            $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).datepicker("refresh");
        }
    };

    ko.bindingHandlers.stopBubble = {
        init: function (element) {
            ko.utils.registerEventHandler(element, "click", function (event) {
                event.cancelBubble = true;
                if (event.stopPropagation) {
                    event.stopPropagation();
                }

            });

        }
    };
    // Knockout custom file handler
    ko.bindingHandlers.file = {
        init: function (element, valueAccessor) {
            $(element).change(function () {
                var file = this.files[0];

                if (ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };
    // validation
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,

        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });


    //ko.applyBindings(viewModel, document.getElementById('home-transaction-tmo'));
    ko.applyBindings(viewModel);
    // -- test error login
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(TokenOnSuccess, TokenOnError);
        StartTaskHub();
    } else {
        accessToken = $.cookie(api.cookie.name);
        StartTaskHub();
        GetCurrentUser(viewModel);
        DealModel.token = accessToken;
        GetThresholdParameter(DealModel, OnSuccessThresholdPrm, OnError);
        //GetParameterData(DealModel, OnSuccessGetTotal, OnErrorDeal);
        viewModel.GetParameters();
        viewModel.GetData();
    }

});


// Function to display notification.
// class name : gritter-success, gritter-warning, gritter-error, gritter-info
function ShowNotification(title, text, className, isSticky) {
    $.gritter.add({
        title: title,
        text: text,
        class_name: className,
        sticky: isSticky
    });
}

// Get SPUser from cookies
function GetCurrentUser() {
    //if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
    if ($.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);

        viewModel.SPUser = spUser;

    }
}
function UploadDocuments(callBack, param) {
    var data = {
        ApplicationID: '',
        CIF: '',
        Name: ''
    };
    var actTitle = viewModel.ActivityTitle();
    switch (actTitle) {
        case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
        case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
        case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
            data = {
                ApplicationID: viewModel.TrackingTMOMaker().Transaction.ApplicationID(),
                CIF: viewModel.TrackingTMOMaker().Transaction.Customer.CIF(),
                Name: viewModel.TrackingTMOMaker().Transaction.Customer.Name()
            };
            break;
        case DBS.AcivityTitle.FXDealTMOMakerNettingTask:
            data = {
                ApplicationID: viewModel.TransactionMakerNetting().Transaction.ApplicationID,
                CIF: viewModel.TransactionMakerNetting().Transaction.Customer.CIF,
                Name: viewModel.TransactionMakerNetting().Transaction.Customer.Name
            };
            break;
    }

    // upload hanya document selain underlying
    var items = ko.utils.arrayFilter(viewModel.MakerDocuments(), function (item) {
        return item.Purpose.ID != 2 && item.IsNewDocument == true;
    });

    if (items != null && items.length > 0) {
        viewModel.counterUpload(0);
        for (var i = 0; items.length > i; i++) {
            UploadFile(data, items[i], callBack, items.length, param);
        }
    } else {
        callBack(param.WFID, param.ApproverID, param.DataID);
    }
}
// Upload the file
function UploadFile(context, document, callBack, countItem, param) {
    // Define the folder path for this example.

    var serverRelativeUrlToFolder = '/Instruction Documents';

    var parts = document.DocumentPath.name.split('.');

    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName;
    if (parts[parts.length - 2] != null) {
        fileName = parts[parts.length - 2] + timeStamp + "." + fileExtension; //document.DocumentPath.name;
    } else {
        fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;
    }


    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBuffer();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolder(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                // Change the display name and title of the list item.
                var changeItem = updateListItem(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    //return output;
                    var documentData = {
                        ID: 0,
                        Type: document.Type,
                        Purpose: document.Purpose,
                        LastModifiedDate: null,
                        LastModifiedBy: null,
                        DocumentPath: output.ServerRelativeUrl,
                        FileName: document.DocumentPath.name,
                        IsDormant: document.IsNewDocument
                    };

                    var actTitle = viewModel.ActivityTitle();
                    switch (actTitle) {

                        case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
                        case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
                        case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
                            viewModel.TrackingTMOMaker().Transaction.Documents.push(documentData);
                            break;
                        case DBS.AcivityTitle.FXDealTMOMakerNettingTask:
                            viewModel.TransactionMakerNetting().Transaction.Documents.push(documentData);
                            break;
                        default:
                            break;
                    }


                    viewModel.counterUpload(viewModel.counterUpload() + 1);
                    if (countItem == viewModel.counterUpload()) {
                        callBack(param.WFID, param.ApproverID, param.DataID);
                    }
                    return 1;
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBuffer() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }
        //bangkit
        reader.readAsArrayBuffer(document.DocumentPath);
        //reader.readAsDataURL(document.DocumentPath);
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(arrayBuffer) {

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);
        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval);
        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                //"content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItem(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItem(itemMetadata) {
        var body = {
            Title: document.DocumentPath.name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: document.Type.Name,
            Document_x0020_Purpose: document.Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };
        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval);
        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}
// Token validation On Success function
function TokenOnSuccess(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // call spuser function
    GetCurrentUser(viewModel);
    DealModel.token = accessToken;
    GetThresholdParameter(DealModel, OnSuccessThresholdPrm, OnError);
    //GetParameterData(DealModel, OnSuccessGetTotal, OnErrorDeal);
}

// Token validation On Error function
function TokenOnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}
function read_u() {
    //var value = parseFloat(viewModel.Amount_u());
    var e = document.getElementById("Amount_u").value;
    e = e.toString().replace(/,/g, '');
    var res = parseInt(e) * parseFloat(viewModel.Rate_u()) / parseFloat(idrrate);
    res = Math.round(res * 100) / 100;
    res = isNaN(res) ? 0 : res; //avoid NaN
    viewModel.AmountUSD_u(parseFloat(res).toFixed(2));
    viewModel.Amount_u(formatNumber(e));
}
//
function OnSuccessSignal() {
}

function OnErrorSignal() {
    ShowNotification("Connection Failed", "Failed connected to signal server", "gritter-error", true);
}

function OnReceivedSignal(data) {
    ShowNotification(data.Sender, data.Message, "gritter-info", false);
}

// SignalR ----------------------
var taskHub = $.hubConnection(config.signal.server + "hub");
var taskProxy = taskHub.createHubProxy("TaskService");
var IsTaskHubConnected = false;

function StartTaskHub() {
    taskHub.start()
        .done(function () {

            IsTaskHubConnected = true;
        })
        .fail(function () {
            ShowNotification("Hub Connection Failed", "Fail while try connecting to hub server", "gritter-error", true);
        }
    );
}

function LockAndUnlockTaskHub(lockType, instanceId, approverID, taskId, activityTitle) {
    if (IsTaskHubConnected) {
        var data = {
            WorkflowInstanceID: instanceId,
            ApproverID: approverID,
            TaskID: taskId,
            ActivityTitle: activityTitle,
            LoginName: spUser.LoginName,
            DisplayName: spUser.DisplayName
        };

        var LockType = lockType.toLowerCase() == "lock" ? "LockTask" : "UnlockTask";

        // try lock current task
        taskProxy.invoke(LockType, data)
            .fail(function (err) {
                ShowNotification("LockType Task Failed", err, "gritter-error", true);
            }
        );
    } else {
        ShowNotification("Task Manager", "There is no active connection to task server manager.", "gritter-error", true);
    }
}


taskProxy.on("Message", function (data) {
    ShowNotification(data.From, data.Message, "gritter-info", false);
});

taskProxy.on("NotifyInfo", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-info", false);
});

taskProxy.on("NotifyWarning", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-warning", false);
});

taskProxy.on("NotifyError", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-error", true);
});
function OnSuccessGetTotal(dataCall) {
    idrrate = dataCall.RateIDR;
}
function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}
function OnErrorDeal(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}
function OnSuccessThresholdPrm() {
    idrrate = DataModel.RateIDR;
}
function OnSuccessTotal() {
    switch (viewModel.ActivityTitle()) {
        case DBS.AcivityTitle.FXTMOMakerAfterReviseTask:
        case DBS.AcivityTitle.FXTMOMakerAfterSubmitTask:
        case DBS.AcivityTitle.FXTMOMakerAfterReopenTask:
            if (viewModel.TrackingTMOMaker().Transaction.AmountUSD() != null) {
                viewModel.CalculateFX(viewModel.TrackingTMOMaker().Transaction.AmountUSD());
            } else {
                viewModel.CalculateFX(0);
            }
            break;
    }
}