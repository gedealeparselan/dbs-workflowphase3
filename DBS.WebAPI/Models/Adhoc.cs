﻿using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;

namespace DBS.WebAPI.Models
{
    [ModelName("Adhoc")]

    #region Property
    public class AdhocModel
    {
        public int AdhocID { get; set; }
        public string CIF { get; set; }
        public string Code { get; set; }
        public string CompanyName { get; set; }
        public Nullable<int> CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string CreditManager { get; set; }
        public Nullable<bool> IsAdhocRequired { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Bank Bank { get; set; }
        public virtual Location Location { get; set; }
        public string Name { get; set; }
        public string Branch { get; set; }

    }
    public class AdhocRow:AdhocModel
    {
        public int RowID { get; set; }
    }
    public class AdhocGrid : Grid
    {
        public IList<AdhocRow> Rows { get; set; }
    }
    #endregion

    #region Filter
        public class AdhocFilter : Filter
        { }
    #endregion

    #region Interface
        public interface IAdhocRepository : IDisposable
        {
            bool GetAdhocByID(int id, ref AdhocModel Adhoc, ref string Message);
            bool GetAdhoc(ref IList<AdhocModel> branchs, int limit, int index, ref string message);
            bool GetAdhoc(int page, int size, IList<AdhocFilter> filters, string sortColumn, string sortOrder, ref AdhocGrid branch, ref string message);
            bool GetAdhoc(AdhocFilter filter, ref IList<AdhocModel> Adhocs, ref string message);
            bool GetAdhoc(string key, int limit, ref IList<AdhocModel> Adhocs, ref string message);
            bool AddAdhoc(AdhocModel Adhoc, ref string message);
            bool UpdateAdhoc(int id, AdhocModel Adhoc, ref string message);
            bool DeleteAdhoc(int id, ref string message);
        }
    #endregion

    #region Repository
        public class AdhocRepository : IAdhocRepository
        {
            private DBSEntities context = new DBSEntities();
            private CurrentUser currentUser = new CurrentUser();

            public bool GetAdhocByID(int id, ref AdhocModel Adhoc, ref string Message)
            {
                bool isSuccess = false;

                try
                {
                    Adhoc = (from a in context.Adhocs
                            where a.IsDeleted.Equals(false) && a.AdhocID.Equals(id)
                             select new AdhocModel
                            {
                                AdhocID = a.AdhocID,
                                Code = a.Code,
                                CIF=a.CIF,
                                CompanyName=a.CompanyName,
                                CategoryID=a.CategoryID,
                                CreditManager=a.CreditManager,
                                IsAdhocRequired=a.IsAdhocRequired,
                                IsDeleted=a.IsDeleted,
                                Customer=a.Customer,
                                Bank=a.Bank,
                                LastModifiedDate = a.CreateDate,
                                LastModifiedBy = a.CreateBy
                            }).SingleOrDefault();

                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    Message = ex.Message;
                }

                return isSuccess;
            }
            public bool GetAdhoc(ref IList<AdhocModel> adhocs, int limit, int index, ref string message)
            {
                bool isSuccess = false;

                try
                {
                    int skip = limit * index;
                    using (DBSEntities context = new DBSEntities())
                    {
                        adhocs = (from a in context.Adhocs
                                 where a.IsDeleted.Equals(false)
                                 orderby new {a.CIF, a.Bank}
                                 select new AdhocModel
                                 {
                                     AdhocID = a.AdhocID,
                                     Code = a.Code,
                                     CIF = a.CIF,
                                     CompanyName = a.CompanyName,
                                     CategoryID = a.CategoryID,
                                     CreditManager = a.CreditManager,
                                     IsAdhocRequired = a.IsAdhocRequired,
                                     IsDeleted = a.IsDeleted,
                                     Customer = a.Customer,
                                     Bank = a.Bank,
                                     LastModifiedDate = a.CreateDate,
                                     LastModifiedBy = a.CreateBy
                                 }).Skip(skip).Take(limit).ToList();
                    }

                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
                return isSuccess;
            }
            public bool GetAdhoc(int page, int size, IList<AdhocFilter> filters, string sortColumn, string sortOrder, ref AdhocGrid AdhocGrid, ref string message)
            {
                bool isSuccess = false;

                try
                {
                    int skip = (page - 1) * size;
                    string orderBy = sortColumn + " " + sortOrder;
                    DateTime maxDate = new DateTime();

                    AdhocModel filter = new AdhocModel();
                    filter.Customer = new Customer { CustomerName = string.Empty };

                    if (filters != null)
                    {
                        filter.CIF = (string)filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                        filter.Customer.CustomerName = (string)filters.Where(a => a.Field.Equals("Customer")).Select(a => a.Value).SingleOrDefault();
                        filter.CreditManager = (string)filters.Where(a => a.Field.Equals("CreditManager")).Select(a => a.Value).SingleOrDefault();
                        filter.IsAdhocRequired = bool.Parse((string)filters.Where(a => a.Field.Equals("IsAdhocRequired")).Select(a => a.Value).SingleOrDefault());
                        filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                        if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                        {
                            filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                            maxDate = filter.LastModifiedDate.Value.AddDays(1);
                        }
                    }

                    using (DBSEntities context = new DBSEntities())
                    {
                        var data = (from a in context.Adhocs
                                    join b in context.Customers on a.CIF equals b.CIF
                                    join c in context.Locations on b.LocationID equals c.LocationID
                                    join d in context.BizSegments on b.BizSegmentID equals d.BizSegmentID
                                    let isCIF = !string.IsNullOrEmpty(filter.Customer.CIF)
                                    let isCustomer = !string.IsNullOrEmpty(filter.Customer.CustomerName)
                                    let isCreditManager = !string.IsNullOrEmpty(filter.CreditManager)
                                    let isIsAdhocRequired = filter.IsAdhocRequired//!string.IsNullOrEmpty(filter.IsAdhocRequired)
                                    let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                    let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                    where a.IsDeleted.Equals(false) &&
                                        (isCIF ? a.CIF.Contains(filter.CIF) : true) &&
                                        (isCreditManager ? a.CreditManager.Contains(filter.CreditManager) : true) &&
                                        (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                        (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                    select new AdhocModel
                                    {
                                        //CategoryName=d.BizSegmentName,
                                        AdhocID = a.AdhocID,
                                        Code = a.Code,
                                        CIF = a.CIF,
                                        Name = a.Customer.CustomerName,
                                        CompanyName = a.CompanyName,
                                        CategoryID = a.CategoryID,
                                        CreditManager = a.CreditManager,
                                        IsAdhocRequired = a.IsAdhocRequired,
                                        CategoryName = d.BizSegmentName,
                                        Customer=a.Customer,
                                        Branch = c.LocationName,
                                        LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                        LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                    });

                        AdhocGrid.Page = page;
                        AdhocGrid.Size = size;
                        AdhocGrid.Total = data.Count();
                        AdhocGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                        AdhocGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                            .Select((a, i) => new AdhocRow
                            {
                                //CategoryName=a.CategoryName,
                                RowID = i + 1,
                                AdhocID = a.AdhocID,
                                Code = a.Code,
                                CIF = a.CIF,
                                Name=a.Customer.CustomerName,
                                CompanyName = a.CompanyName,
                                CategoryID = a.CategoryID,
                                CreditManager = a.CreditManager,
                                IsAdhocRequired = a.IsAdhocRequired,
                                CategoryName=a.CategoryName,
                                Branch=a.Branch,
                                LastModifiedBy = a.LastModifiedBy,
                                LastModifiedDate = a.LastModifiedDate
                            })
                            .Skip(skip)
                            .Take(size)
                            .ToList();
                    }

                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                } 

                return isSuccess;
            }
            public bool GetAdhoc(AdhocFilter filter, ref IList<AdhocModel> Adhocs, ref string message)
            {
                throw new NotImplementedException();
            }
            public bool GetAdhoc(string key, int limit, ref IList<AdhocModel> Adhocs, ref string message)
            {
                bool isSuccess = false;

                try
                {
                    Adhocs = (from a in context.Adhocs
                               join b in context.Customers on a.CIF equals b.CIF
                               join c in context.Locations on b.LocationID equals c.LocationID
                               where a.IsDeleted.Equals(false) && b.IsDeleted.Equals(false) //&& c.IsDeleted(false)
                               orderby new { a.CIF }
                               select new AdhocModel
                               {
                                   AdhocID = a.AdhocID,
                                   Code = a.Code,
                                   CIF = a.CIF,
                                   CompanyName = a.CompanyName,
                                   CategoryID = a.CategoryID,
                                   CreditManager = a.CreditManager,
                                   IsAdhocRequired = a.IsAdhocRequired,
                                   IsDeleted = a.IsDeleted,
                                   Customer = a.Customer,
                                   Bank = a.Bank,
                                   LastModifiedDate = a.CreateDate,
                                   LastModifiedBy = a.CreateBy
                               }).Take(limit).ToList();

                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }

                return isSuccess;
            }
            public bool AddAdhoc(AdhocModel Adhoc, ref string Message)
            {
                bool isSuccess = false;
                if (Adhoc != null)
                {
                    try
                    {
                        if (!context.Adhocs.Where(a => a.CIF == Adhoc.CIF && a.IsDeleted.Equals(false)).Any())
                        {
                            context.Adhocs.Add(new Entity.Adhoc()
                            {
                                Code = Adhoc.Code,
                                CIF = Adhoc.Customer.CIF,
                                CompanyName = Adhoc.CompanyName,
                                CategoryID = Adhoc.CategoryID,
                                CreditManager = Adhoc.CreditManager,
                                IsAdhocRequired = Adhoc.IsAdhocRequired,
                                IsDeleted = false,
                                //Customer = Adhoc.Customer,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().DisplayName
                            });

                            context.SaveChanges();

                            isSuccess = true;
                        }
                        else
                        {
                            Message = string.Format("Adhoc data for is already exist.", Adhoc.CIF);
                        }
                    }
                    catch (Exception ex)
                    {
                        Message = ex.Message;
                    }
                }

                return isSuccess;
            }
            public bool UpdateAdhoc(int id, AdhocModel Adhoc, ref string Message)
            {
                bool isSuccess = false;

                try
                {
                    Entity.Adhoc data = context.Adhocs.Where(a => a.AdhocID.Equals(id)).SingleOrDefault();

                    if (data != null)
                    {
                        //if (context.Adhocs.Where(a => a.AdhocID != Adhoc.AdhocID && a.IsDeleted.Equals(false)).Count() >= 1)
                        //{
                        //    Message = string.Format("Adhoc Name {0} is exist.", Adhoc.CIF);
                        //}
                        
                                data.Code = Adhoc.Code;
                                data.CIF = Adhoc.Customer.CIF;
                                data.CompanyName = Adhoc.CompanyName;
                                data.CategoryID = Adhoc.CategoryID;
                                data.CreditManager = Adhoc.CreditManager;
                                data.IsAdhocRequired = Adhoc.IsAdhocRequired;
                                data.IsDeleted = Adhoc.IsDeleted;
                                //data.Customer = Adhoc.Customer;
                                //data.Bank = Adhoc.Bank;
                                data.UpdateDate = DateTime.UtcNow;
                                data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                            context.SaveChanges();

                            isSuccess = true;
                    }
                    else
                    {
                        Message = string.Format("Adhoc data for CIF {0} is does not exist.", id);
                    }
                }
                catch (Exception ex)
                {
                    Message = ex.Message;
                }

                return isSuccess;
            }

            public bool DeleteAdhoc(int id, ref string Message)
            {
                bool isSuccess = false;

                try
                {
                    Entity.Adhoc data = context.Adhocs.Where(a => a.AdhocID.Equals(id)).SingleOrDefault();

                    if (data != null)
                    {
                        data.IsDeleted = true;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                    else
                    {
                        Message = string.Format("Adhoc data for CIF ID {0} is does not exist.", id);
                    }
                }
                catch (Exception ex)
                {
                    Message = ex.Message;
                }

                return isSuccess;
            }
            private bool disposed = false;
            protected virtual void Dispose(bool disposing)
            {
                if (!this.disposed)
                {
                    if (disposing)
                    {
                        if (context != null)
                        {
                            currentUser = null;
                            context.Dispose();
                        }
                    }
                }
                this.disposed = true;
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

        }
    #endregion

}