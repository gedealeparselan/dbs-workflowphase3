﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("CustomerType")]
    public class CustomerTypeModel
    {
        /// <summary>
        /// CustomerType ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// CustomerTypes Name.
        /// </summary>
        [MaxLength(50, ErrorMessage = "Customer Type Name is must be in {1} characters.")]
        public string Name { get; set; }

        /// <summary>
        /// CustomerType Description.
        /// </summary>
        [MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class CustomerTypeRow : CustomerTypeModel
    {
        public int RowID { get; set; }

    }

    public class CustomerTypeGrid : Grid
    {
        public IList<CustomerTypeRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class CustomerTypeFilter : Filter { }
    #endregion

    #region Interface
    public interface ICustomerTypeRepository : IDisposable
    {
        bool GetCustomerTypeByID(int id, ref CustomerTypeModel customerType, ref string message);
        bool GetCustomerType(ref IList<CustomerTypeModel> customerTypes, int limit, int index, ref string message);
        bool GetCustomerType(ref IList<CustomerTypeModel> customerTypes, ref string message);
        bool GetCustomerType(int page, int size, IList<CustomerTypeFilter> filters, string sortColumn, string sortOrder, ref CustomerTypeGrid customerType, ref string message);
        bool GetCustomerType(CustomerTypeFilter filter, ref IList<CustomerTypeModel> customerTypes, ref string message);
        bool GetCustomerType(string key, int limit, ref IList<CustomerTypeModel> customerTypes, ref string message);
        bool AddCustomerType(CustomerTypeModel customerType, ref string message);
        bool UpdateCustomerType(int id, CustomerTypeModel customerType, ref string message);
        bool DeleteCustomerType(int id, ref string message);
    }
    #endregion

    #region Repository
    public class CustomerTypeRepository : ICustomerTypeRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetCustomerTypeByID(int id, ref CustomerTypeModel customerType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                customerType = (from a in context.CustomerTypes
                               where a.IsDeleted.Equals(false) && a.CustomerTypeID.Equals(id)
                               select new CustomerTypeModel
                               {
                                   ID = a.CustomerTypeID,
                                   Name = a.CustomerTypeName,
                                   Description = a.CustomerTypeDescription,
                                   LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                   LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                               }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerType(ref IList<CustomerTypeModel> customerTypes, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    customerTypes = (from a in context.CustomerTypes
                                    where a.IsDeleted.Equals(false)
                                    orderby new { a.CustomerTypeName, a.CustomerTypeDescription }
                                    select new CustomerTypeModel
                                    {
                                        ID = a.CustomerTypeID,
                                        Name = a.CustomerTypeName,
                                        Description = a.CustomerTypeDescription,
                                        LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                        LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                    }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerType(ref IList<CustomerTypeModel> customerTypes, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    customerTypes = (from a in context.CustomerTypes
                                    where a.IsDeleted.Equals(false)
                                    orderby new { a.CustomerTypeName, a.CustomerTypeDescription }
                                    select new CustomerTypeModel
                                    {
                                        ID = a.CustomerTypeID,
                                        Name = a.CustomerTypeName,
                                        Description = a.CustomerTypeDescription,
                                        LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                        LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                    }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerType(int page, int size, IList<CustomerTypeFilter> filters, string sortColumn, string sortOrder, ref CustomerTypeGrid customerTypeGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                CustomerTypeModel filter = new CustomerTypeModel();
                if (filters != null)
                {
                    filter.Name = (string)filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.CustomerTypes
                                let isCustomerTypeName = !string.IsNullOrEmpty(filter.Name)
                                let isCustomerTypeDescription = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isCustomerTypeName ? a.CustomerTypeName.Contains(filter.Name) : true) &&
                                    (isCustomerTypeDescription ? a.CustomerTypeDescription.Contains(filter.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new CustomerTypeModel
                                {
                                    ID = a.CustomerTypeID,
                                    Name = a.CustomerTypeName,
                                    Description = a.CustomerTypeDescription,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    customerTypeGrid.Page = page;
                    customerTypeGrid.Size = size;
                    customerTypeGrid.Total = data.Count();
                    customerTypeGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    customerTypeGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new CustomerTypeRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Name = a.Name,
                            Description = a.Description,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerType(CustomerTypeFilter filter, ref IList<CustomerTypeModel> customerTypes, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetCustomerType(string key, int limit, ref IList<CustomerTypeModel> customerTypes, ref string message)
        {
            bool isSuccess = false;

            try
            {
                customerTypes = (from a in context.CustomerTypes
                                where a.IsDeleted.Equals(false) &&
                                    a.CustomerTypeName.Contains(key) || a.CustomerTypeDescription.Contains(key)
                                orderby new { a.CustomerTypeName, a.CustomerTypeDescription }
                                select new CustomerTypeModel
                                {
                                    Name = a.CustomerTypeName,
                                    Description = a.CustomerTypeDescription
                                }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddCustomerType(CustomerTypeModel customerType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.CustomerTypes.Where(a => a.CustomerTypeName.Equals(customerType.Name) && a.IsDeleted.Equals(false)).Any())
                {
                    context.CustomerTypes.Add(new Entity.CustomerType()
                    {
                        CustomerTypeName = customerType.Name,
                        CustomerTypeDescription = customerType.Description,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("CustomerType data for CustomerType Name {0} is already exist.", customerType.Name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateCustomerType(int id, CustomerTypeModel customerType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.CustomerType data = context.CustomerTypes.Where(a => a.CustomerTypeID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.CustomerTypes.Where(a => a.CustomerTypeID != customerType.ID && a.CustomerTypeName.Equals(customerType.Name) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Customer Type Name {0} is exist.", customerType.Name);
                    }
                    else
                    {
                        data.CustomerTypeName = customerType.Name;
                        data.CustomerTypeDescription = customerType.Description;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("CustomerType data for Customer Type ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteCustomerType(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.CustomerType data = context.CustomerTypes.Where(a => a.CustomerTypeID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Customer Type data for CustomerType ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}