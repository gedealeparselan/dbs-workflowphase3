﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    public class TBOToolController : ApiController
    {
        private string message = string.Empty;
        private readonly ITBOModelInterface repository = null;

        public TBOToolController() {
            this.repository = new TBOModelRepository();
        }
        public TBOToolController(ITBOModelInterface repository)
        {
            this.repository = repository;
        }

        [ResponseType(typeof(IList<TBOModel>))]
        [HttpGet]
        [Route("api/TBOToolByCIF")]
        public HttpResponseMessage GetTBOByCif(string id)
        {
            IList<TBOModel> output = new List<TBOModel>();

            if (!repository.GetTransactionTBOByCif(ref output, id, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TBOModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(IList<TBOModel>))]
        [HttpGet]
        [Route("api/TBOToolByID/{cif}")]
        public HttpResponseMessage GetTBO(string cif)  
        {
            IList<TBOModel> output = new List<TBOModel>();

            if (!repository.GetTBO(ref output, cif, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TBOModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(IList<TBOModel>))]
        [HttpGet]
        [Route("api/TBOToolByIDWF/{cif}")]
        public HttpResponseMessage GetTBOWF(string cif) 
        { 
            IList<TBOModel> output = new List<TBOModel>();

            if (!repository.GetTBOWF(ref output, cif, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TBOModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(IList<TBOModel>))]
        [HttpGet]
        [Route("api/TBOToolByIDWFAppID/{spUserLoginName}")]
        public HttpResponseMessage GetTBOWFID(string spUserLoginName)  
        {
            IList<TBOModel> output = new List<TBOModel>();

            if (!repository.GetTBOWFID(ref output, spUserLoginName, ref message)) 
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }else {
                    return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }


        [ResponseType(typeof(IList<TransactionTBOTimelineModel>))]
        [HttpGet]
        [Route("api/TBOToolByIDWFInstanceID/{workflowInstanceID}")]
        public HttpResponseMessage GetTBOWFTimelineID(Guid workflowInstanceID)
        {
            IList<TransactionTBOTimelineModel> output = new List<TransactionTBOTimelineModel>();

            if (!repository.GetTransactionTimeline(ref output, workflowInstanceID, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/TBOToolData")]
        public HttpResponseMessage Put(TBOModel TBO)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTBO(TBO, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "TBO data has been Submit." });
                }
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/TBOToolData")]
        public HttpResponseMessage Post(TBOModelWF TBO) 
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.SaveTBO(TBO, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "TBO data has been Submit to Workflow." });
                }
            }
        }

        [ResponseType(typeof(IList<TBOModel>))]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            IList<TBOModel> output = new List<TBOModel>();

            if (!repository.GetTBO(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TBOModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(IList<TBOModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<TBOModel> output = new List<TBOModel>();

            if (!repository.GetTBO(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<TBOModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(TBOModelGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/TBOTool")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<TBOModelFilter> filters)
        {
            TBOModelGrid output = new TBOModelGrid();

            if (!repository.GetTBO(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "CreatedDate" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<TBOModelGrid>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(TBOModelGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/TBOToolWF")]
        public HttpResponseMessage GetTBOWF(int? page, int? size, string sort_column, string sort_order, IList<TBOModelFilter> filters) 
        {
            TBOModelGrid output = new TBOModelGrid();

            if (!repository.GetTBOWF(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "CreatedDate" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<TBOModelGrid>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(TBOModel))]
        [Route("api/TBOTool/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            TBOModel output = new TBOModel();

            if (!repository.GetTBOByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<TBOModel>(HttpStatusCode.OK, output);
            }
        }

    }
}
