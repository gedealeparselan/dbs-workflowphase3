﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminateWorkflowWPUserControl.ascx.cs" Inherits="DBS.Admin.SPWebparts.TerminateWorkflowWP.TerminateWorkflowWPUserControl" %>

<h1 class="header smaller lighter dark">Terminate Workflow</h1>

<div class="content">
	<div class="row">
		<asp:Label runat="server" ID="labelInputType" CssClass="col-lg-3">Input Type</asp:Label>
		<asp:DropDownList runat="server" ID="ddlInputType" AutoPostBack="true" CssClass="col-lg-5" OnSelectedIndexChanged="ddlInputType_SelectedIndexChanged">
			<asp:ListItem Text="Dropdown" Value="1" Selected="True"></asp:ListItem>
			<asp:ListItem Text="Textbox" Value="2"></asp:ListItem>
		</asp:DropDownList>
	</div>
	<br />
	<div class="row">
		<asp:Label runat="server" ID="labelListName" CssClass="col-lg-3">List Name</asp:Label>
		<asp:DropDownList runat="server" ID="ddlListName" CssClass="col-lg-5">
			<asp:ListItem Text="Collateral" Value="CollateralProduct"></asp:ListItem>
			<asp:ListItem Text="FD" Value="FDProduct"></asp:ListItem>
			<asp:ListItem Text="FX Deal" Value="FX Deal"></asp:ListItem>
			<asp:ListItem Text="IPE Payment Product" Value="IPEPaymentProduct"></asp:ListItem>
			<asp:ListItem Text="Loan" Value="LOANProduct"></asp:ListItem>
			<asp:ListItem Text="Payment Product" Value="Payment Product"></asp:ListItem>
			<asp:ListItem Text="Retail CIF" Value="RetailCIFProduct"></asp:ListItem>
			<asp:ListItem Text="TMO" Value="TMOProduct"></asp:ListItem>
			<asp:ListItem Text="UT" Value="UTProduct"></asp:ListItem>
			<asp:ListItem Text="Workflow Entity" Value="WorkflowEntity"></asp:ListItem>
		</asp:DropDownList>
		<asp:TextBox runat="server" ID="txtListName" CssClass="col-lg-5"></asp:TextBox>
	</div>
	<br />
    <div class="row">
		<asp:Label runat="server" ID="labelStartDateAfter" CssClass="col-lg-3">Start Date After</asp:Label>
		<asp:TextBox runat="server" ID="txtStartDateAfter" CssClass="col-lg-5 mydatepicker"></asp:TextBox>
	</div>
	<br />
	<div class="row">
		<asp:Label runat="server" ID="labelStartDateBefore" CssClass="col-lg-3">Start Date Before</asp:Label>
		<asp:TextBox runat="server" ID="txtStartDateBefore" CssClass="col-lg-5 mydatepicker"></asp:TextBox>
	</div>
	<br />
	<div class="row">
		<asp:Button runat="server" ID="btnTerminate" CssClass="btn btn-info" Text="Terminate WF" OnClick="btnTerminate_Click" />
	</div>
    <br />
    <div class="row">
        <asp:Label runat="server" ID="lblMessageResult" CssClass="col-lg-3" Visible="false">Result:</asp:Label>
        <br />
        <asp:Label runat="server" ID="lblResult" CssClass="col-lg-11" Visible="false"></asp:Label>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('.mydatepicker').datepicker({ autoclose: true });
    });
</script>