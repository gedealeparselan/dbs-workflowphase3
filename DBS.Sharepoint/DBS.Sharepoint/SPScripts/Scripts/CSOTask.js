﻿var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};

var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};

var DecodeLogin = function (username) {
    var index = username.lastIndexOf("|");
    var str = username.substring(index + 1);
    return str;
}

var TransactionDataModel = {
    TransactionDataIM: ko.observableArray([InterestMaintenace]),
    TransactionDataRO: ko.observableArray([Rollover]),
    Documents: ko.observableArray([]),
    DocumentsAddHoc: ko.observableArray([])
};

var InterestMaintenace = {
    AccountPreferencial: ko.observable(),
    AllinFTPRat: ko.observable(),
    AllinRate: ko.observable(),
    AmountRollover: ko.observable(),
    ApprovalDOA: ko.observable(),
    ApprovedBy: ko.observable(),
    ApprovedMarginAsPerCM: ko.observable(),
    BaseRate: ko.observable(),
    BizSegmentID: ko.observable(),
    BizSegmentName: ko.observable(),
    CIF: ko.observable(),
    CSO: ko.observable(),
    CSOInterestMaintenanceID: ko.observable(),
    CSOName: ko.observable(),
    CreateBy: ko.observable(),
    CreateDate: ko.observable(),
    CurrencyID: ko.observable(),
    CurrencyName: ko.observable(),
    CurrentInterestRate: ko.observable(),
    CustomerName: ko.observable(),
    DueDate: ko.observable(),
    EmployeeID: ko.observable(),
    InstructionID: ko.observable(),
    InterestMaintenanceID: ko.observable(),
    InterestRateCodeID: ko.observable(),
    InterestRateCodeName: ko.observable(),
    IsAdhoc: ko.observable(),
    IsHeavyEquipment: ko.observable(),
    IsSBLCSecured: ko.observable(),
    LoanContractNo: ko.observable(),
    MaintenanceType: ko.observable(),
    NextInterestDate: ko.observable(),
    NextPrincipalDate: ko.observable(),
    PeggingReviewDate: ko.observable(),
    OtherRemarks: ko.observable(),
    PreviousInterestRate: ko.observable(),
    RM: ko.observable(),
    Remarks: ko.observable(),
    SchemeCode: ko.observable(),
    Selected: ko.observable(),
    SolID: ko.observable(),
    SpecialFTP: ko.observable(),
    SpecialRate: ko.observable(),
    TransactionInterestMaintenanceID: ko.observable(),
    UpdateBy: ko.observable(),
    UpdateDate: ko.observable(),
    ValueDate: ko.observable()
};

var Rollover = {
    SOLID: ko.observable(),
    SchemeCode: ko.observable(),
    CIF: ko.observable(),
    Customer: ko.observable(CustomerModel),
    LoanContractNo: ko.observable(),
    DueDate: ko.observable(),
    Currency: ko.observable(CurrencyModel),
    AmountDue: ko.observable(),
    LimitID: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    RM: ko.observable(),
    CSO: ko.observable(),
    IsSBLCSecured: ko.observable(),
    IsHeavyEquipment: ko.observable(),
    MaintenanceTypeID: ko.observable(),
    AmountRollover: ko.observable(),
    NextPrincipalDate: ko.observable(),
    NextInterestDate: ko.observable(),
    ApprovedMarginAsPerCM: ko.observable(),
    InterestRateCodeID: ko.observable(),
    BaseRate: ko.observable(),
    AllInLP: ko.observable(),
    AllInFTPRate: ko.observable(),
    AccountPreferencial: ko.observable(),
    SpecialFTP: ko.observable(),
    SpecialRateMargin: ko.observable(),
    AllInrate: ko.observable(),
    ApprovalDOA: ko.observable(),
    ApprovedBy: ko.observable(),
    Remarks: ko.observable(),
    IsAdhoc: ko.observable(),
    CSOName: ko.observable(),
    Documents: ko.observableArray([DocumentModel])

};

var DocumentModel = {
    ID: ko.observable(),
    Type: ko.observable(DocumentTypeModel),
    Purpose: ko.observable(DocumentPurposeModel),
    DocumentPath: ko.observable(),
    IsNewDocument: ko.observable(false),
    LastModifiedDate: ko.observable(new Date),
    LastModifiedBy: ko.observable(),
    IsDormant: ko.observable(false)
};

var DocumentPurposeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var DocumentTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ProductModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    WorkflowID: ko.observable(),
    Workflow: ko.observable()
};

var SelectedModel = {
    DocumentType: ko.observable(),
    DocumentPurpose: ko.observable()
};

var Parameter = {
    DocumentTypes: ko.observableArray([DocumentTypeModel]),
    DocumentPurposes: ko.observableArray([DocumentPurposeModel]),

};

var CurrencyModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var CustomerModel = {
    CIF: ko.observable(),
    Name: ko.observable(),
    POAName: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Branch: ko.observable(BranchModel),
    Type: ko.observable(TypeModel),
    RM: ko.observable(RMModel),
    Contacts: ko.observableArray([ContactModel]),
    Functions: ko.observableArray([FunctionModel]),
    Accounts: ko.observableArray([AccountModel]),
    Underlyings: ko.observableArray([UnderlyingModel])
};

var UnderlyingModel = {
    ID: ko.observable(),
    UnderlyingDocument: ko.observable(UnderlyingDocumentModel),
    DocumentType: ko.observable(DocumentTypeModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    AttachmentNo: ko.observable(),
    IsStatementLetter: ko.observable(false),
    IsDeclarationOfException: ko.observable(false),
    StartDate: ko.observable(),
    EndDate: ko.observable(),
    DateOfUnderlying: ko.observable(),
    ExpiredDate: ko.observable(),
    ReferenceNumber: ko.observable(),
    SupplierName: ko.observable(),
    InvoiceNumber: ko.observable()
};

var AccountModel = {
    AccountNumber: ko.observable(),
};

var UnderlyingDocumentModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable()
};

var FunctionModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var BizSegmentModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var BranchModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var TypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var RMModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    RankID: ko.observable(),
    Rank: ko.observable(),
    SegmentID: ko.observable(),
    Segment: ko.observable(),
    BranchID: ko.observable(),
    Branch: ko.observable(),
    Email: ko.observable()
};

var ContactModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    PhoneNumber: ko.observable(),
    DateOfBirth: ko.observable(),
    Address: ko.observable(),
    IDNumber: ko.observable()
};

var ChannelModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var LoanDetailsModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    IsChainTopUrgent: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    Channel: ko.observable(ChannelModel),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel])
};

var TransactionCSODetailsModel = function () {
    this.Transaction = ko.observable(LoanDetailsModel);
};

var ViewModel = function () {
    var self = this;

    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);
        if (date == '1970/01/01 00:00:00' || date == null) {
            return "";
        }

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };
    self.FormatNumber = function (num) {
        return Helper.FormatNumber(num);
    };
    self.CSODetails = ko.observable(TransactionCSODetailsModel);
    self.Products = ko.observableArray([]);
    self.Currencies = ko.observableArray([]);
    self.Channels = ko.observableArray([]);
    self.BizSegments = ko.observableArray([]);
    self.InterestRates = ko.observableArray([]);
    self.MaintenanceTypes = ko.observableArray([]);
    self.TransactionID = ko.observable();
    self.ApplicationID = ko.observable();
    self.Title = ko.observable();
    self.InstructionID = ko.observable();
    self.SPUser = ko.observable();
    self.DocumentPath = ko.observable();
    self.DocumentFileName = ko.observable();
    self.Documents = ko.observableArray([]);
    self.ApprovalDocumentPath = ko.observable();
    self.ApprovalDocuments = ko.observableArray([]);
    self.ChooseInstruction = ko.observableArray([]);
    self.DocumentPath = ko.observable();
    self.DocumentType = ko.observable();
    self.DocumentPurpose = ko.observable();
    self.ddlDocumentTypes = ko.observableArray([]);
    self.DocumentsAddHoc = ko.observableArray([]);
    self.DocumentsMarge = ko.observableArray([]);

    self.IsSubmitted = ko.observable(false);

    //Agung
    self.TransactionData = ko.observable(TransactionDataModel);

    // Options selected value
    self.Selected = ko.observable(SelectedModel);
    self.Parameter = ko.observable(Parameter);
    // Uploading document

    self.UploadDocument = function () {
        self.DocumentPath(null);
        self.Selected().DocumentType(null);
        //self.Selected().DocumentPurpose(null);
        self.DocumentType(null);
        //self.DocumentPurpose(null);
        $("#modal-form-upload").modal('show');
        $('.remove').click();
    }

    self.AddDocument = function () {
        var doc = {
            ID: 0,
            Type: self.DocumentType(),
            FileName: self.DocumentFileName(),
            DocumentPath: self.DocumentPath(),
            LastModifiedDate: new Date(),
            LastModifiedBy: null
        };
        self.Documents.push(doc);
        self.DocumentsMarge.push(doc);

        $('.remove').click();

        $("#modal-form-upload").modal('hide');
        //var handsontable = $container.data('handsontable');
        //handsontable.setDataAtRowProp(attachRow, "Attachment", self.DocumentPath().name);
    };

    self.AddDocumentAddHoc = function () {
        var doc = {
            ID: 1,
            Type: self.DocumentType(),
            FileName: self.DocumentFileName(),
            DocumentPath: self.DocumentPath(),
            LastModifiedDate: new Date(),
            LastModifiedBy: null
        };

        self.DocumentsAddHoc.push(doc);
        self.DocumentsMarge.push(doc);

        $("#modal-form-uploadAddHoc").modal('hide');
        var handsontable = $container.data('handsontable');
        handsontable.setDataAtRowProp(attachRow, "Attachment", self.DocumentPath().name);
        handsontable.setDataAtRowProp(attachRow, "DocumentType", self.DocumentType().Name);
    };

    self.RemoveDocument = function (data) {
        self.Documents.remove(data);
    }

    self.onSelectionChoseeFile = function (item) {
        $("#modal-form-instruction").modal('hide');
        var handsontable = $container.data('handsontable');
        handsontable.setDataAtRowProp(attachRow, "InstructionID", item.ID);
    }

    self.save = function () {
        var handsontable = $container.data('handsontable');
        var data = handsontable.getData();
        var IsSelected = false;
        for (var i = 0; i < data.length; i++) {
            var Select = handsontable.getDataAtRowProp(i, "Selected");
            if (Select == true) {
                IsSelected = true;
            }
        }

        if (IsSelected == true) {
            var isAttachedEmail = CheckEmailApp();
            var AttachFile = false;
            var taskType = $("#cso-task-type").val();
            if (taskType == 2) {
                for (var i = 0; i < data.length; i++) {
                    var Select = handsontable.getDataAtRowProp(i, "Selected");
                    //var Attach = handsontable.getDataAtRowProp(i, "Attachment");
                    var MaintentType = handsontable.getDataAtRowProp(i, "MaintenanceTypeName");
                    var InterestRate = handsontable.getDataAtRowProp(i, "InterestRateCodeName");
                    var Instruction = handsontable.getDataAtRowProp(i, "InstructionID");
                    var DocType = handsontable.getDataAtRowProp(i, "DocumentType");
                    var AdHoc = handsontable.getDataAtRowProp(i, "IsAdhoc");
                    if (Select == true && MaintentType == "") {
                        ShowNotification("Attention", "Please Select Maintenance Type", "gritter-warning", true);
                        return false;
                    }
                    if (Select == true && MaintentType == "Rollover" && InterestRate == "") {
                        ShowNotification("Attention", "Please Select Interest Rate", "gritter-warning", true);
                        return false;
                    }
                    if (Select == true && MaintentType == "Rollover" && Instruction == null) {
                        ShowNotification("Attention", "Please Select Instruction", "gritter-warning", true);
                        return false;
                    }
                    if (Select == true && DocType != config.validate.approval.adhocapp && AdHoc == "Yes") {
                        ShowNotification("Attention", "Please Attach File AdHoc", "gritter-warning", true);
                        return false;
                    }
                    if (Select == true && MaintentType == "Rollover") {
                        AttachFile = true;
                    }
                }
                if (AttachFile == true) {
                    if (isAttachedEmail == true) {
                        Save();
                    }
                    else {
                        ShowNotification("Attention", "You need to attach email approval to continue transaction.", 'gritter-warning', true);
                        return;
                    }
                } else {
                    Save();
                }
            }
            else {
                for (var i = 0; i < data.length; i++) {
                    var Select = handsontable.getDataAtRowProp(i, "Selected");
                    var InterestRate = handsontable.getDataAtRowProp(i, "InterestRateCodeName");
                    var AdHoc = handsontable.getDataAtRowProp(i, "IsAdhoc");
                    if (Select == true && InterestRate == "") {
                        ShowNotification("Attention", "Please Select Interest Rate", "gritter-warning", true);
                        return false;
                    }
                }
                var isAttachedEmail = CheckEmailApp();
                if (isAttachedEmail == true) {
                    Save();
                }
                else {
                    ShowNotification("Attention", "You need to attach email approval to continue transaction.", 'gritter-warning', true);
                    return;
                }
            }
        } else {
            ShowNotification("Attention", "Please select customers on worksheet", "gritter-warning", true);
        }
    };
}

function Save() {
    var data = {
        ApplicationID: null,
        CIF: null,
        Name: null
    };
    viewModel.IsSubmitted(true);

    if (viewModel.DocumentsAddHoc().length > 0) {
        UploadFileRecuresive(data, viewModel.DocumentsMarge(), SaveTransactionData, viewModel.DocumentsMarge().length);
    }
    else if (viewModel.DocumentsAddHoc().length == 0 && viewModel.Documents().length == 0) {
        SaveTransactionData();
    }
    else {
        UploadFileRecuresive(data, viewModel.Documents(), SaveTransactionData, viewModel.Documents().length);
    }

}

function SaveTransactionData() {
    var taskType = $("#cso-task-type").val();
    var endPoint = "";
    var AddDataExcel = $container.data('handsontable').getData();
    var options = "";
    if (taskType == 1) {
        endPoint = api.server + api.url.csoTask + "/InterestMaintenance";
        viewModel.TransactionData().TransactionDataIM(AddDataExcel);
    }
    else {
        endPoint = api.server + api.url.csoTask + "/RolloverSettlement";
        viewModel.TransactionData().TransactionDataRO(AddDataExcel);
    }

    options = {
        url: endPoint,
        token: accessToken,
        data: ko.toJSON(viewModel.TransactionData())
    };

    if (viewModel.TransactionData().Documents().length == viewModel.Documents().length) {
        Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
    }
}

function OnSuccessSaveAPI(data, textStatus, jqXHR) {
    if (data.DataList != null || data.DataList != undefined) {
        var i = 0;
        for (i = 0; i < data.DataList.length; i++) {
            viewModel.ApplicationID(data.DataList[i].ApplicationID);
            viewModel.TransactionID(data.DataList[i].transactionID);
            viewModel.Title(data.DataList[i].Title);
            AddListItemLoan();
        }

    }
}

function GetTransactionDataLoan(instanceId, approverId) {
    var endPointURL;
    endPointURL = api.server + api.url.workflowloan.getppucheckerloan(instanceId, approverId);
    return endPointURL;
}

function AddListItemLoan() {
    var body = {
        Title: viewModel.Title(),
        Application_x0020_ID: viewModel.ApplicationID(),
        Transaction_x0020_ID: viewModel.TransactionID(),
        Initiator_x0020_GroupId: GetUserRole(viewModel.SPUser().Roles),
        __metadata: {
            type: config.sharepoint.metadata.listLoan
        }
    };

    var options = {
        url: config.sharepoint.url.api + "/Lists(guid'" + config.sharepoint.listIdLoan + "')/Items",
        data: JSON.stringify(body),
        digest: jQuery("#__REQUESTDIGEST").val()
    };

    Helper.Sharepoint.List.Add(options, OnSuccessAddListItem, OnError, OnAlways);
}

function OnSuccessAddListItem(data, textStatus, jqXHR) {
    ShowNotification('WorkFlow Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);
    window.location = "/home";
}
function OnAlways() {
    //$box.trigger('reloaded.ace.widget');
    //viewModel.IsEditable(true);
}
function GetUserRole(userData) {
    var sValue;
    if (userData != undefined && userData.length > 0) {
        $.each(userData, function (index, item) {
            if (Const_RoleName[item.ID].toLowerCase().startsWith("dbs ppu") && Const_RoleName[item.ID].toLowerCase().endsWith("maker")) { //if (item.Name.toLowerCase().startsWith("dbs ppu") && item.Name.toLowerCase().endsWith("maker")) {
                sValue = item.ID;
            }
        });
    }
    if (sValue == undefined) {
        sValue = userData[0].ID;
    }
    return sValue;
}

function reinitDOM() {
    $('ul').on('click', 'li', function () {
        alert($(this).text());
    });
    $('#dataTable').bind('click', 'a.attAddhocDoc', function () {
        attachRow = $(this).attr('id').replace('attach_', '');
        $("#modal-form-uploadAddHoc").modal('show');
        $('.remove').click();
    });
}

var accessToken;
var $container;
var attachRow = 0;
var viewModel = new ViewModel();

$.holdReady(true);
var intervalRoleName = setInterval(function () {
    if (Object.keys(Const_RoleName).length != 0) {
        $.holdReady(false);
        clearInterval(intervalRoleName);
    }
}, 100);

//$(document).on("RoleNameReady", function() {
$(document).ready(function () {
    var InterestMaintenance = {};
    var InterestMaintenanceList = [];
    $('.date-picker').datepicker({
        autoclose: true,
        onSelect: function () {
            this.focus();
        }
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });
    $('.date-picker').val(LocalDate(new Date(), true, false));

    $(document).on('click', '.attAddhocDoc', function () {
        attachRow = $(this).attr('id').replace('attach_', '');
        $("#modal-form-uploadAddHoc").modal('show');
        $('.remove').click();
    });

    $(document).on('click', '.calculateDOA', function () {
        attachRow = $(this).attr('id').replace('doa_', '');
        calculateDOA(attachRow);
    });
    $(document).on('click', '.calculateRate', function () {
        attachRow = $(this).attr('id').replace('rate_', '');
        calculateRate(attachRow);
    });
    $(document).on('click', '.calculateAllInRate', function () {
        attachRow = $(this).attr('id').replace('allInRate_', '');
        calculateAllInRate(attachRow);
    });
    $(document).on('click', '.chooseInstruction', function () {
        attachRow = $(this).attr('id').replace('instruction_', '');
        var handsontable = $container.data('handsontable');
        var maintenanceType = handsontable.getDataAtRowProp(attachRow, "MaintenanceTypeName");
        var cif = handsontable.getDataAtRowProp(attachRow, "CIF");
        if (maintenanceType == "Rollover") {
            GetCSOTaskTransaction(cif);
            $("#modal-form-instruction").modal('show');
            $('.remove').click();
        } else {
            ShowNotification("Task Validation Warning", "Please Select Maintenance Type", 'gritter-warning', true);
            return;
        }
    });

    $(document).on('click', '.detailInstruction', function () {
        attachRow = $(this).attr('id').replace('detailinstruction_', '');
        var handsontable = $container.data('handsontable');
        var instandID = handsontable.getDataAtRowProp(attachRow, "InstructionID");
        if (instandID == null) {
            ShowNotification("Task Validation Warning", "Please Select Instruction", 'gritter-warning', true);
            return;
        }
        else {
            var options = {
                url: api.server + api.url.workflowloan.loanCSODetail(instandID),
                token: accessToken
            };

            Helper.Ajax.Get(options, OnSuccessGetCSOTaskDetail, OnError, OnAlways);

            $("#modal-form-detail-loan").modal('show');
        }

        $('.remove').click();
    });

    // Knockout custom file handler
    ko.bindingHandlers.file = {
        init: function (element, valueAccessor) {
            $(element).change(function () {
                var file = this.files[0];

                if (ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };


    ko.dependentObservable(function () {

        var docType = ko.utils.arrayFirst(viewModel.ddlDocumentTypes(), function (item) {
            return item.ID == viewModel.Selected().DocumentType();

        });

        if (docType != null) {
            viewModel.DocumentType(docType);
        }

    });

    // Knockout Bindings
    ko.applyBindings(viewModel);
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(OnSuccessToken, OnError);

    }
    else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);
            viewModel.SPUser(ko.mapping.toJS(spUser));
        }
        viewModel.SPUser(ko.mapping.toJS(spUser));
    }
});

function calculateDOA(attachRow) {
    var taskType = $("#cso-task-type").val();
    var endPoint = "";
    if (taskType == 1) endPoint = api.server + api.url.csoTask + "/IMApprovalDOA"
    else endPoint = api.server + api.url.csoTask + "/ROApprovalDOA"
    var handsontable = $container.data('handsontable');
    var options = {
        url: endPoint,
        token: accessToken,
        data: ko.toJSON(handsontable.getData()[attachRow])
    };

    Helper.Ajax.Post(options, function (result, textStatus, jqXHR) {
        var doa = result.ApprovalDOA;
        if (doa != undefined) {
            var handsontable = $container.data('handsontable');
            //for (var i = 0; i < doa.length; i++) {
            //handsontable.setDataAtRowProp(doa[i].RowID, "ApprovalDOA", doa[i].ApprovalDOAString);
            handsontable.setDataAtRowProp(attachRow, "ApprovalDOA", doa.ApprovalDOAString);
            //}
        }
    }, OnError);
}

function calculateAllInRate(attachRow) {
    var handsontable = $container.data('handsontable');
    var data = handsontable.getData();
    //for (var i = 0; i < data.length; i++) {
    var currCode = handsontable.getDataAtRowProp(attachRow, "CurrencyName");
    var intRateCode = handsontable.getDataAtRowProp(attachRow, "InterestRateCodeName");
    var baseRate = handsontable.getDataAtRowProp(attachRow, "BaseRate");
    var accPref = handsontable.getDataAtRowProp(attachRow, "AccountPreferencial");
    var specFTP = handsontable.getDataAtRowProp(attachRow, "SpecialFTP");
    var specRate = handsontable.getDataAtRowProp(attachRow, "SpecialRate");
    var taskType = $("#cso-task-type").val();
    if (taskType == 1) specRate = handsontable.getDataAtRowProp(attachRow, "SpecialRate");
    else specRate = handsontable.getDataAtRowProp(attachRow, "SpecialRateMargin");
    var allInRate = "0";
    if (currCode == "" || intRateCode == "") {
        allInRate = "0";
    }
    else {
        if (specFTP == 0 && specRate == 0) {
            allInRate = parseFloat(baseRate) + parseFloat(accPref);
        }
        else {
            var data1 = parseFloat(baseRate) + parseFloat(accPref);
            var data2 = parseFloat(specFTP) + parseFloat(specRate);
            if (data1 == data2.toFixed(2)) {
                allInRate = parseFloat(baseRate) + parseFloat(accPref);
            }
            else {
                allInRate = "0";
                var text = "<h4></h4><div><h5><b style='color:#FF0000;'>Mismatch All In Rate !!!</b></h5></div>";
                bootbox.alert(text, function (result) {

                });
            }
        }
    }

    if (allInRate != "0") {
        handsontable.setDataAtRowProp(attachRow, "AllinRate", allInRate.toFixed(4));
    } else {
        handsontable.setDataAtRowProp(attachRow, "AllinRate", allInRate);
    }
    //}
}

function calculateRate(attachRow) {
    var taskType = $("#cso-task-type").val();
    var endPoint = "";
    if (taskType == 1) endPoint = api.server + api.url.csoTask + "/IMBaseRate"
    else endPoint = api.server + api.url.csoTask + "/ROBaseRate"
    var handsontable = $container.data('handsontable');
    var options = {
        url: endPoint,
        token: accessToken,
        data: ko.toJSON(handsontable.getData())
    };

    Helper.Ajax.Post(options, function (result, textStatus, jqXHR) {
        var rate = result.BaseRate;
        if (rate != undefined) {
            var handsontable = $container.data('handsontable');
            handsontable.setDataAtRowProp(attachRow, "BaseRate", rate[attachRow].BaseRate.toFixed(4));
        }
    }, OnError);
}


function GenerateExcel() {
    var selectedType = $("#cso-task-type").val();
    var selectedDate = $("#TaskDate").val();
    if (selectedType == 0) {
        ShowNotification("Form Validation Warning", "Please choose CSO Task Type", 'gritter-warning', false);
        return false;
    }

    if (selectedDate == "") {
        ShowNotification("Form Validation Warning", "Please input Date", 'gritter-warning', false);
        return false;
    }

    GetParameters();
    GetInterestRates();
    GetMaintenanceTypes();
    GetDataExcel();
}

function LocalDate(date, isDateOnly, isDateLong) {
    var localDate = new Date(date);
    if (date == '1970/01/01 00:00:00' || date == null) {
        return "";
    }

    if (moment(localDate).isValid()) {
        if (isDateOnly != undefined || isDateOnly == true) {
            if (isDateLong)
                return moment(localDate).format(config.format.dateLong);
            else
                return moment(localDate).format(config.format.date);
        } else {
            return moment(localDate).format(config.format.dateTime);
        }
    } else {
        return ""
    }
};

function CheckEmailApp() {
    var docs = viewModel.Documents();
    var EmailApp = config.validate.approval.emailapp;
    var isAttachedEmail = false;
    for (var i = 0; i < docs.length; i++) {
        if (EmailApp.indexOf(docs[i].Type["Name"]) > -1) {
            isAttachedEmail = true;
        }
    }
    return isAttachedEmail;
}

function OnSuccessToken(data, textStatus, jqXHR) {
    accessToken = data.AccessToken;
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);

    }
    viewModel.SPUser(ko.mapping.toJS(spUser));
}

function GetUtcAttemp() {
    var options = {
        url: api.server + api.url.utcattemps,
        params: {
        },
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetUtcAttemp, OnError, OnAlways);
}

//Parameter
// get all parameters need
function GetInterestRates() {
    var options = {
        url: api.server + api.url.interestRate,
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetInterestRates, OnError, OnAlways);
}

function OnSuccessGetInterestRates(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        viewModel.InterestRates(data);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function GetMaintenanceTypes() {
    var options = {
        url: api.server + api.url.maintenanceType,
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetMaintenanceTypes, OnError, OnAlways);
}

function OnSuccessGetMaintenanceTypes(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        viewModel.MaintenanceTypes(data);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function GetParameters() {
    var options = {
        url: api.server + api.url.parameter,
        params: {
            select: "Product,Currency,Channel,BizSegment,Bank,BankCharge,AgentCharge,FXCompliance,ChargesType,ProductType,lld,UnderlyingDoc,POAFunction,DocType,PurposeDoc,LoanParameterSystem,LoanType,ProgramType,SchemeCode,AmountDisburse,Interest,RepricingPlan,Frequency,SanctionLimitCurrID,utilization,Outstanding,StatementLetter,Nostro,Sundry,CIFJENISIDENTITAS,CIFASSETBERSIH,CIFPENDPATANBULANAN,CIFPENGHASILANTAMBAHAN,CIFPEKERJAAN,CIFTUJUANPEMBUKAANREKENING,CIFPERKIRAANDANAMASUK,CIFPERKIRAANDANAKELUAR,CIFPERKIRAANTRANSAKSIKELUAR,MaintenanceType,DispatchModeType,TransactionType,RequestTypeCIF"
        },
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
}

function OnSuccessGetParameters(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };

        viewModel.Products(data.Product);
        viewModel.BizSegments(data.BizSegment);
        viewModel.Channels(data.Channel);
        viewModel.Currencies(data.Currency);
        viewModel.Parameter().DocumentTypes(ko.mapping.toJS(data.DocType, mapping));
        viewModel.ddlDocumentTypes(ko.mapping.toJS(data.DocType, mapping));
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}
//End Parameter

//Get CSO Task
function GetCSOTaskTransaction(cif) {
    var options = {
        url: api.server + api.url.csoTaskTransaction,
        params: {
            cif: cif,
            productname: 'Loan Rollover'
        },
        token: accessToken
    };
    Helper.Ajax.Get(options, OnSuccessGetCSOTask, OnError, OnAlways);
};

function OnSuccessGetCSOTask(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        //console.log(ko.toJSON(data));
        viewModel.ChooseInstruction(data);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
    }
}

function OnSuccessGetCSOTaskDetail(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        //console.log(data);
        viewModel.CSODetails(data);
        //viewModel.ChooseInstruction(data);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
    }
}
//GetCsoTask


function GetDataExcel() {
    var LogName = DecodeLogin(spUser.LoginName);
    var selectedType = $("#cso-task-type").val();
    var selectedDate = $("#TaskDate").val();
    var options = {
        url: api.server + api.url.csoTask,
        token: accessToken,
        params: {
            taskType: selectedType,
            taskDate: selectedDate,
            logname: LogName
        }
    };

    Helper.Ajax.Get(options, function (result, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            var type = $("#cso-task-type").val();
            switch (type) {
                case "1":
                    if (result.length > 0) {
                        for (var i = 0; i < result.length; i++) {
                            result[i].DueDate = viewModel.LocalDate(result[i].DueDate, true, false);
                            result[i].ValueDate = viewModel.LocalDate(result[i].ValueDate, true, false);
                            result[i].NextPrincipalDate = viewModel.LocalDate(result[i].NextPrincipalDate, true, false);
                            result[i].NextInterestDate = viewModel.LocalDate(result[i].NextInterestDate, true, false);
                            result[i].PeggingReviewDate = viewModel.LocalDate(result[i].PeggingReviewDate, true, false);
                        }
                    }
                    GenerateInterestMaintenance(result);
                    //GetCSOTaskTransaction();
                    break;
                case "2":
                case "3":
                    if (result.length > 0) {
                        for (var i = 0; i < result.length; i++) {
                            result[i].DueDate = viewModel.LocalDate(result[i].DueDate, true, false);
                            result[i].ValueDate = viewModel.LocalDate(result[i].ValueDate, true, false);
                            result[i].NextPrincipalDate = viewModel.LocalDate(result[i].NextPrincipalDate, true, false);
                            result[i].NextInterestDate = viewModel.LocalDate(result[i].NextInterestDate, true, false);
                        }
                    }
                    GenerateRolloverSettlement(result);
                    //GetCSOTaskTransaction();
                    break;
            }
        }
    }, OnError);
};

function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}


function GenerateInterestMaintenance(result) {
    if ($container != null) {
        $container.handsontable('destroy');
    }

    var colsToHide = [];
    var tdToYellow = [13, 14, 16, 17, 18, 19, 22, 23, 24, 27, 28, 29, 30];
    var tdToGreen = [1, 2, 4, 5, 6, 7, 10, 11, 21, 26];
    var tdToRed = [3, 8, 9, 12, 15];

    function getCustomRenderer() {
        return function (instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            if (colsToHide.indexOf(col) > -1) {
                //th.hidden = true;
                td.hidden = true;
            } else {
                //th.hidden = false;
                td.hidden = false;
            }
            if (tdToYellow.indexOf(col) > -1) {
                td.style.backgroundColor = 'yellow';
            }
            if (tdToGreen.indexOf(col) > -1) {
                td.style.backgroundColor = '#AFF3B7';
            }
            if (tdToRed.indexOf(col) > -1) {
                td.style.backgroundColor = '#CC0000';
            }

        }
    }

    $container = $("#dataTable");
    $container.handsontable({
        data: result,
        //fixedColumnsLeft: 8,
        minSpareCols: 1,
        //rowHeaders: true,
        colHeaders: ["</br></br></br>", "Branch ID", "<center>Scheme</br>Code</center>", "CIF", "Customer Name", "<center>Loan</br>Contract No</center>", "Due Date", "CCY", "<center>Previous</br>Interest Rate</center>", "<center>Current<br>Interest Rate</center>", "<center>IBG</br>Segment</center>", "RM", "CSO",
                    "<center>Cashback or<br>SBLC<br>Secured?<center>", "<center>PLP or Heavy<br>Equipment?<br>(for IBG 3&4 only)</center>",
                    "<center>Value</br>Date</center>", "<center>Next<br>Principal Date</center>", "<center>Next</br>Interest Date<center>", "<center>APPROVED<br>MARGIN AS PER</br>CREDIT MEMO</center>",
                    "<center>Interest</br>Rate Code</center>", "", "<center>Regular Rate</br>Base Rate</center>", "<center>Regular Rate</br>Account Preferecial</center>", "<center>Special Rate</br>Special FTP</center>",
                    "<center>Special Rate</br>Margin</center>", "", "<center>All In</br>Rate</center>", "", "Approval DOA", "Approved By", "<center>Pegging</br>Review Date</center>",
                    "Remark"],

        contextMenu: true,
        columns: [
                    {
                        data: 'Selected',
                        type: 'checkbox'
                    },
                    {
                        data: 'SolID',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'SchemeCode',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'CIF',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'CustomerName',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'LoanContractNo',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'DueDate',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'CurrencyName',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'PreviousInterestRate',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'CurrentInterestRate',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'BizSegmentName',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'RM',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'CSO',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'IsSBLCSecured',
                        renderer: getCustomRenderer(),
                        type: 'dropdown',
                        source: ["No", "Yes"]
                    },
                    {
                        data: 'IsHeavyEquipment',
                        renderer: getCustomRenderer(),
                        type: 'dropdown',
                        source: ["No", "Yes"]
                    },
                    {
                        data: 'ValueDate',
                        type: 'date',
                        dateFormat: 'DD-MMM-YYYY',
                        correctFormat: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'NextPrincipalDate',
                        type: 'date',
                        dateFormat: 'DD-MMM-YYYY',
                        correctFormat: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'NextInterestDate',
                        type: 'date',
                        dateFormat: 'DD-MMM-YYYY',
                        correctFormat: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'ApprovedMarginAsPerCM',
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'InterestRateCodeName',
                        type: 'dropdown',
                        renderer: getCustomRenderer(),
                        source: dataDropDown(),
                        width: 150
                    },
                    {
                        data: 'CalculateRate',
                        renderer: rateRenderer
                    },
                    {
                        data: 'BaseRate',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'AccountPreferencial',
                        renderer: getCustomRenderer()
                    },
                    //{
                    //    data: 'AllInFTPRate',
                    //    renderer: getCustomRenderer()
                    //},
                    {
                        data: 'SpecialFTP',
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'SpecialRate',
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'CalculateAllInRate',
                        renderer: allInRateRenderer
                    },
                    {
                        data: 'AllinRate',
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'Calculate',
                        renderer: doaRenderer
                    },
                    {
                        data: 'ApprovalDOA',
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'ApprovedBy',
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'PeggingReviewDate',
                        type: 'date',
                        dateFormat: 'DD-MMM-YYYY',
                        correctFormat: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'Remarks',
                        renderer: getCustomRenderer()
                    }
                    //,
                    //{
                    //    data: 'Attach',
                    //    renderer: attachRenderer
                    //},
                    //{
                    //    data: 'Attachment',
                    //    renderer: getCustomRenderer()
                    //}
                    //,
                    //{
                    //    data: 'TransactionDetailLink',
                    //    renderer: detailinstructionRenderer
                    //},
                    //{
                    //    data: 'ChooseTransactionDetail',
                    //    renderer: instructionRenderer
                    //}
        ]
    });

    $('div.ht_clone_top.handsontable').css('display', 'none');
    //$('div.ht_master.handsontable>div.wtHolder').css('width', 1290 + 'px'); Rizki 2017-06-15
    $('div.ht_master.handsontable>div.wtHolder').css('min-height', 400 + 'px');
    $('div.ht_master.handsontable>div.wtHolder').css('max-height', 400 + 'px');
    var handsontable = $container.data('handsontable');
    //Hide();
}

function dataDropDown() {
    var selectedId;

    var optionsList = viewModel.InterestRates();

    var values = ("InterestRateCodeID" + "").split(",");
    var value = [];
    for (var index = 0; index < optionsList.length; index++) {
        if ((optionsList[index].InterestRateCodeID) > -1) {
            selectedId = optionsList[index].InterestRateCodeID;
            value.push(optionsList[index].InterestRateCodeName);
        }
    }
    return value;
}

function dataDropDownMaintenanceType() {
    var selectedId;

    var optionsList = viewModel.MaintenanceTypes();

    var values = ("MaintenanceTypeID" + "").split(",");
    var value = [];
    for (var index = 0; index < optionsList.length; index++) {
        if ((optionsList[index].MaintenanceTypeID) > -1) {
            selectedId = optionsList[index].MaintenanceTypeID;
            value.push(optionsList[index].MaintenanceTypeName);
        }
    }
    return value;
}

function attachRenderer(instance, td, row, col, prop, value, cellProperties) {
    var handsontable = $container.data('handsontable');
    var getcolor = handsontable.getDataAtRowProp(row, "IsAdhoc");
    if (getcolor == "Yes") {
        var escaped = Handsontable.helper.stringify("<a href='javascript:void(0)' class='attAddhocDoc' id='attach_" + row + "'>Attach</a>");
        td.innerHTML = escaped;
    }
    else {
        var escaped = Handsontable.helper.stringify("<a href='javascript:void(0)' class='attAddhocDoc' id='attach_" + row + "'></a>");
        td.innerHTML = escaped;
    }
    return td;
}

function doaRenderer(instance, td, row, col, prop, value, cellProperties) {
    var escaped = Handsontable.helper.stringify("<a href='javascript:void(0)' class='calculateDOA' id='doa_" + row + "'>Get</a>");
    td.innerHTML = escaped;

    var tdToBlack = [17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 37, 38];
    var handsontable = $container.data('handsontable');
    var getcolor = handsontable.getDataAtRowProp(row, "MaintenanceTypeName");
    if (getcolor == "Payoff") {
        if (tdToBlack.indexOf(col) > -1) {
            td.style.backgroundColor = '#CC0000';
            td.innerHTML = "";
        }
    }

    return td;
}
function allInRateRenderer(instance, td, row, col, prop, value, cellProperties) {
    var escaped = Handsontable.helper.stringify("<a href='javascript:void(0)' class='calculateAllInRate' id='allInRate_" + row + "'>Get</a>");
    td.innerHTML = escaped;

    var tdToBlack = [17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 37, 38];
    var handsontable = $container.data('handsontable');
    var getcolor = handsontable.getDataAtRowProp(row, "MaintenanceTypeName");
    if (getcolor == "Payoff") {
        if (tdToBlack.indexOf(col) > -1) {
            td.style.backgroundColor = '#CC0000';
            td.innerHTML = "";
        }
    }

    return td;
}
function rateRenderer(instance, td, row, col, prop, value, cellProperties) {
    var escaped = Handsontable.helper.stringify("<a href='javascript:void(0)' class='calculateRate' id='rate_" + row + "'>Get</a>");
    td.innerHTML = escaped;

    var tdToBlack = [17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 37, 38];
    var handsontable = $container.data('handsontable');
    var getcolor = handsontable.getDataAtRowProp(row, "MaintenanceTypeName");
    if (getcolor == "Payoff") {
        if (tdToBlack.indexOf(col) > -1) {
            td.style.backgroundColor = '#CC0000';
            td.innerHTML = "";
        }
    }

    return td;
}

function instructionRenderer(instance, td, row, col, prop, value, cellProperties) {
    var escaped = Handsontable.helper.stringify("<a href='javascript:void(0)' class='chooseInstruction' id='instruction_" + row + "'>Choose</a>");
    td.innerHTML = escaped;
    var tdToBlack = [17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 37, 38];
    var handsontable = $container.data('handsontable');
    var getcolor = handsontable.getDataAtRowProp(row, "MaintenanceTypeName");
    if (getcolor == "Payoff") {
        if (tdToBlack.indexOf(col) > -1) {
            td.style.backgroundColor = '#CC0000';
            td.innerHTML = "";
        }
    }

    return td;
}

function detailinstructionRenderer(instance, td, row, col, prop, value, cellProperties) {
    var escaped = Handsontable.helper.stringify("<a href='javascript:void(0)' class='detailInstruction' id='detailinstruction_" + row + "'>Detail</a>");
    td.innerHTML = escaped;
    var tdToBlack = [17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 37, 38];
    var handsontable = $container.data('handsontable');
    var getcolor = handsontable.getDataAtRowProp(row, "MaintenanceTypeName");
    if (getcolor == "Payoff") {
        if (tdToBlack.indexOf(col) > -1) {
            td.style.backgroundColor = '#CC0000';
            td.innerHTML = "";
        }
    }

    return td;
}

function GenerateRolloverSettlement(result) {
    if ($container != null) {
        $container.handsontable('destroy');
    }

    var colsToHide = [];
    var tdToYellow = [13, 14, 15, 16, 18, 19, 20, 21, 24, 25, 29];
    var tdToGreen = [1, 2, 4, 5, 6, 7, 8, 10, 11, 23, 28, 30];
    var tdToRed = [3, 9, 12, 17];

    var tdToBlack = [17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 37, 38];

    function getCustomRenderer() {
        return function (instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            if (colsToHide.indexOf(col) > -1) {
                //th.hidden = true;
                td.hidden = true;
            } else {
                //th.hidden = false;
                td.hidden = false;
            }
            if (tdToYellow.indexOf(col) > -1) {
                td.style.backgroundColor = 'yellow';
            }
            if (tdToGreen.indexOf(col) > -1) {
                td.style.backgroundColor = '#AFF3B7';
            }
            if (tdToRed.indexOf(col) > -1) {
                td.style.backgroundColor = '#CC0000';
            }
            var handsontable = $container.data('handsontable');
            var getcolor = handsontable.getDataAtRowProp(row, "MaintenanceTypeName");
            if (getcolor == "Payoff") {
                if (tdToBlack.indexOf(col) > -1) {
                    td.style.backgroundColor = '#CC0000';
                    //td.style.Color = '#CC0000';
                }
            }
        }
    }

    $container = $("#dataTable");
    $container.handsontable({
        data: result,
        //fixedColumnsLeft: 8,
        minSpareCols: 1,
        //rowHeaders: true,
        colHeaders: ["</br></br></br>", "Branch ID", "<center>Scheme</br>Code</center>", "CIF", "Customer Name",
                     "<center>Loan</br>Contract No</center>", "Due Date", "CCY", "Amount Due", "Limit ID", "IBG Segment",
                     "RM", "CSO", "<center>Cashback or<br>SBLC<br>Secured?<center>", "<center>PLP or Heavy<br>Equipment?<br>(for IBG 3&4 only)</center>", "<center>Maintenance</br>Type Name</center>",
                     "<center>Amount</br>Rollover</center>", "Value Date", "<center>Next</br>Principal Date</center>", "<center>Next</br>Interest Date</center>",
                     "<center>Approved</br>Margin As</br>Per Credit Memo</center>", "<center>Interest</br>Rate Code</center>", "",
                     "<center>Regular Rate</br>Base Rate</center>", "<center>Regular Rate</br>Account Preferecial</center>", "Special Rate</br>Special FTP", "Special Rate</br>Margin", "", "All In Rate", "",
                     "Approval DOA", "Approved By", "Remark CSO", "Is Adhoc", "Attach", "Attachment", "Document Type",
                     "<center>Transaction</br>Detail Link</center>", "<center>Choose</br>Transaction Detail</br></center>"],
        contextMenu: true,
        columns: [
                    {
                        data: 'Selected',
                        type: 'checkbox'
                    },
                    {
                        data: 'SolID',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'SchemeCode',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'CIF',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'CustomerName',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'LoanContractNo',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'DueDate',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'CurrencyName',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'AmountDue',
                        type: 'numeric',
                        format: '0,0.00',
                        language: 'en',
                        readOnly: true
                        //renderer: getCustomRenderer()
                    },
                    {
                        data: 'LimitID',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'BizSegmentName',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'RM',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'CSO',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'IsSBLCSecured',
                        renderer: getCustomRenderer(),
                        type: 'dropdown',
                        source: ["No", "Yes"]
                    },
                    {
                        data: 'IsHeavyEquipment',
                        renderer: getCustomRenderer(),
                        type: 'dropdown',
                        source: ["No", "Yes"]
                    },
                    {
                        data: 'MaintenanceTypeName',
                        type: 'dropdown',
                        source: dataDropDownMaintenanceType(),
                        renderer: getCustomRenderer(),
                        width: 150
                    },
                    {
                        data: 'AmountRollover',
                        type: 'numeric',
                        format: '0,0.00',
                        language: 'en'
                        //readOnly: true
                        //renderer: getCustomRenderer()
                    },
                    {
                        data: 'ValueDate',
                        type: 'date',
                        dateFormat: 'DD-MMM-YYYY',
                        correctFormat: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'NextPrincipalDate',
                        type: 'date',
                        dateFormat: 'DD-MMM-YYYY',
                        correctFormat: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'NextInterestDate',
                        type: 'date',
                        dateFormat: 'DD-MMM-YYYY',
                        correctFormat: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'ApprovedMarginAsPerCM',
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'InterestRateCodeName',
                        type: 'dropdown',
                        source: dataDropDown(),
                        renderer: getCustomRenderer(),
                        width: 150
                    },
                    {
                        data: 'CalculateRate',
                        renderer: rateRenderer
                    },
                    {
                        data: 'BaseRate',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'AccountPreferencial',
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'SpecialFTP',
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'SpecialRateMargin',
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'CalculateAllInRate',
                        renderer: allInRateRenderer
                    },
                    {
                        data: 'AllinRate',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'Calculate',
                        renderer: doaRenderer
                    },
                    {
                        data: 'ApprovalDOA',
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'ApprovedBy',
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'Remarks',
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'IsAdhoc',
                        readOnly: true,
                        renderer: getCustomRenderer()
                    },
                    {
                        data: 'Attach',
                        renderer: attachRenderer
                    },
                    {
                        data: 'Attachment'
                    },
                    {
                        data: 'DocumentType'
                    },
                    {
                        data: 'TransactionDetailLink',
                        renderer: detailinstructionRenderer
                    },
                    {
                        data: 'ChooseTransactionDetail',
                        renderer: instructionRenderer
                    }
        ]
    });

    $('div.ht_clone_top.handsontable').css('display', 'none');
    //$('div.ht_master.handsontable>div.wtHolder').css('width', 1290 + 'px'); Rizki 2017-06-15
    $('div.ht_master.handsontable>div.wtHolder').css('min-height', 400 + 'px');
    $('div.ht_master.handsontable>div.wtHolder').css('max-height', 400 + 'px');
    var handsontable = $container.data('handsontable');
}

function UploadFileRecuresive(context, document, callBack, numFile) {
    var indexDocument = document.length - numFile;

    serverRelativeUrlToFolder = '/Instruction Documents';

    var parts = document[indexDocument].DocumentPath.name.split('.');
    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp + "." + fileExtension;

    var serverUrl = _spPageContextInfo.webAbsoluteUrl;
    var output;

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBuffer();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolder(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                // Change the display name and title of the list item.
                var changeItem = updateListItem(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    //alert('file uploaded and updated');
                    //return output;
                    var newDoc = {
                        ID: document[indexDocument].ID,
                        Type: document[indexDocument].Type,
                        Purpose: document[indexDocument].Purpose,
                        LastModifiedDate: null,
                        LastModifiedBy: null,
                        DocumentPath: output.ServerRelativeUrl,
                        FileName: document[indexDocument].DocumentPath.name
                    };

                    if (newDoc.ID == 0) {
                        viewModel.TransactionData().Documents.push(newDoc);
                    }
                    else {
                        viewModel.TransactionData().DocumentsAddHoc.push(newDoc);
                    }


                    if (numFile > 1) {
                        UploadFileRecuresive(context, document, callBack, numFile - 1); // recursive function
                    }
                    callBack();
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);
    // Get the local file as an array buffer.
    function getFileBuffer() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }

        if (document[indexDocument].DocumentPath.type == Util.spitemdoc) {
            var fileURI = serverUrl + document[indexDocument].DocumentPath.DocPath;
            // load file:
            fetch(fileURI, convert, alert);

            function convert(buffer) {
                var blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });

                var domURL = self.URL || self.webkitURL || self,
                  url = domURL.createObjectURL(blob),
                  img = new Image;

                img.onload = function () {
                    domURL.revokeObjectURL(url); // clean up
                    //document.body.appendChild(this);
                    // this = image
                };
                img.src = url;
                reader.readAsArrayBuffer(blob);
            }

            function fetch(url, callback, error) {

                var xhr = new XMLHttpRequest();
                try {
                    xhr.open("GET", url);
                    xhr.responseType = "arraybuffer";
                    xhr.onerror = function () {
                        error("Network error")
                    };
                    xhr.onload = function () {
                        if (xhr.status === 200) callback(xhr.response);
                        else error(xhr.statusText);
                    };
                    xhr.send();
                } catch (err) {
                    error(err.message)
                }
            }


        } else {
            reader.readAsArrayBuffer(document[indexDocument].DocumentPath);
        }
        //bangkit

        //reader.readAsDataURL(document.DocumentPath);
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(arrayBuffer) {

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                //"content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItem(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItem(itemMetadata) {
        var body = {
            Title: document[indexDocument].DocumentPath.name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: document[indexDocument].Type.Name,
            //Document_x0020_Purpose: document[indexDocument].Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}
