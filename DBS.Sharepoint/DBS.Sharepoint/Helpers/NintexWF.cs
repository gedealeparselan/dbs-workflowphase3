﻿using Nintex.Workflow.HumanApproval;
using Microsoft.SharePoint;
using System;
using System.Collections;


namespace DBS.Sharepoint.Helpers
{
    public class NintexWF
    {
        #region Variables
        // The Nintex Workflow representation of the task
        private NintexTask _task = null;

        // The Nintex Workflow representation of the approver
        private Approver _approver = null;

        // The SharePoint task list containing the workflow task
        private SPList _taskList = null;

        // The sharepoint task the user interacts with
        private SPListItem _spTaskItem = null;

        // The item the workflow is running on (null for site workflows)
        private SPListItem _item = null;
        #endregion

        public bool RespondApprovalTask(SPWeb web, Guid taskListID, int taskID, int outcomeID, string comments, ref string errorMessage)
        {
            bool result = false;

            SPListCollection lists = web.Lists;
            lists.ListsForCurrentUser = true;

            SPList taskList = null;
            try
            {
                //taskList = web.Lists[taskListName];
                _taskList = web.Lists.GetList(taskListID, true);
                _spTaskItem = _taskList.GetItemById(taskID);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                goto Finish;
            }

            // Retrieve the Nintex Workflow object model representation of the task
            _task = NintexTask.RetrieveTask(taskID, web, _taskList);
            //Hashtable ht = _task.WFContext.Variables;
            
            // Retrieve the Nintex Workflow object model representation of the approver for this SharePoint task item id.
            // A Nintex Workflow task can have multiple approvers, and each approver will have a separated SharePoint task item.
            _approver = _task.Approvers.GetBySPId(taskID);

            if (_approver == null)
            {
                errorMessage = string.Format("Cannot find an approver for SharePoint task item {0}.", taskID);
                goto Finish;
            }

            if (!Nintex.Workflow.HumanApproval.User.CheckCurrentUserMatchesHWUser(web, _approver))
            {
                errorMessage = string.Format("Hi {0}, You are not authorized to approve this task. ", web.CurrentUser.Name);
                goto Finish;
            }

            try
            {
                if (_approver.ApprovalOutcome != Outcome.Pending)
                {
                    errorMessage = "The task is no longer active. Another user may have already completed the task.";
                    goto Finish;
                }

                // Update the properties on the task item that will trigger the workflow to continue
                if (_task is ApprovalTask)
                {
                   // Outcome decision = (isApproved) ? Outcome.Approved : Outcome.Rejected;

                    _spTaskItem[Nintex.Workflow.Common.NWSharePointObjects.FieldDecision] = outcomeID;
                    _spTaskItem[Nintex.Workflow.Common.NWSharePointObjects.FieldComments] = comments;
                }
                else
                {
                    errorMessage = "The task is not an approval task.";
                    goto Finish;
                }

                try
                {
                    _spTaskItem.Update();
                }
                catch
                {
                    //Unlock
                    _approver.UpdateTaskLocked(false);
                    throw;
                }

                result = true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }


            Finish:
            return result;
        }

        public bool RespondReviewTask(SPWeb web, Guid taskListID, int taskID, string comments, ref string errorMessage)
        {
            bool result = false;

            SPListCollection lists = web.Lists;
            lists.ListsForCurrentUser = true;

            SPList taskList = null;
            try
            {
                //taskList = web.Lists[taskListName];
                _taskList = web.Lists.GetList(taskListID, true);
                _spTaskItem = _taskList.GetItemById(taskID);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                goto Finish;
            }

            // Retrieve the Nintex Workflow object model representation of the task
            _task = NintexTask.RetrieveTask(taskID, web, _taskList);

            // Retrieve the Nintex Workflow object model representation of the approver for this SharePoint task item id.
            // A Nintex Workflow task can have multiple approvers, and each approver will have a separated SharePoint task item.
            _approver = _task.Approvers.GetBySPId(taskID);

            if (_approver == null)
            {
                errorMessage = string.Format("Cannot find an approver for SharePoint task item {0}.", taskID);
                goto Finish;
            }

            if (!Nintex.Workflow.HumanApproval.User.CheckCurrentUserMatchesHWUser(web, _approver))
            {
                errorMessage = string.Format("Hi {0}, You are not authorized to review this task. ", web.CurrentUser.Name);
                goto Finish;
            }

            try
            {
                if (_approver.ApprovalOutcome != Outcome.Pending)
                {
                    errorMessage = "The task is no longer active. Another user may have already completed the task.";
                    goto Finish;
                }

                // Update the properties on the task item that will trigger the workflow to continue
                if (_task is ReviewTask)
                {
                    _spTaskItem[Nintex.Workflow.Common.NWSharePointObjects.FieldDecision] = (int)Outcome.Continue;
                    _spTaskItem[Nintex.Workflow.Common.NWSharePointObjects.FieldComments] = comments;
                }
                else
                {
                    errorMessage = "The task is not an approval task.";
                    goto Finish;
                }

                try
                {
                    _spTaskItem.Update();
                }
                catch
                {
                    //Unlock
                    _approver.UpdateTaskLocked(false);
                    throw;
                }

                result = true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }


        Finish:
            return result;
        }

        public bool RespondFlexiTask(SPWeb web, string taskListName, int taskID, ConfiguredOutcome outcome, string comments, ref string errorMessage)
        {
            bool result = false;

            SPListCollection lists = web.Lists;
            lists.ListsForCurrentUser = true;

            SPList taskList = null;
            try
            {
                taskList = web.Lists[taskListName];
                _taskList = web.Lists[taskListName];
                _spTaskItem = _taskList.GetItemById(taskID);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                goto Finish;
            }

            // Retrieve the Nintex Workflow object model representation of the task
            _task = NintexTask.RetrieveTask(taskID, web, _taskList);

            // Retrieve the Nintex Workflow object model representation of the approver for this SharePoint task item id.
            // A Nintex Workflow task can have multiple approvers, and each approver will have a separated SharePoint task item.
            _approver = _task.Approvers.GetBySPId(taskID);

            if (_approver == null)
            {
                errorMessage = string.Format("Cannot find an approver for SharePoint task item {0}.", taskID);
                goto Finish;
            }

            if (!Nintex.Workflow.HumanApproval.User.CheckCurrentUserMatchesHWUser(web, _approver))
            {
                errorMessage = string.Format("Hi {0}, You are not authorized to respond this flexi task. ", web.CurrentUser.Name);
                goto Finish;
            }

            try
            {
                if (_approver.ApprovalOutcome != Outcome.Pending)
                {
                    errorMessage = "The task is no longer active. Another user may have already completed the task.";
                    goto Finish;
                }

                // Update the properties on the task item that will trigger the workflow to continue
                if (_task is MultiOutcomeTask)
                {
                    _spTaskItem[Nintex.Workflow.Common.NWSharePointObjects.FieldDecision] = (int)outcome.Id;
                    _spTaskItem[Nintex.Workflow.Common.NWSharePointObjects.FieldComments] = comments;
                }
                else
                {
                    errorMessage = "The task is not an flexi task.";
                    goto Finish;
                }

                try
                {
                    _spTaskItem.Update();
                }
                catch
                {
                    //Unlock
                    _approver.UpdateTaskLocked(false);
                    throw;
                }

                result = true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }


        Finish:
            return result;
        }

        public bool RespondFlexiTask(SPWeb web, Guid taskListID, int taskID, int outcomeID, string comments, ref string errorMessage)
        {
            bool result = false;

            SPListCollection lists = web.Lists;
            lists.ListsForCurrentUser = true;

            SPList taskList = null;
            try
            {
                //taskList = web.Lists[taskListName];
                _taskList = web.Lists.GetList(taskListID, true);
                _spTaskItem = _taskList.GetItemById(taskID);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                goto Finish;
            }

            // Retrieve the Nintex Workflow object model representation of the task
            _task = NintexTask.RetrieveTask(taskID, web, _taskList);
            
            // Retrieve the Nintex Workflow object model representation of the approver for this SharePoint task item id.
            // A Nintex Workflow task can have multiple approvers, and each approver will have a separated SharePoint task item.
            _approver = _task.Approvers.GetBySPId(taskID);
            
            if (_approver == null)
            {
                errorMessage = string.Format("Cannot find an approver for SharePoint task item {0}.", taskID);
                goto Finish;
            }

            if (!Nintex.Workflow.HumanApproval.User.CheckCurrentUserMatchesHWUser(web, _approver))
            {
                errorMessage = string.Format("Hi {0}, You are not authorized to respond this flexi task. ", web.CurrentUser.Name);
                goto Finish;
            }

            try
            {
                if (_approver.ApprovalOutcome != Outcome.Pending)
                {
                    errorMessage = "The task is no longer active. Another user may have already completed the task.";
                    goto Finish;
                }

                // Update the properties on the task item that will trigger the workflow to continue
                if (_task is MultiOutcomeTask)
                {
                    // validate the outcome id
                    ConfiguredOutcomeCollection outcomes = _approver.AvailableOutcomeInfo.AvailableOutcomes;
                    if (outcomes.GetById(outcomeID) == null)
                    {
                        errorMessage = "The outcome id for current flexi task is invalid";
                        goto Finish;
                    }

                    _spTaskItem[Nintex.Workflow.Common.NWSharePointObjects.FieldDecision] = outcomeID;
                    _spTaskItem[Nintex.Workflow.Common.NWSharePointObjects.FieldComments] = comments;
                }
                else
                {
                    errorMessage = "The task is not an flexi task.";
                    goto Finish;
                }

                try
                {
                    _spTaskItem.Update();
                }
                catch
                {
                    //Unlock
                    _approver.UpdateTaskLocked(false);
                    throw;
                }

                result = true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }


        Finish:
            return result;
        }

        public bool GetOutcomeFlexiTask(SPWeb web, Guid taskListID, int taskID, ref ConfiguredOutcomeCollection outcomes, ref string errorMessage)
        {
            bool result = false;

            SPListCollection lists = web.Lists;
            lists.ListsForCurrentUser = true;

            SPList taskList = null;
            try
            {
                //taskList = web.Lists[taskListName];
                _taskList = web.Lists.GetList(taskListID, true);
                _spTaskItem = _taskList.GetItemById(taskID);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                goto Finish;
            }

            // Retrieve the Nintex Workflow object model representation of the task
            _task = NintexTask.RetrieveTask(taskID, web, _taskList);

            // Retrieve the Nintex Workflow object model representation of the approver for this SharePoint task item id.
            // A Nintex Workflow task can have multiple approvers, and each approver will have a separated SharePoint task item.
            _approver = _task.Approvers.GetBySPId(taskID);

            if (_approver == null)
            {
                errorMessage = string.Format("Cannot find an approver for SharePoint task item {0}.", taskID);
                goto Finish;
            }

            try
            {
                if (_task is MultiOutcomeTask)
                {
                    // get the outcome collections
                    outcomes = _approver.AvailableOutcomeInfo.AvailableOutcomes;
                }
                else
                {
                    errorMessage = "The task is not an flexi task.";
                    goto Finish;
                }

                result = true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }


        Finish:
            return result;
        }

        /*NintexTask task = NintexTask.RetrieveTask(ctx.TaskItem.ID, ctx.TaskItem.Web, ctx.TaskItem.ParentList);
        Approver approv = task.Approvers.GetBySPId(ctx.TaskItem.ID);
        ConfiguredOutcomeCollection outcomes = approv.AvailableOutcomeInfo.AvailableOutcomes;*/

        public bool GetTaskOutcomes(SPWeb web, string taskListName, int taskID, ref bool isAuthorize, ref bool isPending, ref ConfiguredOutcomeCollection outcomes, ref string errorMessage)
        {
            bool result = false;

            SPListCollection lists = web.Lists;
            lists.ListsForCurrentUser = true;

            SPList taskList = null;
            try
            {
                taskList = web.Lists[taskListName];
                _taskList = web.Lists[taskListName];
                _spTaskItem = _taskList.GetItemById(taskID);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                goto Finish;
            }

            // Retrieve the Nintex Workflow object model representation of the task
            _task = NintexTask.RetrieveTask(taskID, web, _taskList);

            // Retrieve the Nintex Workflow object model representation of the approver for this SharePoint task item id.
            // A Nintex Workflow task can have multiple approvers, and each approver will have a separated SharePoint task item.
            _approver = _task.Approvers.GetBySPId(taskID);

            if (_approver == null)
            {
                errorMessage = string.Format("Cannot find an approver for SharePoint task item {0}.", taskID);
                goto Finish;
            }

            try
            {
                // set is authorize to complete the task
                isAuthorize = Nintex.Workflow.HumanApproval.User.CheckCurrentUserMatchesHWUser(web, _approver);
                
                // set is pending task status
                isPending =  _approver.ApprovalOutcome == Outcome.Pending ? true : false;

                // set outcomes for current task
                if (_task is ApprovalTask)
                {
                    outcomes = new ConfiguredOutcomeCollection();

                    outcomes.Add(new ConfiguredOutcome() { Id = (int)Outcome.Approved, Name = "Approve", Description = "Approve the task." });
                    outcomes.Add(new ConfiguredOutcome() { Id = (int)Outcome.Rejected, Name = "Reject", Description = "Reject the task." });

                    if (!isAuthorize) errorMessage = "You are not authorized to approve this item.";
                }
                else if (_task is ReviewTask)
                {
                    outcomes = new ConfiguredOutcomeCollection();

                    outcomes.Add(new ConfiguredOutcome() { Id = (int)Outcome.Continue, Name = "Continue", Description = "Complete the task." });

                    if (!isAuthorize) errorMessage = "You are not authorized to review this item.";
                }
                else if (_task is MultiOutcomeTask)
                {
                    outcomes = _approver.AvailableOutcomeInfo.AvailableOutcomes;

                    if (!isAuthorize) errorMessage = "You are not authorized to respond to this task.";
                }

                result = true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }


        Finish:
            return result;
        }

        public bool GetTaskOutcomes(SPWeb web, Guid taskListID, int taskID, ref bool isAuthorize, ref bool isPending, ref ConfiguredOutcomeCollection outcomes, ref string errorMessage)
        {
            bool result = false;

            SPListCollection lists = web.Lists;
            lists.ListsForCurrentUser = true;

            //SPList taskList = null;
            try
            {
                _taskList = web.Lists.GetList(taskListID, true);
                _spTaskItem = _taskList.GetItemById(taskID);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                goto Finish;
            }

            // Retrieve the Nintex Workflow object model representation of the task
            _task = NintexTask.RetrieveTask(taskID, web, _taskList);

            // Retrieve the Nintex Workflow object model representation of the approver for this SharePoint task item id.
            // A Nintex Workflow task can have multiple approvers, and each approver will have a separated SharePoint task item.
            _approver = _task.Approvers.GetBySPId(taskID);

            if (_approver == null)
            {
                errorMessage = string.Format("Cannot find an approver for SharePoint task item {0}.", taskID);
                goto Finish;
            }

            try
            {
                // set is authorize to complete the task
                isAuthorize = Nintex.Workflow.HumanApproval.User.CheckCurrentUserMatchesHWUser(web, _approver);

                // set is pending task status
                isPending = _approver.ApprovalOutcome == Outcome.Pending ? true : false;

                // set outcomes for current task
                if (_task is ApprovalTask)
                {
                    outcomes = new ConfiguredOutcomeCollection();

                    outcomes.Add(new ConfiguredOutcome() { Id = (int)Outcome.Approved, Name = "Approve", Description = "Approve the task." });
                    outcomes.Add(new ConfiguredOutcome() { Id = (int)Outcome.Rejected, Name = "Reject", Description = "Reject the task." });

                    if (!isAuthorize) errorMessage = "You are not authorized to approve this item.";
                }
                else if (_task is ReviewTask)
                {
                    outcomes = new ConfiguredOutcomeCollection();

                    outcomes.Add(new ConfiguredOutcome() { Id = (int)Outcome.Continue, Name = "Continue", Description = "Complete the task." });

                    if (!isAuthorize) errorMessage = "You are not authorized to review this item.";
                }
                else if (_task is MultiOutcomeTask)
                {
                    outcomes = _approver.AvailableOutcomeInfo.AvailableOutcomes;

                    if (!isAuthorize) errorMessage = "You are not authorized to respond to this task.";
                }

                result = true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }


        Finish:
            return result;
        }
    }
}
