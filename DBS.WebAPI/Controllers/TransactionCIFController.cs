﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class TransactionCIFController : ApiController
    {
        private string message = string.Empty;

        private readonly ITransactionCIFRepository repository = null;

        public TransactionCIFController()
        {
            this.repository = new TransactionCIFRepository();
        }

        public TransactionCIFController(ITransactionCIFRepository repository)
        {
            this.repository = repository;
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS PPU Maker")]
        public HttpResponseMessage Post(TransactionCIFModel transaction)
        {
            long transactionID = 0;
            string ApplicationID = string.Empty;
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransaction(transaction, ref transactionID, ref  ApplicationID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new
                    {
                        ID = transactionID,
                        AppID = ApplicationID,
                        Message = transaction.IsDraft ? "Transaction has been saved as draft." : "New Transaction has been submitted."
                    });
                }
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/TransactionCIF/Draft/{id}")]
        //[Authorize(Roles = "Admin, DBS PPU Maker")]
        public HttpResponseMessage PutDraft(long id, TransactionCIFModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTransactionDraft(id, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction data has been updated." });
                }
            }
        }

        [ResponseType(typeof(TransactionCIFDraftModel))]
        [Route("api/TransactionCIF/Draft/{id}")]
        [HttpGet]
        public HttpResponseMessage GetDraft(Int64 id)
        {
            TransactionCIFDraftModel output = new TransactionCIFDraftModel();

            if (!repository.GetTransactionDraftByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<TransactionCIFDraftModel>(HttpStatusCode.OK, output);
            }
        }
        [ResponseType(typeof(TransactionChangeRMDraftModel))]
        [Route("api/TransactionCIF/ChangeRM/Draft/{id}")]
        [HttpGet]
        public HttpResponseMessage GetDraftChangeRM(Int64 id)
        {
            TransactionChangeRMDraftModel output = new TransactionChangeRMDraftModel();

            if (!repository.GetTransactionChangeRMDraftByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<TransactionChangeRMDraftModel>(HttpStatusCode.OK, output);
            }
        }

        [AcceptVerbs("POST", "PUT")]
        [ResponseType(typeof(string))]
        [Route("api/TransactionCIF/ChangeRM")]
        public HttpResponseMessage AddTransactionChangeRM(TransactionChangeRMModel transaction)
        {
            long transactionID = 0;
            string ApplicationID = string.Empty;
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionChangeRM(transaction, ref transactionID, ref ApplicationID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new
                    {
                        ID = transactionID,
                        AppID = ApplicationID,
                        Message = transaction.IsDraft ? "Transaction has been saved as draft." : "New Transaction has been submitted."
                    });
                }
            }
        }
        [HttpDelete]
        [Route("api/TransactionCIF/Draft/Delete/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage DeleteDraft(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteTransactionDraftByID(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Draft." });
                }
            }
        }

        [HttpDelete]
        [Route("api/TransactionCIF/ChangeRM/Draft/Delete/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage DeleteChangeRMDraft(long id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteTransactionChangeRMByID(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Delete Draft Successed." });
                }
            }
        }
        #region Dispatch Mode Controller
        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/transactionCIF/SubmitDispatchMode")]
        public HttpResponseMessage Post(TransactionDispatchModeModel data)
        {
            long transactionID = 0;
            string applicationID = string.Empty;
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionDispatchMode(data, ref transactionID, ref  applicationID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new
                    {
                        ID = transactionID,
                        AppID = applicationID,
                        Message = data.IsDraft ? "Transaction has been saved as draft." : "New Transaction has been submitted."
                    });
                }
            }
        }

        [HttpDelete]
        [Route("api/transactionCIF/DeleteDraftDispatchMode/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage DeleteDraftDispatchMode(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteTransactionDraftByID(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Draft." });
                }
            }
        }

        [ResponseType(typeof(TransactionDispatchModeModel))]
        [Route("api/TransactionCIF/DispatchDraft/{id}")]
        [HttpGet]
        public HttpResponseMessage DispatchDraft(Int64 id)
        {
            TransactionDispatchModeModel output = new TransactionDispatchModeModel();

            if (!repository.GetTransactionDispatchModeDraftByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<TransactionDispatchModeModel>(HttpStatusCode.OK, output);
            }
        }
        #endregion

    }
}