var accessToken;
		
var ViewModel = function () {
	//Make the self as 'this' reference
	var self = this;
	
	// task properties
	self.ApproverId	= ko.observable();
	self.SPWebID = ko.observable();	
	self.SPSiteID = ko.observable();
	self.SPListID = ko.observable();
	self.SPListItemID = ko.observable();
	self.SPTaskListID = ko.observable();
	self.SPTaskListItemID = ko.observable();
	self.Initiator = ko.observable();
	self.WorkflowInstanceID	= ko.observable();
	self.WorkflowID	= ko.observable();
	self.WorkflowName = ko.observable();
	self.StartTime = ko.observable();
	self.StateID = ko.observable();
	self.StateDescription = ko.observable();
	self.IsAuthorized = ko.observable();
	
	self.TaskTypeID	= ko.observable();
	self.TaskTypeDescription = ko.observable();
	self.Title = ko.observable();
	self.ActivityTitle = ko.observable();
	self.User = ko.observable();
	self.IsSPGroup = ko.observable();
	self.EntryTime = ko.observable();
	self.ExitTime = ko.observable();
	self.OutcomeID = ko.observable();
	self.OutcomeDescription	= ko.observable();
	self.CustomOutcomeID = ko.observable();
	self.CustomOutcomeDescription = ko.observable();
	self.Comments = ko.observable();
	
	// Task status from Nintex custom REST API
	self.IsPendingNintex = ko.observable();
	self.IsAuthorizedNintex = ko.observable();
	self.MessageNintex = ko.observable();
	
	// grid properties
	self.allowFilter = ko.observable(false);
	self.availableSizes = ko.observableArray([10, 25, 50, 75, 100]);
	self.Page = ko.observable(1);
	self.Size = ko.observableArray([10]);
	self.Total = ko.observable(0);
	self.TotalPages = ko.observable(0);

	// filters
	self.FilterWorkflowName = ko.observable("");
	self.FilterInitiator = ko.observable("");
	self.FilterStateDescription = ko.observable("");
	self.FilterStartTime = ko.observable("");
	self.FilterFilterTitle = ko.observable("");
	self.FilterActivityTitle = ko.observable("");
	self.FilterUser = ko.observable("");
	self.FilterEntryTime = ko.observable("");
	self.FilterExitTime = ko.observable("");
	self.FilterOutcomeDescription = ko.observable("");
	self.FilterCustomOutcomeDescription = ko.observable("");
	self.FilterComments = ko.observable("");

	// sorting
	self.SortColumn = ko.observable("EntryTime");
	self.SortOrder = ko.observable("DESC");
		
	//Declare an ObservableArray for Storing the JSON Response
	self.Tasks = ko.observableArray([]);
	self.Outcomes = ko.observableArray([]);

	// bind get data function to view
	self.GetData = function() { GetData(); }
	
	// bind allow filter
	self.AllowFilter = function(){
		self.allowFilter(true);
	}
	
	// bind clear filters
	self.ClearFilters = function(){
		self.FilterWorkflowName("");
		self.FilterInitiator("");
		self.FilterStateDescription("");
		self.FilterStartTime("");
		self.FilterFilterTitle("");
		self.FilterActivityTitle("");
		self.FilterUser("");
		self.FilterEntryTime("");
		self.FilterExitTime("");
		self.FilterOutcomeDescription("");
		self.FilterCustomOutcomeDescription("");
		self.FilterComments("");
		
		GetData();
	}
	
	// get sorted column
	self.GetSortedColumn = function(columnName){
		var sort = "sorting";
		
		if(self.SortColumn() == columnName){
			if(self.SortOrder() == "DESC")
				sort = sort + "_asc";
			else
				sort = sort + "_desc";
		}
		
		return sort;
	}
	
	// Sorting data
	self.Sorting = function (column) {
		if (self.SortColumn() == column) {
			if (self.SortOrder() == "ASC")
				self.SortOrder("DESC");
			else
				self.SortOrder("ASC");
		} else {
			self.SortOrder("ASC");
		}

		self.SortColumn(column);

		self.Page(1);
		
		GetData();
	}
	
	// Colour Status
	self.SetColorStatus = function(id){
		switch(id) {
			case 2 : return "active";
			break;
			case 4 : return "success";
			break;
			case 8 : return "warning";
			break;
			case 64: return "danger";
			break;
			default : return "";
			break;

		}
	}
	
	// Confirm Dialog
	function Confirm(text){
		bootbox.confirm(text, function(result) {
			return result;
		});
	}
	
	GetData(); //Call the Function which gets all records using ajax call
	
	
	function GetData() {
		// widget reloader function start
		var $box = $('#widget-box');
		var event;
		$box.trigger(event = $.Event('reload.ace.widget'))
		if (event.isDefaultPrevented()) return

		$box.blur();

		var $remove = false;
		if($box.css('position') == 'static') {$remove = true; $box.addClass('position-relative');}
		$box.append('<div class="widget-box-overlay"><i class="icon-spinner icon-spin icon-2x white"></i></div>');
		
		$box.one('reloaded.ace.widget', function() {
			$box.find('.widget-box-overlay').remove();
			if($remove) $box.removeClass('position-relative');
		});
		// widget reloader function end

		// define filter
		var filters = [];
		if (self.FilterWorkflowName() != "") filters.push({ Field: 'WorkflowName', Value: self.FilterWorkflowName() });
		if (self.FilterInitiator() != "") filters.push({ Field: 'Initiator', Value: self.FilterInitiator() });
		if (self.FilterStateDescription() != "") filters.push({ Field: 'StateDescription', Value: self.FilterStateDescription() });
		if (self.FilterStartTime() != "") filters.push({ Field: 'StartTime', Value: self.FilterStartTime() });
		if (self.FilterFilterTitle() != "") filters.push({ Field: 'Title', Value: self.FilterFilterTitle() });
		if (self.FilterActivityTitle() != "") filters.push({ Field: 'ActivityTitle', Value: self.FilterActivityTitle() });
		if (self.FilterUser() != "") filters.push({ Field: 'User', Value: self.FilterUser() });
		if (self.FilterEntryTime() != "") filters.push({ Field: 'EntryTime', Value: self.FilterEntryTime() });
		if (self.FilterExitTime() != "") filters.push({ Field: 'ExitTime', Value: self.FilterExitTime() });
		if (self.FilterOutcomeDescription() != "") filters.push({ Field: 'OutcomeDescription', Value: self.FilterOutcomeDescription() });
		if (self.FilterCustomOutcomeDescription() != "") filters.push({ Field: 'CustomOutcomeDescription', Value: self.FilterCustomOutcomeDescription() });
		if (self.FilterComments() != "") filters.push({ Field: 'Comments', Value: self.FilterComments() });


		//Ajax Call Get All Products Records
		$.ajax({
			type: filters.length > 0 ? "POST" : "GET",
			url: api.server + api.url.task + "?webid="+ api.sharepoint.webId +"&siteid="+ api.sharepoint.siteId +"&workflowids="+ api.sharepoint.workflowId +"&state=4&outcome=7&page=" + self.Page() + "&size=" + self.Size() + "&sort_column=" + self.SortColumn() + "&sort_order=" + self.SortOrder(),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: filters.length > 0 ? ko.toJSON(filters) : '',
			headers: {
				"Authorization" : "Bearer " + accessToken
			},
			success: function (data, textStatus, jqXHR) {
				if(jqXHR.status = 200){
					self.Tasks(data.Rows); //Put the response in ObservableArray
	
					self.Page(data['Page']);
					self.Size([data['Size']]);
					self.Total(data['Total']);
					self.TotalPages(Math.ceil(self.Total() / self.Size()));
				}else{
					ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
				}	
			},
			error: function (jqXHR, textStatus, errorThrown) {
				// send notification
				ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
			}
		})
		.always(function(){
			//when finished trigger "reloaded"
			$box.trigger('reloaded.ace.widget');
		});
	}
	
	// Get Nintex task outcomes
	function GetTaskOutcomes(taskListID, taskListItemID){
		var task = { TaskListID: taskListID, TaskListItemID: taskListItemID };
		
		$.ajax({
			type: "POST",
			url: "/_vti_bin/DBSNintex/Services.svc/GetTaskOutcomesByListID",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: ko.toJSON(task),
			processData: true,
			headers: {
				"accept": "application/json;odata=verbose"
			},
			success: function (data, textStatus, jqXHR) {
				if(jqXHR.status = 200){
					self.Outcomes(data.Outcomes);
					
					self.IsPendingNintex(data.IsPending);
					self.IsAuthorizedNintex(data.IsAuthorized);
					self.MessageNintex(data.Message);
				}else{
					ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
				}	
			},
			error: function (jqXHR, textStatus, errorThrown) {
				// send notification
				ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
			}
		});
	}
	
	// completing Nintex task
	function CompletingTask(taskListID, taskListItemID, taskTypeID, outcomeID, comments){
		var task = { 
			TaskListID: taskListID, 
			TaskListItemID: taskListItemID,
			OutcomeID: outcomeID,
			Comment: comments
		};
		
		var endPointURL;
		switch(taskTypeID){
			// Approval
			case 0 : endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondApprovalTask";  
			break;
			
			// Review
			case 1 : endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondReviewTask";  
			break;

			// Flexi
			case 4 : endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondFlexiTask";  
			break;
		}	
			
		$.ajax({
			type: "POST",
			url: endPointURL,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: ko.toJSON(task),
			processData: true,
			headers: {
				"accept": "application/json;odata=verbose"
			},
			success: function (data, textStatus, jqXHR) {
				if(jqXHR.status = 200){
					// send notification
					ShowNotification('Completing Task Success', jqXHR.responseJSON.Message, 'gritter-success', false);

					// close dialog task
					$("#modal-form").modal('hide');
					
					// reload tasks
					GetData();
				}else{
					ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
				}	
			},
			error: function (jqXHR, textStatus, errorThrown) {
				// send notification
				ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
			}
		});
	}

	// Function to display notification. 
	// class name : gritter-success, gritter-warning, gritter-error, gritter-info
	function ShowNotification(title, text, className, isSticky){
		$.gritter.add({
			title: title,
			text: text,
			class_name: className,
			sticky: isSticky,
		});
	}

	//Function to Display record to be updated
	self.GetSelectedRow = function (data) {
		self.ApproverId(data.WorkflowContext.ApproverId);
		self.SPWebID(data.WorkflowContext.SPWebID);	
		self.SPSiteID(data.WorkflowContext.SPSiteID);
		self.SPListID(data.WorkflowContext.SPListID);
		self.SPListItemID(data.WorkflowContext.SPListItemID);
		self.SPTaskListID(data.WorkflowContext.SPTaskListID);
		self.SPTaskListItemID(data.WorkflowContext.SPTaskListItemID);
		self.Initiator(data.WorkflowContext.Initiator);
		self.WorkflowInstanceID(data.WorkflowContext.WorkflowInstanceID);
		self.WorkflowID(data.WorkflowContext.WorkflowID);
		self.WorkflowName(data.WorkflowContext.WorkflowName);
		self.StartTime(data.WorkflowContext.StartTime);
		self.StateID(data.WorkflowContext.StateID);
		self.StateDescription(data.WorkflowContext.StateDescription);
		self.IsAuthorized(data.WorkflowContext.IsAuthorized);
		
		self.TaskTypeID(data.CurrentTask.TaskTypeID);
		self.TaskTypeDescription(data.CurrentTask.TaskTypeDescription);
		self.Title(data.CurrentTask.Title);
		self.ActivityTitle(data.CurrentTask.ActivityTitle);
		self.User(data.CurrentTask.User);
		self.IsSPGroup(data.CurrentTask.IsSPGroup);
		self.EntryTime(data.CurrentTask.EntryTime);
		self.ExitTime(data.CurrentTask.ExitTime);
		self.OutcomeID(data.CurrentTask.OutcomeID);
		self.OutcomeDescription(data.CurrentTask.OutcomeDescription);
		self.CustomOutcomeID(data.CurrentTask.CustomOutcomeID);
		self.CustomOutcomeDescription(data.CurrentTask.CustomOutcomeDescription);
		self.Comments(data.CurrentTask.Comments);
		
		// show the dialog task form
		$("#modal-form").modal('show');
		
		// execute get task function
		GetTaskOutcomes(data.WorkflowContext.SPTaskListID, data.WorkflowContext.SPTaskListItemID);
	};
	
	// get selected outcome to send into custom Nintex REST Api
	self.GetSelectedOutcome = function(data){
		bootbox.confirm("Do you want to '"+ data.Name +"' this task?", function(result) {
			if(result) {
				//alert(JSON.stringify(data));
				
				CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
			}
		});	
	}
	
	// pagination
	self.onPageSizeChange = function () {
		self.Page(1);
		
		GetData();
	};
	
	self.onPageChange = function () {
		if(self.Page() < 1){
			self.Page(1);
		}else{
			if(self.Page() > self.TotalPages())
				self.Page(self.TotalPages());
		}
		
		GetData();
	};


	self.nextPage = function () {
		var page = self.Page();
		if (page < self.TotalPages()) {
			self.Page(page + 1);
			
			GetData();
		}
	}

	self.previousPage = function () {
		var page = self.Page();
		if (page > 1) {
			self.Page(page - 1);
			
			GetData();
		}
	}

	self.firstPage = function () {
		self.Page(1);
		
		GetData();
	}

	self.lastPage = function () {
		self.Page(self.TotalPages());
		
		GetData();
	}

	self.filter = function (data) {
		self.Page(1);
		
		GetData();
	}
};

$(document).ready(function () {
	accessToken = $.cookie(api.cookie.name);

	ko.applyBindings(new ViewModel());
	
	/// block enter key from user to prevent submitted data.
	$("input").keypress(function (event) { 
		var code = event.charCode || event.keyCode;
		if(code == 13){
			$.gritter.add({
				title: "Page Information",
				text: "Enter key is disabled for this form.",
				class_name: "gritter-warning",
				sticky: false,
			});
			
			return false; 
		}
	});
	
	// scrollables
	$('.slim-scroll').each(function () {
		var $this = $(this);
		$this.slimscroll({
			height: $this.data('height') || 100,
			//width: $this.data('width') || 100,
			railVisible:true,
			alwaysVisible:true,
			color:'#D15B47'
		});
	});
	
	// tooltip
	$('[data-rel=tooltip]').tooltip();
	
	// datepicker
	$('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
		$(this).prev().focus();
	});
	
	// validation
	$('#aspnetForm').validate({
		errorElement: 'div',
		errorClass: 'help-block',
		focusInvalid: true,
		rules: {
			code: {
				required: true,
				maxlength:2,
				minlength:2
			},
			name: {
				required: true,
				maxlength: 255
			}
		},

		messages: {
			code: {
				required: "Please provide a valid CIF.",
				maxlength: "Please provide 2 characters valid CIF.",
				minlength: "Please provide 2 characters valid CIF."
			},
			name: {
				required: "Please provide a customer name.",
				maxlength: "Please provide 255 characters."
			}
		},
		highlight: function (e) {
			$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
		},

		success: function (e) {
			$(e).closest('.form-group').removeClass('has-error').addClass('has-info');
			$(e).remove();
		},

		errorPlacement: function (error, element) {
			if(element.is(':checkbox') || element.is(':radio')) {
				var controls = element.closest('div[class*="col-"]');
				if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
				else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
			}
			else if(element.is('.select2')) {
				error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
			}
			else if(element.is('.chosen-select')) {
				error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
			}
			else error.insertAfter(element.parent());
		}
	});
});