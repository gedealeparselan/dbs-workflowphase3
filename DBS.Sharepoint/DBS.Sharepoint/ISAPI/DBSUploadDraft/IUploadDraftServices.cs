﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace DBS.Sharepoint
{
    #region Interfaces

    [ServiceContract]
    public interface IUploadDraftServices
    {
        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Test HelloWorld();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        UploadFileInfo UploadFile(UploadFileModel data);
    }

    #endregion

    #region Properties
    [DataContract]
    public class UploadFileModel
    {
        [DataMember]
        public string Path { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    public class UploadFileInfo
    {
        [DataMember]
        public string Message { get; set; }
    }
    #endregion
}
