﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class RoleDraftController : ApiController
    {
        private string message = string.Empty;

        private readonly IRoleDraftRepository repository = null;

        public RoleDraftController()
        {
            this.repository = new RoleDraftRepository();
        }

        public RoleDraftController(IRoleDraftRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all Roles.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<RoleDraftModel>))]
        public HttpResponseMessage Get()
        {
            IList<RoleDraftModel> output = new List<RoleDraftModel>();

            if (!repository.GetRole(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<RoleDraftModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Roles with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<RoleModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<RoleDraftModel> output = new List<RoleDraftModel>();

            if (!repository.GetRole(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<RoleDraftModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Roles for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(RoleDraftGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<RoleDraftFilter> filters)
        {
            RoleDraftGrid output = new RoleDraftGrid();

            if (!repository.GetRole(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<RoleDraftGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find Roles using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<RoleDraftModel>))]
        [HttpGet]
        [Route("api/RoleDraft/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<RoleDraftModel> output = new List<RoleDraftModel>();

            if (!repository.GetRole(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<RoleDraftModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Role by ID.
        /// </summary>
        /// <param name="id">Role ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(RoleDraftModel))]
        [Route("api/RoleDraft/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(string id)
        {
            RoleDraftModel output = new RoleDraftModel();

            if (!repository.GetRoleByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<RoleDraftModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new Role.
        /// </summary>
        /// <param name="Role">Role data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(RoleDraftModel role)
        {
            long roleid = 0;

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddRole(role, ref roleid, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { RoleID = roleid, Message = message });
                }
            }
        }

        /// <summary>
        /// Update a new Role.
        /// </summary>
        /// <param name="Role">Role data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        [Route("api/RoleDraft/Update")]
        public HttpResponseMessage Update(RoleDraftModel role)
        {
            long roleid = 0;

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateRole(role, ref roleid, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { RoleID = roleid, Message = message });
                }
            }
        }

        /// <summary>
        /// Modify existing Role.
        /// </summary>
        /// <param name="id">ID of Role to be modify.</param>
        /// <param name="Role">Role data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/RoleDraft/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, RoleDraftModel role)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateRole(id, role, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Role data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting Role.
        /// </summary>
        /// <param name="id">ID of Role to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/RoleDraft/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteRole(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Role data has been deleted." });
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}