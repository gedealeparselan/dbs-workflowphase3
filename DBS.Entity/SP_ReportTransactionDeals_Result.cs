//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    
    public partial class SP_ReportTransactionDeals_Result
    {
        public string TZRef { get; set; }
        public string CustomerName { get; set; }
        public string CIF { get; set; }
        public Nullable<System.DateTime> ValueDate { get; set; }
        public string AccountNumber { get; set; }
        public string StatementLetterName { get; set; }
        public string Rate { get; set; }
        public string ProductTypeCode { get; set; }
        public decimal FCY_Amount { get; set; }
        public decimal USD_Equivalent { get; set; }
        public string CurrencyBuy { get; set; }
        public string CurrencySell { get; set; }
    }
}
