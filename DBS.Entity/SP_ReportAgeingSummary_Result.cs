//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    
    public partial class SP_ReportAgeingSummary_Result
    {
        public System.DateTime Date { get; set; }
        public long Total_Proforma_Invoice { get; set; }
        public long Total_Uncompleted_Documents { get; set; }
        public long Completed_in_every_months { get; set; }
        public string Month { get; set; }
    }
}
