﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    public class AvailableCustomerController : ApiController
    {
        private string message = string.Empty;

        private readonly IAvailableCustomerRepository repository = null;

        public AvailableCustomerController()
        {
            this.repository = new AvailableCustomerRepository();
        }

        public AvailableCustomerController(IAvailableCustomerRepository repository)
        {
            this.repository = repository;
        }

        [ResponseType(typeof(AvailableCustomerGrid))]
        [AcceptVerbs("GET", "POST")]

        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<AvailableCustomerFilter> filters)
        {
            AvailableCustomerGrid output = new AvailableCustomerGrid();

            if (!repository.GetAvailableCustomer(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<AvailableCustomerGrid>(HttpStatusCode.OK, output);
            }


        }

           

    }
}
