﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReleaseWorksheetWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.Others.ReleaseWorksheetWP.ReleaseWorksheetWPUserControl" %>

<div id="new-transaction" class="modal-body overflow-visible">
    <div class="row">
        <div class="col-xs-12">
        </div>
    </div>
    <!-- modal header start -->
    <div class="modal-body overflow-visible" id="header-transaction">
        <div class="row">
            <div class="col-xs-12">
                <div id="upload-form" class="form-horizontal" role="form">                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="loan-contract">Loan Contract No</label>
                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="loan-contract" name="loan-contract" data-bind="value: LoanContractNo" class="col-xs-12" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="release"></label>
                        <div class="col-sm-3">
                            <div class="clearfix">
                                <button class="btn btn-sm btn-primary" data-bind="click: $root.btnRelease"><i class="icon-edit"></i>Release</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-form-Upload" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="blue bigger">Release Progress
                    </h4>
                </div>
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="transaction-progress" class="col-sm-12" style="display: none">
                                <h4 class="smaller blue">Release in progress, please wait...</h4>
                                <span class="space-4"></span>
                                <div class="progress progress-small progress-striped active">
                                    <div class="progress-bar progress-bar-info" style="width: 100%"></div>
                                </div>
                            </div>
                            <div id="transaction-result" class="col-sm-12" style="display: none">
                                <div class="dataTables_wrapper" role="grid">
                                    <h4><span data-bind="text: $root.resultHeader()" /></h4>
                                    <span data-bind="text: $root.resultMessage()" />
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.CloseProgres">
                        <i class="icon-save"></i>
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>    
</div>

<!-- additional page scripts start -->
<script type="text/javascript" src="/SiteAssets/Scripts/Helper.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/date-time/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/ReleaseWorksheet.js"></script>