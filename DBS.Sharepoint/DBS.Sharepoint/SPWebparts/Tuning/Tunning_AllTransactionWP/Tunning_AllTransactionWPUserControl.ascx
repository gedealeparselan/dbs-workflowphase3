﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Tunning_AllTransactionWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.Tuning.Tunning_AllTransactionWP.Tunning_AllTransactionWPUserControl" %>

<link rel="stylesheet" href="/SiteAssets/Scripts/w2ui/w2ui-1.5.rc1.min.css" />
<h1 class="header smaller no-margin-top lighter dark">All Transactions</h1>

<!-- widget box start -->
<div id="widget-box" class="widget-box">
    <!-- widget header start -->
    <div class="widget-header widget-hea1der-small header-color-dark">
        <h6>All Transactions Tasks</h6>

        <div class="widget-toolbar">
            <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                <i class="blue icon-filter"></i>
                <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridProperties().AllowFilter" />
                <span class="lbl"></span>
            </label>
            <a href="#" data-bind="click: GetData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                <i class="blue icon-refresh"></i>
            </a>
        </div>
    </div>
    <!-- widget header end -->
    <!-- widget body start -->
    <div class="widget-body">
        <!-- widget main start -->
        <div class="widget-main padding-0">
            <!-- widget slim control start -->
            <div class="slim-scroll" data-height="400">
                <!-- widget content start -->
                <div class="content">

                    <!-- table responsive start -->
                    <div class="table-responsive" id="form-menu-custom" form="AllTransaction">
                        <div class="dataTables_wrapper" role="grid">
                            <table id="workflow-task-table" class="table table-striped table-bordered table-hover dataTable"
                                   workflow-state="running, error"
                                   workflow-outcome="pending, custom"
                                   workflow-custom-outcome=""
                                   workflow-show-contribute="true">
                                <thead>
                                    <tr data-bind="with: GridProperties">
                                        <th>No.</th>
                                        <th data-bind="click: function () { Sorting('BranchCode') }, css: GetSortedColumn('BranchCode')">Branch Code</th>
                                        <th data-bind="click: function () { Sorting('BranchName') }, css: GetSortedColumn('BranchName')">User Location</th>
                                        <th data-bind="click: function () { Sorting('ApplicationID') }, css: GetSortedColumn('ApplicationID')">Application ID</th>
                                        <th data-bind="click: function () { Sorting('Customer') }, css: GetSortedColumn('Customer')">Customer Name</th>
                                        <th data-bind="click: function () { Sorting('Product') }, css: GetSortedColumn('Product')">Product</th>
                                        <th data-bind="click: function () { Sorting('Currency') }, css: GetSortedColumn('Currency')">Currency</th>
                                        <th data-bind="click: function () { Sorting('Amount') }, css: GetSortedColumn('Amount')">Trxn Amount</th>
                                        <th data-bind="click: function () { Sorting('AccountNumber') }, css: GetSortedColumn('AccountNumber')">Debit Acc Number</th>
                                        <th data-bind="click: function () { Sorting('IsFXTransaction') }, css: GetSortedColumn('IsFXTransaction')">FX Transaction</th>
                                        <th data-bind="click: function () { Sorting('IsTopUrgent') }, css: GetSortedColumn('IsTopUrgent')">Urgency</th>
                                        <th data-bind="click: function () { Sorting('TransactionStatus') }, css: GetSortedColumn('TransactionStatus')">Transaction Status</th>
                                        <th data-bind="click: function () { Sorting('LastModifiedBy') }, css: GetSortedColumn('LastModifiedBy')">Modified By</th>
					                    <th data-bind="click: function () { Sorting('LastModifiedDate') }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
					                    <!--end-->
                                    </tr>
                                </thead>
                                <thead data-bind="visible: GridProperties().AllowFilter" class="table-filter">
                                    <tr>
                                        <th class="clear-filter">
                                            <a href="#" data-bind="click: ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                <i class="green icon-trash"></i>
                                            </a>
                                        </th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterBranchCode, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterBranchName, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterApplicationID, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCustomer, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterProduct, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCurrency, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterAmount, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterDebitAccNumber, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterFXTransaction, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterTopUrgent, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterTransactionStatus, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterUser, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterEntryTime, event: { change: GridProperties().Filter }" /></th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: Tasks">
                                   <tr data-bind="click: $root.GetSelectedRow, css: 'active'" data-toggle="modal">
                                        <td><span data-bind="text: RowID"></span></td>
                                        <td><span data-bind="text: Transaction.BranchCode"></span></td>
                                        <td><span data-bind="text: Transaction.BranchName"></span></td>
                                        <td style="white-space: nowrap"><span data-bind="text: Transaction.ApplicationID"></span></td>
                                        <td><span data-bind="text: Transaction.Customer"></span></td>
                                        <td><span data-bind="text: Transaction.Product"></span></td>
                                        <td><span data-bind="text: Transaction.Currency"></span></td>
                                        <td align="right"><span data-bind="text: formatNumber(Transaction.Amount)"></span></td>
                                        <td><span data-bind="text: Transaction.DebitAccNumber"></span></td>
                                        <td><span data-bind="text: Transaction.IsFXTransactionValue"></span></td>
                                        <td><span data-bind="text: Transaction.IsTopUrgentValue"></span></td>
                                        <td><span data-bind="text: Transaction.TransactionStatus"></span></td>
                                        <td><span data-bind="text: Transaction.LastModifiedBy"></span></td>
                                        <td><span data-bind="text: $root.LocalDate(Transaction.LastModifiedDate)"></span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- table responsive end -->
                </div>
                <!-- widget content end -->
            </div>
            <!-- widget slim control end -->
            <!-- widget footer start -->
            <div class="widget-toolbox padding-8 clearfix">
                <div class="row" data-bind="with: GridProperties">
                    <!-- pagination size start -->
                    <div class="col-sm-6">
                        <div class="dataTables_paginate paging_bootstrap pull-left">
                            Showing <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                            rows of <span data-bind="text: Total"></span>
                            entries
                        </div>
                    </div>
                    <!-- pagination size end -->
                    <!-- pagination page jump start -->
                    <div class="col-sm-3">
                        <div class="dataTables_paginate paging_bootstrap">
                            Page <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width:50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                            of <span data-bind="text: TotalPages"></span>
                        </div>
                    </div>
                    <!-- pagination page jump end -->
                    <!-- pagination navigation start -->
                    <div class="col-sm-3">
                        <div class="dataTables_paginate paging_bootstrap">
                            <ul class="pagination">
                                <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                        <i class="icon-double-angle-left"></i>
                                    </a>
                                </li>
                                <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                        <i class="icon-angle-left"></i>
                                    </a>
                                </li>
                                <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                        <i class="icon-angle-right"></i>
                                    </a>
                                </li>
                                <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                        <i class="icon-double-angle-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- pagination navigation end -->

                </div>
            </div>
            <!-- widget footer end -->

        </div>
        <!-- widget main end -->
    </div>
    <!-- widget body end -->
</div>
<!-- widget box end -->
<!-- modal form start -->
<div id="modal-form" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width:100%;">
        <div id="backDrop" class="absCustomBackDrop topCustomBackDrop darkCustomBackDrop hideCustomBackDrop fullHeightCustomBackDrop fullWidthCustomBackDrop centerCustomBackDrop"></div>
        <div class="modal-content">
            <!-- modal header end -->
            <!-- modal body start -->
            <div class="modal-body overflow-visible">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" style="margin-top: -10px;font-size:30px;margin-right: -8px;" data-bind:"$root.OnCloseApproval">×</button>
                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div class="tabbable">
                            <ul class="nav nav-tabs" id="myTab">
                                <li class="active">
                                    <a data-toggle="tab" href="#transaction">
                                        <i class="blue icon-credit-card bigger-110"></i>
                                        <span data-bind="text: ActivityTitle"></span>
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#history">
                                        <i class="blue icon-comments bigger-110"></i>
                                        WorkFlow History
                                    </a>
                                </li>
                                <%-- add by fandi transaction history --%>
                                <li class="TransHistory">
                                    <a data-toggle="tab" href="#Transactionhistory">
                                        <i class="blue icon-list bigger-110"></i>
                                        Transaction History
                                    </a>
                                </li>
                               <%-- end  --%>
                            </ul>

                            <div class="tab-content">
                                <div id="transaction" class="tab-pane in active">
                                    <div id="transaction-progress" class="col-sm-4 col-sm-offset-4" style="display: none">
                                        <h4 class="smaller blue">Please wait...</h4>
                                        <span class="space-4"></span>
                                        <div class="progress progress-small progress-striped active">
                                            <div class="progress-bar progress-bar-info" style="width:100%"></div>
                                        </div>
                                    </div>

                                    <div id="transaction-data" data-bind="template: { name: ApprovalTemplate(), data: ApprovalData() }"></div>
                                </div>

                                <div id="history" class="tab-pane">
                                    <div id="timeline-2" data-bind="with: ApprovalData()">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                                                <div class="timeline-container timeline-style2">
                                                    <!--<span class="timeline-label">
                                                        <b>Today</b>
                                                    </span>-->

                                                    <div data-bind="if: $root.IsTimelines">

                                                        <div class="timeline-items" data-bind="foreach: Timelines">
                                                            <div class="timeline-item clearfix">
                                                                <div class="timeline-info">
                                                                    <span class="timeline-date" data-bind="text: $root.LocalDate(Time)"></span>

                                                                    <i class="timeline-indicator btn btn-info no-hover"></i>
                                                                </div>

                                                                <div class="widget-box transparent">
                                                                    <div class="widget-body">
                                                                        <div class="widget-main no-padding">
                                                                            <span class="bigger-110">
                                                                                <span class="blue bolder" data-bind="text: DisplayName"></span>
                                                                                <!-- ko if: IsGroup -->
                                                                                (<span data-bind="text: UserOrGroup"></span>)
                                                                                <!-- /ko -->
                                                                            </span>

                                                                            <span class="bolder" data-bind="text: Outcome"></span>
                                                                            <span data-bind="text: Activity"></span>

                                                                            <!-- ko ifnot: ApproverID == 0 -->
                                                                            <br />
                                                                            <i class="icon-comments-alt bigger-110 blue"></i>
                                                                            <span data-bind="text: Comment"></span>
                                                                            <!-- /ko -->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <%-- add by fandi --%>	
                                <div id="Transactionhistory" class="tab-pane">
									<div id="transaction-form2" class="form-horizontal" role="form" data-bind="if: TransactionHistory().Transaction != null || TransactionHistory().Payment != null">
										           
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label bolder no-padding-right">
                                                <span data-bind="text: $root.LocalDate($root.CreateDateHeader, true, true)"></span>
                                            </label>

                                            <label class="col-sm-4 pull-right">
                                                <b>Application ID</b> : <span data-bind="text: $root.ApplicationIDHeader"></span>
                                                <br />
                                                <b>User Name</b> : <span data-bind="text: $root.SPUser.DisplayName"></span>
                                            </label>
                                        </div>

                                        <div class="form-group form-line no-margin-bottom no-padding-bottom">
                                            <label class="col-sm-2 control-label bolder no-padding-right" for="customer-name">Customer Name</label>

                                            <label class="col-sm-8">
                                                <span name="customer-name" id="customer-name" data-bind="text: $root.CustomerNameHeader"></span>
                                                <span class="label label-warning arrowed" data-bind="if: $root.IsNewCustomerHeader">
                                                    <i class="icon-warning-sign bigger-120"></i>
                                                    New Customer
                                                </span>
                                            </label>
                                        </div>

                                        <div class="form-group form-line no-margin-bottom no-padding-bottom">
                                            <label class="col-sm-2 control-label bolder no-padding-right" for="cif">CIF</label>

                                            <label class="col-sm-9">
                                                <span name="cif" id="cif" data-bind="text: $root.CIFHeaderHistory"></span>
                                            </label>
                                        </div>

                                        <div class="form-group form-line no-margin-bottom no-padding-bottom">
                                            <label class="col-sm-2 control-label bolder no-padding-right" for="product-code">Product</label>

                                            <label class="col-sm-9">
                                                <span id="product-code" name="product" data-bind="text: $root.ProductCodeHeader"></span>
                                                (<span id="product-name" name="product" data-bind="text: $root.ProductNamerHeader"></span>)
                                            </label>
                                        </div>

                                        <div class="form-group form-line no-margin-bottom no-padding-bottom">
                                            <label class="col-sm-2 control-label bolder no-padding-right" for="currency-code">Currency</label>

                                            <label class="col-sm-8">
                                                <span id="currency-code" name="currency" data-bind="text: $root.CurrencyCodeHeader"></span>
                                                (<span id="currency-desc" name="currency" data-bind="text: $root.CurrencyDescriptionHeader"></span>) 
                                            </label>
                                        </div>

                                        <div class="form-group form-line no-margin-bottom no-padding-bottom">
                                            <label class="col-sm-2 control-label bolder no-padding-right" for="channel">Channel</label>

                                            <label class="col-sm-8">
                                                <span id="channel" name="channel" data-bind="text: $root.ChannelNameHeader"></span>
                                            </label>
                                        </div>

                                        <div class="form-group form-line no-margin-bottom no-padding-bottom">
                                            <label class="col-sm-2 control-label bolder no-padding-right" for="application-date">Application Date</label>

                                            <label class="col-sm-9">
                                                <span id="application-date" name="application-date" data-bind="text: $root.ApplicationDateHeader"></span>
                                            </label>
                                        </div>

                                        <div class="form-group form-line no-margin-bottom no-padding-bottom">
                                            <label class="col-sm-2 control-label bolder no-padding-right" for="biz-segment-code">Biz Segment</label>

                                            <label class="col-sm-8">
                                                <span id="biz-segment-code" name="biz-segment" data-bind="text: $root.BizSegmentDescriptionDescHeader"></span>
                                                (<span id="biz-segment-desc" name="biz-segment" data-bind="text: $root.BizSegmentNameDescHeader"></span>)
                                            </label>
                                        </div>

                                        <div class="space-6"></div>

                                        <div class="dataTables_wrapper" role="grid" style="overflow-x:auto">
                                            <table class="table table-striped table-bordered table-hover dataTable">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Trxn Amount</th>
                                                        <th>Rate</th>
                                                        <th>Eqv. USD</th>
                                                        <th>Debit Acc Number</th>
                                                        <th>Debit Acc CCY</th>
                                                        <th>Debit Sundry</th>
                                                        <th>Bene Name</th>
                                                        <th>Bene Bank</th>
                                                        <th>Bene Acc Number</th>
                                                        <th>Swift Code/ Bank Code</th>
                                                        <th>Bank Charges</th>
                                                        <th>Agent Charges</th>
                                                        <th>Citizen</th>
                                                        <th>Resident</th>
                                                        <th>LLD Code</th>
                                                        <th>Payment Details</th>
                                                        <th>Modifed By</th>
                                                        <th>Modifed Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody data-bind="foreach: $root.TransactionHistory().TransactionMaker, visible: TransactionHistory().TransactionMaker.length > 0">
                                                    <tr>
                                                        <td><span data-bind="text: $index() + 1"></span></td>
                                                        <td align="right"><span data-bind="text: formatNumber_r(Amount)"></span></td>
                                                        <td align="right"><span data-bind="text: formatNumber_r(Rate)"></span></td>
                                                        <td align="right"><span data-bind="text: formatNumber_r(AmountUSD)"></span></td>
                                                        <td><span data-bind="text: DebitAccNumber"></span></td>
                                                        <td><span data-bind="text: CurrencyCode"></span></td>
                                                        <td><span data-bind="text: DebitSundry"></span></td>
                                                        <td><span data-bind="text: BeneName"></span></td>
                                                        <td><span data-bind="text: BeneBankName"></span></td>
                                                        <td><span data-bind="text: BeneAccNumber"></span></td>
                                                        <td><span data-bind="text: BeneBankSwift"></span></td>
                                                        <td align="center"><span data-bind="text: BankChargesCode"></span></td>
                                                        <td align="center"><span data-bind="text: AgentChargesCode"></span></td>
                                                        <td align="center"><span data-bind="text: IsCitizen ? 'Yes' : 'No'"></span></td>
                                                        <td align="center"><span data-bind="text: IsResident ? 'Yes' : 'No'"></span></td>
                                                        <td><span data-bind="text: LLDDescription"></span></td>
                                                        <td><span data-bind="text: PaymentDetails"></span></td>
                                                        <td><span data-bind="text: LastModifiedBy"></span></td>
                                                        <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <%-- end by fandi --%>

                                <div id="context" class="tab-pane">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Parameter</th>
                                                <th>Value</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Web ID</td>
                                                <td><span data-bind="text: SPWebID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Site ID</td>
                                                <td><span data-bind="text: SPSiteID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>List ID</td>
                                                <td><span data-bind="text: SPListID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>List Item ID</td>
                                                <td><span data-bind="text: SPListItemID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Task List ID</td>
                                                <td><span data-bind="text: SPTaskListID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Task List Item ID</td>
                                                <td><span data-bind="text: SPTaskListItemID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Initiator</td>
                                                <td><span data-bind="text: Initiator"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Workflow Instance ID</td>
                                                <td><span data-bind="text: WorkflowInstanceID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Workflow ID</td>
                                                <td><span data-bind="text: WorkflowID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Workflow Name</td>
                                                <td><span data-bind="text: WorkflowName"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Start Time</td>
                                                <td><span data-bind="text: LocalDate(StartTime())"></span></td>
                                            </tr>
                                            <tr>
                                                <td>State ID </td>
                                                <td><span data-bind="text: StateID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>State Description</td>
                                                <td><span data-bind="text: StateDescription"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Is Authorized</td>
                                                <td><span data-bind="text: IsAuthorized"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Task Type</td>
                                                <td><span data-bind="text: TaskTypeDescription"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Title</td>
                                                <td><span data-bind="text: Title"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Activity Title</td>
                                                <td><span data-bind="text: ActivityTitle"></span></td>
                                            </tr>
                                            <tr>
                                                <td>User</td>
                                                <td><span data-bind="text: User"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Is Sharepoint Group</td>
                                                <td><span data-bind="text: IsSPGroup"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Entry Time</td>
                                                <td><span data-bind="text: LocalDate(EntryTime())"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Exit Time</td>
                                                <td><span data-bind="text: LocalDate(ExitTime())"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Outcome ID</td>
                                                <td><span data-bind="text: OutcomeID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Outcome Description</td>
                                                <td><span data-bind="text: OutcomeDescription"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Custom Outcome ID</td>
                                                <td><span data-bind="text: CustomOutcomeID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Custom Outcom Description</td>
                                                <td><span data-bind="text: CustomOutcomeDescription"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Comments</td>
                                                <td><span data-bind="text: Comments"></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div id="ko-model" class="tab-pane">
                                    <pre data-bind="text: ko.toJSON($root.ApprovalData, null, 2)"></pre>
                                    <!--<pre data-bind="text: JSON.stringify(ko.toJS(ApprovalData), null, 2)"></pre>-->
                                </div>
                            </div>
                        </div>
                        <!-- modal body form end -->
                    </div>
                </div>
            </div>
            <!-- modal body end -->
            <!-- modal footer start -->
            <div class="modal-footer">
                <div class="row" data-bind="visible: $root.IsAuthorizedNintex()">
                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="workflow-comments">Comments</label>

                        <div class="col-xs-9 align-left">
                            <div class="clearfix">
                                <textarea class="col-lg-12" id="workflow-comments" name="workflow-comments" rows="4" data-bind="value: $root.Comments, enable: $root.IsPendingNintex()" placeholder="Type any comment here..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="space-8"></div>

                <!-- dynamic outcomes button start -->
                <!--<span data-bind="foreach: Outcomes, visible: $root.IsAuthorizedNintex()">
                    <button data-bind="click: $root.ApprovalProcess, enable: $root.IsPendingNintex()" class="btn btn-sm btn-primary">
                        <i class="icon-approve"></i>
                        <span data-bind="text: $root.ApprovalButton(Name)"></span>
                    </button>
                </span>-->
                <!-- dynamic outcomes button end -->
                <button class="btn btn-sm btn-primary" data-bind="click: $root.ButtonForceComplete, visible: $root.ForceCompleteTransaction"><%--data-dismiss="modal" --%>
                   <i class="icon-approve"></i>
                    Force Complete
                </button>
          
                <button class="btn btn-sm btn-primary"  data-bind="click: $root.ButtonForceCancel, visible: $root.ForceCompleteTransaction"><%--data-dismiss="modal"--%>
                  <%--  <i class="icon-remove"></i>--%>
                    Force Canceled
                </button>

                <button class="btn btn-sm btn-success" data-dismiss="modal" data-bind="click: $root.OnCloseApproval">
                    <i class="icon-remove"></i>
                    Close
                </button>
            </div>
            <!-- modal footer end -->

        </div>
    </div>
</div>
<!-- modal form end -->

<!-- modal form underlying -->
<div id="modal-form-Underlying" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width:100%">
        <div class="modal-content">
            <!-- modal header start Underlying Form -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span>View a Customer Underlying</span>
                </h4>
            </div>
            <!-- modal header end -->
            <!-- modal body start -->
            <div class="modal-body overflow-visible">
                <div id="modalUnderlyingtest">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->

                            <div id="customerUnderlyingform" class="form-horizontal" role="form">
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="StatementLetterID">
                                        Statement Letter
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="statementLetter" name="statementLetter" data-bind="value: $root.StatementLetter_u" class="col-xs-7" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="UnderlyingDocument">
                                        Underlying Document
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <!-- <select id="underlyingDocument" name="underlyingDocument" data-bind="value:UnderlyingDocument_u().ID, options: ddlUnderlyingDocument_u, optionsText: 'CodeName',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select> -->
                                            <input type="text" id="otherUnderlying" name="otherUnderlying" data-bind="value: $root.UnderlyingDocument_u" class="col-xs-7" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="DocumentType">
                                        Type of Document
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <!--<select id="documentType" name="documentType" data-bind="value:$root.DocumentType_u().ID, options: ddlDocumentType_u, optionsText: 'Name',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select> -->
                                            <input type="text" id="documentType" name="documentType" data-bind="value: $root.DocumentType_u" class="col-xs-7" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="Currency">
                                        Currency
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <!--  <select id="currency_u" name="currency_u" data-bind="value:$root.Currency_u().ID,disable:$root.StatementLetter_u().ID()==1, options:ddlCurrency_u, optionsText:'Code',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-2"></select>
                                            <select id="currency_u" name="currency_u" data-bind="value:$root.Currency_u().ID, options:ddlCurrency_u, optionsText:'Code',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-2"  ></select>-->
                                            <input type="text" id="currency_u" name="currency_u" data-bind="value: $root.Currency_u" class="col-xs-7" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="Amount">
                                        Invoice Amount
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="Amount_u" name="Amount_u" data-bind="value: formatNumber($root.Amount_u())" data-rule-required="true" data-rule-number="true" class="col-xs-4 align-right" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="rate">Rate</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" class="col-sm-4 align-right" name="rate_u" id="rate_u" disabled="disabled" data-rule-required="true" data-rule-number="true" data-bind="value: formatNumber(Rate_u())" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="eqv-usd">Eqv. USD</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" class="col-sm-4 align-right" disabled="disabled" name="eqv-usd_u" id="eqv-usd_u" data-rule-required="true" data-rule-number="true" data-bind="value: formatNumber(AmountUSD_u())" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="DateOfUnderlying">
                                        Date of Underlying
                                    </label>
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            <input class="form-control date-picker" type="text" data-date-format="dd-M-yyyy" id="book-underlying-date" name="book-underlying-date" data-bind="value: DateOfUnderlying_u" disabled="disabled" />
                                            <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                        </div>
                                    </div>
                                    <label class="col-sm-2 control-label no-padding-right" for="ExpiredDate">
                                        Expiry Date
                                    </label>
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            <input type="text" id="ExpiredDate" name="ExpiredDate" data-date-format="dd-M-yyyy" data-bind="value: ExpiredDate_u" class="form-control date-picker col-xs-6" data-rule-required="true" disabled="disabled" />
                                            <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="ReferenceNumber">
                                        Reference Number
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="ReferenceNumber" name="ReferenceNumber" data-bind="value: $root.ReferenceNumber_u" disabled="disabled" class="col-xs-9" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="SupplierName">
                                        Supplier Name
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="SupplierName" name="SupplierName" data-bind="value: $root.SupplierName_u" class="col-xs-8" data-rule-required="true" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="InvoiceNumber">
                                        Invoice Number
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="invoiceNumber" name="invoiceNumber" data-bind="value: $root.InvoiceNumber_u" class="col-xs-8" data-rule-required="true" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="AttachmentNo">
                                        Is This doc Purposed to replace the proforma doc
                                    </label>
                                    <div class="col-sm-9">
                                        <label>
                                            <input name="switch-field-1" id="IsProforma" data-bind="checked: IsProforma_u" class="ace ace-switch ace-switch-6" type="checkbox" disabled="disabled" />
                                            <span class="lbl"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="dataTables_wrapper" role="grid" data-bind="visible: $root.IsProforma_u()">
                                    <table class="table table-striped table-bordered table-hover dataTable">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Statement Letter</th>
                                                <th>Underlying Document</th>
                                                <th>Type of Doc</th>
                                                <th>Currency</th>
                                                <th>Amount</th>
                                                <th>Supplier Name</th>
                                                <th>Invoice Number</th>
                                                <th>Reference Number</th>
                                                <th>Date</th>
                                                <th>Expiry Date</th>
                                            </tr>
                                        </thead>
                                        <tbody data-bind="foreach: $root.ProformaDetails, visible: $root.ProformaDetails().length > 0">
                                            <tr data-toggle="modal">
                                                <td><span data-bind="text: $index() + 1"></span></td>
                                                <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                <td><span data-bind="text: DocumentType.Name"></span></td>
                                                <td><span data-bind="text: Currency.Code"></span></td>
                                                <td align="right"><span data-bind="text: $root.FormatNumber(Amount)"></span></td>
                                                <td><span data-bind="text: SupplierName"></span></td>
                                                <td><span data-bind="text: InvoiceNumber"></span></td>
                                                <td><span data-bind="text: ReferenceNumber"></span></td>
                                                <td><span data-bind="text: $root.LocalDate(DateOfUnderlying, true, false)"></span></td>
                                                <td><span data-bind="text: $root.LocalDate(ExpiredDate, true, false)"></span></td>
                                            </tr>
                                        </tbody>
                                        <tbody data-bind="visible: $root.ProformaDetails().length == 0">
                                            <tr>
                                                <td colspan="10" class="text-center">
                                                    No entries available.
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="IsBulkUnderlying">
                                        Is This Bulk Underlying
                                    </label>
                                    <div class="col-sm-9">
                                        <label>
                                            <input name="switch-field-1" id="IsBulkUnderlying" data-bind="checked: IsBulkUnderlying_u" class="ace ace-switch ace-switch-6" type="checkbox" disabled="disabled" />
                                            <span class="lbl"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="dataTables_wrapper" role="grid" data-bind="visible: $root.IsBulkUnderlying_u()">
                                    <table class="table table-striped table-bordered table-hover dataTable">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Statement Letter</th>
                                                <th>Underlying Document</th>
                                                <th>Type of Doc</th>
                                                <th>Currency</th>
                                                <th>Amount</th>
                                                <th>Supplier Name</th>
                                                <th>Invoice Number</th>
                                                <th>Reference Number</th>
                                                <th>Date</th>
                                                <th>Expiry Date</th>
                                            </tr>
                                        </thead>
                                        <tbody data-bind="foreach: $root.BulkDetails, visible: $root.BulkDetails().length > 0">
                                            <tr data-toggle="modal">
                                                <td><span data-bind="text: $index() + 1"></span></td>
                                                <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                <td><span data-bind="text: DocumentType.Name"></span></td>
                                                <td><span data-bind="text: Currency.Code"></span></td>
                                                <td align="right"><span data-bind="text: $root.FormatNumber(Amount)"></span></td>
                                                <td><span data-bind="text: SupplierName"></span></td>
                                                <td><span data-bind="text: InvoiceNumber"></span></td>
                                                <td><span data-bind="text: ReferenceNumber"></span></td>
                                                <td><span data-bind="text: $root.LocalDate(DateOfUnderlying, true, false)"></span></td>
                                                <td><span data-bind="text: $root.LocalDate(ExpiredDate, true, false)"></span></td>
                                            </tr>
                                        </tbody>
                                        <tbody data-bind="visible: $root.BulkDetails().length == 0">
                                            <tr>
                                                <td colspan="10" class="text-center">
                                                    No entries available.
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- modal body form end -->
                        </div>
                    </div>
                </div>
            </div>


            <!-- modal body end -->                            <!-- modal footer start -->
            <div class="modal-footer">
                <button class="btn btn-sm" data-bind="click: $root.CloseUnderlying" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Close
                </button>
            </div>
            <!-- modal footer end -->
        </div>
    </div>
</div>
<!-- modal form enderlying end -->

<!-- approval templates start -->

<!-- Start Payment -->
<script type="text/html" id="TransactionAllIPE" src="/Pages/Approval/TransactionAllIPE.html"></script>
<script type="text/html" id="TransactionAll" src="/Pages/Approval/TransactionAll.html"></script>
<script type="text/html" id="TaskFXDealChecker" src="/Pages/Approval/TransactionDealChecker.html"></script>
<script type="text/html" id="TaskFXTMOChecker" src="/Pages/Approval/TransactionCheckerAllTrackingTMO.html"></script>
<script type="text/html" id="TaskPendingDocumentsChecker" src="/Pages/Approval/TransactionPendingDocuments.html"></script>
<script type="text/html" id="TransactionTimeline" src="/Pages/Approval/Timeline.html"></script>
<%--<script type="text/html" id="TaskLoanMaker" src="/Pages/Approval/TaskLoanMaker.html"></script>--%>
<script type="text/html" id="TaskCBOMaker" src="/Pages/Approval/TransactionCBOMaker.html"></script>
<script type="text/html" id="TaskCBOChecker" src="/Pages/Approval/TransactionCBOChecker.html"></script>
<script type="text/html" id="TransactionAllIPESKNBulk" src="/Pages/Approval/TransactionAllIPESKNBulk.html"></script>

<!-- End Payment -->
<!-- Start Loan Registration -->
<script type="text/html" id="TransactionLoanComplete" src="/Pages/Approval/LoanComplete.html"></script>
<!-- End Loan Registration -->
<!-- Start FD -->
<script type="text/html" id="TransactionAllFD" src="/Pages/Approval/TransactionAllFD.html"></script>
<script type="text/html" id="TaskFDComplete" src="/Pages/Approval/FDComplete.html"></script>
<!-- End FD -->

<!-- Start UT -->
<script type="text/html" id="TransactionAllUT" src="/Pages/Approval/TransactionAllUT.html"></script>
<script type="text/html" id="TaskUTcomplete" src="/Pages/Approval/UTComplete.html"></script>
<!-- End UT -->

<!-- Start TMO Registration -->
<script type="text/html" id="TransactionAllTMOReg" src="/Pages/Approval/TransactionAllTMOReg.html"></script>
<!-- End TMO Registration -->

<%-- RetailCIF --%>
<script type="text/html" id="TaskCIFComplete" src="/Pages/Approval/CIFComplete.html"></script>
<%-- ENd RetailCIF --%>
<%-- TMO --%>
<script type="text/html" id="TaskTMOComplete" src="/Pages/Approval/TMOComplete.html"></script>
<%-- Agung --%>
<script type="text/html" id="TaskTMOMaker" src="/Pages/Approval/TMOMaker.html"></script>
<%-- End --%>
<%-- End TMO --%>
<script type="text/html" id="DocumentFlow" src="/Pages/Approval/DocumentFlowMonitoring.html"></script>
<script type="text/html" id="TaskAllNetting" src="/Pages/Approval/TransactionAllNetting.html"></script>
<!-- approval templates end -->

<!-- additional page scripts start -->
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/DBS.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/CalculatedFX.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/DataFormat.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/AllTransactionTunning.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/w2ui/w2ui-1.5.rc1.min.js"></script>
<!-- additional page scripts end -->