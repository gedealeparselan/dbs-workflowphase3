﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    public class AgeingReportController : ApiController
    {
        private string message = string.Empty;

        private readonly IAgeingReportRepository repository = null;

        public AgeingReportController()
        {
            this.repository = new AgeingReportRepository();
        }

        public AgeingReportController(IAgeingReportRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all currencies.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<AgeingReportModel>))]
        public HttpResponseMessage Get()
        {
            IList<AgeingReportModel> output = new List<AgeingReportModel>();

            if (!repository.GetAgeingReport(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<AgeingReportModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Ageing Report Parameter with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<AgeingReportModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<AgeingReportModel> output = new List<AgeingReportModel>();

            if (!repository.GetAgeingReport(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<AgeingReportModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get currencies for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(AgeingReportGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<AgeingReportFilter> filters)
        {
            AgeingReportGrid output = new AgeingReportGrid();

            if (!repository.GetAgeingReport(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<AgeingReportGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find AgeingReport using criterias Type Of Ageing, Time or days.
        /// </summary>
        /// <param name="query"> Type Of Ageing, Time or days.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<AgeingReportModel>))]
        [HttpGet]
        [Route("api/AgeingReport/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<AgeingReportModel> output = new List<AgeingReportModel>();

            if (!repository.GetAgeingReport(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<AgeingReportModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get AgeingReport by ID.
        /// </summary>
        /// <param name="id">AgeingReport ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(AgeingReportModel))]
        [Route("api/AgeingReport/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            AgeingReportModel output = new AgeingReportModel();

            if (!repository.GetAgeingReportByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<AgeingReportModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new Ageing Report Parameter.
        /// </summary>
        /// <param name="AgeingReport"> Ageing Report Parameter.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(AgeingReportModel AgeingReport)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddAgeingReport(AgeingReport, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.Created, "New Ageing Report Parameter data has been created.");
                }
            }
        }

        /// <summary>
        /// Modify existing AgeingReport.
        /// </summary>
        /// <param name="id">ID of AgeingReport to be modify.</param>
        /// <param name="customer">AgeingReport data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/AgeingReport/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, AgeingReportModel AgeingReport)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateAgeingReport(id, AgeingReport, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "Ageing Report Parameter data has been updated.");
                }
            }
        }

        /// <summary>
        /// Remove exisiting Ageing Report Parameter.
        /// </summary>
        /// <param name="id">ID of Ageing Report Parameter to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/AgeingReport/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteAgeingReport(id, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "Ageing Report Parameter data has been deleted.");
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}