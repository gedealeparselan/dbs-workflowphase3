﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    public class LoanGenerateController : ApiController
    {
        private string message = string.Empty;
        private readonly ILoanGenerateRepository repository = null;

        public LoanGenerateController()
        {
            this.repository = new LoanGenerateRepository();
        }

        public LoanGenerateController(ILoanGenerateRepository repository)
        {
            this.repository = repository;
        }

        [ResponseType(typeof(LoanGrid))]
        [Route("api/LoanIMGenerate/{tanggal}")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage SearchInterest(DateTime? tanggal, int? page, int? size, string sort_column, string sort_order, IList<LoanFilter> filters)
        {
            LoanGrid output = new LoanGrid();

            if (!repository.GetLoanInterestMaintenance(tanggal, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Scheme_Type" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<LoanGrid>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(LoanGrid))]
        [Route("api/LoanROGenerate/{tanggal}")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage SearchRollover(DateTime? tanggal, int? page, int? size, string sort_column, string sort_order, IList<LoanFilter> filters)
        {
            LoanGrid output = new LoanGrid();

            if (!repository.GetLoanRollover(tanggal, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Scheme_Type" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<LoanGrid>(HttpStatusCode.OK, output);
            }
        }
    }
}