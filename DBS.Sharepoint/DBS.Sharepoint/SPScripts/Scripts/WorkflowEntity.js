var WorkflowEntityModel = function (cfg) {
    console.log('WorkflowEntity Module loaded!');
    var self = this;
    self.Readonly = ko.observable(true);
    self.IsNewData = ko.observable(false);
    self.ID = ko.observable();

    self.UserEntity = ko.observable(false);

    // task properties
    self.ApproverId = ko.observable();
    self.SPWebID = ko.observable();
    self.SPSiteID = ko.observable();
    self.SPListID = ko.observable();
    self.SPListItemID = ko.observable();
    self.SPTaskListID = ko.observable();
    self.SPTaskListItemID = ko.observable();
    self.Initiator = ko.observable();
    self.WorkflowInstanceID = ko.observable();
    self.WorkflowID = ko.observable();
    self.WorkflowName = ko.observable();
    self.StartTime = ko.observable();
    self.StateID = ko.observable();
    self.StateDescription = ko.observable();
    self.IsAuthorized = ko.observable();

    self.TaskTypeID = ko.observable();
    self.TaskTypeDescription = ko.observable();
    self.Title = ko.observable();
    self.ActivityTitle = ko.observable();
    self.User = ko.observable();
    self.IsSPGroup = ko.observable();
    self.EntryTime = ko.observable();
    self.ExitTime = ko.observable();
    self.OutcomeID = ko.observable();
    self.OutcomeDescription = ko.observable();
    self.CustomOutcomeID = ko.observable();
    self.CustomOutcomeDescription = ko.observable();
    self.Comments = ko.observable();

    // Task status from Nintex custom REST API
    self.IsPendingNintex = ko.observable();
    self.IsAuthorizedNintex = ko.observable();
    self.MessageNintex = ko.observable();

    //Declare an ObservableArray for Storing the JSON Response
    self.Tasks = ko.observableArray([]);
    self.Outcomes = ko.observableArray([]);

    self.DialogFormName = ko.observable("");
    self.DialogData = ko.observable({ test: ko.observable('') });
    self.IsHomeMenu = ko.observable(false);
    if (cfg.url != null) {
        self.Config = cfg
        self.IsHomeMenu(false);
        if (self.Config.url == "") { self.IsHomeMenu(true); }
        $.ajaxSetup({
            beforeSend: function (jqXHR, data) {
                //console.log(self.Config.url);
                if (data.url.substr(0, 4) != '/?_=' && data.url.indexOf('WorkflowEntity?page=') == -1) $.cookie('api.cookie.timeout', 0); // no reset counter if ajax request on home page for background refresh purpose
                var IsWorkflowed = false;
                if (data.url.indexOf(self.Config.url) > 0) {
                    IsWorkflowed = true;
                    if (IsWorkflowed) if (data.url.indexOf(self.Config.url + '?page=') > 0) IsWorkflowed = false;
                    if (data.type == 'GET') IsWorkflowed = false;
                    if (IsWorkflowed && self.Config.url != null) {
                        jqXHR.abort();
                        //console.log(data);
                        var type = '';
                        var key = data.url.substr(data.url.lastIndexOf('/') + 1);
                        var name = self.Config.url.substr(self.Config.url.lastIndexOf('/') + 1);
                        switch (data.type) {
                            case 'POST': type = 'ADD'; key = ''; break;
                            case 'PUT': type = 'UPDATE'; break;
                            case 'DELETE': type = 'DELETE'; break;
                        }
                        if (data.type != 'GET')
                            self.PostCheckWorkflow(name, type, key, data.data);
                    }
                }
                else if (data.url.indexOf(self.Config.url2) > 0) {
                    IsWorkflowed = true;
                    if (IsWorkflowed) if (data.url.indexOf(self.Config.url2 + '?page=') > 0) IsWorkflowed = false;
                    if (data.type == 'GET') IsWorkflowed = false;
                    if (IsWorkflowed && self.Config.url2 != null) {
                        jqXHR.abort();
                        //console.log(data);
                        var type = '';
                        var key = data.url.substr(data.url.lastIndexOf('/') + 1);
                        var name2 = self.Config.url2.substr(self.Config.url2.lastIndexOf('/') + 1);
                        switch (data.type) {
                            case 'POST': type = 'ADD'; key = ''; break;
                            case 'PUT': type = 'UPDATE'; break;
                            case 'DELETE': type = 'DELETE'; break;
                        }
                        if (data.type != 'GET')


                            self.PostCheckWorkflow(name2, type, key, data.data);


                    }
                }
                else if (data.url.indexOf(self.Config.urlPOAEmail) > 0) {
                    IsWorkflowed = true;
                    if (IsWorkflowed) if (data.url.indexOf(self.Config.urlPOAEmail + '?page=') > 0) IsWorkflowed = false;
                    if (data.type == 'GET') IsWorkflowed = false;
                    if (IsWorkflowed) {
                        jqXHR.abort();
                        //console.log(data);
                        var type = '';
                        var key = data.url.substr(data.url.lastIndexOf('/') + 1);
                        var name2 = self.Config.urlPOAEmail.substr(self.Config.urlPOAEmail.lastIndexOf('/') + 1);
                        switch (data.type) {
                            case 'POST': type = 'ADD'; key = ''; break;
                            case 'PUT': type = 'UPDATE'; break;
                            case 'DELETE': type = 'DELETE'; break;
                        }
                        if (data.type != 'GET')


                            self.PostCheckWorkflow(name2, type, key, data.data);


                    }
                }
                else if (data.url.indexOf(self.Config.urlCustomerCallback) > 0 && data.url.indexOf('Draft') < 0) {
                    IsWorkflowed = true;
                    if (IsWorkflowed) if (data.url.indexOf(self.Config.urlCustomerCallback + '?page=') > 0) IsWorkflowed = false;
                    if (data.type == 'GET') IsWorkflowed = false;
                    if (IsWorkflowed && self.Config.urlCustomerCallback != null) {
                        jqXHR.abort();
                        var type = '';
                        var key = data.url.substr(data.url.lastIndexOf('/') + 1);
                        var name4 = self.Config.urlCustomerCallback.substr(self.Config.urlCustomerCallback.lastIndexOf('/') + 1);
                        switch (data.type) {
                            case 'POST': type = 'ADD'; key = ''; break;
                            case 'PUT': type = 'UPDATE'; break;
                            case 'DELETE': type = 'DELETE'; break;
                        }
                        if (data.type != 'GET')


                            self.PostCheckWorkflow(name4, type, key, data.data);


                    }
                }
            },
            complete: function (jqXHR) {
            }
        });
    } else {
        self.Parent = cfg.parent;
        self.RowClickCallback = cfg.rowClickCallback;
    }

    // ADD OBSERVABLE PROPERTY
    $.each(self.Config.columns.fields, function (index, item) {
        self[item] = self.Config.view[item];
    });

    table_container_id = 'widget-box'; if (self.Config.tablecontainerid != null && self.Config.tablecontainerid != '') table_container_id = self.Config.tablecontainerid;
    if (table_container_id != null) {
        el = $('#' + table_container_id);
        if (el.length > 0) {
            if (table_container_id == 'widget-box')
                el.next().after(TableTemplate());
            else
                el.append(TableTemplate());
        } else {
            // if not found widget box. assume this is single value
            $('.modal-body').after(TableTemplate());
        }
    }



    $('#WorkflowEntity-table > tbody:eq(1) > tr > td')
		.attr('colspan', $('#WorkflowEntity-table > thead:eq(0) > tr > th').length); // fixing no found entry span

    if (self.Config.file == null || self.Config.file == '') {
        $("div#tabbable-workflow").tabs();
        //$('#modal-form').attr('id','modal-form-old');

        /*
            var vm = ko.contextFor($('#aspnetForm')[0]);
            console.log(vm);

            vm.$root.ApprovalTemplate = function(){
                return self.DialogFormName();
            }

            vm.$root.ApprovalData(function(){
                return self.DialogData();
            });

            */
        self.ApprovalTemplate = function () {
            return self.DialogFormName();
        }
        self.ApprovalData = function () {
            return self.DialogData();
        }
    }
    self.url = "api/WorkflowEntity";

    // filter
    self.FilterTaskName = ko.observable("");
    self.FilterValue = ko.observable("");
    self.FilterActionType = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");
    //add by fandi
    self.TaskName = ko.observable("");
    self.ActionType = ko.observable("");
    //end

    self.ClearFilters = function () {
        self.FilterTaskName("");
        self.FilterActionType("");
        self.FilterValue("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");
        GetData();
    };

    // Declare an ObservableArray for Storing the JSON Response
    self.Rows = ko.observableArray([]);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    // set default sorting
    self.GridProperties().SortColumn("LastModifiedDate");
    self.GridProperties().SortOrder("DESC");

    // bind get data function to view
    self.GetData = function () {
        GetData();
    };

    // validation show table entity
    self.ValidationAvailable = function () {
        ValidationAvailable();
    };
    self.AddAccountNumber_c = function () {
    }
    self.UpdateAccountNumber_c = function (index) {
    }
    self.RemoveAccountNumber_c = function (index) {
    }
    self.AccountNumberSave = function () {
    }
    self.AccountNumberUpdate = function () {
    }
    self.Readonly = ko.observable(true);

    //POAFunction
    self.AddPOAFunction_c = function () {
    }
    self.UpdatePOAFunction_c = function (index) {
    }
    self.RemovePOAFunction_c = function (index) {
        console.log(self.POAFunctions_c())
    }
    self.POAFunctionSave = function () {
    }
    self.POAFunctionUpdate = function () {
    }
    function SetViewModelProperty(model, data) {
        rec = eval('(' + data.Data + ')');
        //console.log(rec)
        $.each(rec, function (name, value) {
            //console.log(name + ' = ' + value);
            //console.log(model[name]);
            //console.log();
            //try
            if (ko.isObservable(model[name])) {
                val = model[name]();
                if (val == null) {
                    model[name](value);
                    //console.log(name + ' = ' + value);
                } else {

                    //console.log(model[name]().ID());
                    //console.log(value);;
                    //model[name](new BizSegmentModel(0,'',''));
                }

                //console.log(model[name]());//(value);
            }
            //}catch(){

            //}

        });
    };

    self.IsLocalGetSelectedRow = false;

    self.GetSelectedRow = function (data) {
        self.IsLocalGetSelectedRow = true;
        console.log("==========================");
        console.log(data);
        console.log("==========================");
        switch (data.EntityName) {
            case "Role":
                data["WorkflowTaskActivityTitle"] = "Master Role Approval Task";
                data["ID"] = data.WorkflowID;
                data["SPTaskListID"] = "d18ac7df-ca24-4100-8124-70f7f8bc567e";
                data["WorkflowTaskType"] = 0;
                if (multiViewModel != null) multiViewModel.Form().Show(data);
                break;
            case "User":
                data["WorkflowTaskActivityTitle"] = "Master User Approval Task";
                data["ID"] = data.WorkflowID;
                data["SPTaskListID"] = "91A808BF-FDD4-4C29-B0E7-CD034205FBFA";
                data["WorkflowTaskType"] = 0;
                if (multiViewModel != null) multiViewModel.Form().Show(data);
                break;
            case "MasterUnderlying":
                data["WorkflowTaskActivityTitle"] = "Master Underlying Approval Task";
                data["ID"] = data.WorkflowID;
                data["SPTaskListID"] = config.sharepoint.listIdMasterUnderlyingTask;
                data["WorkflowTaskType"] = 0;
                if (multiViewModel != null) multiViewModel.Form().Show(data);
                break;
            case "CustomerCSO":
                data["WorkflowTaskActivityTitle"] = "Master CustomerCso Approval Task";
                data["ID"] = data.WorkflowID;
                data["SPTaskListID"] = "CAE33D0A-F5E7-423A-8F0C-F5F5E93492B5";
                data["WorkflowTaskType"] = 0;
                if (multiViewModel != null) multiViewModel.Form().Show(data);
                break;
            case "CustomerCallback":
                data["WorkflowTaskActivityTitle"] = "Master Customer Callback Approval Task";
                data["ID"] = data.WorkflowID;
                data["SPTaskListID"] = "CAE33D0A-F5E7-423A-8F0C-F5F5E93492B5";
                data["WorkflowTaskType"] = 0;
                if (multiViewModel != null) multiViewModel.Form().Show(data);
                break;
            case "CustomerPOAEmail":
                data["WorkflowTaskActivityTitle"] = "Master Customer POA Email Approval Task";
                data["ID"] = data.WorkflowID;
                data["SPTaskListID"] = "CAE33D0A-F5E7-423A-8F0C-F5F5E93492B5";
                data["WorkflowTaskType"] = 0;
                if (multiViewModel != null) multiViewModel.Form().Show(data);
                break;
            default:
                $.ajax({
                    type: "GET",
                    url: api.server + self.url + "/" + data.ID,
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            if (data == null || data.Data == "") {
                                ShowNotification("Waring", "Workflow data is empty, please refresh workflow table.", 'gritter-warning', true);
                                return;
                            }
                            console.log(data);
                            self.ID(data.ID);
                            self.ActivityTitle(data.TaskName);
                            self.SPTaskListID(data.SPTaskListID);
                            self.SPTaskListItemID(data.SPTaskListItemID);

                            var rec = eval('(' + data.Data + ')');
                            if (self.Config.file != '') {
                                // IF MODULE PAGE
                                if (self.Config.view.IsLocalGetSelectedRow != null) self.Config.view.IsLocalGetSelectedRow(self.IsLocalGetSelectedRow);
                                SetViewModelProperty(self.Config.view, data);
                                self.Config.view.GetSelectedRow(rec, true);
                                if (self.Config.view.IsLocalGetSelectedRow != null) self.Config.view.IsLocalGetSelectedRow(false);
                            } else {
                                // IF HOME PAGE APPROVAL
                                self.Outcomes.removeAll();

                                var item = self.Config.dialogs[data.EntityName];
                                selected_row_method = 'GetSelectedRow'; if (self.Config.selectedrowmethod != null && self.Config.selectedrowmethod[data.EntityName] != '') selected_row_method = self.Config.selectedrowmethod[data.EntityName];
                                if (selected_row_method == null) selected_row_method = 'GetSelectedRow';

                                var isCCU = false; //aridya CCU 20170910
                                //aridya add for CCU 20170323
                                if (data.EntityName.lastIndexOf('CCU') > -1) {
                                    data.EntityName = data.EntityName.substr(0, data.EntityName.lastIndexOf('CCU'));
                                    isCCU = true;//aridya CCU 20170910
                                }
                                //end aridya

                                if (item.view == null) {
                                    //aridya CCU 20170910
                                    var textLoadHTML = '/Pages/Dialogs/' + data.EntityName + '.html'
                                    if (isCCU) {
                                        textLoadHTML = '/ccu/Pages/Dialogs/' + data.EntityName + '.html';
                                    } else {
                                        textLoadHTML = '/Pages/Dialogs/' + data.EntityName + '.html';
                                    }
                                    //end aridya CCU
                                    // LOAD IF MODULE NOT EXISTS
                                    $(function () {
                                        $('<script>')
                                          .attr('type', 'text/html')
                                          .attr('id', data.EntityName + 'Form')
                                          .appendTo('head')
                                          .load(textLoadHTML, function () { //aridya CCU 20170910
                                              $.getScript(item.file, function () {
                                                  item.view = new ViewModel();
                                                  console.log(selected_row_method);
                                                  SetViewModelProperty(item.view, data);
                                                  console.log("test");
                                                  console.log(item.view);
                                                  console.log(data);
                                                  is_selected_row_method = false;
                                                  if (item.view.GetParameters != null) {
                                                      item.view.OnAfterGetParameters = function () {
                                                          if (item.view.SetParameters != null) item.view.SetParameters(data);
                                                          item.view[selected_row_method](rec, true);
                                                          is_selected_row_method = true;
                                                      }
                                                      item.view.GetParameters();
                                                  }
                                                  if (item.view.GetDropdown != null) {
                                                      item.view.OnAfterGetDropdown = function () {
                                                          item.view[selected_row_method](rec, true);
                                                          is_selected_row_method = true;
                                                      }
                                                      item.view.GetDropdown();
                                                  }
                                                  if (item.view.GetData != null) item.view.GetData();
                                                  console.log("--get data ---");
                                                  console.log(item.view.GetData());
                                                  self.DialogData(null);
                                                  self.DialogFormName(data.EntityName + 'Form');
                                                  self.DialogData(item.view);
                                                  console.log("--item.root---");
                                                  console.log(item.$root);
                                                  self.Comments("");
                                                  item.view.IsWorkflow(true);
                                                  if (item.view.OnAfterGetParameters == null && item.view.OnAfterGetDropdown == null) {
                                                      console.log(selected_row_method);
                                                      item.view[selected_row_method](rec, true);
                                                  }
                                                  GetTaskOutcomes(data.SPTaskListID, data.SPTaskListItemID);
                                                  if (!is_selected_row_method) item.view[selected_row_method](rec, true);
                                              });
                                          });
                                    });
                                } else {
                                    is_selected_row_method = false;
                                    if (item.view.GetParameters != null) {
                                        item.view.OnAfterGetParameters = function () {
                                            item.view[selected_row_method](rec, true);
                                            is_selected_row_method = true;
                                        }
                                        item.view.GetParameters();
                                    }
                                    if (item.view.GetDropdown != null) {
                                        item.view.OnAfterGetDropdown = function () {
                                            item.view[selected_row_method](rec, true);
                                            is_selected_row_method = true;
                                        }
                                        item.view.GetDropdown();
                                    }
                                    if (item.view.GetData != null) item.view.GetData();

                                    self.DialogData(null);
                                    self.DialogFormName(data.EntityName + 'Form');
                                    self.DialogData(item.view);
                                    self.Comments("");
                                    item.view.IsWorkflow(true);
                                    if (item.view.OnAfterGetParameters == null && item.view.OnAfterGetDropdown == null) {
                                        item.view[selected_row_method](rec, true);
                                    }
                                    GetTaskOutcomes(data.SPTaskListID, data.SPTaskListItemID);
                                    if (!is_selected_row_method) item.view[selected_row_method](rec, true);
                                }
                            }
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
        }
    };

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-18
    };

    //add by fandi for datetime
    $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });
    //end

    function GetTaskOutcomes(taskListID, taskListItemID) {
        var task = {
            TaskListID: taskListID,
            TaskListItemID: taskListItemID
        };

        var options = {
            url: "/_vti_bin/DBSNintex/Services.svc/GetTaskOutcomesByListID",
            data: ko.toJSON(task)
        };
        console.log(task);
        Helper.Sharepoint.Nintex.Post(options, OnSuccessTaskOutcomes, OnError, OnAlways);
    }

    function OnSuccessTaskOutcomes(data, textStatus, jqXHR) {
        console.log("----OnSuccessTaskOutcomes----");
        console.log(data);
        if (data.IsSuccess) {
            self.Outcomes(data.Outcomes);

            self.IsPendingNintex(data.IsPending);
            self.IsAuthorizedNintex(data.IsAuthorized);
            self.MessageNintex(data.Message);

            // Get transaction Data
            //GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());

            // lock current task only if user had permission
            if (self.IsPendingNintex() == true && self.IsAuthorizedNintex() == true) {
                LockAndUnlockTaskHub("Lock", self.WorkflowInstanceID(), self.ApproverId(), self.SPTaskListItemID(), self.ActivityTitle());
            }
        } else {
            ShowNotification("An error Occured", jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    function LockAndUnlockTaskHub(lockType, instanceId, approverID, taskId, activityTitle) {
        if (IsTaskHubConnected) {
            var data = {
                WorkflowInstanceID: instanceId,
                ApproverID: approverID,
                TaskID: taskId,
                ActivityTitle: activityTitle,
                LoginName: spUser.LoginName,
                DisplayName: spUser.DisplayName
            };

            var LockType = lockType.toLowerCase() == "lock" ? "LockTask" : "UnlockTask";

            // try lock current task
            taskProxy.invoke(LockType, data)
				.fail(function (err) {
				    ShowNotification("LockType Task Failed", err, "gritter-error", true);
				}
			);
        } else {
            ShowNotification("Task Manager", "There is no active connection to task server manager.", "gritter-error", true);
        }
    }

    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    function OnAlways() {
        //$box.trigger('reloaded.ace.widget');
    }

    //Function get available user get data

    function ValidationAvailable() {
        //get user role entity from api config
        data = api.url.idmunitchecker;

        if (self.Config.customerListUserEntity != null && self.Config.customerListUserEntity == true) {
            self.UserEntity(true);
            DelayGetData();
        }
        else {
            DelayEntityWorkflow();
        }
    }
    var oTimeOut;
    function DelayEntityWorkflow() {
        console.log(spUser);
        console.log(Const_RoleName);
        if (spUser == undefined || Object.keys(Const_RoleName).length == 0) {
            console.log("run timeout...1");
            oTimeOut = setTimeout(function () { startEntityWorkflow() }, 2000);
        } else {
            console.log("normal1");
            startEntityWorkflow();
        }
    }

    var oTimeOut2;
    function DelayGetData() {

        if (accessToken == null) {
            console.log("run timeout...2");
            oTimeOut2 = setTimeout(function () { self.GetData() }, 2000);
        } else {
            console.log("normal2");
            GetData();
        }
    }


    function startEntityWorkflow() {
        if (spUser == null || Object.keys(Const_RoleName).length == 0) {
            DelayEntityWorkflow();
            return;
        }
        if (oTimeOut != null) {
            clearTimeout(oTimeOut);
        }
        // var spUserHasValue = false;
        // for (var i = 0; i < 10 && !spUserHasValue; i++) {
        //     console.log("check spuser");
        //  if (spUser != null) {
        spUserHasValue = true;
        //         console.log("check done");
        //looping under user login roles
        for (i = 0; i < spUser.Roles.length; i++) {
            //looping under idm role config
            for (j = 0; j < data.length; j++) {
                //get current role equals between config and login user
                if (Const_RoleName[spUser.Roles[i].ID].toLowerCase() == data[j].toLowerCase()) { //if (spUser.Roles[i].Name.toLowerCase() == data[j].toLowerCase()) {
                    if (Const_RoleName[spUser.Roles[i].ID].toLowerCase()) { //if (spUser.Roles[i].Name.toLowerCase()) {
                        self.UserEntity(true);
                        self.GetData();
                    }
                }
            }
            //     }
            //  }
        }
    }


    //Function to Read All Customers
    function GetData() {
        console.log('get Data');
        if (accessToken == null) {
            console.log(accessToken);
            DelayGetData();
            return;
        }
        if (oTimeOut2 != null) {
            clearTimeout(oTimeOut2);
        }
        // widget reloader function start
        /*
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });*/
        // widget reloader function end
        console.log("ajxreqest");
        // declare options variable for ajax get request
        var options = {
            url: api.server + self.url,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder(),
                isHomeMenu: self.IsHomeMenu()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }
    //Function to validation dynamic field
    function IsvalidField() { /*
     $("[name^=days]").each(function () {
     $(this).rules('add', {
     required: true,
     maxlength: 2,
     number: true,
     messages: {
     required: "Please provide a valid Day.",
     maxlength: "Please provide maximum 2 number valid day value.",
     number: "Please provide number"
     }
     });
     }); */
        return true;
    }

    function SetStatusMenu() {
        if (self.Config.url) {

        }
    }
    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter

        self.FilterValue("");
        if (self.Config.view != undefined) {
            if (self.Config.view.GetFilterValue != undefined) {
                var valFileter = self.Config.view.GetFilterValue();
                self.FilterValue(valFileter);
            }
        }



        var filters = [];

        // FORCE FILTER BY ENTITY NAME BY URL
        if (self.Config.url != '') filters.push({ Field: 'EntityName', Value: self.Config.url.substr(self.Config.url.lastIndexOf('/') + 1) });

        //change by fandi
        //if (self.FilterTaskName() != "") filters.push({ Field: 'TaskName', Value: self.FilterTaskName() });
        //if (self.FilterActionType() != "") filters.push({ Field: 'ActionType', Value: self.FilterActionType() });
        if (self.TaskName() != "") filters.push({ Field: 'TaskName', Value: self.TaskName() });
        if (self.ActionType() != "") filters.push({ Field: 'ActionType', Value: self.ActionType() });
        //end
        if (self.FilterValue() != "") filters.push({ Field: 'FilterValue', Value: self.FilterValue() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

        return filters;
    };

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            $.each(data.Rows, function (index, item) {
                //console.log(item);
                //console.log(item.Data);

                if (item.Data != "") {
                    var value = '';
                    ar = item.LastModifiedBy.split('|');
                    item.LastModifiedBy = ar[ar.length - 1];
                    rec = eval('(' + item.Data + ')');
                    if (rec.length) {
                        value = '';
                        $.each(rec, function (index, item) {

                            if (item != null && item.Value != undefined)
                                value += item.Value + ' ';
                        });
                        item.Value = value;
                        //console.log(value);
                        //console.log(item);
                    }
                    var except = {};
                    except['ID'] = true;
                    except['LastModifiedBy'] = true;
                    except['LastModifiedDate'] = true;
                    $.each(rec, function (name, value) {
                        if (!except[name])
                            //console.log(name);
                            item[name] = value;
                        //console.log('ADD PROPERTY ' + name + ' : ' +value );
                    });
                    $.each(self.Config.columns.fields, function (index, sitem) {
                        //console.log('ADD PROPERTY ' + sitem + ' : ' + rec[sitem]);
                        item[sitem] = rec[sitem];
                    });
                }
                //console.log(item);
            });

            self.Rows(data.Rows);
            //console.log(data.Rows);
            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }

    self.PostWorkflow = function (EntityName, ActionType, keyID, data) {
        var body = {
            //Title: WorkflowEntityID,
            ActionType: ActionType,
            EntityName: EntityName,
            KeyID: keyID,
            Data: data,
            __metadata: {
                type: "SP.Data.WorkflowEntityListItem"
            }
        };
        Helper.Sharepoint.List.Add({
            url: config.sharepoint.url.api + "/Lists(guid'" + config.sharepoint.workflowEntityId + "')/Items",
            data: JSON.stringify(body),
            digest: jQuery("#__REQUESTDIGEST").val()
        }, function (data, textStatus, jqXHR) {
            // NO RESPOND //
            if (jqXHR.status = 200) {
                console.log(data);
                //self.PostWorkflow(rec.EntityName,rec.ActionType,data.ID);
                // send notification
                $('.modal').modal('hide');
                ShowNotification('Create Workflow Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                //if(callback!=null)callback(data);
            } else {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
            }
        }, function (jqXHR, textStatus, errorThrown) {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }, function () {

        });
    }

    self.PostCheckWorkflow = function (EntityName, ActionType, keyID, data) {
        $.ajax({
            url: config.sharepoint.url.api
            + "/Lists(guid'" + config.sharepoint.workflowEntityId
            + "')/Items?$filter=(EntityName eq '" + EntityName
            + "' and (ActionType eq 'UPDATE' or ActionType eq 'DELETE') and (Workflow0 eq 0 or Workflow0 eq 2) and (KeyID eq '" + keyID + "'))",
            type: 'GET',
            headers: {
                "Accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
            },
            success: function (sdata) {
                console.log(sdata);
                if (sdata.d.results.length > 0) {
                    $('.modal').modal('hide');
                    ShowNotification('WARNING', 'Add workflow failed. Data waiting for approval proccess.', 'gritter-warning', false);
                } else {
                    self.PostWorkflow(EntityName, ActionType, keyID, data);
                }
            },
            error: function (sdata) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
            }
        });
    }

    self.Post = function (name, task, action, data, callback) {
        var rec = {
            "EntityName": name,
            "TaskName": task,
            "ActionType": action,
            "Data": ko.toJSON(data)
        }

        $.ajax({
            type: "POST",
            url: api.server + self.url,
            data: ko.toJSON(rec),
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    console.log(data);
                    //self.PostWorkflow(rec.EntityName,rec.ActionType,data.ID);
                    // send notification
                    ShowNotification('Create Workflow Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                    if (callback != null) callback(data);
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    self.ApprovalProcess = function (data) {
        var text = String.format("{0}<br/><br/>Do you want to <b>{1}</b> this task?", data.Description, data.Name);
        var outcomes = config.nintex.outcome.uncompleted;
        bootbox.confirm(text, function (result) {
            if (result) {
                CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
            }
        });

        /*
        // while user reject, revise or cancel the task, don't validate the form and the form data will not stored on database
        if(outcomes.indexOf(data.Name) > -1){
            if(self.Comments() == ""){
                ShowNotification("Task Validation Info", "Please type any comment before revise this task.", "gritter-warning", false);
            }else{
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                    }
                });
            }
        }else{
            // validation
            var form = $("#aspnetForm");
            form.validate();

            if(form.valid()) {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        //alert(JSON.stringify(ko.toJS(self.PaymentMaker().Payment.LLDCode)))
                        // store data to db
                        SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);

                        // call nintex api
                        //CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                    }
                });
            }
        }*/
    };

    function CompletingTask(taskListID, taskListItemID, taskTypeID, outcomeID, comments) {
        var task = {
            TaskListID: taskListID,
            TaskListItemID: taskListItemID,
            OutcomeID: outcomeID,
            Comment: comments
        };
        /*
                var endPointURL;
                switch(taskTypeID){
                    // Request Approval
                    case 0 : endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondApprovalTask";
                        break;

                    // Request Review
                    case 1 : endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondReviewTask";
                        break;

                    // Flexi Task
                    case 4 : endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondFlexiTask";
                        break;
                }
        */
        var options = {
            url: "/_vti_bin/DBSNintex/Services.svc/RespondApprovalTask",
            data: ko.toJSON(task)
        };

        Helper.Sharepoint.Nintex.Post(options, OnSuccessCompletingTask, OnError, OnAlways);
    }

    function OnSuccessCompletingTask(data, textStatus, jqXHR) {
        if (data.IsSuccess) {
            console.log(data);
            // send notification
            ShowNotification('Completing Task Success', jqXHR.responseJSON.Message, 'gritter-success', false);

            // close dialog task
            $("#modal-form-workflow").modal('hide');

            // reload tasks
        } else {
            ShowNotification('Completing Task Failed', jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // OVERIDE View GetSelectedRow
    if (self.Config.view != null) {
        selected_row_method = 'GetSelectedRow'; if (self.Config.selectedrowmethod != null && self.Config.selectedrowmethod != '') selected_row_method = self.Config.selectedrowmethod;
        if (self.Config.view[selected_row_method] != null) {
            self.ViewGetSelectedRow = self.Config.view[selected_row_method];
            self.ViewGetSelectedRowMain = self.Config.view['GetSelectedRow'];
            self.Config.view.GetSelectedRow = function (data, readonly) {
                if (readonly == true) {
                    self.Config.view.Readonly(true);
                    self.Readonly(true);
                } else {
                    self.Config.view.Readonly(false);
                    self.Readonly(false);
                }
                if (self.IsLocalGetSelectedRow) {
                    self.ViewGetSelectedRow(data);
                } else {
                    var result = self.ViewGetSelectedRowMain(data);
                    if (result != undefined && result == true) {
                        self.ValidationAvailable();
                    }
                }
                self.IsLocalGetSelectedRow = false;
            }
        } else {
            self.Config.view.GetSelectedRow = function (data, readonly) {

            }
        }
    }

    function FormTemplate() {
        console.log();
        if (self.Config.file == null || self.Config.file == '')
            return ''
            + '	<div id="modal-form-workflow" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">'
            + '		<div class="modal-dialog" style="width:100%">'
            + '			<div class="modal-content">'
            + '				<div class="alert alert-danger" data-bind="visible: !$root.IsAuthorizedNintex()">'
            + '					<strong>'
            + '						<i class="icon-remove"></i>'
            + '						Unauthorized!'
            + '					</strong>'
            + '					<span data-bind="text: $root.MessageNintex"></span>'
            + '				</div>'
            + '				<div class="modal-body overflow-visible">'
            + '					<div class="row">'
            + '						<div class="col-xs-12">'
            + '							<div class="tabbable" id="tabbable-workflow">'
            + '								<ul class="nav nav-tabs" id="myTab">'
            + '									<li class="active">'
            + '										<a data-toggle="tab" href="#transaction">'
            + '											<i class="blue icon-credit-card bigger-110"></i>'
            + '											<span data-bind="text: ActivityTitle"></span>'
            + '										</a>'
            + '									</li>'
    //		+ '									<li>'
    //		+ '										<a data-toggle="tab" href="#history">'
    //		+ '											<i class="blue icon-comments bigger-110"></i>'
    //		+ '											History'
    //		+ '										</a>'
    //		+ '									</li>'
            + '								</ul>'
            + '							<div class="tab-content">'
            + '								<div id="transaction" class="tab-pane in active">'
            + '								    <div id="transaction-progress" class="col-sm-4 col-sm-offset-4" style="display: none">'
            + '                                       <h4 class="smaller blue">Please wait...</h4>'
            + '                                       <span class="space-4"></span>'
            + '                                       <div class="progress progress-small progress-striped active">'
            + '                                           <div class="progress-bar progress-bar-info" style="width:100%"></div>'
            + '                                       </div>'
            + '                                    </div>'
            + '									<div id="transaction-data" data-bind="template: { name: ApprovalTemplate(), data: ApprovalData() }"></div>'
            + '								</div>'
    //		+ '								<div id="history" class="tab-pane">'
    //		+ '								</div>'
            + '							</div>'
            + '						</div>'
            + '					</div>'
            + '				</div>'
            + '			</div>'
            + '			<div class="modal-footer">'
            + '				<div class="row" data-bind="visible: $root.IsAuthorizedNintex()">'
            + '		    		<div class="form-group">'
            + '		        		<label class="col-sm-3 control-label bolder no-padding-right" for="workflow-comments">Comments</label>'
            + '		       			<div class="col-xs-9 align-left">'
            + '	                 		<div class="clearfix">'
            + '	                    	<textarea class="col-lg-12" id="workflow-comments" rows="4" data-rule-required="true" data-bind="value: $root.Comments, enable: $root.IsPendingNintex()" placeholder="Type any comment here..."></textarea>'
            + '	                	</div>'
            + '	            	</div>'
            + '		    	</div>'
            + '			</div>'
            + '			<div class="space-8"></div>'
            + '			<span data-bind="foreach: Outcomes, visible: $root.IsAuthorizedNintex()">'
            + '				<button data-bind="click: $root.ApprovalProcess, enable: $root.IsPendingNintex()" class="btn btn-sm btn-primary">'
            + '					<i class="icon-approve"></i>'
            + '					<span data-bind="text: Name"></span>'
            + '				</button>'
            + '			</span>'
            + '			<button class="btn btn-sm btn-success" data-dismiss="modal">'
            + '				<i class="icon-remove"></i>'
            + '				Close'
            + '			</button>'
            + '			</div>'
            + '		</div>'
            + '	</div>';
        else return '';
    }

    function TableTemplate() {
        var tempTemplate;

        if (self.UserEntity() != null || (self.Config.customerListUserEntity != null && self.Config.customerListUserEntity == true)) {
            tempTemplate = '<div id="widget-box-workflow" class="widget-box" data-bind="visible: $root.UserEntity()">';
        } else {
            tempTemplate = '<div id="widget-box-workflow" class="widget-box">';
        }

        return tempTemplate + '<div class="widget-header widget-hea1der-small header-color-dark">'
        + '		<h6>Workflow</h6>'
        + '		<div class="widget-toolbar">'
        + '			<label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">'
        + '				<i class="blue icon-filter"></i>'
        + '				<input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridProperties().AllowFilter" />'
        + '				<span class="lbl"></span>'
        + '			</label>'
        + '				<a href="#" data-bind="click: GetData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">'
        + '					<i class="blue icon-refresh"></i>'
        + '				</a>'
        + '		</div>'
        + '	</div>'
        + '	<div class="widget-body">'
        + '		<div class="widget-main padding-0">'
        + '			<div class="slim-scroll" data-height="400">'
        + '				<div class="content">'
        + '					<div class="table-responsive">'
        + '						<div class="dataTables_wrapper" role="grid">'
        + '							<table id="WorkflowEntity-table" class="table table-striped table-bordered table-hover dataTable">'
        + '                                <thead>'
        + '                                    <tr data-bind="with: GridProperties">'
        + '                                        <th style="width:50px">No.</th>' + self.Config.columns.sort
        + '                                        <th data-bind="click: function () { Sorting(\'TaskName\'); }, css: GetSortedColumn(\'TaskName\')">Name</th>'
        + '                                        <th data-bind="click: function () { Sorting(\'ActionType\'); }, css: GetSortedColumn(\'ActionType\')">Action</th>'
        + '                                        <th data-bind="click: function () { Sorting(\'LastModifiedBy\'); }, css: GetSortedColumn(\'LastModifiedBy\')">Modified By</th>'
        + '                                        <th data-bind="click: function () { Sorting(\'LastModifiedDate\'); }, css: GetSortedColumn(\'LastModifiedDate\')">Modified Date</th>'
        + '                                    </tr>'
        + '                                </thead>'
        + '                                <thead data-bind="visible: GridProperties().AllowFilter" class="table-filter">'
        + '                                    <tr>'
        + '                                        <th class="clear-filter">'
        + '                                           <a href="#" data-bind="click: ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">'
        + '                                                <i class="green icon-trash"></i>'
        + '                                            </a>'
        + '                                        </th>' + self.Config.columns.filter
        + '                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.TaskName, event: { change: GridProperties().Filter }" /></th>'
        + '                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.ActionType, event: { change: GridProperties().Filter }" /></th>'
        + '                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterModifiedBy, event: { change: GridProperties().Filter }" /></th>'
        //+ '                                        <th><input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterModifiedDate, event: { change: GridProperties().Filter }"/></th>'
        + '                                        <th><input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterModifiedDate, event: { change: GridProperties().Filter }" /></th>'
        + '                                    </tr>'
        + '                                </thead>'
        + '                                <tbody data-bind="foreach: Rows, visible: Rows().length > 0">'
        + '                                    <tr data-bind="click: $root.GetSelectedRow" data-toggle="modal">'
        + '                                        <td><span data-bind="text: RowID"></span></td>' + self.Config.columns.row
        + '                                        <td><span data-bind="text: TaskName"></span></td>'
        + '                                        <td><span data-bind="text: $root.ActionTypeFormat(ActionType)"></span></td>'
        + '                                        <td><span data-bind="text: LastModifiedBy"></span></td>'
        + '                                        <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>'
        + '                                    </tr>'
        + '                                </tbody>'
        + '                                <tbody data-bind="visible: Rows().length == 0">'
        + '                                    <tr>'
        + '                                        <td colspan="6" class="text-center">'
        + '                                            No entries available.'
        + '                                       </td>'
        + '                                    </tr>'
        + '                                </tbody>'
        + '                            </table>'
        + '						</div>'
        + '					</div>'
        + '				</div>'
        + '			</div>'
        + '			<div class="widget-toolbox padding-8 clearfix">'
        + '				<div class="row" data-bind="with: GridProperties">'
        + '					<div class="col-sm-6">'
        + '						<div class="dataTables_paginate paging_bootstrap pull-left">'
        + '							Showing <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>'
        + '							rows of <span data-bind="text: Total"></span>'
        + '							entries'
        + '						</div>'
        + '					</div>'
        + '					<div class="col-sm-3">'
        + '						<div class="dataTables_paginate paging_bootstrap">'
        + '							Page <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width:50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page"/>'
        + '							of <span data-bind="text: TotalPages"></span>'
        + '						</div>'
        + '					</div>'
        + '					<div class="col-sm-3">'
        + '						<div class="dataTables_paginate paging_bootstrap">'
        + '							<ul class="pagination">'
        + '								<li data-bind="click: FirstPage, attr: { class : Page() > 1 ? \'\' : \'disabled\' }">'
        + '									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">'
        + '										<i class="icon-double-angle-left"></i>'
        + '									</a>'
        + '								</li>'
        + '								<li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? \'\' : \'disabled\' }">'
        + '									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">'
        + '										<i class="icon-angle-left"></i>'
        + '									</a>'
        + '								</li>'
        + '								<li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? \'\' : \'disabled\' }">'
        + '									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">'
        + '										<i class="icon-angle-right"></i>'
        + '									</a>'
        + '								</li>'
        + '								<li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? \'\' : \'disabled\' }">'
        + '									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">'
        + '										<i class="icon-double-angle-right"></i>'
        + '									</a>'
        + '								</li>'
        + '							</ul>'
        + '						</div>'
        + '					</div>'
        + '				</div>'
        + '			</div>'
        + '		</div>'
        + '	</div>'
        + FormTemplate()
        + '</div>'
    + '';
    }

    self.ActionTypeFormat = function (text) {
        if (text.indexOf("Approval Master User") != -1) {
            var splittext = text.split('(');
            var result = splittext[splittext.length - 1].replace(')', '');
            return result;
        } else {
            return text;
        }
    }

    ko.applyBindings(self, $('#widget-box-workflow')[0]);

    self.ValidationAvailable();
    if (api.workflow.entity.interval != 0) {
        setInterval(function () {
            if (accessToken != null)
                self.ValidationAvailable();
        }, api.workflow.entity.interval);
    }
};