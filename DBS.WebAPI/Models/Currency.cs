﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("Currency")]
    public class CurrencyModel
    {
        /// <summary>
        /// Currency ID.
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Currency ID.
        /// </summary>
        public int? DraftCurrencyID { get; set; }

        /// <summary>
        /// Curreny Code.
        /// </summary>
        // [MaxLength(3, ErrorMessage = "Currency Code is must be in {1} characters.")]
        public string Code { get; set; }

        /// <summary>
        /// Currency Description.
        /// </summary>
        // [MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Currency Description.
        /// </summary>
        // [MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string CodeDescription { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
        #region IPE
        public decimal RupiahRate { get; set; }
        #endregion
    }

    public class CurrencyRateModel : CurrencyModel
    {
        public decimal RupiahRate { get; set; }
    }

    public class CurrencyRow : CurrencyRateModel
    {
        public int RowID { get; set; }

    }

    public class CurrencyGrid : Grid
    {
        public IList<CurrencyRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class CurrencyFilter : Filter { }
    #endregion

    #region Interface
    public interface ICurrencyRepository : IDisposable
    {
        bool GetCurrencyByID(int id, ref CurrencyRateModel currency, ref string message);
        bool GetCurrencyRateByID(int id, ref CurrencyRateModel currency, ref string message);
        bool GetCurrency(ref IList<CurrencyRateModel> currencies, int limit, int index, ref string message);
        bool GetCurrency(ref IList<CurrencyModel> currency, ref string message);
        bool GetCurrencybyCurrency(ref IList<CurrencyModel> currency, int id, ref string message);
        bool GetCurrency(int page, int size, IList<CurrencyFilter> filters, string sortColumn, string sortOrder, ref CurrencyGrid currency, ref string message);
        bool GetCurrency(CurrencyFilter filter, ref IList<CurrencyRateModel> currencies, ref string message);
        bool GetCurrency(string key, int limit, ref IList<CurrencyRateModel> currencies, ref string message);
        bool AddCurrency(CurrencyRateModel currency, ref string message);
        bool UpdateCurrency(int id, CurrencyRateModel currency, ref string message);
        bool DeleteCurrency(int id, ref string message);
    }
    #endregion

    #region Repository
    public class CurrencyRepository : ICurrencyRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetCurrencyByID(int id, ref CurrencyRateModel currency, ref string message)
        {
            bool isSuccess = false;

            try
            {
                currency = (from a in context.Currencies
                            where a.IsDeleted.Equals(false) && a.CurrencyID.Equals(id)
                            select new CurrencyRateModel
                            {
                                ID = a.CurrencyID,
                                Code = a.CurrencyCode,
                                Description = a.CurrencyDescription,
                                RupiahRate = a.RupiahRate,
                                LastModifiedDate = a.CreateDate,
                                LastModifiedBy = a.CreateBy
                            }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCurrencyRateByID(int id, ref CurrencyRateModel currency, ref string message)
        {
            bool isSuccess = false;

            try
            {
                currency = (from a in context.Currencies
                            where a.IsDeleted.Equals(false) && a.CurrencyID.Equals(id)
                            select new CurrencyRateModel
                            {
                                ID = a.CurrencyID,
                                Code = a.CurrencyCode,
                                Description = a.CurrencyDescription,
                                RupiahRate = a.RupiahRate,
                                LastModifiedDate = a.CreateDate,
                                LastModifiedBy = a.CreateBy
                            }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCurrency(ref IList<CurrencyRateModel> currency, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    currency = (from a in context.Currencies
                                where a.IsDeleted.Equals(false)
                                orderby new { a.CurrencyCode, a.CurrencyDescription }
                                select new CurrencyRateModel
                                {
                                    ID = a.CurrencyID,
                                    Code = a.CurrencyCode,
                                    Description = a.CurrencyDescription,
                                    RupiahRate = a.RupiahRate,
                                    LastModifiedBy = a.CreateBy,
                                    LastModifiedDate = a.CreateDate
                                }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCurrency(ref IList<CurrencyModel> currency, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    currency = (from a in context.Currencies
                                where a.IsDeleted.Equals(false)
                                orderby new { a.CurrencyCode, a.CurrencyDescription }
                                select new CurrencyModel
                                {
                                    ID = a.CurrencyID,
                                    Code = a.CurrencyCode,
                                    Description = a.CurrencyDescription,
                                    CodeDescription = a.CurrencyCode + " " + a.CurrencyDescription,
                                    LastModifiedBy = a.CreateBy,
                                    LastModifiedDate = a.CreateDate
                                }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCurrency(int page, int size, IList<CurrencyFilter> filters, string sortColumn, string sortOrder, ref CurrencyGrid customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                CurrencyRateModel filter = new CurrencyRateModel();
                if (filters != null)
                {
                    filter.Code = filters.Where(a => a.Field.Equals("Code")).Select(a => a.Value).SingleOrDefault();
                    filter.RupiahRate = filters.Where(a => a.Field.Equals("RupiahRate")).Select(a => a.Value).SingleOrDefault() != null ? Convert.ToDecimal(filters.Where(a => a.Field.Equals("RupiahRate")).Select(a => a.Value).SingleOrDefault()) : 0;
                    filter.Description = filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.Currencies
                                let isCode = !string.IsNullOrEmpty(filter.Code)
                                let isDesc = !string.IsNullOrEmpty(filter.Description)
                                let isRupiahRate = filter.RupiahRate == 0 ? false : true
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isCode ? a.CurrencyCode.Contains(filter.Code) : true) &&
                                    (isDesc ? a.CurrencyDescription.Contains(filter.Description) : true) &&
                                    (isRupiahRate ? a.RupiahRate == filter.RupiahRate : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new CurrencyRateModel
                                {
                                    ID = a.CurrencyID,
                                    Code = a.CurrencyCode,
                                    Description = a.CurrencyDescription,
                                    RupiahRate = a.RupiahRate,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    customer.Page = page;
                    customer.Size = size;
                    customer.Total = data.Count();
                    customer.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    customer.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new CurrencyRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Code = a.Code,
                            Description = a.Description,
                            RupiahRate = a.RupiahRate,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCurrency(CurrencyFilter filter, ref IList<CurrencyRateModel> customer, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetCurrency(string key, int limit, ref IList<CurrencyRateModel> currency, ref string message)
        {
            bool isSuccess = false;

            try
            {
                currency = (from a in context.Currencies
                            where a.IsDeleted.Equals(false) &&
                                a.CurrencyCode.Contains(key) || a.CurrencyDescription.Contains(key)
                            orderby new { a.CurrencyCode, a.CurrencyDescription }
                            select new CurrencyRateModel
                            {
                                Code = a.CurrencyCode,
                                Description = a.CurrencyDescription,
                                RupiahRate = a.RupiahRate,
                            }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddCurrency(CurrencyRateModel currency, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.Currencies.Where(a => a.CurrencyCode.Equals(currency.Code) && a.IsDeleted.Equals(false)).Any())
                {
                    context.Currencies.Add(new Entity.Currency()
                    {
                        CurrencyCode = currency.Code,
                        CurrencyDescription = currency.Description,
                        RupiahRate = currency.RupiahRate,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Currency data for currency code {0} is already exist.", currency.Code);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateCurrency(int id, CurrencyRateModel currency, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Currency data = context.Currencies.Where(a => a.CurrencyID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.Currencies.Where(a => a.CurrencyID != currency.ID && a.CurrencyCode.Equals(currency.Code) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Currency Code {0} is exist.", currency.Code);
                    }
                    else
                    {
                        data.CurrencyCode = currency.Code;
                        data.CurrencyDescription = currency.Description;
                        data.RupiahRate = currency.RupiahRate;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Currency data for Currency ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteCurrency(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Currency data = context.Currencies.Where(a => a.CurrencyID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Currency data for Currency ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public bool GetCurrencybyCurrency(ref IList<CurrencyModel> currency, int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    currency = (from a in context.Currencies
                                where a.IsDeleted == false && a.CurrencyID == id
                                orderby new { a.CurrencyCode, a.CurrencyDescription }
                                select new CurrencyModel
                                {
                                    ID = a.CurrencyID,
                                    Code = a.CurrencyCode,
                                    Description = a.CurrencyDescription,
                                    CodeDescription = a.CurrencyCode + " " + a.CurrencyDescription,
                                    RupiahRate = a.RupiahRate,
                                    LastModifiedBy = a.CreateBy,
                                    LastModifiedDate = a.CreateDate
                                }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
    }
    #endregion
}