//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_CustomerUnderlying
    {
        public string ReferenceNumber { get; set; }
        public bool IsStatementLetter { get; set; }
        public int UnderlyingDocID { get; set; }
        public int DocTypeID { get; set; }
        public int CurrencyID { get; set; }
        public decimal Amount { get; set; }
        public System.DateTime DateOfUnderlying { get; set; }
        public string SupplierName { get; set; }
        public string AttachmentNo { get; set; }
    }
}
