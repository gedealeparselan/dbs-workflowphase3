/**
 * Created by ruddycahyadi on 9/18/2014.
 */

$(document).ready(function(){
    var workspace = $("#s4-workspace").attr("style");
    var sidebar = $("#sidebar").css("display");

    //alert(sidebar);
    if(workspace == undefined){
        var height = $(window).height();
        var width = $(window).width();
        //alert(height);
        $("#s4-workspace").attr("style", "height:"+ height +"px; width:"+ width +"px");
    }

    if(sidebar == "none"){
        $(".main-content").css("margin-left", "0");
    }

    // Logout handler
    $("#ctl00_DBSHeaderNavigation_aLogout").click(function(){
        if(confirm("Do you want to logout now?")){
            $.removeCookie(api.cookie.name);
            $.removeCookie(api.cookie.spUser);

            //if($.cookie(api.cookie.name) == undefined){
            //	alert("Cookie has been removed")
            //}

            return true;
        }else{
            return false;
        }
    });

});