﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class CustomerUnderlyingFileController : ApiController
    {
        private string message = string.Empty;

        private readonly ICustomerUnderlyingFileRepository repository = null;

        public CustomerUnderlyingFileController()
        {
            this.repository = new CustomerUnderlyingFileRepository();
        }

        public CustomerUnderlyingFileController(ICustomerUnderlyingFileRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all CustomerUnderlyingFile.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<CustomerUnderlyingFileModel>))]
        public HttpResponseMessage Get()
        {
            IList<CustomerUnderlyingFileModel> output = new List<CustomerUnderlyingFileModel>();

            if (!repository.GetCustomerUnderlyingFile(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerUnderlyingFileModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get CustomerUnderlyingFile with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<CustomerUnderlyingFileModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<CustomerUnderlyingFileModel> output = new List<CustomerUnderlyingFileModel>();

            if (!repository.GetCustomerUnderlyingFile(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerUnderlyingFileModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Customer Underlying File for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(BankGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<CustomerUnderlyingFileFilter> filters)
        {
            CustomerUnderlyingFileGrid output = new CustomerUnderlyingFileGrid();

            if (!repository.GetCustomerUnderlyingFile(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerUnderlyingFileGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find CustomerUnderlyingFile using criterias Code or Description.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<CustomerUnderlyingFileModel>))]
        [HttpGet]
        [Route("api/CustomerUnderlyingFile/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<CustomerUnderlyingFileModel> output = new List<CustomerUnderlyingFileModel>();

            if (!repository.GetCustomerUnderlyingFile(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerUnderlyingFileModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Customer Underlying File by ID.
        /// </summary>
        /// <param name="id">Customer Underlying ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(CustomerUnderlyingFileModel))]
        [Route("api/CustomerUnderlyingFile/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            CustomerUnderlyingFileModel output = new CustomerUnderlyingFileModel();

            if (!repository.GetCustomerUnderlyingFileByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<CustomerUnderlyingFileModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new Customer Underlying File.
        /// </summary>
        /// <param name="CustomerUnderlyingFile">Customer Underlying File.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
      //  [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(CustomerUnderlyingFileModel CustomerUnderlyingFile)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddCustomerUnderlyingFile(CustomerUnderlyingFile, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Customer Underlying File data has been created." });
                }
            }
        }

        /// <summary>
        /// Add Customer Underlying File get transaction ID.
        /// </summary>
        /// <returns>UnderlyingFileID</returns>
        [ResponseType(typeof(long))]
        [Route("api/CustomerUnderlyingFile/AddFile")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage AddUnderlyingFile(CustomerUnderlyingFileModel CustomerUnderlyingFile)
        {
           
            long output = 0;

            if (!repository.AddCustomerUnderlyingFileReturnID(CustomerUnderlyingFile, ref message, ref output))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<long>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Modify existing CustomerUnderlyingFile.
        /// </summary>
        /// <param name="id">ID of Customer Underlying File to be modify.</param>
        /// <param name="CustomerUnderlyingFile">Customer Underlying File data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        //[Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        [Route("api/CustomerUnderlyingFile/{id}")]
        public HttpResponseMessage Put(int id, CustomerUnderlyingFileModel CustomerUnderlyingFile)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateCustomerUnderlyingFile(id, CustomerUnderlyingFile, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Customer Underlying File data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting Customer Underlying File.
        /// </summary>
        /// <param name="id">ID of Customer Underlying File to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/CustomerUnderlyingFile/{id}")]
        [ResponseType(typeof(string))]
       // [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteCustomerUnderlyingFile(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Customer Underlying File data has been deleted." });
                }
            }
        }

        /// <summary>
        /// Get Customer Underlying Parameter by Underlying by transaction ID.
        /// </summary>
        /// <param name="id">Transaction PPU Maker Application ID.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(BankGrid))]
        [Route("api/CustomerUnderlyingFile/Transaction/{id}")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetUnderlyingFile(long id, int? page, int? size, string sort_column, string sort_order, IList<CustomerUnderlyingFileFilter> filters)
        {
            CustomerUnderlyingFileGrid output = new CustomerUnderlyingFileGrid();

            if (!repository.GetCustomerUnderlyingFile(id, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerUnderlyingFileGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Customer Underlying Parameter by Underlying by transaction ID.
        /// </summary>
        /// <param name="id">Transaction PPU Maker Application ID.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(BankGrid))]
        [Route("api/CustomerUnderlyingFile/TransactionDeal/{id}")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetUnderlyingDealFile(long id, int? page, int? size, string sort_column, string sort_order, IList<CustomerUnderlyingFileFilter> filters)
        {
            CustomerUnderlyingFileGrid output = new CustomerUnderlyingFileGrid();

            if (!repository.GetDealCustomerUnderlyingFile(id, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerUnderlyingFileGrid>(HttpStatusCode.OK, output);
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}