//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    
    public partial class SP_WF_GetEmailContactAvailability_Result
    {
        public string CIF { get; set; }
        public string CustomerName { get; set; }
        public string RMName { get; set; }
        public string EmailTo { get; set; }
        public string BizSegmentName { get; set; }
    }
}
