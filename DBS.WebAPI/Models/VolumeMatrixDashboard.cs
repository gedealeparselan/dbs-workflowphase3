﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Runtime.Serialization;
using System.Data.Entity;

namespace DBS.WebAPI.Models
{
    [ModelName("VolumeMatrixDashboard")]
    public class VolumeMatrixDashboardModel
    {
        /// <summary>
        /// chart's model
        /// </summary>

        /// <summary>
        /// Product Name
        /// </summary>   
        public string productName { get; set; }

        /// <summary>
        /// Application Date
        /// </summary>     
        public DateTime applicationDate { get; set; }

        /// <summary>
        /// Hour
        /// </summary>       
        public int hour { get; set; }

        /// <summary>
        /// Quantity Completed
        /// </summary>        
        public int qtyCompleted { get; set; }

        /// <summary>
        /// Quantity On Progress 
        /// </summary>        
        public int qtyOnProgress { get; set; }

        /// <summary>
        /// Quantity On Progress 
        /// </summary>        
        public int qtyCancelled { get; set; }

        /// <summary>
        /// Quantity On Progress 
        /// </summary>        
        public int qtyWaitingACK1 { get; set; }

        /// <summary>
        /// Quantity On Progress 
        /// </summary>        
        public int qtyWaitingACK2 { get; set; }

        /// <summary>
        /// table's model
        /// </summary>

        /// <summary>
        /// Application ID
        /// </summary>       
        public string applicationId { get; set; }

        /// <summary>
        /// Customer Name
        /// </summary>       
        public string customerName { get; set; }

        /// <summary>
        /// Transaction Status
        /// </summary>       
        public string transactionStatus { get; set; }

        /// <summary>
        /// Created Date
        /// </summary>
        public DateTime createdDate { get; set; }

        /// <summary>
        /// TAT
        /// </summary>
        public TimeSpan TAT { get; set; }

        /// <summary>
        /// Percentage
        /// </summary>
        public int percentage { get; set; }

        /// <summmary>
        /// State ID
        /// </summary>
        public int stateID { get; set; }


    }

    public interface IVolumeMatrixDashboardChart : IDisposable
    {
        bool GetVolumeMatrixDashboardChart(string productTypeDetailID, Nullable<DateTime> startDate, Nullable<DateTime> endDate, DateTime clientDateTime, string paymentMode, ref IList<VolumeMatrixDashboardModel> allDataChart, ref string message);


    }

    public class VolumeMatrixDashboardRepository : IVolumeMatrixDashboardChart
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetVolumeMatrixDashboardChart(string productTypeDetailID, Nullable<DateTime> startDate, Nullable<DateTime> endDate, DateTime clientDateTime, string paymentMode, ref IList<VolumeMatrixDashboardModel> allDataChart, ref string message)
        {
            bool isSuccess = false;
            DateTime serverUTC = DateTime.UtcNow;
            int hourDiff, oneDayHour = 24;
            string[] productTypeDetail = productTypeDetailID.Split(',');
            IList<int> productTypeDetailInt = new List<int>();

            if (clientDateTime.Hour > serverUTC.Hour)
            {
                hourDiff = clientDateTime.Hour - serverUTC.Hour;
            }
            else if (clientDateTime.Hour < serverUTC.Hour)
            {
                hourDiff = oneDayHour + clientDateTime.Hour - serverUTC.Hour;
            }
            else
            {
                hourDiff = 0;
            }

            foreach (string id in productTypeDetail)
            {
                productTypeDetailInt.Add(int.Parse(id));
            }

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    //20160913 add aridya for payment IPE
                    if (paymentMode == "BCP2")
                    {
                        allDataChart = (from products in context.Products
                                        join transactions in
                                            (
                                                    from transaction in context.Transactions
                                                    join slas in context.SLAs
                                                    on transaction.ProductID equals slas.ProductID
                                                    join progress in context.ProgressStates
                                                    on transaction.StateID equals progress.StateID
                                                    let utcToLocal = DbFunctions.AddHours(transaction.CreateDate, hourDiff)
                                                    where (utcToLocal >= startDate) &&
                                                            (utcToLocal <= endDate) &&
                                                            (transaction.Mode.Equals("BCP2"))
                                                    select new
                                                    {
                                                        ProductID = transaction.ProductID,
                                                        CreateDate = transaction.CreateDate,
                                                        StateID = transaction.StateID,
                                                        TransactionStatus = transaction.Transaction_Status
                                                    }

                                            )
                                        on products.ProductID equals transactions.ProductID into group1
                                        from gr1 in group1.DefaultIfEmpty()
                                        let utcToLocalTransaction = gr1.CreateDate != null ? DbFunctions.AddHours(gr1.CreateDate, hourDiff) : DbFunctions.AddHours(startDate, hourDiff)
                                        where (products.IsDeleted.Equals(false)) &&
                                              (productTypeDetailInt.Contains(products.ProductID)) &&
                                              ((utcToLocalTransaction >= startDate) &&
                                               (utcToLocalTransaction <= endDate)) //&&
                                        //gr1.Mode.Equals("BCP2")
                                        //|| (from product in context.Products select product.ProductID).Contains(gr1.ProductID)
                                        select new
                                        {
                                            productname = products.ProductName,
                                            hour = utcToLocalTransaction.Value.Hour,
                                            stateID = gr1.StateID,
                                            TransactionStatus = gr1.TransactionStatus

                                        } into jointable1
                                        group jointable1 by new
                                        {
                                            Hour = jointable1.hour,
                                            ProductName = jointable1.productname,

                                        } into grouptable1
                                        select new VolumeMatrixDashboardModel
                                        {
                                            productName = grouptable1.Key.ProductName,
                                            hour = grouptable1.Key.Hour,
                                            qtyOnProgress = (from grouptable in grouptable1 where grouptable.stateID == 1 && grouptable.TransactionStatus.Trim() != "IPE ACK1 Task" && grouptable.TransactionStatus.Trim() != "IPE ACK2 Task" select grouptable).Count(),
                                            qtyCompleted = (from grouptable in grouptable1 where grouptable.stateID == 2 select grouptable).Count(),
                                            qtyCancelled = (from grouptable in grouptable1 where grouptable.stateID == 3 select grouptable).Count(),
                                            qtyWaitingACK1 = (from grouptable in grouptable1 where grouptable.TransactionStatus == "IPE ACK1 Task" select grouptable).Count(),
                                            qtyWaitingACK2 = (from grouptable in grouptable1 where grouptable.TransactionStatus == "IPE ACK2 Task" select grouptable).Count()
                                        }).ToList();
                    }
                    else if (paymentMode == "IPE")
                    {
                        allDataChart = (from products in context.Products
                                        join transactions in
                                            (
                                                    from transaction in context.Transactions
                                                    join slas in context.SLAs
                                                    on transaction.ProductID equals slas.ProductID
                                                    join progress in context.ProgressStates
                                                    on transaction.StateID equals progress.StateID
                                                    let utcToLocal = DbFunctions.AddHours(transaction.CreateDate, hourDiff)
                                                    where (utcToLocal >= startDate) &&
                                                            (utcToLocal <= endDate) &&
                                                            ((!transaction.Mode.Equals("BCP2")) || transaction.Mode.Equals(null))
                                                    select new
                                                    {
                                                        ProductID = transaction.ProductID,
                                                        CreateDate = transaction.CreateDate,
                                                        StateID = transaction.StateID,
                                                        TransactionStatus = transaction.Transaction_Status
                                                    }

                                            )
                                        on products.ProductID equals transactions.ProductID into group1
                                        from gr1 in group1.DefaultIfEmpty()
                                        let utcToLocalTransaction = gr1.CreateDate != null ? DbFunctions.AddHours(gr1.CreateDate, hourDiff) : DbFunctions.AddHours(startDate, hourDiff)
                                        where (products.IsDeleted.Equals(false)) &&
                                              (productTypeDetailInt.Contains(products.ProductID)) &&
                                              ((utcToLocalTransaction >= startDate) &&
                                               (utcToLocalTransaction <= endDate)) //&&
                                        //(!gr1.Mode.Equals("BCP2"))
                                        //|| (from product in context.Products select product.ProductID).Contains(gr1.ProductID)
                                        select new
                                        {
                                            productname = products.ProductName,
                                            hour = utcToLocalTransaction.Value.Hour,
                                            stateID = gr1.StateID,
                                            TransactionStatus = gr1.TransactionStatus

                                        } into jointable1
                                        group jointable1 by new
                                        {
                                            Hour = jointable1.hour,
                                            ProductName = jointable1.productname,

                                        } into grouptable1
                                        select new VolumeMatrixDashboardModel
                                        {
                                            productName = grouptable1.Key.ProductName,
                                            hour = grouptable1.Key.Hour,
                                            qtyOnProgress = (from grouptable in grouptable1 where grouptable.stateID == 1 && grouptable.TransactionStatus.Trim() != "IPE ACK1 Task" && grouptable.TransactionStatus.Trim() != "IPE ACK2 Task" select grouptable).Count(),
                                            qtyCompleted = (from grouptable in grouptable1 where grouptable.stateID == 2 select grouptable).Count(),
                                            qtyCancelled = (from grouptable in grouptable1 where grouptable.stateID == 3 select grouptable).Count(),
                                            qtyWaitingACK1 = (from grouptable in grouptable1 where grouptable.TransactionStatus == "IPE ACK1 Task" select grouptable).Count(),
                                            qtyWaitingACK2 = (from grouptable in grouptable1 where grouptable.TransactionStatus == "IPE ACK2 Task" select grouptable).Count()
                                        }).ToList();
                    }
                    else
                    {
                        allDataChart = (from products in context.Products
                                        join transactions in
                                            (
                                                    from transaction in context.Transactions
                                                    join slas in context.SLAs
                                                    on transaction.ProductID equals slas.ProductID
                                                    join progress in context.ProgressStates
                                                    on transaction.StateID equals progress.StateID
                                                    let utcToLocal = DbFunctions.AddHours(transaction.CreateDate, hourDiff)
                                                    where (utcToLocal >= startDate) &&
                                                            (utcToLocal <= endDate)
                                                    select new
                                                    {
                                                        ProductID = transaction.ProductID,
                                                        CreateDate = transaction.CreateDate,
                                                        StateID = transaction.StateID,
                                                        TransactionStatus = transaction.Transaction_Status
                                                    }

                                            )
                                        on products.ProductID equals transactions.ProductID into group1
                                        from gr1 in group1.DefaultIfEmpty()
                                        let utcToLocalTransaction = gr1.CreateDate != null ? DbFunctions.AddHours(gr1.CreateDate, hourDiff) : DbFunctions.AddHours(startDate, hourDiff)
                                        where (products.IsDeleted.Equals(false)) &&
                                              (productTypeDetailInt.Contains(products.ProductID)) &&
                                              ((utcToLocalTransaction >= startDate) &&
                                               (utcToLocalTransaction <= endDate))
                                        //|| (from product in context.Products select product.ProductID).Contains(gr1.ProductID)
                                        select new
                                        {
                                            productname = products.ProductName,
                                            hour = utcToLocalTransaction.Value.Hour,
                                            stateID = gr1.StateID,
                                            TransactionStatus = gr1.TransactionStatus

                                        } into jointable1
                                        group jointable1 by new
                                        {
                                            Hour = jointable1.hour,
                                            ProductName = jointable1.productname,

                                        } into grouptable1
                                        select new VolumeMatrixDashboardModel
                                        {
                                            productName = grouptable1.Key.ProductName,
                                            hour = grouptable1.Key.Hour,
                                            qtyOnProgress = (from grouptable in grouptable1 where grouptable.stateID == 1 && grouptable.TransactionStatus.Trim() != "IPE ACK1 Task" && grouptable.TransactionStatus.Trim() != "IPE ACK2 Task" select grouptable).Count(),
                                            qtyCompleted = (from grouptable in grouptable1 where grouptable.stateID == 2 select grouptable).Count(),
                                            qtyCancelled = (from grouptable in grouptable1 where grouptable.stateID == 3 select grouptable).Count(),
                                            qtyWaitingACK1 = (from grouptable in grouptable1 where grouptable.TransactionStatus == "IPE ACK1 Task" select grouptable).Count(),
                                            qtyWaitingACK2 = (from grouptable in grouptable1 where grouptable.TransactionStatus == "IPE ACK2 Task" select grouptable).Count()
                                        }).ToList();
                    }
                    //end add aridya
                }
                isSuccess = true;

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }



        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }

}