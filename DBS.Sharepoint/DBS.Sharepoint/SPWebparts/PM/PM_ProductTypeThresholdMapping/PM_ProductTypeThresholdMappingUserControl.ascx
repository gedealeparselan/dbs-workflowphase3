﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PM_ProductTypeThresholdMappingUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.PM.PM_ProductTypeThresholdMapping.PM_ProductTypeThresholdMappingUserControl" %>

<!-- widget box start -->
<div id="widget-box" class="widget-box">
	<!-- widget header start -->
	<div class="widget-header widget-hea1der-small header-color-dark">
		<h6>Product Type Threshold Mapping Table</h6>

		<div class="widget-toolbar">
			<label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
				<i class="blue icon-filter"></i>
				<input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridProperties().AllowFilter" />
				<span class="lbl"></span>
			</label>
			<a href="#" data-bind="click: GetData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
				<i class="blue icon-refresh"></i>
			</a>
		</div>
	</div>
	<!-- widget header end -->

	<!-- widget body start -->
	<div class="widget-body">
		<!-- widget main start -->
		<div class="widget-main padding-0">
			<!-- widget slim control start -->
			<div class="slim-scroll" data-height="400">
				<!-- widget content start -->
				<div class="content">

					<!-- table responsive start -->
					<div class="table-responsive">
						<div class="dataTables_wrapper" role="grid">
							<table id="ProductTypeThreshold-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr data-bind="with: GridProperties">
                                        <th style="width:50px">No.</th>
                                        <th data-bind="click: function () { Sorting('ProductTypeCode'); }, css: GetSortedColumn('ProductTypeCode')">Product Type Code</th>
                                        <th data-bind="click: function () { Sorting('GroupName'); }, css: GetSortedColumn('GroupName')">Group Name</th>
                                        <th data-bind="click: function () { Sorting('TransactionType'); }, css: GetSortedColumn('TransactionType')">Transaction Type</th>
                                        <th data-bind="click: function () { Sorting('ResidentType'); }, css: GetSortedColumn('ResidentType')">Resident Name</th>
                                        <th data-bind="click: function () { Sorting('ThresholdName'); }, css: GetSortedColumn('ThresholdName')">Threshold Name</th>
                                        <th data-bind="click: function () { Sorting('ThresholdValue'); }, css: GetSortedColumn('ThresholdValue')">Threshold Value</th>
                                        <th data-bind="click: function () { Sorting('RoundingValue'); }, css: GetSortedColumn('RoundingValue')">Rounding Value</th>
                                    </tr>
                                </thead>
                                <thead data-bind="visible: GridProperties().AllowFilter" class="table-filter">
                                    <tr>
                                        <th class="clear-filter">
                                            <a href="#" data-bind="click: ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                <i class="green icon-trash"></i>
                                            </a>
                                        </th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterProductTypeCode, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterGroupName, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterTransactionType, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterResidentType, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterThresholdType, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterThresholdValue, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterRoundingValue, event: { change: GridProperties().Filter }" /></th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: ProductTypeThresholdMappings, visible: ProductTypeThresholdMappings().length > 0">
                                    <tr data-bind="click: $root.GetSelectedRow" data-toggle="modal">
                                        <td><span data-bind="text: RowID"></span></td>
                                        <td><span data-bind="text: ProductTypeCode"></span></td> 
                                        <td><span data-bind="text: GroupName"></span></td>    
                                        <td><span data-bind="text: TransactionType2"></span></td>
                                   	    <td><span data-bind="text: ResidentType2"></span></td>
                                        <td><span data-bind="text: ThresholdType2"></span></td>     
                                        <td align="right"><span data-bind="text: formatNumber(ThresholdValue)"></span></td>     
                                        <td align="right"><span data-bind="text: formatNumber(RoundingValue)"></span></td>                   
                                    </tr>
                                </tbody>
                                <tbody data-bind="visible: ProductTypeThresholdMappings().length == 0">
                                    <tr>
                                        <td colspan="8" class="text-center">
                                            No entries available.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
						</div>
					</div>
					<!-- table responsive end -->
				</div>
				<!-- widget content end -->
			</div>
			<!-- widget slim control end -->

			<!-- widget footer start -->
			<div class="widget-toolbox padding-8 clearfix">
				<div class="row" data-bind="with: GridProperties">
					<!-- pagination size start -->
					<div class="col-sm-6">
						<div class="dataTables_paginate paging_bootstrap pull-left">
							Showing <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
							rows of <span data-bind="text: Total"></span>
							entries
						</div>
					</div>
					<!-- pagination size end -->

					<!-- pagination page jump start -->
					<div class="col-sm-3">
						<div class="dataTables_paginate paging_bootstrap">
							Page <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width:50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page"/>
							of <span data-bind="text: TotalPages"></span>
						</div>
					</div>
					<!-- pagination page jump end -->

					<!-- pagination navigation start -->
					<div class="col-sm-3">
						<div class="dataTables_paginate paging_bootstrap">
							<ul class="pagination">
								<li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
										<i class="icon-double-angle-left"></i>
									</a>
								</li>
								<li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
										<i class="icon-angle-left"></i>
									</a>
								</li>
								<li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
										<i class="icon-angle-right"></i>
									</a>
								</li>
								<li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
										<i class="icon-double-angle-right"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<!-- pagination navigation end -->

				</div>
			</div>
			<!-- widget footer end -->

		</div>
		<!-- widget main end -->
	</div>
	<!-- widget body end -->
</div>
<!-- widget box end -->

<div id="modal-form" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content" >
			<!-- modal header start -->
			<div class="modal-header">
                <h4 class="blue bigger">
                    <span data-bind="if: $root.IsNewData()">Product Type Threshold Mapping Product</span>
                    <span data-bind="text: $root.HeaderTitle(), visible: $root.IsNewData()"></span>
                </h4>
            </div>
			<!-- modal header end -->

			<!-- modal body start -->
			<div class="modal-body overflow-visible">
                <!-- modal body form start -->
                <div id="customer-form" class="form-horizontal" role="form">

                    <div class="space-4"></div>
                     <div class="form-group">
                     <label class="col-sm-3 control-label no-padding-right" for="ProductTypeCode"  data-bind="disable: $root.Readonly, visible: $root.IsNewData() == true">
                         Product Type Code
                     </label>
                        <div class="col-sm-9">
                           <div class="clearfix">
                                 <input type="text" id="ProductTypeCode" name="ProductTypeCode" data-bind="value: $root.ProductTypeCode, visible: $root.IsNewData, enable: false" class="col-xs-10 col-sm-7" data-rule-required="true" data-rule-maxlength="50"/>
                           </div>
                        </div>
                     </div>

                    <div class="space-4"></div>
                     <div class="form-group">
                     <label class="col-sm-3 control-label no-padding-right" for="GroupName"  data-bind="disable: $root.Readonly, visible: $root.IsNewData() == true">
                         Group Name
                     </label>
                        <div class="col-sm-9">
                           <div class="clearfix">
                                 <input type="text" id="GroupName" name="GroupName" data-bind="value: $root.GroupName, visible: $root.IsNewData, enable: false" class="col-xs-10 col-sm-7" data-rule-required="true" data-rule-maxlength="50"/>
                           </div>
                        </div>
                     </div>

                    <div class="space-4"></div>
                     <div class="form-group">
                     <label class="col-sm-3 control-label no-padding-right" for="TransactionType"  data-bind="disable: $root.Readonly, visible: $root.IsNewData() == true">
                         Transaction Type
                     </label>
                        <div class="col-sm-9">
                           <div class="clearfix">
                                 <input type="text" id="TransactionType" name="TransactionType" data-bind="value: $root.TransactionType, visible: $root.IsNewData, enable: false" class="col-xs-10 col-sm-7" data-rule-required="true" data-rule-maxlength="50"/>
                           </div>
                        </div>
                     </div>

                    <div class="space-4"></div>
                     <div class="form-group">
                     <label class="col-sm-3 control-label no-padding-right" for="ResidentName"  data-bind="disable: $root.Readonly, visible: $root.IsNewData() == true">
                         Resident Name
                     </label>
                        <div class="col-sm-9">
                           <div class="clearfix">
                                 <input type="text" id="ResidentName" name="ResidentName" data-bind="value: $root.ResidentName, visible: $root.IsNewData, enable: false" class="col-xs-10 col-sm-7" data-rule-required="true" data-rule-maxlength="50"/>
                           </div>
                        </div>
                     </div>

                    <div class="space-4"></div>
                     <div class="form-group">
                     <label class="col-sm-3 control-label no-padding-right" for="ThresholdName"  data-bind="disable: $root.Readonly, visible: $root.IsNewData() == true">
                         Threshold Name
                     </label>
                        <div class="col-sm-9">
                           <div class="clearfix">
                                 <input type="text" id="ThresholdName" name="ThresholdName" data-bind="value: $root.ThresholdName, visible: $root.IsNewData, enable: false" class="col-xs-10 col-sm-7" data-rule-required="true" data-rule-maxlength="50"/>
                           </div>
                        </div>
                     </div>

                    <div class="space-4"></div>
                     <div class="form-group">
                     <label class="col-sm-3 control-label no-padding-right" for="ThresholdValue"  data-bind="disable: $root.Readonly, visible: $root.IsNewData() == true">
                         Threshold Value
                     </label>
                        <div class="col-sm-9">
                           <div class="clearfix">
                                 <input type="text" id="ThresholdValue" name="ThresholdValue" data-bind="value: $root.ThresholdValue, visible: $root.IsNewData, enable: false" class="col-xs-10 col-sm-7" data-rule-required="true" data-rule-maxlength="50"/>
                           </div>
                        </div>
                     </div>

                    <div class="space-4"></div>
                     <div class="form-group">
                     <label class="col-sm-3 control-label no-padding-right" for="RoundingValue"  data-bind="disable: $root.Readonly, visible: $root.IsNewData() == true">
                         Rounding Value
                     </label>
                        <div class="col-sm-9">
                           <div class="clearfix">
                                 <input type="text" id="RoundingValue" name="RoundingValue" data-bind="value: $root.RoundingValue, visible: $root.IsNewData, enable: false" class="col-xs-10 col-sm-7" data-rule-required="true" data-rule-maxlength="50"/>
                           </div>
                        </div>
                     </div>

                   <div class="space-4"></div>
                </div>
                <!-- modal body form end -->
			</div>
			<!-- modal body end -->

			<!-- modal footer start -->
			<div class="modal-footer">
                <button class="btn btn-sm" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Close
                </button>
			</div>
			<!-- modal footer end -->
		</div>
	</div>
</div>

<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>