//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class InterestMaintenanceApproval
    {
        public long InterestMaintenanceAppID { get; set; }
        public long TransactionInterestMaintenanceID { get; set; }
        public string ApprovalName { get; set; }
        public string FileName { get; set; }
        public string DocumentPath { get; set; }
        public Nullable<bool> IsApproved { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    
        public virtual TransactionInterestMaintenance TransactionInterestMaintenance { get; set; }
    }
}
