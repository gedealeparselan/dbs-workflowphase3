$.cookie.json = true;

var connection;
var spUser;

var Helper = {
    Ajax: {
        Get: function(options, success, error, always){
            AjaxRequest("GET", options, success, error, always);
        },
        Post: function(options, success, error, always){
            AjaxRequest("POST", options, success, error, always);
        },
        Put: function(options, success, error, always){
            AjaxRequest("PUT", options, success, error, always);
        },
        Delete: function(options, success, error, always){
            AjaxRequest("DELETE", options, success, error, always);
        },
        AutoComplete: function(options, response, success, error){
            AjaxRequestAutoComplete(options, response, success, error);
        }
    },
    Token: {
        Request: function(success, error){
            CreateToken(success, error);
        }
    },
    Signal:{
        Connect: function(success, error, received){
            SignalRConnection(success, error, received);
        }
    },
    Sharepoint: {
        Nintex : {
            Post: function(options, success, error, always){
                AjaxRequestSharepoint("POST", options, success, error, always);
            }
        },
        Validate : {
            User: function(options, success, error, always){
                AjaxRequestSharepoint("POST", options, success, error, always);
            }
        },
        List: {
            Add: function(options, success, error, always){
                AjaxRequestSharepoint("POST", options, success, error, always);
            }
        }
    },
    LocalDate: function (date, isDateOnly, isDateLong) {
        return LocalDate(date, isDateOnly, isDateLong);
    },
    FormatNumber: function (num){
        return FormatNumber(num);
    },
    IsUserRoleExistInConfigRoles: function (userRoles, configRoles) {
        return IsUserRoleExistInConfigRoles(userRoles, configRoles);
    }
};


function AjaxRequest(method, options, success, error, always){
    var isValid = true;
    var Url = options.url;

    // validate & decode params
    if(options.params != null || options.params != undefined){
        Url += DecodeParams(options.params);
    }
    
    // declare ajax options
    var ajaxOptions = {
        type: method,
        url: Url,
        contentType: "application/json; charset=utf-8", //; odata=verbose;
        dataType: "json",
        //data: ko.toJSON(options.data),
        headers: {
            "Authorization" : "Bearer " + options.token
        },
        success: function (data, textStatus, jqXHR) {
            success(data, textStatus, jqXHR);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    };

    // set body request
    if(method != "GET"){
        // validate request body on POST
        if(options.data == null || options.data == undefined){
            alert(method + " method does not contains request body");

            isValid = false;
        }else{
            ajaxOptions.data = options.data;
        }
    }

    // execute ajax request
    if(isValid){
        $.ajax(ajaxOptions).always(function(){
            if(always != null || always != undefined)
                always();
        });
    }else{
        if(always != null || always != undefined)
            always();
    }
}

function AjaxRequestSharepoint(method, options, success, error, always){
    var isValid = true;
    var Url = options.url;

    // validate & decode params
    if(options.params != null || options.params != undefined){
        Url += DecodeParams(options.params);
    }

    // declare ajax options
    var ajaxOptions = {
        type: method,
        url: Url,
        contentType: "application/json; odata=verbose",
        dataType: "json",
        headers: {
            "accept": "application/json; odata=verbose",
            "X-RequestDigest": options.digest
        },
        success: function (data, textStatus, jqXHR) {
            success(data, textStatus, jqXHR);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    };

    // set body request
    if(method != "GET"){
        // validate request body on POST
        if(options.data == null || options.data == undefined){
            alert(method + " method does not contains request body");

            isValid = false;
        }else{
            ajaxOptions.data = options.data;
        }
    }

    // execute ajax request
    if(isValid){
        $.ajax(ajaxOptions).always(function(){
            if(always != null || always != undefined)
                always();
        });
    }else{
        if(always != null || always != undefined)
            always();
    }
}

function AjaxRequestAutoComplete(options, response, success, error){
    // declare ajax options
    var ajaxOptions = {
        type: "GET",
        url: options.url,
        contentType: "application/json; charset=utf-8; odata=verbose;",
        dataType: "json",
        data: options.data,
        headers: {
            "Authorization" : "Bearer " + options.token
        },
        success: function (data, textStatus, jqXHR) {
            success(response, data, textStatus, jqXHR);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    };

    // execute ajax request
    $.ajax(ajaxOptions);
}

function DecodeParams(params){
    if(typeof(params) != 'object'){
        alert("The parameter value is not an object!")
        return false;
    }

    var output = "";

    for(var key in params){
        if(output != ""){
            output += "&";
        }else{
            output += "?";
        }

        output += key + "=" + params[key];
    }

    return output;
}

function CreateToken(success, error){
    // checking stored token on browser cookies
    if($.cookie(api.cookie.name) == undefined) {
        // create rest api request to sharepoint
        $.ajax({
            type: "GET",
            url: "/_api/web/CurrentUser?$select=id,LoginName",
            headers: {
                "accept": "application/json;odata=verbose"
            },
            success: function (data, textStatus, jqXHR) {
                OnSuccessCurrentUser(data, success, error);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                error(jqXHR, textStatus, errorThrown);
            }
        });
    }
}

function OnSuccessCurrentUser(data, success, error){
    $.ajax({
        type: "GET",
        url: "/_api/web/GetUserById("+ data.d.Id +")?$select=Id,LoginName,Title,Email",
        headers: {
            "accept": "application/json;odata=verbose"
        },
        success: function (data, textStatus, jqXHR) {
            OnSuccessUserByID(data, success, error);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    });
}

function OnSuccessUserByID(datas, success, error){
    // fill data to spUser
    spUser = {
        ID : datas.d.Id,
        LoginName : datas.d.LoginName,
        DisplayName : datas.d.Title,
        Email : datas.d.Email,
        Roles : {}
    };

    Const_RoleName = {};

    // get sharepoint user groups
    $.ajax({
        type: "GET",
        url: "/_api/web/GetUserById("+ datas.d.Id +")/Groups?$select=Id,Title&$filter=startswith(Title, 'DBS')",
        headers: {
            "accept": "application/json; odata=verbose"
        },
        success: function (data, textStatus, jqXHR) {
            var roles = []
            for(i=0; i < data.d.results.length; i++){
                roles.push({
                    ID: data.d.results[i].Id,
                    //Name: data.d.results[i].Title //aridya 20170123 Name sudah ga dibawa
                });
                Const_RoleName[data.d.results[i].Id] = data.d.results[i].Title;
            }

            // adding roles from sp groups
            spUser['Roles'] = roles;          

            // store spUser to cookie
            $.cookie(api.cookie.spUser, spUser);

            // call request api token function
            RequestAPIToken(success, error);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    });
}

////aridya 20161219 change createToken method
//function CreateToken(success, error) {
//     //checking stored token on browser cookies
//    if($.cookie(api.cookie.name) == undefined) {
//        // wait until sp.js is loaded then call function init
//        ExecuteOrDelayUntilScriptLoaded(init, 'sp.js');
//        var currentUser; //current user data
//        var groupUser; //current user group/role data

//        //function to get current user data
//        function init() {
//            //get sp client context
//            this.clientContext = new SP.ClientContext.get_current();
//            //get sp web from the context
//            this.oWeb = clientContext.get_web();
//            //get current logged in user
//            currentUser = this.oWeb.get_currentUser();
//            //get logged in groups
//            groupUser = currentUser.get_groups();
//            //load the user and groups to context
//            this.clientContext.load(currentUser);
//            this.clientContext.load(groupUser);
//            //get data
//            this.clientContext.executeQueryAsync(onQuerySucceeded, onQueryFailed);
//        }

//        //when succeed
//        function onQuerySucceeded() {
//            //var to store sharepoint user
//            spUser = {
//                ID: currentUser.get_id(),
//                LoginName: currentUser.get_loginName(),
//                DisplayName: currentUser.get_title(),
//                Email: currentUser.get_email(),
//                Roles: {}
//            };
//            //var to store roles
//            var roles = [];
//            //enumerator of logged in id user groups
//            var groupEnumerator = groupUser.getEnumerator();
//            //while still has next item
//            while (groupEnumerator.moveNext()) {
//                //get current group
//                var group = groupEnumerator.get_current();
//                //get the group id
//                var grpId = group.get_id();
//                //get the group name
//                var grpTitle = group.get_title();
//                //this condition equals to &$filter=startswith(Title, 'DBS') in the old logic
//                if (grpTitle.lastIndexOf('DBS', 0) === 0) {
//                    //push the data if condition is met
//                    roles.push({
//                        ID: grpId,
//                        Name: grpTitle
//                    })
//                }
//            }

//            //put the roles to spUser.Roles
//            spUser['Roles'] = roles;

//            // store spUser to cookie
//            $.cookie(api.cookie.spUser, spUser);

//            // call request api token function
//            RequestAPIToken(success, error);
//        }

//        //when error
//        function onQueryFailed(sender, args) {
//            ShowNotification(args.get_message(), args.get_stackTrace(), 'gritter-error', true);
//        }
//    }
//}
////end aridya

// request new jwt token to DBS API
function RequestAPIToken(success, error) {
    for (var ii = 0; ii < spUser.Roles.length; ii++) {
        spUser.Roles[ii].Name = Const_RoleName[spUser.Roles[ii].ID];
    }

    $.ajax({
        type: "POST",
        url: api.server + api.url.auth,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(spUser),
        success: function (data, textStatus, jqXHR) {
            success(data, textStatus, jqXHR);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    });    
}

// SignalR Helper
function SignalRConnection(success, error, received) {
    // build connection to server
    connection = $.connection(config.signal.server + "echo");

    // start connection
    connection.start().done(success).fail(error);

    // received callback
    connection.received(function(data) {
        received (data);
    });
}

// Gritter Notification
// class name : gritter-success, gritter-warning, gritter-error, gritter-info
function ShowNotification(title, text, className, isSticky){
    $.gritter.add({
        title: title,
        text: text,
        class_name: className,
        sticky: false,
		time:1000
    });
}

//Get local date from given UTC date
function LocalDate (date, isDateOnly, isDateLong) {
    if (date != null) {
        var utcDate = moment.utc(date);
        if (utcDate.isValid()) {
            if (utcDate == '1970/01/01 00:00:00' || utcDate == '1970-01-01T00:00:00' || utcDate == null) {
                return "";
            }
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(utcDate).local().format(config.format.dateLong);
                else
                    return moment(utcDate).local().format(config.format.date);
            }
            else {
                return moment(utcDate).local().format(config.format.dateTime);
            }
        }
        else {
            return ""
        }
    }
    else {
        return ""
    }
};

//Cek apakah roles yang dimiliki user (Const_RoleName) terdapat pada roles yang ada di config (untuk pengecekan permission)
function IsUserRoleExistInConfigRoles(userRoles, configRoles) {
    if (userRoles != null && configRoles != null) {
        for (var i = 0 ; i < Object.keys(userRoles).length; i++) {
            if (userRoles[Object.keys(userRoles)[i]] != null) {
                for (var j = 0; j < configRoles.length; j++) {
                    if (userRoles[Object.keys(userRoles)[i]].toLowerCase() == configRoles[j].toLowerCase()) {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

function FormatNumber (num) {
    if (num != null) {
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    }
    else {
        return 0;
    }
}

