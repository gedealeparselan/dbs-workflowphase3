﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    //[Authorize]
    public class TransactionController : ApiController
    {
        private string message = string.Empty;

        private readonly ITransactionRepository repository = null;

        public TransactionController()
        {
            this.repository = new TransactionRepository();
        }

        public TransactionController(ITransactionRepository repository)
        {
            this.repository = repository;
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }

        /// <summary>
        /// Get Transaction PPU Maker by ID.
        /// </summary>
        /// <param name="id">Transaction PPU Maker ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionModel))]
        [Route("api/Transaction/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(Int64 id)
        {
            TransactionModel output = new TransactionModel();

            if (!repository.GetTransactionByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<TransactionModel>(HttpStatusCode.OK, output);
            }
        }

        // Get Force Complete 
        // ambil nilai untuk hak akses button Force Complete
        // add by adi
        [ResponseType(typeof(long))]
        [Route("api/Transaction/ForceComplete")]
        [HttpGet]
        public HttpResponseMessage GetForce()
        {

            long output = 0;

            if (!repository.GetForceComplete(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<long>(HttpStatusCode.OK, output);
            }

        }


        /// <summary>
        /// Get Transaction PPU Maker by ID.
        /// </summary>
        /// <param name="id">Transaction PPU Maker ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionDraftModel))]
        [Route("api/Transaction/Draft/{id}")]
        [HttpGet]
        public HttpResponseMessage GetDraft(Int64 id)
        {
            TransactionDraftModel output = new TransactionDraftModel();

            if (!repository.GetTransactionDraftByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<TransactionDraftModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Remove exisiting Draft.
        /// </summary>
        /// <param name="id">ID of Draft to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/Transaction/Draft/Delete/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage DeleteDraft(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteTransactionDraftByID(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Draft." });
                }
            }
        }

        /// <summary>
        /// Get Application ID
        /// </summary>
        /// <param name="productID">ID of product</param>
        /// <returns></returns>
        [ResponseType(typeof(string))]
        [Route("api/Transaction/GetApplicationID")]
        [HttpGet]
        public HttpResponseMessage Get(int productID)
        {
            string applicationID = repository.GetApplicationID(productID, ref message);
            if (string.IsNullOrEmpty(applicationID))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (productID < 0)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse(HttpStatusCode.OK, new { ApplicationID = applicationID });
            }
        }

        /// <summary>
        /// Get Transaction PPU Maker for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<TransactionFilter> filters)
        {
            TransactionGrid output = new TransactionGrid();

            if (!repository.GetTransaction(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<TransactionGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Transaction PPU Maker for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionGrid))]
        [Route("api/Transaction/Draft")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetDraft(int? page, int? size, string sort_column, string sort_order, IList<TransactionFilter> filters)
        {
            TransactionGrid output = new TransactionGrid();

            if (!repository.GetTransactionDraft(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<TransactionGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new Transaction PPU Maker.
        /// </summary>
        /// <param name="transaction">Cumrrency data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS PPU Maker")]
        public HttpResponseMessage Post(TransactionDetailModel transaction)
        {
            #region Old API
            ///Andi,24 October 2015
            /*
            long transactionID = 0;
            string ApplicationID = string.Empty;
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransaction(transaction, ref transactionID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new
                    {
                        ID = transactionID,
                        Message = transaction.IsDraft ? "Transaction has been saved as draft." : "New Transaction has been submitted."
                    });
                }
            }
             * */

            #endregion
            #region Enhancement for phase 2
            long transactionID = 0;
            string ApplicationID = string.Empty;
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransaction(transaction, ref transactionID, ref ApplicationID, ref message))
                {
                    if (message.ToLower().EndsWith("already exist on workflow"))
                    {
                        return Request.CreateResponse(HttpStatusCode.PreconditionFailed, new { Message = message });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new
                    {
                        ID = transactionID,
                        AppID = ApplicationID,
                        Message = transaction.IsDraft ? "Transaction has been saved as draft." : "New Transaction has been submitted."
                    });
                }
            }
            #endregion
        }

        #region exchange IPE

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/Transaction/IPE")]
        //[Authorize(Roles = "DBS PPU Maker")]
        public HttpResponseMessage PostIPE(TransactionDetailModel transaction)
        {
            long transactionID = 0;
            string ApplicationID = string.Empty;
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionIPE(transaction, ref transactionID, ref ApplicationID, ref message))
                {
                    if (message.ToLower().EndsWith("already exist on workflow"))
                    {
                        return Request.CreateResponse(HttpStatusCode.PreconditionFailed, new { Message = message });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new
                    {
                        ID = transactionID,
                        AppID = ApplicationID,
                        Message = transaction.IsDraft ? "Transaction has been saved as draft." : "New Transaction has been submitted."
                    });
                }
            }
        }

        [ResponseType(typeof(TransactionDraftModel))]
        [Route("api/Transaction/DraftIPE/{id}")]
        [HttpGet]
        public HttpResponseMessage GetDraftIPE(Int64 id)
        {
            TransactionDraftModel output = new TransactionDraftModel();

            if (!repository.GetTransactionDraftByIDIPE(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<TransactionDraftModel>(HttpStatusCode.OK, output);
            }
        }

        [HttpDelete]
        [Route("api/Transaction/DraftIPE/Delete/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage DeleteDraftIPE(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteTransactionDraftByIDIPE(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Draft." });
                }
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/Transaction/DraftIPE/{id}")]
        //[Authorize(Roles = "Admin, DBS PPU Maker")]
        public HttpResponseMessage PutDraftIPE(long id, TransactionDetailModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTransactionDraftIPE(id, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction data has been updated." });
                }
            }
        }

        #endregion

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/Transaction/Loan")]
        public HttpResponseMessage PostLoan(TransactionDetailModel transaction)
        {
            long transactionID = 0;

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionLoan(transaction, ref transactionID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new
                    {
                        ID = transactionID,
                        Message = transaction.IsDraft ? "Transaction has been saved as draft." : "New Transaction has been submitted."
                    });
                }
            }
        }

        /// <summary>
        /// Add new Transaction Documents.
        /// </summary>
        /// <param name="id">ID of transaction to be modify.</param>
        /// <param name="documents">TransactionDocumentModel.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/Transaction/Document/{id}")]
        //[Authorize(Roles = "DBS PPU Maker")]
        public HttpResponseMessage UpdateDocuments(long id, IList<TransactionDocument> documents) //documents
        {
            if (!repository.UpdateTransactionDocument(id, documents, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Attach Documents has been uploaded." });
            }
        }

        /// <summary>
        /// Add new Transaction Documents.
        /// </summary>
        /// <param name="id">ID of transaction to be modify.</param>
        /// <param name="documents">TransactionDocumentItem.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/Transaction/Document/Insert/{id}")]
        //[Authorize(Roles = "DBS PPU Maker")]
        public HttpResponseMessage InsertDocuments(long id, TransactionDocumentItem documents) //documents
        {
            if (!repository.InsertTransactionDocument(id, documents, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Pending Documents has been submit." });
            }
        }

        /// <summary>
        /// Get Transaction Documents by Transaction ID.
        /// </summary>
        /// <param name="id">Transaction ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionDocument))]
        [Route("api/Transaction/Document/Attach/{id}")]
        [HttpGet]
        public HttpResponseMessage GetFileDocuments(long id)
        {
            IList<TransactionDocument> output = new List<TransactionDocument>();

            if (!repository.GetTransactionDocument(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<TransactionDocument>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Remove exisiting File.
        /// </summary>
        /// <param name="id">ID of File to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/Transaction/Document/Delete/{id}")]
        [ResponseType(typeof(string))]
        // [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteTransactionDocument(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "File data has been deleted." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting File.
        /// </summary>
        /// <param name="DocumentPath">DocumentPath of File to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/Transaction/DocumentPath/Delete/{DocumentPath}")]
        [ResponseType(typeof(string))]
        // [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(string DocumentPath)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteTransactionDocumentbyPath(DocumentPath, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "File data has been deleted." });
                }
            }
        }

        /// <summary>
        /// Modify draft transaction
        /// </summary>
        /// <param name="id">ID of transaction</param>
        /// <param name="transaction">Transaction data</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/Transaction/{id}")]
        //[Authorize(Roles = "Admin, DBS PPU Maker")]
        public HttpResponseMessage Put(long id, TransactionDetailModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTransaction(id, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction data has been updated." });
                }
            }
        }

        /// <summary>
        /// Modify draft transaction
        /// </summary>
        /// <param name="id">ID of transaction</param>
        /// <param name="transaction">Transaction data</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/Transaction/Draft/{id}")]
        //[Authorize(Roles = "Admin, DBS PPU Maker")]
        public HttpResponseMessage PutDraft(long id, TransactionDetailModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTransactionDraft(id, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction data has been updated." });
                }
            }
        }

        /// <summary>
        /// Get Transaction Document by Transaction ID.
        /// </summary>
        /// <param name="id">Transaction ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionDocument))]
        [Route("api/Transaction/transactionID={transactionID}")]
        [HttpGet]
        public HttpResponseMessage GetDocument(Int64 transactionID)
        {
            IList<TransactionDocument> output = new List<TransactionDocument>();

            if (!repository.GetTransactionDocByTransactionID(transactionID, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<TransactionDocument>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Transaction Document for data grid.
        /// </summary>
        /// <param name="id">Transaction Document ID.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionDocumentGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetDocument(Int64? transactionID, int? page, int? size, string sort_column, string sort_order, IList<TransactionDocumentFilter> filters)
        {
            TransactionDocumentGrid output = new TransactionDocumentGrid();

            if (!repository.GetTransactionDocument(transactionID.HasValue ? transactionID.Value : 0, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<TransactionDocumentGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Completed Transaction for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionGrid))]
        [Route("api/Transaction/{mode}/GenerateXMLTxt/{productID}")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetCompletedTransaction(int mode, int productID, int? page, int? size, string sort_column, string sort_order, IList<TransactionFilter> filters)
        {
            TransactionGrid output = new TransactionGrid();

            //if (!repository.GetCompletedTransaction(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            if (!repository.GetSPCompletedTransaction(mode, productID, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<TransactionGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Total Amount Transaction Underlying by Cif
        /// </summary>
        /// <param name="Cif">CIF.</param>
        /// <returns></returns>
        [ResponseType(typeof(decimal))]
        [Route("api/Transaction/GetAmountTransaction={Cif}")]
        [HttpGet]
        public HttpResponseMessage GetAmountTransaction(string Cif)
        {
            decimal output = new decimal();

            if (!repository.GetAmountTransaction(Cif, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == 0)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<decimal>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get pending documents for data grid.
        /// </summary>
        /// <param name="sort_order">//.</param>
        /// <param name="filters">//.</param>
        /// <param name="page">//.</param>
        /// <param name="size">//.</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionGrid))]
        [Route("api/Transaction/Pending")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetPendingDocuments(int? page, int? size, string sort_column, string sort_order, IList<TransactionFilter> filters)
        {
            SPUserTaskGrid output = new SPUserTaskGrid();

            if (!repository.GetPendingDocuments(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "ApplicationID" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<SPUserTaskGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get transaction details by Application ID
        /// </summary>
        /// <param name="ApplicationID">Application ID</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionDisbursementModel))]
        [HttpGet]
        [Route("api/Transaction/{ApplicationID}/Transaction")]
        public HttpResponseMessage GetTransactionLoanDisbursement(Int64 TransactionID)
        {
            TransactionDisbursementModel output = new TransactionDisbursementModel();
            if (!repository.GetLoanDisburseDetails(TransactionID, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<TransactionDisbursementModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get transaction details by Application ID
        /// </summary>
        /// <param name="ApplicationID">Application ID</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionDetailModel))]
        [HttpGet]
        [Route("api/Transaction/{ApplicationID}/PendingTransaction")]
        public HttpResponseMessage GetTransactionDetails(string ApplicationID)
        {
            TransactionDetailModel output = new TransactionDetailModel();

            //if (!repository.GetTransactionDetailsByApplicationID(ApplicationID, ref output, ref message))
            if (!repository.GetPendingDocumentDetailsByApplicationID(ApplicationID, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// check TZ status by Application ID
        /// </summary>
        /// <param name="ApplicationID">Application ID</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionDetailModel))]
        [HttpGet]
        [Route("api/Transaction/TZ/{ApplicationID}")]
        public HttpResponseMessage GetTransactionTZDetails(string ApplicationID)
        {
            bool output = false;
            if (!repository.GetTransactionTZStatus(ApplicationID, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(TZNumberPopulateModel))]
        [Route("api/GetTZNumber")]
        [HttpGet]
        public HttpResponseMessage BindTZNumber(string tznumber, string cif)
        {
            IList<TZNumberPopulateModel> output = new List<TZNumberPopulateModel>();

            if (!repository.PopulateTZNumberID(tznumber, cif, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<TZNumberPopulateModel>>(HttpStatusCode.OK, output);
            }

        }
    }
}
