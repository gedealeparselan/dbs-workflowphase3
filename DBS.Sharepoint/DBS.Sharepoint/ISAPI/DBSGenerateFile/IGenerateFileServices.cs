﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace DBS.Sharepoint
{
    #region Interfaces

    [ServiceContract]
    public interface IGenerateFileServices
    {
        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Test HelloWorld();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        GeneratedFileInfo GenerateFile(DataInformationCollection data);


        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        GeneratedFileInfo GenerateFileXML(DataInformationCollection data);

    }

    #endregion

    #region Properties
    [DataContract]
    public class DataInformation
    {
        //[DataMember]
        //public long GeneratedTransactionID { get; set; }

        [DataMember]
        public long TransactionID { get; set; }

        [DataMember]
        public string ApplicationID { get; set; }

        //[DataMember]
        //public string FileName { get; set; }

        //[DataMember]
        //public DateTime CreateDate { get; set; }

        //[DataMember]
        //public string CreateBy { get; set; }
    }

    [DataContract]
    public class DataInformationCollection
    {
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Format { get; set; }
        [DataMember]
        public IList<DataInformation> DataInformation { get; set; }
    }

    [DataContract]
    public class GeneratedFileInfo
    {
        [DataMember]
        public long InsertedID { get; set; }

        [DataMember]
        public string InsertedURL { get; set; }

        [DataMember]
        public bool IsSuccess { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
    #endregion
}
