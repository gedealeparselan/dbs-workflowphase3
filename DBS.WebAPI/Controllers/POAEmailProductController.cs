﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    public class POAEmailProductController : ApiController
    {
        private string message = string.Empty;

        private readonly IPOAEmailModel repository = null;

        public POAEmailProductController()
        {
            this.repository = new POAEmailProductRepository();
        }

        public POAEmailProductController(IPOAEmailModel repository)
        {
            this.repository = repository;
        }

        [ResponseType(typeof(IList<POAEmailModel>))]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            IList<POAEmailModel> output = new List<POAEmailModel>();

            if (!repository.GetPOAEmailProduct(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<POAEmailModel>>(HttpStatusCode.OK, output);
            }
        }

 
        //[ResponseType(typeof(IList<ProvinsiModel>))]
        //[HttpGet]
        //[Route("api/POAEmailProduct/Search")]
        //public HttpResponseMessage Get(string query, int? limit)
        //{
        //    IList<ProvinsiModel> output = new List<ProvinsiModel>();

        //    if (!repository.GetPOAEmailProduct(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
        //    {
        //        return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
        //    }
        //    else
        //    {
        //        return Request.CreateResponse<IList<ProvinsiModel>>(HttpStatusCode.OK, output);
        //    }
        //}

        [ResponseType(typeof(IList<POAEmailModel>))]
        [HttpGet]
        [Route("api/POAEmailProduct/SearchDetail")]
        public HttpResponseMessage SerachDetail(string query, int? limit)
        {
            IList<POAEmailModel> output = new List<POAEmailModel>();

            if (!repository.GetPOAEmailProduct(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<POAEmailModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(POAEmailModelGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/POAEmailProduct")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<POAEmailModelFilter> filters)
        {
            POAEmailModelGrid output = new POAEmailModelGrid();

            if (!repository.GetPOAEmailProduct(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Group Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<POAEmailModelGrid>(HttpStatusCode.OK, output);
            }
        }


        [ResponseType(typeof(POAEmailModel))]
        [Route("api/POAEmailProduct/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            POAEmailModel output = new POAEmailModel();

            if (!repository.GetPOAEmailProductByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<POAEmailModel>(HttpStatusCode.OK, output);
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/POAEmailProduct")]
        public HttpResponseMessage Post(POAEmailModel POAEmailProduct)
        {

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddPOAEmailProduct(POAEmailProduct, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New POAEmailProduct has been created." });
                }
            }
        }


        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/POAEmailProduct/{ID}")]
        public HttpResponseMessage Update(int id, ref POAEmailModel POAEmailProduct)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdatePOAEmailProduct(id, ref POAEmailProduct, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "POAEmailProduct data has been updated." });
                }
            }
        }


        //Delete Data
        [HttpDelete]
        [Route("api/POAEmailProduct/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeletePOAEmailProduct(id, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "POAEmailProduct data has been deleted.");
                }
            }
        }

    }
}
