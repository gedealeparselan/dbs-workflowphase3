﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    public class FDCompleteController :ApiController
    {
         private string message = string.Empty;
        /// <summary>
        /// Define repo here
        /// </summary>
        private readonly IFDCompleteRepository repocompletefd = null;
        public FDCompleteController()
        {
            this.repocompletefd = new WorkflowFDCompleteRepository();
        }
        /// <summary>
        /// Add Controller below...
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (repocompletefd != null)
                repocompletefd.Dispose();


            base.Dispose(disposing);
        }

        #region Andri
        [ResponseType(typeof(TransactionFDCompleteDetailModel))]
        [HttpGet]
        [Route("api/WorkflowFD/{workflowInstanceID}/FD/Complete/{approverID}")]
        public HttpResponseMessage GetFDCheckerDetails(Guid workflowInstanceID, long approverID)
        {
            FDDetailCompleteModel fdComplete = new FDDetailCompleteModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionFDCompleteDetailModel output = new TransactionFDCompleteDetailModel();

            try
            {
                if (!repocompletefd.GetFDCompleteDetails(workflowInstanceID, ref fdComplete, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repocompletefd.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }else
                    {
                        if(!repocompletefd.GetTransactionCallbackCompleteDetails(workflowInstanceID,approverID,ref output,ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                   
                    else
                    
                        {
                            output.Transaction = fdComplete;
                            output.Timelines = timelines;
                            output.Callbacks = output.Callbacks;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {
                fdComplete = null;
                timelines = null;
            }
        }
        #endregion
    }
}