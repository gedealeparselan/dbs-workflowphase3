﻿var accessToken;

/* dodit@2014.11.08:add format function */
var formatNumber = function (num) {
    if (num != null) {
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    }
}
var formatNumber_r = function (num) {
    if (num != null) {
        num = num.toString().replace(/,/g, '');
        num = parseFloat(num).toFixed(2);
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    } else { return 0; }
}


var $box;
var $remove = false;
var cifData = '0';
var customerNameData = '';
var DocumentModels = ko.observable({ FileName: '', DocumentPath: '' });
var DealModel = { cif: cifData, token: accessToken }
var idrrate = 0;
var countResult = {
    CustomerName: 0,
    DealNumber: 0,
    NettingDealNumber: 0
};
var UnvalidYear = '1900';
var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};

var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};

var AmountModel = {
    TotalPPUAmountsUSD: ko.observable(0),
    TotalDealAmountsUSD: ko.observable(0),
    RemainingBalance: ko.observable(0),
    TotalPPUUtilization: ko.observable(0),
    TotalDealUtilization: ko.observable(0),
    TotalAmountsUSD: ko.observable(0),
};



var StatementModel = {
    ID: ko.observable(0),
    Name: ko.observable("")
};

var BranchModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var TypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var RMModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    RankID: ko.observable(),
    Rank: ko.observable(),
    SegmentID: ko.observable(),
    Segment: ko.observable(),
    BranchID: ko.observable(),
    Branch: ko.observable(),
    Email: ko.observable()
};

var ContactModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    PhoneNumber: ko.observable(),
    DateOfBirth: ko.observable(),
    Address: ko.observable(),
    IDNumber: ko.observable()
};

var FunctionModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var CurrencyModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var AccountModel = {
    AccountNumber: ko.observable(),
    Currency: ko.observable(CurrencyModel)
};

var ProductTypeModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    IsFlowValas: ko.observable(),
    Description: ko.observable()
};

var UnderlyingDocumentModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var UnderlyingDocumentModel2 = function (id, name, code) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Name = ko.observable(name);
    self.CodeName = ko.observable(code + ' (' + name + ')');
};

var DocumentTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var DocumentTypeModel2 = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);

};

var DocumentPurposeModel2 = function (id, name, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(description);
};

var RateTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var FXComplianceModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ChargesModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var SelectModel = function (cif, id) {
    var self = this;
    self.ID = ko.observable(0);
    self.CIF = ko.observable(cif);
    self.UnderlyingID = ko.observable(id);
    self.ParentID = ko.observable(0);
};

var SelectBulkModel = function (cif, id) {
    var self = this;
    self.ID = ko.observable(0);
    self.CIF = ko.observable(cif);
    self.UnderlyingID = ko.observable(id);
    self.ParentID = ko.observable(0);
};

var CurrencyModel2 = function (id, code, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Description = ko.observable(description);
};

var RateTypeModel2 = function (id, code, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Description = ko.observable(description);
};

var StatementLetterModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
};

var DocumentPurposeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var NettingPurposeModel = {
    ID: ko.observable(),
    name: ko.observable()
};

var UnderlyingModel = {
    ID: ko.observable(),
    UnderlyingDocument: ko.observable(UnderlyingDocumentModel),
    DocumentType: ko.observable(DocumentTypeModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    AttachmentNo: ko.observable(),
    IsStatementLetter: ko.observable(),
    IsDeclarationOfException: ko.observable(),
    StartDate: ko.observable(),
    EndDate: ko.observable(),
    DateOfUnderlying: ko.observable(),
    ExpiredDate: ko.observable(),
    ReferenceNumber: ko.observable(),
    SupplierName: ko.observable(),
    InvoiceNumber: ko.observable()
};

var DocumentModel = {
    ID: ko.observable(),
    Type: ko.observable(DocumentTypeModel),
    Purpose: ko.observable(DocumentPurposeModel),
    FileName: ko.observable(),
    DocumentPath: ko.observable(),
    LastModifiedDate: ko.observable(new Date),
    LastModifiedBy: ko.observable()
};

var DocumentPurposeModel = function (id, name, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(description);
};

var DocumentTypeModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);

};

var CustomerUnderlyingMappingModel = function (FileID, UnderlyingID, attachNo) {
    var self = this;
    self.ID = ko.observable(0);
    self.UnderlyingID = ko.observable(UnderlyingID);
    self.UnderlyingFileID = ko.observable(FileID);
    self.AttachmentNo = ko.observable(attachNo);
};

var ProductModel = {
    ID: ko.observable(),
    Code: ko.observable('NT'),
    Name: ko.observable('TMO Netting'),
    WorkflowID: ko.observable(),
    Workflow: ko.observable()
};

var CustomerModel = {
    CIF: ko.observable(),
    Name: ko.observable(),
    POAName: ko.observable(),
    Branch: ko.observable(BranchModel),
    Type: ko.observable(TypeModel),
    RM: ko.observable(RMModel),
    Contacts: ko.observableArray([ContactModel]),
    Functions: ko.observableArray([FunctionModel]),
    Accounts: ko.observableArray([AccountModel]),
    Underlyings: ko.observableArray([UnderlyingModel])
};

var SelectUtillizeModel = function (id, enable, uSDAmount, statementLetter) {
    var self = this;
    self.ID = ko.observable(id);
    self.Enable = ko.observable(enable);
    self.USDAmount = ko.observable(uSDAmount);
    self.StatementLetter = ko.observable(statementLetter);
};

var DealNumberModel = function (id, cif, name, tzreference, murexnumber) {
    var self = this;
    self.ID = ko.observable(id);
    self.CIF = ko.observable(cif);
    self.Name = ko.observable(name);
    self.TzReference = ko.observable(tzreference);
    self.MurexNumber = ko.observable(murexnumber);
};

var NettingDealNumberModel = function (id, cif, name, tzreference, swapdealnumber) {
    var self = this;
    self.ID = ko.observable(id);
    self.CIF = ko.observable(cif);
    self.Name = ko.observable(name);
    self.TzReference = ko.observable(tzreference);
    self.SwapDealNumber = ko.observable(swapdealnumber);
};

var today = Date.now();
var TransactionNettingModel = {
    AccountNumber: ko.observable(),
    StatementLetter: ko.observable(StatementModel),
    Product: ko.observable(ProductModel),
    TZReference: ko.observable(),
    IDTZReference: ko.observable(),
    MurexNumber: ko.observable(),
    SwapDealNumber: ko.observable(),
    NettingTZReference: ko.observable(),
    IDNettingTZReference: ko.observable(),
    ExpectedDateStatementLetter: ko.observable(),
    ExpectedDateUnderlying: ko.observable(),//moment(today).format(config.format.date)),
    ActualDateStatementLetter: ko.observable(),
    ActualDateUnderlying: ko.observable(),//moment(today).format(config.format.date)),
    RateType: ko.observable(),
    ProductType: ko.observable(ProductTypeModel),
    Rate: ko.observable(),
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Currency: ko.observable(CurrencyModel),
    AmountUSD: ko.observable(0),
    Account: ko.observable(AccountModel),
    Type: ko.observable(20),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]),
    utilizationAmount: ko.observable(0.00),
    underlyings: ko.observableArray([]),
    IsDraft: ko.observable(false),
    OtherUnderlying: ko.observable(),
    bookunderlyingamount: ko.observable(),
    bookunderlyingcurrency: ko.observable(),
    bookunderlyingcode: ko.observable(),
    bookunderlyingdesc: ko.observable(),
    Remarks: ko.observable(),
    IsTMO: ko.observable(true),
    IsNettingTransaction: ko.observable(true),
    NettingPurpose: ko.observable(NettingPurposeModel),
    Underlyings: ko.observableArray([])
};

var ParameterModel = {
    Currencies: ko.observableArray([CurrencyModel]),
    ProductType: ko.observableArray([ProductTypeModel]),
    FXCompliances: ko.observableArray([FXComplianceModel]),
    DocumentTypes: ko.observableArray([DocumentTypeModel]),
    DocumentPurposes: ko.observableArray([DocumentPurposeModel]),
    RateType: ko.observableArray([RateTypeModel]),
    StatementLetter: ko.observableArray([StatementModel]),
    NettingPurpose: ko.observableArray([NettingPurposeModel])
};

var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};

var SelectedModel = {
    Currency: ko.observable(),
    Account: ko.observable(),
    DocumentType: ko.observable(),
    DocumentPurpose: ko.observable(),
    NewCustomer: ko.observable({
        Currency: ko.observable()
    }),
    ProductType: ko.observable(),
    RateType: ko.observable(),
    StatementLetter: ko.observable(),
    DebitCurrency: ko.observable(),
    NettingPurpose: ko.observable()
};

var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;
    self.IsRole = function (name) {
        var result = false;
        $.each($.cookie(api.cookie.spUser).Roles, function (index, item) {
            if (Const_RoleName[item.ID] == name) result = true; //if (item.Name == name) result = true;
        });
        return result;
    };

    // Sharepoint User
    self.SPUser = ko.observable(SPUser);

    // TresHold Value
    self.FCYIDRTresHold = ko.observable(0);

    self.IsEditTableUnderlying = ko.observable(true);

    // rate param
    self.Rate = ko.observable(0);

    // Is check attachments
    self.IsFXTransactionAttach = ko.observable(false);
    self.IsUploaded = ko.observable(false);
    self.IsUploading = ko.observable(false);
    self.SelectingUnderlying = ko.observable(false);
    self.IsLoadDraft = ko.observable(false);
    self.IsDraftForm = ko.observable();
    // before value CurrencyID
    self.beforeValueCurrID = ko.observable(0);
    //bu group val
    self.isDealBUGroup = ko.observable(false);
    self.IsPermissionBookDeal = ko.observable(false);
    // Options selected value
    self.Selected = ko.observable(SelectedModel);
    self.IsSearchTZ = ko.observable(false);

    self.ID = ko.observable();
    self.NettingUnderlyings = ko.observableArray([]);
    self.NettingAllDocumentsTemp = ko.observableArray([]);
    self.NettingDocuments = ko.observableArray([]);

    //new underlying type
    self.isNewUnderlying = ko.observable(false);
    self.underlyingCode = ko.observable();

    //is fx transaction
    self.t_IsFxTransaction = ko.observable(false);
    self.t_IsFxTransactionToIDR = ko.observable(false);
    self.t_IsFxTransactionToIDRB = ko.observable(false);

    self.CIFJointAccounts = ko.observableArray([{ ID: false, Name: "Single" }, { ID: true, Name: "Join" }]);
    self.DynamicAccounts = ko.observableArray([]);
    self.IsJointAccount = ko.observable(false);
    self.IsHitThreshold = ko.observable(false);
    self.ThresholdValue = ko.observable(0);
    self.IsNoThresholdValue = ko.observable(false)


    //underlying declare
    self.AmountModel = ko.observable(AmountModel);
    self.FormatNumber = function (num) {
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    }

    self.ApplicationIDColl = ko.observableArray([]);
    self.RetIDColl = ko.observableArray([]);
    self.FinishBulk = function () {
        window.location = "/home";
    }
    self.TZReferenceTemp = ko.observable([]);
    self.NettingTZReferenceTemp = ko.observable([]);
    // input form controls
    self.IsEditable = ko.observable(true);
    self.IsEnableSubmit = ko.observable(true);
    self.IsEditTableUnderlying = ko.observable(true);
    // Parameters
    self.Parameter = ko.observable(ParameterModel);
    self.TZInformations = ko.observableArray([]);
    self.TmpTzNumber = ko.observable("");
    self.MurexNumber = ko.observable("");

    self.TZGridProperties = ko.observable();
    self.TZGridProperties(new GridPropertiesModel(GetDataTZ));
    self.CIFData = ko.observable();

    self.CustomerDocuments = ko.observableArray([]);
    self.CustomerAllDocumentsTemp = ko.observableArray([]);
    self.CustomerDocumentUnderlyings = ko.observableArray([]);

    self.TransactionNetting = ko.observable(TransactionNettingModel);

    // grid properties Underlying
    self.UnderlyingGridProperties = ko.observable();
    self.UnderlyingGridProperties(new GridPropertiesModel(GetDataUnderlying));

    // grid properties Proforma
    self.UnderlyingProformaGridProperties = ko.observable();
    self.UnderlyingProformaGridProperties(new GridPropertiesModel(GetDataUnderlyingProforma));

    // Validation User
    self.UserValidation = ko.observable({
        UserID: ko.observable(),
        Password: ko.observable(),
        Error: ko.observable()
    });
    // set Indicator Submited Statement letter
    self.Submitted = ko.observable();

    // value CIF underlying
    self.Treshold = ko.observable(0);
    self.FCYIDRTreshold = ko.observable(0);
    self.TotalRemainigByCIF = ko.observable(0);
    self.TotalTransFX = ko.observable(0);
    self.TotalUtilization = ko.observable(0);

    self.ThresholdType = ko.observable();
    self.Rounding = ko.observable(0);
    self.Unwind = ko.observable(false);
    // New Data flag
    self.IsNewDataUnderlying = ko.observable(false);
    // New Data flag
    self.IsDeal = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyings = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyingProformas = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyingFiles = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerAttachUnderlyings = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerBulkUnderlyings = ko.observableArray([]);

    // grid properties Attach Grid
    self.AttachGridProperties = ko.observable();
    self.AttachGridProperties(new GridPropertiesModel(GetDataAttachFile));

    // grid properties Underlying File
    self.UnderlyingAttachGridProperties = ko.observable();
    self.UnderlyingAttachGridProperties(new GridPropertiesModel(GetDataUnderlyingAttach));

    // grid properties Bulk // 2015.03.09
    self.UnderlyingBulkGridProperties = ko.observable();
    self.UnderlyingBulkGridProperties(new GridPropertiesModel(GetDataBulkUnderlying));
    self.IsCustomerAvailable = ko.observable(false);
    self.IsTZAvailable = ko.observable(false);
    self.IsBtnDealNumber = ko.observable(false);

    // Attach Variable
    self.ID_a = ko.observable(0);
    self.FileName_a = ko.observable('');
    self.DocumentPurpose_a = ko.observable(new DocumentPurposeModel('', '', ''));
    self.ddlDocumentPurpose_a = ko.observableArray([]);
    self.DocumentType_a = ko.observable(new DocumentTypeModel('', ''));
    self.ddlDocumentType_a = ko.observableArray([]);
    self.DocumentRefNumber_a = ko.observable("");
    self.LastModifiedBy_a = ko.observable("");
    self.LastModifiedDate_a = ko.observable("");
    self.CustomerUnderlyingMappings_a = ko.observableArray([new CustomerUnderlyingMappingModel('', '', '')]);

    //--------------------------- set Underlying function & variable start
    self.IsUnderlyingMode = ko.observable(true);
    self.ID_u = ko.observable("");
    self.CIF_u = ko.observable(cifData);
    self.CustomerName_u = ko.observable(customerNameData);
    self.UnderlyingDocument_u = ko.observable(new UnderlyingDocumentModel2('', '', ''));
    self.ddlUnderlyingDocument_u = ko.observableArray([]);
    self.DocumentType_u = ko.observable(new DocumentTypeModel2('', ''));
    self.ddlDocumentType_u = ko.observableArray([]);

    self.AmountUSD_u = ko.observable(0.00);

    //self.Currency_u = ko.observableArray([new CurrencyModel2('', '', '')]);
    self.Currency_u = ko.observable(new CurrencyModel2('', '', ''));

    //self.RateType_u = ko.observableArray([new RateTypeModel2('', '', '')]);
    self.RateType_u = ko.observable(new RateTypeModel2('', '', ''));

    self.Rate_u = ko.observable(0);

    self.ddlCurrency_u = ko.observableArray([]);
    self.ddlRateType_u = ko.observableArray([]);

    //self.StatementLetter_u = ko.observableArray([new StatementLetterModel('', '')]);
    self.StatementLetter_u = ko.observable(new StatementLetterModel('', ''));
    self.ddlStatementLetter = ko.observableArray([]);
    self.ddlStatementLetter_u = ko.observableArray([]);
    self.Amount_u = ko.observable(0);
    self.AttachmentNo_u = ko.observable("");
    self.SelectUnderlyingDocument_u = ko.observable("");
    self.IsDeclarationOfException_u = ko.observable(false);
    self.StartDate_u = ko.observable('');
    self.EndDate_u = ko.observable('');
    self.DateOfUnderlying_u = ko.observable();
    self.ExpiredDate_u = ko.observable("");
    self.ReferenceNumber_u = ko.observable("");
    self.SupplierName_u = ko.observable("");
    self.InvoiceNumber_u = ko.observable("");
    self.IsStatementA = ko.observable(true);
    self.IsProforma_u = ko.observable(false);
    self.IsSelectedProforma = ko.observable(false);
    self.IsUtilize_u = ko.observable(false);
    // add bulkUnderlying // add 2015.03.09
    self.IsBulkUnderlying_u = ko.observable(false);
    self.IsSelectedBulk = ko.observable(false);
    // temp amount_u for bulk underlying
    self.tempAmountBulk = ko.observable(0);
    self.IsJointAccount_u = ko.observable(false);
    self.AccountNumber_u = ko.observable("");
    /*self.Amounttmp_u = ko.computed({
        read: function () {
            var fromFormat = document.getElementById("Amount_u").value;
            fromFormat = fromFormat.toString().replace(/,/g, '');
            //var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            if (self.StatementLetter_u().ID() == 1) {
                self.Currency_u().ID(1);
                self.Amount_u(TotalDealModel.TreshHold);
                // set default last day of month
                var date = new Date();
                var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                var day = lastDay.getDate();
                var month = lastDay.getMonth() + 1;
                var year = lastDay.getFullYear()
                var fixLastDay = year + "/" + month + "/" + day;
                //var fixLastDay = day + "-" + monthNames[month - 1] + "-" + year;
                self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
                SetDefaultValueStatementA();
            } else if (self.StatementLetter_u().ID() == 2 && self.DateOfUnderlying_u() != "" && self.IsNewDataUnderlying()) {
                if (self.LocalDate(self.DateOfUnderlying_u()) != "") {
                    var date = new Date(self.DateOfUnderlying_u());
                    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                    var day = date.getDate();
                    var month = lastDay.getMonth() + 13;
                    var year = lastDay.getFullYear();
                    if (month > 12) {
                        month -= 12;
                        year += 1;
                    }
                    var fixLastDay = year + "/" + month + "/" + day;
                    if (!moment(fixLastDay, 'YYYY/MM/DD').isValid()) {
                        var LastDay_ = new Date(year, month, 0);
                        day = LastDay_.getDate();
                    }
                    //fixLastDay = day + "-" + monthNames[month - 1] + "-" + year;
                    self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
                }
            }

            var e = document.getElementById("Amount_u").value;
            e = e.toString().replace(/,/g, '');

            var res = parseFloat(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res).toFixed(2));
            self.Amount_u(formatNumber(e));

        },
        write: function (data) {
            var fromFormat = document.getElementById("Amount_u").value;
            fromFormat = fromFormat.toString().replace(/,/g, '');


            var res = parseInt(fromFormat) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res).toFixed(2));
            self.Amount_u(formatNumber(fromFormat));

        }
    }, this);*/

    self.CaluclateRate_u = ko.computed({
        read: function () {
            var CurrencyID = self.Currency_u().ID();
            var AccountID = self.Selected().Account;
            if (CurrencyID != '' && CurrencyID != undefined) {
                GetRateIDRAmount(CurrencyID);
            } else {
                self.Rate_u(0);
            }
            if (AccountID == '' && AccountID != undefined) {
                alert('test');
            }
        },
        write: function () { }
    });

    self.Proformas = ko.observableArray([new SelectModel('', '')]);
    self.BulkUnderlyings = ko.observableArray([new SelectBulkModel('', '')]); // add 2015.03.09
    self.CodeUnderlying = ko.computed({
        read: function () {
            if (self.UnderlyingDocument_u().ID() != undefined && self.UnderlyingDocument_u().ID() != '') {
                var item = ko.utils.arrayFirst(self.ddlUnderlyingDocument_u(), function (x) {
                    return self.UnderlyingDocument_u().ID() == x.ID();
                }
                )
                if (item != null) {
                    return item.Code();
                }
                else {
                    return ''
                }
            } else { return '' }
        },
        write: function (value) {
            return value;
        }
    });
    self.OnChangeStatementLetterUnderlying = function () {
        var fromFormat = document.getElementById("Amount_u").value;
        fromFormat = fromFormat.toString().replace(/,/g, '');
        if (self.StatementLetter_u().ID() == 1) {
            self.Currency_u().ID(1);
            self.Amount_u(formatNumber(DataModel.ThresholdBuy));
            // set default last day of month
            var date = new Date();
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var day = lastDay.getDate();
            var month = lastDay.getMonth() + 1;
            var year = lastDay.getFullYear()
            var fixLastDay = year + "/" + month + "/" + day;
            self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
            SetDefaultValueStatementA();
        } else if (self.StatementLetter_u().ID() == 2 && self.IsNewDataUnderlying()) {
            self.ClearUnderlyingData();
            if (self.LocalDate(self.DateOfUnderlying_u()) != "") {
                var date = new Date(self.DateOfUnderlying_u());
                var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                var day = date.getDate();
                var month = lastDay.getMonth() + 13;
                var year = lastDay.getFullYear();
                if (month > 12) {
                    month -= 12;
                    year += 1;
                }
                var fixLastDay = year + "/" + month + "/" + day;
                if (!moment(fixLastDay, 'YYYY/MM/DD').isValid()) {
                    var LastDay_ = new Date(year, month, 0);
                    day = LastDay_.getDate();
                }
                self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
            }
        }

        var e = document.getElementById("Amount_u").value;
        e = e.toString().replace(/,/g, '');

        var res = parseFloat(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
        res = Math.round(res * 100) / 100;
        res = isNaN(res) ? 0 : res; //avoid NaN
        self.AmountUSD_u(parseFloat(res).toFixed(2));
        self.Amount_u(formatNumber(e));

    }
    self.onChangeDateOfUnderlying = function () {
        if (self.LocalDate(self.DateOfUnderlying_u()) != "") {
            var date = new Date(self.DateOfUnderlying_u());
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var day = date.getDate();
            var month = lastDay.getMonth() + 13;
            var year = lastDay.getFullYear();
            if (month > 12) {
                month -= 12;
                year += 1;
            }
            var fixLastDay = year + "/" + month + "/" + day;
            if (!moment(fixLastDay, 'YYYY/MM/DD').isValid()) {
                var LastDay_ = new Date(year, month, 0);
                day = LastDay_.getDate();
            }
            self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
        }
    }

    self.OtherUnderlying_u = ko.observable('');
    self.tmpOtherUnderlying = ko.observable('');
    self.IsSelected_u = ko.observable(false);
    self.LastModifiedBy_u = ko.observable("");
    self.LastModifiedDate_u = ko.observable("");

    // Attach Variable
    self.ID_a = ko.observable(0);
    self.FileName_a = ko.observable('');
    self.DocumentPurpose_a = ko.observable(new DocumentPurposeModel2('', '', ''));
    self.ddlDocumentPurpose_a = ko.observableArray([]);
    self.DocumentType_a = ko.observable(new DocumentTypeModel2('', ''));
    self.ddlDocumentType_a = ko.observableArray([]);
    self.DocumentRefNumber_a = ko.observable("");
    self.LastModifiedBy_a = ko.observable("");
    self.LastModifiedDate_a = ko.observable("");
    self.CustomerUnderlyingMappings_a = ko.observableArray([new CustomerUnderlyingMappingModel('', '', '')]);
    self.AddNetting = function () {
        $("#modal-form-netting-deal-number").modal('show');
        $('.remove').click();
        $('#backDrop').show();
    };
    self.AddAfterNetting = function () {
        $("#modal-form-after-netting-deal-number").modal('show');
        $('.remove').click();
        $('#backDrop').show();
    };
    self.Documents = ko.observableArray([]);
    self.DocumentPath_a = ko.observable("");
    self.DocumentPath = ko.observable();
    self.DocumentType = ko.observable();
    self.DocumentPurpose = ko.observable();
    self.DocumentFileName = ko.observable();
    // filter Underlying
    self.UnderlyingFilterCIF = ko.observable("");
    self.UnderlyingFilterParentID = ko.observable("");
    self.UnderlyingFilterIsSelected = ko.observable(false);
    self.UnderlyingFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingFilterDocumentType = ko.observable("");
    self.UnderlyingFilterCurrency = ko.observable("");
    self.UnderlyingFilterStatementLetter = ko.observable("");
    self.UnderlyingFilterAmount = ko.observable("");
    self.UnderlyingFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingFilterExpiredDate = ko.observable("");
    self.UnderlyingFilterReferenceNumber = ko.observable("");
    self.UnderlyingFilterSupplierName = ko.observable("");
    self.UnderlyingFilterIsProforma = ko.observable("");
    self.UnderlyingFilterIsAvailable = ko.observable(false);
    self.UnderlyingFilterIsExpiredDate = ko.observable(false);
    self.UnderlyingFilterIsSelectedBulk = ko.observable(false);
    self.UnderlyingFilterAvailableAmount = ko.observable("");
    self.UnderlyingFilterAttachmentNo = ko.observable("");
    self.UnderlyingFilterAccountNumber = ko.observable("");
    self.UnderlyingFilterShow = ko.observable(1);
    self.UnderlyingFilterIsTMO = ko.observable(false);

    // filter Attach Underlying File
    self.AttachFilterFileName = ko.observable("");
    self.AttachFilterDocumentPurpose = ko.observable("");
    self.AttachFilterModifiedDate = ko.observable("");
    self.AttachFilterDocumentRefNumber = ko.observable("");
    self.AttachFilterAttachmentReference = ko.observable("");

    // filter Underlying for Attach
    self.UnderlyingAttachFilterIsSelected = ko.observable("");
    self.UnderlyingAttachFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingAttachFilterDocumentType = ko.observable("");
    self.UnderlyingAttachFilterCurrency = ko.observable("");
    self.UnderlyingAttachFilterStatementLetter = ko.observable("");
    self.UnderlyingAttachFilterAmount = ko.observable("");
    self.UnderlyingAttachFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingAttachFilterExpiredDate = ko.observable("");
    self.UnderlyingAttachFilterReferenceNumber = ko.observable("");
    self.UnderlyingAttachFilterSupplierName = ko.observable("");
    self.UnderlyingAttachFilterAvailableAmount = ko.observable("");

    // filter Underlying for Attach
    self.ProformaFilterIsSelected = ko.observable(false);
    self.ProformaFilterUnderlyingDocument = ko.observable("");
    self.ProformaFilterDocumentType = ko.observable("Proforma");
    self.ProformaFilterCurrency = ko.observable("");
    self.ProformaFilterStatementLetter = ko.observable("");
    self.ProformaFilterAmount = ko.observable("");
    self.ProformaFilterDateOfUnderlying = ko.observable("");
    self.ProformaFilterExpiredDate = ko.observable("");
    self.ProformaFilterReferenceNumber = ko.observable("");
    self.ProformaFilterSupplierName = ko.observable("");
    self.ProformaFilterIsProforma = ko.observable(false);

    // filter Underlying for Attach // add 2015.03.09
    self.BulkFilterIsSelected = ko.observable(false);
    self.BulkFilterUnderlyingDocument = ko.observable("");
    self.BulkFilterDocumentType = ko.observable("");
    self.BulkFilterCurrency = ko.observable("");
    self.BulkFilterStatementLetter = ko.observable("");
    self.BulkFilterAmount = ko.observable("");
    self.BulkFilterDateOfUnderlying = ko.observable("");
    self.BulkFilterExpiredDate = ko.observable("");
    self.BulkFilterReferenceNumber = ko.observable("");
    self.BulkFilterSupplierName = ko.observable("");
    self.BulkFilterInvoiceNumber = ko.observable("");
    self.BulkFilterIsBulkUnderlying = ko.observable(false);

    //// start Checked Calculate for Attach Grid and Underlying Grid
    self.TempSelectedAttachUnderlying = ko.observableArray([]);

    //// start Checked Calculate for Underlying Proforma Grid
    self.TempSelectedUnderlyingProforma = ko.observableArray([]);

    //// start Checked Calculate for Underlying Bulk Grid // add 2015.03.09
    self.TempSelectedUnderlyingBulk = ko.observableArray([]);

    //// start Checked Calculate for Underlying Grid
    self.TempSelectedUnderlying = ko.observableArray([]);

    // set default sorting for Underlying Grid
    self.UnderlyingGridProperties().SortColumn("ID");
    self.UnderlyingGridProperties().SortOrder("ASC");

    // set default sorting for Underlying File Grid
    self.AttachGridProperties().SortColumn("FileName");
    self.AttachGridProperties().SortOrder("ASC");


    // set default sorting for Attach Underlying Grid
    self.UnderlyingAttachGridProperties().SortColumn("ID");
    self.UnderlyingAttachGridProperties().SortOrder("ASC");

    // set default sorting for Proforma grid
    self.UnderlyingProformaGridProperties().SortColumn("IsSelectedProforma");
    self.UnderlyingProformaGridProperties().SortOrder("DESC");

    // set default sorting for Bulk grid //add 2015.03.09
    self.UnderlyingBulkGridProperties().SortColumn("IsSelectedBulk");
    self.UnderlyingBulkGridProperties().SortOrder("DESC");
    self.utilizationAmount = ko.observable(0.00);
    function ResetDataAttachment(index, isSelect) {
        self.CustomerAttachUnderlyings()[index].IsSelectedAttach = isSelect;
        var dataRows = self.CustomerAttachUnderlyings().slice(0);
        self.CustomerAttachUnderlyings([]);
        self.CustomerAttachUnderlyings(dataRows);
    }

    function ResetDataProforma(index, isSelect) {
        self.CustomerUnderlyingProformas()[index].IsSelectedProforma = isSelect;
        var dataRows = self.CustomerUnderlyingProformas().slice(0);
        self.CustomerUnderlyingProformas([]);
        self.CustomerUnderlyingProformas(dataRows);
    }

    function ResetBulkData(index, isSelect) { //add 2015.03.09
        self.CustomerBulkUnderlyings()[index].IsSelectedBulk = isSelect;
        var dataRows = self.CustomerBulkUnderlyings().slice(0);
        self.CustomerBulkUnderlyings([]);
        self.CustomerBulkUnderlyings(dataRows);
    }

    function ResetDataUnderlying(index, isSelect) {
        self.CustomerUnderlyings()[index].IsEnable = isSelect;
        var dataRows = self.CustomerUnderlyings().slice(0);
        self.CustomerUnderlyings([]);
        self.CustomerUnderlyings(dataRows);
    }

    function GetApplicationID() {
        var options = {
            url: api.server + api.url.transaction + "/GetApplicationID",
            params: {
                ProductID: self.TransactionNetting().Product().ID
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetApplicationID, OnError, OnAlways);

    };

    function SetMappingAttachment() {
        CustomerUnderlyingFile.CustomerUnderlyingMappings([]);
        for (var i = 0 ; i < self.TempSelectedAttachUnderlying().length; i++) {
            CustomerUnderlyingFile.CustomerUnderlyingMappings.push(new CustomerUnderlyingMappingModel(0, self.TempSelectedAttachUnderlying()[i].ID, customerNameData + '-(A)'));
        }
    }

    function SetMappingProformas() {
        CustomerUnderlying.Proformas([]);
        for (var i = 0 ; i < self.TempSelectedUnderlyingProforma().length; i++) {
            CustomerUnderlying.Proformas.push(new SelectModel(cifData, self.TempSelectedUnderlyingProforma()[i].ID));
        }
    }

    function SetMappingBulks() { //add 2015.03.09
        //console.log(CustomerUnderlying.BulkUnderlyings());
        CustomerUnderlying.BulkUnderlyings([]);
        for (var i = 0 ; i < self.TempSelectedUnderlyingBulk().length; i++) {
            CustomerUnderlying.BulkUnderlyings.push(new SelectBulkModel(cifData, self.TempSelectedUnderlyingBulk()[i].ID));
        }
    }

    function setTotalUtilize() {
        var total = 0;
        ko.utils.arrayForEach(self.CustomerUnderlyings(), function (item) {
            total += parseFloat(ko.utils.unwrapObservable(item.USDAmount()));
        });
        viewModel.TransactionNetting().utilizationAmount(parseFloat(total).toFixed(2));
    }

    function setStatementA() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetStatementA/" + cifData,
            data: null, //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            }, success: function (data) {
                if (data.IsStatementA) {
                    self.StatementLetter_u(new StatementLetterModel(2, 'Statement B'));
                    self.IsStatementA(false);
                } else { self.IsStatementA(true); }
            }
        });
    }
    function setRefID() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetRefID/" + cifData,
            data: null, //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            }, success: function (data) {
                self.ReferenceNumber_u(customerNameData + '-' + data.ID);
            }
        });

    }
    function SetSelectedProforma(data) {
        // if(self.TempSelectedUnderlyingProforma().length>0) {
        for (var i = 0 ; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlyingProforma(), function (item) {
                return item.ID == data[i].ID;
            });
            if (selected != null) {
                data[i].IsSelectedProforma = true;
            }
            else {
                data[i].IsSelectedProforma = false;
            }
        }
    }

    function SetSelectedBulk(data) { //add 2015.03.09

        for (var i = 0 ; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlyingBulk(), function (item) {
                return item.ID == data[i].ID;
            });
            if (selected != null) {
                data[i].IsSelectedBulk = true;
            }
            else {
                data[i].IsSelectedBulk = false;
            }
        }
    }

    function TotalUtilize() {
        var total = 0;
        ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (item) {
            if (item.Enable() == false)
                total += parseFloat(ko.utils.unwrapObservable(item.USDAmount()));
        });
        var fixTotal = parseFloat(total).toFixed(2);
        return fixTotal;
    }

    function SetDefaultValueStatementA() {
        var today = Date.now();
        var documentType = ko.utils.arrayFirst(self.ddlDocumentType_u(), function (item) { return item.Name == '-' });
        var underlyingDocument = ko.utils.arrayFirst(self.ddlUnderlyingDocument_u(), function (item) { return item.Code() == 998 });

        if (documentType != null) {
            self.DocumentType_u(new DocumentTypeModel2(documentType.ID, documentType.Name));
        }

        if (underlyingDocument != null) {
            self.UnderlyingDocument_u(new UnderlyingDocumentModel2(underlyingDocument.ID(), underlyingDocument.Name(), underlyingDocument.Code()));
        }
        self.DateOfUnderlying_u(self.LocalDate(today, true, false));
        self.SupplierName_u('-');
        self.InvoiceNumber_u('-');
    }
    function GetRateIDRAmount(CurrencyID) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
            params: {
            },
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    self.Rate_u(data.RupiahRate);
                    //if(CurrencyID!=1){ // if not USD

                    var fromFormat = document.getElementById("Amount_u").value;

                    fromFormat = fromFormat.toString().replace(/,/g, '');

                    res = parseFloat(fromFormat) * data.RupiahRate / idrrate;
                    res = Math.round(res * 100) / 100;
                    res = isNaN(res) ? 0 : res; //avoid NaN
                    self.AmountUSD_u(parseFloat(res).toFixed(2));

                    self.Amount_u(formatNumber(fromFormat));
                    //}
                }
            }
        });
    }

    self.onSelectionAttach = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (x) {
            return x.ID == item.ID;
        });
        //console.log(item);
        if (data != null) {
            //self.IsSelectedAttach(false);
            self.TempSelectedAttachUnderlying.remove(data);
            ResetDataAttachment(index, false);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        } else {
            //self.IsSelectedAttach(true);
            self.TempSelectedAttachUnderlying.push({
                ID: item.ID
            });
            ResetDataAttachment(index, true);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        }
    }

    // add 2015.03.09
    self.OnCurrencyChange = function (CurrencyID) {
        if (CurrencyID != null) {
            var item = ko.utils.arrayFirst(self.ddlCurrency_u(), function (x) { return CurrencyID == x.ID(); });
            if (item != null) {
                self.Currency_u(new CurrencyModel2(item.ID(), item.Code(), item.Description()));
            }
            if (!self.IsNewDataUnderlying()) {
                GetSelectedBulkID(GetDataBulkUnderlying);
            } else {
                self.TempSelectedUnderlyingBulk([]);
                GetDataBulkUnderlying();
            }
        }
    }
    self.OnChangeJointAccUnderlying = function () {
        if (!CustomerUnderlying.IsJointAccount()) {
            CustomerUnderlying.AccountNumber(null);
        }
    }
    self.TransactionNetting().bookunderlyingcode.subscribe(function (underlyingID) {
        //get underlying code based on selected
        if (underlyingID > 0) {
            GetUnderlyingDocName(underlyingID);
        }
    });

    function GetUnderlyingDocName(underlyingID) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.underlyingdoc + "/" + underlyingID,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if (data != null) {
                        self.isNewUnderlying(false); //return default disabled

                        // available type to input for new underlying name if code is 999
                        if (data.Code == '999') {
                            self.TransactionNetting().bookunderlyingdesc('');
                            self.isNewUnderlying(true);
                            $('#book-underlying-desc').focus();

                            $('#book-underlying-desc').attr('data-rule-required', true);
                        }
                        else {
                            self.isNewUnderlying(false);
                            self.TransactionNetting().OtherUnderlying('');

                            $('#book-underlying-desc').attr('data-rule-required', false);
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    };
    self.LookupTz = function () {
        if (event.currentTarget.id == 'btnDealNumber') {
            self.IsBtnDealNumber(true);
        } else {
            self.IsBtnDealNumber(false);
        }
        self.GetDataTZ();
        $("#modal-form-Tz").modal('show');
        $('#backDrop').show();
    };

    self.SelectedTZ = function () {

        GetTZInformation(self.CIFData(), self.TmpTzNumber(), self.MurexNumber());


    }
    // bind clear filters
    self.UnderlyingClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.UnderlyingFilterIsSelected(false);
        self.UnderlyingFilterUnderlyingDocument("");
        self.UnderlyingFilterAvailableAmount("");
        self.UnderlyingFilterDocumentType("");
        self.UnderlyingFilterCurrency("");
        self.UnderlyingFilterStatementLetter("");
        self.UnderlyingFilterAmount("");
        self.UnderlyingFilterDateOfUnderlying("");
        self.UnderlyingFilterExpiredDate("");
        self.UnderlyingFilterReferenceNumber("");
        self.UnderlyingFilterSupplierName("");
        self.UnderlyingFilterIsProforma("");
        self.UnderlyingFilterAttachmentNo("");
        self.UnderlyingFilterIsAvailable(false);
        self.UnderlyingFilterIsExpiredDate(false);

        GetDataUnderlying();
    };

    self.ProformaClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.ProformaFilterIsSelected(false);
        self.ProformaFilterUnderlyingDocument("");
        self.ProformaFilterDocumentType("Proforma");
        self.ProformaFilterCurrency("");
        self.ProformaFilterStatementLetter("");
        self.ProformaFilterAmount("");
        self.ProformaFilterDateOfUnderlying("");
        self.ProformaFilterExpiredDate("");
        self.ProformaFilterReferenceNumber("");
        self.ProformaFilterSupplierName("");
        self.ProformaFilterIsProforma("");

        GetDataUnderlyingProforma();
    };
    //add 2015.03.09
    self.BulkClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.BulkFilterIsSelected(false);
        self.BulkFilterUnderlyingDocument("");
        self.BulkFilterDocumentType("");
        self.BulkFilterCurrency("");
        self.BulkFilterStatementLetter("");
        self.BulkFilterAmount("");
        self.BulkFilterDateOfUnderlying("");
        self.BulkFilterExpiredDate("");
        self.BulkFilterReferenceNumber("");
        self.BulkFilterInvoiceNumber("");
        self.BulkFilterSupplierName("");
        self.BulkFilterIsBulkUnderlying("");

        GetDataBulkUnderlying();
    };
    // bind clear filters
    self.AttachClearFilters = function () {
        self.AttachFilterFileName("");
        self.AttachFilterDocumentPurpose("");
        self.AttachFilterModifiedDate("");
        self.AttachFilterDocumentRefNumber("");
        self.AttachFilterAttachmentReference("");

        GetDataAttachFile();
    };

    // bind clear filters
    self.UnderlyingAttachClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.UnderlyingAttachFilterIsSelected(false);
        self.UnderlyingAttachFilterUnderlyingDocument("");
        self.UnderlyingAttachFilterDocumentType("");
        self.UnderlyingAttachFilterCurrency("");
        self.UnderlyingAttachFilterStatementLetter("");
        self.UnderlyingAttachFilterAmount("");
        self.UnderlyingAttachFilterDateOfUnderlying("");
        self.UnderlyingAttachFilterExpiredDate("");
        self.UnderlyingAttachFilterReferenceNumber("");
        self.UnderlyingAttachFilterSupplierName("");

        GetDataUnderlyingAttach();
    };

    self.GetDataUnderlying = function () {
        GetDataUnderlying();
    };

    self.GetDataAttachFile = function () {
        GetDataAttachFile();
    }

    self.GetDataUnderlyingAttach = function () {
        GetDataUnderlyingAttach();
    }

    self.GetDataUnderlyingProforma = function () {
        GetDataUnderlyingProforma();
    }

    self.GetDataBulkUnderlying = function () {
        GetDataBulkUnderlying();
    }

    self.GetUnderlyingParameters = function (data) {
        GetUnderlyingParameters(data);
    }

    self.CalculateFX = function () {
        CalculateFX();
    }
    function GetTZInformation(cif, tzref, murexnumber) {
        var options = {
            url: api.server + api.url.helper + '/TZRef/GetDetailTZReference',
            params: {
                cif: cif,
                TZReference: tzref,
                MurexNumber: murexnumber
            },
            token: accessToken
        }
        Helper.Ajax.Get(options, OnSuccessGetTZInformation, OnError);

    }

    function OnSuccessGetTZInformation(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.Selected().StatementLetter(data.StatementLetter.ID);
            GetCustomerUnderlyingNetting(data.ID);
            self.IsTZAvailable(true);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }

    }

    function GetCustomerUnderlyingNetting(Id) {
        var options = {
            url: api.server + "api/WorkflowDealTMO/Transaction/Maker/Underlying/" + Id,
            token: accessToken
        }

        Helper.Ajax.Get(options, OnSuccessGetCustomerUnderlyingNetting, OnError);

    }

    function OnSuccessGetCustomerUnderlyingNetting(data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            self.CustomerUnderlyings([]);
            self.utilizationAmount(0);
            if (data.Customer.Underlyings != null) {
                $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                    $(this).prev().focus();
                });
                self.NettingUnderlyings([]);               
                self.TempSelectedUnderlying([]);
                self.CustomerDocuments([]);
                self.CustomerAllDocumentsTemp([]);
                self.TransactionNetting().Documents([]);

                ko.utils.arrayForEach(data.Customer.Underlyings, function (item) {//for(var i=0;data.Transaction.Customer.Underlyings.length>i;i++){                        
                    self.NettingUnderlyings.push(item.ID);
                    self.CustomerUnderlyings(data.Customer.Underlyings);

                });
                if (self.CustomerUnderlyings() != null && self.CustomerUnderlyings().length > 0) {
                    self.TempSelectedUnderlying([]);
                    ko.utils.arrayForEach(self.CustomerUnderlyings(), function (underlyingItem) {
                        if (underlyingItem.IsEnable == true) {
                            self.TempSelectedUnderlying.push(new SelectUtillizeModel(underlyingItem.ID, false, underlyingItem.AvailableAmount, underlyingItem.StatementLetter.ID));
                        }
                    });
                    var total = TotalUtilize();
                    self.utilizationAmount(total);
                }
            } 
            //viewModel.GetDataUnderlying();
            DealModel.cif = cifData;
            if (data.Documents != null) {
                ko.utils.arrayForEach(data.Documents, function (item) {
                    item.IsNewDocument = false;
                    self.NettingAllDocumentsTemp.push(item);
                    self.CustomerDocuments.push(item);
                    self.TransactionNetting().Documents.push(item);
                });
            }
            var t_IsFxTransaction = (data.Currency.Code != 'IDR' && data.AccountCurrencyCode == 'IDR');
            var t_IsFxTransactionToIDR = (data.Currency.Code == 'IDR' && data.AccountCurrencyCode != 'IDR');

            self.t_IsFxTransaction(t_IsFxTransaction);
            self.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);

            if (self.t_IsFxTransaction() || self.t_IsFxTransactionToIDR()) {

                GetTotalTransaction(data);// For Get Total IDR - FCY & Utilization
            }

        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }

    }
    function GetTotalTransaction(transaction) {
        if (viewModel != null) {
            var paramhreshold = GetValueParamterThreshold(transaction);
            var options = {
                url: api.server + api.url.helper + "/GetTotalTransactionIDRFCY",
                token: accessToken,
                params: {},
                data: ko.toJSON(paramhreshold)
            };
            Helper.Ajax.Post(options, function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    TotalPPUModel.Total.IDRFCYPPU = data.TotalTransaction;
                    TotalDealModel.Total.IDRFCYDeal = data.TotalTransaction;
                    self.TotalTransFX(data.TotalTransaction);

                    TotalPPUModel.Total.UtilizationPPU = data.TotalUtilizationAmount;
                    TotalDealModel.Total.UtilizationDeal = data.TotalUtilizationAmount;
                    TotalDealModel.Total.RemainingBalance = data.AvailableUnderlyingAmount;
                    self.Rounding(data.RoundingValue);
                    self.ThresholdType(data.ThresholdType);
                    self.ThresholdValue(data.ThresholdValue);
                    self.IsNoThresholdValue(data.ThresholdValue == 0 ? true : false);
                    SetCalculateFX(paramhreshold.AmountUSD);
                }
            }, OnError);
        }
    }
    function GetValueParamterThreshold(transaction) {
        var paramhreshold;
        var Transaction;

        Transaction = transaction;

        if (Transaction != null) {
            paramhreshold = {
                ProductTypeID: GetProductTypeID(Transaction.ProductType),
                DebitCurrencyCode: Transaction.DebitCurrency.Code,
                TransactionCurrencyCode: Transaction.Currency.Code,
                IsResident: Transaction.IsResident,
                AmountUSD: parseFloat(Transaction.AmountUSD),
                CIF: Transaction.Customer.CIF,
                AccountNumber: Transaction.IsOtherAccountNumber ? null : Transaction.Account.AccountNumber,
                IsJointAccount: Transaction.IsJointAccount
            }
        }

        return paramhreshold;
    }

    function GetProductTypeID(FlowValas) {
        return (FlowValas == null ? null : FlowValas.ID);
    }
    function SetCalculateFX(amountUSD) {
        if (self.StatementLetter != null && (self.StatementLetter.ID == 1 || self.StatementLetter.ID == 4)) {
            AmountModel.RemainingBalance(0.00);
        } else if (self.StatementLetter != null && self.StatementLetter.ID != 4) {
            AmountModel.RemainingBalance(TotalDealModel.Total.RemainingBalance);
        }
        AmountModel.TotalPPUAmountsUSD(TotalDealModel.Total.IDRFCYDeal);
        AmountModel.TotalDealAmountsUSD(TotalDealModel.Total.IDRFCYDeal);
        AmountModel.TotalPPUUtilization(TotalPPUModel.Total.UtilizationPPU);
        AmountModel.TotalDealUtilization(TotalDealModel.Total.UtilizationDeal);
    }


    //The Object which stored data entered in the observables
    var CustomerUnderlying = {
        ID: self.ID_u,
        CIF: self.CIF_u,
        CustomerName: self.CustomerName_u,
        UnderlyingDocument: self.UnderlyingDocument_u,
        DocumentType: self.DocumentType_u,
        Currency: self.Currency_u,
        Amount: self.Amount_u,
        Rate: self.Rate_u,
        AmountUSD: self.AmountUSD_u,
        AvailableAmountUSD: self.AvailableAmount_u,
        AttachmentNo: self.AttachmentNo_u,
        StatementLetter: self.StatementLetter_u,
        IsDeclarationOfException: self.IsDeclarationOfException_u,
        StartDate: self.StartDate_u,
        EndDate: self.EndDate_u,
        DateOfUnderlying: self.DateOfUnderlying_u,
        ExpiredDate: self.ExpiredDate_u,
        ReferenceNumber: self.ReferenceNumber_u,
        SupplierName: self.SupplierName_u,
        InvoiceNumber: self.InvoiceNumber_u,
        IsSelectedProforma: self.IsSelectedProforma,
        IsProforma: self.IsProforma_u,
        IsUtilize: self.IsUtilize_u,
        IsEnable: self.IsEnable_u,
        Proformas: self.Proformas,
        IsSelectedBulk: self.IsSelectedBulk,
        IsBulkUnderlying: self.IsBulkUnderlying_u,
        BulkUnderlyings: self.BulkUnderlyings,
        OtherUnderlying: self.OtherUnderlying_u,
        IsJointAccount: self.IsJointAccount_u,
        AccountNumber: self.AccountNumber_u,
        LastModifiedBy: self.LastModifiedBy_u,
        LastModifiedDate: self.LastModifiedDate_u,
        IsTMO: true
    };

    var Documents = {
        Documents: self.Documents,
        DocumentPath: self.DocumentPath_a,
        Type: self.DocumentType_a,
        Purpose: self.DocumentPurpose_a
    }

    var CustomerUnderlyingFile = {
        ID: self.ID_a,
        FileName: DocumentModels().FileName,
        DocumentPath: DocumentModels().DocumentPath,
        DocumentPurpose: self.DocumentPurpose_a,
        DocumentType: self.DocumentType_a,
        DocumentRefNumber: self.DocumentRefNumber_a,
        LastModifiedBy: self.LastModifiedBy_a,
        LastModifiedDate: self.LastModifiedDate_a,
        CustomerUnderlyingMappings: self.CustomerUnderlyingMappings_a
    };

    function CalculateFX() {
        viewModel.AmountModel().TotalUtilization(TotalDealModel.Total.UtilizationDeal);
    }

    self.GetDataTZ = function () {
        GetDataTZ();
    }

    // Uploading document
    self.UploadDocumentUnderlying = function () {
        self.IsUploading(false);
        // show the dialog task form
        $("#modal-form-Attach").modal('show');
        self.TempSelectedAttachUnderlying([]);
        self.ID_a(0);
        self.DocumentPurpose_a(new DocumentPurposeModel2('', '', ''));
        self.DocumentType_a(new DocumentTypeModel2('', ''));
        self.DocumentPath_a('');
        GetDataUnderlyingAttach();
        $('.remove').click();
    }

    self.UploadDocument = function () {
        self.DocumentPath(null);
        self.Selected().DocumentType(null);
        self.Selected().DocumentPurpose(null);
        self.DocumentType(null);
        self.DocumentPurpose(null);
        $("#modal-form-upload").modal('show');

    }

    // Add new document handler
    self.AddDocument = function () {
        var doc = {
            ID: 0,
            Type: self.DocumentType(),
            Purpose: self.DocumentPurpose(),
            FileName: self.DocumentFileName(),
            DocumentPath: self.DocumentPath(),
            LastModifiedDate: new Date(),
            LastModifiedBy: null
        };

        //alert(JSON.stringify(self.DocumentPath()))
        if (doc.Type == null || doc.Purpose == null || doc.DocumentPath == null || doc.DocumentPath == "") {
            alert("Please complete the upload form fields.")
        } else {
            self.Documents.push(doc);
            $('.remove').click();
            // hide upload dialog
            $("#modal-form-upload").modal('hide');
        }
    };

    self.EditDocument = function (data) {
        self.DocumentPath(data.DocumentPath);
        self.Selected().DocumentType(data.Type.ID);
        self.Selected().DocumentPurpose(data.Purpose.ID);

        // show upload dialog
        $("#modal-form-upload").modal('show');
    };

    self.RemoveDocument = function (data) {
        //alert(JSON.stringify(data))
        self.Documents.remove(data);
    };

    self.NewDataUnderlying = function () {
        //Remove required filed if show
        $('.form-group').removeClass('has-error').addClass('has-info');
        $('.help-block').remove();
        $("#modal-form-Underlying").modal('show');
        // flag as new Data
        self.IsNewDataUnderlying(true);

        // bind empty data
        self.ID_u(0);
        self.StatementLetter_u(new StatementLetterModel('', ''));
        self.ReferenceNumber_u('');
        self.DateOfUnderlying_u('');
        self.ClearUnderlyingData();
        self.IsProforma_u(false);
        self.IsSelectedProforma(false);
        self.IsUtilize_u(false);
        self.IsJointAccount_u(false);
        self.AccountNumber_u('');
        self.IsBulkUnderlying_u(false); //add 2015.03.09
        self.IsSelectedBulk(false); //add 2015.03.09
        //Call Grid Underlying for Check Proforma
        GetDataUnderlyingProforma();
        GetDataBulkUnderlying();
        self.TempSelectedUnderlyingBulk([]);
        self.TempSelectedUnderlyingProforma([]);
        setRefID();
    };

    // Save transaction
    self.SaveNettingTransaction = function () {
        self.TransactionNetting().IsDraft(false);
        // validation        
        var form = $("#aspnetForm");
        form.validate();
        if (viewModel.TransactionNetting().IDTZReference() == viewModel.TransactionNetting().IDNettingTZReference()) {
            ShowNotification("Warning", "Deal Number must be not equal with Deal Netting", "gritter-warning", true);
            return false;
        }

        if (form.valid()) {
            self.IsEnableSubmit(false);
            if (viewModel.ID() != null) {
                viewModel.TransactionNetting().NettingTZReference('');
                viewModel.TransactionNetting().TZReference('');
                viewModel.TransactionNetting().TZReference(viewModel.TZReferenceTemp);
                viewModel.TransactionNetting().NettingTZReference(viewModel.NettingTZReferenceTemp);
            }
            ValidateTransaction();
        }
        else {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
            self.IsEnableSubmit(true);
        }
    };

    // Save as draft
    self.SaveDraftNettingTransaction = function () {
        self.TransactionNetting().IsDraft(true);
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            viewModel.IsEditable(false);

            if (viewModel.ID() != null) {
                viewModel.TransactionNetting().NettingTZReference('');
                viewModel.TransactionNetting().TZReference('');
                viewModel.TransactionNetting().TZReference(viewModel.TZReferenceTemp);
                viewModel.TransactionNetting().NettingTZReference(viewModel.NettingTZReferenceTemp);
            }
            ValidateTransaction();

        } else {
            viewModel.IsEditable(true);
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);

        }

    };

    self.RemoveDocumentData = function (data) {
        bootbox.confirm("Are you sure wan't to delete?", function (result) {
            if (result) {
                if (data.Purpose.ID == 2) {
                    // delete underlying document
                    DeleteUnderlyingAttachment(data);

                    var item = ko.utils.arrayFilter(viewModel.TransactionNetting().Documents(), function (dta) { return dta.DocumentPath != data.DocumentPath; });
                    if (item != null) {
                        viewModel.TransactionNetting().Documents(item);
                    }


                } else {
                    if (data.IsNewDocument == false) {
                        var item = ko.utils.arrayFilter(viewModel.TransactionNetting().Documents(), function (dta) { return dta.ID != data.ID; });
                        if (item != null) {
                            viewModel.TransactionNetting().Documents(item);
                        }

                    }
                }
                self.CustomerAllDocumentsTemp.remove(data);
                self.CustomerDocuments.remove(data);

            }
        });

    }

    function DeleteUnderlyingAttachment(item) {
        //Ajax call to delete the Customer

        $.ajax({
            type: "DELETE",
            url: api.server + api.url.customerunderlyingfile + "/" + item.ID,
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                // send notification
                if (jqXHR.status = 200) {
                    //DeleteFile(item.DocumentPath); deleted file underlying
                    //if(!item.IsNewDocument) {
                    // DeleteTransactionDocumentUnderlying(item);
                    //}
                    ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                    // refresh data
                    //GetDataAttachFile();
                } else
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetSelectedProformaID(callback) {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.ProformaFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.ProformaFilterIsSelected() });
        if (self.ProformaFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.ProformaFilterUnderlyingDocument() });
        if (self.ProformaFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.ProformaFilterDocumentType() });


        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SelectedProformaID",
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON(filters),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlyingProforma([]);
                    for (var i = 0; i < data.length; i++) {
                        self.TempSelectedUnderlyingProforma.push({ ID: data[i].ID });
                    }
                    if (callback != null) {
                        callback();
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    // Function to get All selected Bulk Underlying // add 2015.03.09
    function GetSelectedBulkID(callback) {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: self.CIF_u() });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.BulkFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.BulkFilterIsSelected() });
        if (self.BulkFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.BulkFilterUnderlyingDocument() });
        if (self.BulkFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.BulkFilterDocumentType() });


        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SelectedBulkID",
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON(filters),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlyingBulk([]);
                    for (var i = 0; i < data.length; i++) {
                        self.TempSelectedUnderlyingBulk.push({ ID: data[i].ID, Amount: data[i].Amount });
                    }
                    if (callback != null) {
                        callback();
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    //Function to Display record to be updated
    self.GetUnderlyingSelectedRow = function (data) {

        self.IsNewDataUnderlying(false);

        $("#modal-form-Underlying").modal('show');

        self.ID_u(data.ID);
        self.CustomerName_u(data.CustomerName);
        self.UnderlyingDocument_u(new UnderlyingDocumentModel2(data.UnderlyingDocument.ID, data.UnderlyingDocument.Name, data.UnderlyingDocument.Code));
        self.OtherUnderlying_u(data.OtherUnderlying);
        self.DocumentType_u(new DocumentTypeModel2(data.DocumentType.ID, data.DocumentType.Name));

        //self.Currency_u(data.Currency);
        self.Currency_u(new CurrencyModel2(data.Currency.ID, data.Currency.Code, data.Currency.Description));

        self.Amount_u(data.Amount);
        self.AttachmentNo_u(data.AttachmentNo);

        //self.StatementLetter_u(data.StatementLetter);
        self.StatementLetter_u(new StatementLetterModel(data.StatementLetter.ID, data.StatementLetter.Name));

        self.IsDeclarationOfException_u(data.IsDeclarationOfException);
        self.StartDate_u(self.LocalDate(data.StartDate, true, false));
        self.EndDate_u(self.LocalDate(data.EndDate, true, false));
        self.DateOfUnderlying_u(self.LocalDate(data.DateOfUnderlying, true, false));
        self.ExpiredDate_u(self.LocalDate(data.ExpiredDate, true, false));
        self.ReferenceNumber_u(data.ReferenceNumber);
        self.SupplierName_u(data.SupplierName);
        self.InvoiceNumber_u(data.InvoiceNumber);
        self.IsProforma_u(data.IsProforma);
        self.IsUtilize_u(data.IsUtilize);
        self.IsSelectedProforma(data.IsSelectedProforma);
        self.IsBulkUnderlying_u(data.IsBulkUnderlying); // add 2015.03.09
        self.IsSelectedBulk(data.IsSelectedBulk); // add 2015.03.09
        self.tempAmountBulk(data.Amount);
        self.LastModifiedBy_u(data.LastModifiedBy);
        self.LastModifiedDate_u(data.LastModifiedDate);
        //Call Grid Underlying for Check Proforma

        GetSelectedProformaID(GetDataUnderlyingProforma);
        GetSelectedBulkID(GetDataBulkUnderlying);

    };

    function SetSelectedUnderlying(data) {
        for (var i = 0; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (item) {
                return item.ID() == data[i].ID;
            });

            if (selected != null) {
                data[i].IsEnable = true;
            }
            else {
                data[i].IsEnable = false;
            }
        }
        return data;
    }
    self.ClearUnderlyingData = function () {
        if (self.Selected().Currency() != self.Currency_u().ID && self.StatementLetter_u().ID != 1) {
            var cur = ko.utils.arrayFirst(self.ddlCurrency_u(), function (item) {
                return self.Selected().Currency() == item.ID();
            });

            if (cur != null) {
                self.Currency_u(new CurrencyModel2(cur.ID(), cur.Code(), cur.Description()));
            } else {
                self.Currency_u(new CurrencyModel2('', '', ''));
            }

        }
        self.UnderlyingDocument_u(new UnderlyingDocumentModel2('', '', ''));
        self.OtherUnderlying_u('');
        self.DocumentType_u(new DocumentTypeModel2('', ''));

        self.Amount_u(0);
        self.AttachmentNo_u();
        self.IsDeclarationOfException_u(false);
        self.StartDate_u('');
        self.EndDate_u('');
        self.ExpiredDate_u('');
        self.SupplierName_u('');
        self.InvoiceNumber_u('');
    }
    self.onSelectionUtilize = function (index, item) {

        var statementLetterID = self.TransactionNetting().StatementLetter().ID;
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
            return x.ID() == item.ID;
        });

        if (data != null) { //uncheck condition

            self.TempSelectedUnderlying.remove(data);
            self.NettingUnderlyings.remove(item.ID);
            ResetDataUnderlying(index, false);
            setTotalUtilize();
            SetNettingDocuments();

        } else {

            var statementA = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                return x.StatementLetter() == 1;
            });
            var statementB = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                return x.StatementLetter() == 2;
            });

            if (statementA != null && item.StatementLetter.ID == 1 ||
                statementB != null && item.StatementLetter.ID == 2 ||
                self.Selected().StatementLetter() == item.StatementLetter.ID) {

                self.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, false, item.AvailableAmount, item.StatementLetter.ID));
                self.NettingUnderlyings.push(item.ID);
                ResetDataUnderlying(index, true);
                setTotalUtilize();
                SetNettingDocuments();
            } else {
                ResetDataUnderlying(index, false);
            }
        }

    }
    function SetNettingDocuments() {
        // remove all underlying documents        
        ko.utils.arrayForEach(self.NettingAllDocumentsTemp(), function (item) {
            if (item.Purpose.ID == 2) {
                self.CustomerDocuments.remove(item);
                var dataDoc = jQuery.grep(self.TransactionNetting().Documents, function (value) {
                    return value.ID != item.ID;
                });
                self.TransactionNetting().Documents(dataDoc);
            }
        });
        // push underlying by makerunderlyings
        ko.utils.arrayForEach(self.NettingAllDocumentsTemp(), function (item) {
            ko.utils.arrayForEach(self.NettingUnderlyings(), function (item2) {
                if (item2 == item.UnderlyingID && item.Purpose.ID == 2) {
                    self.CustomerDocuments.push(item);
                    self.TransactionNetting().Documents.push(item);
                }
            });
        });
    }

    self.onSelectionBulk = function (index, item) {
        // var temp = self.Amount_u();
        var total = 0;
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlyingBulk(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            //self.IsSelectedAttach(false);
            self.TempSelectedUnderlyingBulk.remove(data);
            ResetBulkData(index, false);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        } else {
            //self.IsSelectedAttach(true);
            self.TempSelectedUnderlyingBulk.push({
                ID: item.ID,
                Amount: item.Amount
            });
            ResetBulkData(index, true);
        }
        for (var i = 0; self.TempSelectedUnderlyingBulk.push() > i; i++) {
            total += self.TempSelectedUnderlyingBulk()[i].Amount;
        }
        if (total > 0) {
            //self.Amount_u(total);
            var e = total;
            var res = parseInt(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res).toFixed(2));
            self.Amount_u(formatNumber(e));
        } else {
            var e = self.tempAmountBulk();
            var res = parseInt(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res).toFixed(2));
            self.Amount_u(formatNumber(e));
            //self.Amount_u(self.tempAmountBulk());
        }

    }

    self.onSelectionProforma = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlyingProforma(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            //self.IsSelectedAttach(false);
            self.TempSelectedUnderlyingProforma.remove(data);
            ResetDataProforma(index, false);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        } else {
            //self.IsSelectedAttach(true);
            self.TempSelectedUnderlyingProforma.push({
                ID: item.ID
            });
            ResetDataProforma(index, true);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));            
        }
    }

    // Function to Read parameter Underlying
    function GetUnderlyingParameters(data) {

        var underlying = data['UnderltyingDoc'];
        for (var i = 0; i < underlying.length; i++) {
            self.ddlUnderlyingDocument_u.push(new UnderlyingDocumentModel2(underlying[i].ID, underlying[i].Name, underlying[i].Code));
        }

        self.ddlDocumentType_u(data['DocType']);
        self.ddlDocumentType_a(data['DocType']);
        ko.utils.arrayForEach(data['Currency'], function (item) {
            self.ddlCurrency_u.push(new CurrencyModel2(item.ID, item.Code, item.Description));
        });
        self.ddlDocumentPurpose_a(data['PurposeDoc']);
        self.ddlStatementLetter(data['StatementLetter']);
        ko.utils.arrayForEach(data['StatementLetter'], function (item) {
            if (item.ID == 1 || item.ID == 2) {
                self.ddlStatementLetter_u.push(item);
            }
        });

        //self.ddlStatementLetter_u(data['StatementLetter']);

        self.ddlRateType_u(data['RateType']);
    }

    //Function to Read All Customers Underlying
    function GetDataUnderlying() {

        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/UnderlyingWithFilterExpiredDate",
            params: {
                page: self.UnderlyingGridProperties().Page(),
                size: self.UnderlyingGridProperties().Size(),
                sort_column: self.UnderlyingGridProperties().SortColumn(),
                sort_order: self.UnderlyingGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        self.UnderlyingFilterIsAvailable(true);
        self.UnderlyingFilterIsExpiredDate(true);
        var filters = GetUnderlyingFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        }
    }

    //Function to Read All Customers Underlying File
    function GetDataAttachFile() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlyingfile,
            params: {
                page: self.AttachGridProperties().Page(),
                size: self.AttachGridProperties().Size(),
                sort_column: self.AttachGridProperties().SortColumn(),
                sort_order: self.AttachGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        }
    }
    //Function to Read All Customers Underlying for Attach File
    function GetDataUnderlyingAttach() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Attach",
            params: {
                page: self.UnderlyingAttachGridProperties().Page(),
                size: self.UnderlyingAttachGridProperties().Size(),
                sort_column: self.UnderlyingAttachGridProperties().SortColumn(),
                sort_order: self.UnderlyingAttachGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetUnderlyingAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingDataAttachFile, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingDataAttachFile, OnError, OnAlways);
        }
    }
    //Function to Read All Customers Underlying Proforma
    function GetDataUnderlyingProforma() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Proforma",
            params: {
                page: self.UnderlyingProformaGridProperties().Page(),
                size: self.UnderlyingProformaGridProperties().Size(),
                sort_column: self.UnderlyingProformaGridProperties().SortColumn(),
                sort_order: self.UnderlyingProformaGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetUnderlyingProformaFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingProforma, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingProforma, OnError, OnAlways);
        }
    }

    //Function to Read All Customers Bulk Underlying
    function GetDataBulkUnderlying() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Bulk",
            params: {
                page: self.UnderlyingBulkGridProperties().Page(),
                size: self.UnderlyingBulkGridProperties().Size(),
                sort_column: self.UnderlyingBulkGridProperties().SortColumn(),
                sort_order: self.UnderlyingBulkGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns

        var filters = GetBulkUnderlyingFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingBulk, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingBulk, OnError, OnAlways);
        }
    }

    function OnSuccessGetDataUnderlying(data, textStatus, jqXHR) {

        if (jqXHR.status == 200) {

            self.CustomerUnderlyings([]);
            self.CustomerUnderlyings(data.Rows);
            self.UnderlyingGridProperties().Page(data['Page']);
            self.UnderlyingGridProperties().Size(data['Size']);
            self.UnderlyingGridProperties().Total(data['Total']);
            self.UnderlyingGridProperties().TotalPages(Math.ceil(self.UnderlyingGridProperties().Total() / self.UnderlyingGridProperties().Size()));
            
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetUnderlyingDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            for (var i = 0 ; i < data.Rows.length; i++) {
                var selected = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (item) {
                    return item.ID == data.Rows[i].ID;
                });
                if (selected != null) {
                    data.Rows[i].IsSelectedAttach = true;
                }
                else {
                    data.Rows[i].IsSelectedAttach = false;
                }
            }
            self.CustomerAttachUnderlyings(data.Rows);

            self.UnderlyingAttachGridProperties().Page(data['Page']);
            self.UnderlyingAttachGridProperties().Size(data['Size']);
            self.UnderlyingAttachGridProperties().Total(data['Total']);
            self.UnderlyingAttachGridProperties().TotalPages(Math.ceil(self.UnderlyingAttachGridProperties().Total() / self.UnderlyingAttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerUnderlyingFiles(data.Rows);

            self.AttachGridProperties().Page(data['Page']);
            self.AttachGridProperties().Size(data['Size']);
            self.AttachGridProperties().Total(data['Total']);
            self.AttachGridProperties().TotalPages(Math.ceil(self.AttachGridProperties().Total() / self.AttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback // Add 2015.03.09
    function OnSuccessGetUnderlyingBulk(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            SetSelectedBulk(data.Rows);

            self.CustomerBulkUnderlyings(data.Rows);

            self.UnderlyingBulkGridProperties().Page(data['Page']);
            self.UnderlyingBulkGridProperties().Size(data['Size']);
            self.UnderlyingBulkGridProperties().Total(data['Total']);
            self.UnderlyingBulkGridProperties().TotalPages(Math.ceil(self.UnderlyingBulkGridProperties().Total() / self.UnderlyingBulkGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetUnderlyingProforma(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            SetSelectedProforma(data.Rows);

            self.CustomerUnderlyingProformas(data.Rows);

            self.UnderlyingProformaGridProperties().Page(data['Page']);
            self.UnderlyingProformaGridProperties().Size(data['Size']);
            self.UnderlyingProformaGridProperties().Total(data['Total']);
            self.UnderlyingProformaGridProperties().TotalPages(Math.ceil(self.UnderlyingProformaGridProperties().Total() / self.UnderlyingProformaGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // Get filtered columns value
    function GetUnderlyingFilteredColumns() {
        // define filter

        var filters = [];
        self.CIF_u(cifData);
        self.UnderlyingFilterIsTMO(true);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingFilterIsSelected() });
        if (self.UnderlyingFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingFilterUnderlyingDocument() });
        if (self.UnderlyingFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingFilterDocumentType() });
        if (self.UnderlyingFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingFilterCurrency() });
        if (self.UnderlyingFilterAvailableAmount() != "") filters.push({ Field: 'AvailableAmount', Value: self.UnderlyingFilterAvailableAmount() });
        if (self.UnderlyingFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingFilterStatementLetter() });
        if (self.UnderlyingFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingFilterAmount() });
        if (self.UnderlyingFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingFilterDateOfUnderlying() });
        if (self.UnderlyingFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingFilterExpiredDate() });
        if (self.UnderlyingFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingFilterReferenceNumber() });
        if (self.UnderlyingFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingFilterSupplierName() });
        if (self.UnderlyingFilterAttachmentNo() != "") filters.push({ Field: 'AttachmentNo', Value: self.UnderlyingFilterAttachmentNo() });
        if (self.UnderlyingFilterIsAvailable() != "") filters.push({ Field: 'IsAvailable', Value: self.UnderlyingFilterIsAvailable() });
        if (self.UnderlyingFilterIsExpiredDate() != "") filters.push({ Field: 'IsExpiredDate', Value: self.UnderlyingFilterIsExpiredDate() });
        if (self.UnderlyingFilterIsSelectedBulk() == false) filters.push({ Field: 'IsSelectedBulk', Value: self.UnderlyingFilterIsSelectedBulk() });
        if (self.UnderlyingFilterIsTMO() != "" && self.UnderlyingFilterIsTMO() != null) filters.push({ Field: 'IsTMO', Value: self.UnderlyingFilterIsTMO() });
        if (self.UnderlyingFilterAccountNumber() != "") filters.push({ Field: 'AccountNumber', Value: self.UnderlyingFilterAccountNumber() });

        filters.push({ Field: 'StatusShowData', Value: self.UnderlyingFilterShow() });
        return filters;
    };

    // Get filtered columns value
    function GetUnderlyingAttachFilteredColumns() {
        var filters = [];

        self.UnderlyingFilterIsSelected(true); // flag for get all datas
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingAttachFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingAttachFilterIsSelected() });
        if (self.UnderlyingAttachFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingAttachFilterUnderlyingDocument() });
        if (self.UnderlyingAttachFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingAttachFilterDocumentType() });
        if (self.UnderlyingAttachFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingAttachFilterCurrency() });
        if (self.UnderlyingAttachFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingAttachFilterStatementLetter() });
        if (self.UnderlyingAttachFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingAttachFilterAmount() });
        if (self.UnderlyingAttachFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingAttachFilterDateOfUnderlying() });
        if (self.UnderlyingAttachFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingAttachFilterExpiredDate() });
        if (self.UnderlyingAttachFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingAttachFilterReferenceNumber() });
        if (self.UnderlyingAttachFilterAvailableAmount() != "") filters.push({ Field: 'AvailableAmount', Value: self.UnderlyingAttachFilterAvailableAmount() });
        if (self.UnderlyingAttachFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingAttachFilterSupplierName() });

        return filters;
    }

    // Get filtered columns value
    function GetAttachFilteredColumns() {
        // define filter

        var filters = [];
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.AttachFilterFileName() != "") filters.push({ Field: 'FileName', Value: self.AttachFilterFileName() });
        if (self.AttachFilterDocumentPurpose() != "") filters.push({ Field: 'DocumentPurpose', Value: self.AttachFilterDocumentPurpose() });
        if (self.AttachFilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.AttachFilterModifiedDate() });
        if (self.AttachFilterDocumentRefNumber() != "") filters.push({ Field: 'DocumentRefNumber', Value: self.AttachFilterDocumentRefNumber() });
        if (self.AttachFilterAttachmentReference() != "") filters.push({ Field: 'AttachmentReference', Value: self.AttachFilterAttachmentReference() });

        return filters;
    };

    // Get filtered columns value
    function GetUnderlyingProformaFilteredColumns() {
        // define filter
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.ProformaFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.ProformaFilterIsSelected() });
        if (self.ProformaFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.ProformaFilterUnderlyingDocument() });
        if (self.ProformaFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.ProformaFilterDocumentType() });
        if (self.ProformaFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.ProformaFilterCurrency() });
        if (self.ProformaFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.ProformaFilterStatementLetter() });
        if (self.ProformaFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.ProformaFilterAmount() });
        if (self.ProformaFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.ProformaFilterDateOfUnderlying() });
        if (self.ProformaFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.ProformaFilterExpiredDate() });
        if (self.ProformaFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.ProformaFilterReferenceNumber() });
        if (self.ProformaFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.ProformaFilterSupplierName() });

        return filters;
    };

    // Get filtered columns value // add 2015.03.09
    function GetBulkUnderlyingFilteredColumns() {
        // define filter
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.BulkFilterCurrency(self.Currency_u().Code() != "" && self.Currency_u().Code() != null ? self.Currency_u().Code() : "xyz");
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: self.CIF_u() });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.BulkFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.BulkFilterIsSelected() });
        if (self.BulkFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.BulkFilterUnderlyingDocument() });
        if (self.BulkFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.BulkFilterDocumentType() });
        if (self.BulkFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.BulkFilterCurrency() });
        if (self.BulkFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.BulkFilterStatementLetter() });
        if (self.BulkFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.BulkFilterAmount() });
        if (self.BulkFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.BulkFilterDateOfUnderlying() });
        if (self.BulkFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.BulkFilterExpiredDate() });
        if (self.BulkFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.BulkFilterReferenceNumber() });
        if (self.BulkFilterInvoiceNumber() != "") filters.push({ Field: 'InvoiceNumber', Value: self.BulkFilterInvoiceNumber() });
        if (self.BulkFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.BulkFilterSupplierName() });

        return filters;
    };

    function GetDataTZ() {
        var options = {
            url: api.server + "api/TransactionDeal/TzInformation/" + self.CIFData(),
            params: {
                page: self.TZGridProperties().Page(),
                size: self.TZGridProperties().Size()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = [];

        if (filters.length > 0) {

            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataTZ, OnError, function () { });

        } else {
            Helper.Ajax.Get(options, OnSuccessGetDataTZ, OnError, function () { });
        }
    }

    function OnSuccessGetDataTZ(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.TZInformations(data.Rows);

            self.TZGridProperties().Page(data['Page']);
            self.TZGridProperties().Size(data['Size']);
            self.TZGridProperties().Total(data['Total']);
            self.TZGridProperties().TotalPages(Math.ceil(self.TZGridProperties().Total() / self.TZGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }
    self.OnCurrencyChange = function (CurrencyID) {
        if (CurrencyID != null) {
            var item = ko.utils.arrayFirst(self.ddlCurrency_u(), function (x) { return CurrencyID == x.ID(); });
            if (item != null) {
                self.Currency_u(new CurrencyModel2(item.ID(), item.Code(), item.Description()));
            }
            if (!self.IsNewDataUnderlying()) {
                GetSelectedBulkID(GetDataBulkUnderlying);
            } else {
                self.TempSelectedUnderlyingBulk([]);
                GetDataBulkUnderlying();
            }
        }
    }
    self.save_u = function () {
        self.IsEditTableUnderlying(false);
        var form = $("#frmUnderlying");
        form.validate();

        if (form.valid()) {
            if (CustomerUnderlying.StatementLetter().ID() == 2 && CustomerUnderlying.UnderlyingDocument().ID() == 36) {
                ShowNotification("Form Validation Warning", "Please Change Document Underlying other 998(tanpa underlying). ", 'gritter-warning', false);
                self.IsEditTableUnderlying(true);
                return false;
            }
            viewModel.Amount_u(viewModel.Amount_u().toString().replace(/,/g, ''));
            CustomerUnderlying.OtherUnderlying = self.CodeUnderlying() == '999' ? self.OtherUnderlying_u : '';
            SetMappingProformas();
            SetMappingBulks(); // add 2015.03.09
            CustomerUnderlying.AvailableAmountUSD = self.AmountUSD_u();
            //Ajax call to insert the CustomerUnderlyings     

            $.ajax({
                type: "POST",
                url: api.server + api.url.customerunderlying + "/Save",
                data: ko.toJSON(CustomerUnderlying), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        $("#modal-form-Underlying").modal('hide');
                        self.IsEditTableUnderlying(true);
                        // refresh data
                        //GetDataUnderlying();

                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                        self.IsEditTableUnderlying(true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    if (jqXHR.responseJSON.Message.toLowerCase().startsWith("double underlying")) {
                        ShowNotification("", jqXHR.responseJSON.Message, 'gritter-warning', true);
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    } self.IsEditTableUnderlying(true);
                }
            });
        } else {
            self.IsEditTableUnderlying(true);
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);

        }
    };

    function onSuccessRemainingBlnc() {
        CalculateFX();
    }

    self.save_a = function () {

        self.IsUploading(true);
        var doc = {
            ID: 0,
            FileName: Documents.DocumentPath() != null ? Documents.DocumentPath().name : null,
            DocumentPath: self.DocumentPath_a(),
            Type: ko.utils.arrayFirst(self.ddlDocumentType_a(), function (item) {
                return item.ID == self.DocumentType_a().ID();
            }),
            Purpose: ko.utils.arrayFirst(self.ddlDocumentPurpose_a(), function (item) {
                return item.ID == self.DocumentPurpose_a().ID();
            }),
            IsNewDocument: true,
            LastModifiedDate: new Date(),
            LastModifiedBy: null
        };
        if (doc.Type == null || doc.Purpose == null || doc.DocumentPath == null || doc.FileName == null) {
            ShowNotification("", "Please complete the upload form fields.", 'gritter-warning', true);
            self.IsUploading(false);
        } else {
            var FxWithUnderlying = (self.TempSelectedAttachUnderlying().length > 0);
            var FxWithNoUnderlying = (doc.Purpose.ID != 2);
            if (FxWithUnderlying || FxWithNoUnderlying) {
                var dataCIF = null;
                var dataName = null;
                dataCIF = viewModel.TransactionNetting().Customer().CIF;
                dataName = viewModel.TransactionNetting().Customer().Name;
                var data = {
                    CIF: dataCIF,
                    Name: dataName,
                    Type: ko.utils.arrayFirst(self.ddlDocumentType_a(), function (item) {
                        return item.ID == self.DocumentType_a().ID();
                    }),
                    Purpose: ko.utils.arrayFirst(self.ddlDocumentPurpose_a(), function (item) {
                        return item.ID == self.DocumentPurpose_a().ID();
                    })
                };
                if (FxWithUnderlying && data.Purpose.ID == 2) {
                    UploadFileUnderlying(data, doc, SaveDataFile);
                }
                if (FxWithNoUnderlying) {
                    self.CustomerDocuments.push(doc);
                }
                $("#modal-form-Attach").modal('hide');
                $("#modal-form-Attach1").modal('hide');
                $('#backDrop').hide();
            } else {
                ShowNotification("", "Please select underlying form the table.", 'gritter-warning', true);
                self.IsUploading(false);
            }
        }
    };

    function SaveDataFile() {
        var transactionID
        SetMappingAttachment(viewModel.TransactionNetting().Customer.Name);
        transactionID = viewModel.TransactionNetting().ID();

        CustomerUnderlyingFile.ID = transactionID;
        CustomerUnderlyingFile.FileName = DocumentModels().FileName;
        CustomerUnderlyingFile.DocumentPath = DocumentModels().DocumentPath;
        CustomerUnderlyingFile.DocumentType = ko.utils.arrayFirst(self.ddlDocumentType_a(), function (item) {
            return item.ID == self.DocumentType_a().ID();
        });
        CustomerUnderlyingFile.DocumentPurpose = ko.utils.arrayFirst(self.ddlDocumentPurpose_a(), function (item) {
            return item.ID == self.DocumentPurpose_a().ID();
        });
        $.ajax({
            type: "POST",
            //url: api.server + api.url.customerunderlyingfile,
            url: api.server + api.url.customerunderlyingfile + "/AddFile",
            data: ko.toJSON(CustomerUnderlyingFile), //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    viewModel.IsEditable(true);
                    // send notification
                    ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                    // hide current popup window
                    $("#modal-form-Attach").modal('hide');
                    $('#backDrop').hide();
                    viewModel.IsUploading(false);
                    viewModel.IsUploading(false);
                    var doc = {
                        ID: data,
                        Type: CustomerUnderlyingFile.DocumentType,
                        Purpose: CustomerUnderlyingFile.DocumentPurpose,
                        FileName: CustomerUnderlyingFile.FileName,
                        DocumentPath: CustomerUnderlyingFile.DocumentPath,
                        IsNewDocument: true,
                        LastModifiedDate: new Date(),
                        LastModifiedBy: null
                    };
                    self.CustomerDocuments.push(doc);
                    //self.GetDataUnderlying();
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    viewModel.IsEditable(true);
                    viewModel.IsUploading(false);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                viewModel.IsEditable(true);
                viewModel.IsUploading(false);
            }
        });
    }

    self.update_u = function () {
        var form = $("#frmUnderlying");
        form.validate();

        if (form.valid()) {
            viewModel.Amount_u(viewModel.Amount_u().toString().replace(/,/g, ''));
            // hide current popup window
            $("#modal-form-Underlying").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form-Underlying").modal('show');
                } else {
                    CustomerUnderlying.OtherUnderlying = self.CodeUnderlying() == '999' ? self.OtherUnderlying_u : '';
                    SetMappingProformas();
                    SetMappingBulks(); // add 2015.03.09
                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.customerunderlying + "/" + CustomerUnderlying.ID(),
                        data: ko.toJSON(CustomerUnderlying),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {

                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                $("#modal-form-Underlying").modal('hide');

                                // refresh data
                                //GetDataUnderlying();

                                //new calculated
                                DealModel.cif = cifData;
                                GetRemainingBalance(DealModel, onSuccessRemainingBlnc, OnErrorDeal);
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            if (jqXHR.responseJSON.Message.toLowerCase().startsWith("double underlying")) {
                                ShowNotification("", jqXHR.responseJSON.Message, 'gritter-warning', true);
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                            }
                        }
                    });
                }
            });
        }
    };

    self.delete_u = function () {
        // hide current popup window
        $("#modal-form-Underlying").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form-Underlying").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customerunderlying + "/" + CustomerUnderlying.ID(),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {

                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                            // refresh data
                            //GetDataUnderlying();

                            //new calculated
                            DealModel.cif = cifData;
                            GetRemainingBalance(DealModel, onSuccessRemainingBlnc, OnErrorDeal);

                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    self.delete_a = function (item) {

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                //$("#modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customerunderlyingfile + "/" + item.ID,
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            DeleteFile(item.DocumentPath);
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetDataAttachFile();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    }

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);
        if (date == '1970/01/01 00:00:00') {
            return "";
        }

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };
    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }



};

var viewModel = new ViewModel();

$.holdReady(true);
var intervalRoleName = setInterval(function () {
    if (Object.keys(Const_RoleName).length != 0) {
        $.holdReady(false);
        clearInterval(intervalRoleName);
    }
}, 100);

// jQuery Init
//$(document).on("RoleNameReady", function() {
$(document).ready(function () {
    // Token Validation
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(OnSuccessToken, OnError);
    } else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);
            viewModel.SPUser(ko.mapping.toJS(spUser));
            viewModel.isDealBUGroup(true);
            $.ajax({
                type: "GET",
                url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + $.cookie(api.cookie.name)
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        viewModel.isDealBUGroup(false);
                        for (i = 0; i < spUser.Roles.length; i++) {
                            for (j = 0; j < data.length; j++) {
                                if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {

                                    if (Const_RoleName[spUser.Roles[i].ID].toLowerCase().startsWith("dbs fx") && Const_RoleName[spUser.Roles[i].ID].toLowerCase().endsWith("maker")) { // develop //if (spUser.Roles[i].Name.toLowerCase().startsWith("dbs fx") && spUser.Roles[i].Name.toLowerCase().endsWith("maker")) { // develop
                                        viewModel.isDealBUGroup(true);
                                    }

                                }
                            }
                        }
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }
        // call get data inside view model
        DealModel.token = accessToken;
        GetThresholdParameter(DealModel, OnSuccessThresholdPrm, OnErrorDeal);
        GetParameters();
        GetNettingPurpose();


    }
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        focusCleanup: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });
    $('.date-picker').datepicker({
        autoclose: true,
        onSelect: function () {
            this.focus();
        }
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    // Knockout Datepicker custom binding handler
    ko.bindingHandlers.datepicker = {
        init: function (element) {
            $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).datepicker("refresh");
        }
    };
    // ace file upload
    $('#document-path').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        thumbnail: false, //| true | large
        blacklist: 'exe|dll'
    });

    $('#document-path-upload').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });

    // Knockout custom file handler
    ko.bindingHandlers.file = {
        init: function (element, valueAccessor) {
            $(element).change(function () {
                var file = this.files[0];

                if (ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };

    // Dependant Observable for dropdown auto fill
    ko.dependentObservable(function () {
        var account = ko.utils.arrayFirst(viewModel.TransactionNetting().Customer().Accounts, function (item) { return item.AccountNumber == viewModel.Selected().Account(); });

        if (viewModel.Selected().Account() == '-') {
            viewModel.IsEmptyAccountNumber(true);
        }
        else {
            if (account != null) {
                viewModel.TransactionNetting().DebitCurrency(account.Currency);
            }
        }

        var currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().Currency(); });
        var debitcurrency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().DebitCurrency(); });
        var docPurpose = ko.utils.arrayFirst(viewModel.Parameter().DocumentPurposes(), function (item) { return item.ID == viewModel.Selected().DocumentPurpose(); });
        var rateType = ko.utils.arrayFirst(viewModel.Parameter().RateType(), function (item) { return item.ID == viewModel.Selected().RateType(); });
        var statementLetter = ko.utils.arrayFirst(viewModel.Parameter().StatementLetter(), function (item) { return item.ID == viewModel.Selected().StatementLetter(); });
        var productType = ko.utils.arrayFirst(viewModel.Parameter().ProductType(), function (item) { return item.ID == viewModel.Selected().ProductType(); });
        var docType = ko.utils.arrayFirst(viewModel.Parameter().DocumentTypes(), function (item) { return item.ID == viewModel.Selected().DocumentType(); });
        var nettingPurpose = ko.utils.arrayFirst(viewModel.Parameter().NettingPurpose(), function (item) { return item.ID == viewModel.Selected().NettingPurpose(); });

        if (viewModel.DocumentPurpose_a() != null) {
            if (viewModel.DocumentPurpose_a().ID() == 2) {
                viewModel.SelectingUnderlying(true);
            }
            else {
                viewModel.SelectingUnderlying(false);
            }
        }

        if (debitcurrency != null) {
            viewModel.TransactionNetting().DebitCurrency(debitcurrency);

        }

        if (productType != null) {
            viewModel.TransactionNetting().ProductType(productType);
        }
        // if currency is selected
        if (currency != null) {

            if (debitcurrency != null) {
                viewModel.TransactionNetting().DebitCurrency(debitcurrency);
            }
            viewModel.TransactionNetting().Currency(currency);

            if (currency.ID != viewModel.beforeValueCurrID()) {
                GetRateAmount(currency.ID);
                viewModel.beforeValueCurrID(currency.ID);
            }

            SetProductType();
        }



        if (account != null) {
            viewModel.TransactionNetting().DebitCurrency(account.Currency);
            viewModel.TransactionNetting().Account(account);
            //calculate();
            SetProductType();
        }
        if (docType != null) viewModel.DocumentType(docType);
        if (docPurpose != null) viewModel.DocumentPurpose(docPurpose);
        if (rateType != null) viewModel.TransactionNetting().RateType(rateType);
        if (statementLetter != null) {
            viewModel.TransactionNetting().StatementLetter(statementLetter);

            if (viewModel.TransactionNetting().StatementLetter() != undefined) {
                if (viewModel.TransactionNetting().StatementLetter().ID == 1) {
                    viewModel.TransactionNetting().bookunderlyingcode(36);
                }
                else {
                    //viewModel.TransactionNetting().bookunderlyingcode('');
                }
            }

            //calculate();
        }
        if (nettingPurpose != null) {
            viewModel.TransactionNetting().NettingPurpose(nettingPurpose);
        }

    });

    $('#aspnetForm').after('<form id="frmUnderlying"></form>');
    $("#modal-form-Underlying").appendTo("#frmUnderlying");
    $("#modal-form-Attach").appendTo("#frmUnderlying");

    $('#frmUnderlying').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        focusCleanup: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    var customerAutocomplete = $("#customer-name");
    var dealnumberAutocomplete = $("#tz-reference");
    var nettingdealnumberAutocomplete = $("#netting-tz-reference");
    var CIFCustomer = viewModel.CIFData();
    GetCustomerAutocompleted(customerAutocomplete);
    GetDealNumberAutocompleted(dealnumberAutocomplete, viewModel.CIFData());
    GetNettingDealNumberAutocompleted(nettingdealnumberAutocomplete, viewModel.CIFData());

    viewModel.TransactionNetting().Product().ID(ConsProductID.FXNettingProductIDCons);
    // Knockout Bindings
    ko.applyBindings(viewModel);


});
function GetRateAmount(CurrencyID) {
    var options = {
        url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
        params: {
        },
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetRateAmount, OnError, OnAlways);
}
function OnSuccessGetApplicationID(data, textStatus, jqXHR) {
    // set ApplicationID
    if (data != null) {
        viewModel.TransactionNetting().ApplicationID(data.ApplicationID);
    }
}
function SetProductType() {
    if (viewModel.t_IsFxTransactionToIDR()) {
        var productType = ko.utils.arrayFirst(viewModel.Parameter().ProductType(), function (item) { return item.ID == 0; }); // ID 0 = null product type
        if (productType != null) {
            viewModel.TransactionNetting().ProductType(productType);
        }
    }
}
function OnSuccessGetRateAmount(data, textStatus, jqXHR) {

    if (jqXHR.status = 200) {
        // bind result to observable array
        viewModel.TransactionNetting().Rate(data.RupiahRate);
        viewModel.Rate(data.RupiahRate);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function read_u() {
    var e = document.getElementById("Amount_u").value;
    e = e.toString().replace(/,/g, '');
    var res = parseInt(e) * parseFloat(viewModel.Rate_u()) / parseFloat(idrrate);
    res = Math.round(res * 100) / 100;
    res = isNaN(res) ? 0 : res; //avoid NaN
    viewModel.AmountUSD_u(parseFloat(res).toFixed(2));
    viewModel.Amount_u(formatNumber(e));
}

function SaveTransaction() {

    if (viewModel.TransactionNetting().underlyings == null) {
        viewModel.TransactionNetting().underlyings = ko.observableArray([]);
    } else {
        viewModel.TransactionNetting().underlyings([]);
    }
    var options = {
        url: api.server + "api/NewTransactionNetting",
        token: accessToken,
        data: ko.toJSON(viewModel.TransactionNetting())
    };

    var obj = ko.toJS(viewModel.TransactionNetting());
    if (countResult.CustomerName === 0 || countResult.DealNumber === 0 || countResult.NettingDealNumber === 0) {
        ShowNotification("Warning", "Customer Name, Deal Number, and Netting Deal Number Fields can not input manually, you have to choose from autocomplete", "gritter-warning", true);
    } else {
        Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
    }

}

function AddListItem() {
    var body = {
        Title: viewModel.TransactionNetting().ApplicationID(),
        IsNetting: true,
        __metadata: {
            type: config.sharepoint.metadata.listDeal
        }
    };

    //console.log('body: ' + body);

    var options = {
        url: config.sharepoint.url.api + "/Lists(guid'" + config.sharepoint.listIdDeal + "')/Items",
        data: JSON.stringify(body),
        digest: jQuery("#__REQUESTDIGEST").val()
    };

    //console.log('option: ' + options);

    Helper.Sharepoint.List.Add(options, OnSuccessAddListItem, OnError, OnAlways);
}

function ValidateTransaction() {
    UploadDocuments();
}

function UploadDocuments() {
    // uploading documents. after upload completed, see SaveTransaction() 
    var data = {
        ApplicationID: viewModel.TransactionNetting().ApplicationID(),
        CIF: viewModel.TransactionNetting().Customer().CIF,
        Name: viewModel.TransactionNetting().Customer().Name
    };

    // upload hanya document selain underlying
    var items = ko.utils.arrayFilter(viewModel.CustomerDocuments(), function (item) {
        return item.Purpose.ID != 2 && item.IsNewDocument == true;
    });

    ko.utils.arrayForEach(viewModel.CustomerDocuments(), function (item) {
        viewModel.NettingDocuments().push(item);
    });

    if (items != null && items.length > 0) {
        //for (var i = 0; i < items.length; i++) {
        UploadFileRecuresive(data, items, SaveTransaction, items.length);
        //}
    }
    else {
        SaveTransaction();
    }
}

// Event handlers declaration start
function OnSuccessUpload(data, textStatus, jqXHR) {
    ShowNotification("Upload Success", JSON.stringify(data), "gritter-success", true);
}

function OnSuccessGetParameters(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        // bind result to observable array
        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };

        viewModel.Parameter().Currencies(ko.mapping.toJS(data.Currency, mapping));
        viewModel.Parameter().ProductType(ko.mapping.toJS(data.ProductType, mapping));
        viewModel.Parameter().FXCompliances(ko.mapping.toJS(data.FXCompliance, mapping));
        viewModel.Parameter().DocumentTypes(ko.mapping.toJS(data.DocType, mapping));
        viewModel.Parameter().DocumentPurposes(ko.mapping.toJS(data.PurposeDoc, mapping));
        viewModel.Parameter().RateType(ko.mapping.toJS(data.RateType, mapping));
        viewModel.Parameter().StatementLetter(ko.mapping.toJS(data.StatementLetter, mapping));

        // underlying parameter
        viewModel.GetUnderlyingParameters(data);
        LoadDraft();
        // enabling form input controls
        viewModel.IsEditable(true);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function OnSuccessUpdateUnderlying(data, textStatus, jqXHR) {
    if (jqXHR.status != 200) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-success', true);
    }
}

function OnSuccessAddListItem(data, textStatus, jqXHR) {
    viewModel.IsEditable(false);
    viewModel.IsEnableSubmit(true);
    var AppIDColl = viewModel.RetIDColl();
    var Cust;
    if (AppIDColl != null) {
        Cust = viewModel.TransactionNetting().Customer();
        viewModel.TransactionNetting().ApplicationID(AppIDColl[0].ApplicationID);
        viewModel.TransactionNetting().ID(AppIDColl[0].TransactionID);

        viewModel.ApplicationIDColl([]);
        var appColl = {
            TransactionID: AppIDColl[0].TransactionID,
            ApplicationID: AppIDColl[0].ApplicationID,
            Customer: Cust,
        }

        viewModel.ApplicationIDColl.push(appColl);
        ShowNotification("Book Success", JSON.stringify(data.Message), "gritter-success", true);
        $("#modal-form-applicationID").modal('show');

    }

}

function OnSuccessSaveAPI(data, textStatus, jqXHR) {
    // Get transaction id from api result
    var AppID = {};
    var DraftID;

    if (data.ID != null || data.ID != undefined) {
        viewModel.RetIDColl([]);
        AppID = {
            TransactionID: data.ID,
            ApplicationID: data.AppID
        };
        viewModel.RetIDColl.push(AppID);
        viewModel.TransactionNetting().ID(data.ID);
        viewModel.TransactionNetting().ApplicationID(data.AppID);
        if (viewModel.TransactionNetting().IsDraft() == false) {
            // insert to sharepoint list item
            if (viewModel.ID() > 0) {
                DeleteDraft(viewModel.ID());
            }
            AddListItem();

        }
        else {
            //viewModel.isEditable(false);
            ShowNotification("Transaction Draft Success", "Transaction has been saved as a draft", "gritter-warning", true);
            window.location = "/home/draft-transactions";
        }
    }
}
function DeleteDraft(id) {
    var apiURI = '';
    apiURI = api.server + "api/TransactionNetting/Draft/Delete/" + id;
    $.ajax({
        type: "DELETE",
        url: apiURI,
        headers: {
            "Authorization": "Bearer " + accessToken
        },
        success: function (data, textStatus, jqXHR) {
            if (jqXHR.status = 200) {
                CreateList();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    });
}
function CreateList() {
    var body = {
        Title: viewModel.TransactionNetting().ApplicationID(),
        IsNetting: true,
        __metadata: {
            type: config.sharepoint.metadata.listDeal
        }
    };

    //console.log('body: ' + body);

    var options = {
        url: config.sharepoint.url.api + "/Lists(guid'" + config.sharepoint.listIdDeal + "')/Items",
        data: JSON.stringify(body),
        digest: jQuery("#__REQUESTDIGEST").val()
    };

    //console.log('option: ' + options);

    Helper.Sharepoint.List.Add(options, OnSuccessCreateListItem, OnError, OnAlways);


}
function OnSuccessCreateListItem(data, textStatus, jqXHR) {
    window.location = "/home";

}

function GetCustomerAutocompleted(element) {
    // autocomplete
    element.autocomplete({
        source: function (request, response) {
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.customer + "/Search",
                data: {
                    query: request.term,
                    limit: 20
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessCustomerAutoComplete, OnError);
        },
        minLength: 2,
        select: function (event, ui) {
            // set customer data model
            if (ui.item.data != undefined || ui.item.data != null) {

                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                viewModel.CIFData(ui.item.data.CIF);
                viewModel.TransactionNetting().Customer(ko.mapping.toJS(ui.item.data, mapping));
                viewModel.TransactionNetting().TZReference('');
                viewModel.TransactionNetting().NettingTZReference('');
                cifData = ui.item.data.CIF;
                DealModel.cif = cifData;
                customerNameData = ui.item.data.Name;
                viewModel.IsCustomerAvailable(true);

            }
            else {
                viewModel.TransactionNetting().Customer(null);
            }
        },
        response: function (event, ui) {
            if (ui.content.length === 0) {
                countResult.CustomerName = 0;
            } else {
                countResult.CustomerName = 1;
            }
        }

    });
}

// Autocomplete
function OnSuccessCustomerAutoComplete(response, data, textStatus, jqXHR) {

    response($.map(data, function (item) {

        return {
            // default autocomplete object
            label: item.CIF + " - " + item.Name,
            value: item.Name,

            // custom object binding
            data: {
                CIF: item.CIF,
                Name: item.Name,
                POAName: item.POAName,
                Accounts: item.Accounts,
                Branch: item.Branch,
                Contacts: item.Contacts,
                Functions: item.Functions,
                RM: item.RM,
                Type: item.Type,
                Underlyings: item.Underlyings,
                LastModifiedBy: item.LastModifiedBy,
                LastModifiedDate: item.LastModifiedDate
            }

        }

    })
    );
}

function GetDealNumberAutocompleted(element, cif) {
    // autocomplete
    element.autocomplete({
        source: function (request, response) {
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.helper + "/TZRefTMO/Complete/Search",
                data: {
                    query: request.term,
                    limit: 20,
                    cif: viewModel.CIFData() != null ? viewModel.CIFData() : ""
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessDealNumberAutoComplete, OnError);
        },
        minLength: 2,
        select: function (event, ui) {
            // set customer data model
            if (ui.item.data != undefined || ui.item.data != null) {
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                viewModel.TransactionNetting().TZReference(null);
                viewModel.TransactionNetting().TZReference(ko.mapping.toJS(ui.item.data, mapping));
                viewModel.TransactionNetting().IDTZReference(ko.mapping.toJS(ui.item.data.ID, mapping));
                viewModel.TransactionNetting().MurexNumber(ui.item.data.MurexNumber);
                if (ui.item.data.MurexNumber != null) {
                    var MurexNumber = ui.item.data.MurexNumber;
                    MurexNumber = MurexNumber.replace(/#/g, '@');
                }

                if (ui.item.data.ExpectedDateStatementLetter != null) {// || ui.item.data.ExpectedDateStatementLetter.substring(0, 4) != UnvalidYear) {
                    viewModel.TransactionNetting().ExpectedDateStatementLetter(viewModel.LocalDate(ui.item.data.ExpectedDateStatementLetter, true, false));
                } else {
                    viewModel.TransactionNetting().ExpectedDateStatementLetter(null);
                }

                if (ui.item.data.ExpectedDateUnderlying != null) {// || ui.item.data.ExpectedDateUnderlying.substring(0, 4) != UnvalidYear) {
                    viewModel.TransactionNetting().ExpectedDateUnderlying(viewModel.LocalDate(ui.item.data.ExpectedDateUnderlying, true, false));
                } else {
                    viewModel.TransactionNetting().ExpectedDateUnderlying(null);
                }

                if (ui.item.data.ActualDateStatementLetter != null) {// || ui.item.data.ActualDateStatementLetter.substring(0, 4) != UnvalidYear) {
                    viewModel.TransactionNetting().ActualDateStatementLetter(viewModel.LocalDate(ui.item.data.ActualDateStatementLetter, true, false));
                } else {
                    viewModel.TransactionNetting().ActualDateStatementLetter(null);
                }

                if (ui.item.data.ActualDateUnderlying != null) {// || ui.item.data.ActualDateUnderlying.substring(0, 4) != UnvalidYear) {
                    viewModel.TransactionNetting().ActualDateUnderlying(viewModel.LocalDate(ui.item.data.ActualDateUnderlying, true, false));
                } else {
                    viewModel.TransactionNetting().ActualDateUnderlying(null);
                }
                if (ui.item.data.TZReference != '') {
                    if (ui.item.data.TZReference.indexOf('#') > 0) {
                        var TZMurexNumber = ui.item.data.TZReference;
                        TZMurexNumber = TZMurexNumber.replace(/#/g, '@');
                        viewModel.TmpTzNumber(TZMurexNumber);
                    } else {
                        viewModel.TmpTzNumber(ui.item.data.TZReference);
                    }
                    viewModel.MurexNumber(MurexNumber);
                } else {
                    viewModel.TmpTzNumber('');
                    viewModel.MurexNumber(MurexNumber);
                }
                viewModel.SelectedTZ();
            }
            else {
                viewModel.TransactionNetting().TZReference(null);
                viewModel.TransactionNetting().IDTZReference(null);
                viewModel.TransactionNetting().MurexNumber(null);
                viewModel.TransactionNetting().ExpectedDateStatementLetter(null);
                viewModel.TransactionNetting().ExpectedDateUnderlying(null);
                viewModel.TransactionNetting().ActualDateStatementLetter(null);
                viewModel.TransactionNetting().ActualDateUnderlying(null);
                viewModel.TmpTzNumber(null);
            }
        },
        response: function (event, ui) {
            if (ui.content.length === 0) {
                countResult.DealNumber = 0;
            } else {
                countResult.DealNumber = 1;
            }
        }
    });
}

// Autocomplete
function OnSuccessDealNumberAutoComplete(response, data, textStatus, jqXHR) {

    response($.map(data, function (item) {

        return {
            // default autocomplete object
            label: item.TZReference != '' ? item.TZReference + " - " + item.CustomerName + "(" + item.CIF + ")" : item.MurexNumber + " - " + item.CustomerName + "(" + item.CIF + ")",
            value: item.TZReference != '' ? item.TZReference : item.MurexNumber,

            // custom object binding
            data: {
                ID: item.ID,
                CIF: item.CIF,
                Name: item.CustomerName,
                TZReference: item.TZReference,
                MurexNumber: item.MurexNumber,
                ExpectedDateStatementLetter: item.ExpectedDateStatementLetter,
                ExpectedDateUnderlying: item.ExpectedDateUnderlying,
                ActualDateStatementLetter: item.ActualDateStatementLetter,
                ActualDateUnderlying: item.ActualDateUnderlying
            }

        }

    })
    );
}
function GetNettingDealNumberAutocompleted(element, cif) {
    // autocomplete
    element.autocomplete({
        source: function (request, response) {
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.helper + "/TZRef/Incomplete/Search",
                data: {
                    query: request.term,
                    limit: 20,
                    cif: viewModel.CIFData() != null ? viewModel.CIFData() : ""
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessNettingDealNumberAutoComplete, OnError);
        },
        minLength: 2,
        select: function (event, ui) {
            // set customer data model
            if (ui.item.data != undefined || ui.item.data != null) {
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                viewModel.TransactionNetting().NettingTZReference(null);
                viewModel.TransactionNetting().NettingTZReference(ko.mapping.toJS(ui.item.data, mapping));
                viewModel.TransactionNetting().IDNettingTZReference(ko.mapping.toJS(ui.item.data.ID, mapping));
                viewModel.TransactionNetting().SwapDealNumber(ui.item.data.SwapDealNumber);
                viewModel.TmpTzNumber(ui.item.data.TZReference);
            }
            else {
                viewModel.TransactionNetting().NettingTZReference(null);
                viewModel.TransactionNetting().IDNettingTZReference(null);
                viewModel.TransactionNetting().SwapDealNumber(null);
                viewModel.TmpTzNumber(null);
            }
        },
        response: function (event, ui) {
            if (ui.content.length === 0) {
                countResult.NettingDealNumber = 0;
            } else {
                countResult.NettingDealNumber = 1;
            }
        }

    });
}

// Autocomplete
function OnSuccessNettingDealNumberAutoComplete(response, data, textStatus, jqXHR) {

    response($.map(data, function (item) {

        return {
            // default autocomplete object
            label: item.TZReference + " - " + item.CustomerName + "(" + item.CIF + ")",
            value: item.TZReference,

            // custom object binding
            data: {
                ID: item.ID,
                CIF: item.CIF,
                Name: item.CustomerName,
                TZReference: item.TZReference,
                SwapDealNumber: item.SwapDealNumber
            }

        }

    })
    );
}
function GetNettingDealNumberDraft(element, cif) {
    // autocomplete
    element.autocomplete({
        source: function (request, response) {
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.helper + "/TZRef/Incomplete/Search",
                data: {
                    query: request.term,
                    limit: 20,
                    cif: viewModel.CIFData() != null ? viewModel.CIFData() : ""
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessNettingDealNumberDraft, OnError);
        },
        minLength: 2,
        select: function (event, ui) {
            // set customer data model
            if (ui.item.data != undefined || ui.item.data != null) {
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                viewModel.TransactionNetting().NettingTZReference(null);
                viewModel.TransactionNetting().NettingTZReference(ko.mapping.toJS(ui.item.data, mapping));
                viewModel.TransactionNetting().IDNettingTZReference(ko.mapping.toJS(ui.item.data.ID, mapping));
                viewModel.TransactionNetting().SwapDealNumber(ui.item.data.SwapDealNumber);
                viewModel.TmpTzNumber(ui.item.data.TZReference);
                viewModel.SelectedTZ();
            }
            else {
                viewModel.TransactionNetting().NettingTZReference(null);
                viewModel.TransactionNetting().IDNettingTZReference(null);
                viewModel.TransactionNetting().SwapDealNumber(null);
                viewModel.TmpTzNumber(null);
            }
        }
    });
}

// Autocomplete
function OnSuccessNettingDealNumberDraft(response, data, textStatus, jqXHR) {

    response($.map(data, function (item) {

        return {
            // default autocomplete object
            label: item.TZReference + " - " + item.CustomerName + "(" + item.CIF + ")",
            value: item.TZReference,

            // custom object binding
            data: {
                ID: item.ID,
                CIF: item.CIF,
                Name: item.CustomerName,
                TZReference: item.TZReference,
                SwapDealNumber: item.SwapDealNumber
            }

        }

    })
    );
}

function GetNettingPurpose() {
    $.ajax({
        async: false,
        type: "GET",
        url: api.server + api.url.parametersystem + "/partype/" + ConsPARSYS.tmoNettingPurpose,
        contentType: "application/json; charset=utf-8; odata=verbose;",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + accessToken
        },
        success: function (data, textStatus, jqXHR) {
            viewModel.Parameter().NettingPurpose(ko.mapping.toJS(data.Parsys));
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });

}

function OnSuccessGetNettingPurpose(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        // bind result to observable array
        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };

        viewModel.Parameter().Currencies(ko.mapping.toJS(data.Currency, mapping));
        viewModel.Parameter().ProductType(ko.mapping.toJS(data.ProductType, mapping));
        viewModel.Parameter().DocumentTypes(ko.mapping.toJS(data.DocType, mapping));
        viewModel.Parameter().DocumentPurposes(ko.mapping.toJS(data.PurposeDoc, mapping));
        viewModel.Parameter().StatementLetter(ko.mapping.toJS(data.StatementLetter, mapping));


    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}
// get all parameters need
function GetParameters() {
    var options = {
        url: api.server + api.url.parameter,
        params: {
            select: "Currency,AgentCharge,FXCompliance,ChargesType,DocType,PurposeDoc,statementletter,underlyingdoc,customer,ratetype,producttype"
        },
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
}
function LoadDraft() {
    var uri = '';
    ar = window.location.hash.split('#');
    if (ar.length < 2) {
        viewModel.IsDraftForm(false);
        return;
    }
    viewModel.IsDraftForm(true);
    uri = api.server + 'api/TransactionNetting/Draft/' + ar[1];
    var options = {
        url: uri,
        token: accessToken
    };
    Helper.Ajax.Get(options, OnSuccessLoadDraft, OnError, OnAlways);


}
function OnSuccessLoadDraft(data, textStatus, jqXHR) {
    if (jqXHR.status == 200) {
        viewModel.IsLoadDraft(true);
        if (data == null) return;
        LoadDraftNetting(data);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
    }
}
function LoadDraftNetting(data) {
    var mapping = {

    };
    /*if (data.SelectedUnderlyings != null){	 	
    	ko.utils.arrayForEach(data.SelectedUnderlyings, function (item) {    		
       		viewModel.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, true, item.AvailableAmount,item.StatementLetter.ID));       		
		});    	
    }*/
    //console.log(data);
    countResult.CustomerName = 1;
    countResult.DealNumber = 1;
    countResult.NettingDealNumber = 1;

    viewModel.TransactionNetting().Customer(data.Customer);
    viewModel.TransactionNetting().ID(data.ID);
    viewModel.ID(data.ID);
    if (data.TZReference.TZReference != '') {
        viewModel.TransactionNetting().TZReference(data.TZReference.TZReference);
    } else {
        viewModel.TransactionNetting().TZReference(data.MurexNumber);
    }
    viewModel.TZReferenceTemp(data.TZReference);
    viewModel.TransactionNetting().NettingTZReference(data.NettingTZReference.TZReference);
    viewModel.NettingTZReferenceTemp(data.NettingTZReference);
    viewModel.TransactionNetting().MurexNumber(data.MurexNumber);
    viewModel.TransactionNetting().SwapDealNumber(data.SwapDealNumber);
    viewModel.TransactionNetting().AccountNumber(undefined);
    viewModel.TransactionNetting().ApplicationID(undefined);
    viewModel.TransactionNetting().StatementLetter().ID(data.StatementLetter.ID);
    viewModel.TransactionNetting().ExpectedDateStatementLetter(viewModel.LocalDate(data.ExpectedDateStatementLetter, true, false));
    viewModel.TransactionNetting().ExpectedDateUnderlying(viewModel.LocalDate(data.ExpectedDateUnderlying, true, false));
    viewModel.TransactionNetting().ActualDateStatementLetter(viewModel.LocalDate(data.ActualDateStatementLetter, true, false));
    viewModel.TransactionNetting().ActualDateUnderlying(viewModel.LocalDate(data.ActualDateUnderlying, true, false));
    viewModel.TransactionNetting().utilizationAmount(data.utilizationAmount);
    viewModel.TransactionNetting().Remarks(data.Remarks);
    viewModel.Selected().NettingPurpose(data.NettingPurpose.ID);
    viewModel.IsTZAvailable(true);

    if (data.Customer.Underlyings != null) {
        $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
            $(this).prev().focus();
        });

        viewModel.CustomerDocuments([]);
        viewModel.CustomerAllDocumentsTemp([]);
        viewModel.TransactionNetting().Documents([]);

        ko.utils.arrayForEach(data.Customer.Underlyings, function (item) {//for(var i=0;data.Transaction.Customer.Underlyings.length>i;i++){                        
            viewModel.NettingUnderlyings.push(item.ID);
            viewModel.CustomerUnderlyings(data.Customer.Underlyings);

        });

    }
    if (data.Documents != null) {
        ko.utils.arrayForEach(data.Documents, function (item) {
            item.IsNewDocument = false;
            viewModel.NettingAllDocumentsTemp.push(item);
            viewModel.CustomerDocuments.push(item);
            viewModel.TransactionNetting().Documents.push(item);
        });
    }

    var t_IsFxTransaction = (data.Currency.Code != 'IDR' && data.AccountCurrencyCode == 'IDR');
    var t_IsFxTransactionToIDR = (data.Currency.Code == 'IDR' && data.AccountCurrencyCode != 'IDR');

    viewModel.t_IsFxTransaction(t_IsFxTransaction);
    viewModel.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);

    if (viewModel.t_IsFxTransaction() || viewModel.t_IsFxTransactionToIDR()) {

        GetTotalTransaction(data);// For Get Total IDR - FCY & Utilization
    }

}
function GetTotalTransaction(transaction) {
    if (viewModel != null) {
        var paramhreshold = GetValueParamterThreshold(transaction);
        var options = {
            url: api.server + api.url.helper + "/GetTotalTransactionIDRFCY",
            token: accessToken,
            params: {},
            data: ko.toJSON(paramhreshold)
        };
        Helper.Ajax.Post(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                TotalPPUModel.Total.IDRFCYPPU = data.TotalTransaction;
                TotalDealModel.Total.IDRFCYDeal = data.TotalTransaction;
                viewModel.TotalTransFX(data.TotalTransaction);

                TotalPPUModel.Total.UtilizationPPU = data.TotalUtilizationAmount;
                TotalDealModel.Total.UtilizationDeal = data.TotalUtilizationAmount;
                TotalDealModel.Total.RemainingBalance = data.AvailableUnderlyingAmount;
                viewModel.Rounding(data.RoundingValue);
                viewModel.ThresholdType(data.ThresholdType);
                viewModel.ThresholdValue(data.ThresholdValue);
                viewModel.IsNoThresholdValue(data.ThresholdValue == 0 ? true : false);
                SetCalculateFX(paramhreshold.AmountUSD);
            }
        }, OnError);
    }
}
function GetValueParamterThreshold(transaction) {
    var paramhreshold;
    var Transaction;

    Transaction = transaction;

    if (Transaction != null) {
        paramhreshold = {
            ProductTypeID: GetProductTypeID(Transaction.ProductType),
            DebitCurrencyCode: Transaction.DebitCurrency.Code,
            TransactionCurrencyCode: Transaction.Currency.Code,
            IsResident: Transaction.IsResident,
            AmountUSD: parseFloat(Transaction.AmountUSD),
            CIF: Transaction.Customer.CIF,
            AccountNumber: Transaction.IsOtherAccountNumber ? null : Transaction.Account.AccountNumber,
            IsJointAccount: Transaction.IsJointAccount
        }
    }

    return paramhreshold;
}

function GetProductTypeID(FlowValas) {
    return (FlowValas == null ? null : FlowValas.ID);
}
function SetCalculateFX(amountUSD) {
    if (viewModel.StatementLetter != null && (viewModel.StatementLetter.ID == 1 || viewModel.StatementLetter.ID == 4)) {
        AmountModel.RemainingBalance(0.00);
    } else if (viewModel.StatementLetter != null && viewModel.StatementLetter.ID != 4) {
        AmountModel.RemainingBalance(TotalDealModel.Total.RemainingBalance);
    }
    AmountModel.TotalPPUAmountsUSD(TotalDealModel.Total.IDRFCYDeal);
    AmountModel.TotalDealAmountsUSD(TotalDealModel.Total.IDRFCYDeal);
    AmountModel.TotalPPUUtilization(TotalPPUModel.Total.UtilizationPPU);
    AmountModel.TotalDealUtilization(TotalDealModel.Total.UtilizationDeal);
}
function OnSuccessGetParameters(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        // bind result to observable array
        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };

        viewModel.Parameter().Currencies(ko.mapping.toJS(data.Currency, mapping));
        viewModel.Parameter().ProductType(ko.mapping.toJS(data.ProductType, mapping));
        viewModel.Parameter().DocumentTypes(ko.mapping.toJS(data.DocType, mapping));
        viewModel.Parameter().DocumentPurposes(ko.mapping.toJS(data.PurposeDoc, mapping));
        viewModel.Parameter().StatementLetter(ko.mapping.toJS(data.StatementLetter, mapping));

        // underlying parameter
        viewModel.GetUnderlyingParameters(data);
        LoadDraft();
        // enabling form input controls
        viewModel.IsEditable(true);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}


// Token validation On Success function
function OnSuccessToken(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // read spuser from cookie
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);
        viewModel.SPUser(ko.mapping.toJS(spUser));
    }

    // get parameter values
    DealModel.token = accessToken;
    //GetParameterData(DealModel, OnSuccessGetTotal, OnErrorDeal);
    GetParameters();
}
// delete & upload document underlying start
function DeleteFile(documentPath) {
    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Delete the file to the SharePoint folder.
    var deleteFile = deleteFileToFolder();
    deleteFile.done(function (file, status, xhr) {
        output = file.d;
    });
    deleteFile.fail(OnError);
    // Call function delete File Shrepoint Folder
    function deleteFileToFolder() {
        return $.ajax({
            url: serverUrl + "/_api/web/GetFileByServerRelativeUrl('" + documentPath + "')",
            type: "POST",
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "X-HTTP-Method": "DELETE",
                "IF-MATCH": "*"
                //"content-length": arrayBuffer.byteLength
            }
        });
    }
}

function UploadFileUnderlying(context, document, callBack) {

    // Define the folder path for this example.
    var serverRelativeUrlToFolder = '/Underlying Documents';

    // Get the file name from the file input control on the page.
    var parts = document.DocumentPath.name.split('.');

    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBufferUnderlying();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolderUnderlying(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItemUnderlying(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                // Change the display name and title of the list item.
                var changeItem = updateListItemUnderlying(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    DocumentModels({ "FileName": document.DocumentPath.name, "DocumentPath": output.ServerRelativeUrl });
                    //viewModel.TransactionNetting().Documents.push(kodok);

                    //self.Documents.push(kodok);
                    callBack();
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBufferUnderlying() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }

        //reader.readAsArrayBuffer(fileInput[0].files[0]);
        reader.readAsArrayBuffer(document.DocumentPath);
        //reader.readAsDataURL(document.DocumentPath());
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolderUnderlying(arrayBuffer) {

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                //"content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItemUnderlying(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItemUnderlying(itemMetadata) {

        // Define the list item changes. Use the FileLeafRef property to change the display name.
        // For simplicity, also use the name as the title.
        // The example gets the list item type from the item's metadata, but you can also get it from the
        // ListItemEntityTypeFullName property of the list.
        var body = {
            Title: document.DocumentPath.name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: context.Type.DocTypeName,
            Document_x0020_Purpose: context.Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}

// Upload the file
function UploadFileRecuresive(context, document, callBack, numFile) {

    var indexDocument = document.length - numFile;
    var IsDraft;

    IsDraft = viewModel.TransactionNetting().IsDraft();
    var serverRelativeUrlToFolder = '';
    if (IsDraft == true) {
        serverRelativeUrlToFolder = '/DraftDocument'
    } else {
        serverRelativeUrlToFolder = '/Instruction Documents'
    }

    var parts = document[indexDocument].DocumentPath.name.split('.');
    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp + "." + fileExtension;

    var serverUrl = _spPageContextInfo.webAbsoluteUrl;
    var output;

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBuffer();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolder(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                // Change the display name and title of the list item.
                var changeItem = updateListItem(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    //alert('file uploaded and updated');
                    //return output;
                    var newDoc = {
                        ID: 0,
                        Type: document[indexDocument].Type,
                        Purpose: document[indexDocument].Purpose,
                        IsNewDocument: false,
                        LastModifiedDate: null,
                        LastModifiedBy: null,
                        DocumentPath: output.ServerRelativeUrl,
                        FileName: document[indexDocument].DocumentPath.name
                    };

                    viewModel.NettingDocuments.push(newDoc);

                    if (numFile > 1) {
                        UploadFileRecuresive(context, document, callBack, numFile - 1); // recursive function
                    }
                    callBack();
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);
    // Get the local file as an array buffer.
    function getFileBuffer() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }

        if (document[indexDocument].DocumentPath.type == Util.spitemdoc) {
            var fileURI = serverUrl + document[indexDocument].DocumentPath.DocPath;
            // load file:
            fetch(fileURI, convert, alert);

            function convert(buffer) {
                var blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });

                var domURL = self.URL || self.webkitURL || self,
                  url = domURL.createObjectURL(blob),
                  img = new Image;

                img.onload = function () {
                    domURL.revokeObjectURL(url); // clean up
                    //document.body.appendChild(this);
                    // this = image
                };
                img.src = url;
                reader.readAsArrayBuffer(blob);
            }

            function fetch(url, callback, error) {

                var xhr = new XMLHttpRequest();
                try {
                    xhr.open("GET", url);
                    xhr.responseType = "arraybuffer";
                    xhr.onerror = function () {
                        error("Network error")
                    };
                    xhr.onload = function () {
                        if (xhr.status === 200) callback(xhr.response);
                        else error(xhr.statusText);
                    };
                    xhr.send();
                } catch (err) {
                    error(err.message)
                }
            }


        } else {
            reader.readAsArrayBuffer(document[indexDocument].DocumentPath);
        }
        //bangkit

        //reader.readAsDataURL(document.DocumentPath);
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(arrayBuffer) {

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                //"content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItem(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItem(itemMetadata) {
        var body = {
            Title: document[indexDocument].DocumentPath.name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: document[indexDocument].Type.Name,
            Document_x0020_Purpose: document[indexDocument].Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }

}
function OnErrorDeal(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

// AJAX On Error Callback
function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}
function OnSuccessThresholdPrm() {
    idrrate = DataModel.RateIDR;
}
function OnSuccessGetTotal(dataCall) {
    idrrate = dataCall.RateIDR;
    //viewModel.FCYIDRTreshold(dataCall.FCYIDRTrashHold[0]);
    //viewModel.Treshold(dataCall.TreshHold[0]);
}
// AJAX On Alway Callback
function OnAlways() {
    // enabling form input controls
    // viewModel.save(true);
}
function OnChangeNettingPurpose() {
    if (viewModel.Selected().NettingPurpose() == TMONettingPurpose.Unwind) {
        viewModel.Unwind(true);
    } else {
        viewModel.Unwind(false);
    }
};