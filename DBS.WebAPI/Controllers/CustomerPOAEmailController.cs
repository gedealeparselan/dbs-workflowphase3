﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;
using DBS.Entity;

namespace DBS.WebAPI.Controllers
{
    public class CustomerPOAEmailController : ApiController
    {
        private string message = string.Empty;

        private readonly ICustomerPOAEmailRepository repository = null;

        public CustomerPOAEmailController()
        {
            this.repository = new CustomerPOAEmailRepository();
        }

        public CustomerPOAEmailController(CustomerPOAEmailRepository repository)
        {
            this.repository = repository;
        }

        [ResponseType(typeof(CustomerPOAEmailGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(string CIF, IList<CustomerPOAEmailFilter> filters, int? page, int? size, string sort_column, string sort_order)
        {
            CustomerPOAEmailGrid output = new CustomerPOAEmailGrid();

            if (!repository.GetCustomerPOAEmailGrid(CIF, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "UpdateDate" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerPOAEmailGrid>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(POAEmailProductModel))]
        [Route("api/CustomerPOAEmail/MasterProduct")]
        [HttpGet]
        public HttpResponseMessage GetMasterEmailProduct()
        {
            IList<POAEmailProductModel> result = new List<POAEmailProductModel>();

            if (!repository.GetMasterPOAEmailProduct(ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<POAEmailProductModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(POAEmailProductModel))]
        [Route("api/CustomerPOAEmail/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            CustomerPOAEmailModel output = new CustomerPOAEmailModel();

            if (!repository.GetPOAEmailByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<CustomerPOAEmailModel>(HttpStatusCode.OK, output);
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS CBO Maker")]
        public HttpResponseMessage Post(JSONSentModel jsm) 
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddCustomerPOAEmailWF(jsm.POAEmail, jsm.Email, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New POA Email data has been created." });
                }
            }
        }

        [ResponseType(typeof(IList<CustomerPOAEmailModel>))]
        [Route("api/CustomerPOAEmail/GetDataPOAEmail")]
        [HttpGet]
        public HttpResponseMessage Get(string CIF)
        {
            IList<CustomerPOAEmailModel> output = new List<CustomerPOAEmailModel>();

            if (!repository.GetDataCustomerPOAEmail(CIF, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerPOAEmailModel>>(HttpStatusCode.OK, output);
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/CustomerPOAEmail/UpdateDataPOAEmail")]
        public HttpResponseMessage Post(JSONSentUpdateModel jsum)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateCustomerPOAEmailWF(jsum.OldEmail, jsum.Email, jsum.POAEmail, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "POA Email data has been Update." });
                }
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/CustomerPOAEmail/DeleteDataPOAEmail")]
        public HttpResponseMessage Post(JSONSentDeleteModel key)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteCustomerPOAEmailWF(key.CIF, key.Email, key.POAEmail, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "POA Email data has been Delete." });
                }
            }
        }
    }
}
