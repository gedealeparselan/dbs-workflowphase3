﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    public class TransactionCallbackDraftController : ApiController
    {
        private string message = string.Empty;
        private readonly ICallbackDraftRepository CallbackDraft = null;

        public TransactionCallbackDraftController() {
            this.CallbackDraft = new TransactionCallbackDraft();
        }
        protected override void Dispose(bool disposing)
        {
            if (CallbackDraft != null)
                CallbackDraft.Dispose();

            base.Dispose(disposing);
        }

        [ResponseType(typeof(string))]
        [HttpPost]
        [Route("api/TransactionCallbackDraft")]
        public HttpResponseMessage SaveTransactionCallbackDraft(IList<TransactionCallbackDraftModel> Callback)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!CallbackDraft.AddTransactionCallbackDraft(Callback, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "Create Data has been created." });
                }
            }
        }

        [ResponseType(typeof(IList<TransactionCallbackDraftModel>))]
        [Route("api/TransactionCallbackDraft/{id}")]
        [HttpGet]
        public HttpResponseMessage GetTransactionCallbackDraft(Int64 id)
        {
            IList<TransactionCallbackDraftModel> output = new List<TransactionCallbackDraftModel>();

            if (!CallbackDraft.GetTransactionCallbackDraft(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<TransactionCallbackDraftModel>>(HttpStatusCode.OK, output);
            }
        }
    }
}