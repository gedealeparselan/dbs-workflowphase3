﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class CustomerContactController : ApiController
    {
        private string message = string.Empty;

        private readonly ICustomerContactRepository repository = null;

        public CustomerContactController()
        {
            this.repository = new CustomerContactRepository();
        }

        public CustomerContactController(ICustomerContactRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all customer contact.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<CustomerContactModel>))]
        public HttpResponseMessage Get()
        {
            IList<CustomerContactModel> output = new List<CustomerContactModel>();

            if (!repository.GetCustomerContact(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerContactModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get customer contacts with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<CustomerContactModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<CustomerContactModel> output = new List<CustomerContactModel>();

            if (!repository.GetCustomerContact(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerContactModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get customer contacts for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(CustomerContactWFGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<CustomerContactFilter> filters)
        {
            CustomerContactWFGrid output = new CustomerContactWFGrid();

            if (!repository.GetCustomerContactWF(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerContactWFGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find customer contacts using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<CustomerContactModel>))]
        [HttpGet]
        [Route("api/CustomerContact/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<CustomerContactModel> output = new List<CustomerContactModel>();

            if (!repository.GetCustomerContact(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerContactModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get customer contacts by ID.
        /// </summary>
        /// <param name="id">Customer Contact ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(CustomerContactModel))]
        [Route("api/CustomerContact/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            CustomerContactModel output = new CustomerContactModel();

            if (!repository.GetCustomerContactByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<CustomerContactModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new customer contact.
        /// </summary>
        /// <param name="CustomerContact">Customer Contact data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS CBO Maker")]
        public HttpResponseMessage Post(CustomerContactWFModel CustomerContact)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
               // if (!repository.AddCustomerContact(CustomerContact, ref message))
                if(!repository.AddCustomerContactWF(CustomerContact,ref message ))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Customer Contact data has been created." });
                }
            }
        }

        /// <summary>
        /// Create a new customer contact draft.
        /// </summary>
        /// <param name="CustomerContact">Customer Contact data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS CBO Maker")]
        [Route("api/CustomerContact/Add/Workflow/{workflowInstanceID}/ApproverID/{approverID}")]
        public HttpResponseMessage PostDraft(Guid workflowInstanceID, long approverID, CustomerContactModel CustomerContact)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionContact(workflowInstanceID, approverID, CustomerContact, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Customer Contact data has been created." });
                }
            }
        }

        /// <summary>
        /// Update customer contact draft.
        /// </summary>
        /// <param name="CustomerContact">Customer Contact data.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS CBO Maker")]
        [Route("api/CustomerContact/Update/Workflow/{workflowInstanceID}/ApproverID/{approverID}")]
        public HttpResponseMessage UpdateDraft(Guid workflowInstanceID, long approverID, CustomerContactModel CustomerContact)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTransactionContact(workflowInstanceID, approverID, CustomerContact, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "Contact data has been updated." });
                }
            }
        }

        /// <summary>
        /// Delete customer contact draft.
        /// </summary>
        /// <param name="CustomerContact">Customer Contact data.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS CBO Maker")]
        [Route("api/CustomerContact/Delete/Workflow/{workflowInstanceID}/ApproverID/{approverID}")]
        public HttpResponseMessage DeleteDraft(Guid workflowInstanceID, long approverID, CustomerContactModel CustomerContact)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteTransactionContact(workflowInstanceID, approverID, CustomerContact, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "Contact data has been deleted." });
                }
            }
        }

        /// <summary>
        /// Modify existing customer contact.
        /// </summary>
        /// <param name="id">ID of CustomerContact to be modify.</param>
        /// <param name="CustomerContact">Customer Contact data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/CustomerContact/{id}")]
        //[Authorize(Roles = "DBS CBO Maker")]
        public HttpResponseMessage Put(int id, CustomerContactWFModel CustomerContact)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateCustomerContactWF(id, CustomerContact, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Customer Contact data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting customer contact.
        /// </summary>
        /// <param name="id">ID of customer contact to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/CustomerContact/{id}")]
        //[Authorize(Roles = "DBS CBO Maker")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteCustomerContact(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Customer Contact data has been deleted." });
                }
            }
        }
        //dayat
        [ResponseType(typeof(CityModel))]
        [Route("api/CustomerContact/AutoCompeleteSelularPhoneNumber")]
        [HttpGet]
        public HttpResponseMessage GetSelular(string CIF, string query)
        {
            IList<CustomerPhoneNumberModel> result = new List<CustomerPhoneNumberModel>();

            if (!repository.AutoCompeleteSelularPhoneNumber(CIF,query, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<CustomerPhoneNumberModel>>(HttpStatusCode.OK, result);
            }
        }
        [ResponseType(typeof(CityModel))]
        [Route("api/CustomerContact/AutoCompeleteOfficePhoneNumber")]
        [HttpGet]
        public HttpResponseMessage GetOffice(string CIF, string query)
        {
            IList<CustomerPhoneNumberModel> result = new List<CustomerPhoneNumberModel>();

            if (!repository.AutoCompeleteOfficePhoneNumber(CIF,query, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<CustomerPhoneNumberModel>>(HttpStatusCode.OK, result);
            }
        }
        [ResponseType(typeof(CityModel))]
        [Route("api/CustomerContact/AutoCompeleteHomePhoneNumber")]
        [HttpGet]
        public HttpResponseMessage GetHome(string CIF, string query)
        {
            IList<CustomerPhoneNumberModel> result = new List<CustomerPhoneNumberModel>();

            if (!repository.AutoCompeleteHomePhoneNumber(CIF, query, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<CustomerPhoneNumberModel>>(HttpStatusCode.OK, result);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}