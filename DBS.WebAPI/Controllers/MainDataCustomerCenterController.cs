﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    public class MainDataCustomerCenterController : ApiController
    {
        private string message = string.Empty;

        private readonly IMainDataCustomerCenterModelRepository repository = null;

        public MainDataCustomerCenterController()
        {
            this.repository = new MainDataCustomerCenterRepository();
        }

        public MainDataCustomerCenterController(IMainDataCustomerCenterModelRepository repository)
        {
            this.repository = repository;
        }
        /// <summary>
        /// Get customers with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<MainDataCustomerCenterModel>))]

        public HttpResponseMessage GetMainDataCustomerCenter()
        {
            IList<MainDataCustomerCenterModel> output = new List<MainDataCustomerCenterModel>();

            if (!repository.GetMainDataCustomerCenter(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<MainDataCustomerCenterModel>>(HttpStatusCode.OK, output);
            }
        }
        [ResponseType(typeof(IList<CBOMaintenancePriorityOrderModel>))]
        public HttpResponseMessage GetMainDataCustomerCenterMaintenanceCC()
        {
            IList<MainDataCustomerCenterModel> output = new List<MainDataCustomerCenterModel>();

            if (!repository.GetMainDataCustomerCenterMaintenanceCC(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<MainDataCustomerCenterModel>>(HttpStatusCode.OK, output);
            }
        }
        [HttpPost]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(IList<MainDataCustomerCenterModel> mainDataCustomerCenter)
        {

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddMainDataCustomerCenter(mainDataCustomerCenter, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Main Data Customer Center data has been created." });
                }
            }
        }
    }
}
