﻿var accessToken;
var NewCustomerFXWarning = "Cannot proccess FX Transaction for New Customer Added.";

//ari 20160907 var to store aucomplete array
var autoCompleteData = [];
var obj = {};

var vLLDAmount; var vLLDRounding; //henggar "wicak" simple var for val 2.5% lld doc

/* dodit@2014.11.08:add format function */
var formatNumber = function (num) {
    if (num == null) {
        return;
    }
    var n = num.toString(), p = n.indexOf('.');
    return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
        return p < 0 || i < p ? ($0 + ',') : $0;
    });
}
//Andi, 22 October 2015
var DecodeLogin = function (username) {
    var index = username.lastIndexOf("|");
    var str = username.substring(index + 1);
    return str;
}
//End Andi
var formatDateValue = function (date, isDateOnly, isDateLong) {
    var DateToFormat = new Date(date);

    if (date == '1970/01/01 00:00:00' || date == null) {
        return "";
    }
    if (moment(DateToFormat).isValid()) {
        return moment(DateToFormat).format(config.format.date);
    } else {
        return "";
    }
};

var formatBeneAccNumber = function (num) {//Rizki - 2016-02-09

    num = num.replace(/-/g, '');

    sheet_a = (num.length > 0) ? num.substr(0, 4) : '';
    sheet_b = (num.length > 4) ? '-' + num.substr(4, 4) : '';
    sheet_c = (num.length > 8) ? '-' + num.substr(8) : '';

    return sheet_a + sheet_b + sheet_c;
}

var $box;
var $remove = false;
var cifData = '0';
var customerNameData = '';
var idrrate = 0;
var DocumentModels = ko.observable({ FileName: '', DocumentPath: '' });
var PPUModel = { cif: cifData, token: accessToken }

var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};

var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};

var BizSegmentModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var BranchModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Code: ko.observable()
};

var CityModel = {
    CityID: ko.observable(),
    CityCode: ko.observable(),
    Description: ko.observable()
};

var TypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ProductModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    WorkflowID: ko.observable(),
    Workflow: ko.observable()
};

var LLDModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var LLDDocument = {
    LLDDocumentID: ko.observable(),
    LLDDocumentCode: ko.observable(),
    Description: ko.observable()
};

var ValidationTransactionModel = {
    Cif: ko.observable(),
    ProductID: ko.observable(),
    CurrencyID: ko.observable(),
    Amount: ko.observable(),
    ApplicationDate: ko.observable(),
    ExecutionDate: ko.observable(),
    AccountNumber: ko.observable(),
    BeneAccount: ko.observable(),
    BeneName: ko.observable()
};
var UnderlyingDocModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ChannelModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var SourceModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var MandatoryFieldPayment = {
    ID: ko.observable(),
    Name: ko.observable()
};

//Tambahan IPE
var BeneficiaryCountryModel = {
    ID: ko.observable(),
    Code: ko.observable()
};
var TransactionRelationshipModel = {
    ID: ko.observable(),
    Code: ko.observable()
};
var CBGCustomerModel = {
    ID: ko.observable(),
    Code: ko.observable()
};
var TransactionUsingDebitSundryModel = {
    ID: ko.observable(),
    Code: ko.observable()
};
//End

var BankModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    BranchCode: ko.observable(),
    SwiftCode: ko.observable(),
    BankAccount: ko.observable(),
    Description: ko.observable(),
    CommonName: ko.observable(),
    Currency: ko.observable(),
    PGSL: ko.observable(),
    IBranchBank: ko.observable()
};

//Started by haqi
var GroupCheckBoxModel = {
    IsNameMaintenance: ko.observable(false),
    IsIdentityTypeMaintenance: ko.observable(false),
    IsNPWPMaintenance: ko.observable(false),
    IsMaritalStatusMaintenance: ko.observable(false),
    IsCorrespondenceMaintenance: ko.observable(false),
    IsIdentityAddressMaintenance: ko.observable(false),
    IsOfficeAddressMaintenance: ko.observable(false),
    IsCorrespondenseAddressMaintenance: ko.observable(false),
    IsPhoneFaxEmailMaintenance: ko.observable(false),
    IsNationalityMaintenance: ko.observable(false),
    IsFundSourceMaintenance: ko.observable(false),
    IsNetAssetMaintenance: ko.observable(false),
    IsMonthlyIncomeMaintenance: ko.observable(false),
    IsJobMaintenance: ko.observable(false),
    IsAccountPurposeMaintenance: ko.observable(false),
    IsMonthlyTransactionMaintenance: ko.observable(false),
    IsBeneficialOwner: ko.observable(false),
    IsOthers: ko.observable(false),

    IsTujuanBukaRekeningLainnya: ko.observable(false),
    IsSumberDanaLainnya: ko.observable(false),
    IsPekerjaanProfesional: ko.observable(false),
    IsPekerjaanLainnya: ko.observable(false),

    //penambahan checkbox retail cif season2 henggar 180417
    IsMotherMaiden: ko.observable(false),
    IsEducation: ko.observable(false),
    IsReligion: ko.observable(false),
    IsDataUBO: ko.observable(false),
    IsTransparansiDataPenawaranProduct: ko.observable(false),
    IsTransparansiPenggunaan: ko.observable(false),
    IsPenawaranProduct: ko.observable(false),
    IsSignature: ko.observable(false),
    IsStampDuty: ko.observable(false),
    IsSLIK: ko.observable(false),
    IsFatcaCRS: ko.observable(false),
    IsChangeSignature: ko.observable(false),
    ChangeSignature: ko.observable(false)
    //end
}

//var TransactionTypeModel = {
//    ID: ko.observable(),
//    ProductID: ko.observable(),
//    ProductName: ko.observable(),
//    Name: ko.observable(),
//    ParameterTypeDisplay: ko.observable()
//};

var BrachRiskRatingModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var CustomerModelModal = {
    CIF: ko.observable(),
    Name: ko.observable()
};

// Tambah Agung
var JoinModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
//End

var FFDAccountModel = {
    AccountNumber: ko.observable(),
    IsAddFFDAccount: ko.observable(false)
};

var DormantAccountModel = {
    AccountNumber: ko.observable(),
    IsAddCurrencyDormant: ko.observable(false)
};

var MaintenanceTypeModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var TransactionSubTypeModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var ParameterSystemModel = {
    ID: ko.observable(),
    Name: ko.observable()
}
var AccountTypeData = [{ ID: 1, Name: 'Single' }, { ID: 2, Name: 'Join' }];
var ModificationData = [{ ID: 1, Name: 'Add' }, { ID: 2, Name: 'Edit' }, { ID: 3, Name: 'Delete' }]
var DispatchModeData = [{ ID: 1, Name: 'BSPL Delivery and Email' }, { ID: 2, Name: 'Collect by Person' }, { ID: 3, Name: 'Email' }, { ID: 4, Name: 'No Dispatch' }, { ID: 5, Name: 'Post' }, { ID: 6, Name: 'SSPL Delivery' }, { ID: 9, Name: '-' }]
//var RiskRatingResultData = [{ ID: 0, Name: 'Low Risk' }, { ID: 1, Name: 'Medium Risk' }, { ID: 2, Name: 'High Risk' }]
//var MaritalStatusData = [{ ID: 0, Name: 'Belum Menikah' }, { ID: 1, Name: 'Duda / Janda' }, { ID: 2, Name: 'Sudah Menikah' }]
var RiskRatingResultData = [{ ID: 1, Name: 'Low Risk' }, { ID: 2, Name: 'Medium Risk' }, { ID: 3, Name: 'High Risk' }]
var MaritalStatusData = [{ ID: 1, Name: 'Belum Menikah' }, { ID: 2, Name: 'Duda / Janda' }, { ID: 3, Name: 'Sudah Menikah' }]
var Source = [{ ID: 1, Name: 'CSO ' }, { ID: 2, Name: 'Counter' }]
var MandatoryNotValidate = [{ ID: 1, Name: '#channel' }, { ID: 2, Name: '#source' }, { ID: 3, Name: '#bene-name' }, { ID: 4, Name: '#bene-acc-number' }, { ID: 5, Name: '#beneficiarybusiness' }]

//henggar 180417
var Education = [{ ID: 1, Name: 'Degree' }, { ID: 2, Name: 'Diploma' }, { ID: 3, Name: 'Graduate' }, { ID: 4, Name: "High School" }, { ID: 5, Name: "Junior High School" }, { ID: 6, Name: 'Master' }, { ID: 7, Name: 'Other' }, { ID: 8, Name: 'Post Graduate' }, { ID: 9, Name: 'Specialist' }, { ID: 10, Name: 'Under Graduate' }]
var Religion = [{ ID: 1, Name: 'Budist' }, { ID: 2, Name: 'Catholic' }, { ID: 3, Name: 'Christian' }, { ID: 4, Name: "Hindu" }, { ID: 5, Name: "Khonghucu" }, { ID: 6, Name: 'Muslim' }, { ID: 7, Name: 'Other' }]
var FATCAReviewStatus = [{ ID: 1, Name: 'Completed' }, { ID: 2, Name: 'Pending' }]
var FATCACRSStatus = [{ ID: 1, Name: 'Yes' }, { ID: 2, Name: 'No' }]
var TaxPayer = [{ ID: 1, Name: 'SSN' }, { ID: 2, Name: 'ITIN' }, { ID: 3, Name: 'ATIN' }, { ID: 4, Name: 'PTIN' }, { ID: 5, Name: 'TIN' }]
var WithholdingCertificationType = [{ ID: 1, Name: 'NOT APLICABLE' }, { ID: 2, Name: 'SELF CERT' }, { ID: 3, Name: 'SELF CERT & LOSS OF CITIZENSHIP' }, { ID: 4, Name: 'W-9' }, { ID: 5, Name: 'W-8BEN' }, { ID: 6, Name: 'W-8BEN & LOSS OF CITIZENSHIP' }]
//end

var RetailCIFCBOModel = {
    //DispatchModeTypeID: ko.observable(),
    AccountNumber: ko.observable(),
    GroupCheckBox: ko.observable(GroupCheckBoxModel),
    Name: ko.observable(),
    IdentityNumber: ko.observable(),
    IdentityStartDate: ko.observable(),
    IdentityEndDate: ko.observable(),
    IdentityAddress: ko.observable(),
    IdentityKelurahan: ko.observable(),
    IdentityKecamatan: ko.observable(),
    IdentityCity: ko.observable(),
    IdentityProvince: ko.observable(),
    IdentityCountry: ko.observable(),
    IdentityPostalCode: ko.observable(),

    IdentityNumber3: ko.observable(),
    IdentityStartDate3: ko.observable(),
    IdentityEndDate3: ko.observable(),
    IdentityAddress3: ko.observable(),
    IdentityKelurahan3: ko.observable(),
    IdentityKecamatan3: ko.observable(),
    IdentityCity3: ko.observable(),
    IdentityProvince3: ko.observable(),
    IdentityCountry3: ko.observable(),
    IdentityPostalCode3: ko.observable(),

    IdentityNumber4: ko.observable(),
    IdentityStartDate4: ko.observable(),
    IdentityEndDate4: ko.observable(),
    IdentityAddress4: ko.observable(),
    IdentityKelurahan4: ko.observable(),
    IdentityKecamatan4: ko.observable(),
    IdentityCity4: ko.observable(),
    IdentityProvince4: ko.observable(),
    IdentityCountry4: ko.observable(),
    IdentityPostalCode4: ko.observable(),

    IdentityNumber2: ko.observable(),
    IdentityStartDate2: ko.observable(),
    IdentityEndDate2: ko.observable(),
    IdentityAddress2: ko.observable(),
    IdentityKelurahan2: ko.observable(),
    IdentityKecamatan2: ko.observable(),
    IdentityCity2: ko.observable(),
    IdentityProvince2: ko.observable(),
    IdentityCountry2: ko.observable(),
    IdentityPostalCode2: ko.observable(),


    NPWPNumber: ko.observable(),
    IsNPWPReceived: ko.observable(),
    MaritalStatusID: ko.observable(ParameterSystemModel),
    SpouseName: ko.observable(),
    IsCorrespondenseToEmail: ko.observable(),
    CorrespondenseAddress: ko.observable(),
    CorrespondenseKelurahan: ko.observable(),
    CorrespondenseKecamatan: ko.observable(),
    CorrespondenseCity: ko.observable(),
    CorrespondenseProvince: ko.observable(),
    CorrespondenseCountry: ko.observable(),
    CorrespondensePostalCode: ko.observable(),
    CellPhoneMethodID: ko.observable(ParameterSystemModel),
    CellPhone: ko.observable(),
    UpdatedCellPhone: ko.observable(),
    HomePhoneMethodID: ko.observable(ParameterSystemModel),
    HomePhone: ko.observable(),
    UpdatedHomePhone: ko.observable(),
    OfficePhoneMethodID: ko.observable(ParameterSystemModel),
    OfficePhone: ko.observable(),
    UpdatedOfficePhone: ko.observable(),
    FaxMethodID: ko.observable(ParameterSystemModel),
    Fax: ko.observable(),
    UpdatedFax: ko.observable(),
    EmailAddress: ko.observable(),
    OfficeAddress: ko.observable(),
    OfficeKelurahan: ko.observable(),
    OfficeKecamatan: ko.observable(),
    OfficeCity: ko.observable(),
    OfficeProvince: ko.observable(),
    OfficeCountry: ko.observable(),
    OfficePostalCode: ko.observable(),
    Nationality: ko.observable(),
    UBOName: ko.observable(),
    UBOIdentityType: ko.observable(),
    UBOPhone: ko.observable(),
    UBOJob: ko.observable(),
    CompanyName: ko.observable(),
    Position: ko.observable(),
    WorkPeriod: ko.observable(),
    IndustryType: ko.observable(),
    //Parameter System
    IdentityTypeID: ko.observable(ParameterSystemModel),
    IdentityTypeID2: ko.observable(ParameterSystemModel),
    IdentityTypeID3: ko.observable(ParameterSystemModel),
    IdentityTypeID4: ko.observable(ParameterSystemModel),
    FundSource: ko.observable(ParameterSystemModel),
    NetAsset: ko.observable(ParameterSystemModel),
    MonthlyIncome: ko.observable(ParameterSystemModel),
    MonthlyExtraIncome: ko.observable(ParameterSystemModel),
    Job: ko.observable(ParameterSystemModel),
    AccountPurpose: ko.observable(ParameterSystemModel),
    IncomeForecast: ko.observable(ParameterSystemModel),
    OutcomeForecast: ko.observable(ParameterSystemModel),
    TransactionForecast: ko.observable(ParameterSystemModel),
    //Risk rating form
    ReportDate: ko.observable(),
    NextReviewDate: ko.observable(),
    RiskRatingResult: ko.observable(ParameterSystemModel),
    ATMNumber: ko.observable(),
    DispatchModeType: ko.observable(ParameterSystemModel),
    HubunganNasabah: ko.observable(),

    //dani 8-4-2016
    SumberDanaLainnya: ko.observable(),
    PekerjaanProfesional: ko.observable(),
    PekerjaanLainnya: ko.observable(),
    TujuanBukaRekeningLainnya: ko.observable(),
    //end dani

    //dani 20-5-2016
    CellPhoneMethodID: ko.observable(ParameterSystemModel),
    HomePhoneMethodID: ko.observable(ParameterSystemModel),
    OfficePhoneMethodID: ko.observable(ParameterSystemModel),
    FaxMethodID: ko.observable(ParameterSystemModel),
    //end dani
    IsSignature: ko.observable(),
    IsStampDuty: ko.observable(),
    IsPenawaranProductPerbankan: ko.observable(),
    SelularPhoneNumbers: ko.observableArray(SelularPhoneNumberModel),
    OfficePhoneNumbers: ko.observableArray(OfficePhoneNumberModel),
    HomePhoneNumbers: ko.observableArray(HomePhoneNumberModel),
    //retail cif penambahan
    MotherMaiden: ko.observable(),
    Education: ko.observable(),
    Religion: ko.observable(),
    DataUBOCif: ko.observable(),
    DataUBOName: ko.observable(),
    DataUBORelationship: ko.observable(),
    TransparansiDataPenawaranProduct: ko.observable(),
    GrosIncomeccy: ko.observable(),
    GrossIncomeAmount: ko.observable(),
    GrossCifSpouse: ko.observable(),
    GrossSpouseName: ko.observable(),
    GrossSpouseRelation: ko.observable(),
    FatcaReviewStatus: ko.observable(),
    FatcaCountryCode: ko.observable(),
    FatcaCRSStatus: ko.observable(),
    FatcaTaxPayer: ko.observable(),
    FatcaWithHolding: ko.observable(),
    FatcaReviewStatusDate: ko.observable(),
    FatcaDateOnForm: ko.observable(),
    FatcaTaxPayerID: ko.observable(),
    AmountLienUnlien: ko.observable(),
    Tier: ko.observable(),
    DispatchModeOther: ko.observable()
};
//end by Haqi

var LLDRounding;
var AmountResCalculate;

var RMModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    RankID: ko.observable(),
    Rank: ko.observable(),
    SegmentID: ko.observable(),
    Segment: ko.observable(),
    BranchID: ko.observable(),
    Branch: ko.observable(),
    Email: ko.observable()
};

var ContactModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    PhoneNumber: ko.observable(),
    DateOfBirth: ko.observable(),
    Address: ko.observable(),
    IDNumber: ko.observable()
};

var FunctionModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var CurrencyModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};
var ChargingAccountCurrencyModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};
//henggar
var SundryModel = {
    ID: ko.observable(),
    OABAccNo: ko.observable()
};

var NostroModel = {
    ID: ko.observable(),
    NostroUsed: ko.observable()
};
//end
/*var AccountModel = {
    AccountNumber: ko.observable(),
    Currency: ko.observable(CurrencyModel)
};*/

var PaymentMode = {
    ModeID: ko.observable(),
    ModeName: ko.observable(),
    Status: ko.observable()
};

var AccountModel = {
    AccountNumber: ko.observable(),
    CustomerName: ko.observable(),
    Currency: ko.observable(CurrencyModel),
    IsJointAccount: ko.observable()
};
var CustomerDraftModel = {
    DraftCIF: ko.observable(),
    DraftCustomerName: ko.observable(),
    DraftAccountNumber: ko.observable(AccountModel)
}

var ProductTypeModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    IsFlowValas: ko.observable(),
    Description: ko.observable()
};
//Agung
var FNACoreModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var FunctionTypeModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var AccountTypeModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var Transaction_TypeModel = {
    TransTypeID: ko.observable(),
    TransactionTypeName: ko.observable()
};
//End Agung
var UnderlyingDocumentModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var DocumentTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var DocumentPurposeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var FXComplianceModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ChargesModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var ThresholdModel = {
    IsHitThreshold: ko.observable(),
    ThresholdValue: ko.observable(),
    RoundingValue: ko.observable(),
    TransactionType: ko.observable(),
    ResidentType: ko.observable()
}

var UnderlyingModel = {
    ID: ko.observable(),
    UnderlyingDocument: ko.observable(UnderlyingDocumentModel),
    DocumentType: ko.observable(DocumentTypeModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    AttachmentNo: ko.observable(),
    IsStatementLetter: ko.observable(),
    IsDeclarationOfException: ko.observable(),
    StartDate: ko.observable(),
    EndDate: ko.observable(),
    DateOfUnderlying: ko.observable(),
    ExpiredDate: ko.observable(),
    ReferenceNumber: ko.observable(),
    SupplierName: ko.observable(),
    InvoiceNumber: ko.observable()
};

var DocumentModel = {
    ID: ko.observable(),
    Type: ko.observable(DocumentTypeModel),
    Purpose: ko.observable(DocumentPurposeModel),
    FileName: ko.observable(),
    DocumentPath: ko.observable(),
    LastModifiedDate: ko.observable(new Date),
    LastModifiedBy: ko.observable()

};

var CustomerModel = {
    CIF: ko.observable(),
    Name: ko.observable(),
    POAName: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Branch: ko.observable(BranchModel),
    Type: ko.observable(TypeModel),
    RM: ko.observable(RMModel),
    Contacts: ko.observableArray([ContactModel]),
    Functions: ko.observableArray([FunctionModel]),
    Accounts: ko.observableArray([AccountModel]),
    Underlyings: ko.observableArray([UnderlyingModel])
};

var BeneficiaryBusinesModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
}

var FundListModel = {
    ID: ko.observable(),
    OffOnshoreModel: ko.observable(),
    FundCode: ko.observable(),
    FundName: ko.observable(),
    AccountNo: ko.observable(),
    SavingPlanDate: ko.observable(),
    IsSwitching: ko.observable(),
    IsSaving: ko.observable(),
    IsDeleted: ko.observable(),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
};
var InvestmentListModel = {
    InvestmentID: ko.observable(),
    Investment: ko.observable(),
    CIF: ko.observable(),
    IsUT: ko.observable(),
    IsBond: ko.observable(),
    STNumber: ko.observable(),
    CheckIN: ko.observable(),
    CheckCIF: ko.observable(),
    CheckName: ko.observable(),
    CheckST: ko.observable(),
}
var SolIDModel = {
    SolID: ko.observable(),
    ID: ko.observable(),
    Name: ko.observable(),
    Code: ko.observable()
}
//Andi, 22 October 2015
var TransactionTypeModel = {
    TransTypeID: ko.observable(),
    TransactionTypeName: ko.observable(),
    ProductID: ko.observable(),
    ProductName: ko.observable()
};
var FDRemarksModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

//End Andi
var LimitProductModel = {
    TransactionLimitProductID: ko.observable(),
    MinAmount: ko.observable(),
    MaxAmount: ko.observable(),
    Unlimited: ko.observable(),
    Product: ko.observable(ProductModel),
    Currency: ko.observable(CurrencyModel)
};

var TBOTransactionModel = {
    TransactionID: ko.observable(),
    ApplicationID: ko.observable(),
    CIF: ko.observable(),
    CustomerName: ko.observable(),
    ID: ko.observable(),
    TBOSLAID: ko.observable(),
    ActDate: ko.observable(),
    ExptDate: ko.observable(),
    Status: ko.observable(),
    TypeofDoc: ko.observable(DocumentTypeModel),
    PurposeofDoc: ko.observable(DocumentPurposeModel),
    FileName: ko.observable(DocumentModel),
    CreatedBy: ko.observable(),
    CreatedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    LastModifiedDate: ko.observable(),
    TBOApplicationID: ko.observable()
};

//Tambah Tag Untag
var TagUntagModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
//End
var MidrateDataModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable(),
    RupiahRate: ko.observable()
};

// added model form underlying -----------
var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};
var SelectModel = function (cif, id) {
    var self = this;
    self.ID = ko.observable(0);
    self.CIF = ko.observable(cif);
    self.UnderlyingID = ko.observable(id);
    self.ParentID = ko.observable(0);
}
var SelectBulkModel = function (cif, id) {
    var self = this;
    self.ID = ko.observable(0);
    self.CIF = ko.observable(cif);
    self.UnderlyingID = ko.observable(id);
    self.ParentID = ko.observable(0);
}
var UnderlyingDocumentModel2 = function (id, name, code) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Name = ko.observable(name);
    self.CodeName = ko.observable(code + ' (' + name + ')');
}
var DocumentTypeModel2 = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}
var StatementLetterModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}
var CurrencyModel2 = function (id, code, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Description = ko.observable(description);
}

var ProductTypeModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    IsFlowValas: ko.observable(),
    Description: ko.observable()
};

var DocumentPurposeModel2 = function (id, name, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(description);
}

var CustomerUnderlyingMappingModel = function (FileID, UnderlyingID, attachNo) {
    var self = this;
    self.ID = ko.observable(0);
    self.UnderlyingID = ko.observable(UnderlyingID);
    self.UnderlyingFileID = ko.observable(FileID);
    self.AttachmentNo = ko.observable(attachNo);
}

var SelectUtillizeModel = function (id, enable, uSDAmount, statementLetter) {
    var self = this;
    self.ID = ko.observable(id);
    self.Enable = ko.observable(enable);
    self.USDAmount = ko.observable(uSDAmount);
    self.StatementLetter = ko.observable(statementLetter);
}
var today = Date.now();
//Andi
var FDModel = {
    WorkflowInstanceID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    IsTopUrgent: ko.observable(0),
    IsTopUrgentChain: ko.observable(false),
    IsNormal: ko.observable(false),
    ApplicationDate: ko.observable(moment(today).format(config.format.date)),
    ExecutionDate: ko.observable(moment(today).format(config.format.date)),
    IsDraft: ko.observable(false),
    IsNewCustomer: ko.observable(false),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    ID: ko.observable(),
    TransactionID: ko.observable(),
    Channel: ko.observable(ChannelModel),
    TransactionType: ko.observable(TransactionTypeModel),
    FDAccNumber: ko.observable(""),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(0),
    CreditAccNumber: ko.observable(""),
    DebitAccNumber: ko.observable(""),
    InterestRate: ko.observable(),
    Tenor: ko.observable(),
    ValueDate: ko.observable(),
    MaturityDate: ko.observable(),
    Remarks: ko.observable(FDRemarksModel),
    FDBankName: ko.observable(""),
    Documents: ko.observableArray([]),
    AttachmentRemarks: ko.observable(),
    IsBringupTask: ko.observable(false),
    Source: ko.observable(SourceModel),
    DocsComplete: ko.observable(false),
    AllInRate: ko.observable(),
    FTPRate: ko.observable()
}
//End Andi
var formatAccount = function () {
    var num = document.getElementById("bene-acc-number").value;
    num = num.replace(/-/g, '');

    viewModel.TransactionModel().BeneAccNumber(num);

    sheet_a = (num.length > 0) ? num.substr(0, 4) : '';
    sheet_b = (num.length > 4) ? '-' + num.substr(4, 4) : '';
    sheet_c = (num.length > 8) ? '-' + num.substr(8) : '';

    viewModel.BeneAccNumberMask(sheet_a + sheet_b + sheet_c);
    viewModel.OnChangedBene();
}
//Dani
var TransactionTMOModel = {
    IsTopUrgent: ko.observable(0),
    IsResident: ko.observable(false),
    IsCitizen: ko.observable(false),
    IsSignatureVerified: ko.observable(false),
    IsDormantAccount: ko.observable(false),
    IsFreezeAccount: ko.observable(false),
    Rate: ko.observable(0.00),
    AmountUSD: ko.observable(0.00),
    BeneName: ko.observable("-"),
    Bank: ko.observable(BankModel),
    BizSegment: ko.observable(BizSegmentModel),
    DebitCurrency: ko.observable(CurrencyModel),
    ApplicationDate: ko.observable(moment(today).format(config.format.date)),

    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Account: ko.observable(AccountModel),
    IsOtherAccountNumber: ko.observable(false),
    OtherAccountNumber: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(0),
    Channel: ko.observable(ChannelModel),
    ValueDate: ko.observable(moment(today).format(config.format.date)),
    DealNumber: ko.observable(""),
    CustomerDraft: ko.observable(CustomerDraftModel),
    Remarks: ko.observable(""),
    IsDraft: ko.observable(false),
    IsNewCustomer: ko.observable(false),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]),
    AttachmentRemarks: ko.observable()
}
//end Dani

var TransactionLoanModel = {
    IsTopUrgent: ko.observable(0),
    IsTopUrgentChain: ko.observable(false),
    IsNormal: ko.observable(false),
    ApplicationDate: ko.observable(moment(today).format(config.format.date)),
    ExecutionDate: ko.observable(moment(today).format(config.format.date)),
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    Currency: ko.observable(CurrencyModel),
    BizSegment: ko.observable(BizSegmentModel),
    Amount: ko.observable(),
    Channel: ko.observable(ChannelModel),
    ValueDate: ko.observable(moment(today).format(config.format.date)),
    IsDraft: ko.observable(false),
    IsNewCustomer: ko.observable(false),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]),
    Source: ko.observable(SourceModel)
}
function SetDate(dt, ext) {
    var dt2 = dt.setFullYear(dt.getFullYear() + ext);
    return dt2;
}
//26-1-2016 dani
function SetDate2(dt, th, tgl) {
    var dt1 = dt.setFullYear(dt.getFullYear() + th);
    var dt2 = new Date(dt1);
    var dt3 = dt.setDate(dt2.getDate() - 1);
    var dt4 = new Date(dt3);
    return dt4;
}
//26-1-2016 dani end
//Agung
var TransactionUTModel = {
    //these needed in my form

    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    FNACore: ko.observable([FNACoreModel]),
    FunctionType: ko.observable([FunctionTypeModel]),
    AccountType: ko.observable([AccountTypeModel]),
    SolID: ko.observable(),
    CustomerRiskEffectiveDate: ko.observable(moment(new Date).format(config.format.date)),
    RiskScore: ko.observable(),
    RiskProfileExpiryDate: ko.observable(moment(SetDate2(new Date, 1, 1)).format(config.format.date)),
    OperativeAccount: ko.observable(),
    Transaction_Type: ko.observable([Transaction_TypeModel]),
    Investment: ko.observable(),
    CustomerDraft: ko.observable(CustomerDraftModel),
    Remarks: ko.observable(""),
    IsDraft: ko.observable(false),
    IsNewCustomer: ko.observable(false),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]),

    //MutualFunds: ko.observableArray([]),
    //Subcriptions: ko.observableArray([]),
    //Redemptions: ko.observableArray([]),
    //Switchings: ko.observableArray([]),
    MutualFundForms: ko.observableArray([]),
    MutualFund: ko.observable(),
    MutualFundCode: ko.observable(),
    MutualCurrency: ko.observable(),
    MutualAmount: ko.observable(),
    MutualSelected: ko.observable(),
    MutualPartial: ko.observable(),
    MutualUnitNumber: ko.observable(),
    MutualFundSwitchFrom: ko.observable(),
    MutualFundCodeSwitchFrom: ko.observable(),
    MutualFundSwitchTo: ko.observable(),
    MutualFundCodeSwitchTo: ko.observable(),

    UTJoin: ko.observableArray([]),
    Join: ko.observable(JoinModel),
    CustomerJoin: ko.observable(CustomerModel),
    SolIDJoin: ko.observable(),
    CustomerRiskEffectiveDateJoin: ko.observable(moment(today).format(config.format.date)),
    RiskScoreJoin: ko.observable(),
    RiskProfileExpiryDateJoin: ko.observable(moment(today).format(config.format.date)),

    MutualFundList: ko.observable(FundListModel),
    MutualFundSwitchFrom: ko.observable(FundListModel),
    MutualFundSwitchTo: ko.observable(FundListModel),
    InvestmentList: ko.observable(InvestmentListModel),
    SolIDList: ko.observable(SolIDModel),
    IsBringupTask: ko.observable(false),
    AttachmentRemarks: ko.observable() // add by adi
}
//End Agung

var TransactionModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    IsTopUrgentChain: ko.observable(false),
    IsNormal: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    IsNewCustomer: ko.observable(false),
    Product: ko.observable(ProductModel),
    ProductType: ko.observable(ProductTypeModel),
    LLD: ko.observable(LLDModel),
    UnderlyingDoc: ko.observable(UnderlyingDocModel),
    OtherUnderlyingDoc: ko.observable(),
    Currency: ko.observable(CurrencyModel),
    Sundry: ko.observable(SundryModel),
    Nostro: ko.observable(NostroModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    TrxRate: ko.observable(),
    AmountUSD: ko.observable(0.00),
    Channel: ko.observable(ChannelModel),
    Account: ko.observable(AccountModel),
    ApplicationDate: ko.observable(moment(today).format(config.format.date)),
    ExecutionDate: ko.observable(moment(today).format(config.format.date)),
    BizSegment: ko.observable(BizSegmentModel),
    Bank: ko.observable(BankModel),
    BankCharges: ko.observable(ChargesModel),
    AgentCharges: ko.observable(ChargesModel),
    Type: ko.observable(20),
    IsResident: ko.observable(true),
    IsCitizen: ko.observable(true),
    PaymentDetails: ko.observable(null),
    IsSignatureVerified: ko.observable(false),
    IsDormantAccount: ko.observable(false),
    IsFreezeAccount: ko.observable(false),
    Others: ko.observable(null),
    IsDraft: ko.observable(false),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]),
    DetailCompliance: ko.observable(),
    BeneName: ko.observable(),
    BeneAccNumber: ko.observable(),
    TZNumber: ko.observable(),
    TotalTransFX: ko.observable(0.00),
    TotalUtilization: ko.observable(0.00),
    utilizationAmount: ko.observable(0.00),
    underlyings: ko.observableArray([]),
    DFUnderlyings: ko.observableArray([]),
    IsOtherBeneBank: ko.observable(true),
    OtherBeneBankName: ko.observable(),
    OtherBeneBankSwift: ko.observable(),
    UnderlyingDocID: ko.observable(),
    CustomerDraft: ko.observable(CustomerDraftModel),
    IsOtherAccountNumber: ko.observable(false),
    OtherAccountNumber: ko.observable(),
    DebitCurrency: ko.observable(CurrencyModel),
    IsJointAccount: ko.observable(),
    BeneficiaryBusines: ko.observable(BeneficiaryBusinesModel),
    IsBeneficiaryResident: ko.observable(null),
    AttachmentRemarks: ko.observable(),
    //IPE
    LLDDocument: ko.observable(LLDDocument),
    LLDUnderlyingAmount: ko.observable(),
    ChargingAccountName: ko.observable(),
    ChargingAccountBank: ko.observable(),
    BeneficiaryAddress: ko.observable(),
    AccountNumber: ko.observable(),
    Compliance: ko.observable(FXComplianceModel),
    CCY: ko.observable(),
    BranchID: ko.observable(),
    CityID: ko.observable(),
    ChargingACCNumber: ko.observable(),
    PaymentMode: ko.observable(PaymentMode),
    ChargingAccountCurrency: ko.observable(),
    PaymentModeStatus: ko.observable(),
    selectedOptionId: ko.observable(null),
    ModePayment: ko.observable(),
    Branch: ko.observable(BranchModel),
    City: ko.observable(CityModel),
    //ChargingAccountCurrency: ko.observable(ChargingAccountCurrencyModel),
    //Tambah Ipe
    BeneficiaryCountry: ko.observable(BeneficiaryCountryModel),
    TransactionRelationship: ko.observable(TransactionRelationshipModel),
    CBGCustomer: ko.observable(CBGCustomerModel),
    TransactionUsingDebitSundry: ko.observable(TransactionUsingDebitSundryModel),
    IsUnderlyingUtilizeStatus: ko.observable(),
    //aridya add 20161011 for SKN BULK ~OFFLINE~
    Remarks: ko.observable(),
    //Rizki - Double transaction
    DoubleTrxApproveBy: ko.observable(),
    DoubleTrxApplicationID: ko.observable(),
    Source: ko.observable(SourceModel),
    MandatoryNotValidate: ko.observable(MandatoryNotValidate),
    //Tambah Agung
    Rounding: ko.observable(),
    IsIPETMO: ko.observable(false),
    IsIPETMOPartial: ko.observable(false),
    TransactionDealID: ko.observable(),
    Instruction: ko.observable(false),
    StatementLetter: ko.observable(false),
    Underlying: ko.observable(false),
    DFAmount: ko.observable(),
    UpdateDFAmount: ko.observable(false),
    //End
};

//Agung
var ParameterUT = {
    FNACores: ko.observableArray([FNACoreModel]),
    FunctionTypes: ko.observableArray([FunctionTypeModel]),
    AccountTypes: ko.observableArray([AccountTypeModel]),
    TransactionTypes: ko.observableArray([Transaction_TypeModel]),
    //Tambah Agung
    Joins: ko.observableArray([{ ID: false, Name: "And" }, { ID: true, Name: "Or" }])
    //End
}
//End Agung

//Started by Haqi
var SelectedDDL = {
    IdentityTypeIDs: ko.observable(null),
    IdentityTypeIDs2: ko.observable(null),
    IdentityTypeIDs3: ko.observable(null),
    IdentityTypeIDs4: ko.observable(null),
    FundSources: ko.observable(null),
    NetAssets: ko.observable(null),
    MonthlyIncomes: ko.observable(null),
    MonthlyExtraIncomes: ko.observable(null),
    Jobs: ko.observable(null),
    AccountPurposes: ko.observable(null),
    IncomeForecasts: ko.observable(null),
    OutcomeForecasts: ko.observable(null),
    TransactionForecasts: ko.observable(null),
    RequestType: ko.observable(null),
    SubType: ko.observable(null),
};
var AccountNumberLienUnlienModel = {
    AccountNumber: ko.observable(),
    IsJointAccount: ko.observable(),
    IsSelected: ko.observable(false),
    LienUnlien: ko.observable()
}
var CIFTransactionModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    CustomerNameRetailCIF: ko.observable(),
    CustomerCIFRetailCIF: ko.observable(),
    IsNewCustomer: ko.observable(false),
    Product: ko.observable(ProductModel),
    TransactionType: ko.observable(TransactionTypeModel),
    MaintenanceType: ko.observable(MaintenanceTypeModel),
    TransactionSubType: ko.observable(TransactionSubTypeModel),
    DocumentsCIF: ko.observableArray([]),
    AddJoinTableCustomerCIF: ko.observableArray([]),
    AddJoinTableFFDAcountCIF: ko.observableArray([]),
    AddJoinTableAccountCIF: ko.observableArray([]),
    AddJoinTableDormantCIF: ko.observableArray([]),
    AddJoinTableFreezeUnfreezeCIF: ko.observableArray([]),
    Currency: ko.observable(CurrencyModel),
    IsDraft: ko.observable(false),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    StaffTagging: ko.observable(TagUntagModel),
    StaffID: ko.observable(),
    ATMNumber: ko.observable(),
    RetailCIFCBO: ko.observable(RetailCIFCBOModel),
    IsLOI: ko.observable(),
    IsPOI: ko.observable(),
    IsPOA: ko.observable(),
    BrachRiskRating: ko.observable(BrachRiskRatingModel),
    AccountNumberDormant: ko.observable(),
    BranchName: ko.observable(),
    AccountNumber: ko.observable(),
    IsBringupTask: ko.observable(false),
    CurrencyLien: ko.observable(CurrencyModel),
    AccountNumberLienUnlien: ko.observableArray([AccountNumberLienUnlienModel]),
	AccountTypeLoiPoiID: ko.observable(AccountModel),
    AccountListPOAs: ko.observableArray(AccountListPOAModel),
    CustomerJoinLoiPois: ko.observableArray()
};

var AccountListPOAModel = {
    AccountNumber: ko.observable(),
    CustomerName: ko.observable(),
    CIF: ko.observable(),
    Currency: ko.observable(),
    IsJoin: ko.observable()
}

//End by Haqi

var Parameter = {
    Products: ko.observableArray([ProductModel]),
    ProductType: ko.observableArray([ProductTypeModel]),
    Currencies: ko.observableArray([CurrencyModel]),
    //henggar
    Sundries: ko.observableArray([SundryModel]),
    Nostroes: ko.observableArray([NostroModel]),
    DynamicNostroes: ko.observableArray([NostroModel]),
    //end
    Channels: ko.observableArray([ChannelModel]),
    BizSegments: ko.observableArray([BizSegmentModel]),
    Banks: ko.observableArray([BankModel]),
    LLDs: ko.observableArray([LLDModel]),
    LLDDocuments: ko.observableArray([LLDDocument]),
    BankCharges: ko.observableArray([ChargesModel]),
    AgentCharges: ko.observableArray([ChargesModel]),
    FXCompliances: ko.observableArray([FXComplianceModel]),
    DocumentTypes: ko.observableArray([DocumentTypeModel]),
    DocumentPurposes: ko.observableArray([DocumentPurposeModel]),
    DocumentPurposesTMO: ko.observableArray([DocumentPurposeModel]),
    DocumentPurposesNonTMO: ko.observableArray([DocumentPurposeModel]),
    UnderlyingDocs: ko.observableArray([UnderlyingDocModel]),
    BeneficiaryBusiness: ko.observableArray([BeneficiaryBusinesModel]),

    //Andi
    TransactionType: ko.observableArray([TransactionTypeModel]),
    FDRemarks: ko.observableArray([FDRemarksModel]),
    //End Andi

    //Started by Haqi
    TransactionTypes: ko.observableArray([TransactionTypeModel]),
    MaintenanceTypes: ko.observableArray([MaintenanceTypeModel]),
    TransactionSubTypes: ko.observableArray([TransactionSubTypeModel]),
    MaritalStatusIDs: ko.observableArray([ParameterSystemModel]),
    CellPhoneMethodIDs: ko.observableArray([ParameterSystemModel]),
    HomePhoneMethodIDs: ko.observableArray([ParameterSystemModel]),
    OfficePhoneMethodIDs: ko.observableArray([ParameterSystemModel]),
    FaxMethodIDs: ko.observableArray([ParameterSystemModel]),
    IdentityTypeIDs: ko.observableArray([ParameterSystemModel]),
    FundSources: ko.observableArray([ParameterSystemModel]),
    NetAssets: ko.observableArray([ParameterSystemModel]),
    MonthlyIncomes: ko.observableArray([ParameterSystemModel]),
    MonthlyExtraIncomes: ko.observableArray([ParameterSystemModel]),
    Jobs: ko.observableArray([ParameterSystemModel]),
    AccountPurposes: ko.observableArray([ParameterSystemModel]),
    IncomeForecasts: ko.observableArray([ParameterSystemModel]),
    OutcomeForecasts: ko.observableArray([ParameterSystemModel]),
    TransactionForecasts: ko.observableArray([ParameterSystemModel]),
    DispatchModeTypes: ko.observableArray([ParameterSystemModel]),
    RiskRatingResults: ko.observableArray([ParameterSystemModel]),
    DynamicTransactionSubTypes: ko.observableArray([TransactionSubTypeModel]),
    DynamicMaintenanceTypes: ko.observableArray([MaintenanceTypeModel]),
    DynamicTransactionTypes: ko.observableArray([TransactionTypeModel]),
    FXCompliance: ko.observableArray([FXComplianceModel]),
    PaymentModes: ko.observableArray([PaymentMode]),
    ChargingAccountCurrencies: ko.observableArray([ChargingAccountCurrencyModel]),
    BeneficiaryCountry: ko.observableArray([BeneficiaryCountryModel]),
    CBGCustomer: ko.observableArray([{ ID: false, Code: "No" }, { ID: true, Code: "Yes" }]),
    TransactionUsingDebitSundry: ko.observableArray([{ ID: false, Code: "No" }, { ID: true, Code: "Yes" }]),
    TransactionRelationship: ko.observableArray([TransactionRelationshipModel]),
    //end by Haqi
    //Tambah Tag UnTag
    DynamicTagUntages: ko.observableArray([TagUntagModel]),
    Sources: ko.observableArray([Source]),

    //henggar 180417
    Education: ko.observableArray([ParameterSystemModel]),
    Religion: ko.observableArray([ParameterSystemModel]),
    FATCAReviewStatus: ko.observableArray([ParameterSystemModel]),
    FATCACRSStatus: ko.observableArray([ParameterSystemModel]),
    TaxPayer: ko.observableArray([ParameterSystemModel]),
    WithholdingCertificationType: ko.observableArray([ParameterSystemModel]),
    GrossIncomeCCY: ko.observableArray([ParameterSystemModel]),
    TenorFDs: ko.observableArray([]),
    AccountTypes: ko.observableArray([])
};

var SelectedModel = {
    Product: ko.observable(),
    Currency: ko.observable(),
    //20150528-reizvan : Channel default "Original"
    Sundry: ko.observable(),
    Nostro: ko.observable(),
    Channel: ko.observable(),
    Account: ko.observable(),
    BizSegment: ko.observable(),
    Bank: ko.observable(),
    LLD: ko.observable(),
    LLDDocument: ko.observable(),
    UnderlyingDoc: ko.observable(),
    DocumentType: ko.observable(),
    DocumentPurpose: ko.observable(),
    NewCustomer: ko.observable({
        Currency: ko.observable(),
        BizSegment: ko.observable()
    }),
    BankCharges: ko.observable(2),
    AgentCharges: ko.observable(2),
    ProductType: ko.observable(),
    DebitCurrency: ko.observable(),
    ChooseProduct: ko.observable(),
    BeneficiaryBusines: ko.observable(),
    TransactionType: ko.observable(),
    FDRemarks: ko.observable(),
    Join: ko.observable(),
    FNACore: ko.observable(),
    FunctionType: ko.observable(),
    AccountType: ko.observable(),
    Transaction_Type: ko.observable(),
    //End Agung
    //Tag Untage
    TagUntag: ko.observable(),
    //End
    //started by haqi
    //, TransactionType: ko.observable(),
    MaintenanceType: ko.observable(),
    TransactionSubType: ko.observable(),
    CellPhoneMethodID: ko.observable(),
    HomePhoneMethodID: ko.observable(),
    OfficePhoneMethodID: ko.observable(),
    FaxMethodID: ko.observable(),
    MaritalStatusID: ko.observable(),
    IdentityTypeID: ko.observable(),
    IdentityTypeID2: ko.observable(),
    IdentityTypeID3: ko.observable(),
    IdentityTypeID4: ko.observable(),
    FundSource: ko.observable(),
    NetAsset: ko.observable(),
    MonthlyIncome: ko.observable(),
    MonthlyExtraIncome: ko.observable(),
    Job: ko.observable(),
    AccountPurpose: ko.observable(),
    IncomeForecast: ko.observable(),
    OutcomeForecast: ko.observable(),
    TransactionForecast: ko.observable(),
    DispatchModeType: ko.observable(),
    RiskRatingResult: ko.observable(),
    cifAccNumber: ko.observable(),
    FXCompliance: ko.observable(),
    PaymentMode: ko.observable(),
    PaymentModeStatus: ko.observable(),
    ChargingAccountCurrency: ko.observable(),
    BeneficiaryCountry: ko.observable(),
    CBGCustomer: ko.observable(),
    TransactionUsingDebitSundry: ko.observable(),
    TransactionRelationship: ko.observable(),
    Source: ko.observable(),
    //end by haqi
    //henggar 180417
    Education: ko.observable(),
    Religion: ko.observable(),
    FATCAReviewStatus: ko.observable(),
    FATCACRSStatus: ko.observable(),
    TaxPayer: ko.observable(),
    WithholdingCertificationType: ko.observable(),
    GrossIncomeCCY: ko.observable(),
    //end
    CurrencyLien: ko.observable(),
    TenorFD: ko.observable()
};

var BankBranchModel = {
    CityCode: ko.observable(),
    CityDescription: ko.observable(),
    CityID: ko.observable(),
    Code: ko.observable(),
    ID: ko.observable(),
    Name: ko.observable(),
};


//telephone
var SelularPhoneNumberModel = function (RetailCIFCBOContactUpdateID, PhoneNumber, CountryCode, CityCode, UpdatePhoneNumber, UpdateCountryCode, UpdateCityCode, ContactType, ActionType) {
    RetailCIFCBOContactUpdateID = ko.observable(),
    PhoneNumber = ko.observable(''),
    CountryCode = ko.observable(),
    CityCode = ko.observable(),
    UpdatePhoneNumber = ko.observable(''),
    UpdateCountryCode = ko.observable(''),
    UpdateCityCode = ko.observable(''),
    ContactType = ko.observable('Selular'),
    ActionType = ko.observable(0)
};
var OfficePhoneNumberModel = function (RetailCIFCBOContactUpdateID, PhoneNumber, CountryCode, CityCode, UpdatePhoneNumber, UpdateCountryCode, UpdateCityCode, ContactType, ActionType) {
    RetailCIFCBOContactUpdateID = ko.observable(),
    PhoneNumber = ko.observable(''),
    CountryCode = ko.observable(),
    CityCode = ko.observable(),
    UpdatePhoneNumber = ko.observable(''),
    UpdateCountryCode = ko.observable(''),
    UpdateCityCode = ko.observable(''),
    ContactType = ko.observable('office'),
    ActionType = ko.observable(0)
};
var HomePhoneNumberModel = function (RetailCIFCBOContactUpdateID, PhoneNumber, CountryCode, CityCode, UpdatePhoneNumber, UpdateCountryCode, UpdateCityCode, ContactType, ActionType) {
    RetailCIFCBOContactUpdateID = ko.observable(),
    PhoneNumber = ko.observable(''),
    CountryCode = ko.observable(),
    CityCode = ko.observable(),
    UpdatePhoneNumber = ko.observable(''),
    UpdateCountryCode = ko.observable(''),
    UpdateCityCode = ko.observable(''),
    ContactType = ko.observable('Home'),
    ActionType = ko.observable(0)
};
var CustomerJoinLoiPoiModel = function (CIF, CustomerName) {
    CustomerName = ko.observable(''),
    CIF = ko.observable('')
};

//Start View Model
var ViewModel = function () {

    var self = this;
    //add by fandi
    self.IsPPUHeadMaker = ko.observable(false);
    self.IsPPURole = ko.observable(false);
    //end
    self.FormatNumber = function (num) {
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    }
    self.SetPartial = function (isPartial) {
        if (isPartial == true)
            return "Partial";
        else if (isPartial == false) return "Full";
        else return "";
    }
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);
        if (date == '1970/01/01 00:00:00' || date == null) {
            return "";
        }

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };
    //dodit@2014.11.14:Add new function to know the role
    self.DraftID = ko.observable(0);

    self.IsRole = function (name) {
        var result = false;
        $.each($.cookie(api.cookie.spUser).Roles, function (index, item) {
            if (Const_RoleName[item.ID] == name) result = true;
        });
        return result;
    };

    self.SPUser = ko.observable();
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(OnSuccessToken, OnError);
    } else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);
            self.SPUser(ko.mapping.toJS(spUser));
        }
    }

    self.selectedBranchID = ko.observable(); //add aridya 20160928 handle enter key on branch autocomplete

    // Sharepoint User
    //self.SPUser = ko.observable();
    self.IsEditTableUnderlying = ko.observable(true);
    // rate param
    self.Rate = ko.observable();
    self.TrxRate = ko.observable();
    self.LLDUnderlyingAmount = ko.observable();
    // temp amount_u for bulk underlying // add 2015.03.09
    self.tempAmountBulk = ko.observable(0);
    // Is check attachments
    self.IsFXTransactionAttach = ko.observable(false);
    self.IsUploading = ko.observable(false);
    self.IsUploaded = ko.observable(false);
    self.SelectingUnderlying = ko.observable(false);
    self.BeneAccNumberMask = ko.observable();
    self.IsLoadDraft = ko.observable(false);
    self.IsEmptyAccountNumber = ko.observable(false);
    self.IsEmptyChargingAccount = ko.observable(false);
    self.IsLoadDraftCIF = ko.observable(false);

    //Andi,28 maret 2016, Add TouchTimeStartDate
    self.TouchTimeStartDate = ko.observable(new Date().toUTCString());

    // FX FCY-IDR & IDR-FCY
    self.IsFxTransaction = ko.observable(false);
    self.IsFxTransactionToIDR = ko.observable(false);

    //change sundry henggar
    self.IsSundryElseIDR = ko.observable(false);
    self.IsSundryFilled = ko.observable(true);
    self.IsBeneFilled = ko.observable(true);
    self.IsSundryStarFalse = ko.observable(false);
    //disable bene bank filled if product selected over booking
    self.IsBeneBankFilled = ko.observable(false);
    self.BeneAccNumberFilled = ko.observable(false);

    //aridya 20170208 unutk handle username double transaction
    self.UserApproveDoubleTransaction = ko.observable();
    //end aridya

    //validation IPE
    self.AmountTZ = ko.observable(0);
    self.IsLimit = ko.observable(false);
    self.RoundingLLD = ko.observable();
    self.RoundingValidation = ko.observable();
    self.RoundingValue = ko.observable();
    self.IsRoundingLLD = ko.observable(false);
    self.IsLLDUndelyingAmount = ko.observable(false);
    self.IsLLDUndelyingDocument = ko.observable(false);
    self.IsLLD = ko.observable(true);
    self.IsLLDDoc = ko.observable(true);
    self.IsLLDUnderlying = ko.observable(true);
    self.IsCompliance = ko.observable(true);
    self.IsUnderlying = ko.observable(true);
    self.IsRate = ko.observable(true);
    self.IsTrxRate = ko.observable(true);
    self.IsTZNumber = ko.observable(true);
    self.IsBranchCode = ko.observable(true);
    self.IsBeneAccNumber = ko.observable(true);
    self.IsDebit = ko.observable(true);
    self.IsChargingMandotary = ko.observable(true);
    self.BeneCountrybyProduct = ko.observable(true);
    self.BeneChargebyProduct = ko.observable(true);
    self.BeneBusinessbyProduct = ko.observable(true);
    self.TrxRelationbyProduct = ko.observable(true);
    self.IsCBGCustomer = ko.observable(true);
    self.IsTransactionUsingDebitSundry = ko.observable(true);

    //editable file attach
    self.IsEditableDocument = ko.observable(false);
    // ddl Joint Accounts
    self.CIFJointAccounts = ko.observableArray([{ ID: false, Name: "Single" }, { ID: true, Name: "Join" }]);
    self.DynamicAccounts = ko.observableArray([]);
    self.JointAccountNumbers = ko.observable([]);
    self.IsJointAccount = ko.observable(false);
    // input form controls


    self.IsEditable = ko.observable(false);
    self.IsValid = ko.observable(false);

    // optional other for bank
    self.IsOtherBank = ko.observable(false);
    self.EnableCCY = ko.observable(false);
    self.IsBranchFilled = ko.observable(false);
    self.EnableUnderlying = ko.observable(false);

    // Parameters
    self.Parameter = ko.observable(Parameter);

    // Disable Product Type
    self.IsProductType = ko.observable(false);

    // Main Model
    self.TransactionModel = ko.observable(TransactionModel);
    //Andi
    self.FDModel = ko.observable(FDModel);
    self.IsDocsComplete = ko.observable(false);
    //End Andi
    self.TransactionLoanModel = ko.observable(TransactionLoanModel);
    self.TransactionUTModel = ko.observable(TransactionUTModel);
    //Dani
    self.TransactionTMOModel = ko.observable(TransactionTMOModel);
    self.IsCSO = ko.observable(false);
    //End Dani
    self.IsValidateResident = ko.observable(false);
    // tambah agung
    //self.ChangedIsResident = ko.computed(function () {
    //    if (self.TransactionModel().IsResident() != null) {
    //        GetTotalTransaction();
    //    }

    //});

    //add henggar 210417
    self.IsStatusCRS = ko.observable(false);
    self.IsTaxPayerType = ko.observable(false);
    //end

    //add by haqi
    self.CIFTransactionModel = ko.observable(CIFTransactionModel);
    self.SelectedDDL = ko.observable(SelectedDDL);

    //add henggar 
    self.LimitProduct = ko.observableArray([LimitProductModel])
    self.MidrateData = ko.observableArray([MidrateDataModel])
    self.TBOData = ko.observableArray([TBOTransactionModel]);

    //Add Rizki - 2015-12-01
    self.IsLoadDraftPayment = ko.observable(false);
    self.IsStatusDraft = ko.observable(false);

    //Agung
    self.ParameterUT = ko.observable(ParameterUT);
    self.IsSP = ko.observable(false);
    self.IsSubscription = ko.observable(false);
    self.IsRedemption = ko.observable(false);
    self.IsSwitching = ko.observable(false);
    //End Agung


    // Double Transaction
    self.DoubleTransactions = ko.observableArray([]);
    self.JointAccounts = ko.observableArray([]);
    //self.ThresholdValue = ko.observable(ThresholdModel);
    // PBI Threshol
    self.Rounding = ko.observable(0);
    self.ParameterRounding = ko.observable(0);
    self.ThresholdType = ko.observable();
    self.ThresholdValue = ko.observable(0);
    self.IsHitThreshold = ko.observable(false);
    self.IsNoThresholdValue = ko.observable(false);
    //self.RoundingValue = ko.observable(0);

    //new underlying type
    self.isNewUnderlying = ko.observable(false);

    // Validation User
    self.UserValidation = ko.observable({
        UserID: ko.observable(),
        Password: ko.observable(),
        Error: ko.observable()
    });

    // Options selected value
    self.Selected = ko.observable(SelectedModel);

    self.BankBranch = ko.observableArray([BankBranchModel]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyings = ko.observableArray([]);
    //Tambah Agung
    self.SelectingInstruction = ko.observable(true);
    self.CustomerUnderlyingsTMO = ko.observableArray([]);
    self.CustomerUnderlyingsTMOALL = ko.observableArray([]);
    self.DFAmount = ko.observable(0);
    self.TotalUtilization = ko.observable(0);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyingProformas = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyingFiles = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerAttachUnderlyings = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerBulkUnderlyings = ko.observableArray([]); // add 2015.03.09

    // Temporary documents
    self.Documents = ko.observableArray([]);
    self.DocumentPath = ko.observable();
    self.DocumentType = ko.observable();
    self.DocumentPurpose = ko.observable();
    self.DocumentFileName = ko.observable();

    //started by haqi
    self.DocumentsCIF = ko.observableArray([]);
    self.TempAttachemntDocuments = ko.observableArray([]);
    self.TempAttachemntDocument = ko.observable(CustomerModelModal);
    self.TempFFDAccounts = ko.observableArray([]);
    self.TempFFDAccount = ko.observable(FFDAccountModel);
    self.TempAddAccounts = ko.observableArray([]);
    self.TempAddAccount = ko.observable(FFDAccountModel);
    self.TempDormantAccounts = ko.observableArray([]);
    self.TempDormantAccountsGet = ko.observableArray([]);
    self.TempDormantAccountsSet = ko.observableArray([]);
    self.TempDormantAccount = ko.observable(DormantAccountModel);
    self.TempFreezeAccounts = ko.observableArray([]);
    self.TempFreezeAccountsGet = ko.observableArray([]);
    self.TempFreezeAccountsSet = ko.observableArray([]);
    self.CIFAccountsNumber = ko.observableArray([]);
    //self.TempFreezeAccount = ko.observable(FreezeUnfreezeModel),
    //self.IsAddFFDAccount = ko.observable(false);
    //end by haqi
    self.GetTotalTransaction = function () {
        GetTotalTransaction();
    }
    self.OnChangedBene = function () {
        OnChangedBene();
    }
    // set checkbox topurgent
    self.setTopUrgentChainFD = ko.computed(function () {
        if (self.FDModel().IsTopUrgent()) {
            self.FDModel().IsTopUrgentChain(0);
            self.FDModel().IsNormal(0);
        }
    });
    self.setTopUrgentFD = ko.computed(function () {
        if (self.FDModel().IsTopUrgentChain()) {
            self.FDModel().IsTopUrgent(0);
            self.FDModel().IsNormal(0);
        }
    });
    self.setNormalFD = ko.computed(function () {
        if (self.FDModel().IsNormal()) {
            self.FDModel().IsTopUrgent(0);
            self.FDModel().IsTopUrgentChain(0);
        }
    });

    self.setTopUrgentChainloan = ko.computed(function () {
        if (self.TransactionLoanModel().IsTopUrgent()) {
            self.TransactionLoanModel().IsTopUrgentChain(0);
        }
    });
    self.setTopUrgentloan = ko.computed(function () {
        if (self.TransactionLoanModel().IsTopUrgentChain()) {
            self.TransactionLoanModel().IsTopUrgent(0);
        }
    });
    self.setNormalLoan = ko.computed(function () {
        if (self.TransactionLoanModel().IsNormal()) {
            self.TransactionLoanModel().IsTopUrgentChain(0);
            self.TransactionLoanModel().IsTopUrgent(0);
        }
    });

    //Set Checkbox Retail CIF
    //add henggar checkbox retail cif
    self.setIsNameMotherMaiden = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsMotherMaiden()) {
                self.CIFTransactionModel().RetailCIFCBO().MotherMaiden('');
            }
        }
    });
    self.setIsDataUBO = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsDataUBO()) {
                self.CIFTransactionModel().RetailCIFCBO().DataUBOCif('');
                self.CIFTransactionModel().RetailCIFCBO().DataUBOName('');
                self.CIFTransactionModel().RetailCIFCBO().DataUBORelationship('');
            }
        }
    });
    self.setIsReligion = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsReligion()) {
                self.Selected().Religion('');
                self.CIFTransactionModel().RetailCIFCBO().Religion('');
            }
        }
    });
    self.setIsEducation = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsEducation()) {
                self.Selected().Education('');
                self.CIFTransactionModel().RetailCIFCBO().Education('');
            }
        }
    });
    self.setIsTransparansiDataPenawaranProduct = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsTransparansiDataPenawaranProduct()) {
                self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsTransparansiPenggunaan(false);
                self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPenawaranProduct(false);
                self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsSignature(false);
                self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsStampDuty(false);
            }
        }
    });
    self.setIsSLIK = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsSLIK()) {
                self.Selected().Currency('');
                self.CIFTransactionModel().RetailCIFCBO().GrosIncomeccy('');
                self.CIFTransactionModel().RetailCIFCBO().GrossIncomeAmount('');
                self.CIFTransactionModel().RetailCIFCBO().GrossCifSpouse('');
                self.CIFTransactionModel().RetailCIFCBO().GrossSpouseName('');
                self.CIFTransactionModel().RetailCIFCBO().GrossSpouseRelation('');
            }
        }
    });
    self.IsChangeSignature = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsChangeSignature()) {
                self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().ChangeSignature(false);
            }
        }
    });
    //end

    self.setIsNameMaintenance = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsNameMaintenance()) {
                self.CIFTransactionModel().RetailCIFCBO().Name('');
            }
        }
    });
    self.setIsIdentityTypeMaintenance = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsIdentityTypeMaintenance()) {
                self.Selected().IdentityTypeID('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityNumber('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityStartDate('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityEndDate('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityAddress('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCity('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityProvince('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCountry('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode('');
                self.Selected().IdentityTypeID2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityNumber2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityStartDate2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityEndDate2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityAddress2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCity2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityProvince2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCountry2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode2('');
                self.Selected().IdentityTypeID3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityNumber3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityStartDate3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityEndDate3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityAddress3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCity3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityProvince3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCountry3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode3('');
                self.Selected().IdentityTypeID4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityNumber4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityStartDate4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityEndDate4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityAddress4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCity4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityProvince4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCountry4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode4('');
                //self.CIFTransactionModel().RetailCIFCBO().DispatchModeOther('');
                self.Add1 = ko.observable(true);
                self.Add2 = ko.observable(true);
                self.Add3 = ko.observable(true);
                self.Add4 = ko.observable(true);
            }
        }
    });
    self.setIsNPWPMaintenance = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsNPWPMaintenance()) {
                self.CIFTransactionModel().RetailCIFCBO().NPWPNumber('');
                self.CIFTransactionModel().RetailCIFCBO().IsNPWPReceived(false);
            }
        }
    });
    self.setIsMaritalStatusMaintenance = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsMaritalStatusMaintenance()) {
                self.Selected().MaritalStatusID('');
                self.CIFTransactionModel().RetailCIFCBO().SpouseName('');
            }
        }
    });
    self.setIsCorrespondenceMaintenance = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsCorrespondenceMaintenance()) {
                self.CIFTransactionModel().RetailCIFCBO().CorrespondenseAddress('');
                self.CIFTransactionModel().RetailCIFCBO().IsCorrespondenseToEmail(false);
                self.CIFTransactionModel().RetailCIFCBO().CorrespondenseCountry('');
                self.CIFTransactionModel().RetailCIFCBO().CorrespondensePostalCode('');
                self.CIFTransactionModel().RetailCIFCBO().CorrespondenseCity('');
                self.CIFTransactionModel().RetailCIFCBO().CorrespondenseProvince('');
                self.CIFTransactionModel().RetailCIFCBO().CorrespondenseKelurahan('');
                self.CIFTransactionModel().RetailCIFCBO().CorrespondenseKecamatan('');
                self.CIFTransactionModel().RetailCIFCBO().CorrespondenseAddress('');
            }
        }
    });
    self.setIsIdentityAddressMaintenance = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsIdentityAddressMaintenance()) {
                self.Selected().IdentityTypeID('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityNumber('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityStartDate('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityEndDate('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityAddress('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCity('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityProvince('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCountry('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode('');
                self.Selected().IdentityTypeID2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityNumber2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityStartDate2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityEndDate2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityAddress2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCity2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityProvince2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCountry2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode2('');
                self.Selected().IdentityTypeID3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityNumber3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityStartDate3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityEndDate3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityAddress3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCity3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityProvince3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCountry3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode3('');
                self.Selected().IdentityTypeID4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityNumber4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityStartDate4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityEndDate4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityAddress4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCity4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityProvince4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCountry4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode4('');
                self.CIFTransactionModel().RetailCIFCBO().Nationality('');
            }
        }
    });
    self.setIsOfficeAddressMaintenance = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsOfficeAddressMaintenance()) {
                self.CIFTransactionModel().RetailCIFCBO().OfficeCountry('');
                self.CIFTransactionModel().RetailCIFCBO().OfficePostalCode('');
                self.CIFTransactionModel().RetailCIFCBO().OfficeCity('');
                self.CIFTransactionModel().RetailCIFCBO().OfficeProvince('');
                self.CIFTransactionModel().RetailCIFCBO().OfficeKelurahan('');
                self.CIFTransactionModel().RetailCIFCBO().OfficeKecamatan('');
                self.CIFTransactionModel().RetailCIFCBO().OfficeAddress('');
            }
        }
    });
    self.setIsCorrespondenseAddressMaintenance = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsCorrespondenseAddressMaintenance()) {
                self.CIFTransactionModel().RetailCIFCBO().CorrespondenseCountry('');
                self.CIFTransactionModel().RetailCIFCBO().CorrespondensePostalCode('');
                self.CIFTransactionModel().RetailCIFCBO().CorrespondenseCity('');
                self.CIFTransactionModel().RetailCIFCBO().CorrespondenseProvince('');
                self.CIFTransactionModel().RetailCIFCBO().CorrespondenseKelurahan('');
                self.CIFTransactionModel().RetailCIFCBO().CorrespondenseKecamatan('');
                self.CIFTransactionModel().RetailCIFCBO().CorrespondenseAddress('');
            }
        }
    });
    self.setIsPhoneFaxEmailMaintenance = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPhoneFaxEmailMaintenance()) {
                self.CIFTransactionModel().RetailCIFCBO().EmailAddress('');
                self.CIFTransactionModel().RetailCIFCBO().Fax('');
                self.CIFTransactionModel().RetailCIFCBO().UpdatedFax('');
                self.CIFTransactionModel().RetailCIFCBO().OfficePhone('');
                self.CIFTransactionModel().RetailCIFCBO().UpdatedOfficePhone('');
                self.CIFTransactionModel().RetailCIFCBO().HomePhone('');
                self.CIFTransactionModel().RetailCIFCBO().UpdatedHomePhone('');
                self.CIFTransactionModel().RetailCIFCBO().CellPhone('');
                self.CIFTransactionModel().RetailCIFCBO().UpdatedCellPhone('');

            }
        }
    });
    self.setIsCorrespondenseToEmail = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().IsCorrespondenseToEmail()) {
                self.Selected().OfficePhoneMethodID('');
                self.Selected().FaxMethodID('');
                self.Selected().HomePhoneMethodID('');
                self.Selected().CellPhoneMethodID('');
                self.CIFTransactionModel().RetailCIFCBO().CellPhone('');
                self.CIFTransactionModel().RetailCIFCBO().Fax('');
                self.CIFTransactionModel().RetailCIFCBO().UpdatedFax('');
                self.CIFTransactionModel().RetailCIFCBO().OfficePhone('');
                self.CIFTransactionModel().RetailCIFCBO().UpdatedOfficePhone('');
                self.CIFTransactionModel().RetailCIFCBO().HomePhone('');
                self.CIFTransactionModel().RetailCIFCBO().UpdatedHomePhone('');
                self.CIFTransactionModel().RetailCIFCBO().CellPhone('');
                self.CIFTransactionModel().RetailCIFCBO().UpdatedCellPhone('');

            }
        }
    });
    self.setIsNationalityMaintenance = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsNationalityMaintenance()) {
                self.Selected().IdentityTypeID('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityNumber('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityStartDate('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityEndDate('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityAddress('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCity('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityProvince('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCountry('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode('');
                self.Selected().IdentityTypeID2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityNumber2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityStartDate2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityEndDate2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityAddress2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCity2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityProvince2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCountry2('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode2('');
                self.Selected().IdentityTypeID3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityNumber3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityStartDate3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityEndDate3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityAddress3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCity3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityProvince3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCountry3('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode3('');
                self.Selected().IdentityTypeID4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityNumber4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityStartDate4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityEndDate4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityAddress4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCity4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityProvince4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityCountry4('');
                self.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode4('');
                self.CIFTransactionModel().RetailCIFCBO().Nationality('');
            }
        }
    });
    self.setIsFundSourceMaintenance = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsFundSourceMaintenance()) {
                self.CIFTransactionModel().RetailCIFCBO().UBOName('');
                self.CIFTransactionModel().RetailCIFCBO().UBOIdentityType('');
                self.CIFTransactionModel().RetailCIFCBO().UBOPhone('');
                self.CIFTransactionModel().RetailCIFCBO().UBOJob('');
                self.Selected().FundSource('');
            }
        }
    });
    self.setIsNetAssetMaintenance = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsNetAssetMaintenance()) {
                self.Selected().NetAsset('');
            }
        }
    });
    self.setIsMonthlyIncomeMaintenance = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsMonthlyIncomeMaintenance()) {
                self.Selected().MonthlyExtraIncome('');
                self.Selected().MonthlyIncome('');
            }
        }
    });
    self.setIsJobMaintenance = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsJobMaintenance()) {
                self.CIFTransactionModel().RetailCIFCBO().IndustryType('');
                self.CIFTransactionModel().RetailCIFCBO().WorkPeriod('');
                self.CIFTransactionModel().RetailCIFCBO().Position('');
                self.CIFTransactionModel().RetailCIFCBO().CompanyName('');
                self.Selected().Job('');
            }
        }
    });
    self.setIsAccountPurposeMaintenance = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsAccountPurposeMaintenance()) {
                self.Selected().AccountPurpose('');
            }
        }
    });
    self.setIsMonthlyTransactionMaintenance = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsMonthlyTransactionMaintenance()) {
                self.Selected().TransactionForecast('');
                self.Selected().OutcomeForecast('');
                self.Selected().IncomeForecast('');
            }
        }
    });
    self.setIsOthers = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsOthers()) {
                self.CIFTransactionModel().RetailCIFCBO().IsSignature(false);
                self.CIFTransactionModel().RetailCIFCBO().IsStampDuty(false);
                self.CIFTransactionModel().RetailCIFCBO().IsPenawaranProductPerbankan(false);
            }
        }
    });
    //End
    //udin
    self.IsValidateResidentChange = function () {
        if (viewModel.TransactionModel().ModePayment() != "BCP2" && viewModel.TransactionModel().ModePayment() != null) {
            FormValidationTrxPayment();
        }
    }
    //end
    self.ModeName = ko.observable("");

    var optionModel = function (id, name) {
        var self = this;
        self.id = id;
        self.name = name;
    }

    //henggar
    self.options = [
        new optionModel(1, "Resident"),
        new optionModel(0, "Non Resident")];

    self.selectedOptionId = ko.observable(null);
    //end

    self.OnChangeModeName = function () {
        var modeID = null;
        var modeName = $("#residentBene option:selected").text();

        if (modeName == "Non Resident") {
            modeID = 0;
        }
        else if (modeName == "Resident") {
            modeID = 1;
        }//edited by dani
        self.selectedOptionId(modeID);

        viewModel.TransactionModel().selectedOptionId(modeID);

        //FormValidationTrxPayment();
    };
    self.OnChangeTenorDraft = function () {
        var quotefd = ko.utils.arrayFirst(viewModel.Parameter().TenorFDs(), function (item) {
            return item.IQuoteID == viewModel.Selected().TenorFD();
        });
        if (quotefd != null) {
            viewModel.FDModel().Tenor(quotefd.Tenor);
            //console.log(quotefd);
            var currencyNow = viewModel.Currencies().Code;
            //console.log(currencyNow);
            //console.log(quotefd[currencyNow]);
            if (quotefd[currencyNow] !== undefined && quotefd[currencyNow] !== '' && quotefd[currencyNow] !== null) {
                viewModel.FDModel().FTPRate(quotefd[currencyNow]);
            } else {
                viewModel.FDModel().FTPRate('-');
            }
        } else {
            viewModel.FDModel().FTPRate('');
            viewModel.FDModel().Tenor('');
        }
    };

    self.OnChangeTrxRate = function () {
        viewModel.TransactionModel().TrxRate(viewModel.TrxRate());
        ValidationRateTZ();

        //validate for df-xxxx
        var typingTimer;
        var $input = $('#tznumber');

        //on keyup, start the countdown
        //$input.on('keyup', function () {
        //    clearTimeout(typingTimer);
        //    typingTimer = setTimeout(doneTyping, 1500);
        //});

        ////on keydown, clear the countdown 
        //$input.on('keydown', function () {
        //    clearTimeout(typingTimer);
        //});

        //function doneTyping() {
        //    var valTzNumber = viewModel.TransactionModel().TZNumber();
        //    var count = 0;
        //    if (valTzNumber != null && valTzNumber != undefined) {
        //        for (x = 0; x < valTzNumber.length; x++) {                    
        //            if (valTzNumber.charAt(x) == "-") {
        //                count++;
        //            }                    
        //        }
        //    }
        //    if (count > 1) {
        //        ShowNotification("Attention", "Please insert Tz Number contain one '-' only, (ex: 'df-0001').", 'gritter-warning', true);
        //        return;
        //    }
        //    if (count < 1) {
        //        ShowNotification("Attention", "Please insert Tz Number contain '-' symbol, (ex: 'df-0001').", 'gritter-warning', true);
        //        return;
        //    }
        //}
    };

    self.OnChangeLLDDocument = function () {
        for (var lld = 0; lld < viewModel.Parameter().LLDDocuments().length ; lld++) {
            if (viewModel.Parameter().LLDDocuments()[lld].LLDDocumentID == viewModel.Selected().LLDDocument()) {
                var llddoc = viewModel.Parameter().LLDDocuments()[lld];
            }
        }
        viewModel.TransactionModel().LLDDocument(llddoc);
        if (viewModel.TransactionModel().LLDDocument() != null) {
            if (viewModel.TransactionModel().LLDDocument().LLDDocumentID == Const_AmountLLD.LLDDocID) {
                viewModel.LLDUnderlyingAmount(formatNumber(viewModel.TransactionModel().Amount())); //change default trxn-amount from eqv-usd
                viewModel.TransactionModel().LLDUnderlyingAmount(viewModel.TransactionModel().Amount()); //change default trxn-amount from eqv-usd
                $('#lldunderlying').data({ ruleRequired: false });
                viewModel.IsLLDUndelyingAmount(false);
                if (viewModel.TransactionModel().Product().Code == "OT") {
                    viewModel.IsRoundingLLD(true);
                    viewModel.RoundingValue(0);
                    if (viewModel.TransactionModel().AmountUSD() != null) {
                        if (viewModel.TransactionModel().LLDUnderlyingAmount() != "") {
                            if (parseFloat(viewModel.TransactionModel().LLDUnderlyingAmount()) > parseFloat(viewModel.TransactionModel().AmountUSD())) {
                                viewModel.RoundingValue(0);
                            } else {
                                viewModel.RoundingValue(formatNumber(viewModel.TransactionModel().Amount() - viewModel.TransactionModel().LLDUnderlyingAmount()));
                            }
                        } else {
                            viewModel.RoundingValue(0);
                        }
                    }
                }
            } else {
                viewModel.LLDUnderlyingAmount("");
                viewModel.TransactionModel().LLDUnderlyingAmount("");
                $('#lldunderlying').data({ ruleRequired: true });
                viewModel.IsLLDUndelyingAmount(true);
                if (viewModel.TransactionModel().Product().Code == "OT") {
                    viewModel.IsRoundingLLD(true);
                    viewModel.RoundingValue(0);
                }
            }
        } else {
            viewModel.LLDUnderlyingAmount("");
            viewModel.TransactionModel().LLDUnderlyingAmount("");
            $('#lldunderlying').data({ ruleRequired: true });
            viewModel.IsLLDUndelyingAmount(true);
            $('#llddoc').data({ ruleRequired: true });
            viewModel.IsLLDUndelyingAmount(true);
            viewModel.IsRoundingLLD(false);
        }
    };

    //8-4-2016 dani
    var ConsParsysTujuanBukaRekeningLainnya = 79;//uat
    var ConsParsysSumberDanaLainnya = 54;//uat
    var ConsParsysPekerjaanProfesional = 72;//uat
    var ConsParsysPekerjaanLainnya = 73;//uat
    self.setIsTujuanBukaRekeningLainnya = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (self.Selected().AccountPurpose() == ConsParsysTujuanBukaRekeningLainnya) {
                self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsTujuanBukaRekeningLainnya(true);
            } else {
                self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsTujuanBukaRekeningLainnya(false);
            }
        }
    });
    self.setIsSumberDanaLainnya = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (self.Selected().FundSource() == ConsParsysSumberDanaLainnya) {
                self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsSumberDanaLainnya(true);
            } else {
                self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsSumberDanaLainnya(false);
            }
        }
    });
    self.setIsPekerjaanProfesional = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (self.Selected().Job() == ConsParsysPekerjaanProfesional) {
                self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPekerjaanProfesional(true);
            } else {
                self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPekerjaanProfesional(false);
            }
        }
    });
    self.setIsPekerjaanLainnya = ko.computed(function () {
        if (self.CIFTransactionModel() != null) {
            if (self.Selected().Job() == ConsParsysPekerjaanLainnya) {
                self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPekerjaanLainnya(true);
            } else {
                self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPekerjaanLainnya(false);
            }
        }
    });

    //end dani


    self.setRiskExpiryDate = ko.computed(function () {
        if (self.TransactionUTModel().CustomerRiskEffectiveDate() != null) {
            //ko.observable(moment(SetDate(new Date, 1)).format(config.format.date))
            var cRiskDate = self.TransactionUTModel().CustomerRiskEffectiveDate();
            var newRiskProfileExpiryDate = SetDate2(new Date(cRiskDate), 1, 1);
            if (newRiskProfileExpiryDate != null)
                self.TransactionUTModel().RiskProfileExpiryDate(moment(newRiskProfileExpiryDate).format(config.format.date));
        }
    });

    //26-1-2016 dani
    self.setRiskExpiryDate2 = ko.computed(function () {
        if (self.TransactionUTModel().CustomerRiskEffectiveDateJoin() != null) {
            //ko.observable(moment(SetDate(new Date, 1)).format(config.format.date))
            var cRiskDate2 = self.TransactionUTModel().CustomerRiskEffectiveDateJoin();
            var newRiskProfileExpiryDate2 = SetDate2(new Date(cRiskDate2), 1, 1);
            if (newRiskProfileExpiryDate2 != null)
                self.TransactionUTModel().RiskProfileExpiryDateJoin(moment(newRiskProfileExpiryDate2).format(config.format.date));
        }
    });
    //26-1-2016 dani end

    // New Customer on change handler
    self.OnChangeJointAcc = function (obj, event) {
        if (obj.IsJointAccount != null) {
            var dataAccounts = ko.utils.arrayFilter(viewModel.TransactionModel().Customer().Accounts, function (dta) {
                var sAccount = dta.IsJointAccount == null ? false : dta.IsJointAccount;
                return sAccount == obj.IsJointAccount()
            });
            if (dataAccounts != null && dataAccounts.length > 0) {
                self.DynamicAccounts([]);
                if (obj.IsJointAccount() == false) {
                    dataAccounts = AddOtherAccounts(dataAccounts);
                }
                self.DynamicAccounts(dataAccounts);
            } else {
                if (!obj.IsJointAccount()) {
                    self.DynamicAccounts([]);
                    if (obj.IsJointAccount() == false) {
                        dataAccounts = AddOtherAccounts(dataAccounts);
                    }
                    self.DynamicAccounts(dataAccounts);
                    //single account               
                    self.UnderlyingFilterShow(1); // show single account
                    self.GetDataUnderlying();
                    self.GetDataAttachFile();
                    UnCheckUnderlyingTable();
                }
            }
            GetTotalTransaction();
        }
    }
    self.OnChangeJointAccUnderlying = function () {
        if (!CustomerUnderlying.IsJointAccount()) {
            CustomerUnderlying.AccountNumber(null);
            //SetStatementA(false, null);
        }
    }

    //aridya 20160907 handle local autocomplete filter
    self.OnBlurBank = function () {
        autoCompleteData = viewModel.TransactionModel().Bank().IBranchBank;
        //viewModel.TransactionModel().ChargingAccountBank(viewModel.TransactionModel().Bank().Description);
    }

    $("#bank-branch-code").keydown(function (event) {
        if (event.which == 13 || event.which == 38 || event.which == 37 || event.which == 39 || event.which == 40 || event.which == 9) {
            //if($(".selector").val().length==0) {
            event.preventDefault();
            return false;
            //}
        }
    });

    //20161129 aridya add kondisi khusus untuk rtgs set ulang branchid 
    self.OnFocusBeneAcc = function (data, event) {
        //if (viewModel.ProductID() == ConsProductID.RTGSProductIDCons) {
        viewModel.TransactionModel().BranchID = viewModel.selectedBranchID();
        //}
    }
    //end add

    self.OnChangeBankBranch = function (data, event) {
        //console.log(viewModel.TransactionModel().Bank().Description);
        //console.log(viewModel.TransactionModel().BranchID);
        //console.log(viewModel.TransactionModel().BranchID());
        if (event.which != 13 && event.which != 38 && event.which != 37 && event.which != 39 && event.which != 40 && event.which != 9) {
            if (!(viewModel.TransactionModel().Bank().Description == "" || viewModel.TransactionModel().Bank().Description == null || typeof viewModel.TransactionModel().Bank().Description === 'undefined')) {
                if (!(typeof viewModel.TransactionModel().BranchID === 'function')) {
                    obj = { filterData: viewModel.TransactionModel().BranchID };
                } else {
                    obj = { filterData: viewModel.TransactionModel().BranchID() };
                }
                //console.log(obj);
            }
        }
        if (event.which == 13) { //|| event.which == 9) {
            //if (viewModel.ProductID() != ConsProductID.RTGSProductIDCons) { //aridya 20161205 disable for bug fixing
            //    $('#bank-branch-code').val(viewModel.TransactionModel().BranchID);
            //}
            viewModel.TransactionModel().BranchID = viewModel.selectedBranchID();
            //console.log(viewModel.TransactionModel().BranchID);
        }
    }

    var checkAutoCompleteBankBranch = function (autoCompleter) {
        return (autoCompleter.Code + '-' + autoCompleter.Name).toLowerCase().includes(this.filterData.toLowerCase());
    }
    //end aridya

    self.OnChangeBank = function () {
        if (viewModel.TransactionModel().Bank().Description == "") {
            //clean data input
            $('#bank-code').val("");
            viewModel.IsOtherBank(false);
            //clean data branch
            $('#bank-branch-code').val("")
            viewModel.TransactionModel().BranchID = undefined;
            //clean data city
            $('#bank-city-code').val("");
            viewModel.IsBranchFilled(false)
            $('#ChAccBank').val("");
        }
    }

    self.OnChangeBeneficiary = function () {
        var beneficiary = ko.utils.arrayFirst(viewModel.Parameter().BeneficiaryBusiness(), function (item) {
            return item.ID == viewModel.Selected().BeneficiaryBusines();
        });
        if (beneficiary != null) {
            viewModel.TransactionModel().BeneficiaryBusines(beneficiary);
        } else {
            viewModel.TransactionModel().BeneficiaryBusines(null);
        }
    }

    self.OnChangeCRSStatus = function () {
        viewModel.IsStatusCRS(false);
        $('#taxayertype').data({ ruleRequired: false });
        var statusCRS = viewModel.Selected().FATCACRSStatus();
        if (statusCRS == 1) {
            viewModel.IsStatusCRS(true);
            $('#taxayertype').data({ ruleRequired: true });
        } else {
            viewModel.IsStatusCRS(false);
            $('#taxayertype').data({ ruleRequired: false });
        }
    }

    self.OnChangeTaxPayerType = function () {
        viewModel.IsTaxPayerType(false);
        $('#taxayerid').data({ ruleRequired: false });
        var statusCRS = viewModel.Selected().TaxPayer();
        if (statusCRS == null) {
            viewModel.IsTaxPayerType(false);
            $('#taxayerid').data({ ruleRequired: false });
        } else {
            viewModel.IsTaxPayerType(true);
            $('#taxayerid').data({ ruleRequired: true });
        }
    }

    self.IsNewCustomerOnChange = function () {

        self.CustomerUnderlyings([]);
        self.CustomerUnderlyingFiles([]);
        self.CustomerAttachUnderlyings([]);

        cifData = '0';
        customerNameData = '';

        self.TransactionModel().Customer().Name = "";
        $("#customer-name").val("");

        // disabled autocomplete if this is a new customer
        $("#customer-name").autocomplete({ disabled: self.IsNewCustomer() });
        self.TransactionModel().Customer().CIF = "";
        $('#cif').val("");
        //add by fandi
        self.TransactionModel().IsNewCustomer(self.IsNewCustomer());
        //end

        //Andi
        self.FDModel().Customer().Name = "";
        $("#customer-nameFD").val("");
        $("#customer-nameFD").autocomplete({ disabled: self.FDModel().IsNewCustomer() });
        self.FDModel().Customer().CIF = "";
        $('#cifFD').val("");
        //End Andi

        //Dani
        self.TransactionTMOModel().Customer().Name = "";
        self.TransactionTMOModel().Customer().CIF = "";//
        $('#ciftmo').val("");
        //End Dani

        //Afif
        self.TransactionLoanModel().Customer().Name = "";
        self.TransactionLoanModel().Customer().CIF = "";//
        $('#cifloan').val("");
        //End Dani

        //Basri
        if ($('#debit-acc-number option[value=-]').length <= 0) {
            $('#debit-acc-number').append('<option value=->-</option>');
        }
        self.IsEmptyAccountNumber(false);
        //End Basri

        //add henggar 10042017
        self.CIFTransactionModel().IsNewCustomer(self.IsNewCustomer());
        self.TransactionUTModel().IsNewCustomer(self.IsNewCustomer());
        //end
        return true;
    };

    // set checkbox topurgent
    self.setTopUrgenChaint = ko.computed(function () {
        if (self.TransactionModel().IsTopUrgent()) {
            self.TransactionModel().IsTopUrgentChain(0);
            self.TransactionModel().IsNormal(0);
        }
    });
    self.setTopUgent = ko.computed(function () {
        if (self.TransactionModel().IsTopUrgentChain()) {
            self.TransactionModel().IsTopUrgent(0);
            self.TransactionModel().IsNormal(0);
        }
    });
    self.setNormal = ko.computed(function () {
        if (self.TransactionModel().IsNormal()) {
            self.TransactionModel().IsTopUrgentChain(0);
            self.TransactionModel().IsTopUrgent(0);
        }
    });

    self.OnChangeAccountCurrency = function () {
        var debitcurrency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) {
            if (viewModel.Selected().Account() == '-') {
                viewModel.TransactionModel().IsOtherAccountNumber(false);
                return item.ID == viewModel.Selected().DebitCurrency();
            }
        });

        if (viewModel.Selected().Account() == '-') {
            self.TransactionModel().ChargingACCcurrency = viewModel.TransactionModel().DebitCurrency().Code;
            //$('#ChAccount-ccy').val(viewModel.TransactionModel().DebitCurrency().Code);
            viewModel.Selected().ChargingAccountCurrency(viewModel.TransactionModel().DebitCurrency().ID);
            self.EnableCCY(true);
        } else {
            $('#ChAccount-ccy').val("");
            self.EnableCCY(false);
        }

        if (debitcurrency != null) {
            viewModel.TransactionModel().DebitCurrency(debitcurrency);
            var AccountNumberSelected = $('#debit-acc-number option:selected').text().trim();
            if (AccountNumberSelected != null && AccountNumberSelected == '-') {
                var IsCurrencyChanged = viewModel.TransactionModel().DebitCurrency().Code;
                var IsFxTransaction = (viewModel.TransactionModel().Currency().Code != 'IDR' && IsCurrencyChanged == 'IDR');
                var IsFxTransactionToIDR = (viewModel.TransactionModel().Currency().Code == 'IDR' && IsCurrencyChanged != 'IDR');
                viewModel.IsFxTransaction(IsFxTransaction);
                viewModel.IsFxTransactionToIDR(IsFxTransactionToIDR);
            }
        }
        if (viewModel.TransactionModel().ModePayment() != "BCP2" && viewModel.TransactionModel().ModePayment() != null) {
            FormValidationTrxPayment();
            ValidationRateTZ();
        }
    }

    self.OnChangeNewCustCurrency = function () {
        if (viewModel.IsNewCustomer() == true) {
            var new_currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().NewCustomer().Currency(); });
            if (new_currency != null) { viewModel.TransactionModel().DebitCurrency(new_currency); }
        }
        if (viewModel.TransactionModel().ModePayment() != "BCP2" && viewModel.TransactionModel().ModePayment() != null) {
            FormValidationTrxPayment();
            ValidationRateTZ();
            ValidationWaiveCharges();
            ValidationCharging();
        }
    }

    self.OnChangeBizSegment = function () {
        if (viewModel.TransactionModel().ModePayment() != "BCP2" && viewModel.TransactionModel().ModePayment() != null) {
            FormValidationTrxPayment();
            ValidationRateTZ();
            ValidationWaiveCharges();
            ValidationCharging();
        }
    }
    //henggar onchange
    self.OnChangeCurrency = function () {
        //if (StatusIPE == "IPE" || StatusIPE == "BCP1") {
        if (viewModel.TransactionModel().Currency() != null) {
            var dataNostro = ko.utils.arrayFilter(viewModel.Parameter().Nostroes(), function (item) {
                return item.Currency.ID == viewModel.TransactionModel().Currency().ID;
            });
            if (dataNostro != null) {
                viewModel.Parameter().DynamicNostroes(dataNostro);
            }
        }

        if (viewModel.TransactionModel().ModePayment() != "BCP2" && viewModel.TransactionModel().ModePayment() != null) {
            if (viewModel.Selected().Currency() != Const_AmountLLD.NonIDRSelected) {
                if (viewModel.TransactionModel().Amount() >= Const_AmountLLD.USDAmount) {
                    viewModel.IsLimit(true);
                } else {
                    viewModel.IsLimit(false);
                    viewModel.TransactionModel().LLDDocument(null);
                    viewModel.Selected().LLDDocument(null);
                }
            } else {
                viewModel.IsLimit(false);
                viewModel.TransactionModel().LLDDocument(null);
                viewModel.Selected().LLDDocument(null);
            }
        }

        var IsSundryElseIDR = (viewModel.TransactionModel().Currency().Code != 'IDR');
        self.IsSundryElseIDR(IsSundryElseIDR);

        if (viewModel.Selected().Currency() != Currency.IDRselected) {
            GetMidrateCurrency();
        }
        if (viewModel.TransactionModel().ModePayment() != "BCP2" && viewModel.TransactionModel().ModePayment() != null) {
            FormValidationTrxPayment();
            ValidationRateTZ();
        }
    }
    //

    self.CurrencyTransactionValidate = function () {
        if (viewModel.TransactionModel().Account().Currency.ID != null || viewModel.TransactionModel().Account().Currency.ID != "undefined") {
            if (viewModel.ProductID() == ConsProductID.SKNProductIDCons || viewModel.ProductID() == ConsProductID.OTTProductIDCons || viewModel.ProductID() == ConsProductID.OverbookingProductIDCons || viewModel.ProductID() == ConsProductID.RTGSProductIDCons) {
                var CurrencyIsIDR = viewModel.Selected().Currency();
                var AccountIsIDR = viewModel.TransactionModel().Account().Currency.ID;

                if (CurrencyIsIDR == optionSelected.IsCurrency && AccountIsIDR != optionSelected.IsAccount) {
                    viewModel.Selected().UnderlyingDoc(optionSelected.SelectedTanpaUnderying);
                    self.EnableUnderlying(true);
                } else if (CurrencyIsIDR != optionSelected.IsCurrency && AccountIsIDR == optionSelected.IsAccount) {
                    viewModel.Selected().UnderlyingDoc(optionSelected.SelectedTanpaUnderying);
                    self.EnableUnderlying(true);
                } else {
                    viewModel.Selected().UnderlyingDoc(void 0);
                    self.EnableUnderlying(false);
                }
            }
        }
    }
    //end udin

    self.OnChangeSundry = function () {
        var Sundry = ko.utils.arrayFirst(viewModel.Parameter().Sundries(),
            function (item) {
                return item.ID == viewModel.Selected().Sundry();
            });
        if (viewModel.TransactionModel().ModePayment() != "BCP2") {
            if (Sundry != null) {
                viewModel.TransactionModel().Sundry(Sundry);
                var iOABAccNo = Sundry.OABAccNo.length;
                var CurrencySoundry = Sundry.OABAccNo.substr(iOABAccNo - 3, iOABAccNo);

                var dCur = ko.utils.arrayFirst(viewModel.Parameter().Currencies(),
                function (item) {
                    return item.Code == CurrencySoundry;
                });

                if (dCur != null) {
                    viewModel.TransactionModel().DebitCurrency(dCur);
                    viewModel.TransactionModel().Account().Currency = dCur;
                    $('#debit-acc-ccy').val(dCur.Code);

                    var IsFxTransaction = (viewModel.TransactionModel().Currency().Code != 'IDR' && dCur.Code == 'IDR');
                    var IsFxTransactionToIDR = (viewModel.TransactionModel().Currency().Code == 'IDR' && dCur.Code != 'IDR');
                    viewModel.Selected().Account("");
                    viewModel.TransactionModel().Account().AccountNumber = null;//added by dani
                    viewModel.IsJointAccount(false);
                    viewModel.IsDebit(false);
                    viewModel.IsFxTransaction(IsFxTransaction);
                    viewModel.IsFxTransactionToIDR(IsFxTransactionToIDR);
                }
                var IsSundryStarFalse = viewModel.TransactionModel().Sundry() == null;
                self.IsSundryFilled(IsSundryStarFalse);
            }
            else {
                var IsSundryStarFalse = viewModel.TransactionModel().Sundry() != null;
                self.IsSundryFilled(IsSundryStarFalse);
                viewModel.TransactionModel().Sundry({ ID: null, OABAccNo: null });
                viewModel.IsBeneAccNumber(true);
                viewModel.IsDebit(true);
                viewModel.IsFxTransaction(false);
                viewModel.IsFxTransactionToIDR(false);
            }
        }
        ValidationCharging();
        ValidationWaiveCharges();
    }

    function OnChangedBene() {
        //aridya 20170201 tambah untuk skn branch code tab
        if (viewModel.TransactionModel().ModePayment() == 'IPE') {
            if (viewModel.ProductID() == ConsProductID.SKNProductIDCons) {
                if ($.trim($('#bank-branch-code').val()) == '') {
                    viewModel.TransactionModel().BranchID = null;
                } else {
                    viewModel.TransactionModel().BranchID = viewModel.selectedBranchID();
                }
            }
        }
        //end aridya
        var bene = viewModel.TransactionModel().BeneAccNumber();
        if (bene != null && bene != "") {
            //$('#debit-sundry').data({ ruleRequired: false })
            var IsBeneStarFalse = viewModel.TransactionModel().BeneAccNumber() == null;
            self.IsBeneFilled(IsBeneStarFalse);
        }
        else {
            var IsBeneStarFalse = viewModel.TransactionModel().BeneAccNumber() != null;
            self.IsBeneFilled(IsBeneStarFalse);
            //$('#debit-sundry').data({ ruleRequired: true })
        }
    }

    self.OnChangeBene = function () {
        OnChangedBene();
    }

    self.OnChangeChargeNumber = function () {
        var AccNumber = viewModel.TransactionModel().OtherAccountNumber();
        if (AccNumber != "") {
            self.TransactionModel().ChargingACCNumber = viewModel.TransactionModel().OtherAccountNumber();
            $('#ChAccount').val(viewModel.TransactionModel().OtherAccountNumber());
        } else {
            $('#ChAccount').val("");
        }
    }

    self.CheckEmptyAccountNumber = function () {
        var EmptyDataAccount;
        if (viewModel.Selected().Account() == undefined) {
            EmptyDataAccount = true;
        } else {
            EmptyDataAccount = false;
        }

        var AccountNumberSelected = $('#debit-acc-number option:selected').text().trim();
        if (AccountNumberSelected == '-') {
            self.IsEmptyAccountNumber(true);
            self.IsEmptyChargingAccount(true);
            //viewModel.TransactionModel().Account().AccountNumber = null;
            viewModel.TransactionModel().IsOtherAccountNumber(true);
            $('#ChAccount-ccy').val("");
            $('#ChAccount').val("");
            if (viewModel.Selected().DebitCurrency() > 0 && viewModel.Selected().DebitCurrency() != undefined) {
                viewModel.TransactionModel().ChargingAccountCurrency = viewModel.Selected().DebitCurrency();
                viewModel.Selected().ChargingAccountCurrency(viewModel.Selected().DebitCurrency());
            }
            self.EnableCCY(false);

        } else {
            if (EmptyDataAccount == true) {
                if (viewModel.Selected().Sundry() == undefined) {
                    self.TransactionModel().ChargingACCNumber = "";
                    $('#ChAccount').val("");
                    self.TransactionModel().ChargingAccountCurrency = "";
                    $('#ChAccount-ccy').val("");
                }
            } else {
                self.TransactionModel().ChargingACCNumber = viewModel.TransactionModel().Account().AccountNumber;
                $('#ChAccount').val(viewModel.TransactionModel().Account().AccountNumber);
                self.TransactionModel().ChargingAccountCurrency = viewModel.TransactionModel().Account().Currency.ID;
                //$('#ChAccount-ccy').val(viewModel.TransactionModel().Account().Currency.ID);
                viewModel.Selected().ChargingAccountCurrency(viewModel.TransactionModel().Account().Currency.ID);
            }
            self.IsEmptyAccountNumber(false);
            self.IsEmptyChargingAccount(false);
            viewModel.TransactionModel().OtherAccountNumber(null);
            viewModel.TransactionModel().IsOtherAccountNumber(false);
            self.EnableCCY(true);
            GetTotalTransaction();
        }
        var accountNumber = viewModel.Selected().Account() != null && viewModel.Selected().Account() != "";

        if (accountNumber != "" && self.TransactionModel().IsJointAccount()) {
            self.UnderlyingFilterAccountNumber(viewModel.Selected().Account());
            self.UnderlyingFilterShow(2); // show data only joint account
            self.GetDataUnderlying();
            self.GetDataAttachFile();
            UnCheckUnderlyingTable();
        }
        //added by dani 13-4-16
        if (viewModel.Parameter().Currencies != undefined) {
            //console.log(viewModel.CIFAccountsNumber()[0]);
            //console.log(viewModel.CIFAccountsNumber());

            if (viewModel.Selected().cifAccNumber() != undefined) {
                var accnum = ko.utils.arrayFirst(viewModel.CIFAccountsNumber(), function (item) {
                    return item.AccountNumber == viewModel.Selected().cifAccNumber();
                });
                //console.log(accnum);
                viewModel.Selected().Currency(ko.mapping.toJS(accnum.Currency.ID));
                //console.log(viewModel.Selected().Currency());
            }
        }
        // end by dani

        //Add henggar IPE
        if (viewModel.ProductID() == ConsProductID.RTGSProductIDCons || viewModel.ProductID() == ConsProductID.SKNProductIDCons || viewModel.ProductID() == ConsProductID.OverbookingProductIDCons) {
            var CurrencyIsIDR = viewModel.Selected().Currency();
            var AccountIsIDR = viewModel.TransactionModel().Account().Currency.ID;

            if (CurrencyIsIDR == optionSelected.IsCurrency && AccountIsIDR != optionSelected.IsAccount) {
                viewModel.Selected().UnderlyingDoc(optionSelected.SelectedTanpaUnderying);
                self.EnableUnderlying(true);
            } else if (CurrencyIsIDR != optionSelected.IsCurrency && AccountIsIDR == optionSelected.IsAccount) {
                viewModel.Selected().UnderlyingDoc(optionSelected.SelectedTanpaUnderying);
                self.EnableUnderlying(true);
            } else {
                viewModel.Selected().UnderlyingDoc(undefined);
                self.EnableUnderlying(false);
            }
        }
        if (viewModel.TransactionModel().ModePayment() != "BCP2" && viewModel.TransactionModel().ModePayment() != null) {
            FormValidationTrxPayment();
        }
        ValidationRateTZ();
    };

    self.OnChangeAccountNumber = function () { //form Underlying
        var accNumber = CustomerUnderlying.AccountNumber() != null ? CustomerUnderlying.AccountNumber() : null;
        if (accNumber != null) {
            //SetStatementA(true, accNumber);
        }
    }
    self.GetCalculating = function (amountUSD) {
        GetCalculating(amountUSD);
    };
    self.GetCalculatingTrxRate = function (amountUSD) {
        GetCalculating(amountUSD);
    };

    function GetDataTBOTool(cif) {
        var param = cif;
        var options = {
            url: api.server + api.url.tbotoolbycif,
            params: {
                id: param,
            },
            token: accessToken
        };
        Helper.Ajax.Get(options, OnSuccessGetDataTBOTool, OnError, OnAlways);
    }

    function OnSuccessGetDataTBOTool(data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            viewModel.TBOData([]);
            for (var i = 0 ; i < data.length; i++) {
                var DataTBO = {
                    TransactionID: data[i].TransactionID,
                    ApplicationID: data[i].ApplicationID,
                    CIF: data[i].CIF,
                    CustomerName: data[i].CustomerName,
                    ID: data[i].ID,
                    TBOSLAID: data[i].TBOSLAID,
                    ActDate: data[i].ActDate,
                    ExptDate: data[i].ExptDate,
                    Status: data[i].Status,
                    CreatedBy: data[i].CreatedBy,
                    CreatedDate: data[i].CreatedDate,
                    LastModifiedBy: data[i].ModifiedBy,
                    LastModifiedDate: data[i].ModifiedDate,
                    TBOApplicationID: data[i].TBOApplicationID
                };
                viewModel.TBOData.push(DataTBO);
            }
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    //Tambah Agung
    function GetDataInstructionDocument() {
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        var options = {
            url: api.server + api.url.workflow.instructiondocumenttmo(viewModel.TransactionModel().Customer().CIF),
            params: {
                page: self.InstructionGridProperties().Page(),
                size: self.InstructionGridProperties().Size(),
                sort_column: self.InstructionGridProperties().SortColumn(),
                sort_order: self.InstructionGridProperties().SortOrder()
            },
            token: accessToken
        };
        var filters = GetInstructionFilteredColumns();
        if (filters.length > 0) {
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetInstructionData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetInstructionData, OnError, OnAlways);
        }
    }

    function OnSuccessGetInstructionData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            var dataInstruction = SetSelectedInstruction(data.Rows);
            self.InstructionDocuments([]);
            self.InstructionDocuments(dataInstruction);
            self.InstructionGridProperties().Page(data['Page']);
            self.InstructionGridProperties().Size(data['Size']);
            self.InstructionGridProperties().Total(data['Total']);
            self.InstructionGridProperties().TotalPages(Math.ceil(self.InstructionGridProperties().Total() / self.InstructionGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }
    self.SelectInstruction = function () {
        self.DocumentPurpose_a(new DocumentPurposeModel2('', '', ''));
        GetDataInstructionDocument();
        $("#modal-form-instruction").modal('show');
        $('#backDrop').show();
    }
    self.onChangeUtilizeAmountDeal = function (item) {
        var index = item.TransactionID;
        var itemdeal = $('#Position' + index).val();
        if (parseFloat($('#Position' + index).val().replace(/,/g, '')) > item.AvailableAmountDeal) {
            $('#Position' + index).val(formatNumber(parseFloat(item.UtilizeAmountDeal.replace(/,/g, ''))));
            ShowNotification("Form Validation Warning", "Available Amount Underlying must be greater and lesh than Utilize Amount Deal", 'gritter-warning', true);
            return false;
        } else {
            $('#Position' + index).val(formatNumber(parseFloat(itemdeal.replace(/,/g, ''))));
            if (item.IsEnable == true) {
                //item.IsEnable = false;
                if (itemdeal != "" && itemdeal != null) {
                    $('#Position' + index).val(formatNumber(parseFloat(itemdeal.replace(/,/g, ''))));
                } else {
                    $('#Position' + index).val(formatNumber(parseFloat(0)));
                }
                ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (itemtemp) {
                    if (itemtemp.ID() == item.ID) {
                        self.TempSelectedUnderlying.remove(itemtemp);
                        itemtemp.USDAmount(parseFloat(itemdeal.replace(/,/g, '')));
                        self.TempSelectedUnderlying.push(itemtemp);
                        setTotalUtilize();
                    }
                });
                //var datad = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                //    return x.ID() == item.ID;
                //});
                //if (datad != null) {
                //    self.TempSelectedUnderlying.remove(datad);
                //    setTotalUtilize();
                //}
                //ResetDataUnderlyingTMO(index, false);
                //viewModel.onSelectionUtilize(index, item);
            }
        }
        //viewModel.CustomerUnderlyings()[item.RowID - 1].UtilizeAmountDeal = formatNumber(parseFloat(itemdeal));
    }

    self.onChangeUtilizeAmountDealAll = function (item) {
        var index = item.RowID - 1;
        var itemdeal = $('#PositionAll' + index).val();
        if (parseFloat($('#PositionAll' + index).val().replace(/,/g, '')) > item.AvailableAmountDeal) {
            if (item.UtilizeAmountDeal != "" && item.UtilizeAmountDeal != null) {
                $('#PositionAll' + index).val(formatNumber(parseFloat(item.UtilizeAmountDeal.replace(/,/g, ''))));
            } else {
                $('#PositionAll' + index).val(formatNumber(parseFloat(0)));
            }
            ShowNotification("Form Validation Warning", "Available Amount Underlying must be greater and lesh than Utilize Amount Deal", 'gritter-warning', true);
            return false;
        } else {
            if (itemdeal != "" && itemdeal != null) {
                $('#PositionAll' + index).val(formatNumber(parseFloat(itemdeal.replace(/,/g, ''))));
            } else {
                $('#PositionAll' + index).val(formatNumber(parseFloat(0)));
            }
            if (item.IsEnable == true) {
                ko.utils.arrayForEach(self.TempSelectedUnderlyingTMO(), function (itemtemp) {
                    if (itemtemp.ID() == item.ID) {
                        self.TempSelectedUnderlyingTMO.remove(itemtemp);
                        itemtemp.UtilizeAmountDeal(parseFloat(itemdeal.replace(/,/g, '')));
                        self.TempSelectedUnderlyingTMO.push(itemtemp);
                    }
                });

                //var datad = ko.utils.arrayFirst(self.TempSelectedUnderlyingTMO(), function (x) {
                //    return x.ID() == item.ID;
                //});
                //self.TempSelectedUnderlyingTMO.remove(datad);
                //ResetDataUnderlyingTMOALL(index, false);
            }
        }
        //viewModel.CustomerUnderlyings()[item.RowID - 1].UtilizeAmountDeal = formatNumber(parseFloat(itemdeal));
    }

    //End
    self.OnchangeProductType = function () {
        GetTotalTransaction();

        //if (viewModel.TrxRate() == "" || viewModel.TrxRate() == undefined) {
        //    if (viewModel.ProductID() == ConsProductID.OverbookingProductIDCons || viewModel.ProductID() == ConsProductID.OTTProductIDCons) {
        //        if (viewModel.Selected().ProductType() == Const_ProductType.vsdBoardC || viewModel.Selected().ProductType() == Const_ProductType.vsdBoardT) {
        //            viewModel.TrxRate($('#rate').val());
        //            viewModel.TransactionModel().TrxRate($('#rate').val());
        //        }
        //        else if (viewModel.Selected().ProductType() == Const_ProductType.vsdQuote) {
        //            viewModel.TrxRate("");
        //            viewModel.TransactionModel().TrxRate("");
        //            calculate();
        //        }
        //    }
        //}
        ValidationRateTZ();
    }
    self.OnCurrencyChange = function () {
        GetTotalTransaction();
    }

    self.SetCustomerAutoComplete = function () {
        $("#customer-name").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.customer + "/Search",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set customer data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.TransactionModel().Customer(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.TransactionModel().IsResident(ui.item.data.IsResident);
                    //Andi
                    viewModel.FDModel().Customer(ko.mapping.toJS(ui.item.data, mapping));
                    //End Andi
                    //Dani
                    viewModel.TransactionTMOModel().Customer(ko.mapping.toJS(ui.item.data, mapping));
                    //End Dani
                    //Afif
                    viewModel.TransactionLoanModel().Customer(ko.mapping.toJS(ui.item.data, mapping));
                    //End Afif
                    //Agung
                    viewModel.TransactionUTModel().Customer(ko.mapping.toJS(ui.item.data, mapping));
                    //End Agung

                    //Haqi
                    viewModel.CIFTransactionModel().Customer(ko.mapping.toJS(ui.item.data, mapping));
                    //End Agung

                    viewModel.Selected().BizSegment(ui.item.data.BizSegment.ID);
                    cifData = ui.item.data.CIF;

                    //started by haqi        
                    //self.TempFreezeAccounts(ko.mapping.toJS(ui.item.data.Accounts));
                    if (ui.item.data.Accounts != null && ui.item.data.Accounts.length > 0) {
                        self.TempFreezeAccounts([]);
                        ko.utils.arrayForEach(ui.item.data.Accounts, function (item) {
                            item.IsAddTblFreezeAccount = false;
                            item.IsFreezeAccount = false;
                            self.TempFreezeAccounts.push(item);
                        });
                    }

                    //self.TempDormantAccounts(ko.mapping.toJS(ui.item.data.Accounts));
                    if (ui.item.data.Accounts != null && ui.item.data.Accounts.length > 0) {
                        self.TempDormantAccounts([]);
                        ko.utils.arrayForEach(ui.item.data.Accounts, function (item) {
                            item.IsAddDormantAccount = false;
                            self.TempDormantAccounts.push(item);
                        });
                    }

                    if (ui.item.data.Accounts != null && ui.item.data.Accounts.length > 0) {
                        self.CIFAccountsNumber([]);
                        ko.utils.arrayForEach(ui.item.data.Accounts, function (item) {
                            self.CIFAccountsNumber.push(item);
                        });
                    }
                    viewModel.CIFTransactionModel().BrachRiskRating().Name(ko.mapping.toJS(ui.item.data.Branch.Name));
                    //viewModel.Selected().Job(ui.item.data.ParameterSystem.ID);
                    //End by haqi

                    customerNameData = ui.item.data.Name;
                    viewModel.GetDataUnderlying();
                    viewModel.GetDataAttachFile();
                    viewModel.IsStatementA(true); //?
                    // Joint Account 
                    viewModel.TransactionModel().IsJointAccount(false);
                    viewModel.IsJointAccount(true); // set default value
                    viewModel.TempSelectedUnderlying([]);
                    viewModel.TransactionModel().utilizationAmount(0.00);
                    //PPUModel.cif = cifData;
                    //GetTotalPPU(PPUModel, OnSuccessTotal, OnErrorDeal);
                    viewModel.SetDynamicAccount();
                    viewModel.Selected().Account(null);
                    //viewModel.TransactionModel().Account().Currency().Code(null);
                    viewModel.Selected().DebitCurrency(null);
                    viewModel.IsEmptyAccountNumber(false);
                    viewModel.TransactionModel().OtherAccountNumber(null);
                    viewModel.TransactionModel().IsOtherAccountNumber(false);

                    //start change adi
                    //viewModel.Selected().Channel();
                    //viewModel.Selected().Channel()
                    //end

                }
                else
                    viewModel.TransactionModel().Customer(null);
            }
        });
    }
    self.SetCustomerAutoCompleteCIFAccount = function () {
        $("#customer-nameCIF").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.customer + "/Search",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set customer data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.TempAttachemntDocument(ko.mapping.toJS(ui.item.data, mapping));
                }
                else
                    viewModel.TempAttachemntDocument(null);
            }
        });
    }
    //Andi,22 October 2015
    self.SetCustomerAutoCompleteFD = function () {
        $("#customer-nameFD").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.customer + "/Search",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set customer data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.FDModel().Customer(ko.mapping.toJS(ui.item.data, mapping));
                    cifData = ui.item.data.CIF;
                }
                else
                    viewModel.TransactionModel().Customer(null);
            }
        });
    }
    self.SetCustomerAutoCompleteUTFNA = function () {
        $("#customer-nameJoinFna").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.customer + "/Search",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set customer data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.TransactionUTModel().CustomerJoin(ko.mapping.toJS(ui.item.data, mapping));
                }
                else
                    viewModel.TransactionUTModel().CustomerJoin(null);
            }
        });
    }
    self.SetCustomerAutoCompleteUTNonFNA = function () {
        $("#customer-nameJoinNonFna").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.customer + "/Search",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set customer data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.TransactionUTModel().CustomerJoin(ko.mapping.toJS(ui.item.data, mapping));
                }
                else
                    viewModel.TransactionUTModel().CustomerJoin(null);
            }
        });
    }
    self.SetSOLIDAutoComplete = function () {
        $("#solidut").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.branch + "/SOLID",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessSolIDAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set customer data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.TransactionUTModel().SolIDList(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.TransactionUTModel().SolID(ui.item.data.SolID);
                }
                else {
                    viewModel.TransactionUTModel().SolIDList(null);
                    viewModel.TransactionUTModel().SolID(null);
                }
            }
        });
    }
    self.SetSOLIDJoinAutoComplete = function () {
        $("#solidjoin").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.branch + "/SOLID",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessSolIDAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set customer data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.TransactionUTModel().SolIDList(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.TransactionUTModel().SolIDJoin(ui.item.data.SolID);
                }
                else {
                    viewModel.TransactionUTModel().SolIDList(null);
                    viewModel.TransactionUTModel().SolIDJoin(null);
                }
            }
        });
    }
    function GetIDFund() {
        var ret = 0;
        switch (viewModel.ProductID()) {
            case ConsProductID.UTOffshoreProductIDCons:
                ret = 1;
                break;
            case ConsProductID.UTOnshoreproductIDCons:
                ret = 2;
                break;
            case ConsProductID.UTCPFProductIDCons:
                ret = 3;
                break;
            default:
                break
        }
        return ret;
    }
    self.SetFundAutoCompleteFD = function () {
        $("#MFund").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.cbofund + "/ParameterFund",
                    data: {
                        query: request.term,
                        limit: 20,
                        pid: GetIDFund()
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessFundAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set customer data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.TransactionUTModel().MutualFundList(ko.mapping.toJS(ui.item.data, mapping));
                }
                else
                    viewModel.TransactionUTModel().MutualFundList(null);
            }
        });
    }
    self.SetFundAutoCompleteFrom = function () {
        $("#MFundFrom").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.cbofund + "/ParameterFund",
                    data: {
                        query: request.term,
                        limit: 20,
                        pid: GetIDFund()
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessFundAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set customer data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.TransactionUTModel().MutualFundSwitchFrom(ko.mapping.toJS(ui.item.data, mapping));
                }
                else
                    viewModel.TransactionUTModel().MutualFundSwitchFrom(null);
            }
        });
    }
    self.SetFundAutoCompleteTo = function () {
        $("#MFundTo").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.cbofund + "/ParameterFund",
                    data: {
                        query: request.term,
                        limit: 20,
                        pid: GetIDFund()
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessFundAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set customer data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.TransactionUTModel().MutualFundSwitchTo(ko.mapping.toJS(ui.item.data, mapping));
                }
                else
                    viewModel.TransactionUTModel().MutualFundSwitchTo(null);
            }
        });
    }
    self.SetInvestmentAutoComplete = function () {
        $("#investmentid").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.transactionutin + "/Investment",
                    data: {
                        query: request.term,
                        limit: 20,
                        cif: viewModel.TransactionUTModel().Customer().CIF
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessInvestmentAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set customer data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.TransactionUTModel().InvestmentList(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.TransactionUTModel().Investment(ui.item.data.Investment);
                }
                else {
                    viewModel.TransactionUTModel().InvestmentList(null);
                    viewModel.TransactionUTModel().Investment(null);
                }
            }
        });
    }
    //tambah Agung

    self.SetTZNumberAutoComplete = function () {
        $("#tznumberAuto").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.transactionTzNumber,
                    data: {
                        tznumber: request.term,
                        limit: 20,
                        cif: viewModel.TransactionModel().Customer().CIF
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessTZNumberAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set customer data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.TransactionModel().TZNumber(ui.item.data.TZNumber);
                    //viewModel.TransactionModel().Documents(ui.item.data.Documents);
                    viewModel.Documents(ui.item.data.Documents);
                    viewModel.TransactionModel().Instruction(ui.item.data.Instruction);
                    viewModel.TransactionModel().Underlying(ui.item.data.Underlying);
                    viewModel.TransactionModel().StatementLetter(ui.item.data.StatementLetter);
                    viewModel.TransactionModel().TransactionDealID(ui.item.data.TransactionDealID);
                    viewModel.AmountTZ(ui.item.data.AmountTZ);
                    viewModel.IsNewDocument(true);

                    var dfamount = 0;
                    var dataunderlying;
                    ko.utils.arrayForEach(ui.item.data.Underlyings, function (item) {
                        if (ui.item.data.Underlyings != null) {
                            ko.utils.arrayForEach(viewModel.CustomerUnderlyingsTMO(), function (item2) {
                                if (item2.ID == item.ID && item.AvailableAmountDeal != 0) {

                                    var datatemp = ko.utils.arrayFirst(viewModel.TempSelectedUnderlying(), function (itemt) {
                                        return itemt.ID() == item2.ID;
                                    });
                                    if (datatemp == null) {
                                        self.TempSelectedUnderlying.push(new SelectUtillizeModel(item2.ID, false, item.UtilizeAmountDeal, item2.StatementLetter.ID));
                                    }
                                }
                            });
                        }
                        dfamount += item.UtilizeAmountDeal;
                    });
                    var dataunderlying = SetSelectedUnderlying(self.CustomerUnderlyingsTMO());
                    self.CustomerUnderlyingsTMO([]);
                    self.CustomerUnderlyingsTMO(dataunderlying);
                    viewModel.TransactionModel().DFAmount(formatNumber(dfamount));
                    setTotalUtilize();
                }
                else {
                    viewModel.TransactionModel().TZNumber(null);
                    viewModel.TransactionModel().Documents(null);
                    viewModel.Documents(null);
                    viewModel.TransactionModel().Instruction(false);
                    viewModel.TransactionModel().Underlying(false);
                    viewModel.TransactionModel().StatementLetter(false);
                    viewModel.TransactionModel().DFAmount(null);
                }
            }
        });
    }
    function OnSuccessTZNumberAutoComplete(response, data, textStatus, jqXHR) {
        response($.map(data, function (item) {
            return {
                label: item.TZNumber,
                value: item.TZNumber,
                data: {
                    TZNumber: item.TZNumber,
                    Instruction: item.Instruction,
                    Underlying: item.Underlying,
                    StatementLetter: item.StatementLetter,
                    TransactionDealID: item.TransactionDealID,
                    AmountTZ: item.AmountTZ,
                    Documents: item.Documents,
                    Underlyings: item.Underlyings
                }
            }
        })
        );
    }
    function ResetDataInstruction(index, isSelect) {
        self.InstructionDocuments()[index].IsSelected = isSelect;
        var dataRows = self.InstructionDocuments().slice(0);
        self.InstructionDocuments([]);
        self.InstructionDocuments(dataRows);
    }
    self.onSelectionInstruction = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedInstruction(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            self.TempSelectedInstruction.remove(data);
            ko.utils.arrayForEach(self.Documents(), function (itemdoc) {
                if (itemdoc.FileName == item.FileName) {
                    self.Documents.remove(itemdoc);
                }
            });
            ko.utils.arrayForEach(self.TransactionModel().Documents(), function (itemdoc) {
                if (itemdoc.FileName == item.FileName) {
                    self.Documents.remove(itemdoc);
                }
            });
            ResetDataInstruction(index, false);
        } else {
            self.TempSelectedInstruction.push({
                ID: item.ID
            });
            self.Documents.push(item);
            self.TransactionModel().Documents.push(item);
            ResetDataInstruction(index, true);
        }
    }
    function SetSelectedInstruction(data) {
        for (var i = 0; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedInstruction(), function (item) {
                return item.ID == data[i].ID;
            });
            if (selected != null) {
                data[i].IsSelected = true;
            }
            else {
                data[i].IsSelected = false;
            }
        }
        return data;
    }
    //End
    //Function Set Dynamic Account
    self.SetDynamicAccount = function () {
        SetDynamicAccount();
    }
    function SetDynamicAccount() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.customerAccount + "/" + cifData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TransactionModel().Customer().Accounts = data;
                    self.DynamicAccounts([])
                    var dataAccount = ko.utils.arrayFilter(data, function (item) {
                        var sAccount = item.IsJointAccount == null ? false : item.IsJointAccount;
                        return sAccount == self.TransactionModel().IsJointAccount();
                    });
                    if (!self.TransactionModel().IsJointAccount()) {
                        dataAccount = AddOtherAccounts(dataAccount);
                    }
                    self.DynamicAccounts(dataAccount);

                    //check customer have joint account
                    var isJointAcc = ko.utils.arrayFilter(data, function (item) {
                        return true == item.IsJointAccount;
                    });
                    if (isJointAcc != null & isJointAcc.length == 0) {
                        self.IsJointAccount(false);
                    }
                    var jointAccount = ko.utils.arrayFilter(data, function (item) {
                        return true == item.IsJointAccount;
                    });
                    if (jointAccount != null && jointAccount.length > 0) {
                        self.JointAccountNumbers(jointAccount);
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
    self.GetThreshold = function (callback) {
        GetThreshold(callback);
    }
    //End Andi
    //dayat start phase 3
    self.IsIdentitas2 = ko.observable(false);
    self.IsIdentitas3 = ko.observable(false);
    self.IsIdentitas4 = ko.observable(false);
    self.IsAdd = ko.observable(true);
    self.IsAdd2 = ko.observable(true);
    self.IsAdd3 = ko.observable(true);
    self.IsAdd4 = ko.observable(true);
    self.AddIdentitas = function () {
        if (self.IsIdentitas3() && self.IsIdentitas2()) {
            self.IsIdentitas4(true)
            self.IsAdd3(false);
            self.IsAdd4(false);
            self.IsAdd(false);
        }
        else if (self.IsIdentitas2()) {
            self.IsIdentitas3(true)
            self.IsAdd3(true)
            self.IsAdd2(false);
        }
        else if (self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsIdentityTypeMaintenance() || self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsNationalityMaintenance()) {
            self.IsIdentitas2(true);
            self.IsAdd(false);
            if (self.IsIdentitas3() || self.IsIdentitas4()) {
                self.IsAdd2(false)
            } else {
                self.IsAdd2(true)
            }
        }


    }
    self.Remove2 = function () {
        if (self.IsIdentitas2()) {
            self.IsIdentitas2(false);
            if (self.IsIdentitas4() && self.IsIdentitas3()) {
                self.IsAdd4(true);
            }
            if (!self.IsIdentitas4() && self.IsIdentitas3()) {
                self.IsAdd3(true)
            }
            if (self.IsIdentitas4() && !self.IsIdentitas3()) {
                self.IsAdd4(true)
            }
            if (!self.IsIdentitas4() && !self.IsIdentitas3()) {
                self.IsAdd(true)
            }

        }
        self.Selected().IdentityTypeID2('');
        self.SelectedDDL().IdentityTypeIDs2('')
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityTypeID2({ ID: ko.observable(0), Name: ko.observable("") });
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityNumber2(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityStartDate2(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityEndDate2(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityAddress2(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan2(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan2(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCity2(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityProvince2(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCountry2(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode2(null);
    }
    self.Remove3 = function () {
        if (self.IsIdentitas3()) {
            self.IsIdentitas3(false);
            if (self.IsIdentitas4() && self.IsIdentitas2()) {
                self.IsAdd4(true);
            }
            if (!self.IsIdentitas4() && self.IsIdentitas2()) {
                self.IsAdd2(true)
            }
            if (self.IsIdentitas4() && !self.IsIdentitas2()) {
                self.IsAdd4(true)
            }

            if (!self.IsIdentitas4() && !self.IsIdentitas2()) {
                self.IsAdd(true)
            }

        }
        self.Selected().IdentityTypeID3('');
        self.SelectedDDL().IdentityTypeIDs3('')
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityTypeID3({ ID: ko.observable(0), Name: ko.observable("") });
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityNumber3(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityStartDate3(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityEndDate3(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityAddress3(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan3(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan3(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCity3(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityProvince3(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCountry3(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode3(null);
    }
    self.Remove4 = function () {
        if (self.IsIdentitas4()) {
            self.IsIdentitas4(false);
            if (!self.IsIdentitas2() && !self.IsIdentitas3()) {
                self.IsAdd(true)
            }
            if (!self.IsIdentitas2() && self.IsIdentitas3()) {
                self.IsAdd3(true)
            }
            if (!self.IsIdentitas3() && self.IsIdentitas2()) {
                self.IsAdd2(true)
            }
            if (self.IsIdentitas2() && self.IsIdentitas3()) {
                self.IsAdd3(true)
            }
        }
        self.Selected().IdentityTypeID4('');
        self.SelectedDDL().IdentityTypeIDs4('')
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityTypeID4({ ID: ko.observable(0), Name: ko.observable("") });
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityNumber4(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityStartDate4(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityEndDate4(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityAddress4(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan4(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan4(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCity4(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityProvince4(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCountry4(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode4(null);


    }


    //hidayat Function modified 1 oktober
    self.RemoveCustomerJoinLoiPoi = function (item) {
        if (self.CIFTransactionModel().CustomerJoinLoiPois().length > 1) {
            if (ko.isObservable(self.CIFTransactionModel().CustomerJoinLoiPois())) {
                self.CIFTransactionModel().CustomerJoinLoiPois().remove(item);
            } else {
                self.CIFTransactionModel().CustomerJoinLoiPois.remove(item);
            }

        }
        else {
            ShowNotification("Attention", "Can't Remove", 'gritter-warning', true);
        }
    }
    self.AddCustomerJoinLoiPoi = function () {
        if (self.CIFTransactionModel().CustomerJoinLoiPois().length < 2) {
            viewModel.CIFTransactionModel().CustomerJoinLoiPois.push({ CustomerName: ko.observable(""), CIF: ko.observable("") });
        } else {
            ShowNotification("Attention", "Maxs 2 Customer Join", 'gritter-warning', true);
        }
    }
    self.AddSelularPhoneNumber = function () {
        if (self.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers().length < 5) {
            var a = { RetailCIFCBOContactUpdateID: null, PhoneNumber: '', CountryCode: '', CityCode: '', UpdatePhoneNumber: '', UpdateCountryCode: '', UpdateCityCode: '', ContactType: 'Selular', ActionType: 0 };
            var b = ko.mapping.fromJS(a);
            self.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers.push(b);
        } else {
            ShowNotification("Attention", "Maxs 5 Selular Phone Number", 'gritter-warning', true);
        }
    }
    self.AddHomePhoneNumber = function () {
        if (self.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers().length < 5) {
            var a = { RetailCIFCBOContactUpdateID: null, PhoneNumber: '', CountryCode: '', CityCode: '', UpdatePhoneNumber: '', UpdateCountryCode: '', UpdateCityCode: '', ContactType: 'Home', ActionType: 0 };
            var b = ko.mapping.fromJS(a);
            self.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers.push(b);
        }
        else {
            ShowNotification("Attention", "Maxs 5 Home Phone Number", 'gritter-warning', true);
        }
    }
    self.AddOfficePhoneNumber = function () {
        if (self.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers().length < 5) {
            var a = { RetailCIFCBOContactUpdateID: null, PhoneNumber: '', CountryCode: '', CityCode: '', UpdatePhoneNumber: '', UpdateCountryCode: '', UpdateCityCode: '', ContactType: 'Office', ActionType: 0 };
            var b = ko.mapping.fromJS(a);
            self.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers.push(b);
        }
        else {
            ShowNotification("Attention", "Maxs 5 Office Phone Number", 'gritter-warning', true);
        }
    }
    // end function add
    self.GetIquoteFD = function () {
        var options = {
            url: api.server + api.url.iquotefd,
            token: accessToken
        };
        Helper.Ajax.Get(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                if (data.length > 0) {
                    self.Parameter().TenorFDs(ko.mapping.toJS(data, mapping));
                }
            }
        }, OnError, OnAlways);
    };
    self.GetIquoteFDLoadDraft = function () {
        var options = {
            url: api.server + api.url.iquotefd,
            token: accessToken
        };
        Helper.Ajax.Get(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                if (data.length > 0) {
                    self.Parameter().TenorFDs(ko.mapping.toJS(data, mapping));
                    if (viewModel.FDModel().Tenor() !== null && viewModel.FDModel().Tenor() !== undefined && viewModel.FDModel().FTPRate() !== null && viewModel.FDModel().FTPRate() !== undefined) {
                        if (viewModel.FDModel().Currency() !== null && viewModel.FDModel().Currency() !== undefined) {
                            var currencyNow = null;
                            if (viewModel.Parameter().Currencies() !== null && viewModel.Parameter().Currencies() !== undefined) {
                                currencyNow = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) {
                                    return item.ID == viewModel.Selected().Currency();
                                });
                            };
                            if (currencyNow !== null && currencyNow !== undefined) {
                                var codeBank = currencyNow.Code;
                                console.log(codeBank);
                                for (var ii = 0; ii < viewModel.Parameter().TenorFDs().length; ii++) {
                                    //console.log(viewModel.Parameter().TenorFDs()[ii].Tenor);
                                    //console.log(viewModel.Parameter().TenorFDs()[ii][codeBank]);
                                    if (viewModel.Parameter().TenorFDs()[ii].Tenor == viewModel.FDModel().Tenor() && viewModel.Parameter().TenorFDs()[ii][codeBank] == viewModel.FDModel().FTPRate()) {
                                        viewModel.Selected().TenorFD(viewModel.Parameter().TenorFDs()[ii].IQuoteID);
                                        //console.log(viewModel.Selected().TenorFD());
                                    } else if (viewModel.Parameter().TenorFDs()[ii].Tenor == viewModel.FDModel().Tenor()) {
                                        viewModel.Selected().TenorFD(viewModel.Parameter().TenorFDs()[ii].IQuoteID);
                                        //console.log(viewModel.Selected().TenorFD());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }, OnError, OnAlways);
    };
    self.RemoveSelularPhoneNumber = function (item) {
        if (self.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers().length > 1) {
            self.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers.remove(item);
        }
        else {
            ShowNotification("Attention", "Can't Remove", 'gritter-warning', true);
        }
    }
    self.RemoveHomePhoneNumber = function (item) {
        if (self.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers().length > 1) {
            self.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers.remove(item);
        }
        else {
            ShowNotification("Attention", "Can't Remove", 'gritter-warning', true);
        }
    }
    self.RemoveOfficePhoneNumber = function (item) {
        if (self.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers().length > 1) {
            self.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers.remove(item);
        }
        else {
            ShowNotification("Attention", "Can't Remove", 'gritter-warning', true);
        }
    }

    //dayat autocomplete
    self.AddAccountListPOAs = function (absc, sValue) {
        var data = ko.utils.arrayFirst(self.CIFTransactionModel().AccountListPOAs(), function (x) {
            return x.AccountNumber() == absc.AccountNumber;
        });
        if (!$("#AccountListPOAsCheckBox" + absc.AccountNumber).is(":checked")) { //uncheck condition
            for (var i = 0; i < viewModel.CIFTransactionModel().AccountListPOAs().length; i++) {
                if (self.CIFTransactionModel().AccountListPOAs()[i].AccountNumber() == data.AccountNumber()) {
                    self.CIFTransactionModel().AccountListPOAs().splice(i, 1);
                }
            }
            $('#AccountListPOAsCheckBox' + absc.AccountNumber).prop('checked', false);
        } else {
            self.CIFTransactionModel().AccountListPOAs.push({
                AccountNumber: ko.observable(absc.AccountNumber),
                CustomerName: ko.observable(absc.CustomerName),
                CIF: ko.observable(absc.CIF),
                Currency: ko.observable(absc.Currency.ID),
                IsJoin: ko.observable(absc.IsJointAccount)
            });
            $('#AccountListPOAsCheckBox' + absc.AccountNumber).prop('checked', true);
        }
        //console.log("Is Checked", isChecked)
        console.log(self.CIFTransactionModel().AccountListPOAs())
    }
    self.AutoCompleteCustomerJoinLoiPoi = function (index) {
        $("#Customer-Name-Loi-Poi" + index).autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.customer + "/Search",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };
                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set customer data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.CIFTransactionModel().CustomerJoinLoiPois()[index].CustomerName(ko.mapping.toJS(ui.item.data.Name, mapping));
                    viewModel.CIFTransactionModel().CustomerJoinLoiPois()[index].CIF(ko.mapping.toJS(ui.item.data.CIF, mapping));
                }
                else
                    viewModel.TransactionModel().Customer(null);
            }
        });
    }
    self.AutoCompleteSelularPhoneNumber = function (index) {
        if ($("#handphone" + index).val() == "1" || $("#handphone" + index).val() == undefined || $("#handphone" + index).val() == "") {
            viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].PhoneNumber($("#PhoneNumber" + index).val());
        } else {
            $("#PhoneNumber" + index).autocomplete({
                source: function (request, response) {
                    // declare options variable for ajax get request
                    var options = {
                        url: api.server + api.url.customercontact + "/AutoCompeleteSelularPhoneNumber",
                        data: {
                            CIF: viewModel.CIFTransactionModel().Customer().CIF,
                            query: request.term,
                            limit: 20
                        },
                        token: accessToken
                    };
                    // exec ajax request
                    Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompletePhoneNumber, OnError);
                },
                minLength: 2,
                select: function (event, ui) {
                    if (ui.item.data != undefined || ui.item.data != null) {
                        var mapping = {
                            'ignore': ["LastModifiedDate", "LastModifiedBy"]
                        };
                        viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].RetailCIFCBOContactUpdateID(ko.mapping.toJS(ui.item.data.RetailCIFCBOContactID, mapping));
                        viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].PhoneNumber(ko.mapping.toJS(ui.item.data.PhoneNumber, mapping));
                        viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].CountryCode(ko.mapping.toJS(ui.item.data.CountryCode, mapping));
                        viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].CityCode(ko.mapping.toJS(ui.item.data.CityCode, mapping));
                    } else {
                        viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].RetailCIFCBOContactUpdateID(null);
                        viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].PhoneNumber(null);
                        viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].CountryCode(null);
                        viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].CityCode(null);
                    }


                }

            });

        }

    }
    self.AutoCompleteOfficePhoneNumber = function (index) {
        if ($("#Office" + index).val() == "1" || $("#Office" + index).val() == undefined || $("#Office" + index).val() == "") {
            viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].PhoneNumber($("#OfficePhoneNumber" + index).val());

        } else {
            $("#OfficePhoneNumber" + index).autocomplete({
                source: function (request, response) {
                    // declare options variable for ajax get request
                    var options = {
                        url: api.server + api.url.customercontact + "/AutoCompeleteOfficePhoneNumber",
                        data: {
                            CIF: viewModel.CIFTransactionModel().Customer().CIF,
                            query: request.term,
                            limit: 20
                        },
                        token: accessToken
                    };
                    // exec ajax request
                    Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompletePhoneNumber, OnError);
                },
                minLength: 2,
                select: function (event, ui) {
                    if (ui.item.data != undefined || ui.item.data != null) {
                        var mapping = {
                            'ignore': ["LastModifiedDate", "LastModifiedBy"]
                        };
                        viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].RetailCIFCBOContactUpdateID(ko.mapping.toJS(ui.item.data.RetailCIFCBOContactID, mapping));
                        viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].PhoneNumber(ko.mapping.toJS(ui.item.data.PhoneNumber, mapping));
                        viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].CountryCode(ko.mapping.toJS(ui.item.data.CountryCode, mapping));
                        viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].CityCode(ko.mapping.toJS(ui.item.data.CityCode, mapping));
                    } else {
                        viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].PhoneNumber(null);
                        viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].PhoneNumber(null);
                        viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].CountryCode(null);
                        viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].CityCode(null);
                    }


                }

            });
        }
    }
    self.AutoCompleteHomePhoneNumber = function (index) {
        if ($("#Home" + index).val() == "1" || $("#Home" + index).val() == undefined || $("#Home" + index).val() == "") {
            viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].PhoneNumber($("#HomePhoneNumber" + index).val());

        } else {
            $("#HomePhoneNumber" + index).autocomplete({
                source: function (request, response) {
                    // declare options variable for ajax get request
                    var options = {
                        url: api.server + api.url.customercontact + "/AutoCompeleteHomePhoneNumber",
                        data: {
                            CIF: viewModel.CIFTransactionModel().Customer().CIF,
                            query: request.term,
                            limit: 20
                        },
                        token: accessToken
                    };
                    // exec ajax request
                    Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompletePhoneNumber, OnError);
                },
                minLength: 2,
                select: function (event, ui) {
                    if (ui.item.data != undefined || ui.item.data != null) {
                        var mapping = {
                            'ignore': ["LastModifiedDate", "LastModifiedBy"]
                        };
                        viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].RetailCIFCBOContactUpdateID(ko.mapping.toJS(ui.item.data.RetailCIFCBOContactID, mapping));
                        viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].PhoneNumber(ko.mapping.toJS(ui.item.data.PhoneNumber, mapping));
                        viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].CountryCode(ko.mapping.toJS(ui.item.data.CountryCode, mapping));
                        viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].CityCode(ko.mapping.toJS(ui.item.data.CityCode, mapping));
                    } else {
                        viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].RetailCIFCBOContactUpdateID(null)
                        viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].PhoneNumber(null);
                        viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].CountryCode(null);
                        viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].CityCode(null);
                    }


                }

            });
        }
    }
    //selular
    self.AutoCompleteCityCode = function (index) {
        $("#CityCode" + index).autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.city + "/AutCom",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };
                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteCityCode, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].CityCode(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].CityID(ko.mapping.toJS(ui.item.data.CityID, mapping));
                } else {
                    viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].CityCode(null);
                    viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].CityID(null);
                }


            }

        });

    }
    self.AutoCompleteCountryCode = function (index) {
        $("#CountryCode" + index).autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.beneficiarycountry + "/AutCom",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };
                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteCountryCode, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].CountryCode(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].BeneficiaryCountryID(ko.mapping.toJS(ui.item.data.ID, mapping));
                } else {
                    viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].CountryCode(null);
                    viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].BeneficiaryCountryID(null);
                }


            }

        });

    }

    self.UpdateAutoCompleteCityCode = function (index) {
        $("#UpdateCityCode" + index).autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.city + "/AutCom",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };
                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteCityCode, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].UpdateCityCode(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].UpdateCityID(ko.mapping.toJS(ui.item.data.CityID, mapping));
                } else {
                    viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].UpdateCityCode(null);
                    viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].UpdateCityID(null);
                }


            }

        });

    }
    self.UpdateAutoCompleteCountryCode = function (index) {
        $("#UpdateCountryCode" + index).autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.beneficiarycountry + "/AutCom",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };
                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteCountryCode, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].UpdateCountryCode(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].UpdateBeneficiaryCountryID(ko.mapping.toJS(ui.item.data.ID, mapping));
                } else {
                    viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].UpdateCountryCode(null);
                    viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].UpdateBeneficiaryCountryID(null);
                }
            }

        });

    }
    self.UpdateAutoCompletePhoneNumber = function (index) {
        $("#UpdateCountryCode" + index).autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.beneficiarycountry + "/AutCom",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };
                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteCountryCode, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].CountryCode(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].BeneficiaryCountryID(ko.mapping.toJS(ui.item.data.ID, mapping));
                } else {
                    viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].CountryCode(null);
                    viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].BeneficiaryCountryID(null);
                }


            }

        });

    }
    //Home
    self.HomeAutoCompleteCityCode = function (index) {
        $("#HomeCityCode" + index).autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.city + "/AutCom",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };
                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteCityCode, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].CityCode(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].CityID(ko.mapping.toJS(ui.item.data.CityID, mapping));
                } else {
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].CityCode(null);
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].CityID(null);
                }


            }

        });

    }
    self.HomeAutoCompleteCountryCode = function (index) {
        $("#HomeCountryCode" + index).autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.beneficiarycountry + "/AutCom",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };
                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteCountryCode, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].CountryCode(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].BeneficiaryCountryID(ko.mapping.toJS(ui.item.data.ID, mapping));
                } else {
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].CountryCode(null);
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].BeneficiaryCountryID(null);
                }


            }

        });

    }
    self.HomeAutoCompletePhoneNumber = function (index) {
        $("#HomeCountryCode" + index).autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.beneficiarycountry + "/AutCom",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };
                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteCountryCode, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].CountryCode(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].BeneficiaryCountryID(ko.mapping.toJS(ui.item.data.ID, mapping));
                } else {
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].CountryCode(null);
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].BeneficiaryCountryID(null);
                }


            }

        });

    }

    self.UpdateHomeAutoCompleteCityCode = function (index) {
        $("#UpdateHomeCityCode" + index).autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.city + "/AutCom",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };
                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteCityCode, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].UpdateCityCode(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].UpdateCityID(ko.mapping.toJS(ui.item.data.CityID, mapping));
                } else {
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].UpdateCityCode(null);
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].UpdateCityID(null);
                }


            }

        });

    }
    self.UpdateHomeAutoCompleteCountryCode = function (index) {
        $("#UpdateHomeCountryCode" + index).autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.beneficiarycountry + "/AutCom",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };
                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteCountryCode, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].UpdateCountryCode(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].UpdateBeneficiaryCountryID(ko.mapping.toJS(ui.item.data.ID, mapping));
                } else {
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].UpdateCountryCode(null);
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].UpdateBeneficiaryCountryID(null);
                }


            }

        });

    }
    self.UpdateHomeAutoCompletePhoneNumber = function (index) {
        $("#UpdateHomeCountryCode" + index).autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.beneficiarycountry + "/AutCom",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };
                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteCountryCode, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].UpdateCountryCode(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].UpdateBeneficiaryCountryID(ko.mapping.toJS(ui.item.data.ID, mapping));
                } else {
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].UpdateCountryCode(null);
                    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].UpdateBeneficiaryCountryID(null);
                }


            }

        });

    }
    //Office
    self.OfficeAutoCompleteCityCode = function (index) {
        $("#OfficeCityCode" + index).autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.city + "/AutCom",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };
                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteCityCode, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].CityCode(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].CityID(ko.mapping.toJS(ui.item.data.CityID, mapping));
                } else {
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].CityCode(null);
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].CityID(null);
                }


            }

        });

    }
    self.OfficeAutoCompleteCountryCode = function (index) {
        $("#OfficeCountryCode" + index).autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.beneficiarycountry + "/AutCom",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };
                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteCountryCode, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].CountryCode(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].BeneficiaryCountryID(ko.mapping.toJS(ui.item.data.ID, mapping));
                } else {
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].CountryCode(null);
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].BeneficiaryCountryID(null);
                }


            }

        });

    }
    self.OfficeAutoCompletePhoneNumber = function (index) {
        $("#OfficeCountryCode" + index).autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.beneficiarycountry + "/AutCom",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };
                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteCountryCode, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].CountryCode(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].BeneficiaryCountryID(ko.mapping.toJS(ui.item.data.ID, mapping));
                } else {
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].CountryCode(null);
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].BeneficiaryCountryID(null);
                }


            }

        });

    }

    self.UpdateOfficeAutoCompleteCityCode = function (index) {
        $("#UpdateOfficeCityCode" + index).autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.city + "/AutCom",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };
                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteCityCode, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].UpdateCityCode(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].UpdateCityID(ko.mapping.toJS(ui.item.data.CityID, mapping));
                } else {
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].UpdateCityCode(null);
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].UpdateCityID(null);
                }


            }

        });

    }
    self.UpdateOfficeAutoCompleteCountryCode = function (index) {
        $("#UpdateOfficeCountryCode" + index).autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.beneficiarycountry + "/AutCom",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };
                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteCountryCode, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].UpdateCountryCode(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].UpdateBeneficiaryCountryID(ko.mapping.toJS(ui.item.data.ID, mapping));
                } else {
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].UpdateCountryCode(null);
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].UpdateBeneficiaryCountryID(null);
                }


            }

        });

    }
    self.UpdateOfficeAutoCompletePhoneNumber = function (index) {
        $("#UpdateOfficeCountryCode" + index).autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.beneficiarycountry + "/AutCom",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };
                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteCountryCode, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].UpdateCountryCode(ko.mapping.toJS(ui.item.data, mapping));
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].UpdateBeneficiaryCountryID(ko.mapping.toJS(ui.item.data.ID, mapping));
                } else {
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].UpdateCountryCode(null);
                    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].UpdateBeneficiaryCountryID(null);
                }


            }

        });

    }

    //end Hidayat 

    //add aridya 20161019 to load IBranchBank from draft transaction
    self.SetBankBranchCompletedDraft = function () {
        if (viewModel != undefined) {
            switch (viewModel.ProductID()) {
                //ipe
                case ConsProductID.RTGSProductIDCons:
                case ConsProductID.OTTProductIDCons:
                case ConsProductID.SKNProductIDCons:
                case ConsProductID.OverbookingProductIDCons:
                    //panggil ajax ui.item.data.ID
                    if (StatusIPE.toLowerCase() !== 'bcp2') {
                        var options = {
                            url: api.server + api.url.bank + "/" + viewModel.TransactionModel().Bank().ID,
                            token: accessToken
                        };
                        // exec ajax request
                        Helper.Ajax.Get(options, function (data) {
                            if (data != undefined) {
                                viewModel.TransactionModel().Bank().IBranchBank = data.IBranchBank;
                                autoCompleteData = viewModel.TransactionModel().Bank().IBranchBank;
                            }
                        }, OnError, OnAlways);
                    }
                    //self.TransactionModel().Bank(ko.mapping.toJS(ui.item.data, mapping));
                    if (viewModel.TransactionModel().Bank().IBranchBank != undefined) {
                        var data = viewModel.TransactionModel().Bank().IBranchBank;
                    }
                    break;
                    //bcp
                case ConsProductID.IDInvestmentProductIDCons:
                case ConsProductID.UTOnshoreproductIDCons:
                case ConsProductID.UTOffshoreProductIDCons:
                case ConsProductID.UTCPFProductIDCons:
                case ConsProductID.FDProductIDCons:
                case ConsProductID.CollateralProductIDCons:
                case ConsProductID.CIFProductIDCons:
                case ConsProductID.LoanDisbursmentProductIDCons:
                case ConsProductID.LoanRolloverProductIDCons:
                case ConsProductID.LoanRolloverProductIDCons:
                case ConsProductID.LoanIMProductIDCons:
                case ConsProductID.LoanSettlementProductIDCons:
                case ConsProductID.TMOProductIDCons:
                case ConsProductID.SavingPlanProductIDCons:
                case ConsProductID.FXProductIDCons:
                case ConsProductID.FXNettingProductIDCons:
                    var data = viewModel.TransactionModel().Bank().IBranchBank;
                    break;
            }
            $("#bank-branch-code").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    //aridya 20160907 filter data autocomplete
                    data = autoCompleteData.filter(checkAutoCompleteBankBranch, obj);
                    //end aridya
                    response($.map(data, function (item) {
                        return {
                            // default autocomplete object
                            label: item.Code + '-' + '(' + item.Name + ')',
                            value: item.Code + '-' + item.Name,

                            // custom object binding
                            data: {
                                ID: item.ID,
                                CityID: item.CityID,
                                CityDescription: item.Description,
                                Code: item.Code,
                                IsHO: item.IsHO,
                                IsJakartaBranch: item.IsJakartaBranch,
                                IsUpcountryBranch: item.IsUpcountryBranch,
                                LastModifiedBy: item.LastModifiedBy,
                                LastModifiedDate: item.LastModifiedDate,
                                Name: item.Name,
                                Region: item.Region,
                                SolID: item.SolID
                            }
                        }
                    }));
                },

                minLength: 2,
                select: function (event, ui) {
                    // set customer data model
                    if (ui.item.data != undefined || ui.item.data != null) {
                        var mapping = {
                            'ignore': ["LastModifiedDate", "LastModifiedBy"]
                        };

                        switch (viewModel.ProductID()) {
                            //ipe
                            case ConsProductID.RTGSProductIDCons:
                            case ConsProductID.OTTProductIDCons:
                            case ConsProductID.SKNProductIDCons:
                            case ConsProductID.OverbookingProductIDCons:
                                // $("#bank-branch-code").val(self.TransactionModel().Bank().IBranchBank);
                                if (viewModel.ProductID() == ConsProductID.OverbookingProductIDCons) {
                                    viewModel.TransactionModel().Bank().IBranchBank = autoCompleteData;
                                }

                                if (viewModel.TransactionModel().Bank().IBranchBank != undefined) {

                                    var ArraySelected = 0;
                                    var Status = false;
                                    for (var i = 0; i < self.TransactionModel().Bank().IBranchBank.length; i++) {
                                        if (self.TransactionModel().Bank().IBranchBank[i].CityID == ui.item.data.CityID) {
                                            Status = true;
                                            ArraySelected = i;
                                        }
                                    }

                                    var StatusSearch = false;
                                    self.BankBranch([]);
                                    for (a = 0; a < self.TransactionModel().Bank().IBranchBank.length; a++) {
                                        for (b = 0; b < self.TransactionModel().Bank().IBranchBank[a].Name.split(' ').length; b++) {
                                            if (self.TransactionModel().Bank().IBranchBank[a].Name.split(' ')[b] == $('#bank-branch-code').val().toUpperCase()) {
                                                StatusSearch = true;
                                                var IBranchBank = {
                                                    CityCode: self.TransactionModel().Bank().IBranchBank[a].CityCode,
                                                    CityDescription: self.TransactionModel().Bank().IBranchBank[a].CityDescription,
                                                    CityID: self.TransactionModel().Bank().IBranchBank[a].CityID,
                                                    Code: self.TransactionModel().Bank().IBranchBank[a].Code,
                                                    ID: self.TransactionModel().Bank().IBranchBank[a].ID,
                                                    Name: self.TransactionModel().Bank().IBranchBank[a].Name
                                                };
                                                self.BankBranch.push(IBranchBank);
                                            }
                                        }
                                    }

                                    var CityCode = self.TransactionModel().Bank().IBranchBank[ArraySelected];
                                    self.TransactionModel().BranchID = ui.item.data.ID;
                                    self.selectedBranchID(ui.item.data.ID); //aridya 20160928 handle enter key
                                    if (CityCode != null) {
                                        $("#bank-city-code").val(CityCode.CityCode + '-' + CityCode.CityDescription);
                                        self.TransactionModel().CityID = CityCode.CityID;
                                        self.IsBranchFilled(true);
                                    }
                                }
                                break;
                                //bcp
                            case ConsProductID.IDInvestmentProductIDCons:
                            case ConsProductID.UTOnshoreproductIDCons:
                            case ConsProductID.UTOffshoreProductIDCons:
                            case ConsProductID.UTCPFProductIDCons:
                            case ConsProductID.FDProductIDCons:
                            case ConsProductID.CollateralProductIDCons:
                            case ConsProductID.CIFProductIDCons:
                            case ConsProductID.LoanDisbursmentProductIDCons:
                            case ConsProductID.LoanRolloverProductIDCons:
                            case ConsProductID.LoanRolloverProductIDCons:
                            case ConsProductID.LoanIMProductIDCons:
                            case ConsProductID.LoanSettlementProductIDCons:
                            case ConsProductID.TMOProductIDCons:
                            case ConsProductID.SavingPlanProductIDCons:
                            case ConsProductID.FXProductIDCons:
                            case ConsProductID.FXNettingProductIDCons:
                                $("#bank-branch-code").val(self.TransactionModel().Bank().IBranchBank);

                                if (viewModel.TransactionModel().Bank().IBranchBank != undefined) {
                                    var CityCode = ko.utils.arrayFirst(self.TransactionModel().Bank().IBranchBank, function (item) {
                                        return item.ID == item.ID;
                                    });
                                    self.TransactionModel().BranchID = CityCode.ID;
                                    if (CityCode != null) {
                                        $("#bank-city-code").val(CityCode.CityCode + '-' + CityCode.CityDescription);
                                        self.TransactionModel().CityID = CityCode.CityID;
                                        self.IsBranchFilled(true);
                                    }
                                }
                                break;
                        }
                    }
                }
            });
            //}
        }
    }
    //end add

    //add henggar
    self.SetBankBranchCompleted = function () {
        if (viewModel != undefined) {
            switch (viewModel.ProductID()) {
                //ipe
                case ConsProductID.RTGSProductIDCons:
                case ConsProductID.OTTProductIDCons:
                case ConsProductID.SKNProductIDCons:
                case ConsProductID.OverbookingProductIDCons:
                    var data = viewModel.TransactionModel().Bank().IBranchBank;
                    break;
                    //bcp
                case ConsProductID.IDInvestmentProductIDCons:
                case ConsProductID.UTOnshoreproductIDCons:
                case ConsProductID.UTOffshoreProductIDCons:
                case ConsProductID.UTCPFProductIDCons:
                case ConsProductID.FDProductIDCons:
                case ConsProductID.CollateralProductIDCons:
                case ConsProductID.CIFProductIDCons:
                case ConsProductID.LoanDisbursmentProductIDCons:
                case ConsProductID.LoanRolloverProductIDCons:
                case ConsProductID.LoanRolloverProductIDCons:
                case ConsProductID.LoanIMProductIDCons:
                case ConsProductID.LoanSettlementProductIDCons:
                case ConsProductID.TMOProductIDCons:
                case ConsProductID.SavingPlanProductIDCons:
                case ConsProductID.FXProductIDCons:
                case ConsProductID.FXNettingProductIDCons:
                    var data = viewModel.TransactionModel().Bank().IBranchBank;
                    break;
            }

            $("#bank-branch-code").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    //aridya 20160907 filter data autocomplete
                    data = autoCompleteData.filter(checkAutoCompleteBankBranch, obj);
                    //end aridya
                    response($.map(data, function (item) {
                        return {
                            // default autocomplete object
                            label: item.Code + '-' + '(' + item.Name + ')',
                            value: item.Code + '-' + item.Name,

                            // custom object binding
                            data: {
                                ID: item.ID,
                                CityID: item.CityID,
                                CityDescription: item.Description,
                                Code: item.Code,
                                IsHO: item.IsHO,
                                IsJakartaBranch: item.IsJakartaBranch,
                                IsUpcountryBranch: item.IsUpcountryBranch,
                                LastModifiedBy: item.LastModifiedBy,
                                LastModifiedDate: item.LastModifiedDate,
                                Name: item.Name,
                                Region: item.Region,
                                SolID: item.SolID
                            }
                        }
                    }));
                },

                minLength: 2,
                select: function (event, ui) {
                    // set customer data model
                    if (ui.item.data != undefined || ui.item.data != null) {
                        var mapping = {
                            'ignore': ["LastModifiedDate", "LastModifiedBy"]
                        };

                        switch (viewModel.ProductID()) {
                            //ipe
                            case ConsProductID.RTGSProductIDCons:
                            case ConsProductID.OTTProductIDCons:
                            case ConsProductID.SKNProductIDCons:
                            case ConsProductID.OverbookingProductIDCons:
                                // $("#bank-branch-code").val(self.TransactionModel().Bank().IBranchBank);
                                if (viewModel.ProductID() == ConsProductID.OverbookingProductIDCons) {
                                    viewModel.TransactionModel().Bank().IBranchBank = autoCompleteData;
                                }

                                if (viewModel.TransactionModel().Bank().IBranchBank != undefined) {

                                    var ArraySelected = 0;
                                    var Status = false;
                                    for (var i = 0; i < self.TransactionModel().Bank().IBranchBank.length; i++) {
                                        if (self.TransactionModel().Bank().IBranchBank[i].CityID == ui.item.data.CityID) {
                                            Status = true;
                                            ArraySelected = i;
                                        }
                                    }

                                    var StatusSearch = false;
                                    self.BankBranch([]);
                                    for (a = 0; a < self.TransactionModel().Bank().IBranchBank.length; a++) {
                                        for (b = 0; b < self.TransactionModel().Bank().IBranchBank[a].Name.split(' ').length; b++) {
                                            if (self.TransactionModel().Bank().IBranchBank[a].Name.split(' ')[b] == $('#bank-branch-code').val().toUpperCase()) {
                                                StatusSearch = true;
                                                var IBranchBank = {
                                                    CityCode: self.TransactionModel().Bank().IBranchBank[a].CityCode,
                                                    CityDescription: self.TransactionModel().Bank().IBranchBank[a].CityDescription,
                                                    CityID: self.TransactionModel().Bank().IBranchBank[a].CityID,
                                                    Code: self.TransactionModel().Bank().IBranchBank[a].Code,
                                                    ID: self.TransactionModel().Bank().IBranchBank[a].ID,
                                                    Name: self.TransactionModel().Bank().IBranchBank[a].Name
                                                };
                                                self.BankBranch.push(IBranchBank);
                                            }
                                        }
                                    }

                                    var CityCode = self.TransactionModel().Bank().IBranchBank[ArraySelected];
                                    self.TransactionModel().BranchID = ui.item.data.ID;
                                    self.selectedBranchID(ui.item.data.ID); //aridya 20160928 handle enter key
                                    if (CityCode != null) {
                                        $("#bank-city-code").val(CityCode.CityCode + '-' + CityCode.CityDescription);
                                        self.TransactionModel().CityID = CityCode.CityID;
                                        self.IsBranchFilled(true);
                                    }
                                }
                                break;
                                //bcp
                            case ConsProductID.IDInvestmentProductIDCons:
                            case ConsProductID.UTOnshoreproductIDCons:
                            case ConsProductID.UTOffshoreProductIDCons:
                            case ConsProductID.UTCPFProductIDCons:
                            case ConsProductID.FDProductIDCons:
                            case ConsProductID.CollateralProductIDCons:
                            case ConsProductID.CIFProductIDCons:
                            case ConsProductID.LoanDisbursmentProductIDCons:
                            case ConsProductID.LoanRolloverProductIDCons:
                            case ConsProductID.LoanRolloverProductIDCons:
                            case ConsProductID.LoanIMProductIDCons:
                            case ConsProductID.LoanSettlementProductIDCons:
                            case ConsProductID.TMOProductIDCons:
                            case ConsProductID.SavingPlanProductIDCons:
                            case ConsProductID.FXProductIDCons:
                            case ConsProductID.FXNettingProductIDCons:
                                $("#bank-branch-code").val(self.TransactionModel().Bank().IBranchBank);

                                if (viewModel.TransactionModel().Bank().IBranchBank != undefined) {
                                    var CityCode = ko.utils.arrayFirst(self.TransactionModel().Bank().IBranchBank, function (item) {
                                        return item.ID == item.ID;
                                    });
                                    self.TransactionModel().BranchID = CityCode.ID;
                                    if (CityCode != null) {
                                        $("#bank-city-code").val(CityCode.CityCode + '-' + CityCode.CityDescription);
                                        self.TransactionModel().CityID = CityCode.CityID;
                                        self.IsBranchFilled(true);
                                    }
                                }
                                break;
                        }
                    }
                }
            });
            //}
        }
    }


    self.SetBankAutoCompleted = function () {
        // autocomplete
        $("#bank-name").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var urlMode;
                var productCons;
                var status = false;
                self.productConstan = [
                        new optionModel(ConsProductID.RTGSProductIDCons, "RTGS"),
                        new optionModel(ConsProductID.OTTProductIDCons, "OTT"),
                        new optionModel(ConsProductID.SKNProductIDCons, "SKN"),
                        new optionModel(ConsProductID.OverbookingProductIDCons, "OVB")
                ];

                for (var i = 0; i < self.productConstan.length; i++) {
                    if (self.productConstan[i].id == viewModel.TransactionModel().Product().ID) {
                        productCons = self.productConstan[i].name;
                        status = true;
                    }
                }


                if (StatusIPE.toLowerCase() !== 'bcp2') {
                    urlMode = api.server + api.url.bank + "/SearchDetailIPE";
                } else {
                    urlMode = api.server + api.url.bank + "/SearchDetail";
                }
                var options = {
                    url: urlMode,
                    data: {
                        query: request.term,
                        limit: 20,
                        name: productCons
                    },
                    token: accessToken
                };
                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessBankAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set bank data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    //panggil ajax ui.item.data.ID
                    if (StatusIPE.toLowerCase() !== 'bcp2') {
                        var options = {
                            url: api.server + api.url.bank + "/" + ui.item.data.ID,
                            token: accessToken
                        };
                        // exec ajax request
                        Helper.Ajax.Get(options, function (data) {
                            viewModel.TransactionModel().Bank().IBranchBank = data.IBranchBank;
                            autoCompleteData = viewModel.TransactionModel().Bank().IBranchBank;
                            if (viewModel.ProductID() == ConsProductID.RTGSProductIDCons || viewModel.ProductID() == ConsProductID.SKNProductIDCons) { //aridya 20161215 add for skn (not only rtgs)
                                DefaultBranchRTGS(viewModel.TransactionModel().Bank().BranchCode);
                            }
                        }, OnError, OnAlways);
                    }
                    self.TransactionModel().Bank(ko.mapping.toJS(ui.item.data, mapping));
                    self.IsOtherBank(true);
                    self.TransactionModel().IsOtherBeneBank(false);
                    viewModel.SetBankBranchCompleted();
                }
            }
        });
    };

    function DefaultBranchRTGS(branchCode) {
        var dBranch = ko.utils.arrayFilter(viewModel.TransactionModel().Bank().IBranchBank, function (item) {
            return item.Code == branchCode;
        });
        if (dBranch != "") {
            viewModel.TransactionModel().BranchID = dBranch[0].ID;
            viewModel.TransactionModel().Branch(dBranch[0]);
            viewModel.TransactionModel().CityID = dBranch[0].CityID;
            viewModel.selectedBranchID(dBranch[0].ID);

            $("#bank-branch-code").val(dBranch[0].Code + ' - ' + dBranch[0].Name);
            $("#bank-city-code").val(dBranch[0].CityCode + '-' + dBranch[0].CityDescription);
        } else {
            $("#bank-branch-code").val("");
            $("#bank-city-code").val("");
        }

    }


    self.SetBankChagingAccount = function () {
        // autocomplete
        console.log('autocomplete bank charging loaded');
        $("#ChAccBank").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.bank + "/SearchDetail",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };
                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessBankChargingAccAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set bank data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    self.TransactionModel().ChargingAccountBank(ko.mapping.toJS(ui.item.data.Description, mapping));
                }
            }
        });
    };

    self.SetLLDAutoCompleted = function () {
        // autocomplete for lld code
        $("#lld-code").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.LLD + "/Search",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessLLDAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set bank data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    self.TransactionModel().LLD(ko.mapping.toJS(ui.item.data, mapping));


                }
            }
        });
    };

    var GetCalculating = function (amountUSD) {
        amountUSD = isNaN(amountUSD) || amountUSD == '' ? 0 : amountUSD; //avoid NaN
        var TotalIDRFCY = parseFloat(TotalPPUModel.Total.IDRFCYPPU) + parseFloat(amountUSD);
        var totalUtilize = TotalPPUModel.Total.UtilizationPPU + parseFloat(amountUSD);
        self.TransactionModel().AmountUSD(parseFloat(amountUSD).toFixed(2));

        if (viewModel.TransactionModel().ModePayment() != "BCP2" && viewModel.TransactionModel().ModePayment() != null) {
            if (viewModel.Selected().Currency() != Const_AmountLLD.NonIDRSelected) {
                if (viewModel.TransactionModel().Product().Code == "OT") {
                    if (viewModel.IsStatusDraft() == false) {
                        if (parseFloat(viewModel.TransactionModel().AmountUSD()) >= Const_AmountLLD.USDAmount) {
                            GetLLDRounding();
                            viewModel.IsLimit(true);
                            if (viewModel.Selected().LLDDocument() == Const_AmountLLD.LLDDocID) {
                                viewModel.TransactionModel().LLDUnderlyingAmount($('#trxn-amount').val());
                                viewModel.LLDUnderlyingAmount($('#trxn-amount').val());
                            } else {
                                viewModel.TransactionModel().LLDUnderlyingAmount("");
                                viewModel.LLDUnderlyingAmount("");
                            }
                            //mandatory
                            viewModel.IsLLDUndelyingDocument(true);
                            viewModel.IsLLDUndelyingAmount(true);
                            $('#llddoc').data({ ruleRequired: true });
                            $('#lldunderlying').data({ ruleRequired: true });
                            //end
                            if (LLDRounding != null) {
                                viewModel.RoundingLLD(viewModel.TransactionModel().Amount() * LLDRounding); //value rounding 2.5 %
                                viewModel.RoundingValidation(viewModel.TransactionModel().Amount() - viewModel.RoundingLLD()); //node for validation
                            }
                        } else {
                            viewModel.IsLimit(false);
                            viewModel.TransactionModel().LLDDocument(null);
                            viewModel.Selected().LLDDocument(null);
                            viewModel.TransactionModel().LLDUnderlyingAmount("");
                            viewModel.LLDUnderlyingAmount("");
                            //mandatory
                            viewModel.IsLLDUndelyingDocument(false);
                            viewModel.IsLLDUndelyingAmount(false);
                            $('#llddoc').data({ ruleRequired: false });
                            $('#lldunderlying').data({ ruleRequired: false });
                            //end
                        }
                    } else {
                        if (self.TransactionModel().AmountUSD() >= Const_AmountLLD.USDAmount) {
                            GetLLDRounding();
                            viewModel.IsLimit(true);
                            //mandatory
                            if (viewModel.IsStatusDraft()) {
                                if (parseFloat(viewModel.TransactionModel().AmountUSD()) > Const_AmountLLD.USDAmount) {
                                    if (viewModel.TransactionModel().LLDDocument() != null) {
                                        if (viewModel.TransactionModel().LLDDocument().LLDDocumentID == Const_AmountLLD.LLDDocID) {
                                            viewModel.IsLLDUndelyingDocument(true);
                                            $('#llddoc').data({ ruleRequired: true });
                                            viewModel.IsLLDUndelyingAmount(false);
                                            $('#lldunderlying').data({ ruleRequired: false });
                                            viewModel.IsRoundingLLD(true);
                                        } else {
                                            viewModel.IsLLDUndelyingDocument(true);
                                            $('#llddoc').data({ ruleRequired: true });
                                            viewModel.IsLLDUndelyingAmount(true);
                                            $('#lldunderlying').data({ ruleRequired: true });
                                            viewModel.IsRoundingLLD(true);
                                            if (viewModel.TransactionModel().AmountUSD() != null) {
                                                if (viewModel.TransactionModel().LLDUnderlyingAmount() != "") {
                                                    if (parseFloat(viewModel.TransactionModel().LLDUnderlyingAmount()) > parseFloat(AmountResCalculate)) {
                                                        viewModel.RoundingValue(0);
                                                    } else {
                                                        viewModel.RoundingValue(formatNumber(viewModel.TransactionModel().Amount() - viewModel.TransactionModel().LLDUnderlyingAmount()));
                                                    }
                                                } else {
                                                    viewModel.RoundingValue(0);
                                                }
                                            }
                                        }
                                    } else {
                                        viewModel.IsLLDUndelyingDocument(true);
                                        $('#llddoc').data({ ruleRequired: true });
                                        viewModel.IsLLDUndelyingAmount(true);
                                        $('#lldunderlying').data({ ruleRequired: true });
                                    }
                                }
                            } else {
                                viewModel.IsLLDUndelyingDocument(true);
                                viewModel.IsLLDUndelyingAmount(true);
                                $('#llddoc').data({ ruleRequired: true });
                                $('#lldunderlying').data({ ruleRequired: true });
                            }
                            //end
                            if (LLDRounding != null) {
                                //viewModel.RoundingLLD(viewModel.TransactionModel().AmountUSD() * LLDRounding); //value rounding 2.5 %
                                //viewModel.RoundingValidation(viewModel.TransactionModel().AmountUSD() - viewModel.RoundingLLD()); //node for validation
                                viewModel.RoundingLLD(viewModel.TransactionModel().Amount() * LLDRounding); //value rounding 2.5 %
                                viewModel.RoundingValidation(viewModel.TransactionModel().Amount() - viewModel.RoundingLLD()); //node for validation
                            }
                        } else {
                            viewModel.IsLimit(false);
                            //mandatory
                            viewModel.IsLLDUndelyingDocument(false);
                            viewModel.IsLLDUndelyingAmount(false);
                            $('#llddoc').data({ ruleRequired: false });
                            $('#lldunderlying').data({ ruleRequired: false });
                            //end
                        }
                    }
                } else {
                    viewModel.IsLimit(false);
                }
            }
        }

        TotalIDRFCY = isNaN(TotalIDRFCY) ? 0 : TotalIDRFCY; //avoid NaN
        self.TransactionModel().TotalTransFX(parseFloat(TotalPPUModel.Total.IDRFCYPPU).toFixed(2));
        if (self.TransactionModel().Currency().Code != 'IDR' && self.TransactionModel().Account().Currency.Code == 'IDR') {
            self.TransactionModel().TotalUtilization(totalUtilize.toFixed(2));
        } else {
            self.TransactionModel().TotalUtilization(totalUtilize.toFixed(2));
        }
        //var IsFxTransactionToIDR = (self.TransactionModel().Currency().Code == 'IDR' && self.TransactionModel().Account().Currency.Code != 'IDR');
        //self.IsFxTransactionToIDR(IsFxTransactionToIDR);
        SetHitThreshold(amountUSD);
        if (viewModel.TransactionModel().ModePayment() != "BCP2" && viewModel.TransactionModel().ModePayment() != null) {
            if (viewModel.IsStatusDraft() == false) {
                FormValidationTrxPayment();
            } else {
                viewModel.IsStatusDraft(false);
            }
        }
    };

    function calculateTrxRate() {
        //x.value = x.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        r.value = r.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        //self.TransactionModel().Amount(x.value);
        self.TrxRate(r.value);
        //res = x.value;
        ret = r.value;

        //x.value = formatNumber(x.value);
        r.value = formatNumber(r.value);

        //currency = self.Selected().Currency();

        //if (ret != "") {
        //    res = res / ret;
        //    res = Math.round(res * 100) / 100;
        //} else {
        //    if (viewModel.Rate() != "") {
        //        if (currency == 13) {
        //            res = res * ret / idrrate;
        //            res = Math.round(res * 100) / 100;
        //        } else {
        //            if (currency == 1) {
        //                res = res;
        //            } else {
        //                res = res / ret;
        //                res = Math.round(res * 100) / 100;
        //            }
        //        }
        //    } else {
        //        res = 0;
        //    }
        //}

        //self.GetCalculatingTrxRate(res);
    };

    function formatLLDDoc() {
        l.value = l.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        self.LLDUnderlyingAmount(l.value);
        viewModel.TransactionModel().LLDUnderlyingAmount(l.value);
        ret = l.value;
        l.value = formatNumber(l.value);
        if (viewModel.TransactionModel().Product().Code == "OT") {
            if (viewModel.TransactionModel().AmountUSD() != null) {
                if (viewModel.TransactionModel().LLDUnderlyingAmount() != "") {
                    if (parseFloat(viewModel.TransactionModel().LLDUnderlyingAmount()) > parseFloat(AmountResCalculate)) {
                        viewModel.RoundingValue(0);
                    } else {
                        viewModel.RoundingValue(formatNumber(viewModel.TransactionModel().Amount() - viewModel.TransactionModel().LLDUnderlyingAmount()));
                    }
                } else {
                    viewModel.RoundingValue(0);
                }
            }
        }
    }

    var x; var y; var z; var d; var r; var l;
    function startCalculate() {
        x = document.getElementById("trxn-amount");
        y = document.getElementById("rate");
        z = document.getElementById("currency");
        d = document.getElementById("eqv-usd");
        if (viewModel.TransactionModel().ModePayment() != "BCP2") {
            r = document.getElementById("trxrate");
            l = document.getElementById("lldunderlying");
        }
        if (x != null && y != null && z != null && d != null) {
            var xstored = x.getAttribute("data-in");
            var ystored = y.getAttribute("data-in");

            setInterval(function () {
                if (x == document.activeElement) {
                    var temp = x.value;
                    if (xstored != temp) {
                        xstored = temp;
                        x.setAttribute("data-in", temp);
                        calculate();
                    }
                }
                if (y == document.activeElement) {
                    var temp = y.value;
                    if (ystored != temp) {
                        ystored = temp;
                        y.setAttribute("data-in", temp);
                        calculate();
                    }
                }
                /* dodit@2014.11.08:add posibility calculate if currency changed */
                if (z == document.activeElement) { // add by dodit 2014/11/8
                    calculate();
                }
            }, 100);

            x.onblur = calculate;
            calculate();

        }
        if (viewModel.TransactionModel().ModePayment() != "BCP2") {
            if (r != null) {
                var rstored = r.getAttribute("data-in");

                setInterval(function () {
                    if (r == document.activeElement) {
                        var temp = r.value;
                        if (rstored != temp) {
                            rstored = temp;
                            r.setAttribute("data-in", temp);
                            calculateTrxRate();
                        }
                    }
                }, 100);
                r.onblur = calculateTrxRate;
                calculateTrxRate();
            }

            if (l != null) {
                var lstored = l.getAttribute("data-in");

                setInterval(function () {
                    if (l == document.activeElement) {
                        var temp = l.value;
                        if (lstored != temp) {
                            lstored = temp;
                            l.setAttribute("data-in", temp);
                            formatLLDDoc();
                        }
                    }
                }, 100);
                l.onblur = formatLLDDoc;
                formatLLDDoc();
            }
        }
    }

    function calculate() {
        /* dodit@2014.11.08:altering calculate process to avoid NaN, avoid formula on USD, and formating feature */

        x.value = x.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        y.value = y.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        self.TransactionModel().Amount(x.value);
        self.TransactionModel().Rate(y.value);
        res = x.value;
        ret = y.value;

        x.value = formatNumber(x.value);
        y.value = formatNumber(y.value);

        currency = self.Selected().Currency();

        if (currency != 1) { // if not USD
            res = res * ret / idrrate;
            res = Math.round(res * 100) / 100;
        } else {
            res = res;
        }

        //if (ret != "") {              //Dipakai yg di atas karena perhitungan yg benar payment lama 08-11-2016
        //    if (currency == 13) {
        //        res = res * ret / idrrate;
        //        res = Math.round(res * 100) / 100;
        //    } else {
        //        if (currency == 1) {
        //            res = res;
        //        } else {
        //            res = res / ret;
        //            res = Math.round(res * 100) / 100;
        //        }
        //    }
        //} else {
        //    res = 0;
        //}
        AmountResCalculate = res;
        self.GetCalculating(res);
    };

    var GrsAmount;
    function startCalculateCIF() {
        GrsAmount = document.getElementById("grossspouseccy");
        if (GrsAmount != null) {
            var xstored = GrsAmount.getAttribute("data-in");

            setInterval(function () {
                if (GrsAmount == document.activeElement) {
                    calculateCIF();
                }

            }, 100);

            GrsAmount.onblur = calculateCIF;
            calculateCIF();
        }
    }

    function calculateCIF() {
        GrsAmount.value = GrsAmount.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        viewModel.CIFTransactionModel().RetailCIFCBO().GrossIncomeAmount(GrsAmount.value);
        GrsAmount.value = formatNumber(GrsAmount.value);
    }

    var fd_a; var fd_b; var fd_allinrate;
    function startCalculateFD() {
        fd_a = document.getElementById("trxn-amountFD_a");
        fd_b = document.getElementById("trxn-amountFD_b");
        fd_allinrate = document.getElementById("all-in-rate-fd");
        if (fd_a != null && fd_b != null) {
            var xstored = fd_a.getAttribute("data-in");
            var ystored = fd_b.getAttribute("data-in");

            setInterval(function () {
                if (fd_a == document.activeElement) {
                    calculateFD(1);
                }
                if (fd_b == document.activeElement) {
                    calculateFD(2);
                }

            }, 100);
        }
        if (fd_allinrate != null) {
            var allinratefd = fd_allinrate.getAttribute("data-in");

            setInterval(function () {
                fd_allinrate.value = fd_allinrate.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '');
                viewModel.FDModel().AllInRate(fd_allinrate.value);
                fd_allinrate.value = formatNumber(fd_allinrate.value);

            }, 100);
        }

    }
    function calculateFD(amountOrder) {
        fd_a.value = fd_a.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        fd_b.value = fd_b.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        if (amountOrder == 1) {
            viewModel.FDModel().Amount(fd_a.value);
        }
        else if (amountOrder == 2) {
            viewModel.FDModel().Amount(fd_b.value);
        }

        fd_a.value = formatNumber(fd_a.value);
        fd_b.value = formatNumber(fd_b.value);
    };

    var amSP; var numUnit;
    function startCalculateUT() {
        amSP = document.getElementById("amountsp");
        numUnit = document.getElementById("numberofunit");
        if (amSP != null) {
            var xstored = amSP.getAttribute("data-in");
            setInterval(function () {
                amSP = document.getElementById("amountsp");
                if (amSP == document.activeElement) {
                    calculateUT(1);
                }
            }, 100);
        }
        if (numUnit != null) {
            var ystored = numUnit.getAttribute("data-in");
            setInterval(function () {
                numUnit = document.getElementById("numberofunit");
                if (numUnit == document.activeElement) {
                    calculateUT(2);
                }
            }, 100);
        }
    }
    function calculateUT(amountOrder) {
        if (amountOrder == 1) {
            amSP.value = amSP.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '');
            viewModel.TransactionUTModel().MutualAmount(amSP.value);
            amSP.value = formatNumber(amSP.value);
        }
        else if (amountOrder == 2) {
            numUnit.value = numUnit.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '');
            viewModel.TransactionUTModel().MutualUnitNumber(numUnit.value);
            numUnit.value = formatNumber(numUnit.value);
        }
    };

    self.GetRateAmount = function (CurrencyID) {
        GetRateAmount(CurrencyID);
    };
    self.GetRateAmountDraft = function (CurrencyID) {
        GetRateAmountDraft(CurrencyID);
    };
    var GetRateAmount = function (CurrencyID) {
        var options = {
            url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetRateAmount, OnError, OnAlways);
    }

    var GetRateAmountDraft = function (CurrencyID) {
        $.ajax({
            type: "Get",
            url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    // send notification
                    viewModel.TransactionModel().Rate(data.RupiahRate);
                    viewModel.Rate(data.RupiahRate);

                    var res = parseInt(viewModel.TransactionModel().Amount()) * parseFloat(viewModel.TransactionModel().Rate()) / parseFloat(idrrate);
                    res = Math.round(res * 100) / 100;
                    res = isNaN(res) ? 0 : res; //avoid NaN

                    $('#rate').value = formatNumber(data.RupiahRate);

                    viewModel.GetCalculating(res);
                } else {

                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    var loan_a;
    function startCalculateLoan() {
        loan_a = document.getElementById("trxn-amount");
        if (loan_a != null) {
            var xstored = loan_a.getAttribute("data-in");

            setInterval(function () {
                if (loan_a == document.activeElement) {
                    calculateLoan();
                }
            }, 100);
        }
    }
    function calculateLoan() {
        loan_a.value = loan_a.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        viewModel.TransactionLoanModel().Amount(loan_a.value);
        loan_a.value = formatNumber(loan_a.value);
    };

    var amountTMO_CSO;
    function startCalculateTMOCSO() {
        amountTMO_CSO = document.getElementById("amounttmo");
        if (amountTMO_CSO != null && amountTMO_CSO != undefined) {
            var tmostored = amountTMO_CSO.getAttribute("data-in");

            setInterval(function () {
                if (amountTMO_CSO == document.activeElement) {
                    calculateTMOCSO();
                }
            }, 100);
        }
    }

    function calculateTMOCSO() {
        amountTMO_CSO.value = amountTMO_CSO.value.replace(/,/g, "");
        viewModel.TransactionTMOModel().Amount(amountTMO_CSO.value);
        amountTMO_CSO.value = formatNumber(amountTMO_CSO.value);
    };

    // Product on change handler
    self.OnProductChange = function () {
        //20150528-reizvan : RTGS/SKN default TransactionCurrency = IDR
        if (viewModel.TransactionModel().Product().Name == 'RTGS' || viewModel.TransactionModel().Product().Name == 'SKN') {
            viewModel.Selected().Currency(13);
        } else {
            viewModel.Selected().Currency(1);
        }
        //Andi, disabled application ID
        /*
        var options = {
            url: api.server + api.url.transaction + "/GetApplicationID",
            params: {
                ProductID: self.TransactionModel().Product().ID
            },
            token: accessToken
        };
    
        Helper.Ajax.Get(options, OnSuccessGetApplicationID, OnError, OnAlways);
        */
        //End Andi
        if (!ko.toJS(viewModel.IsDraftForm())) {
            if (viewModel.ProductID() != ConsProductID.TMOProductIDCons) {
                viewModel.Parameter().DocumentPurposes(viewModel.Parameter().DocumentPurposesNonTMO());

            } else {
                viewModel.Parameter().DocumentPurposes(viewModel.Parameter().DocumentPurposesTMO());

            }
        }
    };

    //Start Haqi
    self.IsMaintenanceTypeVisible = ko.observable(false);
    self.IsPengkinianDataVisible = ko.observable(false);
    self.IsPurposeCIFVisible = ko.observable(false);
    self.IsUpdateFXTierVisible = ko.observable(false);
    self.IsATMVisible = ko.observable(false);
    self.IsRiskRatingFormVisible = ko.observable(false);
    self.IsAddCurrencyVisible = ko.observable(false);
    self.IsSuspendCIFVisible = ko.observable(false);
    self.IsUnsuspendCIFVisible = ko.observable(false);
    self.IsStandingInstructionVisible = ko.observable(false);
    self.IsAddAccountVisible = ko.observable(false);
    self.IsLinktoFFDAccountVisible = ko.observable(false);
    self.IsFreezeUnfreezeVisible = ko.observable(false);
    self.IsActivateDormantVisible = ko.observable(false);
    self.IsLPSVisible = ko.observable(false);
    self.IsLOIPOIPOAVisible = ko.observable(false);
    self.IsChangeRMVisible = ko.observable(false);
    self.IsActivateHPSPVisible = ko.observable(false);
    self.IsTagUntagStaffVisible = ko.observable(false);
    self.IsTeleponSelularEditVisible = ko.observable(false);
    self.IsTeleponRumahEditVisible = ko.observable(false);
    self.IsTeleponKantorEditVisible = ko.observable(false);
    self.IsFaxEditVisible = ko.observable(false);
    self.IsMaritalStatusSudahMenikah = ko.observable(false);
    self.IsATMClosureVisible = ko.observable(false);
    self.IsDispatchModeVisible = ko.observable(false);
    self.IsLienUnlien = ko.observable(false);
    //self.RetailCIFCBO().GroupCheckBox().IsFundSourceMaintenance(false);


    function ClearControlRequestCIF() {
        self.IsMaintenanceTypeVisible(false);
        self.IsSuspendCIFVisible(false);
        self.IsUnsuspendCIFVisible(false);
        self.IsStandingInstructionVisible(false);
        viewModel.Selected().MaintenanceType('');
        //Tambah TagUntag
        viewModel.Selected().TagUntag('');
        //End
        viewModel.Selected().TransactionSubType('');
        viewModel.Selected().Account('');
        viewModel.Selected().Currency('');
        viewModel.Selected().IdentityTypeID('');
        viewModel.Selected().IdentityTypeID2('');
        viewModel.Selected().IdentityTypeID3('');
        viewModel.Selected().IdentityTypeID4('');
        viewModel.Selected().MaritalStatusID('');
        viewModel.Selected().CellPhoneMethodID('');
        viewModel.Selected().HomePhoneMethodID('');
        viewModel.Selected().OfficePhoneMethodID('');
        viewModel.Selected().FaxMethodID('');
        viewModel.Selected().FundSource('');
        viewModel.Selected().NetAsset('');
        viewModel.Selected().MonthlyIncome('');
        viewModel.Selected().MonthlyExtraIncome('');
        viewModel.Selected().Job('');
        viewModel.Selected().AccountPurpose('');
        viewModel.Selected().IncomeForecast('');
        viewModel.Selected().OutcomeForecast('');
        viewModel.Selected().TransactionForecast('');

        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsNameMaintenance(false);
        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsIdentityTypeMaintenance(false);
        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsNPWPMaintenance(false);
        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsMaritalStatusMaintenance(false);
        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsCorrespondenceMaintenance(false);
        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsIdentityAddressMaintenance(false);
        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsOfficeAddressMaintenance(false);
        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsCorrespondenseAddressMaintenance(false);
        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPhoneFaxEmailMaintenance(false);
        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsNationalityMaintenance(false);
        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsFundSourceMaintenance(false);
        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsNetAssetMaintenance(false);
        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsMonthlyIncomeMaintenance(false);
        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsJobMaintenance(false);
        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsAccountPurposeMaintenance(false);
        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsMonthlyTransactionMaintenance(false);
        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsTujuanBukaRekeningLainnya(false)
        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsSumberDanaLainnya(false);
        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPekerjaanProfesional(false);
        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPekerjaanLainnya(false);
        self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsOthers(false);

        viewModel.CIFTransactionModel().ATMNumber(null);
        viewModel.CIFTransactionModel().StaffID(null);

        viewModel.CIFTransactionModel().RetailCIFCBO().ATMNumber(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().UBOJob(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().UBOPhone(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().UBOIdentityType(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().UBOName(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().Nationality(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficePostalCode(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficeCountry(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficeProvince(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficeCity(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficeKecamatan(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficeKelurahan(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficeAddress(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().EmailAddress(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedFax(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().Fax(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().ReportDate(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().NextReviewDate(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IndustryType(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().WorkPeriod(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().Position(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().CompanyName(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedOfficePhone(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhone(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedHomePhone(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().HomePhone(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedCellPhone(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().CellPhone(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().CorrespondensePostalCode(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().CorrespondenseCountry(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().CorrespondenseProvince(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().CorrespondenseCity(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().CorrespondenseKecamatan(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().CorrespondenseKelurahan(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().CorrespondenseAddress(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IsCorrespondenseToEmail(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().SpouseName(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IsNPWPReceived(false);
        viewModel.CIFTransactionModel().RetailCIFCBO().NPWPNumber(null);

        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCountry(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityProvince(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCity(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityAddress(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityEndDate(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityStartDate(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityNumber(null);

        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode2(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCountry2(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityProvince2(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCity2(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan2(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan2(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityAddress2(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityEndDate2(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityStartDate2(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityNumber2(null);

        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode3(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCountry3(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityProvince3(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCity3(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan3(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan3(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityAddress3(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityEndDate3(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityStartDate3(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityNumber3(null);

        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode4(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCountry4(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityProvince4(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCity4(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan4(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan4(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityAddress4(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityEndDate4(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityStartDate4(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IdentityNumber4(null);

        viewModel.CIFTransactionModel().RetailCIFCBO().Name(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().AccountNumber(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().SumberDanaLainnya(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().PekerjaanProfesional(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().PekerjaanLainnya(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().TujuanBukaRekeningLainnya(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IsStampDuty(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IsPenawaranProductPerbankan(null);
        viewModel.CIFTransactionModel().RetailCIFCBO().IsSignature(null);
    };
    self.OnCIFRequestTypeChange = function () {
        if (!viewModel.IsLoadDraftCIF()) {
            //if (event.type === 'change') {
            ClearPengkinianData();
            //}
        } else {
            self.IsPengkinianDataVisible(false);
        }
        var ReqType = self.Selected().TransactionType();
        //if (viewModel.IsLoadDraft() == false)
        //ClearControlRequestCIF();
        //ClearControlMaintenanceCIF();
        switch (ReqType) {
            case ConsTransactionType.MaintenanceCons:
                self.IsMaintenanceTypeVisible(true);
                self.IsSuspendCIFVisible(false);
                self.IsUnsuspendCIFVisible(false);
                self.IsStandingInstructionVisible(false);
                break;
            case ConsTransactionType.SuspendCIFCons:
                self.IsMaintenanceTypeVisible(false);
                self.IsSuspendCIFVisible(true);
                self.IsUnsuspendCIFVisible(false);
                self.IsStandingInstructionVisible(false);
                break;
            case ConsTransactionType.UnsuspendCIFCons:
                self.IsMaintenanceTypeVisible(false);
                self.IsSuspendCIFVisible(false);
                self.IsUnsuspendCIFVisible(true);
                self.IsStandingInstructionVisible(false);
                break;
            case ConsTransactionType.StandingInstructionCons:
                self.IsMaintenanceTypeVisible(false);
                self.IsSuspendCIFVisible(false);
                self.IsUnsuspendCIFVisible(false);
                self.IsStandingInstructionVisible(true);
                break;
            default:
                return;
        }
        if (ReqType != null) {
            switch (ReqType) {
                case ConsTransactionType.MaintenanceCons:
                    if (viewModel.IsCustomerCenter() == true) {
                        var dataMType = ko.utils.arrayFilter(viewModel.Parameter().MaintenanceTypes(), function (dta) {
                            return ((dta.ID == ConsCIFMaintenanceType.ATMClosure)) //|| (dta.ID == ConsCIFMaintenanceType.DispatchMode))
                        })
                    }
                    else {
                        var dataMType = ko.utils.arrayFilter(viewModel.Parameter().MaintenanceTypes(), function (dta) {
                            return ((dta.ID != ConsCIFMaintenanceType.ATMClosure))// && (dta.ID != ConsCIFMaintenanceType.DispatchMode))
                        })
                    }
                    //updated by dani 29-1-2016
                    viewModel.Parameter().DynamicMaintenanceTypes(dataMType);
                    if (viewModel.Parameter().DynamicMaintenanceTypes() != undefined) {
                        if (viewModel.CIFTransactionModel().MaintenanceType() != undefined) {
                            viewModel.Selected().MaintenanceType(viewModel.CIFTransactionModel().MaintenanceType());
                            viewModel.OnCIFMaintenanceTypeChange();
                        }
                    }
                    return;
                    break;
                case ConsTransactionType.StandingInstructionCons:
                    var dataSubTypes = ko.utils.arrayFilter(viewModel.Parameter().TransactionSubTypes(), function (dta) {
                        return dta.TransTypeID == ReqType
                    });
                    //29-1-2016 dani
                    viewModel.Parameter().DynamicTransactionSubTypes(dataSubTypes);
                    if (viewModel.Parameter().DynamicTransactionSubTypes() != undefined) {
                        if (viewModel.CIFTransactionModel().TransactionSubType().ID != null) {
                            viewModel.Selected().TransactionSubType(viewModel.CIFTransactionModel().TransactionSubType().ID);
                        }
                    }
                    break;
                default:
                    viewModel.Parameter().DynamicMaintenanceTypes(viewModel.Parameter().MaintenanceTypes());
                    break;
            }
        }
    };
    function ClearPengkinianData() {
        self.IsPengkinianDataVisible(false);
        viewModel.CIFTransactionModel().RetailCIFCBO().ATMNumber('');
        viewModel.CIFTransactionModel().RetailCIFCBO().UBOJob('');
        viewModel.CIFTransactionModel().RetailCIFCBO().UBOPhone('');
        viewModel.CIFTransactionModel().RetailCIFCBO().UBOIdentityType('');
        viewModel.CIFTransactionModel().RetailCIFCBO().UBOName('');
        //viewModel.CIFTransactionModel().RetailCIFCBO().Nationality('');
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficePostalCode('');
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficeCountry('');
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficeProvince('');
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficeCity('');
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficeKecamatan('');
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficeKelurahan('');
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficeAddress('');
        //viewModel.CIFTransactionModel().RetailCIFCBO().EmailAddress('');
        viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedFax('');
        viewModel.CIFTransactionModel().RetailCIFCBO().Fax('');
        if (!viewModel.IsLoadDraftCIF()) {
            viewModel.CIFTransactionModel().RetailCIFCBO().ReportDate('');
            viewModel.CIFTransactionModel().RetailCIFCBO().NextReviewDate('');
        }
        viewModel.CIFTransactionModel().RetailCIFCBO().SumberDanaLainnya('');
        viewModel.CIFTransactionModel().RetailCIFCBO().PekerjaanProfesional('');
        viewModel.CIFTransactionModel().RetailCIFCBO().PekerjaanLainnya('');
        viewModel.CIFTransactionModel().RetailCIFCBO().TujuanBukaRekeningLainnya('');
    }
    function ClearControlMaintenanceCIF() {
        self.IsPengkinianDataVisible(false);
        self.IsUpdateFXTierVisible(false);
        self.IsATMVisible(false);
        self.IsRiskRatingFormVisible(false);
        self.IsAddCurrencyVisible(false);
        self.IsAddAccountVisible(false);
        self.IsLinktoFFDAccountVisible(false);
        self.IsFreezeUnfreezeVisible(false);
        self.IsActivateDormantVisible(false);
        self.IsLPSVisible(false);
        self.IsLOIPOIPOAVisible(false);
        self.IsChangeRMVisible(false);
        self.IsActivateHPSPVisible(false);
        self.IsTagUntagStaffVisible(false);
        self.IsATMClosureVisible(false);
        self.IsDispatchModeVisible(false);
        self.IsLienUnlien(false);

        viewModel.CIFTransactionModel().IsLOI(false);
        viewModel.CIFTransactionModel().IsPOI(false);
        viewModel.CIFTransactionModel().IsPOA(false);
        viewModel.CIFTransactionModel().StaffID('');
        viewModel.Selected().Currency('');
        viewModel.Selected().RiskRatingResult('');
        viewModel.Selected().TransactionSubType('');
        viewModel.CIFTransactionModel().ATMNumber('');
        viewModel.CIFTransactionModel().StaffID('');
        viewModel.CIFTransactionModel().RetailCIFCBO().ATMNumber('');
        viewModel.CIFTransactionModel().RetailCIFCBO().UBOJob('');
        viewModel.CIFTransactionModel().RetailCIFCBO().UBOPhone('');
        viewModel.CIFTransactionModel().RetailCIFCBO().UBOIdentityType('');
        viewModel.CIFTransactionModel().RetailCIFCBO().UBOName('');
        viewModel.CIFTransactionModel().RetailCIFCBO().Nationality('');
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficePostalCode('');
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficeCountry('');
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficeProvince('');
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficeCity('');
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficeKecamatan('');
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficeKelurahan('');
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficeAddress('');
        viewModel.CIFTransactionModel().RetailCIFCBO().EmailAddress('');
        viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedFax('');
        viewModel.CIFTransactionModel().RetailCIFCBO().Fax('');
        viewModel.CIFTransactionModel().RetailCIFCBO().ReportDate('');
        viewModel.CIFTransactionModel().RetailCIFCBO().NextReviewDate('');
        viewModel.CIFTransactionModel().RetailCIFCBO().SumberDanaLainnya('');
        viewModel.CIFTransactionModel().RetailCIFCBO().PekerjaanProfesional('');
        viewModel.CIFTransactionModel().RetailCIFCBO().PekerjaanLainnya('');
        viewModel.CIFTransactionModel().RetailCIFCBO().TujuanBukaRekeningLainnya('');
    }
    self.LienUnlienList = ko.observableArray([{ ID: 1, Data: "Lien" }, { ID: 2, Data: "Un-lien" }]);
    self.LienUnliendata = ko.observable();
    self.AccountNumberLienUnLienData = ko.observable();
    self.OnCIFLOIAccountTypeChange = function (obj, event) {
        if (viewModel.CIFTransactionModel().AccountTypeLoiPoiID() == 2) {
            viewModel.CIFTransactionModel().CustomerJoinLoiPois.push({ CustomerName: ko.observable(""), CIF: ko.observable("") });
        } else {
            viewModel.CIFTransactionModel().CustomerJoinLoiPois([])
        }
    }
    self.OnCIFMaintenanceTypeChange = function (obj, event) {
        var MaintainType = self.Selected().MaintenanceType();
        if (viewModel.IsLoadDraft() == false) {
            //ClearControlMaintenanceCIF();
        }
        switch (MaintainType) {
            case ConsCIFMaintenanceType.PengkinianDataCons:
                self.IsPengkinianDataVisible(true);
                self.IsUpdateFXTierVisible(false);
                self.IsATMVisible(false);
                self.IsRiskRatingFormVisible(false);
                self.IsAddCurrencyVisible(false);
                self.IsAddAccountVisible(false);
                self.IsLinktoFFDAccountVisible(false);
                self.IsFreezeUnfreezeVisible(false);
                self.IsActivateDormantVisible(false);
                self.IsLPSVisible(false);
                self.IsLOIPOIPOAVisible(false);
                self.IsChangeRMVisible(false);
                self.IsActivateHPSPVisible(false);
                self.IsTagUntagStaffVisible(false);
                self.IsATMClosureVisible(false);
                self.IsDispatchModeVisible(false);
                self.IsLienUnlien(false);
                break;
            case ConsCIFMaintenanceType.UpdateFXTierCons:
                self.IsPengkinianDataVisible(false);
                self.IsUpdateFXTierVisible(true);
                self.IsATMVisible(false);
                self.IsRiskRatingFormVisible(false);
                self.IsAddCurrencyVisible(false);
                self.IsAddAccountVisible(false);
                self.IsLinktoFFDAccountVisible(false);
                self.IsFreezeUnfreezeVisible(false);
                self.IsActivateDormantVisible(false);
                self.IsLPSVisible(false);
                self.IsLOIPOIPOAVisible(false);
                self.IsChangeRMVisible(false);
                self.IsActivateHPSPVisible(false);
                self.IsTagUntagStaffVisible(false);
                self.IsATMClosureVisible(false);
                self.IsDispatchModeVisible(false);
                self.IsLienUnlien(false);
                break;
            case ConsCIFMaintenanceType.AtmCardCons:
                self.IsPengkinianDataVisible(false);
                self.IsUpdateFXTierVisible(false);
                self.IsATMVisible(true);
                self.IsRiskRatingFormVisible(false);
                self.IsAddCurrencyVisible(false);
                self.IsAddAccountVisible(false);
                self.IsLinktoFFDAccountVisible(false);
                self.IsFreezeUnfreezeVisible(false);
                self.IsActivateDormantVisible(false);
                self.IsLPSVisible(false);
                self.IsLOIPOIPOAVisible(false);
                self.IsChangeRMVisible(false);
                self.IsActivateHPSPVisible(false);
                self.IsTagUntagStaffVisible(false);
                self.IsATMClosureVisible(false);
                self.IsDispatchModeVisible(false);
                self.IsLienUnlien(false);
                break;
            case ConsCIFMaintenanceType.RiskRatingCons:
                self.IsPengkinianDataVisible(false);
                self.IsUpdateFXTierVisible(false);
                self.IsATMVisible(false);
                self.IsRiskRatingFormVisible(true);
                self.IsAddCurrencyVisible(false);
                self.IsAddAccountVisible(false);
                self.IsLinktoFFDAccountVisible(false);
                self.IsFreezeUnfreezeVisible(false);
                self.IsActivateDormantVisible(false);
                self.IsLPSVisible(false);
                self.IsLOIPOIPOAVisible(false);
                self.IsChangeRMVisible(false);
                self.IsActivateHPSPVisible(false);
                self.IsTagUntagStaffVisible(false);
                self.IsATMClosureVisible(false);
                self.IsDispatchModeVisible(false);
                self.IsLienUnlien(false);
                break;
            case ConsCIFMaintenanceType.AddCurrencyCons:
                self.IsPengkinianDataVisible(false);
                self.IsUpdateFXTierVisible(false);
                self.IsATMVisible(false);
                self.IsRiskRatingFormVisible(false);
                self.IsAddCurrencyVisible(true);
                self.IsAddAccountVisible(false);
                self.IsLinktoFFDAccountVisible(false);
                self.IsFreezeUnfreezeVisible(false);
                self.IsActivateDormantVisible(false);
                self.IsLPSVisible(false);
                self.IsLOIPOIPOAVisible(false);
                self.IsChangeRMVisible(false);
                self.IsActivateHPSPVisible(false);
                self.IsTagUntagStaffVisible(false);
                self.IsATMClosureVisible(false);
                self.IsDispatchModeVisible(false);
                self.SetCustomerAutoCompleteCIFAccount();
                self.IsLienUnlien(false);
                break;
            case ConsCIFMaintenanceType.AdditionalAccountCons:
                self.IsPengkinianDataVisible(false);
                self.IsUpdateFXTierVisible(false);
                self.IsATMVisible(false);
                self.IsRiskRatingFormVisible(false);
                self.IsAddCurrencyVisible(false);
                self.IsAddAccountVisible(true);
                self.IsLinktoFFDAccountVisible(false);
                self.IsFreezeUnfreezeVisible(false);
                self.IsActivateDormantVisible(false);
                self.IsLPSVisible(false);
                self.IsLOIPOIPOAVisible(false);
                self.IsChangeRMVisible(false);
                self.IsActivateHPSPVisible(false);
                self.IsTagUntagStaffVisible(false);
                self.IsATMClosureVisible(false);
                self.IsDispatchModeVisible(false);
                self.IsLienUnlien(false);
                break;
            case ConsCIFMaintenanceType.LinkFFDCons:
                self.IsPengkinianDataVisible(false);
                self.IsUpdateFXTierVisible(false);
                self.IsATMVisible(false);
                self.IsRiskRatingFormVisible(false);
                self.IsAddCurrencyVisible(false);
                self.IsAddAccountVisible(false);
                self.IsLinktoFFDAccountVisible(true);
                self.IsFreezeUnfreezeVisible(false);
                self.IsActivateDormantVisible(false);
                self.IsLPSVisible(false);
                self.IsLOIPOIPOAVisible(false);
                self.IsChangeRMVisible(false);
                self.IsActivateHPSPVisible(false);
                self.IsTagUntagStaffVisible(false);
                self.IsATMClosureVisible(false);
                self.IsDispatchModeVisible(false);
                self.IsLienUnlien(false);
                break;
            case ConsCIFMaintenanceType.FreezeUnfreezeCons:
                self.IsPengkinianDataVisible(false);
                self.IsUpdateFXTierVisible(false);
                self.IsATMVisible(false);
                self.IsRiskRatingFormVisible(false);
                self.IsAddCurrencyVisible(false);
                self.IsAddAccountVisible(false);
                self.IsLinktoFFDAccountVisible(false);
                self.IsFreezeUnfreezeVisible(true);
                self.IsActivateDormantVisible(false);
                self.IsLPSVisible(false);
                self.IsLOIPOIPOAVisible(false);
                self.IsChangeRMVisible(false);
                self.IsActivateHPSPVisible(false);
                self.IsTagUntagStaffVisible(false);
                self.IsATMClosureVisible(false);
                self.IsDispatchModeVisible(false);
                //added by dani 29-1-2016
                if (self.CIFTransactionModel().Customer()) {
                    if (self.CIFTransactionModel().Customer().Accounts) {
                        self.TempFreezeAccounts([]);
                        ko.utils.arrayForEach(self.CIFTransactionModel().Customer().Accounts, function (item) {
                            item.IsAddTblFreezeAccount = false;
                            item.IsFreezeAccount = false;
                            self.TempFreezeAccounts.push(item);
                        });
                        if (viewModel.CIFTransactionModel().ID() != null) {
                            ko.utils.arrayForEach(viewModel.TempFreezeAccounts(), function (item) {
                                ko.utils.arrayForEach(viewModel.TempFreezeAccountsGet(), function (itemTemp) {
                                    if (item.AccountNumber == itemTemp.AccountNumber) {
                                        item.IsAddTblFreezeAccount = true;
                                        item.IsFreezeAccount = itemTemp.IsFreezeAccount;
                                    }

                                });

                            });
                            var update = self.TempFreezeAccounts();
                            self.TempFreezeAccounts(ko.mapping.toJS(update));
                        }

                    }
                }
                self.IsLienUnlien(false);
                break;
            case ConsCIFMaintenanceType.ActiveDormantCons:
                self.IsPengkinianDataVisible(false);
                self.IsUpdateFXTierVisible(false);
                self.IsATMVisible(false);
                self.IsRiskRatingFormVisible(false);
                self.IsAddCurrencyVisible(false);
                self.IsAddAccountVisible(false);
                self.IsLinktoFFDAccountVisible(false);
                self.IsFreezeUnfreezeVisible(false);
                self.IsActivateDormantVisible(true);
                self.IsLPSVisible(false);
                self.IsLOIPOIPOAVisible(false);
                self.IsChangeRMVisible(false);
                self.IsActivateHPSPVisible(false);
                self.IsTagUntagStaffVisible(false);
                self.IsATMClosureVisible(false);
                self.IsDispatchModeVisible(false);
                if (self.CIFTransactionModel().Customer()) {
                    if (self.CIFTransactionModel().Customer().Accounts) {
                        self.TempDormantAccounts([]);
                        ko.utils.arrayForEach(self.CIFTransactionModel().Customer().Accounts, function (item) {
                            item.IsAddDormantAccount = false;
                            self.TempDormantAccounts.push(item);
                        });
                        if (viewModel.CIFTransactionModel().ID() != null) {
                            ko.utils.arrayForEach(self.TempDormantAccounts(), function (item) {
                                ko.utils.arrayForEach(viewModel.TempDormantAccountsGet(), function (itemTemp) {
                                    if (item.AccountNumber == itemTemp.AccountNumber) {
                                        item.IsAddDormantAccount = true;
                                    }

                                });

                            });
                            var update = self.TempDormantAccounts();
                            self.TempDormantAccounts(ko.mapping.toJS(update));

                        }
                    }
                }
                self.IsLienUnlien(false);
                break;
            case ConsCIFMaintenanceType.LPSCons:
                self.IsPengkinianDataVisible(false);
                self.IsUpdateFXTierVisible(false);
                self.IsATMVisible(false);
                self.IsRiskRatingFormVisible(false);
                self.IsAddCurrencyVisible(false);
                self.IsAddAccountVisible(false);
                self.IsLinktoFFDAccountVisible(false);
                self.IsFreezeUnfreezeVisible(false);
                self.IsActivateDormantVisible(false);
                self.IsLPSVisible(true);
                self.IsLOIPOIPOAVisible(false);
                self.IsChangeRMVisible(false);
                self.IsActivateHPSPVisible(false);
                self.IsTagUntagStaffVisible(false);
                self.IsATMClosureVisible(false);
                self.IsDispatchModeVisible(false);
                self.IsLienUnlien(false);
                break;
            case ConsCIFMaintenanceType.LOIPOIPOACons:
                self.IsPengkinianDataVisible(false);
                self.IsUpdateFXTierVisible(false);
                self.IsATMVisible(false);
                self.IsRiskRatingFormVisible(false);
                self.IsAddCurrencyVisible(false);
                self.IsAddAccountVisible(false);
                self.IsLinktoFFDAccountVisible(false);
                self.IsFreezeUnfreezeVisible(false);
                self.IsActivateDormantVisible(false);
                self.IsLPSVisible(false);
                self.IsLOIPOIPOAVisible(true);
                self.IsChangeRMVisible(false);
                self.IsActivateHPSPVisible(false);
                self.IsTagUntagStaffVisible(false);
                self.IsATMClosureVisible(false);
                self.IsDispatchModeVisible(false);
                self.IsLienUnlien(false);
                GetAccountData();
                break;
            case ConsCIFMaintenanceType.ChangeRMCons:
                self.IsPengkinianDataVisible(false);
                self.IsUpdateFXTierVisible(false);
                self.IsATMVisible(false);
                self.IsRiskRatingFormVisible(false);
                self.IsAddCurrencyVisible(false);
                self.IsAddAccountVisible(false);
                self.IsLinktoFFDAccountVisible(false);
                self.IsFreezeUnfreezeVisible(false);
                self.IsActivateDormantVisible(false);
                self.IsLPSVisible(false);
                self.IsLOIPOIPOAVisible(false);
                self.IsChangeRMVisible(true);
                self.IsActivateHPSPVisible(false);
                self.IsTagUntagStaffVisible(false);
                self.IsATMClosureVisible(false);
                self.IsDispatchModeVisible(false);
                self.IsLienUnlien(false);
                break;
            case ConsCIFMaintenanceType.ActiveHPSPCons:
                self.IsPengkinianDataVisible(false);
                self.IsUpdateFXTierVisible(false);
                self.IsATMVisible(false);
                self.IsRiskRatingFormVisible(false);
                self.IsAddCurrencyVisible(false);
                self.IsAddAccountVisible(false);
                self.IsLinktoFFDAccountVisible(false);
                self.IsFreezeUnfreezeVisible(false);
                self.IsActivateDormantVisible(false);
                self.IsLPSVisible(false);
                self.IsLOIPOIPOAVisible(false);
                self.IsChangeRMVisible(false);
                self.IsActivateHPSPVisible(true);
                self.IsTagUntagStaffVisible(false);
                self.IsATMClosureVisible(false);
                self.IsDispatchModeVisible(false);
                self.IsLienUnlien(false);
                break;
            case ConsCIFMaintenanceType.TagUntagStaffCons:
                self.IsPengkinianDataVisible(false);
                self.IsUpdateFXTierVisible(false);
                self.IsATMVisible(false);
                self.IsRiskRatingFormVisible(false);
                self.IsAddCurrencyVisible(false);
                self.IsAddAccountVisible(false);
                self.IsLinktoFFDAccountVisible(false);
                self.IsFreezeUnfreezeVisible(false);
                self.IsActivateDormantVisible(false);
                self.IsLPSVisible(false);
                self.IsLOIPOIPOAVisible(false);
                self.IsChangeRMVisible(false);
                self.IsActivateHPSPVisible(false);
                self.IsTagUntagStaffVisible(true);
                self.IsATMClosureVisible(false);
                self.IsDispatchModeVisible(false);
                self.IsLienUnlien(false);
                break;
            case ConsCIFMaintenanceType.ATMClosure:
                self.IsPengkinianDataVisible(false);
                self.IsUpdateFXTierVisible(false);
                self.IsATMVisible(false);
                self.IsRiskRatingFormVisible(false);
                self.IsAddCurrencyVisible(false);
                self.IsAddAccountVisible(false);
                self.IsLinktoFFDAccountVisible(false);
                self.IsFreezeUnfreezeVisible(false);
                self.IsActivateDormantVisible(false);
                self.IsLPSVisible(false);
                self.IsLOIPOIPOAVisible(false);
                self.IsChangeRMVisible(false);
                self.IsActivateHPSPVisible(false);
                self.IsTagUntagStaffVisible(false);
                self.IsATMClosureVisible(true);
                self.IsDispatchModeVisible(false);
                self.IsLienUnlien(false);
                break;
            case ConsCIFMaintenanceType.DispatchMode:
                self.IsPengkinianDataVisible(false);
                self.IsUpdateFXTierVisible(false);
                self.IsATMVisible(false);
                self.IsRiskRatingFormVisible(false);
                self.IsAddCurrencyVisible(false);
                self.IsAddAccountVisible(false);
                self.IsLinktoFFDAccountVisible(false);
                self.IsFreezeUnfreezeVisible(false);
                self.IsActivateDormantVisible(false);
                self.IsLPSVisible(false);
                self.IsLOIPOIPOAVisible(false);
                self.IsChangeRMVisible(false);
                self.IsActivateHPSPVisible(false);
                self.IsTagUntagStaffVisible(false);
                self.IsATMClosureVisible(false);
                self.IsDispatchModeVisible(true);
                self.IsLienUnlien(false);
                break;
            case ConsCIFMaintenanceType.LienUnlien:
                self.IsPengkinianDataVisible(false);
                self.IsUpdateFXTierVisible(false);
                self.IsATMVisible(false);
                self.IsRiskRatingFormVisible(false);
                self.IsAddCurrencyVisible(false);
                self.IsAddAccountVisible(false);
                self.IsLinktoFFDAccountVisible(false);
                self.IsFreezeUnfreezeVisible(false);
                self.IsActivateDormantVisible(false);
                self.IsLPSVisible(false);
                self.IsLOIPOIPOAVisible(false);
                self.IsChangeRMVisible(false);
                self.IsActivateHPSPVisible(false);
                self.IsTagUntagStaffVisible(false);
                self.IsATMClosureVisible(false);
                self.IsDispatchModeVisible(false);
                self.IsLienUnlien(true);
                self.Selected().Currency(null);
                self.CIFTransactionModel().Currency(new CurrencyModel2(0, '', ''));

                /*Accunt Number*/
                if (viewModel.IsLoadDraftCIF() != true) {
                    if (self.CIFTransactionModel().Customer().Accounts != null && self.CIFTransactionModel().Customer().Accounts != undefined && self.CIFTransactionModel().Customer().Accounts.length > 0) {
                        var accountNumbers = self.CIFTransactionModel().Customer().Accounts;
                        self.CIFTransactionModel().AccountNumberLienUnlien([]);
                        for (var i = 0; i < accountNumbers.length; i++) {
                            var item = ko.observable(ko.mapping.fromJS({
                                AccountNumber: accountNumbers[i].AccountNumber,
                                IsJointAccount: accountNumbers[i].IsJointAccount,
                                IsSelected: false,
                                LienUnlien: ""
                            }));
                            self.CIFTransactionModel().AccountNumberLienUnlien.push(item);
                        }
                    }
                }
                break;
            default:
                break;
        }
        ///Filtering Sub type
        var ReqType = self.Selected().TransactionType();
        if (ReqType != null) {
            switch (ReqType) {
                case ConsTransactionType.MaintenanceCons:
                    if (viewModel.CIFTransactionModel().StaffTagging().ID != undefined) {
                        viewModel.Selected().TagUntag(viewModel.CIFTransactionModel().StaffTagging().ID);
                    }

                    //added by dani 13-8-2016
                    var dataSubTypes = ko.utils.arrayFilter(viewModel.Parameter().TransactionSubTypes(), function (dta) {
                        return dta.TransTypeID == ReqType
                    });
                    viewModel.Parameter().DynamicTransactionSubTypes(dataSubTypes);
                    if (viewModel.Parameter().DynamicTransactionSubTypes() != undefined) {
                        if (viewModel.CIFTransactionModel().TransactionSubType().ID != null) {
                            viewModel.Selected().TransactionSubType(viewModel.CIFTransactionModel().TransactionSubType().ID);
                        }
                    }
                    break;
                case ConsTransactionType.StandingInstructionCons:
                    var dataSubTypes = ko.utils.arrayFilter(viewModel.Parameter().TransactionSubTypes(), function (dta) {
                        return dta.TransTypeID == ReqType
                    });
                    //29-1-2016 dani
                    viewModel.Parameter().DynamicTransactionSubTypes(dataSubTypes);
                    if (viewModel.Parameter().DynamicTransactionSubTypes() != undefined) {
                        if (viewModel.CIFTransactionModel().TransactionSubType().ID != null) {
                            viewModel.Selected().TransactionSubType(viewModel.CIFTransactionModel().TransactionSubType().ID);
                        }
                    }
                    break;
                default:
                    viewModel.Parameter().DynamicTransactionSubTypes(viewModel.Parameter().TransactionSubTypes());
                    break;
            }
        }
    };


    self.EditAccountNumberLienUnlien = function (data) {
        self.AccountNumberLienUnLienData(data.AccountNumber());
        self.LienUnliendata("");
        $("#modal-form-EditAccountNumber").modal('show');
        $('.remove').click();
    }

    self.OnCIFJob = function () {
        var Job = self.Selected().Job();
        if (viewModel.IsLoadDraft() == false) {
            viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsFundSourceMaintenance(false);
            viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsAccountPurposeMaintenance(false);
        }
        ClearError();
        switch (Job) {
            case ConsParSysCIFPekerjaan.TidakBekerja:
            case ConsParSysCIFPekerjaan.IbuRumahTangga:
                viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsFundSourceMaintenance(true);
                viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsAccountPurposeMaintenance(true);
                $('#company-name').data({ ruleRequired: false })
                $('#position').data({ ruleRequired: false })
                $('#work-period').data({ ruleRequired: false })
                $('#industry-type').data({ ruleRequired: false })

                $('#alamat-kantor').data({ ruleRequired: false })
                $('#kelurahan2').data({ ruleRequired: false })
                $('#kecamatan2').data({ ruleRequired: false })
                $('#kota2').data({ ruleRequired: false })
                $('#propinsi2').data({ ruleRequired: false })
                $('#negara2').data({ ruleRequired: false })
                $('#kode-pos2').data({ ruleRequired: false })
                break;
            default:
                $('#company-name').data({ ruleRequired: true })
                $('#position').data({ ruleRequired: true })
                $('#work-period').data({ ruleRequired: true })
                $('#industry-type').data({ ruleRequired: true })

                $('#alamat-kantor').data({ ruleRequired: true })
                $('#kelurahan2').data({ ruleRequired: true })
                $('#kecamatan2').data({ ruleRequired: true })
                $('#kota2').data({ ruleRequired: true })
                $('#propinsi2').data({ ruleRequired: true })
                $('#negara2').data({ ruleRequired: true })
                $('#kode-pos2').data({ ruleRequired: true })
                break;
        }
    };

    self.OnCIFSumberDana = function () {
        var FundSource = self.Selected().FundSource();
        viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsBeneficialOwner(false);
        switch (FundSource) {
            case ConsSumberDana.Pemberian:
            case ConsSumberDana.Parent:
            case ConsSumberDana.Pasangan:
            case ConsSumberDana.Lain:
                viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsBeneficialOwner(true);
                break;
            default:
                return;
        }
    };

    self.OnCIFTeleponSelularChange = function (index) {
        $("#CountryCode" + index).val("");
        $("#CityCode" + index).val("");
        $("#PhoneNumber" + index).val("");
        var TelponSel;
        TelponSel = self.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].ActionType();
        if (TelponSel < 1 || TelponSel > 3 || TelponSel == undefined) {
            viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].ActionType(0);
        }
        switch (TelponSel) {
            case ConsModification.Edit:
                self.AutoCompleteSelularPhoneNumber(self.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers().length - 1);
                viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].ActionType(TelponSel);
                $("#CountryCode" + index).prop('disabled', true);
                $("#CityCode" + index).prop('disabled', true);
                break;
            case ConsModification.Add:
                viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].ActionType(TelponSel);
                $("#CountryCode" + index).prop('disabled', false);
                $("#CityCode" + index).prop('disabled', false);
                viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].RetailCIFCBOContactUpdateID("");
                break;
            case ConsModification.Delete:
                self.AutoCompleteSelularPhoneNumber(self.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers().length - 1);
                viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers()[index].ActionType(TelponSel);
                $("#CountryCode" + index).prop('disabled', true);
                $("#CityCode" + index).prop('disabled', true);
                break;
            default:
                return;
        }
    };
    self.OnCIFTeleponHomeChange = function (index) {
        var TelponSel;
        $("#HomeCountryCode" + index).val("");
        $("#HomeCityCode" + index).val("");
        $("#HomePhoneNumber" + index).val("");
        TelponSel = self.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].ActionType();
        if (TelponSel < 1 || TelponSel > 3 || TelponSel == undefined) {
            viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].ActionType(0);
        }
        switch (TelponSel) {
            case ConsModification.Edit:
                self.AutoCompleteHomePhoneNumber(self.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers().length - 1);
                viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].ActionType(TelponSel);
                $("#HomeCountryCode" + index).prop('disabled', true);
                $("#HomeCityCode" + index).prop('disabled', true);
                break;
            case ConsModification.Add:
                viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].ActionType(TelponSel);
                $("#HomeCountryCode" + index).prop('disabled', false);
                $("#HomeCityCode" + index).prop('disabled', false);
                viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].RetailCIFCBOContactUpdateID("");
                break;
            case ConsModification.Delete:
                self.AutoCompleteHomePhoneNumber(self.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers().length - 1);
                viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers()[index].ActionType(TelponSel);
                $("#HomeCountryCode" + index).prop('disabled', true);
                $("#HomeCityCode" + index).prop('disabled', true);
                break;
            default:
                return;
        }
    };
    self.OnCIFTeleponOfficeChange = function (index) {
        var TelponSel;
        $("#OfficeCountryCode" + index).val("");
        $("#OfficeCityCode" + index).val("");
        $("#OfficePhoneNumber" + index).val("");
        TelponSel = self.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].ActionType();
        if (TelponSel < 1 || TelponSel > 3 || TelponSel == undefined) {
            viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].ActionType(0);
        }
        switch (TelponSel) {
            case ConsModification.Edit:
                self.AutoCompleteOfficePhoneNumber(self.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers().length - 1);
                viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].ActionType(TelponSel);
                $("#OfficeCountryCode" + index).prop('disabled', true);
                $("#OfficeCityCode" + index).prop('disabled', true);
                break;
            case ConsModification.Add:
                viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].ActionType(TelponSel);
                $("#OfficeCountryCode" + index).prop('disabled', false);
                $("#OfficeCityCode" + index).prop('disabled', false);
                viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].RetailCIFCBOContactUpdateID("");
                break;
            case ConsModification.Delete:
                self.AutoCompleteOfficePhoneNumber(self.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers().length - 1);
                viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers()[index].ActionType(TelponSel);
                $("#OfficeCountryCode" + index).prop('disabled', true);
                $("#OfficeCityCode" + index).prop('disabled', true);
                break;

            default:
                return;
        }
    };

    self.OnCIFMaritalStatusChange = function () {
        //var MaritalStatus = self.Selected().MaritalStatusID();

        //switch (MaritalStatus) {
        //    case ConsMaritalStatus.SudahMenikah:
        //        self.IsMaritalStatusSudahMenikah(true);
        //        break;
        //    case ConsMaritalStatus.BelumMenikah:
        //    case ConsMaritalStatus.DudaJanda:
        //        self.IsMaritalStatusSudahMenikah(false);
        //    default:
        //        return;
        //}
    };

    //self.OnCIFDispatchModeType = function () {
    //    var DispatchMode = self.Selected().DispatchModeType();

    //    switch (DispatchMode) {
    //        case ConsDispatchModeType.SudahMenikah:
    //            self.IsMaritalStatusSudahMenikah(true);
    //            break;
    //        case ConsMaritalStatus.BelumMenikah:
    //        case ConsMaritalStatus.DudaJanda:
    //            self.IsMaritalStatusSudahMenikah(false);
    //        default:
    //            return;
    //    }
    //};

    self.OnCIFTeleponRumahChange = function () {
        var TelponRumah = self.Selected().HomePhoneMethodID();
        //self.IsTeleponSelularEditVisible(true);
        self.IsTeleponRumahEditVisible(false);
        if (TelponRumah < 1 || TelponRumah > 3) {
            viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneMethodID().ID = ko.observable(0);
            viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneMethodID().Name = ko.observable("");
        }

        switch (TelponRumah) {
            case ConsModification.Edit:
                self.IsTeleponRumahEditVisible(true);
                viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneMethodID().ID = TelponRumah;
                viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneMethodID().Name = "";
                break;
            case ConsModification.Add:
            case ConsModification.Delete:
                self.IsTeleponRumahEditVisible(false);
                break;
            default:
                return;
        }
    };

    self.OnCIFTeleponKantorChange = function () {
        var TelponKantor = self.Selected().OfficePhoneMethodID();
        //self.IsTeleponSelularEditVisible(true);        
        //self.IsTeleponSelularEditVisible(true);     
        self.IsTeleponKantorEditVisible(false);
        if (TelponKantor < 1 || TelponKantor > 3) {
            viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneMethodID().ID = ko.observable(0);
            viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneMethodID().Name = ko.observable("");
        }
        switch (TelponKantor) {
            case ConsModification.Edit:
                self.IsTeleponKantorEditVisible(true);
                viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneMethodID().ID = TelponKantor;
                viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneMethodID().Name = "";
                break;
            case ConsModification.Add:
            case ConsModification.Delete:
                self.IsTeleponKantorEditVisible(false);
                break;
            default:
                return;
        }
    };

    self.OnCIFFaxChange = function () {
        var FaxVar = self.Selected().FaxMethodID();
        //self.IsTeleponSelularEditVisible(true);        
        //self.IsTeleponSelularEditVisible(true); 
        self.IsFaxEditVisible(false);
        if (FaxVar < 1 || FaxVar > 3) {
            viewModel.CIFTransactionModel().RetailCIFCBO().FaxMethodID().ID = ko.observable(0);
            viewModel.CIFTransactionModel().RetailCIFCBO().FaxMethodID().Name = ko.observable("");
        }
        switch (FaxVar) {
            case ConsModification.Edit:
                self.IsFaxEditVisible(true);
                viewModel.CIFTransactionModel().RetailCIFCBO().FaxMethodID().ID = FaxVar;
                viewModel.CIFTransactionModel().RetailCIFCBO().FaxMethodID().Name = "";
                break;
            case ConsModification.Add:
            case ConsModification.Delete:
                self.IsFaxEditVisible(false);
            default:
                return;
        }
    };
    //End by Haqi

    // Uploading document
    self.UploadDocumentUnderlying = function () {
        // show upload dialog
        // show the dialog task form
        $("#modal-form-Attach").modal('show');
        self.TempSelectedAttachUnderlying([]);
        self.ID_a(0);
        self.DocumentPurpose_a(new DocumentPurposeModel2('', '', ''));
        self.DocumentType_a(new DocumentTypeModel2('', ''));
        self.DocumentPath_a('');
        GetDataUnderlyingAttach();
        $('.remove').click();
    }

    self.UploadDocument = function () {
        self.DocumentPath(null);
        self.Selected().DocumentType(null);
        self.Selected().DocumentPurpose(null);
        self.DocumentType(null);
        self.DocumentPurpose(null);
        $("#modal-form-upload").modal('show');
        $('.remove').click();
    }

    //Started by haqi
    self.UploadDocumentCIF = function () {
        self.DocumentPath(null);
        self.Selected().DocumentType(null);
        self.DocumentType(null);
        $("#modal-form-upload-CIF").modal('show');
        $('.remove').click();
    };

    self.GetCIFandCustomerName = function () {
        viewModel.TempAttachemntDocument('');
        $("#modal-form-GetCIFandCustomerName").modal('show');
        $('.remove').click();
    };

    self.GetFFDAccountNumber = function () {
        viewModel.TempFFDAccount().AccountNumber('');
        viewModel.TempFFDAccount().IsAddFFDAccount(false);
        $("#modal-form-FFDAccountNumber").modal('show');
        $('.remove').click();
    };
    self.GetAddAcount = function () {
        viewModel.TempAddAccount().AccountNumber('');
        viewModel.Selected().Currency('');
        $("#modal-form-AddAccount").modal('show');
        $('.remove').click();
    };
    //end by haqi 

    self.GetDocument = function (data) {

        //console.log(ko.toJSON(data.ID));
        GetDataUnderlyingAttach();

        $.ajax({
            type: "Get",
            url: api.server + api.url.customerunderlyingfile + "/" + data.ID,
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                // send notification
                if (jqXHR.status = 200) {
                    //console.log(ko.toJSON(data));

                    self.DocumentPurpose_a(data.DocumentPurpose);
                    self.DocumentPath_a(data.DocumentPath);
                    self.DocumentType_a(data.DocumentType);
                    self.FileName_a(data.FileName);

                    self.IsEditableDocument(true);

                    GetDataUnderlying();

                    // refresh data
                    // GetDataAttachFile();
                } else
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
        $("#modal-form-Attach").modal('show');
    }

    // Add new document handler
    self.AddDocument = function () {
        var vDocumentType = ko.utils.arrayFirst(viewModel.Parameter().DocumentTypes(), function (item) {
            return item.ID == self.Selected().DocumentType();
        });

        var vDocumentPurpose = ko.utils.arrayFirst(viewModel.Parameter().DocumentPurposes(), function (item) {
            return item.ID == self.Selected().DocumentPurpose();
        });

        var file = $('#document-path').data().ace_input_files[0];

        var doc = {
            ID: 0,
            Type: vDocumentType,
            Purpose: vDocumentPurpose,
            FileName: file.name,
            DocumentPath: file,
            LastModifiedDate: new Date(),
            LastModifiedBy: null

            //ID: 0,
            //Type: self.DocumentType(),
            //Purpose: self.DocumentPurpose(),
            //FileName: self.DocumentFileName(),
            //DocumentPath: self.DocumentPath(),
            //LastModifiedDate: new Date(),
            //LastModifiedBy: null
        };
        if (doc.Type == null || doc.Purpose == null || doc.DocumentPath == null) {
            alert("Please complete the upload form fields.")
        } else {
            self.Documents.push(doc);
            if (viewModel.TransactionModel().IsIPETMO() == true) {
                var documentStatementLetter = ko.utils.arrayFilter(self.Documents(), function (items) {
                    return items.Purpose.Name == "Statement Letter";
                });
                var documentUnderlying = ko.utils.arrayFilter(self.Documents(), function (itemu) {
                    return itemu.Purpose.Name == "Underlying";
                });
                var documentInstruction = ko.utils.arrayFilter(self.Documents(), function (itemi) {
                    return itemi.Purpose.Name == "Instruction";
                });
                if (documentStatementLetter != "") {
                    viewModel.TransactionModel().StatementLetter(true);
                }
                if (documentUnderlying != "") {
                    viewModel.TransactionModel().Underlying(true);
                }
                if (documentInstruction != "") {
                    viewModel.TransactionModel().Instruction(true);
                }
            }
            //add by fandi
            self.IsNewDocument(false);
            //end
            $('.remove').click();
            // hide upload dialog
            $("#modal-form-upload").modal('hide');
        }

        // test upload
        /*var options = {
         url: "/_api/web/GetFolderByServerRelativeUrl('/sites/fileuploadtest/Documents/Communities)/Files/Add(url='"+ file.name + "',overwrite=true)"
         };
         Helper.Ajax.Post(options, OnSuccessUpload, OnError);*/
    };

    //Started by haqi
    self.AddDocumentCIF = function () {
        var purposeDoc = {
            ID: 1,
            Name: "Instruction",
            Description: "Instruction"
        };
        var doc = {
            ID: 0,
            Type: self.DocumentType(),
            Purpose: purposeDoc,
            FileName: self.DocumentFileName(),
            DocumentPath: self.DocumentPath(),
            LastModifiedDate: new Date(),
            LastModifiedBy: null
        };

        if (doc.Type == null || doc.DocumentPath == null) {
            alert("Please complete the upload form fields.")
        } else {
            self.DocumentsCIF.push(doc);
            $('.remove').click();

            $("#modal-form-upload-CIF").modal('hide');
        }
    };

    self.AddJoinCIFforAddCurrency = function () {
        if (self.TempAttachemntDocument() == null || self.TempAttachemntDocument() == '') {
            ShowNotification("Attention", "Please insert customer join", 'gritter-warning', true);
        } else {
            self.TempAttachemntDocuments.push(self.TempAttachemntDocument());
            $("#modal-form-GetCIFandCustomerName").modal('hide');
        }
    };

    self.AddFFDAccount = function () {
        if (self.TempFFDAccount().AccountNumber() == null || self.TempFFDAccount().AccountNumber() == '') {
            ShowNotification("Attention", "Please insert account", 'gritter-warning', true);
        }
        else {
            var accNum = self.TempFFDAccount().AccountNumber();
            var cbxAddAcc = self.TempFFDAccount().IsAddFFDAccount();
            var tempFFD = {
                AccountNumber: accNum,
                IsAddFFDAccount: cbxAddAcc
            };
            self.TempFFDAccounts.push(tempFFD);
            $("#modal-form-FFDAccountNumber").modal('hide');
        }
    };

    self.AddAccount = function () {
        if (self.TempAddAccount().AccountNumber() == null || self.TempAddAccount().AccountNumber() == '') {
            ShowNotification("Attention", "Please insert account", 'gritter-warning', true);
        }
        else if (viewModel.Selected().Currency() == null) {
            ShowNotification("Attention", "Please insert currency", 'gritter-warning', true);
        }
        else {

            var currencymodal = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) {
                return item.ID == viewModel.Selected().Currency();
            });
            var accNum = self.TempFFDAccount().AccountNumber();
            var tempAddAccount = {
                AccountNumber: accNum,
                Currency: currencymodal
            };

            self.TempAddAccounts.push(tempAddAccount);
            $("#modal-form-AddAccount").modal('hide');
        }
    };

    self.AddAccountNumberLienUnlien = function () {
        if (self.LienUnliendata() == null || self.LienUnliendata() == '' || self.LienUnliendata() === undefined) {
            ShowNotification("Attention", "Please select Line or Un-lien", 'gritter-warning', true);
        } else {

            var temp = ko.utils.arrayFirst(self.CIFTransactionModel().AccountNumberLienUnlien(), function (item) {
                if (item().AccountNumber() == self.AccountNumberLienUnLienData()) {
                    return item;
                }
            });

            //console.log(item().AccountNumber());
            //console.log(self.LienUnliendata());            
            temp().LienUnlien(self.LienUnliendata());
            //console.log(temp());
            $("#modal-form-EditAccountNumber").modal('hide');
        }
    };
    //End by haqi

    self.EditDocument = function (data) {
        self.DocumentPath(data.DocumentPath);
        self.Selected().DocumentType(data.Type.ID);
        self.Selected().DocumentPurpose(data.Purpose.ID);

        // show upload dialog
        $("#modal-form-upload").modal('show');
    };

    //Started by haqi
    self.EditDocumentCIF = function (data) {
        self.DocumentPath(data.DocumentPath);
        self.Selected().DocumentType(data.Type.ID);
        //self.Selected().DocumentPurpose(data.Purpose.ID);

        // show upload dialog
        $("#modal-form-upload-CIF").modal('show');
    };
    //end by haqi

    self.RemoveDocument = function (data) {
        //alert(JSON.stringify(data))
        self.Documents.remove(data);
    };

    //Start by haqi
    self.RemoveDocumentCIF = function (data) {
        self.DocumentsCIF.remove(data);
    };
    self.RemoveCifAccJoin = function (data) {
        self.TempAttachemntDocuments.remove(data);
    };
    self.RemoveAddAccountCIF = function (data) {
        self.TempAddAccounts.remove(data);
    };
    self.RemoveFFDAccountCIF = function (data) {
        self.TempFFDAccounts.remove(data);
    };
    //end by haqi

    self.counterUpload = ko.observable(0);

    // Save as draft handler
    self.SaveAsDraft = function () {
        self.IsEditable(false);
        switch (self.ProductID()) {
            case ConsProductID.SKNBulkProductIDCons: //add aridya 20161012 skn bulk ~OFFLINE~
                var data = {
                    ApplicationID: self.TransactionModel().ApplicationID(),
                    CIF: self.TransactionModel().Customer().CIF,
                    Name: self.TransactionModel().Customer().Name
                };
                self.TransactionModel().IsDraft(true);
                if (self.TransactionModel().ApplicationDate() == '') {
                    self.TransactionModel().ApplicationDate('1970/01/01');
                    self.TransactionModel().ExecutionDate('1970/01/01');
                }
                //SaveTransaction();
                //Andi
                SaveDraftPayment();
                //End Andi
                break;
            case ConsProductID.RTGSProductIDCons:
                if (StatusIPE.toLowerCase() !== 'bcp2') {
                    //if (viewModel.ProductID() == ConsProductID.RTGSProductIDCons) {
                    viewModel.TransactionModel().BranchID = viewModel.selectedBranchID();
                    //}
                }
            case ConsProductID.SKNProductIDCons:
            case ConsProductID.OTTProductIDCons:
            case ConsProductID.OverbookingProductIDCons:
                {
                    var data = {
                        ApplicationID: self.TransactionModel().ApplicationID(),
                        CIF: self.TransactionModel().Customer().CIF,
                        Name: self.TransactionModel().Customer().Name
                    };
                    self.TransactionModel().IsDraft(true);
                    if (self.TransactionModel().ApplicationDate() == '') {
                        self.TransactionModel().ApplicationDate('1970/01/01');
                        self.TransactionModel().ExecutionDate('1970/01/01');
                    }
                    if (viewModel.TrxRate() == "" || viewModel.TrxRate() === undefined || viewModel.TrxRate() === null) {
                        viewModel.TransactionModel().TrxRate(0);
                    } else {
                        viewModel.TransactionModel().TrxRate(viewModel.TrxRate());
                    }
                    //SaveTransaction();
                    //Andi
                    SaveDraftPayment();
                    //End Andi
                }
                break;
            case ConsProductID.TMOProductIDCons:
                {
                    self.TransactionTMOModel().IsDraft(true);
                    if (self.TransactionTMOModel().ApplicationDate() == '') {
                        self.TransactionTMOModel().ApplicationDate('1970/01/01');
                        self.TransactionTMOModel().ExecutionDate('1970/01/01');
                    }
                    self.IsEditable(false);
                    SaveDraftTMO();
                }
                break;
            case ConsProductID.UTOnshoreproductIDCons:
            case ConsProductID.UTOffshoreProductIDCons:
            case ConsProductID.UTCPFProductIDCons:
            case ConsProductID.SavingPlanProductIDCons:
            case ConsProductID.IDInvestmentProductIDCons:
                {
                    if (self.ProductID() == ConsProductID.IDInvestmentProductIDCons) {
                        var SelectedFunc = viewModel.Selected().FunctionType();
                        if (SelectedFunc != ConsUTPar.funcClose) {
                            if (viewModel.Selected().AccountType() == null) {
                                ShowNotification("Attention", "Please input Account Type", 'gritter-warning', true);
                                self.IsEditable(true);
                                return false;
                            }
                            if (viewModel.Selected().FunctionType() == null) {
                                ShowNotification("Attention", "Please input Function Type", 'gritter-warning', true);
                                self.IsEditable(true);
                                return false;
                            }
                            if (viewModel.Selected().FNACore() == null) {
                                ShowNotification("Attention", "Please input FNA Core", 'gritter-warning', true);
                                self.IsEditable(true);
                                return false;
                            }
                        }
                    }
                    self.TransactionUTModel().IsDraft(true);
                    self.IsEditable(false);
                    SaveDraftUT();
                }
                break;
            case ConsProductID.FDProductIDCons:
                SaveFD(true);
                break;
            case ConsProductID.LoanDisbursmentProductIDCons:
            case ConsProductID.LoanRolloverProductIDCons:
            case ConsProductID.LoanIMProductIDCons:
            case ConsProductID.LoanSettlementProductIDCons:
                {
                    var data = {
                        ApplicationID: self.TransactionLoanModel().ApplicationID(),
                        CIF: self.TransactionLoanModel().Customer().CIF,
                        Name: self.TransactionLoanModel().Customer().Name
                    };
                    self.TransactionLoanModel().IsDraft(true);
                    if (self.TransactionLoanModel().ApplicationDate() == '') {
                        self.TransactionLoanModel().ApplicationDate('1970/01/01');
                        self.TransactionLoanModel().ExecutionDate('1970/01/01');
                    }
                    SaveDraftLoan();
                }
                break;
            case ConsProductID.CollateralProductIDCons:
                break;
            case ConsProductID.CIFProductIDCons:
                self.CIFTransactionModel().IsDraft(true);
                self.IsEditable(false);

                //henggar 190417
                if (self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsReligion()) {
                    viewModel.CIFTransactionModel().RetailCIFCBO().Religion(viewModel.Selected().Religion());
                }

                if (self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsEducation()) {
                    viewModel.CIFTransactionModel().RetailCIFCBO().Education(viewModel.Selected().Education());
                }

                if (self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsSLIK()) {
                    viewModel.CIFTransactionModel().RetailCIFCBO().GrosIncomeccy(viewModel.Selected().GrossIncomeCCY());
                }

                if (self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsFatcaCRS()) {
                    viewModel.CIFTransactionModel().RetailCIFCBO().FatcaCountryCode(viewModel.Selected().BeneficiaryCountry());
                    viewModel.CIFTransactionModel().RetailCIFCBO().FatcaReviewStatus(viewModel.Selected().FATCAReviewStatus());
                    viewModel.CIFTransactionModel().RetailCIFCBO().FatcaCRSStatus(viewModel.Selected().FATCACRSStatus());
                    viewModel.CIFTransactionModel().RetailCIFCBO().FatcaTaxPayer(viewModel.Selected().TaxPayer());
                    viewModel.CIFTransactionModel().RetailCIFCBO().FatcaWithHolding(viewModel.Selected().WithholdingCertificationType())
                }

                //handle integer cant to be null
                if (self.CIFTransactionModel().RetailCIFCBO().DataUBOCif() == "") {
                    viewModel.CIFTransactionModel().RetailCIFCBO().DataUBOCif(0);
                }

                if (self.CIFTransactionModel().RetailCIFCBO().GrosIncomeccy() == "") {
                    viewModel.CIFTransactionModel().RetailCIFCBO().GrosIncomeccy(0);
                }

                if (self.CIFTransactionModel().RetailCIFCBO().GrossCifSpouse() == "") {
                    viewModel.CIFTransactionModel().RetailCIFCBO().GrossCifSpouse(0);
                }

                if (self.CIFTransactionModel().RetailCIFCBO().GrossIncomeAmount() == "") {
                    viewModel.CIFTransactionModel().RetailCIFCBO().GrossIncomeAmount(0);
                }

                if (self.CIFTransactionModel().RetailCIFCBO().Education() == "") {
                    viewModel.CIFTransactionModel().RetailCIFCBO().Education(0);
                }

                if (self.CIFTransactionModel().RetailCIFCBO().Religion() == "") {
                    viewModel.CIFTransactionModel().RetailCIFCBO().Religion(0);
                }
                //end

                SaveDraftCIF();
                break;
            default:
                return;
        }

    };
    function CheckEmailApp() {
        var docs = viewModel.Documents();
        var EmailApp = config.validate.approval.emailapp;
        var isAttachedEmail = false;
        for (var i = 0; i < docs.length; i++) {
            if (EmailApp.indexOf(docs[i].Type["Name"]) > -1) {
                isAttachedEmail = true;
            }
        }
        return isAttachedEmail;
    }
    function CheckEmailAppCIF() {
        var docs = viewModel.DocumentsCIF();
        var EmailApp = config.validate.approval.emailapp;
        var isAttachedEmail = false;
        for (var i = 0; i < docs.length; i++) {
            if (EmailApp.indexOf(docs[i].Type["Name"]) > -1) {
                isAttachedEmail = true;
            }
        }
        return isAttachedEmail;
    }

    function IsvalidUT() {
        var isInvestmentAdd = false;
        switch (viewModel.ProductID()) {
            case ConsProductID.IDInvestmentProductIDCons:
                var SelectedFunc = viewModel.Selected().FunctionType();
                if (SelectedFunc != ConsUTPar.funcClose) {
                    if (viewModel.Selected().FunctionType() == null) {
                        ShowNotification("Attention", "Please input Function Type", 'gritter-warning', true);
                        return false;
                    }
                    if (viewModel.Selected().AccountType() == null) {
                        ShowNotification("Attention", "Please input Account Type", 'gritter-warning', true);
                        return false;
                    }
                    if (viewModel.Selected().FNACore() == null) {
                        ShowNotification("Attention", "Please input FNA Core", 'gritter-warning', true);
                        return false;
                    }
                }
                var selFNA = viewModel.Selected().FNACore();
                var selFunc = viewModel.Selected().FunctionType();
                var isJoin = viewModel.IsUTJoin();
                if (selFNA == ConsUTPar.fnaYes) {
                    if (selFunc == ConsUTPar.funcAdd) {
                        isInvestmentAdd = true;
                        var minRisk = viewModel.MinRisk()[0].Name;
                        var maxRisk = viewModel.MaxRisk()[0].Name;
                        if (viewModel.TransactionUTModel().RiskScore() == null) {
                            ShowNotification("Attention", "Maximum Risk = " + maxRisk + ", Minimum Risk=" + minRisk + "", 'gritter-warning', true);
                            return false;
                        }
                        if (viewModel.TransactionUTModel().RiskScore() > Number(maxRisk) || viewModel.TransactionUTModel().RiskScore() < Number(minRisk)) {
                            ShowNotification("Attention", "Maximum Risk = " + maxRisk + ", Minimum Risk=" + minRisk + "", 'gritter-warning', true);
                            return false;
                        }
                        if (viewModel.TransactionUTModel().OperativeAccount() == null || viewModel.TransactionUTModel().OperativeAccount() == '' || viewModel.TransactionUTModel().OperativeAccount() == undefined) {
                            ShowNotification("Attention", "Please input operative account", 'gritter-warning', true);
                            return false;
                        }
                        //26-1-2016 dani
                        if (viewModel.TransactionUTModel().OperativeAccount() != null || viewModel.TransactionUTModel().OperativeAccount() != '' || viewModel.TransactionUTModel().OperativeAccount() != undefined) {
                            var tempData = viewModel.TransactionUTModel().OperativeAccount().toString();
                            if (tempData.length != 10) {
                                ShowNotification("Attention", "Length of Operative Account must be 10 characters", 'gritter-warning', true);
                                return false;
                            }
                        }
                        //26-1-2016 dani end
                        if (viewModel.TransactionUTModel().SolID() == null || viewModel.TransactionUTModel().SolID() == '') {
                            ShowNotification("Attention", "Please input SOL ID", 'gritter-warning', true);
                            return false;
                        }
                        if (viewModel.TransactionUTModel().CustomerRiskEffectiveDate() == null || viewModel.TransactionUTModel().CustomerRiskEffectiveDate() == '') {
                            ShowNotification("Attention", "Please input Customer risk expiry date", 'gritter-warning', true);
                            return false;
                        }
                        if (viewModel.TransactionUTModel().RiskProfileExpiryDate() == null || viewModel.TransactionUTModel().RiskProfileExpiryDate() == '') {
                            ShowNotification("Attention", "Please input risk profile expiry date", 'gritter-warning', true);
                            return false;
                        }
                    }
                    else if (selFunc == ConsUTPar.funcModify) {
                        //10-7-2017 Arsel
                        if (viewModel.TransactionUTModel().OperativeAccount() != null && viewModel.TransactionUTModel().OperativeAccount() != '' && viewModel.TransactionUTModel().OperativeAccount() != undefined) {
                            var tempData = viewModel.TransactionUTModel().OperativeAccount().toString();
                            if (tempData.length != 10) {
                                ShowNotification("Attention", "Length of Operative Account must be 10 characters", 'gritter-warning', true);
                                return false;
                            }
                        }
                    }
                }
                else {
                    if (selFunc == ConsUTPar.funcAdd) {
                        isInvestmentAdd = true;
                        if (viewModel.TransactionUTModel().OperativeAccount() == null || viewModel.TransactionUTModel().OperativeAccount() == '' || viewModel.TransactionUTModel().OperativeAccount() == undefined) {
                            ShowNotification("Attention", "Please input operative account", 'gritter-warning', true);
                            return false;
                        }
                        //26-1-2016 dani
                        if (viewModel.TransactionUTModel().OperativeAccount() != null || viewModel.TransactionUTModel().OperativeAccount() != '' || viewModel.TransactionUTModel().OperativeAccount() != undefined) {
                            var tempData = viewModel.TransactionUTModel().OperativeAccount().toString();
                            if (tempData.length != 10) {
                                ShowNotification("Attention", "Length of Operative Account must be 10 characters", 'gritter-warning', true);
                                return false;
                            }
                        }
                        //26-1-2016 dani end
                        if (viewModel.TransactionUTModel().SolID() == null || viewModel.TransactionUTModel().SolID() == '') {
                            ShowNotification("Attention", "Please input SOL ID", 'gritter-warning', true);
                            return false;
                        }
                    }
                    else if (selFunc == ConsUTPar.funcModify) {
                        //10-7-2017 Arsel
                        if (viewModel.TransactionUTModel().OperativeAccount() != null && viewModel.TransactionUTModel().OperativeAccount() != '' && viewModel.TransactionUTModel().OperativeAccount() != undefined) {
                            var tempData = viewModel.TransactionUTModel().OperativeAccount().toString();
                            if (tempData.length != 10) {
                                ShowNotification("Attention", "Length of Operative Account must be 10 characters", 'gritter-warning', true);
                                return false;
                            }
                        }
                    }
                }
                if (isJoin == true) {
                    if (selFNA == ConsUTPar.fnaYes) {
                        if (selFunc != ConsUTPar.funcModify) {
                            if (viewModel.UTJoinFNA() == null || viewModel.UTJoinFNA() == undefined) {
                                ShowNotification("Attention", "Please input join account", 'gritter-warning', true);
                                return false;
                            }
                            else {
                                if (viewModel.UTJoinFNA().length == 0) {
                                    ShowNotification("Attention", "Please input join account", 'gritter-warning', true);
                                    return false;
                                }
                            }
                        }
                        //26-1-2016 dani
                        if (viewModel.TransactionUTModel().OperativeAccount() != null && viewModel.TransactionUTModel().OperativeAccount() != '' && viewModel.TransactionUTModel().OperativeAccount() != undefined) {
                            var tempData = viewModel.TransactionUTModel().OperativeAccount().toString();
                            if (tempData.length != 10) {
                                ShowNotification("Attention", "Length of Operative Account must be 10 characters", 'gritter-warning', true);
                                return false;
                            }
                        }
                        //26-1-2016 dani end
                    }
                    else if (selFNA == ConsUTPar.fnaNo) {
                        if (selFunc != ConsUTPar.funcModify) {
                            if (viewModel.UTJoinNonFNA() == null || viewModel.UTJoinNonFNA() == undefined) {
                                ShowNotification("Attention", "Please input join account", 'gritter-warning', true);
                                return false;
                            }
                            else {
                                if (viewModel.UTJoinNonFNA().length == 0) {
                                    ShowNotification("Attention", "Please input join account", 'gritter-warning', true);
                                    return false;
                                }
                            }
                        }
                        //26-1-2016 dani
                        if (viewModel.TransactionUTModel().OperativeAccount() != null && viewModel.TransactionUTModel().OperativeAccount() != '' && viewModel.TransactionUTModel().OperativeAccount() != undefined) {
                            var tempData = viewModel.TransactionUTModel().OperativeAccount().toString();
                            if (tempData.length != 10) {
                                ShowNotification("Attention", "Length of Operative Account must be 10 characters", 'gritter-warning', true);
                                return false;
                            }
                        }
                        //26-1-2016 dani end
                    }
                }

                break;
            case ConsProductID.UTCPFProductIDCons:
            case ConsProductID.SavingPlanProductIDCons:
            case ConsProductID.UTOffshoreProductIDCons:
            case ConsProductID.UTOnshoreproductIDCons:
                var IsSP = viewModel.IsSP();
                var IsSubscription = viewModel.IsSubscription();
                var IsRedemption = viewModel.IsRedemption();
                var IsSwitching = viewModel.IsSwitching();
                //added by dani 19-02-2016
                if (viewModel.ProductID() == ConsProductID.UTCPFProductIDCons || viewModel.ProductID() == ConsProductID.UTOffshoreProductIDCons || viewModel.ProductID() == ConsProductID.UTOnshoreproductIDCons) {
                    if (viewModel.Selected().Transaction_Type() == null) {
                        ShowNotification("Attention", "Please input Transaction Type", 'gritter-warning', true);
                        self.IsEditable(true);
                        return false;
                    }
                    if (viewModel.TransactionUTModel().Investment() == undefined || viewModel.TransactionUTModel().Investment() == null || viewModel.TransactionUTModel().Investment().toString().trim() == "") {
                        ShowNotification("Attention", "Please input Investment ID", 'gritter-warning', true);
                        self.IsEditable(true);
                        return false;
                    }
                }
                //end
                if (IsSP == true) {
                    if (viewModel.MutualFundColl() == null || viewModel.MutualFundColl() == undefined) {
                        ShowNotification("Attention", "Please input mutual fund", 'gritter-warning', true);
                        return false;
                    }
                    else {
                        if (viewModel.MutualFundColl().length == 0) {
                            ShowNotification("Attention", "Please input mutual fund", 'gritter-warning', true);
                            return false;
                        }
                    }
                }
                else if (IsSubscription == true) {
                    var ret = viewModel.SubcriptionColl();
                    var sendVal = [];
                    for (var i = 0; i < ret.length; i++) {
                        var mf = {
                            MutualFundList: ret[i].MutualFundList,
                            MutualCurrency: null,
                            MutualAmount: ret[i].MutualAmount,
                            MutualFundSwitchFrom: null,
                            MutualFundSwitchTo: null,
                            MutualPartial: null,
                            MutualUnitNumber: null,
                            MutualSelected: null
                        };
                        sendVal.push(mf);
                    }

                    if (sendVal.length == 0) {
                        ShowNotification("Attention", "Please input mutual Fund", 'gritter-warning', true);
                        return false;
                    }

                }
                else if (IsRedemption == true) {
                    var ret = viewModel.RedemptionColl();
                    var sendVal = [];
                    for (var i = 0; i < ret.length; i++) {
                        if (ret[i].MutualSelected == true) {
                            var mf = {
                                MutualFundList: ret[i].MutualFundList,
                                MutualCurrency: null,
                                MutualAmount: null,
                                MutualFundSwitchFrom: null,
                                MutualFundSwitchTo: null,
                                MutualPartial: ret[i].MutualPartial,
                                MutualUnitNumber: ret[i].MutualUnitNumber,
                                MutualSelected: null
                            };
                            sendVal.push(mf);
                        }
                    }
                    if (sendVal.length == 0) {
                        ShowNotification("Attention", "Please select a Fund", 'gritter-warning', true);
                        return false;
                    }
                }
                else if (IsSwitching == true) {
                    var ret = viewModel.SwitchingColl();
                    var sendVal = [];
                    for (var i = 0; i < ret.length; i++) {
                        if (ret[i].MutualSelected == true) {
                            var mf = {
                                MutualFundList: ret[i].MutualFundList,
                                MutualCurrency: null,
                                MutualAmount: null,
                                MutualFundSwitchFrom: ret[i].MutualFundSwitchFrom,
                                MutualFundSwitchTo: ret[i].MutualFundSwitchTo,
                                MutualPartial: ret[i].MutualPartial,
                                MutualUnitNumber: ret[i].MutualUnitNumber,
                                MutualSelected: null
                            };
                            sendVal.push(mf);
                        }
                    }
                    if (sendVal.length == 0) {
                        ShowNotification("Attention", "Please select a Fund", 'gritter-warning', true);
                        return false;
                    }
                }
                break;
        }
        if (viewModel.Documents() == null || viewModel.Documents().length == 0) {
            if (SelectedFunc != ConsUTPar.funcClose) {
                if (!isInvestmentAdd) {
                    ShowNotification("Attention", "You need to upload attachment to continue transaction.", 'gritter-warning', true);
                    return false;
                }
            }
        }
        return true;
    }

    function IsvalidLoan() {
        if (viewModel.Documents() == null || viewModel.Documents().length == 0) {
            ShowNotification("Attention", "You need to upload attachment to continue transaction.", 'gritter-warning', true);
            return false;
        }
        return true;
    }
    function ValidateEmail(email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
    };
    function IsvalidCIF() {
        //var isAttachMandatory = true; comment azam
        var isAttachMandatory = false;
        var isAddAcc = false;
        var isThicked = false;
        var MaintainType = self.Selected().MaintenanceType();
        switch (MaintainType) {
            case ConsCIFMaintenanceType.AtmCardCons:
                if (!(viewModel.CIFTransactionModel().TransactionSubType().ID > 0)) {
                    if (viewModel.CIFTransactionModel().TransactionSubType().ID() == "" || viewModel.CIFTransactionModel().TransactionSubType().ID() === null || viewModel.CIFTransactionModel().TransactionSubType().ID() === undefined) {
                        ShowNotification("Attention", "Sub Type must be selected", 'gritter-warning', true);
                        return false;
                    }
                }

                if (viewModel.CIFTransactionModel().ATMNumber() == "" || viewModel.CIFTransactionModel().ATMNumber() === null || viewModel.CIFTransactionModel().ATMNumber() === undefined) {
                    ShowNotification("Attention", "ATM Number must be filled", 'gritter-warning', true);
                    return false;
                }
                isAttachMandatory = true;
                break;
            case ConsCIFMaintenanceType.AddCurrencyCons:
                //if (Number(ko.toJS(viewModel.TempAttachemntDocuments().length)) <= 0) {
                //    ShowNotification("Attention", "Please input at least one join account", 'gritter-warning', true);
                //    return false;
                //}
                isAddAcc = false;
                isAttachMandatory = false;
                break;
            case ConsCIFMaintenanceType.AdditionalAccountCons:
                isAddAcc = true;
                break;
            case ConsCIFMaintenanceType.ActiveDormantCons:
                var isCentang = false;
                if (viewModel.TempDormantAccounts() != null && viewModel.TempDormantAccounts().length > 0) {
                    ko.utils.arrayForEach(viewModel.TempDormantAccounts(), function (item) {
                        if (item.IsAddDormantAccount == true)
                            isCentang = true;
                    });
                }
                if (isCentang == false) {
                    ShowNotification("Attention", "You need to check Account Number", 'gritter-warning', true);
                    return false;
                }
                break;
            case ConsCIFMaintenanceType.FreezeUnfreezeCons:
                var isCentang = false;
                if (viewModel.TempFreezeAccounts() != null && viewModel.TempFreezeAccounts().length > 0) {
                    ko.utils.arrayForEach(viewModel.TempFreezeAccounts(), function (item) {
                        if (item.IsAddTblFreezeAccount == true)
                            isCentang = true;
                    });
                }
                if (isCentang == false) {
                    ShowNotification("Attention", "You need to check Account Number", 'gritter-warning', true);
                    return false;
                }
                break;
            case ConsCIFMaintenanceType.TagUntagStaffCons:
                isAttachMandatory = false;
                if (viewModel.CIFTransactionModel().StaffID() == null || viewModel.CIFTransactionModel().StaffID() == undefined || viewModel.CIFTransactionModel().StaffID() == "" || viewModel.CIFTransactionModel().StaffID().length != 6) {
                    ShowNotification("Attention", "Length of Staff ID must be 6(six) character", 'gritter-warning', true);
                    return false;
                }
                break;
            case ConsCIFMaintenanceType.PengkinianDataCons:
                isAttachMandatory = true;
                //mohon tempatkan paling atas
                if (viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPhoneFaxEmailMaintenance() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsNameMaintenance() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsMaritalStatusMaintenance() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsNationalityMaintenance() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsMonthlyIncomeMaintenance() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsMonthlyTransactionMaintenance() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsIdentityTypeMaintenance() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsCorrespondenceMaintenance() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsCorrespondenseAddressMaintenance() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsFundSourceMaintenance() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsJobMaintenance() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsNPWPMaintenance() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsIdentityAddressMaintenance() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPhoneFaxEmailMaintenance() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsNetAssetMaintenance() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsAccountPurposeMaintenance() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsOthers() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsMotherMaiden() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsReligion() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsEducation() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsDataUBO() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsTransparansiDataPenawaranProduct() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsSLIK() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsChangeSignature() == false &&
                    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsFatcaCRS() == false
                    ) {
                    ShowNotification("Attention", "Please select one of Information Type", 'gritter-warning', true);
                    return false;
                }

                if (viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPhoneFaxEmailMaintenance() == true) {


                    //end hidayat validasi
                    if (viewModel.Selected().CellPhoneMethodID() != undefined && viewModel.Selected().CellPhoneMethodID() != null && viewModel.Selected().CellPhoneMethodID() != "") {
                        //add or delete
                        if (viewModel.Selected().CellPhoneMethodID() == 1 || viewModel.Selected().CellPhoneMethodID() == 3) {
                            if (viewModel.CIFTransactionModel().RetailCIFCBO().CellPhone() == undefined || viewModel.CIFTransactionModel().RetailCIFCBO().CellPhone() == null || viewModel.CIFTransactionModel().RetailCIFCBO().CellPhone() == "") {
                                ShowNotification("Attention", "'Telepon Seluler' must be filled.", 'gritter-warning', true);
                                return false;
                            }
                        }
                        //update
                        if (viewModel.Selected().CellPhoneMethodID() == 2) {
                            if (viewModel.CIFTransactionModel().RetailCIFCBO().CellPhone() == undefined || viewModel.CIFTransactionModel().RetailCIFCBO().CellPhone() == null || viewModel.CIFTransactionModel().RetailCIFCBO().CellPhone() == ""
                                || viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedCellPhone() == undefined || viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedCellPhone() == null || viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedCellPhone() == "") {
                                ShowNotification("Attention", "'Telepon Seluler' must be filled and 'Updated To' must be filled.", 'gritter-warning', true);
                                return false;
                            }
                        }
                    }
                    if (viewModel.Selected().HomePhoneMethodID() != undefined && viewModel.Selected().HomePhoneMethodID() != null && viewModel.Selected().HomePhoneMethodID() != "") {
                        //add or delete
                        if (viewModel.Selected().HomePhoneMethodID() == 1 || viewModel.Selected().HomePhoneMethodID() == 3) {
                            if (viewModel.CIFTransactionModel().RetailCIFCBO().HomePhone() == undefined || viewModel.CIFTransactionModel().RetailCIFCBO().HomePhone() == null || viewModel.CIFTransactionModel().RetailCIFCBO().HomePhone() == "") {
                                ShowNotification("Attention", "'Telepon Rumah' must be filled.", 'gritter-warning', true);
                                return false;
                            }
                        }
                        //update
                        if (viewModel.Selected().HomePhoneMethodID() == 2) {
                            if (viewModel.CIFTransactionModel().RetailCIFCBO().HomePhone() == undefined || viewModel.CIFTransactionModel().RetailCIFCBO().HomePhone() == null || viewModel.CIFTransactionModel().RetailCIFCBO().HomePhone() == ""
                                || viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedHomePhone() == undefined || viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedHomePhone() == null || viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedHomePhone() == "") {
                                ShowNotification("Attention", "'Telepon Rumah' must be filled and 'Updated To' must be filled.", 'gritter-warning', true);
                                return false;
                            }
                        }
                    }
                    if (viewModel.Selected().OfficePhoneMethodID() != undefined && viewModel.Selected().OfficePhoneMethodID() != null && viewModel.Selected().OfficePhoneMethodID() != "") {
                        //add or delete
                        if (viewModel.Selected().OfficePhoneMethodID() == 1 || viewModel.Selected().OfficePhoneMethodID() == 3) {
                            if (viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhone() == undefined || viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhone() == null || viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhone() == "") {
                                ShowNotification("Attention", "'Telepon Kantor' must be filled.", 'gritter-warning', true);
                                return false;
                            }
                        }
                        //update
                        if (viewModel.Selected().OfficePhoneMethodID() == 2) {
                            if (viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhone() == undefined || viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhone() == null || viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhone() == ""
                                || viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedOfficePhone() == undefined || viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedOfficePhone() == null || viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedOfficePhone() == "") {
                                ShowNotification("Attention", "'Telepon Kantor' must be filled and 'Updated To' must be filled.", 'gritter-warning', true);
                                return false;
                            }
                        }
                    }
                    if (viewModel.Selected().FaxMethodID() != undefined && viewModel.Selected().FaxMethodID() != null && viewModel.Selected().FaxMethodID() != "") {
                        //add or delete
                        if (viewModel.Selected().FaxMethodID() == 1 || viewModel.Selected().FaxMethodID() == 3) {
                            if (viewModel.CIFTransactionModel().RetailCIFCBO().Fax() == undefined || viewModel.CIFTransactionModel().RetailCIFCBO().Fax() == null || viewModel.CIFTransactionModel().RetailCIFCBO().Fax() == "") {
                                ShowNotification("Attention", "'Fax' must be filled.", 'gritter-warning', true);
                                return false;
                            }
                        }
                        //update
                        if (viewModel.Selected().FaxMethodID() == 2) {
                            if (viewModel.CIFTransactionModel().RetailCIFCBO().Fax() == undefined || viewModel.CIFTransactionModel().RetailCIFCBO().Fax() == null || viewModel.CIFTransactionModel().RetailCIFCBO().Fax() == ""
                                || viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedFax() == undefined || viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedFax() == null || viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedFax() == "") {
                                ShowNotification("Attention", "'Fax' must be filled and 'Updated To' must be filled.", 'gritter-warning', true);
                                return false;
                            }
                        }
                    }

                }
                //for korespondensi if laporan bulanan dikirim lewat email
                if (viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsCorrespondenceMaintenance() == true) {
                    if (viewModel.CIFTransactionModel().RetailCIFCBO().IsCorrespondenseToEmail()) {
                        if (viewModel.CIFTransactionModel().RetailCIFCBO().EmailAddress() == null || viewModel.CIFTransactionModel().RetailCIFCBO().EmailAddress() == undefined || viewModel.CIFTransactionModel().RetailCIFCBO().EmailAddress().trim() == '') {
                            ShowNotification("Attention", "Please fill 'Alamat Email'.", 'gritter-warning', true);
                            return false;
                        }
                    }
                }
                if (viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsAccountPurposeMaintenance() == true) {
                    if (viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsTujuanBukaRekeningLainnya() == true) {
                        if (viewModel.CIFTransactionModel().RetailCIFCBO().TujuanBukaRekeningLainnya() == null ||
                            viewModel.CIFTransactionModel().RetailCIFCBO().TujuanBukaRekeningLainnya() == undefined ||
                            viewModel.CIFTransactionModel().RetailCIFCBO().TujuanBukaRekeningLainnya() == '') {
                            ShowNotification("Attention", "Please fill field 'Tujuan Pembukaan Rekening Lainnya'", 'gritter-warning', true);
                            return false;
                        }
                    }
                }
                if (viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsFundSourceMaintenance() == true) {
                    if (viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsSumberDanaLainnya() == true) {
                        if (viewModel.CIFTransactionModel().RetailCIFCBO().SumberDanaLainnya() == null ||
                            viewModel.CIFTransactionModel().RetailCIFCBO().SumberDanaLainnya() == undefined ||
                            viewModel.CIFTransactionModel().RetailCIFCBO().SumberDanaLainnya() == '') {
                            ShowNotification("Attention", "Please fill field 'Sumber Dana Lainnya'", 'gritter-warning', true);
                            return false;
                        }
                    }
                }
                if (viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsJobMaintenance() == true) {
                    if (viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPekerjaanProfesional() == true) {
                        if (viewModel.CIFTransactionModel().RetailCIFCBO().PekerjaanProfesional() == null ||
                            viewModel.CIFTransactionModel().RetailCIFCBO().PekerjaanProfesional() == undefined ||
                            viewModel.CIFTransactionModel().RetailCIFCBO().PekerjaanProfesional() == '') {
                            ShowNotification("Attention", "Please fill field 'Pekerjaan Profesional'", 'gritter-warning', true);
                            return false;
                        }
                    }
                }
                if (viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsJobMaintenance() == true) {
                    if (viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPekerjaanLainnya() == true) {
                        if (viewModel.CIFTransactionModel().RetailCIFCBO().PekerjaanLainnya() == null ||
                            viewModel.CIFTransactionModel().RetailCIFCBO().PekerjaanLainnya() == undefined ||
                            viewModel.CIFTransactionModel().RetailCIFCBO().PekerjaanLainnya() == '') {
                            ShowNotification("Attention", "Please fill field 'Pekerjaan Lainnya'", 'gritter-warning', true);
                            return false;
                        }
                    }
                }
                break;
                //added by dani end
            case ConsCIFMaintenanceType.LOIPOIPOACons:
                if (!ko.toJS(viewModel.CIFTransactionModel().IsLOI()) &&
                   !ko.toJS(viewModel.CIFTransactionModel().IsPOI()) &&
                   !ko.toJS(viewModel.CIFTransactionModel().IsPOA())) {
                    ShowNotification("Attention", "Please select LOI or POI or POA option to Submit Transaction", 'gritter-warning', true);
                    return false;
                }
                break;
            case ConsCIFMaintenanceType.LinkFFDCons:
                if (Number(ko.toJS(viewModel.TempFFDAccounts().length)) <= 0) {
                    ShowNotification("Attention", "Please input at least one account number", 'gritter-warning', true);
                    return false;
                }
                break;
            case ConsCIFMaintenanceType.LienUnlien:
                isAttachMandatory = true;
                break;
            default:
                break;
        }
        if (isAttachMandatory == true) {
            if (viewModel.DocumentsCIF() == null || viewModel.DocumentsCIF().length == 0) {
                if (viewModel.CIFTransactionModel().MaintenanceType().ID != ConsCIFMaintenanceType.AtmCardCons) {
                    ShowNotification("Attention", "You need to upload attachment to continue transaction.", 'gritter-warning', true);
                    return false;
                }
            }
        }
        if (isAddAcc == true) {
            if (viewModel.TempAddAccounts() == null || viewModel.TempAddAccounts().length == 0) {
                ShowNotification("Attention", "You need to insert account number", 'gritter-warning', true);
                return false;
            }
        }
        return true;
    }

    function CheckCutOff(ProductID) {
        var cutOFFParameter = '';
        switch (viewModel.ProductID()) {
            case ConsProductID.UTOnshoreproductIDCons:
                //pakai library jquery
                if ($.inArray(viewModel.Selected().Transaction_Type(), ConsTransactionType.utSubcription) > -1) {
                    cutOFFParameter = ConsPARSYS.utCutOffSubcription;
                }
                if ($.inArray(viewModel.Selected().Transaction_Type(), ConsTransactionType.utRedemption) > -1) {
                    cutOFFParameter = ConsPARSYS.utCutOffRedemption;
                }
                if ($.inArray(viewModel.Selected().Transaction_Type(), ConsTransactionType.utSwitching) > -1) {
                    cutOFFParameter = ConsPARSYS.utCutOffSwitching;
                }
                break;
            case ConsProductID.UTOffshoreProductIDCons:
                if ($.inArray(viewModel.Selected().Transaction_Type(), ConsTransactionType.utSubcription) > -1) {
                    cutOFFParameter = ConsPARSYS.utCutOffSubcription;
                }
                if ($.inArray(viewModel.Selected().Transaction_Type(), ConsTransactionType.utRedemption) > -1) {
                    cutOFFParameter = ConsPARSYS.utCutOffRedemption;
                }
                if ($.inArray(viewModel.Selected().Transaction_Type(), ConsTransactionType.utSwitching) > -1) {
                    cutOFFParameter = ConsPARSYS.utCutOffSwitching;
                }
                break;
            case ConsProductID.UTCPFProductIDCons:
                if ($.inArray(viewModel.Selected().Transaction_Type(), ConsTransactionType.utSubcription) > -1) {
                    cutOFFParameter = ConsPARSYS.utCutOffSubcription;
                }
                if ($.inArray(viewModel.Selected().Transaction_Type(), ConsTransactionType.utRedemption) > -1) {
                    cutOFFParameter = ConsPARSYS.utCutOffRedemption;
                }
                if ($.inArray(viewModel.Selected().Transaction_Type(), ConsTransactionType.utSwitching) > -1) {
                    cutOFFParameter = ConsPARSYS.utCutOffSwitching;
                }
                break;
            case ConsProductID.SavingPlanProductIDCons:
                cutOFFParameter = ConsPARSYS.utCuttOffSP;
                break;
            case ConsProductID.IDInvestmentProductIDCons:
                cutOFFParameter = ConsPARSYS.utCuttOffIdInvestment;
                break;
            case ConsProductID.FDProductIDCons:
                cutOFFParameter = ConsPARSYS.fdCuttOff;
                break;
            case ConsProductID.CIFProductIDCons:
                cutOFFParameter = ConsPARSYS.cifCuttof;
                break;
            default:
                return;
        }

        var options = {
            url: api.server + api.url.parametersystem + "/IsCutOff/" + cutOFFParameter,
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessCheckCutOff, OnError, OnAlways);
    }

    function OnSuccessCheckCutOff(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            if (data != null) {
                switch (viewModel.ProductID()) {
                    case ConsProductID.UTOnshoreproductIDCons:
                    case ConsProductID.UTOffshoreProductIDCons:
                    case ConsProductID.UTCPFProductIDCons:
                    case ConsProductID.SavingPlanProductIDCons:
                    case ConsProductID.IDInvestmentProductIDCons:
                        viewModel.IsCuttOff(data.IsCutOff);
                        viewModel.strCuttOffTime(data.CuttofValue);
                        if (viewModel.IsDraftForm() == true) {
                            if (viewModel.IsCuttOff() == true) {
                                var isAttachedEmail = CheckEmailApp();
                                if (isAttachedEmail == true) {
                                    SaveUT();
                                    viewModel.IsCuttOff(false);
                                }
                                else {
                                    ShowNotification("Attention", "You need to attach email approval to continue cut off transaction.", 'gritter-warning', true);
                                    return;
                                }
                            }
                            else {
                                SaveUT();
                                viewModel.IsCuttOff(false);
                            }
                        }
                        else {
                            if (viewModel.IsCuttOff() == true) {
                                if (self.ProductID() == ConsProductID.IDInvestmentProductIDCons) {
                                    var SelectedFunc = viewModel.Selected().FunctionType();
                                    if (SelectedFunc != ConsUTPar.funcClose) {
                                        if (viewModel.Selected().AccountType() == null) {
                                            ShowNotification("Attention", "Please input Account Type", 'gritter-warning', true);
                                            self.IsEditable(true);
                                            return false;
                                        }
                                        if (viewModel.Selected().FunctionType() == null) {
                                            ShowNotification("Attention", "Please input Function Type", 'gritter-warning', true);
                                            self.IsEditable(true);
                                            return false;
                                        }
                                        if (viewModel.Selected().FNACore() == null) {
                                            ShowNotification("Attention", "Please input FNA Core", 'gritter-warning', true);
                                            self.IsEditable(true);
                                            return false;
                                        }
                                    }
                                }
                                self.TransactionUTModel().IsDraft(true);
                                self.TransactionUTModel().IsBringupTask(true);
                                self.IsEditable(false);
                                SaveDraftUT();//cutt off going to draft(temporary)                  
                                viewModel.IsCuttOff(true);
                            }
                            else {
                                SaveUT();
                                viewModel.IsCuttOff(false);
                            }
                        }
                        break;
                    case ConsProductID.FDProductIDCons:
                        viewModel.IsCuttOff(data.IsCutOff);
                        viewModel.strCuttOffTime(data.CuttofValue);
                        if (viewModel.IsDraftForm() == true) {
                            if (viewModel.IsCuttOff() == true) {
                                var isAttachedEmail = CheckEmailApp();
                                if (isAttachedEmail == true) {
                                    SaveFD(false);
                                    viewModel.IsCuttOff(false);
                                }
                                else {
                                    ShowNotification("Attention", "You need to attach email approval to continue cut off transaction.", 'gritter-warning', true);
                                    return;
                                }
                            }
                            else {
                                SaveFD(false);
                                viewModel.IsCuttOff(false);
                            }
                        }
                        else {
                            if (viewModel.FDTransaction() == null || viewModel.FDTransaction().length == 0) {
                                ShowNotification("Attention", "You need to insert transaction to continue transaction.", 'gritter-warning', true);
                                return false;
                            }
                            if (viewModel.IsCuttOff() == true) {
                                SaveFD(true);//cutt off going to draft(temporary)                  
                                viewModel.IsCuttOff(true);
                            }
                            else {
                                SaveFD(false);
                                viewModel.IsCuttOff(false);
                            }
                        }
                        break;
                    case ConsProductID.CIFProductIDCons:
                        viewModel.IsCuttOff(data.IsCutOff);
                        viewModel.strCuttOffTime(data.CuttofValue);
                        if (viewModel.CIFTransactionModel().MaintenanceType() != null) {
                            if (viewModel.CIFTransactionModel().MaintenanceType().Name === "Lien/Un-lien Account") {
                                if (viewModel.CIFTransactionModel().AccountNumberLienUnlien().length > 0) {
                                    var anySelected = false;
                                    for (var z = 0; z < viewModel.CIFTransactionModel().AccountNumberLienUnlien().length; z++) {
                                        var dat = viewModel.CIFTransactionModel().AccountNumberLienUnlien()[z];
                                        if (dat().IsSelected() === true) {
                                            anySelected = true;
                                        }
                                        if (dat().IsSelected() === true) {
                                            if (dat().LienUnlien() === null || dat().LienUnlien() === "" || dat().LienUnlien() === undefined) {
                                                ShowNotification("Attention", "Please fill Lien/Unlien on Account Number table when you select data.", 'gritter-warning', true);
                                                return;
                                            }
                                        }
                                    }
                                    if (!anySelected) {
                                        ShowNotification("Attention", "Please select data on Account Number Table.", 'gritter-warning', true);
                                        return;
                                    }
                                }
                            }
                        }
                        if (viewModel.IsDraftForm() == true) {
                            if (viewModel.IsCuttOff() == true) {
                                var isAttachedEmail = CheckEmailAppCIF();
                                if (isAttachedEmail == true) {
                                    SaveCIF();
                                    viewModel.IsCuttOff(false);
                                }
                                else {
                                    ShowNotification("Attention", "You need to attach email approval to continue cut off transaction.", 'gritter-warning', true);
                                    return;
                                }
                            }
                            else {
                                SaveCIF();
                                viewModel.IsCuttOff(false);
                            }
                        }
                        else {
                            if (viewModel.IsCuttOff() == true) {
                                self.CIFTransactionModel().IsDraft(true);
                                self.CIFTransactionModel().IsBringupTask(true);
                                self.IsEditable(false);
                                SaveDraftCIF();//cutt off going to draft(temporary)                  
                                viewModel.IsCuttOff(true);
                            }
                            else {
                                SaveCIF();
                                viewModel.IsCuttOff(false);
                            }
                        }
                        break;
                    default:
                        return;
                }
            }
            else
                ShowNotification('Cut Off Empty!!!', jqXHR.responseText, 'gritter-error', true);

        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    var StatusTopUrgent;
    function ThickTopUrgent() {
        //if (viewModel.ProductID() != ConsProductID.SKNProductIDCons) {
        if (viewModel.TransactionModel().IsNormal() || viewModel.TransactionModel().IsTopUrgentChain() || viewModel.TransactionModel().IsTopUrgent() == true) {
            StatusTopUrgent = true;
        } else {
            StatusTopUrgent = false;
        }
        //} else {
        //    StatusTopUrgent = true;
        //}
        //include validation CR LLD Document
        if (viewModel.TransactionModel().Product().Code == "OT") {
            if (viewModel.TransactionModel().LLDDocument() == null && viewModel.TransactionModel().LLDUnderlyingAmount() == "") {
                $('#llddoc').data({ ruleRequired: true });
                viewModel.IsLLDUndelyingDocument(true);
                $('#lldunderlying').data({ ruleRequired: true });
                viewModel.IsLLDUndelyingAmount(true);
            }
        }
    }

    var IsCheckLimitPass;

    function CheckLimitbyProduct() {
        if (self.ProductID() == ConsProductID.RTGSProductIDCons || self.ProductID() == ConsProductID.SKNProductIDCons) {
            var TotalAmount;
            var Rate = viewModel.TransactionModel().Rate();
            var LimitByProduct = self.LimitProduct()[0];
            if (viewModel.TransactionModel().Currency().ID == Currency.IDRselected) {
                TotalAmount = viewModel.TransactionModel().Amount();
            } else {
                TotalAmount = Rate * viewModel.TransactionModel().Amount();
            }
            if (LimitByProduct.MinAmount <= TotalAmount == true) {
                if (TotalAmount <= LimitByProduct.MaxAmount || LimitByProduct.Unlimited == true) {
                    //lolos
                    IsCheckLimitPass = 1;
                } else if (TotalAmount > LimitByProduct.MaxAmount == true) {
                    IsCheckLimitPass = 0;
                }
            } else {
                IsCheckLimitPass = 0;
            }
        }
    }

    self.Submit = function () {
        switch (self.ProductID()) {
            case ConsProductID.RTGSProductIDCons:
            case ConsProductID.SKNProductIDCons: // SKN Singgle Active
                if (viewModel.TransactionModel().IsIPETMO() == true && self.ProductID() == ConsProductID.RTGSProductIDCons) {
                    if (viewModel.AmountTZ() != 0) {
                        if (parseFloat(viewModel.TransactionModel().Amount()) > viewModel.AmountTZ()) {
                            ShowNotification("Form Warning", "Transaction Amount should not be greater then Amount DF", 'gritter-warning', false);
                            return false;
                        }
                    }
                    var IsFxTransaction = (viewModel.TransactionModel().Currency().Code != 'IDR' && viewModel.TransactionModel().DebitCurrency().Code == 'IDR');
                    var IsFxTransactionToIDR = (viewModel.TransactionModel().Currency().Code == 'IDR' && viewModel.TransactionModel().DebitCurrency().Code != 'IDR');
                    viewModel.IsFxTransaction(IsFxTransaction);
                    viewModel.IsFxTransactionToIDR(IsFxTransactionToIDR);
                    var UtilizeUnderlying = viewModel.TempSelectedUnderlying().length > 0;

                    //var totalUnderlying = GetTotalUnderlyingRounding(data.RoundingValue);
                    var totalUnderlying = GetTotalUnderlyingRounding(self.Rounding());


                    var document = ko.utils.arrayFilter(viewModel.Documents(), function (item) {
                        return item.Purpose.ID == 1; // Instruction Document
                    });

                    if (document.length == 0) {
                        var msg = 'Please upload Instruction Document.';
                        ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
                        return false;
                    }

                    if (viewModel.IsFxTransaction() == true || viewModel.IsFxTransactionToIDR() == true) {
                        if (parseFloat(viewModel.TransactionModel().AmountUSD()) >= 5000000 && UtilizeUnderlying == false) {
                            ShowNotification("Form Underlying Warning", "Please select utilize underlying data.", 'gritter-warning', false);
                            return false;
                        }
                        if (totalUnderlying < parseFloat(viewModel.TransactionModel().AmountUSD()) && parseFloat(viewModel.TransactionModel().AmountUSD()) >= 5000000) {
                            ShowNotification("Form Underlying Warning", "Total Underlying Amount must be equal greater than Transaction Amount", 'gritter-warning', false);
                            return false;
                        }

                        if (parseFloat(viewModel.TransactionModel().AmountUSD()) >= 5000000) {

                            var documentUnderlying = ko.utils.arrayFilter(viewModel.Documents(), function (item) {
                                return item.Purpose.ID == 2; // Underlying Document
                            });
                            var documentStatementLetter = ko.utils.arrayFilter(viewModel.Documents(), function (item) {
                                return item.Purpose.Name == 'Statement Letter'; // Statement Letter Document
                            });

                            if (documentUnderlying.length == 0) {
                                var msg = 'Please upload Underlying Document.';
                                ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
                                return false;
                            }
                            if (documentStatementLetter.length == 0) {
                                var msg = 'Please upload Statement Letter Document.';
                                ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
                                return false;
                            }
                        }

                    }

                }
                //henggar 04102017 tbo payment validasi
                if (viewModel.TBOData().length > 0) {
                    for (var i = 0; viewModel.TBOData().length > i; i++) {
                        if (viewModel.TBOData()[i].Status == null || viewModel.TBOData()[i].Status == "Add") {
                            if (new Date(viewModel.LocalDate(Date.now(), true)) >= new Date(viewModel.LocalDate(viewModel.TBOData()[i].ExptDate, true))) {
                                if (viewModel.Documents().length > 0) {
                                    for (var a = 0; viewModel.Documents().length > a; a++) {
                                        if (viewModel.Documents()[a].Type.ID != documentType.emailApproval) {//hardcode
                                            ShowNotification("Task Validation Warning", String.format("This Transaction Cannot Submitted, Please Attach document type Email Approval to submit transaction"), "gritter-warning", false);
                                            return;
                                        }
                                    }
                                } else {
                                    ShowNotification("Task Validation Warning", String.format("This Transaction Cannot Submitted, Please Attach document type Email Approval to submit transaction"), "gritter-warning", false);
                                    return;
                                }
                            }
                        }
                    }
                }
                //end 04102017 tbo payment validasi
                //aridya add 20161205 binding ulang branchid                
                viewModel.TransactionModel().BranchID = viewModel.selectedBranchID();
                //end add
                if (viewModel.TrxRate() == "") {
                    viewModel.TransactionModel().TrxRate(0);
                } else {
                    viewModel.TransactionModel().TrxRate(viewModel.TrxRate());
                }
                viewModel.TransactionModel().CreateDate(viewModel.TouchTimeStartDate());
                if (viewModel.TransactionModel().ModePayment() != "BCP2") {
                    //add henggar enhandsment 070417
                    if (viewModel.TransactionModel().IsIPETMO() != true) {
                        if ($('#bank-branch-code').val() == "") {
                            ShowNotification("Form Validation Warning", "Can not Submit Transaction, Branch code can not be blank", 'gritter-warning', false);
                            return;
                        }
                    }
                    //end
                    if (viewModel.IsPPUHeadMaker() === true) {
                        if (viewModel.TransactionModel().BeneName() === undefined || viewModel.TransactionModel().BeneName() === null || viewModel.TransactionModel().BeneName() === '') {
                            ShowNotification("Form Validation Warning", "Can not Submit Transaction, Bene name can not be blank", 'gritter-warning', false);
                            return;
                        }
                    }

                    var valTzNumber = viewModel.TransactionModel().TZNumber();
                    if (valTzNumber != null && valTzNumber != undefined) {
                        var count = 0;

                        for (x = 0; x < valTzNumber.length; x++) {
                            if (valTzNumber.charAt(x) == "-") {
                                count++;
                            }
                        }

                        if (count > 1) {
                            ShowNotification("Attention", "Please insert Tz Number contain one '-' only, (ex: 'df-0001').", 'gritter-warning', true);
                            return;
                        }
                        if (count < 1) {
                            ShowNotification("Attention", "Please insert Tz Number contain '-' symbol, (ex: 'df-0001').", 'gritter-warning', true);
                            return;
                        }
                    }
                    ThickTopUrgent();
                    CheckLimitbyProduct();
                    viewModel.IsEditable(false);
                    if (viewModel.Selected().Currency() == Currency.IDRselected) {
                        if (IsCheckLimitPass == 1) {
                            $.ajax({
                                type: "GET",
                                url: api.server + api.url.paymentmode,
                                success: function (data, textStatus, jqXHR) {
                                    if (data == viewModel.TransactionModel().ModePayment()) {
                                        if (StatusTopUrgent == 1) {
                                            SavePayment();
                                        } else {
                                            viewModel.IsEditable(true);
                                            ShowNotification("Form Validation Warning", "Can not Submit Transaction, Please select choose one of urgency toggle", 'gritter-warning', false);
                                        }
                                    } else {
                                        viewModel.IsEditable(true);
                                        ShowNotification("Form Validation Warning", "Can not Submit Transaction, Payment Mode is Change", 'gritter-warning', false);
                                    }
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    error(jqXHR, textStatus, errorThrown);
                                }
                            });
                        } else {
                            viewModel.IsEditable(true);
                            var text = "<h4><b style='color:#FF0000;'>The transaction can not proceed !!!</b></h4><div><h5><b>The Transaction amount less than or Exceed limit product</b></h5></div>";
                            bootbox.alert(text, function (result) {

                            });
                        }
                    } else {
                        viewModel.IsEditable(true);
                        var text = "<h4><b style='color:#FF0000;'>Can not Submit Transaction !!!</b></h4><div><h5><b>Transaction currency should IDR</b></h5></div>";
                        bootbox.alert(text, function (result) {
                        });
                    }
                } else {
                    $.ajax({
                        type: "GET",
                        url: api.server + api.url.paymentmode,
                        success: function (data, textStatus, jqXHR) {
                            if (data == viewModel.TransactionModel().ModePayment()) {
                                SavePayment();
                            } else {
                                ShowNotification("Form Validation Warning", "Can not Submit Transaction, Payment Mode is Change", 'gritter-warning', false);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            error(jqXHR, textStatus, errorThrown);
                        }
                    });
                }
                break;
                //case ConsProductID.SKNProductIDCons: // SKN Singgle Active
                //     viewModel.TransactionModel().CreateDate(viewModel.TouchTimeStartDate());
                //     SavePayment();
                //break;
            case ConsProductID.OTTProductIDCons:
            case ConsProductID.OverbookingProductIDCons:
                if (viewModel.TransactionModel().IsIPETMO() == true) {
                    if (viewModel.AmountTZ() != 0) {
                        if (parseFloat(viewModel.TransactionModel().Amount()) > viewModel.AmountTZ()) {
                            ShowNotification("Form Warning", "Transaction Amount should not be greater then Amount DF", 'gritter-warning', false);
                            return false;
                        }
                    }
                    var IsFxTransaction = (viewModel.TransactionModel().Currency().Code != 'IDR' && viewModel.TransactionModel().DebitCurrency().Code == 'IDR');
                    var IsFxTransactionToIDR = (viewModel.TransactionModel().Currency().Code == 'IDR' && viewModel.TransactionModel().DebitCurrency().Code != 'IDR');
                    viewModel.IsFxTransaction(IsFxTransaction);
                    viewModel.IsFxTransactionToIDR(IsFxTransactionToIDR);
                    var UtilizeUnderlying = viewModel.TempSelectedUnderlying().length > 0;

                    //var totalUnderlying = GetTotalUnderlyingRounding(data.RoundingValue);
                    var totalUnderlying = GetTotalUnderlyingRounding(self.Rounding());


                    var document = ko.utils.arrayFilter(viewModel.Documents(), function (item) {
                        return item.Purpose.ID == 1; // Instruction Document
                    });

                    if (document.length == 0) {
                        var msg = 'Please upload Instruction Document.';
                        ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
                        return false;
                    }

                    if (viewModel.IsFxTransaction() == true || viewModel.IsFxTransactionToIDR() == true) {
                        if (UtilizeUnderlying == false) {
                            ShowNotification("Form Underlying Warning", "Please select utilize underlying data.", 'gritter-warning', false);
                            return false;
                        }
                        if (totalUnderlying < parseFloat(viewModel.TransactionModel().AmountUSD())) {
                            ShowNotification("Form Underlying Warning", "Total Underlying Amount must be equal greater than Transaction Amount", 'gritter-warning', false);
                            return false;
                        }

                        //if (parseFloat(viewModel.TransactionModel().AmountUSD()) >= 5000000) {

                            var documentUnderlying = ko.utils.arrayFilter(viewModel.Documents(), function (item) {
                                return item.Purpose.ID == 2; // Underlying Document
                            });
                            var documentStatementLetter = ko.utils.arrayFilter(viewModel.Documents(), function (item) {
                                return item.Purpose.Name == 'Statement Letter'; // Statement Letter Document
                            });

                            if (documentUnderlying.length == 0) {
                                var msg = 'Please upload Underlying Document.';
                                ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
                                return false;
                            }
                            if (documentStatementLetter.length == 0) {
                                var msg = 'Please upload Statement Letter Document.';
                                ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
                                return false;
                            }
                        //}

                    }

                }
                //henggar 04102017 tbo payment validasi
                if (viewModel.TBOData().length > 0) {
                    for (var i = 0; viewModel.TBOData().length > i; i++) {
                        if (viewModel.TBOData()[i].Status == null || viewModel.TBOData()[i].Status == "Add") {
                            if (new Date(viewModel.LocalDate(Date.now(), true)) >= new Date(viewModel.LocalDate(viewModel.TBOData()[i].ExptDate, true))) {
                                if (viewModel.Documents().length > 0) {
                                    for (var a = 0; viewModel.Documents().length > a; a++) {
                                        if (viewModel.Documents()[a].Type.ID != documentType.emailApproval) {//hardcode
                                            ShowNotification("Task Validation Warning", String.format("This Transaction Cannot Submitted, Please Attach document type Email Approval to submit transaction"), "gritter-warning", false);
                                            return;
                                        }
                                    }
                                } else {
                                    ShowNotification("Task Validation Warning", String.format("This Transaction Cannot Submitted, Please Attach document type Email Approval to submit transaction"), "gritter-warning", false);
                                    return;
                                }
                            }
                        }
                    }
                }
                //end 04102017 tbo payment validasi


                //aridya add 20161205 binding ulang branchid
                viewModel.TransactionModel().BranchID = viewModel.selectedBranchID();
                //end add
                if (viewModel.TrxRate() == "") {
                    viewModel.TransactionModel().TrxRate(0);
                } else {
                    viewModel.TransactionModel().TrxRate(viewModel.TrxRate());
                }
                viewModel.TransactionModel().CreateDate(viewModel.TouchTimeStartDate());
                if (viewModel.TransactionModel().ModePayment() != "BCP2") {
                    var valTzNumber = viewModel.TransactionModel().TZNumber();
                    if (valTzNumber != null && valTzNumber != undefined) {
                        var count = 0;

                        for (x = 0; x < valTzNumber.length; x++) {
                            if (valTzNumber.charAt(x) == "-") {
                                count++;
                            }
                        }

                        if (count > 1) {
                            ShowNotification("Attention", "Please insert Tz Number contain one '-' only, (ex: 'df-0001').", 'gritter-warning', true);
                            return;
                        }
                        if (count < 1) {
                            ShowNotification("Attention", "Please insert Tz Number contain '-' symbol, (ex: 'df-0001').", 'gritter-warning', true);
                            return;
                        }
                    }
                    ThickTopUrgent();
                    $.ajax({
                        type: "GET",
                        url: api.server + api.url.paymentmode,
                        success: function (data, textStatus, jqXHR) {
                            if (data == viewModel.TransactionModel().ModePayment()) {
                                if (viewModel.Selected().Product() == ConsProductID.OTTProductIDCons && viewModel.Selected().Currency() == Currency.IDRselected) {
                                    var text = "<h4><b style='color:#FF0000;'>Can not Submit Transaction !!!</b></h4><div><h5><b>Transaction currency should other than IDR</b></h5></div>";
                                    bootbox.alert(text, function (result) {
                                    });
                                    return;
                                }
                                if (viewModel.TransactionModel().ModePayment() != "BCP2") {
                                    if (viewModel.ProductID() == ConsProductID.OTTProductIDCons) {
                                        if (viewModel.TransactionModel().Currency().ID != Const_AmountLLD.NonIDRSelected) {
                                            //perbaikan henggar 20161210 OTT submit draft
                                            vLLDAmount = (viewModel.TransactionModel().LLDUnderlyingAmount() + "").replace(',', '');
                                            viewModel.TransactionModel().LLDUnderlyingAmount(Number(viewModel.TransactionModel().LLDUnderlyingAmount()));
                                            //end henggar
                                            vLLDRounding = viewModel.RoundingValidation();
                                            if (parseFloat(vLLDAmount) < parseFloat(vLLDRounding)) { //change "mengikuti konsep di rounding fx transaksi".
                                                ShowNotification("Form Validation Warning", "Can not Submit Transaction, Total LLD Underlying Amount must be equal greater than transaction amount", 'gritter-warning', false);
                                                return;
                                            }
                                        }
                                    }
                                }
                                if (StatusTopUrgent == 1) {
                                    SavePayment();
                                } else {
                                    ShowNotification("Form Validation Warning", "Can not Submit Transaction, Please select choose one of urgency toggle", 'gritter-warning', false);
                                }
                            } else {
                                ShowNotification("Form Validation Warning", "Can not Submit Transaction, Payment Mode is Change", 'gritter-warning', false);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            error(jqXHR, textStatus, errorThrown);
                        }
                    });
                } else {
                    $.ajax({
                        type: "GET",
                        url: api.server + api.url.paymentmode,
                        success: function (data, textStatus, jqXHR) {
                            if (data == viewModel.TransactionModel().ModePayment()) {
                                //if (StatusTopUrgent == 1) {
                                SavePayment();
                                //} else {
                                //    ShowNotification("Form Validation Warning", "Can not Submit Transaction, Please select choose one of urgency toggle", 'gritter-warning', false);
                                // }
                            } else {
                                ShowNotification("Form Validation Warning", "Can not Submit Transaction, Payment Mode is Change", 'gritter-warning', false);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            error(jqXHR, textStatus, errorThrown);
                        }
                    });
                }
                break;
            case ConsProductID.TMOProductIDCons:
                viewModel.TransactionTMOModel().CreateDate(viewModel.TouchTimeStartDate());
                SaveTMO();
                break;
            case ConsProductID.UTOnshoreproductIDCons:
            case ConsProductID.UTOffshoreProductIDCons:
            case ConsProductID.UTCPFProductIDCons:
            case ConsProductID.SavingPlanProductIDCons:
            case ConsProductID.IDInvestmentProductIDCons:
                viewModel.TransactionUTModel().Documents([]);
                viewModel.TransactionUTModel().CreateDate(viewModel.TouchTimeStartDate());
                if (IsvalidUT() == true) {
                    CheckCutOff(self.ProductID());
                }
                break;
            case ConsProductID.FDProductIDCons:
                CheckCutOff(self.ProductID());
                break;
            case ConsProductID.LoanDisbursmentProductIDCons:
            case ConsProductID.LoanRolloverProductIDCons:
            case ConsProductID.LoanIMProductIDCons:
            case ConsProductID.LoanSettlementProductIDCons:
                viewModel.TransactionLoanModel().CreateDate(viewModel.TouchTimeStartDate());
                if (IsvalidLoan() == true) {
                    SaveLoan();
                }
                break;
            case ConsProductID.CollateralProductIDCons:
                break;
            case ConsProductID.CIFProductIDCons:
                if (viewModel.DocumentsCIF().length < 1) {
                    if (viewModel.CIFTransactionModel().MaintenanceType().ID == ConsCIFMaintenanceType.AtmCardCons || viewModel.CIFTransactionModel().MaintenanceType().ID == ConsCIFMaintenanceType.UpdateFXTierCons) {
                        ShowNotification("Attention", "Please attach document", 'gritter-warning', true);
                        self.IsEditable(true);
                        return false;
                    }
                }
                if (viewModel.CIFTransactionModel().MaintenanceType().ID == ConsCIFMaintenanceType.LienUnlien) {
                    if (viewModel.CIFTransactionModel().IsNewCustomer() === true) {
                        ShowNotification("Attention", "New Customer can't submit transaction!", 'gritter-warning', false);
                        return false;
                    }
                }
                if (viewModel.CIFTransactionModel().MaintenanceType().ID == ConsCIFMaintenanceType.DispatchMode) {
                    if (viewModel.DocumentsCIF().length == 0) {
                        ShowNotification("Form Validation Warning", "Can not Submit Transaction, Please attach document", 'gritter-warning', false);
                        return false;
                    }
                    if (viewModel.Selected().DispatchModeType() == ConsDispatchModeType['-']) {
                        if (viewModel.CIFTransactionModel().RetailCIFCBO().DispatchModeOther() == undefined || viewModel.CIFTransactionModel().RetailCIFCBO().DispatchModeOther() == "") {
                            ShowNotification("Attention", "Dispatch Mode Other Can't empty!", 'gritter-warning', false);
                            return false;
                        }
                    }
                }

                //Valdase PhoneNUmber
                if (viewModel.CIFTransactionModel().RetailCIFCBO().IsCorrespondenseToEmail() || viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPhoneFaxEmailMaintenance()) {
                    var lengSelular = viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers().length;
                    var lengHome = viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers().length;
                    var lengtOffice = viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers().length;
                    function IfA(a) {
                        var result = false;
                        for (var key in a) {
                            var obj = a[key];
                            var b;
                            if (obj.a == 2) {
                                b = (obj.a != '' && obj.b != '' && obj.c != '');
                            } else {
                                b = (obj.a != '' && obj.b != '');
                            }

                            result = result || b;
                        }

                        return result;
                    }
                    var a1 = [];
                    for (var i = 0; i < lengSelular; i++) {
                        if ($("#handphone" + i).val() == '2') {
                            a1.push({ a: $('#handphone' + i).val(), b: $('#PhoneNumber' + i).val(), c: $('#UpdatePhoneNumber' + i).val() });
                        } else {
                            a1.push({ a: $("#handphone" + i).val(), b: $('#PhoneNumber' + i).val() });
                        }
                    }
                    var a2 = [];
                    for (var i = 0; i < lengSelular; i++) {
                        if ($("#Office" + i).val() == '2') {
                            a2.push({ a: $('#Office' + i).val(), b: $('#OfficePhoneNumber' + i).val(), c: $('#UpdateOfficePhoneNumber' + i).val() });
                        } else {
                            a2.push({ a: $("#Office" + i).val(), b: $('#OfficePhoneNumber' + i).val() });
                        }
                    }
                    var a3 = [];
                    for (var i = 0; i < lengSelular; i++) {

                        if ($("#Home" + i).val() == '2') {
                            a3.push({ a: $('#Home' + i).val(), b: $('#HomePhoneNumber' + i).val(), c: $('#UpdateHomePhoneNumber' + i).val() });
                        } else {
                            a3.push({ a: $("#Home" + i).val(), b: $('#HomePhoneNumber' + i).val() });
                        }
                    }

                    var c = ($('#alamat-email').val() != '') || IfA(a1) || IfA(a2) || IfA(a3);

                    if (!c) {
                        ShowNotification("Attention", "Please fill 'Telepone Selular' or 'Rumah' or 'Kantor' or 'Faksimili' or 'Alamat Email'", 'gritter-warning', true);
                        return false;
                    }

                }

                //henggar 190417
                if (self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsReligion()) {
                    viewModel.CIFTransactionModel().RetailCIFCBO().Religion(viewModel.Selected().Religion());
                }

                if (self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsEducation()) {
                    viewModel.CIFTransactionModel().RetailCIFCBO().Education(viewModel.Selected().Education());
                }

                if (self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsSLIK()) {
                    viewModel.CIFTransactionModel().RetailCIFCBO().GrosIncomeccy(viewModel.Selected().GrossIncomeCCY());
                }

                if (self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsFatcaCRS()) {
                    viewModel.CIFTransactionModel().RetailCIFCBO().FatcaCountryCode(viewModel.Selected().BeneficiaryCountry());
                    viewModel.CIFTransactionModel().RetailCIFCBO().FatcaReviewStatus(viewModel.Selected().FATCAReviewStatus());
                    viewModel.CIFTransactionModel().RetailCIFCBO().FatcaCRSStatus(viewModel.Selected().FATCACRSStatus());
                    viewModel.CIFTransactionModel().RetailCIFCBO().FatcaTaxPayer(viewModel.Selected().TaxPayer());
                    viewModel.CIFTransactionModel().RetailCIFCBO().FatcaWithHolding(viewModel.Selected().WithholdingCertificationType())
                }

                //handle integer cant to be null
                if (self.CIFTransactionModel().RetailCIFCBO().DataUBOCif() == "") {
                    viewModel.CIFTransactionModel().RetailCIFCBO().DataUBOCif(0);
                    $('#dataUBOcif').val('');
                }

                if (self.CIFTransactionModel().RetailCIFCBO().GrosIncomeccy() == "") {
                    viewModel.CIFTransactionModel().RetailCIFCBO().GrosIncomeccy(0);
                }

                if (self.CIFTransactionModel().RetailCIFCBO().GrossCifSpouse() == "") {
                    viewModel.CIFTransactionModel().RetailCIFCBO().GrossCifSpouse(0);
                    $('#cifspouse').val('');
                }

                if (self.CIFTransactionModel().RetailCIFCBO().GrossIncomeAmount() == "") {
                    viewModel.CIFTransactionModel().RetailCIFCBO().GrossIncomeAmount(0);
                    $('#grossspouseccy').val('');
                }

                if (self.CIFTransactionModel().RetailCIFCBO().Education() == "") {
                    viewModel.CIFTransactionModel().RetailCIFCBO().Education(0);
                }

                if (self.CIFTransactionModel().RetailCIFCBO().Religion() == "") {
                    viewModel.CIFTransactionModel().RetailCIFCBO().Religion(0);
                }
                //end

                if (self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsIdentityTypeMaintenance() || self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsNationalityMaintenance()) {

                    if (self.IsIdentitas2() && self.IsIdentitas3() && self.IsIdentitas4()) {
                        if (self.Selected().IdentityTypeID() == self.Selected().IdentityTypeID2() || self.Selected().IdentityTypeID() == self.Selected().IdentityTypeID3() || self.Selected().IdentityTypeID() == self.Selected().IdentityTypeID4() ||
                        self.Selected().IdentityTypeID2() == self.Selected().IdentityTypeID3() || self.Selected().IdentityTypeID2() == self.Selected().IdentityTypeID4() ||
                        self.Selected().IdentityTypeID3() == self.Selected().IdentityTypeID4()) {
                            ShowNotification("Form Validation Warning", "Can not Submit Duplicate Indentity Type", 'gritter-warning', false);
                            return false;
                        }
                    }

                    else if (self.IsIdentitas2() && self.IsIdentitas3()) {
                        if (self.Selected().IdentityTypeID() == self.Selected().IdentityTypeID2() || self.Selected().IdentityTypeID() == self.Selected().IdentityTypeID3() ||
                            self.Selected().IdentityTypeID2() == self.Selected().IdentityTypeID3()) {
                            ShowNotification("Form Validation Warning", "Can not Submit Duplicate Indentity Type", 'gritter-warning', false);
                            return false;
                        }

                    }
                    else if (self.IsIdentitas2() && self.IsIdentitas4()) {
                        if (self.Selected().IdentityTypeID() == self.Selected().IdentityTypeID2() || self.Selected().IdentityTypeID() == self.Selected().IdentityTypeID4() ||
                            self.Selected().IdentityTypeID2() == self.Selected().IdentityTypeID4()) {
                            ShowNotification("Form Validation Warning", "Can not Submit Duplicate Indentity Type", 'gritter-warning', false);
                            return false;
                        }
                    }
                    else if (self.IsIdentitas3() && self.IsIdentitas4()) {
                        if (self.Selected().IdentityTypeID() == self.Selected().IdentityTypeID3() || self.Selected().IdentityTypeID() == self.Selected().IdentityTypeID4() ||
                            self.Selected().IdentityTypeID3() == self.Selected().IdentityTypeID4()) {
                            ShowNotification("Form Validation Warning", "Can not Submit Duplicate Indentity Type", 'gritter-warning', false);
                            return false;
                        }
                    }
                    else if (self.IsIdentitas2()) {
                        if (self.Selected().IdentityTypeID() == self.Selected().IdentityTypeID2()) {
                            ShowNotification("Form Validation Warning", "Can not Submit Duplicate Indentity Type", 'gritter-warning', false);
                            return false;
                        }

                    }
                    else if (self.IsIdentitas3()) {
                        if (self.Selected().IdentityTypeID() == self.Selected().IdentityTypeID3()) {
                            ShowNotification("Form Validation Warning", "Can not Submit Duplicate Indentity Type", 'gritter-warning', false);
                            return false;
                        }

                    }
                    else if (self.IsIdentitas4()) {
                        if (self.Selected().IdentityTypeID() == self.Selected().IdentityTypeID4()) {
                            ShowNotification("Form Validation Warning", "Can not Submit Duplicate Indentity Type", 'gritter-warning', false);
                            return false;
                        }

                    }

                }
                if (viewModel.CIFTransactionModel().RetailCIFCBO().EmailAddress() != null && viewModel.CIFTransactionModel().RetailCIFCBO().EmailAddress() != '') {
                    if (!ValidateEmail(viewModel.CIFTransactionModel().RetailCIFCBO().EmailAddress())) {
                        ShowNotification("Attention", "Invalid Email Address", 'gritter-warning', true);
                        return false;
                    }
                }
                viewModel.CIFTransactionModel().CreateDate(viewModel.TouchTimeStartDate());
                if (IsvalidCIF() == true) {
                    CheckCutOff(self.ProductID());
                }
                break;
                //add aridya 20161011 for SKN Bulk ~OFFLINE~
            case ConsProductID.SKNBulkProductIDCons:
                viewModel.TransactionModel().CreateDate(viewModel.TouchTimeStartDate());
                //aridya add 20161205 change logic to check for skn bulk document
                var SKNBulkDocsFound = false;
                for (var ii = 0; ii < viewModel.Documents().length; ii++) {
                    if (viewModel.Documents()[ii].Type.ID == ConsSKNBulkDocumentDefaultValue.DocumentTypeSKNBulk) {
                        SKNBulkDocsFound = true;
                        break;
                    }
                }
                if (SKNBulkDocsFound) {
                    ThickTopUrgent();
                    if (StatusTopUrgent == 1) {
                        SaveSKNBulk();
                    } else {
                        ShowNotification("Form Validation Warning", "Can not Submit Transaction, Please select choose one of urgency toggle", 'gritter-warning', false);
                    }
                } else {
                    ShowNotification("Form Validation Warning", "Can not Submit Transaction, Please attach the SKN Bulk document", 'gritter-warning', false);
                }
                break;
                //end add
            default:
                return;
        }
    };

    // User Validation process Leleana
    self.ValidationProcess = function () {
        // Ruddy 21.10.2014 : Pending task. Still can't validate user & password to sharepoint.
        // edited 24-01-2015

        var options = {
            url: "/_vti_bin/DBSUserValidation/UserValidationServices.svc/UserAD",
            data: ko.toJSON(self.UserValidation())
        };

        Helper.Sharepoint.Validate.User(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // hide Double Transaction dialog
                // console.log(ko.toJSON(data));
                if (data == "Succeded") {
                    $("#modal-double-transaction").modal('hide');
                    self.UserApproveDoubleTransaction(self.UserValidation().UserID()); //add aridya 20170208 history double trx approve
                    self.UserValidation().UserID("");
                    self.UserValidation().Password("");
                    self.UserValidation().Error("");
                    // start upload docs and save the transaction
                    UploadDocuments();
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, data, 'gritter-error', true);
                }

                //} else {
                // show error
                //self.UserValidation().Error(data.Message);
                //}
            }
        }, OnError, OnAlways);

        // remove this when pending task (user validation has been completed).
        //UploadDocuments(); // start upload docs and save the transaction
    };

    // Continue editing
    self.ContinueEditing = function () {
        // enable form input controls
        self.IsEditable(true);
        self.UserValidation().UserID("");
        self.UserValidation().Password("");
        self.UserValidation().Error("");
    };

    //--------------------------- set Underlying function & variable start
    self.IsUnderlyingMode = ko.observable(true);
    self.ID_u = ko.observable("");
    self.CIF_u = ko.observable(cifData);
    self.CustomerName_u = ko.observable(customerNameData);
    self.UnderlyingDocument_u = ko.observable(new UnderlyingDocumentModel2('', '', ''));
    self.ddlUnderlyingDocument_u = ko.observableArray([]);
    self.DocumentType_u = ko.observable(new DocumentTypeModel2('', ''));
    self.ddlDocumentType_u = ko.observableArray([]);
    self.Currency_u = ko.observable(new CurrencyModel2('', '', ''));
    self.ddlCurrency_u = ko.observableArray([]);
    self.StatementLetter_u = ko.observable(new StatementLetterModel('', ''));
    self.ddlStatementLetter_u = ko.observableArray([]);
    self.DynamicStatementLetter_u = ko.observableArray([]);
    self.AvailableAmount_u = ko.observable(0);
    self.Rate_u = ko.observable(0);
    self.AmountUSD_u = ko.observable(0.00);
    self.Amount_u = ko.observable(0);
    self.AttachmentNo_u = ko.observable("");
    self.SelectUnderlyingDocument_u = ko.observable("");
    self.IsDeclarationOfException_u = ko.observable(false);
    self.StartDate_u = ko.observable();
    self.EndDate_u = ko.observable();
    self.DateOfUnderlying_u = ko.observable();
    self.ExpiredDate_u = ko.observable();
    self.ReferenceNumber_u = ko.observable("");
    self.SupplierName_u = ko.observable("");
    self.InvoiceNumber_u = ko.observable("");
    self.IsStatementA = ko.observable(true);
    self.IsProforma_u = ko.observable(false);
    self.IsSelectedProforma = ko.observable(false);
    self.IsUtilize_u = ko.observable(false);
    self.IsEnable_u = ko.observable(false);
    self.IsJointAccount_u = ko.observable(false);
    self.AccountNumber_u = ko.observable("");
    // add bulkUnderlying // add 2015.03.09
    self.IsBulkUnderlying_u = ko.observable(false);
    self.IsSelectedBulk = ko.observable(false);

    //yes
    self.TransactionModel.BeneName = ko.computed(function () {
        /*var maxLength=$('textarea').attr('maxlength');
        return this.comment().length+ " of "+maxLength;*/
    }, this);

    self.Amounttmp_u = ko.computed({
        read: function () {
            var fromFormat = document.getElementById("Amount_u");
            if (fromFormat != null) {
                fromFormat = fromFormat.value.toString().replace(/,/g, '');
                //var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                if (self.StatementLetter_u().ID() == 1) {
                    self.Currency_u().ID(1);
                    self.Amount_u(formatNumber(DataModel.ThresholdBuy));
                    //self.Amount_u(TotalPPUModel.TreshHold);
                    var date = new Date();
                    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                    var day = lastDay.getDate();
                    var month = lastDay.getMonth() + 1;
                    var year = lastDay.getFullYear();
                    var fixLastDay = year + "/" + month + "/" + day;
                    //var fixLastDay = day + "-" + monthNames[month - 1] + "-" + year;
                    self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
                    SetDefaultValueStatementA();
                } else if (self.StatementLetter_u().ID() == 2 && self.DateOfUnderlying_u() != "" && self.IsNewDataUnderlying()) {
                    if (self.LocalDate(self.DateOfUnderlying_u()) != "") {
                        var date = new Date(self.DateOfUnderlying_u());
                        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                        var day = date.getDate();
                        var month = lastDay.getMonth() + 13;
                        var year = lastDay.getFullYear();
                        if (month > 12) {
                            month -= 12;
                            year += 1;
                        }
                        var fixLastDay = year + "/" + month + "/" + day;
                        if (!moment(fixLastDay, 'YYYY/MM/DD').isValid()) {
                            var LastDay_ = new Date(year, month, 0);
                            day = LastDay_.getDate();
                        }
                        //fixLastDay = day + "-" + monthNames[month - 1] + "-" + year;
                        self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
                    }
                }

                var e = document.getElementById("Amount_u").value;
                e = e.toString().replace(/,/g, '');

                var res = parseInt(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
                res = Math.round(res * 100) / 100;
                res = isNaN(res) ? 0 : res; //avoid NaN
                self.AmountUSD_u(parseFloat(res).toFixed(2));
                self.Amount_u(formatNumber(e));
            }
        },
        write: function (data) {
            var res = parseInt(fromFormat) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res).toFixed(2));
            self.Amount_u(formatNumber(fromFormat));
        }
    }, this);
    self.CaluclateRate_u = ko.computed({
        read: function () {
            var CurrencyID = self.Currency_u().ID();
            var AccountID = self.Selected().Account;
            if (CurrencyID != '' && CurrencyID != undefined) {
                GetRateIDRAmount(CurrencyID);
            } else {
                self.Rate_u(0);
            }
            if (AccountID == '' && AccountID != undefined) {
                alert('test');
            }
        },
        write: function () { }
    });

    self.Proformas = ko.observableArray([new SelectModel('', '')]);
    self.BulkUnderlyings = ko.observableArray([new SelectBulkModel('', '')]); // add 2015.03.09
    self.CodeUnderlying = ko.computed({
        read: function () {
            if (self.UnderlyingDocument_u().ID() != undefined && self.UnderlyingDocument_u().ID() != '') {
                var item = ko.utils.arrayFirst(self.ddlUnderlyingDocument_u(), function (x) {
                    return self.UnderlyingDocument_u().ID() == x.ID();
                }
                )
                if (item != null) {
                    console.log(item.Code() + "|" + item.ID());
                    return item.Code();
                }
                else {
                    return ''
                }
            } else {
                return ''
            }
        },
        write: function (value) {
            return value;
        }
    });

    self.OtherUnderlying_u = ko.observable('');
    self.tmpOtherUnderlying = ko.observable('');
    self.IsSelected_u = ko.observable(false);
    self.LastModifiedBy_u = ko.observable("");
    self.LastModifiedDate_u = ko.observable(new Date());

    // Attach Variable
    self.ID_a = ko.observable(0);
    self.FileName_a = ko.observable('');
    self.DocumentPurpose_a = ko.observable(new DocumentPurposeModel2('', '', ''));
    self.ddlDocumentPurpose_a = ko.observableArray([]);
    self.DocumentType_a = ko.observable(new DocumentTypeModel2('', ''));
    self.ddlDocumentType_a = ko.observableArray([]);
    self.DocumentRefNumber_a = ko.observable("");
    self.LastModifiedBy_a = ko.observable("");
    self.LastModifiedDate_a = ko.observable(new Date());
    self.CustomerUnderlyingMappings_a = ko.observableArray([new CustomerUnderlyingMappingModel('', '', '')]);
    self.Documents = ko.observableArray([]);
    self.DocumentPath_a = ko.observable("");

    // filter Underlying
    self.UnderlyingFilterCIF = ko.observable("");
    self.UnderlyingFilterParentID = ko.observable("");
    self.UnderlyingFilterIsSelected = ko.observable(false);
    self.UnderlyingFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingFilterDocumentType = ko.observable("");
    self.UnderlyingFilterCurrency = ko.observable("");
    self.UnderlyingFilterStatementLetter = ko.observable("");
    self.UnderlyingFilterAmount = ko.observable("");
    self.UnderlyingFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingFilterExpiredDate = ko.observable("");
    self.UnderlyingFilterReferenceNumber = ko.observable("");
    self.UnderlyingFilterSupplierName = ko.observable("");
    self.UnderlyingFilterIsProforma = ko.observable("");
    self.UnderlyingFilterIsAvailable = ko.observable("");
    self.UnderlyingFilterIsExpiredDate = ko.observable("");
    self.UnderlyingFilterIsSelectedBulk = ko.observable(false);
    self.UnderlyingFilterAvailableAmount = ko.observable("");
    self.UnderlyingFilterAttachmentNo = ko.observable("");
    self.UnderlyingFilterAccountNumber = ko.observable("");
    self.UnderlyingFilterShow = ko.observable(1);
    // filter Attach Underlying File
    self.AttachFilterFileName = ko.observable("");
    self.AttachFilterDocumentPurpose = ko.observable("");
    self.AttachFilterModifiedDate = ko.observable("");
    self.AttachFilterDocumentRefNumber = ko.observable("");
    self.AttachFilterAccountNumber = ko.observable("");
    // filter Underlying for Attach
    self.UnderlyingAttachFilterIsSelected = ko.observable("");
    self.UnderlyingAttachFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingAttachFilterDocumentType = ko.observable("");
    self.UnderlyingAttachFilterCurrency = ko.observable("");
    self.UnderlyingAttachFilterStatementLetter = ko.observable("");
    self.UnderlyingAttachFilterAmount = ko.observable("");
    self.UnderlyingAttachFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingAttachFilterExpiredDate = ko.observable("");
    self.UnderlyingAttachFilterReferenceNumber = ko.observable("");
    self.UnderlyingAttachFilterSupplierName = ko.observable("");
    self.UnderlyingAttachFilterAccountNumber = ko.observable("");

    //Tambah Agung
    self.TempSelectedInstruction = ko.observableArray([]);
    self.InstructionFilterIsSelected = ko.observable(false);
    self.InstructionFilterApplicationID = ko.observable("");
    self.InstructionFilterFileName = ko.observable("");
    self.InstructionFilterPurposeofDoc = ko.observable("");
    self.InstructionFilterDocumentType = ko.observable("");
    self.InstructionFilterLastModifiedDate = ko.observable("");
    self.InstructionFilterShow = ko.observable(1);
    //End

    // filter Underlying for Attach
    self.ProformaFilterIsSelected = ko.observable(false);
    self.ProformaFilterUnderlyingDocument = ko.observable("");
    self.ProformaFilterDocumentType = ko.observable("Proforma");
    self.ProformaFilterCurrency = ko.observable("");
    self.ProformaFilterStatementLetter = ko.observable("");
    self.ProformaFilterAmount = ko.observable("");
    self.ProformaFilterDateOfUnderlying = ko.observable("");
    self.ProformaFilterExpiredDate = ko.observable("");
    self.ProformaFilterReferenceNumber = ko.observable("");
    self.ProformaFilterSupplierName = ko.observable("");
    self.ProformaFilterIsProforma = ko.observable(false);

    // filter Underlying for Attach // add 2015.03.09
    self.BulkFilterIsSelected = ko.observable(false);
    self.BulkFilterUnderlyingDocument = ko.observable("");
    self.BulkFilterDocumentType = ko.observable("");
    self.BulkFilterCurrency = ko.observable("");
    self.BulkFilterStatementLetter = ko.observable("");
    self.BulkFilterAmount = ko.observable("");
    self.BulkFilterDateOfUnderlying = ko.observable("");
    self.BulkFilterExpiredDate = ko.observable("");
    self.BulkFilterReferenceNumber = ko.observable("");
    self.BulkFilterSupplierName = ko.observable("");
    self.BulkFilterInvoiceNumber = ko.observable("");
    self.BulkFilterIsBulkUnderlying = ko.observable(false);

    //// start Checked Calculate for Attach Grid and Underlying Grid
    self.TempSelectedAttachUnderlying = ko.observableArray([]);

    //// start Checked Calculate for Underlying Proforma Grid
    self.TempSelectedUnderlyingProforma = ko.observableArray([]);

    //// start Checked Calculate for Underlying Bulk Grid // add 2015.03.09
    self.TempSelectedUnderlyingBulk = ko.observableArray([]);

    //// start Checked Calculate for Underlying Grid
    self.TempSelectedUnderlying = ko.observableArray([]);

    self.TempSelectedUnderlyingTMO = ko.observableArray([]);

    self.TransactionModel().UnderlyingDoc.subscribe(function (UnderlyingDoc) {
        //get underlying code based on selected
        if (UnderlyingDoc > 0) {
            GetUnderlyingDocName(UnderlyingDoc);
        }
    });

    //The Object which stored data entered in the observables
    var CustomerUnderlying = {
        ID: self.ID_u,
        CIF: self.CIF_u,
        CustomerName: self.CustomerName_u,
        UnderlyingDocument: self.UnderlyingDocument_u,
        DocumentType: self.DocumentType_u,
        Currency: self.Currency_u,
        Amount: self.Amount_u,
        Rate: self.Rate_u,
        AmountUSD: self.AmountUSD_u,
        AvailableAmountUSD: self.AvailableAmount_u,
        AttachmentNo: self.AttachmentNo_u,
        StatementLetter: self.StatementLetter_u,
        IsDeclarationOfException: self.IsDeclarationOfException_u,
        StartDate: self.StartDate_u,
        EndDate: self.EndDate_u,
        DateOfUnderlying: self.DateOfUnderlying_u,
        ExpiredDate: self.ExpiredDate_u,
        ReferenceNumber: self.ReferenceNumber_u,
        SupplierName: self.SupplierName_u,
        InvoiceNumber: self.InvoiceNumber_u,
        IsSelectedProforma: self.IsSelectedProforma,
        IsProforma: self.IsProforma_u,
        IsUtilize: self.IsUtilize_u,
        IsEnable: self.IsEnable_u,
        Proformas: self.Proformas,
        IsSelectedBulk: self.IsSelectedBulk,
        IsBulkUnderlying: self.IsBulkUnderlying_u,
        BulkUnderlyings: self.BulkUnderlyings,
        IsJointAccount: self.IsJointAccount_u,
        AccountNumber: self.AccountNumber_u,
        OtherUnderlying: self.OtherUnderlying_u,
        LastModifiedBy: self.LastModifiedBy_u,
        LastModifiedDate: self.LastModifiedDate_u
    };

    var Documents = {
        Documents: self.Documents,
        DocumentPath: self.DocumentPath_a,
        Type: self.DocumentType_a,
        Purpose: self.DocumentPurpose_a
    }

    //Agung
    //var MutualFundForms = {
    //    MutualFundSP: TransactionUTModel.MutualFundSP,
    //    MutualFundCodeSP: TransactionUTModel.MutualFundCodeSP,
    //    CurrensySP: "1",
    //    AmountSP: TransactionUTModel.AmountSP
    //}
    //End Agung

    var CustomerUnderlyingFile = {
        ID: self.ID_a,
        FileName: DocumentModels().FileName,
        DocumentPath: DocumentModels().DocumentPath,
        DocumentPurpose: self.DocumentPurpose_a,
        DocumentType: self.DocumentType_a,
        DocumentRefNumber: self.DocumentRefNumber_a,
        LastModifiedBy: self.LastModifiedBy_a,
        LastModifiedDate: self.LastModifiedDate_a,
        CustomerUnderlyingMappings: self.CustomerUnderlyingMappings_a
    };

    self.onSelectionAttach = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            //self.IsSelectedAttach(false);
            self.TempSelectedAttachUnderlying.remove(data);
            ResetDataAttachment(index, false);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        } else {
            //self.IsSelectedAttach(true);
            self.TempSelectedAttachUnderlying.push({
                ID: item.ID
            });
            ResetDataAttachment(index, true);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        }
    }

    self.onSelectionProforma = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlyingProforma(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            //self.IsSelectedAttach(false);
            self.TempSelectedUnderlyingProforma.remove(data);
            ResetDataProforma(index, false);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        } else {
            //self.IsSelectedAttach(true);
            self.TempSelectedUnderlyingProforma.push({
                ID: item.ID
            });
            ResetDataProforma(index, true);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        }
    };
    // add 2015.03.09
    self.OnCurrencyChange = function (CurrencyID) {
        if (CurrencyID != null) {
            var item = ko.utils.arrayFirst(self.ddlCurrency_u(), function (x) { return CurrencyID == x.ID(); });
            if (item != null) {
                self.Currency_u(new CurrencyModel2(item.ID(), item.Code(), item.Description()));
            }
            if (!self.IsNewDataUnderlying()) {
                GetSelectedBulkID(GetDataBulkUnderlying);
            } else {
                self.TempSelectedUnderlyingBulk([]);
                GetDataBulkUnderlying();
            }

        }
    }
    self.onSelectionBulk = function (index, item) {
        // var temp = self.Amount_u();
        var total = 0;
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlyingBulk(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            //self.IsSelectedAttach(false);
            self.TempSelectedUnderlyingBulk.remove(data);
            ResetBulkData(index, false);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        } else {
            //self.IsSelectedAttach(true);
            self.TempSelectedUnderlyingBulk.push({
                ID: item.ID,
                Amount: item.Amount
            });
            ResetBulkData(index, true);
        }
        for (var i = 0; self.TempSelectedUnderlyingBulk.push() > i; i++) {
            total += self.TempSelectedUnderlyingBulk()[i].Amount;
        }
        if (total > 0) {
            //self.Amount_u(total);
            var e = total;
            var res = parseInt(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res).toFixed(2));
            self.Amount_u(formatNumber(e));
        } else {
            var e = self.tempAmountBulk();
            var res = parseInt(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res).toFixed(2));
            self.Amount_u(formatNumber(e));
            //self.Amount_u(self.tempAmountBulk());
        }
    }

    self.onSelectionUtilize = function (index, item) {
        var itemdeal;
        itemdeal = $('#Position' + index).val();
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
            return x.ID() == item.ID;
        });
        if (data != null) { //uncheck condition
            self.TempSelectedUnderlying.remove(data);
            if (viewModel.TransactionModel().IsIPETMO() == true) {
                ResetDataUnderlyingTMO(index, false);
            } else {
                ResetDataUnderlying(index, false);
            }
            setTotalUtilize();
        } else {
            var statementA = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                return x.StatementLetter() == 1;
            });
            var statementB = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                return x.StatementLetter() == 2;
            });
            var annualStatement = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                return x.StatementLetter() == 4;
            });
            var amount = document.getElementById("eqv-usd").value.replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '');
            if (item.StatementLetter.ID == 4) {
                ko.utils.arrayFilter(self.TempSelectedUnderlying(), function (yy) {
                    if (yy.StatementLetter() == 1 || yy.StatementLetter() == 2) {
                        ShowNotification('Please unchecked Statement A or Statement B', jqXHR.responseJSON.Message, 'gritter-warning', false);
                        return;
                    }
                });
                //Tambah Agung
                if (viewModel.TransactionModel().IsIPETMO() == true) {
                    self.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, false, parseFloat(itemdeal.replace(/,/g, '')), item.StatementLetter.ID));
                    ResetDataUnderlyingTMO(index, true);
                } else {
                    self.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, false, item.AvailableAmount, item.StatementLetter.ID));

                    ResetDataUnderlying(index, true);
                }
                setTotalUtilize();
            }
            else if (((statementA == null && item.StatementLetter.ID == 2) || (statementB == null && item.StatementLetter.ID == 1))
                //&& parseFloat(self.TransactionModel().utilizationAmount()) < parseFloat(amount)
                ) {
                //Tambah Agung
                if (viewModel.TransactionModel().IsIPETMO() == true) {
                    self.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, false, parseFloat(itemdeal.replace(/,/g, '')), item.StatementLetter.ID));
                    ResetDataUnderlyingTMO(index, true);
                } else {
                    self.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, false, item.AvailableAmount, item.StatementLetter.ID));

                    ResetDataUnderlying(index, true);
                }
                //setDataRow(index, true);
                setTotalUtilize();
            } else {
                if (viewModel.TransactionModel().IsIPETMO() == true) {
                    ResetDataUnderlyingTMO(index, false);
                } else {
                    ResetDataUnderlying(index, false);
                }
            }
        }
        //console.log(ko.toJSON(self.TempSelectedUnderlying()));
    };

    self.onSelectionUtilizeAll = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlyingTMO(), function (x) {
            return x.ID() == item.ID;
        });
        if (data != null) {
            self.TempSelectedUnderlyingTMO.remove(data);
            setTotalUtilizeTMO();
        } else {
            self.TempSelectedUnderlyingTMO.push(new SelectUtillizeModel(item.ID, false, item.UtilizeAmountDeal, item.StatementLetter.ID));
            setTotalUtilizeTMO();
        }

        var total = 0;
        if (self.TempSelectedUnderlyingTMO() != '') {
            ko.utils.arrayForEach(self.TempSelectedUnderlyingTMO(), function (item) {
                total += parseFloat(ko.utils.unwrapObservable(item.USDAmount().replace(/,/g, '')));
            });
        }
        if (parseFloat(viewModel.TransactionModel().DFAmount().replace(/,/g, '')) < total) {
            var datad = ko.utils.arrayFirst(self.TempSelectedUnderlyingTMO(), function (x) {
                return x.ID() == item.ID;
            });
            self.TempSelectedUnderlyingTMO.remove(datad);
            ResetDataUnderlyingTMOALL(index, false);
            setTotalUtilizeTMO();
            ShowNotification("Form Validation Warning", "Available Amount Underlying must be greater and lesh than Utilize Amount Deal", 'gritter-warning', true);
            return false;
        }
    };

    // New Data flag
    self.IsNewDataUnderlying = ko.observable(false);

    //tambah Agung
    self.InstructionDocuments = ko.observableArray([]);
    self.InstructionGridProperties = ko.observable();
    self.InstructionGridProperties(new GridPropertiesModel(GetDataInstructionDocument));
    self.InstructionGridProperties().SortColumn("ID");
    self.InstructionGridProperties().SortOrder("ASC");
    //End

    // grid properties Underlying
    self.UnderlyingGridProperties = ko.observable();
    self.UnderlyingGridProperties(new GridPropertiesModel(GetDataUnderlying));

    // grid properties Attach Grid
    self.AttachGridProperties = ko.observable();
    self.AttachGridProperties(new GridPropertiesModel(GetDataAttachFile));

    // grid properties Underlying File
    self.UnderlyingAttachGridProperties = ko.observable();
    self.UnderlyingAttachGridProperties(new GridPropertiesModel(GetDataUnderlyingAttach));

    // grid properties Proforma
    self.UnderlyingProformaGridProperties = ko.observable();
    self.UnderlyingProformaGridProperties(new GridPropertiesModel(GetDataUnderlyingProforma));

    // grid properties Bulk // 2015.03.09
    self.UnderlyingBulkGridProperties = ko.observable();
    self.UnderlyingBulkGridProperties(new GridPropertiesModel(GetDataBulkUnderlying));

    // set default sorting for Underlying Grid
    self.UnderlyingGridProperties().SortColumn("ID");
    self.UnderlyingGridProperties().SortOrder("ASC");

    // set default sorting for Underlying File Grid
    self.AttachGridProperties().SortColumn("FileName");
    self.AttachGridProperties().SortOrder("ASC");

    // set default sorting for Attach Underlying Grid
    self.UnderlyingAttachGridProperties().SortColumn("ID");
    self.UnderlyingAttachGridProperties().SortOrder("DESC");

    // set default sorting for Proforma grid
    self.UnderlyingProformaGridProperties().SortColumn("IsSelectedBulk");
    self.UnderlyingProformaGridProperties().SortOrder("DESC");

    // set default sorting for Bulk grid //add 2015.03.09
    self.UnderlyingBulkGridProperties().SortColumn("IsSelectedBulk");
    self.UnderlyingBulkGridProperties().SortOrder("DESC");

    //Started by Haqi
    self.setJenisDanAlamatIdentitas = ko.computed(function () {
        if (self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsIdentityTypeMaintenance()) {
            self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsIdentityAddressMaintenance(1);
        }
    });

    self.setUncexAlamatDanJenisIdentitas = ko.computed(function () {
        if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsIdentityAddressMaintenance()) {
            self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsIdentityTypeMaintenance(0);
        }
    });

    self.setAlamatKantorDanPekerjaan = ko.computed(function () {
        if (self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsOfficeAddressMaintenance()) {
            self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsJobMaintenance(1);
        }
    });

    self.setUncexAlamatKantorDanPekerjaan = ko.computed(function () {
        if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsJobMaintenance()) {
            self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsOfficeAddressMaintenance(0);
        }
    });

    self.setStatusPerkawinan = ko.computed(function () {
        if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsMaritalStatusMaintenance()) {
            self.IsMaritalStatusSudahMenikah(0);
        }
    });

    self.setUncekKorespondensiDanLaporanKeuangan = ko.computed(function () {
        if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsCorrespondenceMaintenance()) {
            self.CIFTransactionModel().RetailCIFCBO().IsCorrespondenseToEmail(0);
        }
    });
    //end by haqi
    self.setAmountFormatNumber = ko.computed(function () {
        //console.log(self.CIFTransactionModel().RetailCIFCBO().AmountLienUnlien);
        //console.log(document.getElementById("amount-lienunlien").value);
        var inputAmountData = self.CIFTransactionModel().RetailCIFCBO().AmountLienUnlien();//document.getElementById("amount-lienunlien").value;        
        if (inputAmountData !== null && inputAmountData !== undefined && inputAmountData != '') {
            inputAmountData = inputAmountData.toString();
            var val = Math.round(Number(inputAmountData.replace(/,/g, '')) * 100) / 100;
            var parts = val.toString().split(".");
            var num = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
            if (num === "0" || num === 0) return num = '';
            self.CIFTransactionModel().RetailCIFCBO().AmountLienUnlien(num);
        }
    });


    // bind clear filters
    self.UnderlyingClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.UnderlyingFilterIsSelected(false);
        self.UnderlyingFilterUnderlyingDocument("");
        self.UnderlyingFilterDocumentType("");
        self.UnderlyingFilterCurrency("");
        self.UnderlyingFilterStatementLetter("");
        self.UnderlyingFilterAmount("");
        self.UnderlyingFilterAvailableAmount("");
        self.UnderlyingFilterDateOfUnderlying("");
        self.UnderlyingFilterExpiredDate("");
        self.UnderlyingFilterReferenceNumber("");
        self.UnderlyingFilterSupplierName("");
        self.UnderlyingFilterIsProforma("");
        self.UnderlyingFilterIsAvailable(false);
        self.UnderlyingFilterIsExpiredDate(false);
        self.UnderlyingFilterIsSelectedBulk(false);

        GetDataUnderlying();
    };

    self.ProformaClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.ProformaFilterIsSelected(false);
        self.ProformaFilterUnderlyingDocument("");
        self.ProformaFilterDocumentType("Proforma");
        self.ProformaFilterCurrency("");
        self.ProformaFilterStatementLetter("");
        self.ProformaFilterAmount("");
        self.ProformaFilterDateOfUnderlying("");
        self.ProformaFilterExpiredDate("");
        self.ProformaFilterReferenceNumber("");
        self.ProformaFilterSupplierName("");
        self.ProformaFilterIsProforma("");

        GetDataUnderlyingProforma();
    };

    //add 2015.03.09
    self.BulkClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.BulkFilterIsSelected(false);
        self.BulkFilterUnderlyingDocument("");
        self.BulkFilterDocumentType("");
        self.BulkFilterCurrency("");
        self.BulkFilterStatementLetter("");
        self.BulkFilterAmount("");
        self.BulkFilterDateOfUnderlying("");
        self.BulkFilterExpiredDate("");
        self.BulkFilterReferenceNumber("");
        self.BulkFilterInvoiceNumber("");
        self.BulkFilterSupplierName("");
        self.BulkFilterIsBulkUnderlying("");

        GetDataBulkUnderlying();
    };

    // bind clear filters
    self.AttachClearFilters = function () {
        self.AttachFilterFileName("");
        self.AttachFilterDocumentPurpose("");
        self.AttachFilterModifiedDate("");
        self.AttachFilterDocumentRefNumber("");
        self.AttachFilterAccountNumber("");
        GetDataAttachFile();
    };

    // bind clear filters
    self.UnderlyingAttachClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.UnderlyingAttachFilterIsSelected(false);
        self.UnderlyingAttachFilterUnderlyingDocument("");
        self.UnderlyingAttachFilterDocumentType("");
        self.UnderlyingAttachFilterCurrency("");
        self.UnderlyingAttachFilterStatementLetter("");
        self.UnderlyingAttachFilterAmount("");
        self.UnderlyingAttachFilterDateOfUnderlying("");
        self.UnderlyingAttachFilterExpiredDate("");
        self.UnderlyingAttachFilterReferenceNumber("");
        self.UnderlyingAttachFilterSupplierName("");
        self.UnderlyingAttachFilterAccountNumber("");
        GetDataUnderlyingAttach();
    };

    //Tambah Agung
    self.InstructionClearFilters = function () {
        self.InstructionFilterIsSelected(false);
        self.InstructionFilterApplicationID("");
        self.InstructionFilterFileName("");
        self.InstructionFilterPurposeofDoc("");
        self.InstructionFilterDocumentType("");
        self.InstructionFilterLastModifiedDate("");
        GetDataInstructionDocument();
    };
    //End

    // bind get data function to view
    self.GetDataUnderlying = function () {
        GetDataUnderlying();
    };

    self.GetSelectedUtilizeID = function () {
        GetSelectedUtilizeID();
    }

    self.GetDataAttachFile = function () {
        GetDataAttachFile();
    }

    self.GetDataUnderlyingAttach = function () {
        GetDataUnderlyingAttach();
    }

    self.GetDataUnderlyingProforma = function () {
        GetDataUnderlyingProforma();
    }

    // 2015.03.09
    self.GetDataBulkUnderlying = function () {
        GetDataBulkUnderlying();
    }

    //Tambah Agung
    self.GetDataInstructionDocument = function () {
        GetDataInstructionDocument();
    }

    self.save_utmo = function () {
        self.CustomerUnderlyingsTMO([]);
        self.TempSelectedUnderlying([]);
        ko.utils.arrayForEach(self.CustomerUnderlyingsTMOALL(), function (item) {
            if (item.IsEnable == true) {
                var data = ko.utils.arrayFirst(self.TempSelectedUnderlyingTMO(), function (x) {
                    return x.ID() == item.ID;
                });
                if (data != null) {
                    self.TempSelectedUnderlyingTMO.remove(data)
                }
                item.AvailableAmountDeal = parseFloat(item.UtilizeAmountDeal.replace(/,/g, ''));
                self.CustomerUnderlyingsTMO.push(item)
                self.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, false, parseFloat(item.UtilizeAmountDeal.replace(/,/g, '')), item.StatementLetter.ID));
            }
        });
        setTotalUtilize();
        self.TransactionModel().UpdateDFAmount(true);
        if (viewModel.TransactionModel().UpdateDFAmount() == true) {
            viewModel.TransactionModel().DFUnderlyings([]);
            ko.utils.arrayForEach(viewModel.CustomerUnderlyingsTMO(), function (itemu) {
                if (itemu.IsEnable == true) {
                    viewModel.TransactionModel().DFUnderlyings.push(new SelectUtillizeModel(itemu.ID, false, itemu.AvailableAmountDeal, ''));
                }

            });
        }
        $("#modal-form-UnderlyingTMO").modal('hide');
    };
    //End

    self.GetUnderlyingParameters = function (data) {
        GetUnderlyingParameters(data);
    }

    self.save_u = function () {
        self.IsEditTableUnderlying(false);
        var form = $("#frmUnderlying");
        form.validate();

        if (form.valid()) {
            viewModel.Amount_u(viewModel.Amount_u().toString().replace(/,/g, ''));
            CustomerUnderlying.OtherUnderlying = self.CodeUnderlying() == '999' ? self.OtherUnderlying_u : '';
            SetMappingProformas();
            SetMappingBulks(); // add 2015.03.09
            self.AmountUSD_u(self.AmountUSD_u().toString().replace(/,/g, ''));
            CustomerUnderlying.AvailableAmountUSD(self.AmountUSD_u());
            //Ajax call to insert the CustomerUnderlyings
            $.ajax({
                type: "POST",
                url: api.server + api.url.customerunderlying + "/Save",
                data: ko.toJSON(CustomerUnderlying), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {

                        // send notification
                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        // hide current popup window
                        $("#modal-form-Underlying").modal('hide');
                        self.IsEditTableUnderlying(true);
                        self.TempSelectedUnderlyingTMO([]);
                        // refresh data
                        GetDataUnderlying();
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                        self.IsEditTableUnderlying(true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    if (jqXHR.responseJSON.Message.toLowerCase().startsWith("double underlying")) {
                        ShowNotification("Attention", jqXHR.responseJSON.Message, 'gritter-warning', true);
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    } self.IsEditTableUnderlying(true);
                }
            });
        } else {
            self.IsEditTableUnderlying(true);
        }
    };

    self.save_a = function () {

        self.IsUploading(true);

        var form = $("#frmUnderlying");
        form.validate();
        if (form.valid()) {

            var FxWithUnderlying = (self.TempSelectedAttachUnderlying().length > 0);
            var FxWithNoUnderlying = (self.DocumentPurpose_a().ID() != 2);

            if (FxWithUnderlying || FxWithNoUnderlying) {
                var data = {
                    CIF: CustomerUnderlying.CIF,
                    Name: CustomerUnderlying.CustomerName,
                    Type: ko.utils.arrayFirst(self.ddlDocumentType_a(), function (item) {
                        return item.ID == self.DocumentType_a().ID();
                    }),
                    Purpose: ko.utils.arrayFirst(self.ddlDocumentPurpose_a(), function (item) {
                        return item.ID == self.DocumentPurpose_a().ID();
                    })
                };

                if (FxWithUnderlying) {
                    UploadFileUnderlying(data, Documents, SaveDataFile);
                }
                else if (FxWithNoUnderlying) {
                    self.IsFXTransactionAttach(true);
                    var file = $('#document-path-upload').data().ace_input_files[0];
                    var doc = {
                        ID: 0,
                        Type: data.Type,
                        Purpose: data.Purpose,
                        FileName: file.name,
                        DocumentPath: file,
                        //FileName: Documents.DocumentPath().name,
                        //DocumentPath: self.DocumentPath_a(),
                        LastModifiedDate: new Date(),
                        LastModifiedBy: null
                    };

                    //alert(JSON.stringify(self.DocumentPath()))
                    if (doc.Type == null || doc.Purpose == null || doc.DocumentPath == null) {
                        alert("Please complete the upload form fields.");
                    } else {
                        self.Documents.push(doc);
                        if (viewModel.TransactionModel().IsIPETMO() == true) {
                            var documentStatementLetter = ko.utils.arrayFilter(self.Documents(), function (items) {
                                return items.Purpose.Name == "Statement Letter";
                            });
                            var documentUnderlying = ko.utils.arrayFilter(self.Documents(), function (itemu) {
                                return itemu.Purpose.Name == "Underlying";
                            });
                            var documentInstruction = ko.utils.arrayFilter(self.Documents(), function (itemi) {
                                return itemi.Purpose.Name == "Instruction";
                            });
                            if (documentStatementLetter != "") {
                                viewModel.TransactionModel().StatementLetter(true);
                            }
                            if (documentUnderlying != "") {
                                viewModel.TransactionModel().Underlying(true);
                            }
                            if (documentInstruction != "") {
                                viewModel.TransactionModel().Instruction(true);
                            }
                        }
                        $('.remove').click();

                        // hide upload dialog
                        $("#modal-form-Attach").modal('hide');
                        viewModel.IsEditable(true);
                        viewModel.IsUploading(false);
                    }
                }
            }
            else {
                viewModel.IsEditable(true);
                viewModel.IsUploading(false);
                alert('Please select the underlying document');
            }
        } else {
            viewModel.IsEditable(true);
            viewModel.IsUploading(false);
        };
    };

    self.update_u = function () {


        var form = $("#frmUnderlying");
        form.validate();

        if (form.valid()) {
            // hide current popup window
            //$("#modal-form-Underlying").modal('hide');
            viewModel.Amount_u(viewModel.Amount_u().toString().replace(/,/g, ''));

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form-Underlying").modal('show');
                } else {
                    CustomerUnderlying.OtherUnderlying = self.CodeUnderlying() == '999' ? self.OtherUnderlying_u : '';
                    SetMappingProformas();
                    SetMappingBulks(); // Add 2015.03.09
                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.customerunderlying + "/" + CustomerUnderlying.ID(),
                        data: ko.toJSON(CustomerUnderlying),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                                // enable Rule validation
                                $("#modal-form-Underlying").modal('hide');

                                // refresh data
                                GetDataUnderlying();
                                ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (itemtemp) {
                                    if (itemtemp.ID() == CustomerUnderlying.ID()) {
                                        self.TempSelectedUnderlying.remove(itemtemp);
                                        itemtemp.USDAmount(CustomerUnderlying.AmountUSD());
                                        self.TempSelectedUnderlying.push(itemtemp);
                                        setTotalUtilize();
                                    }
                                });
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            if (jqXHR.responseJSON.Message.toLowerCase().startsWith("double underlying")) {
                                ShowNotification("Attention", jqXHR.responseJSON.Message, 'gritter-warning', true);
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                            }
                        }
                    });
                }
            });
        }
    };

    self.delete_u = function () {
        // hide current popup window
        $("#modal-form-Underlying").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form-Underlying").modal('show');
            } else {
                // remove check utilize underlying
                RemoveTempUtilizeByID(underlyingID);
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customerunderlying + "/" + CustomerUnderlying.ID(),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                            // refresh data
                            GetDataUnderlying();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    self.cancel_u = function () {

    }

    self.cancel_a = function () {
        viewModel.IsEditable(true);
        viewModel.IsUploading(false);
    }
    function RemoveTempUtilizeByID(underlyingID) {


    }
    self.delete_a = function (item) {
        // hide current popup window
        //$("#modal-form").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                //$("#modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customerunderlyingfile + "/" + item.ID,
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            DeleteFile(item.DocumentPath);
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetDataAttachFile();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    }

    //Function to Display record to be updated
    self.GetUnderlyingSelectedRow = function (data) {
        self.IsNewDataUnderlying(false);

        $("#modal-form-Underlying").modal('show');

        self.ID_u(data.ID);
        self.IsUtilize_u(data.IsUtilize);
        self.CustomerName_u(data.CustomerName);

        self.UnderlyingDocument_u(new UnderlyingDocumentModel2(data.UnderlyingDocument.ID, data.UnderlyingDocument.Name, data.UnderlyingDocument.Code));
        self.OtherUnderlying_u(data.OtherUnderlying);
        self.DocumentType_u(new DocumentTypeModel2(data.DocumentType.ID, data.DocumentType.Name));
        self.Currency_u(new CurrencyModel2(data.Currency.ID, data.Currency.Code, data.Currency.Description));
        self.Amount_u(data.Amount);
        self.Rate_u(data.Rate);
        self.AvailableAmount_u(data.AvailableAmountUSD);
        self.AttachmentNo_u(data.AttachmentNo);
        self.StatementLetter_u(new StatementLetterModel(data.StatementLetter.ID, data.StatementLetter.Name));
        self.IsDeclarationOfException_u(data.IsDeclarationOfException);
        self.StartDate_u(self.LocalDate(data.StartDate, true, false));
        self.EndDate_u(self.LocalDate(data.EndDate, true, false));
        self.DateOfUnderlying_u(self.LocalDate(data.DateOfUnderlying, true, false));
        self.ExpiredDate_u(self.LocalDate(data.ExpiredDate, true, false));
        self.ReferenceNumber_u(data.ReferenceNumber);
        self.SupplierName_u(data.SupplierName);
        self.InvoiceNumber_u(data.InvoiceNumber);
        self.IsProforma_u(data.IsProforma);
        self.IsJointAccount_u(data.IsJointAccount);
        self.AccountNumber_u(data.AccountNumber);
        self.IsSelectedProforma(data.IsSelectedProforma);
        self.IsBulkUnderlying_u(data.IsBulkUnderlying); // add 2015.03.09
        self.IsSelectedBulk(data.IsSelectedBulk); // add 2015.03.09
        self.tempAmountBulk(0);//add 2015.03.09
        self.LastModifiedBy_u(data.LastModifiedBy);
        self.LastModifiedDate_u(data.LastModifiedDate);

        //Call Grid Underlying for Check Proforma
        GetSelectedProformaID(GetDataUnderlyingProforma);
        //GetDataUnderlyingProforma();
        //Call Grid Underlying for Check Bullk // add 2015.03.09
        GetSelectedBulkID(GetDataBulkUnderlying);
        //GetDataBulkUnderlying();
        // save temp Amount
        self.tempAmountBulk(data.Amount);
    };

    //Add Attach File
    self.NewAttachFile = function () {
        // show the dialog task form
        // ace file upload


        $("#modal-form-Attach").modal('show');
        self.TempSelectedAttachUnderlying([]);
        self.ID_a(0);
        self.DocumentPurpose_a(new DocumentPurposeModel2('', '', ''));
        self.DocumentType_a(new DocumentTypeModel2('', ''));
        self.DocumentPath_a('');
        GetDataUnderlyingAttach();
    }
    //insert new

    self.NewDataUnderlying = function () {

        if (self.TransactionModel().IsNewCustomer() && !viewModel.IsLoadDraft() && (self.IsFxTransaction() || self.IsFxTransactionToIDR())) {
            ShowNotification("Page Information", NewCustomerFXWarning, "gritter-warning", false);
            self.IsEditable(true);
            return false;
            //NewCustomerFXWarning
        }

        if (TransactionModel.Product().Name == 'OTT') {
            if (viewModel.TransactionModel().Sundry() == null) {
                var IsSundryStarFalse = viewModel.TransactionModel().Sundry() != null;
                self.IsSundryFilled(IsSundryStarFalse);
            }
            else {
                var IsSundryStarFalse = viewModel.TransactionModel().Sundry() == null;
                self.IsSundryFilled(IsSundryStarFalse);
            }

        } else {
            if (viewModel.TransactionModel().BeneAccNumber() != null && viewModel.TransactionModel().BeneAccNumber() != "") {
                //$('#debit-sundry').data({ ruleRequired: true })
            }
            else {
                //$('#debit-sundry').data({ ruleRequired: false })
            }
        }
        //$('#underlying-code').data({ ruleRequired: false });
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            //Remove required filed if show
            $('.form-group').removeClass('has-error').addClass('has-info');
            $('.help-block').remove();

            $("#modal-form-Underlying").modal('show');
            // flag as new Data
            self.IsNewDataUnderlying(true);
            // bind empty data
            self.ID_u(0);
            self.IsUtilize_u(false);
            self.UnderlyingDocument_u(new UnderlyingDocumentModel2('', '', ''));
            self.OtherUnderlying_u('');
            self.DocumentType_u(new DocumentTypeModel2('', ''));
            self.StatementLetter_u(new StatementLetterModel('', ''));
            self.Currency_u(new CurrencyModel2('', '', ''));
            self.Amount_u(0);
            self.AvailableAmount_u(0);
            self.Rate_u(0);
            self.AttachmentNo_u('');
            self.IsDeclarationOfException_u(false);
            self.StartDate_u('');
            self.EndDate_u('');
            self.DateOfUnderlying_u('');
            self.ExpiredDate_u('');
            self.ReferenceNumber_u('');
            self.SupplierName_u('');
            self.InvoiceNumber_u('');
            self.IsProforma_u(false);
            self.IsSelectedProforma(false);
            self.IsBulkUnderlying_u(false); //add 2015.03.09
            self.IsSelectedBulk(false); //add 2015.03.09
            self.IsJointAccount_u(self.TransactionModel().IsJointAccount());
            if (self.IsJointAccount_u()) {
                self.AccountNumber_u(self.Selected().Account());
            }
            //Call Grid Underlying for Check Proforma
            GetDataUnderlyingProforma();
            GetDataBulkUnderlying();
            self.TempSelectedUnderlyingProforma([]);
            self.TempSelectedUnderlyingBulk([]);
            //SetStatementA(false, null);
            setRefID();
        } else {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled.", 'gritter-warning', false);
        }
    };
    //Tambah Agung
    self.NewDataUnderlyingTMO = function () {
        $('.form-group').removeClass('has-error').addClass('has-info');
        $('.help-block').remove();

        $("#modal-form-Underlying").modal('show');
        // flag as new Data
        self.IsNewDataUnderlying(true);
        // bind empty data
        self.ID_u(0);
        self.IsUtilize_u(false);
        self.UnderlyingDocument_u(new UnderlyingDocumentModel2('', '', ''));
        self.OtherUnderlying_u('');
        self.DocumentType_u(new DocumentTypeModel2('', ''));
        self.StatementLetter_u(new StatementLetterModel('', ''));
        self.Currency_u(new CurrencyModel2('', '', ''));
        self.Amount_u(0);
        self.AvailableAmount_u(0);
        self.Rate_u(0);
        self.AttachmentNo_u('');
        self.IsDeclarationOfException_u(false);
        self.StartDate_u('');
        self.EndDate_u('');
        self.DateOfUnderlying_u('');
        self.ExpiredDate_u('');
        self.ReferenceNumber_u('');
        self.SupplierName_u('');
        self.InvoiceNumber_u('');
        self.IsProforma_u(false);
        self.IsSelectedProforma(false);
        self.IsBulkUnderlying_u(false); //add 2015.03.09
        self.IsSelectedBulk(false); //add 2015.03.09
        self.IsJointAccount_u(self.TransactionModel().IsJointAccount());
        if (self.IsJointAccount_u()) {
            self.AccountNumber_u(self.Selected().Account());
        }
        //Call Grid Underlying for Check Proforma
        GetDataUnderlyingProforma();
        GetDataBulkUnderlying();
        self.TempSelectedUnderlyingProforma([]);
        self.TempSelectedUnderlyingBulk([]);
        //SetStatementA(false, null);
        setRefID();
    };

    self.UpdateDataUnderlyingTMO = function () {
        viewModel.GetDataUnderlying();
        self.DFAmount($('#dfamount').val());
        $('.form-group').removeClass('has-error').addClass('has-info');
        $('.help-block').remove();

        $("#modal-form-UnderlyingTMO").modal('show');
    };
    //End
    //aridya add 20161011 add for SKN Bulk ~OFFLINE~
    function SaveSKNBulk() {
        var form = $("#aspnetForm");
        form.validate();
        if (form.valid() && viewModel.TransactionModel().Customer().CIF != "") {
            // set as not draft
            self.TransactionModel().IsDraft(false);
            self.IsEditable(false);
            self.DoubleTransactions().removeAll;

            //aridya 20161011 set default for double transaction in validatetransaction ~OFFLINE~
            self.TransactionModel().Amount('0');
            viewModel.TransactionModel().Currency().Code = "usd" //will default to usd
            self.TransactionModel().DebitCurrency().ID(optionSelected.IsCurrency); //will default to value in DBS.js
            //end add

            ValidateTransaction();
            //var text = "Are you sure to make this transaction as an <b>Exceptional Handling</b>?";
            //bootbox.confirm(text, function (result) {
            //    if (result) {
            //        // Validation #2 : Check new transaction status
            //        ValidateTransaction();
            //    } else {
            //        // enable form input controls
            //        self.IsEditable(true);
            //    }
            //});
        }
        else {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        }
    }
    //end add

    function SavePayment() {
        if (self.TransactionModel().IsNewCustomer() && (self.IsFxTransaction() || self.IsFxTransactionToIDR())) {
            ShowNotification("Page Information", NewCustomerFXWarning, "gritter-warning", false);
            self.IsEditable(true);
            return false;
            //NewCustomerFXWarning
        }

        //enter tab
        //9-11-2016 dani
        if (self.selectedBranchID() !== undefined && self.selectedBranchID() !== null && self.selectedBranchID() != "" && (typeof self.selectedBranchID() !== 'function')) {
            viewModel.TransactionModel().BranchID = self.selectedBranchID();
        }
        //dani end

        if (TransactionModel.Product().Name == 'OTT') {
            if (viewModel.TransactionModel().Sundry() == null) {
                var IsSundryStarFalse = viewModel.TransactionModel().Sundry() != null;
                self.IsSundryFilled(IsSundryStarFalse);
            }
            else {
                var IsSundryStarFalse = viewModel.TransactionModel().Sundry() == null;
                self.IsSundryFilled(IsSundryStarFalse);
            }

            /*if (viewModel.Selected().UnderlyingDoc() == null) {
                ShowNotification("Form Validation Warning", "Please select 'underlying' in Underlying Code", 'gritter-warning', true);
                return;
            }else*/

            if (!IsValidDataUnderlying()) {
                ShowNotification("Form Validation Warning", "Please dont select 'tanpa undrlying' in Underlying Code", 'gritter-warning', true);
                return;
            }

            if (viewModel.TransactionModel().ModePayment() != "IPE" && viewModel.TransactionModel().ModePayment() != "BCP1") {
                $('#underlying-code').data({ ruleRequired: true });
            }
            if (viewModel.TransactionModel().ModePayment() == "IPE" || viewModel.TransactionModel().ModePayment() == "BCP1") {
                if (viewModel.TransactionModel().IsOtherAccountNumber() === false) {
                    $('#debit-acc-number').data({ ruleRequired: true });
                }
            }
            //setDataUnderlying();           

        } else {
            if (viewModel.TransactionModel().ModePayment() == "IPE" || viewModel.TransactionModel().ModePayment() == "BCP1") {
                if (viewModel.TransactionModel().IsOtherAccountNumber() === false) {
                    $('#debit-acc-number').data({ ruleRequired: true });
                }
            }
            if (viewModel.TransactionModel().BeneAccNumber() != null && viewModel.TransactionModel().BeneAccNumber() != "") {
                //$('#debit-sundry').data({ ruleRequired: true })
            }
            else {
                //$('#debit-sundry').data({ ruleRequired: false })
            }
        }

        //echange henggar best practice 8 juni 2017
        viewModel.IsValid(false);
        if (TransactionModel.Product().Code == 'SK') {
            var a;
            if (viewModel.TransactionModel().ModePayment() == "IPE") {
                for (var i = 0; i < MandatoryNotValidate.length; i++) {
                    if ($(MandatoryNotValidate[i].Name).val() == "") {
                        $(MandatoryNotValidate[i].Name).data({ ruleRequired: true });
                        var a = $(MandatoryNotValidate[i].Name);
                        a.validate();
                        a.valid();
                        if (a.valid() == false) {
                            viewModel.IsEditable(true);
                            viewModel.IsValid(true);
                        }
                    }
                }
            }
        }
            //end
        else if (TransactionModel.Product().Code == 'OT' || TransactionModel.Product().Code == 'RT' || TransactionModel.Product().Code == 'OV') {
            if (viewModel.IsPPURole() == true) {
                var FieldToValidate = [{ ID: 1, Name: '#channel' }, { ID: 2, Name: '#source' }, { ID: 3, Name: '#lldcode' }, { ID: 4, Name: '#underlying-code' }, { ID: 5, Name: '#ComCode' }, { ID: 6, Name: '#debit-acc-number-empty' }, { ID: 7, Name: '#ChAccount' }]
            } else {
                var FieldToValidate = [{ ID: 1, Name: '#channel' }, { ID: 2, Name: '#lldcode' }, { ID: 3, Name: '#underlying-code' }, { ID: 4, Name: '#ComCode' }, { ID: 5, Name: '#debit-acc-number-empty' }, { ID: 6, Name: '#ChAccount' }]
            }
            if (viewModel.TransactionModel().ModePayment() == "IPE") {
                for (var i = 0; i < FieldToValidate.length; i++) {
                    if ($(FieldToValidate[i].Name).data().ruleRequired == true) {
                        var a = $(FieldToValidate[i].Name);
                        a.validate();
                        a.valid();

                        if (a.valid() == false) {
                            viewModel.IsEditable(true);
                            viewModel.IsValid(true);
                        }
                    }
                }
            }
        }

        // validation
        var form = $("#aspnetForm");
        form.validate();
        if (form.valid() && viewModel.TransactionModel().Customer().CIF != "") {
            // set as not draft
            self.TransactionModel().IsDraft(false);
            self.IsEditable(false);
            self.DoubleTransactions().removeAll;

            // Validation #1 : Check "Others" field
            if (self.TransactionModel().Others() == undefined || self.TransactionModel().Others() == "") {
                ValidateTransaction();

            } else {
                var text = "Are you sure to make this transaction as an <b>Exceptional Handling</b>?";
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // Validation #2 : Check new transaction status
                        ValidateTransaction();
                    } else {
                        // enable form input controls
                        self.IsEditable(true);
                    }
                });
            }
        }
        else {
            if (viewModel.IsValid()) { // --> //echange henggar best practice 8 juni 2017 if id html can not validate..
                viewModel.IsEditable(true);
                ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
            } else {
                viewModel.IsEditable(true);
                ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
            }
        }
    }

    //Afif
    function SaveLoan() {
        var form = $("#aspnetForm");
        form.validate();
        if (form.valid() && viewModel.TransactionLoanModel().Customer().CIF != "") {
            self.TransactionLoanModel().IsDraft(false);
            self.TransactionLoanModel().Amount($("#trxn-amount").val().replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(' ', ''));
            self.IsEditable(false);
            var data = {
                ApplicationID: viewModel.TransactionLoanModel().ApplicationID(),
                CIF: viewModel.TransactionLoanModel().Customer().CIF,
                Name: viewModel.TransactionLoanModel().Customer().Name
            };
            if (viewModel.Documents().length > 0) {
                //for (var i = 0; i < viewModel.Documents().length; i++) {
                //    UploadFile(data, viewModel.Documents()[i], SaveLoanTransaction);
                UploadFileRecuresive(data, viewModel.Documents(), SaveLoanTransaction, viewModel.Documents().length);
                //}
            } else {
                SaveLoanTransaction();
            }
        }
        else {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        }
    }
    //End Afif

    //Dani dp 5-10-2015
    function SaveTMO() {
        CheckCSO();
        var form = $("#aspnetForm");
        if (form.valid() && viewModel.TransactionTMOModel().Customer().CIF != "") {
            self.TransactionTMOModel().IsDraft(false);
            self.IsEditable(false);
            var data = {
                ApplicationID: viewModel.TransactionTMOModel().ApplicationID(),
                CIF: viewModel.TransactionTMOModel().Customer().CIF,
                Name: viewModel.TransactionTMOModel().Customer().Name
            };
            //if cso document is mandatory
            if (viewModel.Documents().length < 1) {
                ShowNotification("Attention", "Please attach document", 'gritter-warning', true);
                self.IsEditable(true);
                return;
            }
            if (viewModel.Documents().length > 0) {
                //for (var i = 0; i < viewModel.Documents().length; i++) {
                //UploadFile(data, viewModel.Documents()[i], SaveTransactionTMO);
                UploadFileRecuresive(data, viewModel.Documents(), SaveTransactionTMO, viewModel.Documents().length);

                //}
            } else {
                SaveTransactionTMO();
            }
        }
        else {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        }
    }
    //End Dani
    //Agung
    function SaveUT() {
        var form = $("#aspnetForm");
        form.validate();
        if (form.valid() && viewModel.TransactionUTModel().Customer().CIF != "") {
            self.TransactionUTModel().IsDraft(false);
            self.IsEditable(false);
            var data = {
                ApplicationID: viewModel.TransactionUTModel().ApplicationID(),
                CIF: viewModel.TransactionUTModel().Customer().CIF,
                Name: viewModel.TransactionUTModel().Customer().Name
            };
            if (viewModel.Documents().length > 0) {//dani dp S
                //for (var i = 0; i < viewModel.Documents().length; i++) {
                //    UploadFile(data, viewModel.Documents()[i], SaveTransactionUT);
                UploadFileRecuresive(data, viewModel.Documents(), SaveTransactionUT, viewModel.Documents().length);
                //}
            } else {
                SaveTransactionUT();
            }
        }
        else {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        }
    }
    //End AGung

    function SaveCIF() {
        var form = $("#aspnetForm");
        form.validate();
        if (form.valid() && viewModel.CIFTransactionModel().Customer().CIF != "") {
            var data = {
                ApplicationID: viewModel.CIFTransactionModel().ApplicationID(),
                CIF: viewModel.CIFTransactionModel().Customer().CIF,
                Name: viewModel.CIFTransactionModel().Customer().Name
            };

            self.Selected().DocumentPurpose(1);

            if (viewModel.DocumentsCIF().length > 0) {
                //for (var i = 0; i < viewModel.DocumentsCIF().length; i++) {
                UploadFileRecuresive(data, viewModel.DocumentsCIF(), SaveTransactionCIF, viewModel.DocumentsCIF().length);
                //UploadFileCIF(data, viewModel.DocumentsCIF()[i], SaveTransactionCIF);
                //}
            } else {
                SaveTransactionCIF()
            }
        }
    }
    function GetUnderlyingDocName(UnderlyingDoc) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.underlyingdoc + "/" + UnderlyingDoc.ID,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if (data != null) {
                        self.isNewUnderlying(false); //return default disabled

                        // available type to input for new underlying name if code is 999
                        if (data.Code == '999') {
                            viewModel.TransactionModel().UnderlyingDoc().Description('');
                            viewModel.isNewUnderlying(true);
                            $('#book-underlying-desc').focus();
                            $('#book-underlying-desc').attr('data-rule-required', true);
                        }
                        else {
                            viewModel.isNewUnderlying(false);
                            viewModel.TransactionModel().OtherUnderlyingDoc('');

                            $('#book-underlying-desc').attr('data-rule-required', false);
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function ResetDataAttachment(index, isSelect) {
        self.CustomerAttachUnderlyings()[index].IsSelectedAttach = isSelect;
        var dataRows = self.CustomerAttachUnderlyings().slice(0);
        self.CustomerAttachUnderlyings([]);
        self.CustomerAttachUnderlyings(dataRows);
    }

    function ResetDataProforma(index, isSelect) {
        self.CustomerUnderlyingProformas()[index].IsSelectedProforma = isSelect;
        var dataRows = self.CustomerUnderlyingProformas().slice(0);
        self.CustomerUnderlyingProformas([]);
        self.CustomerUnderlyingProformas(dataRows);
    }

    function ResetBulkData(index, isSelect) { //add 2015.03.09
        self.CustomerBulkUnderlyings()[index].IsSelectedBulk = isSelect;
        var dataRows = self.CustomerBulkUnderlyings().slice(0);
        self.CustomerBulkUnderlyings([]);
        self.CustomerBulkUnderlyings(dataRows);
    }

    function ResetDataUnderlying(index, isSelect) {
        self.CustomerUnderlyings()[index].IsEnable = isSelect;
        var dataRows = self.CustomerUnderlyings().slice(0);
        self.CustomerUnderlyings([]);
        self.CustomerUnderlyings(dataRows);
    }

    //Tambah Agung
    function ResetDataUnderlyingTMO(index, isSelect) {
        self.CustomerUnderlyingsTMO()[index].IsEnable = isSelect;
        var itemdeal = $('#Position' + index).val();
        if (itemdeal != undefined) {
            self.CustomerUnderlyingsTMO()[index].UtilizeAmountDeal = itemdeal;
        }
        var dataRows = self.CustomerUnderlyingsTMO().slice(0);
        self.CustomerUnderlyingsTMO([]);
        if (dataRows[0].UtilizeAmountDeal == "NaN") {
            dataRows[0].UtilizeAmountDeal = formatNumber(dataRows[0].AvailableAmountDeal);
        }
        self.CustomerUnderlyingsTMO(dataRows);
    }
    function ResetDataUnderlyingTMOALL(index, isSelect) {
        self.CustomerUnderlyingsTMOALL()[index].IsEnable = isSelect;
        var dataRows = self.CustomerUnderlyingsTMOALL().slice(0);
        self.CustomerUnderlyingsTMOALL([]);
        self.CustomerUnderlyingsTMOALL(dataRows);
    }
    //End

    function SetSelectedProforma(data) {
        for (var i = 0; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlyingProforma(), function (item) {
                return item.ID == data[i].ID;
            });
            if (selected != null) {
                data[i].IsSelectedProforma = true;
            }
            else {
                data[i].IsSelectedProforma = false;
            }
        }
    }

    function SetSelectedBulk(data) { //add 2015.03.09

        for (var i = 0 ; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlyingBulk(), function (item) {
                return item.ID == data[i].ID;
            });
            if (selected != null) {
                data[i].IsSelectedBulk = true;
            }
            else {
                data[i].IsSelectedBulk = false;
            }
        }
        return data;
    }

    function TotalUtilize() {
        var total = 0;
        ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (item) {
            if (item.Enable() == false)
                total += parseFloat(ko.utils.unwrapObservable(item.USDAmount()));
        });
        var fixTotal = parseFloat(total).toFixed(2);
        return fixTotal;
    }

    function GetRateIDRAmount(CurrencyID) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
            params: {
            },
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    self.Rate_u(data.RupiahRate);
                    //if(CurrencyID!=1){ // if not USD
                    var fromFormat = document.getElementById("Amount_u").value;
                    fromFormat = fromFormat.toString().replace(/,/g, '');
                    res = parseFloat(fromFormat) * data.RupiahRate / idrrate;
                    res = Math.round(res * 100) / 100;
                    res = isNaN(res) ? 0 : res; //avoid NaN
                    self.AmountUSD_u(parseFloat(res).toFixed(2));

                    self.Amount_u(formatNumber(fromFormat));
                }
            }
        });
    }

    function GetUSDAmount(currencyID, amount, IdUnderlying) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetAmountUSD/" + currencyID + "/" + amount,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlying.push(new SelectUtillizeModel(IdUnderlying, false, data, ''));
                    var total = TotalUtilize();
                    viewModel.TransactionModel().utilizationAmount(parseFloat(total));
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function SetSelectedUnderlying(data) {
        for (var i = 0; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (item) {
                return item.ID() == data[i].ID;
            });
            if (selected != null) {
                data[i].IsEnable = true;
                data[i].UtilizeAmountDeal = formatNumber(selected.USDAmount());
            }
            else {
                data[i].IsEnable = false;
            }
        }
        return data;
    }

    function SetMappingAttachment() {
        CustomerUnderlyingFile.CustomerUnderlyingMappings([]);
        for (var i = 0 ; i < self.TempSelectedAttachUnderlying().length; i++) {
            CustomerUnderlyingFile.CustomerUnderlyingMappings.push(new CustomerUnderlyingMappingModel(0, self.TempSelectedAttachUnderlying()[i].ID, customerNameData + '-(A)'));
        }
    }

    function SetMappingProformas() {
        CustomerUnderlying.Proformas([]);
        for (var i = 0 ; i < self.TempSelectedUnderlyingProforma().length; i++) {
            CustomerUnderlying.Proformas.push(new SelectModel(cifData, self.TempSelectedUnderlyingProforma()[i].ID));
        }
    }
    function SetMappingBulks() { //add 2015.03.09
        CustomerUnderlying.BulkUnderlyings([]);
        for (var i = 0 ; i < self.TempSelectedUnderlyingBulk().length; i++) {
            CustomerUnderlying.BulkUnderlyings.push(new SelectBulkModel(cifData, self.TempSelectedUnderlyingBulk()[i].ID));
        }
    }

    function setTotalUtilize() {
        var total = 0;
        ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (item) {
            total += parseFloat(ko.utils.unwrapObservable(item.USDAmount()));
        });
        viewModel.TransactionModel().utilizationAmount(parseFloat(total).toFixed(2));
        var sRounding = GetRounding(parseFloat(total), self.ParameterRounding());
        if (sRounding != "NaN") {
            viewModel.Rounding(sRounding);
            viewModel.TransactionModel().Rounding(sRounding);
        } else {
            viewModel.Rounding(0);
            viewModel.TransactionModel().Rounding(0);
        }
    }

    function setTotalUtilizeTMO() {
        var total = 0;
        ko.utils.arrayForEach(self.TempSelectedUnderlyingTMO(), function (item) {
            total += parseFloat(ko.utils.unwrapObservable(item.USDAmount()));
        });
        viewModel.TransactionModel().utilizationAmount(parseFloat(total).toFixed(2));
        var sRounding = GetRounding(parseFloat(total), self.ParameterRounding());
        if (sRounding != "NaN") {
            viewModel.Rounding(sRounding);
            viewModel.TransactionModel().Rounding(sRounding);
        } else {
            viewModel.Rounding(0);
            viewModel.TransactionModel().Rounding(0);
        }
    }

    function GetRounding(totalUnderlying, parameterRounding) {
        if (totalUnderlying != null && parameterRounding != null) {
            var rounding = totalUnderlying % parameterRounding;
            if (rounding != null && rounding != 0) {
                return rounding = parseFloat(parameterRounding - rounding).toFixed(2);
            }
        }
        return 0;
    }

    //add validation if statement B is choose then underlying doc select "tanpa underlying" by henggar wicaksana
    function IsValidDataUnderlying() {
        var isStatus = true;
        var tmpData = [];
        if (viewModel.CustomerUnderlyings() != null && viewModel.CustomerUnderlyings().length > 0 && (viewModel.IsFxTransaction() || (viewModel.IsFxTransactionToIDR() && !viewModel.IsNoThresholdValue()))) {
            for (var i = 0; i < viewModel.CustomerUnderlyings().length; i++) {
                if (viewModel.CustomerUnderlyings()[i].IsEnable == true) {
                    tmpData.push(viewModel.CustomerUnderlyings()[i]);
                }
            }
            //console.log(tmpData);
            for (var i = 0; i < tmpData.length; i++) {
                if (tmpData[i].StatementLetter.Name == 'Statement B') {
                    if (viewModel.Selected().UnderlyingDoc() == Const_Underlying.SelectedUnderlying) {
                        return false;
                    }
                }
            }
        }
        return isStatus;
    }
    //end wicak

    function SetStatementA(isJointAccount, accountNumber) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetStatementA/" + cifData + "/" + isJointAccount + "/" + accountNumber,
            data: null, //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            }, success: function (data) {
                self.DynamicStatementLetter_u([]);
                if (data.IsStatementA) {
                    self.StatementLetter_u(new StatementLetterModel(CONST_STATEMENT.StatementB_ID, 'Statement B'));
                    ko.utils.arrayForEach(self.ddlStatementLetter_u(), function (item) {
                        if (item.ID != CONST_STATEMENT.StatementA_ID) {
                            self.DynamicStatementLetter_u.push(item);
                        }
                    });
                }
                else {
                    self.DynamicStatementLetter_u(self.ddlStatementLetter_u());
                    //self.IsStatementA(true);
                }
            }
        });
    }
    function UnCheckUnderlyingTable() {
        self.TempSelectedUnderlying([]);
        var dataRows = [];
        ko.utils.arrayForEach(self.CustomerUnderlyings(), function (item) {
            item.IsEnable = false;
            dataRows.push(item);
        });
        self.CustomerUnderlyings([]);
        self.CustomerUnderlyings(dataRows);
    }
    function setRefID() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetRefID/" + cifData,
            data: null, //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            }, success: function (data) {
                self.ReferenceNumber_u(customerNameData + '-' + data.ID);
            }
        });
    }

    function SaveDataFile() {

        SetMappingAttachment();
        //Ajax call to insert the CustomerUnderlyings
        CustomerUnderlyingFile.FileName = DocumentModels().FileName;
        CustomerUnderlyingFile.DocumentPath = DocumentModels().DocumentPath;

        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlyingfile,
            data: ko.toJSON(CustomerUnderlyingFile), //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    // send notification
                    ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                    // hide current popup window
                    $("#modal-form-Attach").modal('hide');
                    viewModel.IsUploading(false);

                    if (viewModel.TransactionModel().IsIPETMO() == true) {

                        CustomerUnderlyingFile.DocumentType = ko.utils.arrayFirst(self.ddlDocumentType_a(), function (item) {
                            return item.ID == self.DocumentType_a().ID();
                        });
                        CustomerUnderlyingFile.DocumentPurpose = ko.utils.arrayFirst(self.ddlDocumentPurpose_a(), function (item) {
                            return item.ID == self.DocumentPurpose_a().ID();
                        });

                        var doc = {
                            ID: data,
                            Type: CustomerUnderlyingFile.DocumentType,
                            Purpose: CustomerUnderlyingFile.DocumentPurpose,
                            FileName: CustomerUnderlyingFile.FileName,
                            DocumentPath: viewModel.DocumentPath_a(),
                            IsNewDocument: true,
                            LastModifiedDate: new Date(),
                            LastModifiedBy: null,
                            UnderlyingID: 0
                        };

                        self.Documents.push(doc);

                        var documentStatementLetter = ko.utils.arrayFilter(self.Documents(), function (items) {
                            return items.Purpose.Name == "Statement Letter";
                        });
                        var documentUnderlying = ko.utils.arrayFilter(self.Documents(), function (itemu) {
                            return itemu.Purpose.Name == "Underlying";
                        });
                        var documentInstruction = ko.utils.arrayFilter(self.Documents(), function (itemi) {
                            return itemi.Purpose.Name == "Instruction";
                        });
                        if (documentStatementLetter != "") {
                            viewModel.TransactionModel().StatementLetter(true);
                        }
                        if (documentUnderlying != "") {
                            viewModel.TransactionModel().Underlying(true);
                        }
                        if (documentInstruction != "") {
                            viewModel.TransactionModel().Instruction(true);
                        }
                    }


                    // refresh data
                    viewModel.GetDataUnderlying();
                    GetDataAttachFile();
                } else {
                    viewModel.IsUploading(false);
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                viewModel.IsUploading(false);
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });


    }

    //#region Threshold & Validation
    function GetThreshold(callback) {
        var paramhreshold = {
            TzRef: viewModel.TransactionModel().TZNumber() != null ? viewModel.TransactionModel().TZNumber() : "N/A",
            StatementLetterID: viewModel.TempSelectedUnderlying().length > 0 ? viewModel.TempSelectedUnderlying()[0].StatementLetter() : 1,
            ProductTypeID: viewModel.TransactionModel().ProductType().ID,
            DebitCurrencyCode: viewModel.TransactionModel().DebitCurrency().Code,
            TransactionCurrencyCode: viewModel.TransactionModel().Currency().Code,
            IsResident: viewModel.TransactionModel().IsResident(),
            AmountUSD: parseFloat(viewModel.TransactionModel().AmountUSD()),
            CIF: viewModel.TransactionModel().Customer().CIF,
            AccountNumber: viewModel.TransactionModel().IsOtherAccountNumber() ? null : viewModel.TransactionModel().Account().AccountNumber,
            IsJointAccount: viewModel.TransactionModel().IsJointAccount()
        }
        var options = {
            url: api.server + api.url.helper + "/GetThresholdFXValue",
            token: accessToken,
            params: {},
            data: ko.toJSON(paramhreshold)
        };
        Helper.Ajax.Post(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                //viewModel.ThresholdValue(ko.mapping.fromJS(data));
                if (viewModel.TransactionModel().UnderlyingDoc().Code != "998") {
                    if (IsValidationFX(data, paramhreshold.AmountUSD)) {
                        callback();
                    }
                } else {
                    callback();
                }
            }
        }, OnError);
    }
    function GetTotalTransaction() {
        if (viewModel != null) {
            var paramhreshold = {
                ProductTypeID: viewModel.TransactionModel().ProductType().ID,
                DebitCurrencyCode: viewModel.TransactionModel().DebitCurrency().Code,
                TransactionCurrencyCode: viewModel.TransactionModel().Currency().Code,
                IsResident: viewModel.TransactionModel().IsResident(),
                AmountUSD: parseFloat(viewModel.TransactionModel().AmountUSD()),
                CIF: viewModel.TransactionModel().Customer().CIF,
                AccountNumber: viewModel.TransactionModel().IsOtherAccountNumber() ? null : viewModel.TransactionModel().Account().AccountNumber,
                IsJointAccount: viewModel.TransactionModel().IsJointAccount()
            }
            var options = {
                url: api.server + api.url.helper + "/GetTotalTransactionIDRFCY",
                token: accessToken,
                params: {},
                data: ko.toJSON(paramhreshold)
            };
            Helper.Ajax.Post(options, function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    TotalPPUModel.Total.IDRFCYPPU = data.TotalTransaction;
                    viewModel.TransactionModel().TotalTransFX(data.TotalTransaction);
                    TotalPPUModel.Total.UtilizationPPU = data.TotalUtilizationAmount;
                    viewModel.TransactionModel().TotalUtilization(data.TotalUtilizationAmount);

                    //viewModel.Rounding(data.RoundingValue);
                    viewModel.ParameterRounding(data.RoundingValue);
                    viewModel.ThresholdType(data.ThresholdType);
                    viewModel.ThresholdValue(data.ThresholdValue);
                    if (viewModel.TransactionModel().IsIPETMO() == true) {
                        if (viewModel.TransactionModel().Product().ID == ConsProductID.RTGSProductIDCons) {
                            if (viewModel.TransactionModel().AmountUSD() >= 5000000) {
                                viewModel.IsNoThresholdValue(false);
                            } else {
                                viewModel.IsNoThresholdValue(true);
                            }
                        } else {
                            viewModel.IsNoThresholdValue(data.ThresholdValue == 0 ? true : false);
                        }
                    } else {
                        viewModel.IsNoThresholdValue(data.ThresholdValue == 0 ? true : false);
                    }
                    //SetHitThreshold(parseFloat(viewModel.TransactionModel().AmountUSD()));

                }
            }, OnError);
        }
    }
    function GetTotalUnderlyingRounding(rounding) {
        var totalUnderlying;
        if (viewModel.TempSelectedUnderlying() != null && viewModel.TempSelectedUnderlying().length > 0) {
            switch (viewModel.TempSelectedUnderlying()[0].StatementLetter()) {
                case 2:
                    totalUnderlying = parseFloat(viewModel.TransactionModel().utilizationAmount()) + parseFloat(rounding);
                    break;
                default:
                    totalUnderlying = parseFloat(viewModel.TransactionModel().utilizationAmount());
                    break;
            }
        } else {
            totalUnderlying = parseFloat(viewModel.TransactionModel().utilizationAmount());
        }
        return totalUnderlying;
    }
    function IsValidationFX(data, amountUSD) {
        var statementLetterID = viewModel.TempSelectedUnderlying() != null && viewModel.TempSelectedUnderlying().length > 0 ? viewModel.TempSelectedUnderlying()[0].StatementLetter() : null;
        var isStatus = true;
        //var totalUnderlying = GetTotalUnderlyingRounding(data.RoundingValue);
        var totalUnderlying = GetTotalUnderlyingRounding(self.Rounding());
        if ((self.TempSelectedUnderlying() == null || (self.TempSelectedUnderlying() != null && self.TempSelectedUnderlying().length == 0)) && !self.IsNoThresholdValue()) {
            ShowNotification("Form Underlying Warning", "Please select utilize underlying data.", 'gritter-warning', false);
            viewModel.IsEditable(true);
            viewModel.IsUploaded(true);
            return false;
        }
        if ((amountUSD <= totalUnderlying) || statementLetterID == 4 || self.IsNoThresholdValue()) {
            if (data.IsHitThreshold && (self.TempSelectedUnderlying()[0].StatementLetter() == 1 || self.TempSelectedUnderlying()[0].StatementLetter() == 3)) {
                ShowNotification("Form Underlying Warning", "Total IDR->FCY Amount greater than " + data.ThresholdValue + " (USD), Please add statement B underlying", 'gritter-warning', false);
                viewModel.IsEditable(true);
                viewModel.IsUploaded(true);
                isStatus = false;
            }
        } else {
            ShowNotification("Form Underlying Warning", "Total Underlying Amount must be equal greater than Transaction Amount", 'gritter-warning', false);
            viewModel.IsEditable(true);
            viewModel.IsUploaded(true);
            isStatus = false;
        }
        return isStatus;
        /* var IsStatus = true;
         var totalUnderlying = GetTotalUnderlyingRounding(data.RoundingValue);
         if (amountUSD <= totalUnderlying) {
             if ((viewModel.TempSelectedUnderlying()[0].StatementLetter() == 1 || viewModel.TempSelectedUnderlying()[0].StatementLetter() == 3)) {
                 viewModel.IsEditable(true);
                 viewModel.IsUploaded(true);
                 ShowNotification("Form Underlying Warning", "Total IDR->FCY Amount greater than " + data.ThresholdValue + " (USD), Please add statement B underlying", 'gritter-warning', false);
                 IsStatus = false;
             }
         } else {
             viewModel.IsEditable(true);
             viewModel.IsUploaded(true);
             ShowNotification("Form Underlying Warning", "Total Underlying Amount must be equal greater than Transaction Amount", 'gritter-warning', false);
             IsStatus = false;
         }
         return IsStatus; */
    }
    function SetHitThreshold(amountUSD) {
        if (self.IsFxTransactionToIDR()) {
            self.IsHitThreshold(false);
            if (self.ThresholdValue() != null && self.ThresholdValue() != 0) {
                if (self.ThresholdType() == 'T') {
                    if (amountUSD > self.ThresholdValue()) {
                        self.IsHitThreshold(true);
                    } else {
                        // self.AmountModel().TotalAmountsUSD(0);
                    }
                } else {
                    if (TotalPPUModel.Total.IDRFCYPPU > self.ThresholdValue()) {
                        self.IsHitThreshold(true);
                    } else {
                        //self.AmountModel().TotalAmountsUSD(0);
                    }
                }
            }
        }
    }
    //#endregion

    // Function to Read parameter Underlying
    function GetUnderlyingParameters(data) {
        var underlying = data['UnderltyingDoc'];
        for (var i = 0; i < underlying.length; i++) {
            self.ddlUnderlyingDocument_u.push(new UnderlyingDocumentModel2(underlying[i].ID, underlying[i].Name, underlying[i].Code));
        }

        self.ddlDocumentType_u(data['DocType']);
        self.ddlDocumentType_a(data['DocType']);

        self.ddlCurrency_u([]);
        ko.utils.arrayForEach(data['Currency'], function (item) {
            self.ddlCurrency_u.push(new CurrencyModel2(item.ID, item.Code, item.Description));
        });
        //self.ddlCurrency_u(data['Currency']);
        self.ddlDocumentPurpose_a(data['PurposeDoc']);
        ko.utils.arrayForEach(data['StatementLetter'], function (item) {
            if (item.ID == 1 || item.ID == 2) {
                self.ddlStatementLetter_u.push(item);
                self.DynamicStatementLetter_u.push(item);
            }
        });
        //self.ddlStatementLetter_u(data['StatementLetter']);
    }
    //Function to Read All Customers Underlying
    function GetDataUnderlying() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Underlying",
            params: {
                page: self.UnderlyingGridProperties().Page(),
                size: self.UnderlyingGridProperties().Size(),
                sort_column: self.UnderlyingGridProperties().SortColumn(),
                sort_order: self.UnderlyingGridProperties().SortOrder()
            },
            token: accessToken
        };

        self.UnderlyingFilterIsAvailable(true);
        self.UnderlyingFilterIsExpiredDate(true);
        // get filtered columns
        var filters = GetUnderlyingFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        }
    }

    // On success GetData callback
    function OnSuccessGetDataUnderlying(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            var dataunderlying = SetSelectedUnderlying(data.Rows);
            self.CustomerUnderlyings([]);
            ko.utils.arrayForEach(dataunderlying, function (itemd, index) {
                itemd.UtilizeAmountDeal = formatNumber(itemd.UtilizeAmountDeal);
                itemd.TransactionID = index;
            });
            if (viewModel.TransactionModel().IsIPETMOPartial() == true) {
                self.CustomerUnderlyingsTMO(dataunderlying);
            } else {
                self.CustomerUnderlyings(dataunderlying);
            }
            //Tambah Agung
            if (self.TempSelectedUnderlyingTMO() == '') {
                self.CustomerUnderlyingsTMOALL([]);
                if (self.CustomerUnderlyings() != null) {
                    ko.utils.arrayForEach(self.CustomerUnderlyings(), function (item) {
                        if (viewModel.TransactionModel().Customer().Underlyings != null) {
                            ko.utils.arrayForEach(viewModel.TransactionModel().Customer().Underlyings, function (item2) {
                                if (item2.ID == item.ID) {
                                    item.UtilizeAmountDeal = formatNumber(item2.UtilizeAmountDeal);
                                    item.AvailableAmountDeal = item2.AvailableAmountDeal;
                                    //item.AvailableAmount = item.AvailableAmount + item2.UtilizeAmountDeal;
                                    self.TempSelectedUnderlyingTMO.push(new SelectUtillizeModel(item.ID, false, item.UtilizeAmountDeal, item.StatementLetter.ID));
                                } else {
                                    item.UtilizeAmountDeal = formatNumber(item.UtilizeAmountDeal);
                                }
                            });
                        }
                        self.CustomerUnderlyingsTMOALL.push(item);
                    });
                }
            }
            self.UnderlyingGridProperties().Page(data['Page']);
            self.UnderlyingGridProperties().Size(data['Size']);
            self.UnderlyingGridProperties().Total(data['Total']);
            self.UnderlyingGridProperties().TotalPages(Math.ceil(self.UnderlyingGridProperties().Total() / self.UnderlyingGridProperties().Size()));
            //self.TempSelectedUnderlying([]);
            if (self.TempSelectedUnderlying() == "") {
                viewModel.TransactionModel().utilizationAmount(0.00);
            }
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    //Function to Read All Customers Underlying File
    function GetDataAttachFile() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlyingfile,
            params: {
                page: self.AttachGridProperties().Page(),
                size: self.AttachGridProperties().Size(),
                sort_column: self.AttachGridProperties().SortColumn(),
                sort_order: self.AttachGridProperties().SortOrder()
            },
            token: accessToken
        };
        self.AttachFilterAccountNumber(self.UnderlyingFilterAccountNumber());
        // get filtered columns
        var filters = GetAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        }
    }

    // On success GetData callback
    function OnSuccessGetDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerUnderlyingFiles(data.Rows);

            self.AttachGridProperties().Page(data['Page']);
            self.AttachGridProperties().Size(data['Size']);
            self.AttachGridProperties().Total(data['Total']);
            self.AttachGridProperties().TotalPages(Math.ceil(self.AttachGridProperties().Total() / self.AttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }


    //Function to Read All Customers Underlying for Attach File
    function GetDataUnderlyingAttach() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Attach",
            params: {
                page: self.UnderlyingAttachGridProperties().Page(),
                size: self.UnderlyingAttachGridProperties().Size(),
                sort_column: self.UnderlyingAttachGridProperties().SortColumn(),
                sort_order: self.UnderlyingAttachGridProperties().SortOrder()
            },
            token: accessToken
        };
        self.UnderlyingAttachFilterAccountNumber(self.UnderlyingFilterAccountNumber());
        // get filtered columns
        var filters = GetUnderlyingAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingDataAttachFile, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingDataAttachFile, OnError, OnAlways);
        }
    }

    // On success GetData callback
    function OnSuccessGetUnderlyingDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {

            for (var i = 0 ; i < data.Rows.length; i++) {
                var selected = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (item) {
                    return item.ID == data.Rows[i].ID;
                });
                if (selected != null) {
                    data.Rows[i].IsSelectedAttach = true;
                }
                else {
                    data.Rows[i].IsSelectedAttach = false;
                }
            }
            self.CustomerAttachUnderlyings(data.Rows);

            self.UnderlyingAttachGridProperties().Page(data['Page']);
            self.UnderlyingAttachGridProperties().Size(data['Size']);
            self.UnderlyingAttachGridProperties().Total(data['Total']);
            self.UnderlyingAttachGridProperties().TotalPages(Math.ceil(self.UnderlyingAttachGridProperties().Total() / self.UnderlyingAttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    //Function to Read All Customers Underlying Proforma
    function GetDataUnderlyingProforma() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Proforma",
            params: {
                page: self.UnderlyingProformaGridProperties().Page(),
                size: self.UnderlyingProformaGridProperties().Size(),
                sort_column: self.UnderlyingProformaGridProperties().SortColumn(),
                sort_order: self.UnderlyingProformaGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetUnderlyingProformaFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingProforma, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingProforma, OnError, OnAlways);
        }
    }

    //Function to Read All Customers Bulk Underlying // add 2015.03.09
    function GetDataBulkUnderlying() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Bulk",
            params: {
                page: self.UnderlyingBulkGridProperties().Page(),
                size: self.UnderlyingBulkGridProperties().Size(),
                sort_column: self.UnderlyingBulkGridProperties().SortColumn(),
                sort_order: self.UnderlyingBulkGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns

        var filters = GetBulkUnderlyingFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingBulk, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingBulk, OnError, OnAlways);
        }
    }

    // On success GetData callback
    function OnSuccessGetUnderlyingProforma(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            SetSelectedProforma(data.Rows);

            self.CustomerUnderlyingProformas(data.Rows);

            self.UnderlyingProformaGridProperties().Page(data['Page']);
            self.UnderlyingProformaGridProperties().Size(data['Size']);
            self.UnderlyingProformaGridProperties().Total(data['Total']);
            self.UnderlyingProformaGridProperties().TotalPages(Math.ceil(self.UnderlyingProformaGridProperties().Total() / self.UnderlyingProformaGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback // Add 2015.03.09
    function OnSuccessGetUnderlyingBulk(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            var databulk = SetSelectedBulk(data.Rows);

            self.CustomerBulkUnderlyings(databulk);

            self.UnderlyingBulkGridProperties().Page(data['Page']);
            self.UnderlyingBulkGridProperties().Size(data['Size']);
            self.UnderlyingBulkGridProperties().Total(data['Total']);
            self.UnderlyingBulkGridProperties().TotalPages(Math.ceil(self.UnderlyingBulkGridProperties().Total() / self.UnderlyingBulkGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    function GetSelectedUtilizeID() {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });

        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SelectedUtilizeID",
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON(filters),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlying([]);
                    for (var i = 0; i < data.length; i++) {
                        self.TempSelectedUnderlying.push(new SelectUtillizeModel(data[i].ID, false, 0, data[i].StatementLetter.ID));
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetSelectedProformaID(callback) {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.ProformaFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.ProformaFilterIsSelected() });
        if (self.ProformaFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.ProformaFilterUnderlyingDocument() });
        if (self.ProformaFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.ProformaFilterDocumentType() });

        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SelectedProformaID",
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON(filters),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlyingProforma([]);
                    for (var i = 0; i < data.length; i++) {
                        self.TempSelectedUnderlyingProforma.push({ ID: data[i].ID });
                    }
                    if (callback != null) {
                        callback();
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    // Function to get All selected Bulk Underlying // add 2015.03.09
    function GetSelectedBulkID(callback) {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: self.CIF_u() });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.BulkFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.BulkFilterIsSelected() });
        if (self.BulkFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.BulkFilterUnderlyingDocument() });
        if (self.BulkFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.BulkFilterDocumentType() });


        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SelectedBulkID",
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON(filters),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlyingBulk([]);
                    for (var i = 0; i < data.length; i++) {
                        self.TempSelectedUnderlyingBulk.push({ ID: data[i].ID, Amount: data[i].Amount });
                    }
                    if (callback != null) {
                        callback();
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    // Get filtered columns value
    function GetUnderlyingFilteredColumns() {
        // define filter

        var filters = [];
        self.CIF_u(cifData);

        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingFilterIsSelected() });
        if (self.UnderlyingFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingFilterUnderlyingDocument() });
        if (self.UnderlyingFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingFilterDocumentType() });
        if (self.UnderlyingFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingFilterCurrency() });
        if (self.UnderlyingFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingFilterStatementLetter() });
        if (self.UnderlyingFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingFilterAmount() });
        if (self.UnderlyingFilterAvailableAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingFilterAvailableAmount() });
        if (self.UnderlyingFilterAttachmentNo() != "") filters.push({ Field: 'AttachmentNo', Value: self.UnderlyingFilterAttachmentNo() });
        if (self.UnderlyingFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingFilterDateOfUnderlying() });
        if (self.UnderlyingFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingFilterExpiredDate() });
        if (self.UnderlyingFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingFilterReferenceNumber() });
        if (self.UnderlyingFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingFilterSupplierName() });
        if (self.UnderlyingFilterIsAvailable() != "") filters.push({ Field: 'IsAvailable', Value: self.UnderlyingFilterIsAvailable() });
        if (self.UnderlyingFilterIsExpiredDate() != "") filters.push({ Field: 'IsExpiredDate', Value: self.UnderlyingFilterIsExpiredDate() });
        if (self.UnderlyingFilterIsSelectedBulk() == false) filters.push({ Field: 'IsSelectedBulk', Value: self.UnderlyingFilterIsSelectedBulk() });
        if (self.UnderlyingFilterAccountNumber() != "") filters.push({ Field: 'AccountNumber', Value: self.UnderlyingFilterAccountNumber() });

        filters.push({ Field: 'StatusShowData', Value: self.UnderlyingFilterShow() });
        return filters;
    };

    //Tambah Agung
    function GetInstructionFilteredColumns() {
        var filters = [];
        self.InstructionFilterIsSelected(true);
        //if (self.InstructionFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.InstructionFilterIsSelected() });
        if (self.InstructionFilterApplicationID() != "") filters.push({ Field: 'ApplicationID', Value: self.InstructionFilterApplicationID() });
        if (self.InstructionFilterFileName() != "") filters.push({ Field: 'FileName', Value: self.InstructionFilterFileName() });
        if (self.InstructionFilterPurposeofDoc() != "") filters.push({ Field: 'PurposeofDoc', Value: self.InstructionFilterPurposeofDoc() });
        if (self.InstructionFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.InstructionFilterDocumentType() });
        if (self.InstructionFilterLastModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.InstructionFilterLastModifiedDate() });

        filters.push({ Field: 'StatusShowData', Value: self.InstructionFilterShow() });
        return filters;
    }
    //End
    // Get filtered columns value

    function GetUnderlyingAttachFilteredColumns() {
        var filters = [];
        self.UnderlyingFilterIsSelected(true); // flag for get all datas
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingAttachFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingAttachFilterIsSelected() });
        if (self.UnderlyingAttachFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingAttachFilterUnderlyingDocument() });
        if (self.UnderlyingAttachFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingFilterDocumentType() });
        if (self.UnderlyingAttachFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingFilterCurrency() });
        if (self.UnderlyingAttachFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingFilterStatementLetter() });
        if (self.UnderlyingAttachFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingFilterAmount() });
        if (self.UnderlyingAttachFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingFilterDateOfUnderlying() });
        if (self.UnderlyingAttachFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingFilterExpiredDate() });
        if (self.UnderlyingAttachFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingFilterReferenceNumber() });
        if (self.UnderlyingAttachFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingFilterSupplierName() });
        if (self.UnderlyingAttachFilterAccountNumber() != "") filters.push({ Field: 'AccountNumber', Value: self.UnderlyingAttachFilterAccountNumber() });

        filters.push({ Field: 'StatusShowData', Value: self.UnderlyingFilterShow() });
        return filters;
    }

    // Get filtered columns value
    function GetAttachFilteredColumns() {
        // define filter

        var filters = [];
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.AttachFilterFileName() != "") filters.push({ Field: 'FileName', Value: self.AttachFilterFileName() });
        if (self.AttachFilterDocumentPurpose() != "") filters.push({ Field: 'DocumentPurpose', Value: self.AttachFilterDocumentPurpose() });
        if (self.AttachFilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.AttachFilterModifiedDate() });
        if (self.AttachFilterDocumentRefNumber() != "") filters.push({ Field: 'DocumentRefNumber', Value: self.AttachFilterDocumentRefNumber() });
        if (self.AttachFilterAccountNumber() != "") filters.push({ Field: 'AccountNumber', Value: self.AttachFilterAccountNumber() });

        filters.push({ Field: 'StatusShowData', Value: self.UnderlyingFilterShow() });
        return filters;
    };

    // Get filtered columns value
    function GetUnderlyingProformaFilteredColumns() {
        // define filter
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.ProformaFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.ProformaFilterIsSelected() });
        if (self.ProformaFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.ProformaFilterUnderlyingDocument() });
        if (self.ProformaFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.ProformaFilterDocumentType() });
        if (self.ProformaFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.ProformaFilterCurrency() });
        if (self.ProformaFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.ProformaFilterStatementLetter() });
        if (self.ProformaFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.ProformaFilterAmount() });
        if (self.ProformaFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.ProformaFilterDateOfUnderlying() });
        if (self.ProformaFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.ProformaFilterExpiredDate() });
        if (self.ProformaFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.ProformaFilterReferenceNumber() });
        if (self.ProformaFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.ProformaFilterSupplierName() });

        return filters;
    };

    // Get filtered columns value // add 2015.03.09
    function GetBulkUnderlyingFilteredColumns() {
        // define filter
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.BulkFilterCurrency(self.Currency_u().Code() != "" || self.Currency_u().Code() != null ? self.Currency_u().Code() : "xyz");
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: self.CIF_u() });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.BulkFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.BulkFilterIsSelected() });
        if (self.BulkFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.BulkFilterUnderlyingDocument() });
        if (self.BulkFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.BulkFilterDocumentType() });
        if (self.BulkFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.BulkFilterCurrency() });
        if (self.BulkFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.BulkFilterStatementLetter() });
        if (self.BulkFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.BulkFilterAmount() });
        if (self.BulkFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.BulkFilterDateOfUnderlying() });
        if (self.BulkFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.BulkFilterExpiredDate() });
        if (self.BulkFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.BulkFilterReferenceNumber() });
        if (self.BulkFilterInvoiceNumber() != "") filters.push({ Field: 'InvoiceNumber', Value: self.BulkFilterInvoiceNumber() });
        if (self.BulkFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.BulkFilterSupplierName() });

        return filters;
    };

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        if (viewModel != undefined) $box.trigger('reloaded.ace.widget');
    }

    //--------------------------- set Underlying Function & variable end
    self.GetCustomerData = function (cif, acc) {
        $.ajax({
            //type: "DELETE",
            url: api.server + api.url.customer + "/" + cif,
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TransactionModel().Customer(data);
                    self.Selected().Account(acc);
                    self.Selected().BizSegment(data.BizSegment.ID);
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    //Declare observable which will be bind with UI
    self.ProductTitle = ko.observable();
    self.ProductID = ko.observable();
    self.TransactionModel = ko.observable(TransactionModel);
    self.Products = ko.observableArray([]);
    self.DynamicProducts = ko.observableArray([]);
    self.Customer = ko.observableArray([]);
    self.IsDraft = ko.observable(false);
    self.IsNewCustomer = ko.observable(false);
    self.SetCustomerAutoComplete();
    //add by fandi
    self.IsNewDocument = ko.observable(false);
    //end

    //Andi, 6 October 2015
    self.IsDraftForm = ko.observable();
    self.IsHO = ko.observable();
    self.IsJakartaBranch = ko.observable();
    self.IsUpcountryBranch = ko.observable();
    self.BranchName = ko.observable();
    self.IsCustomerCenter = ko.observable(false);
    self.ApplicationIDColl = ko.observableArray([]);
    self.RetIDColl = ko.observableArray([]);
    self.SelectedTransactionType = ko.observable(0);
    self.SelectedFDRemarks = ko.observable(0);
    self.FinishBulk = function () {
        window.location = "/home";
    }
    self.CutOffTime = ko.observable();
    self.IsCuttOff = ko.observable(false);
    self.strCuttOffTime = ko.observable('');
    self.NotifHeader = ko.observable();
    self.NotifTitle = ko.observable();
    self.NotifMessage = ko.observable();
    self.NotifOK = function () {
        if (viewModel.IsCuttOff() == true) {
            window.location = "/home/bring-up";
        }
        else
            window.location = "/home";
    }
    self.AddFDTransaction = function () {
        if (self.Selected().Channel() == undefined) {
            ShowNotification("Attention", "Please input Mandotary Field", 'gritter-warning', true);
            return false;
        }
        self.IsNewDataFD(true);
        self.Selected().Currency(null);
        self.Selected().TransactionType(null);
        self.Selected().FDRemarks(null);
        self.Selected().TenorFD(null);
        //self.Selected().Channel(null);
        self.FDModel().FDAccNumber(null);
        self.FDModel().Currency(null);
        self.FDModel().Amount(null);
        self.FDModel().CreditAccNumber(null);
        self.FDModel().DebitAccNumber(null);
        self.FDModel().InterestRate(null);
        self.FDModel().Tenor(null);
        self.FDModel().ValueDate(null);
        self.FDModel().MaturityDate(null);
        self.FDModel().Remarks(null);
        self.FDModel().FDBankName(null);
        self.FDModel().Documents([]);
        self.FDRemarks(null);
        self.FDModel().DocsComplete(null);
        self.FDModel().AllInRate(null);
        self.FDModel().FTPRate(null);
        $("#modal-form-fdaddtrans").modal('show');
        GetFDParameter();
        SetDocumentPathFDStyle();
        $("#modal-form-fdaddtrans").appendTo("#frmFD");
        $("#modal-form-uploadFD").appendTo("#frmFD");
        ClearError();
    };
    self.RemoveFDTransaction = function (data) {
        self.FDTransaction.remove(data);
    };
    //Agung
    self.ddlAccountListPOAs = ko.observableArray([]);
    self.IsNewDataUT = ko.observable();
    self.MaxRisk = ko.observable();
    self.MinRisk = ko.observable();
    self.IsUTFNA = ko.observable();
    self.IsUTModify = ko.observable();
    self.IsUTNew = ko.observable();
    self.IsUTClosed = ko.observable();
    self.IsUTSingle = ko.observable();
    self.IsUTJoin = ko.observable();
    self.UTJoinFNA = ko.observableArray([]);
    self.UTJoinNonFNA = ko.observableArray([]);
    self.SubcriptionColl = ko.observableArray([]);
    self.RedemptionColl = ko.observableArray([]);
    self.SwitchingColl = ko.observableArray([]);
    self.MutualFundColl = ko.observableArray([]);
    self.UTIndexUpdate = ko.observable();
    self.SelectedUTFNACore = ko.observable(null);
    self.SelectedUTFunctionType = ko.observable(null);
    self.SelectedUTAccountType = ko.observable(null);
    self.SelectedUTTransType = ko.observable(null);
    //henggar 13032017 
    self.IsAccountType = ko.observable();
    self.IsFNACore = ko.observable();
    self.IsSolID = ko.observable();
    self.IsOperativeAcc = ko.observable();
    self.IsInvestmentID = ko.observable();
    //end
    self.OnUTChange = function () {
        var selFNA = viewModel.Selected().FNACore();
        var selFunc = viewModel.Selected().FunctionType();
        var selAcc = viewModel.Selected().AccountType();

        self.IsUTNew(true);
        self.IsUTModify(false);
        self.IsUTClosed(false);
        self.IsUTSingle(false);
        self.IsUTJoin(false);
        self.IsUTFNA(false);
        self.IsInvestmentID(false);

        switch (selFNA) {
            case ConsUTPar.fnaYes:
                self.IsUTFNA(true);
                break;
            case ConsUTPar.fnaNo:
                self.IsUTFNA(false);
                break;
            default:
                break;
        }

        switch (selFunc) {
            case ConsUTPar.funcAdd:
                self.IsUTNew(true);
                self.IsUTModify(false);
                self.IsUTClosed(false);
                if (viewModel.IsDraftForm() == false) {
                    self.UTJoinFNA([]);
                    self.UTJoinNonFNA([]);
                }
                //self.TransactionUTModel().Investment(null);
                //add henggar 13032017 
                self.IsAccountType(true);
                $('#accounttype').data({ ruleRequired: true });
                self.IsFNACore(true);
                $('#fnacore').data({ ruleRequired: true });
                self.IsSolID(true);
                $('#solidut').data({ ruleRequired: true });
                self.IsOperativeAcc(true);
                $('#operativeaccount').data({ ruleRequired: true });
                $('#investmentid').data({ ruleRequired: false });
                self.IsInvestmentID(false);
                //end
                break;
            case ConsUTPar.funcModify:
                self.IsUTNew(false);
                self.IsUTModify(true);
                self.IsUTClosed(false);
                if (viewModel.IsDraftForm() == false) {
                    self.UTJoinFNA([]);
                    self.UTJoinNonFNA([]);
                }
                //self.TransactionUTModel().Investment(null);
                //add henggar 13032017 
                self.IsAccountType(true);
                $('#accounttype').data({ ruleRequired: true });
                self.IsFNACore(true);
                $('#fnacore').data({ ruleRequired: true });
                self.IsSolID(true);
                $('#solidut').data({ ruleRequired: true });
                self.IsOperativeAcc(true);
                $('#operativeaccount').data({ ruleRequired: false });
                $('#investmentid').data({ ruleRequired: true });
                self.IsInvestmentID(true);
                //end
                break;
            case ConsUTPar.funcClose:
                self.IsUTNew(false);
                self.IsUTModify(false);
                self.IsUTClosed(true);
                if (viewModel.IsDraftForm() == false) {
                    self.UTJoinFNA([]);
                    self.UTJoinNonFNA([]);
                }
                //self.TransactionUTModel().Investment(null);
                //add henggar 13032017 
                self.IsAccountType(false);
                $('#accounttype').data({ ruleRequired: false });
                self.IsFNACore(false);
                $('#fnacore').data({ ruleRequired: false });
                self.IsSolID(false);
                $('#solidut').data({ ruleRequired: false });
                self.IsOperativeAcc(false);
                $('#operativeaccount').data({ ruleRequired: false });
                $('#investmentid').data({ ruleRequired: true });
                self.IsInvestmentID(true);
                viewModel.TransactionUTModel().AccountType(null);
                viewModel.TransactionUTModel().FNACore(null);
                //end
                break;
            default:
                break;
        }

        switch (selAcc) {
            case ConsUTPar.accSingle:
                self.IsUTSingle(true);
                self.IsUTJoin(false);
                break;
            case ConsUTPar.accJoin:
                self.IsUTSingle(false);
                self.IsUTJoin(true);
                break;
            default:
                break;
        }
    };

    self.AddJoin = function () {
        var selFNA = viewModel.Selected().FNACore();
        self.IsNewDataUT(true);
        self.TransactionUTModel().CustomerJoin('');
        self.TransactionUTModel().CustomerRiskEffectiveDateJoin(null);
        self.TransactionUTModel().RiskScoreJoin(null);
        self.TransactionUTModel().RiskProfileExpiryDateJoin(null);
        self.TransactionUTModel().SolIDJoin(null);
        if (selFNA == ConsUTPar.fnaYes) {
            $("#modal-form-fdaddjoinfna").modal('show');
        }
        else if (selFNA == ConsUTPar.fnaNo) {
            $("#modal-form-fdaddjoinnonfna").modal('show');
        }
    };
    self.GetSelectedRowUTFna = function (index, data) {
        self.IsNewDataUT(false);
        self.UTIndexUpdate(index);
        self.IsNewDataUT(false);
        self.TransactionUTModel().CustomerJoin(data.CustomerJoin);
        self.TransactionUTModel().CustomerRiskEffectiveDateJoin(viewModel.LocalDate(data.CustomerRiskEffectiveDateJoin, true, false));
        self.TransactionUTModel().RiskScoreJoin(data.RiskScoreJoin);
        self.TransactionUTModel().RiskProfileExpiryDateJoin(viewModel.LocalDate(data.RiskProfileExpiryDateJoin, true, false));
        $("#modal-form-fdaddjoinfna").modal('show');
    };
    self.GetSelectedRowUTNonFna = function (index, data) {
        self.IsNewDataUT(false);
        self.UTIndexUpdate(index);
        self.IsNewDataUT(false);
        self.TransactionUTModel().CustomerJoin(data.CustomerJoin);
        self.TransactionUTModel().SolIDJoin(data.SolIDJoin);
        self.Selected().Join(data.Join.ID);
        $("#modal-form-fdaddjoinnonfna").modal('show');
    };
    self.AddJoinFna = function () {
        if (self.TransactionUTModel().CustomerJoin() == null || self.TransactionUTModel().CustomerJoin() == '') {
            ShowNotification("Attention", "Please input customer join", 'gritter-warning', true);
            return false;
        }
        if (self.TransactionUTModel().RiskScoreJoin() == null) {
            ShowNotification("Attention", "Please input risk score", 'gritter-warning', true);
            return false;
        }
        else {
            var minRisk = viewModel.MinRisk()[0].Name;
            var maxRisk = viewModel.MaxRisk()[0].Name;
            if (viewModel.TransactionUTModel().RiskScoreJoin() > Number(maxRisk) || viewModel.TransactionUTModel().RiskScoreJoin() < Number(minRisk)) {
                ShowNotification("Attention", "Maximum Risk = " + maxRisk + ", Minimum Risk=" + minRisk + "", 'gritter-warning', true);
                return false;
            }
        }
        if (self.TransactionUTModel().RiskProfileExpiryDateJoin() == null) {
            ShowNotification("Attention", "Please input risk profile expiry date", 'gritter-warning', true);
            return false;
        }
        if (self.TransactionUTModel().CustomerRiskEffectiveDateJoin() == null) {
            ShowNotification("Attention", "Please input risk efective date", 'gritter-warning', true);
            return false;
        }
        if (self.TransactionUTModel().CustomerRiskEffectiveDateJoin() == null) {
            ShowNotification("Attention", "Please input risk efective date", 'gritter-warning', true);
            return false;
        }
        if (viewModel.UTJoinFNA() != "") {
            if (viewModel.UTJoinFNA()[0].Join.ID == viewModel.Selected().Join()) {
                var UTFNA = {
                    CustomerJoin: self.TransactionUTModel().CustomerJoin(),
                    CustomerRiskEffectiveDateJoin: self.TransactionUTModel().CustomerRiskEffectiveDateJoin(),
                    RiskScoreJoin: self.TransactionUTModel().RiskScoreJoin(),
                    RiskProfileExpiryDateJoin: self.TransactionUTModel().RiskProfileExpiryDateJoin(),
                    SolIDJoin: null,
                    JoinOrder: 0,
                    Join: self.TransactionUTModel().Join()
                };
                self.UTJoinFNA.push(UTFNA);
            } else {
                ShowNotification("Attention", "Join Type must be Equal", 'gritter-warning', true);
                return false;
            }

        } else {
            var UTFNA = {
                CustomerJoin: self.TransactionUTModel().CustomerJoin(),
                CustomerRiskEffectiveDateJoin: self.TransactionUTModel().CustomerRiskEffectiveDateJoin(),
                RiskScoreJoin: self.TransactionUTModel().RiskScoreJoin(),
                RiskProfileExpiryDateJoin: self.TransactionUTModel().RiskProfileExpiryDateJoin(),
                SolIDJoin: null,
                JoinOrder: 0,
                Join: self.TransactionUTModel().Join()
            };
            self.UTJoinFNA.push(UTFNA);
        }
        $('.remove').click();
        $("#modal-form-fdaddjoinfna").modal('hide');
    }
    self.AddJoinNonFna = function () {
        if (self.TransactionUTModel().CustomerJoin() == null || self.TransactionUTModel().CustomerJoin() == '') {
            ShowNotification("Attention", "Please input customer join", 'gritter-warning', true);
            return false;
        }
        if (self.TransactionUTModel().SolIDJoin() == null) {
            ShowNotification("Attention", "Please input SOL ID", 'gritter-warning', true);
            return false;
        }
        if (viewModel.UTJoinNonFNA() != "") {
            if (viewModel.UTJoinNonFNA()[0].Join.ID == viewModel.Selected().Join()) {
                var UTNONFNA = {
                    CustomerJoin: self.TransactionUTModel().CustomerJoin(),
                    CustomerRiskEffectiveDateJoin: null,
                    RiskScoreJoin: null,
                    RiskProfileExpiryDateJoin: null,
                    SolIDJoin: self.TransactionUTModel().SolIDJoin(),
                    JoinOrder: 0,
                    Join: self.TransactionUTModel().Join()

                };
                self.UTJoinNonFNA.push(UTNONFNA);
            } else {
                ShowNotification("Attention", "Join Type must be Equal", 'gritter-warning', true);
                return false;
            }
        } else {
            var UTNONFNA = {
                CustomerJoin: self.TransactionUTModel().CustomerJoin(),
                CustomerRiskEffectiveDateJoin: null,
                RiskScoreJoin: null,
                RiskProfileExpiryDateJoin: null,
                SolIDJoin: self.TransactionUTModel().SolIDJoin(),
                JoinOrder: 0,
                Join: self.TransactionUTModel().Join()

            };
            self.UTJoinNonFNA.push(UTNONFNA);
        }
        $('.remove').click();
        $("#modal-form-fdaddjoinnonfna").modal('hide');
    }

    self.UpdateJoinFna = function () {
        var UTNONFN = {};
        if (viewModel.UTJoinFNA().length == 1) {
            UTFNA = {
                CustomerJoin: self.TransactionUTModel().CustomerJoin(),
                CustomerRiskEffectiveDateJoin: self.TransactionUTModel().CustomerRiskEffectiveDateJoin(),
                RiskScoreJoin: self.TransactionUTModel().RiskScoreJoin(),
                RiskProfileExpiryDateJoin: self.TransactionUTModel().RiskProfileExpiryDateJoin(),
                SolIDJoin: null,
                JoinOrder: 0,
                Join: self.TransactionUTModel().Join()
            };
            viewModel.UTJoinFNA.replace(viewModel.UTJoinFNA()[self.UTIndexUpdate()], UTFNA);
        } else {
            if (viewModel.UTJoinFNA()[0].Join.ID == viewModel.Selected().Join()) {
                UTFNA = {
                    CustomerJoin: self.TransactionUTModel().CustomerJoin(),
                    CustomerRiskEffectiveDateJoin: self.TransactionUTModel().CustomerRiskEffectiveDateJoin(),
                    RiskScoreJoin: self.TransactionUTModel().RiskScoreJoin(),
                    RiskProfileExpiryDateJoin: self.TransactionUTModel().RiskProfileExpiryDateJoin(),
                    SolIDJoin: null,
                    JoinOrder: 0,
                    Join: self.TransactionUTModel().Join()
                };
                viewModel.UTJoinFNA.replace(viewModel.UTJoinFNA()[self.UTIndexUpdate()], UTFNA);
            } else {
                ShowNotification("Attention", "Join Type must be Equal", 'gritter-warning', true);
                return false;
            }
        }
        $('.remove').click();
        $("#modal-form-fdaddjoinfna").modal('hide');
    }
    self.UpdateJoinNonFna = function () {
        var UTNONFN = {};
        if (viewModel.UTJoinNonFNA().length == 1) {
            UTNONFNA = {
                CustomerJoin: self.TransactionUTModel().CustomerJoin(),
                CustomerRiskEffectiveDateJoin: null,
                RiskScoreJoin: null,
                RiskProfileExpiryDateJoin: null,
                SolIDJoin: self.TransactionUTModel().SolIDJoin(),
                JoinOrder: 0,
                Join: self.TransactionUTModel().Join()
            };
            viewModel.UTJoinNonFNA.replace(viewModel.UTJoinNonFNA()[self.UTIndexUpdate()], UTNONFNA);
        } else {
            if (viewModel.UTJoinFNA()[0].Join.ID == viewModel.Selected().Join()) {
                UTNONFNA = {
                    CustomerJoin: self.TransactionUTModel().CustomerJoin(),
                    CustomerRiskEffectiveDateJoin: null,
                    RiskScoreJoin: null,
                    RiskProfileExpiryDateJoin: null,
                    SolIDJoin: self.TransactionUTModel().SolIDJoin(),
                    JoinOrder: 0,
                    Join: self.TransactionUTModel().Join()
                };
                viewModel.UTJoinNonFNA.replace(viewModel.UTJoinNonFNA()[self.UTIndexUpdate()], UTNONFNA);
            } else {
                ShowNotification("Attention", "Join Type must be Equal", 'gritter-warning', true);
                return false;
            }
        }
        $('.remove').click();
        $("#modal-form-fdaddjoinnonfna").modal('hide');
    }
    self.RemoveJoinFNA = function (data) { self.UTJoinFNA.remove(data); }
    self.RemoveJoinNONFNA = function (data) { self.UTJoinNonFNA.remove(data); }
    self.RemoveMutual = function (data) { self.MutualFundColl.remove(data); }
    self.RemoveSubcription = function (data) { self.SubcriptionColl.remove(data); }
    self.RemoveSwitching = function (data) { self.SwitchingColl.remove(data); }
    self.RemoveRedemption = function (data) { self.RedemptionColl.remove(data); }

    self.AddMutualFund = function () {
        if (self.IsSP() == true) {
            if (viewModel.TransactionUTModel().MutualFundList() == null || viewModel.TransactionUTModel().MutualFundList() == '') {
                ShowNotification("Attention", "Please input Mutual Fund", 'gritter-warning', true);
                return;
            }
            if (viewModel.Currencies() == null || viewModel.TransactionUTModel() == '') {
                ShowNotification("Attention", "Please input Currency", 'gritter-warning', true);
                return;
            }
            var mf = {
                MutualFundList: viewModel.TransactionUTModel().MutualFundList(),
                MutualCurrency: viewModel.Currencies(),
                MutualAmount: viewModel.TransactionUTModel().MutualAmount(),
            };
            self.MutualFundColl.push(mf);
        }
        else if (self.IsSubscription() == true) {
            if (viewModel.TransactionUTModel().MutualFundList() == null || viewModel.TransactionUTModel().MutualFundList() == '') {
                ShowNotification("Attention", "Please input Mutual Fund", 'gritter-warning', true);
                return;
            }
            var mf = {
                MutualFundList: viewModel.TransactionUTModel().MutualFundList(),
                MutualAmount: viewModel.TransactionUTModel().MutualAmount(),
            };
            self.SubcriptionColl.push(mf);
        } else if (self.IsRedemption() == true) {
            var mf = {
                MutualFundList: viewModel.TransactionUTModel().MutualFundList(),
                MutualCurrency: null,
                MutualAmount: null,
                MutualFundSwitchFrom: null,
                MutualFundSwitchTo: null,
                MutualPartial: viewModel.TransactionUTModel().MutualPartial(),
                MutualUnitNumber: viewModel.TransactionUTModel().MutualUnitNumber(),
                MutualSelected: false
            };
            self.RedemptionColl.push(mf);
        } else if (self.IsSwitching() == true) {
            var mf = {
                MutualFundList: viewModel.TransactionUTModel().MutualFundSwitchFrom(),
                MutualCurrency: null,
                MutualAmount: null,
                MutualFundSwitchFrom: viewModel.TransactionUTModel().MutualFundSwitchFrom(),
                MutualFundSwitchTo: viewModel.TransactionUTModel().MutualFundSwitchTo(),
                MutualPartial: viewModel.TransactionUTModel().MutualPartial(),
                MutualUnitNumber: viewModel.TransactionUTModel().MutualUnitNumber(),
                MutualSelected: false
            };
            self.SwitchingColl.push(mf);
        }
        $('.remove').click();
        $("#mutualfund-form").modal('hide');
    }
    self.UpdateMutualFund = function () {
        if (self.IsSP() == true) {
            var mf = {
                MutualFundList: viewModel.TransactionUTModel().MutualFundList(),
                MutualCurrency: viewModel.Currencies(),
                MutualAmount: viewModel.TransactionUTModel().MutualAmount(),
                MutualFundSwitchFrom: null,
                MutualFundSwitchTo: null,
                MutualPartial: null,
                MutualUnitNumber: null,
                MutualSelected: null
            };
            viewModel.MutualFundColl.replace(viewModel.MutualFundColl()[self.UTIndexUpdate()], mf);
        }
        else if (self.IsSubscription() == true) {
            var mf = {
                MutualFundList: viewModel.TransactionUTModel().MutualFundList(),
                MutualCurrency: null,
                MutualAmount: viewModel.TransactionUTModel().MutualAmount(),
                MutualFundSwitchFrom: null,
                MutualFundSwitchTo: null,
                MutualPartial: null,
                MutualUnitNumber: null,
                MutualSelected: null
            };
            viewModel.SubcriptionColl.replace(viewModel.SubcriptionColl()[self.UTIndexUpdate()], mf);
        }
        else if (self.IsRedemption() == true) {
            var mf = {
                MutualFundList: viewModel.TransactionUTModel().MutualFundList(),
                MutualCurrency: null,
                MutualAmount: null,
                MutualFundSwitchFrom: null,
                MutualFundSwitchTo: null,
                MutualPartial: viewModel.TransactionUTModel().MutualPartial(),
                MutualUnitNumber: viewModel.TransactionUTModel().MutualUnitNumber(),
                MutualSelected: false
            };
            viewModel.RedemptionColl.replace(viewModel.RedemptionColl()[self.UTIndexUpdate()], mf);
        }
        else if (self.IsSwitching() == true) {
            //dani start
            var SwitchFrom = viewModel.TransactionUTModel().MutualFundSwitchFrom();
            var SwitchTo = viewModel.TransactionUTModel().MutualFundSwitchTo();
            if (SwitchFrom != null && SwitchTo != null) {
                if (SwitchFrom.FundName == SwitchTo.FundName || SwitchFrom.FundCode == SwitchTo.FundCode) {
                    ShowNotification("Attention", "Switch From Mutual Fund and Switch To Mutual Fund Can't same!", 'gritter-warning', true);
                    return;
                } else {
                    var mf = {
                        MutualFundList: viewModel.TransactionUTModel().MutualFundList(),
                        MutualCurrency: null,
                        MutualAmount: null,
                        MutualFundSwitchFrom: viewModel.TransactionUTModel().MutualFundSwitchFrom(),
                        MutualFundSwitchTo: viewModel.TransactionUTModel().MutualFundSwitchTo(),
                        MutualPartial: viewModel.TransactionUTModel().MutualPartial(),
                        MutualUnitNumber: viewModel.TransactionUTModel().MutualUnitNumber(),
                        MutualSelected: false
                    };
                    viewModel.SwitchingColl.replace(viewModel.SwitchingColl()[self.UTIndexUpdate()], mf);
                }
            }
            //dani end
        }
        $('.remove').click();
        $("#mutualfund-form").modal('hide');
    }
    function Mutual() {
        self.IsSubscription(false);
        self.IsRedemption(false);
        self.IsSwitching(false);
        self.IsSP(false);
        self.TransactionUTModel().MutualFundCode(null);
        if (viewModel.ProductID() == ConsProductID.SavingPlanProductIDCons) {
            self.IsSP(true);
            return;
        }
        var utSubc = ConsTransactionType.utSubcription;
        var utSwitch = ConsTransactionType.utSwitching;
        var utRedemp = ConsTransactionType.utRedemption;
        var sel = self.Selected().Transaction_Type();
        if (utSubc.indexOf(sel) > -1) { self.IsSubscription(true); }
        if (utSwitch.indexOf(sel) > -1) { self.IsSwitching(true); }
        if (utRedemp.indexOf(sel) > -1) { self.IsRedemption(true); }
    }
    self.MutualFundAdd = function () {
        self.IsNewDataUT(true);
        self.TransactionUTModel().MutualFund(null);
        self.TransactionUTModel().MutualFundCode(null);
        self.TransactionUTModel().MutualCurrency(null);
        self.TransactionUTModel().MutualAmount(null);
        self.TransactionUTModel().MutualFundSwitchFrom([]);
        self.TransactionUTModel().MutualFundSwitchTo([]);
        self.TransactionUTModel().MutualUnitNumber(null);
        self.TransactionUTModel().MutualPartial(null);
        viewModel.TransactionUTModel().MutualFundList([]);
        Mutual();
        self.SetFundAutoCompleteFD();
        self.SetFundAutoCompleteFrom();
        self.SetFundAutoCompleteTo();
        viewModel.Selected().Currency(null); //default kosong
        $("#mutualfund-form").modal('show');
        $('.remove').click();
    }
    self.OnTTypeChange = function () {
        self.SetInvestmentAutoComplete();
    }
    self.GetSelectedRowUTSP = function (index, data) {
        self.IsNewDataUT(false);
        self.UTIndexUpdate(index);
        viewModel.TransactionUTModel().MutualFundList(data.MutualFundList);
        viewModel.Selected().Currency(data.MutualCurrency.ID);
        viewModel.TransactionUTModel().MutualAmount(data.MutualAmount);

        var amSP = formatNumber(viewModel.TransactionUTModel().MutualAmount());
        $('#amountsp').val(amSP);

        $("#mutualfund-form").modal('show');
    };
    self.GetSelectedRowUTSubc = function (index, data) {
        self.SetFundAutoCompleteFD();
        self.IsNewDataUT(false);
        self.UTIndexUpdate(index);
        viewModel.TransactionUTModel().MutualFundList(data.MutualFundList);
        viewModel.TransactionUTModel().MutualAmount(data.MutualAmount);

        var numunit = formatNumber(viewModel.TransactionUTModel().MutualAmount());
        $('#amountsp').val(numunit);

        $("#mutualfund-form").modal('show');
    };
    self.GetSelectedRowUTRedempt = function (index, data) {
        self.SetFundAutoCompleteFD();
        self.IsNewDataUT(false);
        self.UTIndexUpdate(index);
        viewModel.TransactionUTModel().MutualFundList(data.MutualFundList);
        viewModel.TransactionUTModel().MutualPartial(data.MutualPartial);
        viewModel.TransactionUTModel().MutualUnitNumber(data.MutualUnitNumber);

        var numunit = formatNumber(viewModel.TransactionUTModel().MutualUnitNumber());
        $('#numberofunit').val(numunit);

        $("#mutualfund-form").modal('show');
    };
    self.GetSelectedRowUTSwitch = function (index, data) {
        self.SetFundAutoCompleteFrom();
        self.SetFundAutoCompleteTo();
        self.IsNewDataUT(false);
        self.UTIndexUpdate(index);
        viewModel.TransactionUTModel().MutualFundList(data.MutualFundList);

        viewModel.TransactionUTModel().MutualFundSwitchFrom(data.MutualFundSwitchFrom);
        viewModel.TransactionUTModel().MutualFundCodeSwitchFrom(data.MutualFundCodeSwitchFrom);
        viewModel.TransactionUTModel().MutualFundSwitchTo(data.MutualFundSwitchTo);
        viewModel.TransactionUTModel().MutualFundCodeSwitchTo(data.MutualFundCodeSwitchTo);
        viewModel.TransactionUTModel().MutualPartial(data.MutualPartial);
        viewModel.TransactionUTModel().MutualUnitNumber(data.MutualUnitNumber);

        var numunit = formatNumber(viewModel.TransactionUTModel().MutualUnitNumber());
        $('#numberofunit').val(numunit);

        $("#mutualfund-form").modal('show');
    };
    self.BindInvestment = function () {
        var Invest = {
            url: api.server + api.url.transactionutin + "/investID/" + viewModel.TransactionUTModel().Investment(),
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(Invest, OnSuccessBindInvestment, OnError, OnAlways);
    }
    self.BindInvestmentSP = function () {
        var Invest = {
            url: api.server + api.url.transactionutsp + "/investIDSP/" + viewModel.TransactionUTModel().Investment().trim(),//updated by dani 23-6-2016
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(Invest, OnSuccessBindInvestmentSP, OnError, OnAlways);
    }
    //End Agung
    self.IsNewPlacement = ko.observable();
    self.IsPremature = ko.observable();
    self.IsBreakMaturity = ko.observable();
    self.IsChangeInstruction = ko.observable();
    self.IsFDMaintenance = ko.observable();
    function IsvalidFD() {
        if (viewModel.FDTransaction() == null || viewModel.FDTransaction().length == 0) {
            ShowNotification("Attention", "You need to insert transaction to continue transaction.", 'gritter-warning', true);
            return false;
        }
        return true;
    }
    function isValidFD() {
        if (self.FDModel().Documents() == null || self.FDModel().Documents().length == 0) {
            ShowNotification("Attention", "You need to upload attachment to continue transaction.", 'gritter-warning', true);
            return false;
        }
        if (self.FDModel().Amount() == null || self.FDModel().Amount() == 0) {
            ShowNotification("Attention", "Insert Amount", 'gritter-warning', true);
            return false;
        }
        return true;
    }
    self.AddTransactionClose = function () {
        var form = $("#frmFD");
        form.validate();

        if (form.valid()) {
            if (!isValidFD()) {
                return;
            }
            if (self.IsDocsComplete() === true) {
                if (self.FDModel().Tenor() === undefined || self.FDModel().Tenor() === null || self.FDModel().Tenor() === '') {
                    ShowNotification("Task Validation Warning", "Please select Tenor.", "gritter-warning", false);
                    return false;
                }
            }
            var fdLength = self.FDTransaction().length;
            var FDTrans = {
                Customer: self.FDModel().Customer(),
                Product: self.FDModel().Product,
                IsTopUrgent: self.FDModel().IsTopUrgent,
                IsTopUrgentChain: self.FDModel().IsTopUrgentChain,
                IsNormal: self.FDModel().IsNormal,
                IsDraft: self.FDModel().IsDraft,
                IsNewCustomer: self.FDModel().IsNewCustomer,
                TransactionID: fdLength + 1,
                ApplicationID: "-",
                Channel: self.FDModel().Channel(),
                Source: self.FDModel().Source(),
                TransactionType: self.TransactionType(),
                FDAccNumber: self.FDModel().FDAccNumber(),
                Currency: self.Currencies(),
                Amount: self.FDModel().Amount(),
                CreditAccNumber: self.FDModel().CreditAccNumber(),
                DebitAccNumber: self.FDModel().DebitAccNumber(),
                InterestRate: self.FDModel().InterestRate(),
                Tenor: self.FDModel().Tenor(),
                ValueDate: self.FDModel().ValueDate(),
                MaturityDate: self.FDModel().MaturityDate(),
                Remarks: self.FDRemarks() == null ? { ID: 0, Name: '' } : self.FDRemarks(),
                FDBankName: self.FDModel().FDBankName(),
                Documents: self.FDModel().Documents(),
                AttachmentRemarks: self.FDModel().AttachmentRemarks(),
                IsBringupTask: false,
                CreateDate: new Date(),
                DocsComplete: self.FDModel().DocsComplete(),
                AllInRate: self.FDModel().AllInRate(),
                FTPRate: self.FDModel().FTPRate()
            };
            self.FDTransaction.push(FDTrans);
            $('.remove').click();
            $("#modal-form-fdaddtrans").modal('hide');
        }

    }
    self.FDTransaction = ko.observableArray([]);
    self.TransactionType = ko.observable();
    self.FDRemarks = ko.observable();
    self.Currencies = ko.observable();
    self.IsNewDataFD = ko.observable();
    self.FDIndexUpdate = ko.observable();
    self.FdTransLoop = ko.observable();
    self.GetSelectedRowFD = function (index, data) {
        self.FDIndexUpdate(index);
        self.IsNewDataFD(false);
        self.FDRemarks(null);
        self.Selected().FDRemarks(null);
        self.Selected().TransactionType(data.TransactionType.TransTypeID);
        self.Selected().Currency(data.Currency.ID);
        self.Selected().FDRemarks(data.Remarks.ID);
        self.FDModel().FDAccNumber(data.FDAccNumber);
        self.FDModel().TransactionID(data.TransID);
        self.FDModel().ApplicationID(data.ApplicationID);
        self.FDModel().Amount(data.Amount);
        self.FDModel().CreditAccNumber(data.CreditAccNumber);
        self.FDModel().DebitAccNumber(data.DebitAccNumber);
        self.FDModel().InterestRate(data.InterestRate);
        self.FDModel().Tenor(data.Tenor);
        self.FDModel().ValueDate(data.ValueDate);
        self.FDModel().MaturityDate(data.MaturityDate);
        self.FDModel().FDBankName(data.FDBankName);
        self.FDModel().Documents(data.Documents);
        self.FDModel().AttachmentRemarks(data.AttachmentRemarks);
        self.FDModel().FTPRate(data.FTPRate);
        self.FDModel().DocsComplete(data.DocsComplete);
        self.FDModel().AllInRate(data.AllInRate);
        self.OnFDTransactionTypeChange();
        $("#modal-form-fdaddtrans").modal('show');
    };
    self.AddBulkTransaction = function () {
        var form = $("#frmFD");
        form.validate();
        if (form.valid()) {
            if (!isValidFD()) {
                return;
            }
            if (self.IsDocsComplete() === true) {
                if (self.FDModel().Tenor() === undefined || self.FDModel().Tenor() === null || self.FDModel().Tenor() === '') {
                    ShowNotification("Task Validation Warning", "Please select Tenor.", "gritter-warning", false);
                    return false;
                }
            }
            var fdLength = self.FDTransaction().length;
            ClearFDTType();
            var FDTrans = {
                Customer: self.FDModel().Customer(),
                Product: self.FDModel().Product,
                IsTopUrgent: self.FDModel().IsTopUrgent,
                IsTopUrgentChain: self.FDModel().IsTopUrgentChain,
                IsNormal: self.FDModel().IsNormal,
                IsDraft: self.FDModel().IsDraft,
                IsNewCustomer: self.FDModel().IsNewCustomer,
                TransactionID: fdLength + 1,
                ApplicationID: "-",
                Channel: self.FDModel().Channel(),
                Source: self.FDModel().Source(),
                TransactionType: self.TransactionType(),
                FDAccNumber: self.FDModel().FDAccNumber(),
                Currency: self.Currencies(),
                Amount: self.FDModel().Amount(),
                CreditAccNumber: self.FDModel().CreditAccNumber(),
                DebitAccNumber: self.FDModel().DebitAccNumber(),
                InterestRate: self.FDModel().InterestRate(),
                Tenor: self.FDModel().Tenor(),
                ValueDate: self.FDModel().ValueDate(),
                MaturityDate: self.FDModel().MaturityDate(),
                Remarks: self.FDRemarks() == null ? { ID: 0, Name: '' } : self.FDRemarks(),
                FDBankName: self.FDModel().FDBankName(),
                Documents: self.FDModel().Documents(),
                AttachmentRemarks: self.FDModel().AttachmentRemarks(),
                IsBringupTask: false,
                CreateDate: new Date(),
                DocsComplete: self.FDModel().DocsComplete(),
                AllInRate: self.FDModel().AllInRate(),
                FTPRate: self.FDModel().FTPRate()
            };
            self.FDTransaction.push(FDTrans);
            $('.remove').click();
            self.AddFDTransaction();
        }
    };
    self.UpdateFDTransaction = function (data) {
        var form = $("#frmFD");
        form.validate();
        if (form.valid()) {
            if (!isValidFD()) {
                return;
            }

            var FDTrans = {
                Customer: self.FDModel().Customer(),
                Product: self.FDModel().Product,
                IsTopUrgent: self.FDModel().IsTopUrgent,
                IsTopUrgentChain: self.FDModel().IsTopUrgentChain,
                IsNormal: self.FDModel().IsNormal,
                IsDraft: self.FDModel().IsDraft,
                IsNewCustomer: self.FDModel().IsNewCustomer,
                TransactionID: self.FDModel().TransactionID(),
                ApplicationID: "-",
                Channel: self.FDModel().Channel(),
                Source: self.FDModel().Source(),
                TransactionType: self.TransactionType(),
                FDAccNumber: self.FDModel().FDAccNumber(),
                Currency: self.Currencies(),
                Amount: self.FDModel().Amount(),
                CreditAccNumber: self.FDModel().CreditAccNumber(),
                DebitAccNumber: self.FDModel().DebitAccNumber(),
                InterestRate: self.FDModel().InterestRate(),
                Tenor: self.FDModel().Tenor(),
                ValueDate: self.FDModel().ValueDate(),
                MaturityDate: self.FDModel().MaturityDate(),
                Remarks: self.FDRemarks() == null ? { ID: 0, Name: '' } : self.FDRemarks(),
                FDBankName: self.FDModel().FDBankName(),
                Documents: self.FDModel().Documents(),
                AttachmentRemarks: self.FDModel().AttachmentRemarks(),
                DocsComplete: self.FDModel().DocsComplete(),
                AllInRate: self.FDModel().AllInRate(),
                FTPRate: self.FDModel().FTPRate(),
                IsBringupTask: false
            };
            viewModel.FDTransaction.replace(viewModel.FDTransaction()[self.FDIndexUpdate()], FDTrans);
            $('.remove').click();
            $("#modal-form-fdaddtrans").modal('hide');
        }
    };
    self.OnFDTransactionTypeChange = function () {
        var TransTypeID = viewModel.Selected().TransactionType();
        viewModel.IsNewPlacement(false);
        viewModel.IsPremature(false);
        viewModel.IsBreakMaturity(false);
        viewModel.IsChangeInstruction(false);
        viewModel.IsFDMaintenance(false);
        switch (TransTypeID) {
            case ConsTransactionType.fdNewPlacement:
                viewModel.IsNewPlacement(true);
                break;
            case ConsTransactionType.fdPrematurebreak:
                viewModel.IsPremature(true);
                break;
            case ConsTransactionType.fdBreakmaturity:
                viewModel.IsBreakMaturity(true);
                break;
            case ConsTransactionType.fdChangeInstruction:
                viewModel.IsChangeInstruction(true);
                break;
            case ConsTransactionType.fdMaintenance:
                viewModel.IsFDMaintenance(true);
                break;
            default:
                break;
        }
    };
    self.UploadDocumentFD = function () {
        self.DocumentPath(null);
        self.Selected().DocumentType(null);
        self.Selected().DocumentPurpose(null);
        self.DocumentType(null);
        self.DocumentPurpose(null);
        $("#modal-form-uploadFD").modal('show');
        $('.remove').click();
    }
    self.AddDocumentFD = function () {
        var doc = {
            ID: 0,
            Type: self.DocumentType(),
            Purpose: self.DocumentPurpose(),
            FileName: self.DocumentPath().name.replace(/[<>:"\/\\|?!@#$%^&*]+/g, '_'),//self.DocumentFileName(),
            DocumentPath: self.DocumentPath(),
            LastModifiedDate: new Date(),
            LastModifiedBy: null
        };

        if (doc.Type == null || doc.Purpose == null || doc.DocumentPath == null) {
            alert("Please complete the upload form fields.")
        } else {
            self.FDModel().Documents.push(doc);

            $('.remove').click();
            $("#modal-form-uploadFD").modal('hide');
        }

    };
    self.EditDocumentFD = function (data) {
        self.DocumentPath(data.DocumentPath);
        self.Selected().DocumentType(data.Type.ID);
        self.Selected().DocumentPurpose(data.Purpose.ID);

        // show upload dialog
        $("#modal-form-uploadFD").modal('show');
    };
    self.RemoveDocumentFD = function (data) {
        //alert(JSON.stringify(data))
        self.FDModel().Documents.remove(data);
    };
    //End Andi
    self.SetTemplate = function () {
        startCalculate();
        startCalculateFD();
        startCalculateUT();
        startCalculateLoan();
        startCalculateTMOCSO();
        startCalculateCIF();
        $('.date-picker').datepicker({
            autoclose: true,
            onSelect: function () {
                this.focus();
            }
        }).next().on(ace.click_event, function () {
            $(this).prev().focus();
        });
    }
    self.CloseTransaction = function () {
        if (self.IsDraftForm() == true) {
            window.location = "/home/draft-transactions";
            return;
        }
        //clear data when close html trx.. from ott become other trx product.
        viewModel.TransactionModel().Amount("");
        viewModel.TransactionModel().AmountUSD("0.00");
        viewModel.IsLimit(false);

        //Rizki - 2016-02-11 - Reset variable
        viewModel.IsFxTransaction(false);
        viewModel.IsFxTransactionToIDR(false);

        $("#newtransaction-form").hide();
        $("#header-transaction").show();
        $('#lblNewTR').text('');
    }
    self.SelectedTemplate = function () {
        //console.log(StatusIPE);
        var form = $("#aspnetForm");
        form.validate();
        if (form.valid()) {
            //aridya 20170120 kasih validasi new customer
            //if new customer is checked
            if (viewModel.IsNewCustomer()) {
                //if cif sudah bukan observable()
                if (!ko.isObservable(viewModel.TransactionModel().Customer().CIF)) {
                    //cek kalau kosong
                    if (viewModel.TransactionModel().Customer().CIF == '') {
                        ShowNotification("New Customer", "Please insert CIF Number for new customer.", "gritter-error", false);
                    } else {
                        //cek cif sudah ada atau belum di db
                        $.ajax({
                            type: "GET",
                            url: api.server + api.url.customer + "/" + viewModel.TransactionModel().Customer().CIF,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            headers: {
                                "Authorization": "Bearer " + accessToken
                            },
                            crossDomain: true,
                            cache: false,
                            success: function (data, textStatus, jqXHR) {
                                //jika ada balikan
                                if (jqXHR.status == 200) {
                                    ShowNotification("New Customer", "Can't create new customer, CIF Number already exists.", "gritter-error", false);
                                } else if (jqXHR.status == 204) { //kalau no content
                                    viewModel.LoadTemplate();
                                    viewModel.IsPPUOrBranchRole();
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                // send notification
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
                            }
                        });
                    }
                }
            } else {
                viewModel.LoadTemplate();
                viewModel.IsPPUOrBranchRole();
            }
            //end add aridya
        }
    }

    self.IsPPUOrBranchRole = function () {
        var i = 0;
        for (i = 0; i < viewModel.SPUser().Roles.length; i++) {
            if (Const_RoleName[viewModel.SPUser().Roles[i].ID].toLowerCase() == "dbs ppu head office maker") {
                self.IsPPURole(true);
                break;
            }
        }
    }

    //add aridya 20170120 pindahin load template karena dipakai berulang
    self.LoadTemplate = function () {
        $('#lblNewTR').text(self.ProductTitle());
        self.ApprovalData();
        $("#header-transaction").hide();
        //add by fandi 
        switch (self.ProductID()) {
            case ConsProductID.CIFProductIDCons:
                if (self.CIFTransactionModel() != null) {
                    if (!self.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPhoneFaxEmailMaintenance()) {
                        var a = { RetailCIFCBOContactUpdateID: null, PhoneNumber: '', CountryCode: '', CityCode: '', UpdatePhoneNumber: '', UpdateCountryCode: '', UpdateCityCode: '', ContactType: 'Selular', ActionType: 0 };
                        var b = ko.mapping.fromJS(a);
                        var c = { RetailCIFCBOContactUpdateID: null, PhoneNumber: '', CountryCode: '', CityCode: '', UpdatePhoneNumber: '', UpdateCountryCode: '', UpdateCityCode: '', ContactType: 'Office', ActionType: 0 };
                        var d = ko.mapping.fromJS(c);
                        var e = { RetailCIFCBOContactUpdateID: null, PhoneNumber: '', CountryCode: '', CityCode: '', UpdatePhoneNumber: '', UpdateCountryCode: '', UpdateCityCode: '', ContactType: 'Home', ActionType: 0 };
                        var f = ko.mapping.fromJS(e);

                        self.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers.push(f);
                        self.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers.push(d);
                        self.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers.push(b);
                    }
                }
                break;
            case ConsProductID.RTGSProductIDCons:
            case ConsProductID.SKNProductIDCons:
            case ConsProductID.OTTProductIDCons:
            case ConsProductID.OverbookingProductIDCons:
                //20161005 add aridya handle ovb autocomplete IPE 
                if (StatusIPE.toLowerCase() !== 'bcp2') {
                    if (self.ProductID() == ConsProductID.OverbookingProductIDCons) {
                        var options = {
                            url: api.server + api.url.bank + "/" + Const_OverBookingProduct.SelectedBeneBank,//viewModel.TransactionModel().Bank().ID,
                            token: accessToken
                        };
                        // exec ajax request
                        Helper.Ajax.Get(options, function (data) {
                            viewModel.TransactionModel().Bank().IBranchBank = data.IBranchBank;
                            autoCompleteData = viewModel.TransactionModel().Bank().IBranchBank;
                        }, OnError, OnAlways);

                        //$('#bank-name').data('ui-autocomplete')._trigger('select');
                        //self.SetBankAutoCompleted(function () { autoCompleteData = viewModel.TransactionModel().Bank().IBranchBank; });                            
                    }
                }
                //end add aridya

                if (self.ProductID() == ConsProductID.RTGSProductIDCons || self.ProductID() == ConsProductID.SKNProductIDCons) {
                    GetMidrateCurrency();
                    viewModel.TransactionModel().Rate(viewModel.MidrateData()[0].RupiahRate);
                    viewModel.Rate(viewModel.MidrateData()[0].RupiahRate);
                }
                viewModel.TransactionModel().ChargingAccountName(viewModel.TransactionModel().Customer().Name);
                self.UnderlyingFilterShow(1)// 1 = filter underlying by cif
                var i = 0;
                for (i = 0; i < viewModel.SPUser().Roles.length; i++) {
                    if (Const_RoleName[viewModel.SPUser().Roles[i].ID].toLowerCase() == "dbs ppu head office maker") {
                        self.IsPPUHeadMaker(true);
                        break;
                    }
                }
                if (self.IsNewCustomer() == true) {
                    document.getElementById("IsNewCus").checked = true;
                    document.getElementById("IsNewCus").disabled = true;
                    self.IsDraft(true);
                } else {
                    self.IsDraft(false);
                    document.getElementById("IsNewCus").disabled = true;
                }
                if (ConsProductID.OverbookingProductIDCons == self.ProductID()) {
                    var options = {
                        url: api.server + api.url.bank,
                        token: accessToken
                    };
                    // exec ajax request
                    Helper.Ajax.Get(options, function (data) {
                        //console.log(data);
                        //dataBankAll = data;
                        var new_BeneBank = ko.utils.arrayFirst(data, function (item) { return item.ID == Const_OverBookingProduct.SelectedBeneBank });
                        if (new_BeneBank != null)
                            viewModel.TransactionModel().Bank(new_BeneBank);
                        viewModel.TransactionModel().ChargingAccountBank(new_BeneBank.Description);
                    }, OnError, OnAlways);
                    //console.log(dataBankAll);
                    viewModel.IsDraft(false);
                    viewModel.IsOtherBank(false);
                    viewModel.TransactionModel().IsOtherBeneBank(false);
                }
                GetDataTBOTool(viewModel.TransactionModel().Customer().CIF);
                break;
            case ConsProductID.IDInvestmentProductIDCons:
                viewModel.TransactionUTModel().Customer().Name = viewModel.TransactionModel().Customer().Name;
                viewModel.TransactionUTModel().Customer().CIF = viewModel.TransactionModel().Customer().CIF;
                $('#customerut').val(viewModel.TransactionModel().Customer().Name);
                $('#cifut').val(viewModel.TransactionModel().Customer().CIF);
                break;
            default:
                break;
        }

        $("#newtransaction-form").show();
        $("#transaction-data").show();

        $('.date-picker').datepicker({
            autoclose: true,
            onSelect: function () {
                this.focus();
            }
        }).next().on(ace.click_event, function () {
            $(this).prev().focus();
        });
        if (self.TransactionModel().ModePayment() != "BCP2" && self.TransactionModel().ModePayment() != null) {
            if (self.ProductID() == ConsProductID.RTGSProductIDCons) { //aridya 20161215 buat branch code produk skn not mandatory (|| self.ProductID() == ConsProductID.SKNProductIDCons)
                if (viewModel.TransactionModel().IsIPETMO() == true) {
                    viewModel.IsBranchCode(false);
                    $('#bank-branch-code').data({ ruleRequired: false });
                } else {
                    viewModel.IsBranchCode(true);
                    $('#bank-branch-code').data({ ruleRequired: true });
                }
            } else {
                viewModel.IsBranchCode(false);
                $('#bank-branch-code').data({ ruleRequired: false });
            }
            if (ConsProductID.RTGSProductIDCons == viewModel.ProductID() || ConsProductID.SKNProductIDCons == viewModel.ProductID()) {
                GetLimitProduct();
            }
            if (ConsProductID.RTGSProductIDCons == viewModel.ProductID() || ConsProductID.SKNProductIDCons == viewModel.ProductID() ||
                ConsProductID.OTTProductIDCons == viewModel.ProductID() || ConsProductID.OverbookingProductIDCons == viewModel.ProductID()) {
                enforceMaxLengthIPE()//add aridya 20170212 untuk max length IPE
            }
            ValidationMandotary();
            ValidationWaiveCharges();
            viewModel.GetDataUnderlying();
        }
        startCalculate();
        startCalculateFD();
        startCalculateUT();
        startCalculateLoan();
        startCalculateTMOCSO();
        startCalculateCIF();
        GetParameterSystems();
        GetParameterbyProduct();
        //GetAllParameterSystems();
        self.SetCustomerAutoComplete();
        self.SetBankAutoCompleted();
        self.SetTZNumberAutoComplete();
        self.SetBankChagingAccount();
        self.SetCustomerAutoCompleteFD();
        self.SetFundAutoCompleteFD();
        self.SetFundAutoCompleteFrom();
        self.SetFundAutoCompleteTo();
        self.SetInvestmentAutoComplete();
        self.SetCustomerAutoCompleteUTFNA();
        self.SetCustomerAutoCompleteUTNonFNA();
        self.SetCustomerAutoCompleteCIFAccount();
        self.SetSOLIDAutoComplete();
        self.SetSOLIDJoinAutoComplete();
        if ($('#debit-acc-number option[value=-]').length <= 0) {
            $('#debit-acc-number').append('<option value=->-</option>');
        }
        SetDocumentPathCIFStyle();
    }
    //end aridya

    self.ApprovalTemplate = function () {
        var output;
        //self.IsDraftForm(false);
        ar = window.location.hash.split('#');
        if (ar != null) {
            if (ar[2] == "TMOIPE") {
                output = "PaymentTransactionTMOIPE";
            } else {
                switch (self.ProductID()) {
                    //aridya add 20161011 SKN BULK ~OFFLINE~
                    case ConsProductID.SKNBulkProductIDCons:
                        output = "SKNBulk";
                        break;
                    case ConsProductID.RTGSProductIDCons:
                    case ConsProductID.SKNProductIDCons: // SKN Singgle Active
                    case ConsProductID.OTTProductIDCons:
                    case ConsProductID.OverbookingProductIDCons:
                        CheckTMO();
                        if (StatusIPE == "BCP2") {
                            output = "PaymentTransaction";
                        } else {
                            if (viewModel.TransactionModel().IsIPETMO() == true) {
                                output = "PaymentTransactionTMOIPE";
                            } else {
                                output = "PaymentTransactionIPE";
                            }
                        }
                        break;
                        //case ConsProductID.SKNProductIDCons: // SKN Singgle Active
                        //        output = "PaymentTransaction";
                        //    break;
                    case ConsProductID.TMOProductIDCons:
                        output = "TMOTransaction";
                        CheckCSO();
                        break;
                    case ConsProductID.FDProductIDCons:
                        output = "FDTransaction";
                        SetFDValue();
                        break;
                    case ConsProductID.IDInvestmentProductIDCons:
                        self.SetSOLIDAutoComplete();
                        self.SetSOLIDJoinAutoComplete();
                        viewModel.IsUTNew(true);
                        output = "UTINTransaction";
                        break;
                    case ConsProductID.SavingPlanProductIDCons:
                    case ConsProductID.UTOnshoreproductIDCons:
                    case ConsProductID.UTOffshoreProductIDCons:
                    case ConsProductID.UTCPFProductIDCons:
                        self.SetFundAutoCompleteFD();
                        self.SetFundAutoCompleteFrom();
                        self.SetFundAutoCompleteTo();
                        self.SetInvestmentAutoComplete();
                        self.SetCustomerAutoCompleteUTFNA();
                        self.SetCustomerAutoCompleteUTNonFNA();
                        output = "UTSPTransaction";
                        Mutual();
                        break;
                    case ConsProductID.CollateralProductIDCons:
                        output = "CollateralTransaction";
                        break;
                    case ConsProductID.CIFProductIDCons:
                        output = "CIFTransaction";
                        CheckCustomerCenter();

                        SetDocumentPathCIFStyle();
                        //Start Haqi
                        GetAllParameterSystems();
                        //End Haqi

                        break;
                    case ConsProductID.LoanDisbursmentProductIDCons:
                    case ConsProductID.LoanRolloverProductIDCons:
                    case ConsProductID.LoanIMProductIDCons:
                    case ConsProductID.LoanSettlementProductIDCons:
                        output = "LoanTransaction";
                        break;
                    default:
                        if (StatusIPE == "BCP2") {
                            output = "PaymentTransaction";
                        } else {
                            output = "PaymentTransactionIPE";
                        }
                        break;
                }
            }
        } else {
            switch (self.ProductID()) {
                //aridya add 20161011 SKN BULK ~OFFLINE~
                case ConsProductID.SKNBulkProductIDCons:
                    output = "SKNBulk";
                    break;
                case ConsProductID.RTGSProductIDCons:
                case ConsProductID.SKNProductIDCons: // SKN Singgle Active
                case ConsProductID.OTTProductIDCons:
                case ConsProductID.OverbookingProductIDCons:
                    if (StatusIPE == "BCP2") {
                        output = "PaymentTransaction";
                    } else {
                        output = "PaymentTransactionIPE";
                    }
                    break;
                    //case ConsProductID.SKNProductIDCons: // SKN Singgle Active
                    //        output = "PaymentTransaction";
                    //    break;
                case ConsProductID.TMOProductIDCons:
                    output = "TMOTransaction";
                    CheckCSO();
                    break;
                case ConsProductID.FDProductIDCons:
                    output = "FDTransaction";
                    SetFDValue();
                    break;
                case ConsProductID.IDInvestmentProductIDCons:
                    self.SetSOLIDAutoComplete();
                    self.SetSOLIDJoinAutoComplete();
                    viewModel.IsUTNew(true);
                    output = "UTINTransaction";
                    break;
                case ConsProductID.SavingPlanProductIDCons:
                case ConsProductID.UTOnshoreproductIDCons:
                case ConsProductID.UTOffshoreProductIDCons:
                case ConsProductID.UTCPFProductIDCons:
                    self.SetFundAutoCompleteFD();
                    self.SetFundAutoCompleteFrom();
                    self.SetFundAutoCompleteTo();
                    self.SetInvestmentAutoComplete();
                    self.SetCustomerAutoCompleteUTFNA();
                    self.SetCustomerAutoCompleteUTNonFNA();
                    output = "UTSPTransaction";
                    Mutual();
                    break;
                case ConsProductID.CollateralProductIDCons:
                    output = "CollateralTransaction";
                    break;
                case ConsProductID.CIFProductIDCons:
                    output = "CIFTransaction";
                    CheckCustomerCenter();

                    SetDocumentPathCIFStyle();
                    //Start Haqi
                    GetAllParameterSystems();
                    //End Haqi

                    break;
                case ConsProductID.LoanDisbursmentProductIDCons:
                case ConsProductID.LoanRolloverProductIDCons:
                case ConsProductID.LoanIMProductIDCons:
                case ConsProductID.LoanSettlementProductIDCons:
                    output = "LoanTransaction";
                    break;
                default:
                    if (StatusIPE == "BCP2") {
                        output = "PaymentTransaction";
                    } else {
                        output = "PaymentTransactionIPE";
                    }
                    break;
            }
        }

        return output;
    }
    // set approval data template
    self.ApprovalData = function () {
        var output;
        switch (self.ProductID()) {
            case ConsProductID.RTGSProductIDCons:
            case ConsProductID.SKNProductIDCons:
            case ConsProductID.OTTProductIDCons:
            case ConsProductID.OverbookingProductIDCons:
                output = self.TransactionModel();
                break;
            case ConsProductID.TMOProductIDCons:
                output = self.TransactionTMOModel();
                break;
            case ConsProductID.FDProductIDCons:
                output = self.FDModel();
                break;
            case ConsProductID.LoanDisbursmentProductIDCons:
            case ConsProductID.LoanIMProductIDCons:
            case ConsProductID.LoanRolloverProductIDCons:
            case ConsProductID.LoanSettlementProductIDCons:
                output = self.TransactionLoanModel();
                break
            case ConsProductID.IDInvestmentProductIDCons:
            case ConsProductID.UTCPFProductIDCons:
            case ConsProductID.UTOffshoreProductIDCons:
            case ConsProductID.UTOnshoreproductIDCons:
            case ConsProductID.SavingPlanProductIDCons:
                output = self.TransactionUTModel();
                break;
            case ConsProductID.CIFProductIDCons:
                output = self.CIFTransactionModel();
                break;
            default:
                output = self.TransactionModel();
                break;
        }
        return output;
    };
    self.SetCalculatePayment = function () {
        startCalculate();
    };
};
//End View Model

var viewModel = new ViewModel();

var StatusIPE = {
    Value: ""
};

var CheckPayment = {
    Status: false
};

var UnderlyingUtilizeStatus;

function GetAccountData() {
    var cif = viewModel.CIFTransactionModel().Customer().CIF;
    var options = {
        url: api.server + api.url.transactioncif + "/AccountListJoin/" + cif,
        token: accessToken
    };
    Helper.Ajax.Get(options, OnSuccessGetAccountData, OnError, OnAlways);
}
function OnSuccessGetAccountData(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        viewModel.ddlAccountListPOAs(data);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
    }
}
function FormValidationTrxPayment() {
    //add IPE form validation 
    //start by henggar 15 august 2016    
    if (viewModel != undefined) {
        //SKN
        if (viewModel.Selected().Product() == ConsProductID.SKNProductIDCons) {
            if (viewModel.TransactionModel().IsResident() == true && viewModel.TransactionModel().DebitCurrency().Code == "IDR" && viewModel.Selected().Currency() == Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: false });
                viewModel.IsLLD(false);
                $('#ComCode').data({ ruleRequired: false });
                viewModel.IsCompliance(false);
                $('#underlying-code').data({ ruleRequired: false });
                viewModel.Selected().UnderlyingDoc(void 0);
                viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
                viewModel.EnableUnderlying(false);
                viewModel.IsUnderlying(false);
            } else if (viewModel.TransactionModel().IsResident() == true && viewModel.TransactionModel().DebitCurrency().Code != "IDR" && viewModel.Selected().Currency() == Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: false });
                viewModel.IsLLD(false);
                $('#ComCode').data({ ruleRequired: true });
                viewModel.IsCompliance(true);
                viewModel.Selected().UnderlyingDoc(optionSelected.SelectedTanpaUnderying);
                $('#underlying-code').data({ ruleRequired: false });
                viewModel.EnableUnderlying(true);
                viewModel.IsUnderlying(false);
            } else if (viewModel.TransactionModel().IsResident() != true && viewModel.TransactionModel().DebitCurrency().Code == "IDR" && viewModel.Selected().Currency() == Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: false });
                viewModel.IsLLD(false);
                $('#ComCode').data({ ruleRequired: false });
                viewModel.IsCompliance(false);
                $('#underlying-code').data({ ruleRequired: false });
                viewModel.Selected().UnderlyingDoc(void 0);
                viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
                viewModel.EnableUnderlying(false);
                viewModel.IsUnderlying(false);
            } else if (viewModel.TransactionModel().IsResident() != true && viewModel.TransactionModel().DebitCurrency().Code != "IDR" && viewModel.Selected().Currency() == Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: false });
                viewModel.IsLLD(false);
                $('#ComCode').data({ ruleRequired: true });
                viewModel.IsCompliance(true);
                if (parseFloat(viewModel.TransactionModel().AmountUSD()) > Currency.LimitEqvUSD) {
                    viewModel.Selected().UnderlyingDoc(void 0);
                    viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
                    $('#underlying-code').data({ ruleRequired: true });
                    viewModel.EnableUnderlying(false);
                    viewModel.IsUnderlying(true);
                } else {
                    $('#underlying-code').data({ ruleRequired: false });
                    viewModel.Selected().UnderlyingDoc(optionSelected.SelectedTanpaUnderying);
                    viewModel.EnableUnderlying(true);
                    viewModel.IsUnderlying(false);
                }
            }
            else {
                viewModel.CurrencyTransactionValidate();
            }
            //RTGS
        } else if (viewModel.Selected().Product() == ConsProductID.RTGSProductIDCons) {
            if (viewModel.TransactionModel().IsResident() == true && viewModel.TransactionModel().DebitCurrency().Code == "IDR" && viewModel.Selected().Currency() == Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: false });
                viewModel.IsLLD(false);
                $('#ComCode').data({ ruleRequired: false });
                viewModel.IsCompliance(false);
                $('#underlying-code').data({ ruleRequired: false });
                viewModel.Selected().UnderlyingDoc(void 0);
                viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
                viewModel.EnableUnderlying(false);
                viewModel.IsUnderlying(false);
                if (viewModel.TransactionModel().IsIPETMO() == true) {
                    viewModel.IsFxTransaction(false);
                    viewModel.IsNoThresholdValue(true);
                }
            } else if (viewModel.TransactionModel().IsResident() == true && viewModel.TransactionModel().DebitCurrency().Code != "IDR" && viewModel.Selected().Currency() == Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: false });
                viewModel.IsLLD(false);
                $('#ComCode').data({ ruleRequired: true });
                viewModel.IsCompliance(true);
                if (viewModel.TransactionModel().IsIPETMO() == true) {
                    if (parseFloat(viewModel.TransactionModel().AmountUSD()) >= 5000000) {
                        viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
                        viewModel.IsFxTransaction(true);
                        $('#underlying-code').data({ ruleRequired: true });
                        viewModel.EnableUnderlying(false);
                        viewModel.IsUnderlying(true);
                    } else {
                        viewModel.Selected().UnderlyingDoc(optionSelected.SelectedTanpaUnderying);
                        $('#underlying-code').data({ ruleRequired: false });
                        viewModel.IsFxTransaction(false);
                        viewModel.IsNoThresholdValue(true);
                        viewModel.EnableUnderlying(true);
                        viewModel.IsUnderlying(false);
                    }
                } else {
                    viewModel.Selected().UnderlyingDoc(optionSelected.SelectedTanpaUnderying);
                    $('#underlying-code').data({ ruleRequired: false });
                    viewModel.EnableUnderlying(true);
                    viewModel.IsUnderlying(false);
                }
            } else if (viewModel.TransactionModel().IsResident() != true && viewModel.TransactionModel().DebitCurrency().Code == "IDR" && viewModel.Selected().Currency() == Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: false });
                viewModel.IsLLD(false);
                $('#ComCode').data({ ruleRequired: false });
                viewModel.IsCompliance(false);
                $('#underlying-code').data({ ruleRequired: false });
                viewModel.Selected().UnderlyingDoc(void 0);
                viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
                viewModel.EnableUnderlying(false);
                viewModel.IsUnderlying(false);
                if (viewModel.TransactionModel().IsIPETMO() == true) {
                    viewModel.IsFxTransaction(false);
                    viewModel.IsNoThresholdValue(true);
                }
            } else if (viewModel.TransactionModel().IsResident() != true && viewModel.TransactionModel().DebitCurrency().Code != "IDR" && viewModel.Selected().Currency() == Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: false });
                viewModel.IsLLD(false);
                $('#ComCode').data({ ruleRequired: true });
                viewModel.IsCompliance(true);
                if (parseFloat(viewModel.TransactionModel().AmountUSD()) > Currency.LimitEqvUSD) {
                    viewModel.Selected().UnderlyingDoc(void 0);
                    viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
                    $('#underlying-code').data({ ruleRequired: true });
                    viewModel.EnableUnderlying(false);
                    viewModel.IsUnderlying(true);
                } else {
                    viewModel.Selected().UnderlyingDoc(optionSelected.SelectedTanpaUnderying);
                    $('#underlying-code').data({ ruleRequired: false });
                    viewModel.EnableUnderlying(true);
                    viewModel.IsUnderlying(false);
                }
                if (viewModel.TransactionModel().IsIPETMO() == true) {
                    viewModel.IsFxTransaction(false);
                    viewModel.IsNoThresholdValue(true);
                }
            }
            else {
                viewModel.CurrencyTransactionValidate();
            }
            //OverBooking
        } else if (viewModel.Selected().Product() == ConsProductID.OverbookingProductIDCons) {
            if (viewModel.TransactionModel().IsResident() == true && viewModel.TransactionModel().DebitCurrency().Code == "IDR" && viewModel.Selected().Currency() == Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: false });
                viewModel.IsLLD(false);
                $('#ComCode').data({ ruleRequired: false });
                viewModel.IsCompliance(false);
                $('#underlying-code').data({ ruleRequired: false });
                viewModel.Selected().UnderlyingDoc(void 0);
                viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
                viewModel.EnableUnderlying(false);
                viewModel.IsUnderlying(false);
                //Done
            } else if (viewModel.TransactionModel().IsResident() == true && viewModel.TransactionModel().DebitCurrency().Code != "IDR" && viewModel.Selected().Currency() == Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: false });
                viewModel.IsLLD(false);
                $('#ComCode').data({ ruleRequired: true });
                viewModel.IsCompliance(true);
                $('#underlying-code').data({ ruleRequired: false });
                viewModel.Selected().UnderlyingDoc(optionSelected.SelectedTanpaUnderying);
                viewModel.EnableUnderlying(true);
                viewModel.IsUnderlying(false);
                //Done
            } else if (viewModel.TransactionModel().IsResident() == true && viewModel.TransactionModel().DebitCurrency().Code == "IDR" && viewModel.Selected().Currency() != Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: false });
                viewModel.IsLLD(false);
                $('#ComCode').data({ ruleRequired: true });
                viewModel.IsCompliance(true);
                viewModel.EnableUnderlying(false);
                viewModel.Selected().UnderlyingDoc(void 0);
                viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
                $('#underlying-code').data({ ruleRequired: true }); // +the underlying
                viewModel.IsUnderlying(true);
                UnderlyingUtilizeStatus = 1;
            } else if (viewModel.TransactionModel().IsResident() == true && viewModel.TransactionModel().DebitCurrency().Code != "IDR" && viewModel.Selected().Currency() != Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: false });
                viewModel.IsLLD(false);
                if (viewModel.TransactionModel().DebitCurrency().Code == viewModel.TransactionModel().Currency().Code) {
                    $('#ComCode').data({ ruleRequired: false });//for non-same currency trx only
                    viewModel.IsCompliance(false);
                } else {
                    $('#ComCode').data({ ruleRequired: true });//for non-same currency trx only
                    viewModel.IsCompliance(true);
                }
                $('#underlying-code').data({ ruleRequired: false });
                viewModel.Selected().UnderlyingDoc(void 0);
                viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
                viewModel.EnableUnderlying(false);
                viewModel.IsUnderlying(false);
            } else if (viewModel.TransactionModel().IsResident() != true && viewModel.TransactionModel().DebitCurrency().Code == "IDR" && viewModel.Selected().Currency() == Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: true });
                viewModel.IsLLD(true);
                $('#ComCode').data({ ruleRequired: false });
                viewModel.IsCompliance(false);
                $('#underlying-code').data({ ruleRequired: false });
                viewModel.Selected().UnderlyingDoc(void 0);
                viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
                viewModel.EnableUnderlying(false);
                viewModel.IsUnderlying(false);
            } else if (viewModel.TransactionModel().IsResident() != true && viewModel.TransactionModel().DebitCurrency().Code != "IDR" && viewModel.Selected().Currency() == Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: true });
                viewModel.IsLLD(true);
                $('#ComCode').data({ ruleRequired: true });
                viewModel.IsCompliance(true);
                if (parseFloat(viewModel.TransactionModel().AmountUSD()) > Currency.LimitEqvUSD) {
                    viewModel.Selected().UnderlyingDoc(void 0);
                    viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
                    $('#underlying-code').data({ ruleRequired: true });
                    viewModel.IsUnderlying(true);
                    viewModel.EnableUnderlying(false);
                } else {
                    viewModel.Selected().UnderlyingDoc(optionSelected.SelectedTanpaUnderying);
                    viewModel.EnableUnderlying(true);
                    viewModel.IsUnderlying(false);
                }
            } else if (viewModel.TransactionModel().IsResident() != true && viewModel.TransactionModel().DebitCurrency().Code == "IDR" && viewModel.Selected().Currency() != Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: true });
                viewModel.IsLLD(true);
                $('#ComCode').data({ ruleRequired: true });
                viewModel.IsCompliance(true);
                viewModel.Selected().UnderlyingDoc(void 0);
                viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
                $('#underlying-code').data({ ruleRequired: true }); // +the underlying
                viewModel.EnableUnderlying(false);
                viewModel.IsUnderlying(true);
            } else if (viewModel.TransactionModel().IsResident() != true && viewModel.TransactionModel().DebitCurrency().Code != "IDR" && viewModel.Selected().Currency() != Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: true });
                viewModel.IsLLD(true);
                if (viewModel.TransactionModel().DebitCurrency().Code == viewModel.TransactionModel().Currency().Code) {
                    $('#ComCode').data({ ruleRequired: false });//for non-same currency trx only
                    viewModel.IsCompliance(false);
                } else {
                    $('#ComCode').data({ ruleRequired: true });//for non-same currency trx only
                    viewModel.IsCompliance(true);
                }
                $('#underlying-code').data({ ruleRequired: false });
                viewModel.Selected().UnderlyingDoc(void 0);
                viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
                viewModel.EnableUnderlying(false);
                viewModel.IsUnderlying(false);
            }
            else {
                viewModel.CurrencyTransactionValidate();
            }
            //OTT
        } else if (viewModel.Selected().Product() == ConsProductID.OTTProductIDCons) {
            if (viewModel.TransactionModel().IsResident() == true && viewModel.TransactionModel().DebitCurrency().Code == "IDR" && viewModel.Selected().Currency() != Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: true });
                viewModel.IsLLD(true);
                $('#ComCode').data({ ruleRequired: true });
                viewModel.IsCompliance(true);
                viewModel.Selected().UnderlyingDoc(void 0);
                viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
                $('#underlying-code').data({ ruleRequired: true }); //+the underlying
                viewModel.IsUnderlying(true);
                viewModel.EnableUnderlying(false);
            } else if (viewModel.TransactionModel().IsResident() == true && viewModel.TransactionModel().DebitCurrency().Code != "IDR" && viewModel.Selected().Currency() != Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: true });
                viewModel.IsLLD(true);
                if (viewModel.TransactionModel().DebitCurrency().Code == viewModel.TransactionModel().Currency().Code) {
                    $('#ComCode').data({ ruleRequired: false });//for non-same currency trx only
                    viewModel.IsCompliance(false);
                } else {
                    $('#ComCode').data({ ruleRequired: true });//for non-same currency trx only
                    viewModel.IsCompliance(true);
                }
                $('#underlying-code').data({ ruleRequired: false });
                viewModel.Selected().UnderlyingDoc(void 0);
                viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
                viewModel.EnableUnderlying(false);
                viewModel.IsUnderlying(false);
            } else if (viewModel.TransactionModel().IsResident() == true && viewModel.TransactionModel().DebitCurrency().Code == "IDR" && viewModel.Selected().Currency() == Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: true });
                viewModel.IsLLD(true);
                $('#ComCode').data({ ruleRequired: false });
                viewModel.IsCompliance(false);
                $('#underlying-code').data({ ruleRequired: false });
                viewModel.Selected().UnderlyingDoc(void 0);
                viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
                viewModel.EnableUnderlying(false);
                viewModel.IsUnderlying(false);
            } else if (viewModel.TransactionModel().IsResident() == true && viewModel.TransactionModel().DebitCurrency().Code != "IDR" && viewModel.Selected().Currency() == Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: true });
                viewModel.IsLLD(true);
                $('#ComCode').data({ ruleRequired: true });
                viewModel.IsCompliance(true);
                viewModel.EnableUnderlying(true);
                viewModel.Selected().UnderlyingDoc(optionSelected.SelectedTanpaUnderying);
                viewModel.IsUnderlying(false);
            } else if (viewModel.TransactionModel().IsResident() != true && viewModel.TransactionModel().DebitCurrency().Code == "IDR" && viewModel.Selected().Currency() != Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: true });
                viewModel.IsLLD(true);
                $('#ComCode').data({ ruleRequired: true });
                viewModel.IsCompliance(true);
                $('#underlying-code').data({ ruleRequired: true });
                viewModel.IsUnderlying(true);
                viewModel.EnableUnderlying(false);
                viewModel.Selected().UnderlyingDoc(void 0);
                viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
            } else if (viewModel.TransactionModel().IsResident() != true && viewModel.TransactionModel().DebitCurrency().Code != "IDR" && viewModel.Selected().Currency() != Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: true });
                viewModel.IsLLD(true);
                if (viewModel.TransactionModel().DebitCurrency().Code == viewModel.TransactionModel().Currency().Code) {
                    $('#ComCode').data({ ruleRequired: false });//for non-same currency trx only
                    viewModel.IsCompliance(false);
                } else {
                    $('#ComCode').data({ ruleRequired: true });//for non-same currency trx only
                    viewModel.IsCompliance(true);
                }
                $('#underlying-code').data({ ruleRequired: false });
                viewModel.EnableUnderlying(false);
                viewModel.IsUnderlying(false);
            } else if (viewModel.TransactionModel().IsResident() != true && viewModel.TransactionModel().DebitCurrency().Code == "IDR" && viewModel.Selected().Currency() == Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: true });
                viewModel.IsLLD(true);
                $('#ComCode').data({ ruleRequired: false });
                viewModel.IsCompliance(false);
                $('#underlying-code').data({ ruleRequired: false });
                viewModel.Selected().UnderlyingDoc(void 0);
                viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
                viewModel.EnableUnderlying(false);
                viewModel.IsUnderlying(false);
            } else if (viewModel.TransactionModel().IsResident() != true && viewModel.TransactionModel().DebitCurrency().Code != "IDR" && viewModel.Selected().Currency() == Currency.IDRselected) {
                $('#lldcode').data({ ruleRequired: true });
                viewModel.IsLLD(true);
                $('#ComCode').data({ ruleRequired: true });
                viewModel.IsCompliance(true);
                if (parseFloat(viewModel.TransactionModel().AmountUSD()) > Currency.LimitEqvUSD) {
                    viewModel.Selected().UnderlyingDoc(void 0);
                    viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
                    $('#underlying-code').data({ ruleRequired: true });
                    viewModel.EnableUnderlying(false);
                    viewModel.IsUnderlying(true);
                } else {
                    viewModel.Selected().UnderlyingDoc(optionSelected.SelectedTanpaUnderying);
                    viewModel.EnableUnderlying(true);
                    viewModel.IsUnderlying(false);
                }
            } else {
                viewModel.CurrencyTransactionValidate();
            }
        }
        viewModel.TransactionModel().IsUnderlyingUtilizeStatus(UnderlyingUtilizeStatus);
    }
}

function ValidationRateTZ() {
    //if (viewModel.TransactionModel().DebitCurrency().Code != "function c(){if(0<arguments.length)return c.Ka(d,arguments[0])&&(c.P(),d=arguments[0],c.O()),this;a.k.zb(c);return d}") {
    //    if (viewModel.TransactionModel().DebitCurrency().Code != viewModel.TransactionModel().Currency().Code) {
    //        $('#trxrate').data({ ruleRequired: true });
    //        viewModel.IsRate(true);
    //    } else {
    //        $('#trxrate').data({ ruleRequired: false });
    //        viewModel.IsRate(false);
    //    }
    //} else {
    //    $('#trxrate').data({ ruleRequired: false });
    //    viewModel.IsRate(false);
    //}
    if (viewModel.TransactionModel().IsIPETMO() == true) {
        var IsFxTransaction = (viewModel.TransactionModel().Currency().Code != 'IDR' && viewModel.TransactionModel().DebitCurrency().Code == 'IDR');
        var IsFxTransactionToIDR = (viewModel.TransactionModel().Currency().Code == 'IDR' && viewModel.TransactionModel().DebitCurrency().Code != 'IDR');
        viewModel.IsFxTransaction(IsFxTransaction);
        viewModel.IsFxTransactionToIDR(IsFxTransactionToIDR);
        if (viewModel.IsFxTransaction() == true || viewModel.IsFxTransactionToIDR() == true) {
            $('#tznumberauto').data({ ruleRequired: true });
            viewModel.IsTZNumber(true);
        } else {
            $('#tznumberauto').data({ ruleRequired: false });
            viewModel.IsTZNumber(false);
        }
    } else {
        if (viewModel.ProductID() == ConsProductID.OverbookingProductIDCons || viewModel.ProductID() == ConsProductID.OTTProductIDCons) {
            if (viewModel.Selected().ProductType() == undefined || viewModel.Selected().ProductType() == Const_ProductType.vsdBoardC || viewModel.Selected().ProductType() == Const_ProductType.vsdBoardT) {
                $('#tznumber').data({ ruleRequired: false });
                viewModel.IsTZNumber(false);
            } else {
                $('#tznumber').data({ ruleRequired: true });
                viewModel.IsTZNumber(true);
            }
        } else {
            $('#tznumber').data({ ruleRequired: false });
            viewModel.IsTZNumber(false);
        }
    }
}

function ValidationMandotary() {
    if (ConsProductID.RTGSProductIDCons == viewModel.ProductID() || ConsProductID.OTTProductIDCons == viewModel.ProductID() || ConsProductID.OverbookingProductIDCons == viewModel.ProductID() || ConsProductID.SKNProductIDCons == viewModel.ProductID()) {
        //if OTT Bank charge default SHA
        if (viewModel.ProductID() == ConsProductID.OTTProductIDCons) {
            $('#bank-charges').val(Const_BankCharge.SHA);
            $('#agent-charges').val(Const_AgentCharge.SHA);// default OTT bank charges default SHA,, ID di sesuaikan di dev or in UAT
        }

        if (StatusIPE.toLowerCase() !== 'bcp2' && viewModel.Selected().Product() == ConsProductID.SKNProductIDCons) {
            $('#residentBene').data({ ruleRequired: true });
            viewModel.BeneChargebyProduct(true);
            $('#beneficiarybusiness').data({ ruleRequired: true });
            viewModel.BeneBusinessbyProduct(true);
        } else {
            $('#residentBene').data({ ruleRequired: false });
            viewModel.BeneChargebyProduct(false);
            $('#beneficiarybusiness').data({ ruleRequired: false });
            viewModel.BeneBusinessbyProduct(false);
        }
        //bene country not mandatory
        $('#benecountrycode').data({ ruleRequired: false });
        viewModel.BeneCountrybyProduct(false);
        //trx RelationShip
        $('#transactionrelationship').data({ ruleRequired: false });
        viewModel.TrxRelationbyProduct(false);
        //set default chargingaccbank
        if (viewModel.TransactionModel().ChargingAccountBank() == undefined) {
            viewModel.TransactionModel().ChargingAccountBank("DBS Bank Indonesia");
        }
    }
}

function ValidationWaiveCharges() {
    if (viewModel.TransactionModel().BizSegment().Name == "CBG") {
        if (viewModel.TransactionModel().Sundry().ID > 0 && viewModel.TransactionModel().Sundry().ID != undefined) {
            viewModel.IsCBGCustomer(false);
            viewModel.IsTransactionUsingDebitSundry(true);
        } else {
            viewModel.IsCBGCustomer(true);
            viewModel.IsTransactionUsingDebitSundry(false);
        }
    } else {
        if (viewModel.TransactionModel().Sundry().ID > 0 && viewModel.TransactionModel().Sundry().ID != undefined) {
            viewModel.IsCBGCustomer(false);
            viewModel.IsTransactionUsingDebitSundry(true);
        } else {
            viewModel.IsCBGCustomer(false);
            viewModel.IsTransactionUsingDebitSundry(false);
        }
    }
}

function ValidationCharging() {
    if (viewModel.ProductID() == ConsProductID.SKNProductIDCon || viewModel.ProductID() == ConsProductID.RTGSProductIDCons) {
        $('#ChAccName').data({ ruleRequired: true });
        $('#ChAccBank').data({ ruleRequired: true });
        $('#ChAccount').data({ ruleRequired: true });
        $('#ChAccount-ccy').data({ ruleRequired: true });
        viewModel.IsChargingMandotary(true);
    } else {
        if (viewModel.ProductID() == ConsProductID.OverbookingProductIDCons && viewModel.Selected().Sundry() != undefined) {
            $('#ChAccName').data({ ruleRequired: false });
            $('#ChAccBank').data({ ruleRequired: false });
            $('#ChAccount').data({ ruleRequired: false });
            $('#ChAccount-ccy').data({ ruleRequired: false });
            viewModel.IsChargingMandotary(false);
        } else {
            $('#ChAccName').data({ ruleRequired: true });
            $('#ChAccBank').data({ ruleRequired: true });
            $('#ChAccount').data({ ruleRequired: true });
            $('#ChAccount-ccy').data({ ruleRequired: true });
            viewModel.IsChargingMandotary(true);
        }
    }
};

function ResetBeneBank() {
    //reset bene bank mapped data
    viewModel.TransactionModel().Bank().SwiftCode('');
    viewModel.TransactionModel().Bank().BankAccount('');
    viewModel.TransactionModel().Bank().Code('999');
    viewModel.IsOtherBank(false);
    viewModel.TransactionModel().IsOtherBeneBank(true);
    $('#bank-code').val('');
    $('#bene-acc-number').val('');
}

function GetPaymentStatus(callback) {
    $.ajax({
        type: "GET",
        url: api.server + api.url.paymentmode,
        success: function (data, textStatus, jqXHR) {
            OnSuccessGetParametersIPE(data);
            if (callback != null) callback(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    });
}

function OnSuccessGetParametersIPE(data) {
    StatusIPE = data;
}

function GetLLDRounding() {
    $.ajax({
        type: "GET",
        url: api.server + api.url.lldroundingamount,
        success: function (data, textStatus, jqXHR) {
            OnSuccessGetParametersLLDRounding(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    });
}

function ClearJenisIdentitas() {
    self.IsIdentitas2 = ko.observable(false);
    self.IsIdentitas3 = ko.observable(false);
    self.IsIdentitas4 = ko.observable(false);
    self.IsAdd = false;
    self.IsAdd2 = false;
    self.IsAdd3 = false;
    self.IsAdd4 = false;
    viewModel.SelectedDDL().IdentityTypeIDs(null);
    viewModel.SelectedDDL().IdentityTypeIDs2(null);
    viewModel.SelectedDDL().IdentityTypeIDs3(null);
    viewModel.SelectedDDL().IdentityTypeIDs4(null);
}

function OnSuccessGetParametersLLDRounding(data) {
    LLDRounding = data;
}

$.holdReady(true);
var intervalRoleName = setInterval(function () {
    if (Object.keys(Const_RoleName).length != 0) {
        $.holdReady(false);
        clearInterval(intervalRoleName);
    }
}, 100);

//$(document).on("RoleNameReady", function() {
$(document).ready(function () {
    //add henggar fo switch mode IPE or BCP
    GetPaymentStatus(function (data) {
        viewModel.TransactionModel().ModePayment(StatusIPE);

        $box = $('#widget-box');
        var event;
        $box.trigger(event = $.Event('reload.ace.widget'))
        if (event.isDefaultPrevented()) return
        $box.blur();

        // block enter key from user to prevent submitted data.
        $(this).keypress(function (event) {
            var code = event.charCode || event.keyCode;
            if (code == 13) {
                ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

                return false;
            }
        });

        // Load all templates
        $('script[src][type="text/html"]').each(function () {
            //alert($(this).attr("src"))
            var src = $(this).attr("src");
            if (src != undefined) {
                $(this).load(src);
            }
        });

        // scrollables
        $('.slim-scroll').each(function () {
            var $this = $(this);
            $this.slimscroll({
                height: $this.data('height') || 100,
                railVisible: true,
                alwaysVisible: true,
                color: '#D15B47'
            });
        });

        // tooltip
        $('[data-rel=tooltip]').tooltip();

        $.fn.ForceNumericOnly = function () {

            return this.each(function () {

                $(this).keydown(function (e) {

                    var key = e.charCode || e.keyCode || 0;

                    return (

                        key == 8 ||
                        key == 9 ||
                        key == 13 ||
                        key == 46 ||
                        key == 110 ||
                        key == 190 ||
                        (key >= 35 && key <= 40) ||
                        (key >= 48 && key <= 57) ||
                        (key >= 96 && key <= 105));

                });

            });

        };
        $(".input-numericonly").ForceNumericOnly();

        $('.btn').each(function () {
            dataBind = $(this).attr('data-bind');
            if (dataBind == "click: SaveAsDraft, disable: !IsEditable(), visible: IsRole('DBS Admin')") {
                $(this).attr('data-bind', "click: SaveAsDraft, disable: !IsEditable(), visible: true")
            }
        });

        /// block enter key from user to prevent submitted data.
        $("input").keypress(function (event) {
            var code = event.charCode || event.keyCode;
            if (code == 13) {
                ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

                return false;
            }
        });

        $('#bank-name').keyup(function (event) {
            var code = event.charCode || event.keyCode;
            if (code == 46 || code == 8) {
                if (event.target.value == '') {
                    viewModel.TransactionModel().Bank().SwiftCode = '';
                    viewModel.TransactionModel().Bank().BankAccount = '';
                    viewModel.TransactionModel().Bank().Code = '999';
                    viewModel.IsOtherBank(false);
                    viewModel.TransactionModel().IsOtherBeneBank(true);
                    $('#bank-code').val('');
                }
            }
        });

        //$('#modal-form-Underlying')
        //console.log(viewModel.IsRole('DBS Admin'));
        $('#aspnetForm').after('<form id="frmUnderlying"></form>');
        //Andi
        $('#aspnetForm').after('<form id="frmFD"></form>');
        //End Andi
        $("#modal-form-Underlying").appendTo("#frmUnderlying");
        $("#modal-form-Attach").appendTo("#frmUnderlying");
        $box = $('#widget-box');
        var event;
        $box.trigger(event = $.Event('reload.ace.widget'))
        if (event.isDefaultPrevented()) return
        $box.blur();

        // datepicker
        $('.date-picker').datepicker({
            autoclose: true,
            onSelect: function () {
                this.focus();
            }
        }).next().on(ace.click_event, function () {
            $(this).prev().focus();
        });

        // validation
        //$('#aspnetForm1').validate({
        $('#aspnetForm').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: true,
            focusCleanup: true,
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },

            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                $(e).remove();
            },

            errorPlacement: function (error, element) {
                if (element.is(':checkbox') || element.is(':radio')) {
                    var controls = element.closest('div[class*="col-"]');
                    if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if (element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if (element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            }
        });

        $('#frmUnderlying').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: true,
            focusCleanup: true,
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },

            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                $(e).remove();
            },

            errorPlacement: function (error, element) {
                if (element.is(':checkbox') || element.is(':radio')) {
                    var controls = element.closest('div[class*="col-"]');
                    if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if (element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if (element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            }
        });
        //Andi
        $('#frmFD').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: true,
            focusCleanup: true,
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },

            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                $(e).remove();
            },

            errorPlacement: function (error, element) {
                if (element.is(':checkbox') || element.is(':radio')) {
                    var controls = element.closest('div[class*="col-"]');
                    if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if (element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if (element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            }
        });

        //End Andi
        // autocomplete customer

        if (viewModel != undefined) {
            viewModel.SetBankAutoCompleted();
            viewModel.SetBankBranchCompleted();
            viewModel.SetBankChagingAccount();
            viewModel.SetLLDAutoCompleted();

        }
        // ace file upload
        $('#document-path').ace_file_input({
            no_file: 'No File ...',
            btn_choose: 'Choose',
            btn_change: 'Change',
            droppable: false,
            //onchange:null,
            thumbnail: false, //| true | large
            //whitelist:'gif|png|jpg|jpeg'
            blacklist: 'exe|dll'
            //onchange:''
            //
        });

        $('#document-path-upload').ace_file_input({
            no_file: 'No File ...',
            btn_choose: 'Choose',
            btn_change: 'Change',
            droppable: false,
            //onchange:null,
            thumbnail: false, //| true | large
            //whitelist:'gif|png|jpg|jpeg'
            blacklist: 'exe|dll'
            //onchange:''
            //
        });

        // Knockout Datepicker custom binding handler
        ko.bindingHandlers.datepicker = {
            init: function (element) {
                $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                    $(this).prev().focus();
                });
            },
            update: function (element) {
                $(element).datepicker("refresh");
            }
        };

        // Knockout custom file handler
        ko.bindingHandlers.file = {
            init: function (element, valueAccessor) {
                $(element).change(function () {
                    var file = this.files[0];

                    if (ko.isObservable(valueAccessor()))
                        valueAccessor()(file);
                });
            }
        };

        //dani 16-1-2016
        //for numberofunit
        ko.bindingHandlers.decimalPlacement = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                ko.utils.registerEventHandler(element, 'change', function (event) {
                    var observable = valueAccessor();
                    observable(CurrencyFormat(element.value));
                    observable.notifySubscribers(5);
                });
            },
            update: function (element, valueAccessor, allBindingsAccessor) {
                var value = ko.utils.unwrapObservable(valueAccessor());
                $(element).val(value);
            }
        };

        ko.bindingHandlers.currencyPlacement = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                ko.utils.registerEventHandler(element, 'change', function (event) {
                    var observable = valueAccessor();
                    var val = Math.round(Number(element.value) * 100) / 100;
                    var parts = val.toString().split(".");
                    var formattedValue = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
                    observable(formattedValue);
                    observable.notifySubscribers(5);
                });
            },
            update: function (element, valueAccessor, allBindingsAccessor) {
                var value = ko.utils.unwrapObservable(valueAccessor());
                $(element).val(value);
            }
        };
        //dani end       

        // Dependant Observable for dropdown auto fill
        if (viewModel != undefined) {
            ko.dependentObservable(function () {

                if (viewModel.Selected().Account() == '-' && !viewModel.TransactionModel().IsNewCustomer()) {
                    viewModel.IsEmptyAccountNumber(true);
                }

                var IsCurrencyChanged;
                var account;
                var product = ko.utils.arrayFirst(viewModel.Parameter().Products(), function (item) {
                    return item.ID == viewModel.Selected().Product();
                });
                var chooseproduct = ko.utils.arrayFirst(viewModel.Parameter().Products(), function (item) {
                    return item.ID == viewModel.Selected().ChooseProduct();
                });
                if (ko.isObservable(viewModel.TransactionModel().Customer)) {
                    if (viewModel.TransactionModel().Customer() != null) {
                        account = ko.utils.arrayFirst(viewModel.TransactionModel().Customer().Accounts, function (item) {
                            if (viewModel.Selected().Account() == '-') {
                                viewModel.TransactionModel().IsOtherAccountNumber(true);
                            } else {
                                viewModel.TransactionModel().IsOtherAccountNumber(false);
                            } return item.AccountNumber == viewModel.Selected().Account();
                        });
                    }
                }
                var debitcurrency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) {
                    if (viewModel.Selected().Account() == '-') {
                        viewModel.TransactionModel().IsOtherAccountNumber(false);
                        return item.ID == viewModel.Selected().DebitCurrency();
                    } else {
                        viewModel.TransactionModel().IsOtherAccountNumber(true);
                        if (account != null) {
                            return item.ID == account.Currency.ID;
                        }
                    }
                });
                var channel = ko.utils.arrayFirst(viewModel.Parameter().Channels(), function (item) {
                    return item.ID == viewModel.Selected().Channel();
                });

                //started by haqi
                var transactionType = ko.utils.arrayFirst(viewModel.Parameter().TransactionTypes(), function (item) {
                    return item.TransTypeID == viewModel.Selected().TransactionType();
                });
                var maintenanceType = ko.utils.arrayFirst(viewModel.Parameter().MaintenanceTypes(), function (item) {
                    return item.ID == viewModel.Selected().MaintenanceType();
                });
                var cellPhoneMethodID = ko.utils.arrayFirst(viewModel.Parameter().CellPhoneMethodIDs(), function (item) {
                    return item.ID == viewModel.Selected().CellPhoneMethodID();
                });
                var homePhoneMethodID = ko.utils.arrayFirst(viewModel.Parameter().HomePhoneMethodIDs(), function (item) {
                    return item.ID == viewModel.Selected().HomePhoneMethodID();
                });
                var officePhoneMethodID = ko.utils.arrayFirst(viewModel.Parameter().OfficePhoneMethodIDs(), function (item) {
                    return item.ID == viewModel.Selected().OfficePhoneMethodID();
                });
                var faxMethodID = ko.utils.arrayFirst(viewModel.Parameter().FaxMethodIDs(), function (item) {
                    return item.ID == viewModel.Selected().FaxMethodID();
                });
                var maritalStatusID = ko.utils.arrayFirst(viewModel.Parameter().MaritalStatusIDs(), function (item) {
                    return item.ID == viewModel.Selected().MaritalStatusID();
                });
                var source = ko.utils.arrayFirst(viewModel.Parameter().Sources(), function (item) {
                    return item.ID == viewModel.Selected().Source();
                });
                var grossIncome = ko.utils.arrayFirst(viewModel.Parameter().GrossIncomeCCY(), function (item) {
                    return item.ID == viewModel.Selected().GrossIncomeCCY();
                });
                var dispatchModeID = ko.utils.arrayFirst(viewModel.Parameter().DispatchModeTypes(), function (item) {
                    return item.ID == viewModel.Selected().DispatchModeType();
                });
                var transactionSubType = ko.utils.arrayFirst(viewModel.Parameter().TransactionSubTypes(), function (item) {
                    return item.ID == viewModel.Selected().TransactionSubType();
                });
                var identityTypeID = ko.utils.arrayFirst(viewModel.Parameter().IdentityTypeIDs(), function (item) {
                    return item.ID == viewModel.Selected().IdentityTypeID();
                });
                var identityTypeID2 = ko.utils.arrayFirst(viewModel.Parameter().IdentityTypeIDs(), function (item) {
                    return item.ID == viewModel.Selected().IdentityTypeID2();
                });
                var identityTypeID3 = ko.utils.arrayFirst(viewModel.Parameter().IdentityTypeIDs(), function (item) {
                    return item.ID == viewModel.Selected().IdentityTypeID3();
                });
                var identityTypeID4 = ko.utils.arrayFirst(viewModel.Parameter().IdentityTypeIDs(), function (item) {
                    return item.ID == viewModel.Selected().IdentityTypeID4();
                });
                var fundSource = ko.utils.arrayFirst(viewModel.Parameter().FundSources(), function (item) {
                    return item.ID == viewModel.Selected().FundSource();
                });
                var netAsset = ko.utils.arrayFirst(viewModel.Parameter().NetAssets(), function (item) {
                    return item.ID == viewModel.Selected().NetAsset();
                });
                var monthlyIncome = ko.utils.arrayFirst(viewModel.Parameter().MonthlyIncomes(), function (item) {
                    return item.ID == viewModel.Selected().MonthlyIncome();
                });
                var monthlyExtraIncome = ko.utils.arrayFirst(viewModel.Parameter().MonthlyExtraIncomes(), function (item) {
                    return item.ID == viewModel.Selected().MonthlyExtraIncome();
                });
                var job = ko.utils.arrayFirst(viewModel.Parameter().Jobs(), function (item) {
                    return item.ID == viewModel.Selected().Job();
                });
                var accountPurpose = ko.utils.arrayFirst(viewModel.Parameter().AccountPurposes(), function (item) {
                    return item.ID == viewModel.Selected().AccountPurpose();
                });
                var incomeForecast = ko.utils.arrayFirst(viewModel.Parameter().IncomeForecasts(), function (item) {
                    return item.ID == viewModel.Selected().IncomeForecast();
                });
                var outcomeForecast = ko.utils.arrayFirst(viewModel.Parameter().OutcomeForecasts(), function (item) {
                    return item.ID == viewModel.Selected().OutcomeForecast();
                });
                var transactionForecast = ko.utils.arrayFirst(viewModel.Parameter().TransactionForecasts(), function (item) {
                    return item.ID == viewModel.Selected().TransactionForecast();
                });
                var currencycif = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) {
                    return item.ID == viewModel.Selected().Currency();
                });
                var currencyCifLienUnlien = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) {
                    return item.ID == viewModel.Selected().CurrencyLien();
                });
                var riskRatingResult = ko.utils.arrayFirst(viewModel.Parameter().RiskRatingResults(), function (item) {
                    return item.ID == viewModel.Selected().RiskRatingResult();
                });
                var cifAccountNumber = ko.utils.arrayFirst(viewModel.CIFAccountsNumber(), function (item) {
                    return item.AccountNumber == viewModel.Selected().cifAccNumber();
                });

                var sTagging = ko.utils.arrayFirst(viewModel.Parameter().DynamicTagUntages(), function (item) {
                    return item.ID == viewModel.Selected().TagUntag();
                });

                if (sTagging != null) viewModel.CIFTransactionModel().StaffTagging(sTagging);


                //end by haqi

                // Tambah Agung
                var join = ko.utils.arrayFirst(viewModel.ParameterUT().Joins(), function (item) {
                    return item.ID == viewModel.Selected().Join();
                });
                var fnacore = ko.utils.arrayFirst(viewModel.ParameterUT().FNACores(), function (item) {
                    return item.ID == viewModel.Selected().FNACore();
                });
                var functiontype = ko.utils.arrayFirst(viewModel.ParameterUT().FunctionTypes(), function (item) {
                    return item.ID == viewModel.Selected().FunctionType();
                });
                var accounttype = ko.utils.arrayFirst(viewModel.ParameterUT().AccountTypes(), function (item) {
                    return item.ID == viewModel.Selected().AccountType();
                });
                var transaction_type = ko.utils.arrayFirst(viewModel.ParameterUT().TransactionTypes(), function (item) {
                    return item.TransTypeID == viewModel.Selected().Transaction_Type();
                });
                //End Agung

                var bizSegment = ko.utils.arrayFirst(viewModel.Parameter().BizSegments(), function (item) {
                    return item.ID == viewModel.Selected().BizSegment();
                });
                //var bank = ko.utils.arrayFirst(viewModel.Parameter().Banks(), function (item){ return item.ID == viewModel.Selected().Bank(); });
                var lld = ko.utils.arrayFirst(viewModel.Parameter().LLDs(), function (item) {
                    return item.ID == viewModel.Selected().LLD();
                });
                var llddocument = ko.utils.arrayFirst(viewModel.Parameter().LLDDocuments(), function (item) {
                    return item.ID == viewModel.Selected().LLDDocument();
                });
                var underlyingDoc = ko.utils.arrayFirst(viewModel.Parameter().UnderlyingDocs(), function (item) {
                    return item.ID == viewModel.Selected().UnderlyingDoc();
                });
                var fxcomplianceCode = ko.utils.arrayFirst(viewModel.Parameter().FXCompliances(), function (item) {
                    return item.ID == viewModel.Selected().FXCompliance();
                })
                var ChargingAccountCurrency = ko.utils.arrayFirst(viewModel.Parameter().ChargingAccountCurrencies(), function (item) {
                    return item.ID == viewModel.Selected().ChargingAccountCurrency();
                })


                var docPurpose = ko.utils.arrayFirst(viewModel.Parameter().DocumentPurposes(), function (item) {
                    return item.ID == viewModel.Selected().DocumentPurpose();
                });
                var docType = ko.utils.arrayFirst(viewModel.Parameter().DocumentTypes(), function (item) {
                    return item.ID == viewModel.Selected().DocumentType();
                });
                //Edit Rizki 2015-12-01
                var bankCharges = ko.utils.arrayFirst(viewModel.Parameter().BankCharges(), function (item) {
                    //if (!viewModel.IsLoadDraftPayment()) { //Remark Rizki 2016-02-14
                    return item.ID == viewModel.Selected().BankCharges();
                    // }
                });
                var agentCharges = ko.utils.arrayFirst(viewModel.Parameter().AgentCharges(), function (item) {
                    //if (!viewModel.IsLoadDraftPayment()) { //Remark Rizki 2016-02-14
                    return item.ID == viewModel.Selected().AgentCharges();
                    //}
                });
                //End Rizki
                var productType = ko.utils.arrayFirst(viewModel.Parameter().ProductType(), function (item) {
                    if (viewModel.Selected().Account() == '-')//&&  == false)
                    {
                        viewModel.TransactionModel().IsOtherAccountNumber(true);
                        viewModel.IsEmptyAccountNumber(false);
                    } else {
                        viewModel.TransactionModel().IsOtherAccountNumber(false);
                        viewModel.IsEmptyAccountNumber(true);
                    } return item.ID == viewModel.Selected().ProductType();
                });
                if (viewModel.DocumentPurpose_a() != null) {
                    if (viewModel.DocumentPurpose_a().ID() == 2) {
                        viewModel.SelectingUnderlying(true);
                        viewModel.SelectingInstruction(false);
                    }
                    else {
                        viewModel.SelectingUnderlying(false);
                        viewModel.SelectingInstruction(true);
                    }
                }
                if (account != null) {
                    viewModel.TransactionModel().Account(account);
                }
                if (productType != null) {
                    if (viewModel.TransactionModel().IsNewCustomer()) {
                        var new_currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().NewCustomer().Currency(); });
                        if (new_currency != null) viewModel.TransactionModel().Account().Currency = new_currency;
                    }

                    viewModel.TransactionModel().ProductType(productType);

                    if (!viewModel.TransactionModel().IsOtherAccountNumber())
                        IsCurrencyChanged = viewModel.TransactionModel().Account().Currency.Code;
                    else
                        IsCurrencyChanged = viewModel.TransactionModel().DebitCurrency().Code;

                    var IsFxTransaction = (viewModel.TransactionModel().Currency().Code != 'IDR' && IsCurrencyChanged == 'IDR');
                    var IsFxTransactionToIDR = (viewModel.TransactionModel().Currency().Code == 'IDR' && IsCurrencyChanged != 'IDR');


                    viewModel.IsFxTransaction(IsFxTransaction);
                    viewModel.IsFxTransactionToIDR(IsFxTransactionToIDR);
                }

                if (underlyingDoc != null) {
                    viewModel.TransactionModel().UnderlyingDoc(underlyingDoc);
                    viewModel.isNewUnderlying(false); //return default disabled

                    // available type to input for new underlying name if code is 999
                    if (underlyingDoc.Code == '999') {
                        viewModel.TransactionModel().OtherUnderlyingDoc('');
                        viewModel.isNewUnderlying(true);
                        $('#underlying-desc').focus();

                        $('#underlying-desc').attr('data-rule-required', true);
                    }
                    else {
                        viewModel.isNewUnderlying(false);
                        //viewModel.TransactionModel().OtherUnderlyingDoc('');

                        $('#underlying-desc').attr('data-rule-required', false);
                    }
                } else {
                    viewModel.TransactionModel().UnderlyingDoc({ ID: 0, Code: null, Name: null, Description: null });
                }

                if (fxcomplianceCode != null) {
                    viewModel.TransactionModel().Compliance(fxcomplianceCode);
                } else {
                    viewModel.TransactionModel().Compliance({ ID: 0, Name: null, Description: null });
                }

                //Tambahan IPE Agung
                var sundrydata = ko.utils.arrayFirst(viewModel.Parameter().Sundries(), function (item) {
                    return item.ID == viewModel.Selected().Sundry();
                })
                var beneficiarycountry = ko.utils.arrayFirst(viewModel.Parameter().BeneficiaryCountry(), function (item) {
                    return item.ID == viewModel.Selected().BeneficiaryCountry();
                })
                var transactionrelationship = ko.utils.arrayFirst(viewModel.Parameter().TransactionRelationship(), function (item) {
                    return item.ID == viewModel.Selected().TransactionRelationship();
                })
                var cbgcustomer = ko.utils.arrayFirst(viewModel.Parameter().CBGCustomer(), function (item) {
                    return item.ID == viewModel.Selected().CBGCustomer();
                })
                var transactionusingdebitsundry = ko.utils.arrayFirst(viewModel.Parameter().TransactionUsingDebitSundry(), function (item) {
                    return item.ID == viewModel.Selected().TransactionUsingDebitSundry();
                })

                var nostro = ko.utils.arrayFirst(viewModel.Parameter().Nostroes(), function (item) {
                    return item.ID == viewModel.Selected().Nostro();
                })

                if (nostro != null) {
                    viewModel.TransactionModel().Nostro(nostro);
                }
                if (sundrydata != null) {
                    viewModel.TransactionModel().Sundry(sundrydata);
                }

                if (beneficiarycountry != null) {
                    viewModel.TransactionModel().BeneficiaryCountry(beneficiarycountry);
                }
                if (transactionrelationship != null) {
                    viewModel.TransactionModel().TransactionRelationship(transactionrelationship);
                }
                if (cbgcustomer != null) {
                    viewModel.TransactionModel().CBGCustomer(cbgcustomer);
                }
                if (transactionusingdebitsundry != null) {
                    viewModel.TransactionModel().TransactionUsingDebitSundry(transactionusingdebitsundry);
                }
                //End


                var currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) {
                    if (viewModel.Selected().Account() == '-') { //&& viewModel.IsEmptyAccountNumber() == false) {
                        viewModel.TransactionModel().IsOtherAccountNumber(true);
                        viewModel.IsEmptyAccountNumber(true);
                    } else {
                        viewModel.TransactionModel().IsOtherAccountNumber(false);
                        viewModel.IsEmptyAccountNumber(false);
                    }
                    return item.ID == viewModel.Selected().Currency();
                });

                if (debitcurrency != null) {
                    if (!viewModel.TransactionModel().IsOtherAccountNumber())
                        IsCurrencyChanged = viewModel.TransactionModel().Account().Currency.Code;
                    else
                        //IsCurrencyChanged = viewModel.TransactionModel().DebitCurrency().Code;
                        IsCurrencyChanged = viewModel.TransactionModel().DebitCurrency().Code;
                    if (viewModel.TransactionModel().IsNewCustomer()) {
                        var new_currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().NewCustomer().Currency(); });
                        if (new_currency != null) viewModel.TransactionModel().Account().Currency = new_currency;
                    }

                    viewModel.TransactionModel().DebitCurrency(debitcurrency);
                    //viewModel.GetRateAmount(currency.ID);

                    if (!viewModel.TransactionModel().IsOtherAccountNumber())
                        IsCurrencyChanged = viewModel.TransactionModel().Account().Currency.Code;
                    else
                        IsCurrencyChanged = viewModel.TransactionModel().DebitCurrency().Code;

                    var IsFxTransaction = (viewModel.TransactionModel().Currency().Code != 'IDR' && IsCurrencyChanged == 'IDR');
                    var IsFxTransactionToIDR = (viewModel.TransactionModel().Currency().Code == 'IDR' && IsCurrencyChanged != 'IDR');
                    viewModel.IsFxTransaction(IsFxTransaction);
                    viewModel.IsFxTransactionToIDR(IsFxTransactionToIDR);
                }

                if (currency != null) {
                    if (viewModel.TransactionModel().IsNewCustomer()) {
                        var new_currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().NewCustomer().Currency(); });
                        if (new_currency != null) viewModel.TransactionModel().Account().Currency = new_currency;
                    }
                    viewModel.TransactionModel().Currency(currency);

                    //Dani
                    viewModel.TransactionTMOModel().Currency(currency);
                    //End Dani

                    //Afif
                    viewModel.TransactionLoanModel().Currency(currency);
                    //End Afif

                    viewModel.GetRateAmount(currency.ID);

                    if (!viewModel.TransactionModel().IsOtherAccountNumber()) {
                        if (viewModel.TransactionModel().Account().Currency.Code != null) { //Rizki - 2016-02-11
                            IsCurrencyChanged = viewModel.TransactionModel().Account().Currency.Code;
                        }
                        else {
                            IsCurrencyChanged = "";
                        }
                    }
                    else {
                        if (viewModel.TransactionModel().DebitCurrency().Code != null) {
                            IsCurrencyChanged = viewModel.TransactionModel().DebitCurrency().Code;
                        }
                        else {
                            IsCurrencyChanged = "";
                        }
                    }

                    var IsFxTransaction = (viewModel.TransactionModel().Currency().Code != 'IDR' && IsCurrencyChanged == 'IDR');
                    var IsFxTransactionToIDR = (viewModel.TransactionModel().Currency().Code == 'IDR' && IsCurrencyChanged != 'IDR' && IsCurrencyChanged != "");
                    viewModel.IsFxTransaction(IsFxTransaction);
                    viewModel.IsFxTransactionToIDR(IsFxTransactionToIDR);
                    if (IsFxTransaction == true || IsFxTransactionToIDR == true) {
                        if (viewModel.TransactionModel().IsIPETMOPartial() == true) {
                            $("#btn-updateunderlying").hide();
                        }
                    }
                }

                //Started by haqi
                if (transactionType != null) viewModel.CIFTransactionModel().TransactionType(transactionType);
                if (maintenanceType != null) viewModel.CIFTransactionModel().MaintenanceType(maintenanceType);
                if (maritalStatusID != null) {
                    viewModel.CIFTransactionModel().RetailCIFCBO().MaritalStatusID(maritalStatusID);
                    switch (maritalStatusID.ID) {
                        case ConsMaritalStatus.SudahMenikah:
                            viewModel.IsMaritalStatusSudahMenikah(true);
                            break;
                        case ConsMaritalStatus.BelumMenikah:
                        case ConsMaritalStatus.DudaJanda:
                            viewModel.IsMaritalStatusSudahMenikah(false);
                        default:
                            break;
                    }
                }
                if (dispatchModeID != null) viewModel.CIFTransactionModel().RetailCIFCBO().DispatchModeType(dispatchModeID);
                if (cellPhoneMethodID != null) viewModel.CIFTransactionModel().RetailCIFCBO().CellPhoneMethodID(cellPhoneMethodID);
                if (homePhoneMethodID != null) viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneMethodID(homePhoneMethodID);
                if (officePhoneMethodID != null) viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneMethodID(officePhoneMethodID);
                if (faxMethodID != null) viewModel.CIFTransactionModel().RetailCIFCBO().FaxMethodID(faxMethodID);
                if (transactionSubType != null) viewModel.CIFTransactionModel().TransactionSubType(transactionSubType);
                if (currencycif != null) viewModel.CIFTransactionModel().Currency(currencycif);
                if (viewModel.CIFTransactionModel().MaintenanceType().ID == ConsCIFMaintenanceType.LienUnlien) {
                    if (currencyCifLienUnlien != null) { viewModel.CIFTransactionModel().Currency(currencyCifLienUnlien) } else { viewModel.CIFTransactionModel().Currency(new CurrencyModel2(0, '', '')) };
                }
                if (identityTypeID != null) viewModel.CIFTransactionModel().RetailCIFCBO().IdentityTypeID(identityTypeID);
                if (identityTypeID2 != null) viewModel.CIFTransactionModel().RetailCIFCBO().IdentityTypeID2(identityTypeID2);
                if (identityTypeID3 != null) viewModel.CIFTransactionModel().RetailCIFCBO().IdentityTypeID3(identityTypeID3);
                if (identityTypeID4 != null) viewModel.CIFTransactionModel().RetailCIFCBO().IdentityTypeID4(identityTypeID4);
                if (fundSource != null) viewModel.CIFTransactionModel().RetailCIFCBO().FundSource(fundSource);
                if (netAsset != null) viewModel.CIFTransactionModel().RetailCIFCBO().NetAsset(netAsset);
                if (monthlyIncome != null) viewModel.CIFTransactionModel().RetailCIFCBO().MonthlyIncome(monthlyIncome);
                if (monthlyExtraIncome != null) viewModel.CIFTransactionModel().RetailCIFCBO().MonthlyExtraIncome(monthlyExtraIncome);
                if (job != null) viewModel.CIFTransactionModel().RetailCIFCBO().Job(job);
                if (accountPurpose != null) viewModel.CIFTransactionModel().RetailCIFCBO().AccountPurpose(accountPurpose);
                if (incomeForecast != null) viewModel.CIFTransactionModel().RetailCIFCBO().IncomeForecast(incomeForecast);
                if (outcomeForecast != null) viewModel.CIFTransactionModel().RetailCIFCBO().OutcomeForecast(outcomeForecast);
                if (transactionForecast != null) viewModel.CIFTransactionModel().RetailCIFCBO().TransactionForecast(transactionForecast);
                if (riskRatingResult != null) viewModel.CIFTransactionModel().RetailCIFCBO().RiskRatingResult(riskRatingResult);

                if (cifAccountNumber != null) {
                    viewModel.CIFTransactionModel().AccountNumber(cifAccountNumber);
                }
                //End by haqi

                if (channel != null) {
                    viewModel.TransactionModel().Channel(channel);
                    //Dani
                    viewModel.TransactionTMOModel().Channel(channel);
                    viewModel.FDModel().Channel(channel);
                    //End Dani

                    //Afif
                    viewModel.TransactionLoanModel().Channel(channel);
                    //End Afif
                }

                if (source != null) {
                    viewModel.TransactionModel().Source(source);
                    viewModel.FDModel().Source(source);
                    viewModel.TransactionLoanModel().Source(source);
                }
                //Agung
                if (join != null) {
                    viewModel.TransactionUTModel().Join(join);
                }
                if (fnacore != null) {
                    viewModel.TransactionUTModel().FNACore(fnacore);
                }
                if (functiontype != null) {
                    viewModel.TransactionUTModel().FunctionType(functiontype);
                }
                if (accounttype != null) {
                    viewModel.TransactionUTModel().AccountType(accounttype);
                }
                if (transaction_type != null) {
                    viewModel.TransactionUTModel().Transaction_Type(transaction_type);
                }
                //Agung
                if (bizSegment != null) {
                    viewModel.TransactionModel().BizSegment(bizSegment);
                    //Dani
                    viewModel.TransactionTMOModel().BizSegment(bizSegment);
                    //End Dani

                    //Afif
                    viewModel.TransactionLoanModel().BizSegment(bizSegment);
                    //End Afif
                }
                //if(bank != null) viewModel.TransactionModel().Bank(bank);
                if (lld != null) {
                    viewModel.TransactionModel().LLD(lld);
                }
                else {
                    viewModel.TransactionModel().LLD({ ID: 0, Code: null, Description: null });
                }
                if (account != null) {
                    if (viewModel.TransactionModel().IsNewCustomer()) {
                        var new_currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().NewCustomer().Currency(); });
                        if (new_currency != null) viewModel.TransactionModel().Account().Currency = new_currency;
                    }
                    viewModel.TransactionModel().Account(account);

                    if (!viewModel.TransactionModel().IsOtherAccountNumber()) {
                        IsCurrencyChanged = viewModel.TransactionModel().Account().Currency.Code;
                    }
                    else {
                        IsCurrencyChanged = viewModel.TransactionModel().DebitCurrency().Code;
                    }

                    var IsFxTransaction = (viewModel.TransactionModel().Currency().Code != 'IDR' && IsCurrencyChanged == 'IDR');
                    var IsFxTransactionToIDR = (viewModel.TransactionModel().Currency().Code == 'IDR' && IsCurrencyChanged != 'IDR');
                    viewModel.IsFxTransaction(IsFxTransaction);
                    viewModel.IsFxTransactionToIDR(IsFxTransactionToIDR);
                };

                if (product != null) {
                    viewModel.TransactionModel().Product(product);
                    //Dani
                    viewModel.TransactionTMOModel().Product(product);
                    //End Dani
                    //Afif
                    viewModel.TransactionLoanModel().Product(product);
                    //End Afif
                    viewModel.TransactionUTModel().Product(product);
                    viewModel.CIFTransactionModel().Product(product);
                    var productSelected = viewModel.TransactionModel().Product().Name;
                    if (!viewModel.IsLoadDraftPayment()) { //Rizki 2015-12-01
                        if (productSelected == 'RTGS') {
                            viewModel.Selected().BankCharges(viewModel.TransactionModel().BankCharges);
                            //viewModel.Selected().AgentCharges(viewModel.TransactionModel().AgentCharges);
                        } else if (productSelected == 'OTT' || productSelected == 'SKN') {
                            viewModel.Selected().BankCharges(viewModel.TransactionModel().BankCharges);
                            viewModel.Selected().AgentCharges(viewModel.TransactionModel().AgentCharges);
                        }
                    }


                }
                if (chooseproduct != null) {
                    viewModel.TransactionModel().Product(chooseproduct);
                    //Andi
                    viewModel.FDModel().Product(chooseproduct);
                    //End Andi
                    viewModel.TransactionTMOModel().Product(chooseproduct);
                    viewModel.TransactionLoanModel().Product(chooseproduct);
                    viewModel.TransactionUTModel().Product(chooseproduct);

                    viewModel.Selected().Product(chooseproduct.ID)
                    viewModel.ProductID(chooseproduct.ID);
                    viewModel.ProductTitle(chooseproduct.Name);
                }
                if (docType != null) viewModel.DocumentType(docType);


                if (docPurpose != null) viewModel.DocumentPurpose(docPurpose);
                if (bankCharges != null) viewModel.TransactionModel().BankCharges(bankCharges);
                if (agentCharges != null) viewModel.TransactionModel().AgentCharges(agentCharges);
                //henggar bene bank filled if product selected is over booking
                //if (viewModel.TransactionModel().Product().ID == Const_OverBookingProduct.SelectedProduct) {
                // GetBankAllTunning();
                // var new_BeneBank = ko.utils.arrayFirst(DataBank, function (item) { return item.ID == Const_OverBookingProduct.SelectedBeneBank });
                //if (new_BeneBank != null) viewModel.TransactionModel().Bank(new_BeneBank); 
                //}

                if (viewModel.TransactionModel().Product().ID == Const_OverBookingProduct.SelectedProduct) {
                    viewModel.IsDraft(true)
                    viewModel.IsOtherBank(true)
                    viewModel.SetBankAutoCompleted();
                    viewModel.SetBankBranchCompleted();
                    viewModel.SetBankChagingAccount();
                }
                else {
                    viewModel.IsDraft(false)
                    //viewModel.IsOtherBank(false)
                }
                //end henggar

                if (viewModel.TransactionModel().IsNewCustomer()) {
                    var new_currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().NewCustomer().Currency(); });
                    var new_bizSegment = ko.utils.arrayFirst(viewModel.Parameter().BizSegments(), function (item) { return item.ID == viewModel.Selected().NewCustomer().BizSegment(); });

                    if (new_currency != null) viewModel.TransactionModel().Account().Currency = new_currency;
                    if (new_bizSegment != null) viewModel.TransactionModel().Customer().BizSegment(new_bizSegment);
                }

                if (viewModel.TransactionModel().IsNewCustomer() && !viewModel.IsLoadDraft() && (viewModel.IsFxTransaction() || viewModel.IsFxTransactionToIDR())) {
                    ShowNotification("Page Information", NewCustomerFXWarning, "gritter-warning", false);
                    //NewCustomerFXWarning
                }

                //Andi
                var TType = ko.utils.arrayFirst(viewModel.Parameter().TransactionType(), function (item) {
                    return item.TransTypeID == viewModel.Selected().TransactionType();
                });
                var fdRemarks = ko.utils.arrayFirst(viewModel.Parameter().FDRemarks(), function (item) {
                    return item.ID == viewModel.Selected().FDRemarks();
                });
                if (TType != null) viewModel.TransactionType(TType);
                if (fdRemarks != null) viewModel.FDRemarks(fdRemarks);
                if (currency != null) viewModel.Currencies(currency);
                if (product != null) {
                    viewModel.FDModel().Product(product);
                }
                if (ChargingAccountCurrency !== null && ChargingAccountCurrency !== undefined) {
                    viewModel.TransactionModel().ChargingAccountCurrency = ChargingAccountCurrency.ID;
                }
                //End Andi
                if (viewModel.FDModel().DocsComplete() === true) {
                    viewModel.IsDocsComplete(true);
                    viewModel.FDModel().InterestRate('');
                    $("#tenor-fd").data({ ruleRequired: true });
                } else {
                    viewModel.IsDocsComplete(false);
                    $("#tenor-fd").data({ ruleRequired: false });
                }
                var quotefd = ko.utils.arrayFirst(viewModel.Parameter().TenorFDs(), function (item) {
                    return item.IQuoteID == viewModel.Selected().TenorFD();
                });
                if (quotefd != null) {
                    viewModel.FDModel().Tenor(quotefd.Tenor);
                    //console.log(quotefd);
                    var currencyNow = viewModel.Currencies().Code;
                    //console.log(currencyNow);
                    //console.log(quotefd[currencyNow]);
                    if (quotefd[currencyNow] !== undefined && quotefd[currencyNow] !== '' && quotefd[currencyNow] !== null) {
                        viewModel.FDModel().FTPRate(quotefd[currencyNow]);
                    } else {
                        viewModel.FDModel().FTPRate('-');
                    }
                } else if (viewModel.IsDraftForm() !== true) {
                    viewModel.FDModel().FTPRate('');
                    viewModel.FDModel().Tenor('');
                }
            });
        }
        //viewModel['Calculate'] = calculate();
        //================================= End Eqv Calculation ====================================
        self.SPUser = ko.observable();
        // Token Validation
        if ($.cookie(api.cookie.name) == undefined) {
            Helper.Token.Request(OnSuccessToken, OnError);
        } else {
            // read token from cookie
            accessToken = $.cookie(api.cookie.name);

            // read spuser from cookie
            if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
                spUser = $.cookie(api.cookie.spUser);
                self.SPUser(ko.mapping.toJS(spUser));
            }

            // call get data inside view model
            GetParameters();

            //PPUModel.token = accessToken;
            //GetParameterData(PPUModel, OnSuccessGetTotal, OnErrorDeal);
            PPUModel.token = accessToken;
            GetThresholdParameter(PPUModel, OnSuccessThresholdPrm, OnErrorDeal);
            ResetBeneBank();
            GetEmployeeLocation();
        }
        GetPaymentStatus();
        ko.applyBindings(viewModel);

        //ko.applyBindings(viewModel, document.getElementById('new-transaction'));
        GetRateIDR();
        function GetRateIDR() {
            var options = {
                url: api.server + api.url.currency + "/CurrencyRate/" + "1",
                params: {
                },
                token: accessToken
            };

            Helper.Ajax.Get(options, OnSuccessGetRateIDR, OnError, OnAlways);
        }
        //================================= Eqv eion ====================================        
    });
    //end switch mode
});


//Started by haqi
function GetAllParameterSystems() {
    var options = {
        url: api.server + api.url.parametersystemretailcif,
        params: {
            select: "CIFMaintencanceParam"
        },
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetAllParameterSystems, OnError, OnAlways);

    var TT = {
        url: api.server + api.url.transactiontype + "/Product/" + viewModel.ProductID(),
        params: {
        },
        token: accessToken
    };

    Helper.Ajax.Get(TT, OnSuccessGetTransactionTypesCIF, OnError, OnAlways);

}
function OnSuccessGetTransactionTypesCIF(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        viewModel.Parameter().TransactionTypes(ko.mapping.toJS(data.TransactionType));
        FilterCIFCCRequestType();
        if (viewModel.SelectedDDL().RequestType() != 0 || viewModel.SelectedDDL().RequestType() != null || viewModel.SelectedDDL().RequestType() != undefined) {
            viewModel.Selected().TransactionType(viewModel.SelectedDDL().RequestType())
            viewModel.OnCIFRequestTypeChange();
        }
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

}
//end by haqi

// get all parameters need
function GetParameters() {
    var options = {
        url: api.server + api.url.parameter,
        params: {
            // select: "TransactionSubType,MaintenanceType,Product,Currency,Channel,BizSegment,Bank,BankCharge,AgentCharge,FXCompliance,ChargesType,DocType,PurposeDoc,statementletter,underlyingdoc,customer,lld,producttype,Sundry,Nostro,BeneficiaryBusines,ChargingAccountCurrency,beneficiarycountry,transactionrelationship"
            //select: "lld,underlyingdoc,FXCompliance,Product,Channel,Currency,Sundry,Nostro,beneficiarycountry,transactionrelationship,BizSegment,ChargesType,DocType,producttype,BeneficiaryBusines,TransactionSubType,MaintenanceType,PurposeDoc,statementletter"
            select: "lld,LLDDocument,underlyingdoc,FXCompliance,Product,Channel,Currency,Sundry,Nostro,beneficiarycountry,transactionrelationship,BizSegment,ChargesType,DocType,producttype,BeneficiaryBusines,TransactionSubType,MaintenanceType,PurposeDoc,statementletter"
        },
        token: accessToken
    };
    Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
}

function GetParameterbyProduct() {
    var IDProduct = viewModel.ProductID();
    var ProductSelected;
    var Status = false;

    var productSelect = function (id, name) {
        var self = this;
        self.id = id;
        self.name = name;
    }

    self.optionsProduct = [
    new productSelect(ConsProductID.RTGSProductIDCons, "RTGS"),
    new productSelect(ConsProductID.OTTProductIDCons, "OTT"),
    new productSelect(ConsProductID.SKNProductIDCons, "SKN"),
    new productSelect(ConsProductID.OverbookingProductIDCons, "OVB")]

    for (var i = 0; i < self.optionsProduct.length; i++) {
        if (self.optionsProduct[i].id == IDProduct) {
            ProductSelected = self.optionsProduct[i].name;
            Status = true;
        }
    }

    var options = {
        url: api.server + api.url.lldbyProduct,
        params: {
            id: IDProduct,
            name: ProductSelected
        },
        token: accessToken
    };
    Helper.Ajax.Get(options, OnSuccessGetParametersbyID, OnError, OnAlways);
}

//Agung
function GetParameterSystems() {
    var FNA = {
        url: api.server + api.url.parametersystem + "/partype/" + ConsPARSYS.utFNACore,
        params: {
        },
        token: accessToken
    };

    Helper.Ajax.Get(FNA, OnSuccessGetFNACores, OnError, OnAlways);

    var FT = {
        url: api.server + api.url.parametersystem + "/partype/" + ConsPARSYS.utFunctionType,
        params: {
        },
        token: accessToken
    };

    Helper.Ajax.Get(FT, OnSuccessGetFunctionTypes, OnError, OnAlways);

    var AT = {
        url: api.server + api.url.parametersystem + "/partype/" + ConsPARSYS.utAccType,
        params: {
        },
        token: accessToken
    };

    Helper.Ajax.Get(AT, OnSuccessGetAccountTypes, OnError, OnAlways);

    var TT = {
        url: api.server + api.url.transactiontype + "/Product/" + viewModel.ProductID(),
        params: {
        },
        token: accessToken
    };

    Helper.Ajax.Get(TT, OnSuccessGetTransactionTypes, OnError, OnAlways);

    var MaxRisk = {
        url: api.server + api.url.parametersystem + "/partype/" + ConsPARSYS.utMaxRisk,
        params: {
        },
        token: accessToken
    };

    Helper.Ajax.Get(MaxRisk, OnSuccessGetMaxRisk, OnError, OnAlways);

    var MinRisk = {
        url: api.server + api.url.parametersystem + "/partype/" + ConsPARSYS.utMinRisk,
        params: {
        },
        token: accessToken
    };

    Helper.Ajax.Get(MinRisk, OnSuccessGetMinRisk, OnError, OnAlways);
}
//End Agung
function SetDefaultValueStatementA() {
    var today = Date.now();
    var documentType = ko.utils.arrayFirst(viewModel.ddlDocumentType_u(), function (item) { return item.Name == '-' });
    var underlyingDocument = ko.utils.arrayFirst(viewModel.ddlUnderlyingDocument_u(), function (item) { return item.Code() == 998 });

    if (documentType != null) {
        viewModel.DocumentType_u(new DocumentTypeModel2(documentType.ID, documentType.Name));
    }

    if (underlyingDocument != null) {
        viewModel.UnderlyingDocument_u(new UnderlyingDocumentModel2(underlyingDocument.ID(), underlyingDocument.Name(), underlyingDocument.Code()));
    }
    viewModel.DateOfUnderlying_u(viewModel.LocalDate(today, true, false));
    viewModel.SupplierName_u('-');
    viewModel.InvoiceNumber_u('-');
}
function LoadDraft(PurposeDoc) {
    ar = window.location.hash.split('#');
    if (ar.length < 2) {
        viewModel.IsDraftForm(false);
        return;
    }

    viewModel.IsDraftForm(true);
    var uri = '';
    if (ar[2] == "TMOIPE") {
        uri = api.server + api.url.transactiondrafttmoipe + '/' + ar[1];
    } else {
        switch (Number(ar[2])) {
            case ConsProductID.RTGSProductIDCons:
            case ConsProductID.SKNProductIDCons: // SKN Singgle Active
            case ConsProductID.OTTProductIDCons:
            case ConsProductID.OverbookingProductIDCons:
                if (StatusIPE == "BCP2") {
                    uri = api.server + api.url.transactiondraft + '/' + ar[1];
                } else {
                    uri = api.server + api.url.transactiondraftipe + '/' + ar[1];
                }
                break;
            case ConsProductID.SKNBulkProductIDCons: //add aridya 20161012 ~OFFLINE~
                uri = api.server + api.url.transactiondraftipe + '/' + ar[1];
                break;
                //case ConsProductID.SKNProductIDCons: // SKN Singgle Active
                //    uri = api.server + api.url.transactiondraft + '/' + ar[1];
                //break;
            case ConsProductID.TMOProductIDCons:
                uri = api.server + api.url.transactiontmo + '/Draft/' + ar[1];
                break;
            case ConsProductID.FDProductIDCons:
                uri = api.server + api.url.transactionfd + '/Draft/' + ar[1];
                break;
            case ConsProductID.IDInvestmentProductIDCons:
                uri = api.server + api.url.transactionutin + '/Draft/' + ar[1];
                break;
            case ConsProductID.SavingPlanProductIDCons:
            case ConsProductID.UTOnshoreproductIDCons:
            case ConsProductID.UTOffshoreProductIDCons:
            case ConsProductID.UTCPFProductIDCons:
                uri = api.server + api.url.transactionutsp + '/Draft/' + ar[1];
                break;
            case ConsProductID.CollateralProductIDCons:
                break;
            case ConsProductID.CIFProductIDCons:
                uri = api.server + api.url.transactioncif + '/Draft/' + ar[1];
                break;
            case ConsProductID.LoanDisbursmentProductIDCons:
            case ConsProductID.LoanRolloverProductIDCons:
            case ConsProductID.LoanIMProductIDCons:
            case ConsProductID.LoanSettlementProductIDCons:
                uri = api.server + api.url.transactionloan + '/Draft/' + ar[1];
                break;
            default:
                break;
        }
    }

    var mapping = {
        'ignore': ["LastModifiedDate", "LastModifiedBy"]
    };

    if (Number(ar[2]) != ConsProductID.TMOProductIDCons) {
        var items = ko.utils.arrayFilter(PurposeDoc, function (item) {
            return item.ID != 2;
        });

        if (items != null) {
            viewModel.Parameter().DocumentPurposes(ko.mapping.toJS(items, mapping));

        }
    } else {

        viewModel.Parameter().DocumentPurposes([]);
        viewModel.Parameter().DocumentPurposes(PurposeDoc);
    }

    var options = {
        url: uri,
        token: accessToken
    };
    Helper.Ajax.Get(options, OnSuccessLoadDraft, OnError, OnAlways);
}

//started by haqi
function SaveTransactionCIF() {
    //console.log("dayat",ko.toJSON(viewModel.CIFTransactionModel()))
    if (viewModel.CIFTransactionModel().IsDraft() == true) {

        if (viewModel.CIFTransactionModel().ID() == null) {
            if (viewModel.CIFTransactionModel().DocumentsCIF().length == viewModel.DocumentsCIF().length) {
                viewModel.CIFTransactionModel().AddJoinTableCustomerCIF(viewModel.TempAttachemntDocuments());
                viewModel.CIFTransactionModel().AddJoinTableFFDAcountCIF(viewModel.TempFFDAccounts());
                viewModel.CIFTransactionModel().AddJoinTableAccountCIF(viewModel.TempAddAccounts());
                viewModel.CIFTransactionModel().AddJoinTableDormantCIF(viewModel.TempDormantAccounts());
                viewModel.CIFTransactionModel().AddJoinTableFreezeUnfreezeCIF(viewModel.TempFreezeAccounts());

                var options = {
                    url: api.server + api.url.transactioncif,
                    token: accessToken,
                    data: ko.toJSON(viewModel.CIFTransactionModel())
                };
                Helper.Ajax.Post(options, OnSuccessSaveDraftCIF, OnError, OnAlways);
            }
        } else {

            if (viewModel.CIFTransactionModel().DocumentsCIF().length == viewModel.DocumentsCIF().length) {
                viewModel.CIFTransactionModel().AddJoinTableCustomerCIF(viewModel.TempAttachemntDocuments());
                viewModel.CIFTransactionModel().AddJoinTableFFDAcountCIF(viewModel.TempFFDAccounts());
                viewModel.CIFTransactionModel().AddJoinTableAccountCIF(viewModel.TempAddAccounts());

                ko.utils.arrayForEach(viewModel.TempDormantAccounts(), function (item) {
                    if (item.IsAddDormantAccount == true) {
                        viewModel.TempDormantAccountsSet().push(item);
                    }

                });

                ko.utils.arrayForEach(viewModel.TempFreezeAccounts(), function (item) {
                    if (item.IsAddTblFreezeAccount == true) {
                        viewModel.TempFreezeAccountsSet().push(item);
                    }

                });

                viewModel.CIFTransactionModel().AddJoinTableDormantCIF(viewModel.TempDormantAccountsSet());
                viewModel.CIFTransactionModel().AddJoinTableFreezeUnfreezeCIF(viewModel.TempFreezeAccountsSet());

                var options = {
                    url: api.server + api.url.transactioncif + "/Draft/" + viewModel.CIFTransactionModel().ID(),
                    token: accessToken,
                    data: ko.toJSON(viewModel.CIFTransactionModel())
                };
                Helper.Ajax.Put(options, OnSuccessSaveDraftCIF, OnError, OnAlways);
            }
        }
        return;
    }

    if (viewModel.CIFTransactionModel().DocumentsCIF().length == viewModel.DocumentsCIF().length) {
        viewModel.CIFTransactionModel().AddJoinTableCustomerCIF(viewModel.TempAttachemntDocuments());
        viewModel.CIFTransactionModel().AddJoinTableFFDAcountCIF(viewModel.TempFFDAccounts());
        viewModel.CIFTransactionModel().AddJoinTableAccountCIF(viewModel.TempAddAccounts());
        viewModel.CIFTransactionModel().AddJoinTableDormantCIF(viewModel.TempDormantAccounts());
        viewModel.CIFTransactionModel().AddJoinTableFreezeUnfreezeCIF(viewModel.TempFreezeAccounts());

        if (viewModel.CIFTransactionModel().MaintenanceType().ID == null || viewModel.CIFTransactionModel().MaintenanceType().ID == undefined) {
            viewModel.CIFTransactionModel().MaintenanceType().ID = 3;
        }
        if (viewModel.Selected().Account() != null && viewModel.Selected().Account() != '') {
            viewModel.CIFTransactionModel().AccountNumber().AccountNumber(viewModel.Selected().Account());
        }

        var options = {
            url: api.server + api.url.transactioncif,
            token: accessToken,
            data: ko.toJSON(viewModel.CIFTransactionModel())
        };
        if (viewModel.CIFTransactionModel().ID() == null) {
            Helper.Ajax.Post(options, OnSuccessSaveAPICIF, OnError, OnAlways);
        } else {
            Helper.Ajax.Post(options, OnSuccessSaveAPICIF, OnError, OnAlways);
        }
    }
}
//end by haqi

function SaveDraftCIF() {
    var data = {
        ApplicationID: viewModel.CIFTransactionModel().ID(),
        CIF: viewModel.CIFTransactionModel().Customer().CIF,
        Name: viewModel.CIFTransactionModel().Customer().Name
    };
    if (viewModel.DocumentsCIF().length > 0) {
        UploadFileRecuresive(data, viewModel.DocumentsCIF(), SaveTransactionCIF, viewModel.DocumentsCIF().length);
    } else {
        SaveTransactionCIF();
    }
}

//Dani
function SaveDraftTMO() {
    var data = {
        ApplicationID: viewModel.TransactionTMOModel().ApplicationID(),
        CIF: viewModel.TransactionTMOModel().Customer().CIF,
        Name: viewModel.TransactionTMOModel().Customer().Name
    };

    if (viewModel.Documents().length > 0) {
        //for (var i = 0; i < viewModel.Documents().length; i++) {
        //    UploadFile(data, viewModel.Documents()[i], SaveTransactionTMO);
        UploadFileRecuresive(data, viewModel.Documents(), SaveTransactionTMO, viewModel.Documents().length);
        //}
    } else {
        SaveTransactionTMO();
    }
}
function SaveDraftUT() {
    var data = {
        ApplicationID: viewModel.TransactionUTModel().ApplicationID(),
        CIF: viewModel.TransactionUTModel().Customer().CIF,
        Name: viewModel.TransactionUTModel().Customer().Name
    };

    if (viewModel.Documents().length > 0) {
        //for (var i = 0; i < viewModel.Documents().length; i++) {
        //    UploadFile(data, viewModel.Documents()[i], SaveTransactionUT);
        UploadFileRecuresive(data, viewModel.Documents(), SaveTransactionUT, viewModel.Documents().length);
        //}
    } else {
        SaveTransactionUT();
    }
}
function SaveTransactionTMO() {
    if (viewModel.TransactionTMOModel().IsDraft() == true) {
        viewModel.TransactionTMOModel().Amount((viewModel.TransactionTMOModel().Amount() > 0) ? viewModel.TransactionTMOModel().Amount() : 0);

        if (viewModel.TransactionTMOModel().IsNewCustomer()) {//jika customer baru
            var CustomerName = $("#customer-name").val();
            viewModel.TransactionTMOModel().CustomerDraft().DraftCustomerName(CustomerName);
            viewModel.TransactionTMOModel().CustomerDraft().DraftCIF(viewModel.TransactionTMOModel().Customer().CIF);
            viewModel.TransactionTMOModel().CustomerDraft().DraftAccountNumber(viewModel.TransactionTMOModel().Account());

            viewModel.TransactionTMOModel().Currency().DraftCurrencyID = viewModel.Selected().NewCustomer().Currency();
        }

        if (viewModel.TransactionTMOModel().ID() == null) {//draft baru 
            var options = {
                url: api.server + api.url.transactiontmo,
                token: accessToken,
                data: ko.toJSON(viewModel.TransactionTMOModel())
            };
            if (viewModel.TransactionTMOModel().Documents().length == viewModel.Documents().length)
                Helper.Ajax.Post(options, OnSuccessSaveDraft, OnError, OnAlways);
        } else {//update draft
            var options = {
                url: api.server + api.url.transactiontmo + "/Draft/" + viewModel.TransactionTMOModel().ID(),
                token: accessToken,
                data: ko.toJSON(viewModel.TransactionTMOModel())
            };
            if (viewModel.TransactionTMOModel().Documents().length == viewModel.Documents().length)
                Helper.Ajax.Put(options, OnSuccessSaveDraft, OnError, OnAlways);
        }
        return;
    }

    if (viewModel.TransactionTMOModel().Documents().length == viewModel.Documents().length) {
        var options = {
            url: api.server + api.url.transactiontmo,
            token: accessToken,
            data: ko.toJSON(viewModel.TransactionTMOModel())
        };

        // Save or Update
        if (viewModel.TransactionTMOModel().ID() == null) {
            // if id is null = Save
            Helper.Ajax.Post(options, OnSuccessSaveTMOAPI, OnError, OnAlways);
        } else {
            // if id is not null = Update            
            Helper.Ajax.Post(options, OnSuccessSaveTMOAPI, OnError, OnAlways);
        }
    }
}
//End Dani

//Afif
function SaveDraftLoan() {
    var data = {
        ApplicationID: viewModel.TransactionLoanModel().ApplicationID(),
        CIF: viewModel.TransactionLoanModel().Customer().CIF,
        Name: viewModel.TransactionLoanModel().Customer().Name
    };

    if (viewModel.Documents().length > 0) {
        //for (var i = 0; i < viewModel.Documents().length; i++) {
        //    UploadFile(data, viewModel.Documents()[i], SaveLoanTransaction);
        UploadFileRecuresive(data, viewModel.Documents(), SaveLoanTransaction, viewModel.Documents().length);
        //}
    } else {
        SaveLoanTransaction();
    }
}
function SaveLoanTransaction() {
    viewModel.TransactionLoanModel().IsTopUrgent(viewModel.TransactionLoanModel().IsTopUrgent() == 1 ? 1 : 0);
    viewModel.TransactionLoanModel().IsTopUrgentChain(viewModel.TransactionLoanModel().IsTopUrgentChain() == 1 ? 1 : 0)
    viewModel.TransactionLoanModel().IsNormal(viewModel.TransactionLoanModel().IsNormal() == 1 ? 1 : 0)
    viewModel.IsPPUOrBranchRole();
    if (viewModel.IsPPURole() == false) {
        viewModel.TransactionLoanModel().Source(null);
    }
    if (viewModel.TransactionLoanModel().IsDraft() == true) {
        //viewModel.TransactionLoanModel().Amount((!viewModel.TransactionLoanModel().Amount() > 0) ? 0 : viewModel.TransactionLoanModel().Amount());
        //viewModel.TransactionLoanModel().Rate((!viewModel.TransactionLoanModel().Rate() > 0) ? 0 : viewModel.TransactionLoanModel().Rate());

        if (viewModel.TransactionLoanModel().IsNewCustomer()) {
            var CustomerName = $("#customer-name").val();
            viewModel.TransactionLoanModel().CustomerDraft().DraftCustomerName(CustomerName);
            viewModel.TransactionLoanModel().CustomerDraft().DraftCIF(viewModel.TransactionLoanModel().Customer().CIF);
            viewModel.TransactionLoanModel().CustomerDraft().DraftAccountNumber(viewModel.TransactionLoanModel().Account());
            viewModel.TransactionLoanModel().Currency().DraftCurrencyID = viewModel.Selected().NewCustomer().Currency();
        }

        if (viewModel.TransactionLoanModel().ID() == null) {
            var options = {
                url: api.server + api.url.transactionloan,
                token: accessToken,
                data: ko.toJSON(viewModel.TransactionLoanModel())
            };
            if (viewModel.TransactionLoanModel().Documents().length == viewModel.Documents().length)
                Helper.Ajax.Post(options, OnSuccessSaveDraft, OnError, OnAlways);
        } else {
            var options = {
                url: api.server + api.url.transactionloan + "/Draft/" + viewModel.TransactionLoanModel().ID(),
                token: accessToken,
                data: ko.toJSON(viewModel.TransactionLoanModel())
            };
            if (viewModel.TransactionLoanModel().Documents().length == viewModel.Documents().length)
                Helper.Ajax.Put(options, OnSuccessSaveDraft, OnError, OnAlways);
        }
        return;
    }

    if (viewModel.TransactionLoanModel().Documents().length == viewModel.Documents().length) {
        var options = {
            url: api.server + api.url.transactionloan,
            token: accessToken,
            data: ko.toJSON(viewModel.TransactionLoanModel())
        };
        if (viewModel.TransactionLoanModel().ID() == null) {
            Helper.Ajax.Post(options, OnSuccessSaveAPILoan, OnError, OnAlways);
        } else {
            Helper.Ajax.Post(options, OnSuccessSaveAPILoan, OnError, OnAlways);
        }
    }

}
//End Afif

//Agung Suhendar
function SaveTransactionUT() {
    if (viewModel.TransactionUTModel().IsDraft() == true) {
        if (viewModel.TransactionUTModel().ID() == null) {
            switch (viewModel.ProductID()) {
                case ConsProductID.IDInvestmentProductIDCons:
                    var selFNA = viewModel.Selected().FNACore();
                    var selFunc = viewModel.Selected().FunctionType();
                    var isJoin = viewModel.IsUTJoin();
                    if (isJoin == true) {
                        if (selFNA == ConsUTPar.fnaYes) {
                            viewModel.TransactionUTModel().UTJoin(viewModel.UTJoinFNA());
                        }
                        else if (selFNA == ConsUTPar.fnaNo) {
                            viewModel.TransactionUTModel().UTJoin(viewModel.UTJoinNonFNA());
                        }
                        else
                            viewModel.TransactionUTModel().UTJoin(null);
                    }
                    else
                        viewModel.TransactionUTModel().UTJoin(null);

                    var options = {
                        url: api.server + api.url.transactionutin,
                        token: accessToken,
                        data: ko.toJSON(viewModel.TransactionUTModel())
                    };
                    break;
                case ConsProductID.UTCPFProductIDCons:
                case ConsProductID.SavingPlanProductIDCons:
                case ConsProductID.UTOffshoreProductIDCons:
                case ConsProductID.UTOnshoreproductIDCons:
                    var IsSP = viewModel.IsSP();
                    var IsSubscription = viewModel.IsSubscription();
                    var IsRedemption = viewModel.IsRedemption();
                    var IsSwitching = viewModel.IsSwitching();

                    if (IsSP == true) {
                        viewModel.TransactionUTModel().MutualFundForms(viewModel.MutualFundColl());
                    }
                    else if (IsSubscription == true) {
                        var ret = viewModel.SubcriptionColl();
                        var sendVal = [];
                        for (var i = 0; i < ret.length; i++) {
                            var mf = {
                                MutualFundList: ret[i].MutualFundList,
                                MutualCurrency: null,
                                MutualAmount: ret[i].MutualAmount,
                                MutualFundSwitchFrom: null,
                                MutualFundSwitchTo: null,
                                MutualPartial: null,
                                MutualUnitNumber: null,
                                MutualSelected: null
                            };
                            sendVal.push(mf);
                        }

                        if (sendVal.length > 0) {
                            viewModel.TransactionUTModel().MutualFundForms(sendVal);
                        }
                    }
                    else if (IsRedemption == true) {
                        var ret = viewModel.RedemptionColl();
                        var sendVal = [];
                        for (var i = 0; i < ret.length; i++) {
                            if (ret[i].MutualSelected == true) {
                                var mf = {
                                    MutualFundList: ret[i].MutualFundList,
                                    MutualCurrency: null,
                                    MutualAmount: null,
                                    MutualFundSwitchFrom: null,
                                    MutualFundSwitchTo: null,
                                    MutualPartial: ret[i].MutualPartial,
                                    MutualUnitNumber: ret[i].MutualUnitNumber,
                                    MutualSelected: null
                                };
                                sendVal.push(mf);
                            }
                        }
                        if (sendVal.length > 0) {
                            viewModel.TransactionUTModel().MutualFundForms(sendVal);
                        }
                    }
                    else if (IsSwitching == true) {
                        var ret = viewModel.SwitchingColl();
                        var sendVal = [];
                        for (var i = 0; i < ret.length; i++) {
                            if (ret[i].MutualSelected == true) {
                                var mf = {
                                    MutualFundList: ret[i].MutualFundList,
                                    MutualCurrency: null,
                                    MutualAmount: null,
                                    MutualFundSwitchFrom: ret[i].MutualFundSwitchFrom,
                                    MutualFundSwitchTo: ret[i].MutualFundSwitchTo,
                                    MutualPartial: ret[i].MutualPartial,
                                    MutualUnitNumber: ret[i].MutualUnitNumber,
                                    MutualSelected: null
                                };
                                sendVal.push(mf);
                            }
                        }
                        if (sendVal.length > 0) {
                            viewModel.TransactionUTModel().MutualFundForms(sendVal);
                        }
                    }

                    var options = {
                        url: api.server + api.url.transactionutsp,
                        token: accessToken,
                        data: ko.toJSON(viewModel.TransactionUTModel())
                    };
                    break;
            }
            if (viewModel.TransactionUTModel().Documents().length == viewModel.Documents().length)
                Helper.Ajax.Post(options, OnSuccessSaveDraftUT, OnError, OnAlways);
        }
        else {
            switch (viewModel.ProductID()) {
                case ConsProductID.IDInvestmentProductIDCons:
                    var options = {
                        url: api.server + api.url.transactionutin + "/Draft/" + viewModel.TransactionUTModel().ID(),
                        token: accessToken,
                        data: ko.toJSON(viewModel.TransactionUTModel())
                    };
                    break;
                case ConsProductID.UTCPFProductIDCons:
                case ConsProductID.SavingPlanProductIDCons:
                case ConsProductID.UTOffshoreProductIDCons:
                case ConsProductID.UTOnshoreproductIDCons:
                    //added by dani dp 19-2-2016
                    var IsSP = viewModel.IsSP();
                    var IsSubscription = viewModel.IsSubscription();
                    var IsRedemption = viewModel.IsRedemption();
                    var IsSwitching = viewModel.IsSwitching();

                    if (IsSP == true) {
                        viewModel.TransactionUTModel().MutualFundForms(viewModel.MutualFundColl());
                    }
                    else if (IsSubscription == true) {
                        var ret = viewModel.SubcriptionColl();
                        var sendVal = [];
                        for (var i = 0; i < ret.length; i++) {
                            var mf = {
                                MutualFundList: ret[i].MutualFundList,
                                MutualCurrency: null,
                                MutualAmount: ret[i].MutualAmount,
                                MutualFundSwitchFrom: null,
                                MutualFundSwitchTo: null,
                                MutualPartial: null,
                                MutualUnitNumber: null,
                                MutualSelected: null
                            };
                            sendVal.push(mf);
                        }

                        if (sendVal.length > 0) {
                            viewModel.TransactionUTModel().MutualFundForms(sendVal);
                        }
                    }
                    else if (IsRedemption == true) {
                        var ret = viewModel.RedemptionColl();
                        var sendVal = [];
                        for (var i = 0; i < ret.length; i++) {
                            if (ret[i].MutualSelected == true) {
                                var mf = {
                                    MutualFundList: ret[i].MutualFundList,
                                    MutualCurrency: null,
                                    MutualAmount: null,
                                    MutualFundSwitchFrom: null,
                                    MutualFundSwitchTo: null,
                                    MutualPartial: ret[i].MutualPartial,
                                    MutualUnitNumber: ret[i].MutualUnitNumber,
                                    MutualSelected: null
                                };
                                sendVal.push(mf);
                            }
                        }
                        if (sendVal.length > 0) {
                            viewModel.TransactionUTModel().MutualFundForms(sendVal);
                        }
                    }
                    else if (IsSwitching == true) {
                        var ret = viewModel.SwitchingColl();
                        var sendVal = [];
                        for (var i = 0; i < ret.length; i++) {
                            if (ret[i].MutualSelected == true) {
                                var mf = {
                                    MutualFundList: ret[i].MutualFundList,
                                    MutualCurrency: null,
                                    MutualAmount: null,
                                    MutualFundSwitchFrom: ret[i].MutualFundSwitchFrom,
                                    MutualFundSwitchTo: ret[i].MutualFundSwitchTo,
                                    MutualPartial: ret[i].MutualPartial,
                                    MutualUnitNumber: ret[i].MutualUnitNumber,
                                    MutualSelected: null
                                };
                                sendVal.push(mf);
                            }
                        }
                        if (sendVal.length > 0) {
                            viewModel.TransactionUTModel().MutualFundForms(sendVal);
                        }
                    }
                    //added by dani dp 19-2-2016 end
                    var options = {
                        url: api.server + api.url.transactionutsp + "/Draft/" + viewModel.TransactionUTModel().ID(),
                        token: accessToken,
                        data: ko.toJSON(viewModel.TransactionUTModel())
                    };
                    break;
            }
            if (viewModel.TransactionUTModel().Documents().length == viewModel.Documents().length)
                Helper.Ajax.Put(options, OnSuccessSaveDraftUT, OnError, OnAlways);
        }
        return;
    }
    if (viewModel.TransactionUTModel().Documents().length == viewModel.Documents().length) {
        switch (viewModel.ProductID()) {
            case ConsProductID.IDInvestmentProductIDCons:
                var selFNA = viewModel.Selected().FNACore();
                var selFunc = viewModel.Selected().FunctionType();
                var isJoin = viewModel.IsUTJoin();
                //add henggar 30032017
                viewModel.TransactionUTModel().IsNewCustomer(viewModel.IsNewCustomer());
                //end
                if (isJoin == true) {
                    if (selFNA == ConsUTPar.fnaYes) {
                        viewModel.TransactionUTModel().UTJoin(viewModel.UTJoinFNA());
                    }
                    else if (selFNA == ConsUTPar.fnaNo) {
                        viewModel.TransactionUTModel().UTJoin(viewModel.UTJoinNonFNA());
                    }
                    else
                        viewModel.TransactionUTModel().UTJoin(null);
                }
                else
                    viewModel.TransactionUTModel().UTJoin(null);

                var options = {
                    url: api.server + api.url.transactionutin,
                    token: accessToken,
                    data: ko.toJSON(viewModel.TransactionUTModel())
                };

                break;
            case ConsProductID.UTCPFProductIDCons:
            case ConsProductID.SavingPlanProductIDCons:
            case ConsProductID.UTOffshoreProductIDCons:
            case ConsProductID.UTOnshoreproductIDCons:
                var IsSP = viewModel.IsSP();
                var IsSubscription = viewModel.IsSubscription();
                var IsRedemption = viewModel.IsRedemption();
                var IsSwitching = viewModel.IsSwitching();

                //add henggar 30032017
                viewModel.TransactionUTModel().IsNewCustomer(viewModel.IsNewCustomer());
                //end
                if (IsSP == true) {
                    viewModel.TransactionUTModel().MutualFundForms(viewModel.MutualFundColl());
                }
                else if (IsSubscription == true) {
                    var ret = viewModel.SubcriptionColl();
                    var sendVal = [];
                    for (var i = 0; i < ret.length; i++) {
                        var mf = {
                            MutualFundList: ret[i].MutualFundList,
                            MutualCurrency: null,
                            MutualAmount: ret[i].MutualAmount,
                            MutualFundSwitchFrom: null,
                            MutualFundSwitchTo: null,
                            MutualPartial: null,
                            MutualUnitNumber: null,
                            MutualSelected: null
                        };
                        sendVal.push(mf);
                    }

                    if (sendVal.length > 0) {
                        viewModel.TransactionUTModel().MutualFundForms(sendVal);
                    }
                }
                else if (IsRedemption == true) {
                    var ret = viewModel.RedemptionColl();
                    var sendVal = [];
                    for (var i = 0; i < ret.length; i++) {
                        if (ret[i].MutualSelected == true) {
                            var mf = {
                                MutualFundList: ret[i].MutualFundList,
                                MutualCurrency: null,
                                MutualAmount: null,
                                MutualFundSwitchFrom: null,
                                MutualFundSwitchTo: null,
                                MutualPartial: ret[i].MutualPartial,
                                MutualUnitNumber: ret[i].MutualUnitNumber,
                                MutualSelected: null
                            };
                            sendVal.push(mf);
                        }
                    }
                    if (sendVal.length > 0) {
                        viewModel.TransactionUTModel().MutualFundForms(sendVal);
                    }
                }
                else if (IsSwitching == true) {
                    var ret = viewModel.SwitchingColl();
                    var sendVal = [];
                    for (var i = 0; i < ret.length; i++) {
                        if (ret[i].MutualSelected == true) {
                            var mf = {
                                MutualFundList: ret[i].MutualFundList,
                                MutualCurrency: null,
                                MutualAmount: null,
                                MutualFundSwitchFrom: ret[i].MutualFundSwitchFrom,
                                MutualFundSwitchTo: ret[i].MutualFundSwitchTo,
                                MutualPartial: ret[i].MutualPartial,
                                MutualUnitNumber: ret[i].MutualUnitNumber,
                                MutualSelected: null
                            };
                            sendVal.push(mf);
                        }
                    }
                    if (sendVal.length > 0) {
                        viewModel.TransactionUTModel().MutualFundForms(sendVal);
                    }
                }

                var options = {
                    url: api.server + api.url.transactionutsp,
                    token: accessToken,
                    data: ko.toJSON(viewModel.TransactionUTModel())
                };
                break;
        }

        if (viewModel.TransactionUTModel().ID() == null) {
            Helper.Ajax.Post(options, OnSuccessSaveUTAPI, OnError, OnAlways);
        } else {
            Helper.Ajax.Post(options, OnSuccessSaveUTAPI, OnError, OnAlways);
        }
    }
}
//End Agung
function SaveDraftPayment() {
    var data = {
        ApplicationID: viewModel.TransactionModel().ApplicationID(),
        CIF: viewModel.TransactionModel().Customer().CIF,
        Name: viewModel.TransactionModel().Customer().Name
    };

    if (viewModel.Documents().length > 0) {
        //for (var i = 0; i < viewModel.Documents().length; i++) {
        UploadFileRecuresive(data, viewModel.Documents(), SaveTransaction, viewModel.Documents().length);
        //}
    } else {
        SaveTransaction();
    }
}
function SaveTransaction() {
    viewModel.IsEditable(false);
    viewModel.IsPPUOrBranchRole();
    if (viewModel.IsPPURole() == false) {
        viewModel.TransactionModel().Source(null);
    }
    if (viewModel.ProductID() == ConsProductID.SKNProductIDCons || viewModel.ProductID() == ConsProductID.RTGSProductIDCons) {
        if (viewModel.TransactionModel().Rate() == null || viewModel.TransactionModel().Rate() == "") {
            viewModel.TransactionModel().Rate(0);
        }
    }
    if (viewModel.ProductID() != ConsProductID.SKNProductIDCons) {
        viewModel.TransactionModel().IsBeneficiaryResident(false);
        //viewModel.TransactionModel().BeneficiaryBusines(null);
    }
    else {
        //viewModel.TransactionModel().ModePayment("BCP2"); // SKN Singgle Active
    }/* else {
        if (!(viewModel.TransactionModel().BeneficiaryBusines() != null && ko.toJS(viewModel.TransactionModel().BeneficiaryBusines().ID) != null)) {
            viewModel.TransactionModel().BeneficiaryBusines(null);
        }
    }*/
    if (viewModel.TransactionModel().Branch() != null && viewModel.IsLoadDraftPayment() == true) {
        if (viewModel.TransactionModel().BranchID != 'function c(){if(0<arguments.length)return c.Ka(d,arguments[0])&&(c.P(),d=arguments[0],c.O()),this;a.k.zb(c);return d}') {
            if (viewModel.TransactionModel().Branch() != null) {
                viewModel.TransactionModel().BranchID = viewModel.TransactionModel().Branch().ID;
            } else {
                viewModel.TransactionModel().BranchID = viewModel.TransactionModel().BranchID;
            }
        } else {
            viewModel.TransactionModel().BranchID = viewModel.TransactionModel().Branch().ID;
        }
        if (viewModel.TransactionModel().CityID != 'function c(){if(0<arguments.length)return c.Ka(d,arguments[0])&&(c.P(),d=arguments[0],c.O()),this;a.k.zb(c);return d}') {
            viewModel.TransactionModel().CityID = viewModel.TransactionModel().CityID;
        } else {
            viewModel.TransactionModel().CityID = viewModel.TransactionModel().City().CityID;
        }
    } else {
        if (viewModel.TransactionModel().Branch() != null) {
            if (viewModel.TransactionModel().ModePayment() == "BCP2") {
                viewModel.TransactionModel().BranchID = viewModel.TransactionModel().Branch().ID;
            }
        }
    }

    if (ko.toJS(viewModel.IsEmptyAccountNumber()) == true) {
        var new_debitcurrency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().DebitCurrency(); });
        if (new_debitcurrency != null) {
            viewModel.TransactionModel().DebitCurrency(new_debitcurrency);
        }

    } else if (ko.toJS(viewModel.TransactionModel().IsNewCustomer()) == true) {
        var new_debitcurrency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().NewCustomer().Currency(); });
        if (new_debitcurrency != null) {
            viewModel.TransactionModel().DebitCurrency(new_debitcurrency);
        }
    } else if ((ko.toJS(viewModel.IsEmptyAccountNumber()) == false) && (ko.toJS(viewModel.TransactionModel().IsNewCustomer()) == false)) {
        var new_debitcurrency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.TransactionModel().Account().Currency.ID; });
        if (new_debitcurrency != null) {
            viewModel.TransactionModel().DebitCurrency(new_debitcurrency);
        }
    }
    // convert top urgent to integer
    viewModel.TransactionModel().IsTopUrgent(viewModel.TransactionModel().IsTopUrgent() == 1 ? 1 : 0);
    viewModel.TransactionModel().IsTopUrgentChain(viewModel.TransactionModel().IsTopUrgentChain() == 1 ? 1 : 0)
    viewModel.TransactionModel().IsNormal(viewModel.TransactionModel().IsNormal() == 1 ? 1 : 0)
    // Set BeneName IS NULL
    if (viewModel.TransactionModel().BeneName() == null) {
        viewModel.TransactionModel().BeneName('');
    }

    // Set BeneAccNumber IS NULL
    if (viewModel.TransactionModel().BeneAccNumber() == null) {
        viewModel.TransactionModel().BeneAccNumber('');
    }

    //Data double transaction: Rizki - 2017-04-27
    if (viewModel.UserApproveDoubleTransaction() != null) {
        viewModel.TransactionModel().DoubleTrxApproveBy("i:0#.f|dbsmembership|" + viewModel.UserApproveDoubleTransaction());

        if (viewModel.DoubleTransactions().length > 0) {
            var temp = "";
            for (var i = 0 ; i < viewModel.DoubleTransactions().length; i++) {
                temp = temp + viewModel.DoubleTransactions()[i].ApplicationID + ", ";
            }

            viewModel.TransactionModel().DoubleTrxApplicationID(temp);
        }
    }

    //End Rizki

    // begin dodit@2014.11.14:Add draft posting
    if (viewModel.TransactionModel().IsDraft() == true) {
        viewModel.TransactionModel().Amount((!viewModel.TransactionModel().Amount() > 0) ? 0 : viewModel.TransactionModel().Amount());
        viewModel.TransactionModel().Rate((!viewModel.TransactionModel().Rate() > 0) ? 0 : viewModel.TransactionModel().Rate());

        if (viewModel.TransactionModel().IsNewCustomer()) {
            var CustomerName = $("#customer-name").val();
            viewModel.TransactionModel().CustomerDraft().DraftCustomerName(CustomerName);
            viewModel.TransactionModel().CustomerDraft().DraftCIF(viewModel.TransactionModel().Customer().CIF);
            viewModel.TransactionModel().CustomerDraft().DraftAccountNumber(viewModel.TransactionModel().Account());
            viewModel.TransactionModel().Currency().DraftCurrencyID = viewModel.Selected().NewCustomer().Currency();
        }


        if (viewModel.TransactionModel().ID() == null) {
            //viewModel.TransactionModel().DebitCurrencyID = $('#debit-acc-ccy').val(data.Account.Currency.Code); 
            var options;
            //aridya 20161214 skn bulk will always default to ipe
            if (viewModel.ProductID() == ConsProductID.SKNBulkProductIDCons) {
                options = {
                    url: api.server + api.url.transactionipe,
                    token: accessToken,
                    data: ko.toJSON(viewModel.TransactionModel())
                };
            } else {
                if (viewModel.TransactionModel().ModePayment() == "BCP2") {
                    options = {
                        url: api.server + api.url.transaction,
                        token: accessToken,
                        data: ko.toJSON(viewModel.TransactionModel())
                    };
                } else {
                    options = {
                        url: api.server + api.url.transactionipe,
                        token: accessToken,
                        data: ko.toJSON(viewModel.TransactionModel())
                    };
                }
            }
            //end add aridya
            if (viewModel.TransactionModel().Documents().length == viewModel.Documents().length)//Andi
                Helper.Ajax.Post(options, OnSuccessSaveDraft, OnError, OnAlways);
        } else {
            var options;
            //aridya 20161214 skn bulk will always default to ipe
            if (viewModel.ProductID() == ConsProductID.SKNBulkProductIDCons) {
                options = {
                    url: api.server + api.url.transaction + "/DraftIPE/" + viewModel.TransactionModel().ID(),
                    token: accessToken,
                    data: ko.toJSON(viewModel.TransactionModel())
                };
            } else {
                if (viewModel.TransactionModel().ModePayment() == "BCP2") {
                    options = {
                        url: api.server + api.url.transaction + "/Draft/" + viewModel.TransactionModel().ID(),
                        token: accessToken,
                        data: ko.toJSON(viewModel.TransactionModel())
                    };
                } else {
                    options = {
                        url: api.server + api.url.transaction + "/DraftIPE/" + viewModel.TransactionModel().ID(),
                        token: accessToken,
                        data: ko.toJSON(viewModel.TransactionModel())
                    };
                }
            }
            //end add aridya
            if (viewModel.TransactionModel().Documents().length == viewModel.Documents().length)//Andi
                Helper.Ajax.Put(options, OnSuccessSaveDraft, OnError, OnAlways);
        }
        return;
    }
    ko.utils.arrayForEach(viewModel.TempSelectedUnderlying(), function (item) {
        if (item.Enable() == false)
            if (viewModel.TransactionModel().IsIPETMO() == true) {
                viewModel.TransactionModel().underlyings.push(new SelectUtillizeModel(item.ID(), false, item.USDAmount(), ''));
            } else {
                viewModel.TransactionModel().underlyings.push(new SelectUtillizeModel(item.ID(), false, item.USDAmount(), ''));
            }
    });
    if (viewModel.TransactionModel().IsIPETMO() == true) {
        viewModel.TransactionModel().underlyings([]);
        ko.utils.arrayForEach(viewModel.TempSelectedUnderlying(), function (item) {
            if (item.Enable() == false)
                viewModel.TransactionModel().underlyings.push(new SelectUtillizeModel(item.ID(), false, item.USDAmount(), ''));
        });
    }
    // add chandra - set transaction underlying
    if ((viewModel.IsFxTransaction() || viewModel.IsFxTransactionToIDR()) && (viewModel.ProductID() == ConsProductID.OTTProductIDCons || viewModel.ProductID() == ConsProductID.OverbookingProductIDCons)) {
        if (viewModel.TransactionModel().Documents().length == viewModel.Documents().length) {
            if (viewModel.TransactionModel().IsIPETMO() != true) {
                viewModel.TransactionModel().underlyings([]);
                ko.utils.arrayForEach(viewModel.TempSelectedUnderlying(), function (item) {
                    if (item.Enable() == false)
                        viewModel.TransactionModel().underlyings.push(new SelectUtillizeModel(item.ID(), false, item.USDAmount(), ''));

                });
            }
            // save transaction  fx
            var options;
            //aridya 20161214 skn bulk will always default to ipe
            if (viewModel.ProductID() == ConsProductID.SKNBulkProductIDCons) {
                options = {
                    url: api.server + api.url.transactionipe,
                    token: accessToken,
                    data: ko.toJSON(viewModel.TransactionModel())
                };
            } else {
                if (viewModel.TransactionModel().ModePayment() == "BCP2") {
                    options = {
                        url: api.server + api.url.transaction,
                        token: accessToken,
                        data: ko.toJSON(viewModel.TransactionModel())
                    };
                } else {
                    options = {
                        url: api.server + api.url.transactionipe,
                        token: accessToken,
                        data: ko.toJSON(viewModel.TransactionModel())
                    };
                }
            }
            //end add aridya

            // Save or Update
            if (viewModel.TransactionModel().ID() == null) {
                Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
            } else {
                // if id is not null = Update
                Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
            }

        }

        //var amount = document.getElementById("eqv-usd").value.replace(',','').replace(' ','');
        /*  var amountUSD = viewModel.TransactionModel().AmountUSD();

        if (parseFloat(amountUSD) <= parseFloat(viewModel.TransactionModel().utilizationAmount())) {
            //if (parseFloat(viewModel.TransactionModel().TotalTransFX()) > parseFloat(TotalPPUModel.TreshHold) && viewModel.TempSelectedUnderlying()[0].StatementLetter() == 1) {
            if (parseFloat(viewModel.TransactionModel().TotalUtilization()) > parseFloat(TotalPPUModel.TreshHold) && viewModel.TempSelectedUnderlying()[0].StatementLetter() == 1) {
                viewModel.IsEditable(true);
                viewModel.IsUploaded(true);
                ShowNotification("Form Underlying Warning", "Total Utilization greater than " + TotalPPUModel.TreshHold + " (USD), Please add statement B underlying", 'gritter-warning', false);
                return;
            } else {
                viewModel.TransactionModel().underlyings([]);
                ko.utils.arrayForEach(viewModel.TempSelectedUnderlying(), function (item) {
                    if (item.Enable() == false)
                        viewModel.TransactionModel().underlyings.push(new SelectUtillizeModel(item.ID(), false, item.USDAmount(), ''));
                });
                // save transaction  fx
                if (viewModel.TransactionModel().Documents().length == viewModel.Documents().length) {
                    var options = {
                        url: api.server + api.url.transaction,
                        token: accessToken,
                        data: ko.toJSON(viewModel.TransactionModel())
                    };

                    // Save or Update
                    if (viewModel.TransactionModel().ID() == null) {
                        Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
                    } else {
                        // if id is not null = Update
                        Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
                    }
                }
            }
        } else {
            viewModel.IsEditable(true);
            viewModel.IsUploaded(true);
            ShowNotification("Form Underlying Warning", "Total Utilization Amount must be equal greater than Transaction Amount", 'gritter-warning', false);
            return;
          }*/
    } else {
        // save transactio non fx
        if (viewModel.TransactionModel().Documents().length == viewModel.Documents().length) {
            var options;
            //aridya 20161214 skn bulk will always default to ipe
            if (viewModel.ProductID() == ConsProductID.SKNBulkProductIDCons) {
                options = {
                    url: api.server + api.url.transactionipe,
                    token: accessToken,
                    data: ko.toJSON(viewModel.TransactionModel())
                };
            } else {
                if (viewModel.TransactionModel().ModePayment() == "BCP2") {
                    options = {
                        url: api.server + api.url.transaction,
                        token: accessToken,
                        data: ko.toJSON(viewModel.TransactionModel())
                    };
                } else {
                    options = {
                        url: api.server + api.url.transactionipe,
                        token: accessToken,
                        data: ko.toJSON(viewModel.TransactionModel())
                    };
                }
            }
            //end add aridya
            // Save or Update
            if (viewModel.TransactionModel().ID() == null) {
                // if id is null = Save
                Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
            } else {
                Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
            }
        }
    }
}
function CheckOtherBank(bank) {
    if (bank == undefined) {
        viewModel.TransactionModel().Bank().SwiftCode = '';
        viewModel.TransactionModel().Bank().BankAccount = '';
        viewModel.TransactionModel().Bank().Code = '999';
        viewModel.IsOtherBank(false);
        viewModel.TransactionModel().IsOtherBeneBank(true);
    }
}
function AddListItem() {
    var body;
    var urlListTransaction;
    var Listtitle;
    var ListInitGroup = GetUserRole(viewModel.SPUser().Roles);
    var ListTransactionID;
    var ListAppID;
    var ListTipe;
    switch (viewModel.ProductID()) {
        //aridya 20161012 add for skn bulk ~OFFLINE~
        case ConsProductID.SKNBulkProductIDCons:
            urlListTransaction = config.sharepoint.listIdIpe;
            Listtitle = viewModel.TransactionModel().ApplicationID() + " - " + viewModel.TransactionModel().Customer().CIF;
            ListTransactionID = viewModel.TransactionModel().ID();
            ListAppID = viewModel.TransactionModel().ApplicationID();
            ListTipe = config.sharepoint.metadata.listIpe;
            break;
            //end add
        case ConsProductID.RTGSProductIDCons:
        case ConsProductID.SKNProductIDCons: // SKN Singgle Active
        case ConsProductID.OTTProductIDCons:
        case ConsProductID.OverbookingProductIDCons:
            if (StatusIPE == "BCP2") {
                urlListTransaction = config.sharepoint.listId;
                Listtitle = viewModel.TransactionModel().ApplicationID() + " - " + viewModel.TransactionModel().Customer().CIF;
                ListTransactionID = viewModel.TransactionModel().ID();
                ListAppID = viewModel.TransactionModel().ApplicationID();
                ListTipe = config.sharepoint.metadata.list;
            } else {
                urlListTransaction = config.sharepoint.listIdIpe;
                Listtitle = viewModel.TransactionModel().ApplicationID() + " - " + viewModel.TransactionModel().Customer().CIF;
                ListTransactionID = viewModel.TransactionModel().ID();
                ListAppID = viewModel.TransactionModel().ApplicationID();
                ListTipe = config.sharepoint.metadata.listIpe;
            }
            break;
            //case ConsProductID.SKNProductIDCons:   // SKN Singgle Active
            //        urlListTransaction = config.sharepoint.listId;
            //        Listtitle = viewModel.TransactionModel().ApplicationID() + " - " + viewModel.TransactionModel().Customer().CIF;
            //        ListTransactionID = viewModel.TransactionModel().ID();
            //        ListAppID = viewModel.TransactionModel().ApplicationID();
            //        ListTipe = config.sharepoint.metadata.list;
            //        break;
        case ConsProductID.TMOProductIDCons:
            urlListTransaction = config.sharepoint.listIdTMOProduct;
            Listtitle = viewModel.TransactionTMOModel().ApplicationID() + " - " + viewModel.TransactionTMOModel().Customer().CIF;
            ListTransactionID = viewModel.TransactionTMOModel().ID();
            ListAppID = viewModel.TransactionTMOModel().ApplicationID();
            ListTipe = config.sharepoint.metadata.listTMOProduct;
            break;
        case ConsProductID.LoanDisbursmentProductIDCons:
        case ConsProductID.LoanIMProductIDCons:
        case ConsProductID.LoanRolloverProductIDCons:
        case ConsProductID.LoanSettlementProductIDCons:
            urlListTransaction = config.sharepoint.listIdLoan;
            Listtitle = viewModel.TransactionLoanModel().ApplicationID() + " - " + viewModel.TransactionLoanModel().Customer().CIF;
            ListTransactionID = viewModel.TransactionLoanModel().ID();
            ListAppID = viewModel.TransactionLoanModel().ApplicationID();
            ListTipe = config.sharepoint.metadata.listLoan;
            break;
        case ConsProductID.IDInvestmentProductIDCons:
        case ConsProductID.UTCPFProductIDCons:
        case ConsProductID.UTOffshoreProductIDCons:
        case ConsProductID.UTOnshoreproductIDCons:
        case ConsProductID.SavingPlanProductIDCons:
            urlListTransaction = config.sharepoint.listIdUTProduct;
            Listtitle = viewModel.TransactionUTModel().ApplicationID() + " - " + viewModel.TransactionUTModel().Customer().CIF;
            ListTransactionID = viewModel.TransactionUTModel().ID();
            ListAppID = viewModel.TransactionUTModel().ApplicationID();
            ListTipe = config.sharepoint.metadata.listUTProduct;
            break;
        case ConsProductID.CIFProductIDCons:
            urlListTransaction = config.sharepoint.listIdRetailCIF;
            Listtitle = viewModel.CIFTransactionModel().ApplicationID() + " - " + viewModel.CIFTransactionModel().Customer().CIF;
            ListTransactionID = viewModel.CIFTransactionModel().ID();
            ListAppID = viewModel.CIFTransactionModel().ApplicationID();
            ListTipe = config.sharepoint.metadata.listRetailCIF;
            break;
    }

    body = {
        Title: Listtitle,
        Initiator_x0020_GroupId: ListInitGroup,
        Transaction_x0020_ID: ListTransactionID,
        Application_x0020_ID: ListAppID,
        __metadata: {
            type: ListTipe
        }
    };

    UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token: Rizki 2017-01-31

    var options = {
        url: config.sharepoint.url.api + "/Lists(guid'" + urlListTransaction + "')/Items",
        data: JSON.stringify(body),
        digest: jQuery("#__REQUESTDIGEST").val()
    };
    //Helper.Sharepoint.List.Add(options, OnSuccessAddListItem, OnError, OnAlways);
    //Andi
    Helper.Sharepoint.List.Add(options, OnSuccessAddListWorkflow, OnError, OnAlways);
    //End Andi
}
function AddOtherAccounts(accounts) {
    if (accounts != null) {
        var accountData = {
            AccountNumber: "-",
            CustomerName: null,
            IsJointAccount: false,
            Currency: null
        };
        accounts.push(accountData);
    }
    return accounts;
}
// added chandra
function GetUserRole(userData) {
    var sValue;
    if (userData != undefined && userData.length > 0) {
        $.each(userData, function (index, item) {
            if (Const_RoleName[item.ID].toLowerCase().startsWith("dbs ppu") && Const_RoleName[item.ID].toLowerCase().endsWith("maker")) { //if (item.Name.toLowerCase().startsWith("dbs ppu") && item.Name.toLowerCase().endsWith("maker")) {
                sValue = item.ID;
                return sValue;
            }
        });

        //Untuk user non PPU tetapi bisa New Transaction
        $.each(userData, function (index, item) {
            if (Const_RoleName[item.ID].toLowerCase().endsWith("maker")) {
                sValue = item.ID;
                return sValue;
            }
        });
    }
    if (sValue == null) {
        sValue = userData[0].ID;
    }
    return sValue;
}

//add henggar
function GetLimitProduct() {
    var ProductIDSelected = viewModel.ProductID();
    var options = {
        url: api.server + api.url.transactionlimitproductbyProduct,
        params: {
            id: ProductIDSelected,
        },
        token: accessToken
    };
    Helper.Ajax.Get(options, OnSuccessGetLimitProduct, OnError, OnAlways);
}
//end

function OnSuccessGetLimitProduct(data, textStatus, jqXHR) {
    if (jqXHR.status == 200) {
        viewModel.LimitProduct([]);
        for (var i = 0 ; i < data.length; i++) {
            var DataLimitProduct = {
                TransactionLimitProductID: data[i].ID,
                MinAmount: data[i].MinAmount,
                MaxAmount: data[i].MaxAmount,
                Unlimited: data[i].Unlimited,
                Product: {
                    ID: data[i].Product.ID,
                    Name: data[i].Product.Name
                },
                Currency: {
                    ID: data[i].Currency.ID,
                    Code: data[i].Currency.Code
                }
            };
            viewModel.LimitProduct.push(DataLimitProduct)
        }
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function GetMidrateCurrency() {
    var CurrencySelected = viewModel.Selected().Currency();
    var options = {
        url: api.server + api.url.currencybyID,
        params: {
            id: CurrencySelected,
        },
        token: accessToken
    };
    Helper.Ajax.Get(options, OnSuccessGetMidrateData, OnError, OnAlways);
}

function OnSuccessGetMidrateData(data, textStatus, jqXHR) {
    if (jqXHR.status == 200) {
        viewModel.MidrateData([]);
        for (var i = 0 ; i < data.length; i++) {
            var DataMidrateData = {
                ID: data[i].ID,
                Code: data[i].Code,
                Description: data[i].Description,
                RupiahRate: data[i].RupiahRate,
            };
            viewModel.MidrateData.push(DataMidrateData)
        }
        viewModel.TransactionModel().Rate(viewModel.MidrateData()[0].RupiahRate);
        viewModel.Rate(viewModel.MidrateData()[0].RupiahRate);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}


//edited Lele 26 jan 2015
//add From Pak Dennes checking double Transaction 25 mar 2015
function ValidateTransaction() {
    // Ruddy : new exception to validate double transaction/underlying
    var OtherAccNumber = viewModel.TransactionModel().OtherAccountNumber();
    if (viewModel.TransactionModel().IsOtherAccountNumber()) {
        viewModel.TransactionModel().Account().AccountNumber = OtherAccNumber;
    }
    // IS NULL Bene Account Number
    if (viewModel.TransactionModel().BeneAccNumber() == null) {
        viewModel.TransactionModel().BeneAccNumber(null);
    }
    if (viewModel.TransactionModel().BeneAccNumber() != null && viewModel.TransactionModel().BeneAccNumber().trim() == '') {
        viewModel.TransactionModel().BeneAccNumber(null);
    }
    // IS NULL Bene Name
    if (viewModel.TransactionModel().BeneName() != null && viewModel.TransactionModel().BeneName().trim() == '') {
        viewModel.TransactionModel().BeneName(null);
    }

    var DoubleTransaction = {
        CIF: viewModel.TransactionModel().Customer().CIF,
        ProductID: viewModel.TransactionModel().Product().ID,
        CurrencyID: viewModel.TransactionModel().Currency().ID,
        Amount: parseFloat(viewModel.TransactionModel().Amount().replace(/[, ]+/g, " ").trim()),
        ApplicationDate: viewModel.TransactionModel().ApplicationDate(),
        ExecutionDate: viewModel.TransactionModel().ExecutionDate(),
        AccountNumber: viewModel.TransactionModel().Account().AccountNumber,
        BeneAccount: viewModel.TransactionModel().BeneAccNumber(),
        BeneName: viewModel.TransactionModel().BeneName()
    };
    var options = {
        url: api.server + api.url.workflow.transactionCheck,
        token: accessToken,
        params: {},
        data: JSON.stringify(DoubleTransaction)
    };
    Helper.Ajax.Post(options, function (data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            // Double transaction handling
            if (data.IsDoubleTransaction) {
                // fil double transactions
                viewModel.DoubleTransactions(data.Transactions);

                // Show Double Transaction dialog
                $("#modal-double-transaction").modal('show');
            } else {
                // start upload docs and save the transaction
                var isSubmit = true;
                if ((viewModel.IsFxTransaction() || viewModel.IsFxTransactionToIDR()) && (viewModel.ProductID() == ConsProductID.OTTProductIDCons || viewModel.ProductID() == ConsProductID.OverbookingProductIDCons)) {
                    if (viewModel.TransactionModel().TZNumber() != null && viewModel.TransactionModel().TZNumber() != "") {
                        $.ajax({
                            type: "GET",
                            url: api.server + api.url.helper + "/IsAlreadyTZNumber/" + viewModel.TransactionModel().TZNumber(),
                            contentType: "application/json",
                            headers: {
                                "Authorization": "Bearer " + $.cookie(api.cookie.name)
                            },
                            success: function (data, textStatus, jqXHR) {
                                if (!data) {
                                    viewModel.GetThreshold(UploadDocuments);
                                } else {
                                    ShowNotification("Warning validation", "TZ Number Already Exists on PPU Transaction.", 'gritter-warning', false);
                                    viewModel.IsEditable(true);
                                    viewModel.IsUploaded(true);
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                ShowNotification("Warning validation", "Fail to checking TZ Number.", 'gritter-error', true);
                                viewModel.IsEditable(true);
                                viewModel.IsUploaded(true);
                            }
                        });
                    } else {
                        viewModel.GetThreshold(UploadDocuments); // execute validation threshold
                    }
                } else {
                    UploadDocuments();
                }
            }
        }
    }, OnError);
}

//aridya 20170208 add for saving double transaction approval history
function ApproveDoubleTransactionHistory() {
    var data = {
        UserID: viewModel.UserApproveDoubleTransaction(),
        TransactionID: viewModel.TransactionModel().ID()
    }

    var options = {
        url: api.server + api.url.workflow.saveDoubleTransactionHistory,
        token: accessToken,
        data: JSON.stringify(data)
    };

    Helper.Ajax.Post(options, function (data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            viewModel.UserApproveDoubleTransaction('');
        }
    }, OnError, OnAlways);

}
//end aridya

function UploadDocuments() {
    // uploading documents. after upload completed, see SaveTransaction()
    var data = {
        ApplicationID: viewModel.TransactionModel().ApplicationID(),
        CIF: viewModel.TransactionModel().Customer().CIF,
        Name: viewModel.TransactionModel().Customer().Name
    };

    //Rizki 2016-03-12: handle cross currency untuk selain OTT
    if (viewModel.IsFXTransactionAttach() && !(viewModel.IsUploaded())) {
        //for (var i = 0; i < viewModel.Documents().length; i++) {
        //UploadFile(data, viewModel.Documents()[i], SaveTransaction);
        UploadFileRecuresive(data, viewModel.Documents(), SaveTransaction, viewModel.Documents().length);

        //}
    }
        //bangkit
        // chandra - underlying
    else if (viewModel.Documents().length > 0) { //&& !(self.TransactionModel.Currency().Code != 'IDR' && self.TransactionModel.Account().Currency.Code == 'IDR')) { // FXCONDITION YES
        //for (var i = 0; i < viewModel.Documents().length; i++) {
        //UploadFile(data, viewModel.Documents()[i], SaveTransaction);
        UploadFileRecuresive(data, viewModel.Documents(), SaveTransaction, viewModel.Documents().length);

        //}
    }
    else {
        SaveTransaction();
    }
}

// delete & upload document underlying start
function DeleteFile(documentPath) {
    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;
    // output variable
    var output;

    // Delete the file to the SharePoint folder.
    var deleteFile = deleteFileToFolder();
    deleteFile.done(function (file, status, xhr) {
        output = file.d;
    });
    deleteFile.fail(OnError);
    // Call function delete File Shrepoint Folder
    function deleteFileToFolder() {
        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token: Rizki 2017-01-31

        return $.ajax({
            url: serverUrl + "/_api/web/GetFileByServerRelativeUrl('" + documentPath + "')",
            type: "POST",
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "X-HTTP-Method": "DELETE",
                "IF-MATCH": "*"
                //"content-length": arrayBuffer.byteLength
            }
        });
    }
}

function UploadFileUnderlying(context, document, callBack) {

    // Define the folder path for this example.
    var serverRelativeUrlToFolder = '/Underlying Documents';


    // Get the file name from the file input control on the page.
    if (document.DocumentPath().name != undefined) {
        var parts = document.DocumentPath().name.split('.');
    } else {
        //self.DocumentPath_a('');
        ShowNotification("Attention", "Please select a file", 'gritter-warning', true);
    }

    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;


    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBufferUnderlying();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolderUnderlying(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItemUnderlying(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                // Change the display name and title of the list item.
                var changeItem = updateListItemUnderlying(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    //DocumentModels({ "FileName": document.name, "DocumentPath": output.ServerRelativeUrl });
                    DocumentModels({ "FileName": document.DocumentPath().name, "DocumentPath": output.ServerRelativeUrl });
                    callBack();
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBufferUnderlying() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }
        //reader.readAsArrayBuffer(fileInput[0].files[0]);
        //reader.readAsDataURL(document.DocumentPath());
        reader.readAsArrayBuffer(document.DocumentPath());
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolderUnderlying(arrayBuffer) {

        // Get the file name from the file input control on the page.
        //var parts = fileInput[0].value.split('\\');
        //var fileName = parts[parts.length - 1];

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token: Rizki 2017-01-31

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                //"content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItemUnderlying(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItemUnderlying(itemMetadata) {

        // Define the list item changes. Use the FileLeafRef property to change the display name.
        // For simplicity, also use the name as the title.
        // The example gets the list item type from the item's metadata, but you can also get it from the
        // ListItemEntityTypeFullName property of the list.
        //var body = String.format("{{'__metadata':{{'type':'{0}'}},'FileLeafRef':'{1}','Title':'{2}'}}", itemMetadata.type, fileName, fileName);
        var body = {
            Title: document.name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: context.Type.DocTypeName,
            Document_x0020_Purpose: context.Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };

        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token: Rizki 2017-01-31

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}
// delete & upload document underlying end

// Upload the file
function UploadFile(context, document, callBack) {
    var IsDraft;
    //default value to payment module
    IsDraft = viewModel.TransactionModel().IsDraft();
    switch (viewModel.ProductID()) {
        case ConsProductID.RTGSProductIDCons:
        case ConsProductID.SKNProductIDCons:
        case ConsProductID.OTTProductIDCons:
        case ConsProductID.OverbookingProductIDCons:
            IsDraft = viewModel.TransactionModel().IsDraft();
            //add by fandi
            viewModel.IsNewDocument(true);
            //end
            break;
        case ConsProductID.TMOProductIDCons:
            IsDraft = viewModel.TransactionTMOModel().IsDraft();
            break;
        case ConsProductID.FDProductIDCons:
            IsDraft = viewModel.FDModel().IsDraft();
            break;
        case ConsProductID.LoanDisbursmentProductIDCons:
        case ConsProductID.LoanIMProductIDCons:
        case ConsProductID.LoanRolloverProductIDCons:
        case ConsProductID.LoanSettlementProductIDCons:
            IsDraft = viewModel.TransactionLoanModel().IsDraft();
            break;
        case ConsProductID.UTOnshoreproductIDCons:
        case ConsProductID.UTOffshoreProductIDCons:
        case ConsProductID.UTCPFProductIDCons:
        case ConsProductID.SavingPlanProductIDCons:
        case ConsProductID.IDInvestmentProductIDCons:
            IsDraft = viewModel.TransactionUTModel().IsDraft();
            break;
    }

    var serverRelativeUrlToFolder = '';
    if (IsDraft == true)
        serverRelativeUrlToFolder = '/DraftDocument';
    else
        serverRelativeUrlToFolder = '/Instruction Documents';

    var parts = document.DocumentPath.name.split('.');
    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBuffer();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolder(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                // Change the display name and title of the list item.
                var changeItem = updateListItem(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    //alert('file uploaded and updated');
                    //return output;
                    var newDoc = {
                        ID: 0,
                        Type: document.Type,
                        Purpose: document.Purpose,
                        LastModifiedDate: null,
                        LastModifiedBy: null,
                        DocumentPath: output.ServerRelativeUrl,
                        FileName: document.DocumentPath.name
                    };
                    switch (viewModel.ProductID()) {
                        case ConsProductID.RTGSProductIDCons:
                        case ConsProductID.SKNProductIDCons:
                        case ConsProductID.OTTProductIDCons:
                        case ConsProductID.OverbookingProductIDCons:
                            viewModel.TransactionModel().Documents.push(newDoc);
                            break;
                        case ConsProductID.TMOProductIDCons:
                            viewModel.TransactionTMOModel().Documents.push(newDoc);
                            break;
                        case ConsProductID.FDProductIDCons:
                            viewModel.FDModel().Documents.push(newDoc);
                            break;
                        case ConsProductID.LoanDisbursmentProductIDCons:
                        case ConsProductID.LoanIMProductIDCons:
                        case ConsProductID.LoanRolloverProductIDCons:
                        case ConsProductID.LoanSettlementProductIDCons:
                            viewModel.TransactionLoanModel().Documents.push(newDoc);
                            break;
                        case ConsProductID.UTOnshoreproductIDCons:
                        case ConsProductID.UTOffshoreProductIDCons:
                        case ConsProductID.UTCPFProductIDCons:
                        case ConsProductID.SavingPlanProductIDCons:
                        case ConsProductID.IDInvestmentProductIDCons:
                            viewModel.TransactionUTModel().Documents.push(newDoc);
                            break;
                    }
                    callBack();
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBuffer() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }

        if (document.DocumentPath.type == Util.spitemdoc) {
            var fileURI = serverUrl + document.DocumentPath.DocPath;
            var ExistFile = $.ajax({
                url: fileURI,
                type: "GET",
                headers: { "accept": "application/json;odata=verbose" }
            });
            return ExistFile;
        }
        //bangkit
        reader.readAsArrayBuffer(document.DocumentPath);
        //reader.readAsDataURL(document.DocumentPath);
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(arrayBuffer) {

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token: Rizki 2017-01-31

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                //"content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItem(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItem(itemMetadata) {
        var body = {
            Title: document.DocumentPath.name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: document.Type.Name,
            Document_x0020_Purpose: document.Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };

        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token: Rizki 2017-01-31

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}
function UploadFileRecuresive(context, document, callBack, numFile) {
    var indexDocument = document.length - numFile;
    var IsDraft;
    //default value to payment module
    IsDraft = viewModel.TransactionModel().IsDraft();
    switch (viewModel.ProductID()) {
        case ConsProductID.SKNBulkProductIDCons: //add aridya 20161012 skn bulk ~OFFLINE~
        case ConsProductID.RTGSProductIDCons:
        case ConsProductID.SKNProductIDCons:
        case ConsProductID.OTTProductIDCons:
        case ConsProductID.OverbookingProductIDCons:
            IsDraft = viewModel.TransactionModel().IsDraft();
            break;
        case ConsProductID.TMOProductIDCons:
            IsDraft = viewModel.TransactionTMOModel().IsDraft();
            break;
        case ConsProductID.FDProductIDCons:
            IsDraft = viewModel.FDModel().IsDraft();
            break;
        case ConsProductID.LoanDisbursmentProductIDCons:
        case ConsProductID.LoanIMProductIDCons:
        case ConsProductID.LoanRolloverProductIDCons:
        case ConsProductID.LoanSettlementProductIDCons:
            IsDraft = viewModel.TransactionLoanModel().IsDraft();
            break;
        case ConsProductID.UTOnshoreproductIDCons:
        case ConsProductID.UTOffshoreProductIDCons:
        case ConsProductID.UTCPFProductIDCons:
        case ConsProductID.SavingPlanProductIDCons:
        case ConsProductID.IDInvestmentProductIDCons:
            IsDraft = viewModel.TransactionUTModel().IsDraft();
            break;
        case ConsProductID.CIFProductIDCons:
            IsDraft = viewModel.CIFTransactionModel().IsDraft();
            break;
    }

    var serverRelativeUrlToFolder = '';
    if (IsDraft == true)
        serverRelativeUrlToFolder = '/DraftDocument';
    else {
        //add aridya 20161024 skn bulk ~OFFLINE~
        if (viewModel.ProductID() == ConsProductID.SKNBulkProductIDCons) {
            serverRelativeUrlToFolder = '/SKNBulkData';
        } else {
            serverRelativeUrlToFolder = '/Instruction Documents';
        }
        //end add
    }

    var parts = document[indexDocument].DocumentPath.name.split('.');
    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBuffer();
    getFile.done(function (arrayBuffer) {
        // Add the file to the SharePoint folder.
        var addFile = addFileToFolder(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {
                // Change the display name and title of the list item.
                var changeItem = updateListItem(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    //alert('file uploaded and updated');
                    //return output;
                    var newDoc = {
                        ID: 0,
                        Type: document[indexDocument].Type,
                        Purpose: document[indexDocument].Purpose,
                        LastModifiedDate: null,
                        LastModifiedBy: null,
                        DocumentPath: output.ServerRelativeUrl,
                        FileName: document[indexDocument].DocumentPath.name
                    };
                    switch (viewModel.ProductID()) {
                        case ConsProductID.SKNBulkProductIDCons: //add aridya 20161012 skn bulk ~OFFLINE~
                        case ConsProductID.RTGSProductIDCons:
                        case ConsProductID.SKNProductIDCons:
                        case ConsProductID.OTTProductIDCons:
                        case ConsProductID.OverbookingProductIDCons:
                            viewModel.TransactionModel().Documents.push(newDoc);
                            break;
                        case ConsProductID.TMOProductIDCons:
                            viewModel.TransactionTMOModel().Documents.push(newDoc);
                            break;
                        case ConsProductID.FDProductIDCons:
                            viewModel.FDModel().Documents.push(newDoc);
                            break;
                        case ConsProductID.LoanDisbursmentProductIDCons:
                        case ConsProductID.LoanIMProductIDCons:
                        case ConsProductID.LoanRolloverProductIDCons:
                        case ConsProductID.LoanSettlementProductIDCons:
                            viewModel.TransactionLoanModel().Documents.push(newDoc);
                            break;
                        case ConsProductID.UTOnshoreproductIDCons:
                        case ConsProductID.UTOffshoreProductIDCons:
                        case ConsProductID.UTCPFProductIDCons:
                        case ConsProductID.SavingPlanProductIDCons:
                        case ConsProductID.IDInvestmentProductIDCons:
                            viewModel.TransactionUTModel().Documents.push(newDoc);
                            break;
                        case ConsProductID.CIFProductIDCons:
                            viewModel.CIFTransactionModel().DocumentsCIF.push(newDoc);
                            break;
                    }
                    if (numFile > 1) {
                        UploadFileRecuresive(context, document, callBack, numFile - 1); // recursive function
                    }
                    callBack();
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBuffer() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }

        if (document[indexDocument].DocumentPath.type == Util.spitemdoc) {
            var fileURI = serverUrl + document[indexDocument].DocumentPath.DocPath;
            // load file:
            fetch(fileURI, convert, alert);

            function convert(buffer) {
                var blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });

                var domURL = self.URL || self.webkitURL || self,
                  url = domURL.createObjectURL(blob),
                  img = new Image;

                img.onload = function () {
                    domURL.revokeObjectURL(url); // clean up
                    //document.body.appendChild(this);
                    // this = image
                };
                img.src = url;
                reader.readAsArrayBuffer(blob);
            }

            function fetch(url, callback, error) {

                var xhr = new XMLHttpRequest();
                try {
                    xhr.open("GET", url);
                    xhr.responseType = "arraybuffer";
                    xhr.onerror = function () {
                        error("Network error")
                    };
                    xhr.onload = function () {
                        if (xhr.status === 200) callback(xhr.response);
                        else error(xhr.statusText);
                    };
                    xhr.send();
                } catch (err) {
                    error(err.message)
                }
            }


        } else {
            reader.readAsArrayBuffer(document[indexDocument].DocumentPath);
        }
        //bangkit

        //reader.readAsDataURL(document.DocumentPath);
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(arrayBuffer) {

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token: Rizki 2017-01-31

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                //"content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItem(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItem(itemMetadata) {
        var body = {
            Title: document[indexDocument].DocumentPath.name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: document[indexDocument].Type.Name,
            Document_x0020_Purpose: document[indexDocument].Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };

        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token: Rizki 2017-01-31

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}
// On success GetData callback
function OnSuccessGetUnderlyingProforma(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        SetSelectedProforma(data.Rows);

        self.CustomerUnderlyingProformas(data.Rows);

        self.UnderlyingProformaGridProperties().Page(data['Page']);
        self.UnderlyingProformaGridProperties().Size(data['Size']);
        self.UnderlyingProformaGridProperties().Total(data['Total']);
        self.UnderlyingProformaGridProperties().TotalPages(Math.ceil(self.UnderlyingProformaGridProperties().Total() / self.UnderlyingProformaGridProperties().Size()));
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
    }
}
function OnSuccessGetRateIDR(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        // bind result to observable array
        idrrate = data.RupiahRate;
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}
function OnSuccessSaveDraft(data, textStatus, jqXHR) {
    if (data.ID != null || data.ID != undefined) {
        switch (viewModel.ProductID()) {
            case ConsProductID.RTGSProductIDCons:
            case ConsProductID.SKNProductIDCons:
            case ConsProductID.OTTProductIDCons:
            case ConsProductID.OverbookingProductIDCons:
                viewModel.TransactionModel().ID(data.ID);
                break;
            case ConsProductID.TMOProductIDCons:
                break;
            case ConsProductID.UTOnshoreproductIDCons:
            case ConsProductID.UTOffshoreProductIDCons:
            case ConsProductID.UTCPFProductIDCons:
            case ConsProductID.SavingPlanProductIDCons:
            case ConsProductID.IDInvestmentProductIDCons:
                if (data.ID != null || data.ID != undefined) {
                    viewModel.TransactionUTModel().ID(data.ID);
                }
                break;
            case ConsProductID.FDProductIDCons:
                break;
            case ConsProductID.LoanDisbursmentProductIDCons:
            case ConsProductID.LoanRolloverProductIDCons:
            case ConsProductID.LoanIMProductIDCons:
            case ConsProductID.LoanSettlementProductIDCons:
                viewModel.TransactionLoanModel().ID(data.ID);
                break;
            case ConsProductID.CollateralProductIDCons:
                break;
            case ConsProductID.CIFProductIDCons:
                break;
        }
    }
    //ShowNotification("Transaction Draft Success", "Transaction draft not save attachments and underlying", "gritter-warning", true);
    ShowNotification("Transaction Draft Success", "Transaction draft save attachments and underlying", "gritter-warning", true);
    window.location = "/home/draft-transactions";
}
// Event handlers declaration start
function OnSuccessUpload(data, textStatus, jqXHR) {
    ShowNotification("Upload Success", JSON.stringify(data), "gritter-success", true);
}

function OnSuccessGetApplicationID(data, textStatus, jqXHR) {
    // set ApplicationID
    if (data != null) {
        viewModel.TransactionModel().ApplicationID(data.ApplicationID);
    }
}

function OnSuccessGetRateAmount(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        // bind result to observable array
        //if (viewModel.TransactionModel().ModePayment() != "BCP2") {
        //if (viewModel.TransactionModel().Product().Name != "RTGS" && viewModel.TransactionModel().Product().Name != "SKN") {
        //    viewModel.TransactionModel().Rate(data.RupiahRate);
        //    viewModel.Rate(data.RupiahRate);
        //}
        //} else {
        //viewModel.TransactionModel().Rate(data.RupiahRate);
        //viewModel.Rate(data.RupiahRate);
        //}
        //} else {
        viewModel.TransactionModel().Rate(data.RupiahRate);
        viewModel.Rate(data.RupiahRate);
        //}
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function OnSuccessGetParametersbyID(data, textStatus, jqXHR) {
    if (jqXHR.status == 200) {
        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };

        viewModel.Parameter().LLDs(ko.mapping.toJS(data, mapping));
    }
}
//hidayat
var filtering = function (ProductUnusedID, data) {
    var newData = [];
    $.each(data, function (a, b) {
        if (!ProductUnusedID[b.ID]) {
            newData.push(b)
        }
    });
    return newData;
}
//hidayat
function OnSuccessGetParameters(data, textStatus, jqXHR) {
    if (jqXHR.status == 200) {
        // bind result to observable array

        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };
        viewModel.Parameter().LLDs(ko.mapping.toJS(data.LLD, mapping));
        viewModel.Parameter().LLDDocuments(ko.mapping.toJS(data.LLDDocument, mapping));
        viewModel.Parameter().UnderlyingDocs(ko.mapping.toJS(data.UnderltyingDoc, mapping));
        //var newData = [];
        viewModel.Parameter().FXCompliances(ko.mapping.toJS(data.FXCompliance, mapping));
        //for (var ii = 0; ii < data.Product.length; ii++) {
        //    if (!ProductUnusedID[data.Product.ID]) {
        //        newData.push(data.Product[ii])
        //    }
        //}
        //hidayat
        data.Product = filtering(ProductUnusedID, data.Product);
        //hidayat
        viewModel.Parameter().Products(ko.mapping.toJS(data.Product, mapping));
        if (data.Product != null) {
            ko.utils.arrayForEach(data.Product, function (itemP) {
                if (itemP.ID == ConsProductID.OTTProductIDCons || itemP.ID == ConsProductID.OverbookingProductIDCons || itemP.ID == ConsProductID.RTGSProductIDCons || itemP.ID == ConsProductID.SKNProductIDCons) {
                    viewModel.DynamicProducts.push(itemP);
                }
            });
        }
        viewModel.Parameter().Channels(ko.mapping.toJS(data.Channel, mapping));
        viewModel.Parameter().Currencies(ko.mapping.toJS(data.Currency, mapping));
        viewModel.Parameter().ChargingAccountCurrencies(ko.mapping.toJS(data.Currency, mapping));
        //henggar
        viewModel.Parameter().Sundries(ko.mapping.toJS(data.Sundry, mapping));
        viewModel.Parameter().Nostroes(ko.mapping.toJS(data.Nostro, mapping));
        viewModel.Parameter().BeneficiaryCountry(ko.mapping.toJS(data.BeneficiaryCountry, mapping));
        viewModel.Parameter().TransactionRelationship(ko.mapping.toJS(data.TransactionRelationship, mapping));
        //end
        viewModel.Parameter().BizSegments(ko.mapping.toJS(data.BizSegment, mapping));
        //viewModel.Parameter().Banks(ko.mapping.toJS(data.Bank, mapping));

        viewModel.Parameter().FXCompliances(ko.mapping.toJS(data.FXCompliance, mapping));
        viewModel.Parameter().BankCharges(ko.mapping.toJS(data.ChargesType, mapping));
        viewModel.Parameter().AgentCharges(ko.mapping.toJS(data.ChargesType, mapping));
        viewModel.Parameter().DocumentTypes(ko.mapping.toJS(data.DocType, mapping));
        viewModel.Parameter().ProductType(ko.mapping.toJS(data.ProductType, mapping));
        viewModel.Parameter().BeneficiaryBusiness(ko.mapping.toJS(data.BeneficiaryBusines, mapping));
        //started by Haqi
        viewModel.Parameter().MaritalStatusIDs(ko.mapping.toJS(MaritalStatusData, mapping));
        viewModel.Parameter().DispatchModeTypes(ko.mapping.toJS(DispatchModeData, mapping));
        viewModel.Parameter().AccountTypes(ko.mapping.toJS(AccountTypeData, mapping));
        //viewModel.Parameter().TransactionTypes(ko.mapping.toJS(data.TransactionType, mapping));
        viewModel.Parameter().MaintenanceTypes(ko.mapping.toJS(data.MaintenanceType, mapping));
        viewModel.Parameter().TransactionSubTypes(ko.mapping.toJS(data.TransactionSubType, mapping));
        viewModel.Parameter().CellPhoneMethodIDs(ko.mapping.toJS(ModificationData, mapping));
        viewModel.Parameter().HomePhoneMethodIDs(ko.mapping.toJS(ModificationData, mapping));
        viewModel.Parameter().OfficePhoneMethodIDs(ko.mapping.toJS(ModificationData, mapping));
        viewModel.Parameter().FaxMethodIDs(ko.mapping.toJS(ModificationData, mapping));
        viewModel.Parameter().RiskRatingResults(ko.mapping.toJS(RiskRatingResultData, mapping));

        viewModel.Parameter().Education(ko.mapping.toJS(Education, mapping));
        viewModel.Parameter().Religion(ko.mapping.toJS(Religion, mapping));
        viewModel.Parameter().FATCAReviewStatus(ko.mapping.toJS(FATCAReviewStatus, mapping));
        viewModel.Parameter().FATCACRSStatus(ko.mapping.toJS(FATCACRSStatus, mapping));
        viewModel.Parameter().TaxPayer(ko.mapping.toJS(TaxPayer, mapping));
        viewModel.Parameter().WithholdingCertificationType(ko.mapping.toJS(WithholdingCertificationType, mapping));
        viewModel.Parameter().Sources(ko.mapping.toJS(Source, mapping));
        viewModel.Parameter().GrossIncomeCCY(ko.mapping.toJS(data.Currency, mapping));

        //end by haqi

        //start change by adi from 1 to 0
        if (data.Channel != null) {
            viewModel.Selected().Channel(data.Channel.ID);
        }
        //viewModel.Selected().Channel(1);	        
        //end

        var items = ko.utils.arrayFilter(data.PurposeDoc, function (item) {
            return item.ID != 2;
        });
        if (items != null) {
            viewModel.Parameter().DocumentPurposes([]);//ko.mapping.toJS(items, mapping));
            viewModel.Parameter().DocumentPurposesNonTMO(ko.mapping.toJS(items, mapping));

        }
        viewModel.Parameter().DocumentPurposesTMO(ko.mapping.toJS(data.PurposeDoc, mapping));

        // underlying parameter
        viewModel.GetUnderlyingParameters(data);
        // load draft transaction if hastag
        LoadDraft(viewModel.Parameter().DocumentPurposesTMO());
        // enabling form input controls
        viewModel.IsEditable(true);
    }
}

//Started by Haqi
function OnSuccessGetAllParameterSystems(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };

        viewModel.Parameter().IdentityTypeIDs(ko.mapping.toJS(data.CIF_JENIS_IDENTITAS, mapping));
        viewModel.Parameter().FundSources(ko.mapping.toJS(data.CIF_SUMBER_DANA, mapping));
        viewModel.Parameter().NetAssets(ko.mapping.toJS(data.CIF_ASSET_BERSIH, mapping));
        viewModel.Parameter().MonthlyIncomes(ko.mapping.toJS(data.CIF_PENDAPATAN_BULANAN, mapping));
        viewModel.Parameter().MonthlyExtraIncomes(ko.mapping.toJS(data.CIF_PENGHASILAN_TAMBAHAN, mapping));
        viewModel.Parameter().Jobs(ko.mapping.toJS(data.CIF_PEKERJAAN, mapping));
        viewModel.Parameter().AccountPurposes(ko.mapping.toJS(data.CIF_TUJUAN_PEMBUKAAN_REKENING, mapping));
        viewModel.Parameter().IncomeForecasts(ko.mapping.toJS(data.CIF_PERKIRAAN_DANA_MASUK, mapping));
        viewModel.Parameter().OutcomeForecasts(ko.mapping.toJS(data.CIF_PERKIRAAN_DANA_KELUAR, mapping));
        viewModel.Parameter().TransactionForecasts(ko.mapping.toJS(data.CIF_PERKIRAAN_TRANSAKSI_KELUAR, mapping));

        //CIF_Staff_Tagging
        viewModel.Parameter().DynamicTagUntages(ko.mapping.toJS(data.CIF_STAFF_TAGGING, mapping));
        //End

        if (viewModel.SelectedDDL().IdentityTypeIDs() != 0 || viewModel.SelectedDDL().IdentityTypeIDs() != null || viewModel.SelectedDDL().IdentityTypeIDs() != undefined) {
            if (viewModel.SelectedDDL().IdentityTypeIDs() != null) {
                viewModel.Selected().IdentityTypeID(viewModel.SelectedDDL().IdentityTypeIDs().ID)
            }
            if (viewModel.SelectedDDL().IdentityTypeIDs2() != null) {
                viewModel.Selected().IdentityTypeID2(viewModel.SelectedDDL().IdentityTypeIDs2().ID)
            }
            if (viewModel.SelectedDDL().IdentityTypeIDs3() != null) {
                viewModel.Selected().IdentityTypeID3(viewModel.SelectedDDL().IdentityTypeIDs3().ID)
            }
            if (viewModel.SelectedDDL().IdentityTypeIDs4() != null) {
                viewModel.Selected().IdentityTypeID4(viewModel.SelectedDDL().IdentityTypeIDs4().ID)
            }
        }
        if (viewModel.SelectedDDL().FundSources() != 0 || viewModel.SelectedDDL().FundSources() != null || viewModel.SelectedDDL().FundSources() != undefined) {
            if (viewModel.SelectedDDL().FundSources() != null)
                viewModel.Selected().FundSource(viewModel.SelectedDDL().FundSources().ID)
        }
        if (viewModel.SelectedDDL().NetAssets() != 0 || viewModel.SelectedDDL().NetAssets() != null || viewModel.SelectedDDL().NetAssets() != undefined) {
            if (viewModel.SelectedDDL().NetAssets() != null)
                viewModel.Selected().NetAsset(viewModel.SelectedDDL().NetAssets().ID)
        }
        if (viewModel.SelectedDDL().MonthlyIncomes() != 0 || viewModel.SelectedDDL().MonthlyIncomes() != null || viewModel.SelectedDDL().MonthlyIncomes() != undefined) {
            if (viewModel.SelectedDDL().MonthlyIncomes() != null)
                viewModel.Selected().MonthlyIncome(viewModel.SelectedDDL().MonthlyIncomes().ID)
        }
        if (viewModel.SelectedDDL().MonthlyExtraIncomes() != 0 || viewModel.SelectedDDL().MonthlyExtraIncomes() != null || viewModel.SelectedDDL().MonthlyExtraIncomes() != undefined) {
            if (viewModel.SelectedDDL().MonthlyExtraIncomes() != null)
                viewModel.Selected().MonthlyExtraIncome(viewModel.SelectedDDL().MonthlyExtraIncomes().ID)
        }
        if (viewModel.SelectedDDL().Jobs() != 0 || viewModel.SelectedDDL().Jobs() != null || viewModel.SelectedDDL().Jobs() != undefined) {
            if (viewModel.SelectedDDL().Jobs() != null)
                viewModel.Selected().Job(viewModel.SelectedDDL().Jobs().ID)
        }
        if (viewModel.SelectedDDL().AccountPurposes() != 0 || viewModel.SelectedDDL().AccountPurposes() != null || viewModel.SelectedDDL().AccountPurposes() != undefined) {
            if (viewModel.SelectedDDL().AccountPurposes() != null)
                viewModel.Selected().AccountPurpose(viewModel.SelectedDDL().AccountPurposes().ID)
        }
        if (viewModel.SelectedDDL().IncomeForecasts() != 0 || viewModel.SelectedDDL().IncomeForecasts() != null || viewModel.SelectedDDL().IncomeForecasts() != undefined) {
            if (viewModel.SelectedDDL().IncomeForecasts() != null)
                viewModel.Selected().IncomeForecast(viewModel.SelectedDDL().IncomeForecasts().ID)
        }
        if (viewModel.SelectedDDL().OutcomeForecasts() != 0 || viewModel.SelectedDDL().OutcomeForecasts() != null || viewModel.SelectedDDL().OutcomeForecasts() != undefined) {
            if (viewModel.SelectedDDL().OutcomeForecasts() != null)
                viewModel.Selected().OutcomeForecast(viewModel.SelectedDDL().OutcomeForecasts().ID)
        }
        if (viewModel.SelectedDDL().TransactionForecasts() != 0 || viewModel.SelectedDDL().TransactionForecasts() != null || viewModel.SelectedDDL().TransactionForecasts() != undefined) {
            if (viewModel.SelectedDDL().TransactionForecasts() != null)
                viewModel.Selected().TransactionForecast(viewModel.SelectedDDL().TransactionForecasts().ID)
        }
        if (viewModel.Parameter().DynamicTagUntages() != 0 || viewModel.Parameter().DynamicTagUntages() != null || viewModel.Parameter().DynamicTagUntages() != undefined) {
            if (viewModel.CIFTransactionModel().StaffTagging() != null || viewModel.CIFTransactionModel().StaffTagging() != undefined) {
                if (viewModel.CIFTransactionModel().StaffTagging().ID != null || viewModel.CIFTransactionModel().StaffTagging().ID != undefined || viewModel.CIFTransactionModel().StaffTagging().ID > 0) {
                    viewModel.Selected().TagUntag(viewModel.CIFTransactionModel().StaffTagging().ID);
                }
            }
        }

    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}
//end by haqi

//Agung
function OnSuccessBindInvestment(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        if (data != null) {
            if (data.FNACore == true) {
                viewModel.Selected().FNACore(ConsUTPar.fnaYes);
                viewModel.UTJoinFNA(data.UTJoin);
            }
            else {
                viewModel.Selected().FNACore(ConsUTPar.fnaNo);
                viewModel.UTJoinNonFNA(data.UTJoin);
            }

            viewModel.Selected().AccountType(data.AccountType);
            viewModel.TransactionUTModel().SolID(data.SolID);

            viewModel.TransactionUTModel().CustomerRiskEffectiveDate(viewModel.LocalDate(data.CustomerRiskEffectiveDate, true, false));
            viewModel.TransactionUTModel().RiskScore(data.RiskScore);
            viewModel.TransactionUTModel().RiskProfileExpiryDate(viewModel.LocalDate(data.RiskProfileExpiryDate, true, false));
            viewModel.TransactionUTModel().OperativeAccount(data.OperativeAccount);
            viewModel.OnUTChange();

            if (data.FNACore == true) {
                viewModel.UTJoinFNA(data.UTJoin);
            }
            else {
                viewModel.UTJoinNonFNA(data.UTJoin);
            }
        }
        else {
            viewModel.UTJoinFNA([]);
            viewModel.UTJoinNonFNA([]);
            viewModel.Selected().FNACore(null);
            viewModel.Selected().AccountType(null);
            viewModel.TransactionUTModel().SolID(null);
            viewModel.TransactionUTModel().CustomerRiskEffectiveDate(null);
            viewModel.TransactionUTModel().RiskScore(null);
            viewModel.TransactionUTModel().RiskProfileExpiryDate(null);
            viewModel.TransactionUTModel().OperativeAccount(null);
            viewModel.OnUTChange();
        }
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}
function OnSuccessBindInvestmentSP(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        if (data != null) {
            var IsSP = viewModel.IsSP();
            var IsSubscription = viewModel.IsSubscription();
            var IsRedemption = viewModel.IsRedemption();
            var IsSwitching = viewModel.IsSwitching();

            if (IsSP == true) {
                viewModel.MutualFundColl(data);
            }
            else if (IsSubscription == true) {
                viewModel.SubcriptionColl(data);
            }
            else if (IsRedemption == true) {
                viewModel.RedemptionColl(data);
            }
            else if (IsSwitching == true) {
                viewModel.SwitchingColl(data);
            }
        }
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}
function OnSuccessGetFNACores(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        viewModel.ParameterUT().FNACores(ko.mapping.toJS(data.Parsys));
        viewModel.IsEditable(true);

        if (viewModel.SelectedUTFNACore() != 0 || viewModel.SelectedUTFNACore() != null || viewModel.SelectedUTFNACore() != undefined) {
            if (viewModel.SelectedUTFNACore() == 1) {
                viewModel.Selected().FNACore(ConsUTPar.fnaYes);
            }
            else if (viewModel.SelectedUTFNACore() == 0)
            { viewModel.Selected().FNACore(ConsUTPar.fnaNo); }
            viewModel.OnUTChange();
        }
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function OnSuccessGetMaxRisk(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        viewModel.MaxRisk(data.Parsys);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}
function OnSuccessGetMinRisk(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        viewModel.MinRisk(data.Parsys);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}
function OnSuccessGetFunctionTypes(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        viewModel.ParameterUT().FunctionTypes(ko.mapping.toJS(data.Parsys));
        viewModel.IsEditable(true);

        if (viewModel.SelectedUTFunctionType() != 0 || viewModel.SelectedUTFNACore() != null || viewModel.SelectedUTFNACore() != undefined) {
            viewModel.Selected().FunctionType(viewModel.SelectedUTFunctionType())
            viewModel.OnUTChange();
        }
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function OnSuccessGetAccountTypes(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        viewModel.ParameterUT().AccountTypes(ko.mapping.toJS(data.Parsys));
        viewModel.IsEditable(true);

        if (viewModel.SelectedUTAccountType() != 0 || viewModel.SelectedUTAccountType() != null || viewModel.SelectedUTAccountType() != undefined) {
            viewModel.Selected().AccountType(viewModel.SelectedUTAccountType())
            viewModel.OnUTChange();
        }

    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function OnSuccessGetTransactionTypes(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        viewModel.ParameterUT().TransactionTypes(ko.mapping.toJS(data.TransactionType));
        viewModel.IsEditable(true);

        if (viewModel.SelectedUTTransType() != 0 || viewModel.SelectedUTTransType() != null || viewModel.SelectedUTTransType() != undefined) {
            viewModel.Selected().Transaction_Type(viewModel.SelectedUTTransType())
        }
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}
//End Agung
function GetCustomerByCIF(cif) {
    $.ajax({
        type: "GET",
        url: api.server + api.url.customer + "/" + cif,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + accessToken
        },
        crossDomain: true,
        cache: false,
        success: function (data, textStatus, jqXHR) {
            if (jqXHR.status = 200) {
                //console.log(ko.toJSON(data)); //Put the response in ObservableArray
                if (data != null) {
                    ShowNotification("Page Information", "Customer CIF has added as New Customer.", "gritter-warning", false);
                    viewModel.TransactionModel().Customer(data);

                    var update = viewModel.TransactionModel().Customer();
                    viewModel.TransactionModel().Customer(ko.mapping.toJS(update));
                    viewModel.TransactionModel().IsNewCustomer(false);

                    customerNameData = data.Name;
                    cifData = data.CIF;
                    viewModel.GetDataUnderlying();
                    viewModel.GetDataAttachFile();
                    viewModel.IsStatementA(true); //?
                    viewModel.TransactionModel().utilizationAmount(0.00);
                    PPUModel.cif = cifData;
                    //GetTotalPPU(PPUModel, OnSuccessTotal, OnErrorDeal);
                }

            } else {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // send notification
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    });
}
function OnSuccessLoadDraft(data, textStatus, jqXHR) {
    if (jqXHR.status == 200) {

        viewModel.IsLoadDraft(true);
        if (data == null) return;
        if (data.Product != null) {
            viewModel.ProductTitle(data.Product.Name);
        } else {
            data.Product = { ID: 3, Name: 'OTT' };
        }
        switch (data.Product.ID) {
            case ConsProductID.SKNBulkProductIDCons: //add aridya 20161012 skn bulk ~OFFLINE~
            case ConsProductID.RTGSProductIDCons:
            case ConsProductID.SKNProductIDCons:
            case ConsProductID.OTTProductIDCons:
            case ConsProductID.OverbookingProductIDCons:
                //LoadDraftPayment(data);
                //viewModel.ProductID(data.Product.ID);
                break;
            case ConsProductID.TMOProductIDCons:
                LoadDraftTMO(data);
                viewModel.ProductID(data.Product.ID);
                break;
            case ConsProductID.FDProductIDCons:
                viewModel.ProductID(data.Product.ID);
                GetFDParameter();
                viewModel.SetTemplate();
                LoadDraftFD(data);
                break;
            case ConsProductID.LoanDisbursmentProductIDCons:
            case ConsProductID.LoanRolloverProductIDCons:
            case ConsProductID.LoanIMProductIDCons:
            case ConsProductID.LoanSettlementProductIDCons:
                viewModel.ProductID(data.Product.ID);
                LoadDraftLoan(data);
                break;
            case ConsProductID.IDInvestmentProductIDCons:
                viewModel.ProductID(data.Product.ID);
                GetParameterSystems();
                viewModel.SetTemplate();
                LoadDraftUTIN(data);
                break;
            case ConsProductID.SavingPlanProductIDCons:
            case ConsProductID.UTOnshoreproductIDCons:
            case ConsProductID.UTOffshoreProductIDCons:
            case ConsProductID.UTCPFProductIDCons:
                viewModel.ProductID(data.Product.ID);
                GetParameterSystems();
                viewModel.SetTemplate();
                LoadDraftUTSP(data);
                break;
            case ConsProductID.CIFProductIDCons:
                viewModel.IsLoadDraftCIF(true);
                viewModel.ProductID(data.Product.ID);
                viewModel.SetTemplate();
                LoadDraftCIFRetail(data);
                break;
            default:
                break;
        }
        $('#lblNewTR').text(viewModel.ProductTitle());
        $("#header-transaction").hide();
        $("#newtransaction-form").show();
        $("#transaction-data").show();
        viewModel.SetCustomerAutoComplete();
        viewModel.SetCustomerAutoCompleteFD();
        viewModel.SetFundAutoCompleteFD();
        viewModel.SetFundAutoCompleteFrom();
        viewModel.SetFundAutoCompleteTo();
        viewModel.SetInvestmentAutoComplete();
        viewModel.SetCustomerAutoCompleteUTFNA();
        viewModel.SetCustomerAutoCompleteUTNonFNA();
        viewModel.SetSOLIDAutoComplete();
        viewModel.SetSOLIDJoinAutoComplete();
        viewModel.IsPPUOrBranchRole();

        switch (data.Product.ID) {
            case ConsProductID.SKNBulkProductIDCons: //add aridya 20161012 skn bulk ~OFFLINE~
                LoadDraftPayment(data);
                viewModel.ProductID(data.Product.ID);
                viewModel.EnableCCY(true);
                viewModel.IsOtherBank(true);
                var i = 0;
                for (i = 0; i < viewModel.SPUser().Roles.length; i++) {
                    if (Const_RoleName[viewModel.SPUser().Roles[i].ID] == "DBS PPU HEAD OFFICE Maker") {
                        viewModel.IsPPUHeadMaker(true);
                    }
                }
                break;
            case ConsProductID.RTGSProductIDCons:
            case ConsProductID.SKNProductIDCons:
            case ConsProductID.OTTProductIDCons:
            case ConsProductID.OverbookingProductIDCons:
                //Tambah Agung
                ar = window.location.hash.split('#');
                if (ar != null && ar != "") {
                    if (ar[2] == "TMOIPE") {
                        viewModel.TransactionModel().IsIPETMO(true);
                        viewModel.TransactionModel().TransactionDealID(ar[1]);
                    }
                }
                //End
                LoadDraftPayment(data);
                viewModel.IsOtherBank(true);
                viewModel.EnableCCY(true);
                viewModel.ProductID(data.Product.ID);
                //add by fandi
                //check for HO or Branch for mandatory
                var i = 0;
                for (i = 0; i < viewModel.SPUser().Roles.length; i++) {
                    if (Const_RoleName[viewModel.SPUser().Roles[i].ID] == "DBS PPU HEAD OFFICE Maker") {
                        viewModel.IsPPUHeadMaker(true);
                    }
                }

                //AutoPopulate beneBank
                viewModel.SetBankAutoCompleted();
                viewModel.SetBankBranchCompleted();
                viewModel.SetBankBranchCompletedDraft(); //add aridya 20161019 function to get IBranchBank from draft transaction
                viewModel.SetBankChagingAccount();
                //end

                /*var objLength = 0;
                objLength = $('#debit-acc-number option').length + 1;

                if ($('#debit-acc-number option[value=-]').length <= 0) {
                    $('#debit-acc-number').append('<option value=->-</option>');
                }*/

                var amount_d = formatNumber(viewModel.TransactionModel().Amount());
                $('#trxn-amount').val(amount_d);

                if (!data.IsNewCustomer) {
                    if (data.Account != null && !data.IsOtherAccountNumber && data.DebitCurrency != null) {
                        $('#debit-acc-ccy').val(data.DebitCurrency.Code);
                    }
                }
                else {
                    if (data.DraftCurrencyID != null) {
                        $('#debit-acc-number').val(data.DraftAccountNumber);
                    }
                }

                if (viewModel.TransactionModel().ModePayment() != "BCP2" && viewModel.TransactionModel().ModePayment() != null) {
                    FormValidationTrxPayment();
                }

                viewModel.SetCalculatePayment();

                if (data.LLDDocument != null) {
                    viewModel.Selected().LLDDocument(data.LLDDocument.LLDDocumentID);
                    viewModel.TransactionModel().LLDDocument(data.LLDDocument);
                    if (viewModel.Selected().LLDDocument() != Const_AmountLLD.LLDDocID) {
                        viewModel.IsLLDUndelyingAmount(true);
                        $('#lldunderlying').data({ ruleRequired: true })
                    } else {
                        viewModel.IsLLDUndelyingAmount(false);
                        $('#lldunderlying').data({ ruleRequired: false })
                    }
                }

                if (data.LLDUnderlyingAmount != null) {
                    viewModel.TransactionModel().LLDUnderlyingAmount(data.LLDUnderlyingAmount);
                    viewModel.LLDUnderlyingAmount(formatNumber(data.LLDUnderlyingAmount));
                }

                if (data.UnderlyingDoc != null) {
                    viewModel.Selected().UnderlyingDoc(data.UnderlyingDoc.ID);
                    if (viewModel.TransactionModel().UnderlyingDoc().Code == "999") {
                        viewModel.isNewUnderlying(true);
                        viewModel.TransactionModel().UnderlyingDoc().Description = data.OthersUnderlyingDoc;
                    }
                }

                if (data.Sundry != null) {
                    if (viewModel.Selected().Sundry() == null) {
                        console.log(viewModel.Selected().Sundry() == null);
                    }
                    else if (viewModel.Selected().Sundry() != null) {
                        //self.IsSundryFilled = true;
                    }
                }
                else {
                    if (viewModel.TransactionModel().BeneAccNumber() != null && viewModel.TransactionModel().BeneAccNumber() != "") {
                        //self.IsBeneFilled = false;
                        //$('#debit-sundry').data({ ruleRequired: false })
                    }
                    else if (viewModel.TransactionModel().BeneAccNumber() == null && viewModel.TransactionModel().BeneAccNumber() == "") {
                        //self.IsBeneFilled = true;
                        //$('#debit-sundry').data({ ruleRequired: true })
                    }
                }
                viewModel.IsStatusDraft(true);

                //aridya add 20170216 enforce max length dari draft
                if (viewModel.TransactionModel().ModePayment() == 'IPE') {
                    enforceMaxLengthIPE();
                }
                //end aridya
                break;
            case ConsProductID.FDProductIDCons:
                var amountfd_a = formatNumber(viewModel.FDModel().Amount());
                $('#trxn-amountFD_a').val(amountfd_a);
                var amountfd_b = formatNumber(viewModel.FDModel().Amount());
                $('#trxn-amountFD_b').val(amountfd_b);
                break;
            case ConsProductID.LoanDisbursmentProductIDCons:
            case ConsProductID.LoanRolloverProductIDCons:
            case ConsProductID.LoanIMProductIDCons:
            case ConsProductID.LoanSettlementProductIDCons:
                var amountLoan = formatNumber(viewModel.TransactionLoanModel().Amount());
                $('#trxn-amount').val(amountLoan);
                break;
            default:
                break;
        }

    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
    }
}
function LoadDraftUTIN(data) {
    viewModel.IsDraftForm(true);
    viewModel.IsNewDataFD(false);
    if (data.Product != null) {
        viewModel.Selected().Product(data.Product.ID);
        viewModel.OnProductChange();
    }
    viewModel.IsNewCustomer(data.IsNewCustomer);
    viewModel.TransactionUTModel().IsNewCustomer(data.IsNewCustomer);
    viewModel.TransactionUTModel().Customer(data.Customer);
    viewModel.TransactionUTModel().ID(data.ID);
    viewModel.TransactionUTModel().ApplicationID(data.ApplicationID);
    viewModel.TransactionUTModel().CustomerRiskEffectiveDate(viewModel.LocalDate(data.CustomerRiskEffectiveDate, true, false));

    viewModel.TransactionUTModel().SolID(data.SolID);
    viewModel.TransactionUTModel().RiskScore(data.RiskScore);
    viewModel.TransactionUTModel().RiskProfileExpiryDate(viewModel.LocalDate(data.RiskProfileExpiryDate, true, false));
    viewModel.TransactionUTModel().OperativeAccount(data.OperativeAccount);
    viewModel.TransactionUTModel().Investment(data.Investment);
    viewModel.TransactionUTModel().AttachmentRemarks(data.AttachmentRemarks);//add by adi
    viewModel.Documents(data.Documents);

    viewModel.SelectedUTFunctionType(data.FunctionType.ID);
    viewModel.SelectedUTAccountType(data.AccountType.ID);
    viewModel.SelectedUTFNACore(data.FNACore.ID);
    viewModel.UTJoinFNA(data.UTJoin);
    viewModel.UTJoinNonFNA(data.UTJoin);
    viewModel.IsLoadDraft(false);
}
function LoadDraftUTSP(data) {
    viewModel.IsDraftForm(true);
    viewModel.IsNewDataFD(false);
    if (data.Product != null) {
        viewModel.Selected().Product(data.Product.ID);
        viewModel.OnProductChange();
    }
    viewModel.TransactionUTModel().Customer(data.Customer);
    viewModel.TransactionUTModel().ID(data.ID);
    viewModel.TransactionUTModel().ApplicationID(data.ApplicationID);

    viewModel.TransactionUTModel().Investment(data.Investment);
    viewModel.TransactionUTModel().Remarks(data.Remarks);
    //add by adi
    viewModel.TransactionUTModel().AttachmentRemarks(data.AttachmentRemarks);
    //end
    viewModel.SelectedUTTransType(data.Transaction_Type.TransTypeID);

    viewModel.MutualFundColl(data.MutualFundForms);
    viewModel.SubcriptionColl(data.MutualFundForms);
    viewModel.RedemptionColl(data.MutualFundForms);
    viewModel.SwitchingColl(data.MutualFundForms);

    viewModel.Documents(data.Documents);
    viewModel.IsLoadDraft(false);
    viewModel.OnTTypeChange();
}
function LoadDraftLoan(data) {
    if (data.ApplicationDate != '1970-01-01T00:00:00') {
        viewModel.TransactionLoanModel().ApplicationDate(viewModel.LocalDate(data.ApplicationDate, true, false));
        viewModel.TransactionLoanModel().ExecutionDate(viewModel.LocalDate(data.ExecutionDate, true, false));
    }
    else {
        viewModel.TransactionLoanModel().ApplicationDate('');
        viewModel.TransactionLoanModel().ExecutionDate('');
    }

    viewModel.TransactionLoanModel().ID(data.ID);

    if (data.Currency != null) {
        viewModel.Selected().Currency(data.Currency.ID);
    }

    viewModel.TransactionLoanModel().Amount(data.Amount);
    viewModel.TransactionLoanModel().IsNewCustomer(data.IsNewCustomer);
    viewModel.TransactionLoanModel().IsTopUrgent(data.IsTopUrgent == 1 ? 1 : 0);
    viewModel.TransactionLoanModel().IsTopUrgentChain(data.IsTopUrgent == 1 ? 1 : 0);//2 ? 1 : 0);
    viewModel.TransactionLoanModel().IsNormal(data.IsNormal == 1 ? 1 : 0);
    viewModel.TransactionLoanModel().ApplicationID(data.ApplicationID);

    viewModel.TransactionLoanModel().Customer(data.Customer);

    if (data.Channel != null) {
        viewModel.Selected().Channel(data.Channel.ID);
    }
    if (data.SourceID != null) {
        viewModel.Selected().Source(data.SourceID);
    }
    if (data.BizSegment != null) {
        viewModel.Selected().BizSegment(data.BizSegment.ID);
    }
    if (data.Product != null) {
        viewModel.Selected().Product(data.Product.ID);
        viewModel.OnProductChange();
    }
    if (data.Channel != null) {
        viewModel.Selected().Channel(data.Channel.ID);
    }
    viewModel.IsLoadDraft(false);
    if (data.Documents != null) {
        viewModel.Documents(data.Documents);
    }
}
function LoadDraftPayment(data) {
    viewModel.IsLoadDraftPayment(true);
    //add by fandi
    viewModel.IsNewDocument(true);

    if (data.Product != null) {
        viewModel.Selected().Product(data.Product.ID);
        viewModel.OnProductChange();
    }
    //end
    viewModel.IsJointAccount(true);// set default value
    viewModel.TransactionModel().IsOtherAccountNumber(data.IsOtherAccountNumber);
    viewModel.IsEmptyAccountNumber(data.IsOtherAccountNumber);
    viewModel.ProductID(data.Product.ID);
    if (ConsProductID.RTGSProductIDCons == data.Product.ID || ConsProductID.SKNProductIDCons == data.Product.ID) {
        GetLimitProduct();
    }

    if (data.ApplicationDate != '1970-01-01T00:00:00') {
        //viewModel.TransactionModel().ApplicationDate(formatDateValue(data.ApplicationDate));
        viewModel.TransactionModel().ApplicationDate(viewModel.LocalDate(data.ApplicationDate, true, false));
    }
    else {
        viewModel.TransactionModel().ApplicationDate('');
    }

    if (data.ExecutionDate != null) {
        viewModel.TransactionModel().ExecutionDate(viewModel.LocalDate(data.ExecutionDate, true, false));
    }

    if (data.Currency != null) {
        var dataNostro = ko.utils.arrayFilter(viewModel.Parameter().Nostroes(), function (item) {
            return item.Currency.ID == data.Currency.ID;
        });
        if (dataNostro != null) {
            viewModel.Parameter().DynamicNostroes(dataNostro);
        }
    }

    viewModel.TransactionModel().ID(data.ID);

    //Tambah Binding IsBeneficiaryResident
    if (data.IsBeneficiaryResident != null) {
        viewModel.TransactionModel().IsBeneficiaryResident(data.IsBeneficiaryResident);
    }

    if (data.IsResident != null) {
        viewModel.TransactionModel().IsResident(data.IsResident);
    }

    if (data.SourceID != null) {
        viewModel.Selected().Source(data.SourceID);
    }

    if (data.Currency != null) {
        viewModel.Selected().Currency(data.Currency.ID);
        viewModel.GetRateAmountDraft(data.Currency.ID);
        viewModel.IsSundryElseIDR(data.Currency.Code != 'IDR' && data.Product.ID == ConsProductID.OTTProductIDCons);
    }
    else if (data.DraftCurrencyID != null) {
        viewModel.Selected().Currency(data.DraftCurrencyID);
        viewModel.GetRateAmountDraft(data.DraftCurrencyID);
    }

    if (data.Amount == 0) {
        viewModel.TransactionModel().Amount(null);
    } else {
        viewModel.TransactionModel().Amount(data.Amount);
    }
    viewModel.TransactionModel().IsNewCustomer(data.IsNewCustomer);
    viewModel.TransactionModel().IsJointAccount(data.IsJointAccount);
    if (!data.IsNewCustomer) {
        viewModel.TransactionModel().Customer(data.Customer);

        var update = viewModel.TransactionModel().Customer();
        viewModel.TransactionModel().Customer(ko.mapping.toJS(update));
        //#region jointAccount
        var dataAccount = ko.utils.arrayFilter(viewModel.TransactionModel().Customer().Accounts, function (item) {
            var sAccount = item.IsJointAccount == null ? false : item.IsJointAccount;
            return sAccount == data.IsJointAccount;
        });
        if (!data.IsJointAccount) {
            dataAccount = AddOtherAccounts(dataAccount);
            //single account               
            viewModel.UnderlyingFilterShow(1); // show single account
            viewModel.GetDataUnderlying();
        } else {
            if (data.Account.AccountNumber != null) {
                viewModel.UnderlyingFilterAccountNumber(data.Account.AccountNumber);
                viewModel.UnderlyingFilterShow(2); // show single account
                viewModel.GetDataUnderlying();
            }
        }
        viewModel.GetDataAttachFile();
        //UnCheckUnderlyingTable();
        viewModel.DynamicAccounts(dataAccount);
        viewModel.TempSelectedUnderlying([]);
        //check customer have joint account
        var isJointAcc = ko.utils.arrayFilter(data.Customer.Accounts, function (item) {
            return true == item.IsJointAccount;
        });
        if (isJointAcc != null & isJointAcc.length == 0) {
            viewModel.IsJointAccount(false);
        }
        //#endregion
        if (data.Account != null) {
            if (!data.IsOtherAccountNumber) {
                viewModel.Selected().Account(data.Account.AccountNumber);
                if (data.DebitCurrency != null) {
                    viewModel.TransactionModel().Account().Currency.Code = data.DebitCurrency.Code;
                }
                if (data.Account != null) {
                    viewModel.Selected().Account(data.Account.AccountNumber);
                }
            }
            else {
                viewModel.Selected().Account('-');
                if (data.DebitCurrency != null) {
                    viewModel.TransactionModel().DebitCurrency().Code = data.DebitCurrency.Code;
                    viewModel.Selected().DebitCurrency(data.DebitCurrency.ID);
                }
                viewModel.TransactionModel().OtherAccountNumber(data.OtherAccountNumber);
            }

            if (viewModel.Selected().Account() != "-") {
                viewModel.TransactionModel().ChargingACCNumber(data.AccountNumber);
                viewModel.Selected().ChargingAccountCurrency(data.Account.Currency.ID);
            } else {
                if (data.DebitCurrency != null) {
                    viewModel.TransactionModel().ChargingACCNumber(data.OtherAccountNumber);
                    viewModel.Selected().ChargingAccountCurrency(data.DebitCurrency.ID);
                }
            }
        }
        customerNameData = data.Customer.Name;
        cifData = data.Customer.CIF;
        //viewModel.GetDataUnderlying();
        viewModel.GetDataAttachFile();
        viewModel.IsStatementA(true);
        viewModel.TransactionModel().utilizationAmount(0.00);

        //PPUModel.cif = cifData;
        //GetTotalPPU(PPUModel, OnSuccessTotal, OnErrorDeal);
    }
    else {
        viewModel.TransactionModel().Customer().Name(data.DraftCustomerName);
        viewModel.TransactionModel().Customer().CIF(data.DraftCIF);
        viewModel.TransactionModel().Customer().Accounts().AccountNumber = data.DraftAccountNumber;
        viewModel.TransactionModel().Account().AccountNumber = data.DraftAccountNumber;
        if (data.DraftCurrencyID != null) {
            viewModel.TransactionModel().Customer().Accounts().Currency = data.DraftCurrencyID;
            viewModel.Selected().NewCustomer().Currency(data.DraftCurrencyID);
            viewModel.TransactionModel().Customer().Accounts().AccountNumber = data.DraftAccountNumber;
            //$('#debit-acc-number').val(data.DraftAccountNumber);
        }

        // check new customer process by CIF
        GetCustomerByCIF(data.DraftCIF);
    }

    //----- end load data customer-----

    viewModel.TransactionModel().IsTopUrgent(data.IsTopUrgent == 1 ? 1 : 0);
    viewModel.TransactionModel().IsTopUrgentChain(data.IsTopUrgent == 2 ? 1 : 0);//2 ? 1 : 0);
    viewModel.TransactionModel().IsNormal(data.IsNormal == 3 ? 1 : 0);
    viewModel.TransactionModel().BeneName(data.BeneName);
    viewModel.TransactionModel().PaymentDetails(data.PaymentDetails);
    viewModel.TransactionModel().DetailCompliance(data.DetailCompliance);
    viewModel.TransactionModel().ChargingAccountName(data.ChargingAccountName);
    viewModel.TransactionModel().ChargingAccountBank(data.ChargingAccountBank);
    viewModel.TransactionModel().BeneficiaryAddress(data.BeneficiaryAddress);
    viewModel.TransactionModel().TrxRate(data.TrxRate);
    //----add azam-----
    viewModel.TransactionModel().IsCitizen(data.IsCitizen == 1 ? 1 : 0);
    viewModel.TransactionModel().IsResident(data.IsResident == 1 ? 1 : 0);
    //-----end azam
    //add aridya 20161201 for remarks
    viewModel.TransactionModel().Remarks(data.Remarks);
    //end aridya


    if (data.IsBeneficiaryResident == true) {
        viewModel.selectedOptionId(true);
    } else if (data.IsBeneficiaryResident == false) {
        viewModel.selectedOptionId(false);
    } else {
        viewModel.selectedOptionId(null);
    }
    if (data.Branch != null) {
        viewModel.TransactionModel().BranchID(data.Bank.IBranchBank[0].Code + '- ' + data.Bank.IBranchBank[0].Name);
        viewModel.TransactionModel().CityID(data.Bank.IBranchBank[0].CityCode + '- ' + data.Bank.IBranchBank[0].CityDescription);
        viewModel.TransactionModel().City(data.City);
        viewModel.TransactionModel().Branch(data.Branch);
    }
    viewModel.TransactionModel().IsBeneficiaryResident(data.IsBeneficiaryResident);

    viewModel.IsBranchFilled(true);

    viewModel.GetDataUnderlying();
    viewModel.IsOtherBank(true);
    viewModel.EnableCCY(true);

    viewModel.TransactionModel().BeneAccNumber(data.BeneAccNumber);
    viewModel.BeneAccNumberMask(data.BeneAccNumber != null ? formatBeneAccNumber(data.BeneAccNumber) : '');

    //formatAccount();

    viewModel.TransactionModel().ApplicationID(data.ApplicationID);
    //Tambahan IPE
    if (data.Sundry != null) {
        viewModel.Selected().Sundry(data.Sundry.ID);
    }
    //if (data.BeneficiaryCountry != null) {
    //    viewModel.Selected().BeneficiaryCountry(data.BeneficiaryCountry.ID);
    //}
    if (data.BeneficiaryCountry != null) {
        viewModel.Selected().BeneficiaryCountry(data.BeneficiaryCountry.ID);
    }
    if (data.TransactionRelationship != null) {
        viewModel.Selected().TransactionRelationship(data.TransactionRelationship.ID);
    }
    if (data.CBGCustomer != null) {
        viewModel.Selected().CBGCustomer(data.CBGCustomer.ID);
    }
    if (data.TransactionUsingDebitSundry != null) {
        viewModel.Selected().TransactionUsingDebitSundry(data.TransactionUsingDebitSundry.ID);
    }
    //end
    if (data.Channel != null) {
        viewModel.Selected().Channel(data.Channel.ID);
    }

    viewModel.TransactionModel().TZNumber(data.TZNumber);

    if (data.Bank != null) {
        if (data.Bank.Code == '999') {
            data.Bank.Description = data.OtherBeneBankName;
            data.Bank.SwiftCode = data.OtherBeneBankSwift;
            viewModel.IsOtherBank(false);
            viewModel.TransactionModel().IsOtherBeneBank(true);
        } else {

            viewModel.IsOtherBank(true);
            viewModel.TransactionModel().IsOtherBeneBank(false);
        }
    }
    else {
        viewModel.IsOtherBank(false);
        viewModel.TransactionModel().IsOtherBeneBank(true);
    }

    viewModel.TransactionModel().Bank(data.Bank);

    if (data.BizSegment != null) {
        viewModel.Selected().BizSegment(data.BizSegment.ID);
    }

    if (data.Sundry != null) {
        viewModel.TransactionModel().Sundry(data.Sundry);
        viewModel.Selected().Sundry(data.Sundry.ID);
    }

    if (data.Nostro != null) {
        viewModel.TransactionModel().Nostro(data.Nostro);
        viewModel.Selected().Nostro(data.Nostro.ID);
    }

    if (data.Channel != null) {
        viewModel.Selected().Channel(data.Channel.ID);
    }
    if (data.ProductType != null) {
        viewModel.Selected().ProductType(data.ProductType.ID);

    }
    if (data.BankCharges != null) {
        viewModel.TransactionModel().BankCharges(data.BankCharges);
        viewModel.Selected().BankCharges(data.BankCharges.ID);
    }
    if (data.AgentCharges != null) {
        viewModel.TransactionModel().AgentCharges(data.AgentCharges);
        viewModel.Selected().AgentCharges(data.AgentCharges.ID);
    }
    if (data.LLD != null) {
        viewModel.Selected().LLD(data.LLD.ID);
    }
    if (data.Compliance != null) {
        viewModel.Selected().FXCompliance(data.Compliance.ID);
    }

    if (data.BeneficiaryBusines != null) {
        viewModel.TransactionModel().BeneficiaryBusines(data.BeneficiaryBusines);
        viewModel.Selected().BeneficiaryBusines(data.BeneficiaryBusines.ID);
    }

    viewModel.IsEmptyAccountNumber(data.IsOtherAccountNumber);
    viewModel.TransactionModel().OtherAccountNumber(data.OtherAccountNumber);

    viewModel.IsLoadDraft(false);

    if (data.Documents != null) {
        viewModel.Documents(data.Documents);
    }
    viewModel.GetTotalTransaction();
    if (viewModel.TransactionModel().ModePayment() != "BCP2" && viewModel.TransactionModel().ModePayment() != null) {
        if (data.IsNewCustomer == true) {
            var new_currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().NewCustomer().Currency(); });
            if (new_currency != null) { viewModel.TransactionModel().DebitCurrency(new_currency); }
        }
        if (data.TransactionRate !== null && data.TransactionRate !== undefined) {
            if (data.TransactionRate == 0) {
                viewModel.TrxRate(null);
            } else {
                viewModel.TrxRate(data.TransactionRate);
            }
        }
        if (data.Product.ID == ConsProductID.RTGSProductIDCons) { //aridya 20161215 buat branch code produk skn not mandatory (|| self.ProductID() == ConsProductID.SKNProductIDCons)
            viewModel.IsBranchCode(true);
            $('#bank-branch-code').data({ ruleRequired: true });
        } else {
            viewModel.IsBranchCode(false);
            $('#bank-branch-code').data({ ruleRequired: false });
        }
        viewModel.TransactionModel().ChargingACCNumber(data.ChargingACCNumber);
        viewModel.Selected().ChargingAccountCurrency(data.ChargingAccountCurrency);
        ValidationMandotary();
        FormValidationTrxPayment();
        ValidationWaiveCharges();
        ValidationRateTZ();
        viewModel.OnChangeSundry();

        if (data.UnderlyingDoc != null) {
            viewModel.Selected().UnderlyingDoc(data.UnderlyingDoc.ID);
            if (viewModel.TransactionModel().UnderlyingDoc().Code == "999") {
                viewModel.isNewUnderlying(true);
                viewModel.TransactionModel().UnderlyingDoc().Description = data.OthersUnderlyingDoc;
            }
        }

        if (viewModel.ProductID() == ConsProductID.OTTProductIDCons || viewModel.ProductID() == ConsProductID.OverbookingProductIDCons) {
            var IsFxTransaction = (viewModel.TransactionModel().Currency().Code != 'IDR' && viewModel.TransactionModel().DebitCurrency().Code == 'IDR');
            var IsFxTransactionToIDR = (viewModel.TransactionModel().Currency().Code == 'IDR' && viewModel.TransactionModel().DebitCurrency().Code != 'IDR');

            viewModel.IsFxTransaction(IsFxTransaction);
            viewModel.IsFxTransactionToIDR(IsFxTransactionToIDR);
        }
    }

    if (viewModel.TransactionModel().IsIPETMO() == true) {
        var dfamount = 0;
        if (viewModel.TransactionModel().Customer().Underlyings != null) {
            for (var index = 0; index < viewModel.TransactionModel().Customer().Underlyings.length ; index++) {
                dfamount += viewModel.TransactionModel().Customer().Underlyings[index].UtilizeAmountDeal;
            }

            for (var i = 0; i < viewModel.TransactionModel().Customer().Underlyings.length; i++) {
                viewModel.TempSelectedUnderlying.push(new SelectUtillizeModel(viewModel.TransactionModel().Customer().Underlyings[i].ID, false, viewModel.TransactionModel().Customer().Underlyings[i].UtilizeAmountDeal, viewModel.TransactionModel().Customer().Underlyings[i].StatementLetter.ID));
            }
        }
        ko.utils.arrayForEach(data.Customer.Underlyings, function (itemd, index) {
            itemd.IsEnable = true;
            itemd.UtilizeAmountDeal = formatNumber(itemd.UtilizeAmountDeal);
            itemd.TransactionID = index;
        });
        viewModel.CustomerUnderlyingsTMO(data.Customer.Underlyings);
        viewModel.TransactionModel().DFAmount(formatNumber(dfamount));
        viewModel.TransactionModel().utilizationAmount(dfamount);
        viewModel.TransactionModel().Instruction(data.Instruction);
        viewModel.TransactionModel().StatementLetter(data.StatementLetter);
        viewModel.TransactionModel().Underlying(data.Underlying);
        viewModel.TempSelectedInstruction([]);
        if (data.IsIPETMOPartial != null) {
            viewModel.TransactionModel().IsIPETMOPartial(data.IsIPETMOPartial);
        }
        viewModel.Selected().Product(undefined);
        //$("#utilization-amount").val(dfamount);
    }
    //else {
    //    if (viewModel.ProductID() == ConsProductID.OTTProductIDCons) {
    //        var IsFxTransaction = (viewModel.TransactionModel().Currency().Code != 'IDR' && viewModel.TransactionModel().DebitCurrency().Code == 'IDR');
    //        var IsFxTransactionToIDR = (viewModel.TransactionModel().Currency().Code == 'IDR' && viewModel.TransactionModel().DebitCurrency().Code != 'IDR');

    //        viewModel.IsFxTransaction(IsFxTransaction);
    //        viewModel.IsFxTransactionToIDR(IsFxTransactionToIDR);
    //}
    //}
}
//Dani
function LoadDraftTMO(data) {
    GetCustomerByCIF(data.DraftCIF);
    if (data.ApplicationDate != '1970-01-01T00:00:00') {
        viewModel.TransactionTMOModel().ApplicationDate(viewModel.LocalDate(data.ApplicationDate, true, false));
        //viewModel.TransactionTMOModel().ExecutionDate(viewModel.LocalDate(data.ExecutionDate, true, false));
    }
    else {
        viewModel.TransactionTMOModel().ApplicationDate('');
        //viewModel.TransactionTMOModel().ExecutionDate('');
    }
    viewModel.TransactionTMOModel().ID(data.ID);
    if (data.Currency != null) {
        viewModel.Selected().Currency(data.Currency.ID);
    }
    else if (data.DraftCurrencyID != null) {
        viewModel.Selected().Currency(data.DraftCurrencyID);
    }
    if (data.Remarks != null) {

    }
    viewModel.TransactionTMOModel().Amount(data.Amount);
    viewModel.TransactionTMOModel().IsNewCustomer(data.IsNewCustomer);
    viewModel.TransactionTMOModel().Customer(CustomerModel);
    viewModel.TransactionTMOModel().Customer().Name("");

    if (!data.IsNewCustomer) {//bukan customer baru
        if (data.CIF != null && data.Customer == null) {
            $.ajax({
                type: "GET",
                url: api.server + api.url.transactiontmo + "/GetCustomer/" + data.CIF,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                crossDomain: true,
                cache: false,
                success: function (data2, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        if (data != null) {
                            viewModel.TransactionTMOModel().Customer(data2);
                        }

                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
                }
            });
        } else if (data.Customer != null) {
            viewModel.TransactionTMOModel().Customer(data.Customer);
        }
    }
    else {//new customer

    }

    viewModel.TransactionTMOModel().ApplicationID(data.ApplicationID);
    if (data.BizSegment != null) {
        viewModel.Selected().BizSegment(data.BizSegment.ID);
    }
    if (data.Product != null) {
        viewModel.Selected().Product(data.Product.ID);
        viewModel.OnProductChange();
    }
    //console.log("Channel : ")
    //console.log(data.ChannelID);
    if (data.Channel != null) {
        viewModel.Selected().Channel(data.Channel.ID);
    } else {
        viewModel.Selected().Channel(data.ChannelID);
    }
    if (data.DealNumber != null) {
        viewModel.TransactionTMOModel().DealNumber(data.DealNumber);
    }
    if (data.ValueDate != null && data.ValueDate != '1970-01-01T00:00:00') {
        viewModel.TransactionTMOModel().ValueDate(viewModel.LocalDate(data.ValueDate))
    }
    if (data.Remarks != null) {
        viewModel.TransactionTMOModel().Remarks(data.Remarks);
    }
    if (data.Documents != null) {
        viewModel.Documents(data.Documents);
    }
    viewModel.IsLoadDraft(false);
}
//End Dani
//Haqi
function LoadDraftCIFRetail(data) {
    ClearJenisIdentitas()
    $('.date-picker').datepicker({
        autoclose: true,
        onSelect: function () {
            this.focus();
        }
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });
    viewModel.CIFTransactionModel().ApplicationID(data.ApplicationID);
    viewModel.CIFTransactionModel().Customer().CIF(data.CIF);
    viewModel.CIFTransactionModel().Customer().Name(data.DraftCustomerName);
    viewModel.CIFTransactionModel().ID(data.ID);

    if (data.Product != null) {
        viewModel.Selected().Product(data.Product.ID);
        viewModel.OnProductChange();
        CIFTransactionModel.Product(data.Product.ID);
    }
    if (data.AccountNumber != null) {
        viewModel.CIFAccountsNumber(data.Customer.Accounts);
        viewModel.Selected().cifAccNumber(data.AccountNumber.AccountNumber);
        CIFTransactionModel.AccountNumber(data.AccountNumber.AccountNumber);
    } else {
        if (data.Customer != null) {
            viewModel.CIFAccountsNumber(ko.mapping.toJS(data.Customer.Accounts));
        }
        //viewModel.Parameter().IdentityTypeIDs(ko.mapping.toJS(data.CIF_JENIS_IDENTITAS, mapping));
    }
    //end
    if (data.TransactionType != null) {
        viewModel.Selected().TransactionType(data.TransactionType.ID);
        CIFTransactionModel.TransactionType(data.TransactionType.ID);
    }

    if (data.MaintenanceType != null) {
        viewModel.Selected().MaintenanceType(data.MaintenanceType.ID);
        CIFTransactionModel.MaintenanceType(data.MaintenanceType.ID);
    }

    //Tambah TagUntag
    if (data.StaffTagging != null) {
        viewModel.Selected().TagUntag(data.StaffTagging.ID);
        CIFTransactionModel.StaffTagging(data.StaffTagging);
    }
    //End
    //29-1-2016 dani
    if (data.TransactionSubType != null) {
        viewModel.Selected().TransactionSubType(data.TransactionSubType.ID);
        CIFTransactionModel.TransactionSubType(data.TransactionSubType);//13-2-2016
    }
    if (data.Currency != null) {
        viewModel.Selected().Currency(data.Currency.CurrencyID);
        CIFTransactionModel.Currency(data.Currency);
        viewModel.Selected().CurrencyLien(data.Currency.CurrencyID);
    }
    //29-1-2016 dani end
    //return;
    //30-1-2016 dani 
    if (data.BrachRiskRating != null) {
        viewModel.CIFTransactionModel().BrachRiskRating(ko.mapping.toJS(data.BrachRiskRating));
    }
    //30-1-2016 dani end
    viewModel.SelectedDDL().IdentityTypeIDs(data.RetailCIFCBO.IdentityTypeID);
    viewModel.SelectedDDL().FundSources(data.RetailCIFCBO.FundSource);
    viewModel.SelectedDDL().NetAssets(data.RetailCIFCBO.NetAsset);
    viewModel.SelectedDDL().MonthlyIncomes(data.RetailCIFCBO.MonthlyIncome);
    viewModel.SelectedDDL().MonthlyExtraIncomes(data.RetailCIFCBO.MonthlyExtraIncome);
    viewModel.SelectedDDL().Jobs(data.RetailCIFCBO.Job);
    viewModel.SelectedDDL().AccountPurposes(data.RetailCIFCBO.AccountPurpose);
    viewModel.SelectedDDL().IncomeForecasts(data.RetailCIFCBO.IncomeForecast);
    viewModel.SelectedDDL().OutcomeForecasts(data.RetailCIFCBO.OutcomeForecast);
    viewModel.SelectedDDL().TransactionForecasts(data.RetailCIFCBO.TransactionForecast);
    viewModel.SelectedDDL().RequestType(data.RetailCIFCBO.RequestTypeID);

    viewModel.Selected().MaritalStatusID(data.RetailCIFCBO.MaritalStatusID.ID);
    viewModel.Selected().DispatchModeType(data.RetailCIFCBO.DispatchModeType.ID);
    viewModel.Selected().RiskRatingResult(data.RetailCIFCBO.RiskRatingResult.ID);



    viewModel.CIFTransactionModel().Currency(data.Currency);
    if (data.Customer != null) {
        viewModel.CIFTransactionModel().Customer(data.Customer);
    }
    //viewModel.CIFTransactionModel().DocumentsCIF(data.DocumentsCIF);
    viewModel.CIFTransactionModel().AddJoinTableCustomerCIF(data.AddJoinTableCustomerCIF);
    viewModel.CIFTransactionModel().AddJoinTableFFDAcountCIF(data.AddJoinTableFFDAcountCIF);
    viewModel.CIFTransactionModel().AddJoinTableAccountCIF(data.AddJoinTableAccountCIF);
    viewModel.CIFTransactionModel().AddJoinTableDormantCIF(data.AddJoinTableDormantCIF);
    viewModel.CIFTransactionModel().AddJoinTableFreezeUnfreezeCIF(data.AddJoinTableFreezeUnfreezeCIF);
    viewModel.CIFTransactionModel().RetailCIFCBO().IsSignature(data.RetailCIFCBO.IsSignature);
    viewModel.CIFTransactionModel().RetailCIFCBO().IsStampDuty(data.RetailCIFCBO.IsStampDuty);
    viewModel.CIFTransactionModel().RetailCIFCBO().IsPenawaranProductPerbankan(data.RetailCIFCBO.IsPenawaranProductPerbankan);
    viewModel.CIFTransactionModel().RetailCIFCBO().AmountLienUnlien(data.RetailCIFCBO.AmountLienUnlien);
    if (data.MaintenanceType != null) {
        if (data.MaintenanceType.ID != null) {
            switch (data.MaintenanceType.ID) {
                case ConsCIFMaintenanceType.AddCurrencyCons:
                    viewModel.TempAttachemntDocuments(data.AddJoinTableCustomerCIF);
                    break;
                case ConsCIFMaintenanceType.AdditionalAccountCons:
                    viewModel.TempAddAccounts(data.AddJoinTableAccountCIF);
                    break;
                case ConsCIFMaintenanceType.LinkFFDCons:
                    viewModel.TempFFDAccounts(data.AddJoinTableFFDAcountCIF);
                    break;
                case ConsCIFMaintenanceType.FreezeUnfreezeCons:
                    viewModel.TempFreezeAccountsGet(data.AddJoinTableFreezeUnfreezeCIF);
                    break;
                case ConsCIFMaintenanceType.ActiveDormantCons:
                    viewModel.TempDormantAccountsGet(data.AddJoinTableDormantCIF);
                    break;
            }
        }
    }
    viewModel.CIFTransactionModel().StaffID(data.RetailCIFCBO.StaffID);
    //added by dani 29-1-2016
    if (viewModel.CIFTransactionModel().StaffID() == undefined || viewModel.CIFTransactionModel().StaffID() == null || viewModel.CIFTransactionModel().StaffID() == "") {
        viewModel.CIFTransactionModel().StaffID(data.StaffID);
    }
    //added by dani 29-1-2016 end
    viewModel.CIFTransactionModel().ATMNumber(data.RetailCIFCBO.ATMNumber);
    //added by dani 29-1-2016
    if (viewModel.CIFTransactionModel().ATMNumber() == undefined || viewModel.CIFTransactionModel().ATMNumber() == null || viewModel.CIFTransactionModel().ATMNumber() == "") {
        viewModel.CIFTransactionModel().ATMNumber(data.ATMNumber);
    }
    if (viewModel.CIFTransactionModel().Customer().Accounts != null && viewModel.CIFTransactionModel().Customer().Accounts != undefined && viewModel.CIFTransactionModel().Customer().Accounts.length > 0) {
        var accountNumbers = viewModel.CIFTransactionModel().Customer().Accounts;
        var accountNumberSelected = data.AccountNumberLienUnlien;
        viewModel.CIFTransactionModel().AccountNumberLienUnlien([]);
        var arrayA = [];
        if (accountNumbers.length > 0) {
            for (var c = 0; c < accountNumbers.length; c++) {
                var cd = {
                    AccountNumber: accountNumbers[c].AccountNumber,
                    IsJointAccount: accountNumbers[c].IsJointAccount,
                    IsSelected: false,
                    LienUnlien: ""
                };
                arrayA.push(cd);
            }
        }
        if (accountNumberSelected != null && accountNumberSelected != undefined && accountNumberSelected.length > 0) {
            if (arrayA.length > 0) {
                for (var d = 0; d < arrayA.length; d++) {
                    for (var e = 0; e < accountNumberSelected.length; e++) {
                        if (arrayA[d].AccountNumber == accountNumberSelected[e].AccountNumber) {
                            arrayA[d].IsSelected = accountNumberSelected[e].IsSelected;
                            arrayA[d].LienUnlien = accountNumberSelected[e].LienUnlien == "Lien" ? 1 : 2;
                        }
                    }
                }
            }            
        }
        if (arrayA.length > 0) {
            for (var i = 0; i < arrayA.length; i++) {
                var item = ko.observable(ko.mapping.fromJS(arrayA[i]));
                viewModel.CIFTransactionModel().AccountNumberLienUnlien.push(item);
            }
        }        
    }
    //added by dani 29-1-2016 end
    viewModel.CIFTransactionModel().IsLOI(data.IsLOI);
    viewModel.CIFTransactionModel().IsPOI(data.IsPOI);
    viewModel.CIFTransactionModel().IsPOA(data.IsPOA);
    viewModel.CIFTransactionModel().AccountNumberDormant(null);

    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsNameMaintenance(data.GroupCheckBox.IsNameMaintenance);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsIdentityTypeMaintenance(data.GroupCheckBox.IsIdentityTypeMaintenance);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsNPWPMaintenance(data.GroupCheckBox.IsNPWPMaintenance);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsMaritalStatusMaintenance(data.GroupCheckBox.IsMaritalStatusMaintenance);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsCorrespondenceMaintenance(data.GroupCheckBox.IsCorrespondenceMaintenance);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsIdentityAddressMaintenance(data.GroupCheckBox.IsIdentityAddressMaintenance);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsOfficeAddressMaintenance(data.GroupCheckBox.IsOfficeAddressMaintenance);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsCorrespondenseAddressMaintenance(data.GroupCheckBox.IsCorrespondenseAddressMaintenance);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPhoneFaxEmailMaintenance(data.GroupCheckBox.IsPhoneFaxEmailMaintenance);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsNationalityMaintenance(data.GroupCheckBox.IsNationalityMaintenance);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsFundSourceMaintenance(data.GroupCheckBox.IsFundSourceMaintenance);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsNetAssetMaintenance(data.GroupCheckBox.IsNetAssetMaintenance);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsMonthlyIncomeMaintenance(data.GroupCheckBox.IsMonthlyIncomeMaintenance);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsJobMaintenance(data.GroupCheckBox.IsJobMaintenance);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsAccountPurposeMaintenance(data.GroupCheckBox.IsAccountPurposeMaintenance);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsMonthlyTransactionMaintenance(data.GroupCheckBox.IsMonthlyTransactionMaintenance);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsTujuanBukaRekeningLainnya(data.GroupCheckBox.IsTujuanBukaRekeningLainnya);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsSumberDanaLainnya(data.GroupCheckBox.IsSumberDanaLainnya);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPekerjaanProfesional(data.GroupCheckBox.IsPekerjaanProfesional);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPekerjaanLainnya(data.GroupCheckBox.IsPekerjaanLainnya);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsOthers(data.GroupCheckBox.IsOthers);

    viewModel.CIFTransactionModel().RetailCIFCBO().Name(data.RetailCIFCBO.Name);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityNumber(data.RetailCIFCBO.IdentityNumber);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityStartDate(self.LocalDate(data.RetailCIFCBO.IdentityStartDate, true, false));
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityEndDate(self.LocalDate(data.RetailCIFCBO.IdentityEndDate, true, false));
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityAddress(data.RetailCIFCBO.IdentityAddress);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan(data.RetailCIFCBO.IdentityKelurahan);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan(data.RetailCIFCBO.IdentityKecamatan);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCity(data.RetailCIFCBO.IdentityCity);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityProvince(data.RetailCIFCBO.IdentityProvince);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCountry(data.RetailCIFCBO.IdentityCountry);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode(data.RetailCIFCBO.IdentityPostalCode);

    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityNumber2(data.RetailCIFCBO.IdentityNumber2);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityStartDate2(self.LocalDate(data.RetailCIFCBO.IdentityStartDate2, true, false));
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityEndDate2(self.LocalDate(data.RetailCIFCBO.IdentityEndDate2, true, false));
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityAddress2(data.RetailCIFCBO.IdentityAddress2);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan2(data.RetailCIFCBO.IdentityKelurahan2);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan2(data.RetailCIFCBO.IdentityKecamatan2);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCity2(data.RetailCIFCBO.IdentityCity2);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityProvince2(data.RetailCIFCBO.IdentityProvince2);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCountry2(data.RetailCIFCBO.IdentityCountry);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode2(data.RetailCIFCBO.IdentityPostalCode2);

    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityNumber3(data.RetailCIFCBO.IdentityNumber3);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityStartDate3(self.LocalDate(data.RetailCIFCBO.IdentityStartDate3, true, false));
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityEndDate3(self.LocalDate(data.RetailCIFCBO.IdentityEndDate3, true, false));
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityAddress3(data.RetailCIFCBO.IdentityAddress3);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan3(data.RetailCIFCBO.IdentityKelurahan3);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan3(data.RetailCIFCBO.IdentityKecamatan3);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCity3(data.RetailCIFCBO.IdentityCity3);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityProvince3(data.RetailCIFCBO.IdentityProvince3);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCountry3(data.RetailCIFCBO.IdentityCountry3);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode3(data.RetailCIFCBO.IdentityPostalCode3);

    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityNumber4(data.RetailCIFCBO.IdentityNumber4);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityStartDate4(self.LocalDate(data.RetailCIFCBO.IdentityStartDate4, true, false));
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityEndDate4(self.LocalDate(data.RetailCIFCBO.IdentityEndDate4, true, false));
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityAddress4(data.RetailCIFCBO.IdentityAddress4);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKelurahan4(data.RetailCIFCBO.IdentityKelurahan4);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityKecamatan4(data.RetailCIFCBO.IdentityKecamatan4);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCity4(data.RetailCIFCBO.IdentityCity4);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityProvince4(data.RetailCIFCBO.IdentityProvince4);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityCountry4(data.RetailCIFCBO.IdentityCountry4);
    viewModel.CIFTransactionModel().RetailCIFCBO().IdentityPostalCode4(data.RetailCIFCBO.IdentityPostalCode4);

    viewModel.CIFTransactionModel().RetailCIFCBO().NPWPNumber(data.RetailCIFCBO.NPWPNumber);
    viewModel.CIFTransactionModel().RetailCIFCBO().IsNPWPReceived(data.RetailCIFCBO.IsNPWPReceived);
    viewModel.CIFTransactionModel().RetailCIFCBO().SpouseName(data.RetailCIFCBO.SpouseName);
    viewModel.CIFTransactionModel().RetailCIFCBO().IsCorrespondenseToEmail(data.RetailCIFCBO.IsCorrespondenseToEmail);
    viewModel.CIFTransactionModel().RetailCIFCBO().CorrespondenseAddress(data.RetailCIFCBO.CorrespondenseAddress);
    viewModel.CIFTransactionModel().RetailCIFCBO().CorrespondenseKelurahan(data.RetailCIFCBO.CorrespondenseKelurahan);

    viewModel.CIFTransactionModel().RetailCIFCBO().CorrespondenseKecamatan(data.RetailCIFCBO.CorrespondenseKecamatan);
    viewModel.CIFTransactionModel().RetailCIFCBO().CorrespondenseCity(data.RetailCIFCBO.CorrespondenseCity);
    viewModel.CIFTransactionModel().RetailCIFCBO().CorrespondenseProvince(data.RetailCIFCBO.CorrespondenseProvince);
    viewModel.CIFTransactionModel().RetailCIFCBO().CorrespondenseCountry(data.RetailCIFCBO.CorrespondenseCountry);
    viewModel.CIFTransactionModel().RetailCIFCBO().CorrespondensePostalCode(data.RetailCIFCBO.CorrespondensePostalCode);

    viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedCellPhone(data.RetailCIFCBO.UpdatedCellPhone);
    viewModel.CIFTransactionModel().RetailCIFCBO().HomePhone(data.RetailCIFCBO.HomePhone);
    viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedHomePhone(data.RetailCIFCBO.UpdatedHomePhone);
    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhone(data.RetailCIFCBO.OfficePhone);
    viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedOfficePhone(data.RetailCIFCBO.UpdatedOfficePhone);
    viewModel.CIFTransactionModel().RetailCIFCBO().Fax(data.RetailCIFCBO.Fax);
    viewModel.CIFTransactionModel().RetailCIFCBO().UpdatedFax(data.RetailCIFCBO.UpdatedFax);
    viewModel.CIFTransactionModel().RetailCIFCBO().EmailAddress(data.RetailCIFCBO.EmailAddress);
    viewModel.CIFTransactionModel().RetailCIFCBO().OfficeAddress(data.RetailCIFCBO.OfficeAddress);
    viewModel.CIFTransactionModel().RetailCIFCBO().OfficeKelurahan(data.RetailCIFCBO.OfficeKelurahan);
    viewModel.CIFTransactionModel().RetailCIFCBO().OfficeKecamatan(data.RetailCIFCBO.OfficeKecamatan);
    viewModel.CIFTransactionModel().RetailCIFCBO().OfficeCity(data.RetailCIFCBO.OfficeCity);
    viewModel.CIFTransactionModel().RetailCIFCBO().OfficeProvince(data.RetailCIFCBO.OfficeProvince);
    viewModel.CIFTransactionModel().RetailCIFCBO().OfficeCountry(data.RetailCIFCBO.OfficeCountry);
    viewModel.CIFTransactionModel().RetailCIFCBO().OfficePostalCode(data.RetailCIFCBO.OfficePostalCode);
    viewModel.CIFTransactionModel().RetailCIFCBO().Nationality(data.RetailCIFCBO.Nationality);
    viewModel.CIFTransactionModel().RetailCIFCBO().UBOName(data.RetailCIFCBO.UBOName);
    viewModel.CIFTransactionModel().RetailCIFCBO().UBOIdentityType(data.RetailCIFCBO.UBOIdentityType);
    viewModel.CIFTransactionModel().RetailCIFCBO().UBOPhone(data.RetailCIFCBO.UBOPhone);
    viewModel.CIFTransactionModel().RetailCIFCBO().UBOJob(data.RetailCIFCBO.UBOJob);
    viewModel.CIFTransactionModel().RetailCIFCBO().CompanyName(data.RetailCIFCBO.CompanyName);
    viewModel.CIFTransactionModel().RetailCIFCBO().Position(data.RetailCIFCBO.Position);
    viewModel.CIFTransactionModel().RetailCIFCBO().WorkPeriod(data.RetailCIFCBO.WorkPeriod);
    viewModel.CIFTransactionModel().RetailCIFCBO().IndustryType(data.RetailCIFCBO.IndustryType);
    viewModel.CIFTransactionModel().RetailCIFCBO().ReportDate(viewModel.LocalDate(data.RetailCIFCBO.ReportDate, true, false));//updated by dani 30-1-2016
    viewModel.CIFTransactionModel().RetailCIFCBO().NextReviewDate(viewModel.LocalDate(data.RetailCIFCBO.NextReviewDate, true, false));//updated by dani 30-1-2016
    viewModel.CIFTransactionModel().RetailCIFCBO().ATMNumber(data.RetailCIFCBO.AtmNumber);
    viewModel.CIFTransactionModel().RetailCIFCBO().HubunganNasabah(data.RetailCIFCBO.HubunganNasabah);

    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsMotherMaiden(data.RetailCIFCBO.IsMotherMaiden);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsEducation(data.RetailCIFCBO.IsEducation);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsReligion(data.RetailCIFCBO.IsReligion);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsDataUBO(data.RetailCIFCBO.IsDataUBO);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsTransparansiDataPenawaranProduct(data.RetailCIFCBO.IsTransparansiDataPenawaranProduct);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsTransparansiPenggunaan(data.RetailCIFCBO.IsTransparansiPenggunaan);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPenawaranProduct(data.RetailCIFCBO.IsPenawaranProduct);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsSignature(data.RetailCIFCBO.IsSignature);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsStampDuty(data.RetailCIFCBO.IsStampDuty);
    viewModel.CIFTransactionModel().RetailCIFCBO().Education(data.RetailCIFCBO.Education);
    viewModel.CIFTransactionModel().RetailCIFCBO().Religion(data.RetailCIFCBO.Religion);
    viewModel.CIFTransactionModel().RetailCIFCBO().MotherMaiden(data.RetailCIFCBO.MotherMaiden);
    viewModel.CIFTransactionModel().RetailCIFCBO().DataUBOCif(data.RetailCIFCBO.CIFUBO);
    viewModel.CIFTransactionModel().RetailCIFCBO().DataUBOName(data.RetailCIFCBO.UBOName);
    viewModel.CIFTransactionModel().RetailCIFCBO().DataUBORelationship(data.RetailCIFCBO.UBORelationship);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsSLIK(data.RetailCIFCBO.IsSLIK);
    viewModel.CIFTransactionModel().RetailCIFCBO().GrosIncomeccy(data.RetailCIFCBO.GrosIncomeccy);
    viewModel.CIFTransactionModel().RetailCIFCBO().GrossIncomeAmount(formatNumber(data.RetailCIFCBO.GrossIncomeAmount));
    viewModel.CIFTransactionModel().RetailCIFCBO().GrossCifSpouse(data.RetailCIFCBO.GrossCifSpouse);
    viewModel.CIFTransactionModel().RetailCIFCBO().GrossSpouseName(data.RetailCIFCBO.GrossSpouseName);
    viewModel.CIFTransactionModel().RetailCIFCBO().GrossSpouseRelation(data.RetailCIFCBO.GrossSpouseRelation);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsChangeSignature(data.RetailCIFCBO.IsChangeSignature);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().ChangeSignature(data.RetailCIFCBO.ChangeSignature);
    viewModel.CIFTransactionModel().RetailCIFCBO().FatcaCountryCode(data.RetailCIFCBO.FatcaCountryCode);
    viewModel.CIFTransactionModel().RetailCIFCBO().FatcaReviewStatus(data.RetailCIFCBO.FatcaReviewStatus);
    viewModel.CIFTransactionModel().RetailCIFCBO().FatcaCRSStatus(data.RetailCIFCBO.FatcaCRSStatus);
    viewModel.CIFTransactionModel().RetailCIFCBO().FatcaTaxPayer(data.RetailCIFCBO.FatcaTaxPayer);
    viewModel.CIFTransactionModel().RetailCIFCBO().FatcaWithHolding(data.RetailCIFCBO.FatcaWithHolding);
    viewModel.CIFTransactionModel().RetailCIFCBO().FatcaDateOnForm(self.LocalDate(data.RetailCIFCBO.FatcaDateOnForm, true, false));
    viewModel.CIFTransactionModel().RetailCIFCBO().FatcaReviewStatusDate(self.LocalDate(data.RetailCIFCBO.FatcaReviewStatusDate, true, false));
    viewModel.CIFTransactionModel().RetailCIFCBO().FatcaTaxPayerID(data.RetailCIFCBO.FatcaTaxPayerID);
    viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsFatcaCRS(data.RetailCIFCBO.IsFatcaCRS);
    viewModel.CIFTransactionModel().RetailCIFCBO().Tier(data.RetailCIFCBO.Tier);

    if (viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsReligion()) {
        viewModel.Selected().Religion(data.RetailCIFCBO.Religion);
    }

    if (viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsEducation()) {
        viewModel.Selected().Education(data.RetailCIFCBO.Education);
    }

    if (viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsDataUBO()) {
        if (viewModel.CIFTransactionModel().RetailCIFCBO().DataUBOCif() == 0) {
            $('#dataUBOcif').val('');
        }
    }

    if (viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsSLIK()) {
        if (viewModel.CIFTransactionModel().RetailCIFCBO().GrossCifSpouse() == 0) {
            $('#cifspouse').val('');
        }
        viewModel.Selected().GrossIncomeCCY(data.RetailCIFCBO.GrosIncomeccy);
        if (data.RetailCIFCBO.GrossIncomeAmount == 0) {
            $('#grossspouseccy').val('');
        }
    }

    if (viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsFatcaCRS()) {
        viewModel.Selected().BeneficiaryCountry(data.RetailCIFCBO.FatcaCountryCode);
        viewModel.Selected().FATCAReviewStatus(data.RetailCIFCBO.FatcaReviewStatus);
        viewModel.Selected().FATCACRSStatus(data.RetailCIFCBO.FatcaCRSStatus);
        viewModel.Selected().TaxPayer(data.RetailCIFCBO.FatcaTaxPayer);
        viewModel.Selected().WithholdingCertificationType(data.RetailCIFCBO.FatcaWithHolding);
        if (data.RetailCIFCBO.FatcaTaxPayerID == 0) {
            $('#taxayerid').val('');
        }
    }

    viewModel.IsStatusCRS(false);
    $('#taxayertype').data({ ruleRequired: false });
    var statusCRS = viewModel.Selected().FATCACRSStatus();
    if (statusCRS == 1) {
        viewModel.IsStatusCRS(true);
        $('#taxayertype').data({ ruleRequired: true });
    } else {
        viewModel.IsStatusCRS(false);
        $('#taxayertype').data({ ruleRequired: false });
    }

    viewModel.IsTaxPayerType(false);
    $('#taxayerid').data({ ruleRequired: false });
    var statusCRS = viewModel.Selected().TaxPayer();
    if (statusCRS == null) {
        viewModel.IsTaxPayerType(false);
        $('#taxayerid').data({ ruleRequired: false });
    } else {
        viewModel.IsTaxPayerType(true);
        $('#taxayerid').data({ ruleRequired: true });
    }

    //Binding Ulang
    viewModel.Selected().CellPhoneMethodID(data.RetailCIFCBO.CellPhoneMethodID.ID);
    if (data.RetailCIFCBO.CellPhoneMethodID.ID > 0) {
        viewModel.CIFTransactionModel().RetailCIFCBO().CellPhoneMethodID(data.RetailCIFCBO.CellPhoneMethodID);
        viewModel.CIFTransactionModel().RetailCIFCBO().CellPhone(data.RetailCIFCBO.CellPhone);
    }
    if (data.RetailCIFCBO.SelularPhoneNumbers != null) {
        var a = ko.mapping.fromJS(data.RetailCIFCBO.SelularPhoneNumbers);
        viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers(a());
    }
    if (data.RetailCIFCBO.OfficePhoneNumbers != null) {
        var a = ko.mapping.fromJS(data.RetailCIFCBO.OfficePhoneNumbers);
        viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers(a());
    }
    if (data.RetailCIFCBO.HomePhoneNumbers != null) {
        var a = ko.mapping.fromJS(data.RetailCIFCBO.HomePhoneNumbers);
        viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers(a());
    }
    viewModel.Selected().HomePhoneMethodID(data.RetailCIFCBO.HomePhoneMethodID.ID);
    viewModel.Selected().OfficePhoneMethodID(data.RetailCIFCBO.OfficePhoneMethodID.ID);
    viewModel.Selected().FaxMethodID(data.RetailCIFCBO.FaxMethodID.ID);
    viewModel.Selected().MaritalStatusID(data.RetailCIFCBO.MaritalStatusID.ID);
    viewModel.Selected().DispatchModeType(data.RetailCIFCBO.DispatchModeType.ID);
    viewModel.Selected().RiskRatingResult(data.RetailCIFCBO.RiskRatingResult.ID);
    //End
    //added by dani 13-4-16 start
    //console.log(data.RetailCIFCBO.SumberDanaLainnya);
    viewModel.CIFTransactionModel().RetailCIFCBO().SumberDanaLainnya(data.RetailCIFCBO.SumberDanaLainnya);
    viewModel.CIFTransactionModel().RetailCIFCBO().PekerjaanLainnya(data.RetailCIFCBO.PekerjaanLainnya);
    viewModel.CIFTransactionModel().RetailCIFCBO().PekerjaanProfesional(data.RetailCIFCBO.PekerjaanProfesional);
    viewModel.CIFTransactionModel().RetailCIFCBO().TujuanBukaRekeningLainnya(data.RetailCIFCBO.TujuanBukaRekeningLainnya);
    if (data.GroupCheckBox.IsSumberDanaLainnya)
        viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsBeneficialOwner(true);
    //13-4-16 end
    if (data.Documents != null) {
        viewModel.DocumentsCIF(data.Documents);
    }
    viewModel.Selected().CellPhoneMethodID(data.RetailCIFCBO.CellPhoneMethodID.ID);
    viewModel.Selected().HomePhoneMethodID(data.RetailCIFCBO.HomePhoneMethodID.ID);
    viewModel.Selected().OfficePhoneMethodID(data.RetailCIFCBO.OfficePhoneMethodID.ID);
    viewModel.Selected().FaxMethodID(data.RetailCIFCBO.FaxMethodID.ID);

    viewModel.SelectedDDL().IdentityTypeIDs2(data.RetailCIFCBO.IdentityTypeID2);
    viewModel.SelectedDDL().IdentityTypeIDs3(data.RetailCIFCBO.IdentityTypeID3);
    viewModel.SelectedDDL().IdentityTypeIDs4(data.RetailCIFCBO.IdentityTypeID4);

    if (viewModel.SelectedDDL().IdentityTypeIDs().ID != null) {
        if (viewModel.SelectedDDL().IdentityTypeIDs2().ID == null || viewModel.SelectedDDL().IdentityTypeIDs2().ID == 0 && viewModel.SelectedDDL().IdentityTypeIDs3().ID == null || viewModel.SelectedDDL().IdentityTypeIDs3().ID == 0 && viewModel.SelectedDDL().IdentityTypeIDs4().ID == null || viewModel.SelectedDDL().IdentityTypeIDs4().ID == 0) {
            viewModel.IsAdd(true)
        } else {
            viewModel.IsAdd(false)
        }
    }
    if (viewModel.SelectedDDL().IdentityTypeIDs2().ID != null && viewModel.SelectedDDL().IdentityTypeIDs2().ID != 0) {
        if (viewModel.SelectedDDL().IdentityTypeIDs3().ID == null || viewModel.SelectedDDL().IdentityTypeIDs3().ID == 0 && viewModel.SelectedDDL().IdentityTypeIDs4().ID == null || viewModel.SelectedDDL().IdentityTypeIDs4().ID == 0) {
            viewModel.IsAdd2(true);
            viewModel.IsIdentitas2(true);
        }
        else {
            viewModel.IsAdd2(false);
            viewModel.IsIdentitas2(true);
        }
    } else {
        viewModel.IsAdd2(true);
        viewModel.IsIdentitas2(false);
    }

    if (viewModel.SelectedDDL().IdentityTypeIDs3().ID != null && viewModel.SelectedDDL().IdentityTypeIDs3().ID != 0) {
        if (viewModel.SelectedDDL().IdentityTypeIDs4().ID == null || viewModel.SelectedDDL().IdentityTypeIDs4().ID == 0) {
            viewModel.IsAdd3(true);
            viewModel.IsIdentitas3(true);
        }
        else {
            viewModel.IsAdd3(false);
            viewModel.IsIdentitas3(true);
        }
    } else {
        viewModel.IsAdd3(true);
        viewModel.IsIdentitas3(false);
    }

    if (viewModel.SelectedDDL().IdentityTypeIDs4().ID != null && viewModel.SelectedDDL().IdentityTypeIDs4().ID != 0) {
        viewModel.IsAdd4(false)
        viewModel.IsIdentitas4(true);
    } else {
        viewModel.IsAdd4(true);
        viewModel.IsIdentitas4(false);
    }
    if (viewModel.CIFTransactionModel().RetailCIFCBO().GroupCheckBox().IsPhoneFaxEmailMaintenance()) {
        if (viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers().length == 0) {
            var e = { RetailCIFCBOContactUpdateID: null, PhoneNumber: '', CountryCode: '', CityCode: '', UpdatePhoneNumber: '', UpdateCountryCode: '', UpdateCityCode: '', ContactType: 'Home', ActionType: 0 };
            var f = ko.mapping.fromJS(e);
            viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers.push(f);
            //viewModel.AutoCompleteHomePhoneNumber(viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneNumbers().length - 1);
        }
        if (viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers().length == 0) {
            var c = { RetailCIFCBOContactUpdateID: null, PhoneNumber: '', CountryCode: '', CityCode: '', UpdatePhoneNumber: '', UpdateCountryCode: '', UpdateCityCode: '', ContactType: 'Office', ActionType: 0 };
            var d = ko.mapping.fromJS(c);
            viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers.push(d);
            //viewModel.AutoCompleteOfficePhoneNumber(viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneNumbers().length - 1);
        }
        if (viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers().length == 0) {
            var a = { RetailCIFCBOContactUpdateID: null, PhoneNumber: '', CountryCode: '', CityCode: '', UpdatePhoneNumber: '', UpdateCountryCode: '', UpdateCityCode: '', ContactType: 'Selular', ActionType: 0 };
            var b = ko.mapping.fromJS(a);
            viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers.push(b);
            //viewModel.AutoCompleteSelularPhoneNumber(viewModel.CIFTransactionModel().RetailCIFCBO().SelularPhoneNumbers().length - 1);
        }
    }
    CIFDraftMaintenanceTypeChange();
    CIFDraftRequestTypeChange();
}
function CIFDraftMaintenanceTypeChange() {
    var MaintainType = viewModel.Selected().MaintenanceType();
    switch (MaintainType) {
        case ConsCIFMaintenanceType.PengkinianDataCons:
            viewModel.IsPengkinianDataVisible(true);
            break;
        case ConsCIFMaintenanceType.UpdateFXTierCons:
            viewModel.IsUpdateFXTierVisible(true);
            break;
        case ConsCIFMaintenanceType.AtmCardCons:
            viewModel.IsATMVisible(true);
            break;
        case ConsCIFMaintenanceType.RiskRatingCons:
            viewModel.IsRiskRatingFormVisible(true);
            break;
        case ConsCIFMaintenanceType.AddCurrencyCons:
            viewModel.IsAddCurrencyVisible(true);
            break;
        case ConsCIFMaintenanceType.AdditionalAccountCons:
            viewModel.IsAddAccountVisible(true);
            break;
        case ConsCIFMaintenanceType.LinkFFDCons:
            viewModel.IsLinktoFFDAccountVisible(true);
            break;
        case ConsCIFMaintenanceType.FreezeUnfreezeCons:
            viewModel.IsFreezeUnfreezeVisible(true);
            break;
        case ConsCIFMaintenanceType.ActiveDormantCons:
            viewModel.IsActivateDormantVisible(true);
            break;
        case ConsCIFMaintenanceType.LPSCons:
            viewModel.IsLPSVisible(true);
            break;
        case ConsCIFMaintenanceType.LOIPOIPOACons:
            viewModel.IsLOIPOIPOAVisible(true);
            break;
        case ConsCIFMaintenanceType.ChangeRMCons:
            viewModel.IsChangeRMVisible(true);
            break;
        case ConsCIFMaintenanceType.ActiveHPSPCons:
            viewModel.IsActivateHPSPVisible(true);
            break;
        case ConsCIFMaintenanceType.TagUntagStaffCons:
            viewModel.IsTagUntagStaffVisible(true);
            break;
        case ConsCIFMaintenanceType.ATMClosure:
            viewModel.IsATMClosureVisible(true);
            break;
        case ConsCIFMaintenanceType.DispatchMode:
            viewModel.IsDispatchModeVisible(true);
            break;
        default:
            break;
    }
    ///Filtering Sub type
    var ReqType = viewModel.Selected().TransactionType();
    if (ReqType == null || ReqType == undefined || ReqType == "") {
        ReqType = viewModel.CIFTransactionModel().TransactionType();
    }
    if (ReqType != null) {
        switch (ReqType) {
            case ConsTransactionType.MaintenanceCons:
            case ConsTransactionType.StandingInstructionCons:
                var dataSubTypes = ko.utils.arrayFilter(viewModel.Parameter().TransactionSubTypes(), function (dta) {
                    return dta.TransTypeID == ReqType
                });

                //29-1-2016 updated by dani 
                viewModel.Parameter().DynamicTransactionSubTypes(dataSubTypes);
                if (viewModel.Parameter().DynamicTransactionSubTypes() != undefined) {
                    if (viewModel.CIFTransactionModel().TransactionSubType().ID != null) {
                        viewModel.Selected().TransactionSubType(viewModel.CIFTransactionModel().TransactionSubType().ID);
                    }
                }
                //29-1-2016 updated by dani end
                //13-2-2016
                if (viewModel.CIFTransactionModel().Customer().Accounts != undefined) {
                    if (viewModel.CIFTransactionModel().AccountNumber() != null) {
                        viewModel.Selected().cifAccNumber(viewModel.CIFTransactionModel().AccountNumber());
                    }
                }
                //13-2-2016 end
                break;
            default:
                break;
        }
    }
}
function CIFDraftRequestTypeChange() {
    var ReqType = viewModel.Selected().TransactionType();
    switch (ReqType) {
        case ConsTransactionType.MaintenanceCons:
            viewModel.IsMaintenanceTypeVisible(true);
            break;
        case ConsTransactionType.SuspendCIFCons:
            viewModel.IsSuspendCIFVisible(true);
            break;
        case ConsTransactionType.UnsuspendCIFCons:
            viewModel.IsUnsuspendCIFVisible(true);
            break;
        case ConsTransactionType.StandingInstructionCons:
            viewModel.IsStandingInstructionVisible(true);
            break;
        default:
            return;
    }
}
//End Haqi
function OnSuccessSaveAsDraft(data, textStatus, jqXHR) {
    if (data.ID != null || data.ID != undefined) {
        viewModel.TransactionModel().ID(data.ID);
    }
    // send notification
    ShowNotification(self.TransactionModel().IsDraft() ? 'Saving Success' : 'Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);
}
function OnSuccessUpdateUnderlying(data, textStatus, jqXHR) {
    if (jqXHR.status != 200) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function OnSuccessAddListItem(data, textStatus, jqXHR) {
    window.location = "/home";
}

function OnSuccessSaveAPI(data, textStatus, jqXHR) {
    if (data.ID != null || data.ID != undefined) {
        ar = window.location.hash.split('#');
        if (ar != null) {
            if (ar[2] == "TMOIPE") {
                //Andi
                var AppID = {
                    TransactionID: data.ID,
                    ApplicationID: data.AppID
                };
                viewModel.RetIDColl([]);
                viewModel.RetIDColl.push(AppID);
                viewModel.TransactionModel().ApplicationID(data.AppID);
                //End Andi
                viewModel.TransactionModel().ID(data.ID);
                AddListItem();
            } else {
                if (viewModel.TransactionModel().ID() != null) {
                    //DeleteDraft(viewModel.TransactionModel().ID(), data.ID);
                    //Andi
                    var DraftID = viewModel.TransactionModel().ID();
                    var AppID = {
                        TransactionID: data.ID,
                        ApplicationID: data.AppID
                    };
                    viewModel.RetIDColl([]);
                    viewModel.RetIDColl.push(AppID);
                    viewModel.TransactionModel().ApplicationID(data.AppID);
                    viewModel.TransactionModel().ID(data.ID);
                    DeleteDraftTr(ProductName.payment, DraftID, data.ID);
                    //End Andi
                }
                else {
                    //Andi
                    var AppID = {
                        TransactionID: data.ID,
                        ApplicationID: data.AppID
                    };
                    viewModel.RetIDColl([]);
                    viewModel.RetIDColl.push(AppID);
                    viewModel.TransactionModel().ApplicationID(data.AppID);
                    //End Andi
                    viewModel.TransactionModel().ID(data.ID);
                    AddListItem();
                }
            }
        }

        //add aridya 20170208 add for double transaction approval history after saving transaction - Remarks Rizki - 2017-04-27
        //if (viewModel.DoubleTransactions().length > 0) {
        //    ApproveDoubleTransactionHistory();
        //}
        //end aridya
    }
}

//Dani dp
function OnSuccessSaveTMOAPI(data, textStatus, jqXHR) {
    if (data.ID != null || data.ID != undefined) {
        if (viewModel.TransactionTMOModel().ID() != null) {
            var DraftID = viewModel.TransactionTMOModel().ID();
            var AppID = {
                TransactionID: data.ID,
                ApplicationID: data.AppID
            };
            viewModel.RetIDColl([]);
            viewModel.RetIDColl.push(AppID);
            viewModel.TransactionTMOModel().ApplicationID(data.AppID);
            viewModel.TransactionTMOModel().ID(data.ID);
            DeleteDraftTr(ProductName.tmo, DraftID, data.ID);
        }
        else {
            var AppID = {
                TransactionID: data.ID,
                ApplicationID: data.AppID
            };
            viewModel.RetIDColl([]);
            viewModel.RetIDColl.push(AppID);
            viewModel.TransactionTMOModel().ApplicationID(data.AppID);
            viewModel.TransactionTMOModel().ID(data.ID);
            AddListItem();
        }
    }
}
//End Dani

//Afif
function OnSuccessSaveAPILoan(data, textStatus, jqXHR) {
    if (data.ID != null || data.ID != undefined) {
        if (viewModel.TransactionLoanModel().ID() != null) {
            var DraftID = viewModel.TransactionLoanModel().ID();
            var AppID = {
                TransactionID: data.ID,
                ApplicationID: data.AppID
            };
            viewModel.RetIDColl([]);
            viewModel.RetIDColl.push(AppID);
            viewModel.TransactionLoanModel().ApplicationID(data.AppID);
            viewModel.TransactionLoanModel().ID(data.ID);
            DeleteDraftTr(ProductName.loan, DraftID, data.ID);
        }
        else {
            var AppID = {
                TransactionID: data.ID,
                ApplicationID: data.AppID
            };
            viewModel.RetIDColl([]);
            viewModel.RetIDColl.push(AppID);
            viewModel.TransactionLoanModel().ApplicationID(data.AppID);
            viewModel.TransactionLoanModel().ID(data.ID);
            AddListItem();
        }
    }
}
//End Afif
//Agung
function OnSuccessSaveUTAPI(data, textStatus, jqXHR) {
    // Get transaction id from api result
    if (data.ID != null || data.ID != undefined) {
        if (viewModel.TransactionUTModel().ID() != null) {
            var DraftID = viewModel.TransactionUTModel().ID();
            var AppID = {
                TransactionID: data.ID,
                ApplicationID: data.AppID
            };
            viewModel.RetIDColl([]);
            viewModel.RetIDColl.push(AppID);
            viewModel.TransactionUTModel().ApplicationID(data.AppID);
            viewModel.TransactionUTModel().ID(data.ID);
            DeleteDraftTr(ProductName.ut, DraftID, data.ID);
        }
        else {
            var AppID = {
                TransactionID: data.ID,
                ApplicationID: data.AppID
            };
            viewModel.RetIDColl([]);
            viewModel.RetIDColl.push(AppID);
            viewModel.TransactionUTModel().ApplicationID(data.AppID);
            viewModel.TransactionUTModel().ID(data.ID);
            AddListItem();
        }
    }
}
//End Agung

//Haqi
function OnSuccessSaveAPICIF(data, textStatus, jqXHR) {
    if (data.ID != null || data.ID != undefined) {
        if (viewModel.CIFTransactionModel().ID() != null) {
            var DraftID = viewModel.CIFTransactionModel().ID();
            var AppID = {
                TransactionID: data.ID,
                ApplicationID: data.AppID
            };
            viewModel.RetIDColl([]);
            viewModel.RetIDColl.push(AppID);
            viewModel.CIFTransactionModel().ApplicationID(data.AppID);
            viewModel.CIFTransactionModel().ID(data.ID);
            DeleteDraftTr(ProductName.cif, DraftID, data.ID);
        }
        else {
            var AppID = {
                TransactionID: data.ID,
                ApplicationID: data.AppID
            };
            viewModel.RetIDColl([]);
            viewModel.RetIDColl.push(AppID);
            viewModel.CIFTransactionModel().ApplicationID(data.AppID);
            viewModel.CIFTransactionModel().ID(data.ID);
            AddListItem();
        }
    }

};
//End Haqi
function DeleteDraft(id, transactionID) {
    //Ajax call to delete the Customer
    $.ajax({
        type: "DELETE",
        url: api.server + api.url.transaction + "/Draft/Delete/" + id,
        //data: ko.toJSON(Product),
        headers: {
            "Authorization": "Bearer " + accessToken
        },
        success: function (data, textStatus, jqXHR) {
            // send notification
            if (jqXHR.status = 200) {
                viewModel.TransactionModel().ID(transactionID);
                AddListItem();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // send notification
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    });


};
// Event handlers declaration end

// Onsuccess Autocomplete
function OnSuccessAutoComplete(response, data, textStatus, jqXHR) {
    response($.map(data, function (item) {
        return {
            // default autocomplete object
            label: item.CIF + " - " + item.Name,
            value: item.Name,

            // custom object binding
            data: {
                CIF: item.CIF,
                Name: item.Name,
                POAName: item.POAName,
                Accounts: item.Accounts,
                IsResident: item.IsResident,
                BizSegment: item.BizSegment,
                Branch: item.Branch,
                Contacts: item.Contacts,
                Functions: item.Functions,
                RM: item.RM,
                Type: item.Type,
                Underlyings: item.Underlyings,
                LastModifiedBy: item.LastModifiedBy,
                LastModifiedDate: item.LastModifiedDate
            }
        }
    })
    );
}

function OnSuccessAutoCompleteCityCode(response, data, textStatus, jqXHR) {
    response($.map(data, function (item) {
        return {
            // default autocomplete object
            label: item.CityCode + " - " + item.Description,
            value: item.CityCode,

            // custom object binding
            data: {
                CityID: item.CityID,
                CityCode: item.CityCode,
                Description: item.Description,
                ProvinceID: item.ProvinceID,
                LastModifiedBy: item.UpdateBy,
                LastModifiedDate: item.UpdateDate
            }
        }
    })
    );
}

function OnSuccessAutoCompleteCountryCode(response, data, textStatus, jqXHR) {
    response($.map(data, function (item) {
        return {
            // default autocomplete object
            label: item.CountryCodeNumber + " - " + item.Description,
            value: item.CountryCodeNumber,

            // custom object binding
            data: {
                ID: item.ID,
                Code: item.Code,
                Description: item.Description,
                CountryCodeNumber: item.CountryCodeNumber,
                LastModifiedDate: item.UpdateDate,
                LastModifiedBy: item.UpdateBy
            }
        }
    })
    );
}

function OnSuccessAutoCompletePhoneNumber(response, data, textStatus, jqXHR) {
    response($.map(data, function (item) {
        return {
            // default autocomplete object
            label: item.PhoneNumber,
            value: item.PhoneNumber,
            // custom object binding
            data: {
                CIF: item.CIF,
                PhoneNumber: item.PhoneNumber,
                RetailCIFCBOContactID: item.RetailCIFCBOContactID,
                RetailCIFCBOID: item.RetailCIFCBOID,
                CountryCode: item.CountryCode,
                CityCode: item.CityCode,
                CustomerPhoneNumberID: item.CustomerPhoneNumberID
            }
        }
    })
    );
}

function OnSuccessAutoCompleteOfficePhoneNumber(response, data, textStatus, jqXHR) {
    response($.map(data, function (item) {
        return {
            // default autocomplete object
            label: item.PhoneNumber,
            value: item.PhoneNumber,
            // custom object binding
            data: {
                CIF: item.CIF,
                PhoneNumber: item.PhoneNumber,
                RetailCIFCBOContactID: item.RetailCIFCBOContactID,
                RetailCIFCBOID: item.RetailCIFCBOID,
                CountryCode: item.CountryCode,
                CityCode: item.CityCode,
                CustomerPhoneNumberID: item.CustomerPhoneNumberID
            }
        }
    })
    );
}

function OnSuccessAutoCompleteHomePhoneNumber(response, data, textStatus, jqXHR) {
    response($.map(data, function (item) {
        return {
            // default autocomplete object
            label: item.PhoneNumber,
            value: item.PhoneNumber,
            // custom object binding
            data: {
                CIF: item.CIF,
                PhoneNumber: item.PhoneNumber,
                RetailCIFCBOContactID: item.RetailCIFCBOContactID,
                RetailCIFCBOID: item.RetailCIFCBOID,
                CountryCode: item.CountryCode,
                CityCode: item.CityCode,
                CustomerPhoneNumberID: item.CustomerPhoneNumberID
            }
        }
    })
    );
}

function OnSuccessSolIDAutoComplete(response, data, textStatus, jqXHR) {
    response($.map(data, function (item) {
        return {
            label: item.SolID + " - " + item.Name,
            value: item.SolID,
            data: {
                ID: item.ID,
                Code: item.Code,
                Name: item.Name,
                SolID: item.SolID
            }
        }
    })
    );
}

function OnSuccessFundAutoComplete(response, data, textStatus, jqXHR) {
    response($.map(data, function (item) {
        return {
            label: item.FundCode + " - " + item.FundName,
            value: item.FundName,
            data: {
                ID: item.ID,
                OffOnshoreModel: item.OffOnshoreModel,
                FundCode: item.FundCode,
                FundName: item.FundName,
                AccountNo: item.AccountNo,
                SavingPlanDate: item.SavingPlanDate,
                IsSwitching: item.IsSwitching,
                IsSaving: item.IsSaving,
                IsDeleted: item.IsDeleted,
                LastModifiedDate: item.LastModifiedDate,
                LastModifiedBy: item.LastModifiedBy
            }
        }
    })
    );
}

function OnSuccessInvestmentAutoComplete(response, data, textStatus, jqXHR) {
    response($.map(data, function (item) {
        return {
            label: item.Investment,
            value: item.Investment,
            data: {
                InvestmentID: item.InvestmentID,
                Investment: item.Investment,
                CIF: item.CIF,
                IsUT: item.IsUT,
                IsBond: item.IsBond,
                STNumber: item.STNumber,
                CheckIN: item.CheckIN,
                CheckCIF: item.CheckCIF,
                CheckName: item.CheckName,
                CheckST: item.CheckST
            }
        }
    })
    );
}

function OnSuccessBankAutoComplete(response, data, textStatus, jqXHR) {

    //reset bene bank mapped data
    viewModel.TransactionModel().Bank().SwiftCode = '';
    viewModel.TransactionModel().Bank().BankAccount = '';
    viewModel.TransactionModel().Bank().Code = '999';
    viewModel.IsOtherBank(false);
    viewModel.TransactionModel().IsOtherBeneBank(true);
    $('#bank-code').val('');
    $('#bene-acc-number').val('');

    response($.map(data, function (item) {
        return {
            // default autocomplete object
            label: item.SwiftCode + ' - ' + item.Description,
            value: item.Description,

            // custom object binding
            data: {
                BankAccount: item.BankAccount,
                BranchCode: item.BranchCode,
                Code: item.Code,
                CommonName: item.CommonName,
                Currency: item.Currency,
                Description: item.Description,
                ID: item.ID,
                LastModifiedBy: item.LastModifiedBy,
                LastModifiedDate: item.LastModifiedDate,
                PGSL: item.PGSL,
                SwiftCode: item.SwiftCode,
                IBranchBank: item.IBranchBank
            }
        }
    })
    );
}

function OnSuccessBankChargingAccAutoComplete(response, data, textStatus, jqXHR) {
    response($.map(data, function (item) {
        return {
            // default autocomplete object
            label: item.SwiftCode + ' - ' + item.Description,
            value: item.Description,

            // custom object binding
            data: {
                BankAccount: item.BankAccount,
                BranchCode: item.BranchCode,
                Code: item.Code,
                CommonName: item.CommonName,
                Currency: item.Currency,
                Description: item.Description,
                ID: item.ID,
                LastModifiedBy: item.LastModifiedBy,
                LastModifiedDate: item.LastModifiedDate,
                PGSL: item.PGSL,
                SwiftCode: item.SwiftCode,
                IBranchBank: item.IBranchBank
            }
        }
    })
    );
}

function OnSuccesGetBankBranchOV(response, data, textStatus, jqXHR) {
    response($.map(data, function (item) {
        return {
            label: item.Code + '-' + '(' + item.Name + ')',
            value: item.Code + '-' + item.Name,
            data: {
                CityDescription: item.CityDescription,
                CityID: item.CityID,
                CityCode: item.CityCode,
                Code: item.Code,
                ID: item.ID,
                IsHO: item.IsHO,
                IsJakartaBranch: item.IsJakartaBranch,
                IsUpcountryBranch: item.IsUpcountryBranch,
                LastModifiedBy: item.LastModifiedBy,
                LastModifiedDate: item.LastModifiedDate,
                Name: item.Name,
                ProvinceID: item.ProvinceID,
                Region: item.Region,
                SolID: item.SolID
            }
        }
    })
    );
}

function OnSuccessLLDAutoComplete(response, data, textStatus, jqXHR) {
    //reset LLD null mapped data
    viewModel.TransactionModel().LLD().ID = null;
    viewModel.TransactionModel().LLD().Description = '';
    viewModel.TransactionModel().LLD().Code = '';

    response($.map(data, function (item) {
        return {
            // default autocomplete object
            label: item.Description,
            value: item.Description,

            // custom object binding
            data: {
                ID: item.ID,
                Code: item.Code,
                Description: item.Description,

            }
        }
    })
    );
}

//aridya 20170212 handle using maxLength for IPE
function enforceMaxLengthIPE() {
    if (viewModel.TransactionModel().ModePayment() !== 'BCP2') {
        if (viewModel.TransactionModel().Product().Name == 'SKN') {
            document.getElementById("bene-name").maxLength = 70;
        }
        else {
            //if (viewModel.TransactionModel().Product().Name != 'OTT') {
            document.getElementById("bene-name").maxLength = 35;
            //} else if (viewModel.TransactionModel().Product().Name == 'OTT') {
            //    document.getElementById("bene-name").maxLength = 35;
            //}
            //else {
            //    document.getElementById("bene-name").maxLength = 255;
            //}
        }
    }
}
//end aridya
function enforceMaxlength(data, event) {
    if (viewModel.TransactionModel().Product().Name == 'SKN') {
        if (event.target.value.length >= 70) {
            return false;
        }
    }
    else {
        if (viewModel.TransactionModel().Product().Name != 'OTT') {
            if (event.target.value.length >= 35) {
                return false;
            }
        }
        else {
            if (event.target.value.length >= 255) {
                return false;
            }
        }
    }

    return true;
}

function read_u() {
    var e = document.getElementById("Amount_u").value;
    e = e.toString().replace(/,/g, '');
    var res = parseInt(e) * parseFloat(viewModel.Rate_u()) / parseFloat(idrrate);
    res = Math.round(res * 100) / 100;
    res = isNaN(res) ? 0 : res; //avoid NaN
    viewModel.AmountUSD_u(parseFloat(res).toFixed(2));
    viewModel.Amount_u(formatNumber(e));
}

// Token validation On Success function
function OnSuccessToken(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // read spuser from cookie
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);
        viewModel.SPUser(ko.mapping.toJS(spUser));
    }

    // get parameter values
    GetParameters();
    //PPUModel.token = accessToken;
    //GetParameterData(PPUModel, OnSuccessGetTotal, OnErrorDeal);
    PPUModel.token = accessToken;
    GetThresholdParameter(PPUModel, OnSuccessThresholdPrm, OnErrorDeal);
    GetEmployeeLocation();
}

function OnSuccessGetTotal(dataCall) {
    idrrate = dataCall.RateIDR;;
}

function OnSuccessTotal() {
    viewModel.GetCalculating(viewModel.TransactionModel().AmountUSD());
}

// AJAX On Error Callback
function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

function OnErrorDeal(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}
// AJAX On Alway Callback
function OnAlways() {
    // $box.trigger('reloaded.ace.widget');
    if (viewModel != undefined) {
        // enabling form input controls
        viewModel.IsEditable(true);
    }
}

//Andi, 22 OCtober 2015
//Start General Function
function AddListItemBulk(productname) {
    var body;
    var urlListTransaction;
    var Listtitle;
    var ListInitGroup = GetUserRole(viewModel.SPUser().Roles);
    var ListTransactionID;
    var ListAppID;
    var ListTipe;

    switch (productname) {
        case ProductName.payment:
            break;
        case ProductName.fd:
            urlListTransaction = config.sharepoint.listIdFD;
            Listtitle = viewModel.FDModel().ApplicationID() + " - " + viewModel.FDModel().Customer().CIF;
            ListTransactionID = viewModel.FDModel().TransactionID();
            ListAppID = viewModel.FDModel().ApplicationID();
            ListTipe = config.sharepoint.metadata.listFDProduct;
            break;
        case ProductName.ut:
            break;
        case ProductName.cif:
            break;
        case ProductName.tmo:
            break;
        case ProductName.collateral:
            break;
        case ProductName.loan:
            break;
        default:
            break;
    }
    body = {
        Title: Listtitle,
        Initiator_x0020_GroupId: ListInitGroup,
        Transaction_x0020_ID: ListTransactionID,
        Application_x0020_ID: ListAppID,
        __metadata: {
            type: ListTipe
        }
    };

    UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token: Rizki 2017-01-31

    var options = {
        url: config.sharepoint.url.api + "/Lists(guid'" + urlListTransaction + "')/Items",
        data: JSON.stringify(body),
        digest: jQuery("#__REQUESTDIGEST").val()
    };

    switch (productname) {
        case ProductName.payment:
            break;
        case ProductName.fd:
            Helper.Sharepoint.List.Add(options, OnSuccessAddListItemBulkFD, OnError, OnAlways);
            break;
        case ProductName.ut:
            break;
        case ProductName.cif:
            break;
        case ProductName.tmo:
            break;
        case ProductName.collateral:
            break;
        case ProductName.loan:
            break;
        default:
            break;
    }
}
function ClearError() {
    $('.form-group').each(function () { $(this).removeClass('has-success'); });
    $('.form-group').each(function () { $(this).removeClass('has-error'); });
    $('.form-group').each(function () { $(this).removeClass('has-feedback'); });
    $('.help-block').each(function () { $(this).remove(); });
    $('.form-control-feedback').each(function () { $(this).remove(); });
}
function GetEmployeeLocation() {
    var LogName = DecodeLogin(spUser.LoginName);
    $.ajax({
        type: "GET",
        url: api.server + api.url.userapproval + "/Loc/" + LogName.trim(),
        contentType: "application/json; charset=utf-8",
        data: ko.toJSON(BranchModel),
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + accessToken
        },
        success: function (data, textStatus, jqXHR) {
            if (jqXHR.status = 200) {
                if (viewModel != undefined) {
                    if (data != null) {
                        viewModel.IsHO(data["IsHO"]);
                        viewModel.IsJakartaBranch(data["IsJakartaBranch"]);
                        viewModel.IsUpcountryBranch(data["IsUpcountryBranch"]);
                        viewModel.BranchName(data["Name"]);
                        viewModel.CIFTransactionModel().BranchName(data["Name"]);
                    }
                    else {
                        viewModel.IsHO(true);
                        viewModel.IsJakartaBranch(false);
                        viewModel.IsUpcountryBranch(false);
                        viewModel.BranchName(null);
                        viewModel.CIFTransactionModel().BranchName();
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // send notification
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    });
}
function CheckCSO() {
    var userLog = viewModel.SPUser();
    var isCSO = false;
    if (userLog != null) {
        var userRole = userLog.Roles;
        if (userRole != null) {
            for (var i = 0; i < userRole.length; i++) {
                if (Const_RoleName[userRole[i].ID].indexOf('CSO') >= 0) {
                    isCSO = true;
                }
            }
        }
    }
    viewModel.IsCSO(isCSO);
}
function CheckTMO() {
    var userLog = viewModel.SPUser();
    var isTMO = false;
    if (userLog != null) {
        var userRole = userLog.Roles;
        if (userRole != null) {
            for (var i = 0; i < userRole.length; i++) {
                if (Const_RoleName[userRole[i].ID].indexOf('DBS TMO MAKER') >= 0) {
                    isTMO = true;
                }
            }
        }
    }
    viewModel.TransactionModel().IsIPETMO(isTMO);
    viewModel.TransactionModel().IsIPETMOPartial(isTMO);
}
function CheckCustomerCenter() {
    var userLog = viewModel.SPUser();
    var isCC = false;
    if (userLog != null) {
        var userRole = userLog.Roles;
        if (userRole != null) {
            for (var i = 0; i < userRole.length; i++) {
                if (Const_RoleName[userRole[i].ID].indexOf('Customer Center') >= 0) {
                    isCC = true;
                }
            }
        }
    }
    viewModel.IsCustomerCenter(isCC);
}
function FilterCIFCCRequestType() {
    if (viewModel.IsCustomerCenter() == true) {
        var dataRType = ko.utils.arrayFilter(viewModel.Parameter().TransactionTypes(), function (dta) {
            return (dta.TransTypeID == ConsTransactionType.MaintenanceCons)
        })
    }
    else {
        var dataRType = ko.utils.arrayFilter(viewModel.Parameter().TransactionTypes(), function (dta) {
            return (dta.TransTypeID != 0)
        })
    }
    viewModel.Parameter().DynamicTransactionTypes(dataRType);
}

function OnSuccessSaveBulkDraft(data, textStatus, jqXHR) {
    if (viewModel.IsCuttOff() == true) {
        viewModel.NotifHeader("Cut Off " + viewModel.strCuttOffTime());
        viewModel.NotifTitle("Attention");
        viewModel.NotifMessage("Transaction has exceeded cut off time, you need to attach email approval to continue cut off transaction. Please open Bring up file menu.");
        $("#modal-form-Notif").modal('show');
    }
    else {
        ShowNotification("Transaction Draft Success", "Transaction draft save attachments and underlying", "gritter-success", true);
        window.location = "/home/draft-transactions";
    }
}
function OnSuccessSaveDraftCIF(data, textStatus, jqXHR) {
    if (viewModel.IsCuttOff() == true) {
        viewModel.NotifHeader("Cut Off " + viewModel.strCuttOffTime());
        viewModel.NotifTitle("Attention");
        viewModel.NotifMessage("Transaction has exceeded cut off time, you need to attach email approval to continue cut off transaction. Please open Bring up file menu.");
        $("#modal-form-Notif").modal('show');
    }
    else {
        ShowNotification("Transaction Draft Success", "Transaction draft save attachments and underlying", "gritter-success", true);
        window.location = "/home/draft-transactions";
    }
}
function OnSuccessSaveDraftUT(data, textStatus, jqXHR) {
    if (data.ID != null || data.ID != undefined) {
        viewModel.TransactionUTModel().ID(data.ID);
    }
    if (viewModel.IsCuttOff() == true) {
        viewModel.NotifHeader("Cut Off " + viewModel.strCuttOffTime());
        viewModel.NotifTitle("Attention");
        viewModel.NotifMessage("Transaction has exceeded cut off time, you need to attach email approval to continue cut off transaction. Please open Bring up file menu.");
        $("#modal-form-Notif").modal('show');
    }
    else {
        ShowNotification("Transaction Draft Success", "Transaction draft save attachments and underlying", "gritter-success", true);
        window.location = "/home/draft-transactions";
    }
}
function callBackBulk(productname, TransIndex, DocTransLength, docUploaded) {
    switch (productname) {
        case ProductName.payment:
            break;
        case ProductName.fd:
            SaveTransactionFDBulk(TransIndex, DocTransLength, docUploaded);
            break;
        case ProductName.ut:
            break;
        case ProductName.cif:
            break;
        case ProductName.tmo:
            break;
        case ProductName.collateral:
            break;
        case ProductName.loan:
            break;
        default:
            break;
    }
}
function DeleteDraftTr(productname, DraftID, NewTransactionID) {
    var apiURI = '';
    apiURI = api.server + api.url.transaction + "/Draft/Delete/" + DraftID;
    switch (productname) {
        case ProductName.payment:
            apiURI = api.server + api.url.transaction + "/Draft/Delete/" + DraftID;
            break;
        case ProductName.fd:
            apiURI = api.server + api.url.transactionfd + "/Draft/Delete/" + DraftID;
            break;
        case ProductName.ut:
            apiURI = api.server + api.url.transactionutin + "/Draft/Delete/" + DraftID;
            break;
        case ProductName.cif:
            apiURI = api.server + api.url.transactioncif + "/Draft/Delete/" + DraftID;
            break;
        case ProductName.tmo:
            apiURI = api.server + api.url.transactiontmo + "/Draft/Delete/" + DraftID;
            break;
        case ProductName.collateral:
            break;
        case ProductName.loan:
            apiURI = api.server + api.url.transactionloan + "/Draft/Delete/" + DraftID;
            break;
        default:
            break;
    }
    $.ajax({
        type: "DELETE",
        url: apiURI,
        headers: {
            "Authorization": "Bearer " + accessToken
        },
        success: function (data, textStatus, jqXHR) {
            if (jqXHR.status = 200) {
                CreateList(productname);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    });
};
function CreateList(productname) {
    var body;
    var urlListTransaction;
    var Listtitle;
    var ListInitGroup = GetUserRole(viewModel.SPUser().Roles);
    var ListTransactionID;
    var ListAppID;
    var ListTipe;

    switch (productname) {
        case ProductName.payment:
            if (StatusIPE == "BCP2") { //|| viewModel.ProductID() == ConsProductID.SKNProductIDCons) { //skn single
                urlListTransaction = config.sharepoint.listId;
                Listtitle = viewModel.TransactionModel().ApplicationID() + " - " + viewModel.TransactionModel().Customer().CIF;
                ListTransactionID = viewModel.TransactionModel().ID();
                ListAppID = viewModel.TransactionModel().ApplicationID();
                ListTipe = config.sharepoint.metadata.list;
            }
            else {
                urlListTransaction = config.sharepoint.listIdIpe;
                Listtitle = viewModel.TransactionModel().ApplicationID() + " - " + viewModel.TransactionModel().Customer().CIF;
                ListTransactionID = viewModel.TransactionModel().ID();
                ListAppID = viewModel.TransactionModel().ApplicationID();
                ListTipe = config.sharepoint.metadata.listIpe;
            }
            break;
        case ProductName.fd:
            urlListTransaction = config.sharepoint.listIdFD;
            Listtitle = viewModel.FDModel().ApplicationID() + " - " + viewModel.FDModel().Customer().CIF;
            ListTransactionID = viewModel.FDModel().TransactionID();
            ListAppID = viewModel.FDModel().ApplicationID();
            ListTipe = config.sharepoint.metadata.listFDProduct;
            break;
        case ProductName.ut:
            urlListTransaction = config.sharepoint.listIdUTProduct;
            Listtitle = viewModel.TransactionUTModel().ApplicationID() + " - " + viewModel.TransactionUTModel().Customer().CIF;
            ListTransactionID = viewModel.TransactionUTModel().ID();
            ListAppID = viewModel.TransactionUTModel().ApplicationID();
            ListTipe = config.sharepoint.metadata.listUTProduct;
            break;
        case ProductName.cif:
            urlListTransaction = config.sharepoint.listIdRetailCIF;
            Listtitle = viewModel.CIFTransactionModel().ApplicationID() + " - " + viewModel.CIFTransactionModel().Customer().CIF;
            ListTransactionID = viewModel.CIFTransactionModel().ID();
            ListAppID = viewModel.CIFTransactionModel().ApplicationID();
            ListTipe = config.sharepoint.metadata.listRetailCIF;
            break;
        case ProductName.tmo:
            urlListTransaction = config.sharepoint.listIdTMOProduct;
            Listtitle = viewModel.TransactionTMOModel().ApplicationID() + " - " + viewModel.TransactionTMOModel().Customer().CIF;
            ListTransactionID = viewModel.TransactionTMOModel().ID();
            ListAppID = viewModel.TransactionTMOModel().ApplicationID();
            ListTipe = config.sharepoint.metadata.listTMOProduct;
            break;
        case ProductName.collateral:
            break;
        case ProductName.loan:
            urlListTransaction = config.sharepoint.listIdLoan;
            Listtitle = viewModel.TransactionLoanModel().ApplicationID() + " - " + viewModel.TransactionLoanModel().Customer().CIF;
            ListTransactionID = viewModel.TransactionLoanModel().ID();
            ListAppID = viewModel.TransactionLoanModel().ApplicationID();
            ListTipe = config.sharepoint.metadata.listLoan;
            break;
        default:
            break;
    }
    body = {
        Title: Listtitle,
        Initiator_x0020_GroupId: ListInitGroup,
        Transaction_x0020_ID: ListTransactionID,
        Application_x0020_ID: ListAppID,
        __metadata: {
            type: ListTipe
        }
    };

    UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token: Rizki 2017-01-31

    var options = {
        url: config.sharepoint.url.api + "/Lists(guid'" + urlListTransaction + "')/Items",
        data: JSON.stringify(body),
        digest: jQuery("#__REQUESTDIGEST").val()
    };

    switch (productname) {
        case ProductName.payment:
            Helper.Sharepoint.List.Add(options, OnSuccessAddListWorkflow, OnError, OnAlways);
            break;
        case ProductName.fd:
            Helper.Sharepoint.List.Add(options, OnSuccessCreateListFD, OnError, OnAlways);
            break;
        case ProductName.ut:
            Helper.Sharepoint.List.Add(options, OnSuccessAddListWorkflow, OnError, OnAlways);
            break;
        case ProductName.cif:
            Helper.Sharepoint.List.Add(options, OnSuccessAddListWorkflow, OnError, OnAlways);
            break;
        case ProductName.tmo:
            Helper.Sharepoint.List.Add(options, OnSuccessAddListWorkflow, OnError, OnAlways);
            break;
        case ProductName.collateral:
            break;
        case ProductName.loan:
            Helper.Sharepoint.List.Add(options, OnSuccessAddListWorkflow, OnError, OnAlways);
            break;
        default:
            break;
    }
}
//End General

//Start Module Function FD
function GetTransactionType() {
    var options = {
        url: api.server + api.url.transactiontype + "/Product/" + viewModel.ProductID(),
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetTTypeParameters, OnError, OnAlways);
}
function GetFDParameter() {
    GetTransactionType();
    GetFDRemarks();
    viewModel.GetIquoteFD();
}
function OnSuccessGetTTypeParameters(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };
        viewModel.Parameter().TransactionType(ko.mapping.toJS(data.TransactionType, mapping));
        if (viewModel.SelectedTransactionType() != 0 || viewModel.SelectedTransactionType() != null || viewModel.SelectedTransactionType() != undefined) {
            viewModel.Selected().TransactionType(viewModel.SelectedTransactionType());
            viewModel.OnFDTransactionTypeChange();
        }
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}
function GetFDCutoff() {
    var options = {
        url: api.server + api.url.parametersystem + "/Parsys/" + ConsPARSYS.fdCuttOff,
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetCutOffFD, OnError, OnAlways);
}

function OnSuccessGetCutOffFD(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };
        if (data != null) {
            var split = data.Value.split(":");
            if (split.length > 0) {
                GetcuttOff(split[0], split[1]);
            }
        }
        else
            ShowNotification('Cut Off Empty!!!', jqXHR.responseText, 'gritter-error', true);

    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function OnSuccessGetCutOffFD(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };
        if (data != null) {
            var split = data.Value.split(":");
            if (split.length > 0) {
                GetcuttOff(split[0], split[1]);
            }
        }
        else
            ShowNotification('Cut Off Empty!!!', jqXHR.responseText, 'gritter-error', true);

    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}
function GetcuttOff(hour, minute) {
    var dtNow = new Date();
    var dt = dtNow.getDate();
    var mo = dtNow.getMonth();
    var yr = dtNow.getFullYear();
    var cuttofftime = new Date(yr, mo, dt, hour, minute);
    viewModel.CutOffTime(cuttofftime);
}
function GetFDRemarks() {
    var options = {
        url: api.server + api.url.parametersystem + "/partype/" + ConsPARSYS.fdRemarks,
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetRemarksParameters, OnError, OnAlways);
}
function OnSuccessGetRemarksParameters(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };
        viewModel.Parameter().FDRemarks(ko.mapping.toJS(data.Parsys, mapping));
        if (viewModel.SelectedFDRemarks() != 0 || viewModel.SelectedFDRemarks() != null || viewModel.SelectedFDRemarks() != undefined) {
            viewModel.Selected().FDRemarks(viewModel.SelectedFDRemarks());
        }
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}
function SetDocumentPathFDStyle() {
    $('#document-pathFD').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });
}
function SetDocumentPathCIFStyle() {
    $('#document-pathCIF').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });
}
function ClearFDTType() {
    if (viewModel.IsNewPlacement()) {
        viewModel.Selected().FDRemarks([]);
        viewModel.FDModel().FDAccNumber(null);
        viewModel.FDModel().CreditAccNumber(null);
        viewModel.FDModel().DebitAccNumber(null);
        //viewModel.FDModel().InterestRate(null);
        //viewModel.FDModel().Tenor(null);
        viewModel.FDModel().ValueDate(null);
        viewModel.FDModel().MaturityDate(null);
        viewModel.FDRemarks(null);
        viewModel.FDModel().FDBankName(null);
    }
    else if (viewModel.IsPremature()) {
        viewModel.FDModel().DebitAccNumber(null);
        //viewModel.FDModel().Tenor(null);
    }
    else if (viewModel.IsBreakMaturity()) {
        viewModel.FDModel().DebitAccNumber(null);
        //viewModel.FDModel().Tenor(null);
    }
    else if (viewModel.IsChangeInstruction()) {
        viewModel.FDModel().CreditAccNumber(null);
        //viewModel.FDModel().Tenor(null);
    }
    else if (viewModel.IsFDMaintenance()) {
        viewModel.FDModel().CreditAccNumber(null);
        viewModel.FDModel().DebitAccNumber(null);
        viewModel.FDRemarks(null);
        viewModel.FDModel().FDBankName(null);
    }
}
function SetFDValue() {
    viewModel.FDModel().Customer(viewModel.TransactionModel().Customer());
    GetFDCutoff();
    viewModel.GetIquoteFD();
}
function SaveFD(isDraft) {
    var form = $("#aspnetForm");
    form.validate();
    viewModel.IsPPUOrBranchRole();
    if (viewModel.IsPPURole() == false) {
        viewModel.FDModel().Source(null);
    }
    if (form.valid() && viewModel.FDModel().Customer().CIF != "") {
        viewModel.RetIDColl([]);
        viewModel.FdTransLoop = 0;
        viewModel.FDModel().IsDraft(isDraft);
        viewModel.FDModel().CreateDate(viewModel.TouchTimeStartDate());
        viewModel.IsEditable(false);
        var data = {
            ApplicationID: viewModel.FDModel().ApplicationID(),
            CIF: viewModel.FDModel().Customer().CIF,
            Name: viewModel.FDModel().Customer().Name
        };
        if (viewModel.IsDraftForm() == false) {
            var TransColl = viewModel.FDTransaction();
            var DocTransColl;
            for (var i = 0; i < TransColl.length ; i++) {
                if (viewModel.IsPPURole() == false) {
                    viewModel.FDTransaction()[i].Source = null
                }
                DocTransColl = TransColl[i].Documents;
                if (DocTransColl.length > 0) {
                    for (var j = 0; j < DocTransColl.length; j++) {
                        DocTransColl[j].DocumentPath.name = DocTransColl[j].DocumentPath.name.replace(/[<>:"\/\\|?!@#$%^&*]+/g, '_');
                    }
                    //for (var j = 0; j < DocTransColl.length; j++) {
                    // UploadFileBulk(data, DocTransColl[j], i, j, DocTransColl.length);
                    UploadFileBulkFD(data, DocTransColl, i, DocTransColl.length, DocTransColl.length);
                    //}
                } else {
                    SaveTransactionFDBulk(i, 0, 0);
                }

            }
        }
        else {
            //From draft(not bulk)
            if (viewModel.Documents().length > 0) {
                //for (var i = 0; i < viewModel.Documents().length; i++) {
                //  UploadFile(data, viewModel.Documents()[i], SaveTransactionFDFromDraft);
                UploadFileRecuresive(data, viewModel.Documents(), SaveTransactionFDFromDraft, viewModel.Documents().length);
                //}
            }
            else {
                viewModel.FDModel().Documents([]);
                viewModel.Documents([]);
                SaveTransactionFDFromDraft();
            }
        }
    }
    else {
        ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
    }
}
function SaveTransactionFDFromDraft() {
    if (viewModel.FDModel().Documents().length == viewModel.Documents().length) {
        var curr = viewModel.Currencies();
        var trtype = viewModel.TransactionType();
        var fdRemarks = viewModel.FDRemarks() == null ? { ID: 0, Name: '' } : viewModel.FDRemarks();
        viewModel.FDModel().Currency(curr);
        viewModel.FDModel().Remarks(fdRemarks);
        viewModel.FDModel().TransactionType(trtype);
        if (viewModel.IsCuttOff() == true) {
            viewModel.FDModel().IsBringupTask(true);
        }
        var options = {
            url: api.server + api.url.transactionfd,
            token: accessToken,
            data: ko.toJSON(viewModel.FDModel())
        };
        if (viewModel.FDModel().IsDraft() == true) {
            Helper.Ajax.Post(options, OnSuccessSaveBulkDraft, OnError, OnAlways);
        }
        else {
            Helper.Ajax.Post(options, OnSuccessSaveFD, OnError, OnAlways);
        }
    }
}
function SaveTransactionFDBulk(TransactionIndex, TransactionDocumentLength, TransactionDocumentUploaded) {
    if (TransactionDocumentLength == TransactionDocumentUploaded) {
        var attachmentremarks = viewModel.FDModel().AttachmentRemarks();
        viewModel.FDTransaction()[TransactionIndex].AttachmentRemarks = attachmentremarks;
        viewModel.FDTransaction()[TransactionIndex].CreateDate = viewModel.TouchTimeStartDate();
        if (viewModel.IsCuttOff() == true) {
            viewModel.FDTransaction()[TransactionIndex].IsBringupTask = true;
        }
        var options = {
            url: api.server + api.url.transactionfd,
            token: accessToken,
            data: ko.toJSON(viewModel.FDTransaction()[TransactionIndex])
        };
        if (viewModel.FDModel().IsDraft() == true) {
            Helper.Ajax.Post(options, OnSuccessSaveBulkDraft, OnError, OnAlways);
        }
        else {
            Helper.Ajax.Post(options, OnSuccessSaveFD, OnError, OnAlways);
        }
    }
}
function UploadFileBulk(context, document, TransIndex, docIndex, DocTransLength) {
    var serverRelativeUrlToFolder = '';
    if (viewModel.FDModel().IsDraft() == true)
        serverRelativeUrlToFolder = '/DraftDocument';
    else
        serverRelativeUrlToFolder = '/Instruction Documents';

    if (document.DocumentPath.name != undefined) {
        var parts = document.DocumentPath.name.split('.');
    } else {
        self.IsEditable(true);
    }

    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp + "." + fileExtension;
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    var output;
    var getFile = getFileBuffer();
    getFile.done(function (arrayBuffer) {
        var addFile = addFileToFolder(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;
            var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                var changeItem = updateListItem(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    var newDoc = {
                        ID: 0,
                        Type: document.Type,
                        Purpose: document.Purpose,
                        LastModifiedDate: null,
                        LastModifiedBy: null,
                        DocumentPath: output.ServerRelativeUrl,
                        FileName: document.DocumentPath.name
                    };
                    switch (viewModel.ProductID()) {
                        case ConsProductID.FDProductIDCons:
                            viewModel.FDTransaction()[TransIndex].Documents[docIndex] = newDoc;
                            break;
                    }
                    callBackBulk(ProductName.fd, TransIndex, DocTransLength, docIndex + 1);
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBuffer() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }
        reader.readAsArrayBuffer(document.DocumentPath);
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(arrayBuffer) {

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token: Rizki 2017-01-31

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                //"content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItem(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItem(itemMetadata) {
        var body = {
            Title: document.DocumentPath.name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: document.Type.Name,
            Document_x0020_Purpose: document.Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };

        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token: Rizki 2017-01-31

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}

function UploadFileBulkFD(context, document, TransIndex, DocTransLength, numFile) {

    var indexDocument = document.length - numFile;

    var serverRelativeUrlToFolder = '';
    if (viewModel.FDModel().IsDraft() == true)
        serverRelativeUrlToFolder = '/DraftDocument';
    else
        serverRelativeUrlToFolder = '/Instruction Documents';

    if (document[indexDocument].DocumentPath.name != undefined) {
        var parts = document[indexDocument].DocumentPath.name.split('.');
    } else {
        self.IsEditable = true; //self.IsEditable(true);
    }

    //updated by dani 23-6-2016
    var fileExtension = parts[parts.length - 1];
    //var timeStamp = new Date().getTime();
    var timeStamp = new Date().getTime() + Math.random();
    var timeStamp2 = timeStamp + "";
    timeStamp2 = timeStamp2.replace('.', '');
    var fileName = timeStamp2 + "." + fileExtension;
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    var output;
    var getFile = getFileBuffer();
    getFile.done(function (arrayBuffer) {
        var addFile = addFileToFolder(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;
            var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                var changeItem = updateListItem(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    var newDoc = {
                        ID: 0,
                        Type: document[indexDocument].Type,
                        Purpose: document[indexDocument].Purpose,
                        LastModifiedDate: null,
                        LastModifiedBy: null,
                        DocumentPath: output.ServerRelativeUrl,
                        FileName: document[indexDocument].DocumentPath.name.replace(/[<>:"\/\\|?!@#$%^&*]+/g, '_')
                    };
                    switch (viewModel.ProductID()) {
                        case ConsProductID.FDProductIDCons:
                            viewModel.FDTransaction()[TransIndex].Documents[indexDocument] = newDoc;
                            break;
                    }
                    callBackBulk(ProductName.fd, TransIndex, DocTransLength, indexDocument + 1);

                    if (numFile > 1) {
                        UploadFileBulkFD(context, document, TransIndex, DocTransLength, numFile - 1); // recursive function
                    }
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBuffer() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }
        reader.readAsArrayBuffer(document[indexDocument].DocumentPath);
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(arrayBuffer) {

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token: Rizki 2017-01-31

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                //"content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItem(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItem(itemMetadata) {
        var body = {
            Title: document[indexDocument].DocumentPath.name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: document[indexDocument].Type.Name,
            Document_x0020_Purpose: document[indexDocument].Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };

        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token: Rizki 2017-01-31

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}

function OnSuccessSaveFD(data, textStatus, jqXHR) {
    if (data.ID != null || data.ID != undefined) {
        if (viewModel.IsDraftForm() == true) {
            var AppID = {
                TransactionID: data.ID,
                ApplicationID: data.AppID,
                TransType: data.TransType
            };
            viewModel.RetIDColl([]);
            viewModel.RetIDColl.push(AppID);
            viewModel.FDModel().ApplicationID(data.AppID);
            viewModel.FDModel().TransactionID(data.ID);
            DeleteDraftTr(ProductName.fd, viewModel.FDModel().ID(), data.ID);
        }
        else {
            viewModel.FDModel().ID(data.ID);
            var AppID = {
                TransactionID: data.ID,
                ApplicationID: data.AppID,
                TransType: data.TransType
            };
            viewModel.RetIDColl.push(AppID);
            viewModel.FDModel().ApplicationID(data.AppID);
            viewModel.FDModel().TransactionID(data.ID);
            AddListItemBulk(ProductName.fd);
        }
    }
}
function LoadDraftFD(data) {
    viewModel.SetTemplate();
    viewModel.IsDraftForm(true);
    viewModel.IsNewDataFD(false);
    if (data.Product != null) {
        viewModel.Selected().Product(data.Product.ID);
        viewModel.OnProductChange();
    }
    //var mapping = {};
    //viewModel.FDModel(ko.mapping.fromJS(data, mapping));

    viewModel.SelectedTransactionType(data.TransactionType.TransTypeID);
    viewModel.Selected().Currency(data.Currency.ID);
    viewModel.SelectedFDRemarks(data.Remarks.Name);
    viewModel.FDModel().ValueDate(viewModel.LocalDate(data.ValueDate, true, false));
    viewModel.FDModel().MaturityDate(viewModel.LocalDate(data.MaturityDate, true, false));
    viewModel.Documents(data.Documents);
    viewModel.Selected().Channel(data.Channel.ID);
    viewModel.Selected().Source(data.SourceID);
    //viewModel.Selected().TenorFD(data.Tenor)
    viewModel.IsLoadDraft(false);
    viewModel.FDModel().Customer(data.Customer);
    viewModel.FDModel().FDAccNumber(data.FDAccNumber);
    viewModel.FDModel().ID(data.ID);
    viewModel.FDModel().TransactionID(data.TransID);
    viewModel.FDModel().ApplicationID(data.ApplicationID);
    viewModel.FDModel().Amount(data.Amount);
    viewModel.FDModel().CreditAccNumber(data.CreditAccNumber);
    viewModel.FDModel().DebitAccNumber(data.DebitAccNumber);
    viewModel.FDModel().InterestRate(data.InterestRate);
    viewModel.FDModel().Tenor(data.Tenor);
    viewModel.FDModel().FDBankName(data.FDBankName);
    viewModel.FDModel().IsTopUrgent(data.IsTopUrgent);
    viewModel.FDModel().IsTopUrgentChain(data.IsTopUrgentChain);
    viewModel.FDModel().IsNormal(data.IsNormal);
    viewModel.FDModel().IsNewCustomer(data.IsNewCustomer);
    viewModel.FDModel().AttachmentRemarks(data.AttachmentRemarks);
    viewModel.FDModel().Channel(data.Channel);
    viewModel.FDModel().FTPRate(data.FTPRate);
    viewModel.FDModel().DocsComplete(data.DocsComplete);
    viewModel.FDModel().AllInRate(data.AllInRate);
    viewModel.GetIquoteFDLoadDraft();

}
function OnSuccessAddListItemBulkFD(data, textStatus, jqXHR) {
    viewModel.FdTransLoop = viewModel.FdTransLoop + 1;
    var LoopTransaction = viewModel.FdTransLoop;
    if (viewModel.FDTransaction().length == LoopTransaction) {
        var AppIDColl = viewModel.RetIDColl();
        viewModel.ApplicationIDColl([]);
        for (var i = 0; i < LoopTransaction; i++) {
            var appColl = {
                TransactionID: AppIDColl[i].TransactionID,
                ApplicationID: AppIDColl[i].ApplicationID,
                Customer: viewModel.FDTransaction()[i].Customer,
                TransactionType: AppIDColl[i].TransType,
            }
            viewModel.ApplicationIDColl.push(appColl);
        }
        ShowNotification("Submit Transaction Success", "", "gritter-success", true);
        $("#modal-form-appID").modal('show');
        viewModel.FDModel().ApplicationID("-");
        viewModel.FDModel().TransactionID(null);
    }
}
function OnSuccessCreateListFD(data, textStatus, jqXHR) {
    var AppIDColl = viewModel.RetIDColl();
    viewModel.ApplicationIDColl([]);
    var appColl = {
        TransactionID: AppIDColl[0].TransactionID,
        ApplicationID: AppIDColl[0].ApplicationID,
        Customer: viewModel.FDModel().Customer(),
        //TransactionType: viewModel.FDModel().TransactionType(),
        TransactionType: AppIDColl[0].TransType,
    }
    viewModel.ApplicationIDColl.push(appColl);
    ShowNotification("Submit Transaction Success", "", "gritter-success", true);
    $("#modal-form-appID").modal('show');
    viewModel.FDModel().ApplicationID(AppIDColl[0].ApplicationID);
    viewModel.FDModel().TransactionID(AppIDColl[0].TransactionID);
}
function OnSuccessAddListWorkflow(data, textStatus, jqXHR) {
    var AppIDColl = viewModel.RetIDColl();
    var Cust;
    if (AppIDColl != null) {
        switch (viewModel.ProductID()) {
            //aridya 20161012 add for skn bulk ~OFFLINE~
            case ConsProductID.SKNBulkProductIDCons:
                Cust = viewModel.TransactionModel().Customer();
                viewModel.TransactionModel().ApplicationID(AppIDColl[0].ApplicationID);
                viewModel.TransactionModel().ID(AppIDColl[0].TransactionID);
                break;
                //end add
            case ConsProductID.RTGSProductIDCons:
            case ConsProductID.SKNProductIDCons:
            case ConsProductID.OTTProductIDCons:
            case ConsProductID.OverbookingProductIDCons:
                Cust = viewModel.TransactionModel().Customer();
                viewModel.TransactionModel().ApplicationID(AppIDColl[0].ApplicationID);
                viewModel.TransactionModel().ID(AppIDColl[0].TransactionID);
                break;
            case ConsProductID.TMOProductIDCons:
                Cust = viewModel.TransactionTMOModel().Customer();
                viewModel.TransactionTMOModel().ApplicationID(AppIDColl[0].ApplicationID);
                viewModel.TransactionTMOModel().ID(AppIDColl[0].TransactionID);
                break;
            case ConsProductID.UTOnshoreproductIDCons:
            case ConsProductID.UTOffshoreProductIDCons:
            case ConsProductID.UTCPFProductIDCons:
            case ConsProductID.SavingPlanProductIDCons:
            case ConsProductID.IDInvestmentProductIDCons:
                Cust = viewModel.TransactionUTModel().Customer();
                viewModel.TransactionUTModel().ApplicationID(AppIDColl[0].ApplicationID);
                viewModel.TransactionUTModel().ID(AppIDColl[0].TransactionID);
                break;
            case ConsProductID.FDProductIDCons:
                break;
            case ConsProductID.LoanDisbursmentProductIDCons:
            case ConsProductID.LoanRolloverProductIDCons:
            case ConsProductID.LoanIMProductIDCons:
            case ConsProductID.LoanSettlementProductIDCons:
                Cust = viewModel.TransactionLoanModel().Customer();
                viewModel.TransactionLoanModel().ApplicationID(AppIDColl[0].ApplicationID);
                viewModel.TransactionLoanModel().ID(AppIDColl[0].TransactionID);
                break;
            case ConsProductID.CollateralProductIDCons:
                break;
            case ConsProductID.CIFProductIDCons:
                Cust = viewModel.CIFTransactionModel().Customer();
                viewModel.CIFTransactionModel().ApplicationID(AppIDColl[0].ApplicationID);
                viewModel.CIFTransactionModel().ID(AppIDColl[0].TransactionID);
                break;
            default:
                return;
        }

        viewModel.ApplicationIDColl([]);
        var appColl = {
            TransactionID: AppIDColl[0].TransactionID,
            ApplicationID: AppIDColl[0].ApplicationID,
            Customer: Cust,
        }
        viewModel.ApplicationIDColl.push(appColl);
        ShowNotification("Submit Transaction Success", "", "gritter-success", true);
        $("#modal-form-applicationID").modal('show');
    }
    else
        window.location = "/home";
}
function OnSuccessThresholdPrm() {
    idrrate = DataModel.RateIDR;
}
//End Module Function FD
//End Andi
//dani 16-1-2016
//exp: 123,000,000.0001
function CurrencyFormat(number) {
    number = number.replace(/,/g, "");//replace all "," with ""
    var num = isNaN(parseFloat(number));
    if (!num) {
        var decimalplaces = 4;
        var decimalcharacter = ".";
        var thousandseparater = ",";
        number = parseFloat(number);
        var sign = number < 0 ? "-" : "";
        var formatted = new String(number.toFixed(decimalplaces));
        if (decimalcharacter.length && decimalcharacter != ".") { formatted = formatted.replace(/\./, decimalcharacter); }
        var integer = "";
        var fraction = "";
        var strnumber = new String(formatted);
        var dotpos = decimalcharacter.length ? strnumber.indexOf(decimalcharacter) : -1;
        if (dotpos > -1) {
            if (dotpos) { integer = strnumber.substr(0, dotpos); }
            fraction = strnumber.substr(dotpos + 1);
        }
        else { integer = strnumber; }
        if (integer) { integer = String(Math.abs(integer)); }
        while (fraction.length < decimalplaces) { fraction += "0"; }
        temparray = new Array();
        while (integer.length > 3) {
            temparray.unshift(integer.substr(-3));
            integer = integer.substr(0, integer.length - 3);
        }
        temparray.unshift(integer);
        integer = temparray.join(thousandseparater);
        return sign + integer + decimalcharacter + fraction;
    } else {
        return parseFloat(0.000);
    }
}

//dani END