﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("Region")]
    public class RegionModel
    {      

        /// <summary>
        /// Region ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Region Name.
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Type Of Ageing is must be in {1} characters.")]
        public string Name { get; set; }


        /// <summary>
        /// Region Description
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Region Description.
        /// </summary>        
        public RoleModel Role { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }




    }
    public class RegionRow : RegionModel
    {
        public int RowID { get; set; }

    }

    public class RegionGrid : Grid
    {
        public IList<RegionRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class RegionFilter : Filter { }
    #endregion

    #region Interface
    public interface IRegionRepository : IDisposable
    {
        bool GetRegion(ref IList<RegionModel> regions, ref string message);
        bool GetRegionByID(int id, ref RegionModel regions, ref string message);
        bool GetRegion(ref IList<RegionModel> regions, int limit, int index, ref string message);
        bool GetRegion(int page, int size, IList<RegionFilter> filters, string sortColumn, string sortOrder, ref RegionGrid regionGrid, ref string message);
        bool GetRegion(RegionFilter filter, ref IList<RegionModel> regions, ref string message);
        bool GetRegion(string key, int limit, ref IList<RegionModel> regions, ref string message);
        bool AddRegion(RegionModel regions, ref string message);
        bool UpdateRegion(int id, RegionModel regions, ref string message);
        bool DeleteRegion(int id, ref string message);
    }
    #endregion

    #region Repository
    public class RegionRepository : IRegionRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetRegionByID(int id, ref RegionModel regions, ref string message)
        {
            bool isSuccess = false;

            try
            {
                regions = (from a in context.Regions
                          where a.IsDeleted.Equals(false) && a.RegionID.Equals(id)
                                orderby a.RegionID
                                select new RegionModel
                                {
                                    ID = a.RegionID,
                                    Name = a.RegionName,
                                    Description = a.RegionDescription,
                                    Role = new RoleModel { ID = a.Role.RoleID, Name = a.Role.RoleName },                                    
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetRegion(ref IList<RegionModel> regions, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    regions = (from a in context.Regions                           
                                where a.IsDeleted.Equals(false)
                                     orderby new { a.RegionName}
                                     select new RegionModel
                                     {
                                         ID = a.RegionID,
                                         Name = a.RegionName,
                                         Description = a.RegionDescription,
                                         Role = new RoleModel { ID = a.Role.RoleID, Name = a.Role.RoleName },
                                         LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                         LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                     }).ToList();
                }


                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetRegion(ref IList<RegionModel> regions, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    regions = (from a in context.Regions                              
                                 where a.IsDeleted.Equals(false)
                                 orderby new { a.RegionName }
                                     select new RegionModel
                                     {
                                         ID = a.RegionID,
                                         Name = a.RegionName,
                                         Description = a.RegionDescription,
                                         Role = new RoleModel { ID = a.Role.RoleID, Name = a.Role.RoleName },
                                         LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                         LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                     }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRegion(int page, int size, IList<RegionFilter> filters, string sortColumn, string sortOrder, ref RegionGrid regionGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                RegionModel filter = new RegionModel();
                filter.Role = new RoleModel { Name = string.Empty };
                if (filters != null)
                {
                    filter.Name = (string)filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.Role.Name = (string)filters.Where(a => a.Field.Equals("Role")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.Regions
                                let isName = !string.IsNullOrEmpty(filter.Name)
                                let isDescription = !string.IsNullOrEmpty(filter.Description)
                                let isRole = !string.IsNullOrEmpty(filter.Role.Name)                                
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false)  &&
                                    (isName ? a.RegionName.Contains(filter.Name) : true) &&
                                    (isDescription ? a.RegionDescription.Contains(filter.Description) : true) &&
                                    (isRole ? a.Role.RoleName.Contains(filter.Role.Name) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)

                                select new RegionModel
                                {
                                    ID = a.RegionID,
                                    Name = a.RegionName,
                                    Description = a.RegionDescription,
                                    Role = new RoleModel { ID = a.Role.RoleID, Name = a.Role.RoleName },
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    regionGrid.Page = page;
                    regionGrid.Size = size;
                    regionGrid.Total = data.Count();
                    regionGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    regionGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new RegionRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Name = a.Name,
                            Description = a.Description,
                            Role = a.Role,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRegion(RegionFilter filter, ref IList<RegionModel> regions, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetRegion(string key, int limit, ref IList<RegionModel> regions, ref string message)
        {
            bool isSuccess = false;

            try
            {
                regions = (from a in context.Regions
                           join b in context.Roles
                           on a.RoleID equals b.RoleID
                                 where a.IsDeleted.Equals(false) &&
                                       b.IsDeleted.Equals(false) &&
                                     a.RegionName.Contains(key) || a.RegionDescription.Equals(key) || a.Role.RoleName.Equals(key)
                                 orderby new { a.RegionID }
                                 select new RegionModel
                                 {
                                     Name = a.RegionName,                                     
                                 }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddRegion(RegionModel regions, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.Regions.Where(a => a.RegionName.Equals(regions.Name) && a.IsDeleted.Equals(false)).Any())
                {
                    context.Regions.Add(new Entity.Region
                    {
                        RegionName = regions.Name,
                        RegionDescription =regions.Description,
                        RoleID = regions.Role.ID,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Region Parameter data for Region Parameter Name {0} is already exist.", regions.Name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateRegion(int id, RegionModel regions, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Region data = context.Regions.Where(a => a.RegionID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.Regions.Where(a => a.RegionID != regions.ID && a.RegionName.Equals(regions.Name) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Region Name {0} is exist.", regions.Name);
                    }
                    else
                    {
                        data.RegionName = regions.Name;
                        data.RegionDescription = regions.Description;
                        data.RoleID = regions.Role.ID;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Region Parameter data for Region Parameter ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteRegion(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Region data = context.Regions.Where(a => a.RegionID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Region data for Region Parameter ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
    #endregion Property
}