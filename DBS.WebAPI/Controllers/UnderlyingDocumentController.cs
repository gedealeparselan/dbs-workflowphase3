﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class UnderlyingDocController : ApiController
    {
        private string message = string.Empty;

        private readonly IUnderlyingDocRepository repository = null;

        public UnderlyingDocController()
        {
            this.repository = new UnderlyingDocRepository();
        }

        public UnderlyingDocController(IUnderlyingDocRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all Underlying Docs.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<UnderlyingDocModel>))]
        public HttpResponseMessage Get()
        {
            IList<UnderlyingDocModel> output = new List<UnderlyingDocModel>();

            if (!repository.GetUnderlyingDoc(ref output, 10, 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<UnderlyingDocModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Underlying Docs with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<UnderlyingDocModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<UnderlyingDocModel> output = new List<UnderlyingDocModel>();

            if (!repository.GetUnderlyingDoc(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<UnderlyingDocModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Underlying Docs for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(UnderlyingDocGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<UnderlyingDocFilter> filters)
        {
            UnderlyingDocGrid output = new UnderlyingDocGrid();

            if (!repository.GetUnderlyingDoc(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<UnderlyingDocGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find Underlying Docs using criterias Code or Description.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<UnderlyingDocModel>))]
        [HttpGet]
        [Route("api/UnderlyingDoc/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<UnderlyingDocModel> output = new List<UnderlyingDocModel>();

            if (!repository.GetUnderlyingDoc(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<UnderlyingDocModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Underlying Doc by ID.
        /// </summary>
        /// <param name="id">Underlying Doc ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(UnderlyingDocModel))]
        [Route("api/UnderlyingDoc/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            UnderlyingDocModel output = new UnderlyingDocModel();

            if (!repository.GetUnderlyingDocByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<UnderlyingDocModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new UnderlyingDoc.
        /// </summary>
        /// <param name="underlyingDoc">Underlying Doc data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(UnderlyingDocModel underlyingDoc)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddUnderlyingDoc(underlyingDoc, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New UnderlyingDoc data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing UnderlyingDoc.
        /// </summary>
        /// <param name="id">ID of Underlying Doc to be modify.</param>
        /// <param name="underlyingDoc">Underlying Doc data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/UnderlyingDoc/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, UnderlyingDocModel underlyingDoc)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateUnderlyingDoc(id, underlyingDoc, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Underlying Doc. data has been updated."});
                }
            }
        }

        /// <summary>
        /// Remove exisiting Underlying Doc.
        /// </summary>
        /// <param name="id">ID of Underlying Doc to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/UnderlyingDoc/{id}")]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteUnderlyingDoc(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message});
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Underlying Doc data has been deleted."});
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}