var accessToken;
var $box;
var $remove = false;

var formatNumber = function (num) {
    var n = num.toString(), p = n.indexOf('.');
    return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
        return p < 0 || i < p ? ($0 + ',') : $0;
    });
}

var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};

var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};

var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};

var BizSegmentModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var BranchModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var TypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ProductModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    WorkflowID: ko.observable(),
    Workflow: ko.observable()
};

var ChannelModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var BankModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    BranchCode: ko.observable(),
    SwiftCode: ko.observable(),
    BankAccount: ko.observable(),
    Description: ko.observable(),
    CommonName: ko.observable(),
    Currency: ko.observable(),
    PGSL: ko.observable()
};

var RMModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    RankID: ko.observable(),
    Rank: ko.observable(),
    SegmentID: ko.observable(),
    Segment: ko.observable(),
    BranchID: ko.observable(),
    Branch: ko.observable(),
    Email: ko.observable()
};

var ContactModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    PhoneNumber: ko.observable(),
    DateOfBirth: ko.observable(),
    Address: ko.observable(),
    IDNumber: ko.observable()
};

var FunctionModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var CurrencyModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var AccountModel = {
    AccountNumber: ko.observable(),
    Currency: ko.observable(CurrencyModel)
};

var UnderlyingDocumentModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var DocumentTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var FXComplianceModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ChargesModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var DocumentPurposeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var UnderlyingModel = {
    ID: ko.observable(),
    UnderlyingDocument: ko.observable(UnderlyingDocumentModel),
    DocumentType: ko.observable(DocumentTypeModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    AttachmentNo: ko.observable(),
    IsStatementLetter: ko.observable(false),
    IsDeclarationOfException: ko.observable(false),
    StartDate: ko.observable(),
    EndDate: ko.observable(),
    DateOfUnderlying: ko.observable(),
    ExpiredDate: ko.observable(),
    ReferenceNumber: ko.observable(),
    SupplierName: ko.observable(),
    InvoiceNumber: ko.observable()
};

var DocumentModel = {
    ID: ko.observable(),
    Type: ko.observable(DocumentTypeModel),
    Purpose: ko.observable(DocumentPurposeModel),
    DocumentPath: ko.observable(),
    LastModifiedDate: ko.observable(new Date),
    LastModifiedBy: ko.observable()
};

var CustomerModel = {
    CIF: ko.observable(),
    Name: ko.observable(),
    POAName: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Branch: ko.observable(BranchModel),
    Type: ko.observable(TypeModel),
    RM: ko.observable(RMModel),
    Contacts: ko.observableArray([ContactModel]),
    Functions: ko.observableArray([FunctionModel]),
    Accounts: ko.observableArray([AccountModel]),
    Underlyings: ko.observableArray([UnderlyingModel])
};

var TransactionModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    IsNewCustomer: ko.observable(false),
    Product: ko.observable(ProductModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    Channel: ko.observable(ChannelModel),
    Account: ko.observable(AccountModel),
    ApplicationDate: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Bank: ko.observable(BankModel),
    LLDCode: ko.observable(),
    LLDInfo: ko.observable(),
    IsSignatureVerified: ko.observable(false),
    IsDormantAccount: ko.observable(false),
    IsFreezeAccount: ko.observable(false),
    Others: ko.observable(),
    IsDraft: ko.observable(false),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]), //DocumentModel
    DetailCompliance: ko.observable(),
    BeneName: ko.observable(),
    TZNumber: ko.observable()
};

var PaymentModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    IsNewCustomer: ko.observable(false),
    Product: ko.observable(ProductModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    Channel: ko.observable(ChannelModel),
    Account: ko.observable(AccountModel),
    ApplicationDate: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Bank: ko.observable(BankModel),
    BankCharges: ko.observable(ChargesModel),
    AgentCharges: ko.observable(ChargesModel),
    Type: ko.observable(),
    IsCitizen: ko.observable(false),
    IsResident: ko.observable(false),
    LLDCode: ko.observable(),
    LLDInfo: ko.observable(),
    PaymentDetails: ko.observable(),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]), //DocumentModel
    DetailCompliance: ko.observable(),
    BeneName: ko.observable(),
    TZNumber: ko.observable(),
    FXCompliance: ko.observable(FXComplianceModel)
};

var CheckerModel = {
    ID: ko.observable(0),
    LLDCode: ko.observable(""),
    LLDInfo: ko.observable("")
};

var VerifyModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    IsVerified: ko.observable()
};

var TimelineModel = {
    ApproverID: ko.observable(),
    Activity: ko.observable(),
    Time: ko.observable(),
    Outcome: ko.observable(),
    UserOrGroup: ko.observable(),
    IsGroup: ko.observable(),
    DisplayName: ko.observable(),
    Comment: ko.observable()
};

var TransactionMakerModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var TransactionCheckerModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var TransactionCheckerCallbackModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    this.Calback = ko.observable();
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var TransactionCallbackModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var TransactionContactModel = function () {
    this.Contacts = ko.observableArray([ContactModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var PaymentMakerModel = function () {
    this.Payment = ko.observable(PaymentModel);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var PaymentCheckerModel = function () {
    this.Payment = ko.observable(PaymentModel);
    this.Timelines = ko.observableArray([TimelineModel]);
    this.Verify = ko.observableArray([VerifyModel]);
};

var SelectedModel = {
    Product: ko.observable(),
    Currency: ko.observable(),
    Channel: ko.observable(),
    Account: ko.observable(),
    BizSegment: ko.observable(),
    Bank: ko.observable(),
    DocumentType: ko.observable(),
    DocumentPurpose: ko.observable(),
    NewCustomer: ko.observable({
        Currency: ko.observable(),
        BizSegment: ko.observable()
    }),
    BankCharges: ko.observable(3),
    AgentCharges: ko.observable(3),
    FXCompliaance: ko.observable()
};

var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;

    // Sharepoint User
    self.SPUser = ko.observable();

    // Workflow task config
    self.WorkflowConfig = ko.observable();

    // parameters
    self.Products = ko.observableArray([]);
    self.Currencies = ko.observableArray([]);
    self.Channels = ko.observableArray([]);
    self.BizSegments = ko.observableArray([]);
    self.Banks = ko.observableArray([]);
    self.BankCharges = ko.observableArray([]);
    self.AgentCharges = ko.observableArray([]);
    self.FXCompliances = ko.observableArray([]);

    // task properties
    self.ApproverId = ko.observable();
    self.SPWebID = ko.observable();
    self.SPSiteID = ko.observable();
    self.SPListID = ko.observable();
    self.SPListItemID = ko.observable();
    self.SPTaskListID = ko.observable();
    self.SPTaskListItemID = ko.observable();
    self.Initiator = ko.observable();
    self.WorkflowInstanceID = ko.observable();
    self.WorkflowID = ko.observable();
    self.WorkflowName = ko.observable();
    self.StartTime = ko.observable();
    self.StateID = ko.observable();
    self.StateDescription = ko.observable();
    self.IsAuthorized = ko.observable();

    self.TaskTypeID = ko.observable();
    self.TaskTypeDescription = ko.observable();
    self.Title = ko.observable();
    self.ActivityTitle = ko.observable();
    self.User = ko.observable();
    self.IsSPGroup = ko.observable();
    self.EntryTime = ko.observable();
    self.ExitTime = ko.observable();
    self.OutcomeID = ko.observable();
    self.OutcomeDescription = ko.observable();
    self.CustomOutcomeID = ko.observable();
    self.CustomOutcomeDescription = ko.observable();
    self.Comments = ko.observable();

    // Transaction Details
    self.TransactionMaker = ko.observable(TransactionMakerModel);
    self.TransactionChecker = ko.observable(TransactionCheckerModel);
    self.TransactionCheckerCallback = ko.observable(TransactionCheckerCallbackModel);
    self.TransactionContact = ko.observable(TransactionContactModel);
    self.TransactionCallback = ko.observable(TransactionCallbackModel);
    self.PaymentMaker = ko.observable(PaymentMakerModel);
    self.PaymentChecker = ko.observable(PaymentCheckerModel);

    // Task status from Nintex custom REST API
    self.IsPendingNintex = ko.observable();
    self.IsAuthorizedNintex = ko.observable();
    self.MessageNintex = ko.observable();

    // Task activity properties
    self.PPUChecker = ko.observable();
    //self.PPUChecker(new PPUCheckerModel());
    //alert(self.PPUChecker().data());

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    self.GridProperties().SortColumn("ApplicationDate");
    self.GridProperties().SortOrder("DESC");

    // Options selected value
    self.Selected = ko.observable(SelectedModel);

    // filters
    self.FilterWorkflowName = ko.observable("");
    self.FilterInitiator = ko.observable("");
    self.FilterStateDescription = ko.observable("");
    self.FilterStartTime = ko.observable("");
    self.FilterTitle = ko.observable("");
    self.FilterActivityTitle = ko.observable("");
    self.FilterUser = ko.observable("");
    self.FilterEntryTime = ko.observable("");
    self.FilterExitTime = ko.observable("");
    self.FilterOutcomeDescription = ko.observable("");
    self.FilterCustomOutcomeDescription = ko.observable("");
    self.FilterComments = ko.observable("");

    // filters transaction
    self.FilterApplicationID = ko.observable("");
    self.FilterCustomer = ko.observable("");
    self.FilterProduct = ko.observable("");
    self.FilterCurrency = ko.observable("");
    self.FilterAmount = ko.observable("");
    self.FilterAmountUSD = ko.observable("");
    self.FilterFXTransaction = ko.observable("");
    self.FilterTopUrgent = ko.observable("");
    self.FilterDebitAccNumber = ko.observable("");
    self.FilterApplicationDate = ko.observable("");

    self.FilterLastModifiedBy = ko.observable("");
    self.FilterLastModifiedDate = ko.observable("");

    //Declare an ObservableArray for Storing the JSON Response
    self.Tasks = ko.observableArray([]);
    self.Outcomes = ko.observableArray([]);

    // bind get data function to view
    self.GetData = function () { GetData(); };
    self.GetParameters = function () { GetParameters(); };

    // bind clear filters
    self.ClearFilters = function () {
        self.FilterWorkflowName("");
        self.FilterInitiator("");
        self.FilterStateDescription("");
        self.FilterStartTime("");
        self.FilterTitle("");
        self.FilterActivityTitle("");
        self.FilterUser("");
        self.FilterEntryTime("");
        self.FilterExitTime("");
        self.FilterOutcomeDescription("");
        self.FilterCustomOutcomeDescription("");
        self.FilterComments("");
        self.FilterApplicationID("");
        self.FilterCustomer("");
        self.FilterProduct("");
        self.FilterCurrency("");
        self.FilterAmount("");
        self.FilterAmountUSD("");
        self.FilterFXTransaction("");
        self.FilterTopUrgent("");
        self.FilterDebitAccNumber("");

        self.FilterApplicationDate("");
        self.FilterLastModifiedBy("");
        self.FilterLastModifiedDate("");

        GetData();
    };

    // Colour Status
    self.SetColorStatus = function (id) {
        switch (id) {
            case 2: return "active";
                break;
            case 4: return "success";
                break;
            case 8: return "warning";
                break;
            case 64: return "danger";
                break;
            default: return "";
                break;

        }
    };

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (date == '1970/01/01 00:00:00' || date == '1970-01-01T00:00:00' || date == null) {
            return "";
        }
        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || date == '1970-01-01T00:00:00' || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return ""
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-18
    };


    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {

        if (data.productID == ConsProductID.FXNettingProductIDCons) {
            window.location = "/netting#" + data.ID + "#" + data.productID;
        }
        else if (data.productID == ConsProductID.DFProductIDCons) {
            window.location = "/document-flow#" + data.ID + "#" + data.productID;
        } else if (data.fxTransaction == "IPETMO") {
            window.location = "/home/new-transaction#" + data.ID + "#" + "TMOIPE"
        } else if (data.productID == ConsProductID.CIFProductIDCons && data.customerName == "DUMMYCIF") {
            window.location = "/dispatch-mode#" + data.ID + "#" + ""
        }
        else {
            window.location = "/home/new-transaction#" + data.ID + "#" + data.productID;
            $('.date-picker').datepicker({
                autoclose: true,
                onSelect: function () {
                    this.focus();
                }
            }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        }
        // self.ApproverId(data.WorkflowContext.ApproverID);
        // self.SPWebID(data.WorkflowContext.SPWebID);
        // self.SPSiteID(data.WorkflowContext.SPSiteID);
        // self.SPListID(data.WorkflowContext.SPListID);
        // self.SPListItemID(data.WorkflowContext.SPListItemID);
        // self.SPTaskListID(data.WorkflowContext.SPTaskListID);
        // self.SPTaskListItemID(data.WorkflowContext.SPTaskListItemID);
        // self.Initiator(data.WorkflowContext.Initiator);
        // self.WorkflowInstanceID(data.WorkflowContext.WorkflowInstanceID);
        // self.WorkflowID(data.WorkflowContext.WorkflowID);
        // self.WorkflowName(data.WorkflowContext.WorkflowName);
        // self.StartTime(data.WorkflowContext.StartTime);
        // self.StateID(data.WorkflowContext.StateID);
        // self.StateDescription(data.WorkflowContext.StateDescription);
        // self.IsAuthorized(data.WorkflowContext.IsAuthorized);

        // self.TaskTypeID(data.CurrentTask.TaskTypeID);
        // self.TaskTypeDescription(data.CurrentTask.TaskTypeDescription);
        // self.Title(data.CurrentTask.Title);
        // self.ActivityTitle(data.CurrentTask.ActivityTitle);
        // self.User(data.CurrentTask.User);
        // self.IsSPGroup(data.CurrentTask.IsSPGroup);
        // self.EntryTime(data.CurrentTask.EntryTime);
        // self.ExitTime(data.CurrentTask.ExitTime);
        // self.OutcomeID(data.CurrentTask.OutcomeID);
        // self.OutcomeDescription(data.CurrentTask.OutcomeDescription);
        // self.CustomOutcomeID(data.CurrentTask.CustomOutcomeID);
        // self.CustomOutcomeDescription(data.CurrentTask.CustomOutcomeDescription);
        // self.Comments(data.CurrentTask.Comments);

        // self.Outcomes.removeAll();

        // show the dialog task form
        //$("#modal-form").modal('show');

        // while is pending task, we need to get outcomes
        // if(data.CurrentTask.OutcomeID == 2)
        // {
        // lock current task for current user only
        // LockAndUnlockTaskHub("Lock", self.WorkflowInstanceID(), self.ApproverId(), self.SPTaskListItemID(), self.ActivityTitle());

        // get nintex outcomes
        // GetTaskOutcomes(data.WorkflowContext.SPTaskListID, data.WorkflowContext.SPTaskListItemID);
        // }

        // ruddy : by pass to check pending outcome
        // get nintex outcomes
        // GetTaskOutcomes(data.WorkflowContext.SPTaskListID, data.WorkflowContext.SPTaskListItemID);

        // test only
        //LockAndUnlockTaskHub("Lock", self.WorkflowInstanceID(), self.ApproverId(), self.SPTaskListItemID(), self.ActivityTitle());

        // get transaction data
        //if(self.TransactionMaker() == undefined || self.TransactionMaker().WorkflowInstanceID != self.WorkflowInstanceID()){
        /*$("#transaction-progress").show();

        setTimeout(function(){
            GetTransactionData(self.WorkflowInstanceID());
        }, 2000);*/

        //GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());
        //}
        //GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());
    };

    self.OpenTask = function (data) {

    };

    // set approval ui template
    self.ApprovalTemplate = function () {
        var output;

        switch (self.ActivityTitle()) {
            case "PPU Maker Task":
                output = "TaskPPUMaker";
                break;
            case "PPU Checker Task":
                output = "TaskPPUChecker";
                break;
            case "PPU Checker After PPU Caller Task":
                output = "TaskPPUCheckerAfterPPUCaller";
                break;
            case "PPU Caller Task":
                output = "TaskPPUCaller";
                break;
            case "CBO Maker Task":
                output = "TaskCBOMaker";
                break;
            case "CBO Checker Task":
                output = "TaskCBOChecker";
                break;
            case "Payment Maker Task":
                output = "TaskPaymentMaker";
                break;
            case "Payment Maker Revise Task":
                output = "TaskPaymentMaker";
                break;
            case "Payment Checker Task":
                output = "TaskPaymentChecker";
                break;
        }

        return output;
    };

    // set approval data template
    self.ApprovalData = function () {
        var output;

        switch (self.ActivityTitle()) {
            case "PPU Maker Task":
                output = self.TransactionMaker();
                break;
            case "PPU Checker Task":
                output = self.TransactionChecker();
                break;
            case "PPU Checker After PPU Caller Task":
                output = self.TransactionCheckerCallback();
                break;
            case "PPU Caller Task":
                output = self.TransactionCallback();
                break;
            case "CBO Maker Task":
                output = self.TransactionContact();
                break;
            case "CBO Checker Task":
                output = self.TransactionContact();
                break;
            case "Payment Maker Task":
                output = self.PaymentMaker();
                break;
            case "Payment Maker Revise Task":
                output = self.PaymentMaker();
                break;
            case "Payment Checker Task":
                output = self.PaymentChecker();
                break;
        }

        return output;
    };

    // workflow approval process & get selected outcome to send into custom Nintex REST Api
    self.ApprovalProcess = function (data) {
        var text = String.format("{0}<br/><br/>Do you want to <b>{1}</b> this task?", data.Description, data.Name);
        var outcomes = config.nintex.outcome.uncompleted;

        // while user reject, revise or cancel the task, don't validate the form and the form data will not stored on database
        if (outcomes.indexOf(data.Name) > -1) {
            if (self.Comments() == "") {
                ShowNotification("Task Validation Warning", String.format("Please type any comment before <b>{0}</b> this task.", data.Name), "gritter-warning", false);
            } else {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                    }
                });
            }
        } else {
            // validation
            var form = $("#aspnetForm");
            form.validate();

            if (form.valid()) {
                // Ruddy 29.12.2014 : While current task as PPU Checker, user must checked all checkbox to approve task.
                if (!self.IsAllChecked()) {
                    ShowNotification("Task Validation Warning", String.format("Please tick all checkbox to {0} this task.", data.Name), "gritter-warning", false);
                } else {
                    bootbox.confirm(text, function (result) {
                        if (result) {
                            //alert(JSON.stringify(ko.toJS(self.PaymentMaker().Payment.LLDCode)))
                            // store data to db
                            SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);

                            // call nintex api
                            //CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                        }
                    });
                }
            }
        }
    };
    /*self.ApprovalProcess = function(data){
        var text = String.format("{0}<br/><br/>Do you want to <b>{1}</b> this task?", data.Description, data.Name);
        var outcomes = config.nintex.outcome.uncompleted;

        // while user reject, revise or cancel the task, don't validate the form and the form data will not stored on database
        if(outcomes.indexOf(data.Name) > -1){
            if(self.Comments() == ""){
                ShowNotification("Task Validation Info", "Please type any comment before revise this task.", "gritter-warning", false);
            }else{
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                    }
                });
            }
        }else{
            // validation
            var form = $("#aspnetForm");
            form.validate();

            if(form.valid()) {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        //alert(JSON.stringify(ko.toJS(self.PaymentMaker().Payment.LLDCode)))
                        // store data to db
                        SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);

                        // call nintex api
                        //CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                    }
                });
            }
        }
	};*/

    self.VerifyColumn = function (colName) {
        //var data =  ko.utils.arrayFirst(self.TransactionChecker().Verify(), function (item){ return item.Name == colName; });
        var data = ko.utils.arrayFirst(self.ApprovalData().Verify, function (item) { return item.Name == colName; });
        return ko.observable(data);
    };

    /*self.VerifyOnChange = function(data){
        var verify =  ko.utils.arrayFirst(self.TransactionChecker().Verify(), function (item){ return item.ID == data.ID; });
        var index = self.TransactionChecker().Verify.indexOf(verify);
        //var x = self.TransactionChecker().Verify()[index];
        //alert(JSON.stringify(x))
        if(index > -1){
            self.TransactionChecker().Verify()[index].IsVerified = data.IsVerified;
        }

        alert(JSON.stringify(self.TransactionChecker().Verify()[index]))
    };*/

    self.IsAllChecked = function () {
        var isAllChecked = true;

        if (self.TransactionChecker().Verify != undefined) {
            var skipTerms = ["Document Completeness"]; // set verify name to exclude from validation checkbox

            for (var i = 0; i < self.TransactionChecker().Verify.length; i++) {
                // skip to check Document Completeness
                if (skipTerms.indexOf(self.TransactionChecker().Verify[i].Name) == -1) {
                    if (!self.TransactionChecker().Verify[i].IsVerified) {
                        //console.log(self.TransactionChecker().Verify[i].IsVerified)

                        isAllChecked = false;
                        break;
                    }
                }
            }
        }

        return isAllChecked;
    };

    // rename status fx transaction & top urgent
    self.RenameStatus = function (type, value) {
        if (type == "FX Transaction") {
            if (value) {
                return "Yes";
            } else {
                return "No";
            }
        } else if (type == "Urgency") {
            if (value == 1) {
                return "Top Urgent";
            } else if (value == 2) {
                return "Top Urgent Chain";
            } else { return "Normal" }
        } else {
            return "Unknown";
        }
    };

    // Auto populate data from select
    self.PopulateSelected = function (form, element) {
        switch (form) {
            case "PaymentMaker":
                switch (element) {
                    case "Currency":
                        var data = ko.utils.arrayFirst(self.Currencies(),
                            function (item) {
                                return item.ID == self.Selected().Currency();
                            });

                        if (data != null) {
                            //bangkit
                            GetRateAmount(data.ID);
                            self.PaymentMaker().Payment.Currency = data;
                        }
                        //alert(JSON.stringify(self.PaymentMaker().Payment.Currency))
                        break;
                    case "FXCompliance":
                        var data = ko.utils.arrayFirst(self.FXCompliances(),
                            function (item) {
                                return item.ID == self.Selected().FXCompliance;
                            });

                        if (data != null) {
                            self.PaymentMaker().Payment.FXCompliance = data;
                        }
                        break;
                    case "AccountNumber":
                        var data = ko.utils.arrayFirst(self.PaymentMaker().Payment.Customer.Accounts,
                            function (item) {
                                return item.AccountNumber == self.Selected().Account();
                            });

                        if (data != null) {
                            self.PaymentMaker().Payment.Account = data;
                        }
                        //alert(JSON.stringify(self.PaymentMaker().Payment.Account))
                        break;
                    case "Bank":
                        var data = ko.utils.arrayFirst(self.Banks(),
                            function (item) {
                                return item.ID == self.Selected().Bank();
                            });

                        if (data != null) {
                            self.PaymentMaker().Payment.Bank = data;
                        }
                        //alert(JSON.stringify(self.PaymentMaker().Payment.Account))
                        break;
                }
                break;

            case "TransactionMaker":
                switch (element) {
                    case "Currency":
                        var data = ko.utils.arrayFirst(self.Currencies(),
                            function (item) {
                                return item.ID == self.Selected().Currency();
                            });

                        if (data != null) {
                            self.TransactionMaker().Transaction.Currency = data;
                        }
                        break;
                    case "AccountNumber":
                        var data = ko.utils.arrayFirst(self.TransactionMaker().Transaction.Customer.Accounts,
                            function (item) {
                                return item.AccountNumber == self.Selected().Account();
                            });

                        if (data != null) {
                            self.TransactionMaker().Transaction.Account = data;
                        }
                        break;
                    case "Bank":
                        var data = ko.utils.arrayFirst(self.Banks(),
                            function (item) {
                                return item.ID == self.Selected().Bank();
                            });

                        if (data != null) {
                            self.TransactionMaker().Transaction.Bank = data;
                        }
                        break;
                }
                break;
        }

        // re-updating observable. This method will updating the UI (view)
        self.UpdateTemplateUI(form);
    };

    self.UpdateTemplateUI = function (form) {
        // re-updating observable. This method will updating the UI (view)
        switch (form) {
            case "PaymentMaker":
                var update = self.PaymentMaker();
                self.PaymentMaker(ko.mapping.toJS(update));
                break;
            case "TransactionMaker":
                var update = self.TransactionMaker();
                self.TransactionMaker(ko.mapping.toJS(update));
                break;
            case "TransactionContactMaker":
                var update = self.TransactionContact();
                self.TransactionContact(ko.mapping.toJS(update));
                break;
        }
    };

    // Ruddy 2014.11.09 : Generate Transaction Status
    self.FormatTransactionStatus = function (outcome, customOutcome, user) {
        console.log('start: formating transaction status');

        var output;

        if (outcome != 'Custom') {
            output = String.format("{0} by {1}", outcome, user);
        } else {
            output = String.format("{0} by {1}", customOutcome, user);
        }

        console.log('end: ' + output);
        return output;
    };

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterWorkflowName() != "") filters.push({ Field: 'WorkflowName', Value: self.FilterWorkflowName() });
        if (self.FilterInitiator() != "") filters.push({ Field: 'Initiator', Value: self.FilterInitiator() });
        if (self.FilterStateDescription() != "") filters.push({ Field: 'StateDescription', Value: self.FilterStateDescription() });
        if (self.FilterStartTime() != "") filters.push({ Field: 'StartTime', Value: self.FilterStartTime() });
        if (self.FilterTitle() != "") filters.push({ Field: 'Title', Value: self.FilterTitle() });
        if (self.FilterActivityTitle() != "") filters.push({ Field: 'ActivityTitle', Value: self.FilterActivityTitle() });
        if (self.FilterUser() != "") filters.push({ Field: 'User', Value: self.FilterUser() });
        //change by fandi for sorting datetime
        //if (self.FilterEntryTime() != "") filters.push({ Field: 'EntryTime', Value: self.FilterEntryTime() });
        if (self.FilterEntryTime() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterEntryTime() });
        //end
        if (self.FilterExitTime() != "") filters.push({ Field: 'ExitTime', Value: self.FilterExitTime() });
        if (self.FilterOutcomeDescription() != "") filters.push({ Field: 'OutcomeDescription', Value: self.FilterOutcomeDescription() });
        if (self.FilterCustomOutcomeDescription() != "") filters.push({ Field: 'CustomOutcomeDescription', Value: self.FilterCustomOutcomeDescription() });
        if (self.FilterComments() != "") filters.push({ Field: 'Comments', Value: self.FilterComments() });


        if (self.FilterApplicationID() != "") filters.push({ Field: 'applicationID', Value: self.FilterApplicationID() });
        if (self.FilterCustomer() != "") filters.push({ Field: 'customerName', Value: self.FilterCustomer() });
        if (self.FilterProduct() != "") filters.push({ Field: 'productName', Value: self.FilterProduct() });
        if (self.FilterCurrency() != "") filters.push({ Field: 'currencyCode', Value: self.FilterCurrency() });
        if (self.FilterAmount() != "") filters.push({ Field: 'transactionAmount', Value: self.FilterAmount() });
        if (self.FilterAmountUSD() != "") filters.push({ Field: 'eqvUsd', Value: self.FilterAmountUSD() });
        if (self.FilterFXTransaction() != "") filters.push({ Field: 'fxTransaction', Value: self.FilterFXTransaction() });
        if (self.FilterTopUrgent() != "") filters.push({ Field: 'topUrgentDesc', Value: self.FilterTopUrgent() });
        if (self.FilterApplicationDate() != "") filters.push({ Field: 'ApplicationDate', Value: self.FilterApplicationDate() });
        if (self.FilterLastModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterLastModifiedBy() });
        if (self.FilterLastModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterLastModifiedDate() });
        //FilterDebitAccNumber
        if (self.FilterDebitAccNumber() != "") filters.push({ Field: 'debitAccNumber', Value: self.FilterDebitAccNumber() });

        return filters;
    };

    // get all parameters need
    function GetParameters() {
        var options = {
            url: api.server + api.url.parameter,
            params: {
                select: "Product,Currency,Channel,BizSegment,Bank,BankCharge,AgentCharge,FXCompliance,ChargesType"
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
    }

    // Get data / refresh data
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') { $remove = true; $box.addClass('position-relative'); }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.transactiondraft,
            params: {
                webid: config.sharepoint.webId,
                siteid: config.sharepoint.siteId,
                workflowids: config.sharepoint.workflowId,
                state: self.WorkflowConfig().State,
                outcome: self.WorkflowConfig().Outcome,
                //customOutcome:  "1,2,3,4,5,6,7,8",
                customOutcome: self.WorkflowConfig().CustomOutcome,
                showContribute: self.WorkflowConfig().ShowContribute,
                showActiveTask: self.WorkflowConfig().ShowActiveTask,
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        /*if(self.WokflowConfig().CustomOutcome != undefined){
            options.params.customOutcome = self.WokflowConfig().CustomOutcome;
        }*/

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }

    // get Transaction data
    function GetTransactionData(instanceId, approverId) {
        var endPointURL;

        switch (self.ActivityTitle()) {
            case "PPU Maker Task":
                endPointURL = api.server + api.url.workflow.transactionMaker(instanceId, approverId);
                break;
            case "PPU Checker Task":
                endPointURL = api.server + api.url.workflow.transactionChecker(instanceId, approverId);
                break;
            case "PPU Checker After PPU Caller Task":
                endPointURL = api.server + api.url.workflow.transactionCheckerCallback(instanceId, approverId);
                break;
            case "PPU Caller Task":
                endPointURL = api.server + api.url.workflow.transactionCallback(instanceId, approverId);
                break;
            case "CBO Maker Task":
                endPointURL = api.server + api.url.workflow.transactionContact(instanceId, approverId);
                break;
            case "CBO Checker Task":
                endPointURL = api.server + api.url.workflow.transactionContact(instanceId, approverId);
                break;
            case "Payment Maker Task":
                endPointURL = api.server + api.url.workflow.paymentMaker(instanceId, approverId);
                break;
            case "Payment Maker Revise Task":
                endPointURL = api.server + api.url.workflow.paymentMaker(instanceId, approverId);
                break;
            case "Payment Checker Task":
                endPointURL = api.server + api.url.workflow.paymentChecker(instanceId, approverId);
                break;
        }

        // declare options variable for ajax get request
        var options = {
            url: endPointURL,
            token: accessToken
        };

        // show progress bar
        $("#transaction-progress").show();

        // hide transaction data
        $("#transaction-data").hide();

        // call ajax
        Helper.Ajax.Get(options, OnSuccessGetTransactionData, OnError, function () {
            // hide progress bar
            $("#transaction-progress").hide();

            // show transaction data
            $("#transaction-data").show();
        });
    }

    function SaveApprovalData(instanceId, approverId, outcomeId) {
        var endPointURL;
        var body;

        switch (self.ActivityTitle()) {
            case "PPU Maker Task":
                endPointURL = api.server + api.url.workflow.transactionMaker(instanceId, approverId);
                body = ko.toJS(self.TransactionMaker().Transaction);
                break;
            case "PPU Checker Task":
                endPointURL = api.server + api.url.workflow.transactionChecker(instanceId, approverId);
                body = ko.toJS(self.TransactionChecker());
                break;
            case "PPU Checker After PPU Caller Task":
                endPointURL = api.server + api.url.workflow.transactionChecker(instanceId, approverId);
                body = ko.toJS(self.TransactionCheckerCallback());
                break;
            case "PPU Caller Task":
                endPointURL = api.server + api.url.workflow.transactionCallback(instanceId, approverId);
                body = ko.toJS(self.TransactionCallback());
                break;
            case "CBO Maker Task":
                endPointURL = api.server + api.url.workflow.transactionContact(instanceId, approverId);
                body = ko.toJS(self.TransactionContact().Contacts);
                break;
            case "CBO Checker Task":
                endPointURL = api.server + api.url.workflow.transactionContact(instanceId, approverId);
                body = ko.toJS(self.TransactionContact().Contacts);
                break;
            case "Payment Maker Task":
                endPointURL = api.server + api.url.workflow.paymentMaker(instanceId, approverId);
                body = ko.toJS(self.PaymentMaker().Payment);
                break;
            case "Payment Maker Revise Task":
                endPointURL = api.server + api.url.workflow.paymentMaker(instanceId, approverId);
                body = ko.toJS(self.PaymentMaker().Payment);
                break;
            case "Payment Checker Task":
                endPointURL = api.server + api.url.workflow.paymentChecker(instanceId, approverId);
                body = ko.toJS({ Verify: self.PaymentChecker().Verify });
                break;
        }

        // declare options variable for ajax get request
        var options = {
            url: endPointURL,
            token: accessToken,
            data: ko.toJSON(body)
        };

        //Helper.Ajax.Post(options, OnSuccessSaveApprovalData(outcomeId), OnError, OnAlways);
        Helper.Ajax.Post(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
            }
        }, OnError, OnAlways);
    }

    // Get Nintex task outcomes
    function GetTaskOutcomes(taskListID, taskListItemID) {
        var task = {
            TaskListID: taskListID,
            TaskListItemID: taskListItemID
        };

        var options = {
            url: "/_vti_bin/DBSNintex/Services.svc/GetTaskOutcomesByListID",
            data: ko.toJSON(task)
        };

        Helper.Sharepoint.Nintex.Post(options, OnSuccessTaskOutcomes, OnError, OnAlways);
    }

    // Completing Nintex task
    function CompletingTask(taskListID, taskListItemID, taskTypeID, outcomeID, comments) {
        var task = {
            TaskListID: taskListID,
            TaskListItemID: taskListItemID,
            OutcomeID: outcomeID,
            Comment: comments
        };

        var endPointURL;
        switch (taskTypeID) {
            // Request Approval
            case 0: endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondApprovalTask";
                break;

                // Request Review
            case 1: endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondReviewTask";
                break;

                // Flexi Task
            case 4: endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondFlexiTask";
                break;
        }

        var options = {
            url: endPointURL,
            data: ko.toJSON(task)
        };

        Helper.Sharepoint.Nintex.Post(options, OnSuccessCompletingTask, OnError, OnAlways);
    }

    // Event handlers declaration start
    function OnSuccessGetParameters(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            self.Products(data.Product);
            self.Channels(data.Channel);
            self.Currencies(data.Currency);
            self.BizSegments(data.BizSegment);
            self.Banks(data.Bank);
            self.FXCompliances(data.FXCompliance);
            self.BankCharges(data.ChargesType);
            self.AgentCharges(data.ChargesType);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.Tasks(data.Rows); //Put the response in ObservableArray

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    function OnSuccessGetTransactionData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            var mapping = {
                //'ignore': ["LastModifiedDate", "LastModifiedBy"]
            };

            switch (self.ActivityTitle()) {
                case "PPU Maker Task":
                    self.TransactionMaker(ko.mapping.toJS(data, mapping));

                    // fill selected id for auto populate field
                    self.Selected().Currency(data.Transaction.Currency.ID);
                    self.Selected().Account(data.Transaction.Account.AccountNumber);
                    self.Selected().Bank(data.Transaction.Bank.ID);
                    break;
                case "PPU Checker Task":
                    self.TransactionChecker(ko.mapping.toJS(data, mapping));
                    break;
                case "PPU Checker After PPU Caller Task":
                    self.TransactionCheckerCallback(ko.mapping.toJS(data, mapping));
                    break;
                case "PPU Caller Task":
                    self.TransactionCallback(ko.mapping.toJS(data, mapping));
                    break;
                case "CBO Maker Task":
                    self.TransactionContact(ko.mapping.toJS(data, mapping));
                    break;
                case "CBO Checker Task":
                    self.TransactionContact(ko.mapping.toJS(data, mapping));
                    break;
                case "Payment Maker Task":
                    self.PaymentMaker(ko.mapping.toJS(data, mapping));

                    // fill selected id for auto populate field
                    self.Selected().Account(data.Payment.Account.AccountNumber);
                    self.Selected().Bank(data.Payment.Bank.ID);
                    break;
                case "Payment Maker Revise Task":
                    self.PaymentMaker(ko.mapping.toJS(data, mapping));

                    // fill selected id for auto populate field
                    self.Selected().Account(data.Payment.Account.AccountNumber);
                    self.Selected().Bank(data.Payment.Bank.ID);
                    break;
                case "Payment Checker Task":
                    self.PaymentChecker(ko.mapping.toJS(data, mapping));
                    break;
            }

            // disable form if this task is not authorized
            if (self.IsPendingNintex()) {
                if (!self.IsAuthorizedNintex()) {
                    DisableForm();
                }
            } else {
                DisableForm();
            }
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    function OnSuccessSaveApprovalData(outcomeId) {
        // completing task
        CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
    }

    function DisableForm() {
        $("#transaction-form").find(" input, select, textarea, button").prop("disabled", true);
    }

    function OnSuccessTaskOutcomes(data, textStatus, jqXHR) {
        if (data.IsSuccess) {
            self.Outcomes(data.Outcomes);

            self.IsPendingNintex(data.IsPending);
            self.IsAuthorizedNintex(data.IsAuthorized);
            self.MessageNintex(data.Message);

            // Get transaction Data
            GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());

            // lock current task only if user had permission
            if (self.IsPendingNintex() == true && self.IsAuthorizedNintex() == true) {
                LockAndUnlockTaskHub("Lock", self.WorkflowInstanceID(), self.ApproverId(), self.SPTaskListItemID(), self.ActivityTitle());
            }
        } else {
            ShowNotification("An error Occured", jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    function OnSuccessCompletingTask(data, textStatus, jqXHR) {
        if (data.IsSuccess) {
            // send notification
            ShowNotification('Completing Task Success', jqXHR.responseJSON.Message, 'gritter-success', false);

            // close dialog task
            $("#modal-form").modal('hide');

            // reload tasks
            GetData();
        } else {
            ShowNotification('Completing Task Failed', jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    //bangkit
    function GetRateAmount(CurrencyID) {
        var options = {
            url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetRateAmount, OnError, OnAlways);
    }

    function OnSuccessGetRateAmount(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            self.PaymentMaker().Payment.Rate = data.RupiahRate;
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    var idrrate = 0;

    //GetRateIDR();

    function GetRateIDR() {
        var options = {
            url: api.server + api.url.currency + "/CurrencyRate/" + "1",
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetRateIDR, OnError, OnAlways);
    }

    function OnSuccessGetRateIDR(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            idrrate = data.RupiahRate;
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
    // Event handlers declaration end
};

// View Model
var viewModel = new ViewModel();

// jQuery Init
$(document).ready(function () {
    /*
     Nintex 2013 DB

     WorkflowState Enumeration :
     Running: 2
     Complete: 4
     Cancelled: 8
     Error: 64

     Outcome Enumeration :
     None: -1 (Represents no outcome.)
     Approved: 0 (Represents the 'approved' response of a 'Request Approval' task.)
     Rejected: 1 (Represents the 'rejected' response of a 'Request Approval' task.)
     Pending: 2 (Represent a task that is waiting for a response.)
     Cancelled: 3 (Represents a task that was cancelled because of a cancelled workflow.)
     NotRequired: 4 (Represents a task that no longer requires a response as a result of the business rules of the task action.)
     Continue: 5 (Represents a completed 'Assign todo task' or 'Request data' task.)
     Delegated: 6 (Represents a task that was delegated to another user.)
     Custom: 7 (Represents a completed 'Assign Flexi task' action.)
     OverrideApproved: 8 (Represents a task that was force approved by a 'Complete workflow task' action.)
     OverrideRejected: 9 (Represents a task that was force rejected by a 'Complete workflow task' action.)
     OverrideContinue: 10 (Represents a task that was force completed by a 'Complete workflow task' action.)
    */
    // get table parameter
    var workflow = {
        State: $("#workflow-task-table").attr("workflow-state"),
        Outcome: $("#workflow-task-table").attr("workflow-outcome"),
        CustomOutcome: $("#workflow-task-table").attr("workflow-custom-outcome"),
        ShowContribute: $("#workflow-task-table").attr("workflow-show-contribute"),
        ShowActiveTask: $("#workflow-task-table").attr("workflow-show-active-task")
    };

    // set workflow inside vm
    viewModel.WorkflowConfig(workflow);

    // widget loader
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    // block enter key from user to prevent submitted data.
    $(this).keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

            return false;
        }
    });

    // Load all templates
    $('script[src][type="text/html"]').each(function () {
        //alert($(this).attr("src"))
        var src = $(this).attr("src");
        if (src != undefined) {
            $(this).load(src);
        }
    });

    // scrollables
    $('.slim-scroll').each(function () {
        var $this = $(this);
        $this.slimscroll({
            height: $this.data('height') || 100,
            //width: $this.data('width') || 100,
            railVisible: true,
            alwaysVisible: true,
            color: '#D15B47'
        });
    });

    // tooltip
    $('[data-rel=tooltip]').tooltip();

    // datepicker
    $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    $('.time-picker').timepicker({
        defaultTime: "",
        minuteStep: 1,
        showSeconds: false,
        showMeridian: false
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    // validation
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,

        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });

    // Knockout Datepicker custom binding handler
    ko.bindingHandlers.datepicker = {
        init: function (element) {
            $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).datepicker("refresh");
        }
    };

    // Knockout Timepicker custom binding handler
    ko.bindingHandlers.timepicker = {
        init: function (element) {
            $(element).timepicker({
                defaultTime: "",
                minuteStep: 1,
                showSeconds: false,
                showMeridian: false
            }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).timepicker('showWidget');
        }
    };


    // Knockout Bindings
    ko.applyBindings(viewModel);

    // Token Validation
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(TokenOnSuccess, TokenOnError);
    } else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // call spuser function
        GetCurrentUser(viewModel);

        // call get data inside view model
        viewModel.GetParameters();
        viewModel.GetData();

        StartTaskHub();
    }

    // Modal form on close handler
    $("#modal-form").on('hidden.bs.modal', function () {
        //alert("close")

        LockAndUnlockTaskHub("unlock", viewModel.WorkflowInstanceID(), viewModel.ApproverId(), viewModel.SPTaskListItemID(), viewModel.ActivityTitle());
    });
});

function OnKeyUp(form) {
    //================================= Eqv Calculation ====================================
    //bangkit
    var x = document.getElementById("trxn-amount");
    var y = document.getElementById("rate");
    var d = document.getElementById("eqv-usd");
    var xstored = x.getAttribute("data-in");
    var ystored = y.getAttribute("data-in");
    setInterval(function () {
        if (x == document.activeElement) {
            var temp = x.value;
            if (xstored != temp) {
                xstored = temp;
                x.setAttribute("data-in", temp);
                calculate();
            }
        }
        if (y == document.activeElement) {
            var temp = y.value;
            if (ystored != temp) {
                ystored = temp;
                y.setAttribute("data-in", temp);
                calculate();
            }
        }
    }, 50);

    function calculate() {
        var res = (x.value * y.value) / viewModel.PaymentMaker().Payment.Rate;
        res = res.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
        //$("#eqv-usd").val(res);
        viewModel.PaymentMaker().Payment.Amount = x.value;
        viewModel.PaymentMaker().Payment.AmountUSD = res;
    }

    x.onblur = calculate;
    calculate();

    // re-updating observable. This method will updating the UI (view)
    switch (form) {
        case "PaymentMaker":
            var update = viewModel.PaymentMaker();
            viewModel.PaymentMaker(ko.mapping.toJS(update));
            document.getElementById("trxn-amount").focus();
            break;
        case "TransactionMaker":
            var update = viewModel.TransactionMaker();
            viewModel.TransactionMaker(ko.mapping.toJS(update));
            document.getElementById("trxn-amount").focus();
            break;
    }
    //================================= End Eqv Calculation ====================================
};

// Function to display notification.
// class name : gritter-success, gritter-warning, gritter-error, gritter-info
function ShowNotification(title, text, className, isSticky) {
    $.gritter.add({
        title: title,
        text: text,
        class_name: className,
        sticky: isSticky
    });
}

// Get SPUser from cookies
function GetCurrentUser() {
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);

        viewModel.SPUser = spUser;

        //alert(JSON.stringify(vm.SPUser));

        //Helper.Signal.Connect(OnSuccessSignal, OnErrorSignal, OnReceivedSignal);

        // load hub script
        //$.getScript(config.signal.server +"Scripts/hubs.js");

    }
}

// Token validation On Success function
function TokenOnSuccess(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // call spuser function
    GetCurrentUser();

    // call get data inside view model
    viewModel.GetParameters();
    viewModel.GetData();
}

// Token validation On Error function
function TokenOnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

//
function OnSuccessSignal() {
    //ShowNotification("Connection Success", "Connected to signal server", "gritter-success", false);
    /*    var hub = $.connection.task;
        $.connection.hub.url = config.signal.server+ "task";
    
        $.connection.hub.start()
            .done(function () {
                alert("Connected");
            })
            .fail(function() {
                alert("Connection failed!");
            });*/
}

function OnErrorSignal() {
    ShowNotification("Connection Failed", "Failed connected to signal server", "gritter-error", true);
}

function OnReceivedSignal(data) {
    ShowNotification(data.Sender, data.Message, "gritter-info", false);
}

// SignalR ----------------------
var taskHub = $.hubConnection(config.signal.server + "hub");
var taskProxy = taskHub.createHubProxy("TaskService");
var IsTaskHubConnected = false;

function StartTaskHub() {
    taskHub.start()
        .done(function () {
            //alert("Connected");
            /*var msg = { From: spUser.DisplayName, Message: "Hahaha"};
            taskProxy.invoke("Send", msg)
            .done(function(result) {
                //alert("ok: "+ result);
            })
            .fail(function(err) {
                alert("error: "+err);
            });*/

            /*var checkData = {
                WorkflowInstanceID: "dsadas sadsad",
                TaskID: 99
            };
            // check current task status
            taskProxy.invoke("CheckTask", checkData)
                .fail(function(err) {
                    ShowNotification("Checking Task Failed", err, "gritter-error", true);
                }
            );*/

            /*var data = {
                WorkflowInstanceID: "dsadas sadsad",
                TaskID: 99,
                LoginName: spUser.LoginName,
                DisplayName: spUser.DisplayName
            };

            // try lock current task
            taskProxy.invoke("LockTask", data)
                .fail(function(err) {
                    ShowNotification("Lock Task Failed", err, "gritter-error", true);
                }
            );*/

            IsTaskHubConnected = true;
        })
        .fail(function () {
            ShowNotification("Hub Connection Failed", "Fail while try connecting to hub server", "gritter-error", true);
        }
    );
}

function LockAndUnlockTaskHub(lockType, instanceId, approverID, taskId, activityTitle) {
    if (IsTaskHubConnected) {
        var data = {
            WorkflowInstanceID: instanceId,
            ApproverID: approverID,
            TaskID: taskId,
            ActivityTitle: activityTitle,
            LoginName: spUser.LoginName,
            DisplayName: spUser.DisplayName
        };

        var LockType = lockType.toLowerCase() == "lock" ? "LockTask" : "UnlockTask";

        // try lock current task
        taskProxy.invoke(LockType, data)
            .fail(function (err) {
                ShowNotification("LockType Task Failed", err, "gritter-error", true);
            }
        );
    } else {
        ShowNotification("Task Manager", "There is no active connection to task server manager.", "gritter-error", true);
    }
}

taskProxy.on("Message", function (data) {
    ShowNotification(data.From, data.Message, "gritter-info", false);
});

taskProxy.on("NotifyInfo", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-info", false);
});

taskProxy.on("NotifyWarning", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-warning", false);
});

taskProxy.on("NotifyError", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-error", true);
});