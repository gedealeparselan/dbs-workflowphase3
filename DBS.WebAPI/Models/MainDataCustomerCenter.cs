﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Net.Http;
using System.Web.Script.Serialization;
using System.Transactions;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("MainDataCustomerCenter")]
    public class MainDataCustomerCenterModel
    {
        /// <summary>
        /// EcalationCustomer ID.
        /// </summary>
        /// 
        public int? CBOMaintainSubID { get; set; } //informationType
        public int CBOMaintainCCentreID { get; set; }
        public Nullable<int> CBOMaintainID { get; set; }
        public string SubTypeName { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }
    public class MainDataCustomerCenterRow : MainDataCustomerCenterModel
    {
        public int RowID { get; set; }

    }

    public class MainDataCustomerCenterGrid : Grid
    {
        public IList<MainDataCustomerCenterRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class MainDataCustomerCenterRowFilter : Filter { }
    #endregion

    #region Interface
    public interface IMainDataCustomerCenterModelRepository : IDisposable
    {
        //bool GetEscalationCustomerID(int id, ref EscalationCustomerModel escalationCustomers, ref string message);
        //bool GetEscalationCustomer(ref IList<EscalationCustomerModel> escalationCustomers, int limit, int index, ref string message);
        bool GetMainDataCustomerCenter(ref IList<MainDataCustomerCenterModel> MainDataCustomerCenters, ref string message);
        bool GetMainDataCustomerCenterMaintenanceCC(ref IList<MainDataCustomerCenterModel> MainDataCustomerCenters, ref string message);
        bool AddMainDataCustomerCenter(IList<MainDataCustomerCenterModel> MainDataCustomerCenters, ref string message);
        //bool GetEscalationCustomer(int page, int size, IList<EscalationCustomerFilter> filters, string sortColumn, string sortOrder, ref EscalationCustomerGrid ageingReportGrid, ref string message);
        //bool GetEscalationCustomer(EscalationCustomerFilter filter, ref IList<EscalationCustomerModel> escalationCustomers, ref string message);
        //bool GetEscalationCustomer(string key, int limit, ref IList<EscalationCustomerModel> escalationCustomers, ref string message);
        //bool AddEscalationCustomer(EscalationCustomerModel escalationCustomer, ref string message);
        //bool UpdateEscalationCustomer(int id, EscalationCustomerModel escalationCustomer, ref string message);
        //bool DeleteEscalationCustomer(int id, ref string message);
    }
    #endregion

    #region Repository
    public class MainDataCustomerCenterRepository : IMainDataCustomerCenterModelRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetMainDataCustomerCenter(ref IList<MainDataCustomerCenterModel> MainDataCustomerCenters, ref string message)
        {
            bool isSuccess = false;

            try
            {

                MainDataCustomerCenters = (from a in context.InformationTypes
                                      //join b in context.MaintenanceTypePriorities on a.CBOMaintainID equals b.CBOMaintainID 
                                           where !(from b in context.MaintenanceCCs where b.IsDeleted == false select b.CBOMaintainSubID).Contains(a.CBOMaintainSubID)
                                      // from abc in ab.Where(b=>b.IsDeleted==false && b.CBOMaintainID != a.CBOMaintainID)
                                      select new MainDataCustomerCenterModel
                                      {

                                         CBOMaintainSubID = a.CBOMaintainSubID,
                                         SubTypeName = a.SubTypeName




                                      }).ToList();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetMainDataCustomerCenterMaintenanceCC(ref IList<MainDataCustomerCenterModel> MainDataCustomerCenters, ref string message)
        {
            bool isSuccess = false;

            try
            {
                MainDataCustomerCenters = (from a in context.MaintenanceCCs
                                      join b in context.InformationTypes on a.CBOMaintainSubID equals b.CBOMaintainSubID
                                      where a.IsDeleted.Equals(false)// && a.CBOMaintainID!=b.CBOMaintainID
                                      select new MainDataCustomerCenterModel
                                      {
                                          CBOMaintainSubID = b.CBOMaintainSubID,
                                          SubTypeName = b.SubTypeName
                                          //CreateBy = a.CreateBy,
                                          //CreateDate = a.CreateDate,
                                          //UpdateBy =a.UpdateBy,
                                          //UpdateDate =a.UpdateDate

                                      }).ToList();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool AddMainDataCustomerCenter(IList<MainDataCustomerCenterModel> MainDataCustomerCenters, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                if (MainDataCustomerCenters != null)
                {
                    try
                    {
                        var MaindataCustomercenters = context.MaintenanceCCs.ToList();
                        
                        if (MaindataCustomercenters != null && MaindataCustomercenters.Count > 0)
                        {
                            MaindataCustomercenters.ForEach(a =>
                            {
                                a.IsDeleted = true;
                            });
                            context.SaveChanges();

                        }
                        //add by fandi jika ditabel grid MaintenanceCC sudah kosong jagn di input data MaintenanceCC yang baru  
                        MainDataCustomerCenters = (from CC in context.MaintenanceCCs
                                                   where CC.IsDeleted.Equals(false)
                                                   select new MainDataCustomerCenterModel
                                                   {
                                                       CBOMaintainCCentreID = CC.CBOMaintainCCentreID,
                                                       CBOMaintainSubID = CC.CBOMaintainSubID,
                                                       IsDeleted = false

                                                   }).ToList();

                        if (MainDataCustomerCenters.Count >= 1)
                        {
                            foreach (var data in MainDataCustomerCenters)
                            {

                                context.MaintenanceCCs.Add(new Entity.MaintenanceCC()
                                {
                                    CBOMaintainCCentreID = data.CBOMaintainCCentreID,
                                    CBOMaintainSubID = data.CBOMaintainSubID,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName,

                                    //UpdateDate = data.UpdateDate,
                                    //UpdateBy = data.UpdateBy

                                });

                                context.SaveChanges();

                            }
                        }
                        //end by fandi
                        ts.Complete();
                        isSuccess = true;


                    }

                    catch (Exception ex)
                    {
                        ts.Dispose();
                        message = ex.Message;
                    }
                }
            }

            return isSuccess;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                        context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

    #endregion
}