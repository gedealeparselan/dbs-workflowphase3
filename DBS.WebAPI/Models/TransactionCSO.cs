﻿using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DBS.WebAPI.Models
{
    public class TransactionCSO
    {
        #region property
        public class TransactionCSOModel
        {

            public long TrasnsactionID { get; set; }

            /// <summary>
            /// Workflow Instance ID
            /// </summary>
            public Guid? WorkflowInstanceID { get; set; }

            /// <summary>
            /// Transaction ID.
            /// </summary>
            public long ID { get; set; }

            /// <summary>
            /// Application ID
            /// </summary>
            public string ApplicationID { get; set; }

            /// <summary>
            /// Top Urgent
            /// </summary>
            public int? IsTopUrgent { get; set; }
            /// <summary>
            /// Top Urgent
            /// </summary>
            public int? IsTopUrgentChain { get; set; }
            /// <summary>
            /// Customer Data Details
            /// </summary>
            public CustomerModel Customer { get; set; }
            /// <summary>
            /// Others
            /// </summary>
            public string Others { get; set; }

            /// <summary>
            /// Product Details
            /// </summary>
            public ProductModel Product { get; set; }

            /// <summary>
            /// Currency Details
            /// </summary>
            public CurrencyModel Currency { get; set; }

            /// <summary>
            /// Transaction Amount
            /// </summary>
            public decimal Amount { get; set; }

            /// <summary>
            /// Rate
            /// </summary>
            public decimal Rate { get; set; }

            /// <summary>
            /// Eqv.USD
            /// </summary>
            public decimal AmountUSD { get; set; }

            /// <summary>
            /// Channel Details
            /// </summary>
            public ChannelModel Channel { get; set; }


            /// <summary>
            /// Application Date
            /// </summary>
            public DateTime ValueDate { get; set; }

            /// <summary>
            /// Bene Segment Details
            /// </summary>
            public BizSegmentModel BizSegment { get; set; }
            public bool? IsSignatureVerified { get; set; }

            /// <summary>
            /// Dormant Account
            /// </summary>
            public bool? IsDormantAccount { get; set; }

            /// <summary>
            /// Freeze Account
            /// </summary>
            public bool? IsFreezeAccount { get; set; }
            /// <summary>
            /// IsDocumentComplete
            /// </summary>
            public bool IsDocumentComplete { get; set; }
            /// <summary>
            /// IsCallbackRequired
            /// </summary>
            public bool IsCallbackRequired { get; set; }
            /// <summary>
            /// IsLOIAvailable
            /// </summary>
            public bool IsLOIAvailable { get; set; }
            /// <summary>
            /// ApproverID
            /// </summary>
            public long? ApproverID { get; set; }
            /// <summary>
            /// CreateDate
            /// </summary>
            public DateTime CreateDate { get; set; }

            /// <summary>
            /// Last modified date
            /// </summary>
            public DateTime LastModifiedDate { get; set; }

            /// <summary>
            /// Last modified By
            /// </summary>
            public string LastModifiedBy { get; set; }

            /// <summary>
            /// List Transaction Document Details
            /// </summary>
            public IList<TransactionDocumentModel> Documents { get; set; }
            /// <summary>
            /// FundingMemo
            /// </summary>
            public FundingMemoLTModel FundingMemo { get; set; }

        }

        public class TransactionCSORow : FundingMemoLTModel//TransactionCSOModel
        {
            public int RowID { get; set; }
        }

        public class TransactionCSOGrid : Grid
        {
            public IList<TransactionCSORow> Rows { get; set; }
        }
        #endregion

        #region filter
        public class TransactionCSOFilter : Filter
        {

        }
        #endregion

        #region Interface
        public interface ITransactionCSORepository:IDisposable
        {
            bool GetTransactionCSOFunding(int page, int size, IList<TransactionCSOFilter> filters, string sortColumn, string sortOrder, ref TransactionCSOGrid Funding, ref string message);
        }
        #endregion

        #region repository
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetTransactionDetails(Guid instanceID, ref TransactionCSOModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                TransactionCSOModel datatransaction = (from a in context.Transactions
                                                          where a.WorkflowInstanceID.Value.Equals(instanceID)
                                                           select new TransactionCSOModel
                                                          {
                                                              ID = a.TransactionID,
                                                              WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                                              ApplicationID = a.ApplicationID,
                                                              //ApplicationDate = a.ApplicationDate,
                                                              ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                                                              //BankCharges = new cha

                                                              Product = new ProductModel()
                                                              {
                                                                  ID = a.Product.ProductID,
                                                                  Code = a.Product.ProductCode,
                                                                  Name = a.Product.ProductName,
                                                                  WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                                                  Workflow = a.Product.ProductWorkflow.WorkflowName,
                                                                  LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                                                  LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                                              },
                                                              Currency = new CurrencyModel()
                                                              {
                                                                  ID = a.Currency.CurrencyID,
                                                                  Code = a.Currency.CurrencyCode,
                                                                  Description = a.Currency.CurrencyDescription,
                                                                  LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                                  LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                              },
                                                              Amount = a.Amount,
                                                              AmountUSD = a.AmountUSD,
                                                              Rate = a.Rate,
                                                              Channel = new ChannelModel()
                                                              {
                                                                  ID = a.Channel.ChannelID,
                                                                  Name = a.Channel.ChannelName,
                                                                  LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                                                  LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                                              },

                                                              BizSegment = new BizSegmentModel()
                                                              {
                                                                  ID = a.BizSegment.BizSegmentID,
                                                                  Name = a.BizSegment.BizSegmentName,
                                                                  Description = a.BizSegment.BizSegmentDescription,
                                                                  LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                                  LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                              },

                                                              IsTopUrgent = a.IsTopUrgent,
                                                     
                                                              Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                                                              {
                                                                  ID = x.TransactionDocumentID,
                                                                  Type = new DocumentTypeModel()
                                                                  {
                                                                      ID = x.DocumentType.DocTypeID,
                                                                      Name = x.DocumentType.DocTypeName,
                                                                      Description = x.DocumentType.DocTypeDescription,
                                                                      LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                      LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                  },
                                                                  Purpose = new DocumentPurposeModel()
                                                                  {
                                                                      ID = x.DocumentPurpose.PurposeID,
                                                                      Name = x.DocumentPurpose.PurposeName,
                                                                      Description = x.DocumentPurpose.PurposeDescription,
                                                                      LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                      LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                                  },
                                                                  FileName = x.Filename,
                                                                  DocumentPath = x.DocumentPath,
                                                                  LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                  LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value,
                                                                  IsDormant = x.IsDormant == null ? false : x.IsDormant
                                                              }).ToList(),
                                                              CreateDate = a.CreateDate,
                                                              LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                              LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                          }).SingleOrDefault();

                // fill payment data
                if (datatransaction != null)
                {
                    //datatransaction.IsOtherBeneBank = datatransaction.Bank.Code != "999" ? false : true;
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(instanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    // changed, chandra : 2015.03.23
                    if (customerRepo.GetCustomerByTransaction(datatransaction.ID, ref customer, ref message))
                    {
                        datatransaction.Customer = customer;
                    }

                    var data = context.Transactions.Where(x => x.TransactionID.Equals(datatransaction.ID)).Select(x => x).FirstOrDefault();

                    


                    customerRepo.Dispose();
                    customer = null;
                    long IDTrns = datatransaction.ID;
                    var checker = context.TransactionCheckerDatas.Where(x => x.TransactionID.Equals(IDTrns)).Select(x => x).OrderByDescending(b => b.ApproverID).FirstOrDefault();
                    if (checker != null)
                    {
                        //datatransaction.LLDCode = checker.LLDCode;
                        //datatransaction.LLDInfo = checker.LLDInfo;   
                        // newlld
                        datatransaction.Others = checker.Others;

                        if (checker.LLD != null)
                        {
                            LLDModel lm = new LLDModel();

                            lm.ID = checker.LLD.LLDID;
                            lm.Code = checker.LLD.LLDCode;
                            lm.Description = checker.LLD.LLDDescription;

                            //datatransaction.LLD = lm;
                        }

                        datatransaction.IsSignatureVerified = checker.IsSignatureVerified;
                        //basri 30-09-2015
                        datatransaction.IsDocumentComplete = datatransaction.IsDocumentComplete;
                        datatransaction.IsCallbackRequired = checker.IsCallbackRequired;
                        datatransaction.IsLOIAvailable = checker.IsLOIAvailable;
                        //end basri
                        datatransaction.IsDormantAccount = checker.IsDormantAccount;
                        datatransaction.IsFreezeAccount = checker.IsFreezeAccount;
                    }

                }

                // check charges if not null
                //var charges = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(instanceID)).SingleOrDefault();

                //charges = null;


                //======================================
                //star get fundingMemo
                    FundingMemoLTModel FundingMemo = (from funding in context.CSOFundingMemoes
                                                      join location in context.Locations on funding.SolID equals location.SolID
                                                      join AmountDisburseID in context.Currencies on funding.AmountDisburseID equals AmountDisburseID.CurrencyID
                                                      join SanctionLimitCurrID in context.Currencies on funding.SanctionLimitCurrID equals SanctionLimitCurrID.CurrencyID
                                                      join utilization in context.Currencies on funding.UtilizationCurrID equals utilization.CurrencyID
                                                      join OutstandingID in context.Currencies on funding.OutstandingCurrID equals OutstandingID.CurrencyID
                                                      join parametersysmodel in context.ParameterSystems on funding.LoanTypeID equals parametersysmodel.ParsysID
                                                      join parametersysmodel1 in context.ParameterSystems on funding.ProgramTypeID equals parametersysmodel1.ParsysID
                                                      join parametersysmodel2 in context.ParameterSystems on funding.FinacleSchemeCodeID equals parametersysmodel2.ParsysID
                                                      join parametersysmodel3 in context.ParameterSystems on funding.InterestFrequencyID equals parametersysmodel3.ParsysID
                                                      join parametersysmodel4 in context.ParameterSystems on funding.RepricingPlanID equals parametersysmodel4.ParsysID
                                                      join parametersysmodel5 in context.ParameterSystems on funding.PeggingFrequencyID equals parametersysmodel5.ParsysID
                                                      join parametersysmodel6 in context.ParameterSystems on funding.InterestFrequencyID equals parametersysmodel6.ParsysID
                                                      where datatransaction.Customer.CIF.Equals(funding.CIF)
                                                      select new FundingMemoLTModel
                                                      {
                                                          CIF=funding.CIF,
                                                          Customer=funding.Customer != null ? new CustomerModel { CIF = funding.Customer.CIF, Name = funding.Customer.CustomerName } : new CustomerModel { CIF = "", Name = "" },
                                                          Location=location!=null ?new LocationModel{ID=location.LocationID,SolId=location.SolID} : new LocationModel{ID=0,SolId=""},    
                                                          CSOName = funding.CSOName,
                                                            SolID = funding.SolID,
                                                            CustomerCategoryID = funding.CustomerCategoryID,
                                                            CSOFundingMemoID = funding.CSOFundingMemoID,//Convert.ToInt32(funding.CSOFundingMemoID),
                                                            TransactionFundingMemoID =funding.TransactionFundingMemoID,//Convert.ToInt32(funding.TransactionFundingMemoID),
                                                            ValueDate = funding.ValueDate,
                                                            IsAdhoc = funding.IsAdhoc,
                                                            CreditingOperative = funding.CreditingOperative,
                                                            DebitingOperative = funding.DebitingOperative,
                                                            MaturityDate = funding.MaturityDate,
                                                            BaseRate = funding.BaseRate,
                                                            AccountPreferencial = funding.AccountPreferencial,
                                                            AllInRate = funding.AllInRate,
                                                            ApprovedMarginAsPerCM = funding.ApprovedMarginAsPerCM,
                                                            SpecialFTP = funding.SpecialFTP,
                                                            Margin = funding.Margin,
                                                            AllInSpecialRate = funding.AllInSpecialRate,
                                                            PeggingDate = funding.PeggingDate,
                                                            NoOfInstallment = funding.NoOfInstallment,
                                                            LimitIDinFin10 = funding.LimitIDinFin10,
                                                            LimitExpiryDate = funding.LimitExpiryDate,
                                                            SanctionLimitAmount = funding.SanctionLimit,
                                                            utilizationAmount = funding.utilization,
                                                            OutstandingAmount = funding.Outstanding,
                                                            OutstandingCurrID = funding.OutstandingCurrID,
                                                            Remarks = funding.Remarks,
                                                            OtherRemarks = funding.OtherRemarks,
                                                            AmountDisburse=funding.AmountDisburse,
                                                            PrincipalStartDate=funding.PrincipalStartDate,
                                                            InterestStartDate=funding.InterestStartDate,
                                                            LastModifiedDate = funding.UpdateDate == null ? funding.CreateDate : funding.UpdateDate.Value,
                                                            LastModifiedBy = funding.UpdateDate == null ? funding.CreateBy : funding.UpdateBy,
                                                            ParameterSystemModel = funding.Customer != null ? new ParameterSystemModelFunding { LoanTypeID = funding.LoanTypeID, LoanTypeName = parametersysmodel.ParameterValue } : new ParameterSystemModelFunding { LoanTypeID = 0, LoanTypeName = "" },
                                                            AmountDisburseID = funding.AmountDisburseID != null ? new AmountDisburseIDModel { ID = funding.AmountDisburseID, CodeDescription = AmountDisburseID.CurrencyCode + "(" + AmountDisburseID.CurrencyDescription + ")" } : new AmountDisburseIDModel { ID = 0, CodeDescription = "" },
                                                            SanctionLimitCurr = funding.SanctionLimitCurrID != null ? new SanctionLimitCurrModel { ID = funding.SanctionLimitCurrID, CodeDescription = SanctionLimitCurrID.CurrencyCode + "(" + SanctionLimitCurrID.CurrencyDescription + ")" } : new SanctionLimitCurrModel { ID = 0, CodeDescription = "" },
                                                            utilizationID = funding.UtilizationCurrID != null ? new utilizationIDModel { ID = funding.UtilizationCurrID, CodeDescription = utilization.CurrencyCode + "(" + utilization.CurrencyDescription + ")" } : new utilizationIDModel { ID = 0, CodeDescription = "" },
                                                            OutstandingID = funding.OutstandingCurrID != null ? new OutstandingIDModel { ID = funding.OutstandingCurrID, CodeDescription = OutstandingID.CurrencyCode + "(" + OutstandingID.CurrencyDescription + ")" } : new OutstandingIDModel { ID = 0, CodeDescription = "" },
                                                            ProgramType = funding.Customer != null ? new ProgramType { ProgramTypeID = funding.ProgramTypeID, ProgramTypeName = parametersysmodel1.ParameterValue } : new ProgramType { ProgramTypeID = 0, ProgramTypeName = "" },
                                                            FinacleScheme = funding.Customer != null ? new FinacleScheme { FinacleSchemeCodeID = funding.FinacleSchemeCodeID, FinacleSchemeCodeName = parametersysmodel2.ParameterValue } : new FinacleScheme { FinacleSchemeCodeID = 0, FinacleSchemeCodeName = "" },
                                                            Interest = funding.Customer != null ? new Interest { InterestCodeID = funding.InterestCodeID, InterestCodeName = parametersysmodel3.ParameterValue } : new Interest { InterestCodeID = "", InterestCodeName = "" },
                                                            //Interest = funding.Customer != null ? new Interest { InterestCodeID = int.Parse((string)funding.InterestCodeID), InterestCodeName = parametersysmodel3.ParameterValue } : new Interest { InterestCodeID = 0, InterestCodeName = "" },
                                                            RepricingPlan = funding.Customer != null ? new RepricingPlan { RepricingPlanID = funding.RepricingPlanID, RepricingPlanName = parametersysmodel4.ParameterValue } : new RepricingPlan { RepricingPlanID = 0, RepricingPlanName = "" },
                                                            PeggingFrequency = funding.Customer != null ? new PeggingFrequency { PeggingFrequencyID = funding.PeggingFrequencyID, PeggingFrequencyName = parametersysmodel5.ParameterValue } : new PeggingFrequency { PeggingFrequencyID = 0, PeggingFrequencyName = "" },
                                                            InterestFrequency = funding.Customer != null ? new InterestFrequency { InterestFrequencyID = funding.InterestFrequencyID, InterestFrequencyName = parametersysmodel6.ParameterValue } : new InterestFrequency { InterestFrequencyID = 0, InterestFrequencyName = "" }
                                                      }).SingleOrDefault();
                    if (FundingMemo != null)
                    {
                        datatransaction.FundingMemo = FundingMemo;
                        transaction = datatransaction;
                    }
                    //end fundingmemo
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionCSOFunding(int page, int size, IList<TransactionCSOFilter> filters, string sortColumn, string sortOrder, ref TransactionCSOGrid Funding, ref string message)
        {
            bool isSuccess = false;
            try
            {
                FundingMemoLTModel filter = new FundingMemoLTModel();
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();


                if (filters != null)
                {
                    filter.CIF = filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.Customer.Name = filters.Where(a => a.Field.Equals("Customer")).Select(a => a.Value).SingleOrDefault();
                    filter.BizSegment.Name = filters.Where(a => a.Field.Equals("BizSegment")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from funding in context.CSOFundingMemoes
                                join customer in context.Customers on funding.CIF equals customer.CIF
                                //join bizsegment in context.BizSegments on funding.BizSegmentID equals bizsegment.BizSegmentID
                                //join currency in context.Currencies on funding.Currency.CurrencyID equals currency.CurrencyID
                                //join cso in context.CSOes on funding.CIF equals cso.CIF
                                join location in context.Locations on funding.SolID equals location.SolID //on customer.LocationID equals location.LocationID
                                join AmountDisburseID in context.Currencies on funding.AmountDisburseID equals AmountDisburseID.CurrencyID
                                join SanctionLimitCurrID in context.Currencies on funding.SanctionLimitCurrID equals SanctionLimitCurrID.CurrencyID
                                join utilization in context.Currencies on funding.UtilizationCurrID equals utilization.CurrencyID
                                join OutstandingID in context.Currencies on funding.OutstandingCurrID equals OutstandingID.CurrencyID
                                join parametersysmodel in context.ParameterSystems on funding.LoanTypeID equals parametersysmodel.ParsysID
                                join parametersysmodel1 in context.ParameterSystems on funding.ProgramTypeID equals parametersysmodel1.ParsysID
                                join parametersysmodel2 in context.ParameterSystems on funding.FinacleSchemeCodeID equals parametersysmodel2.ParsysID
                                join parametersysmodel3 in context.ParameterSystems on funding.InterestFrequencyID equals parametersysmodel3.ParsysID
                                join parametersysmodel4 in context.ParameterSystems on funding.RepricingPlanID equals parametersysmodel4.ParsysID
                                join parametersysmodel5 in context.ParameterSystems on funding.PeggingFrequencyID equals parametersysmodel5.ParsysID
                                join parametersysmodel6 in context.ParameterSystems on funding.InterestFrequencyID equals parametersysmodel6.ParsysID
                                //let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                //let isCustomer = !string.IsNullOrEmpty(filter.Customer.Name)
                                //let isBizSegment = !string.IsNullOrEmpty(filter.BizSegment.Name)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where //(isCIF ? funding.CIF.Contains(filter.CIF) : true) &&
                                    //(isCustomer ? funding.Customer.CustomerName.Contains(filter.Customer.Name) : true) &&
                                    //(isBizSegment ? funding.BizSegment.BizSegmentName.Contains(filter.BizSegment.Name) : true)
                                 (isCreateBy ? (funding.UpdateDate == null ? funding.CreateBy.Contains(filter.LastModifiedBy) : funding.UpdateBy.Contains(filter.LastModifiedBy)) : true)
                                 && (isCreateDate ? ((funding.UpdateDate == null ? funding.CreateDate : funding.UpdateDate) > filter.LastModifiedDate.Value && ((funding.UpdateDate == null ? funding.CreateDate : funding.UpdateDate.Value) < maxDate)) : true)
                                select new FundingMemoLTModel
                                {
                                    CIF = funding.CIF,
                                    Customer = funding.Customer != null ? new CustomerModel { CIF = funding.Customer.CIF, Name = funding.Customer.CustomerName } : new CustomerModel { CIF = "", Name = "" },
                                    //BizSegment = funding.BizSegment != null ? new BizSegmentModel { ID = funding.BizSegment.BizSegmentID, Name = funding.BizSegment.BizSegmentName, Description = funding.BizSegment.BizSegmentDescription } : new BizSegmentModel { ID = 0, Name = "", Description = "" },
                                    //BizSegmentName = funding.BizSegmentName,//bizsegment.BizSegmentDescription,
                                    //Currency = funding.Currency != null ? new CurrencyModel { ID = funding.Currency.CurrencyID, CodeDescription = funding.Currency.CurrencyCode + "(" + funding.Currency.CurrencyDescription + ")" } : new CurrencyModel { ID = 0, CodeDescription = "" },
                                    Location = location != null ? new LocationModel { ID = location.LocationID, SolId = location.SolID } : new LocationModel { ID = 0, SolId = "" },    //customer != null ? new LocationModel { ID = customer.LocationID, SolId = location.SolID } : new LocationModel { ID = 0, SolId = "" },
                                    CSOName = funding.CSOName,
                                    SolID = funding.SolID,
                                    CustomerCategoryID = funding.CustomerCategoryID,
                                    CSOFundingMemoID = funding.CSOFundingMemoID,//Convert.ToInt32(funding.CSOFundingMemoID),
                                    TransactionFundingMemoID = funding.TransactionFundingMemoID,//Convert.ToInt32(funding.TransactionFundingMemoID),
                                    ValueDate = funding.ValueDate,
                                    IsAdhoc = funding.IsAdhoc,
                                    CreditingOperative = funding.CreditingOperative,
                                    DebitingOperative = funding.DebitingOperative,
                                    MaturityDate = funding.MaturityDate,
                                    BaseRate = funding.BaseRate,
                                    AccountPreferencial = funding.AccountPreferencial,
                                    AllInRate = funding.AllInRate,
                                    ApprovedMarginAsPerCM = funding.ApprovedMarginAsPerCM,
                                    SpecialFTP = funding.SpecialFTP,
                                    Margin = funding.Margin,
                                    AllInSpecialRate = funding.AllInSpecialRate,
                                    PeggingDate = funding.PeggingDate,
                                    NoOfInstallment = funding.NoOfInstallment,
                                    LimitIDinFin10 = funding.LimitIDinFin10,
                                    LimitExpiryDate = funding.LimitExpiryDate,
                                    SanctionLimitAmount = funding.SanctionLimit,
                                    utilizationAmount = funding.utilization,
                                    OutstandingAmount = funding.Outstanding,
                                    OutstandingCurrID = funding.OutstandingCurrID,
                                    Remarks = funding.Remarks,
                                    OtherRemarks = funding.OtherRemarks,
                                    AmountDisburse = funding.AmountDisburse,
                                    PrincipalStartDate = funding.PrincipalStartDate,
                                    InterestStartDate = funding.InterestStartDate,
                                    LastModifiedDate = funding.UpdateDate == null ? funding.CreateDate : funding.UpdateDate.Value,
                                    LastModifiedBy = funding.UpdateDate == null ? funding.CreateBy : funding.UpdateBy,
                                    ParameterSystemModel = funding.Customer != null ? new ParameterSystemModelFunding { LoanTypeID = funding.LoanTypeID, LoanTypeName = parametersysmodel.ParameterValue } : new ParameterSystemModelFunding { LoanTypeID = 0, LoanTypeName = "" },
                                    AmountDisburseID = funding.AmountDisburseID != null ? new AmountDisburseIDModel { ID = funding.AmountDisburseID, CodeDescription = AmountDisburseID.CurrencyCode + "(" + AmountDisburseID.CurrencyDescription + ")" } : new AmountDisburseIDModel { ID = 0, CodeDescription = "" },
                                    SanctionLimitCurr = funding.SanctionLimitCurrID != null ? new SanctionLimitCurrModel { ID = funding.SanctionLimitCurrID, CodeDescription = SanctionLimitCurrID.CurrencyCode + "(" + SanctionLimitCurrID.CurrencyDescription + ")" } : new SanctionLimitCurrModel { ID = 0, CodeDescription = "" },
                                    utilizationID = funding.UtilizationCurrID != null ? new utilizationIDModel { ID = funding.UtilizationCurrID, CodeDescription = utilization.CurrencyCode + "(" + utilization.CurrencyDescription + ")" } : new utilizationIDModel { ID = 0, CodeDescription = "" },
                                    OutstandingID = funding.OutstandingCurrID != null ? new OutstandingIDModel { ID = funding.OutstandingCurrID, CodeDescription = OutstandingID.CurrencyCode + "(" + OutstandingID.CurrencyDescription + ")" } : new OutstandingIDModel { ID = 0, CodeDescription = "" },
                                    ProgramType = funding.Customer != null ? new ProgramType { ProgramTypeID = funding.ProgramTypeID, ProgramTypeName = parametersysmodel1.ParameterValue } : new ProgramType { ProgramTypeID = 0, ProgramTypeName = "" },
                                    FinacleScheme = funding.Customer != null ? new FinacleScheme { FinacleSchemeCodeID = funding.FinacleSchemeCodeID, FinacleSchemeCodeName = parametersysmodel2.ParameterValue } : new FinacleScheme { FinacleSchemeCodeID = 0, FinacleSchemeCodeName = "" },
                                    Interest = funding.Customer != null ? new Interest { InterestCodeID = funding.InterestCodeID, InterestCodeName = parametersysmodel3.ParameterValue } : new Interest { InterestCodeID = "", InterestCodeName = "" },
                                    //Interest = funding.Customer != null ? new Interest { InterestCodeID = int.Parse((string)funding.InterestCodeID), InterestCodeName = parametersysmodel3.ParameterValue } : new Interest { InterestCodeID = 0, InterestCodeName = "" },
                                    RepricingPlan = funding.Customer != null ? new RepricingPlan { RepricingPlanID = funding.RepricingPlanID, RepricingPlanName = parametersysmodel4.ParameterValue } : new RepricingPlan { RepricingPlanID = 0, RepricingPlanName = "" },
                                    PeggingFrequency = funding.Customer != null ? new PeggingFrequency { PeggingFrequencyID = funding.PeggingFrequencyID, PeggingFrequencyName = parametersysmodel5.ParameterValue } : new PeggingFrequency { PeggingFrequencyID = 0, PeggingFrequencyName = "" },
                                    InterestFrequency = funding.Customer != null ? new InterestFrequency { InterestFrequencyID = funding.InterestFrequencyID, InterestFrequencyName = parametersysmodel6.ParameterValue } : new InterestFrequency { InterestFrequencyID = 0, InterestFrequencyName = "" }
                                });

                    Funding.Page = page;
                    Funding.Size = size;
                    Funding.Total = data.Count();
                    Funding.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    Funding.Rows = data.OrderBy(orderBy).AsEnumerable()
                    .Select((a, i) => new TransactionCSORow
                    {
                        RowID = i + 1,
                        CIF = a.CIF,
                        Customer = a.Customer,
                        ////BizSegment=a.BizSegment,
                        //BizSegmentName = a.BizSegmentName,
                        Currency = a.Currency,
                        SolID = a.SolID,
                        CustomerCategoryID = a.CustomerCategoryID,
                        ParameterSystemModel = a.ParameterSystemModel,
                        ProgramType = a.ProgramType,
                        LoanID = a.ParameterSystemModel,
                        FinacleScheme = a.FinacleScheme,
                        AmountDisburseID = a.AmountDisburseID,
                        SanctionLimitCurr = a.SanctionLimitCurr,
                        utilizationID = a.utilizationID,
                        OutstandingID = a.OutstandingID,
                        Interest = a.Interest,
                        RepricingPlan = a.RepricingPlan,
                        PeggingFrequency = a.PeggingFrequency,
                        InterestFrequency = a.InterestFrequency,
                        AmountDisburse = a.AmountDisburse,
                        //SolID = a.SolID,
                        CSOName = a.CSOName,
                        CSOFundingMemoID = a.CSOFundingMemoID,//Convert.ToInt32(a.CSOFundingMemoID),
                        TransactionFundingMemoID = a.TransactionFundingMemoID,//Convert.ToInt32(a.TransactionFundingMemoID),
                        ValueDate = a.ValueDate,
                        IsAdhoc = a.IsAdhoc,
                        CreditingOperative = a.CreditingOperative,
                        DebitingOperative = a.DebitingOperative,
                        MaturityDate = a.MaturityDate,
                        BaseRate = a.BaseRate,
                        AccountPreferencial = a.AccountPreferencial,
                        AllInRate = a.AllInRate,
                        ApprovedMarginAsPerCM = a.ApprovedMarginAsPerCM,
                        SpecialFTP = a.SpecialFTP,
                        Margin = a.Margin,
                        PrincipalStartDate = a.PrincipalStartDate,
                        InterestStartDate = a.InterestStartDate,
                        AllInSpecialRate = a.AllInSpecialRate,
                        PeggingDate = a.PeggingDate,
                        NoOfInstallment = a.NoOfInstallment,
                        LimitIDinFin10 = a.LimitIDinFin10,
                        LimitExpiryDate = a.LimitExpiryDate,
                        SanctionLimitAmount = a.SanctionLimitAmount,
                        utilizationAmount = a.utilizationAmount,
                        OutstandingAmount = a.OutstandingAmount,
                        OutstandingCurrID = a.OutstandingCurrID,
                        Remarks = a.Remarks,
                        OtherRemarks = a.OtherRemarks,
                        LastModifiedBy = a.LastModifiedBy,
                        LastModifiedDate = a.LastModifiedDate
                    })
                    .Skip(skip)
                    .Take(size)
                    .ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        currentUser = null;
                        context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}