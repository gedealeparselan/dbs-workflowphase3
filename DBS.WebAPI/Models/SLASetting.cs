﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("SLASetting")]
    public class SLASettingModel
    {
        /// <summary>
        /// SLASetting ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// SLASettings Product ID.
        /// </summary>
        //[MaxLength(50, ErrorMessage = "Bene Segment Name is must be in {1} characters.")]
        public int ProductID { get; set; }
        
        /// <summary>
        /// SLASetting LocationID.
        /// </summary> 
        public int SLATime { get; set; }
                
        /// <summary>
        /// Last Product
        /// </summary>
        public string Product { get; set; }
        

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class SLASettingRow : SLASettingModel
    {
        public int RowID { get; set; }

    }

    public class SLASettingGrid : Grid
    {
        public IList<SLASettingRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class SLASettingFilter : Filter { }
    #endregion

    #region Interface
    public interface ISLASettingRepository : IDisposable
    {
        bool GetSLASettingByID(int id, ref SLASettingModel SLASetting, ref string message);
        bool GetSLASetting(ref IList<SLASettingModel> SLASettings, int limit, int index, ref string message);
        bool GetSLASetting(ref IList<SLASettingModel> SLASettings, ref string message);
        bool GetSLASetting(int page, int size, IList<SLASettingFilter> filters, string sortColumn, string sortOrder, ref SLASettingGrid SLASetting, ref string message);
        bool GetSLASetting(SLASettingFilter filter, ref IList<SLASettingModel> SLASettings, ref string message);
        bool GetSLASetting(string key, int limit, ref IList<SLASettingModel> SLASettings, ref string message);
        bool AddSLASetting(SLASettingModel SLASetting, ref string message);
        bool UpdateSLASetting(int id, SLASettingModel SLASetting, ref string message);
        bool DeleteSLASetting(int id, ref string message);
    }
    #endregion

    #region Repository
    public class SLASettingRepository : ISLASettingRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetSLASettingByID(int id, ref SLASettingModel SLASetting, ref string message)
        {
            bool isSuccess = false;

            try
            {
                SLASetting = (from a in context.SLAs                              
                              join e in context.Products on a.ProductID equals e.ProductID
                              where a.IsDeleted.Equals(false) && e.IsDeleted.Equals(false) && a.SLAID.Equals(id)
                              select new SLASettingModel
                               {
                                   ID = a.SLAID,
                                   ProductID = e.ProductID,
                                   Product = e.ProductName,                                   
                                   SLATime = a.SLATime,
                                   LastModifiedDate = a.CreateDate,
                                   LastModifiedBy = a.CreateBy,
                               }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetSLASetting(ref IList<SLASettingModel> SLASettings, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    SLASettings = (from a in context.SLAs                                   
                                   join e in context.Products on a.ProductID equals e.ProductID
                                   where a.IsDeleted.Equals(false) && e.IsDeleted.Equals(false)
                                   orderby new { a.SLATime }
                                   select new SLASettingModel
                                   {
                                       ID = a.SLAID,
                                       ProductID = e.ProductID,
                                       Product = e.ProductName,                                       
                                       SLATime = a.SLATime,
                                       LastModifiedDate = a.CreateDate,
                                       LastModifiedBy = a.CreateBy,
                                   }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetSLASetting(ref IList<SLASettingModel> SLASettings, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    SLASettings = (from a in context.SLAs                                   
                                   join e in context.Products on a.ProductID equals e.ProductID
                                   where a.IsDeleted.Equals(false) && e.IsDeleted.Equals(false)
                                   orderby new { a.SLATime }
                                   select new SLASettingModel
                                   {
                                       ID = a.SLAID,
                                       ProductID = a.ProductID,
                                       Product = e.ProductName,                                       
                                       SLATime = a.SLATime,
                                       LastModifiedDate = a.CreateDate,
                                       LastModifiedBy = a.CreateBy,
                                   }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetSLASetting(int page, int size, IList<SLASettingFilter> filters, string sortColumn, string sortOrder, ref SLASettingGrid SLASettingGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                SLASettingModel filter = new SLASettingModel();
                if (filters != null)
                {

                    filter.SLATime = Convert.ToInt32((string)filters.Where(a => a.Field.Equals("SLATime")).Select(a => a.Value).SingleOrDefault());                    
                    filter.Product = (string)filters.Where(a => a.Field.Equals("Product")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.SLAs                                
                                join e in context.Products on a.ProductID equals e.ProductID
                                let isSLATime = filter.SLATime == 0 ? false : true                                
                                let isProduct = !string.IsNullOrEmpty(filter.Product)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) && e.IsDeleted.Equals(false) &&
                                    (isSLATime ? a.SLATime.Equals(filter.SLATime) : true) &&                                    
                                    (isProduct ? e.ProductName.Contains(filter.Product) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new SLASettingModel
                                {
                                    ID = a.SLAID,                                    
                                    SLATime = a.SLATime,
                                    ProductID = a.ProductID,
                                    Product = e.ProductName,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    SLASettingGrid.Page = page;
                    SLASettingGrid.Size = size;
                    SLASettingGrid.Total = data.Count();
                    SLASettingGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    SLASettingGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new SLASettingRow
                        {
                            RowID = i + 1,
                            ID = a.ID,                           
                            SLATime = a.SLATime,
                            Product = a.Product,
                            ProductID = a.ProductID,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetSLASetting(SLASettingFilter filter, ref IList<SLASettingModel> SLASettings, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetSLASetting(string key, int limit, ref IList<SLASettingModel> SLASettings, ref string message)
        {
            bool isSuccess = false;

            try
            {
                SLASettings = (from a in context.SLAs                               
                               join e in context.Products on a.ProductID equals e.ProductID
                               where a.IsDeleted.Equals(false) && e.IsDeleted.Equals(false) &&
                                    a.SLATime.Equals(key) ||  e.ProductName.Contains(key)
                               orderby new { a.SLATime }
                               select new SLASettingModel
                               {                                   
                                   ProductID = a.ProductID,
                                   Product = e.ProductName,
                                   SLATime = a.SLATime,
                               }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddSLASetting(SLASettingModel SLASetting, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.SLAs.Where(a => a.ProductID.Equals(SLASetting.ProductID) &&
                        a.IsDeleted.Equals(false)).Any())
                {
                    context.SLAs.Add(new Entity.SLA()
                    {
                        SLATime = SLASetting.SLATime,                       
                        ProductID = SLASetting.ProductID,
                        CustomerTypeID = 1,
                        BizSegmentID = 1,
                        LocationID = 1,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("SLA setting combination is already exist.", SLASetting.SLATime);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateSLASetting(int id, SLASettingModel SLASetting, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.SLA data = context.SLAs.Where(a => a.SLAID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.SLAs.Where(a => a.SLAID != SLASetting.ID &&                        
                        a.ProductID.Equals(SLASetting.ProductID) &&
                        a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("SLA setting combination is already exist.");
                    }
                    else
                    {
                        data.SLATime = SLASetting.SLATime;                       
                        data.ProductID = SLASetting.ProductID;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("SLA Setting data is does not exist.");
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteSLASetting(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.SLA data = context.SLAs.Where(a => a.SLAID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("SLA Setting data for SLA Setting ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}