﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Net.Http;
using System.Web.Script.Serialization;

namespace DBS.WebAPI.Models
{
    #region Property
    public class TransactionCBODetailModel
    {
        /// <summary>
        /// Workflow Instance ID
        /// </summary>
        public Guid? WorkflowInstanceID { get; set; }

        /// <summary>
        /// Transaction ID.
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Application ID
        /// </summary>
        public string ApplicationID { get; set; }

        /// <summary>
        /// Top Urgent
        /// </summary>
        public int? IsTopUrgent { get; set; }
        /// <summary>
        /// Top Urgent
        /// </summary>
        public int? IsTopUrgentChain { get; set; }
        /// <summary>
        /// Customer Data Details
        /// </summary>
        public CustomerModel Customer { get; set; }

        /// <summary>
        /// New Customer
        /// </summary>
        public bool IsNewCustomer { get; set; }

        /// <summary>
        /// Product Details
        /// </summary>
        public ProductModel Product { get; set; }

        /// <summary>
        /// Product Type Details
        /// </summary>
        public ProductTypeModel ProductType { get; set; }

        /// <summary>
        /// LLD Details
        /// </summary>
        public LLDModel LLD { get; set; }

        /// <summary>
        /// LLD Details
        /// </summary>
        public UnderlyingDocModel UnderlyingDoc { get; set; }
        /// <summary>
        /// Other Underlying
        /// </summary>
        public string OtherUnderlyingDoc { get; set; }

        /// <summary>
        /// Currency Details
        /// </summary>
        public CurrencyModel Currency { get; set; }

        /// <summary>
        /// Transaction Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Rate
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Eqv.USD
        /// </summary>
        public decimal AmountUSD { get; set; }

        /// <summary>
        /// Channel Details
        /// </summary>
        public ChannelModel Channel { get; set; }

        /// <summary>
        /// Debit Acc Number
        /// </summary>
        public CustomerAccountModel Account { get; set; }

        /// <summary>
        /// Application Date
        /// </summary>
        public DateTime ApplicationDate { get; set; }

        /// <summary>
        /// Bene Segment Details
        /// </summary>
        public BizSegmentModel BizSegment { get; set; }

        /// <summary>
        /// Bank Details
        /// </summary>
        public BankModel Bank { get; set; }

        /// <summary>
        /// Bank Charges
        /// </summary>
        public ChargesTypeModel BankCharges { get; set; }

        /// <summary>
        /// Agent Charges
        /// </summary>
        public ChargesTypeModel AgentCharges { get; set; }

        /// <summary>
        ///    TransactionType
        /// </summary>
        public TransactionTypeModel TransactionType { get; set; }

        /// <summary>
        /// MaintenanceType
        /// </summary>
        public MaintenanceTypeModel MaintenanceType { get; set; }
        public TransactionSubTypeModel TransactionSubType { get; set; }

        //TagUntag
        public StaffTaggingModel StaffTagging { get; set; }
        //End

        public BeneficiaryCountryModel BeneficiaryCountry { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        public decimal? Type { get; set; }

        /// <summary>
        /// Is Citizen
        /// </summary>
        public bool IsCitizen { get; set; }

        /// <summary>
        /// Is Resident
        /// </summary>
        public bool IsResident { get; set; }

        ///// <summary>
        ///// Lld Code
        ///// </summary>
        //public string LLDCode { get; set; }

        ///// <summary>
        ///// Lld Info
        ///// </summary>
        //public string LLDInfo { get; set; }        

        /// <summary>
        /// Signature Verification
        /// </summary>
        public bool IsSignatureVerified { get; set; }

        /// <summary>
        /// Dormant Account
        /// </summary>
        public bool IsDormantAccount { get; set; }

        /// <summary>
        /// Freeze Account
        /// </summary>
        public bool IsFreezeAccount { get; set; }

        /// basri 30-09-2015
        /// <summary>
        /// Callback Required
        /// </summary>
        public bool IsCallbackRequired { get; set; }

        /// <summary>
        /// Letter of Indemnity Available
        /// </summary>
        public bool IsLOIAvailable { get; set; }
        ///end basri


        /// <summary>
        /// Others
        /// </summary>
        public string Others { get; set; }

        /// <summary>
        /// Is Draft
        /// </summary>
        public bool IsDraft { get; set; }

        /// <summary>
        /// Create Date
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// List Transaction Document Details
        /// </summary>
        public IList<TransactionDocumentModel> Documents { get; set; }

        /// <summary>
        /// Purpose detail of compliance
        /// </summary>
        public string DetailCompliance { get; set; }

        /// <summary>
        /// Bene Name
        /// </summary>
        public string BeneName { get; set; }

        /// <summary>
        /// Bene Name
        /// </summary>
        public string BeneAccNumber { get; set; }

        /// <summary>
        /// TZ Number
        /// </summary>
        public string TZNumber { get; set; }

        public string PaymentDetails { get; set; }

        /// <summary>
        /// Underlyings
        /// </summary> 
        public IList<TransUnderlyingModel> Underlyings { get; set; }

        /// <summary>
        /// Transaction Create By
        /// </summary> //add reizvan
        public string CreateBy { get; set; }

        /// <summary>
        /// Transaction Is Other Bene Bank
        /// </summary> //add reizvan
        public bool IsOtherBeneBank { get; set; }

        /// <summary>
        /// Transaction Other Bene Bank Name
        /// </summary> //add reizvan
        public string OtherBeneBankName { get; set; }

        /// <summary>
        /// Transaction Other Bene Bank Swift
        /// </summary> //add reizvan
        public string OtherBeneBankSwift { get; set; }
        public string StaffID { get; set; }
        public string AtmNumber { get; set; }
        public bool? Selected { get; set; }
        public bool? IsChangeRM { get; set; }
        public bool? IsSegment { get; set; }
        public bool? IsSolID { get; set; }
        public bool? IsLOI { get; set; }
        public bool? IsPOI { get; set; }
        public bool? IsPOA { get; set; }
        public CustomerDraftModel CustomerDraft { get; set; }
        public bool IsDocumentComplete { get; set; }
        public long? ApproverID { get; set; }
        public bool? IsOtherAccountNumber { get; set; }
        public string OtherAccountNumber { get; set; }
        public CurrencyModel DebitCurrency { get; set; }
        public RetailCIFCBO RetailCIFCBO { get; set; }//add by fandi
        public IList<CustomerModalModel> AddJoinTableCustomerCIF { get; set; }
        public IList<FFDAccountModalModel> AddJoinTableFFDAccountCIF { get; set; }
        public IList<FFDAccountModalModel> AddJoinTableAccountCIF { get; set; }
        public IList<DormantAccountModel> AddJoinTableDormantCIF { get; set; }
        public IList<FreezeUnfreezeModel> AddJoinTableFreezeUnfreezeCIF { get; set; }
        public IList<ChangeRMModel> ChangeRMModel { get; set; }
        public IList<ReviseMakerDocumentModel> ReviseMakerDocuments { get; set; }
        public BranchRiskRatingModel BrachRiskRating { get; set; }
        public IList<AccountNumberLienUnlienModel> AccountNumberLienUnlien { get; set; }
        public string AccountNumber { get; set; }

        public IList<RCCBOContactModel> SelularPhoneNumbers { get; set; }
        public IList<RCCBOContactModel> OfficePhoneNumbers { get; set; }
        public IList<RCCBOContactModel> HomePhoneNumbers { get; set; }
        public string DispatchModeOther { get; set; }
    }
    public class RCCBOContactModel
    {
        public long RCCBOContactID { get; set; }
        public string ActionType { get; set; }
        public string CountryCode { get; set; }
        public string CityCode { get; set; }
        public string ContactType { get; set; }
        public string PhoneNumber { get; set; }
        public long? RetailCIFCBOID { get; set; }
        public string UpdateCountryCode { get; set; }
        public string UpdateCityCode { get; set; }
        public string UpdatePhoneNumber { get; set; }
        public long? RetailCIFCBOContactUpdateID { get; set; }


    }
    public class RCCBOContactDraftModel
    {
        public string ActionType { get; set; }
        public string CountryCode { get; set; }
        public string CityCode { get; set; }
        public string ContactType { get; set; }
        public string PhoneNumber { get; set; }
        public long? RetailCIFCBODraftID { get; set; }
        public string UpdateCountryCode { get; set; }
        public string UpdateCityCode { get; set; }
        public string UpdatePhoneNumber { get; set; }

    }
    public class TransactionCBOContactDetailModel
    {
        public TransactionCBODetailModel Transaction { get; set; }
        public IList<CustomerContactModel> Contacts { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public IList<VerifyModel> Verify { get; set; }
        public TransactionCheckerDataModel Checker { get; set; }
        public IList<TransactionCallbackTimeModel> Callbacks { get; set; }
    }
    public class RetailCIFCBO
    {
        public long RetailCIFCBOID { get; set; }
        public long TransactionID { get; set; }
        public int? DispatchModeTypeID { get; set; }
        public string AccountNumber { get; set; }
        public int? RequestTypeID { get; set; }
        public Nullable<int> MaintenanceTypeID { get; set; }
        public bool? IsNameMaintenance { get; set; }
        public bool? IsIdentityTypeMaintenance { get; set; }
        public bool? IsNPWPMaintenance { get; set; }
        public bool? IsMaritalStatusMaintenance { get; set; }
        public bool? IsCorrespondenceMaintenance { get; set; }
        public bool? IsIdentityAddressMaintenance { get; set; }
        public bool? IsOfficeAddressMaintenance { get; set; }
        public bool? IsCorrespondenseAddressMaintenance { get; set; }
        public bool? IsPhoneFaxEmailMaintenance { get; set; }
        public bool? IsNationalityMaintenance { get; set; }
        public bool? IsFundSourceMaintenance { get; set; }
        public bool? IsNetAssetMaintenance { get; set; }
        public bool? IsMonthlyIncomeMaintenance { get; set; }
        public bool? IsJobMaintenance { get; set; }
        public bool? IsAccountPurposeMaintenance { get; set; }
        public bool? IsMonthlyTransactionMaintenance { get; set; }
        public string Name { get; set; }
        public int? IdentityTypeID { get; set; }
        public string IdentityNumber { get; set; }
        public string IdentityType3 { get; set; }
        public string IdentityType2 { get; set; }
        public string IdentityType4 { get; set; }
        public DateTime? IdentityStartDate { get; set; }
        public DateTime? IdentityEndDate { get; set; }
        public string IdentityAddress { get; set; }
        public string IdentityKelurahan { get; set; }
        public string IdentityKecamatan { get; set; }
        public string IdentityCity { get; set; }
        public string IdentityProvince { get; set; }
        public string IdentityCountry { get; set; }
        public string IdentityPostalCode { get; set; }

        public int? IdentityTypeID2 { get; set; }
        public string IdentityNumber2 { get; set; }
        public DateTime? IdentityStartDate2 { get; set; }
        public DateTime? IdentityEndDate2 { get; set; }
        public string IdentityAddress2 { get; set; }
        public string IdentityKelurahan2 { get; set; }
        public string IdentityKecamatan2 { get; set; }
        public string IdentityCity2 { get; set; }
        public string IdentityProvince2 { get; set; }
        public string IdentityCountry2 { get; set; }
        public string IdentityPostalCode2 { get; set; }

        public int? IdentityTypeID3 { get; set; }
        public string IdentityNumber3 { get; set; }
        public DateTime? IdentityStartDate3 { get; set; }
        public DateTime? IdentityEndDate3 { get; set; }
        public string IdentityAddress3 { get; set; }
        public string IdentityKelurahan3 { get; set; }
        public string IdentityKecamatan3 { get; set; }
        public string IdentityCity3 { get; set; }
        public string IdentityProvince3 { get; set; }
        public string IdentityCountry3 { get; set; }
        public string IdentityPostalCode3 { get; set; }

        public int? IdentityTypeID4 { get; set; }
        public string IdentityNumber4 { get; set; }
        public DateTime? IdentityStartDate4 { get; set; }
        public DateTime? IdentityEndDate4 { get; set; }
        public string IdentityAddress4 { get; set; }
        public string IdentityKelurahan4 { get; set; }
        public string IdentityKecamatan4 { get; set; }
        public string IdentityCity4 { get; set; }
        public string IdentityProvince4 { get; set; }
        public string IdentityCountry4 { get; set; }
        public string IdentityPostalCode4 { get; set; }
        public string NPWPNumber { get; set; }
        public bool? IsNPWPReceived { get; set; }
        public int? MaritalStatusID { get; set; }
        public string SpouseName { get; set; }
        public bool? IsCorrespondenseToEmail { get; set; }
        public string CorrespondenseAddress { get; set; }
        public string CorrespondenseKelurahan { get; set; }
        public string CorrespondenseKecamatan { get; set; }
        public string CorrespondenseCity { get; set; }
        public string CorrespondenseProvince { get; set; }
        public string CorrespondenseCountry { get; set; }
        public string CorrespondensePostalCode { get; set; }
        public Nullable<int> CellPhoneMethodID { get; set; }
        public string CellPhone { get; set; }
        public string UpdatedCellPhone { get; set; }
        public int? HomePhoneMethodID { get; set; }
        public string HomePhone { get; set; }
        public string UpdatedHomePhone { get; set; }
        public int? OfficePhoneMethodID { get; set; }
        public string OfficePhone { get; set; }
        public string UpdatedOfficePhone { get; set; }
        public int? FaxMethodID { get; set; }
        public string Fax { get; set; }
        public string UpdatedFax { get; set; }
        public string EmailAddress { get; set; }
        public string OfficeAddress { get; set; }
        public string OfficeKelurahan { get; set; }
        public string OfficeKecamatan { get; set; }
        public string OfficeCity { get; set; }
        public string OfficeProvince { get; set; }
        public string OfficeCountry { get; set; }
        public string OfficePostalCode { get; set; }
        public string Nationality { get; set; }
        public int? FundSource { get; set; }
        public string UBOName { get; set; }
        public string UBOIdentityType { get; set; }
        public string UBOPhone { get; set; }
        public string UBOJob { get; set; }
        public int? NetAsset { get; set; }
        public int? MonthlyIncome { get; set; }
        public int? MonthlyExtraIncome { get; set; }
        public int? Job { get; set; }
        public int? AccountPurpose { get; set; }
        public int? IncomeForecast { get; set; }
        public int? OutcomeForecast { get; set; }
        public int? TransactionForecast { get; set; }
        public int? SubType { get; set; }
        public string AtmNumber { get; set; }
        public DateTime? RiskRatingReportDate { get; set; }
        public DateTime? RiskRatingNextReviewDate { get; set; }
        public int? RiskRatingResultID { get; set; }
        public string Remarks { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }

        public Transaction Transaction { get; set; }
        public RetailCIFCBOAccNo RetailCIFCBOAccNoes { get; set; }
        public string IdentityType { get; set; }
        public string MaritalStatus { get; set; }
        public string FundSources { get; set; }
        public string NetAssets { get; set; }
        public string MonthlyIncomes { get; set; }
        public string MonthlyExtraIncomes { get; set; }
        public string Jobs { get; set; }
        public string AccountPurposes { get; set; }
        public string IncomeForecasts { get; set; }
        public string OutcomeForecasts { get; set; }
        public string TransactionForecasts { get; set; }
        public string SubTypes { get; set; }
        public string DispatchModeType { get; set; }
        public string MaintenanceTypes { get; set; }
        public string RequestTypes { get; set; }
        public string CompanyName { get; set; }
        public string Position { get; set; }
        public string WorkPeriod { get; set; }
        public string IndustryType { get; set; }
        //added by dani 12-apr-16 start
        public string PekerjaanProfesional { get; set; }
        public string PekerjaanLainnya { get; set; }
        public string TujuanBukaRekeningLainnya { get; set; }
        public string SumberDanaLainnya { get; set; }
        public decimal? AmountLienUnlien { get; set; }

        public bool? IsTujuanBukaRekeningLainnya { get; set; }
        public bool? IsSumberDanaLainnya { get; set; }
        public bool? IsPekerjaanProfesional { get; set; }
        public bool? IsPekerjaanLainnya { get; set; }

        public bool? IsCommingPersonally { get; set; }
        public bool? IsPersonallyKnown { get; set; }
        //end by dani
        public bool? IsSignature { get; set; }
        public bool? IsPenawaranProductPerbankan { get; set; }
        public bool? IsStampDuty { get; set; }
        public bool? IsOthers { get; set; }

        //add new henggar 210417
        public bool? IsMotherMaiden { get; set; }
        public bool? IsEducation { get; set; }
        public bool? IsReligion { get; set; }
        public bool? IsDataUBO { get; set; }
        public bool? IsTransparansiDataPenawaranProduct { get; set; }
        public bool? IsTransparansiPenggunaan { get; set; }
        public bool? IsPenawaranProduct { get; set; }
        public bool? IsSLIK { get; set; }
        public bool? IsFatcaCRS { get; set; }
        public bool? IsChangeSignature { get; set; }
        public bool? ChangeSignature { get; set; }
        public string MotherMaiden { get; set; }
        public int? Education { get; set; }
        public int? Religion { get; set; }
        public int? DataUBOCif { get; set; }
        public string DataUBOName { get; set; }
        public string DataUBORelationship { get; set; }
        public int? GrosIncomeccy { get; set; }
        public decimal? GrossIncomeAmount { get; set; }
        public int? GrossCifSpouse { get; set; }
        public string GrossSpouseName { get; set; }
        public string GrossSpouseRelation { get; set; }
        public int? FatcaCountryCode { get; set; }
        public int? FatcaReviewStatus { get; set; }
        public int? FatcaCRSStatus { get; set; }
        public int? FatcaTaxPayer { get; set; }
        public int? FatcaWithHolding { get; set; }
        public DateTime? FatcaReviewStatusDate { get; set; }
        public DateTime? FatcaDateOnForm { get; set; }
        public string FatcaTaxPayerID { get; set; }
        public bool? IDS { get; set; }
        public bool? CallbackIsRequired { get; set; }
        public bool? UtcApproval { get; set; }
        public int? Tier { get; set; }
        public string DispatchModeOther { get; set; }
        //end
    }
    public class AccountNumberLienUnlienModel
    {
        public string AccountNumber { get; set; }
        public bool? IsJointAccount { get; set; }
        public bool? IsSelected { get; set; }
        public string LienUnlien { get; set; }
        public bool IsBranchCheckerVerify { get; set; }
        public bool IsCBOAccountCheckerVerify { get; set; }
    }
    public class ChangeRMModel
    {
        public long ChangeRMTransactionID { get; set; }
        public long TransactionID { get; set; }
        public int? ActivityHistoryID { get; set; }
        public string No { get; set; }
        public string CIF { get; set; }
        public string CustomerName { get; set; }
        public string ChangeSegmentFrom { get; set; }
        public string ChangeSegmentTo { get; set; }
        public string ChangeFromBranchCode { get; set; }
        public string ChangeFromRMCode { get; set; }
        public string ChangeFromBU { get; set; }
        public string ChangeToBranchCode { get; set; }
        public string ChangeToRMCode { get; set; }
        public string ChangeToBU { get; set; }
        public string Account { get; set; }
        public string PCCode { get; set; }
        public string EATPB { get; set; }
        public string Remarks { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public bool? Selected { get; set; }
    }
    public class CIFSUMBERDANA
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class CIFASSETBERSIH
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class CIFPENDPATANBULANAN
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class CIFPENGHASILANTAMBAHAN
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class CIFPEKERJAAN
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class CIFTUJUANPEMBUKAANREKENING
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class CIFPERKIRAANDANAMASUK
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class CIFPERKIRAANDANAKELUAR
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class CIFPERKIRAANTRANSAKSIKELUAR
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class CIFJENISIDENTITAS
    {
        public int ID;
        public string Name;
    }
    #endregion

    #region Interface
    public interface IWorkflowCIFRepository : IDisposable
    {
        #region Affandy
        bool GetCIFSUMBERDANA(ref IList<CIFSUMBERDANA> CIFSUMBERDANA, ref string message);
        bool GetCIFJENISIDENTITAS(ref IList<CIFJENISIDENTITAS> CIFJENISIDENTITAS, ref string message);
        bool GetCIFASSETBERSIH(ref IList<CIFASSETBERSIH> CIFASETBERSIH, ref string message);
        bool GetCIFPENDPATANBULANAN(ref IList<CIFPENDPATANBULANAN> CIFPENDPATANBULANAN, ref string message);
        bool GetCIFPENGHASILANTAMBAHAN(ref IList<CIFPENGHASILANTAMBAHAN> CIFPENGHASILANTAMBAHAN, ref string message);
        bool GetCIFPEKERJAAN(ref IList<CIFPEKERJAAN> CIFPEKERJAAN, ref string message);
        bool GetCIFTUJUANPEMBUKAANREKENING(ref IList<CIFTUJUANPEMBUKAANREKENING> CIFTUJUANPEMBUKAANREKENING, ref string message);
        bool GetCIFPERKIRAANDANAMASUK(ref IList<CIFPERKIRAANDANAMASUK> CIFPERKIRAANDANAMASUK, ref string message);
        bool GetCIFPERKIRAANDANAKELUAR(ref IList<CIFPERKIRAANDANAKELUAR> CIFPERKIRAANDANAKELUAR, ref string message);
        bool GetCIFPERKIRAANTRANSAKSIKELUAR(ref IList<CIFPERKIRAANTRANSAKSIKELUAR> CIFPERKIRAANTRANSAKSIKELUAR, ref string message);
        bool GetTransactionDetails(Guid instanceID, ref TransactionCBODetailModel transaction, ref string message);
        bool GetTransactionDetailsChangeRM(Guid instanceID, ref TransactionCBODetailModel transaction, ref string message);
        bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineModel> timelines, ref string message);
        bool GetTransactionContactDetails(Guid workflowInstanceID, long approverID, ref IList<CustomerContactModel> output, ref string message);
        bool GetTransactionCheckerDetails(Guid workflowInstanceID, long approverID, ref TransactionCBOContactDetailModel output, ref string message);
        bool GetTransactionCallbackDetails(Guid workflowInstanceID, long approverID, ref TransactionCBOContactDetailModel output, ref string message);
        bool GetTransactionCBOCheckerDetails(Guid workflowInstanceID, long approverID, ref TransactionCBOContactDetailModel output, ref string message);
        bool AddTransactionCallback(Guid workflowInstanceID, long approverID, TransactionCallbackDetailModel data, ref string message);
        bool AddTransactionCheckerDetails(Guid workflowInstanceID, long approverID, TransactionCheckerDetailModel data, ref string message);
        bool AddTransactionCBOCheckerDetails(Guid workflowInstanceID, long approverID, TransactionCheckerDetailModel data, ref string message);
        bool AddTransactionCBOCheckerDetailsRevise(Guid workflowInstanceID, long approverID, TransactionCheckerDetailModel data, ref string message);
        bool UpdateTransactionCheckerDetails(Guid workflowInstanceID, long approverID, TransactionCheckerDetailModel data, ref string message);
        bool UpdateTransactionForBranchMaker(Guid workflowInstanceID, long approverID, TransactionCBOContactDetailModel transaction, ref string message);
        bool UpdateTransactionCBOMakerATMDetails(Guid workflowInstanceID, long approverID, TransactionCBOContactDetailModel transaction, ref string message);
        bool UpdateTransactionCBOMakerDispatchDetails(Guid workflowInstanceID, long approverID, TransactionCBOContactDetailModel transaction, ref string message);
        bool GetTransactionDetailsChangeRMUploaded(IList<int> activityHistoryID, ref IList<ChangeRMModel> ChangeRMModel, ref string message);
        bool GetTransactionDetailsChangeRMReuploaded(long transactionID, int activityHistoryID, bool isBringUp, ref IList<ChangeRMModel> ChangeRMModel, ref string message);
        bool GetCIfStaffTagging(ref IList<CIFPEKERJAAN> CIFStaffTagging, ref string message);
        #endregion
    }
    #endregion

    #region Repository
    public class WorkflowCIFRepository : IWorkflowCIFRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        #region Affandi
        //get dropdown all CIF
        public bool GetCIFSUMBERDANA(ref IList<CIFSUMBERDANA> CIFSUMBERDANA, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    CIFSUMBERDANA = (from a in context.ParameterSystems
                                     where a.ParameterType == "CIF_SUMBER_DANA"
                                     orderby new { a.ParameterValue }
                                     select new CIFSUMBERDANA
                                     {
                                         ID = a.ParsysID,
                                         Name = a.ParameterValue
                                     }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetCIFASSETBERSIH(ref IList<CIFASSETBERSIH> CIFASETBERSIH, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    CIFASETBERSIH = (from a in context.ParameterSystems
                                     where a.ParameterType == "CIF_ASSET_BERSIH"
                                     orderby new { a.ParameterValue }
                                     select new CIFASSETBERSIH
                                     {
                                         ID = a.ParsysID,
                                         Name = a.ParameterValue
                                     }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetCIFPENDPATANBULANAN(ref IList<CIFPENDPATANBULANAN> CIFPENDPATANBULANAN, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    CIFPENDPATANBULANAN = (from a in context.ParameterSystems
                                           where a.ParameterType == "CIF_PENDAPATAN_BULANAN"
                                           orderby new { a.ParameterValue }
                                           select new CIFPENDPATANBULANAN
                                           {
                                               ID = a.ParsysID,
                                               Name = a.ParameterValue
                                           }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetCIFPENGHASILANTAMBAHAN(ref IList<CIFPENGHASILANTAMBAHAN> CIFPENGHASILANTAMBAHAN, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    CIFPENGHASILANTAMBAHAN = (from a in context.ParameterSystems
                                              where a.ParameterType == "CIF_PENGHASILAN_TAMBAHAN"
                                              orderby new { a.ParameterValue }
                                              select new CIFPENGHASILANTAMBAHAN
                                              {
                                                  ID = a.ParsysID,
                                                  Name = a.ParameterValue
                                              }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetCIFPEKERJAAN(ref IList<CIFPEKERJAAN> CIFPEKERJAAN, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    CIFPEKERJAAN = (from a in context.ParameterSystems
                                    where a.ParameterType == "CIF_PEKERJAAN"
                                    orderby new { a.ParameterValue }
                                    select new CIFPEKERJAAN
                                    {
                                        ID = a.ParsysID,
                                        Name = a.ParameterValue
                                    }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetCIFTUJUANPEMBUKAANREKENING(ref IList<CIFTUJUANPEMBUKAANREKENING> CIFTUJUANPEMBUKAANREKENING, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    CIFTUJUANPEMBUKAANREKENING = (from a in context.ParameterSystems
                                                  where a.ParameterType == "CIF_TUJUAN_PEMBUKAAN_REKENING"
                                                  orderby new { a.ParameterValue }
                                                  select new CIFTUJUANPEMBUKAANREKENING
                                                  {
                                                      ID = a.ParsysID,
                                                      Name = a.ParameterValue
                                                  }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetCIFPERKIRAANDANAMASUK(ref IList<CIFPERKIRAANDANAMASUK> CIFPERKIRAANDANAMASUK, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    CIFPERKIRAANDANAMASUK = (from a in context.ParameterSystems
                                             where a.ParameterType == "CIF_PERKIRAAN_DANA_MASUK"
                                             orderby new { a.ParameterValue }
                                             select new CIFPERKIRAANDANAMASUK
                                             {
                                                 ID = a.ParsysID,
                                                 Name = a.ParameterValue
                                             }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetCIFPERKIRAANDANAKELUAR(ref IList<CIFPERKIRAANDANAKELUAR> CIFPERKIRAANDANAKELUAR, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    CIFPERKIRAANDANAKELUAR = (from a in context.ParameterSystems
                                              where a.ParameterType == "CIF_PERKIRAAN_DANA_KELUAR"
                                              orderby new { a.ParameterValue }
                                              select new CIFPERKIRAANDANAKELUAR
                                              {
                                                  ID = a.ParsysID,
                                                  Name = a.ParameterValue
                                              }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetCIFPERKIRAANTRANSAKSIKELUAR(ref IList<CIFPERKIRAANTRANSAKSIKELUAR> CIFPERKIRAANTRANSAKSIKELUAR, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    CIFPERKIRAANTRANSAKSIKELUAR = (from a in context.ParameterSystems
                                                   where a.ParameterType == "CIF_PERKIRAAN_TRANSAKSI_KELUAR"
                                                   orderby new { a.ParameterValue }
                                                   select new CIFPERKIRAANTRANSAKSIKELUAR
                                                   {
                                                       ID = a.ParsysID,
                                                       Name = a.ParameterValue
                                                   }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetCIFJENISIDENTITAS(ref IList<CIFJENISIDENTITAS> CIFJENISIDENTITAS, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    CIFJENISIDENTITAS = (from a in context.ParameterSystems
                                         where a.ParameterType == "CIF_JENIS_IDENTITAS"
                                         orderby new { a.ParameterValue }
                                         select new CIFJENISIDENTITAS
                                         {
                                             ID = a.ParsysID,
                                             Name = a.ParameterValue
                                         }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCIfStaffTagging(ref IList<CIFPEKERJAAN> CIFStaffTagging, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    CIFStaffTagging = (from a in context.ParameterSystems
                                       where a.ParameterType == "CIF_STAFF_TAGGING"
                                       orderby new { a.ParameterValue }
                                       select new CIFPEKERJAAN
                                       {
                                           ID = a.ParsysID,
                                           Name = a.ParameterValue
                                       }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        //end by dani

        public bool GetTransactionDetails(Guid instanceID, ref TransactionCBODetailModel transaction, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                context.Transactions.AsNoTracking();
                TransactionCBODetailModel datatransaction = new TransactionCBODetailModel();

                var dataTransaction = (from trans in context.SP_GetTransactionCIF(instanceID) select trans).SingleOrDefault();

                datatransaction.ID = dataTransaction.TransactionID;
                datatransaction.WorkflowInstanceID = dataTransaction.WorkflowInstanceID;
                datatransaction.ApplicationID = dataTransaction.ApplicationID;
                datatransaction.ApplicationDate = dataTransaction.ApplicationDate;
                datatransaction.ApproverID = dataTransaction.ApproverID;
                datatransaction.Product = dataTransaction.ProductID > 0 ? new ProductModel()
                {
                    ID = dataTransaction.ProductID,
                    Code = dataTransaction.ProductCode,
                    Name = dataTransaction.ProductName,
                    WorkflowID = dataTransaction.ProductWorkflowID,
                    Workflow = dataTransaction.WorkflowName,
                    LastModifiedBy = dataTransaction.UpdateDate_Product == null ? dataTransaction.CreateBy_Product : dataTransaction.UpdateBy_Product,
                    LastModifiedDate = dataTransaction.UpdateDate_Product ?? dataTransaction.CreateDate_Product
                } : new ProductModel()
                {
                    ID = 19,
                    Code = "",
                    Name = "",
                    WorkflowID = 0,
                    Workflow = "",
                    LastModifiedBy = dataTransaction.UpdateDate_Product == null ? dataTransaction.CreateBy_Product : dataTransaction.UpdateBy_Product,
                    LastModifiedDate = dataTransaction.UpdateDate_Product ?? dataTransaction.CreateDate_Product
                };
                datatransaction.Channel = dataTransaction.ChannelID > 0 ? new ChannelModel()
                {
                    ID = dataTransaction.ChannelID,
                    Name = dataTransaction.ChannelName,
                    LastModifiedBy = dataTransaction.UpdateDate_Product == null ? dataTransaction.CreateBy_Product : dataTransaction.UpdateBy_Product,
                    LastModifiedDate = dataTransaction.UpdateDate_Product ?? dataTransaction.CreateDate_Product
                } : new ChannelModel()
                {
                    ID = 1,
                    Name = "",
                    LastModifiedBy = dataTransaction.UpdateDate_Product == null ? dataTransaction.CreateBy_Product : dataTransaction.UpdateBy_Product,
                    LastModifiedDate = dataTransaction.UpdateDate_Product ?? dataTransaction.CreateDate_Product
                };
                datatransaction.Currency = dataTransaction.CurrencyID > 0 ? new CurrencyModel()
                {
                    ID = dataTransaction.CurrencyID,
                    Code = dataTransaction.CurrencyCode,
                    Description = dataTransaction.CurrencyDescription,
                    CodeDescription = dataTransaction.CurrencyCode + " (" + dataTransaction.CurrencyDescription + ")",
                    LastModifiedBy = dataTransaction.UpdateDate_Currency == null ? dataTransaction.CreateBY_Currency : dataTransaction.UpdateBy_Currency,
                    LastModifiedDate = dataTransaction.UpdateDate_Currency ?? dataTransaction.CreateDate_Currency
                } : new CurrencyModel()
                {
                    ID = 13,
                    Code = "",
                    Description = "",
                    CodeDescription = "(-) -",
                    LastModifiedBy = dataTransaction.UpdateDate_Currency == null ? dataTransaction.CreateBY_Currency : dataTransaction.UpdateBy_Currency,
                    LastModifiedDate = dataTransaction.UpdateDate_Currency ?? dataTransaction.CreateDate_Currency
                };
                datatransaction.Bank = dataTransaction.BankID > 0 ? new BankModel()
                {
                    ID = dataTransaction.BankID,
                    Code = dataTransaction.BankCode,
                    BankAccount = dataTransaction.BankAccount,
                    BranchCode = dataTransaction.BranchCode,
                    CommonName = dataTransaction.CommonName,
                    Currency = dataTransaction.Currency,
                    PGSL = dataTransaction.Currency,
                    SwiftCode = dataTransaction.SwiftCode,
                    Description = dataTransaction.BankDescription,
                    LastModifiedBy = dataTransaction.UpdateDate_Bank == null ? dataTransaction.CreateBy_Bank : dataTransaction.UpdateBy_Bank,
                    LastModifiedDate = dataTransaction.UpdateDate_Bank ?? dataTransaction.CreateDate_Bank
                } : new BankModel()
                {
                    ID = 1,
                    Code = "",
                    BankAccount = "",
                    BranchCode = "",
                    CommonName = "",
                    Currency = "",
                    PGSL = "",
                    SwiftCode = "",
                    Description = "",
                    LastModifiedBy = dataTransaction.UpdateDate_Bank == null ? dataTransaction.CreateBy_Bank : dataTransaction.UpdateBy_Bank,
                    LastModifiedDate = dataTransaction.UpdateDate_Bank ?? dataTransaction.CreateDate_Bank
                };
                datatransaction.BizSegment = dataTransaction.BizSegmentID > 0 ? new BizSegmentModel()
                {
                    ID = dataTransaction.BizSegmentID,
                    Name = dataTransaction.BizSegmentName,
                    Description = dataTransaction.BizSegmentDescription,
                    LastModifiedBy = dataTransaction.UpdateDate_BizSegment == null ? dataTransaction.CreateBy_BizSegment : dataTransaction.UpdateBy_BizSegment,
                    LastModifiedDate = dataTransaction.UpdateDate_BizSegment ?? dataTransaction.CreateDate_BizSegment
                } : new BizSegmentModel()
                {
                    ID = 1,
                    Name = "",
                    Description = "",
                    LastModifiedBy = dataTransaction.UpdateDate_BizSegment == null ? dataTransaction.CreateBy_BizSegment : dataTransaction.UpdateBy_BizSegment,
                    LastModifiedDate = dataTransaction.UpdateDate_BizSegment ?? dataTransaction.CreateDate_BizSegment
                };
                datatransaction.TransactionType = dataTransaction.TransactionTypeID > 0 ? new TransactionTypeModel()
                {
                    ID = dataTransaction.TransactionTypeID,
                    Name = dataTransaction.TransactionType
                } : new TransactionTypeModel()
                {
                    ID = 1,
                    Name = ""
                };
                datatransaction.MaintenanceType = dataTransaction.CBOMaintainID > 0 ? new MaintenanceTypeModel()
                {
                    ID = dataTransaction.CBOMaintainID,
                    Name = dataTransaction.MaintenanceTypeName
                } : new MaintenanceTypeModel()
                {
                    ID = 3,
                    Name = ""
                };
                datatransaction.TransactionSubType = dataTransaction.TransactionSubTypeID > 0 ? new TransactionSubTypeModel()
                {
                    ID = dataTransaction.TransactionSubTypeID,
                    Name = dataTransaction.TransactionSubType

                } : new TransactionSubTypeModel()
                {
                    ID = 1,
                    Name = ""
                };
                datatransaction.StaffID = dataTransaction.StaffID;
                datatransaction.StaffTagging = dataTransaction.StaffTaggingID > 0 ? new StaffTaggingModel()
                {
                    ID = dataTransaction.StaffTaggingID,
                    Name = dataTransaction.StaffTaggingName

                } : new StaffTaggingModel()
                {
                    ID = 1,
                    Name = ""
                };
                datatransaction.AtmNumber = dataTransaction.ATMNumber;
                datatransaction.Amount = dataTransaction.Amount;
                datatransaction.AmountUSD = dataTransaction.AmountUSD;
                datatransaction.AccountNumber = dataTransaction.AccountNumber;
                datatransaction.Account = !string.IsNullOrEmpty(dataTransaction.AccountNumber) ? new CustomerAccountModel()
                {
                    AccountNumber = dataTransaction.AccountNumber

                } : new CustomerAccountModel()
                {
                    AccountNumber = ""
                };
                datatransaction.Rate = dataTransaction.Rate;
                datatransaction.BeneName = dataTransaction.BeneName;
                datatransaction.BeneAccNumber = dataTransaction.BeneAccNumber;
                datatransaction.Others = dataTransaction.Others;
                datatransaction.TZNumber = dataTransaction.TZNumber;
                datatransaction.IsTopUrgent = dataTransaction.IsTopUrgent;
                datatransaction.IsSignatureVerified = dataTransaction.IsSignatureVerified;
                datatransaction.IsFreezeAccount = dataTransaction.IsFrezeAccount;
                datatransaction.IsDormantAccount = dataTransaction.IsDormantAccount;
                datatransaction.IsDraft = dataTransaction.IsDraft;
                datatransaction.IsNewCustomer = dataTransaction.IsNewCustomer;
                datatransaction.IsLOI = dataTransaction.IsLOI;
                datatransaction.IsPOI = dataTransaction.IsPOI;
                datatransaction.IsPOA = dataTransaction.IsPOA;
                datatransaction.IsOtherAccountNumber = dataTransaction.IsOtherAccountNumber;
                datatransaction.IsChangeRM = dataTransaction.IsChangeRM;
                datatransaction.IsSegment = dataTransaction.IsSegment;
                datatransaction.IsSolID = dataTransaction.IsSolID;
                datatransaction.CreateDate = dataTransaction.CreateDate;
                datatransaction.LastModifiedBy = dataTransaction.UpdateDate == null ? dataTransaction.CreateBy : dataTransaction.UpdateBy;
                datatransaction.LastModifiedDate = dataTransaction.UpdateDate ?? dataTransaction.CreateDate;
                datatransaction.OtherBeneBankSwift = dataTransaction.OtherBeneBankSwift;
                datatransaction.OtherBeneBankName = dataTransaction.OtherBeneBankName;
                datatransaction.IsCitizen = dataTransaction.IsCitizen;
                datatransaction.IsResident = dataTransaction.IsResident;
                datatransaction.Type = dataTransaction.Type;
                datatransaction.IsDocumentComplete = dataTransaction.IsDocumentComplete;
                datatransaction.Selected = dataTransaction.IsChangeRM;

                if (datatransaction != null)
                {
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    RetailCIFCBO rcc = new RetailCIFCBO();
                    long IDTrns = datatransaction.ID;
                    datatransaction.SelularPhoneNumbers = new List<RCCBOContactModel>();
                    datatransaction.HomePhoneNumbers = new List<RCCBOContactModel>();
                    datatransaction.OfficePhoneNumbers = new List<RCCBOContactModel>();
                    datatransaction.Documents = new List<TransactionDocumentModel>();
                    //datatransaction.IsOtherBeneBank = datatransaction.Bank.Code != "999" ? false : true;
                    #region GetDocuments
                    List<TransactionDocumentModel> Docu = context.TransactionDocuments.Where(a => a.TransactionID == datatransaction.ID && a.IsDeleted == false).Select(x => new TransactionDocumentModel()
                    {
                        ID = x.TransactionDocumentID,
                        Type = new DocumentTypeModel()
                        {
                            ID = x.DocumentType.DocTypeID,
                            Name = x.DocumentType.DocTypeName,
                            Description = x.DocumentType.DocTypeDescription,
                            LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                            LastModifiedDate = x.DocumentType.UpdateDate ?? x.DocumentType.CreateDate
                        },
                        Purpose = new DocumentPurposeModel()
                        {
                            ID = x.DocumentPurpose.PurposeID,
                            Name = x.DocumentPurpose.PurposeName,
                            Description = x.DocumentPurpose.PurposeDescription,
                            LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                            LastModifiedDate = x.DocumentPurpose.UpdateDate ?? x.DocumentPurpose.CreateDate
                        },
                        FileName = x.Filename,
                        DocumentPath = x.DocumentPath,
                        LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                        LastModifiedDate = x.UpdateDate ?? x.CreateDate,
                        IsDormant = x.IsDormant == null ? false : x.IsDormant
                    }).ToList();

                    if (Docu.Count > 0)
                    {
                        datatransaction.Documents = Docu;
                    }
                    #endregion

                    //string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(instanceID)).Select(a => a.Customer.CIF).FirstOrDefault();                    

                    #region GetCustomerByTransaction
                    if (customerRepo.GetCustomerByTransaction(datatransaction.ID, ref customer, ref message))
                    {
                        datatransaction.Customer = customer;
                    }

                    /*var data = context.RetailCIFCBOes.Where(x => x.TransactionID.Equals(datatransaction.ID)).Select(x => x).FirstOrDefault();

                    if (data != null)
                    //end
                    {
                        if (string.IsNullOrEmpty(data.AccountNumber))
                        {
                            CustomerAccountModel Account = new CustomerAccountModel();
                            datatransaction.Account = Account;

                            CurrencyModel AccountCurrency = new CurrencyModel();
                            datatransaction.DebitCurrency = AccountCurrency;
                        }
                        else
                        {
                            CustomerAccountModel Account = new CustomerAccountModel();
                            Account.AccountNumber = data.AccountNumber;
                            datatransaction.Account = Account;

                        }
                    }
                    else
                    {
                        CustomerAccountModel Account = new CustomerAccountModel();
                        datatransaction.Account = Account;

                        CurrencyModel AccountCurrency = new CurrencyModel();
                        datatransaction.DebitCurrency = AccountCurrency;
                    }

                    if (datatransaction.AccountNumber != null)
                        datatransaction.Currency.Code = context.CustomerAccounts.Where(x => x.AccountNumber.Equals(datatransaction.AccountNumber)).Select(x => x.Currency.CurrencyCode).FirstOrDefault();
                    */
                    customerRepo.Dispose();
                    customer = null;
                    #endregion

                    if (IDTrns != null)
                    {
                        #region GetRetailCIFCBO
                        DetailSPRCCBOResult r = (from cifcbo in context.SP_GetRetailCIFCBO(IDTrns) select cifcbo).SingleOrDefault();
                        #region if r!=null
                        if (r != null)
                        {
                            rcc.AccountNumber = r.AccountNumber;
                            rcc.AccountPurpose = r.AccountPurpose;
                            rcc.AtmNumber = r.AtmNumber;
                            rcc.CellPhone = r.CellPhone;
                            rcc.CellPhoneMethodID = r.CellPhoneMethodID;
                            rcc.CompanyName = r.CompanyName;
                            rcc.CorrespondenseAddress = r.CorrespondenseAddress;
                            rcc.CorrespondenseCity = r.CorrespondenseCity;
                            rcc.CorrespondenseCountry = r.CorrespondenseCountry;
                            rcc.CorrespondenseKecamatan = r.CorrespondenseKecamatan;
                            rcc.CorrespondenseKelurahan = r.CorrespondenseKelurahan;
                            rcc.CorrespondensePostalCode = r.CorrespondensePostalCode;
                            rcc.CorrespondenseProvince = r.CorrespondenseProvince;
                            rcc.CreateBy = r.CreateBy;
                            rcc.CreateDate = r.CreateDate;
                            rcc.DispatchModeTypeID = r.DispatchModeTypeID;
                            rcc.DispatchModeOther = r.DispatchModeOther;
                            rcc.EmailAddress = r.EmailAddress;
                            rcc.Fax = r.Fax;
                            rcc.FaxMethodID = r.FaxMethodID;
                            rcc.FundSource = r.FundSource;
                            rcc.FundSources = r.FundSources;
                            rcc.HomePhone = r.HomePhone;
                            rcc.HomePhoneMethodID = r.HomePhoneMethodID;
                            rcc.IdentityAddress = r.IdentityAddress;
                            rcc.IdentityCity = r.IdentityCity;
                            rcc.IdentityCountry = r.IdentityCountry;
                            rcc.IdentityEndDate = r.IdentityEndDate;
                            rcc.IdentityKecamatan = r.IdentityKecamatan;
                            rcc.IdentityKelurahan = r.IdentityKelurahan;
                            rcc.IdentityNumber = r.IdentityNumber;
                            rcc.IdentityPostalCode = r.IdentityPostalCode;
                            rcc.IdentityProvince = r.IdentityProvince;
                            rcc.IdentityStartDate = r.IdentityStartDate;
                            rcc.IdentityType = r.IdentityType;
                            rcc.IdentityTypeID = r.IdentityTypeID;
                            rcc.IdentityAddress2 = r.IdentityAddress2;
                            rcc.IdentityCity2 = r.IdentityCity2;
                            rcc.IdentityCountry2 = r.IdentityCountry2;
                            rcc.IdentityEndDate2 = r.IdentityEndDate2;
                            rcc.IdentityKecamatan2 = r.IdentityKecamatan2;
                            rcc.IdentityKelurahan2 = r.IdentityKelurahan2;
                            rcc.IdentityNumber2 = r.IdentityNumber2;
                            rcc.IdentityPostalCode2 = r.IdentityPostalCode2;
                            rcc.IdentityProvince2 = r.IdentityProvince2;
                            rcc.IdentityStartDate2 = r.IdentityStartDate2;
                            rcc.IdentityType2 = r.IdentityType2;
                            rcc.IdentityTypeID2 = r.IdentityTypeID2;
                            rcc.IdentityAddress3 = r.IdentityAddress3;
                            rcc.IdentityCity3 = r.IdentityCity3;
                            rcc.IdentityCountry3 = r.IdentityCountry3;
                            rcc.IdentityEndDate3 = r.IdentityEndDate3;
                            rcc.IdentityKecamatan3 = r.IdentityKecamatan3;
                            rcc.IdentityKelurahan3 = r.IdentityKelurahan3;
                            rcc.IdentityNumber3 = r.IdentityNumber3;
                            rcc.IdentityPostalCode3 = r.IdentityPostalCode3;
                            rcc.IdentityProvince3 = r.IdentityProvince3;
                            rcc.IdentityStartDate3 = r.IdentityStartDate3;
                            rcc.IdentityType3 = r.IdentityType3;
                            rcc.IdentityTypeID3 = r.IdentityTypeID3;
                            rcc.IdentityAddress4 = r.IdentityAddress4;
                            rcc.IdentityCity4 = r.IdentityCity4;
                            rcc.IdentityCountry4 = r.IdentityCountry4;
                            rcc.IdentityEndDate4 = r.IdentityEndDate4;
                            rcc.IdentityKecamatan4 = r.IdentityKecamatan4;
                            rcc.IdentityKelurahan4 = r.IdentityKelurahan4;
                            rcc.IdentityNumber4 = r.IdentityNumber4;
                            rcc.IdentityPostalCode4 = r.IdentityPostalCode4;
                            rcc.IdentityProvince4 = r.IdentityProvince4;
                            rcc.IdentityStartDate4 = r.IdentityStartDate4;
                            rcc.IdentityType4 = r.IdentityType4;
                            rcc.IdentityTypeID4 = r.IdentityTypeID4;
                            rcc.IncomeForecast = r.IncomeForecast;

                            rcc.IndustryType = r.IndustryType;
                            rcc.IsAccountPurposeMaintenance = r.IsAccountPurposeMaintenance;
                            rcc.IsCorrespondenceMaintenance = r.IsCorrespondenceMaintenance;
                            rcc.IsCorrespondenseAddressMaintenance = r.IsCorrespondenseAddressMaintenance;
                            rcc.IsCorrespondenseToEmail = r.IsCorrespondenseToEmail;
                            rcc.IsFundSourceMaintenance = r.IsFundSourceMaintenance;
                            rcc.IsIdentityAddressMaintenance = r.IsIdentityAddressMaintenance;
                            rcc.IsIdentityTypeMaintenance = r.IsIdentityTypeMaintenance;
                            rcc.IsJobMaintenance = r.IsJobMaintenance;
                            rcc.IsMaritalStatusMaintenance = r.IsMaritalStatusMaintenance;
                            rcc.IsMonthlyIncomeMaintenance = r.IsMonthlyIncomeMaintenance;
                            rcc.IsMonthlyTransactionMaintenance = r.IsMonthlyTransactionMaintenance;
                            rcc.IsNameMaintenance = r.IsNameMaintenance;
                            rcc.IsNationalityMaintenance = r.IsNationalityMaintenance;
                            rcc.IsNetAssetMaintenance = r.IsNetAssetMaintenance;
                            rcc.IsNPWPMaintenance = r.IsNPWPMaintenance;
                            rcc.IsNPWPReceived = r.IsNPWPReceived;
                            rcc.IsOfficeAddressMaintenance = r.IsOfficeAddressMaintenance;
                            rcc.IsPekerjaanLainnya = r.IsPekerjaanLainnya;
                            rcc.IsPekerjaanProfesional = r.IsPekerjaanProfesional;
                            rcc.IsPhoneFaxEmailMaintenance = r.IsPhoneFaxEmailMaintenance;
                            rcc.IsSumberDanaLainnya = r.IsSumberDanaLainnya;
                            rcc.IsTujuanBukaRekeningLainnya = r.IsTujuanBukaRekeningLainnya;
                            rcc.Job = r.Job;
                            rcc.MaintenanceTypeID = r.MaintenanceTypeID;
                            rcc.MaritalStatusID = r.MaritalStatusID;
                            rcc.MonthlyExtraIncome = r.MonthlyExtraIncome;
                            rcc.Name = r.Name;
                            rcc.Nationality = r.Nationality;
                            rcc.NetAsset = r.NetAsset;
                            rcc.NPWPNumber = r.NPWPNumber;
                            rcc.OfficeAddress = r.OfficeAddress;
                            rcc.OfficeCity = r.OfficeCity;
                            rcc.OfficeCountry = r.OfficeCountry;
                            rcc.OfficeKecamatan = r.OfficeKecamatan;
                            rcc.OfficeKelurahan = r.OfficeKelurahan;
                            rcc.OfficePhone = r.OfficePhone;
                            rcc.OfficePhoneMethodID = r.OfficePhoneMethodID;
                            rcc.OfficePostalCode = r.OfficePostalCode;
                            rcc.OfficeProvince = r.OfficeProvince;
                            rcc.OutcomeForecast = r.OutcomeForecast;
                            rcc.PekerjaanLainnya = r.PekerjaanLainnya;
                            rcc.PekerjaanProfesional = r.PekerjaanProfesional;
                            rcc.Position = r.Position;
                            rcc.Remarks = r.Remarks;
                            rcc.RequestTypeID = r.RequestTypeID;
                            rcc.RetailCIFCBOID = r.RetailCIFCBOID;
                            rcc.RiskRatingNextReviewDate = r.RiskRatingNextReviewDate;
                            rcc.RiskRatingReportDate = r.RiskRatingReportDate;
                            rcc.RiskRatingResultID = r.RiskRatingResultID;
                            rcc.SpouseName = r.SpouseName;
                            rcc.SubType = r.SubType;
                            rcc.AccountPurposes = r.AccountPurposes;
                            rcc.Jobs = r.Jobs;
                            rcc.IncomeForecasts = r.IncomeForecasts;
                            rcc.MaintenanceTypes = r.MaintenanceTypes;
                            rcc.MonthlyExtraIncomes = r.MonthlyExtraIncomes;
                            rcc.MonthlyIncome = r.MonthlyIncome;
                            rcc.MonthlyIncomes = r.MonthlyIncomes;
                            rcc.NetAssets = r.NetAssets;
                            rcc.OutcomeForecasts = r.OutcomeForecasts;
                            rcc.RequestTypes = r.RequestTypes;
                            rcc.SubTypes = r.SubTypes;
                            rcc.TransactionForecasts = r.TransactionForecasts;
                            rcc.SumberDanaLainnya = r.SumberDanaLainnya;
                            rcc.TransactionForecast = r.TransactionForecast;
                            rcc.TransactionID = r.TransactionID;
                            rcc.TujuanBukaRekeningLainnya = r.TujuanBukaRekeningLainnya;
                            rcc.UBOIdentityType = r.UBOIdentityType;
                            rcc.UBOJob = r.UBOJob;
                            rcc.UBOName = r.UBOName;
                            rcc.UBOPhone = r.UBOPhone;
                            rcc.UpdateBy = r.UpdateBy;
                            rcc.UpdateDate = r.UpdateDate;
                            rcc.UpdatedCellPhone = r.UpdatedCellPhone;
                            rcc.UpdatedFax = r.UpdatedFax;
                            rcc.UpdatedHomePhone = r.UpdatedHomePhone;
                            rcc.UpdatedOfficePhone = r.UpdatedOfficePhone;
                            rcc.WorkPeriod = r.WorkPeriod;
                            rcc.IsStampDuty = r.IsStampDuty;
                            rcc.IsPenawaranProductPerbankan = r.IsPenawaranProductPerbankan;
                            rcc.IsSignature = r.IsSignature;
                            rcc.IsOthers = r.IsOthers;
                            //add henggar
                            rcc.IsMotherMaiden = r.IsMotherMaiden;
                            rcc.MotherMaiden = r.MotherMaiden;
                            rcc.IsReligion = r.IsReligion;
                            rcc.Religion = r.Religion;
                            rcc.IsEducation = r.IsEducation;
                            rcc.Education = r.Education;
                            rcc.IsChangeSignature = r.IsChangeSignature;
                            rcc.ChangeSignature = r.ChangeSignature;
                            rcc.IsDataUBO = r.IsDataUBO;
                            rcc.DataUBOCif = r.CIFUBO;
                            rcc.UBOName = r.UBOName;
                            rcc.DataUBORelationship = r.UBORelationship;
                            rcc.IsTransparansiDataPenawaranProduct = r.IsTransparansiDataPenawaranProduct;
                            rcc.IsTransparansiPenggunaan = r.IsTransparansiPenggunaan;
                            rcc.IsPenawaranProduct = r.IsPenawaranProduct;
                            rcc.IsSLIK = r.IsSLIK;
                            rcc.GrosIncomeccy = r.GrosIncomeccy;
                            rcc.GrossCifSpouse = r.GrossCifSpouse;
                            rcc.GrossIncomeAmount = r.GrossIncomeAmount;
                            rcc.GrossSpouseName = r.GrossSpouseName;
                            rcc.GrossSpouseRelation = r.GrossSpouseRelation;
                            rcc.IsFatcaCRS = r.IsFatcaCRS;
                            rcc.FatcaCountryCode = r.FatcaCountryCode;
                            rcc.FatcaCRSStatus = r.FatcaCRSStatus;
                            rcc.FatcaDateOnForm = r.FatcaDateOnForm;
                            rcc.FatcaReviewStatus = r.FatcaReviewStatus;
                            rcc.FatcaReviewStatusDate = r.FatcaReviewStatusDate;
                            rcc.FatcaTaxPayer = r.FatcaTaxPayer;
                            rcc.FatcaTaxPayerID = r.FatcaTaxPayerID;
                            rcc.FatcaWithHolding = r.FatcaWithHolding;
                            rcc.IDS = r.IDS;
                            rcc.CallbackIsRequired = r.CallbackIsRequired;
                            rcc.UtcApproval = r.UtcApproval;
                            rcc.Tier = r.Tier;
                            rcc.AmountLienUnlien = r.AmountLienUnlien;

                            #region dayat getContactCBO
                            if (r.RetailCIFCBOID > 0)
                            {
                                List<RetailCIFCBOContact> GetRCCBOContact = context.RetailCIFCBOContacts.Where(a => a.RetailCIFCBOID == r.RetailCIFCBOID).ToList();
                                #region cbo contact
                                if (GetRCCBOContact != null && GetRCCBOContact.Count > 0)
                                {
                                    foreach (var item in GetRCCBOContact)
                                    {
                                        RCCBOContactModel RCCBOC = new RCCBOContactModel();
                                        RCCBOC.RCCBOContactID = item.RetailCIFCBOContactID;
                                        RCCBOC.ActionType = item.ActionType;
                                        RCCBOC.CountryCode = item.CountryCode;
                                        RCCBOC.CityCode = item.CityCode;
                                        RCCBOC.ContactType = item.ContactType;
                                        RCCBOC.PhoneNumber = item.PhoneNumber;
                                        RCCBOC.UpdateCountryCode = null;
                                        RCCBOC.UpdateCityCode = null;
                                        RCCBOC.UpdatePhoneNumber = null;
                                        RCCBOC.RetailCIFCBOContactUpdateID = null;
                                        if (item.ActionType == "2")
                                        {
                                            RCCBOC.UpdateCountryCode = item.UpdateCountryCode;
                                            RCCBOC.UpdateCityCode = item.UpdateCityCode;
                                            RCCBOC.UpdatePhoneNumber = item.UpdatePhoneNumber;
                                            RCCBOC.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactID;
                                        }
                                        if (item.ActionType == "3")
                                        {
                                            RCCBOC.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactID;
                                        }
                                        if (item.ContactType.Trim().ToLower() == "selular")
                                        {
                                            datatransaction.SelularPhoneNumbers.Add(RCCBOC);
                                        }
                                        else if (item.ContactType.Trim().ToLower() == "office")
                                        {
                                            datatransaction.OfficePhoneNumbers.Add(RCCBOC);

                                        }
                                        else if (item.ContactType.Trim().ToLower() == "home")
                                        {
                                            datatransaction.HomePhoneNumbers.Add(RCCBOC);
                                        }
                                    }
                                }
                                #endregion
                            }
                            #endregion
                        }
                        #endregion

                        if (rcc != null)
                        {
                            if (string.IsNullOrEmpty(rcc.AccountNumber))
                            {
                                CustomerAccountModel Account = new CustomerAccountModel();
                                datatransaction.Account = Account;

                                CurrencyModel AccountCurrency = new CurrencyModel();
                                datatransaction.DebitCurrency = AccountCurrency;
                            }
                            else
                            {
                                CustomerAccountModel Account = new CustomerAccountModel();
                                Account.AccountNumber = rcc.AccountNumber;
                                datatransaction.Account = Account;
                            }

                            if (rcc.GrosIncomeccy != 0 && rcc.GrosIncomeccy != null)
                            {
                                datatransaction.Currency = context.Currencies.Where(a => a.CurrencyID.Equals(r.GrosIncomeccy.Value))
                                    .Select(a => new CurrencyModel()
                                    {
                                        ID = a.CurrencyID,
                                        Code = a.CurrencyCode,
                                        Description = a.CurrencyDescription
                                    }).SingleOrDefault();
                            }

                            if (rcc.FatcaCountryCode != 0 && rcc.FatcaCountryCode != null)
                            {
                                datatransaction.BeneficiaryCountry = context.BeneficiaryCountries.Where(a => a.BeneficiaryCountryID.Equals(r.FatcaCountryCode.Value))
                                    .Select(a => new BeneficiaryCountryModel()
                                    {
                                        ID = a.BeneficiaryCountryID,
                                        Code = a.BeneficiaryCountryCode,
                                        Description = a.Description
                                    }).SingleOrDefault();
                            }
                        }
                        else
                        {
                            CustomerAccountModel Account = new CustomerAccountModel();
                            datatransaction.Account = Account;

                            CurrencyModel AccountCurrency = new CurrencyModel();
                            datatransaction.DebitCurrency = AccountCurrency;
                        }

                        if (datatransaction.AccountNumber != null)
                            datatransaction.Currency.Code = context.CustomerAccounts.Where(x => x.AccountNumber.Equals(datatransaction.AccountNumber)).Select(x => x.Currency.CurrencyCode).FirstOrDefault();

                        if (rcc != null)
                        {
                            datatransaction.RetailCIFCBO = rcc;
                        }
                        else
                        {
                            //add by fandi for changerm
                            RetailCIFCBO Retail = new RetailCIFCBO();
                            if (datatransaction.TransactionType != null && datatransaction.TransactionType.ID > 0)
                            {
                                Retail.RequestTypes = context.TransactionTypes.Where(x => x.TransTypeID == datatransaction.TransactionType.ID).Select(x => x.TransactionType1).FirstOrDefault();
                            }
                            else
                                Retail.RequestTypes = "";

                            if (datatransaction.MaintenanceType != null && datatransaction.MaintenanceType.ID > 0)
                                Retail.MaintenanceTypes = context.MaintenanceTypes.Where(x => x.CBOMaintainID == datatransaction.MaintenanceType.ID).Select(x => x.MaintenanceTypeName).FirstOrDefault();
                            else
                                Retail.MaintenanceTypes = "";
                            datatransaction.RetailCIFCBO = Retail;
                            //end
                        }
                        #endregion
                        #region GetChangeRM
                        datatransaction.ChangeRMModel = new List<ChangeRMModel>();
                        List<ChangeRMTransaction> ChangeRMVal = context.ChangeRMTransactions.Where(y => y.TransactionID == datatransaction.ID).ToList();
                        if (ChangeRMVal != null && ChangeRMVal.Count > 0)
                        {
                            RetailCIFCBO Retail = new RetailCIFCBO();
                            if (datatransaction.TransactionType != null && datatransaction.TransactionType.ID != null)
                            {
                                Retail.RequestTypes = context.TransactionTypes.Where(x => x.TransTypeID == datatransaction.TransactionType.ID).Select(x => x.TransactionType1).FirstOrDefault();
                            }
                            else
                                Retail.RequestTypes = "";

                            if (datatransaction.MaintenanceType != null && datatransaction.MaintenanceType.ID > 0)
                                Retail.MaintenanceTypes = context.MaintenanceTypes.Where(x => x.CBOMaintainID == datatransaction.MaintenanceType.ID).Select(x => x.MaintenanceTypeName).FirstOrDefault();
                            else
                                Retail.MaintenanceTypes = "";
                            datatransaction.RetailCIFCBO = Retail;

                            foreach (var item in ChangeRMVal)
                            {
                                ChangeRMModel Changeval = new ChangeRMModel();
                                Changeval.Selected = true;
                                Changeval.Account = item.Account;
                                Changeval.CustomerName = item.Customer.CustomerName;
                                Changeval.ChangeFromBranchCode = item.ChangeFromBranchCode;
                                Changeval.ChangeFromBU = item.ChangeFromBU;
                                Changeval.ChangeFromRMCode = item.ChangeFromRMCode;
                                Changeval.ChangeRMTransactionID = item.ChangeRMTransactionID;
                                Changeval.ChangeSegmentFrom = item.ChangeSegmentFrom;
                                Changeval.ChangeSegmentTo = item.ChangeSegmentTo;
                                Changeval.ChangeToBranchCode = item.ChangeToBranchCode;
                                Changeval.ChangeToBU = item.ChangeToBU;
                                Changeval.ChangeToRMCode = item.ChangeToRMCode;
                                Changeval.CIF = item.CIF;
                                Changeval.PCCode = item.PCCode;
                                Changeval.Remarks = item.Remarks;
                                Changeval.EATPB = item.EATPB;
                                Changeval.CreateBy = item.CreateBy;
                                Changeval.CreateDate = item.CreateDate;
                                //add 
                                datatransaction.ChangeRMModel.Add(Changeval);
                            }

                        }
                        #endregion
                        #region AddJointCustomerCIF
                        datatransaction.AddJoinTableCustomerCIF = new List<CustomerModalModel>();
                        List<RetailCIFCBOAccountJoin> AddJointCustomerCIF = context.RetailCIFCBOAccountJoins.Where(y => y.TransactionID == datatransaction.ID).ToList();
                        if (AddJointCustomerCIF != null && AddJointCustomerCIF.Count > 0)
                        {
                            foreach (var item in AddJointCustomerCIF)
                            {
                                CustomerModalModel CustomerJoin = new CustomerModalModel();
                                CustomerJoin.CIF = item.CIF;
                                CustomerJoin.Name = item.Customer.CustomerName;
                                datatransaction.AddJoinTableCustomerCIF.Add(CustomerJoin);
                            }
                        }
                        #endregion
                        #region AddJointTableFFDAccount
                        datatransaction.AddJoinTableFFDAccountCIF = new List<FFDAccountModalModel>();
                        long RetailCIFCBOID = context.RetailCIFCBOes.Where(y => y.TransactionID == datatransaction.ID).Select(y => y.RetailCIFCBOID).FirstOrDefault();
                        List<RetailCIFCBOAccNo> AddJoinTableFFDAccountCIF = context.RetailCIFCBOAccNoes.Where(y => y.RetailCIFCBOID == RetailCIFCBOID).ToList(); //&& y.IsFFD.Value == true).ToList();
                        if (AddJoinTableFFDAccountCIF != null && AddJoinTableFFDAccountCIF.Count > 0)
                        {
                            foreach (var item in AddJoinTableFFDAccountCIF)
                            {
                                FFDAccountModalModel FFDAccountJoin = new FFDAccountModalModel();
                                #region Old Code
                                /*FFDAccountJoin.AccountNumber = item.AccountNumber;
                                FFDAccountJoin.Currency = item.Currency != null ? new CurrencyModalModel()
                                {
                                    ID = item.Currency.CurrencyID,
                                    Code = item.Currency.CurrencyCode,
                                    Description = item.Currency.CurrencyDescription

                                } : new CurrencyModalModel()
                                {
                                    ID = 0,
                                    Code = "",
                                    Description = ""
                                };                                
                                FFDAccountJoin.IsAddFFDAccount = item.IsFFD;*/
                                #endregion
                                FFDAccountJoin.AccountNumber = item.AccountNumber;
                                FFDAccountJoin.IsAddFFDAccount = item.IsFFD;
                                datatransaction.AddJoinTableFFDAccountCIF.Add(FFDAccountJoin);
                            }

                        }
                        #endregion
                        #region AddJoinTableAccount
                        datatransaction.AddJoinTableAccountCIF = new List<FFDAccountModalModel>();
                        List<RetailCIFCBOAccNo> AddJoinTableAccountCIF = context.RetailCIFCBOAccNoes.Where(y => y.RetailCIFCBOID == RetailCIFCBOID && (y.IsFFD.Value == null || y.IsFFD.Value == false)).ToList();
                        if (AddJoinTableAccountCIF != null && AddJoinTableAccountCIF.Count > 0)
                        {
                            foreach (var item in AddJoinTableAccountCIF)
                            {
                                FFDAccountModalModel AccountJoin = new FFDAccountModalModel();
                                AccountJoin.AccountNumber = item.AccountNumber;
                                AccountJoin.Currency = item.Currency != null ? new CurrencyModalModel()
                                {
                                    ID = item.Currency.CurrencyID,
                                    Code = item.Currency.CurrencyCode,
                                    Description = item.Currency.CurrencyDescription

                                } : new CurrencyModalModel()
                                {
                                    ID = 13,
                                    Code = "",
                                    Description = ""
                                };
                                AccountJoin.IsAddFFDAccount = item.IsFFD;
                                #region Old Code
                                /*AccountJoin.AccountNumber = item.AccountNumber;
                                AccountJoin.IsAddFFDAccount = item.IsFFD;*/
                                #endregion
                                datatransaction.AddJoinTableAccountCIF.Add(AccountJoin);
                            }
                        }
                        #endregion
                        #region AddJoinTableDorman
                        datatransaction.AddJoinTableDormantCIF = new List<DormantAccountModel>();
                        List<RetailCIFCBOAccNo> AddJoinTableDormantCIF = context.RetailCIFCBOAccNoes.Where(y => y.RetailCIFCBOID == RetailCIFCBOID && (y.IsFFD == null || y.IsFFD.Value == false)).ToList();
                        if (AddJoinTableDormantCIF != null && AddJoinTableDormantCIF.Count > 0)
                        {
                            foreach (var item in AddJoinTableDormantCIF)
                            {
                                DormantAccountModel DormantCIF = new DormantAccountModel();
                                DormantCIF.AccountNumber = item.AccountNumber;
                                DormantCIF.Currency = item.Currency != null ? new CurrencyModalModel()
                                {
                                    ID = item.Currency.CurrencyID,
                                    Code = item.Currency.CurrencyCode,
                                    Description = item.Currency.CurrencyDescription
                                } : new CurrencyModalModel()
                                {
                                    ID = 13,
                                    Code = "",
                                    Description = ""
                                };
                                datatransaction.AddJoinTableDormantCIF.Add(DormantCIF);
                            }
                        }
                        #endregion
                        #region AddJoinTableFreezeUnfreeze
                        datatransaction.AddJoinTableFreezeUnfreezeCIF = new List<FreezeUnfreezeModel>();
                        List<RetailCIFCBOFreezeAccount> AddJoinTableFreezeUnfreezeCIF = context.RetailCIFCBOFreezeAccounts.Where(y => y.RetailCIFCBOID.Value == RetailCIFCBOID).ToList();
                        if (AddJoinTableFreezeUnfreezeCIF != null && AddJoinTableFreezeUnfreezeCIF.Count > 0)
                        {
                            foreach (var item in AddJoinTableFreezeUnfreezeCIF)
                            {
                                int CurrencyID = context.CustomerAccounts.Where(y => y.AccountNumber == item.AccountNumber).Select(y => y.CurrencyID).FirstOrDefault();
                                string CurrencyCode = context.Currencies.Where(c => c.CurrencyID == CurrencyID).Select(c => c.CurrencyCode).FirstOrDefault();

                                FreezeUnfreezeModel FreezeUnfreezeCIF = new FreezeUnfreezeModel();
                                FreezeUnfreezeCIF.AccountNumber = item.AccountNumber;
                                FreezeUnfreezeCIF.CurrencyCode = CurrencyCode;
                                FreezeUnfreezeCIF.IsFreezeAccount = item.IsFreeze;
                                datatransaction.AddJoinTableFreezeUnfreezeCIF.Add(FreezeUnfreezeCIF);
                            }
                        }
                        #endregion
                        #region GetDataFromLienUnlien
                        datatransaction.AccountNumberLienUnlien = new List<AccountNumberLienUnlienModel>();
                        if ((!string.IsNullOrEmpty(datatransaction.Customer.CIF)) && (datatransaction.Customer.Accounts.Count > 0))
                        {
                            IList<CustomerAccountModel> AccountNumberArray = datatransaction.Customer.Accounts;
                            IList<Entity.LienUnlien> lienUnlienDatas = context.LienUnliens.Where(a => a.CIF.Trim() == datatransaction.Customer.CIF.Trim()).Where(b => b.TransactionId == datatransaction.ID).Where(c => c.IsDeleted.Value == false).ToList();
                            IList<AccountNumberLienUnlienModel> arrayA = new List<AccountNumberLienUnlienModel>();
                            if (AccountNumberArray != null)
                            {
                                if (AccountNumberArray.Count > 0)
                                {
                                    foreach (var item in AccountNumberArray)
                                    {
                                        AccountNumberLienUnlienModel itemDataLienUnlien = new AccountNumberLienUnlienModel();
                                        itemDataLienUnlien.AccountNumber = item.AccountNumber;
                                        itemDataLienUnlien.LienUnlien = "";
                                        itemDataLienUnlien.IsSelected = false;
                                        arrayA.Add(itemDataLienUnlien);
                                    }
                                }
                                if (lienUnlienDatas != null)
                                {
                                    if (AccountNumberArray.Count > 0 && lienUnlienDatas.Count > 0)
                                    {
                                        foreach (var satuan in arrayA)
                                        {
                                            foreach (var lien in lienUnlienDatas)
                                            {
                                                if (satuan.AccountNumber == lien.AccountNumber)
                                                {
                                                    satuan.IsSelected = true;
                                                    satuan.LienUnlien = lien.LienUnlienData;
                                                    satuan.IsBranchCheckerVerify = lien.IsBranchCheckerVerify ?? false;
                                                    satuan.IsCBOAccountCheckerVerify = lien.IsCBOAccountCheckerVerify ?? false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (arrayA != null && arrayA.Count > 0)
                            {
                                datatransaction.AccountNumberLienUnlien = arrayA;
                            }
                        }
                        #endregion
                    }
                    #region CheckerData
                    var checker = context.TransactionCheckerDatas.Where(x => x.TransactionID.Equals(IDTrns)).Select(x => x).OrderByDescending(b => b.ApproverID).FirstOrDefault();
                    if (checker != null)
                    {

                        datatransaction.Others = checker.Others;
                        datatransaction.IsSignatureVerified = checker.IsSignatureVerified;
                        datatransaction.IsDocumentComplete = datatransaction.IsDocumentComplete;
                        datatransaction.IsCallbackRequired = checker.IsCallbackRequired;
                        datatransaction.IsLOIAvailable = checker.IsLOIAvailable;
                        datatransaction.IsDormantAccount = checker.IsDormantAccount;
                        datatransaction.IsFreezeAccount = checker.IsFreezeAccount;
                    }
                    #endregion
                }

                transaction = datatransaction;

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineModel> timelines, ref string message)
        {
            bool IsSuccess = false;

            try
            {

                timelines = (from a in context.SP_GetNintexTaskTimeline(workflowInstanceID)
                             select new TransactionTimelineModel
                             {
                                 ApproverID = a.WorkflowApproverID,
                                 Activity = a.ActivityTitle,
                                 Time = a.ActivityTime,
                                 UserOrGroup = a.UserOrGroup,
                                 IsGroup = a.IsSPGroup.Value,
                                 Outcome = a.Outcome,
                                 DisplayName = a.DisplayName,
                                 Comment = a.Comment
                             }).ToList();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionContactDetails(Guid workflowInstanceID, long approverID, ref IList<CustomerContactModel> output, ref string message)
        {
            bool isSuccess = false;

            try
            {
                output = (from a in context.Transactions.AsNoTracking()
                          join b in context.TransactionContacts on a.CIF equals b.CIF
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          && b.IsDeleted.Equals(false)
                          select new CustomerContactModel
                          {
                              ID = b.ContactID,
                              Name = b.ContactName,
                              PhoneNumber = b.PhoneNumber,
                              DateOfBirth = b.DateOfBirth.HasValue ? b.DateOfBirth : null,
                              Address = b.Address,
                              IDNumber = b.IdentificationNumber,
                              POAFunction = b.POAFunction != null ? new POAFunctionModel()
                              {
                                  ID = b.POAFunction.POAFunctionID,
                                  Name = b.POAFunction.POAFunctionName,
                                  Description = (b.POAFunctionID == 9 ? b.POAFunctionOther : b.POAFunction.POAFunctionDescription),
                                  LastModifiedBy = b.UpdateDate == null ? b.CreateBy : b.UpdateBy,
                                  LastModifiedDate = b.UpdateDate == null ? b.CreateDate : b.UpdateDate.Value
                              } : new POAFunctionModel()
                              {
                                  ID = 0,
                                  Name = "",
                                  Description = "",
                                  LastModifiedBy = b.POAFunction.CreateBy,
                                  LastModifiedDate = b.POAFunction.CreateDate
                              },
                              OccupationInID = b.OccupationInID,
                              PlaceOfBirth = b.PlaceOfBirth,
                              EffectiveDate = b.EffectiveDate.HasValue ? b.EffectiveDate : null,
                              CancellationDate = b.CancellationDate.HasValue ? b.CancellationDate : null,
                              POAFunctionOther = b.POAFunctionOther,
                              LastModifiedBy = b.UpdateDate == null ? b.CreateBy : b.UpdateBy,
                              LastModifiedDate = b.UpdateDate == null ? b.CreateDate : b.UpdateDate.Value
                          }).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetTransactionCheckerDetails(Guid workflowInstanceID, long approverID, ref TransactionCBOContactDetailModel output, ref string message)
        {
            bool IsSuccess = false;

            var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

            try
            {
                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new TransactionCBOContactDetailModel
                          {
                              Verify = a.TransactionCheckers
                                .Where(x => x.TransactionID.Equals(transactionID) && x.IsDeleted == false)
                                .Select(x => new VerifyModel()
                                {
                                    ID = x.TransactionColumnID,
                                    Name = x.TransactionColumn.ColumnName,
                                    IsVerified = x.IsVerified
                                }).ToList()
                          }).SingleOrDefault();

                // replace output while Checker & Verify is empty
                if (output != null)
                {
                    // replace Checker while empty
                    if (output.Checker == null)
                    {
                        var trans = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).SingleOrDefault();

                        output.Checker = new TransactionCheckerDataModel()
                        {
                            ID = 0,
                            Others = trans.Others,

                        };
                    }

                    // replace verify list while empty
                    if (!output.Verify.Any())
                    {
                        output.Verify = context.TransactionColumns
                            .Where(x => x.IsBranchCheckerRetailCIF == true && x.IsDeleted == false)
                            .Select(x => new VerifyModel()
                            {
                                ID = x.TransactionColumnID,
                                Name = x.ColumnName,
                                IsVerified = false
                            })
                            .ToList();
                    }
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        public bool GetTransactionCBOCheckerDetails(Guid workflowInstanceID, long approverID, ref TransactionCBOContactDetailModel output, ref string message)
        {
            bool isSuccess = false;
            var transactionID = context.Transactions
                     .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                     .Select(a => a.TransactionID)
                     .SingleOrDefault();
            try
            {
                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new TransactionCBOContactDetailModel
                          {
                              Verify = a.TransactionRetailCIFCBOCheckers
                                     .Where(x => x.TransactionID.Equals(transactionID) && x.IsDeleted.Equals(false))
                                     .Select(x => new VerifyModel()
                                     {
                                         ID = x.TransactionColumnID,
                                         Name = x.TransactionColumn.ColumnName,
                                         IsVerified = x.IsVerified
                                     }).ToList()
                          }).SingleOrDefault();
                if (output != null)
                {
                    if (output.Checker != null)
                    {
                        var trans = context.Transactions.Where(x => x.WorkflowInstanceID.Value.Equals(workflowInstanceID)).SingleOrDefault();
                        output.Checker = new TransactionCheckerDataModel()
                        {
                            ID = 0,
                            Others = trans.Others
                        };

                    }

                    if (!output.Verify.Any())
                    {
                        output.Verify = context.TransactionColumns
                            .Where(x => x.IsCBOAccountCheckerRetailCIF == true && x.IsDeleted == false)
                            .Select(x => new VerifyModel()
                            {
                                ID = x.TransactionColumnID,
                                Name = x.ColumnName,
                                IsVerified = false
                            }).ToList();
                    }
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return isSuccess;
        }

        //submit after revise for branch maker

        public bool UpdateTransactionForBranchMaker(Guid workflowInstanceID, long approverID, TransactionCBOContactDetailModel transaction, ref string message)
        {
            bool isSuccess = false;
            int? ActivityHistoryIDSelected = 0;
            string CIFSelected = string.Empty;
            string CustomerNameSelected = string.Empty;

            IDictionary<int, string> MaintenanceTypes = new Dictionary<int, string>();
            MaintenanceTypes.Add(0, "Add Currency");
            MaintenanceTypes.Add(1, "Add Account");
            MaintenanceTypes.Add(2, "Link To FFD Account");
            MaintenanceTypes.Add(3, "Activate dormant account");
            MaintenanceTypes.Add(4, "Freeze and Unfreeze Account");

            using (System.Transactions.TransactionScope ts = new System.Transactions.TransactionScope())
            {
                try
                {
                    long RetailCIFCBOID = 0;
                    long TransactionID = 0;
                    context.Transactions.AsNoTracking();

                    if (transaction.Transaction.ChangeRMModel != null && transaction.Transaction.ChangeRMModel.Count > 0)
                    {
                        #region ChangeRM
                        Entity.Transaction data = context.Transactions.Where(d => d.TransactionID.Equals(transaction.Transaction.ID)).SingleOrDefault();
                        if (data != null)
                        {
                            #region Unused field but Not Null
                            data.IsSignatureVerified = false;
                            data.IsDormantAccount = false;
                            data.IsFrezeAccount = false;
                            data.BeneName = "-";
                            data.BankID = 1;
                            data.IsResident = false;
                            data.IsCitizen = false;
                            data.AmountUSD = 0.00M;
                            data.Rate = 0.00M;
                            data.DebitCurrencyID = 1;
                            data.BizSegmentID = 1;
                            data.ChannelID = 1;
                            #endregion
                            data.CIF = transaction.Transaction.Customer.CIF;
                            data.ProductID = transaction.Transaction.Product.ID;
                            data.TransactionID = transaction.Transaction.ID;
                            data.ApplicationID = transaction.Transaction.ApplicationID;
                            data.IsDraft = transaction.Transaction.IsDraft;
                            data.IsNewCustomer = transaction.Transaction.IsNewCustomer;
                            data.ApplicationDate = DateTime.Now;
                            //data.CreateDate = DateTime.UtcNow;
                            //data.CreateBy = currentUser.GetCurrentUser().LoginName;
                            data.CurrencyID = transaction.Transaction.Currency.ID;
                            data.Amount = 0;
                            data.IsTopUrgent = 0;
                            data.IsDocumentComplete = false;
                            data.StateID = (int)StateID.OnProgress;
                            data.StaffTaggingID = (transaction.Transaction.StaffTagging != null && transaction.Transaction.StaffTagging.ID != null) ? transaction.Transaction.StaffTagging.ID : (int?)null;
                            data.StaffID = transaction.Transaction.StaffID;
                            data.ATMNumber = transaction.Transaction.AtmNumber;
                            data.IsLOI = transaction.Transaction.IsLOI;
                            data.IsPOI = transaction.Transaction.IsPOI;
                            data.IsPOA = transaction.Transaction.IsPOA;

                            context.SaveChanges();
                        }
                        #endregion
                    }
                    else
                    {
                        Entity.Transaction data = context.Transactions.Where(d => d.TransactionID.Equals(transaction.Transaction.ID)).SingleOrDefault();
                        if (data != null)
                        {
                            #region Unused field but Not Null
                            data.IsSignatureVerified = false;
                            data.IsDormantAccount = false;
                            data.IsFrezeAccount = false;
                            data.BeneName = "-";
                            data.BankID = 1;
                            data.IsResident = false;
                            data.IsCitizen = false;
                            data.AmountUSD = 0.00M;
                            data.Rate = 0.00M;
                            data.DebitCurrencyID = 1;
                            data.BizSegmentID = 1;
                            data.ChannelID = 1;
                            #endregion
                            data.CIF = transaction.Transaction.Customer.CIF;
                            data.ProductID = transaction.Transaction.Product.ID;
                            data.TransactionID = transaction.Transaction.ID;
                            data.ApplicationID = transaction.Transaction.ApplicationID;
                            data.IsDraft = transaction.Transaction.IsDraft;
                            data.IsNewCustomer = transaction.Transaction.IsNewCustomer;
                            //data.CreateDate = DateTime.UtcNow;
                            //data.CreateBy = currentUser.GetCurrentUser().LoginName;
                            data.CurrencyID = (transaction.Transaction.Currency.ID != null && transaction.Transaction.Currency.ID != 0 ? transaction.Transaction.Currency.ID : 13);
                            data.Amount = 0;
                            data.IsTopUrgent = 0;
                            data.IsDocumentComplete = false;
                            data.StateID = (int)StateID.OnProgress;
                            data.StaffTaggingID = (transaction.Transaction.StaffTagging != null && transaction.Transaction.StaffTagging.ID != null) ? transaction.Transaction.StaffTagging.ID : (int?)null;
                            data.StaffID = transaction.Transaction.StaffID;
                            data.AccountNumber = (transaction.Transaction.AccountNumber == null ? null : transaction.Transaction.AccountNumber);
                            data.IsChangeRM = transaction.Transaction.IsChangeRM;
                            data.IsSegment = transaction.Transaction.IsSegment;
                            data.IsSolID = transaction.Transaction.IsSolID;
                            data.ATMNumber = transaction.Transaction.AtmNumber;
                            data.IsLOI = transaction.Transaction.IsLOI;
                            data.IsPOI = transaction.Transaction.IsPOI;
                            data.IsPOA = transaction.Transaction.IsPOA;
                            context.SaveChanges();
                        }
                    }

                    TransactionID = transaction.Transaction.ID;

                    #region add attachment
                    if (transaction.Transaction.Documents != null)
                    {
                        if (transaction.Transaction.Documents.Count > 0)
                        {
                            var existingDocuments = (from a in context.TransactionDocuments
                                                     where a.TransactionID == TransactionID && a.IsDeleted == false
                                                     select a);
                            if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                            {
                                foreach (var item in existingDocuments.ToList())
                                {
                                    context.TransactionDocuments.Remove(item);
                                    context.SaveChanges();
                                }
                                foreach (var item in transaction.Transaction.Documents)
                                {
                                    Entity.TransactionDocument document = new Entity.TransactionDocument()
                                    {
                                        TransactionID = TransactionID,
                                        DocTypeID = item.Type.ID,
                                        PurposeID = item.Purpose.ID,
                                        Filename = item.FileName,
                                        IsDeleted = false,
                                        DocumentPath = item.DocumentPath,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName
                                    };

                                    context.TransactionDocuments.Add(document);
                                    context.SaveChanges();
                                }
                            }
                            else
                            {
                                foreach (var item in transaction.Transaction.Documents)
                                {
                                    Entity.TransactionDocument document = new Entity.TransactionDocument()
                                    {
                                        TransactionID = TransactionID,
                                        DocTypeID = item.Type.ID,
                                        PurposeID = item.Purpose.ID,
                                        Filename = item.FileName,
                                        IsDeleted = false,
                                        DocumentPath = item.DocumentPath,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName
                                    };
                                    context.TransactionDocuments.Add(document);
                                    context.SaveChanges();
                                }

                            }
                        }
                        else
                        {
                            var existingDocuments = (from a in context.TransactionDocuments
                                                     where a.TransactionID == TransactionID
                                                         && a.IsDeleted == false
                                                     select a);

                            if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                            {
                                foreach (var item in existingDocuments.ToList())
                                {

                                    context.TransactionDocuments.Remove(item);
                                    context.SaveChanges();
                                }
                            }
                        }
                    }
                    #endregion
                    //update retailcif
                    #region RetailCIFCBO
                    Entity.RetailCIFCBO Retail = context.RetailCIFCBOes.Where(r => r.TransactionID.Equals(transaction.Transaction.ID)).SingleOrDefault();
                    if (Retail != null)
                    {
                        Retail.RetailCIFCBOID = transaction.Transaction.RetailCIFCBO.RetailCIFCBOID;
                        Retail.TransactionID = transaction.Transaction.RetailCIFCBO.TransactionID;
                        Retail.DispatchModeTypeID = transaction.Transaction.RetailCIFCBO.DispatchModeTypeID;
                        Retail.AccountNumber = transaction.Transaction.RetailCIFCBO.AccountNumber;
                        Retail.RequestTypeID = transaction.Transaction.RetailCIFCBO.RequestTypeID;
                        Retail.MaintenanceTypeID = transaction.Transaction.RetailCIFCBO.MaintenanceTypeID;
                        Retail.IsNameMaintenance = transaction.Transaction.RetailCIFCBO.IsNameMaintenance;
                        Retail.IsIdentityTypeMaintenance = transaction.Transaction.RetailCIFCBO.IsIdentityTypeMaintenance;
                        Retail.IsNPWPMaintenance = transaction.Transaction.RetailCIFCBO.IsNPWPMaintenance;
                        Retail.IsMaritalStatusMaintenance = transaction.Transaction.RetailCIFCBO.IsMaritalStatusMaintenance;
                        Retail.IsCorrespondenceMaintenance = transaction.Transaction.RetailCIFCBO.IsCorrespondenceMaintenance;
                        Retail.IsIdentityAddressMaintenance = transaction.Transaction.RetailCIFCBO.IsIdentityAddressMaintenance;
                        Retail.IsOfficeAddressMaintenance = transaction.Transaction.RetailCIFCBO.IsOfficeAddressMaintenance;
                        Retail.IsCorrespondenseAddressMaintenance = transaction.Transaction.RetailCIFCBO.IsCorrespondenseAddressMaintenance;
                        Retail.IsPhoneFaxEmailMaintenance = transaction.Transaction.RetailCIFCBO.IsPhoneFaxEmailMaintenance;
                        Retail.IsNationalityMaintenance = transaction.Transaction.RetailCIFCBO.IsNationalityMaintenance;
                        Retail.IsFundSourceMaintenance = transaction.Transaction.RetailCIFCBO.IsFundSourceMaintenance;
                        Retail.IsNetAssetMaintenance = transaction.Transaction.RetailCIFCBO.IsNetAssetMaintenance;
                        Retail.IsMonthlyIncomeMaintenance = transaction.Transaction.RetailCIFCBO.IsMonthlyIncomeMaintenance;
                        Retail.IsJobMaintenance = transaction.Transaction.RetailCIFCBO.IsJobMaintenance;
                        Retail.Name = transaction.Transaction.RetailCIFCBO.Name;
                        Retail.IdentityTypeID = transaction.Transaction.RetailCIFCBO.IdentityTypeID;
                        Retail.IdentityNumber = transaction.Transaction.RetailCIFCBO.IdentityNumber;
                        Retail.IdentityStartDate = transaction.Transaction.RetailCIFCBO.IdentityStartDate;
                        Retail.IdentityEndDate = transaction.Transaction.RetailCIFCBO.IdentityEndDate;
                        Retail.IdentityAddress = transaction.Transaction.RetailCIFCBO.IdentityAddress;
                        Retail.IdentityKelurahan = transaction.Transaction.RetailCIFCBO.IdentityKelurahan;
                        Retail.IdentityKecamatan = transaction.Transaction.RetailCIFCBO.IdentityKecamatan;
                        Retail.IdentityCity = transaction.Transaction.RetailCIFCBO.IdentityCity;
                        Retail.IdentityProvince = transaction.Transaction.RetailCIFCBO.IdentityProvince;
                        Retail.IdentityCountry = transaction.Transaction.RetailCIFCBO.IdentityCountry;
                        Retail.IdentityPostalCode = transaction.Transaction.RetailCIFCBO.IdentityPostalCode;

                        Retail.IdentityTypeID2 = transaction.Transaction.RetailCIFCBO.IdentityTypeID2;
                        Retail.IdentityNumber2 = transaction.Transaction.RetailCIFCBO.IdentityNumber2;
                        Retail.IdentityStartDate2 = transaction.Transaction.RetailCIFCBO.IdentityStartDate2;
                        Retail.IdentityEndDate2 = transaction.Transaction.RetailCIFCBO.IdentityEndDate2;
                        Retail.IdentityAddress2 = transaction.Transaction.RetailCIFCBO.IdentityAddress2;
                        Retail.IdentityKelurahan2 = transaction.Transaction.RetailCIFCBO.IdentityKelurahan2;
                        Retail.IdentityKecamatan2 = transaction.Transaction.RetailCIFCBO.IdentityKecamatan2;
                        Retail.IdentityCity2 = transaction.Transaction.RetailCIFCBO.IdentityCity2;
                        Retail.IdentityProvince2 = transaction.Transaction.RetailCIFCBO.IdentityProvince2;
                        Retail.IdentityCountry2 = transaction.Transaction.RetailCIFCBO.IdentityCountry2;
                        Retail.IdentityPostalCode2 = transaction.Transaction.RetailCIFCBO.IdentityPostalCode2;

                        Retail.IdentityTypeID3 = transaction.Transaction.RetailCIFCBO.IdentityTypeID3;
                        Retail.IdentityNumber3 = transaction.Transaction.RetailCIFCBO.IdentityNumber3;
                        Retail.IdentityStartDate3 = transaction.Transaction.RetailCIFCBO.IdentityStartDate3;
                        Retail.IdentityEndDate3 = transaction.Transaction.RetailCIFCBO.IdentityEndDate3;
                        Retail.IdentityAddress3 = transaction.Transaction.RetailCIFCBO.IdentityAddress3;
                        Retail.IdentityKelurahan3 = transaction.Transaction.RetailCIFCBO.IdentityKelurahan3;
                        Retail.IdentityKecamatan3 = transaction.Transaction.RetailCIFCBO.IdentityKecamatan3;
                        Retail.IdentityCity3 = transaction.Transaction.RetailCIFCBO.IdentityCity3;
                        Retail.IdentityProvince3 = transaction.Transaction.RetailCIFCBO.IdentityProvince3;
                        Retail.IdentityCountry3 = transaction.Transaction.RetailCIFCBO.IdentityCountry3;
                        Retail.IdentityPostalCode3 = transaction.Transaction.RetailCIFCBO.IdentityPostalCode3;

                        Retail.IdentityTypeID4 = transaction.Transaction.RetailCIFCBO.IdentityTypeID4;
                        Retail.IdentityNumber4 = transaction.Transaction.RetailCIFCBO.IdentityNumber4;
                        Retail.IdentityStartDate4 = transaction.Transaction.RetailCIFCBO.IdentityStartDate4;
                        Retail.IdentityEndDate4 = transaction.Transaction.RetailCIFCBO.IdentityEndDate4;
                        Retail.IdentityAddress4 = transaction.Transaction.RetailCIFCBO.IdentityAddress4;
                        Retail.IdentityKelurahan4 = transaction.Transaction.RetailCIFCBO.IdentityKelurahan4;
                        Retail.IdentityKecamatan4 = transaction.Transaction.RetailCIFCBO.IdentityKecamatan4;
                        Retail.IdentityCity4 = transaction.Transaction.RetailCIFCBO.IdentityCity4;
                        Retail.IdentityProvince4 = transaction.Transaction.RetailCIFCBO.IdentityProvince4;
                        Retail.IdentityCountry4 = transaction.Transaction.RetailCIFCBO.IdentityCountry4;
                        Retail.IdentityPostalCode4 = transaction.Transaction.RetailCIFCBO.IdentityPostalCode4;

                        Retail.NPWPNumber = transaction.Transaction.RetailCIFCBO.NPWPNumber;
                        Retail.IsNPWPReceived = transaction.Transaction.RetailCIFCBO.IsNPWPReceived;
                        Retail.MaritalStatusID = transaction.Transaction.RetailCIFCBO.MaritalStatusID;
                        Retail.SpouseName = transaction.Transaction.RetailCIFCBO.SpouseName;
                        Retail.IsCorrespondenseToEmail = transaction.Transaction.RetailCIFCBO.IsCorrespondenseToEmail;
                        Retail.CorrespondenseAddress = transaction.Transaction.RetailCIFCBO.CorrespondenseAddress;
                        Retail.CorrespondenseKelurahan = transaction.Transaction.RetailCIFCBO.CorrespondenseKelurahan;
                        Retail.CorrespondenseKecamatan = transaction.Transaction.RetailCIFCBO.CorrespondenseKecamatan;
                        Retail.CorrespondenseCity = transaction.Transaction.RetailCIFCBO.CorrespondenseCity;
                        Retail.CorrespondenseProvince = transaction.Transaction.RetailCIFCBO.CorrespondenseProvince;
                        Retail.CorrespondenseCountry = transaction.Transaction.RetailCIFCBO.CorrespondenseCountry;
                        Retail.CorrespondensePostalCode = transaction.Transaction.RetailCIFCBO.CorrespondensePostalCode;
                        Retail.CellPhoneMethodID = transaction.Transaction.RetailCIFCBO.CellPhoneMethodID;
                        Retail.CellPhone = transaction.Transaction.RetailCIFCBO.CellPhone;
                        Retail.UpdatedCellPhone = transaction.Transaction.RetailCIFCBO.UpdatedCellPhone;
                        Retail.HomePhoneMethodID = transaction.Transaction.RetailCIFCBO.HomePhoneMethodID;
                        Retail.HomePhone = transaction.Transaction.RetailCIFCBO.HomePhone;
                        Retail.UpdatedHomePhone = transaction.Transaction.RetailCIFCBO.UpdatedHomePhone;
                        Retail.OfficePhoneMethodID = transaction.Transaction.RetailCIFCBO.OfficePhoneMethodID;
                        Retail.OfficePhone = transaction.Transaction.RetailCIFCBO.OfficePhone;
                        Retail.UpdatedOfficePhone = transaction.Transaction.RetailCIFCBO.UpdatedOfficePhone;
                        Retail.FaxMethodID = transaction.Transaction.RetailCIFCBO.FaxMethodID;
                        Retail.Fax = transaction.Transaction.RetailCIFCBO.Fax;
                        Retail.UpdatedFax = transaction.Transaction.RetailCIFCBO.UpdatedFax;
                        Retail.EmailAddress = transaction.Transaction.RetailCIFCBO.EmailAddress;
                        Retail.IdentityAddress = transaction.Transaction.RetailCIFCBO.IdentityAddress;
                        Retail.OfficeAddress = transaction.Transaction.RetailCIFCBO.OfficeAddress;
                        Retail.OfficeKelurahan = transaction.Transaction.RetailCIFCBO.OfficeKelurahan;
                        Retail.OfficeKecamatan = transaction.Transaction.RetailCIFCBO.OfficeKecamatan;
                        Retail.OfficeCity = transaction.Transaction.RetailCIFCBO.OfficeCity;
                        Retail.OfficeProvince = transaction.Transaction.RetailCIFCBO.OfficeProvince;
                        Retail.OfficeCountry = transaction.Transaction.RetailCIFCBO.OfficeCountry;
                        Retail.OfficePostalCode = transaction.Transaction.RetailCIFCBO.OfficePostalCode;
                        Retail.Nationality = transaction.Transaction.RetailCIFCBO.Nationality;
                        Retail.FundSource = transaction.Transaction.RetailCIFCBO.FundSource;
                        Retail.UBOName = transaction.Transaction.RetailCIFCBO.UBOName;
                        Retail.UBOIdentityType = transaction.Transaction.RetailCIFCBO.UBOIdentityType;
                        Retail.UBOPhone = transaction.Transaction.RetailCIFCBO.UBOPhone;
                        Retail.UBOJob = transaction.Transaction.RetailCIFCBO.UBOJob;
                        Retail.NetAsset = transaction.Transaction.RetailCIFCBO.NetAsset;
                        Retail.MonthlyIncome = transaction.Transaction.RetailCIFCBO.MonthlyIncome;
                        Retail.MonthlyExtraIncome = transaction.Transaction.RetailCIFCBO.MonthlyExtraIncome;
                        Retail.Job = transaction.Transaction.RetailCIFCBO.Job;
                        Retail.AccountPurpose = transaction.Transaction.RetailCIFCBO.AccountPurpose;
                        Retail.IncomeForecast = transaction.Transaction.RetailCIFCBO.IncomeForecast;
                        Retail.OutcomeForecast = transaction.Transaction.RetailCIFCBO.OutcomeForecast;
                        Retail.TransactionForecast = transaction.Transaction.RetailCIFCBO.TransactionForecast;
                        Retail.SubType = transaction.Transaction.RetailCIFCBO.SubType;
                        Retail.AtmNumber = transaction.Transaction.RetailCIFCBO.AtmNumber;
                        Retail.RiskRatingReportDate = transaction.Transaction.RetailCIFCBO.RiskRatingReportDate;
                        Retail.RiskRatingNextReviewDate = transaction.Transaction.RetailCIFCBO.RiskRatingNextReviewDate;
                        Retail.RiskRatingResultID = transaction.Transaction.RetailCIFCBO.RiskRatingResultID;
                        Retail.Remarks = transaction.Transaction.RetailCIFCBO.Remarks;
                        Retail.CreateDate = transaction.Transaction.RetailCIFCBO.CreateDate;
                        Retail.CreateBy = transaction.Transaction.RetailCIFCBO.CreateBy;
                        Retail.UpdateDate = transaction.Transaction.RetailCIFCBO.UpdateDate;
                        Retail.UpdateBy = transaction.Transaction.RetailCIFCBO.UpdateBy;
                        Retail.CompanyName = transaction.Transaction.RetailCIFCBO.CompanyName;
                        Retail.Position = transaction.Transaction.RetailCIFCBO.Position;
                        Retail.WorkPeriod = transaction.Transaction.RetailCIFCBO.WorkPeriod;
                        Retail.IndustryType = transaction.Transaction.RetailCIFCBO.IndustryType;
                        Retail.PekerjaanProfesional = transaction.Transaction.RetailCIFCBO.PekerjaanProfesional;
                        Retail.PekerjaanLainnya = transaction.Transaction.RetailCIFCBO.PekerjaanLainnya;
                        Retail.SumberDanaLainnya = transaction.Transaction.RetailCIFCBO.SumberDanaLainnya;
                        Retail.TujuanBukaRekeningLainnya = transaction.Transaction.RetailCIFCBO.TujuanBukaRekeningLainnya;
                        Retail.IsPekerjaanProfesional = transaction.Transaction.RetailCIFCBO.IsPekerjaanProfesional;
                        Retail.IsPekerjaanLainnya = transaction.Transaction.RetailCIFCBO.IsPekerjaanLainnya;
                        Retail.IsTujuanBukaRekeningLainnya = transaction.Transaction.RetailCIFCBO.IsTujuanBukaRekeningLainnya;
                        Retail.IsSumberDanaLainnya = transaction.Transaction.RetailCIFCBO.IsSumberDanaLainnya;
                        Retail.IsOthers = transaction.Transaction.RetailCIFCBO.IsOthers;
                        Retail.IsPenawaranProductPerbankan = transaction.Transaction.RetailCIFCBO.IsPenawaranProductPerbankan;
                        Retail.IsSignature = transaction.Transaction.RetailCIFCBO.IsSignature;
                        Retail.IsStampDuty = transaction.Transaction.RetailCIFCBO.IsStampDuty;
                        Retail.AmountLienUnlien = transaction.Transaction.RetailCIFCBO.AmountLienUnlien;
                        Retail.IsMotherMaiden = transaction.Transaction.RetailCIFCBO.IsMotherMaiden;
                        Retail.MotherMaiden = transaction.Transaction.RetailCIFCBO.MotherMaiden;
                        Retail.IsReligion = transaction.Transaction.RetailCIFCBO.IsReligion;
                        Retail.Religion = transaction.Transaction.RetailCIFCBO.Religion;
                        Retail.IsEducation = transaction.Transaction.RetailCIFCBO.IsEducation;
                        Retail.Education = transaction.Transaction.RetailCIFCBO.Education;
                        Retail.IsDataUBO = transaction.Transaction.RetailCIFCBO.IsDataUBO;
                        Retail.CIFUBO = transaction.Transaction.RetailCIFCBO.DataUBOCif;
                        Retail.UBOName = transaction.Transaction.RetailCIFCBO.UBOName;
                        Retail.UBORelationship = transaction.Transaction.RetailCIFCBO.DataUBORelationship;
                        Retail.IsTransparansiDataPenawaranProduct = transaction.Transaction.RetailCIFCBO.IsTransparansiDataPenawaranProduct;
                        Retail.IsTransparansiPenggunaan = transaction.Transaction.RetailCIFCBO.IsTransparansiPenggunaan;
                        Retail.IsPenawaranProduct = transaction.Transaction.RetailCIFCBO.IsPenawaranProduct;
                        Retail.IsSignature = transaction.Transaction.RetailCIFCBO.IsSignature;
                        Retail.IsStampDuty = transaction.Transaction.RetailCIFCBO.IsStampDuty;
                        Retail.IsSLIK = transaction.Transaction.RetailCIFCBO.IsSLIK;
                        Retail.GrosIncomeccy = transaction.Transaction.RetailCIFCBO.GrosIncomeccy;
                        Retail.GrossCifSpouse = transaction.Transaction.RetailCIFCBO.GrossCifSpouse;
                        Retail.GrossIncomeAmount = transaction.Transaction.RetailCIFCBO.GrossIncomeAmount;
                        Retail.GrossSpouseName = transaction.Transaction.RetailCIFCBO.GrossSpouseName;
                        Retail.GrossSpouseRelation = transaction.Transaction.RetailCIFCBO.GrossSpouseRelation;
                        Retail.IsFatcaCRS = transaction.Transaction.RetailCIFCBO.IsFatcaCRS;
                        Retail.FatcaCountryCode = transaction.Transaction.RetailCIFCBO.FatcaCountryCode;
                        Retail.FatcaCRSStatus = transaction.Transaction.RetailCIFCBO.FatcaCRSStatus;
                        Retail.FatcaDateOnForm = transaction.Transaction.RetailCIFCBO.FatcaDateOnForm;
                        Retail.FatcaReviewStatus = transaction.Transaction.RetailCIFCBO.FatcaReviewStatus;
                        Retail.FatcaReviewStatusDate = transaction.Transaction.RetailCIFCBO.FatcaReviewStatusDate;
                        Retail.FatcaTaxPayer = transaction.Transaction.RetailCIFCBO.FatcaTaxPayer;
                        Retail.FatcaTaxPayerID = transaction.Transaction.RetailCIFCBO.FatcaTaxPayerID;
                        Retail.FatcaWithHolding = transaction.Transaction.RetailCIFCBO.FatcaWithHolding;
                        Retail.IsChangeSignature = transaction.Transaction.RetailCIFCBO.IsChangeSignature;
                        Retail.ChangeSignature = transaction.Transaction.RetailCIFCBO.ChangeSignature;
                        Retail.Tier = transaction.Transaction.RetailCIFCBO.Tier;
                        Retail.DispatchModeOther = transaction.Transaction.RetailCIFCBO.DispatchModeOther;

                        context.SaveChanges();

                        RetailCIFCBOID = Retail.RetailCIFCBOID;
                    }
                    #endregion
                    #region RetailCIFCBOContact dayat
                    context.RetailCIFCBOContacts.RemoveRange(context.RetailCIFCBOContacts.Where(xx => xx.RetailCIFCBOID == RetailCIFCBOID));
                    context.SaveChanges();

                    DBS.Entity.RetailCIFCBOContact dataRetailCIFContact = new DBS.Entity.RetailCIFCBOContact();
                    //selular
                    if (transaction.Transaction.SelularPhoneNumbers != null && transaction.Transaction.SelularPhoneNumbers.Count > 0)
                    {
                        foreach (var item in transaction.Transaction.SelularPhoneNumbers)
                        {
                            if (item.ActionType != null)
                            {
                                dataRetailCIFContact.RetailCIFCBOID = RetailCIFCBOID;
                                dataRetailCIFContact.ActionType = item.ActionType;
                                dataRetailCIFContact.ContactType = item.ContactType;
                                dataRetailCIFContact.PhoneNumber = item.PhoneNumber;
                                dataRetailCIFContact.CityCode = item.CityCode;
                                dataRetailCIFContact.CountryCode = item.CountryCode;
                                dataRetailCIFContact.CreateDate = transaction.Transaction.RetailCIFCBO.CreateDate;
                                dataRetailCIFContact.CreateBy = transaction.Transaction.RetailCIFCBO.CreateBy;
                                dataRetailCIFContact.UpdateDate = transaction.Transaction.RetailCIFCBO.UpdateDate;
                                dataRetailCIFContact.UpdateBy = transaction.Transaction.RetailCIFCBO.UpdateBy;
                                dataRetailCIFContact.RetailCIFCBOContactUpdateID = null;
                                dataRetailCIFContact.UpdateCountryCode = null;
                                dataRetailCIFContact.UpdateCityCode = null;
                                dataRetailCIFContact.UpdatePhoneNumber = null;
                                if (item.ActionType == "2")
                                {
                                    dataRetailCIFContact.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                    dataRetailCIFContact.UpdateCountryCode = item.UpdateCountryCode;
                                    dataRetailCIFContact.UpdateCityCode = item.UpdateCityCode;
                                    dataRetailCIFContact.UpdatePhoneNumber = item.UpdatePhoneNumber;
                                }
                                if (item.ActionType == "3")
                                {
                                    dataRetailCIFContact.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                }

                                context.RetailCIFCBOContacts.Add(dataRetailCIFContact);
                                context.SaveChanges();
                            }
                        }
                    }
                    //oofice
                    if (transaction.Transaction.OfficePhoneNumbers != null && transaction.Transaction.OfficePhoneNumbers.Count > 0)
                    {
                        foreach (var item in transaction.Transaction.OfficePhoneNumbers)
                        {
                            if (item.ActionType != null)
                            {
                                dataRetailCIFContact.RetailCIFCBOID = RetailCIFCBOID;
                                dataRetailCIFContact.ActionType = item.ActionType;
                                dataRetailCIFContact.ContactType = item.ContactType;
                                dataRetailCIFContact.PhoneNumber = item.PhoneNumber;
                                dataRetailCIFContact.CityCode = item.CityCode;
                                dataRetailCIFContact.CountryCode = item.CountryCode;
                                dataRetailCIFContact.CreateDate = transaction.Transaction.RetailCIFCBO.CreateDate;
                                dataRetailCIFContact.CreateBy = transaction.Transaction.RetailCIFCBO.CreateBy;
                                dataRetailCIFContact.UpdateDate = transaction.Transaction.RetailCIFCBO.UpdateDate;
                                dataRetailCIFContact.UpdateBy = transaction.Transaction.RetailCIFCBO.UpdateBy;
                                dataRetailCIFContact.RetailCIFCBOContactUpdateID = null;
                                dataRetailCIFContact.UpdateCountryCode = null;
                                dataRetailCIFContact.UpdateCityCode = null;
                                dataRetailCIFContact.UpdatePhoneNumber = null;
                                if (item.ActionType == "2")
                                {
                                    dataRetailCIFContact.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                    dataRetailCIFContact.UpdateCountryCode = item.UpdateCountryCode;
                                    dataRetailCIFContact.UpdateCityCode = item.UpdateCityCode;
                                    dataRetailCIFContact.UpdatePhoneNumber = item.UpdatePhoneNumber;
                                }
                                if (item.ActionType == "3")
                                {
                                    dataRetailCIFContact.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                }
                                context.RetailCIFCBOContacts.Add(dataRetailCIFContact);
                                context.SaveChanges();
                            }
                        }

                    }
                    //Home
                    if (transaction.Transaction.HomePhoneNumbers != null && transaction.Transaction.HomePhoneNumbers.Count > 0)
                    {
                        foreach (var item in transaction.Transaction.HomePhoneNumbers)
                        {
                            if (item.ActionType != null)
                            {
                                dataRetailCIFContact.RetailCIFCBOID = RetailCIFCBOID;
                                dataRetailCIFContact.ActionType = item.ActionType;
                                dataRetailCIFContact.ContactType = item.ContactType;
                                dataRetailCIFContact.PhoneNumber = item.PhoneNumber;
                                dataRetailCIFContact.CityCode = item.CityCode;
                                dataRetailCIFContact.CountryCode = item.CountryCode;
                                dataRetailCIFContact.CreateDate = transaction.Transaction.RetailCIFCBO.CreateDate;
                                dataRetailCIFContact.CreateBy = transaction.Transaction.RetailCIFCBO.CreateBy;
                                dataRetailCIFContact.UpdateDate = transaction.Transaction.RetailCIFCBO.UpdateDate;
                                dataRetailCIFContact.UpdateBy = transaction.Transaction.RetailCIFCBO.UpdateBy;
                                dataRetailCIFContact.RetailCIFCBOContactUpdateID = null;
                                dataRetailCIFContact.UpdateCountryCode = null;
                                dataRetailCIFContact.UpdateCityCode = null;
                                dataRetailCIFContact.UpdatePhoneNumber = null;
                                if (item.ActionType == "2")
                                {
                                    dataRetailCIFContact.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                    dataRetailCIFContact.UpdateCountryCode = item.UpdateCountryCode;
                                    dataRetailCIFContact.UpdateCityCode = item.UpdateCityCode;
                                    dataRetailCIFContact.UpdatePhoneNumber = item.UpdatePhoneNumber;
                                }
                                if (item.ActionType == "3")
                                {
                                    dataRetailCIFContact.RetailCIFCBOContactUpdateID = item.RetailCIFCBOContactUpdateID;
                                }
                                context.RetailCIFCBOContacts.Add(dataRetailCIFContact);
                                context.SaveChanges();
                            }
                        }
                    }
                    #endregion

                    #region OldCode
                    /*
                    #region Customer Join                 
                    Entity.RetailCIFCBOAccountJoin RetailAccJoin = context.RetailCIFCBOAccountJoins.Where(t => t.TransactionID == transaction.Transaction.ID).FirstOrDefault();
                    if (RetailAccJoin != null)
                    {
                        foreach (var item in transaction.Transaction.AddJoinTableCustomerCIF)
                        {
                            RetailAccJoin.TransactionID = TransactionID;
                            RetailAccJoin.CIF = item.CIF;
                            RetailAccJoin.UpdateDate = DateTime.UtcNow;
                            RetailAccJoin.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        }
                        context.SaveChanges();                    
                    }
                    #endregion

                    #region FDD Account
                    Entity.RetailCIFCBOAccNo RetailFFDAccount = context.RetailCIFCBOAccNoes.Where(t => t.RetailCIFCBOID == RetailCIFCBOID).FirstOrDefault();
                    if (RetailFFDAccount != null)
                    {
                        var SelectedFFD = transaction.Transaction.AddJoinTableFFDAccountCIF.Where(s => s.IsAddFFDAccount == true);
                        foreach (var item in SelectedFFD)
                        {
                            RetailFFDAccount.RetailCIFCBOID = RetailCIFCBOID;
                            RetailFFDAccount.AccountNumber = item.AccountNumber;
                            RetailFFDAccount.IsFFD = item.IsAddFFDAccount;                            
                            RetailFFDAccount.UpdateDate = DateTime.UtcNow;
                            RetailFFDAccount.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        }
                        context.SaveChanges();                    
                    }
                    #endregion

                    #region FDD Account Join
                    Entity.RetailCIFCBOAccNo RetailFFDAccountJoin = context.RetailCIFCBOAccNoes.Where(t => t.RetailCIFCBOID == RetailCIFCBOID).FirstOrDefault();
                    if (RetailFFDAccountJoin != null)
                    {
                        var SelectedFDDJoin = transaction.Transaction.AddJoinTableAccountCIF.Where(s => s.IsAddFFDAccount == true);
                        foreach (var item in SelectedFDDJoin)
                        {
                            RetailFFDAccountJoin.RetailCIFCBOID = RetailCIFCBOID;
                            RetailFFDAccountJoin.AccountNumber = item.AccountNumber;
                            RetailFFDAccount.CurrencyID = item.Currency.ID;
                            RetailFFDAccountJoin.UpdateDate = DateTime.UtcNow;
                            RetailFFDAccountJoin.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        }
                        context.SaveChanges();
                    }
                    #endregion

                    #region Dormant
                    Entity.RetailCIFCBOAccNo RetailDormant = context.RetailCIFCBOAccNoes.Where(t => t.RetailCIFCBOID == RetailCIFCBOID).FirstOrDefault();
                    if (RetailDormant != null)
                    {
                        var SelectedDormant = transaction.Transaction.AddJoinTableDormantCIF.Where(s => s.IsAddDormantAccount == true);
                        foreach (var item in SelectedDormant)
                        {
                            RetailDormant.RetailCIFCBOID = RetailCIFCBOID;
                            RetailDormant.AccountNumber = item.AccountNumber;
                            RetailDormant.CurrencyID = item.Currency.ID;
                            RetailDormant.UpdateDate = DateTime.UtcNow;
                            RetailDormant.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        }
                        context.SaveChanges();
                    }
                    #endregion

                    #region Freeze Account
                    Entity.RetailCIFCBOFreezeAccount RetailFreezeAccount = context.RetailCIFCBOFreezeAccounts.Where(t => t.RetailCIFCBOID == RetailCIFCBOID).FirstOrDefault();
                    if (RetailFreezeAccount != null)
                    {
                        var SelectedFreeze = transaction.Transaction.AddJoinTableFreezeUnfreezeCIF.Where(s => s.IsAddTblFreezeAccount == true);
                        foreach (var item in SelectedFreeze)
                        {
                            RetailFreezeAccount.RetailCIFCBOID = RetailCIFCBOID;
                            RetailFreezeAccount.AccountNumber = item.AccountNumber;
                            RetailFreezeAccount.IsFreeze = item.IsFreezeAccount;
                            RetailFreezeAccount.UpdateDate = DateTime.UtcNow;
                            RetailFreezeAccount.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        }
                        context.SaveChanges();
                    }
                    #endregion
                     */
                    #endregion
                    #region Customer Join
                    if (transaction.Transaction.AddJoinTableCustomerCIF != null && transaction.Transaction.AddJoinTableCustomerCIF.Count() > 0)
                    {
                        IList<Entity.RetailCIFCBOAccountJoin> ExistingJoinTableCustomer = context.RetailCIFCBOAccountJoins.Where(t => t.TransactionID == transaction.Transaction.ID).ToList();
                        if (ExistingJoinTableCustomer != null && ExistingJoinTableCustomer.Count > 0)
                        {
                            foreach (var iJoinTableCustomer in ExistingJoinTableCustomer)
                            {
                                context.RetailCIFCBOAccountJoins.Remove(iJoinTableCustomer);
                            }
                            context.SaveChanges();

                            //if (transaction.Transaction.AddJoinTableCustomerCIF != null)
                            //{
                            //    if (transaction.Transaction.AddJoinTableCustomerCIF.Count() > 0)
                            //    {
                            foreach (var iJoinTableCustomerNew in transaction.Transaction.AddJoinTableCustomerCIF)
                            {
                                Entity.RetailCIFCBOAccountJoin dataRetailCIFCBOAccountJoin = new Entity.RetailCIFCBOAccountJoin()
                                {
                                    TransactionID = TransactionID,
                                    CIF = iJoinTableCustomerNew.CIF,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                };
                                context.RetailCIFCBOAccountJoins.Add(dataRetailCIFCBOAccountJoin);
                            }
                            context.SaveChanges();
                            //    }
                            //}
                        }
                    }
                    else
                    {
                        if (transaction.Transaction.RetailCIFCBO.MaintenanceTypes == MaintenanceTypes[0])
                        {
                            IList<Entity.RetailCIFCBOAccountJoin> ExistingJoinTableCustomer = context.RetailCIFCBOAccountJoins.Where(t => t.TransactionID == transaction.Transaction.ID).ToList();
                            if (ExistingJoinTableCustomer != null)
                            {
                                foreach (var iJoinTableCustomer in ExistingJoinTableCustomer)
                                {
                                    context.RetailCIFCBOAccountJoins.Remove(iJoinTableCustomer);
                                }
                                context.SaveChanges();
                            }
                        }

                    }
                    #endregion

                    #region FDD Account
                    if (transaction.Transaction.AddJoinTableFFDAccountCIF != null && transaction.Transaction.AddJoinTableFFDAccountCIF.Count() > 0)
                    {
                        IList<Entity.RetailCIFCBOAccNo> ExistingJoinTableFFDAccount = context.RetailCIFCBOAccNoes.Where(t => t.RetailCIFCBOID == RetailCIFCBOID).ToList();
                        if (ExistingJoinTableFFDAccount != null && ExistingJoinTableFFDAccount.Count > 0)
                        {
                            foreach (var iJoinTableFFDAccount in ExistingJoinTableFFDAccount)
                            {
                                context.RetailCIFCBOAccNoes.Remove(iJoinTableFFDAccount);
                            }
                            context.SaveChanges();

                            //if (transaction.Transaction.AddJoinTableFFDAccountCIF != null)
                            //{
                            //    if (transaction.Transaction.AddJoinTableFFDAccountCIF.Count() > 0)
                            //    {
                            foreach (var iJoinTableFFDAccountNew in transaction.Transaction.AddJoinTableFFDAccountCIF)
                            {
                                Entity.RetailCIFCBOAccNo dataRetailCIFCBOAccount = new Entity.RetailCIFCBOAccNo()
                                {
                                    RetailCIFCBOID = RetailCIFCBOID,
                                    AccountNumber = iJoinTableFFDAccountNew.AccountNumber,
                                    IsFFD = iJoinTableFFDAccountNew.IsAddFFDAccount,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                };
                                context.RetailCIFCBOAccNoes.Add(dataRetailCIFCBOAccount);
                            }
                            context.SaveChanges();
                            //    }
                            //}
                        }
                    }
                    else
                    {
                        if (transaction.Transaction.RetailCIFCBO.MaintenanceTypes == MaintenanceTypes[2])
                        {
                            IList<Entity.RetailCIFCBOAccNo> ExistingJoinTableFFDAccount = context.RetailCIFCBOAccNoes.Where(t => t.RetailCIFCBOID == RetailCIFCBOID).ToList();
                            if (ExistingJoinTableFFDAccount != null)
                            {
                                foreach (var iJoinTableFFDAccount in ExistingJoinTableFFDAccount)
                                {
                                    context.RetailCIFCBOAccNoes.Remove(iJoinTableFFDAccount);
                                }
                                context.SaveChanges();
                            }
                        }
                    }
                    #endregion


                    #region ChangeRM
                    /*var ChangeRM = context.ChangeRMTransactions.Where(t => t.TransactionID == transaction.Transaction.ID).FirstOrDefault();
                    if (ChangeRM != null)
                    {
                        foreach (var item in transaction.Transaction.ChangeRMModel)
                        {
                            ChangeRM.CIF = item.CIF;
                            ChangeRM.ChangeSegmentFrom = item.ChangeSegmentFrom;
                            ChangeRM.ChangeSegmentTo = item.ChangeSegmentTo;
                            ChangeRM.ChangeFromBranchCode = item.ChangeFromBranchCode;
                            ChangeRM.ChangeFromRMCode = item.ChangeToRMCode;
                            ChangeRM.ChangeFromBU = item.ChangeFromBU;
                            ChangeRM.ChangeToBranchCode = item.ChangeToBranchCode;
                            ChangeRM.ChangeToRMCode = item.ChangeToRMCode;
                            ChangeRM.ChangeToBU = item.ChangeToBU;
                            ChangeRM.Account = item.Account;
                            ChangeRM.PCCode = item.PCCode;
                            ChangeRM.EATPB = item.EATPB;
                            ChangeRM.Remarks = item.Remarks;

                        }
                        context.SaveChanges();
                        RetailCIFCBOID = ChangeRM.TransactionID;
                    }*/

                    if (transaction.Transaction.ChangeRMModel != null)
                    {
                        if (transaction.Transaction.ChangeRMModel.Count() > 0)
                        {
                            foreach (var item in transaction.Transaction.ChangeRMModel)
                            {
                                if (item.Selected == true && !string.IsNullOrEmpty(item.CIF))
                                {
                                    var ChangeRM = context.ChangeRMTransactions.Where(t => t.TransactionID == transaction.Transaction.ID && t.CIF.Equals(item.CIF)).FirstOrDefault();
                                    if (ChangeRM != null)
                                    {
                                        ChangeRM.CIF = !string.IsNullOrEmpty(item.CIF) ? item.CIF : "";
                                        ChangeRM.ChangeSegmentFrom = !string.IsNullOrEmpty(item.ChangeSegmentFrom) ? item.ChangeSegmentFrom : "";
                                        ChangeRM.ChangeSegmentTo = !string.IsNullOrEmpty(item.ChangeSegmentTo) ? item.ChangeSegmentTo : "";
                                        ChangeRM.ChangeFromBranchCode = !string.IsNullOrEmpty(item.ChangeFromBranchCode) ? item.ChangeFromBranchCode : "";
                                        ChangeRM.ChangeFromRMCode = !string.IsNullOrEmpty(item.ChangeFromRMCode) ? item.ChangeFromRMCode : "";
                                        ChangeRM.ChangeFromBU = !string.IsNullOrEmpty(item.ChangeFromBU) ? item.ChangeFromBU : "";
                                        ChangeRM.ChangeToBranchCode = !string.IsNullOrEmpty(item.ChangeToBranchCode) ? item.ChangeToBranchCode : "";
                                        ChangeRM.ChangeToRMCode = !string.IsNullOrEmpty(item.ChangeToRMCode) ? item.ChangeToRMCode : "";
                                        ChangeRM.ChangeToBU = !string.IsNullOrEmpty(item.ChangeToBU) ? item.ChangeToBU : "";
                                        ChangeRM.Account = !string.IsNullOrEmpty(item.Account) ? item.Account : "";
                                        ChangeRM.PCCode = !string.IsNullOrEmpty(item.PCCode) ? item.PCCode : "";
                                        ChangeRM.EATPB = !string.IsNullOrEmpty(item.EATPB) ? item.EATPB : "";
                                        ChangeRM.Remarks = !string.IsNullOrEmpty(item.Remarks) ? item.Remarks : "";
                                        context.SaveChanges();
                                    }
                                    else
                                    {
                                        DBS.Entity.ChangeRMTransaction ChangeRMSegmentSOLID = new DBS.Entity.ChangeRMTransaction()
                                        {
                                            TransactionID = TransactionID,
                                            CIF = item.CIF,
                                            ChangeSegmentFrom = !string.IsNullOrEmpty(item.ChangeSegmentFrom) ? item.ChangeSegmentFrom : "",
                                            ChangeSegmentTo = !string.IsNullOrEmpty(item.ChangeSegmentTo) ? item.ChangeSegmentTo : "",
                                            ChangeFromBranchCode = !string.IsNullOrEmpty(item.ChangeFromBranchCode) ? item.ChangeFromBranchCode : "",
                                            ChangeFromRMCode = !string.IsNullOrEmpty(item.ChangeFromRMCode) ? item.ChangeFromRMCode : "",
                                            ChangeFromBU = !string.IsNullOrEmpty(item.ChangeFromBU) ? item.ChangeFromBU : "",
                                            ChangeToBranchCode = !string.IsNullOrEmpty(item.ChangeToBranchCode) ? item.ChangeToBranchCode : "",
                                            ChangeToRMCode = !string.IsNullOrEmpty(item.ChangeToRMCode) ? item.ChangeToRMCode : "",
                                            ChangeToBU = !string.IsNullOrEmpty(item.ChangeToBU) ? item.ChangeToBU : "",
                                            Account = !string.IsNullOrEmpty(item.Account) ? item.Account : "",
                                            PCCode = !string.IsNullOrEmpty(item.PCCode) ? item.PCCode : "",
                                            EATPB = !string.IsNullOrEmpty(item.EATPB) ? item.EATPB : "",
                                            Remarks = !string.IsNullOrEmpty(item.Remarks) ? item.Remarks : "",
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context.ChangeRMTransactions.Add(ChangeRMSegmentSOLID);
                                    }
                                }
                                else
                                {
                                    var ChangeRM = context.ChangeRMTransactions.Where(t => t.TransactionID == transaction.Transaction.ID && t.CIF.Equals(item.CIF)).FirstOrDefault();
                                    if (ChangeRM != null)
                                    {
                                        context.ChangeRMTransactions.Remove(ChangeRM);
                                        context.SaveChanges();
                                    }
                                }
                                ActivityHistoryIDSelected = item.ActivityHistoryID;
                                CIFSelected = item.CIF;
                                CustomerNameSelected = item.CustomerName;

                                IList<Entity.ChangeRM> ChangeRMSelected = context.ChangeRMs.Where(a => a.ActivityHistoryID == ActivityHistoryIDSelected && a.CIF == CIFSelected && a.CUSTOMER_NAME == CustomerNameSelected).ToList();

                                if (ChangeRMSelected != null && ChangeRMSelected.Count > 0)
                                {
                                    foreach (var itemselected in ChangeRMSelected)
                                    {
                                        itemselected.Selected = true;
                                    }
                                    context.SaveChanges();
                                }
                            }
                            context.SaveChanges();
                        }

                    }
                    #endregion

                    #region Account Join
                    if (transaction.Transaction.AddJoinTableAccountCIF != null && transaction.Transaction.AddJoinTableAccountCIF.Count() > 0)
                    {
                        IList<Entity.RetailCIFCBOAccNo> ExistingJoinTableAccount = context.RetailCIFCBOAccNoes.Where(t => t.RetailCIFCBOID == RetailCIFCBOID).ToList();
                        if (ExistingJoinTableAccount != null && ExistingJoinTableAccount.Count > 0)
                        {
                            foreach (var iJoinTableAccount in ExistingJoinTableAccount)
                            {
                                context.RetailCIFCBOAccNoes.Remove(iJoinTableAccount);
                            }
                            context.SaveChanges();

                            //if (transaction.Transaction.AddJoinTableAccountCIF != null)
                            //{
                            //    if (transaction.Transaction.AddJoinTableAccountCIF.Count() > 0)
                            //    {
                            foreach (var iJoinTableAccountNew in transaction.Transaction.AddJoinTableAccountCIF)
                            {
                                Entity.RetailCIFCBOAccNo dataRetailCIFCBOAccount = new Entity.RetailCIFCBOAccNo()
                                {
                                    RetailCIFCBOID = RetailCIFCBOID,
                                    AccountNumber = iJoinTableAccountNew.AccountNumber,
                                    CurrencyID = iJoinTableAccountNew.Currency.ID,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                };
                                context.RetailCIFCBOAccNoes.Add(dataRetailCIFCBOAccount);

                            }
                            context.SaveChanges();
                            //    }

                            //}
                        }

                    }
                    else
                    {
                        if (transaction.Transaction.RetailCIFCBO.MaintenanceTypes == MaintenanceTypes[1])
                        {
                            IList<Entity.RetailCIFCBOAccNo> ExistingJoinTableAccount = context.RetailCIFCBOAccNoes.Where(t => t.RetailCIFCBOID == RetailCIFCBOID).ToList();
                            if (ExistingJoinTableAccount != null)
                            {
                                foreach (var iJoinTableAccount in ExistingJoinTableAccount)
                                {
                                    context.RetailCIFCBOAccNoes.Remove(iJoinTableAccount);
                                }
                                context.SaveChanges();
                            }
                        }
                    }
                    #endregion

                    #region Dormant
                    if (transaction.Transaction.AddJoinTableDormantCIF != null && transaction.Transaction.AddJoinTableDormantCIF.Count() > 0)
                    {
                        IList<Entity.RetailCIFCBOAccNo> ExistingJoinTableDormant = context.RetailCIFCBOAccNoes.Where(t => t.RetailCIFCBOID == RetailCIFCBOID).ToList();
                        if (ExistingJoinTableDormant != null)
                        {
                            foreach (var iJoinTableDormant in ExistingJoinTableDormant)
                            {
                                context.RetailCIFCBOAccNoes.Remove(iJoinTableDormant);
                            }
                            context.SaveChanges();
                        }
                        //if (transaction.Transaction.AddJoinTableDormantCIF != null)
                        //{
                        //    if (transaction.Transaction.AddJoinTableDormantCIF.Count() > 0)
                        //    {
                        var SelectedDormant = transaction.Transaction.AddJoinTableDormantCIF.Where(s => s.IsAddDormantAccount == true);
                        if (SelectedDormant.Count() > 0)
                        {
                            foreach (var iJoinDormantNew in SelectedDormant)
                            {
                                Entity.RetailCIFCBOAccNo dataRetailCIFCBDormant = new Entity.RetailCIFCBOAccNo()
                                {
                                    RetailCIFCBOID = RetailCIFCBOID,
                                    AccountNumber = iJoinDormantNew.AccountNumber,
                                    CurrencyID = iJoinDormantNew.Currency.ID,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                };
                                context.RetailCIFCBOAccNoes.Add(dataRetailCIFCBDormant);
                            }
                            context.SaveChanges();
                        }
                        //}
                        //}

                    }

                    #endregion

                    #region Freeze Account
                    if (transaction.Transaction.AddJoinTableFreezeUnfreezeCIF != null && transaction.Transaction.AddJoinTableFreezeUnfreezeCIF.Count() > 0)
                    {
                        IList<Entity.RetailCIFCBOFreezeAccount> ExistingJoinFreezeUnfreeze = context.RetailCIFCBOFreezeAccounts.Where(t => t.RetailCIFCBOID == RetailCIFCBOID).ToList();
                        if (ExistingJoinFreezeUnfreeze != null)
                        {
                            foreach (var iJoinTableFreezeUnfreeze in ExistingJoinFreezeUnfreeze)
                            {
                                context.RetailCIFCBOFreezeAccounts.Remove(iJoinTableFreezeUnfreeze);
                            }
                            context.SaveChanges();
                        }
                        //if (transaction.Transaction.AddJoinTableFreezeUnfreezeCIF != null)
                        //{
                        //    if (transaction.Transaction.AddJoinTableFreezeUnfreezeCIF.Count() > 0)
                        //    {
                        var SelectedFreeze = transaction.Transaction.AddJoinTableFreezeUnfreezeCIF.Where(s => s.IsAddTblFreezeAccount == true);
                        foreach (var iJoinTableFreezeUnfreezeNew in SelectedFreeze)
                        {
                            Entity.RetailCIFCBOFreezeAccount dataRetailFreezeUnfreezeAccount = new Entity.RetailCIFCBOFreezeAccount()
                            {
                                RetailCIFCBOID = RetailCIFCBOID,
                                AccountNumber = iJoinTableFreezeUnfreezeNew.AccountNumber,
                                IsFreeze = iJoinTableFreezeUnfreezeNew.IsFreezeAccount != null ? iJoinTableFreezeUnfreezeNew.IsFreezeAccount : null,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName,
                            };
                            context.RetailCIFCBOFreezeAccounts.Add(dataRetailFreezeUnfreezeAccount);
                        }
                        context.SaveChanges();
                        //    }
                        //}

                    }
                    #endregion

                    #region lien unlien
                    IList<Entity.LienUnlien> lu = context.LienUnliens.Where(x => x.TransactionId.Value == TransactionID).Where(z => z.IsDeleted.Value == false).ToList();
                    if (lu != null && lu.Count > 0)
                    {
                        foreach (var item in lu)
                        {
                            context.LienUnliens.Remove(item);
                        }
                        context.SaveChanges();
                    }
                    if (transaction.Transaction.AccountNumberLienUnlien != null)
                    {
                        if (transaction.Transaction.AccountNumberLienUnlien.Count > 0)
                        {
                            foreach (var item in transaction.Transaction.AccountNumberLienUnlien)
                            {
                                Entity.LienUnlien addNew = new Entity.LienUnlien();
                                addNew.AccountNumber = item.AccountNumber;
                                addNew.CIF = transaction.Transaction.Customer.CIF;
                                addNew.IsDeleted = false;
                                addNew.LienUnlienData = item.LienUnlien;
                                addNew.TransactionId = TransactionID;
                                addNew.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                addNew.UpdateDate = DateTime.UtcNow;
                                addNew.IsBranchCheckerVerify = false;
                                addNew.IsCBOAccountCheckerVerify = false;
                                context.LienUnliens.Add(addNew);
                            }
                            context.SaveChanges();
                        }
                    }
                    #endregion

                    ts.Complete();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }
            return isSuccess;
        }
        //end

        //getDetailChangeRM
        public bool GetTransactionDetailsChangeRM(Guid instanceID, ref TransactionCBODetailModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                TransactionCBODetailModel datatransaction = (from a in context.Transactions
                                                             where a.WorkflowInstanceID.Value.Equals(instanceID)
                                                             select new TransactionCBODetailModel
                                                             {
                                                                 ID = a.TransactionID,
                                                                 WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                                                 ApplicationID = a.ApplicationID,
                                                                 ApplicationDate = a.ApplicationDate,
                                                                 ApproverID = a.ApproverID ?? 0,
                                                                 Product = new ProductModel()
                                                                 {
                                                                     ID = a.Product.ProductID,
                                                                     Code = a.Product.ProductCode,
                                                                     Name = a.Product.ProductName,
                                                                     WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                                                     Workflow = a.Product.ProductWorkflow.WorkflowName,
                                                                     LastModifiedBy = a.Product.UpdateDate.HasValue ? a.Product.UpdateBy : a.Product.CreateBy,
                                                                     LastModifiedDate = a.Product.UpdateDate ?? a.Product.CreateDate
                                                                 },
                                                                 Currency = new CurrencyModel()
                                                                 {
                                                                     ID = a.Currency.CurrencyID,
                                                                     Code = a.Currency.CurrencyCode,
                                                                     Description = a.Currency.CurrencyDescription,
                                                                     LastModifiedBy = a.Currency.UpdateDate.HasValue ? a.Currency.UpdateBy : a.Currency.CreateBy,
                                                                     LastModifiedDate = a.Currency.UpdateDate ?? a.Currency.CreateDate
                                                                 },
                                                                 Amount = a.Amount,
                                                                 AmountUSD = a.AmountUSD,
                                                                 Rate = a.Rate,
                                                                 Channel = new ChannelModel()
                                                                 {
                                                                     ID = a.Channel.ChannelID,
                                                                     Name = a.Channel.ChannelName,
                                                                     LastModifiedBy = a.Channel.UpdateDate.HasValue ? a.Channel.UpdateBy : a.Channel.CreateBy,
                                                                     LastModifiedDate = a.Channel.UpdateDate ?? a.Channel.CreateDate
                                                                 },
                                                                 BizSegment = new BizSegmentModel()
                                                                 {
                                                                     ID = a.BizSegment.BizSegmentID,
                                                                     Name = a.BizSegment.BizSegmentName,
                                                                     Description = a.BizSegment.BizSegmentDescription,
                                                                     LastModifiedBy = a.BizSegment.UpdateDate.HasValue ? a.BizSegment.UpdateBy : a.BizSegment.CreateBy,
                                                                     LastModifiedDate = a.BizSegment.UpdateDate ?? a.BizSegment.CreateDate
                                                                 },
                                                                 BeneName = a.BeneName,
                                                                 BeneAccNumber = a.BeneAccNumber,

                                                                 //LLDCode = a.LLDCode,
                                                                 //LLDInfo = a.LLDInfo,  
                                                                 // newlld

                                                                 Bank = new BankModel()
                                                                 {
                                                                     ID = a.Bank.BankID,
                                                                     Code = a.Bank.BankCode,
                                                                     BankAccount = a.Bank.BankAccount,
                                                                     BranchCode = a.Bank.BranchCode,
                                                                     CommonName = a.Bank.CommonName,
                                                                     Currency = a.Bank.Currency,
                                                                     PGSL = a.Bank.Currency,
                                                                     SwiftCode = a.Bank.SwiftCode,
                                                                     Description = a.Bank.BankDescription,
                                                                     LastModifiedBy = a.Bank.UpdateDate.HasValue ? a.Bank.UpdateBy : a.Bank.CreateBy,
                                                                     LastModifiedDate = a.Bank.UpdateDate ?? a.Bank.CreateDate
                                                                 },

                                                                 //TransactionType
                                                                 TransactionType = new TransactionTypeModel()
                                                                 {
                                                                     ID = a.TransactionType.TransTypeID,
                                                                     Name = a.TransactionType.Product.ProductName
                                                                 },
                                                                 //MaintenanceType
                                                                 MaintenanceType = new MaintenanceTypeModel()
                                                                 {
                                                                     ID = a.MaintenanceType.CBOMaintainID,
                                                                     Name = a.MaintenanceType.MaintenanceTypeName
                                                                 },
                                                                 //CIFASSETBERSIH
                                                                 //CIFASSETBERSIH = new CIFASSETBERSIH()
                                                                 //{
                                                                 //    ID = a.CIFASSETBERSIH. 
                                                                 //},
                                                                 IsTopUrgent = a.IsTopUrgent,
                                                                 IsSignatureVerified = a.IsSignatureVerified,
                                                                 IsFreezeAccount = a.IsFrezeAccount,
                                                                 IsDormantAccount = a.IsDormantAccount,
                                                                 IsDraft = a.IsDraft,
                                                                 IsNewCustomer = a.IsNewCustomer,
                                                                 IsOtherAccountNumber = a.IsOtherAccountNumber,

                                                                 //DetailCompliance = a.DetailCompliance,                                                              
                                                                 Others = a.Others,
                                                                 TZNumber = a.TZNumber,

                                                                 Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                                                                 {
                                                                     ID = x.TransactionDocumentID,
                                                                     Type = new DocumentTypeModel()
                                                                     {
                                                                         ID = x.DocumentType.DocTypeID,
                                                                         Name = x.DocumentType.DocTypeName,
                                                                         Description = x.DocumentType.DocTypeDescription,
                                                                         LastModifiedBy = x.DocumentType.UpdateDate.HasValue ? x.DocumentType.UpdateBy : x.DocumentType.CreateBy,
                                                                         LastModifiedDate = x.DocumentType.UpdateDate ?? x.DocumentType.CreateDate
                                                                     },
                                                                     Purpose = new DocumentPurposeModel()
                                                                     {
                                                                         ID = x.DocumentPurpose.PurposeID,
                                                                         Name = x.DocumentPurpose.PurposeName,
                                                                         Description = x.DocumentPurpose.PurposeDescription,
                                                                         LastModifiedBy = x.DocumentPurpose.UpdateDate.HasValue ? x.DocumentPurpose.UpdateBy : x.DocumentPurpose.CreateBy,
                                                                         LastModifiedDate = x.DocumentPurpose.UpdateDate ?? x.DocumentPurpose.CreateDate
                                                                     },
                                                                     FileName = x.Filename,
                                                                     DocumentPath = x.DocumentPath,
                                                                     LastModifiedBy = x.UpdateDate.HasValue ? x.UpdateBy : x.CreateBy,
                                                                     LastModifiedDate = x.UpdateDate ?? x.CreateDate,
                                                                     IsDormant = x.IsDormant ?? false
                                                                 }).ToList(),
                                                                 CreateDate = a.CreateDate,
                                                                 LastModifiedBy = a.UpdateDate.HasValue ? a.UpdateBy : a.CreateBy,
                                                                 LastModifiedDate = a.UpdateDate ?? a.CreateDate,
                                                                 OtherBeneBankSwift = a.OtherBeneBankSwift,
                                                                 OtherBeneBankName = a.OtherBeneBankName,
                                                                 IsCitizen = a.IsCitizen,
                                                                 IsResident = a.IsResident,
                                                                 Type = a.Type,
                                                                 PaymentDetails = a.PaymentDetails,
                                                                 IsDocumentComplete = a.IsDocumentComplete
                                                             }).SingleOrDefault();

                // fill payment data
                if (datatransaction != null)
                {
                    //datatransaction.IsOtherBeneBank = datatransaction.Bank.Code != "999" ? false : true;
                    //string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(instanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    if (customerRepo.GetCustomerByTransaction(datatransaction.ID, ref customer, ref message))
                    {
                        datatransaction.Customer = customer;
                    }


                    var data = context.Transactions.Where(x => x.TransactionID.Equals(datatransaction.ID)).Select(x => x).FirstOrDefault();

                    //if (datatransaction.IsOtherAccountNumber != null && datatransaction.IsOtherAccountNumber == true)
                    //{


                    //    CustomerAccountModel Account = new CustomerAccountModel();
                    //    datatransaction.Account = Account;

                    //    datatransaction.OtherAccountNumber = data.OtherAccountNumber;

                    //    CurrencyModel AccountCurrency = new CurrencyModel();

                    //    AccountCurrency.ID = data.Currency1.CurrencyID;
                    //    AccountCurrency.Code = data.Currency1.CurrencyCode;
                    //    AccountCurrency.Description = data.Currency1.CurrencyDescription;


                    //    datatransaction.DebitCurrency = AccountCurrency;

                    //    if (datatransaction.Customer.Accounts != null)
                    //        datatransaction.Customer.Accounts = customer.Accounts.Where(x => x.Currency.Code.Equals(datatransaction.DebitCurrency.Code) || x.AccountNumber.Equals("-")).Select(y => y).ToList();

                    //}
                    //else
                    //{
                    //    CustomerAccountModel Account = new CustomerAccountModel();
                    //    Account.AccountNumber = data.CustomerAccount.AccountNumber;
                    //    Account.LastModifiedBy = data.CustomerAccount.UpdateDate == null ? data.CustomerAccount.CreateBy : data.CustomerAccount.UpdateBy;
                    //    Account.LastModifiedDate = data.CustomerAccount.UpdateDate == null ? data.CustomerAccount.CreateDate : data.CustomerAccount.UpdateDate.Value;

                    //    datatransaction.Account = Account;

                    //    CurrencyModel AccountCurrency = new CurrencyModel();
                    //    AccountCurrency.ID = data.Currency1.CurrencyID;
                    //    AccountCurrency.Code = data.Currency1.CurrencyCode;
                    //    AccountCurrency.Description = data.Currency1.CurrencyDescription;

                    //    datatransaction.DebitCurrency = AccountCurrency;

                    //    datatransaction.Customer.Accounts = customer.Accounts.Where(x => x.Currency.Code.Equals(datatransaction.DebitCurrency.Code) || x.AccountNumber.Equals("-")).Select(y => y).ToList();

                    //}


                    customerRepo.Dispose();
                    customer = null;

                    if (data.ProductType != null)
                    {
                        ProductTypeModel ptm = new ProductTypeModel();

                        ptm.ID = data.ProductType.ProductTypeID;
                        ptm.Code = data.ProductType.ProductTypeCode;
                        ptm.Description = data.ProductType.ProductTypeDesc;
                        ptm.IsFlowValas = data.ProductType.IsFlowValas;

                        datatransaction.ProductType = ptm;
                    }

                    if (data.LLD != null)
                    {
                        LLDModel lm = new LLDModel();

                        lm.ID = data.LLD.LLDID;
                        lm.Code = data.LLD.LLDCode;
                        lm.Description = data.LLD.LLDDescription;

                        datatransaction.LLD = lm;
                    }

                    if (data.UnderlyingDocument != null)
                    {
                        UnderlyingDocModel udm = new UnderlyingDocModel();

                        udm.ID = data.UnderlyingDocument.UnderlyingDocID;
                        udm.Code = data.UnderlyingDocument.UnderlyingDocCode;
                        udm.Name = data.UnderlyingDocument.UnderlyingDocName;

                        datatransaction.UnderlyingDoc = udm;

                        if (udm.Code != null && udm.Code.Equals("999"))
                        {
                            datatransaction.OtherUnderlyingDoc = data.OtherUnderlyingDoc;
                        }
                    }

                    if (data.Type != null)
                    {
                        datatransaction.Type = data.Type;
                    }

                    if (data.BankChargesID != null)
                    {
                        datatransaction.BankCharges = context.Charges.Where(a => a.ChargesID.Equals(data.BankChargesID.Value))
                            .Select(a => new ChargesTypeModel()
                            {
                                ID = a.ChargesID,
                                Code = a.ChargesCode,
                                Description = a.ChargesDescription,
                                LastModifiedBy = a.UpdateDate.HasValue ? a.UpdateBy : a.CreateBy,
                                LastModifiedDate = a.UpdateDate ?? a.CreateDate
                            }).SingleOrDefault();
                    }

                    if (data.AgentChargesID.HasValue)
                    {
                        datatransaction.AgentCharges = context.Charges.Where(a => a.ChargesID.Equals(data.AgentChargesID.Value))
                           .Select(a => new ChargesTypeModel()
                           {
                               ID = a.ChargesID,
                               Code = a.ChargesCode,
                               Description = a.ChargesDescription,
                               LastModifiedBy = a.UpdateDate.HasValue ? a.UpdateBy : a.CreateBy,
                               LastModifiedDate = a.UpdateDate ?? a.CreateDate
                           }).SingleOrDefault();
                    }

                    if (datatransaction.ID > 0)
                    {
                        datatransaction.ChangeRMModel = new List<ChangeRMModel>();
                        List<ChangeRMTransaction> ChangeRMVal = context.ChangeRMTransactions.Where(y => y.TransactionID == datatransaction.ID).ToList();
                        if (ChangeRMVal != null && ChangeRMVal.Count > 0)
                        {
                            foreach (var item in ChangeRMVal)
                            {
                                ChangeRMModel Changeval = new ChangeRMModel();
                                Changeval.Account = item.Account;
                                Changeval.ChangeFromBranchCode = item.ChangeFromBranchCode;
                                Changeval.ChangeFromBU = item.ChangeFromBU;
                                Changeval.ChangeFromRMCode = item.ChangeFromRMCode;
                                Changeval.ChangeRMTransactionID = item.ChangeRMTransactionID;
                                Changeval.ChangeSegmentFrom = item.ChangeSegmentFrom;
                                Changeval.ChangeSegmentTo = item.ChangeSegmentTo;
                                Changeval.ChangeToBranchCode = item.ChangeToBranchCode;
                                Changeval.ChangeToBU = item.ChangeToBU;
                                Changeval.ChangeToRMCode = item.ChangeToRMCode;
                                Changeval.CIF = item.CIF;
                                Changeval.PCCode = item.PCCode;
                                Changeval.Remarks = item.Remarks;
                                Changeval.EATPB = item.EATPB;
                                Changeval.CreateBy = item.CreateBy;
                                Changeval.CreateDate = item.CreateDate;
                                //add 
                                datatransaction.ChangeRMModel.Add(Changeval);
                            };
                        }

                        // get lld code & info from last checker
                        /* var checker = (from a in context.Transactions
                                    where a.WorkflowInstanceID.Value.Equals(instanceID)
                                    select a.TransactionCheckerDatas
                                     .OrderByDescending(b => b.ApproverID)
                                     .FirstOrDefault()
                                    ).SingleOrDefault(); */
                        var checker = context.TransactionCheckerDatas.Where(x => x.TransactionID == datatransaction.ID).Select(x => x).OrderByDescending(b => b.ApproverID).FirstOrDefault();
                        if (checker != null)
                        {
                            //datatransaction.LLDCode = checker.LLDCode;
                            //datatransaction.LLDInfo = checker.LLDInfo;   
                            // newlld
                            datatransaction.Others = checker.Others;

                            if (checker.LLD != null)
                            {
                                LLDModel lm = new LLDModel();

                                lm.ID = checker.LLD.LLDID;
                                lm.Code = checker.LLD.LLDCode;
                                lm.Description = checker.LLD.LLDDescription;

                                datatransaction.LLD = lm;
                            }

                            datatransaction.IsSignatureVerified = checker.IsSignatureVerified;
                            //basri 30-09-2015
                            datatransaction.IsDocumentComplete = datatransaction.IsDocumentComplete;
                            datatransaction.IsCallbackRequired = checker.IsCallbackRequired;
                            datatransaction.IsLOIAvailable = checker.IsLOIAvailable;
                            //end basri
                            datatransaction.IsDormantAccount = checker.IsDormantAccount;
                            datatransaction.IsFreezeAccount = checker.IsFreezeAccount;
                        }
                    }
                    //end   
                }

                transaction = datatransaction;
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }

        //getTransactionCallBack
        public bool GetTransactionCallbackDetails(Guid workflowInstanceID, long approverID, ref TransactionCBOContactDetailModel output, ref string message)
        {
            bool IsSuccess = false; //is

            try
            {
                var transactionID = context.Transactions
                       .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                       .Select(a => a.TransactionID)
                       .SingleOrDefault();

                var callbackID = context.TransactionCallbacks
                    .Where(a => a.IsUTC == false && a.TransactionID.Equals(transactionID))
                    .OrderByDescending(a => a.TransactionCallbackID)
                    .Select(a => a.TransactionCallbackID)
                    .FirstOrDefault();



                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new TransactionCBOContactDetailModel
                          {
                              Callbacks = (from b in context.TransactionCallbacks
                                           //let isCallbackId = callbackID != null ? true : false
                                           where b.TransactionID.Equals(transactionID)
                                           //&& (isCallbackId ? b.TransactionCallbackID > callbackID : true)
                                           select new TransactionCallbackTimeModel
                                           {
                                               ID = b.TransactionCallbackID,
                                               ApproverID = b.ApproverID,
                                               Contact = b.TransactionCallbackContacts.Select(y => new CustomerContactModel()
                                               {
                                                   ID = y.CustomerContact.ContactID,
                                                   Name = y.CustomerContact.ContactName,
                                                   PhoneNumber = y.CustomerContact.PhoneNumber,
                                                   DateOfBirth = y.CustomerContact.DateOfBirth,
                                                   //Address = y.CustomerContact.Address,
                                                   IDNumber = y.CustomerContact.IdentificationNumber,
                                               }).FirstOrDefault(),
                                               Time = b.Time,
                                               IsUTC = b.IsUTC,
                                               ReasonID = b.ReasonID,//added by dani 10-2-2016
                                               Remark = b.Remark
                                           }).ToList(),
                              Verify = a.TransactionCallbackCheckers
                                .Select(z => new VerifyModel()
                                {
                                    ID = z.TransactionColumnID,
                                    Name = z.TransactionColumn.ColumnName,
                                    IsVerified = z.IsVerified
                                }).ToList()
                          }).SingleOrDefault();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        //addCallbackDetails
        public bool AddTransactionCallback(Guid workflowInstanceID, long approverID, TransactionCallbackDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            //feedbackcaller

            using (System.Transactions.TransactionScope ts = new System.Transactions.TransactionScope())
            {
                try
                {
                    // insert trasaction callback data
                    DBS.Entity.TransactionCallbackData dataCallback = new DBS.Entity.TransactionCallbackData()
                    {
                        TransactionID = data.Transaction.ID,
                        ApproverID = approverID,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    };
                    dataCallback.IsSignatureVerified = false;
                    dataCallback.IsDormantAccount = false;
                    dataCallback.IsFreezeAccount = false;
                    dataCallback.ChannelID = 1;
                    dataCallback.Amount = 0;
                    dataCallback.AmountUSD = 0;
                    dataCallback.Rate = 0;
                    dataCallback.ApplicationDate = DateTime.UtcNow;
                    dataCallback.CurrencyID = 1;
                    dataCallback.DebitCurrencyID = 1;

                    context.TransactionCallbackDatas.Add(dataCallback);
                    context.SaveChanges();

                    // insert callback
                    foreach (var item in data.Callbacks)
                    {
                        if (item.Time.Hour > 0)
                        {
                            // Insert transaction callback
                            TransactionCallback callback = new TransactionCallback()
                            {
                                ApproverID = approverID,
                                TransactionID = data.Transaction.ID,
                                Time = item.Time.ToUniversalTime(),
                                IsUTC = item.IsUTC,
                                ReasonID = item.ReasonID,
                                Remark = item.Remark,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().DisplayName
                            };

                            context.TransactionCallbacks.Add(callback);
                            context.SaveChanges();

                            // Insert transaction callback contact
                            if (item.Contact != null)
                            {
                                context.TransactionCallbackContacts.Add(new TransactionCallbackContact()
                                {
                                    TransactionCallbackID = callback.TransactionCallbackID,
                                    ContactID = item.Contact.ID,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });
                            }
                            context.SaveChanges();

                            callback = null;
                        }
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }
        //end

        public bool AddTransactionCheckerDetails(Guid workflowInstanceID, long approverID, TransactionCheckerDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            using (System.Transactions.TransactionScope ts = new System.Transactions.TransactionScope())
            {
                try
                {
                    var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

                    if (transactionID > 0)
                    {
                        //by dani cp dan pk
                        Entity.RetailCIFCBO retailCIFCBO = context.RetailCIFCBOes
                                                      .Where(x => x.TransactionID
                                                      .Equals(transactionID))
                                                      .SingleOrDefault();
                        if (retailCIFCBO != null)
                        {
                            //retailCIFCBO.IsCommingPersonally = data.Transaction.RetailCIFCBO.IsCommingPersonally;
                            //retailCIFCBO.IsPersonallyKnown = data.Transaction.RetailCIFCBO.IsPersonallyKnown;
                            retailCIFCBO.IDS = data.Transaction.RetailCIFCBO.IDS;
                            retailCIFCBO.CallbackIsRequired = data.Transaction.RetailCIFCBO.CallbackIsRequired;
                            retailCIFCBO.UtcApproval = data.Transaction.RetailCIFCBO.UtcApproval;
                            context.SaveChanges();
                        }
                        if (data.Transaction.AccountNumberLienUnlien != null && data.Transaction.AccountNumberLienUnlien.Count > 0)
                        {
                            IList<Entity.LienUnlien> dataLien = context.LienUnliens
                                                      .Where(x => x.TransactionId.Value == transactionID)
                                                      .ToList();
                            if (dataLien != null && dataLien.Count > 0)
                            {
                                for (int i = 0; i < dataLien.Count; i++)
                                {
                                    for (int j = 0; j < data.Transaction.AccountNumberLienUnlien.Count; j++)
                                    {
                                        if (dataLien[i].AccountNumber == data.Transaction.AccountNumberLienUnlien[j].AccountNumber)
                                        {
                                            if (data.Transaction.AccountNumberLienUnlien[j].IsSelected == true)
                                            {
                                                dataLien[i].IsBranchCheckerVerify = data.Transaction.AccountNumberLienUnlien[j].IsBranchCheckerVerify;
                                                dataLien[i].IsCBOAccountCheckerVerify = data.Transaction.AccountNumberLienUnlien[j].IsCBOAccountCheckerVerify;
                                                context.SaveChanges();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        var checker = context.TransactionCheckers
                           .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                           .ToList();

                        #region Transaction Checker
                        if (checker.Count > 0)
                        {
                            var checkerdata = context.TransactionCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                            foreach (var item in checkerdata)
                            {
                                context.TransactionCheckers.Remove(item);
                            }
                            context.SaveChanges();

                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });

                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });

                                context.SaveChanges();
                            }
                        }
                        #endregion

                        #region add attachment
                        if (data.Transaction.Documents != null)
                        {
                            if (data.Transaction.Documents.Count > 0)
                            {
                                var existingDocuments = (from a in context.TransactionDocuments
                                                         where a.TransactionID == transactionID
                                                             && a.IsDeleted == false
                                                         select a);
                                if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                                {
                                    foreach (var item in existingDocuments.ToList())
                                    {

                                        context.TransactionDocuments.Remove(item);
                                        context.SaveChanges();
                                    }
                                    foreach (var item in data.Transaction.Documents)
                                    {

                                        Entity.TransactionDocument document = new Entity.TransactionDocument()
                                        {
                                            TransactionID = transactionID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            IsDeleted = false,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().DisplayName
                                        };

                                        context.TransactionDocuments.Add(document);
                                        context.SaveChanges();
                                    }
                                }
                                else
                                {
                                    foreach (var item in data.Transaction.Documents)
                                    {
                                        Entity.TransactionDocument document = new Entity.TransactionDocument()
                                        {
                                            TransactionID = transactionID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            IsDeleted = false,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().DisplayName
                                        };
                                        context.TransactionDocuments.Add(document);
                                        context.SaveChanges();
                                    }

                                }
                            }
                            else
                            {
                                var existingDocuments = (from a in context.TransactionDocuments
                                                         where a.TransactionID == transactionID
                                                             && a.IsDeleted == false
                                                         select a);

                                if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                                {
                                    foreach (var item in existingDocuments.ToList())
                                    {

                                        context.TransactionDocuments.Remove(item);
                                        context.SaveChanges();
                                    }
                                }
                            }
                        }
                        #endregion

                        checker = null;
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }

        public bool AddTransactionCBOCheckerDetails(Guid workflowInstanceID, long approverID, TransactionCheckerDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            using (System.Transactions.TransactionScope ts = new System.Transactions.TransactionScope())
            {
                try
                {
                    var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

                    if (transactionID > 0)
                    {
                        var checker = context.TransactionRetailCIFCBOCheckers
                           .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                           .ToList();
                        if (data.Transaction.AccountNumberLienUnlien != null && data.Transaction.AccountNumberLienUnlien.Count > 0)
                        {
                            IList<Entity.LienUnlien> dataLien = context.LienUnliens
                                                      .Where(x => x.TransactionId.Value == transactionID)
                                                      .ToList();
                            if (dataLien != null && dataLien.Count > 0)
                            {
                                for (int i = 0; i < dataLien.Count; i++)
                                {
                                    for (int j = 0; j < data.Transaction.AccountNumberLienUnlien.Count; j++)
                                    {
                                        if (dataLien[i].AccountNumber == data.Transaction.AccountNumberLienUnlien[j].AccountNumber)
                                        {
                                            if (data.Transaction.AccountNumberLienUnlien[j].IsSelected == true)
                                            {
                                                dataLien[i].IsBranchCheckerVerify = false;
                                                dataLien[i].IsCBOAccountCheckerVerify = false;
                                                context.SaveChanges();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #region Transaction CBO Checker
                        if (checker.Count > 0)
                        {
                            var checkerdata = context.TransactionRetailCIFCBOCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                            foreach (var item in checkerdata)
                            {
                                context.TransactionRetailCIFCBOCheckers.Remove(item);
                            }
                            context.SaveChanges();

                            foreach (var item in data.Verify)
                            {
                                context.TransactionRetailCIFCBOCheckers.Add(new TransactionRetailCIFCBOChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });

                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            foreach (var item in data.Verify)
                            {
                                context.TransactionRetailCIFCBOCheckers.Add(new TransactionRetailCIFCBOChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });

                                context.SaveChanges();
                            }
                        }
                        #endregion

                        #region Insert CustomerPhonNumber
                        string CIF = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.CIF)
                        .SingleOrDefault();

                        long RetailCIFCBOID = context.RetailCIFCBOes.Where(a => a.TransactionID == transactionID).Select(a => a.RetailCIFCBOID).SingleOrDefault();
                        if (RetailCIFCBOID > 0)
                        {
                            List<RetailCIFCBOContact> RetailCIFCBOContactNumber = context.RetailCIFCBOContacts.Where(a => a.RetailCIFCBOID == RetailCIFCBOID).ToList();

                            if (RetailCIFCBOContactNumber != null && RetailCIFCBOContactNumber.Count > 0)
                            {
                                CustomerPhoneNumber CustomerPhoneNumber = new CustomerPhoneNumber();

                                foreach (var item in RetailCIFCBOContactNumber)
                                {
                                    if (item.ActionType == "3")
                                    {
                                        CustomerPhoneNumber deleted = context.CustomerPhoneNumbers.Where(a => a.RetailCIFCBOContactID == item.RetailCIFCBOContactUpdateID.Value).SingleOrDefault();
                                        if (deleted != null)
                                        {
                                            deleted.IsDeleted = true;
                                            deleted.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                            deleted.UpdateDate = item.UpdateDate != null ? item.UpdateDate : item.CreateDate;
                                            context.SaveChanges();
                                        }
                                    }
                                    else if (item.ActionType == "2")
                                    {
                                        if (item.CountryCode == "" || item.CityCode == "")
                                        {
                                            CustomerPhoneNumber.CountryCode = item.UpdateCountryCode;
                                            CustomerPhoneNumber.CityCode = item.UpdateCityCode;
                                            CustomerPhoneNumber.PhoneNumber = item.UpdatePhoneNumber;
                                            CustomerPhoneNumber.ActionType = item.ActionType;
                                            CustomerPhoneNumber.CIF = CIF;
                                            CustomerPhoneNumber.ContactType = item.ContactType;
                                            CustomerPhoneNumber.CreateBy = item.CreateBy;
                                            CustomerPhoneNumber.CreateDate = item.CreateDate;
                                            CustomerPhoneNumber.RetailCIFCBOContactID = item.RetailCIFCBOContactID;
                                            CustomerPhoneNumber.RetailCIFCBOID = item.RetailCIFCBOID;
                                            CustomerPhoneNumber.IsDeleted = false;
                                            CustomerPhoneNumber.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                            CustomerPhoneNumber.UpdateDate = item.UpdateDate;
                                            context.CustomerPhoneNumbers.Add(CustomerPhoneNumber);
                                            context.SaveChanges();
                                        }
                                        else
                                        {
                                            CustomerPhoneNumber deleted = context.CustomerPhoneNumbers.Where(a => a.RetailCIFCBOContactID == item.RetailCIFCBOContactUpdateID.Value).SingleOrDefault();
                                            if (deleted != null)
                                            {
                                                deleted.IsDeleted = true;
                                                deleted.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                                deleted.UpdateDate = item.UpdateDate;
                                                context.SaveChanges();
                                                CustomerPhoneNumber.CountryCode = item.UpdateCountryCode;
                                                CustomerPhoneNumber.CityCode = item.UpdateCityCode;
                                                CustomerPhoneNumber.PhoneNumber = item.UpdatePhoneNumber;
                                                CustomerPhoneNumber.ActionType = item.ActionType;
                                                CustomerPhoneNumber.CIF = CIF;
                                                CustomerPhoneNumber.ContactType = item.ContactType;
                                                CustomerPhoneNumber.CreateBy = deleted.CreateBy;
                                                CustomerPhoneNumber.CreateDate = deleted.CreateDate;
                                                CustomerPhoneNumber.RetailCIFCBOContactID = item.RetailCIFCBOContactID;
                                                CustomerPhoneNumber.RetailCIFCBOID = item.RetailCIFCBOID;
                                                CustomerPhoneNumber.IsDeleted = false;
                                                CustomerPhoneNumber.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                                CustomerPhoneNumber.UpdateDate = item.UpdateDate != null ? item.UpdateDate : item.CreateDate;
                                                context.CustomerPhoneNumbers.Add(CustomerPhoneNumber);
                                                context.SaveChanges();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        CustomerPhoneNumber.CountryCode = item.CountryCode;
                                        CustomerPhoneNumber.CityCode = item.CityCode;
                                        CustomerPhoneNumber.PhoneNumber = item.PhoneNumber;
                                        CustomerPhoneNumber.ActionType = item.ActionType;
                                        CustomerPhoneNumber.CIF = CIF;
                                        CustomerPhoneNumber.ContactType = item.ContactType;
                                        CustomerPhoneNumber.CreateBy = item.CreateBy;
                                        CustomerPhoneNumber.CreateDate = item.CreateDate;
                                        CustomerPhoneNumber.RetailCIFCBOContactID = item.RetailCIFCBOContactID;
                                        CustomerPhoneNumber.RetailCIFCBOID = item.RetailCIFCBOID;
                                        CustomerPhoneNumber.IsDeleted = false;
                                        CustomerPhoneNumber.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                        CustomerPhoneNumber.UpdateDate = item.UpdateDate;
                                        context.CustomerPhoneNumbers.Add(CustomerPhoneNumber);
                                        context.SaveChanges();
                                    }
                                }
                            }
                        }

                        #endregion
                        checker = null;
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null && ex.InnerException.InnerException != null)
                        message = ex.InnerException.InnerException.Message;
                    else
                        message = ex.Message;

                    ts.Dispose();
                }
            }


            return IsSuccess;
        }
        public bool AddTransactionCBOCheckerDetailsRevise(Guid workflowInstanceID, long approverID, TransactionCheckerDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            using (System.Transactions.TransactionScope ts = new System.Transactions.TransactionScope())
            {
                try
                {
                    var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

                    if (transactionID > 0)
                    {
                        var checker = context.TransactionRetailCIFCBOCheckers
                           .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                           .ToList();
                        if (data.Transaction.AccountNumberLienUnlien != null && data.Transaction.AccountNumberLienUnlien.Count > 0)
                        {
                            IList<Entity.LienUnlien> dataLien = context.LienUnliens
                                                      .Where(x => x.TransactionId.Value == transactionID)
                                                      .ToList();
                            if (dataLien != null && dataLien.Count > 0)
                            {
                                for (int i = 0; i < dataLien.Count; i++)
                                {
                                    for (int j = 0; j < data.Transaction.AccountNumberLienUnlien.Count; j++)
                                    {
                                        if (dataLien[i].AccountNumber == data.Transaction.AccountNumberLienUnlien[j].AccountNumber)
                                        {
                                            if (data.Transaction.AccountNumberLienUnlien[j].IsSelected == true)
                                            {
                                                dataLien[i].IsBranchCheckerVerify = data.Transaction.AccountNumberLienUnlien[j].IsBranchCheckerVerify;
                                                dataLien[i].IsCBOAccountCheckerVerify = data.Transaction.AccountNumberLienUnlien[j].IsCBOAccountCheckerVerify;
                                                context.SaveChanges();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #region Transaction CBO Checker
                        if (checker.Count > 0)
                        {
                            //sama dengan yg di atas 20-9-2017 Arsel
                            //var checkerdata = context.TransactionRetailCIFCBOCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                            foreach (var item in checker)
                            {
                                context.TransactionRetailCIFCBOCheckers.Remove(item);
                            }
                            context.SaveChanges();

                            foreach (var item in data.Verify)
                            {
                                context.TransactionRetailCIFCBOCheckers.Add(new TransactionRetailCIFCBOChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });
                            }
                            context.SaveChanges();
                        }
                        else
                        {
                            foreach (var item in data.Verify)
                            {
                                context.TransactionRetailCIFCBOCheckers.Add(new TransactionRetailCIFCBOChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });

                            }
                            context.SaveChanges();
                        }
                        #endregion

                        checker = null;
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }


            return IsSuccess;
        }
        public bool UpdateTransactionCheckerDetails(Guid workflowInstanceID, long approverID, TransactionCheckerDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            using (System.Transactions.TransactionScope ts = new System.Transactions.TransactionScope())
            {
                try
                {
                    var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

                    if (transactionID > 0)
                    {
                        //update document completed


                        var checker = context.TransactionCheckers
                           .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                           .ToList();

                        if (checker.Count > 0)
                        {
                            var checkerdata = context.TransactionCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                            foreach (var item in checkerdata)
                            {
                                context.TransactionCheckers.Remove(item);
                            }
                            context.SaveChanges();

                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });

                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });

                                context.SaveChanges();
                            }
                        }

                        checker = null;
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }

        public bool UpdateTransactionCBOMakerATMDetails(Guid workflowInstanceID, long approverID, TransactionCBOContactDetailModel transaction, ref string message)
        {
            bool IsSuccess = false;
            int[] MaintenanceType = { 20, 21 };

            using (System.Transactions.TransactionScope ts = new System.Transactions.TransactionScope())
            {
                try
                {
                    Entity.Transaction transactionCBOMaker = context.Transactions
                                                     .Where(x => x.WorkflowInstanceID.Value
                                                     .Equals(workflowInstanceID))
                                                     .SingleOrDefault();
                    if (transactionCBOMaker != null)
                    {
                        transactionCBOMaker.CIF = transaction.Transaction.Customer.CIF;
                        transactionCBOMaker.UpdateDate = DateTime.UtcNow;
                        transactionCBOMaker.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        context.SaveChanges();
                    }

                    Entity.RetailCIFCBO retailCIFCBO = context.RetailCIFCBOes
                                                 .Where(x => x.TransactionID.Equals(transaction.Transaction.ID))
                                                 .SingleOrDefault();
                    if (retailCIFCBO != null)
                    {
                        retailCIFCBO.AtmNumber = transaction.Transaction.RetailCIFCBO.AtmNumber;
                        context.SaveChanges();
                    }

                    ts.Complete();
                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.InnerException.Message;
                }
            }
            return IsSuccess;
        }
        public bool UpdateTransactionCBOMakerDispatchDetails(Guid workflowInstanceID, long approverID, TransactionCBOContactDetailModel transaction, ref string message)
        {
            bool IsSuccess = false;
            using (System.Transactions.TransactionScope ts = new System.Transactions.TransactionScope())
            {
                try
                {
                    Entity.Transaction transactionCBOMaker = context.Transactions
                                                             .Where(x => x.WorkflowInstanceID.Value
                                                             .Equals(workflowInstanceID))
                                                             .SingleOrDefault();
                    if (transactionCBOMaker != null)
                    {
                        transactionCBOMaker.CIF = transaction.Transaction.Customer.CIF;
                        transactionCBOMaker.UpdateDate = DateTime.UtcNow;
                        transactionCBOMaker.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        context.SaveChanges();
                    }

                    Entity.RetailCIFCBO retailCIFCBO = context.RetailCIFCBOes
                                                      .Where(x => x.TransactionID
                                                      .Equals(transaction.Transaction.ID))
                                                      .SingleOrDefault();
                    if (retailCIFCBO != null)
                    {
                        retailCIFCBO.DispatchModeTypeID = transaction.Transaction.RetailCIFCBO.DispatchModeTypeID;
                        context.SaveChanges();
                    }
                    ts.Complete();
                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.InnerException.Message;
                }
            }
            return IsSuccess;
        }

        public bool GetTransactionDetailsChangeRMUploaded(IList<int> activityHistoryID, ref IList<ChangeRMModel> ChangeRMModel, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                ChangeRMModel = (from a in context.ChangeRMs
                                 where activityHistoryID.Contains(a.ActivityHistoryID.Value)
                                 orderby new { a.ChangeRMID }
                                 select new ChangeRMModel
                                 {
                                     ChangeRMTransactionID = a.ChangeRMID,
                                     ActivityHistoryID = a.ActivityHistoryID,
                                     No = a.NO,
                                     CIF = a.CIF,
                                     CustomerName = a.CUSTOMER_NAME,
                                     ChangeSegmentFrom = a.CHANGE_SEGMENT_FROM,
                                     ChangeSegmentTo = a.CHANGE_SEGMENT_TO,
                                     ChangeFromBranchCode = a.CHANGE_FROM_BRANCH_CODE,
                                     ChangeFromRMCode = a.CHANGE_FROM_RM_CODE,
                                     ChangeFromBU = a.CHANGE_FROM_BU,
                                     ChangeToBranchCode = a.CHANGE_TO_BRANCH_CODE,
                                     ChangeToRMCode = a.CHANGE_TO_RM_CODE,
                                     ChangeToBU = a.CHANGE_TO_BU,
                                     Account = a.ACCOUNT__,
                                     PCCode = a.PC_CODE,
                                     EATPB = a.FREE_TEXT_5__EA_TPB_EA_TPB_,
                                     Remarks = a.REMARKS___REASON,
                                     CreateDate = a.CreateDate,
                                     CreateBy = a.CreateBy,
                                     UpdateBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                     UpdateDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value

                                 }).ToList();
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }

        public bool GetTransactionDetailsChangeRMReuploaded(long transactionID, int activityHistoryID, bool isBringUp, ref IList<ChangeRMModel> ChangeRMModel, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                if (!isBringUp)
                {
                    ChangeRMModel = (from b in context.ChangeRMTransactions
                                     where b.TransactionID == transactionID
                                     select new ChangeRMModel
                                     {
                                         ChangeRMTransactionID = b.ChangeRMTransactionID,
                                         ActivityHistoryID = activityHistoryID,
                                         CIF = b.CIF,
                                         Selected = true,
                                         CustomerName = b.Customer.CustomerName,
                                         ChangeSegmentFrom = b.ChangeSegmentFrom,
                                         ChangeSegmentTo = b.ChangeSegmentTo,
                                         ChangeFromBranchCode = b.ChangeFromBranchCode,
                                         ChangeFromRMCode = b.ChangeFromRMCode,
                                         ChangeFromBU = b.ChangeFromBU,
                                         ChangeToBranchCode = b.ChangeToBranchCode,
                                         ChangeToRMCode = b.ChangeToRMCode,
                                         ChangeToBU = b.ChangeToBU,
                                         Account = b.Account,
                                         PCCode = b.PCCode,
                                         EATPB = b.EATPB,
                                         Remarks = b.Remarks,
                                         CreateDate = b.CreateDate,
                                         CreateBy = b.CreateBy,
                                         UpdateBy = b.UpdateDate == null ? b.CreateBy : b.UpdateBy,
                                         UpdateDate = b.UpdateDate == null ? b.CreateDate : b.UpdateDate.Value
                                     }).Concat(from a in context.ChangeRMs
                                               where a.ActivityHistoryID.Value == activityHistoryID
                                               orderby new { a.ChangeRMID }
                                               select new ChangeRMModel
                                               {
                                                   ChangeRMTransactionID = a.ChangeRMID,
                                                   ActivityHistoryID = a.ActivityHistoryID,
                                                   CIF = a.CIF,
                                                   Selected = false,
                                                   CustomerName = a.CUSTOMER_NAME,
                                                   ChangeSegmentFrom = a.CHANGE_SEGMENT_FROM,
                                                   ChangeSegmentTo = a.CHANGE_SEGMENT_TO,
                                                   ChangeFromBranchCode = a.CHANGE_FROM_BRANCH_CODE,
                                                   ChangeFromRMCode = a.CHANGE_FROM_RM_CODE,
                                                   ChangeFromBU = a.CHANGE_FROM_BU,
                                                   ChangeToBranchCode = a.CHANGE_TO_BRANCH_CODE,
                                                   ChangeToRMCode = a.CHANGE_TO_RM_CODE,
                                                   ChangeToBU = a.CHANGE_TO_BU,
                                                   Account = a.ACCOUNT__,
                                                   PCCode = a.PC_CODE,
                                                   EATPB = a.FREE_TEXT_5__EA_TPB_EA_TPB_,
                                                   Remarks = a.REMARKS___REASON,
                                                   CreateDate = a.CreateDate,
                                                   CreateBy = a.CreateBy,
                                                   UpdateBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                   UpdateDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                               }).ToList();
                    ChangeRMModel.AsEnumerable().Select((x, i) => new ChangeRMModel
                    {
                        ChangeRMTransactionID = x.ChangeRMTransactionID,
                        ActivityHistoryID = x.ActivityHistoryID,
                        CIF = x.CIF,
                        No = (i + 1).ToString(),
                        Selected = true,
                        CustomerName = x.CustomerName,
                        ChangeSegmentFrom = x.ChangeSegmentFrom,
                        ChangeSegmentTo = x.ChangeSegmentTo,
                        ChangeFromBranchCode = x.ChangeFromBranchCode,
                        ChangeFromRMCode = x.ChangeFromRMCode,
                        ChangeFromBU = x.ChangeFromBU,
                        ChangeToBranchCode = x.ChangeToBranchCode,
                        ChangeToRMCode = x.ChangeToRMCode,
                        ChangeToBU = x.ChangeToBU,
                        Account = x.Account,
                        PCCode = x.PCCode,
                        EATPB = x.EATPB,
                        Remarks = x.Remarks,
                        CreateDate = x.CreateDate,
                        CreateBy = x.CreateBy,
                        UpdateBy = x.UpdateBy,
                        UpdateDate = x.UpdateDate
                    }).ToList();
                }
                else
                {
                    ChangeRMModel = (from b in context.ChangeRMTransactionDrafts
                                     where b.TransactionID == transactionID
                                     select new ChangeRMModel
                                     {
                                         ChangeRMTransactionID = b.ChangeRMTransactionDraftID,
                                         ActivityHistoryID = activityHistoryID,
                                         CIF = b.CIF,
                                         Selected = true,
                                         CustomerName = b.Customer.CustomerName,
                                         ChangeSegmentFrom = b.ChangeSegmentFrom,
                                         ChangeSegmentTo = b.ChangeSegmentTo,
                                         ChangeFromBranchCode = b.ChangeFromBranchCode,
                                         ChangeFromRMCode = b.ChangeFromRMCode,
                                         ChangeFromBU = b.ChangeFromBU,
                                         ChangeToBranchCode = b.ChangeToBranchCode,
                                         ChangeToRMCode = b.ChangeToRMCode,
                                         ChangeToBU = b.ChangeToBU,
                                         Account = b.Account,
                                         PCCode = b.PCCode,
                                         EATPB = b.EATPB,
                                         Remarks = b.Remarks,
                                         CreateDate = b.CreateDate,
                                         CreateBy = b.CreateBy,
                                         UpdateBy = b.UpdateDate == null ? b.CreateBy : b.UpdateBy,
                                         UpdateDate = b.UpdateDate == null ? b.CreateDate : b.UpdateDate.Value
                                     }).Concat(from a in context.ChangeRMs
                                               where a.ActivityHistoryID.Value == activityHistoryID
                                               orderby new { a.ChangeRMID }
                                               select new ChangeRMModel
                                               {
                                                   ChangeRMTransactionID = a.ChangeRMID,
                                                   ActivityHistoryID = a.ActivityHistoryID,
                                                   CIF = a.CIF,
                                                   Selected = false,
                                                   CustomerName = a.CUSTOMER_NAME,
                                                   ChangeSegmentFrom = a.CHANGE_SEGMENT_FROM,
                                                   ChangeSegmentTo = a.CHANGE_SEGMENT_TO,
                                                   ChangeFromBranchCode = a.CHANGE_FROM_BRANCH_CODE,
                                                   ChangeFromRMCode = a.CHANGE_FROM_RM_CODE,
                                                   ChangeFromBU = a.CHANGE_FROM_BU,
                                                   ChangeToBranchCode = a.CHANGE_TO_BRANCH_CODE,
                                                   ChangeToRMCode = a.CHANGE_TO_RM_CODE,
                                                   ChangeToBU = a.CHANGE_TO_BU,
                                                   Account = a.ACCOUNT__,
                                                   PCCode = a.PC_CODE,
                                                   EATPB = a.FREE_TEXT_5__EA_TPB_EA_TPB_,
                                                   Remarks = a.REMARKS___REASON,
                                                   CreateDate = a.CreateDate,
                                                   CreateBy = a.CreateBy,
                                                   UpdateBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                   UpdateDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                               }).ToList();
                    ChangeRMModel.AsEnumerable().Select((x, i) => new ChangeRMModel
                    {
                        ChangeRMTransactionID = x.ChangeRMTransactionID,
                        ActivityHistoryID = x.ActivityHistoryID,
                        CIF = x.CIF,
                        No = (i + 1).ToString(),
                        Selected = true,
                        CustomerName = x.CustomerName,
                        ChangeSegmentFrom = x.ChangeSegmentFrom,
                        ChangeSegmentTo = x.ChangeSegmentTo,
                        ChangeFromBranchCode = x.ChangeFromBranchCode,
                        ChangeFromRMCode = x.ChangeFromRMCode,
                        ChangeFromBU = x.ChangeFromBU,
                        ChangeToBranchCode = x.ChangeToBranchCode,
                        ChangeToRMCode = x.ChangeToRMCode,
                        ChangeToBU = x.ChangeToBU,
                        Account = x.Account,
                        PCCode = x.PCCode,
                        EATPB = x.EATPB,
                        Remarks = x.Remarks,
                        CreateDate = x.CreateDate,
                        CreateBy = x.CreateBy,
                        UpdateBy = x.UpdateBy,
                        UpdateDate = x.UpdateDate
                    }).ToList();
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }

        #endregion
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}