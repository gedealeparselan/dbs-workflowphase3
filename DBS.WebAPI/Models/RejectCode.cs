﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;


namespace DBS.WebAPI.Models
{
    #region property
    [ModelName("RejectCode")]
    public class RejectCodeModel
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }

    public class RejectCodeRow : RejectCodeModel
    {
        public int RowID { get; set; }
    }
    public class RejectCodeGrid : Grid
    {
        public IList<RejectCodeRow> Rows { get; set; }
    }
    #endregion

    #region filter
    public class RejectCodeFilter : Filter { }
    #endregion
    #region interface
    public interface IRejectCodeRepo : IDisposable
    {
        bool GetRejectedCodeByID(int id, ref RejectCodeModel RejectCodeID, ref string message);
        bool GetRejectCode(ref IList<RejectCodeModel> RejectCode, int limit, int index, ref string message);
        bool GetRejectCode(ref IList<RejectCodeModel> RejectCode, ref string message);
        bool GetRejectCode(int page, int size, IList<RejectCodeFilter> filters, string sortColumn, string sortOrder, ref RejectCodeGrid rejectCode, ref string message);
        bool GetRejectCode(RejectCodeFilter filter, ref IList<RejectCodeModel> RejectCodes, ref string message);
        bool GetRejectCode(string key, int limit, ref IList<RejectCodeModel> RejectCodes, ref string message);
        bool AddRejectCode(RejectCodeModel rejectCode, ref string message);
        bool UpdateRejectCode(int id, RejectCodeModel rejectCode, ref string message);
        bool DeleteRejectCode(int id, ref string message);
    }
    #endregion

    #region Repository
    public class RejectCodeRepository : IRejectCodeRepo
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();


        public bool GetRejectedCodeByID(int id, ref RejectCodeModel RejectCodeID, ref string message)
        {
            bool isSuccess = false;

            try
            {
                RejectCodeID = (from a in context.RejectCodes
                                where a.IsDeleted.Equals(false) && a.RejectCodeID.Equals(id)
                                select new RejectCodeModel
                                {
                                    ID = a.RejectCodeID,
                                    Code = a.Code,
                                    Description = a.RejectCodeDescription,
                                    LastModifiedDate = a.CreateDate,
                                    LastModifiedBy = a.CreateBy
                                }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRejectCode(ref IList<RejectCodeModel> RejectCode, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    RejectCode = (from a in context.RejectCodes
                                  where a.IsDeleted.Equals(false)
                                  orderby new { a.Code, a.RejectCodeDescription }
                                  select new RejectCodeModel
                                  {
                                      ID = a.RejectCodeID,
                                      Code = a.Code,
                                      Description = a.RejectCodeDescription,
                                      LastModifiedBy = a.CreateBy,
                                      LastModifiedDate = a.CreateDate
                                  }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRejectCode(ref IList<RejectCodeModel> RejectCode, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    RejectCode = (from a in context.RejectCodes
                                  where a.IsDeleted.Equals(false)
                                  orderby new { a.Code, a.RejectCodeDescription }
                                  select new RejectCodeModel
                                  {
                                      ID = a.RejectCodeID,
                                      Code = a.Code,
                                      Description = a.RejectCodeDescription,
                                      LastModifiedBy = a.CreateBy,
                                      LastModifiedDate = a.CreateDate
                                  }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRejectCode(int page, int size, IList<RejectCodeFilter> filters, string sortColumn, string sortOrder, ref RejectCodeGrid rejectCode, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                RejectCodeModel filter = new RejectCodeModel();
                if (filters != null)
                {
                    filter.Code = (string)filters.Where(a => a.Field.Equals("Code")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.RejectCodes
                                let isRejectCode = !string.IsNullOrEmpty(filter.Code)
                                let isRejectCodeDescription = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isRejectCode ? a.Code.Contains(filter.Code) : true) &&
                                    (isRejectCodeDescription ? a.RejectCodeDescription.Contains(filter.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new RejectCodeModel
                                {
                                    ID = a.RejectCodeID,
                                    Code = a.Code,
                                    Description = a.RejectCodeDescription,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    rejectCode.Page = page;
                    rejectCode.Size = size;
                    rejectCode.Total = data.Count();
                    rejectCode.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    rejectCode.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new RejectCodeRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Code = a.Code,
                            Description = a.Description,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRejectCode(RejectCodeFilter filter, ref IList<RejectCodeModel> RejectCodes, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetRejectCode(string key, int limit, ref IList<RejectCodeModel> RejectCodes, ref string message)
        {
            bool isSuccess = false;

            try
            {
                RejectCodes = (from a in context.RejectCodes
                               where a.IsDeleted.Equals(false) &&
                                   a.Code.Contains(key) || a.RejectCodeDescription.Contains(key)
                               orderby new { a.Code, a.RejectCodeDescription }
                               select new RejectCodeModel
                               {
                                   Code = a.Code,
                                   Description = a.RejectCodeDescription
                               }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddRejectCode(RejectCodeModel rejectCode, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.RejectCodes.Where(a => a.Code.Trim() == rejectCode.Code.Trim() && a.IsDeleted.Equals(false)).Any())
                {
                    context.RejectCodes.Add(new Entity.RejectCode()
                    {
                        Code = rejectCode.Code,
                        RejectCodeDescription = rejectCode.Description,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Reject Code data for Reject Code  {0} is already exist.", rejectCode.Code);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateRejectCode(int id, RejectCodeModel rejectCode, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.RejectCode data = context.RejectCodes.Where(a => a.RejectCodeID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.RejectCodes.Where(a => a.RejectCodeID != rejectCode.ID && a.Code.Equals(rejectCode.Code) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Reject Code {0} is exist.", rejectCode.Code);
                    }
                    else
                    {
                        data.Code = rejectCode.Code;
                        data.RejectCodeDescription = rejectCode.Description;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Reject Code with id {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteRejectCode(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.RejectCode data = context.RejectCodes.Where(a => a.RejectCodeID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("RejectCode data for RejectCode ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

    #endregion





}