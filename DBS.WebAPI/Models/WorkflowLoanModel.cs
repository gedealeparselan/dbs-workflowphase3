﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Net.Http;
using System.Web.Script.Serialization;
using System.Transactions;

//=======================================================================================//
///FOR ALL DEVELOPER...
///PLEASE ADD NEW REGION BY YOUR NAME TO KEEP CLEAN !!! DONT BE A DIRTY PROGRAMMER!!!
///ANDIWIJAYA
//=======================================================================================//

namespace DBS.WebAPI.Models
{
    #region Property
    #region excel
    public class TransactionRolloverLoanModel
    {
        public bool? Selected { get; set; }
        public long CSORolloverID { get; set; }
        public long TransactionRolloverID { get; set; }
        public ParameterSystemModelFunding ParameterSystem { get; set; }
        public string CIF { get; set; }
        public string CustomerName { get; set; }
        public string LoanContractNo { get; set; }
        public CurrencyModel Currency { get; set; }
        public string SOLID { get; set; }
        public string SchemeCode { get; set; }
        public DateTime? DueDate { get; set; }
        public decimal? AmountDue { get; set; }
        public string LimitID { get; set; }
        public BizSegmentModel BizSegment { get; set; }
        public EmployeeModel RM { get; set; }
        public string CSO { get; set; }
        public bool? IsSBLCSecured { get; set; }
        public bool? IsHeavyEquipment { get; set; }
        public int? MaintenanceTypeID { get; set; }
        public decimal? AmountRollover { get; set; }
        public DateTime? NextPrincipalDate { get; set; }
        public DateTime? NextInterestDate { get; set; }
        public decimal? ApprovedMarginAsPerCM { get; set; }
        public int? InterestRateCodeID { get; set; }
        public decimal? BaseRate { get; set; }
        public decimal? AllInLP { get; set; }
        public decimal? AllInFTPRate { get; set; }
        public decimal? AccountPreferencial { get; set; }
        public decimal? SpecialFTP { get; set; }
        public decimal? SpecialRateMargin { get; set; }
        public decimal? AllInrate { get; set; }
        public string ApprovalDOA { get; set; }
        public string ApprovedBy { get; set; }
        public string CSORemarks { get; set; }
        public string LoanRemarks { get; set; }
        public bool? IsAdhoc { get; set; }
        public string CSOName { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime Updatedate { get; set; }
        public string UpdateBy { get; set; }
        public string DetailLink { get; set; }

    }

    public class TransactionDisbursementLoanModel : TransactionModel
    {
        public long CSOFundingMemoID { get; set; }
        public long TransactionFundingMemoID { get; set; }
        public CustomerModel Customer { get; set; }
        public string LoanContractNo { get; set; }
        public CurrencyModel Currency { get; set; }
        public BizSegmentModel BizSegment { get; set; }
        public ChannelModel Channel { get; set; }
        public DateTime ValueDate { get; set; }
        public ProductModel Product { get; set; }
        public int CustomerCategoryID { get; set; }
        public int CustomerCategoryName { get; set; }
        public int LoanTypeID { get; set; }
        public int LoanTypeName { get; set; }
        public string SolID { get; set; }
        public Location Location { get; set; }
        public int ProgramTypeID { get; set; }
        public ParameterSystemModelFunding ParameterSystem { get; set; }
        public int FinacleSchemeCodeID { get; set; }
        public bool IsAdhoc { get; set; }
        public int CreditingOperativeID { get; set; }
        public int DebitingOperativeID { get; set; }
        public DateTime MaturityDate { get; set; }
        public string InterestCodeID { get; set; }
        public decimal BaseRate { get; set; }
        public decimal AccountPreferencial { get; set; }
        public decimal AllInRate { get; set; }
        public int RepricingPlanID { get; set; }
        public decimal ApprovedMarginAsPerCM { get; set; }
        public decimal SpecialFTP { get; set; }
        public decimal Margin { get; set; }
        public decimal AllInSpecialRate { get; set; }
        public DateTime PeggingDate { get; set; }
        public int PeggingFrequencyID { get; set; }
        public int PrincipalStartDate { get; set; }
        public DateTime InterestStartDate { get; set; }
        public string NoOfInstallment { get; set; }
        public int PrincipalFrequencyID { get; set; }
        public int InterestFrequencyID { get; set; }
        public string LimitIDinFin10 { get; set; }
        public DateTime LimitExpiryDate { get; set; }
        public decimal SanctionLimit { get; set; }
        public CurrencyModel SanctionLimitCurr { get; set; }
        public decimal utilization { get; set; }
        public CurrencyModel UtilizationCurr { get; set; }
        public decimal Outstanding { get; set; }
        public CurrencyModel OutstandingCurr { get; set; }
        public string CSOName { get; set; }
        public string Remarks { get; set; }
        public string OtherRemarks { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }


    }

    public class RolloverLoanCheckerModel
    {
        public bool IsChecked { get; set; }
        public long LoanRolloverID { get; set; }
        public long TransactionRolloverID { get; set; }
        public string CIF { get; set; }
        public string CustomerName { get; set; }
        public string LoanContractNo { get; set; }
        public CurrencyModel Currency { get; set; }
        public string SOLID { get; set; }
        public string SchemeCode { get; set; }
        public DateTime? DueDate { get; set; }
        public decimal? AmountDue { get; set; }
        public string LimitID { get; set; }
        public BizSegmentModel BizSegment { get; set; }
        public EmployeeModel Employee { get; set; }
        public string CSO { get; set; }
        public string IsSBLCSecured { get; set; }
        public string IsHeavyEquipment { get; set; }
        public int? MaintenanceTypeID { get; set; }
        public decimal? AmountRollover { get; set; }
        public DateTime? NextPrincipalDate { get; set; }
        public DateTime? NextInterestDate { get; set; }
        public decimal? ApprovedMarginAsPerCM { get; set; }
        public int? InterestRateCodeID { get; set; }
        public decimal? BaseRate { get; set; }
        public decimal? AllInLP { get; set; }
        public decimal? AllInFTPRate { get; set; }
        public decimal? AccountPreferencial { get; set; }
        public decimal? SpecialFTP { get; set; }
        public decimal? SpecialRateMargin { get; set; }
        public decimal? AllInrate { get; set; }
        public string ApprovalDOA { get; set; }
        public string ApprovedBy { get; set; }
        public string CSORemarks { get; set; }
        public bool? RemarksLoan { get; set; }
        public string IsAdhoc { get; set; }
        public string CSOName { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime Updatedate { get; set; }
        public string UpdateBy { get; set; }
        public string DetailLink { get; set; }
        public IList<TransactionDocumentModel> Documents { get; set; }


        public long? InstructionID { get; set; }

        public DateTime ValueDate { get; set; }

        public EmployeeModel RM { get; set; }

        public string MaintenanceTypeName { get; set; }

        public string InterestRateCodeName { get; set; }

        public string Remarks { get; set; }

        public string LinkAddHoc { get; set; }

        public DateTime? PeggingReviewDate { get; set; }
    }
    public class LoanCheckerMakerModel
    {
        public EmployeeModel RM { get; set; }
        public long TransactionRolloverID { get; set; }
        public long CSORollOverID { get; set; }
        public string CIF { get; set; }
        public string CustomerName { get; set; }
        public string LoanContractNo { get; set; }
        public CurrencyModel Currency { get; set; }
        public string SOLID { get; set; }
        public string SchemeCode { get; set; }
        public DateTime? DueDate { get; set; }
        public decimal? AmountDue { get; set; }
        public string LimitID { get; set; }
        public BizSegmentModel BizSegment { get; set; }
        //public EmployeeModel RM { get; set; }
        public string CSO { get; set; }
        public string IsSBLCSecured { get; set; }
        public string IsHeavyEquipment { get; set; }
        public int? MaintenanceTypeID { get; set; }
        public decimal? AmountRollover { get; set; }
        public DateTime? NextPrincipalDate { get; set; }
        public DateTime? NextInterestDate { get; set; }
        public decimal? ApprovedMarginAsPerCM { get; set; }
        public int? InterestRateCodeID { get; set; }
        public decimal? BaseRate { get; set; }
        public decimal? AllInLP { get; set; }
        public decimal? AllInFTPRate { get; set; }
        public decimal? AccountPreferencial { get; set; }
        public decimal? SpecialFTP { get; set; }
        public decimal? SpecialRateMargin { get; set; }
        public decimal? AllInrate { get; set; }
        public string ApprovalDOA { get; set; }
        public string ApprovedBy { get; set; }
        public string Remarks { get; set; }
        public string IsAdhoc { get; set; }
        public string CSOName { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? Updatedate { get; set; }
        public string UpdateBy { get; set; }
        public string DetailLink { get; set; }
        public bool IsChecked { get; set; }
        public IList<TransactionDocumentModel> Documents { get; set; }


        public long? InstructionID { get; set; }

        //public DateTime ValueDate { get; set; }

        public DateTime ValueDate { get; set; }

        public string MaintenanceTypeName { get; set; }

        public string InterestRateCodeName { get; set; }

        public string LinkAddHoc { get; set; }

        public EmployeeModel Employee { get; set; }

        public DateTime? PeggingReviewDate { get; set; }
    }
    public class TransactionInterestMaintenanceLoanModel
    {
        public bool IsChecked { get; set; }
        public long LoanInterestMaintenanceID { get; set; }
        public long CSOInterestMaintenanceID { get; set; }
        public long TransactionInterestMaintenanceID { get; set; }
        public string CIF { get; set; }
        public string CustomerName { get; set; }
        public string LoanContractNo { get; set; }
        public BizSegmentModel BizSegment { get; set; }
        public int CurrencyID { get; set; }
        public decimal? AmountDue { get; set; }
        public string LimitID { get; set; }
        public string SolID { get; set; }
        public string SchemeCode { get; set; }
        public DateTime? DueDate { get; set; }
        public decimal PreviousInterestRate { get; set; }
        public decimal CurrentInterestRate { get; set; }
        public int BizSegmentID { get; set; }
        public long EmployeeID { get; set; }
        public string CSO { get; set; }
        public string IsSBLCSecured { get; set; }
        public string IsHeavyEquipment { get; set; }
        public int? MaintenanceTypeID { get; set; }
        public decimal? AmountRollover { get; set; }
        public DateTime? NextPrincipalDate { get; set; }
        public DateTime? NextInterestDate { get; set; }
        public decimal? ApprovedMarginAsPerCM { get; set; }
        public int? InterestRateCodeID { get; set; }
        public decimal? BaseRate { get; set; }
        public decimal? AllinFTPRate { get; set; }
        public decimal? AccountPreferencial { get; set; }
        public decimal? SpecialFTP { get; set; }
        public decimal? SpecialRate { get; set; }
        public decimal? AllinRate { get; set; }
        public string ApprovalDOA { get; set; }
        public string ApprovedBy { get; set; }
        public string Remarks { get; set; }
        public string OtherRemarks { get; set; }
        public string IsAdhoc { get; set; }
        public string CSOName { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public string DetailLink { get; set; }
        public IList<TransactionDocumentModel> Documents { get; set; }
        public CurrencyModel Currency { get; set; }

        public long? InstructionID { get; set; }

        //public DateTime ValueDate { get; set; }

        public DateTime? ValueDate { get; set; }

        public string InterestRateCodeName { get; set; }

        public EmployeeModel Employee { get; set; }

        public DateTime? PeggingReviewDate { get; set; }
    }
    public class LoanCheckerDetailLoanModel
    {
        public TransactionLoanModel Transaction { get; set; }
        public TransactionDisbursementLoanModel LoanDisbursement { get; set; }
        public IList<TransactionRolloverLoanModel> LoanRollover { get; set; }
        public IList<TransactionInterestMaintenanceLoanModel> IM { get; set; }
        public IList<TransactionTimelineLoanModel> Timelines { get; set; }
        public IList<VerifyLoanModel> Verify { get; set; }
        public TransactionCheckerDataLoanModel Checker { get; set; }
    }
    public class DataCheckerRollover
    {
        public TransactionLoanScheduledSettlementModel Transaction { get; set; }
        public IList<RolloverLoanCheckerModel> CheckerRollover { get; set; }
        public IList<TransactionTimelineLoanModel> Timelines { get; set; }
        public TransactionCheckerDataLoanModel Checker { get; set; }

    }
    public class LoanCheckerInterestMaintenance
    {
        public TransactionLoanScheduledSettlementModel Transaction { get; set; }
        public IList<TransactionInterestMaintenanceLoanModel> LoanCheckerMakerIM { get; set; }
        public IList<TransactionTimelineLoanModel> Timelines { get; set; }

    }
    public class LoanCheckerRollover
    {
        public TransactionLoanScheduledSettlementModel Transaction { get; set; }
        public IList<LoanCheckerMakerModel> LoanCheckerMakerRollover { get; set; }
        public IList<TransactionTimelineLoanModel> Timelines { get; set; }

    }
    public class TransactionLoanScheduledSettlementModel
    {
        /// <summary>
        /// Workflow Instance ID
        /// </summary>
        public Guid? WorkflowInstanceID { get; set; }

        /// <summary>
        /// Transaction ID.
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Application ID
        /// </summary>
        public string ApplicationID { get; set; }

        /// <summary>
        /// Approver ID
        /// </summary>
        public long? ApproverID { get; set; }

        /// <summary>
        /// Product Details
        /// </summary>
        public ProductModel Product { get; set; }

        /// <summary>
        /// Channel Details
        /// </summary>
        public ChannelModel Channel { get; set; }

        /// <summary>
        /// Create Date
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Top Urgent
        /// </summary>
        public bool IsTopUrgent { get; set; }

        /// <summary>
        /// Top Urgent Chain
        /// </summary>
        public bool IsTopUrgentChain { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// List Transaction Document Details
        /// </summary>
        public IList<TransactionDocumentModel> Documents { get; set; }



        public BizSegmentModel BizSegment { get; set; }
    }
    public class LoanInterestMaintenance
    {
        public bool IsChecked { get; set; }
        public decimal? AccountPreferencial { get; set; }
        public decimal? AllinFTPRate { get; set; }
        public decimal? AllinRate { get; set; }
        public decimal? AmountRollover { get; set; }
        public string ApprovalDOA { get; set; }
        public string ApprovedBy { get; set; }
        public decimal? ApprovedMarginAsPerCM { get; set; }
        public decimal? BaseRate { get; set; }
        public BizSegmentModel BizSegment { get; set; }
        public string CIF { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string CSO { get; set; }
        public string CSOName { get; set; }
        public CurrencyModel Currency { get; set; }
        public int? CurrencyID { get; set; }
        public decimal? CurrentInterestRate { get; set; }
        public string CustomerName { get; set; }
        public DateTime? DueDate { get; set; }
        public EmployeeModel Employee { get; set; }
        public int? InterestRateCodeID { get; set; }
        public string IsAdhoc { get; set; }
        public string IsHeavyEquipment { get; set; }
        public string IsSBLCSecured { get; set; }
        public string DetailLink { get; set; }
        public IList<TransactionDocumentModel> Documents { get; set; }
        public string LoanContractNo { get; set; }
        public long LoanInterestMaintenanceID { get; set; }
        public int? MaintenanceTypeID { get; set; }
        public DateTime? NextInterestDate { get; set; }
        public DateTime? NextPrincipalDate { get; set; }
        public string OtherRemarks { get; set; }
        public decimal? PreviousInterestRate { get; set; }
        public string Remarks { get; set; }
        public string SchemeCode { get; set; }
        public string SolID { get; set; }
        public decimal? SpecialFTP { get; set; }
        public decimal? SpecialRate { get; set; }
        public string UpdateBy { get; set; }
        public long TransactionInterestMaintenanceID { get; set; }
        public DateTime? UpdateDate { get; set; }

        public DateTime? ValueDate { get; set; }

        public string InterestRateCodeName { get; set; }

        public DateTime? PeggingReviewDate { get; set; }
    }
    public class DataCheckerRolloverDetail
    {
        public TransactionLoanScheduledSettlementModel Transaction { get; set; }
        public RolloverLoanCheckerModel LoanCheckerRollover { get; set; }
    }
    public class InterestMaintenanceExcelCheckerLoan
    {
        public TransactionLoanScheduledSettlementModel Transaction { get; set; }
        public IList<LoanInterestMaintenance> LoanCheckerIM { get; set; }
        public IList<TransactionTimelineLoanModel> Timelines { get; set; }
    }
    public class LoanCheckerRolloverDetails
    {
        public TransactionLoanScheduledSettlementModel Transaction { get; set; }
        public LoanCheckerMakerModel LoanCheckerRollover { get; set; }
    }
    public class TransactionCSODetailModel
    {
        public TransactionCSOModel Transaction { get; set; }
        public IList<TransactionTimelineLoanModel> Timelines { get; set; }
        public TransactionCheckerDataModel Checker { get; set; }
        public IList<VerifyLoanModel> Verify { get; set; }
    }
    public class DataMakerIMDetail
    {
        public TransactionLoanScheduledSettlementModel Transaction { get; set; }
        public TransactionInterestMaintenanceLoanModel LoanCheckerRollover { get; set; }
    }
    #endregion
    #region PropertyGeneralLoan

    public class TransactionTimelineLoanModel
    {
        public long ApproverID { get; set; }
        public string Activity { get; set; }
        public DateTime? Time { get; set; }
        public string Outcome { get; set; }
        public string UserOrGroup { get; set; }
        public bool IsGroup { get; set; }
        public string DisplayName { get; set; }
        public string Comment { get; set; }
    }
    public class VerifyLoanModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool IsVerified { get; set; }
    }
    public class TransactionLoanModel
    {
        //Andriyanto 16 nov 2015
        public string TranID { get; set; }
        public string SolID { get; set; }
        public bool? IsFNACore { get; set; }
        public int? FunctionType { get; set; }
        public string strfunctionType { get; set; }
        public string strAccountType { get; set; }
        public string InvestmenName { get; set; }
        public int? RiskScore { get; set; }
        public int? AccountType { get; set; }
        public DateTime? RiskProfileExpiryDate { get; set; }
        public string OperativeAccount { get; set; }
        public DateTime? CRiskEfectiveDate { get; set; }
        public VerifyModel Verify { get; set; }
        public InvestmentIDModel Investmen { get; set; }
        public TransactionTypeParameterModel Transactiontype { get; set; }
        //end
        public long TrasnsactionID { get; set; }

        /// <summary>
        /// Workflow Instance ID
        /// </summary>
        public Guid? WorkflowInstanceID { get; set; }

        /// <summary>
        /// Transaction ID.
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Application ID
        /// </summary>
        public string ApplicationID { get; set; }
        public DateTime ApplicationDate { get; set; }

        /// <summary>
        /// Top Urgent
        /// </summary>
        public int? IsTopUrgent { get; set; }
        /// <summary>
        /// Top Urgent
        /// </summary>
        public int? IsTopUrgentChain { get; set; }
        /// <summary>
        /// Customer Data Details
        /// </summary>
        public CustomerModel Customer { get; set; }
        /// <summary>
        /// Others
        /// </summary>
        public string Others { get; set; }

        /// <summary>
        /// Product Details
        /// </summary>
        public ProductModel Product { get; set; }

        /// <summary>
        /// Currency Details
        /// </summary>
        public CurrencyModel Currency { get; set; }

        /// <summary>
        /// Transaction Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Rate
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Eqv.USD
        /// </summary>
        public decimal AmountUSD { get; set; }

        /// <summary>
        /// Channel Details
        /// </summary>
        public ChannelModel Channel { get; set; }


        /// <summary>
        /// Application Date
        /// </summary>
        public DateTime? ValueDate { get; set; }

        public string LoanContractNo { get; set; }

        /// <summary>
        /// Bene Segment Details
        /// </summary>
        public BizSegmentModel BizSegment { get; set; }
        public bool IsSignatureVerified { get; set; }

        /// <summary>
        /// Dormant Account
        /// </summary>
        public bool IsDormantAccount { get; set; }
        public bool? IsSyndication { get; set; }
        public bool IsStatementLetterCopy { get; set; }
        /// <summary>
        /// Freeze Account
        /// </summary>
        public bool IsFreezeAccount { get; set; }
        /// <summary>
        /// IsDocumentComplete
        /// </summary>
        public bool IsDocumentComplete { get; set; }
        /// <summary>
        /// IsCallbackRequired
        /// </summary>
        public bool IsCallbackRequired { get; set; }
        /// <summary>
        /// IsLOIAvailable
        /// </summary>
        public bool IsLOIAvailable { get; set; }
        /// <summary>
        /// ApproverID
        /// </summary>
        public long? ApproverID { get; set; }
        /// <summary>
        /// CreateDate
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// List Transaction Document Details
        /// </summary>
        public IList<TransactionDocumentModel> Documents { get; set; }
        /// <summary>
        /// FundingMemo
        /// </summary>
        public FundingMemoLTModel FundingMemoAdd { get; set; }
        public FundingMemoTranModel FundingMemo { get; set; }
        public IList<FundingMemoDocumentModel> fundingmemodocument { get; set; }

        public bool? IsOtherAccountNumber { get; set; }
        public string OtherAccountNumber { get; set; }
        public CustomerAccountModel Account { get; set; }
        public CurrencyModel DebitCurrency { get; set; }
        public BankModel Bank { get; set; }
        public string LoanTransID { get; set; }
        public int SourceID { get; set; }
    }

    public class TransactionCompleteLoanModel
    {
        public TransactionLoanModel Transaction { get; set; }
        public TransactionDisbursementModel Disbursement { get; set; }
        public IList<TransactionRolloverModel> LoanRollover { get; set; }
        public IList<TransactionInterestMaintenanceModel> LoanIM { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public IList<TransactionCallbackTimeLoanModel> Callbacks { get; set; }
    }

    public class TransactionUpcountryBranchCheckerModel
    {
        public TransactionLoanModel Transaction { get; set; }
        public TransactionDisbursementModel Disbursement { get; set; }
        public IList<TransactionRolloverModel> LoanRollover { get; set; }
        public IList<TransactionInterestMaintenanceModel> LoanIM { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public TransactionCallbackDetailLoanModel Callbacks { get; set; }
        public IList<VerifyLoanModel> Verify { get; set; }
    }
    public class TransactionCheckerDataLoanModel
    {
        public long ID { get; set; }
        //public string LLDCode { get; set; }
        //public string LLDInfo { get; set; }

        public string Others { get; set; }

        /// <summary>
        /// Signature Verification
        /// </summary>
        public bool IsSignatureVerified { get; set; }
        public bool? IsSyndication { get; set; }

        /// <summary>
        /// Dormant Account
        /// </summary>
        public bool IsDormantAccount { get; set; }

        /// <summary>
        /// Freeze Account
        /// </summary>
        public bool IsFreezeAccount { get; set; }

        public bool IsCallbackRequired { get; set; }
        public bool IsLOIAvailable { get; set; }
        public bool IsStatementLetterCopy { get; set; }
        public bool IsDocComplete { get; set; }
    }
    public class TransactionCallbackTimeLoanModel
    {
        public long ID { get; set; }
        public long ApproverID { get; set; }
        public CustomerContactModel Contact { get; set; }
        public DateTime Time { get; set; }
        public bool? IsUTC { get; set; }
        public string Remark { get; set; }
        //public DateTime ModifiedDate { get; set; }
        //public string ModifiedBy { get; set; }
    }
    #endregion
    #region Property Fandi
    public class TransactionPPUCheckerCSODetailModel
    {
        public TransactionCSOModel Transaction { get; set; }
        public IList<TransactionTimelineLoanModel> Timelines { get; set; }
        public IList<VerifyLoanModel> Verify { get; set; }
    }
    public class TransactionCSOModel
    {
        public string LoanTransID { get; set; }
        public long TrasnsactionID { get; set; }

        /// <summary>
        /// Workflow Instance ID
        /// </summary>
        public Guid? WorkflowInstanceID { get; set; }

        /// <summary>
        /// Transaction ID.
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Application ID
        /// </summary>
        public string ApplicationID { get; set; }

        /// <summary>
        /// Top Urgent
        /// </summary>
        public int? IsTopUrgent { get; set; }
        /// <summary>
        /// Top Urgent
        /// </summary>
        public int? IsTopUrgentChain { get; set; }
        /// <summary>
        /// Customer Data Details
        /// </summary>
        public CustomerModel Customer { get; set; }
        /// <summary>
        /// Others
        /// </summary>
        public string Others { get; set; }

        /// <summary>
        /// Product Details
        /// </summary>
        public ProductModel Product { get; set; }

        /// <summary>
        /// Currency Details
        /// </summary>
        public CurrencyModel Currency { get; set; }

        /// <summary>
        /// Transaction Amount
        /// </summary>
        public decimal Amount { get; set; }


        public int SourceID { get; set; }
        /// <summary>
        /// Rate
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Eqv.USD
        /// </summary>
        public decimal AmountUSD { get; set; }

        /// <summary>
        /// Channel Details
        /// </summary>
        public ChannelModel Channel { get; set; }


        /// <summary>
        /// Application Date
        /// </summary>
        public DateTime? ValueDate { get; set; }

        /// <summary>
        /// Bene Segment Details
        /// </summary>
        /// 
        public string LoanContractNo { get; set; }
        public BizSegmentModel BizSegment { get; set; }
        public bool? IsSignatureVerified { get; set; }

        /// <summary>
        /// Dormant Account
        /// </summary>
        public bool? IsDormantAccount { get; set; }

        /// <summary>
        /// Freeze Account
        /// </summary>
        public bool? IsFreezeAccount { get; set; }
        /// <summary>
        /// IsDocumentComplete
        /// </summary>
        public bool IsDocumentComplete { get; set; }
        /// <summary>
        /// IsCallbackRequired
        /// </summary>
        public bool IsCallbackRequired { get; set; }
        /// <summary>
        /// IsLOIAvailable
        /// </summary>
        public bool IsLOIAvailable { get; set; }
        /// <summary>
        /// ApproverID
        /// </summary>
        public long? ApproverID { get; set; }
        /// <summary>
        /// CreateDate
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// List Transaction Document Details
        /// </summary>
        public IList<TransactionDocumentModel> Documents { get; set; }
        /// <summary>
        /// FundingMemo
        /// </summary>
        public FundingMemoLTModel FundingMemo { get; set; }

    }
    #endregion
    #region PropertyAndri
    #region LoanCheker
    //AndriYanto 2 Desember 2015
    public class LoanPPUCheckerModel
    {
        public TransactionLoanModel Transaction { get; set; }
        public IList<TransactionTimelineLoanModel> Timelines { get; set; }
        public IList<VerifyLoanModel> Verify { get; set; }
        public TransactionCheckerDataLoanModel Checker { get; set; }
        public IList<TransactionCallbackTimeLoanModel> Callbacks { get; set; }
    }
    //End

    #endregion
    #region loanMaker
    public class TransactionCheckerAfterCallbackDetailLoanModel
    {
        public TransactionLoanModel Transaction { get; set; }
        public IList<TransactionTimelineLoanModel> Timelines { get; set; }
        public TransactionCheckerDataLoanModel Checker { get; set; }
        public IList<VerifyLoanModel> Verify { get; set; }
    }
    public class TransactionCallbackDetailLoanModel
    {
        public TransactionLoanModel Transaction { get; set; }
        public IList<TransactionTimelineLoanModel> Timelines { get; set; }
        public IList<TransactionCallbackTimeLoanModel> Callbacks { get; set; }
        public IList<VerifyLoanModel> Verify { get; set; }
    }
    #endregion
    #region loanSettlement
    public class LoanMakerSettlementUnscheduledDetailModel
    {
        public LoanMakerSettlementUnscheduledModel Transaction { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public IList<VerifyLoanModel> Verify { get; set; }
        public TransactionCheckerDataLoanModel Checker { get; set; }
    }
    public class LoanMakerSettlementUnscheduledModel
    {
        public Guid? WorkflowInstanceID { get; set; }
        public long ID { get; set; }
        public string ApplicationID { get; set; }
        public long TransactionID { get; set; }
        public CustomerModel Customer { get; set; }
        public ProductModel Product { get; set; }
        public BizSegmentModel BizSegment { get; set; }
        public CurrencyModel Currency { get; set; }
        public decimal Amount { get; set; }
        public ChannelModel Channel { get; set; }
        public DateTime? ValueDate { get; set; }
        public string LoanContractNo { get; set; }
        public string LoanTransID { get; set; }
        public long? ApproverID { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public IList<TransactionDocumentModel> Documents { get; set; }

        public int? IsTopUrgent { get; set; }
        public int SourceID { get; set; }
    }
    #endregion
    #endregion
    #region basri
    public class TransactionCheckerLoanScheduledSettlementDetailModel
    {
        public TransactionLoanScheduledSettlementModel Transaction { get; set; }
        public ScheduledSettlementModel ScheduledSettlementLoan { get; set; }


    }
    public class ScheduledSettlementModel
    {
        public long TransactionRolloverID { get; set; }
        public long CSORollOverID { get; set; }
        public string CIF { get; set; }
        public string CustomerName { get; set; }
        public string LoanContractNo { get; set; }
        public CurrencyModel Currency { get; set; }
        public string SOLID { get; set; }
        public string SchemeCode { get; set; }
        public DateTime? DueDate { get; set; }
        public decimal? AmountDue { get; set; }
        public string LimitID { get; set; }
        public BizSegmentModel BizSegment { get; set; }
        public EmployeeModel RM { get; set; }
        public string CSO { get; set; }
        public bool IsSBLCSecured { get; set; }
        public bool IsHeavyEquipment { get; set; }
        public int? MaintenanceTypeID { get; set; }
        public decimal? AmountRollover { get; set; }
        public DateTime? NextPrincipalDate { get; set; }
        public DateTime? NextInterestDate { get; set; }
        public decimal? ApprovedMarginAsPerCM { get; set; }
        public int? InterestRateCodeID { get; set; }
        public decimal? BaseRate { get; set; }
        public decimal? AllInLP { get; set; }
        public decimal? AllInFTPRate { get; set; }
        public decimal? AccountPreferencial { get; set; }
        public decimal? SpecialFTP { get; set; }
        public decimal? SpecialRateMargin { get; set; }
        public decimal? AllInrate { get; set; }
        public string ApprovalDOA { get; set; }
        public string ApprovedBy { get; set; }
        public string Remarks { get; set; }
        public bool? IsAdhoc { get; set; }
        public string CSOName { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? Updatedate { get; set; }
        public string UpdateBy { get; set; }
        public string DetailLink { get; set; }
        public bool IsChecked { get; set; }
        public IList<TransactionDocumentModel> Documents { get; set; }

    }

    //End Basri
    public class TransactionCheckerLoanScheduledSettlementModel
    {
        public TransactionLoanScheduledSettlementModel Transaction { get; set; }
        public IList<ScheduledSettlementModel> ScheduledSettlementLoan { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }

    }
    #endregion
    #endregion

    #region Interface
    public interface IWorkflowLoanRepository : IDisposable
    {
        #region InterfaceGeneralLoan
        bool GetTransactionLoanTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineLoanModel> timelines, ref string message);

        #endregion

        #region InterfaceAndri
        bool AddCSOReviseTask(Guid workflowInstanceID, long approverID, TransactionPPUCheckerCSODetailModel data, ref string message);
        bool UpdateReviseCSOTask(Guid workflowInstanceID, long approverID, TransactionPPUCheckerCSODetailModel data, ref string message);

        #region CSO
        bool AddLoanMakerTask(Guid workflowInstanceID, long approverID, LoanCheckerDetailLoanModel data, ref string message);
        bool GetTransactionCSODetails(Guid instanceID, ref TransactionCSOModel transaction, ref string message);
        bool AddTransactionCSODetails(Guid workflowInstanceID, long approverID, TransactionPPUCheckerCSODetailModel data, ref string message);
        bool GetCheckerCSOLoan(Guid workflowInstanceID, long approverID, ref TransactionPPUCheckerCSODetailModel output, ref string message);
        #endregion
        #region LoanPPUChecker
        #region Revise Excel
        #region LoanPPUChecker
        bool AddReviseCSOChecker(Guid workflowInstanceID, long approverID, TransactionPPUCheckerCSODetailModel output, ref string message);
        bool UpdateTransactionIM(Guid workflowInstanceID, long approverID, LoanCheckerInterestMaintenance data, ref string message);
        bool UpdateTransactionRollover(Guid workflowInstanceID, long approverID, LoanCheckerRollover data, ref string message);
        bool ReviseTransactionIMChecker(Guid workflowInstanceID, long approverID, InterestMaintenanceExcelCheckerLoan data, ref string message);
        bool ReviseTransactionRolloverChecker(Guid workflowInstanceID, long approverID, DataCheckerRollover data, ref string message);
        #endregion
        bool UpdateTransactionCheckerDetails(Guid workflowInstanceID, long approverID, LoanPPUCheckerModel data, ref string message);
        bool UpdateTransactionUpcountryCheckerDetails(Guid workflowInstanceID, long approverID, IList<VerifyLoanModel> data, ref string message);
        bool GetTransactionLoanPPUChecker(Guid workflowInstanceID, long approverID, ref TransactionLoanModel transaction, ref string message);
        bool GetCheckerLoanPPUchecker(Guid workflowInstanceID, long approverID, ref LoanPPUCheckerModel output, ref string message);
        bool AddPPUCheckerLoan(Guid workflowInstanceID, long approverID, LoanPPUCheckerModel data, ref string message);
        bool GetLoanDisburseDetails(Guid workflowInstanceID, long approverID, ref TransactionDisbursementModel output, ref string message);

        bool GetLoanUpcountryBranchCheckerDetails(Guid workflowInstanceID, long approverID, ref TransactionLoanModel transaction, ref IList<VerifyLoanModel> verify, ref TransactionDisbursementModel Disbursement, ref IList<TransactionRolloverModel> rollover, ref IList<TransactionInterestMaintenanceModel> im, ref string message);

        #endregion
        #region LoanPPUMaker
        bool GetTransactionLoanPPUMaker(Guid workflowInstanceID, long approverID, ref TransactionLoanModel transaction, ref string message);
        bool GetCheckerPPULoanMaker(Guid workflowInstanceID, long approverID, ref LoanPPUCheckerModel output, ref string message);
        bool AddTransactionLoanMaker(Guid workflowInstanceID, long approverID, TransactionLoanModel transaction, ref string message);
        #endregion
        #region loancaller
        bool GetTransactionLoanCaller(Guid workflowInstanceID, long approverID, ref TransactionLoanModel transaction, ref string message);
        bool GetTransactionCallbackDetails(Guid workflowInstanceID, long approverID, ref TransactionCallbackDetailLoanModel output, ref string message);
        bool AddTransactionCallback(Guid workflowInstanceID, long approverID, LoanPPUCheckerModel data, ref string message);
        #endregion
        #region excel
        bool AddDataLoanRollover(Guid workflowInstanceID, long approverID, ref DataCheckerRollover output, ref string message);
        bool GetDataRolloverDetail(Guid workflowInstanceID, long LoanRolloverID, ref DataCheckerRolloverDetail output, ref string message);
        bool GetDataImDetail(Guid workflowInstanceID, long LoanRolloverID, ref DataMakerIMDetail output, ref string message);
        bool GetDataRollover(Guid workflowInstanceID, long approverID, ref LoanCheckerRollover output, ref string message);
        bool GetDataRolloverCSO(Guid workflowInstanceID, long approverID, ref LoanCheckerRollover output, ref string message);
        bool GetScheduledSettlementDetails(Guid workflowInstanceID, long csoRolloverID, ref LoanCheckerRolloverDetails output, ref string message);
        bool AddDataRolloverChecker(Guid workflowInstanceId, ref DataCheckerRollover output, long approverID, ref string message);
        bool GetLoanDetails(Guid workflowInstanceId, ref TransactionCSOModel output, ref string message);
        bool AddDataRollover(Guid workflowInstanceId, ref LoanCheckerRollover output, long approverID, ref string message);
        bool GetDataIM(Guid workflowInstanceID, long approverID, ref LoanCheckerInterestMaintenance output, ref string message);
        bool GetDataIMCSO(Guid workflowInstanceID, long approverID, ref LoanCheckerInterestMaintenance output, ref string message);
        bool GetDataSettlementScheduled(Guid workflowInstanceID, long approverID, ref LoanCheckerRollover output, ref string message);
        bool GetTransactionDetailsScheduledSettlement(Guid workflowInstanceID, ref TransactionLoanScheduledSettlementModel transaction, ref string message);
        bool GetTransactionExcel(Guid workflowInstanceID, ref TransactionLoanScheduledSettlementModel transaction, ref string message);
        bool GetDataRolloverLoanChecker(Guid workflowInstanceID, long approverID, ref DataCheckerRollover output, ref string message);
        bool GetDataIMLoanChecker(Guid workflowInstanceID, long approverID, ref InterestMaintenanceExcelCheckerLoan output, ref string message);
        bool GetDataSettlementScheduledLoanChecker(ref IList<RolloverLoanCheckerModel> output, ref string message);
        bool AddDataIMMaker(Guid workflowInstanceID, ref LoanCheckerInterestMaintenance output, long approverID, ref string message);
        bool AddDataIMCSO(Guid workflowInstanceID, ref LoanCheckerInterestMaintenance output, long approverID, ref string message);
        bool AddCSORollover(Guid workflowInstanceId, ref LoanCheckerRollover output, long approverID, ref string message);
        bool AddDataIMChecker(Guid workflowInstanceID, ref InterestMaintenanceExcelCheckerLoan output, long approverID, ref string message);
        bool AddDataSettlement(LoanCheckerDetailLoanModel output, ref string message);
        #endregion
        #region LoanChecker
        bool GetTransactionLoanDetails(Guid instanceID, ref TransactionLoanModel transaction, ref string message);
        bool AddLoanChecker(Guid workflowInstanceID, long approverID, ref LoanCheckerDetailLoanModel output, ref string message);
        bool GetCheckerLoanMaker(Guid workflowInstanceID, long approverID, ref LoanCheckerDetailLoanModel output, ref string message);

        #endregion
        #endregion

        #region dani dp
        bool GetTransactionLoanMakerSettlementUnscheduledDetails(Guid workflowInstanceID, ref LoanMakerSettlementUnscheduledModel output, ref string message);
        bool AddTransactionLoanMakerSettlementUnscheduledDetails(LoanMakerSettlementUnscheduledDetailModel data, ref string message);
        bool GetCheckerUnsettlement(Guid workflowInstanceID, long approverID, ref LoanMakerSettlementUnscheduledDetailModel output, ref string message);
        #endregion

        #region basri
        //Basri 28-10-2015
        bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineModel> timelines, ref string message);
        bool GetTransactionCheckerDetailsScheduledSettlement(Guid workflowInstanceID, long approverID, ref TransactionCheckerLoanScheduledSettlementModel output, ref string message);
        bool GetScheduledSettlementDetails(Guid workflowInstanceID, long csoRolloverID, ref TransactionCheckerLoanScheduledSettlementDetailModel transaction, ref string message);
        bool AddTransactionCheckerDetailsScheduledSettlement(Guid workflowInstanceID, long approverID, TransactionCheckerLoanScheduledSettlementModel data, ref string message);

        //end basri
        #endregion

    }
        #endregion
    #endregion

    #region Repository
    public class WorkflowLoanRepository : IWorkflowLoanRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        #region General
        public bool GetTransactionDetailsScheduledSettlement(Guid workflowInstanceID, ref TransactionLoanScheduledSettlementModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                TransactionLoanScheduledSettlementModel datatransaction = (from a in context.Transactions
                                                                           where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                                                                           select new TransactionLoanScheduledSettlementModel
                                                                           {
                                                                               ID = a.TransactionID,
                                                                               WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                                                               ApplicationID = a.ApplicationID,
                                                                               ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                                                                               Product = new ProductModel()
                                                                               {
                                                                                   ID = a.Product.ProductID,
                                                                                   Code = a.Product.ProductCode,
                                                                                   Name = a.Product.ProductName,
                                                                                   WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                                                                   Workflow = a.Product.ProductWorkflow.WorkflowName,
                                                                                   LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                                                                   LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                                                               },
                                                                               BizSegment = new BizSegmentModel()
                                                                               {
                                                                                   ID = a.BizSegment.BizSegmentID,
                                                                                   Name = a.BizSegment.BizSegmentName,
                                                                                   Description = a.BizSegment.BizSegmentDescription,
                                                                                   LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                                                   LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                                               },
                                                                               Channel = new ChannelModel()
                                                                               {
                                                                                   ID = a.Channel.ChannelID,
                                                                                   Name = a.Channel.ChannelName,
                                                                                   LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                                                                   LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                                                               },
                                                                               IsTopUrgent = (a.IsTopUrgent == 1) ? true : false,
                                                                               IsTopUrgentChain = (a.IsTopUrgent == 2) ? true : false,
                                                                               CreateDate = a.CreateDate,
                                                                               LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                               LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value

                                                                           }).SingleOrDefault();
                if (datatransaction != null)
                {
                    var InstructionID = context.TransactionRollovers.Where(a => a.TransactionID.Equals(datatransaction.ID)).Select(a => a.Instruction == null ? 0 : a.Instruction.Value).ToList();

                    if (InstructionID.Count > 0)
                    {
                        var Documents = (from x in context.TransactionDocuments
                                         where InstructionID.Equals(x.TransactionID) && x.IsDeleted.Equals(false)
                                         select new TransactionDocumentModel()
                                         {
                                             ID = x.TransactionDocumentID,
                                             Type = new DocumentTypeModel()
                                             {
                                                 ID = x.DocumentType.DocTypeID,
                                                 Name = x.DocumentType.DocTypeName,
                                                 Description = x.DocumentType.DocTypeDescription,
                                                 LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                 LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                             },
                                             Purpose = new DocumentPurposeModel()
                                             {
                                                 ID = x.DocumentPurpose.PurposeID,
                                                 Name = x.DocumentPurpose.PurposeName,
                                                 Description = x.DocumentPurpose.PurposeDescription,
                                                 LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                 LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                             },
                                             FileName = x.Filename,
                                             DocumentPath = x.DocumentPath,
                                             LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                             LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                         }).ToList();


                        if (Documents.Count > 0)
                        {
                            datatransaction.Documents = Documents;
                        }
                        else
                        {
                            datatransaction.Documents = new List<TransactionDocumentModel>();
                        }
                    }
                    else
                    {
                        datatransaction.Documents = new List<TransactionDocumentModel>();
                    }
                    transaction = datatransaction;
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionLoanTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineLoanModel> timelines, ref string message)
        {
            bool IsSuccess = false;

            try
            {

                timelines = (from a in context.SP_GetNintexTaskTimeline(workflowInstanceID)
                             select new TransactionTimelineLoanModel
                             {
                                 ApproverID = a.WorkflowApproverID,
                                 Activity = a.ActivityTitle,
                                 Time = a.ActivityTime,
                                 UserOrGroup = a.UserOrGroup,
                                 IsGroup = a.IsSPGroup.Value,
                                 Outcome = a.Outcome,
                                 DisplayName = a.DisplayName,
                                 Comment = a.Comment
                             }).ToList();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionExcel(Guid workflowInstanceID, ref TransactionLoanScheduledSettlementModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                TransactionLoanScheduledSettlementModel datatransaction = (from a in context.Transactions
                                                                           where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                                                                           select new TransactionLoanScheduledSettlementModel
                                                                           {
                                                                               ID = a.TransactionID,
                                                                               WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                                                               ApplicationID = a.ApplicationID,
                                                                               ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                                                                               Product = new ProductModel()
                                                                               {
                                                                                   ID = a.Product.ProductID,
                                                                                   Code = a.Product.ProductCode,
                                                                                   Name = a.Product.ProductName,
                                                                                   WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                                                                   Workflow = a.Product.ProductWorkflow.WorkflowName,
                                                                                   LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                                                                   LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                                                               },
                                                                               BizSegment = new BizSegmentModel()
                                                                               {
                                                                                   ID = a.BizSegment.BizSegmentID,
                                                                                   Name = a.BizSegment.BizSegmentName,
                                                                                   Description = a.BizSegment.BizSegmentDescription,
                                                                                   LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                                                   LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                                               },
                                                                               Channel = new ChannelModel()
                                                                               {
                                                                                   ID = a.Channel.ChannelID,
                                                                                   Name = a.Channel.ChannelName,
                                                                                   LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                                                                   LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                                                               },
                                                                               Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                                                                               {
                                                                                   ID = x.TransactionDocumentID,
                                                                                   Type = new DocumentTypeModel()
                                                                                   {
                                                                                       ID = x.DocumentType.DocTypeID,
                                                                                       Name = x.DocumentType.DocTypeName,
                                                                                       Description = x.DocumentType.DocTypeDescription,
                                                                                       LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                                       LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                                   },
                                                                                   Purpose = new DocumentPurposeModel()
                                                                                   {
                                                                                       ID = x.DocumentPurpose.PurposeID,
                                                                                       Name = x.DocumentPurpose.PurposeName,
                                                                                       Description = x.DocumentPurpose.PurposeDescription,
                                                                                       LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                                       LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                                                   },
                                                                                   FileName = x.Filename,
                                                                                   DocumentPath = x.DocumentPath,
                                                                                   LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                                   LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value,
                                                                                   IsDormant = x.IsDormant == null ? false : x.IsDormant
                                                                               }).ToList(),
                                                                               IsTopUrgent = (a.IsTopUrgent == 1) ? true : false,
                                                                               IsTopUrgentChain = (a.IsTopUrgent == 2) ? true : false,
                                                                               CreateDate = a.CreateDate,
                                                                               LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                               LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value

                                                                           }).SingleOrDefault();
                if (datatransaction != null)
                {
                    transaction = datatransaction;
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }

        #endregion

        #region LoanMaker
        public bool GetLoanDisburseDetails(Guid workflowInstanceID, long approverID, ref TransactionDisbursementModel output, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                context.Transactions.AsNoTracking();
                output = (from a in context.Transactions
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new TransactionDisbursementModel
                          {
                              ID = a.TransactionID,
                              workflowInstanceID = a.WorkflowInstanceID.Value,
                              applicationID = a.ApplicationID,
                              applicationDate = a.ApplicationDate,
                              ValueDate = a.ApplicationDate,
                              Customer = new CustomerModel()
                              {
                                  CIF = a.CIF
                              },
                              Product = new ProductModel()
                              {
                                  ID = a.Product.ProductID,
                                  Code = a.Product.ProductCode,
                                  Name = a.Product.ProductName
                              },
                              Currency = new CurrencyModel()
                              {
                                  ID = a.Currency.CurrencyID,
                                  Code = a.Currency.CurrencyCode,
                                  Description = a.Currency.CurrencyDescription,
                                  LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                  LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                              },
                              BizSegment = new BizSegmentModel()
                              {
                                  ID = a.BizSegment.BizSegmentID,
                                  Name = a.BizSegment.BizSegmentName,
                                  Description = a.BizSegment.BizSegmentDescription,
                                  LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                  LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                              },
                              Channel = new ChannelModel()
                              {
                                  ID = a.Channel.ChannelID,
                                  Name = a.Channel.ChannelName
                              },
                              transactionAmount = a.Amount,
                              LoanContractNo = a.LoanContractNo,
                              IsNewCustomer = a.IsNewCustomer,
                              CreateDate = a.CreateDate,
                              LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                              LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                          }).SingleOrDefault();

                if (output != null)
                {
                    if (output.Product.ID == 20 || output.Product.ID == 2)
                    {
                        // get current payment data if exist
                        //var disburse = context.SP_GetLoanDisbursement(output.ID).FirstOrDefault();
                        //output.ParameterSystem = new ParameterSystemModelFunding();
                        //output.ParameterSystem.CustomerCategory = new CustomerCategory() { CustomerCategoryID = disburse.CustomerCategoryID, CustomerCategoryName = disburse.CustomerCategoryName };
                        //output.ParameterSystem.LoanTypeID = disburse.LoanTypeID;
                        //output.ParameterSystem.LoanTypeName = disburse.LoanTypeName;
                        //output.ParameterSystem.InterestFrequency = new InterestFrequency() { InterestFrequencyID = disburse.InterestFrequencyID, InterestFrequencyName = disburse.InterestFrequencyName };
                        //output.SolID = disburse.SolID;
                        //output.ParameterSystem.ProgramType = new ProgramType() { ProgramTypeID = disburse.ProgramTypeID, ProgramTypeName = disburse.ProgramTypeName };
                        //output.ParameterSystem.FinacleScheme = new FinacleScheme() { FinacleSchemeCodeID = disburse.FinacleSchemeCodeID, FinacleSchemeCodeName = disburse.FinacleSchemeCodeName };
                        //output.IsAdhoc = disburse.IsAdhoc.Value;
                        //output.DebitingOperativeID = disburse.DebitingOperativeID.Value;
                        //output.CreditingOperativeID = disburse.CreditingOperativeID.Value;
                        //output.MaturityDate = disburse.MaturityDate.Value;
                        //output.ParameterSystem.Interest = new Interest() { InterestCodeID = disburse.InterestCodeID, InterestCodeName = disburse.InterestCodeName };
                        //output.BaseRate = disburse.BaseRate.Value;
                        //output.AccountPreferencial = disburse.AccountPreferencial.Value;
                        //output.AllInRate = disburse.AllInRate.Value;
                        //output.ParameterSystem.RepricingPlan = new RepricingPlan() { RepricingPlanID = disburse.RepricingPlanID, RepricingPlanName = disburse.RepricingPlanName };
                        //output.ApprovedMarginAsPerCM = disburse.ApprovedMarginAsPerCM.Value;
                        //output.SpecialFTP = disburse.SpecialFTP.Value;
                        //output.Margin = disburse.Margin.Value;
                        //output.AllInSpecialRate = disburse.AllInSpecialRate.Value;
                        //output.PeggingDate = disburse.PeggingDate.Value;
                        //output.ParameterSystem.PeggingFrequency = new PeggingFrequency() { PeggingFrequencyID = disburse.PeggingFrequencyID, PeggingFrequencyName = disburse.PeggingFrequencyName };
                        //output.PrincipalStartDate = disburse.PrincipalStartDate.Value;
                        //output.InterestStartDate = disburse.InterestStartDate.Value;
                        //output.NoOfInstallment = disburse.NoOfInstallment;
                        //output.ParameterSystem.PrincipalFrequency = new PrincipalFrequency() { PrincipalFrequencyID = disburse.PrincipalFrequencyID, PricipalFrequencyName = disburse.PrincipalFrequencyName };
                        //output.LimitIDinFin10 = disburse.LimitIDinFin10;
                        //output.LimitExpiryDate = disburse.LimitExpiryDate.Value;
                        //output.SanctionLimit = disburse.SanctionLimit.Value;
                        //output.SanctionLimitCurr = new CurrencyModel() { ID = disburse.SanctionLimitCurrID.Value, Code = disburse.SanctionLimitCurrName };
                        //output.utilization = disburse.Utilization.Value;
                        //output.UtilizationCurr = new CurrencyModel() { ID = disburse.UtilizationCurrID.Value, Code = disburse.UtilizationCurrName };
                        //output.Outstanding = disburse.Outstanding.Value;
                        //output.OutstandingCurr = new CurrencyModel() { ID = disburse.OutstandingCurrID.Value, Code = disburse.OutstandingCurrName };
                        //output.CSOName = disburse.CSOName;
                        if (output != null) IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetCheckerCSOLoan(Guid workflowInstanceID, long approverID, ref TransactionPPUCheckerCSODetailModel output, ref string message)
        {
            bool IsSuccess = false;

            var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

            try
            {
                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new TransactionPPUCheckerCSODetailModel
                          {
                              Verify = a.TransactionCheckers
                                .Where(x => x.TransactionID.Equals(transactionID) && x.IsDeleted == false)
                                .Select(x => new VerifyLoanModel()
                                {
                                    ID = x.TransactionColumnID,
                                    Name = x.TransactionColumn.ColumnName,
                                    IsVerified = x.IsVerified
                                }).ToList()
                          }).SingleOrDefault();
                if (!output.Verify.Any())
                {
                    output.Verify = context.TransactionColumns
                        //.Where(x => x.IsPPUCheckerLoan.Equals(true) && x.IsDeleted.Equals(false))
                        .Where(x => x.IsDeleted == false && x.IsLoanCheckerDisbursment == true)
                        .Select(x => new VerifyLoanModel()
                        {
                            ID = x.TransactionColumnID,
                            Name = x.ColumnName,
                            IsVerified = false
                        })
                        .ToList();
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetCheckerLoanMaker(Guid workflowInstanceID, long approverID, ref LoanCheckerDetailLoanModel output, ref string message)
        {
            bool IsSuccess = false;

            var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

            try
            {

                {
                    output = (from a in context.Transactions.AsNoTracking()
                              where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                              select new LoanCheckerDetailLoanModel
                              {
                                  //start add azam
                                  //Verify = context.TransactionColumns.Where(x => x.IsDeleted.Equals(false) && x.IsLoanCheckerDisbursment == (true)).Select(x => new VerifyLoanModel()
                                  //{
                                  //    ID = x.TransactionColumnID,
                                  //    Name = x.ColumnName,
                                  //    IsVerified = false
                                  //}).ToList()
                                  //end azam
                                  //start coment azam
                                  Verify = a.TransactionCheckers
                                    .Where(x => x.TransactionID.Equals(transactionID) && x.IsDeleted == false)
                                    .Select(x => new VerifyLoanModel()
                                    {
                                        ID = x.TransactionColumnID,
                                        Name = x.TransactionColumn.ColumnName,
                                        IsVerified = x.IsVerified
                                    }).ToList()
                                  //end comment azam
                              }).SingleOrDefault();
                    if (!output.Verify.Any())
                    {
                        output.Verify = context.TransactionColumns
                            //.Where(x => x.IsPPUCheckerLoan.Equals(true) && x.IsDeleted.Equals(false))
                            .Where(x => x.IsDeleted.Equals(false) && x.IsLoanCheckerDisbursment == (true))
                            .Select(x => new VerifyLoanModel()
                            {
                                ID = x.TransactionColumnID,
                                Name = x.ColumnName,
                                IsVerified = false
                            })
                            .ToList();
                    }
                }
                IsSuccess = true;
            }

            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        #endregion

        #region repoFandi
        #region LoanCSO
        public bool GetTransactionCSODetails(Guid instanceID, ref TransactionCSOModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                TransactionCSOModel datatransaction = (from a in context.Transactions
                                                       where a.WorkflowInstanceID.Value.Equals(instanceID)
                                                       select new TransactionCSOModel
                                                       {
                                                           ID = a.TransactionID,
                                                           WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                                           ApplicationID = a.ApplicationID,
                                                           ApproverID = a.ApproverID != null ? a.ApproverID : 0,

                                                           Product = new ProductModel()
                                                           {
                                                               ID = a.Product.ProductID,
                                                               Code = a.Product.ProductCode,
                                                               Name = a.Product.ProductName,
                                                               WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                                               Workflow = a.Product.ProductWorkflow.WorkflowName,
                                                               LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                                               LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                                           },
                                                           Currency = new CurrencyModel()
                                                           {
                                                               ID = a.Currency.CurrencyID,
                                                               Code = a.Currency.CurrencyCode,
                                                               Description = a.Currency.CurrencyDescription,
                                                               LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                               LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                           },
                                                           Amount = a.Amount,
                                                           AmountUSD = a.AmountUSD,
                                                           Rate = a.Rate,
                                                           Channel = new ChannelModel()
                                                           {
                                                               ID = a.Channel.ChannelID,
                                                               Name = a.Channel.ChannelName,
                                                               LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                                               LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                                           },
                                                           SourceID = a.SourceID.HasValue? a.SourceID.Value: 0,
                                                           BizSegment = new BizSegmentModel()
                                                           {
                                                               ID = a.BizSegment.BizSegmentID,
                                                               Name = a.BizSegment.BizSegmentName,
                                                               Description = a.BizSegment.BizSegmentDescription,
                                                               LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                               LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                           },

                                                           IsTopUrgent = a.IsTopUrgent,

                                                           Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                                                           {
                                                               ID = x.TransactionDocumentID,
                                                               Type = new DocumentTypeModel()
                                                               {
                                                                   ID = x.DocumentType.DocTypeID,
                                                                   Name = x.DocumentType.DocTypeName,
                                                                   Description = x.DocumentType.DocTypeDescription,
                                                                   LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                   LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                               },
                                                               Purpose = new DocumentPurposeModel()
                                                               {
                                                                   ID = x.DocumentPurpose.PurposeID,
                                                                   Name = x.DocumentPurpose.PurposeName,
                                                                   Description = x.DocumentPurpose.PurposeDescription,
                                                                   LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                   LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                               },
                                                               FileName = x.Filename,
                                                               DocumentPath = x.DocumentPath,
                                                               LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                               LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value,
                                                               IsDormant = x.IsDormant == null ? false : x.IsDormant
                                                           }).ToList(),
                                                           CreateDate = a.CreateDate,
                                                           LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                           LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                                           ValueDate = a.ValueDate
                                                       }).SingleOrDefault();


                if (datatransaction != null)
                {
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(instanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    // changed, chandra : 2015.03.23
                    if (customerRepo.GetCustomerByTransaction(datatransaction.ID, ref customer, ref message))
                    {
                        datatransaction.Customer = customer;
                    }

                }
                FundingMemoLTModel FundingMemo = (from funding in context.SP_GetCSOFundingMemoTransaction(datatransaction.ID)
                                                  select new FundingMemoLTModel
                                                  {
                                                      CIF = funding.CIF,
                                                      Customer = funding.CIF != null ? new CustomerModel { CIF = funding.CIF, Name = funding.CustomerName } : new CustomerModel { CIF = "", Name = "" },
                                                      Location = funding.LocationID != null ? new LocationModel { ID = funding.LocationID, SolId = funding.SolID } : new LocationModel { ID = 0, SolId = "" },
                                                      CSOName = funding.CSOName,
                                                      ApprovalDOA = funding.DoAName,
                                                      LoanTypeID = funding.LoanTypeID ?? 0,

                                                      LoanTypeName = funding.LoanTypeName,
                                                      SolID = funding.solidfun,
                                                      BizName = funding.BizSegmentName,
                                                      CustomerCategoryID = funding.CustomerCategoryID,
                                                      CustomerCategoryName = funding.CustomerCategoryName,
                                                      CSOFundingMemoID = funding.CSOFundingMemoID,//Convert.ToInt32(funding.CSOFundingMemoID),
                                                      TransactionFundingMemoID = funding.TransactionFundingMemoID,//Convert.ToInt32(funding.TransactionFundingMemoID),
                                                      ValueDate = funding.ValueDate,
                                                      IsAdhoc = funding.IsAdhoc == null ? false : funding.IsAdhoc,
                                                      CreditingOperative = funding.CreditingOperative,
                                                      DebitingOperative = funding.DebitingOperative,
                                                      MaturityDate = funding.MaturityDate,
                                                      BaseRate = funding.BaseRate,
                                                      AccountPreferencial = funding.AccountPreferencial,
                                                      AllInRate = funding.AllInRate,
                                                      ApprovedMarginAsPerCM = funding.ApprovedMarginAsPerCM,
                                                      SpecialFTP = funding.SpecialFTP,
                                                      Margin = funding.Margin,
                                                      AllInSpecialRate = funding.AllInSpecialRate,
                                                      PeggingDate = funding.PeggingDate,
                                                      NoOfInstallment = funding.NoOfInstallment,
                                                      LimitIDinFin10 = funding.LimitIDinFin10,
                                                      LimitExpiryDate = funding.LimitExpiryDate,
                                                      SanctionLimitAmount = funding.SanctionLimit,
                                                      utilizationAmount = funding.utilization,
                                                      OutstandingAmount = funding.Outstanding,
                                                      OutstandingCurrID = funding.OutstandingCurrID,
                                                      Remarks = funding.Remarks,
                                                      OtherRemarks = funding.OtherRemarks,
                                                      AmountDisburse = funding.AmountDisburse,
                                                      PrincipalStartDate = funding.PrincipalStartDate,
                                                      InterestStartDate = funding.InterestStartDate,
                                                      LastModifiedDate = funding.UpdateDate == null ? DateTime.Now : funding.UpdateDate.Value,
                                                      LastModifiedBy = funding.UpdateBy == null ? "" : funding.UpdateBy,
                                                      ParameterSystemModel = funding.LoanTypeID != null ? new ParameterSystemModelFunding { LoanTypeID = funding.LoanTypeID, LoanTypeName = string.Empty } : new ParameterSystemModelFunding { LoanTypeID = 0, LoanTypeName = "" },
                                                      AmountDisburseID = funding.AmountDisburseID != null ? new AmountDisburseIDModel { ID = funding.AmountDisburseID, CodeDescription = string.Empty } : new AmountDisburseIDModel { ID = 0, CodeDescription = "" },
                                                      SanctionLimitCurr = funding.SanctionLimitCurrID != null ? new SanctionLimitCurrModel { ID = funding.SanctionLimitCurrID, CodeDescription = string.Empty } : new SanctionLimitCurrModel { ID = 0, CodeDescription = "" },
                                                      utilizationID = funding.UtilizationCurrID != null ? new utilizationIDModel { ID = funding.UtilizationCurrID, CodeDescription = string.Empty } : new utilizationIDModel { ID = 0, CodeDescription = "" },
                                                      OutstandingID = funding.OutstandingCurrID != null ? new OutstandingIDModel { ID = funding.OutstandingCurrID, CodeDescription = string.Empty } : new OutstandingIDModel { ID = 0, CodeDescription = "" },
                                                      ProgramType = funding.ProgramTypeID != null ? new ProgramType { ProgramTypeID = funding.ProgramTypeID, ProgramTypeName = string.Empty } : new ProgramType { ProgramTypeID = 0, ProgramTypeName = "" },
                                                      FinacleScheme = funding.FinacleSchemeCodeID != null ? new FinacleScheme { FinacleSchemeCodeID = funding.FinacleSchemeCodeID, FinacleSchemeCodeName = string.Empty } : new FinacleScheme { FinacleSchemeCodeID = 0, FinacleSchemeCodeName = "" },
                                                      Interest = funding.InterestCodeID != null ? new Interest { InterestCodeID = funding.InterestCodeID, InterestCodeName = string.Empty } : new Interest { InterestCodeID = "", InterestCodeName = "" },
                                                      //Interest = funding.Customer != null ? new Interest { InterestCodeID = int.Parse((string)funding.InterestCodeID), InterestCodeName = parametersysmodel3.ParameterValue } : new Interest { InterestCodeID = 0, InterestCodeName = "" },
                                                      RepricingPlan = funding.RepricingPlanID != null ? new RepricingPlan { RepricingPlanID = funding.RepricingPlanID, RepricingPlanName = string.Empty } : new RepricingPlan { RepricingPlanID = 0, RepricingPlanName = "" },
                                                      PeggingFrequency = funding.PeggingFrequencyID != null ? new PeggingFrequency { PeggingFrequencyID = funding.PeggingFrequencyID, PeggingFrequencyName = string.Empty } : new PeggingFrequency { PeggingFrequencyID = 0, PeggingFrequencyName = "" },
                                                      InterestFrequency = funding.InterestFrequencyID != null ? new InterestFrequency { InterestFrequencyID = funding.InterestFrequencyID, InterestFrequencyName = string.Empty } : new InterestFrequency { InterestFrequencyID = 0, InterestFrequencyName = "" },
                                                      PrincipalFrequencyID = funding.PrincipalFrequencyID
                                                  }).SingleOrDefault();

                if (FundingMemo != null)
                {
                    //getDOc
                    var CIFa = FundingMemo.CSOFundingMemoID;
                    List<FundingMemoDocument> fundingDoc = context.FundingMemoDocuments.Where(x => x.FundingMemoID == CIFa).ToList();

                    if (fundingDoc != null)
                    {
                        FundingMemo.fundingmemodocument = new List<FundingMemoDocumentModel>();
                        foreach (var item in fundingDoc)
                        {
                            FundingMemoDocumentModel Doc = new FundingMemoDocumentModel();
                            {
                                Doc.FundingMemoID = item.FundingMemoID;
                                Doc.DocumentPath = item.DocumentPath;
                                Doc.FileName = item.Filename;


                                Doc.Type = new DocumentTypeModel()
                                {
                                    ID = item.DocumentType.DocTypeID,
                                    Name = item.DocumentType.DocTypeName,
                                    Description = item.DocumentType.DocTypeDescription,
                                    LastModifiedBy = item.DocumentType.UpdateDate == null ? item.DocumentType.CreateBy : item.DocumentType.UpdateBy,
                                    LastModifiedDate = item.DocumentType.UpdateDate == null ? item.DocumentType.CreateDate : item.DocumentType.UpdateDate.Value
                                };
                                Doc.Purpose = new DocumentPurposeModel()
                                {
                                    ID = item.DocumentPurpose.PurposeID,
                                    Name = item.DocumentPurpose.PurposeName,
                                    Description = item.DocumentPurpose.PurposeDescription,
                                    LastModifiedBy = item.DocumentPurpose.UpdateDate == null ? item.DocumentPurpose.CreateBy : item.DocumentPurpose.UpdateBy,
                                    LastModifiedDate = item.DocumentPurpose.UpdateDate == null ? item.DocumentPurpose.CreateDate : item.DocumentPurpose.UpdateDate.Value

                                };
                                Doc.CreatedBy = item.CreateBy;
                                Doc.CreatedDate = item.CreateDate;
                            };
                            FundingMemo.fundingmemodocument.Add(Doc);
                        }
                    }
                    //end
                    datatransaction.FundingMemo = FundingMemo;
                    transaction = datatransaction;
                }
                else
                {
                    transaction = datatransaction;
                }
                //end fundingmemo
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        public bool AddTransactionCSODetails(Guid workflowInstanceID, long approverID, TransactionPPUCheckerCSODetailModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

                    if (transactionID > 0)
                    {
                        Entity.Transaction data_t = context.Transactions.Where(a => a.TransactionID.Equals(transactionID)).SingleOrDefault();
                        if (data_t != null)
                        {
                            data_t.ValueDate = data.Transaction.ValueDate.HasValue ? data.Transaction.ValueDate.Value : DateTime.Now;
                            //data_t.IsDocumentComplete = data.Verify.ToList().Where(a => a.Name.Equals("Document Completeness")).FirstOrDefault().IsVerified;
                            //data_t.IsDocumentComplete = data.Transaction.IsDocumentComplete;
                            context.SaveChanges();
                        }

                        var checker = context.TransactionCheckers
                               .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                               .ToList();
                        var checkerData = context.TransactionCheckerDatas
                                .Where(a => a.TransactionID.Equals(transactionID))
                                .SingleOrDefault();


                        if (data.Transaction.Product.ID == (int)DBSProductID.LoanDisbursmentProductIDCons)
                        {


                            //add fandi
                            //save TransactionFundingMemo
                            var transactionFundingMemo = context.TransactionFundingMemoes
                                .Where(a => a.TransactionID.Equals(transactionID))
                                .SingleOrDefault();
                            if (transactionFundingMemo == null)
                            {
                                transactionFundingMemo = new TransactionFundingMemo();
                                context.TransactionFundingMemoes.Add(new TransactionFundingMemo()
                                {
                                    TransactionID = data.Transaction.ID,
                                    LastFundingMemoID = 1,
                                    LastStatus = "1",
                                    CreateBy = currentUser.GetCurrentUser().DisplayName,
                                    CreateDate = DateTime.UtcNow

                                });

                                context.SaveChanges();
                            }

                            //get transactinFundingMemID
                            var getFundingID = context.TransactionFundingMemoes
                            .Where(a => a.TransactionID.Equals(transactionID))
                            .SingleOrDefault();

                            //var funding = context.CSOFundingMemoes
                            Entity.CSOFundingMemo funding = context.CSOFundingMemoes
                                .Where(a => a.CSOFundingMemoID.Equals(data.Transaction.FundingMemo.CSOFundingMemoID == null ? 0 : data.Transaction.FundingMemo.CSOFundingMemoID.Value))
                            .FirstOrDefault();


                            if (data.Transaction.FundingMemo.CSOFundingMemoID != null)
                            {
                                //funding = new CSOFundingMemo();
                                //satu
                                funding.TransactionFundingMemoID = getFundingID.TransactionID;//data.Transaction.ID;
                                funding.CIF = data.Transaction.FundingMemo.CIF;
                                funding.SolID = data.Transaction.FundingMemo.SolID;
                                funding.BizSegmentID = data.Transaction.BizSegment.ID;
                                funding.CustomerCategoryID = data.Transaction.FundingMemo.CustomerCategoryID;
                                funding.ProgramTypeID = data.Transaction.FundingMemo.ProgramType.ProgramTypeID;//add
                                funding.BizName = data.Transaction.FundingMemo.BizName;
                                funding.FinacleSchemeCodeID = data.Transaction.FundingMemo.FinacleScheme.FinacleSchemeCodeID;//add
                                funding.LoanTypeID = data.Transaction.FundingMemo.LoanID.LoanTypeID;//add
                                funding.IsAdhoc = data.Transaction.FundingMemo.IsAdhoc;//data.Transaction.FundingMemo.IsAdhoc,
                                //EndSatu

                                //Dua
                                funding.AmountDisburse = data.Transaction.FundingMemo.AmountDisburse;
                                funding.AmountDisburseID = data.Transaction.FundingMemo.AmountDisburseID.ID;
                                funding.ValueDate = data.Transaction.FundingMemo.ValueDate;
                                funding.CreditingOperative = data.Transaction.FundingMemo.CreditingOperative;
                                funding.DebitingOperative = data.Transaction.FundingMemo.DebitingOperative;
                                funding.MaturityDate = data.Transaction.FundingMemo.MaturityDate;//data.Transaction.FundingMemo.MaturityDate,
                                //EndDua
                                //Tiga
                                funding.InterestCodeID = data.Transaction.FundingMemo.Interest.InterestCodeID;
                                funding.BaseRate = data.Transaction.FundingMemo.BaseRate;
                                funding.AccountPreferencial = data.Transaction.FundingMemo.AccountPreferencial;
                                funding.AllInRate = data.Transaction.FundingMemo.AllInRate;
                                funding.RepricingPlanID = data.Transaction.FundingMemo.RepricingPlan.RepricingPlanID;
                                funding.ApprovedMarginAsPerCM = data.Transaction.FundingMemo.ApprovedMarginAsPerCM;
                                funding.SpecialFTP = data.Transaction.FundingMemo.SpecialFTP;
                                funding.Margin = data.Transaction.FundingMemo.Margin;
                                funding.AllInSpecialRate = data.Transaction.FundingMemo.AllInSpecialRate;
                                funding.PeggingDate = data.Transaction.FundingMemo.PeggingDate;//data.Transaction.FundingMemo.PeggingDate,
                                funding.PeggingFrequencyID = data.Transaction.FundingMemo.PeggingFrequency.PeggingFrequencyID;
                                //EndTiga

                                //Empat
                                funding.PrincipalStartDate = data.Transaction.FundingMemo.PrincipalStartDate;
                                funding.InterestStartDate = data.Transaction.FundingMemo.InterestStartDate;
                                funding.NoOfInstallment = data.Transaction.FundingMemo.NoOfInstallment;
                                funding.PrincipalFrequencyID = data.Transaction.FundingMemo.PrincipalFrequencyID;
                                funding.InterestFrequencyID = data.Transaction.FundingMemo.InterestFrequencyID;
                                //EndEmpat

                                //Lima
                                funding.LimitIDinFin10 = data.Transaction.FundingMemo.LimitIDinFin10;
                                funding.LimitExpiryDate = data.Transaction.FundingMemo.LimitExpiryDate;
                                funding.SanctionLimitCurrID = data.Transaction.FundingMemo.SanctionLimitCurrID;
                                funding.SanctionLimit = data.Transaction.FundingMemo.SanctionLimitAmount;
                                funding.UtilizationCurrID = data.Transaction.FundingMemo.utilizationID.ID;
                                funding.utilization = data.Transaction.FundingMemo.utilizationAmount;
                                funding.Outstanding = data.Transaction.FundingMemo.OutstandingAmount;
                                funding.OutstandingCurrID = data.Transaction.FundingMemo.OutstandingID.ID;
                                //add refere currency
                                //End


                                funding.DoAName = data.Transaction.FundingMemo.DoAName;

                                funding.CSOName = data.Transaction.FundingMemo.CSOName;


                                funding.UpdateDate = DateTime.UtcNow;
                                funding.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                context.SaveChanges();
                                Entity.Transaction trans = context.Transactions.Where(a => a.TransactionID == getFundingID.TransactionID).FirstOrDefault();
                                trans.ValueDate = data.Transaction.ValueDate;
                                context.SaveChanges();

                                if (data.Transaction.Documents != null)
                                {
                                    var existingDocuments = (from x in context.TransactionDocuments
                                                             where x.TransactionID == data.Transaction.ID && x.IsDeleted == false
                                                             select x);
                                    if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                                    {
                                        foreach (var item in existingDocuments.ToList()) // remove All transaction document
                                        {
                                            //if (item.PurposeID != 2)
                                            {
                                                context.TransactionDocuments.Remove(item);
                                            }
                                        }
                                        context.SaveChanges();

                                        foreach (var item in data.Transaction.Documents) //Add All Transaction document
                                        {
                                            //if (item.Purpose.ID != 2)
                                            {
                                                Entity.TransactionDocument document = new Entity.TransactionDocument()
                                                {
                                                    TransactionID = data.Transaction.ID,
                                                    DocTypeID = item.Type.ID,
                                                    PurposeID = item.Purpose.ID,
                                                    Filename = item.FileName,
                                                    IsDeleted = false,
                                                    DocumentPath = item.DocumentPath,
                                                    CreateDate = DateTime.UtcNow,
                                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                                };

                                                context.TransactionDocuments.Add(document);
                                            }
                                            context.SaveChanges();
                                        }
                                    }

                                }

                            }
                            else
                            {
                                //add fundingMemo by apprval page
                                funding = new CSOFundingMemo();
                                context.CSOFundingMemoes.Add(new CSOFundingMemo()
                                {
                                    //Satu
                                    TransactionFundingMemoID = getFundingID.TransactionID,
                                    CIF = data.Transaction.FundingMemo.CIF,
                                    SolID = data.Transaction.FundingMemo.SolID,
                                    BizSegmentID = data.Transaction.BizSegment.ID,
                                    CustomerCategoryID = data.Transaction.FundingMemo.CustomerCategoryID,//add
                                    ProgramTypeID = data.Transaction.FundingMemo.ProgramType.ProgramTypeID,//add
                                    BizName = data.Transaction.FundingMemo.BizName,
                                    FinacleSchemeCodeID = data.Transaction.FundingMemo.FinacleScheme.FinacleSchemeCodeID,//add
                                    LoanTypeID = data.Transaction.FundingMemo.LoanID.LoanTypeID,//add
                                    IsAdhoc = data.Transaction.FundingMemo.IsAdhoc,//data.Transaction.FundingMemo.IsAdhoc,
                                    //EndSatu

                                    //Dua
                                    AmountDisburse = data.Transaction.FundingMemo.AmountDisburse,
                                    AmountDisburseID = data.Transaction.FundingMemo.AmountDisburseID.ID,
                                    ValueDate = data.Transaction.FundingMemo.ValueDate,//data.Transaction.FundingMemo.ValueDate,
                                    CreditingOperative = data.Transaction.FundingMemo.CreditingOperative,
                                    DebitingOperative = data.Transaction.FundingMemo.DebitingOperative,
                                    MaturityDate = data.Transaction.FundingMemo.MaturityDate,//data.Transaction.FundingMemo.MaturityDate,
                                    //EndDua

                                    //Tiga
                                    InterestCodeID = data.Transaction.FundingMemo.Interest.InterestCodeID,
                                    BaseRate = data.Transaction.FundingMemo.BaseRate,
                                    AccountPreferencial = data.Transaction.FundingMemo.AccountPreferencial,
                                    AllInRate = data.Transaction.FundingMemo.AllInRate,
                                    RepricingPlanID = data.Transaction.FundingMemo.RepricingPlan.RepricingPlanID,//add
                                    ApprovedMarginAsPerCM = data.Transaction.FundingMemo.ApprovedMarginAsPerCM,
                                    SpecialFTP = data.Transaction.FundingMemo.SpecialFTP,
                                    Margin = data.Transaction.FundingMemo.Margin,
                                    AllInSpecialRate = data.Transaction.FundingMemo.AllInSpecialRate,
                                    PeggingDate = data.Transaction.FundingMemo.PeggingDate,//data.Transaction.FundingMemo.PeggingDate,
                                    PeggingFrequencyID = data.Transaction.FundingMemo.PeggingFrequency.PeggingFrequencyID,//add
                                    //EndTiga

                                    //Empat
                                    PrincipalStartDate = data.Transaction.FundingMemo.PrincipalStartDate,//data.Transaction.FundingMemo.PrincipalStartDate,
                                    InterestStartDate = data.Transaction.FundingMemo.InterestStartDate,//data.Transaction.FundingMemo.InterestStartDate,
                                    NoOfInstallment = data.Transaction.FundingMemo.NoOfInstallment,
                                    PrincipalFrequencyID = data.Transaction.FundingMemo.PrincipalFrequencyID,//add interest same principal
                                    InterestFrequencyID = data.Transaction.FundingMemo.InterestFrequencyID,
                                    //EndEmpat

                                    //Lima
                                    LimitIDinFin10 = data.Transaction.FundingMemo.LimitIDinFin10,
                                    LimitExpiryDate = data.Transaction.FundingMemo.LimitExpiryDate,
                                    SanctionLimitCurrID = data.Transaction.FundingMemo.SanctionLimitCurrID,
                                    SanctionLimit = data.Transaction.FundingMemo.SanctionLimitAmount,
                                    UtilizationCurrID = data.Transaction.FundingMemo.utilizationID.ID,//add refere currency
                                    utilization = data.Transaction.FundingMemo.utilizationAmount,
                                    Outstanding = data.Transaction.FundingMemo.OutstandingAmount,
                                    OutstandingCurrID = data.Transaction.FundingMemo.OutstandingID.ID,//add refere currency
                                    //add refere currency
                                    //End

                                    DoAName = data.Transaction.FundingMemo.DoAName,

                                    CSOName = data.Transaction.FundingMemo.CSOName,


                                    //Remarks = funding.Remarks,
                                    //OtherRemarks = funding.OtherRemarks,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName

                                });
                                context.SaveChanges();
                                if (data.Transaction.Documents != null)
                                {
                                    var existingDocuments = (from x in context.TransactionDocuments
                                                             where x.TransactionID == data.Transaction.ID && x.IsDeleted == false
                                                             select x);
                                    if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                                    {
                                        foreach (var item in existingDocuments.ToList()) // remove All transaction document
                                        {
                                            //if (item.PurposeID != 2)
                                            {
                                                context.TransactionDocuments.Remove(item);
                                            }
                                        }
                                        context.SaveChanges();

                                        foreach (var item in data.Transaction.Documents) //Add All Transaction document
                                        {
                                            //if (item.Purpose.ID != 2)
                                            {
                                                Entity.TransactionDocument document = new Entity.TransactionDocument()
                                                {
                                                    TransactionID = data.Transaction.ID,
                                                    DocTypeID = item.Type.ID,
                                                    PurposeID = item.Purpose.ID,
                                                    Filename = item.FileName,
                                                    IsDeleted = false,
                                                    DocumentPath = item.DocumentPath,
                                                    CreateDate = DateTime.UtcNow,
                                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                                };

                                                context.TransactionDocuments.Add(document);
                                            }
                                            context.SaveChanges();
                                        }
                                    }

                                }
                            }
                        }
                        else
                        {
                            if (data.Transaction.Documents != null)
                            {
                                var existingDocuments = (from x in context.TransactionDocuments
                                                         where x.TransactionID == data.Transaction.ID && x.IsDeleted == false
                                                         select x);
                                if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                                {
                                    foreach (var item in existingDocuments.ToList()) // remove All transaction document
                                    {
                                        //if (item.PurposeID != 2)
                                        {
                                            context.TransactionDocuments.Remove(item);
                                        }
                                    }
                                    context.SaveChanges();

                                    foreach (var item in data.Transaction.Documents) //Add All Transaction document
                                    {
                                        //if (item.Purpose.ID != 2)
                                        {
                                            Entity.TransactionDocument document = new Entity.TransactionDocument()
                                            {
                                                TransactionID = data.Transaction.ID,
                                                DocTypeID = item.Type.ID,
                                                PurposeID = item.Purpose.ID,
                                                Filename = item.FileName,
                                                IsDeleted = false,
                                                DocumentPath = item.DocumentPath,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().DisplayName
                                            };

                                            context.TransactionDocuments.Add(document);
                                        }
                                        context.SaveChanges();
                                    }
                                }

                            }
                        }

                        //end

                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }

        public bool UpdateReviseCSOTask(Guid workflowInstanceID, long approverID, TransactionPPUCheckerCSODetailModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionID = context.Transactions
                                   .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                                   .Select(a => a.TransactionID)
                                   .SingleOrDefault();
                    if (data.Transaction.Documents != null)
                    {
                        var existingDocuments = (from x in context.TransactionDocuments
                                                 where x.TransactionID == data.Transaction.ID && x.IsDeleted == false
                                                 select x);
                        if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                        {
                            foreach (var item in existingDocuments.ToList()) // remove All transaction document
                            {
                                //if (item.PurposeID != 2)
                                {
                                    context.TransactionDocuments.Remove(item);
                                }
                            }
                            context.SaveChanges();

                            foreach (var item in data.Transaction.Documents) //Add All Transaction document
                            {
                                //if (item.Purpose.ID != 2)
                                {
                                    Entity.TransactionDocument document = new Entity.TransactionDocument()
                                    {
                                        TransactionID = data.Transaction.ID,
                                        DocTypeID = item.Type.ID,
                                        PurposeID = item.Purpose.ID,
                                        Filename = item.FileName,
                                        IsDeleted = false,
                                        DocumentPath = item.DocumentPath,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().DisplayName
                                    };

                                    context.TransactionDocuments.Add(document);
                                }
                                context.SaveChanges();
                            }
                        }

                    }
                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }
            return IsSuccess;
        }

        public bool AddCSOReviseTask(Guid workflowInstanceID, long approverID, TransactionPPUCheckerCSODetailModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                var transactionID = context.Transactions
                      .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                      .Select(a => a.TransactionID)
                      .SingleOrDefault();
                try
                {
                    var getFundingID = context.TransactionFundingMemoes
                            .Where(a => a.TransactionID.Equals(transactionID))
                            .SingleOrDefault();

                    //var funding = context.CSOFundingMemoes
                    Entity.CSOFundingMemo funding = context.CSOFundingMemoes
                        .Where(a => a.CSOFundingMemoID.Equals(data.Transaction.FundingMemo.CSOFundingMemoID == null ? 0 : data.Transaction.FundingMemo.CSOFundingMemoID.Value))
                    .FirstOrDefault();



                    //funding = new CSOFundingMemo();
                    //satu
                    funding.TransactionFundingMemoID = getFundingID.TransactionID;//data.Transaction.ID;
                    funding.CIF = data.Transaction.FundingMemo.CIF;
                    funding.SolID = data.Transaction.FundingMemo.SolID;
                    funding.BizSegmentID = data.Transaction.BizSegment.ID;
                    funding.CustomerCategoryID = data.Transaction.FundingMemo.CustomerCategoryID;
                    funding.ProgramTypeID = data.Transaction.FundingMemo.ProgramType.ProgramTypeID;//add
                    funding.BizName = data.Transaction.FundingMemo.BizName;
                    funding.FinacleSchemeCodeID = data.Transaction.FundingMemo.FinacleScheme.FinacleSchemeCodeID;//add
                    funding.LoanTypeID = data.Transaction.FundingMemo.LoanTypeID;//add
                    funding.IsAdhoc = data.Transaction.FundingMemo.IsAdhoc;//data.Transaction.FundingMemo.IsAdhoc,

                    //EndSatu

                    //Dua
                    funding.AmountDisburse = data.Transaction.FundingMemo.AmountDisburse;
                    funding.AmountDisburseID = data.Transaction.FundingMemo.AmountDisburseID.ID;
                    funding.ValueDate = data.Transaction.FundingMemo.ValueDate;
                    funding.CreditingOperative = data.Transaction.FundingMemo.CreditingOperative;
                    funding.DebitingOperative = data.Transaction.FundingMemo.DebitingOperative;
                    funding.MaturityDate = data.Transaction.FundingMemo.MaturityDate;//data.Transaction.FundingMemo.MaturityDate,
                    //EndDua
                    //Tiga
                    funding.InterestCodeID = data.Transaction.FundingMemo.Interest.InterestCodeID;
                    funding.BaseRate = data.Transaction.FundingMemo.BaseRate;
                    funding.AccountPreferencial = data.Transaction.FundingMemo.AccountPreferencial;
                    funding.AllInRate = data.Transaction.FundingMemo.AllInRate;
                    funding.RepricingPlanID = data.Transaction.FundingMemo.RepricingPlan.RepricingPlanID;
                    funding.ApprovedMarginAsPerCM = data.Transaction.FundingMemo.ApprovedMarginAsPerCM;
                    funding.SpecialFTP = data.Transaction.FundingMemo.SpecialFTP;
                    funding.Margin = data.Transaction.FundingMemo.Margin;
                    funding.AllInSpecialRate = data.Transaction.FundingMemo.AllInSpecialRate;
                    funding.PeggingDate = data.Transaction.FundingMemo.PeggingDate;//data.Transaction.FundingMemo.PeggingDate,
                    funding.PeggingFrequencyID = data.Transaction.FundingMemo.PeggingFrequency.PeggingFrequencyID;
                    //EndTiga

                    //Empat
                    funding.PrincipalStartDate = data.Transaction.FundingMemo.PrincipalStartDate;
                    funding.InterestStartDate = data.Transaction.FundingMemo.InterestStartDate;
                    funding.NoOfInstallment = data.Transaction.FundingMemo.NoOfInstallment;
                    funding.PrincipalFrequencyID = data.Transaction.FundingMemo.PrincipalFrequencyID;
                    funding.InterestFrequencyID = data.Transaction.FundingMemo.InterestFrequencyID;
                    //EndEmpat

                    //Lima
                    funding.LimitIDinFin10 = data.Transaction.FundingMemo.LimitIDinFin10;
                    funding.LimitExpiryDate = data.Transaction.FundingMemo.LimitExpiryDate;
                    funding.SanctionLimitCurrID = data.Transaction.FundingMemo.SanctionLimitCurrID;
                    funding.SanctionLimit = data.Transaction.FundingMemo.SanctionLimitAmount;
                    funding.UtilizationCurrID = data.Transaction.FundingMemo.utilizationID.ID;
                    funding.utilization = data.Transaction.FundingMemo.utilizationAmount;
                    funding.Outstanding = data.Transaction.FundingMemo.OutstandingAmount;
                    funding.OutstandingCurrID = data.Transaction.FundingMemo.OutstandingID.ID;
                    //add refere currency
                    //End


                    funding.DoAName = data.Transaction.FundingMemo.DoAName;

                    funding.CSOName = data.Transaction.FundingMemo.CSOName;


                    funding.UpdateDate = DateTime.UtcNow;
                    funding.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                    context.SaveChanges();
                    Entity.Transaction trans = context.Transactions.Where(a => a.TransactionID == getFundingID.TransactionID).FirstOrDefault();
                    trans.ValueDate = data.Transaction.ValueDate;
                    context.SaveChanges();



                    if (data.Transaction.Documents != null)
                    {
                        var existingDocuments = (from x in context.TransactionDocuments
                                                 where x.TransactionID == data.Transaction.ID && x.IsDeleted == false
                                                 select x);
                        if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                        {
                            foreach (var item in existingDocuments.ToList()) // remove All transaction document
                            {
                                //if (item.PurposeID != 2)
                                {
                                    context.TransactionDocuments.Remove(item);
                                }
                            }
                            context.SaveChanges();

                            foreach (var item in data.Transaction.Documents) //Add All Transaction document
                            {
                                //if (item.Purpose.ID != 2)
                                {
                                    Entity.TransactionDocument document = new Entity.TransactionDocument()
                                    {
                                        TransactionID = data.Transaction.ID,
                                        DocTypeID = item.Type.ID,
                                        PurposeID = item.Purpose.ID,
                                        Filename = item.FileName,
                                        IsDeleted = false,
                                        DocumentPath = item.DocumentPath,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().DisplayName
                                    };

                                    context.TransactionDocuments.Add(document);
                                }
                                context.SaveChanges();

                            }
                        }
                        else
                        {
                            foreach (var item in data.Transaction.Documents) //Add All Transaction document
                            {
                                //if (item.Purpose.ID != 2)
                                {
                                    Entity.TransactionDocument document = new Entity.TransactionDocument()
                                    {
                                        TransactionID = data.Transaction.ID,
                                        DocTypeID = item.Type.ID,
                                        PurposeID = item.Purpose.ID,
                                        Filename = item.FileName,
                                        IsDeleted = false,
                                        DocumentPath = item.DocumentPath,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().DisplayName
                                    };

                                    context.TransactionDocuments.Add(document);
                                }
                                context.SaveChanges();

                            }
                        }

                    }
                    ts.Complete();

                    IsSuccess = true;
                }

                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }

            }
            return IsSuccess;

        }


        #endregion
        #endregion

        #region repoAndri
        #region LoanPPUCheckerModel
        public bool GetTransactionLoanPPUChecker(Guid workflowInstanceID, long approverID, ref TransactionLoanModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                TransactionLoanModel datatransaction = (from a in context.Transactions
                                                        where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                                                        select new TransactionLoanModel
                                                        {
                                                            ID = a.TransactionID,
                                                            WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                                            ApplicationID = a.ApplicationID,
                                                            ApproverID = a.ApproverID != null ? a.ApproverID : 0,

                                                            Product = new ProductModel()
                                                            {
                                                                ID = a.Product.ProductID,
                                                                Code = a.Product.ProductCode,
                                                                Name = a.Product.ProductName,
                                                                WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                                                Workflow = a.Product.ProductWorkflow.WorkflowName,
                                                                LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                                                LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                                            },
                                                            Currency = new CurrencyModel()
                                                            {
                                                                ID = a.Currency.CurrencyID,
                                                                Code = a.Currency.CurrencyCode,
                                                                Description = a.Currency.CurrencyDescription,
                                                                LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                                LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                            },
                                                            SourceID = a.SourceID.HasValue? a.SourceID.Value: 0,
                                                            Amount = a.Amount,
                                                            AmountUSD = a.AmountUSD,
                                                            Rate = a.Rate,
                                                            Channel = new ChannelModel()
                                                            {
                                                                ID = a.Channel.ChannelID,
                                                                Name = a.Channel.ChannelName,
                                                                LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                                                LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                                            },

                                                            BizSegment = new BizSegmentModel()
                                                            {
                                                                ID = a.BizSegment.BizSegmentID,
                                                                Name = a.BizSegment.BizSegmentName,
                                                                Description = a.BizSegment.BizSegmentDescription,
                                                                LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                                LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                            },

                                                            LoanContractNo = a.LoanContractNo,

                                                            IsTopUrgent = a.IsTopUrgent,

                                                            Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                                                            {
                                                                ID = x.TransactionDocumentID,
                                                                Type = new DocumentTypeModel()
                                                                {
                                                                    ID = x.DocumentType.DocTypeID,
                                                                    Name = x.DocumentType.DocTypeName,
                                                                    Description = x.DocumentType.DocTypeDescription,
                                                                    LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                    LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                },
                                                                Purpose = new DocumentPurposeModel()
                                                                {
                                                                    ID = x.DocumentPurpose.PurposeID,
                                                                    Name = x.DocumentPurpose.PurposeName,
                                                                    Description = x.DocumentPurpose.PurposeDescription,
                                                                    LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                    LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value

                                                                },
                                                                FileName = x.Filename,
                                                                DocumentPath = x.DocumentPath,
                                                                LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value,
                                                                IsDormant = x.IsDormant == null ? false : x.IsDormant

                                                            }).ToList(),
                                                            CreateDate = a.CreateDate,
                                                            LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                            LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                        }).SingleOrDefault();



                if (datatransaction != null)
                {
                    //datatransaction.IsOtherBeneBank = datatransaction.Bank.Code != "999" ? false : true;
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    // changed, chandra : 2015.03.23
                    if (customerRepo.GetCustomerByTransaction(datatransaction.ID, ref customer, ref message))
                    {
                        datatransaction.Customer = customer;
                    }

                    var data = context.Transactions.Where(x => x.TransactionID.Equals(datatransaction.ID)).Select(x => x).FirstOrDefault();




                    customerRepo.Dispose();
                    customer = null;
                    long IDTrns = datatransaction.ID;
                    var checker = context.TransactionCheckerDatas.Where(x => x.TransactionID.Equals(IDTrns)).Select(x => x).OrderByDescending(b => b.ApproverID).FirstOrDefault();
                    if (checker != null)
                    {
                        datatransaction.IsSignatureVerified = checker.IsSignatureVerified;
                        //basri 30-09-2015x.UpdateDate == null ? x.CreateBy : x.UpdateBy
                        datatransaction.IsDocumentComplete = false;
                        datatransaction.IsCallbackRequired = false;
                        datatransaction.IsLOIAvailable = checker.IsLOIAvailable;
                        //end basri
                        datatransaction.IsDormantAccount = checker.IsDormantAccount;
                        datatransaction.IsFreezeAccount = checker.IsFreezeAccount;
                        datatransaction.IsSyndication = checker.IsSyndication;
                    }
                    else
                    {
                        datatransaction.IsSignatureVerified = false;
                        //basri 30-09-2015x.UpdateDate == null ? x.CreateBy : x.UpdateBy
                        datatransaction.IsDocumentComplete = false;
                        datatransaction.IsCallbackRequired = false;
                        datatransaction.IsLOIAvailable = false;
                        //end basri
                        datatransaction.IsDormantAccount = false;
                        datatransaction.IsFreezeAccount = false;
                        datatransaction.IsSyndication = false;
                    }

                }
                {
                    transaction = datatransaction;
                }
                //end fundingmemo
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }

        public bool GetLoanUpcountryBranchCheckerDetails(Guid workflowInstanceID, long approverID, ref TransactionLoanModel transaction, ref IList<VerifyLoanModel> verify, ref TransactionDisbursementModel disbursement, ref IList<TransactionRolloverModel> rollover, ref IList<TransactionInterestMaintenanceModel> im, ref string message)
        {
            bool IsSuccess = false;
            context.Transactions.AsNoTracking();
            // get main transaction data
            try
            {
                transaction = (from a in context.Transactions
                               where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                               select new TransactionLoanModel
                               {
                                   ID = a.TransactionID,
                                   WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                   ApplicationID = a.ApplicationID,
                                   ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                                   Amount = a.Amount,
                                   ValueDate = a.ValueDate,
                                   BizSegment = new BizSegmentModel()
                                   {
                                       ID = a.BizSegment.BizSegmentID,
                                       Name = a.BizSegment.BizSegmentName,
                                       Description = a.BizSegment.BizSegmentDescription
                                   },
                                   Currency = new CurrencyModel()
                                   {
                                       Code = a.Currency.CurrencyCode,
                                       Description = a.Currency.CurrencyDescription
                                   },
                                   Product = new ProductModel()
                                   {
                                       ID = a.Product.ProductID,
                                       Code = a.Product.ProductCode,
                                       Name = a.Product.ProductName,
                                       WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                       Workflow = a.Product.ProductWorkflow.WorkflowName,
                                       LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                       LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                   },
                                   Channel = new ChannelModel()
                                   {
                                       ID = a.Channel.ChannelID,
                                       Name = a.Channel.ChannelName,
                                       LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                       LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                   },
                                   Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                                   {
                                       ID = x.TransactionDocumentID,
                                       Type = new DocumentTypeModel()
                                       {
                                           ID = x.DocumentType.DocTypeID,
                                           Name = x.DocumentType.DocTypeName,
                                           Description = x.DocumentType.DocTypeDescription,
                                           LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                           LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                       },
                                       Purpose = new DocumentPurposeModel()
                                       {
                                           ID = x.DocumentPurpose.PurposeID,
                                           Name = x.DocumentPurpose.PurposeName,
                                           Description = x.DocumentPurpose.PurposeDescription,
                                           LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                           LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                       },
                                       FileName = x.Filename,
                                       DocumentPath = x.DocumentPath,
                                       LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                       LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                   }).ToList(),
                                   CreateDate = a.CreateDate,
                                   LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                   LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                   IsTopUrgent = a.IsTopUrgent
                               }).FirstOrDefault();

                if (transaction != null)
                {
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();

                    if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                    {
                        transaction.Customer = customer;
                    }
                    customerRepo.Dispose();
                    customer = null;
                    long transID = transaction.ID;
                    verify = context.TransactionCheckers
                                .Where(x => x.TransactionID.Equals(transID) && x.IsDeleted == false)
                                .Select(x => new VerifyLoanModel()
                                {
                                    ID = x.TransactionColumnID,
                                    Name = x.TransactionColumn.ColumnName,
                                    IsVerified = x.IsVerified
                                }).ToList();


                    var checker = context.TransactionCheckerDatas.Where(x => x.TransactionID.Equals(transID)).Select(x => x).OrderByDescending(b => b.ApproverID).FirstOrDefault();
                    if (checker != null)
                    {
                        transaction.IsSignatureVerified = checker.IsSignatureVerified;
                        transaction.IsDocumentComplete = false;
                        transaction.IsCallbackRequired = false;
                        transaction.IsLOIAvailable = checker.IsLOIAvailable;
                        transaction.IsDormantAccount = checker.IsDormantAccount;
                        transaction.IsFreezeAccount = checker.IsFreezeAccount;
                        transaction.IsSyndication = checker.IsSyndication;
                    }
                    else
                    {
                        transaction.IsSignatureVerified = false;
                        transaction.IsDocumentComplete = false;
                        transaction.IsCallbackRequired = false;
                        transaction.IsLOIAvailable = false;
                        transaction.IsDormantAccount = false;
                        transaction.IsFreezeAccount = false;
                        transaction.IsSyndication = false;
                    }

                    disbursement = new TransactionDisbursementModel();
                    rollover = new List<TransactionRolloverModel>();
                    im = new List<TransactionInterestMaintenanceModel>();

                    switch (transaction.Product.ID)
                    {
                        case (int)DBSProductID.LoanDisbursmentProductIDCons:
                            disbursement = (from f in context.SP_GetFundingMemoTransactionComplete(transID)
                                            select new TransactionDisbursementModel
                                            {

                                                BizName = f.BizName,
                                                Customer = new CustomerModel()
                                                {
                                                    CIF = f.CIF,
                                                    Name = f.CustomerName
                                                },
                                                SolID = f.solidfun == null ? string.Empty : f.solidfun,
                                                BizSegment = new BizSegmentModel()
                                                {
                                                    ID = f.BizSegmentID ?? 0,
                                                    Name = f.BizName
                                                },

                                                peggingname = f.peegingfreq,
                                                interestname = f.interestratecode,
                                                ValueDateFun = f.valuedatefun,
                                                CustomerCategoryID = f.CustomerCategoryID ?? 0,
                                                CustomerCategoryName = f.CustomerCategoryName == null ? string.Empty : f.CustomerCategoryName,
                                                ProgramTypeID = f.ProgramTypeID.HasValue ? f.ProgramTypeID.Value : 0,
                                                ProgramTypeName = f.ProgramTypeName,
                                                LoanTypeID = f.LoanTypeID.HasValue ? f.LoanTypeID.Value : 0,
                                                LoanTypeName = f.LoanTypeName,

                                                FinacleSchemeCodeID = f.FinacleSchemeCodeID ?? 0,
                                                FinacleSchemeCodeName = f.FinacleSchemeCodeName,
                                                IsAdhoc = f.IsAdhoc.HasValue ? f.IsAdhoc.Value : false,
                                                AmountDisburse = f.AmountDisburse ?? 0,
                                                CreditingOperativeID = f.CreditingOperativeID.HasValue ? f.CreditingOperativeID.Value : 0,
                                                DebitingOperativeID = f.DebitingOperativeID.HasValue ? f.DebitingOperativeID.Value : 0,
                                                MaturityDate = f.MaturityDate.HasValue ? f.MaturityDate.Value : DateTime.Now,
                                                InterestCodeID = f.InterestCodeID,
                                                creditoperaitv = f.CreditingOperative,
                                                transactionAmount = f.AmountDisburse.Value,
                                                debitingoperative = f.AmountDisburse.HasValue ? f.DebitingOperative : "0",
                                                ApprovedMarginAsPerCM = f.ApprovedMarginAsPerCM.HasValue ? f.ApprovedMarginAsPerCM.Value : 0,
                                                BaseRate = f.BaseRate.HasValue ? f.BaseRate.Value : 0,
                                                SpecialFTP = f.SpecialFTP.HasValue ? f.SpecialFTP.Value : 0,
                                                AccountPreferencial = f.AccountPreferencial ?? 0,
                                                Margin = f.Margin.HasValue ? f.Margin.Value : 0,
                                                AllInRate = f.AllInRate.HasValue ? f.AllInRate.Value : 0,
                                                AllInSpecialRate = f.AllInSpecialRate ?? 0,
                                                RepricingPlanID = f.RepricingPlanID ?? 0,
                                                PeggingFrequencyID = f.PeggingFrequencyID ?? 0,
                                                PeggingDate = f.PeggingDate,
                                                PrincipalDate = f.PrincipalStartDate,
                                                InterestStartDate = f.InterestStartDate,
                                                PrincipalFrequencyID = f.PrincipalFrequencyID ?? 0,
                                                InterestFrequencyID = f.InterestFrequencyID ?? 0,
                                                NoOfInstallment = f.NoOfInstallment,
                                                LimitIDinFin10 = f.LimitIDinFin10,
                                                SanctionLimitCurr = new CurrencyModel()
                                                {
                                                    Code = f.SactionLimitCurrCode
                                                },
                                                SanctionLimit = f.SanctionLimit.Value,
                                                LimitExpiryDate = f.LimitExpiryDate,
                                                UtilizationCurr = new CurrencyModel()
                                                {
                                                    Code = f.UtilizationCurrCode
                                                },
                                                utilization = f.utilization,
                                                OutstandingCurr = new CurrencyModel() { Code = f.OutstandingCurrCode },
                                                Outstanding = f.Outstanding.HasValue ? f.Outstanding.Value : 0,
                                                CSOName = f.CSOName,
                                                DoaName = f.DoAName
                                            }).FirstOrDefault();
                            if (disbursement == null)
                            {
                                disbursement = new TransactionDisbursementModel();
                                disbursement.Customer = new CustomerModel();
                                disbursement.BizSegment = new BizSegmentModel();
                                disbursement.SanctionLimitCurr = new CurrencyModel();
                                disbursement.UtilizationCurr = new CurrencyModel();
                                disbursement.OutstandingCurr = new CurrencyModel();
                            }
                            break;
                        case (int)DBSProductID.LoanIMProductIDCons:
                            im = (from b in context.CSOInterestMaintenances
                                  join c in context.TransactionInterestMaintenances
                                  on b.TransactionInterestMaintenanceID equals c.TransactionInterestMaintenanceID
                                  join d in context.BizSegments
                                  on b.BizSegmentID equals d.BizSegmentID
                                  where c.TransactionID == transID
                                  select new TransactionInterestMaintenanceModel
                                  {
                                      SolID = b.SolID,
                                      SchemeCode = b.SchemeCode,
                                      CIF = b.CIF,
                                      CustomerName = b.Customer.CustomerName,
                                      LoanContractNo = b.LoanContractNo,
                                      DueDate = b.DueDate.Value,
                                      CurrencyCode = b.Currency.CurrencyCode,
                                      PreviousInterestRate = b.PreviousInterestRate.Value,
                                      CurrentInterestRate = b.CurrentInterestRate.Value,
                                      BizSegmentName = d.BizSegmentName,
                                      EmployeeName = b.EmployeeID == null ? "" : context.Employees.Where(x => x.EmployeeID.Equals(b.EmployeeID.Value)).Select(x => x.EmployeeName).FirstOrDefault(),
                                      CSO = b.CSO,
                                      IsSBLCSecured = b.IsSBLCSecured == null ? false : b.IsSBLCSecured.Value,
                                      IsHeavyEquipment = b.IsHeavyEquipment == null ? false : b.IsHeavyEquipment.Value,
                                      MaintenanceTypeID = b.MaintenanceTypeID.HasValue ? b.MaintenanceTypeID.Value : 0,
                                      NextPrincipalDate = b.NextPrincipalDate.HasValue ? b.NextPrincipalDate.Value : DateTime.Now,
                                      NextInterestDate = b.NextInterestDate.HasValue ? b.NextInterestDate.Value : DateTime.Now,
                                      ApprovedMarginAsPerCM = b.ApprovedMarginAsPerCM.HasValue ? b.ApprovedMarginAsPerCM.Value : 0,
                                      InterestRateCodeID = b.InterestRateCodeID.HasValue ? b.InterestRateCodeID : 0,
                                      BaseRate = b.BaseRate.HasValue ? b.BaseRate.Value : 0,
                                      AllinRate = b.AllinRate.HasValue ? b.AllinRate.Value : 0,
                                      AllinFTPRate = b.AllinFTPRate.HasValue ? b.AllinFTPRate.Value : 0,
                                      AccountPreferencial = b.AccountPreferencial.HasValue ? b.AccountPreferencial.Value : 0,
                                      SpecialFTP = b.SpecialFTP.HasValue ? b.SpecialFTP.Value : 0,
                                      SpecialRate = b.SpecialRateMargin.Value,
                                      AllinLP = b.AllinRate.HasValue ? b.AllinRate.Value : 0,
                                      ApprovalDOA = b.ApprovalDOA,
                                      ApprovedBy = b.ApprovedBy,
                                      Remarks = b.Remarks,

                                  }).ToList();
                            if (im == null) im = new List<TransactionInterestMaintenanceModel>();
                            break;
                        case (int)DBSProductID.LoanRolloverProductIDCons:
                        case (int)DBSProductID.LoanSettlementProductIDCons:
                            rollover = (from b in context.CSORollovers
                                        join c in context.TransactionRollovers
                                        on b.TransactionRolloverID equals c.TransactionRolloverID
                                        join d in context.BizSegments
                                        on b.BizSegmentID equals d.BizSegmentID
                                        where c.TransactionID == transID
                                        select new TransactionRolloverModel
                                        {
                                            SOLID = b.SOLID,
                                            SchemeCode = b.SchemeCode,
                                            CIF = b.CIF,
                                            CustomerName = b.Customer.CustomerName,
                                            LoanContractNo = b.LoanContractNo,
                                            DueDate = b.DueDate.Value,
                                            Currency = new CurrencyModel() { Code = b.Currency.CurrencyCode },
                                            AmountDue = b.AmountDue.Value,
                                            LimitID = b.LimitID,
                                            BizSegment = new BizSegmentModel() { ID = b.BizSegmentID.Value, Name = d.BizSegmentName },
                                            RM = new EmployeeModel()
                                            {
                                                EmployeeID = b.EmployeeID == null ? 0 : b.EmployeeID.Value,
                                                EmployeeName = b.EmployeeID == null ? "" : context.Employees.Where(x => x.EmployeeID.Equals(b.EmployeeID.Value)).Select(x => x.EmployeeName).FirstOrDefault()
                                            },
                                            CSO = b.CSO,
                                            IsSBLCSecured = b.IsSBLCSecured == null ? false : b.IsSBLCSecured.Value,
                                            IsHeavyEquipment = b.IsHeavyEquipment == null ? false : b.IsHeavyEquipment.Value,
                                            MaintenanceTypeID = b.MaintenanceTypeID.HasValue ? b.MaintenanceTypeID.Value : 0,
                                            AmountRollover = b.AmountRollover.HasValue ? b.AmountRollover.Value : 0,
                                            NextPrincipalDate = b.NextPrincipalDate.HasValue ? b.NextPrincipalDate.Value : DateTime.Now,
                                            NextInterestDate = b.NextInterestDate.HasValue ? b.NextInterestDate.Value : DateTime.Now,
                                            ApprovedMarginAsPerCM = b.ApprovedMarginAsPerCM.HasValue ? b.ApprovedMarginAsPerCM.Value : 0,
                                            InterestRateCodeID = b.InterestRateCodeID.HasValue ? b.InterestRateCodeID : 0,
                                            BaseRate = b.BaseRate.HasValue ? b.BaseRate.Value : 0,
                                            AllInLP = b.AllInLP.HasValue ? b.AllInLP.Value : 0,
                                            AllInFTPRate = b.AllInFTPRate.HasValue ? b.AllInFTPRate.Value : 0,
                                            AccountPreferencial = b.AccountPreferencial.HasValue ? b.AccountPreferencial.Value : 0,
                                            SpecialFTP = b.SpecialFTP.HasValue ? b.SpecialFTP.Value : 0,
                                            SpecialRateMargin = b.SpecialRateMargin.HasValue ? b.SpecialRateMargin.Value : 0,
                                            AllInrate = b.AllInrate.HasValue ? b.AllInrate.Value : 0,
                                            ApprovalDOA = b.ApprovalDOA,
                                            ApprovedBy = b.ApprovedBy,
                                            Remarks = b.Remarks,
                                            IsAdhoc = b.IsAdhoc.HasValue ? b.IsAdhoc.Value : false,
                                            CSOName = b.CSOName,

                                        }).ToList();
                            if (rollover == null) rollover = new List<TransactionRolloverModel>();
                            break;
                    }
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetCheckerLoanPPUchecker(Guid workflowInstanceID, long approverID, ref LoanPPUCheckerModel output, ref string message)
        {
            bool IsSuccess = false;

            var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

            try
            {
                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new LoanPPUCheckerModel
                          {
                              Verify = a.TransactionCheckers
                                .Where(x => x.TransactionID.Equals(transactionID) && x.IsDeleted == false)
                                .Select(x => new VerifyLoanModel()
                                {
                                    ID = x.TransactionColumnID,
                                    Name = x.TransactionColumn.ColumnName,
                                    IsVerified = x.IsVerified
                                }).ToList()
                          }).SingleOrDefault();
                if (!output.Verify.Any())
                {
                    output.Verify = context.TransactionColumns
                        //.Where(x => x.IsPPUCheckerLoan.Equals(true) && x.IsDeleted.Equals(false))
                        .Where(x => x.IsDeleted.Equals(false) && x.IsPPUCheckerLoan == true)
                        .Select(x => new VerifyLoanModel()
                        {
                            ID = x.TransactionColumnID,
                            Name = x.ColumnName,
                            IsVerified = false
                        })
                        .ToList();
                }


                IsSuccess = true;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string _message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(_message, raise);
                    }
                }
                throw raise;
            }

            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }


        public bool AddPPUCheckerLoan(Guid workflowInstanceID, long approverID, LoanPPUCheckerModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

                    if (transactionID > 0)
                    {
                        Entity.Transaction data_t = context.Transactions.Where(a => a.TransactionID.Equals(transactionID)).SingleOrDefault();
                        if (data_t != null)
                        {
                            //data_t.IsDocumentComplete = data.Verify.ToList().Where(a => a.Name.Equals("Document Completeness")).FirstOrDefault().IsVerified;
                            data_t.IsDocumentComplete = data.Transaction.IsDocumentComplete;
                            context.SaveChanges();
                        }

                        var checker = context.TransactionCheckers
                           .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                           .ToList();

                        var checkerData = context.TransactionCheckerDatas
                            .Where(a => a.TransactionID.Equals(transactionID))
                            .SingleOrDefault();

                        #region Transaction Checker Data
                        if (checkerData != null)
                        {
                            checkerData.IsCallbackRequired = data.Transaction.IsCallbackRequired;
                            checkerData.IsSyndication = data.Transaction.IsSyndication;
                            checkerData.IsSignatureVerified = data.Transaction.IsSignatureVerified;
                            checkerData.IsDormantAccount = data.Transaction.IsDormantAccount;
                            checkerData.IsFreezeAccount = data.Transaction.IsFreezeAccount;
                            checkerData.IsLOIAvailable = data.Transaction.IsLOIAvailable;
                            checkerData.UpdateDate = DateTime.UtcNow;
                            checkerData.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                            context.SaveChanges();
                        }
                        else
                        {
                            // Insert
                            context.TransactionCheckerDatas.Add(new TransactionCheckerData()
                            {
                                ApproverID = approverID,
                                TransactionID = transactionID,
                                IsSignatureVerified = data.Transaction.IsSignatureVerified,
                                IsSyndication = data.Transaction.IsSyndication,
                                IsDormantAccount = data.Transaction.IsDormantAccount,
                                IsFreezeAccount = data.Transaction.IsFreezeAccount,
                                IsCallbackRequired = data.Transaction.IsCallbackRequired,
                                IsLOIAvailable = data.Transaction.IsLOIAvailable,
                                IsStatementLetterCopy = false,
                                CreatedDate = DateTime.UtcNow,
                                CreatedBy = currentUser.GetCurrentUser().DisplayName
                            });

                            context.SaveChanges();
                        }
                        #endregion

                        #region Transaction Checker
                        if (checker.Count > 0)
                        {
                            var checkerdata = context.TransactionCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                            foreach (var item in checkerdata)
                            {
                                context.TransactionCheckers.Remove(item);
                            }
                            context.SaveChanges();
                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = true,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });

                                context.SaveChanges();
                            }



                        }
                        else
                        {
                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = true,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });

                                context.SaveChanges();
                            }
                        }
                        if (data.Transaction.IsCallbackRequired == true)
                        {

                            // insert trasaction callback data
                            DBS.Entity.TransactionCallbackData dataCallback = new DBS.Entity.TransactionCallbackData()
                            {
                                TransactionID = data.Transaction.ID,
                                ApproverID = approverID,
                                CurrencyID = data.Transaction.Currency.ID,
                                Amount = data.Transaction.Amount,
                                AmountUSD = data.Transaction.AmountUSD,
                                Rate = 0,
                                ChannelID = data.Transaction.Channel.ID,
                                SourceID = data.Transaction.SourceID,
                                AccountNumber = null,
                                OtherAccountNumber = null,
                                DebitCurrencyID = data.Transaction.Currency.ID,
                                ApplicationDate = DateTime.UtcNow,
                                LLDID = null,
                                BankID = null,
                                OtherBeneBankName = null,
                                OtherBeneBankSwift = null,
                                BeneACCNumber = null,
                                IsSignatureVerified = data.Transaction.IsSignatureVerified,
                                IsDormantAccount = data.Transaction.IsDormantAccount,
                                IsFreezeAccount = data.Transaction.IsFreezeAccount,
                                IsOtherAccountNumber = false,
                                DetailCompliance = null,
                                Other = null,
                                IsSyndication = data.Transaction.IsSyndication,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().DisplayName,
                                UpdateBy = null,
                                UpdateDate = null,
                                PaymentDetails = null
                            };
                            context.TransactionCallbackDatas.Add(dataCallback);
                            context.SaveChanges();
                        }
                        #endregion

                        checker = null;
                        checkerData = null;
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string _message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(_message, raise);
                        }
                    }
                    throw raise;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }
        public bool UpdateTransactionUpcountryCheckerDetails(Guid workflowInstanceID, long approverID, IList<VerifyLoanModel> data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

                    if (transactionID > 0)
                    {
                        var checker = context.TransactionCheckers
                           .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                           .ToList();

                        if (checker.Count > 0)
                        {
                            var checkerdata = context.TransactionCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                            foreach (var item in checkerdata)
                            {
                                context.TransactionCheckers.Remove(item);
                            }
                            context.SaveChanges();

                            foreach (var item in data)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });
                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            foreach (var item in data)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });

                                context.SaveChanges();
                            }
                        }

                        checker = null;
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }
        public bool UpdateTransactionCheckerDetails(Guid workflowInstanceID, long approverID, LoanPPUCheckerModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

                    if (transactionID > 0)
                    {
                        var checker = context.TransactionCheckers
                           .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                           .ToList();

                        if (checker.Count > 0)
                        {
                            var checkerdata = context.TransactionCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                            foreach (var item in checkerdata)
                            {
                                context.TransactionCheckers.Remove(item);
                            }
                            //context.SaveChanges();

                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });
                            }
                            context.SaveChanges();
                        }
                        else
                        {
                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });

                                context.SaveChanges();
                            }
                        }

                        checker = null;
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }

        #region Loan Revise Maker Checker
        public bool UpdateTransactionIM(Guid workflowInstanceID, long approverID, LoanCheckerInterestMaintenance output, ref string message)
        {
            bool IsSuccess = false;

            using (DBSEntities context = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        foreach (var item in output.LoanCheckerMakerIM)
                        {
                            if (item.IsChecked == true)
                            {
                                var updateIsSelected = context.TransactionInterestMaintenances.Where(a => a.TransactionInterestMaintenanceID.Equals(item.TransactionInterestMaintenanceID)).SingleOrDefault();
                                updateIsSelected.IsSelected = true;
                            }
                            else
                            {
                                var updateIsSelected = context.TransactionInterestMaintenances.Where(a => a.TransactionInterestMaintenanceID.Equals(item.TransactionInterestMaintenanceID)).SingleOrDefault();
                                updateIsSelected.IsSelected = false;
                            }
                        }
                        context.SaveChanges();

                    }
                    catch (Exception ex)
                    {
                        message = ex.InnerException.Message;
                    }
                    ts.Complete();

                    IsSuccess = true;
                }
                return IsSuccess;
            }
        }
        public bool UpdateTransactionRollover(Guid workflowInstanceID, long approverID, LoanCheckerRollover output, ref string message)
        {
            bool IsSuccess = false;

            using (DBSEntities context = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        foreach (var item in output.LoanCheckerMakerRollover)
                        {
                            if (item.IsChecked == true)
                            {
                                var updateIsSelected = context.TransactionRollovers.Where(a => a.TransactionRolloverID.Equals(item.TransactionRolloverID)).SingleOrDefault();
                                updateIsSelected.IsSelected = true;
                            }
                            else
                            {
                                var updateIsSelected = context.TransactionRollovers.Where(a => a.TransactionRolloverID.Equals(item.TransactionRolloverID)).SingleOrDefault();
                                updateIsSelected.IsSelected = false;
                            }
                        }
                        context.SaveChanges();

                    }
                    catch (Exception ex)
                    {
                        message = ex.InnerException.Message;
                    }
                    ts.Complete();

                    IsSuccess = true;
                }
                return IsSuccess;
            }
        }
        public bool ReviseTransactionIMChecker(Guid workflowInstanceID, long approverID, InterestMaintenanceExcelCheckerLoan output, ref string message)
        {
            bool IsSuccess = false;

            using (DBSEntities context = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        foreach (var item in output.LoanCheckerIM)
                        {
                            if (item.IsChecked == true)
                            {
                                var updateIsSelected = context.TransactionInterestMaintenances.Where(a => a.TransactionInterestMaintenanceID.Equals(item.TransactionInterestMaintenanceID)).SingleOrDefault();
                                updateIsSelected.IsSelected = true;
                            }
                            else
                            {
                                var updateIsSelected = context.TransactionInterestMaintenances.Where(a => a.TransactionInterestMaintenanceID.Equals(item.TransactionInterestMaintenanceID)).SingleOrDefault();
                                updateIsSelected.IsSelected = false;
                            }
                        }
                        context.SaveChanges();

                    }
                    catch (Exception ex)
                    {
                        message = ex.InnerException.Message;
                    }
                    ts.Complete();

                    IsSuccess = true;
                }
                return IsSuccess;
            }
        }
        public bool ReviseTransactionRolloverChecker(Guid workflowInstanceID, long approverID, DataCheckerRollover output, ref string message)
        {
            bool IsSuccess = false;

            using (DBSEntities context = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        foreach (var item in output.CheckerRollover)
                        {
                            if (item.IsChecked == true)
                            {
                                var updateIsSelected = context.TransactionRollovers.Where(a => a.TransactionRolloverID.Equals(item.TransactionRolloverID)).SingleOrDefault();
                                updateIsSelected.IsSelected = true;
                            }
                            else
                            {
                                var updateIsSelected = context.TransactionRollovers.Where(a => a.TransactionRolloverID.Equals(item.TransactionRolloverID)).SingleOrDefault();
                                updateIsSelected.IsSelected = false;
                            }
                        }
                        context.SaveChanges();

                    }
                    catch (Exception ex)
                    {
                        message = ex.InnerException.Message;
                    }
                    ts.Complete();

                    IsSuccess = true;
                }
                return IsSuccess;
            }
        }

        #endregion
        public bool AddLoanChecker(Guid workflowInstanceID, long approverID, ref LoanCheckerDetailLoanModel output, ref string message)
        {
            var transactionID = context.Transactions
                               .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                               .Select(a => a.TransactionID)
                               .SingleOrDefault();
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (transactionID > 0)
                    {
                        //update document completed


                        var checker = context.TransactionCheckers
                           .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                           .ToList();

                        if (checker.Count > 0)
                        {
                            var checkerdata = context.TransactionCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                            foreach (var item in checkerdata)
                            {
                                context.TransactionCheckers.Remove(item);
                            }
                            context.SaveChanges();


                        }


                        checker = null;
                    }

                    IList<DBS.Entity.TransactionChecker> selectedModel = (from item in output.Verify
                                                                          select new DBS.Entity.TransactionChecker
                                                                          {
                                                                              ApproverID = approverID,
                                                                              TransactionColumnID = item.ID,
                                                                              TransactionID = transactionID,
                                                                              IsVerified = item.IsVerified,
                                                                              IsDeleted = false,
                                                                              CreateDate = DateTime.UtcNow,
                                                                              CreateBy = currentUser.GetCurrentUser().LoginName,
                                                                              UpdateBy = null,
                                                                              UpdateDate = null,

                                                                          }).ToList();
                    // context.TransactionCheckers.AddRange(selectedModel);
                    context.SaveChanges();

                    Entity.Transaction Trans = context.Transactions.Where(a => a.TransactionID == transactionID).FirstOrDefault();
                    Trans.LoanContractNo = output.Transaction.LoanContractNo == null ? null : output.Transaction.LoanContractNo;
                    Trans.ValueDate = output.Transaction.ValueDate == null ? null : output.Transaction.ValueDate;
                    Trans.LoanTransID = output.Transaction.LoanTransID == null ? null : output.Transaction.LoanTransID;
                    context.SaveChanges();
                    //InvestmentID invest = new InvestmentID()
                    //{
                    //    CIF = output.Transaction.Customer.CIF,
                    //    IsUT = true,
                    //    IsBond = false,
                    //    STNumber = null,
                    //    CheckIN = null,
                    //    CheckCIF = null,
                    //    CheckName = null,
                    //    CheckST = null,
                    //    IsDelete = false,
                    //    CreateDate = DateTime.UtcNow,
                    //    CreateBy = currentUser.GetCurrentUser().LoginName,
                    //    Investment = output.Transaction.InvestmenName,

                    //};

                    //context.InvestmentIDs.Add(invest);
                    //context.SaveChanges();



                    ts.Complete();

                    IsSuccess = true;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string _message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(_message, raise);
                        }
                    }
                    throw raise;
                }

                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();

                }
            }
            return IsSuccess;
        }

        public bool AddReviseCSOChecker(Guid workflowInstanceID, long approverID, TransactionPPUCheckerCSODetailModel output, ref string message)
        {
            var transactionID = context.Transactions
                               .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                               .Select(a => a.TransactionID)
                               .SingleOrDefault();
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var checker = context.TransactionCheckers
                          .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                          .ToList();
                    if (checker.Count > 0)
                    {
                        var checkerdata = context.TransactionCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                        foreach (var item in checkerdata)
                        {
                            context.TransactionCheckers.Remove(item);
                        }
                        context.SaveChanges();


                    }
                    ts.Complete();

                    IsSuccess = true;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string _message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(_message, raise);
                        }
                    }
                    throw raise;
                }

                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();

                }
            }
            return IsSuccess;
        }
        public bool AddLoanMakerTask(Guid workflowInstanceID, long approverID, LoanCheckerDetailLoanModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                var transactionID = context.Transactions
                      .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                      .Select(a => a.TransactionID)
                      .SingleOrDefault();
                try
                {
                    var getFundingID = context.TransactionFundingMemoes
                            .Where(a => a.TransactionID.Equals(transactionID))
                            .SingleOrDefault();

                    //var funding = context.CSOFundingMemoes
                    if (getFundingID != null)
                    {
                        Entity.CSOFundingMemo funding = context.CSOFundingMemoes
                            .Where(a => a.CSOFundingMemoID.Equals(data.Transaction.FundingMemo.CSOFundingMemoID == null ? 0 : data.Transaction.FundingMemo.CSOFundingMemoID.Value))
                        .FirstOrDefault();



                        //funding = new CSOFundingMemo();
                        //satu
                        funding.TransactionFundingMemoID = data.Transaction.ID;//data.Transaction.ID;
                        //funding.CIF = data.Transaction.FundingMemo.CIF;
                        //funding.SolID = data.Transaction.FundingMemo.SolID;
                        //funding.BizSegmentID = data.Transaction.BizSegment.ID;
                        funding.CustomerCategoryID = data.Transaction.FundingMemo.CustomerCategoryID;
                        funding.ProgramTypeID = data.Transaction.FundingMemo.ProgramTypeID;//add
                        funding.BizName = data.Transaction.FundingMemo.BizName;
                        funding.FinacleSchemeCodeID = data.Transaction.FundingMemo.FinacleSchemeCodeID;//add
                        funding.LoanTypeID = data.Transaction.FundingMemo.LoanTypeID;//add
                        funding.IsAdhoc = data.Transaction.FundingMemo.IsAdhoc;//data.Transaction.FundingMemo.IsAdhoc,
                        //EndSatu

                        //Dua
                        funding.AmountDisburse = data.Transaction.FundingMemo.AmountDisburse;
                        funding.AmountDisburseID = data.Transaction.FundingMemo.AmountDisburseID;
                        funding.ValueDate = data.Transaction.FundingMemo.ValueDateFun;
                        funding.CreditingOperative = data.Transaction.FundingMemo.CreditingOperative;
                        funding.DebitingOperative = data.Transaction.FundingMemo.DebitingOperative;
                        funding.MaturityDate = data.Transaction.FundingMemo.MaturityDate;//data.Transaction.FundingMemo.MaturityDate,
                        //EndDua
                        //Tiga
                        funding.InterestCodeID = data.Transaction.FundingMemo.InterestCodeID;
                        funding.BaseRate = data.Transaction.FundingMemo.BaseRate;
                        funding.AccountPreferencial = data.Transaction.FundingMemo.AccountPreferencial;
                        funding.AllInRate = data.Transaction.FundingMemo.AllInRate;
                        funding.RepricingPlanID = data.Transaction.FundingMemo.RepricingPlanID;
                        funding.ApprovedMarginAsPerCM = data.Transaction.FundingMemo.ApprovedMarginAsPerCM;
                        funding.SpecialFTP = data.Transaction.FundingMemo.SpecialFTP;
                        funding.Margin = data.Transaction.FundingMemo.Margin;
                        funding.AllInSpecialRate = data.Transaction.FundingMemo.AllInSpecialRate;
                        funding.PeggingDate = data.Transaction.FundingMemo.PeggingDate;//data.Transaction.FundingMemo.PeggingDate,
                        funding.PeggingFrequencyID = data.Transaction.FundingMemo.PeggingFrequencyID;
                        //EndTiga

                        //Empat
                        funding.PrincipalStartDate = data.Transaction.FundingMemo.PrincipalStartDate;
                        funding.InterestStartDate = data.Transaction.FundingMemo.InterestStartDate;
                        funding.NoOfInstallment = data.Transaction.FundingMemo.NoOfInstallment;
                        funding.PrincipalFrequencyID = data.Transaction.FundingMemo.PrincipalFrequencyID;
                        funding.InterestFrequencyID = data.Transaction.FundingMemo.InterestFrequencyID;
                        //EndEmpat

                        //Lima
                        funding.LimitIDinFin10 = data.Transaction.FundingMemo.LimitIDinFin10;
                        funding.LimitExpiryDate = data.Transaction.FundingMemo.LimitExpiryDate;
                        funding.SanctionLimitCurrID = data.Transaction.FundingMemo.SanctionLimitCurrID;
                        funding.SanctionLimit = data.Transaction.FundingMemo.sactionlimit;
                        funding.UtilizationCurrID = data.Transaction.FundingMemo.UtilizationCurrID;
                        funding.utilization = data.Transaction.FundingMemo.utilazition;
                        funding.Outstanding = data.Transaction.FundingMemo.outstanding;
                        funding.OutstandingCurrID = data.Transaction.FundingMemo.OutstandingCurrID;
                        //add refere currency
                        //End


                        funding.DoAName = data.Transaction.FundingMemo.DoaName;

                        funding.CSOName = data.Transaction.FundingMemo.CSOName;


                        funding.UpdateDate = DateTime.UtcNow;
                        funding.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        context.SaveChanges();
                        Entity.Transaction trans = context.Transactions.Where(a => a.TransactionID == getFundingID.TransactionID).FirstOrDefault();
                        trans.ValueDate = data.Transaction.ValueDate;
                        context.SaveChanges();



                        if (data.Transaction.Documents != null)
                        {
                            var existingDocuments = (from x in context.TransactionDocuments
                                                     where x.TransactionID == data.Transaction.ID && x.IsDeleted == false
                                                     select x);
                            if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                            {
                                foreach (var item in existingDocuments.ToList()) // remove All transaction document
                                {
                                    //if (item.PurposeID != 2)
                                    {
                                        context.TransactionDocuments.Remove(item);
                                    }
                                }
                                context.SaveChanges();

                                foreach (var item in data.Transaction.Documents) //Add All Transaction document
                                {
                                    //if (item.Purpose.ID != 2)
                                    {
                                        Entity.TransactionDocument document = new Entity.TransactionDocument()
                                        {
                                            TransactionID = data.Transaction.ID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            IsDeleted = false,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().DisplayName
                                        };

                                        context.TransactionDocuments.Add(document);
                                    }
                                    context.SaveChanges();

                                }
                            }
                            else
                            {
                                foreach (var item in data.Transaction.Documents) //Add All Transaction document
                                {
                                    //if (item.Purpose.ID != 2)
                                    {
                                        Entity.TransactionDocument document = new Entity.TransactionDocument()
                                        {
                                            TransactionID = data.Transaction.ID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            IsDeleted = false,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().DisplayName
                                        };

                                        context.TransactionDocuments.Add(document);
                                    }
                                    context.SaveChanges();

                                }
                            }

                        }
                    }
                    ts.Complete();

                    IsSuccess = true;
                }

                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }

            }
            return IsSuccess;

        }

        #endregion

        #region PPULoanMaker
        public bool GetTransactionLoanPPUMaker(Guid workflowInstanceID, long approverID, ref TransactionLoanModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                TransactionLoanModel datatransaction = (from a in context.Transactions
                                                        where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                                                        select new TransactionLoanModel
                                                        {
                                                            ID = a.TransactionID,
                                                            WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                                            ApplicationID = a.ApplicationID,
                                                            ApproverID = a.ApproverID != null ? a.ApproverID : 0,

                                                            Product = new ProductModel()
                                                            {
                                                                ID = a.Product.ProductID,
                                                                Code = a.Product.ProductCode,
                                                                Name = a.Product.ProductName,
                                                                WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                                                Workflow = a.Product.ProductWorkflow.WorkflowName,
                                                                LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                                                LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                                            },
                                                            Currency = new CurrencyModel()
                                                            {
                                                                ID = a.Currency.CurrencyID,
                                                                Code = a.Currency.CurrencyCode,
                                                                Description = a.Currency.CurrencyDescription,
                                                                LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                                LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                            },
                                                            Amount = a.Amount,
                                                            AmountUSD = a.AmountUSD,
                                                            Rate = a.Rate,
                                                            Channel = new ChannelModel()
                                                            {
                                                                ID = a.Channel.ChannelID,
                                                                Name = a.Channel.ChannelName,
                                                                LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                                                LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                                            },
                                                            SourceID = a.SourceID.HasValue? a.SourceID.Value: 0,
                                                            BizSegment = new BizSegmentModel()
                                                            {
                                                                ID = a.BizSegment.BizSegmentID,
                                                                Name = a.BizSegment.BizSegmentName,
                                                                Description = a.BizSegment.BizSegmentDescription,
                                                                LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                                LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                            },
                                                            IsTopUrgent = a.IsTopUrgent,
                                                            Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                                                            {
                                                                ID = x.TransactionDocumentID,
                                                                Type = new DocumentTypeModel()
                                                                {
                                                                    ID = x.DocumentType.DocTypeID,
                                                                    Name = x.DocumentType.DocTypeName,
                                                                    Description = x.DocumentType.DocTypeDescription,
                                                                    LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                    LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                },
                                                                Purpose = new DocumentPurposeModel()
                                                                {
                                                                    ID = x.DocumentPurpose.PurposeID,
                                                                    Name = x.DocumentPurpose.PurposeName,
                                                                    Description = x.DocumentPurpose.PurposeDescription,
                                                                    LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                    LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value

                                                                },
                                                                FileName = x.Filename,
                                                                DocumentPath = x.DocumentPath,
                                                                LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value,
                                                                IsDormant = x.IsDormant == null ? false : x.IsDormant

                                                            }).ToList(),
                                                            CreateDate = a.CreateDate,
                                                            LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                            LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                        }).SingleOrDefault();



                if (datatransaction != null)
                {
                    //datatransaction.IsOtherBeneBank = datatransaction.Bank.Code != "999" ? false : true;
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    // changed, chandra : 2015.03.23
                    if (customerRepo.GetCustomerByTransaction(datatransaction.ID, ref customer, ref message))
                    {
                        datatransaction.Customer = customer;
                    }

                    var data = context.Transactions.Where(x => x.TransactionID.Equals(datatransaction.ID)).Select(x => x).FirstOrDefault();

                    customerRepo.Dispose();
                    customer = null;
                    long IDTrns = datatransaction.ID;
                    var checker = context.TransactionCheckerDatas.Where(x => x.TransactionID.Equals(IDTrns)).Select(x => x).OrderByDescending(b => b.ApproverID).FirstOrDefault();
                    if (checker != null)
                    {
                        datatransaction.IsSignatureVerified = checker.IsSignatureVerified;
                        datatransaction.IsDocumentComplete = data.IsDocumentComplete == null ? data.IsDocumentComplete : false;
                        datatransaction.IsCallbackRequired = false;
                        datatransaction.IsLOIAvailable = false;
                        datatransaction.IsDormantAccount = false;
                        datatransaction.IsFreezeAccount = false;
                        datatransaction.IsSyndication = false;
                    }

                }
                {
                    transaction = datatransaction;
                }
                //end fundingmemo
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        public bool GetCheckerPPULoanMaker(Guid workflowInstanceID, long approverID, ref LoanPPUCheckerModel output, ref string message)
        {
            bool IsSuccess = false;

            var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

            try
            {
                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new LoanPPUCheckerModel
                          {
                              Verify = a.TransactionCheckers
                                .Where(x => x.TransactionID.Equals(transactionID) && x.IsDeleted == false)
                                .Select(x => new VerifyLoanModel()
                                {
                                    ID = x.TransactionColumnID,
                                    Name = x.TransactionColumn.ColumnName,
                                    IsVerified = x.IsVerified
                                }).ToList()
                          }).SingleOrDefault();
                if (!output.Verify.Any())
                {
                    output.Verify = context.TransactionColumns
                        .Where(x => x.IsLoanCheckerDisbursment.Value.Equals(true) && x.IsDeleted.Equals(false))
                        //.Where(x => x.IsDeleted.Equals(false))
                        .Select(x => new VerifyLoanModel()
                        {
                            ID = x.TransactionColumnID,
                            Name = x.ColumnName,
                            IsVerified = false
                        })
                        .ToList();
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool AddTransactionLoanMaker(Guid workflowInstanceID, long approverID, TransactionLoanModel transaction, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var data = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).SingleOrDefault();
                    if (data != null)
                    {
                        data.Customer.CustomerName = transaction.Customer.Name;
                        data.CIF = transaction.Customer.CIF;
                        data.Product.ProductID = transaction.Product.ID;
                        data.CurrencyID = transaction.Currency.ID;
                        data.ApproverID = approverID;
                        data.Amount = transaction.Amount;
                        data.Rate = transaction.Rate;
                        data.AmountUSD = transaction.AmountUSD;
                        data.ChannelID = transaction.Channel.ID;
                        data.SourceID = transaction.SourceID;
                        data.BizSegmentID = transaction.BizSegment.ID;
                        //data.LoanContractNo = transaction.LoanContractNo;
                        //data.ValueDate = transaction.ValueDate;

                        context.SaveChanges();

                        long TransID = transaction.ID;
                        #region Save Document
                        var existingDocs = context.TransactionDocuments.Where(a => a.TransactionID.Equals(TransID) && a.IsDeleted == false).ToList();
                        if (existingDocs != null)
                        {
                            if (existingDocs.Count() > 0)
                            {
                                foreach (var item2 in existingDocs)
                                {
                                    var deleteDoc = context.TransactionDocuments.Where(a => a.TransactionDocumentID.Equals(item2.TransactionDocumentID)).SingleOrDefault();
                                    deleteDoc.IsDeleted = true;
                                    context.SaveChanges();
                                }
                            }
                        }
                        if (transaction.Documents.Count > 0)
                        {
                            foreach (var item3 in transaction.Documents)
                            {
                                Entity.TransactionDocument document = new Entity.TransactionDocument()
                                {
                                    TransactionID = TransID,
                                    DocTypeID = item3.Type.ID,
                                    PurposeID = item3.Purpose.ID,
                                    Filename = item3.FileName,
                                    DocumentPath = item3.DocumentPath,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                };
                                context.TransactionDocuments.Add(document);
                            }
                            context.SaveChanges();
                        }
                        #endregion

                    }
                    ts.Complete();
                    isSuccess = true;
                }

                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }

            return isSuccess;
        }




        #endregion

        #region Loancaller
        public bool GetTransactionLoanCaller(Guid workflowInstanceID, long approverID, ref TransactionLoanModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                TransactionLoanModel datatransaction = (from a in context.Transactions
                                                        where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                                                        select new TransactionLoanModel
                                                        {
                                                            ID = a.TransactionID,
                                                            WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                                            ApplicationID = a.ApplicationID,
                                                            ApproverID = a.ApproverID != null ? a.ApproverID : 0,

                                                            Product = new ProductModel()
                                                            {
                                                                ID = a.Product.ProductID,
                                                                Code = a.Product.ProductCode,
                                                                Name = a.Product.ProductName,
                                                                WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                                                Workflow = a.Product.ProductWorkflow.WorkflowName,
                                                                LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                                                LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                                            },
                                                            Currency = new CurrencyModel()
                                                            {
                                                                ID = a.Currency.CurrencyID,
                                                                Code = a.Currency.CurrencyCode,
                                                                Description = a.Currency.CurrencyDescription,
                                                                LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                                LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                            },
                                                            Amount = a.Amount,
                                                            AmountUSD = a.AmountUSD,
                                                            Rate = a.Rate,
                                                            Channel = new ChannelModel()
                                                            {
                                                                ID = a.Channel.ChannelID,
                                                                Name = a.Channel.ChannelName,
                                                                LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                                                LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                                            },
                                                            SourceID = a.SourceID.HasValue? a.SourceID.Value : 0,
                                                            BizSegment = new BizSegmentModel()
                                                            {
                                                                ID = a.BizSegment.BizSegmentID,
                                                                Name = a.BizSegment.BizSegmentName,
                                                                Description = a.BizSegment.BizSegmentDescription,
                                                                LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                                LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                            },

                                                            LoanContractNo = a.LoanContractNo,

                                                            IsTopUrgent = a.IsTopUrgent,

                                                            Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                                                            {
                                                                ID = x.TransactionDocumentID,
                                                                Type = new DocumentTypeModel()
                                                                {
                                                                    ID = x.DocumentType.DocTypeID,
                                                                    Name = x.DocumentType.DocTypeName,
                                                                    Description = x.DocumentType.DocTypeDescription,
                                                                    LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                    LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                },
                                                                Purpose = new DocumentPurposeModel()
                                                                {
                                                                    ID = x.DocumentPurpose.PurposeID,
                                                                    Name = x.DocumentPurpose.PurposeName,
                                                                    Description = x.DocumentPurpose.PurposeDescription,
                                                                    LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                    LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value

                                                                },
                                                                FileName = x.Filename,
                                                                DocumentPath = x.DocumentPath,
                                                                LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value,
                                                                IsDormant = x.IsDormant == null ? false : x.IsDormant

                                                            }).ToList(),
                                                            CreateDate = a.CreateDate,
                                                            LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                            LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                        }).SingleOrDefault();



                if (datatransaction != null)
                {
                    //datatransaction.IsOtherBeneBank = datatransaction.Bank.Code != "999" ? false : true;
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    // changed, chandra : 2015.03.23
                    if (customerRepo.GetCustomerByTransaction(datatransaction.ID, ref customer, ref message))
                    {
                        datatransaction.Customer = customer;
                    }

                    var data = context.Transactions.Where(x => x.TransactionID.Equals(datatransaction.ID)).Select(x => x).FirstOrDefault();




                    customerRepo.Dispose();
                    customer = null;
                    long IDTrns = datatransaction.ID;
                    var checker = context.TransactionCheckerDatas.Where(x => x.TransactionID.Equals(IDTrns)).Select(x => x).OrderByDescending(b => b.ApproverID).FirstOrDefault();
                    if (checker != null)
                    {
                        datatransaction.IsSignatureVerified = checker.IsSignatureVerified;
                        datatransaction.IsDocumentComplete = data.IsDocumentComplete;
                        datatransaction.IsCallbackRequired = checker.IsCallbackRequired;
                        datatransaction.IsLOIAvailable = checker.IsLOIAvailable;
                        //end basri
                        datatransaction.IsDormantAccount = checker.IsDormantAccount;
                        datatransaction.IsFreezeAccount = checker.IsFreezeAccount;
                        datatransaction.IsSyndication = checker.IsSyndication;
                    }

                }
                {
                    transaction = datatransaction;
                }
                //end fundingmemo
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }

        public bool GetTransactionCallbackDetails(Guid workflowInstanceID, long approverID, ref TransactionCallbackDetailLoanModel output, ref string message)
        {
            bool IsSuccess = false; //is

            try
            {
                var transactionID = context.Transactions
                       .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                       .Select(a => a.TransactionID)
                       .SingleOrDefault();

                var callbackID = context.TransactionCallbacks
                    .Where(a => a.IsUTC == false && a.TransactionID.Equals(transactionID))
                    .OrderByDescending(a => a.TransactionCallbackID)
                    .Select(a => a.TransactionCallbackID)
                    .FirstOrDefault();

                //where a.IsUTC.Equals(false) && a.TransactionID.Equals(transactionID)
                //select a.TransactionCallbackID).so;

                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new TransactionCallbackDetailLoanModel
                          {


                              Callbacks = (from b in context.TransactionCallbacks
                                           where b.TransactionID.Equals(transactionID)
                                           select new TransactionCallbackTimeLoanModel
                                           {
                                               ID = b.TransactionCallbackID,
                                               ApproverID = b.ApproverID,

                                               Contact = b.TransactionCallbackContacts.Select(y => new CustomerContactModel()
                                               {
                                                   ID = y.CustomerContact.ContactID,
                                                   Name = y.CustomerContact.ContactName,
                                                   PhoneNumber = y.CustomerContact.PhoneNumber,
                                                   DateOfBirth = y.CustomerContact.DateOfBirth,
                                                   //Address = y.CustomerContact.Address,
                                                   IDNumber = y.CustomerContact.IdentificationNumber,
                                               }).FirstOrDefault(),
                                               Time = b.Time,
                                               IsUTC = b.IsUTC,
                                               Remark = b.Remark
                                           }).ToList(),
                              Verify = a.TransactionCallbackCheckers
                                  //.Where(z => z.ApproverID.Equals(approverID))
                                .Select(z => new VerifyLoanModel()
                                {
                                    ID = z.TransactionColumnID,
                                    Name = z.TransactionColumn.ColumnName,
                                    IsVerified = z.IsVerified
                                }).ToList()
                          }).SingleOrDefault();

                // replace verify list while empty
                if (output != null)
                {
                    output.Verify = (from a in context.TransactionColumns
                                     where a.IsDeleted.Equals(false) && a.IsCallBack.Equals(true)
                                     select new VerifyLoanModel()
                                     {
                                         ID = a.TransactionColumnID,
                                         Name = a.ColumnName,
                                         IsVerified = false
                                     }).ToList();


                    var documentCompleteness = (from a in context.TransactionCheckers
                                                join b in context.TransactionColumns
                                                on a.TransactionColumnID equals b.TransactionColumnID
                                                where a.TransactionID.Equals(transactionID) && b.ColumnName.Equals("Document Completeness") && a.IsDeleted == false
                                                select new VerifyLoanModel()
                                                {
                                                    ID = a.TransactionColumnID,
                                                    Name = b.ColumnName,
                                                    IsVerified = a.IsVerified
                                                }).SingleOrDefault();

                    if (documentCompleteness != null)
                    {
                        output.Verify.Add(documentCompleteness);
                    }
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool AddTransactionCallback(Guid workflowInstanceID, long approverID, LoanPPUCheckerModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {

                    // insert trasaction callback data
                    DBS.Entity.TransactionCallbackData dataCallback = new DBS.Entity.TransactionCallbackData()
                    {
                        TransactionID = data.Transaction.ID,
                        ApproverID = approverID,
                        CurrencyID = data.Transaction.Currency.ID,
                        Amount = data.Transaction.Amount,
                        AmountUSD = data.Transaction.AmountUSD,
                        Rate = 0,
                        ChannelID = data.Transaction.Channel.ID,
                        SourceID = data.Transaction.SourceID,
                        AccountNumber = null,
                        OtherAccountNumber = null,
                        DebitCurrencyID = data.Transaction.Currency.ID,
                        ApplicationDate = DateTime.UtcNow,
                        LLDID = null,
                        BankID = null,
                        OtherBeneBankName = null,
                        OtherBeneBankSwift = null,
                        BeneACCNumber = null,
                        IsSignatureVerified = data.Transaction.IsSignatureVerified,
                        IsDormantAccount = data.Transaction.IsDormantAccount,
                        IsFreezeAccount = data.Transaction.IsFreezeAccount,
                        IsOtherAccountNumber = false,
                        DetailCompliance = null,
                        Other = null,
                        IsSyndication = data.Transaction.IsSyndication,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName,
                        UpdateBy = null,
                        UpdateDate = null,
                        PaymentDetails = null
                    };
                    context.TransactionCallbackDatas.Add(dataCallback);

                    context.SaveChanges();

                    var checker = context.TransactionCheckers
                         .Where(a => a.TransactionID.Equals(data.Transaction.ID) && a.IsDeleted == false)
                         .ToList();

                    var checkerData = context.TransactionCheckerDatas
                        .Where(a => a.TransactionID.Equals(data.Transaction.ID))
                        .SingleOrDefault();

                    #region Transaction Checker Data
                    if (checkerData != null)
                    {
                        checkerData.IsCallbackRequired = data.Transaction.IsCallbackRequired;
                        checkerData.IsSyndication = data.Transaction.IsSyndication;
                        checkerData.IsSignatureVerified = data.Transaction.IsSignatureVerified;
                        checkerData.IsDormantAccount = data.Transaction.IsDormantAccount;
                        checkerData.IsFreezeAccount = data.Transaction.IsFreezeAccount;
                        checkerData.IsLOIAvailable = data.Transaction.IsLOIAvailable;
                        checkerData.UpdateDate = DateTime.UtcNow;
                        checkerData.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();
                    }
                    else
                    {
                        // Insert
                        context.TransactionCheckerDatas.Add(new TransactionCheckerData()
                        {
                            ApproverID = approverID,
                            TransactionID = data.Transaction.ID,
                            IsSignatureVerified = data.Transaction.IsSignatureVerified,
                            IsSyndication = data.Transaction.IsSyndication,
                            IsDormantAccount = data.Transaction.IsDormantAccount,
                            IsFreezeAccount = data.Transaction.IsFreezeAccount,
                            IsCallbackRequired = data.Transaction.IsCallbackRequired,
                            IsLOIAvailable = data.Transaction.IsLOIAvailable,
                            IsStatementLetterCopy = false,
                            CreatedDate = DateTime.UtcNow,
                            CreatedBy = currentUser.GetCurrentUser().DisplayName
                        });

                        context.SaveChanges();
                    }
                    #endregion
                    // insert callback
                    foreach (var item in data.Callbacks)
                    {
                        if (item.ID == 0 && item.Time.Hour > 0)
                        {
                            // Insert transaction callback
                            TransactionCallback callback = new TransactionCallback()
                            {
                                ApproverID = approverID,
                                TransactionID = data.Transaction.ID,
                                Time = item.Time.ToUniversalTime(),
                                IsUTC = item.IsUTC,
                                Remark = item.Remark,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName
                            };

                            context.TransactionCallbacks.Add(callback);
                            context.SaveChanges();

                            // Insert transaction callback contact
                            if (item.Contact != null)
                            {
                                context.TransactionCallbackContacts.Add(new TransactionCallbackContact()
                                {
                                    TransactionCallbackID = callback.TransactionCallbackID,
                                    ContactID = item.Contact.ID,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });
                            }
                            context.SaveChanges();

                            callback = null;
                        }
                    }

                    //flag callback draft as deleted
                    List<Entity.TransactionCallbackDraft> callbackDraft = context.TransactionCallbackDrafts.Where(a => a.TransactionID == data.Transaction.ID && a.IsDeleted != true).ToList();
                    if (callbackDraft.Count > 0)
                    {
                        foreach (var item in callbackDraft)
                        {
                            item.IsDeleted = true;
                            item.UpdateDate = DateTime.UtcNow;
                            item.UpdateBy = currentUser.GetCurrentUser().LoginName;
                            context.SaveChanges();
                        }
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }
        #endregion

        #region LoanChecker
        public bool GetTransactionLoanDetails(Guid instanceID, ref TransactionLoanModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                TransactionLoanModel datatransaction = (from a in context.Transactions
                                                        where a.WorkflowInstanceID.Value.Equals(instanceID)
                                                        select new TransactionLoanModel
                                                        {
                                                            ID = a.TransactionID,
                                                            WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                                            ApplicationID = a.ApplicationID,
                                                            LoanTransID = a.LoanTransID,
                                                            ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                                                            ValueDate = a.ValueDate,

                                                            Product = new ProductModel()
                                                            {
                                                                ID = a.Product.ProductID,
                                                                Code = a.Product.ProductCode,
                                                                Name = a.Product.ProductName,
                                                                WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                                                Workflow = a.Product.ProductWorkflow.WorkflowName,
                                                                LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                                                LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                                            },
                                                            Currency = new CurrencyModel()
                                                            {
                                                                ID = a.Currency.CurrencyID,
                                                                Code = a.Currency.CurrencyCode,
                                                                Description = a.Currency.CurrencyDescription,
                                                                LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                                LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                            },
                                                            Amount = a.Amount,
                                                            AmountUSD = a.AmountUSD,
                                                            Rate = a.Rate,
                                                            Channel = new ChannelModel()
                                                            {
                                                                ID = a.Channel.ChannelID,
                                                                Name = a.Channel.ChannelName,
                                                                LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                                                LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                                            },
                                                            SourceID = a.SourceID.HasValue? a.SourceID.Value :0,
                                                            BizSegment = new BizSegmentModel()
                                                            {
                                                                ID = a.BizSegment.BizSegmentID,
                                                                Name = a.BizSegment.BizSegmentName,
                                                                Description = a.BizSegment.BizSegmentDescription,
                                                                LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                                LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                            },

                                                            LoanContractNo = a.LoanContractNo,

                                                            IsTopUrgent = a.IsTopUrgent,

                                                            Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                                                            {
                                                                ID = x.TransactionDocumentID,
                                                                Type = new DocumentTypeModel()
                                                                {
                                                                    ID = x.DocumentType.DocTypeID,
                                                                    Name = x.DocumentType.DocTypeName,
                                                                    Description = x.DocumentType.DocTypeDescription,
                                                                    LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                    LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                },
                                                                Purpose = new DocumentPurposeModel()
                                                                {
                                                                    ID = x.DocumentPurpose.PurposeID,
                                                                    Name = x.DocumentPurpose.PurposeName,
                                                                    Description = x.DocumentPurpose.PurposeDescription,
                                                                    LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                    LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                                },
                                                                FileName = x.Filename,

                                                                DocumentPath = x.DocumentPath,
                                                                LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value,
                                                                IsDormant = x.IsDormant == null ? false : x.IsDormant
                                                            }).ToList(),
                                                            CreateDate = a.CreateDate,
                                                            LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                            LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                        }).SingleOrDefault();


                if (datatransaction != null)
                {
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(instanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    if (customerRepo.GetCustomerByTransaction(datatransaction.ID, ref customer, ref message))
                    {
                        datatransaction.Customer = customer;
                    }

                    var data = context.Transactions.Where(x => x.TransactionID.Equals(datatransaction.ID)).Select(x => x).FirstOrDefault();

                    customerRepo.Dispose();
                    customer = null;
                    long IDTrns = datatransaction.ID;
                    var checker = context.TransactionCheckerDatas.Where(x => x.TransactionID.Equals(IDTrns)).Select(x => x).OrderByDescending(b => b.ApproverID).FirstOrDefault();
                    if (checker != null)
                    {
                        datatransaction.Others = checker.Others;

                        if (checker.LLD != null)
                        {
                            LLDModel lm = new LLDModel();
                            lm.ID = checker.LLD.LLDID;
                            lm.Code = checker.LLD.LLDCode;
                            lm.Description = checker.LLD.LLDDescription;
                        }

                        datatransaction.IsSignatureVerified = checker.IsSignatureVerified;
                        datatransaction.IsDocumentComplete = datatransaction.IsDocumentComplete;
                        datatransaction.IsCallbackRequired = checker.IsCallbackRequired;
                        datatransaction.IsLOIAvailable = checker.IsLOIAvailable;
                        datatransaction.IsDormantAccount = checker.IsDormantAccount;
                        datatransaction.IsFreezeAccount = checker.IsFreezeAccount;
                    }
                    else
                    {
                        datatransaction.IsSignatureVerified = false;
                        datatransaction.IsDocumentComplete = false;
                        datatransaction.IsCallbackRequired = false;
                        datatransaction.IsLOIAvailable = false;
                        datatransaction.IsDormantAccount = false;
                        datatransaction.IsFreezeAccount = false;
                    }

                }

                FundingMemoTranModel FundingMemo = (from funding in context.SP_GetFundingMemoTransaction(datatransaction.ID)
                                                    select new FundingMemoTranModel
                                                    {
                                                        CSOFundingMemoID = funding.CSOFundingMemoID,
                                                        CodeAmount = funding.amountCode,
                                                        DoaName = funding.DoAName,
                                                        FundingMemoDocumentID = funding.CSOFundingMemoID,
                                                        CIF = funding.CIF,
                                                        utilazition = funding.utilization,
                                                        ValueDateFun = funding.ValueDate,
                                                        sactionlimit = funding.SanctionLimit,
                                                        outstanding = funding.Outstanding,
                                                        CustomerName = funding.CustomerName,
                                                        BizSegmentID = funding.BizSegmentID,
                                                        BizSegmentName = funding.BizName,
                                                        LocationID = funding.LocationID,
                                                        SolID = funding.solidfun,
                                                        CustomerCategoryID = funding.CustomerCategoryID,
                                                        CustomerCategoryName = funding.CustomerCategoryName,
                                                        LoanTypeID = funding.LoanTypeID,
                                                        LoanTypeName = funding.LoanTypeName,
                                                        ProgramTypeID = funding.ProgramTypeID,
                                                        ProgramTypeName = funding.ProgramTypeName,
                                                        FinacleSchemeCodeID = funding.FinacleSchemeCodeID,
                                                        FinacleSchemeCodeName = funding.FinacleSchemeCodeName,
                                                        IsAdhoc = funding.IsAdhoc,
                                                        AmountDisburseID = funding.AmountDisburseID,
                                                        AmountDisburse = funding.AmountDisburse,
                                                        CreditingOperativeID = funding.CreditingOperativeID,
                                                        CreditingOperative = funding.CreditingOperative,
                                                        DebitingOperativeID = funding.DebitingOperativeID,
                                                        DebitingOperative = funding.DebitingOperative,
                                                        MaturityDate = funding.MaturityDate,
                                                        InterestCodeID = funding.InterestCodeID,
                                                        BaseRate = funding.BaseRate,
                                                        AccountPreferencial = funding.AccountPreferencial,
                                                        AllInRate = funding.AllInRate,
                                                        RepricingPlanID = funding.RepricingPlanID,
                                                        PeggingDate = funding.PeggingDate,
                                                        ApprovedMarginAsPerCM = funding.ApprovedMarginAsPerCM,
                                                        SpecialFTP = funding.SpecialFTP,
                                                        Margin = funding.Margin,
                                                        AllInSpecialRate = funding.AllInSpecialRate,
                                                        PeggingFrequencyID = funding.PeggingFrequencyID,
                                                        PrincipalStartDate = funding.PrincipalStartDate,
                                                        PrincipalFrequencyID = funding.PrincipalFrequencyID,
                                                        NoOfInstallment = funding.NoOfInstallment,
                                                        InterestStartDate = funding.InterestStartDate,
                                                        InterestFrequencyID = funding.InterestFrequencyID,
                                                        LimitIDinFin10 = funding.LimitIDinFin10,
                                                        LimitExpiryDate = funding.LimitExpiryDate,
                                                        SanctionLimitCurrID = funding.SanctionLimitCurrID,
                                                        SactionLimitCurrCode = funding.SactionLimitCurrCode,
                                                        UtilizationCurrID = funding.UtilizationCurrID,
                                                        UtilizationCurrCode = funding.UtilizationCurrCode,
                                                        OutstandingCurrID = funding.OutstandingCurrID,
                                                        OutstandingCurrCode = funding.OutstandingCurrCode,
                                                        CSOName = funding.CSOName


                                                    }).SingleOrDefault();

                if (FundingMemo != null)
                {
                    datatransaction.fundingmemodocument = (from d in context.FundingMemoDocuments
                                                           where d.FundingMemoID == FundingMemo.FundingMemoDocumentID
                                                           select new FundingMemoDocumentModel()
                                                           {
                                                               ID = d.FundingMemoDocumentID,
                                                               Type = new DocumentTypeModel()
                                                               {
                                                                   ID = d.DocumentType.DocTypeID,
                                                                   Name = d.DocumentType.DocTypeName,
                                                                   Description = d.DocumentType.DocTypeDescription,
                                                                   LastModifiedBy = d.DocumentType.UpdateDate == null ? d.DocumentType.CreateBy : d.DocumentType.UpdateBy,
                                                                   LastModifiedDate = d.DocumentType.UpdateDate == null ? d.DocumentType.CreateDate : d.DocumentType.UpdateDate.Value
                                                               },
                                                               Purpose = new DocumentPurposeModel()
                                                               {
                                                                   ID = d.DocumentPurpose.PurposeID,
                                                                   Name = d.DocumentPurpose.PurposeName,
                                                                   Description = d.DocumentPurpose.PurposeDescription,
                                                                   LastModifiedBy = d.DocumentPurpose.UpdateDate == null ? d.DocumentPurpose.CreateBy : d.DocumentPurpose.UpdateBy,
                                                                   LastModifiedDate = d.DocumentPurpose.UpdateDate == null ? d.DocumentPurpose.CreateDate : d.DocumentPurpose.UpdateDate.Value
                                                               },
                                                               FileName = d.Filename,

                                                               DocumentPath = d.DocumentPath,
                                                               LastModifiedBy = d.UpdateDate == null ? d.CreateBy : d.UpdateBy,
                                                               LastModifiedDate = d.UpdateDate == null ? d.CreateDate : d.UpdateDate.Value,


                                                           }).ToList();

                    datatransaction.FundingMemo = FundingMemo;
                    transaction = datatransaction;
                }
                else
                {
                    transaction = datatransaction;
                }
                //end fundingmemo
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }


        #endregion
        #endregion

        #region Agung Excel
        #region General
        public bool GetDataSettlementScheduled(Guid workflowInstanceID, long approverID, ref LoanCheckerRollover output, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                var TransactionID = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a.TransactionID).FirstOrDefault();
                var InstructionID = context.TransactionRollovers.Where(a => a.TransactionID.Equals(TransactionID)).Select(a => a.Instruction.Value).ToList();

                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new LoanCheckerRollover
                          {

                              LoanCheckerMakerRollover = (from b in context.CSORollovers
                                                          join c in context.TransactionRollovers
                                                          on b.TransactionRolloverID equals c.TransactionRolloverID
                                                          where c.TransactionID.Equals(a.TransactionID)
                                                          select new LoanCheckerMakerModel
                                                          {
                                                              IsChecked = c.IsSelected.HasValue ? (c.IsSelected.Value == true ? true : false) : false,
                                                              TransactionRolloverID = b.TransactionRolloverID,
                                                              CSORollOverID = b.CSORolloverID,
                                                              SOLID = b.SOLID,
                                                              SchemeCode = b.SchemeCode,
                                                              CIF = b.CIF,
                                                              CustomerName = context.Customers.Where(d => d.CIF.Equals(b.CIF)).Select(d => d.CustomerName).FirstOrDefault(),
                                                              LoanContractNo = b.LoanContractNo,
                                                              DueDate = b.DueDate.Value,
                                                              Currency = new CurrencyModel()
                                                              {
                                                                  ID = a.Currency.CurrencyID,
                                                                  Code = a.Currency.CurrencyCode,
                                                                  Description = a.Currency.CurrencyDescription,
                                                                  LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                                  LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                              },
                                                              AmountDue = b.AmountDue.Value,
                                                              LimitID = b.LimitID,
                                                              BizSegment = new BizSegmentModel()
                                                              {
                                                                  ID = a.BizSegment.BizSegmentID,
                                                                  Name = a.BizSegment.BizSegmentName,
                                                                  Description = a.BizSegment.BizSegmentDescription,
                                                                  LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                                  LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                              },
                                                              RM = new EmployeeModel()
                                                              {
                                                                  EmployeeID = b.EmployeeID == null ? 1 : b.EmployeeID.Value,
                                                                  EmployeeName = b.EmployeeID == null ? "" : context.Employees.Where(x => x.EmployeeID.Equals(b.EmployeeID.Value)).Select(x => x.EmployeeName).FirstOrDefault()
                                                              },
                                                              CSO = b.CSO,
                                                              IsSBLCSecured = b.IsSBLCSecured == true ? "Yes" : "No",
                                                              IsHeavyEquipment = b.IsHeavyEquipment == true ? "Yes" : "No",
                                                              MaintenanceTypeID = b.MaintenanceTypeID.Value,
                                                              AmountRollover = b.AmountRollover.Value,
                                                              NextPrincipalDate = b.NextPrincipalDate.Value,
                                                              NextInterestDate = b.NextInterestDate.Value,
                                                              ApprovedMarginAsPerCM = b.ApprovedMarginAsPerCM.Value,
                                                              InterestRateCodeID = b.InterestRateCodeID,
                                                              BaseRate = b.BaseRate.Value,
                                                              AllInLP = b.AllInLP.Value,
                                                              AllInFTPRate = b.AllInFTPRate.Value,
                                                              AccountPreferencial = b.AccountPreferencial.Value,
                                                              SpecialFTP = b.SpecialFTP.Value,
                                                              SpecialRateMargin = b.SpecialRateMargin.Value,
                                                              AllInrate = b.AllInrate.Value,
                                                              ApprovalDOA = b.ApprovalDOA,
                                                              ApprovedBy = b.ApprovedBy,
                                                              Remarks = b.Remarks,
                                                              IsAdhoc = b.IsAdhoc.Value == true ? "Yes" : "No",
                                                              CSOName = b.CSOName,
                                                              DetailLink = @"<a href='#' class='ShowDetail' onClick='$(this).ShowDetail(this)'>Detail</a>",
                                                              Documents = (from x in context.TransactionDocuments
                                                                           where InstructionID.Contains(x.TransactionID) && x.IsDeleted.Equals(false)
                                                                           select new TransactionDocumentModel()
                                                                           {
                                                                               ID = x.TransactionDocumentID,
                                                                               Type = new DocumentTypeModel()
                                                                               {
                                                                                   ID = x.DocumentType.DocTypeID,
                                                                                   Name = x.DocumentType.DocTypeName,
                                                                                   Description = x.DocumentType.DocTypeDescription,
                                                                                   LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                                   LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                               },
                                                                               Purpose = new DocumentPurposeModel()
                                                                               {
                                                                                   ID = x.DocumentPurpose.PurposeID,
                                                                                   Name = x.DocumentPurpose.PurposeName,
                                                                                   Description = x.DocumentPurpose.PurposeDescription,
                                                                                   LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                                   LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                                               },
                                                                               FileName = x.Filename,
                                                                               DocumentPath = x.DocumentPath,
                                                                               LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                               LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                                                           }).ToList()
                                                          }).ToList()
                          }).SingleOrDefault();



                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }


            return IsSuccess;

        }
        public bool GetDataSettlementScheduledLoanChecker(ref IList<RolloverLoanCheckerModel> output, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    output = (from a in context.LoanRollovers
                              //join b in context.CSOes on a. equal b.
                              join b in context.Employees on a.EmployeeID equals b.EmployeeID
                              select new RolloverLoanCheckerModel
                              {
                                  IsChecked = false,
                                  LoanRolloverID = a.LoanRolloverID,
                                  TransactionRolloverID = a.TransactionRolloverID,
                                  CSOName = a.CSOName,
                                  SOLID = a.SOLID,
                                  SchemeCode = a.SchemeCode,
                                  CIF = a.CIF,
                                  //CustomerName = a.Customer.CustomerName,
                                  LoanContractNo = a.LoanContractNo,
                                  DueDate = a.DueDate,
                                  AmountDue = a.AmountDue,
                                  LimitID = a.LimitID,
                                  Currency = new CurrencyModel()
                                  {
                                      ID = a.Currency.CurrencyID,
                                      Code = a.Currency.CurrencyCode
                                  },
                                  BizSegment = new BizSegmentModel()
                                  {
                                      ID = a.BizSegment.BizSegmentID,
                                      Name = a.BizSegment.BizSegmentName,
                                      Description = a.BizSegment.BizSegmentDescription,
                                      LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                      LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                  },
                                  Employee = new EmployeeModel()
                                  {
                                      EmployeeID = b.EmployeeID,
                                      EmployeeName = b.EmployeeName,
                                      EmployeeEmail = b.EmployeeEmail,
                                      EmployeeUsername = b.EmployeeUsername,
                                      RankID = b.RankID.Value,
                                      SegmentID = b.BizSegmentID.Value
                                  },
                                  CSO = a.CSO,
                                  IsSBLCSecured = a.IsSBLCSecured.Value == true ? "Yes" : "No",
                                  IsHeavyEquipment = a.IsHeavyEquipment.Value == true ? "Yes" : "No",
                                  MaintenanceTypeID = a.MaintenanceTypeID,
                                  AmountRollover = a.AmountRollover,
                                  NextPrincipalDate = a.NextPrincipalDate,
                                  NextInterestDate = a.NextInterestDate,
                                  ApprovedMarginAsPerCM = a.ApprovedMarginAsPerCM,
                                  InterestRateCodeID = a.InterestRateCodeID,
                                  BaseRate = a.BaseRate,
                                  AllInFTPRate = a.AllInFTPRate,
                                  AccountPreferencial = a.AccountPreferencial,
                                  SpecialFTP = a.SpecialFTP,
                                  SpecialRateMargin = a.SpecialRateMargin,
                                  AllInrate = a.AllInrate,
                                  ApprovalDOA = a.ApprovalDOA,
                                  ApprovedBy = a.ApprovedBy,
                                  CSORemarks = a.Remarks,
                                  RemarksLoan = a.LoanRemarks,
                                  IsAdhoc = a.IsAdhoc.Value == true ? "Yes" : "No",
                                  CreateDate = a.CreateDate,
                                  Updatedate = DateTime.UtcNow,
                                  UpdateBy = a.UpdateBy,
                                  CreateBy = a.CreateBy

                              }).ToList();
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return IsSuccess;
        }
        #endregion

        #region Agung Edit

        #region Get And Add Data Interest Maintenance

        #region Loan Maker
        public bool GetDataIM(Guid workflowInstanceID, long approverID, ref LoanCheckerInterestMaintenance output, ref string message)
        {
            bool IsSuccess = false;
            long TransactionID = 0;
            try
            {
                TransactionID = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a.TransactionID).FirstOrDefault();
                var InstructionID = context.TransactionInterestMaintenances.Where(a => a.TransactionID.Equals(TransactionID)).Select(a => a.InterestMaintenanceID).ToList();

                long IMID = context.TransactionInterestMaintenances.Where(a => a.TransactionID.Equals(TransactionID)).Select(a => a.TransactionInterestMaintenanceID).FirstOrDefault();
                var LoanData = context.LoanInterestMaintenances.Where(a => a.TransactionInterestMaintenanceID.Equals(IMID)).FirstOrDefault();

                if (LoanData != null)
                {
                    output = (from a in context.Transactions.AsNoTracking()
                              where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                              select new LoanCheckerInterestMaintenance
                              {
                                  LoanCheckerMakerIM = (from b in context.LoanInterestMaintenances
                                                        join c in context.TransactionInterestMaintenances on b.TransactionInterestMaintenanceID equals c.TransactionInterestMaintenanceID
                                                        //join d in context.ParameterSystems on b.InterestRateCodeID equals d.ParsysID
                                                        where c.TransactionID.Equals(TransactionID)
                                                        select new TransactionInterestMaintenanceLoanModel
                                                        {
                                                            IsChecked = c.IsSelected.HasValue ? (c.IsSelected.Value == true ? true : false) : false,
                                                            TransactionInterestMaintenanceID = b.TransactionInterestMaintenanceID,
                                                            //CSOInterestMaintenanceID = b.CSOInterestMaintenanceID,
                                                            InstructionID = c.InstructionID,
                                                            SolID = b.SolID,
                                                            SchemeCode = b.SchemeCode,
                                                            CIF = b.CIF,
                                                            CustomerName = context.Customers.Where(e => e.CIF.Equals(b.CIF)).Select(e => e.CustomerName).FirstOrDefault(),
                                                            LoanContractNo = b.LoanContractNo,
                                                            DueDate = b.DueDate.Value,
                                                            PreviousInterestRate = b.PreviousInterestRate.Value,
                                                            CurrentInterestRate = b.CurrentInterestRate.Value,
                                                            ValueDate = b.ValueDate,
                                                            Currency = new CurrencyModel()
                                                            {
                                                                ID = a.Currency.CurrencyID,
                                                                Code = a.Currency.CurrencyCode,
                                                                Description = a.Currency.CurrencyDescription,
                                                                LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                                LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                            },
                                                            AmountDue = b.AmountRollover,
                                                            BizSegment = new BizSegmentModel()
                                                            {
                                                                ID = a.BizSegment.BizSegmentID,
                                                                Name = a.BizSegment.BizSegmentName,
                                                                Description = a.BizSegment.BizSegmentDescription,
                                                                LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                                LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                            },
                                                            Employee = new EmployeeModel()
                                                            {
                                                                EmployeeID = b.EmployeeID == null ? 1 : b.EmployeeID.Value,
                                                                EmployeeName = b.EmployeeID == null ? "" : context.Employees.Where(x => x.EmployeeID.Equals(b.EmployeeID.Value)).Select(x => x.EmployeeName).FirstOrDefault()
                                                            },
                                                            CSO = b.CSO,
                                                            IsSBLCSecured = b.IsSBLCSecured.Value == true ? "Yes" : "No",
                                                            IsHeavyEquipment = b.IsHeavyEquipment.Value == true ? "Yes" : "No",
                                                            MaintenanceTypeID = b.MaintenanceTypeID.Value,
                                                            AmountRollover = b.AmountRollover.Value,
                                                            NextPrincipalDate = b.NextPrincipalDate.Value,
                                                            NextInterestDate = b.NextInterestDate.Value,
                                                            ApprovedMarginAsPerCM = b.ApprovedMarginAsPerCM.Value,
                                                            InterestRateCodeID = b.InterestRateCodeID,
                                                            InterestRateCodeName = b.InterestRateCodeID.Value != null ? context.ParameterSystems.Where(d => d.ParsysID.Equals(b.InterestRateCodeID.Value)).Select(d => d.ParameterValue).FirstOrDefault() : "",
                                                            BaseRate = b.BaseRate.Value,
                                                            AllinFTPRate = b.AllinFTPRate.Value,
                                                            AccountPreferencial = b.AccountPreferencial.Value,
                                                            SpecialFTP = b.SpecialFTP.Value,
                                                            SpecialRate = b.SpecialRate.Value,
                                                            AllinRate = b.AllinRate.Value,
                                                            ApprovalDOA = b.ApprovalDOA,
                                                            ApprovedBy = b.ApprovedBy,
                                                            Remarks = b.Remarks,
                                                            IsAdhoc = b.IsAdhoc,
                                                            CSOName = b.CSOName,
                                                            PeggingReviewDate = b.PeggingReviewDate,
                                                            DetailLink = @"<a href='#' class='ShowDetail' onClick='$(this).ShowDetail(this)'>Detail</a>",
                                                            Documents = (from x in context.TransactionDocuments
                                                                         where InstructionID.Contains(x.TransactionID) && x.IsDeleted.Equals(false)
                                                                         select new TransactionDocumentModel()
                                                                         {
                                                                             ID = x.TransactionDocumentID,
                                                                             Type = new DocumentTypeModel()
                                                                             {
                                                                                 ID = x.DocumentType.DocTypeID,
                                                                                 Name = x.DocumentType.DocTypeName,
                                                                                 Description = x.DocumentType.DocTypeDescription,
                                                                                 LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                                 LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                             },
                                                                             Purpose = new DocumentPurposeModel()
                                                                             {
                                                                                 ID = x.DocumentPurpose.PurposeID,
                                                                                 Name = x.DocumentPurpose.PurposeName,
                                                                                 Description = x.DocumentPurpose.PurposeDescription,
                                                                                 LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                                 LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                                             },
                                                                             FileName = x.Filename,
                                                                             DocumentPath = x.DocumentPath,
                                                                             LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                             LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                                                         }).ToList()
                                                        }).ToList()
                              }).SingleOrDefault();
                }
                else
                {
                    output = (from a in context.Transactions.AsNoTracking()
                              where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                              select new LoanCheckerInterestMaintenance
                              {
                                  LoanCheckerMakerIM = (from b in context.CSOInterestMaintenances
                                                        join c in context.TransactionInterestMaintenances on b.TransactionInterestMaintenanceID equals c.TransactionInterestMaintenanceID
                                                        //join d in context.ParameterSystems on b.InterestRateCodeID equals d.ParsysID
                                                        where c.TransactionID.Equals(TransactionID)
                                                        select new TransactionInterestMaintenanceLoanModel
                                                        {
                                                            IsChecked = c.IsSelected.HasValue ? (c.IsSelected.Value == true ? true : false) : false,
                                                            TransactionInterestMaintenanceID = b.TransactionInterestMaintenanceID,
                                                            //CSOInterestMaintenanceID = b.CSOInterestMaintenanceID,
                                                            InstructionID = c.InstructionID,
                                                            SolID = b.SolID,
                                                            SchemeCode = b.SchemeCode,
                                                            CIF = b.CIF,
                                                            CustomerName = context.Customers.Where(e => e.CIF.Equals(b.CIF)).Select(e => e.CustomerName).FirstOrDefault(),
                                                            LoanContractNo = b.LoanContractNo,
                                                            DueDate = b.DueDate.Value,
                                                            PreviousInterestRate = b.PreviousInterestRate.Value,
                                                            CurrentInterestRate = b.CurrentInterestRate.Value,
                                                            ValueDate = b.ValueDate,
                                                            Currency = new CurrencyModel()
                                                            {
                                                                ID = a.Currency.CurrencyID,
                                                                Code = a.Currency.CurrencyCode,
                                                                Description = a.Currency.CurrencyDescription,
                                                                LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                                LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                            },
                                                            AmountDue = b.AmountRollover,
                                                            BizSegment = new BizSegmentModel()
                                                            {
                                                                ID = a.BizSegment.BizSegmentID,
                                                                Name = a.BizSegment.BizSegmentName,
                                                                Description = a.BizSegment.BizSegmentDescription,
                                                                LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                                LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                            },
                                                            Employee = new EmployeeModel()
                                                            {
                                                                EmployeeID = b.EmployeeID == null ? 1 : b.EmployeeID.Value,
                                                                EmployeeName = b.EmployeeID == null ? "" : context.Employees.Where(x => x.EmployeeID.Equals(b.EmployeeID.Value)).Select(x => x.EmployeeName).FirstOrDefault()
                                                            },
                                                            CSO = b.CSO,
                                                            IsSBLCSecured = b.IsSBLCSecured.Value == true ? "Yes" : "No",
                                                            IsHeavyEquipment = b.IsHeavyEquipment.Value == true ? "Yes" : "No",
                                                            MaintenanceTypeID = b.MaintenanceTypeID.Value,
                                                            AmountRollover = b.AmountRollover.Value,
                                                            NextPrincipalDate = b.NextPrincipalDate.Value,
                                                            NextInterestDate = b.NextInterestDate.Value,
                                                            ApprovedMarginAsPerCM = b.ApprovedMarginAsPerCM.Value,
                                                            InterestRateCodeID = b.InterestRateCodeID,
                                                            InterestRateCodeName = b.InterestRateCodeID.Value != null ? context.ParameterSystems.Where(d => d.ParsysID.Equals(b.InterestRateCodeID.Value)).Select(d => d.ParameterValue).FirstOrDefault() : "",
                                                            BaseRate = b.BaseRate.Value,
                                                            AllinFTPRate = b.AllinFTPRate.Value,
                                                            AccountPreferencial = b.AccountPreferencial.Value,
                                                            SpecialFTP = b.SpecialFTP.Value,
                                                            SpecialRate = b.SpecialRate.Value,
                                                            AllinRate = b.AllinRate.Value,
                                                            ApprovalDOA = b.ApprovalDOA,
                                                            ApprovedBy = b.ApprovedBy,
                                                            Remarks = b.Remarks,
                                                            IsAdhoc = b.IsAdhoc,
                                                            CSOName = b.CSOName,
                                                            PeggingReviewDate = b.PeggingReviewDate,
                                                            DetailLink = @"<a href='#' class='ShowDetail' onClick='$(this).ShowDetail(this)'>Detail</a>",
                                                            Documents = (from x in context.TransactionDocuments
                                                                         where InstructionID.Contains(x.TransactionID) && x.IsDeleted.Equals(false)
                                                                         select new TransactionDocumentModel()
                                                                         {
                                                                             ID = x.TransactionDocumentID,
                                                                             Type = new DocumentTypeModel()
                                                                             {
                                                                                 ID = x.DocumentType.DocTypeID,
                                                                                 Name = x.DocumentType.DocTypeName,
                                                                                 Description = x.DocumentType.DocTypeDescription,
                                                                                 LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                                 LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                             },
                                                                             Purpose = new DocumentPurposeModel()
                                                                             {
                                                                                 ID = x.DocumentPurpose.PurposeID,
                                                                                 Name = x.DocumentPurpose.PurposeName,
                                                                                 Description = x.DocumentPurpose.PurposeDescription,
                                                                                 LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                                 LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                                             },
                                                                             FileName = x.Filename,
                                                                             DocumentPath = x.DocumentPath,
                                                                             LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                             LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                                                         }).ToList()
                                                        }).ToList()
                              }).SingleOrDefault();
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return IsSuccess;
        }
        public bool AddDataIMMaker(Guid workflowInstanceID, ref LoanCheckerInterestMaintenance output, long approverID, ref string message)
        {
            bool IsSuccess = false;

            using (DBSEntities context = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        foreach (var item in output.LoanCheckerMakerIM)
                        {
                            //if (item.IsChecked == true)
                            //{
                            var LoanData = context.LoanInterestMaintenances.Where(a => a.TransactionInterestMaintenanceID.Equals(item.TransactionInterestMaintenanceID)).ToList();

                            if (LoanData.Count > 0)
                            {
                                // For Resubmit Loan Maker
                                var updateLoan = context.LoanInterestMaintenances.Where(a => a.TransactionInterestMaintenanceID.Equals(item.TransactionInterestMaintenanceID)).SingleOrDefault();
                                updateLoan.TransactionInterestMaintenanceID = item.TransactionInterestMaintenanceID;
                                updateLoan.CIF = item.CIF;
                                updateLoan.LoanContractNo = item.LoanContractNo;
                                updateLoan.CurrencyID = item.Currency.ID;
                                updateLoan.SolID = item.SolID;
                                updateLoan.SchemeCode = item.SchemeCode;
                                updateLoan.DueDate = item.DueDate;
                                updateLoan.PreviousInterestRate = item.PreviousInterestRate;
                                updateLoan.CurrentInterestRate = item.CurrentInterestRate;
                                updateLoan.BizSegmentID = item.BizSegment.ID;
                                updateLoan.EmployeeID = item.Employee.EmployeeID;
                                updateLoan.CSO = item.CSO;
                                updateLoan.IsSBLCSecured = item.IsSBLCSecured == "Yes" ? true : false;
                                updateLoan.IsHeavyEquipment = item.IsHeavyEquipment == "Yes" ? true : false;
                                updateLoan.ValueDate = item.ValueDate;
                                updateLoan.NextInterestDate = item.NextInterestDate;
                                updateLoan.NextPrincipalDate = item.NextPrincipalDate;
                                updateLoan.ApprovedMarginAsPerCM = item.ApprovedMarginAsPerCM;
                                updateLoan.InterestRateCodeID = item.InterestRateCodeID;
                                updateLoan.BaseRate = item.BaseRate;
                                updateLoan.AllinFTPRate = item.AllinFTPRate;
                                updateLoan.AccountPreferencial = item.AccountPreferencial;
                                updateLoan.SpecialFTP = item.SpecialFTP;
                                updateLoan.SpecialRate = item.SpecialRate;
                                updateLoan.AllinRate = item.AllinRate;
                                updateLoan.ApprovalDOA = item.ApprovalDOA;
                                updateLoan.ApprovedBy = item.ApprovedBy;
                                updateLoan.Remarks = item.Remarks;
                                updateLoan.OtherRemarks = item.OtherRemarks;
                                updateLoan.IsAdhoc = item.IsAdhoc;
                                updateLoan.CSOName = item.CSOName;
                                updateLoan.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                updateLoan.UpdateDate = DateTime.UtcNow;
                                updateLoan.PeggingReviewDate = item.PeggingReviewDate;
                                updateLoan.IsCompleted = false;

                                context.SaveChanges();
                            }
                            else
                            {
                                //Add Loan Checker
                                DBS.Entity.LoanInterestMaintenance DataLoan = new DBS.Entity.LoanInterestMaintenance()
                                {
                                    TransactionInterestMaintenanceID = item.TransactionInterestMaintenanceID,
                                    CIF = item.CIF,
                                    LoanContractNo = item.LoanContractNo,
                                    CurrencyID = item.Currency.ID,
                                    SolID = item.SolID,
                                    SchemeCode = item.SchemeCode,
                                    DueDate = item.DueDate,
                                    PreviousInterestRate = item.PreviousInterestRate,
                                    CurrentInterestRate = item.CurrentInterestRate,
                                    BizSegmentID = item.BizSegment.ID,
                                    EmployeeID = item.Employee.EmployeeID,
                                    CSO = item.CSO,
                                    IsSBLCSecured = item.IsSBLCSecured == "Yes" ? true : false,
                                    IsHeavyEquipment = item.IsHeavyEquipment == "Yes" ? true : false,
                                    //MaintenanceTypeID = item.MaintenanceTypeID,
                                    //AmountRollover = item.AmountRollover,
                                    ValueDate = item.ValueDate,
                                    NextInterestDate = item.NextInterestDate,
                                    NextPrincipalDate = item.NextPrincipalDate,
                                    ApprovedMarginAsPerCM = item.ApprovedMarginAsPerCM,
                                    InterestRateCodeID = item.InterestRateCodeID,
                                    BaseRate = item.BaseRate,
                                    AllinFTPRate = item.AllinFTPRate,
                                    AccountPreferencial = item.AccountPreferencial,
                                    SpecialFTP = item.SpecialFTP,
                                    SpecialRate = item.SpecialRate,
                                    AllinRate = item.AllinRate,
                                    ApprovalDOA = item.ApprovalDOA,
                                    ApprovedBy = item.ApprovedBy,
                                    Remarks = item.Remarks,
                                    OtherRemarks = item.OtherRemarks,
                                    IsAdhoc = item.IsAdhoc,
                                    CSOName = item.CSOName,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName,
                                    UpdateBy = null,
                                    UpdateDate = null,
                                    PeggingReviewDate = item.PeggingReviewDate,
                                    IsCompleted = false

                                };
                                context.LoanInterestMaintenances.Add(DataLoan);
                                context.SaveChanges();
                            }
                        }
                        //}
                    }
                    catch (Exception ex)
                    {
                        message = ex.InnerException.Message;
                    }
                    ts.Complete();

                    IsSuccess = true;
                }
                return IsSuccess;
            }
        }
        public bool GetDataIMCSO(Guid workflowInstanceID, long approverID, ref LoanCheckerInterestMaintenance output, ref string message)
        {
            bool IsSuccess = false;
            long TransactionID = 0;
            try
            {
                TransactionID = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a.TransactionID).FirstOrDefault();
                var InstructionID = context.TransactionInterestMaintenances.Where(a => a.TransactionID.Equals(TransactionID)).Select(a => a.InterestMaintenanceID).ToList();

                long IMID = context.TransactionInterestMaintenances.Where(a => a.TransactionID.Equals(TransactionID)).Select(a => a.TransactionInterestMaintenanceID).FirstOrDefault();
                var LoanData = context.LoanInterestMaintenances.Where(a => a.TransactionInterestMaintenanceID.Equals(IMID)).FirstOrDefault();

                if (LoanData != null)
                {
                    output = (from a in context.Transactions.AsNoTracking()
                              where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                              select new LoanCheckerInterestMaintenance
                              {
                                  LoanCheckerMakerIM = (from b in context.LoanInterestMaintenances
                                                        join c in context.TransactionInterestMaintenances on b.TransactionInterestMaintenanceID equals c.TransactionInterestMaintenanceID
                                                        //join d in context.ParameterSystems on b.InterestRateCodeID equals d.ParsysID
                                                        where c.TransactionID.Equals(TransactionID)
                                                        select new TransactionInterestMaintenanceLoanModel
                                                        {
                                                            IsChecked = c.IsSelected.HasValue ? (c.IsSelected.Value == true ? true : false) : false,
                                                            TransactionInterestMaintenanceID = b.TransactionInterestMaintenanceID,
                                                            //CSOInterestMaintenanceID = b.CSOInterestMaintenanceID,
                                                            InstructionID = c.InstructionID,
                                                            SolID = b.SolID,
                                                            SchemeCode = b.SchemeCode,
                                                            CIF = b.CIF,
                                                            CustomerName = context.Customers.Where(e => e.CIF.Equals(b.CIF)).Select(e => e.CustomerName).FirstOrDefault(),
                                                            LoanContractNo = b.LoanContractNo,
                                                            DueDate = b.DueDate.Value,
                                                            PreviousInterestRate = b.PreviousInterestRate.Value,
                                                            CurrentInterestRate = b.CurrentInterestRate.Value,
                                                            ValueDate = b.ValueDate,
                                                            Currency = new CurrencyModel()
                                                            {
                                                                ID = a.Currency.CurrencyID,
                                                                Code = a.Currency.CurrencyCode,
                                                                Description = a.Currency.CurrencyDescription,
                                                                LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                                LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                            },
                                                            AmountDue = b.AmountRollover,
                                                            BizSegment = new BizSegmentModel()
                                                            {
                                                                ID = a.BizSegment.BizSegmentID,
                                                                Name = a.BizSegment.BizSegmentName,
                                                                Description = a.BizSegment.BizSegmentDescription,
                                                                LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                                LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                            },
                                                            Employee = new EmployeeModel()
                                                            {
                                                                EmployeeID = b.EmployeeID == null ? 1 : b.EmployeeID.Value,
                                                                EmployeeName = b.EmployeeID == null ? "" : context.Employees.Where(x => x.EmployeeID.Equals(b.EmployeeID.Value)).Select(x => x.EmployeeName).FirstOrDefault()
                                                            },
                                                            CSO = b.CSO,
                                                            IsSBLCSecured = b.IsSBLCSecured.Value == true ? "Yes" : "No",
                                                            IsHeavyEquipment = b.IsHeavyEquipment.Value == true ? "Yes" : "No",
                                                            MaintenanceTypeID = b.MaintenanceTypeID.Value,
                                                            AmountRollover = b.AmountRollover.Value,
                                                            NextPrincipalDate = b.NextPrincipalDate.Value,
                                                            NextInterestDate = b.NextInterestDate.Value,
                                                            ApprovedMarginAsPerCM = b.ApprovedMarginAsPerCM.Value,
                                                            InterestRateCodeID = b.InterestRateCodeID,
                                                            InterestRateCodeName = b.InterestRateCodeID.Value != null ? context.ParameterSystems.Where(d => d.ParsysID.Equals(b.InterestRateCodeID.Value)).Select(d => d.ParameterValue).FirstOrDefault() : "",
                                                            BaseRate = b.BaseRate.Value,
                                                            AllinFTPRate = b.AllinFTPRate.Value,
                                                            AccountPreferencial = b.AccountPreferencial.Value,
                                                            SpecialFTP = b.SpecialFTP.Value,
                                                            SpecialRate = b.SpecialRate.Value,
                                                            AllinRate = b.AllinRate.Value,
                                                            ApprovalDOA = b.ApprovalDOA,
                                                            ApprovedBy = b.ApprovedBy,
                                                            Remarks = b.Remarks,
                                                            IsAdhoc = b.IsAdhoc,
                                                            CSOName = b.CSOName,
                                                            PeggingReviewDate = b.PeggingReviewDate,
                                                            DetailLink = @"<a href='#' class='ShowDetail' onClick='$(this).ShowDetail(this)'>Detail</a>",
                                                            Documents = (from x in context.TransactionDocuments
                                                                         where InstructionID.Contains(x.TransactionID) && x.IsDeleted.Equals(false)
                                                                         select new TransactionDocumentModel()
                                                                         {
                                                                             ID = x.TransactionDocumentID,
                                                                             Type = new DocumentTypeModel()
                                                                             {
                                                                                 ID = x.DocumentType.DocTypeID,
                                                                                 Name = x.DocumentType.DocTypeName,
                                                                                 Description = x.DocumentType.DocTypeDescription,
                                                                                 LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                                 LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                             },
                                                                             Purpose = new DocumentPurposeModel()
                                                                             {
                                                                                 ID = x.DocumentPurpose.PurposeID,
                                                                                 Name = x.DocumentPurpose.PurposeName,
                                                                                 Description = x.DocumentPurpose.PurposeDescription,
                                                                                 LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                                 LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                                             },
                                                                             FileName = x.Filename,
                                                                             DocumentPath = x.DocumentPath,
                                                                             LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                             LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                                                         }).ToList()
                                                        }).ToList()
                              }).SingleOrDefault();
                }
                else
                {
                    output = (from a in context.Transactions.AsNoTracking()
                              where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                              select new LoanCheckerInterestMaintenance
                              {
                                  LoanCheckerMakerIM = (from b in context.CSOInterestMaintenances
                                                        join c in context.TransactionInterestMaintenances on b.TransactionInterestMaintenanceID equals c.TransactionInterestMaintenanceID
                                                        //join d in context.ParameterSystems on b.InterestRateCodeID equals d.ParsysID
                                                        where c.TransactionID.Equals(TransactionID)
                                                        select new TransactionInterestMaintenanceLoanModel
                                                        {
                                                            IsChecked = c.IsSelected.HasValue ? (c.IsSelected.Value == true ? true : false) : false,
                                                            TransactionInterestMaintenanceID = b.TransactionInterestMaintenanceID,
                                                            //CSOInterestMaintenanceID = b.CSOInterestMaintenanceID,
                                                            InstructionID = c.InstructionID,
                                                            SolID = b.SolID,
                                                            SchemeCode = b.SchemeCode,
                                                            CIF = b.CIF,
                                                            CustomerName = context.Customers.Where(e => e.CIF.Equals(b.CIF)).Select(e => e.CustomerName).FirstOrDefault(),
                                                            LoanContractNo = b.LoanContractNo,
                                                            DueDate = b.DueDate.Value,
                                                            PreviousInterestRate = b.PreviousInterestRate.Value,
                                                            CurrentInterestRate = b.CurrentInterestRate.Value,
                                                            ValueDate = b.ValueDate,
                                                            Currency = new CurrencyModel()
                                                            {
                                                                ID = a.Currency.CurrencyID,
                                                                Code = a.Currency.CurrencyCode,
                                                                Description = a.Currency.CurrencyDescription,
                                                                LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                                LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                            },
                                                            AmountDue = b.AmountRollover,
                                                            BizSegment = new BizSegmentModel()
                                                            {
                                                                ID = a.BizSegment.BizSegmentID,
                                                                Name = a.BizSegment.BizSegmentName,
                                                                Description = a.BizSegment.BizSegmentDescription,
                                                                LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                                LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                            },
                                                            Employee = new EmployeeModel()
                                                            {
                                                                EmployeeID = b.EmployeeID == null ? 1 : b.EmployeeID.Value,
                                                                EmployeeName = b.EmployeeID == null ? "" : context.Employees.Where(x => x.EmployeeID.Equals(b.EmployeeID.Value)).Select(x => x.EmployeeName).FirstOrDefault()
                                                            },
                                                            CSO = b.CSO,
                                                            IsSBLCSecured = b.IsSBLCSecured.Value == true ? "Yes" : "No",
                                                            IsHeavyEquipment = b.IsHeavyEquipment.Value == true ? "Yes" : "No",
                                                            MaintenanceTypeID = b.MaintenanceTypeID.Value,
                                                            AmountRollover = b.AmountRollover.Value,
                                                            NextPrincipalDate = b.NextPrincipalDate.Value,
                                                            NextInterestDate = b.NextInterestDate.Value,
                                                            ApprovedMarginAsPerCM = b.ApprovedMarginAsPerCM.Value,
                                                            InterestRateCodeID = b.InterestRateCodeID,
                                                            InterestRateCodeName = b.InterestRateCodeID.Value != null ? context.ParameterSystems.Where(d => d.ParsysID.Equals(b.InterestRateCodeID.Value)).Select(d => d.ParameterValue).FirstOrDefault() : "",
                                                            BaseRate = b.BaseRate.Value,
                                                            AllinFTPRate = b.AllinFTPRate.Value,
                                                            AccountPreferencial = b.AccountPreferencial.Value,
                                                            SpecialFTP = b.SpecialFTP.Value,
                                                            SpecialRate = b.SpecialRate.Value,
                                                            AllinRate = b.AllinRate.Value,
                                                            ApprovalDOA = b.ApprovalDOA,
                                                            ApprovedBy = b.ApprovedBy,
                                                            Remarks = b.Remarks,
                                                            IsAdhoc = b.IsAdhoc,
                                                            CSOName = b.CSOName,
                                                            PeggingReviewDate = b.PeggingReviewDate,
                                                            DetailLink = @"<a href='#' class='ShowDetail' onClick='$(this).ShowDetail(this)'>Detail</a>",
                                                            Documents = (from x in context.TransactionDocuments
                                                                         where InstructionID.Contains(x.TransactionID) && x.IsDeleted.Equals(false)
                                                                         select new TransactionDocumentModel()
                                                                         {
                                                                             ID = x.TransactionDocumentID,
                                                                             Type = new DocumentTypeModel()
                                                                             {
                                                                                 ID = x.DocumentType.DocTypeID,
                                                                                 Name = x.DocumentType.DocTypeName,
                                                                                 Description = x.DocumentType.DocTypeDescription,
                                                                                 LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                                 LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                             },
                                                                             Purpose = new DocumentPurposeModel()
                                                                             {
                                                                                 ID = x.DocumentPurpose.PurposeID,
                                                                                 Name = x.DocumentPurpose.PurposeName,
                                                                                 Description = x.DocumentPurpose.PurposeDescription,
                                                                                 LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                                 LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                                             },
                                                                             FileName = x.Filename,
                                                                             DocumentPath = x.DocumentPath,
                                                                             LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                             LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                                                         }).ToList()
                                                        }).ToList()
                              }).SingleOrDefault();
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return IsSuccess;
        }
        public bool AddDataIMCSO(Guid workflowInstanceID, ref LoanCheckerInterestMaintenance output, long approverID, ref string message)
        {
            bool IsSuccess = false;

            using (DBSEntities context = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        foreach (var item in output.LoanCheckerMakerIM)
                        {
                            var updateIM = context.TransactionInterestMaintenances.Where(a => a.TransactionInterestMaintenanceID.Equals(item.TransactionInterestMaintenanceID)).SingleOrDefault();
                            updateIM.IsSelected = item.IsChecked;
                            updateIM.UpdateBy = currentUser.GetCurrentUser().LoginName;
                            updateIM.UpdateDate = DateTime.UtcNow;

                            context.SaveChanges();

                            var detailIM = context.CSOInterestMaintenances.Where(a => a.TransactionInterestMaintenanceID.Equals(item.TransactionInterestMaintenanceID)).SingleOrDefault();
                            //detailIM.SolID = item.SolID;
                            //detailIM.SchemeCode = item.SchemeCode;
                            //detailIM.CIF = item.CIF;
                            //detailIM.DueDate = item.DueDate;
                            //detailIM.LoanContractNo = item.LoanContractNo;
                            //detailIM.CurrencyID = item.CurrencyID;
                            //detailIM.PreviousInterestRate = item.PreviousInterestRate;
                            //detailIM.CurrentInterestRate = item.CurrentInterestRate;
                            //detailIM.BizSegmentID = item.BizSegmentID;
                            //detailIM.EmployeeID = item.EmployeeID;
                            //detailIM.CSO = item.CSO;
                            //detailIM.CSOName = item.CSOName;
                            detailIM.IsSBLCSecured = item.IsSBLCSecured == "Yes" ? true : false;
                            detailIM.IsHeavyEquipment = item.IsHeavyEquipment == "Yes" ? true : false;
                            //detailIM.ValueDate = item.ValueDate;
                            detailIM.NextInterestDate = item.NextInterestDate;
                            detailIM.NextPrincipalDate = item.NextPrincipalDate;
                            detailIM.ApprovedMarginAsPerCM = item.ApprovedMarginAsPerCM.Value;
                            detailIM.InterestRateCodeID = item.InterestRateCodeName != null ? context.ParameterSystems.Where(k => k.ParameterValue.Equals(item.InterestRateCodeName)).Select(d => d.ParsysID).FirstOrDefault() : 0;
                            detailIM.BaseRate = item.BaseRate;
                            detailIM.AccountPreferencial = item.AccountPreferencial;
                            detailIM.SpecialFTP = item.SpecialFTP;
                            detailIM.SpecialRate = item.SpecialRate;
                            detailIM.AllinRate = item.AllinRate;
                            detailIM.ApprovalDOA = item.ApprovalDOA;
                            detailIM.ApprovedBy = item.ApprovedBy;
                            detailIM.Remarks = item.Remarks;
                            detailIM.UpdateBy = currentUser.GetCurrentUser().LoginName;
                            detailIM.UpdateDate = DateTime.UtcNow;

                            context.SaveChanges();

                            long TransID = output.Transaction.ID;
                            #region Save Document
                            var existingDocs = context.TransactionDocuments.Where(a => a.TransactionID.Equals(TransID) && a.IsDeleted == false).ToList();
                            if (existingDocs != null)
                            {
                                if (existingDocs.Count() > 0)
                                {
                                    foreach (var item2 in existingDocs)
                                    {
                                        var deleteDoc = context.TransactionDocuments.Where(a => a.TransactionDocumentID.Equals(item2.TransactionDocumentID)).SingleOrDefault();
                                        deleteDoc.IsDeleted = true;
                                        context.SaveChanges();
                                    }
                                }
                            }
                            if (output.Transaction.Documents.Count > 0)
                            {
                                foreach (var item3 in output.Transaction.Documents)
                                {
                                    Entity.TransactionDocument document = new Entity.TransactionDocument()
                                    {
                                        TransactionID = TransID,
                                        DocTypeID = item3.Type.ID,
                                        PurposeID = item3.Purpose.ID,
                                        Filename = item3.FileName,
                                        DocumentPath = item3.DocumentPath,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName
                                    };
                                    context.TransactionDocuments.Add(document);
                                }
                                context.SaveChanges();
                            }
                            #endregion

                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.InnerException.Message;
                    }
                    ts.Complete();

                    IsSuccess = true;
                }
                return IsSuccess;
            }
        }

        #endregion

        #region Loan Checker
        public bool GetDataIMLoanChecker(Guid workflowInstanceID, long approverID, ref InterestMaintenanceExcelCheckerLoan output, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                var TransactionID = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a.TransactionID).FirstOrDefault();
                var InstructionID = context.TransactionInterestMaintenances.Where(a => a.TransactionID.Equals(TransactionID)).Select(a => a.InterestMaintenanceID).ToList();

                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new InterestMaintenanceExcelCheckerLoan
                          {
                              LoanCheckerIM = (from b in context.LoanInterestMaintenances
                                               join c in context.TransactionInterestMaintenances on b.TransactionInterestMaintenanceID equals c.TransactionInterestMaintenanceID
                                               //join d in context.ParameterSystems on b.InterestRateCodeID equals d.ParsysID
                                               where c.TransactionID.Equals(TransactionID) && b.IsCompleted.Value != true
                                               select new LoanInterestMaintenance
                                               {
                                                   IsChecked = c.IsSelected.HasValue ? (c.IsSelected.Value == true ? true : false) : false,
                                                   LoanInterestMaintenanceID = b.LoanInterestMaintenanceID,
                                                   TransactionInterestMaintenanceID = c.TransactionInterestMaintenanceID,
                                                   SolID = b.SolID,
                                                   SchemeCode = b.SchemeCode,
                                                   CIF = b.CIF,
                                                   CustomerName = context.Customers.Where(e => e.CIF.Equals(b.CIF)).Select(e => e.CustomerName).FirstOrDefault(),
                                                   LoanContractNo = b.LoanContractNo,
                                                   DueDate = b.DueDate.Value,
                                                   Currency = new CurrencyModel()
                                                   {
                                                       ID = a.Currency.CurrencyID,
                                                       Code = a.Currency.CurrencyCode,
                                                       Description = a.Currency.CurrencyDescription,
                                                       LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                       LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                   },

                                                   BizSegment = new BizSegmentModel()
                                                   {
                                                       ID = a.BizSegment.BizSegmentID,
                                                       Name = a.BizSegment.BizSegmentName,
                                                       Description = a.BizSegment.BizSegmentDescription,
                                                       LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                       LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                   },
                                                   Employee = new EmployeeModel()
                                                   {
                                                       EmployeeID = b.EmployeeID == null ? 1 : b.EmployeeID.Value,
                                                       EmployeeName = b.EmployeeID == null ? "" : context.Employees.Where(x => x.EmployeeID.Equals(b.EmployeeID.Value)).Select(x => x.EmployeeName).FirstOrDefault()
                                                   },
                                                   CSO = b.CSO,
                                                   PreviousInterestRate = b.PreviousInterestRate,
                                                   CurrentInterestRate = b.CurrentInterestRate,
                                                   IsSBLCSecured = b.IsSBLCSecured.Value == true ? "Yes" : "No",
                                                   IsHeavyEquipment = b.IsHeavyEquipment.Value == true ? "Yes" : "No",
                                                   ValueDate = b.ValueDate,
                                                   MaintenanceTypeID = b.MaintenanceTypeID.Value,
                                                   AmountRollover = b.AmountRollover.Value,
                                                   NextPrincipalDate = b.NextPrincipalDate.Value,
                                                   NextInterestDate = b.NextInterestDate.Value,
                                                   ApprovedMarginAsPerCM = b.ApprovedMarginAsPerCM.Value,
                                                   InterestRateCodeID = b.InterestRateCodeID,
                                                   InterestRateCodeName = b.InterestRateCodeID.Value != null ? context.ParameterSystems.Where(d => d.ParsysID.Equals(b.InterestRateCodeID.Value)).Select(d => d.ParameterValue).FirstOrDefault() : "",
                                                   BaseRate = b.BaseRate.Value,
                                                   AllinFTPRate = b.AllinFTPRate.Value,
                                                   AccountPreferencial = b.AccountPreferencial.Value,
                                                   SpecialFTP = b.SpecialFTP.Value,
                                                   SpecialRate = b.SpecialRate.Value,
                                                   AllinRate = b.AllinRate.Value,
                                                   ApprovalDOA = b.ApprovalDOA,
                                                   ApprovedBy = b.ApprovedBy,
                                                   Remarks = b.Remarks,
                                                   IsAdhoc = b.IsAdhoc,
                                                   CSOName = b.CSOName,
                                                   PeggingReviewDate = b.PeggingReviewDate,
                                                   DetailLink = @"<a href='#' class='ShowDetail' onClick='$(this).ShowDetail(this)'>Detail</a>",
                                                   Documents = (from x in context.TransactionDocuments
                                                                where InstructionID.Contains(x.TransactionID) && x.IsDeleted.Equals(false)
                                                                select new TransactionDocumentModel()
                                                                {
                                                                    ID = x.TransactionDocumentID,
                                                                    Type = new DocumentTypeModel()
                                                                    {
                                                                        ID = x.DocumentType.DocTypeID,
                                                                        Name = x.DocumentType.DocTypeName,
                                                                        Description = x.DocumentType.DocTypeDescription,
                                                                        LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                        LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                    },
                                                                    Purpose = new DocumentPurposeModel()
                                                                    {
                                                                        ID = x.DocumentPurpose.PurposeID,
                                                                        Name = x.DocumentPurpose.PurposeName,
                                                                        Description = x.DocumentPurpose.PurposeDescription,
                                                                        LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                        LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                                    },
                                                                    FileName = x.Filename,
                                                                    DocumentPath = x.DocumentPath,
                                                                    LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                    LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                                                }).ToList()
                                               }).ToList()
                          }).SingleOrDefault();
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return IsSuccess;
        }
        #region tak Terpakai
        //public bool AddDataIMChecker(Guid workflowInstanceID, ref InterestMaintenanceExcelCheckerLoan output, long approverID, ref string message)
        //{
        //    bool IsSuccess = false;

        //    using (DBSEntities context = new DBSEntities())
        //    {
        //        using (TransactionScope ts = new TransactionScope())
        //        {
        //            try
        //            {

        //                IList<DBS.Entity.LoanInterestMaintenance> selectedModel = (from item in output.LoanCheckerIM
        //                                                                           where item.IsChecked == true
        //                                                                           select new DBS.Entity.LoanInterestMaintenance
        //                                                                           {
        //                                                                               //Wajib Isi
        //                                                                               TransactionInterestMaintenanceID = item.TransactionInterestMaintenanceID,  
        //                                                                               CIF = item.CIF,
        //                                                                               LoanContractNo = item.LoanContractNo,
        //                                                                               //Wajib
        //                                                                               CurrencyID = item.Currency.ID,
        //                                                                               SolID = item.SolID,
        //                                                                               SchemeCode = item.SchemeCode,
        //                                                                               DueDate = item.DueDate,
        //                                                                               PreviousInterestRate = item.PreviousInterestRate,
        //                                                                               CurrentInterestRate = item.CurrentInterestRate,
        //                                                                               BizSegmentID = item.BizSegment.ID,
        //                                                                               //EmployeeID = item.RM.EmployeeID,
        //                                                                               CSO = item.CSO,
        //                                                                               IsSBLCSecured = item.IsSBLCSecured == "Yes" ? true : false,
        //                                                                               IsHeavyEquipment = item.IsHeavyEquipment == "Yes" ? true : false,
        //                                                                               //MaintenanceTypeID = item.MaintenanceTypeID,
        //                                                                               AmountRollover = item.AmountRollover.Value,
        //                                                                               ValueDate = item.ValueDate,
        //                                                                               NextInterestDate = item.NextInterestDate,
        //                                                                               NextPrincipalDate = item.NextPrincipalDate,
        //                                                                               ApprovedMarginAsPerCM = item.ApprovedMarginAsPerCM,
        //                                                                               InterestRateCodeID = item.InterestRateCodeID,
        //                                                                               BaseRate = item.BaseRate,
        //                                                                               AllinFTPRate = item.AllinFTPRate,
        //                                                                               AccountPreferencial = item.AccountPreferencial,
        //                                                                               SpecialFTP = item.SpecialFTP,
        //                                                                               SpecialRate = item.SpecialRate,
        //                                                                               AllinRate = item.AllinRate,
        //                                                                               ApprovalDOA = item.ApprovalDOA,
        //                                                                               ApprovedBy = item.ApprovedBy,
        //                                                                               Remarks = item.Remarks,
        //                                                                               OtherRemarks = item.OtherRemarks,
        //                                                                               IsAdhoc = item.IsAdhoc,
        //                                                                               CSOName = item.CSOName,
        //                                                                               //Wajib
        //                                                                               CreateDate = DateTime.UtcNow,
        //                                                                               CreateBy = currentUser.GetCurrentUser().LoginName,
        //                                                                               //Wajib
        //                                                                               UpdateBy = null,
        //                                                                               UpdateDate = null,
        //                                                                               IsCompleted = true

        //                                                                           }).ToList();
        //                context.LoanInterestMaintenances.AddRange(selectedModel);
        //                context.SaveChanges();
        //            }
        //            catch (Exception ex)
        //            {
        //                message = ex.InnerException.Message;
        //            }
        //            ts.Complete();

        //            IsSuccess = true;
        //        }
        //        return IsSuccess;
        //    }
        //}
        #endregion
        public bool AddDataIMChecker(Guid workflowInstanceID, ref InterestMaintenanceExcelCheckerLoan output, long approverID, ref string message)
        {
            bool IsSuccess = false;

            using (DBSEntities context = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        foreach (var item in output.LoanCheckerIM)
                        {
                            //if (item.IsChecked == true)
                            //{
                            var updateIsCompleted = context.LoanInterestMaintenances.Where(a => a.LoanInterestMaintenanceID.Equals(item.LoanInterestMaintenanceID)).SingleOrDefault();
                            updateIsCompleted.IsCompleted = true;
                            //}
                        }
                        context.SaveChanges();

                    }
                    catch (Exception ex)
                    {
                        message = ex.InnerException.Message;
                    }
                    ts.Complete();

                    IsSuccess = true;
                }
                return IsSuccess;
            }
        }

        #endregion

        #endregion

        #region Get And Add Data Rollover
        #region Revise CSO
        public bool GetDataRolloverCSO(Guid workflowInstanceID, long approverID, ref LoanCheckerRollover output, ref string message)
        {
            bool IsSuccess = false;
            long TransactionID = 0;
            try
            {

                TransactionID = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a.TransactionID).FirstOrDefault();
                long RolID = context.TransactionRollovers.Where(a => a.TransactionID.Equals(TransactionID)).Select(a => a.TransactionRolloverID).FirstOrDefault();

                var DataLoan = context.LoanRollovers.Where(a => a.TransactionRolloverID.Equals(RolID)).FirstOrDefault();

                if (DataLoan != null)
                {
                    output = (from a in context.Transactions.AsNoTracking()
                              where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                              select new LoanCheckerRollover
                              {

                                  LoanCheckerMakerRollover = (from b in context.LoanRollovers
                                                              join c in context.TransactionRollovers on b.TransactionRolloverID equals c.TransactionRolloverID
                                                              //join d in context.ParameterSystems on b.MaintenanceTypeID equals d.ParsysID
                                                              //join e in context.ParameterSystems on b.InterestRateCodeID equals e.ParsysID
                                                              where c.TransactionID.Equals(a.TransactionID)

                                                              select new LoanCheckerMakerModel
                                                              {
                                                                  IsChecked = c.IsSelected.HasValue ? (c.IsSelected.Value == true ? true : false) : false,
                                                                  TransactionRolloverID = b.TransactionRolloverID,
                                                                  InstructionID = c.Instruction,
                                                                  //CSORollOverID = b.Rollo,
                                                                  SOLID = b.SOLID,
                                                                  SchemeCode = b.SchemeCode,
                                                                  CIF = b.CIF,
                                                                  CustomerName = context.Customers.Where(f => f.CIF.Equals(b.CIF)).Select(f => f.CustomerName).FirstOrDefault(),
                                                                  LoanContractNo = b.LoanContractNo,
                                                                  DueDate = b.DueDate.Value,
                                                                  ValueDate = DateTime.UtcNow,
                                                                  Currency = new CurrencyModel()
                                                                  {
                                                                      ID = a.Currency.CurrencyID,
                                                                      Code = a.Currency.CurrencyCode,
                                                                      Description = a.Currency.CurrencyDescription,
                                                                      LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                                      LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                                  },
                                                                  AmountDue = b.AmountDue.Value,
                                                                  LimitID = b.LimitID,
                                                                  BizSegment = new BizSegmentModel()
                                                                  {
                                                                      ID = a.BizSegment.BizSegmentID,
                                                                      Name = a.BizSegment.BizSegmentName,
                                                                      Description = a.BizSegment.BizSegmentDescription,
                                                                      LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                                      LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                                  },
                                                                  Employee = new EmployeeModel()
                                                                  {
                                                                      EmployeeID = b.EmployeeID == null ? 1 : b.EmployeeID.Value,
                                                                      EmployeeName = b.EmployeeID == null ? "" : context.Employees.Where(x => x.EmployeeID.Equals(b.EmployeeID.Value)).Select(x => x.EmployeeName).FirstOrDefault()
                                                                  },
                                                                  CSO = b.CSO,
                                                                  IsSBLCSecured = b.IsSBLCSecured.Value == true ? "Yes" : "No",
                                                                  IsHeavyEquipment = b.IsHeavyEquipment.Value == true ? "Yes" : "No",
                                                                  MaintenanceTypeID = b.MaintenanceTypeID.Value,
                                                                  MaintenanceTypeName = b.MaintenanceTypeID.Value != null ? context.ParameterSystems.Where(d => d.ParsysID.Equals(b.MaintenanceTypeID.Value)).Select(d => d.ParameterValue).FirstOrDefault() : "",
                                                                  AmountRollover = b.AmountRollover.Value,
                                                                  NextPrincipalDate = b.NextPrincipalDate.Value,
                                                                  NextInterestDate = b.NextInterestDate.Value,
                                                                  ApprovedMarginAsPerCM = b.ApprovedMarginAsPerCM.Value,
                                                                  InterestRateCodeID = b.InterestRateCodeID,
                                                                  InterestRateCodeName = b.InterestRateCodeID.Value != null ? context.ParameterSystems.Where(d => d.ParsysID.Equals(b.InterestRateCodeID.Value)).Select(d => d.ParameterValue).FirstOrDefault() : "",
                                                                  BaseRate = b.BaseRate.Value,
                                                                  AllInLP = b.AllInLP.Value,
                                                                  AllInFTPRate = b.AllInFTPRate.Value,
                                                                  AccountPreferencial = b.AccountPreferencial.Value,
                                                                  SpecialFTP = b.SpecialFTP.Value,
                                                                  SpecialRateMargin = b.SpecialRateMargin.Value,
                                                                  AllInrate = b.AllInrate.Value,
                                                                  ApprovalDOA = b.ApprovalDOA,
                                                                  ApprovedBy = b.ApprovedBy,
                                                                  Remarks = b.Remarks,
                                                                  IsAdhoc = b.IsAdhoc.Value == true ? "Yes" : "No",
                                                                  CSOName = b.CSOName,
                                                                  DetailLink = @"<a href='#' class='ShowDetail' onClick='$(this).ShowDetail(this)'>Detail</a>",
                                                                  LinkAddHoc = context.TransactionDocumentLoans.Where(f => f.TransactionID.Value.Equals(b.TransactionRolloverID)).Select(f => @"<a href='" + f.DocumentPath + "' target='_blank'>" + f.Filename + "</a>").FirstOrDefault()
                                                              }).ToList()
                              }).SingleOrDefault();
                }
                else
                {
                    output = (from a in context.Transactions.AsNoTracking()
                              where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                              select new LoanCheckerRollover
                              {

                                  LoanCheckerMakerRollover = (from b in context.CSORollovers
                                                              join c in context.TransactionRollovers on b.TransactionRolloverID equals c.TransactionRolloverID
                                                              //join d in context.ParameterSystems on b.MaintenanceTypeID equals d.ParsysID
                                                              //join e in context.ParameterSystems on b.InterestRateCodeID equals e.ParsysID
                                                              where c.TransactionID.Equals(a.TransactionID)

                                                              select new LoanCheckerMakerModel
                                                              {
                                                                  IsChecked = c.IsSelected.HasValue ? (c.IsSelected.Value == true ? true : false) : false,
                                                                  TransactionRolloverID = b.TransactionRolloverID,
                                                                  InstructionID = c.Instruction,
                                                                  CSORollOverID = b.CSORolloverID,
                                                                  SOLID = b.SOLID,
                                                                  SchemeCode = b.SchemeCode,
                                                                  CIF = b.CIF,
                                                                  CustomerName = context.Customers.Where(f => f.CIF.Equals(b.CIF)).Select(f => f.CustomerName).FirstOrDefault(),
                                                                  LoanContractNo = b.LoanContractNo,
                                                                  DueDate = b.DueDate.Value,
                                                                  ValueDate = DateTime.UtcNow,
                                                                  Currency = new CurrencyModel()
                                                                  {
                                                                      ID = a.Currency.CurrencyID,
                                                                      Code = a.Currency.CurrencyCode,
                                                                      Description = a.Currency.CurrencyDescription,
                                                                      LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                                      LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                                  },
                                                                  AmountDue = b.AmountDue.Value,
                                                                  LimitID = b.LimitID,
                                                                  BizSegment = new BizSegmentModel()
                                                                  {
                                                                      ID = a.BizSegment.BizSegmentID,
                                                                      Name = a.BizSegment.BizSegmentName,
                                                                      Description = a.BizSegment.BizSegmentDescription,
                                                                      LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                                      LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                                  },
                                                                  Employee = new EmployeeModel()
                                                                  {
                                                                      EmployeeID = b.EmployeeID == null ? 1 : b.EmployeeID.Value,
                                                                      EmployeeName = b.EmployeeID == null ? "" : context.Employees.Where(x => x.EmployeeID.Equals(b.EmployeeID.Value)).Select(x => x.EmployeeName).FirstOrDefault()
                                                                  },
                                                                  CSO = b.CSO,
                                                                  IsSBLCSecured = b.IsSBLCSecured.Value == true ? "Yes" : "No",
                                                                  IsHeavyEquipment = b.IsHeavyEquipment.Value == true ? "Yes" : "No",
                                                                  MaintenanceTypeID = b.MaintenanceTypeID.Value,
                                                                  MaintenanceTypeName = b.MaintenanceTypeID.Value != null ? context.ParameterSystems.Where(d => d.ParsysID.Equals(b.MaintenanceTypeID.Value)).Select(d => d.ParameterValue).FirstOrDefault() : "",
                                                                  AmountRollover = b.AmountRollover.Value,
                                                                  NextPrincipalDate = b.NextPrincipalDate.Value,
                                                                  NextInterestDate = b.NextInterestDate.Value,
                                                                  ApprovedMarginAsPerCM = b.ApprovedMarginAsPerCM.Value,
                                                                  InterestRateCodeID = b.InterestRateCodeID,
                                                                  InterestRateCodeName = b.InterestRateCodeID.Value != null ? context.ParameterSystems.Where(d => d.ParsysID.Equals(b.InterestRateCodeID.Value)).Select(d => d.ParameterValue).FirstOrDefault() : "",
                                                                  BaseRate = b.BaseRate.Value,
                                                                  AllInLP = b.AllInLP.Value,
                                                                  AllInFTPRate = b.AllInFTPRate.Value,
                                                                  AccountPreferencial = b.AccountPreferencial.Value,
                                                                  SpecialFTP = b.SpecialFTP.Value,
                                                                  SpecialRateMargin = b.SpecialRateMargin.Value,
                                                                  AllInrate = b.AllInrate.Value,
                                                                  ApprovalDOA = b.ApprovalDOA,
                                                                  ApprovedBy = b.ApprovedBy,
                                                                  Remarks = b.Remarks,
                                                                  IsAdhoc = b.IsAdhoc.Value == true ? "Yes" : "No",
                                                                  CSOName = b.CSOName,
                                                                  DetailLink = @"<a href='#' class='ShowDetail' onClick='$(this).ShowDetail(this)'>Detail</a>",
                                                                  LinkAddHoc = context.TransactionDocumentLoans.Where(f => f.TransactionID.Value.Equals(b.TransactionRolloverID)).Select(f => @"<a href='" + f.DocumentPath + "' target='_blank'>" + f.Filename + "</a>").FirstOrDefault()
                                                              }).ToList()
                              }).SingleOrDefault();
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return IsSuccess;
        }

        public bool AddCSORollover(Guid workflowInstanceID, ref LoanCheckerRollover output, long approverID, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    foreach (var item in output.LoanCheckerMakerRollover)
                    {
                        var updateRO = context.TransactionRollovers.Where(a => a.TransactionRolloverID.Equals(item.TransactionRolloverID)).SingleOrDefault();
                        updateRO.IsSelected = item.IsChecked;

                        context.SaveChanges();

                        var detailRO = context.CSORollovers.Where(a => a.TransactionRolloverID.Equals(item.TransactionRolloverID)).SingleOrDefault();
                        if (item.MaintenanceTypeName == "Rollover")
                        {
                            //detailRO.TransactionRolloverID = item.TransactionRolloverID;
                            //detailRO.SOLID = item.SOLID;
                            //detailRO.SchemeCode = item.SchemeCode;
                            //detailRO.CIF = item.CIF;
                            //detailRO.DueDate = item.DueDate;
                            //detailRO.LoanContractNo = item.LoanContractNo;
                            //detailRO.CurrencyID = item.Currency.ID;
                            //detailRO.AmountDue = item.AmountDue;
                            //detailRO.LimitID = item.LimitID;
                            //detailRO.BizSegmentID = item.BizSegment.ID;
                            //detailRO.EmployeeID = item.RM.EmployeeID;
                            //detailRO.CSO = item.CSO;
                            //detailRO.CSOName = item.CSOName;
                            detailRO.IsSBLCSecured = item.IsSBLCSecured == "Yes" ? true : false;
                            detailRO.IsHeavyEquipment = item.IsHeavyEquipment == "Yes" ? true : false;
                            detailRO.MaintenanceTypeID = item.MaintenanceTypeName != null ? context.ParameterSystems.Where(j => j.ParameterValue.Equals(item.MaintenanceTypeName)).Select(j => j.ParsysID).FirstOrDefault() : 0;
                            detailRO.AmountRollover = item.AmountRollover;
                            detailRO.NextInterestDate = item.NextInterestDate;
                            detailRO.NextPrincipalDate = item.NextPrincipalDate;
                            detailRO.ApprovedMarginAsPerCM = item.ApprovedMarginAsPerCM;
                            detailRO.InterestRateCodeID = item.InterestRateCodeName != null ? context.ParameterSystems.Where(k => k.ParameterValue.Equals(item.InterestRateCodeName)).Select(d => d.ParsysID).FirstOrDefault() : 0;
                            detailRO.BaseRate = item.BaseRate;
                            detailRO.AccountPreferencial = item.AccountPreferencial;
                            detailRO.SpecialFTP = item.SpecialFTP;
                            detailRO.SpecialRateMargin = item.SpecialRateMargin;
                            //detailRO.AllInrate = item.AllinRate;
                            detailRO.AllInrate = item.AllInrate;
                            detailRO.ApprovalDOA = item.ApprovalDOA;
                            detailRO.ApprovedBy = item.ApprovedBy;
                            detailRO.Remarks = item.Remarks;
                            detailRO.UpdateBy = currentUser.GetCurrentUser().LoginName;
                            detailRO.Updatedate = DateTime.UtcNow;
                            detailRO.IsAdhoc = item.IsAdhoc == "Yes" ? true : false;
                        }
                        else
                        {
                            //detailRO.TransactionRolloverID = item.TransactionRolloverID;
                            //detailRO.SOLID = item.SOLID;
                            //detailRO.SchemeCode = item.SchemeCode;
                            //detailRO.CIF = item.CIF;
                            //detailRO.DueDate = item.DueDate;
                            //detailRO.LoanContractNo = item.LoanContractNo;
                            //detailRO.CurrencyID = item.Currency.ID;
                            //detailRO.AmountDue = item.AmountDue;
                            //detailRO.LimitID = item.LimitID;
                            //detailRO.BizSegmentID = item.BizSegment.ID;
                            //detailRO.EmployeeID = item.RM.EmployeeID;
                            //detailRO.CSO = item.CSO;
                            //detailRO.CSOName = item.CSOName;
                            detailRO.IsSBLCSecured = item.IsSBLCSecured == "Yes" ? true : false;
                            detailRO.IsHeavyEquipment = item.IsHeavyEquipment == "Yes" ? true : false;
                            detailRO.MaintenanceTypeID = item.MaintenanceTypeName != null ? context.ParameterSystems.Where(j => j.ParameterValue.Equals(item.MaintenanceTypeName)).Select(j => j.ParsysID).FirstOrDefault() : 0;
                            detailRO.AmountRollover = item.AmountRollover;
                            detailRO.NextInterestDate = null;
                            detailRO.NextPrincipalDate = null;
                            detailRO.ApprovedMarginAsPerCM = null;
                            detailRO.InterestRateCodeID = null;
                            detailRO.BaseRate = null;
                            detailRO.AccountPreferencial = null;
                            detailRO.SpecialFTP = null;
                            detailRO.SpecialRateMargin = null;
                            detailRO.AllInrate = null;
                            detailRO.ApprovalDOA = null;
                            detailRO.ApprovedBy = null;
                            detailRO.Remarks = item.Remarks;
                            detailRO.UpdateBy = currentUser.GetCurrentUser().LoginName;
                            detailRO.Updatedate = DateTime.UtcNow;
                            detailRO.IsAdhoc = item.IsAdhoc == "Yes" ? true : false;
                        }
                        context.SaveChanges();
                    }

                    long TransID = output.Transaction.ID;
                    #region Save Document
                    var existingDocs = context.TransactionDocuments.Where(a => a.TransactionID.Equals(TransID) && a.IsDeleted == false).ToList();
                    if (existingDocs != null)
                    {
                        if (existingDocs.Count() > 0)
                        {
                            foreach (var item2 in existingDocs)
                            {
                                var deleteDoc = context.TransactionDocuments.Where(a => a.TransactionDocumentID.Equals(item2.TransactionDocumentID)).SingleOrDefault();
                                deleteDoc.IsDeleted = true;
                                context.SaveChanges();
                            }
                        }
                    }
                    if (output.Transaction.Documents.Count > 0)
                    {
                        foreach (var item3 in output.Transaction.Documents)
                        {
                            Entity.TransactionDocument document = new Entity.TransactionDocument()
                            {
                                TransactionID = TransID,
                                DocTypeID = item3.Type.ID,
                                PurposeID = item3.Purpose.ID,
                                Filename = item3.FileName,
                                DocumentPath = item3.DocumentPath,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName
                            };
                            context.TransactionDocuments.Add(document);
                        }
                        context.SaveChanges();
                    }
                    #endregion

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string _message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(_message, raise);
                        }
                    }
                    throw raise;
                }

                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();

                }
            }
            return IsSuccess;
        }

        #endregion
        #region Loan Maker
        public bool GetDataRollover(Guid workflowInstanceID, long approverID, ref LoanCheckerRollover output, ref string message)
        {
            bool IsSuccess = false;
            long TransactionID = 0;
            try
            {

                TransactionID = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a.TransactionID).FirstOrDefault();
                long RolID = context.TransactionRollovers.Where(a => a.TransactionID.Equals(TransactionID)).Select(a => a.TransactionRolloverID).FirstOrDefault();

                var DataLoan = context.LoanRollovers.Where(a => a.TransactionRolloverID.Equals(RolID)).FirstOrDefault();

                if (DataLoan != null)
                {
                    output = (from a in context.Transactions.AsNoTracking()
                              where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                              select new LoanCheckerRollover
                              {

                                  LoanCheckerMakerRollover = (from b in context.LoanRollovers
                                                              join c in context.TransactionRollovers on b.TransactionRolloverID equals c.TransactionRolloverID
                                                              //join d in context.ParameterSystems on b.MaintenanceTypeID equals d.ParsysID
                                                              //join e in context.ParameterSystems on b.InterestRateCodeID equals e.ParsysID
                                                              where c.TransactionID.Equals(a.TransactionID)

                                                              select new LoanCheckerMakerModel
                                                              {
                                                                  IsChecked = c.IsSelected.HasValue ? (c.IsSelected.Value == true ? true : false) : false,
                                                                  TransactionRolloverID = b.TransactionRolloverID,
                                                                  InstructionID = c.Instruction,
                                                                  //CSORollOverID = b.Rollo,
                                                                  SOLID = b.SOLID,
                                                                  SchemeCode = b.SchemeCode,
                                                                  CIF = b.CIF,
                                                                  CustomerName = context.Customers.Where(f => f.CIF.Equals(b.CIF)).Select(f => f.CustomerName).FirstOrDefault(),
                                                                  LoanContractNo = b.LoanContractNo,
                                                                  DueDate = b.DueDate.Value,
                                                                  ValueDate = DateTime.UtcNow,
                                                                  Currency = new CurrencyModel()
                                                                  {
                                                                      ID = a.Currency.CurrencyID,
                                                                      Code = a.Currency.CurrencyCode,
                                                                      Description = a.Currency.CurrencyDescription,
                                                                      LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                                      LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                                  },
                                                                  AmountDue = b.AmountDue.Value,
                                                                  LimitID = b.LimitID,
                                                                  BizSegment = new BizSegmentModel()
                                                                  {
                                                                      ID = a.BizSegment.BizSegmentID,
                                                                      Name = a.BizSegment.BizSegmentName,
                                                                      Description = a.BizSegment.BizSegmentDescription,
                                                                      LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                                      LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                                  },
                                                                  Employee = new EmployeeModel()
                                                                  {
                                                                      EmployeeID = b.EmployeeID == null ? 1 : b.EmployeeID.Value,
                                                                      EmployeeName = b.EmployeeID == null ? "" : context.Employees.Where(x => x.EmployeeID.Equals(b.EmployeeID.Value)).Select(x => x.EmployeeName).FirstOrDefault()
                                                                  },
                                                                  CSO = b.CSO,
                                                                  IsSBLCSecured = b.IsSBLCSecured.Value == true ? "Yes" : "No",
                                                                  IsHeavyEquipment = b.IsHeavyEquipment.Value == true ? "Yes" : "No",
                                                                  MaintenanceTypeID = b.MaintenanceTypeID.Value,
                                                                  MaintenanceTypeName = b.MaintenanceTypeID.Value != null ? context.ParameterSystems.Where(d => d.ParsysID.Equals(b.MaintenanceTypeID.Value)).Select(d => d.ParameterValue).FirstOrDefault() : "",
                                                                  AmountRollover = b.AmountRollover.Value,
                                                                  NextPrincipalDate = b.NextPrincipalDate.Value,
                                                                  NextInterestDate = b.NextInterestDate.Value,
                                                                  ApprovedMarginAsPerCM = b.ApprovedMarginAsPerCM.Value,
                                                                  InterestRateCodeID = b.InterestRateCodeID,
                                                                  InterestRateCodeName = b.InterestRateCodeID.Value != null ? context.ParameterSystems.Where(d => d.ParsysID.Equals(b.InterestRateCodeID.Value)).Select(d => d.ParameterValue).FirstOrDefault() : "",
                                                                  BaseRate = b.BaseRate.Value,
                                                                  AllInLP = b.AllInLP.Value,
                                                                  AllInFTPRate = b.AllInFTPRate.Value,
                                                                  AccountPreferencial = b.AccountPreferencial.Value,
                                                                  SpecialFTP = b.SpecialFTP.Value,
                                                                  SpecialRateMargin = b.SpecialRateMargin.Value,
                                                                  AllInrate = b.AllInrate.Value,
                                                                  ApprovalDOA = b.ApprovalDOA,
                                                                  ApprovedBy = b.ApprovedBy,
                                                                  Remarks = b.Remarks,
                                                                  IsAdhoc = b.IsAdhoc.Value == true ? "Yes" : "No",
                                                                  CSOName = b.CSOName,
                                                                  DetailLink = @"<a href='#' class='ShowDetail' onClick='$(this).ShowDetail(this)'>Detail</a>",
                                                                  LinkAddHoc = context.TransactionDocumentLoans.Where(f => f.TransactionID.Value.Equals(b.TransactionRolloverID)).Select(f => @"<a href='" + f.DocumentPath + "' target='_blank'>" + f.Filename + "</a>").FirstOrDefault()
                                                              }).ToList()
                              }).SingleOrDefault();
                }
                else
                {
                    output = (from a in context.Transactions.AsNoTracking()
                              where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                              select new LoanCheckerRollover
                              {

                                  LoanCheckerMakerRollover = (from b in context.CSORollovers
                                                              join c in context.TransactionRollovers on b.TransactionRolloverID equals c.TransactionRolloverID
                                                              //join d in context.ParameterSystems on b.MaintenanceTypeID equals d.ParsysID
                                                              //join e in context.ParameterSystems on b.InterestRateCodeID equals e.ParsysID
                                                              where c.TransactionID.Equals(a.TransactionID)

                                                              select new LoanCheckerMakerModel
                                                              {
                                                                  IsChecked = c.IsSelected.HasValue ? (c.IsSelected.Value == true ? true : false) : false,
                                                                  TransactionRolloverID = b.TransactionRolloverID,
                                                                  InstructionID = c.Instruction,
                                                                  CSORollOverID = b.CSORolloverID,
                                                                  SOLID = b.SOLID,
                                                                  SchemeCode = b.SchemeCode,
                                                                  CIF = b.CIF,
                                                                  CustomerName = context.Customers.Where(f => f.CIF.Equals(b.CIF)).Select(f => f.CustomerName).FirstOrDefault(),
                                                                  LoanContractNo = b.LoanContractNo,
                                                                  DueDate = b.DueDate.Value,
                                                                  ValueDate = DateTime.UtcNow,
                                                                  Currency = new CurrencyModel()
                                                                  {
                                                                      ID = a.Currency.CurrencyID,
                                                                      Code = a.Currency.CurrencyCode,
                                                                      Description = a.Currency.CurrencyDescription,
                                                                      LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                                      LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                                  },
                                                                  AmountDue = b.AmountDue.Value,
                                                                  LimitID = b.LimitID,
                                                                  BizSegment = new BizSegmentModel()
                                                                  {
                                                                      ID = a.BizSegment.BizSegmentID,
                                                                      Name = a.BizSegment.BizSegmentName,
                                                                      Description = a.BizSegment.BizSegmentDescription,
                                                                      LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                                      LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                                  },
                                                                  Employee = new EmployeeModel()
                                                                  {
                                                                      EmployeeID = b.EmployeeID == null ? 1 : b.EmployeeID.Value,
                                                                      EmployeeName = b.EmployeeID == null ? "" : context.Employees.Where(x => x.EmployeeID.Equals(b.EmployeeID.Value)).Select(x => x.EmployeeName).FirstOrDefault()
                                                                  },
                                                                  CSO = b.CSO,
                                                                  IsSBLCSecured = b.IsSBLCSecured.Value == true ? "Yes" : "No",
                                                                  IsHeavyEquipment = b.IsHeavyEquipment.Value == true ? "Yes" : "No",
                                                                  MaintenanceTypeID = b.MaintenanceTypeID.Value,
                                                                  MaintenanceTypeName = b.MaintenanceTypeID.Value != null ? context.ParameterSystems.Where(d => d.ParsysID.Equals(b.MaintenanceTypeID.Value)).Select(d => d.ParameterValue).FirstOrDefault() : "",
                                                                  AmountRollover = b.AmountRollover.Value,
                                                                  NextPrincipalDate = b.NextPrincipalDate.Value,
                                                                  NextInterestDate = b.NextInterestDate.Value,
                                                                  ApprovedMarginAsPerCM = b.ApprovedMarginAsPerCM.Value,
                                                                  InterestRateCodeID = b.InterestRateCodeID,
                                                                  InterestRateCodeName = b.InterestRateCodeID.Value != null ? context.ParameterSystems.Where(d => d.ParsysID.Equals(b.InterestRateCodeID.Value)).Select(d => d.ParameterValue).FirstOrDefault() : "",
                                                                  BaseRate = b.BaseRate.Value,
                                                                  AllInLP = b.AllInLP.Value,
                                                                  AllInFTPRate = b.AllInFTPRate.Value,
                                                                  AccountPreferencial = b.AccountPreferencial.Value,
                                                                  SpecialFTP = b.SpecialFTP.Value,
                                                                  SpecialRateMargin = b.SpecialRateMargin.Value,
                                                                  AllInrate = b.AllInrate.Value,
                                                                  ApprovalDOA = b.ApprovalDOA,
                                                                  ApprovedBy = b.ApprovedBy,
                                                                  Remarks = b.Remarks,
                                                                  IsAdhoc = b.IsAdhoc.Value == true ? "Yes" : "No",
                                                                  CSOName = b.CSOName,
                                                                  DetailLink = @"<a href='#' class='ShowDetail' onClick='$(this).ShowDetail(this)'>Detail</a>",
                                                                  LinkAddHoc = context.TransactionDocumentLoans.Where(f => f.TransactionID.Value.Equals(b.TransactionRolloverID)).Select(f => @"<a href='" + f.DocumentPath + "' target='_blank'>" + f.Filename + "</a>").FirstOrDefault()
                                                              }).ToList()
                              }).SingleOrDefault();
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return IsSuccess;
        }
        public bool AddDataRollover(Guid workflowInstanceID, ref LoanCheckerRollover output, long approverID, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    foreach (var item in output.LoanCheckerMakerRollover)
                    {
                        //if (item.IsChecked == true)
                        //{
                        var LoanData = context.LoanRollovers.Where(a => a.TransactionRolloverID.Equals(item.TransactionRolloverID)).ToList();
                        if (LoanData.Count > 0)
                        {
                            var updateLoan = context.LoanRollovers.Where(a => a.TransactionRolloverID.Equals(item.TransactionRolloverID)).SingleOrDefault();
                            updateLoan.TransactionRolloverID = item.TransactionRolloverID;
                            updateLoan.CIF = item.CIF;
                            updateLoan.LoanContractNo = item.LoanContractNo;
                            updateLoan.CurrencyID = item.Currency.ID;
                            updateLoan.SOLID = item.SOLID;
                            updateLoan.SchemeCode = item.SchemeCode;
                            updateLoan.DueDate = item.DueDate;
                            updateLoan.AmountDue = item.AmountDue;
                            updateLoan.LimitID = item.LimitID;
                            updateLoan.BizSegmentID = item.BizSegment.ID;
                            updateLoan.EmployeeID = item.Employee.EmployeeID;
                            updateLoan.CSO = item.CSO;
                            updateLoan.IsSBLCSecured = item.IsSBLCSecured == "Yes" ? true : false;
                            updateLoan.IsHeavyEquipment = item.IsHeavyEquipment == "Yes" ? true : false;
                            updateLoan.MaintenanceTypeID = item.MaintenanceTypeID;
                            updateLoan.AmountRollover = item.AmountRollover;
                            updateLoan.NextPrincipalDate = item.NextPrincipalDate;
                            updateLoan.NextInterestDate = item.NextInterestDate;
                            updateLoan.ApprovedMarginAsPerCM = item.ApprovedMarginAsPerCM;
                            updateLoan.InterestRateCodeID = item.InterestRateCodeID;
                            updateLoan.BaseRate = item.BaseRate;
                            updateLoan.AllInLP = item.AllInLP;
                            updateLoan.AllInFTPRate = item.AllInFTPRate;
                            updateLoan.AccountPreferencial = item.AccountPreferencial;
                            updateLoan.SpecialFTP = item.SpecialFTP;
                            updateLoan.SpecialRateMargin = item.SpecialRateMargin;
                            updateLoan.AllInrate = item.AllInrate;
                            updateLoan.ApprovalDOA = item.ApprovalDOA;
                            updateLoan.ApprovedBy = item.ApprovedBy;
                            updateLoan.Remarks = item.Remarks;
                            updateLoan.IsAdhoc = item.IsAdhoc == "Yes" ? true : false;
                            updateLoan.CSOName = item.CSOName;
                            updateLoan.CreateDate = DateTime.UtcNow;
                            updateLoan.CreateBy = currentUser.GetCurrentUser().LoginName;
                            updateLoan.UpdateBy = null;
                            updateLoan.Updatedate = null;
                            updateLoan.PeggingReviewDate = item.PeggingReviewDate;
                            updateLoan.IsCompleted = false;

                            context.SaveChanges();
                        }
                        else
                        {
                            //Add Loan Checker
                            DBS.Entity.LoanRollover DataLoan = new DBS.Entity.LoanRollover()
                            {
                                TransactionRolloverID = item.TransactionRolloverID,
                                CIF = item.CIF,
                                LoanContractNo = item.LoanContractNo,
                                CurrencyID = item.Currency.ID,
                                SOLID = item.SOLID,
                                SchemeCode = item.SchemeCode,
                                DueDate = item.DueDate,
                                AmountDue = item.AmountDue,
                                LimitID = item.LimitID,
                                BizSegmentID = item.BizSegment.ID,
                                EmployeeID = item.Employee.EmployeeID,
                                CSO = item.CSO,
                                IsSBLCSecured = item.IsSBLCSecured == "Yes" ? true : false,
                                IsHeavyEquipment = item.IsHeavyEquipment == "Yes" ? true : false,
                                MaintenanceTypeID = item.MaintenanceTypeID,
                                AmountRollover = item.AmountRollover,
                                NextPrincipalDate = item.NextPrincipalDate,
                                NextInterestDate = item.NextInterestDate,
                                ApprovedMarginAsPerCM = item.ApprovedMarginAsPerCM,
                                InterestRateCodeID = item.InterestRateCodeID,
                                BaseRate = item.BaseRate,
                                AllInLP = item.AllInLP,
                                AllInFTPRate = item.AllInFTPRate,
                                AccountPreferencial = item.AccountPreferencial,
                                SpecialFTP = item.SpecialFTP,
                                SpecialRateMargin = item.SpecialRateMargin,
                                AllInrate = item.AllInrate,
                                ApprovalDOA = item.ApprovalDOA,
                                ApprovedBy = item.ApprovedBy,
                                Remarks = item.Remarks,
                                IsAdhoc = item.IsAdhoc == "Yes" ? true : false,
                                CSOName = item.CSOName,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName,
                                UpdateBy = null,
                                Updatedate = null,
                                PeggingReviewDate = item.PeggingReviewDate,
                                IsCompleted = false
                            };
                            context.LoanRollovers.Add(DataLoan);
                            context.SaveChanges();
                        }
                        //}
                    }



                    foreach (var r in output.LoanCheckerMakerRollover)
                    {
                        TransactionRollover ro = (from t in context.TransactionRollovers
                                                  where t.TransactionRolloverID == r.TransactionRolloverID
                                                  select t).FirstOrDefault();
                        ro.Instruction = r.InstructionID;
                        //ro.IsSelected = r.IsChecked;

                        context.SaveChanges();
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string _message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(_message, raise);
                        }
                    }
                    throw raise;
                }

                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();

                }
            }
            return IsSuccess;
        }
        #endregion

        #region Loan Checker
        public bool GetDataRolloverLoanChecker(Guid workflowInstanceID, long approverID, ref DataCheckerRollover output, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                var TransactionID = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a.TransactionID).FirstOrDefault();
                var InstructionID = context.TransactionRollovers.Where(a => a.TransactionID.Equals(TransactionID)).Select(a => a.Instruction.HasValue == null ? a.Instruction.Value : 0).ToList();
                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new DataCheckerRollover
                          {
                              CheckerRollover = (from b in context.LoanRollovers
                                                 join c in context.TransactionRollovers on b.TransactionRolloverID equals c.TransactionRolloverID
                                                 //join d in context.ParameterSystems on b.MaintenanceTypeID equals d.ParsysID
                                                 //join e in context.ParameterSystems on b.InterestRateCodeID equals e.ParsysID
                                                 where c.TransactionID.Equals(a.TransactionID)
                                                 select new RolloverLoanCheckerModel
                                                 {
                                                     IsChecked = c.IsSelected.HasValue ? (c.IsSelected.Value == true ? true : false) : false,
                                                     TransactionRolloverID = b.TransactionRolloverID,
                                                     InstructionID = c.Instruction,
                                                     LoanRolloverID = b.LoanRolloverID,
                                                     SOLID = b.SOLID,
                                                     SchemeCode = b.SchemeCode,
                                                     CIF = b.CIF,
                                                     CustomerName = context.Customers.Where(f => f.CIF.Equals(b.CIF)).Select(f => f.CustomerName).FirstOrDefault(),
                                                     LoanContractNo = b.LoanContractNo,
                                                     DueDate = b.DueDate.Value,
                                                     ValueDate = DateTime.UtcNow,
                                                     Currency = new CurrencyModel()
                                                     {
                                                         ID = a.Currency.CurrencyID,
                                                         Code = a.Currency.CurrencyCode,
                                                         Description = a.Currency.CurrencyDescription,
                                                         LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                         LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                     },
                                                     AmountDue = b.AmountDue.Value,
                                                     LimitID = b.LimitID,
                                                     BizSegment = new BizSegmentModel()
                                                     {
                                                         ID = a.BizSegment.BizSegmentID,
                                                         Name = a.BizSegment.BizSegmentName,
                                                         Description = a.BizSegment.BizSegmentDescription,
                                                         LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                         LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                     },
                                                     Employee = new EmployeeModel()
                                                     {
                                                         EmployeeID = b.EmployeeID == null ? 1 : b.EmployeeID.Value,
                                                         EmployeeName = b.EmployeeID == null ? "" : context.Employees.Where(x => x.EmployeeID.Equals(b.EmployeeID.Value)).Select(x => x.EmployeeName).FirstOrDefault()
                                                     },
                                                     CSO = b.CSO,
                                                     IsSBLCSecured = b.IsSBLCSecured.Value == true ? "Yes" : "No",
                                                     IsHeavyEquipment = b.IsHeavyEquipment.Value == true ? "Yes" : "No",
                                                     MaintenanceTypeID = b.MaintenanceTypeID.Value,
                                                     MaintenanceTypeName = context.ParameterSystems.Where(d => d.ParsysID.Equals(b.MaintenanceTypeID.Value)).Select(d => d.ParameterValue).FirstOrDefault(),
                                                     AmountRollover = b.AmountRollover.Value,
                                                     NextPrincipalDate = b.NextPrincipalDate.Value,
                                                     NextInterestDate = b.NextInterestDate.Value,
                                                     ApprovedMarginAsPerCM = b.ApprovedMarginAsPerCM.Value,
                                                     InterestRateCodeID = b.InterestRateCodeID,
                                                     InterestRateCodeName = context.ParameterSystems.Where(d => d.ParsysID.Equals(b.InterestRateCodeID.Value)).Select(d => d.ParameterValue).FirstOrDefault(),
                                                     BaseRate = b.BaseRate.Value,
                                                     AllInLP = b.AllInLP.Value,
                                                     AllInFTPRate = b.AllInFTPRate.Value,
                                                     AccountPreferencial = b.AccountPreferencial.Value,
                                                     SpecialFTP = b.SpecialFTP.Value,
                                                     SpecialRateMargin = b.SpecialRateMargin.Value,
                                                     AllInrate = b.AllInrate.Value,
                                                     ApprovalDOA = b.ApprovalDOA,
                                                     ApprovedBy = b.ApprovedBy,
                                                     Remarks = b.Remarks,
                                                     IsAdhoc = b.IsAdhoc.Value == true ? "Yes" : "No",
                                                     CSOName = b.CSOName,
                                                     PeggingReviewDate = b.PeggingReviewDate,
                                                     DetailLink = @"<a href='#' class='ShowDetail' onClick='$(this).ShowDetail(this)'>Detail</a>",
                                                     LinkAddHoc = context.TransactionDocumentLoans.Where(f => f.TransactionID.Value.Equals(b.TransactionRolloverID)).Select(f => @"<a href='" + f.DocumentPath + "' target='_blank'>" + f.Filename + "</a>").FirstOrDefault()
                                                 }).ToList()
                          }).SingleOrDefault();
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return IsSuccess;
        }
        public bool AddDataRolloverChecker(Guid workflowInstanceID, ref DataCheckerRollover output, long approverID, ref string message)
        {
            bool IsSuccess = false;

            using (DBSEntities context_ts = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        foreach (var item in output.CheckerRollover)
                        {
                            //if (item.IsChecked == true)
                            //{
                            var updateIsCompleted = context_ts.LoanRollovers.Where(a => a.LoanRolloverID.Equals(item.LoanRolloverID)).SingleOrDefault();
                            updateIsCompleted.IsCompleted = true;
                            //}
                        }
                        context_ts.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        message = ex.InnerException.Message;
                    }
                    ts.Complete();
                    IsSuccess = true;
                }
            }
            return IsSuccess;
        }
        //bagian loan checker rollover
        public bool AddDataLoanRollover(Guid workflowInstanceID, long approverID, ref DataCheckerRollover output, ref string message)
        {
            var transactionID = context.Transactions
                  .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                  .Select(a => a.TransactionID)
                  .SingleOrDefault();
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    context.TransactionCheckerDatas.Add(new TransactionCheckerData()
                    {
                        ApproverID = approverID,
                        TransactionID = transactionID,
                        CreatedBy = currentUser.GetCurrentUser().LoginName,
                        CreatedDate = DateTime.UtcNow,
                        IsCallbackRequired = false,
                        IsDormantAccount = false,
                        IsFreezeAccount = false,
                        IsLOIAvailable = false,
                        IsSignatureVerified = false,
                        IsStatementLetterCopy = false,
                        IsSyndication = false,
                        LLDID = null,
                        Others = null,
                        UpdateBy = null,
                        UpdateDate = null,

                    });

                    context.SaveChanges();

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();

                }
            }
            return IsSuccess;
        }
        //end
        //Hanya Untuk Detail PopUp
        #endregion

        #endregion

        #region Tak Terpakai
        public bool AddDataIM(LoanCheckerDetailLoanModel output, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    foreach (var item in output.IM)
                    {
                        DBS.Entity.LoanRollover Data = new DBS.Entity.LoanRollover()
                        {

                            SOLID = item.SolID,
                            SchemeCode = item.SchemeCode,
                            CIF = item.CIF,

                            LoanContractNo = item.LoanContractNo,
                            DueDate = item.DueDate,
                            AmountDue = item.AmountDue,
                            LimitID = item.LimitID,
                            BizSegmentID = item.BizSegment.ID,
                            EmployeeID = item.Employee.EmployeeID,
                            CSO = item.CSO,
                            IsSBLCSecured = item.IsSBLCSecured == "Yes" ? true : false,
                            IsHeavyEquipment = item.IsHeavyEquipment == "Yes" ? true : false,
                            MaintenanceTypeID = item.MaintenanceTypeID,
                            AmountRollover = item.AmountRollover,
                            NextPrincipalDate = item.NextPrincipalDate,
                            NextInterestDate = item.NextInterestDate,
                            ApprovedMarginAsPerCM = item.ApprovedMarginAsPerCM,
                            InterestRateCodeID = item.InterestRateCodeID,
                            BaseRate = item.BaseRate,
                            AllInFTPRate = item.AllinFTPRate,
                            AccountPreferencial = item.AccountPreferencial,
                            SpecialFTP = item.SpecialFTP,
                            SpecialRateMargin = item.SpecialRate,
                            AllInrate = item.AllinRate,
                            ApprovalDOA = item.ApprovalDOA,
                            ApprovedBy = item.ApprovedBy,
                            // Remarks = item.CSORemarks,
                            //LoanRemarks = a.LoanRemarks,
                            //IsAdhoc = item.IsAdhoc,

                        };
                        context.LoanRollovers.Add(Data);
                        context.SaveChanges();
                        ts.Complete();

                        IsSuccess = true;
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();

                }
            }
            return IsSuccess;
        }
        public bool AddDataSettlement(LoanCheckerDetailLoanModel output, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    foreach (var item in output.LoanRollover)
                    {
                        DBS.Entity.LoanRollover Data = new DBS.Entity.LoanRollover()
                        {
                            LoanRolloverID = item.CSORolloverID,
                            SOLID = item.SOLID,
                            SchemeCode = item.SchemeCode,
                            CIF = item.CIF,

                            LoanContractNo = item.LoanContractNo,
                            DueDate = item.DueDate,
                            AmountDue = item.AmountDue,
                            LimitID = item.LimitID,
                            BizSegmentID = item.BizSegment.ID,
                            EmployeeID = item.RM.EmployeeID,
                            CSO = item.CSO,
                            IsSBLCSecured = item.IsSBLCSecured,
                            IsHeavyEquipment = item.IsHeavyEquipment,
                            MaintenanceTypeID = item.MaintenanceTypeID,
                            AmountRollover = item.AmountRollover,
                            NextPrincipalDate = item.NextPrincipalDate,
                            NextInterestDate = item.NextInterestDate,
                            ApprovedMarginAsPerCM = item.ApprovedMarginAsPerCM,
                            InterestRateCodeID = item.InterestRateCodeID,
                            BaseRate = item.BaseRate,
                            AllInFTPRate = item.AllInFTPRate,
                            AccountPreferencial = item.AccountPreferencial,
                            SpecialFTP = item.SpecialFTP,
                            SpecialRateMargin = item.SpecialRateMargin,
                            AllInrate = item.AllInrate,
                            ApprovalDOA = item.ApprovalDOA,
                            ApprovedBy = item.ApprovedBy,
                            Remarks = item.CSORemarks,
                            //LoanRemarks = a.LoanRemarks,
                            IsAdhoc = item.IsAdhoc,

                        };
                        context.LoanRollovers.Add(Data);
                        context.SaveChanges();
                        ts.Complete();

                        IsSuccess = true;
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();

                }
            }
            return IsSuccess;
        }
        public bool GetScheduledSettlementDetails(Guid workflowInstanceID, long csoRolloverID, ref LoanCheckerRolloverDetails output, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                var TransactionRolloverID = context.CSORollovers.Where(a => a.CSORolloverID.Equals(csoRolloverID)).Select(a => a.TransactionRolloverID).SingleOrDefault();
                var InstructionID = context.TransactionRollovers.Where(a => a.TransactionRolloverID.Equals(TransactionRolloverID)).Select(a => a.Instruction.Value).ToList();

                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new LoanCheckerRolloverDetails
                          {

                              LoanCheckerRollover = (from b in context.CSORollovers
                                                     join c in context.TransactionRollovers
                                                     on b.TransactionRolloverID equals c.TransactionRolloverID
                                                     where b.CSORolloverID.Equals(csoRolloverID)
                                                     select new LoanCheckerMakerModel
                                                     {
                                                         IsChecked = c.IsSelected.HasValue ? (c.IsSelected.Value == true ? true : false) : false,
                                                         CSORollOverID = b.CSORolloverID,
                                                         SOLID = b.SOLID,
                                                         SchemeCode = b.SchemeCode,
                                                         CIF = b.CIF,
                                                         CustomerName = context.Customers.Where(d => d.CIF.Equals(b.CIF)).Select(d => d.CustomerName).FirstOrDefault(),
                                                         LoanContractNo = b.LoanContractNo,
                                                         DueDate = b.DueDate.Value,
                                                         Currency = new CurrencyModel()
                                                         {
                                                             ID = a.Currency.CurrencyID,
                                                             Code = a.Currency.CurrencyCode,
                                                             Description = a.Currency.CurrencyDescription,
                                                             LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                             LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                         },
                                                         AmountDue = b.AmountDue.Value,
                                                         LimitID = b.LimitID,
                                                         BizSegment = new BizSegmentModel()
                                                         {
                                                             ID = a.BizSegment.BizSegmentID,
                                                             Name = a.BizSegment.BizSegmentName,
                                                             Description = a.BizSegment.BizSegmentDescription,
                                                             LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                             LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                         },
                                                         CSO = b.CSO,
                                                         IsSBLCSecured = b.IsSBLCSecured.Value == true ? "Yes" : "No",
                                                         IsHeavyEquipment = b.IsHeavyEquipment.Value == true ? "Yes" : "No",
                                                         MaintenanceTypeID = b.MaintenanceTypeID.Value,
                                                         AmountRollover = b.AmountRollover.Value,
                                                         NextPrincipalDate = b.NextPrincipalDate.Value,
                                                         NextInterestDate = b.NextInterestDate.Value,
                                                         ApprovedMarginAsPerCM = b.ApprovedMarginAsPerCM.Value,
                                                         InterestRateCodeID = b.InterestRateCodeID,
                                                         BaseRate = b.BaseRate.Value,
                                                         AllInLP = b.AllInLP.Value,
                                                         AllInFTPRate = b.AllInFTPRate.Value,
                                                         AccountPreferencial = b.AccountPreferencial.Value,
                                                         SpecialFTP = b.SpecialFTP.Value,
                                                         SpecialRateMargin = b.SpecialRateMargin.Value,
                                                         AllInrate = b.AllInrate.Value,
                                                         ApprovalDOA = b.ApprovalDOA,
                                                         ApprovedBy = b.ApprovedBy,
                                                         Remarks = b.Remarks,
                                                         IsAdhoc = b.IsAdhoc.Value == true ? "Yes" : "No",
                                                         CSOName = b.CSOName,
                                                         DetailLink = @"<a href='#' class='ShowDetail' onClick='$(this).ShowDetail(this)'>Detail</a>",
                                                         Documents = (from x in context.TransactionDocuments
                                                                      where InstructionID.Contains(x.TransactionID) && x.IsDeleted.Equals(false)
                                                                      select new TransactionDocumentModel()
                                                                      {
                                                                          ID = x.TransactionDocumentID,
                                                                          Type = new DocumentTypeModel()
                                                                          {
                                                                              ID = x.DocumentType.DocTypeID,
                                                                              Name = x.DocumentType.DocTypeName,
                                                                              Description = x.DocumentType.DocTypeDescription,
                                                                              LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                              LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                          },
                                                                          Purpose = new DocumentPurposeModel()
                                                                          {
                                                                              ID = x.DocumentPurpose.PurposeID,
                                                                              Name = x.DocumentPurpose.PurposeName,
                                                                              Description = x.DocumentPurpose.PurposeDescription,
                                                                              LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                              LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                                          },
                                                                          FileName = x.Filename,
                                                                          DocumentPath = x.DocumentPath,
                                                                          LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                          LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                                                      }).ToList()
                                                     }).FirstOrDefault()
                          }).SingleOrDefault();
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return IsSuccess;
        }
        public bool GetDataRolloverDetail(Guid workflowInstanceID, long LoanRolloverID, ref DataCheckerRolloverDetail output, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                var TransactionRolloverID = context.LoanRollovers.Where(a => a.LoanRolloverID.Equals(LoanRolloverID)).Select(a => a.TransactionRolloverID).SingleOrDefault();
                var InstructionID = context.TransactionRollovers.Where(a => a.TransactionRolloverID.Equals(TransactionRolloverID)).Select(a => a.Instruction.Value).ToList();

                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new DataCheckerRolloverDetail
                          {

                              LoanCheckerRollover = (from b in context.LoanRollovers
                                                     join c in context.TransactionRollovers
                                                     on b.TransactionRolloverID equals c.TransactionRolloverID
                                                     where b.LoanRolloverID.Equals(LoanRolloverID)
                                                     select new RolloverLoanCheckerModel
                                                     {
                                                         IsChecked = c.IsSelected.HasValue ? (c.IsSelected.Value == true ? true : false) : false,
                                                         LoanRolloverID = b.LoanRolloverID,
                                                         SOLID = b.SOLID,
                                                         SchemeCode = b.SchemeCode,
                                                         CIF = b.CIF,
                                                         CustomerName = context.Customers.Where(d => d.CIF.Equals(b.CIF)).Select(d => d.CustomerName).FirstOrDefault(),
                                                         LoanContractNo = b.LoanContractNo,
                                                         DueDate = b.DueDate.Value,
                                                         Currency = new CurrencyModel()
                                                         {
                                                             ID = a.Currency.CurrencyID,
                                                             Code = a.Currency.CurrencyCode,
                                                             Description = a.Currency.CurrencyDescription,
                                                             LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                             LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                         },
                                                         AmountDue = b.AmountDue.Value,
                                                         LimitID = b.LimitID,
                                                         BizSegment = new BizSegmentModel()
                                                         {
                                                             ID = a.BizSegment.BizSegmentID,
                                                             Name = a.BizSegment.BizSegmentName,
                                                             Description = a.BizSegment.BizSegmentDescription,
                                                             LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                             LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                         },
                                                         Employee = new EmployeeModel()
                                                         {
                                                             EmployeeID = b.EmployeeID == null ? 1 : b.EmployeeID.Value,
                                                             EmployeeName = b.EmployeeID == null ? "" : context.Employees.Where(x => x.EmployeeID.Equals(b.EmployeeID.Value)).Select(x => x.EmployeeName).FirstOrDefault()
                                                         },
                                                         CSO = b.CSO,
                                                         IsSBLCSecured = b.IsSBLCSecured.Value == true ? "Yes" : "No",
                                                         IsHeavyEquipment = b.IsHeavyEquipment.Value == true ? "Yes" : "No",
                                                         MaintenanceTypeID = b.MaintenanceTypeID.Value,
                                                         AmountRollover = b.AmountRollover.Value,
                                                         NextPrincipalDate = b.NextPrincipalDate.Value,
                                                         NextInterestDate = b.NextInterestDate.Value,
                                                         ApprovedMarginAsPerCM = b.ApprovedMarginAsPerCM.Value,
                                                         InterestRateCodeID = b.InterestRateCodeID,
                                                         BaseRate = b.BaseRate.Value,
                                                         AllInLP = b.AllInLP.Value,
                                                         AllInFTPRate = b.AllInFTPRate.Value,
                                                         AccountPreferencial = b.AccountPreferencial.Value,
                                                         SpecialFTP = b.SpecialFTP.Value,
                                                         SpecialRateMargin = b.SpecialRateMargin.Value,
                                                         AllInrate = b.AllInrate.Value,
                                                         ApprovalDOA = b.ApprovalDOA,
                                                         ApprovedBy = b.ApprovedBy,
                                                         CSORemarks = b.Remarks,
                                                         IsAdhoc = b.IsAdhoc.Value == true ? "Yes" : "No",
                                                         CSOName = b.CSOName,
                                                         DetailLink = @"<a href='#' class='ShowDetail' onClick='$(this).ShowDetail(this)'>Detail</a>",
                                                         Documents = (from x in context.TransactionDocuments
                                                                      where InstructionID.Contains(x.TransactionID) && x.IsDeleted.Equals(false)
                                                                      select new TransactionDocumentModel()
                                                                      {
                                                                          ID = x.TransactionDocumentID,
                                                                          Type = new DocumentTypeModel()
                                                                          {
                                                                              ID = x.DocumentType.DocTypeID,
                                                                              Name = x.DocumentType.DocTypeName,
                                                                              Description = x.DocumentType.DocTypeDescription,
                                                                              LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                              LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                          },
                                                                          Purpose = new DocumentPurposeModel()
                                                                          {
                                                                              ID = x.DocumentPurpose.PurposeID,
                                                                              Name = x.DocumentPurpose.PurposeName,
                                                                              Description = x.DocumentPurpose.PurposeDescription,
                                                                              LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                              LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                                          },
                                                                          FileName = x.Filename,
                                                                          DocumentPath = x.DocumentPath,
                                                                          LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                          LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                                                      }).ToList()
                                                     }).FirstOrDefault()
                          }).SingleOrDefault();
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return IsSuccess;
        }
        public bool GetDataImDetail(Guid workflowInstanceID, long CSOInterestMaintenanceID, ref DataMakerIMDetail output, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                var TransactionInterestMaintenanceID = context.CSOInterestMaintenances.Where(a => a.CSOInterestMaintenanceID.Equals(CSOInterestMaintenanceID)).Select(a => a.CSOInterestMaintenanceID).SingleOrDefault();
                var InstructionID = context.TransactionInterestMaintenances.Where(a => a.TransactionInterestMaintenanceID.Equals(TransactionInterestMaintenanceID)).Select(a => a.InstructionID.Value).ToList();

                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new DataMakerIMDetail
                          {

                              LoanCheckerRollover = (from b in context.CSOInterestMaintenances
                                                     join c in context.TransactionInterestMaintenances
                                                     on b.TransactionInterestMaintenanceID equals c.TransactionInterestMaintenanceID
                                                     where b.CSOInterestMaintenanceID.Equals(CSOInterestMaintenanceID)
                                                     select new TransactionInterestMaintenanceLoanModel
                                                     {
                                                         IsChecked = c.IsSelected.HasValue ? (c.IsSelected.Value == true ? true : false) : false,
                                                         CSOInterestMaintenanceID = b.CSOInterestMaintenanceID,
                                                         SolID = b.SolID,
                                                         SchemeCode = b.SchemeCode,
                                                         CIF = b.CIF,
                                                         CustomerName = context.Customers.Where(d => d.CIF.Equals(b.CIF)).Select(d => d.CustomerName).FirstOrDefault(),
                                                         LoanContractNo = b.LoanContractNo,
                                                         DueDate = b.DueDate.Value,
                                                         Currency = new CurrencyModel()
                                                         {
                                                             ID = a.Currency.CurrencyID,
                                                             Code = a.Currency.CurrencyCode,
                                                             Description = a.Currency.CurrencyDescription,
                                                             LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                             LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                         },
                                                         AmountDue = b.AmountRollover,
                                                         //LimitID = b.LimitID,
                                                         BizSegment = new BizSegmentModel()
                                                         {
                                                             ID = a.BizSegment.BizSegmentID,
                                                             Name = a.BizSegment.BizSegmentName,
                                                             Description = a.BizSegment.BizSegmentDescription,
                                                             LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                             LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                         },
                                                         Employee = new EmployeeModel()
                                                         {
                                                             EmployeeID = b.EmployeeID == null ? 1 : b.EmployeeID.Value,
                                                             EmployeeName = b.EmployeeID == null ? "" : context.Employees.Where(x => x.EmployeeID.Equals(b.EmployeeID.Value)).Select(x => x.EmployeeName).FirstOrDefault()
                                                         },
                                                         CSO = b.CSO,
                                                         IsSBLCSecured = b.IsSBLCSecured.Value ? "Yes" : "No",
                                                         IsHeavyEquipment = b.IsHeavyEquipment.Value ? "Yes" : "No",
                                                         MaintenanceTypeID = b.MaintenanceTypeID.Value,
                                                         AmountRollover = b.AmountRollover.Value,
                                                         NextPrincipalDate = b.NextPrincipalDate.Value,
                                                         NextInterestDate = b.NextInterestDate.Value,
                                                         ApprovedMarginAsPerCM = b.ApprovedMarginAsPerCM.Value,
                                                         InterestRateCodeID = b.InterestRateCodeID,
                                                         BaseRate = b.BaseRate.Value,
                                                         AllinFTPRate = b.AllinFTPRate.Value,
                                                         AccountPreferencial = b.AccountPreferencial.Value,
                                                         SpecialFTP = b.SpecialFTP.Value,
                                                         SpecialRate = b.SpecialRate.Value,
                                                         AllinRate = b.AllinRate.Value,
                                                         ApprovalDOA = b.ApprovalDOA,
                                                         ApprovedBy = b.ApprovedBy,
                                                         Remarks = b.Remarks,
                                                         IsAdhoc = b.IsAdhoc,
                                                         CSOName = b.CSOName,
                                                         DetailLink = @"<a href='#' class='ShowDetail' onClick='$(this).ShowDetail(this)'>Detail</a>",
                                                         Documents = (from x in context.TransactionDocuments
                                                                      where InstructionID.Contains(x.TransactionID) && x.IsDeleted.Equals(false)
                                                                      select new TransactionDocumentModel()
                                                                      {
                                                                          ID = x.TransactionDocumentID,
                                                                          Type = new DocumentTypeModel()
                                                                          {
                                                                              ID = x.DocumentType.DocTypeID,
                                                                              Name = x.DocumentType.DocTypeName,
                                                                              Description = x.DocumentType.DocTypeDescription,
                                                                              LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                              LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                          },
                                                                          Purpose = new DocumentPurposeModel()
                                                                          {
                                                                              ID = x.DocumentPurpose.PurposeID,
                                                                              Name = x.DocumentPurpose.PurposeName,
                                                                              Description = x.DocumentPurpose.PurposeDescription,
                                                                              LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                              LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                                          },
                                                                          FileName = x.Filename,
                                                                          DocumentPath = x.DocumentPath,
                                                                          LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                          LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                                                      }).ToList()
                                                     }).FirstOrDefault()
                          }).SingleOrDefault();



                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }


            return IsSuccess;
        }
        public bool GetLoanDetails(Guid instanceID, ref TransactionCSOModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();
                // get main transaction data

                transaction = (from a in context.Transactions
                               where a.WorkflowInstanceID.Value.Equals(instanceID)
                               select new TransactionCSOModel
                               {
                                   ID = a.TransactionID,
                                   WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                   ApplicationID = a.ApplicationID,
                                   ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                                   Product = new ProductModel()
                                   {
                                       ID = a.Product.ProductID,
                                       Code = a.Product.ProductCode,
                                       Name = a.Product.ProductName,
                                       WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                       Workflow = a.Product.ProductWorkflow.WorkflowName,
                                       LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                       LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                   },
                                   Channel = new ChannelModel()
                                   {
                                       ID = a.Channel.ChannelID,
                                       Name = a.Channel.ChannelName,
                                       LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                       LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                   },
                                   Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                                   {
                                       ID = x.TransactionDocumentID,
                                       Type = new DocumentTypeModel()
                                       {
                                           ID = x.DocumentType.DocTypeID,
                                           Name = x.DocumentType.DocTypeName,
                                           Description = x.DocumentType.DocTypeDescription,
                                           LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                           LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                       },
                                       Purpose = new DocumentPurposeModel()
                                       {
                                           ID = x.DocumentPurpose.PurposeID,
                                           Name = x.DocumentPurpose.PurposeName,
                                           Description = x.DocumentPurpose.PurposeDescription,
                                           LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                           LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                       },
                                       FileName = x.Filename,
                                       DocumentPath = x.DocumentPath,
                                       LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                       LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                   }).ToList(),
                                   CreateDate = a.CreateDate,
                                   LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                   LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                               }).SingleOrDefault();

                if (transaction != null)
                {
                    //datatransaction.IsOtherBeneBank = datatransaction.Bank.Code != "999" ? false : true;
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(instanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    // changed, chandra : 2015.03.23
                    if (customerRepo.GetCustomerByTransaction(transaction.ID, ref customer, ref message))
                    {
                        transaction.Customer = customer;
                    }
                    else
                    {
                        if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                        {
                            transaction.Customer = customer;
                        }
                    }
                    //output.Customer = customer;
                    customerRepo.Dispose();
                    customer = null;
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        #endregion

        #endregion
        #endregion

        #region dani dp
        public bool GetTransactionLoanMakerSettlementUnscheduledDetails(Guid workflowInstanceID, ref LoanMakerSettlementUnscheduledModel output, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                context.Transactions.AsNoTracking();
                LoanMakerSettlementUnscheduledModel datatransaction = (from a in context.Transactions
                                                                       where a.WorkflowInstanceID.Value == workflowInstanceID
                                                                       select new LoanMakerSettlementUnscheduledModel
                                                                       {
                                                                           ID = a.TransactionID,
                                                                           WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                                                           ApplicationID = a.ApplicationID,
                                                                           ApproverID = a.ApproverID.Value,// (a.ApproverID.HasValue && a.ApproverID.Value > 0) ? a.ApproverID : null,                                                                         
                                                                           CreateDate = a.CreateDate,
                                                                           LastModifiedBy = a.UpdateDate.HasValue ? a.UpdateBy : a.CreateBy,
                                                                           LastModifiedDate = a.UpdateDate.HasValue ? a.UpdateDate.Value : a.CreateDate,
                                                                           Product = new ProductModel()
                                                                           {
                                                                               ID = a.Product.ProductID,
                                                                               Code = a.Product.ProductCode,
                                                                               Name = a.Product.ProductName,
                                                                               WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                                                               Workflow = a.Product.ProductWorkflow.WorkflowName,
                                                                               LastModifiedBy = a.Product.UpdateDate.HasValue ? a.Product.UpdateBy : a.Product.CreateBy,
                                                                               LastModifiedDate = a.Product.UpdateDate.HasValue ? a.Product.UpdateDate.Value : a.Product.CreateDate
                                                                           },
                                                                           Currency = new CurrencyModel()
                                                                           {
                                                                               ID = a.Currency.CurrencyID,
                                                                               Code = a.Currency.CurrencyCode,
                                                                               Description = a.Currency.CurrencyDescription,
                                                                               LastModifiedBy = a.Currency.UpdateDate.HasValue ? a.Currency.UpdateBy : a.Currency.CreateBy,
                                                                               LastModifiedDate = a.Currency.UpdateDate.HasValue ? a.Currency.UpdateDate.Value : a.Currency.CreateDate
                                                                           },
                                                                           Amount = a.Amount,
                                                                           Channel = new ChannelModel()
                                                                           {
                                                                               ID = a.Channel.ChannelID,
                                                                               Name = a.Channel.ChannelName,
                                                                               LastModifiedBy = a.Channel.UpdateDate.HasValue ? a.Channel.UpdateBy : a.Channel.CreateBy,
                                                                               LastModifiedDate = a.Channel.UpdateDate.HasValue ? a.Channel.UpdateDate.Value : a.Channel.CreateDate
                                                                           },
                                                                           SourceID = a.SourceID.HasValue? a.SourceID.Value : 0,
                                                                           BizSegment = new BizSegmentModel()
                                                                           {
                                                                               ID = a.BizSegment.BizSegmentID,
                                                                               Name = a.BizSegment.BizSegmentName,
                                                                               Description = a.BizSegment.BizSegmentDescription,
                                                                               LastModifiedBy = a.BizSegment.UpdateDate.HasValue ? a.BizSegment.UpdateBy : a.BizSegment.CreateBy,
                                                                               LastModifiedDate = a.BizSegment.UpdateDate.HasValue ? a.BizSegment.UpdateDate.Value : a.BizSegment.CreateDate
                                                                           },
                                                                           Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                                                                           {
                                                                               ID = x.TransactionDocumentID,
                                                                               Type = new DocumentTypeModel()
                                                                               {
                                                                                   ID = x.DocumentType.DocTypeID,
                                                                                   Name = x.DocumentType.DocTypeName,
                                                                                   Description = x.DocumentType.DocTypeDescription,
                                                                                   LastModifiedBy = x.DocumentType.UpdateDate.HasValue ? x.DocumentType.UpdateBy : x.DocumentType.CreateBy,
                                                                                   LastModifiedDate = x.DocumentType.UpdateDate.HasValue ? x.DocumentType.UpdateDate.Value : x.DocumentType.CreateDate
                                                                               },
                                                                               Purpose = new DocumentPurposeModel()
                                                                               {
                                                                                   ID = x.DocumentPurpose.PurposeID,
                                                                                   Name = x.DocumentPurpose.PurposeName,
                                                                                   Description = x.DocumentPurpose.PurposeDescription,
                                                                                   LastModifiedBy = x.DocumentPurpose.UpdateDate.HasValue ? x.DocumentPurpose.UpdateBy : x.DocumentPurpose.CreateBy,
                                                                                   LastModifiedDate = x.DocumentPurpose.UpdateDate.HasValue ? x.DocumentPurpose.UpdateDate.Value : x.DocumentPurpose.CreateDate
                                                                               },
                                                                               FileName = x.Filename,
                                                                               DocumentPath = x.DocumentPath,
                                                                               LastModifiedBy = x.UpdateDate.HasValue ? x.UpdateBy : x.CreateBy,
                                                                               LastModifiedDate = x.UpdateDate.HasValue ? x.UpdateDate.Value : x.CreateDate,
                                                                               IsDormant = x.IsDormant.HasValue ? x.IsDormant : false
                                                                           }).ToList(),
                                                                           LoanContractNo = a.LoanContractNo == null ? string.Empty : a.LoanContractNo,
                                                                           ValueDate = a.ValueDate == null ? null : a.ValueDate,
                                                                           LoanTransID = a.LoanTransID == null ? string.Empty : a.LoanTransID,
                                                                           IsTopUrgent = a.IsTopUrgent
                                                                       }).SingleOrDefault();


                if (datatransaction != null)
                {
                    //string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    if (customerRepo.GetCustomerByTransaction(datatransaction.ID, ref customer, ref message))
                    {
                        datatransaction.Customer = customer;
                    }
                    customerRepo.Dispose();
                    customer = null;
                    output = datatransaction;
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return IsSuccess;
        }
        public bool AddTransactionLoanMakerSettlementUnscheduledDetails(LoanMakerSettlementUnscheduledDetailModel data, ref string message)
        {
            bool IsSuccess = false;
            using (DBSEntities context_ts = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        //var transactionID = context.Transactions
                        //.Where(a => a.WorkflowInstanceID.Value.Equals(data.Transaction.WorkflowInstanceID.Value))
                        //.Select(a => a.TransactionID)
                        //.SingleOrDefault();

                        context_ts.TransactionLoans.Add(new TransactionLoan()
                        {
                            ApproverID = data.Transaction.ApproverID.HasValue ? data.Transaction.ApproverID : null,
                            CreateBy = currentUser.GetCurrentUser().LoginName,
                            CreateDate = DateTime.UtcNow,
                            LoanContractNo = data.Transaction.LoanContractNo,
                            WorkflowInstanceID = data.Transaction.WorkflowInstanceID.Value,
                            ValueDate = data.Transaction.ValueDate.Value,
                            TransactionID = data.Transaction.ID //transactionID
                        });
                        context_ts.SaveChanges();
                        ts.Complete();
                        IsSuccess = true;
                    }
                    catch (Exception ex)
                    {
                        ts.Dispose();
                        message = ex.Message;
                    }
                }
            }
            return IsSuccess;
        }
        public bool GetCheckerUnsettlement(Guid workflowInstanceID, long approverID, ref LoanMakerSettlementUnscheduledDetailModel output, ref string message)
        {
            bool IsSuccess = false;

            var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

            try
            {

                {
                    output.Verify = context.TransactionColumns
                        //.Where(x => x.IsPPUCheckerLoan.Equals(true) && x.IsDeleted.Equals(false))
                        .Where(x => x.IsDeleted.Equals(false) && x.IsLoanCheckerDisbursment == (true))
                        .Select(x => new VerifyLoanModel()
                        {
                            ID = x.TransactionColumnID,
                            Name = x.ColumnName,
                            IsVerified = false
                        })
                        .ToList();
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        #endregion

        #region basri
        //Basri 28-10-2015
        public bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineModel> timelines, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                //Remark Rizki 2015-04-21, ganti dari View ke SP
                /* timelines = (from a in context.V_NintexTaskTimeline.AsNoTracking()
                              where a.WorkflowInstanceID.Value.Equals(workflowInstanceID) && !a.Outcome.Equals("Cancelled")
                              orderby a.ActivityTime
                              select new TransactionTimelineModel
                              {
                                  ApproverID = a.WorkflowApproverID,
                                  Activity = a.ActivityTitle,
                                  Time = a.ActivityTime,
                                  UserOrGroup = a.UserOrGroup,
                                  IsGroup = a.IsSPGroup.Value,
                                  Outcome = a.Outcome,
                                  DisplayName = a.DisplayName,
                                  Comment = a.Comment
                              }).ToList(); */

                timelines = (from a in context.SP_GetNintexTaskTimeline(workflowInstanceID)
                             select new TransactionTimelineModel
                             {
                                 ApproverID = a.WorkflowApproverID,
                                 Activity = a.ActivityTitle,
                                 Time = a.ActivityTime,
                                 UserOrGroup = a.UserOrGroup,
                                 IsGroup = a.IsSPGroup.Value,
                                 Outcome = a.Outcome,
                                 DisplayName = a.DisplayName,
                                 Comment = a.Comment
                             }).ToList();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionCheckerDetailsScheduledSettlement(Guid workflowInstanceID, long approverID, ref TransactionCheckerLoanScheduledSettlementModel output, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                var TransactionID = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a.TransactionID).FirstOrDefault();
                var InstructionID = context.TransactionRollovers.Where(a => a.TransactionID.Equals(TransactionID)).Select(a => a.Instruction.Value).ToList();

                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new TransactionCheckerLoanScheduledSettlementModel
                          {

                              ScheduledSettlementLoan = (from b in context.CSORollovers
                                                         join c in context.TransactionRollovers
                                                         on b.TransactionRolloverID equals c.TransactionRolloverID
                                                         where c.TransactionID.Equals(a.TransactionID)
                                                         select new ScheduledSettlementModel
                                                         {
                                                             IsChecked = c.IsSelected.HasValue ? (c.IsSelected.Value == true ? true : false) : false,
                                                             TransactionRolloverID = b.TransactionRolloverID,
                                                             CSORollOverID = b.CSORolloverID,
                                                             SOLID = b.SOLID,
                                                             SchemeCode = b.SchemeCode,
                                                             CIF = b.CIF,
                                                             CustomerName = context.Customers.Where(d => d.CIF.Equals(b.CIF)).Select(d => d.CustomerName).FirstOrDefault(),
                                                             LoanContractNo = b.LoanContractNo,
                                                             DueDate = b.DueDate.Value,
                                                             Currency = new CurrencyModel()
                                                             {
                                                                 ID = a.Currency.CurrencyID,
                                                                 Code = a.Currency.CurrencyCode,
                                                                 Description = a.Currency.CurrencyDescription,
                                                                 LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                                 LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                             },
                                                             AmountDue = b.AmountDue.Value,
                                                             LimitID = b.LimitID,
                                                             BizSegment = new BizSegmentModel()
                                                             {
                                                                 ID = a.BizSegment.BizSegmentID,
                                                                 Name = a.BizSegment.BizSegmentName,
                                                                 Description = a.BizSegment.BizSegmentDescription,
                                                                 LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                                 LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                             },
                                                             RM = new EmployeeModel()
                                                             {
                                                                 EmployeeID = b.EmployeeID == null ? 1 : b.EmployeeID.Value,
                                                                 EmployeeName = b.EmployeeID == null ? "" : context.Employees.Where(x => x.EmployeeID.Equals(b.EmployeeID.Value)).Select(x => x.EmployeeName).FirstOrDefault()
                                                             },
                                                             CSO = b.CSO,
                                                             IsSBLCSecured = b.IsSBLCSecured == null ? false : b.IsSBLCSecured.Value,
                                                             IsHeavyEquipment = b.IsHeavyEquipment == null ? false : b.IsHeavyEquipment.Value,
                                                             MaintenanceTypeID = b.MaintenanceTypeID.Value,
                                                             AmountRollover = b.AmountRollover.Value,
                                                             NextPrincipalDate = b.NextPrincipalDate.Value,
                                                             NextInterestDate = b.NextInterestDate.Value,
                                                             ApprovedMarginAsPerCM = b.ApprovedMarginAsPerCM.Value,
                                                             InterestRateCodeID = b.InterestRateCodeID,
                                                             BaseRate = b.BaseRate.Value,
                                                             AllInLP = b.AllInLP.Value,
                                                             AllInFTPRate = b.AllInFTPRate.Value,
                                                             AccountPreferencial = b.AccountPreferencial.Value,
                                                             SpecialFTP = b.SpecialFTP.Value,
                                                             SpecialRateMargin = b.SpecialRateMargin.Value,
                                                             AllInrate = b.AllInrate.Value,
                                                             ApprovalDOA = b.ApprovalDOA,
                                                             ApprovedBy = b.ApprovedBy,
                                                             Remarks = b.Remarks,
                                                             IsAdhoc = b.IsAdhoc.Value,
                                                             CSOName = b.CSOName,
                                                             DetailLink = @"<a href='#' class='ShowDetail' onClick='$(this).ShowDetail(this)'>Detail</a>",
                                                             Documents = (from x in context.TransactionDocuments
                                                                          where InstructionID.Contains(x.TransactionID) && x.IsDeleted.Equals(false)
                                                                          select new TransactionDocumentModel()
                                                                          {
                                                                              ID = x.TransactionDocumentID,
                                                                              Type = new DocumentTypeModel()
                                                                              {
                                                                                  ID = x.DocumentType.DocTypeID,
                                                                                  Name = x.DocumentType.DocTypeName,
                                                                                  Description = x.DocumentType.DocTypeDescription,
                                                                                  LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                                  LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                              },
                                                                              Purpose = new DocumentPurposeModel()
                                                                              {
                                                                                  ID = x.DocumentPurpose.PurposeID,
                                                                                  Name = x.DocumentPurpose.PurposeName,
                                                                                  Description = x.DocumentPurpose.PurposeDescription,
                                                                                  LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                                  LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                                              },
                                                                              FileName = x.Filename,
                                                                              DocumentPath = x.DocumentPath,
                                                                              LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                              LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                                                          }).ToList()
                                                         }).ToList()
                          }).SingleOrDefault();



                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }


            return IsSuccess;
        }
        public bool GetScheduledSettlementDetails(Guid workflowInstanceID, long csoRolloverID, ref TransactionCheckerLoanScheduledSettlementDetailModel output, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                var TransactionRolloverID = context.CSORollovers.Where(a => a.CSORolloverID.Equals(csoRolloverID)).Select(a => a.TransactionRolloverID).SingleOrDefault();
                var InstructionID = context.TransactionRollovers.Where(a => a.TransactionRolloverID.Equals(TransactionRolloverID)).Select(a => a.Instruction.Value).ToList();

                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new TransactionCheckerLoanScheduledSettlementDetailModel
                          {

                              ScheduledSettlementLoan = (from b in context.CSORollovers
                                                         join c in context.TransactionRollovers
                                                         on b.TransactionRolloverID equals c.TransactionRolloverID
                                                         where b.CSORolloverID.Equals(csoRolloverID)
                                                         select new ScheduledSettlementModel
                                                         {
                                                             IsChecked = c.IsSelected.HasValue ? (c.IsSelected.Value == true ? true : false) : false,
                                                             CSORollOverID = b.CSORolloverID,
                                                             SOLID = b.SOLID,
                                                             SchemeCode = b.SchemeCode,
                                                             CIF = b.CIF,
                                                             CustomerName = context.Customers.Where(d => d.CIF.Equals(b.CIF)).Select(d => d.CustomerName).FirstOrDefault(),
                                                             LoanContractNo = b.LoanContractNo,
                                                             DueDate = b.DueDate.Value,
                                                             Currency = new CurrencyModel()
                                                             {
                                                                 ID = a.Currency.CurrencyID,
                                                                 Code = a.Currency.CurrencyCode,
                                                                 Description = a.Currency.CurrencyDescription,
                                                                 LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                                 LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                             },
                                                             AmountDue = b.AmountDue.Value,
                                                             LimitID = b.LimitID,
                                                             BizSegment = new BizSegmentModel()
                                                             {
                                                                 ID = a.BizSegment.BizSegmentID,
                                                                 Name = a.BizSegment.BizSegmentName,
                                                                 Description = a.BizSegment.BizSegmentDescription,
                                                                 LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                                 LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                             },
                                                             RM = new EmployeeModel()
                                                             {
                                                                 EmployeeID = b.EmployeeID == null ? 1 : b.EmployeeID.Value,
                                                                 EmployeeName = b.EmployeeID == null ? "" : context.Employees.Where(x => x.EmployeeID.Equals(b.EmployeeID.Value)).Select(x => x.EmployeeName).FirstOrDefault()
                                                             },
                                                             CSO = b.CSO,
                                                             IsSBLCSecured = b.IsSBLCSecured == null ? false : b.IsSBLCSecured.Value,
                                                             IsHeavyEquipment = b.IsHeavyEquipment == null ? false : b.IsHeavyEquipment.Value,
                                                             MaintenanceTypeID = b.MaintenanceTypeID.Value,
                                                             AmountRollover = b.AmountRollover.Value,
                                                             NextPrincipalDate = b.NextPrincipalDate.Value,
                                                             NextInterestDate = b.NextInterestDate.Value,
                                                             ApprovedMarginAsPerCM = b.ApprovedMarginAsPerCM.Value,
                                                             InterestRateCodeID = b.InterestRateCodeID,
                                                             BaseRate = b.BaseRate.Value,
                                                             AllInLP = b.AllInLP.Value,
                                                             AllInFTPRate = b.AllInFTPRate.Value,
                                                             AccountPreferencial = b.AccountPreferencial.Value,
                                                             SpecialFTP = b.SpecialFTP.Value,
                                                             SpecialRateMargin = b.SpecialRateMargin.Value,
                                                             AllInrate = b.AllInrate.Value,
                                                             ApprovalDOA = b.ApprovalDOA,
                                                             ApprovedBy = b.ApprovedBy,
                                                             Remarks = b.Remarks,
                                                             IsAdhoc = b.IsAdhoc.Value,
                                                             CSOName = b.CSOName,
                                                             DetailLink = @"<a href='#' class='ShowDetail' onClick='$(this).ShowDetail(this)'>Detail</a>",
                                                             Documents = (from x in context.TransactionDocuments
                                                                          where InstructionID.Contains(x.TransactionID) && x.IsDeleted.Equals(false)
                                                                          select new TransactionDocumentModel()
                                                                          {
                                                                              ID = x.TransactionDocumentID,
                                                                              Type = new DocumentTypeModel()
                                                                              {
                                                                                  ID = x.DocumentType.DocTypeID,
                                                                                  Name = x.DocumentType.DocTypeName,
                                                                                  Description = x.DocumentType.DocTypeDescription,
                                                                                  LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                                  LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                              },
                                                                              Purpose = new DocumentPurposeModel()
                                                                              {
                                                                                  ID = x.DocumentPurpose.PurposeID,
                                                                                  Name = x.DocumentPurpose.PurposeName,
                                                                                  Description = x.DocumentPurpose.PurposeDescription,
                                                                                  LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                                  LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                                              },
                                                                              FileName = x.Filename,
                                                                              DocumentPath = x.DocumentPath,
                                                                              LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                              LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                                                          }).ToList()
                                                         }).FirstOrDefault()
                          }).SingleOrDefault();



                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }


            return IsSuccess;
        }
        public bool AddTransactionCheckerDetailsScheduledSettlement(Guid workflowInstanceID, long approverID, TransactionCheckerLoanScheduledSettlementModel data, ref string message)
        {
            bool IsSuccess = false;
            long transactionRolloverID;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

                    if (transactionID > 0)
                    {
                        var datarollover = context.TransactionRollovers.Where(a => a.TransactionID.Equals(transactionID)).Select(a => a.TransactionRolloverID).ToList();

                        if (datarollover != null)
                        {
                            foreach (var item in data.ScheduledSettlementLoan)
                            {
                                if (item.IsChecked == true)
                                {
                                    transactionRolloverID = item.TransactionRolloverID;
                                    Entity.TransactionRollover dataRollover = context.TransactionRollovers.Where(a => a.TransactionRolloverID.Equals(transactionRolloverID)).SingleOrDefault();
                                    dataRollover.IsSelected = true;
                                    context.SaveChanges();
                                }
                            }

                        }

                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }
            return IsSuccess;

        }
        //end basri
        #endregion

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}

//=======================================================================================//
///FOR ALL DEVELOPER...
///PLEASE ADD NEW REGION BY YOUR NAME TO KEEP CLEAN !!! DONT BE A DIRTY PROGRAMMER!!!
///ANDIWIJAYA
//=======================================================================================//