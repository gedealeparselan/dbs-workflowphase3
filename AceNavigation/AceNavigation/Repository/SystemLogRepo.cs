﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AceNavigation.Model;
using AceNavigation.Entity;
using System.Linq.Dynamic;

namespace AceNavigation.Repository
{
    class SystemLogRepo
    {
        public IList<Model.SystemLogModel> GetSystemLog(string userName, string logType, DateTime? createDate, string orderBy, int take, int skip)
        {
            using (DBSEntitiesNav context = new DBSEntitiesNav())
            {
                try
                {
                    var data =
                        from a in context.SystemLogs
                        let isName = !string.IsNullOrEmpty(userName)
                        let isLogType = !string.IsNullOrEmpty(logType)
                        let isDateStart = createDate.HasValue
                        where
                            (isName ? a.UserName.Contains(userName) : true) &&
                            (isLogType ? a.LogType.Contains(logType) : true) &&
                            (isDateStart ? a.CreateDate == createDate : true)
                        
                        select a;

                    var result = data.OrderBy(orderBy).AsEnumerable()
                        .Select((x, i) => new Model.SystemLogModel
                        {
                            RowNumber = i + 1,
                            LogID = x.LogID,
                            UserName = x.UserName,
                            LogType = x.LogType,
                            Remark = x.Remark,
                            CreateDate = x.CreateDate
                        })
                        .Skip(skip)
                        .Take(take);

                    return result.ToList();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public bool InsertSystemLog(string userName, string logType, string remark)
        {
            using (DBSEntitiesNav DBSEnt = new DBSEntitiesNav())
            {
                bool res = false;
                List<Entity.SystemLog> Perubahan = new List<Entity.SystemLog>();
                Entity.SystemLog InserToSystemLog = new Entity.SystemLog();
                InserToSystemLog.UserName = userName;
                InserToSystemLog.LogType = logType;
                InserToSystemLog.Remark = remark;
                InserToSystemLog.CreateDate = DateTime.UtcNow;

                DBSEnt.SystemLogs.Add(InserToSystemLog);
                DBSEnt.SaveChanges();
                return res;
            }
        }
    }
}
