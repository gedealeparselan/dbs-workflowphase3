var accessToken;
var $box;
var $remove = false;

var formatNumber = function (num) {
    if(num != null) {
    var n = num.toString(), p = n.indexOf('.');
    return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
        return p < 0 || i < p ? ($0 + ',') : $0;
    });
}else{
        return 0;
    }}

function GetMainTableItems(module){
	table_id = '';if(module.tableid!=null && module.tableid!='')table_id = module.tableid;
	if(table_id=='')
		var thead = $('.dataTable > thead');
	else
		var thead = $('table[id="' + table_id + '"] > thead');
	
	var tHeadSort = '';
	var tRows = $('tr > th', $(thead[0]));
	$.each(tRows,function(index){
		if(index>0 && index< tRows.length-2){
			s = $(this).prop('outerHTML');
			i1 = s.indexOf('click');
			i2 = s.indexOf('">');
			s = s.substring(0,i1) + s.substring(i2,s.length);
			tHeadSort += s;
		}
	});
	var tHeadFilter = '';
	var tRows = $('tr > th', $(thead[1]));
	$.each(tRows,function(index){
		if(index>0 && index< tRows.length-2){
			s = $(this).prop('outerHTML');
			i1 = s.indexOf('<input');
			i2 = s.indexOf('">');
			s = s.substring(0,i1) + s.substring(i2+2,s.length);
			tHeadFilter += s;
		}
	});
	if(table_id=='')
		var tbody = $('.dataTable > tbody:first');
	else
		var tbody = $('table[id="' + table_id + '"] > tbody:first');
		
	var tRows = $('tr > td', tbody);
	var tHeadRow = '';
	var fields = new Array();
	$.each(tRows,function(index){
		if(index>0 && index< tRows.length-2){
				s = $(this).find('span').attr('data-bind');
				ar = s.split(':');
				ss = ar[1].trim();
				//fields.push(ss);
				// if function
				i = ss.indexOf('(');
				if(i>-1){
					i2=ss.indexOf(',');
					ss = ss.substring(i+1,i2);
				}
				
				ars = ss.split('.');
				if(ars.length>1){
					if(ars[0]=='$root'){
						//fields.push(ss);
					}else{
						fields.push(ars[0])//+ars[1]);
					//console.log(ars[0]+ars[1]);
					}
				}else{
					fields.push(ars[0]);
					//console.log(ars[0]);
				}
				
				sss = $(this).prop('outerHTML');
				
				/*arss = sss.split('.');
				if(arss.length==2){
					ssss = arss[1];
					//arssss = ssss.split('"');
					//ssss = '"' + arssss[1];
					sss = arss[0] + ssss;
					console.log(sss);
				}*/
				tHeadRow += sss;
			}
	});	
	return {sort : tHeadSort, filter : tHeadFilter, row : tHeadRow, fields : fields}
}
		
// Get SPUser from cookies
function GetCurrentUser(vm) {
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);

        vm.SPUser = spUser;
    }
}


// Token validation On Success function
function TokenOnSuccess(data, textStatus, jqXHR){
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // call spuser function
    GetCurrentUser();

    // call get data inside view model
	if(viewModel.GetDropdown!=null)viewModel.GetDropdown();
	if(viewModel.GetParameters!=null)viewModel.GetParameters();
    viewModel.GetData();
}

// Token validation On Error function
function TokenOnError(jqXHR, textStatus, errorThrown){
    ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

    /// block enter key from user to prevent submitted data.
    $("input").keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

            return false;
        }
    });

    // scrollables
    $('.slim-scroll').each(function () {
        var $this = $(this);
        $this.slimscroll({
            height: $this.data('height') || 100,
            //width: $this.data('width') || 100,
            railVisible: true,
            alwaysVisible: true,
            color: '#D15B47'
        });
    });
	
    $('.date-picker').datepicker({autoclose: true}).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

function OnAlways(){
    //$box.trigger('reloaded.ace.widget');
    if (typeof viewModel != 'undefined') {
        if (viewModel.IsEditable) viewModel.IsEditable(true);
    }
}