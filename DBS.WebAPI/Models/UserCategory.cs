﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{

    #region Property
    [ModelName("UserCategory")]
    public class UserCategoryModel
    {
        public int UserCategoryID { get; set; }
        public string UserCategoryCode { get; set; }
        public ProductModel Product { get; set; }
        public CurrencyModel Currency { get; set; }
        public decimal MinTransaction { get; set; }
        public decimal MaxTransaction { get; set; }        
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public string UserGroup { get; set; }
        public Nullable<bool> Unlimited { get; set; }
    }

    public class UserCategoryRow : UserCategoryModel
    {
        public int RowID { get; set; }

    }

    public class UserCategoryGrid : Grid
    {
        public IList<UserCategoryRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class UserCategoryFilter : Filter { }
    #endregion

    #region Interface
    public interface IUserCategoryRepository : IDisposable
    {
        bool GetUserCategoryByID(int id, ref UserCategoryModel UserCategoryModel, ref string message);
        bool GetUserCategory(ref IList<UserCategoryModel> UserCategoryModel, int limit, int index, ref string message);
        bool GetUserCategory(ref IList<UserCategoryModel> UserCategoryModel, ref string message);
        bool GetUserCategoryIPE(ref IList<UserCategoryModel> UserCategoryModel, int id, string name , string text, ref string message); 
        bool GetUserCategory(int page, int size, IList<UserCategoryFilter> filters, string sortColumn, string sortOrder, ref UserCategoryGrid UserCategoryGrid, ref string message);
        bool GetUserCategory(UserCategoryFilter filter, ref IList<UserCategoryModel> UserCategoryModel, ref string message);
        bool GetUserCategory(string key, int limit, ref IList<UserCategoryModel> UserCategoryModel, ref string message);
        bool AddUserCategory(UserCategoryModel UserCategoryModel, ref string message);
        bool UpdateUserCategory(int id, UserCategoryModel UserCategoryModel, ref string message);
        bool DeleteUserCategory(int id, ref string message);
    }
    #endregion

    #region Repository
    public class UserCategoryRepository : IUserCategoryRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetUserCategoryByID(int id, ref UserCategoryModel UserCategoryModel, ref string message)
        {
            bool isSuccess = false;

            try
            {
                UserCategoryModel = (from a in context.UserCategories
                                     join b in context.Products on a.ProductID equals b.ProductID
                                     join c in context.Currencies on a.CurrencyID equals c.CurrencyID
                                     where a.IsDeleted.Equals(false) && a.UserCategoryID.Equals(id)
                                     select new UserCategoryModel
                                     {
                                         UserCategoryID = a.UserCategoryID,
                                         UserCategoryCode = a.UserCategoryCode,
                                         Product = new ProductModel()
                                         {
                                             ID = (a.Product != null) ? a.Product.ProductID : 0,
                                             Name = (a.Product != null) ? a.Product.ProductName : "-"
                                         },
                                         Currency = new CurrencyModel()
                                         {
                                             ID = (a.Currency != null) ? a.Currency.CurrencyID : 0,
                                             Code = (a.Currency != null) ? a.Currency.CurrencyCode : "-"
                                         },
                                         MaxTransaction = a.MaxTransaction,
                                         MinTransaction = a.MinTransaction,
                                         CreateDate = a.CreateDate,
                                         CreateBy = a.CreateBy,
                                         UpdateDate = a.UpdateDate,
                                         UpdateBy = a.UpdateBy,
                                         UserGroup = a.UserGroup,
                                         Unlimited = a.Unlimited
                                     }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetUserCategory(ref IList<UserCategoryModel> UserCategoryModel, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    UserCategoryModel = (from a in context.UserCategories
                                         join b in context.Products on a.ProductID equals b.ProductID
                                         join c in context.Currencies on a.CurrencyID equals c.CurrencyID
                                         where a.IsDeleted.Equals(false)
                                         orderby new { a.UserCategoryCode }
                                         select new UserCategoryModel
                                         {
                                             UserCategoryID = a.UserCategoryID,
                                             UserCategoryCode = a.UserCategoryCode,
                                             Product = new ProductModel()
                                             {
                                                 ID = (a.Product != null) ? a.Product.ProductID : 0,
                                                 Name = (a.Product != null) ? a.Product.ProductName : "-"
                                             },
                                             Currency = new CurrencyModel()
                                             {
                                                 ID = (a.Currency != null) ? a.Currency.CurrencyID : 0,
                                                 Code = (a.Currency != null) ? a.Currency.CurrencyCode : "-"
                                             },
                                             MaxTransaction = a.MaxTransaction,
                                             MinTransaction = a.MinTransaction,
                                             CreateDate = a.CreateDate,
                                             CreateBy = a.CreateBy,
                                             UpdateDate = a.UpdateDate,
                                             UpdateBy = a.UpdateBy,
                                             UserGroup = a.UserGroup,
                                             Unlimited = a.Unlimited
                                         }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetUserCategory(ref IList<UserCategoryModel> UserCategoryModel, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    UserCategoryModel = (from a in context.UserCategories
                                         join b in context.Products on a.ProductID equals b.ProductID
                                         join c in context.Currencies on a.CurrencyID equals c.CurrencyID
                                         where a.IsDeleted == false
                                         orderby new { a.UserCategoryCode }
                                         select new UserCategoryModel
                                         {
                                             UserCategoryID = a.UserCategoryID,
                                             UserCategoryCode = a.UserCategoryCode,
                                             Product = new ProductModel()
                                             {
                                                 ID = (a.Product != null) ? a.Product.ProductID : 0,
                                                 Name = (a.Product != null) ? a.Product.ProductName : "-"
                                             },
                                             Currency = new CurrencyModel()
                                             {
                                                 ID = (a.Currency != null) ? a.Currency.CurrencyID : 0,
                                                 Code = (a.Currency != null) ? a.Currency.CurrencyCode : "-"
                                             },
                                             MaxTransaction = a.MaxTransaction,
                                             MinTransaction = a.MinTransaction,
                                             CreateDate = a.CreateDate,
                                             CreateBy = a.CreateBy,
                                             UpdateDate = a.UpdateDate,
                                             UpdateBy = a.UpdateBy,
                                             UserGroup = a.UserGroup,
                                             Unlimited = a.Unlimited
                                         }
                                         ).ToList();
                    UserCategoryModel = UserCategoryModel.GroupBy(p => p.UserCategoryCode).Select(g => g.First()).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetUserCategory(int page, int size, IList<UserCategoryFilter> filters, string sortColumn, string sortOrder, ref UserCategoryGrid UserCategoryGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                UserCategoryModel filter = new UserCategoryModel();
                filter.Product = new ProductModel { Name = string.Empty };
                filter.Currency = new CurrencyModel { Code = string.Empty };
                if (filters != null)
                {
                    filter.UserCategoryCode = (string)filters.Where(a => a.Field.Equals("UserCategoryCode")).Select(a => a.Value).SingleOrDefault();
                    filter.MaxTransaction = Convert.ToInt32((string)filters.Where(a => a.Field.Equals("MaxTransaction")).Select(a => a.Value).SingleOrDefault());
                    filter.MinTransaction = Convert.ToInt32((string)filters.Where(a => a.Field.Equals("MinTransaction")).Select(a => a.Value).SingleOrDefault());
                    filter.Product = new ProductModel { Name = (string)filters.Where(a => a.Field.Equals("Product")).Select(a => a.Value).SingleOrDefault() };
                    filter.Currency = new CurrencyModel { Code = (string)filters.Where(a => a.Field.Equals("Currency")).Select(a => a.Value).SingleOrDefault() };
                    
                    filter.UserGroup = (string)filters.Where(a => a.Field.Equals("UserGroup")).Select(a => a.Value).SingleOrDefault();                   
                    filter.UpdateBy = (string)filters.Where(a => a.Field.Equals("UpdateBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("UpdateDate")).Any())
                    {
                        filter.UpdateDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("UpdateDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.UpdateDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.UserCategories
                                let isUserCategoryCode = !string.IsNullOrEmpty(filter.UserCategoryCode)
                                let isUserGroup = !string.IsNullOrEmpty(filter.UserGroup)
                                let isMaxTransaction = filter.MaxTransaction == 0 ? false : true
                                let isMinTransaction = filter.MinTransaction == 0 ? false : true
                                let isProduct = !string.IsNullOrEmpty(filter.Product.Name)
                                let isCurrency = !string.IsNullOrEmpty(filter.Currency.Code)
                                let isUpdateBy = !string.IsNullOrEmpty(filter.UpdateBy)
                                let isUpdateDate = filter.UpdateDate.HasValue ? true : false
                                where a.IsDeleted == false &&
                                    (isUserCategoryCode ? a.UserCategoryCode.Contains(filter.UserCategoryCode) : true) &&
                                    (isUserGroup ? a.UserGroup.Contains(filter.UserGroup) : true) &&
                                    (isMaxTransaction ? a.MaxTransaction == filter.MaxTransaction : true) &&
                                    (isMinTransaction ? a.MinTransaction == filter.MinTransaction : true) &&
                                    (isProduct ? a.Product.ProductName.Contains(filter.Product.Name) : true) &&
                                    (isCurrency ? a.Currency.CurrencyCode.Contains(filter.Currency.Code) : true) &&
                                    (isUpdateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.UpdateBy) : a.UpdateBy.Contains(filter.UpdateBy)) : true) &&
                                    (isUpdateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.UpdateDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new UserCategoryModel
                                {
                                    UserCategoryID = a.UserCategoryID,
                                    UserCategoryCode = a.UserCategoryCode,
                                    Product = new ProductModel()
                                    {
                                        ID = (a.Product != null) ? a.Product.ProductID : 0,
                                        Name = (a.Product != null) ? a.Product.ProductName : "-"
                                    },
                                    Currency = new CurrencyModel()
                                    {
                                        ID = (a.Currency != null) ? a.Currency.CurrencyID : 0,
                                        Code = (a.Currency != null) ? a.Currency.CurrencyCode : "-"
                                    },
                                    MaxTransaction = a.MaxTransaction,
                                    MinTransaction = a.MinTransaction,
                                    CreateDate = a.CreateDate,
                                    CreateBy = a.CreateBy,
                                    UpdateDate = a.UpdateDate,
                                    UpdateBy = a.UpdateBy,
                                    UserGroup = a.UserGroup,
                                    Unlimited = a.Unlimited ?? false
                                });

                    UserCategoryGrid.Page = page;
                    UserCategoryGrid.Size = size;
                    UserCategoryGrid.Total = data.Count();
                    UserCategoryGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    UserCategoryGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new UserCategoryRow
                        {
                            RowID = i + 1,
                            UserCategoryID = a.UserCategoryID,
                            UserCategoryCode = a.UserCategoryCode,
                            Product = a.Product,
                            Currency = a.Currency,
                            //Product = new Product()
                            //{
                            //    ProductID = (a.Product != null) ? a.Product.ProductID : 0,
                            //    ProductName = (a.Product != null) ? a.Product.ProductName : "-"
                            //},
                            //Currency = new Currency()
                            //{
                            //    CurrencyID = (a.Currency != null) ? a.Currency.CurrencyID : 0,
                            //    CurrencyCode = (a.Currency != null) ? a.Currency.CurrencyCode : "-"
                            //},
                            MaxTransaction = a.MaxTransaction,
                            MinTransaction = a.MinTransaction,
                            CreateDate = a.CreateDate,
                            CreateBy = a.CreateBy,
                            UpdateDate = a.UpdateDate,
                            UpdateBy = a.UpdateBy,
                            UserGroup = a.UserGroup,
                            Unlimited = a.Unlimited
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetUserCategory(UserCategoryFilter filter, ref IList<UserCategoryModel> UserCategoryModel, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetUserCategory(string key, int limit, ref IList<UserCategoryModel> UserCategoryModel, ref string message)
        {
            bool isSuccess = false;

            try
            {
                UserCategoryModel = (from a in context.UserCategories
                                     where a.IsDeleted.Equals(false) &&
                                         a.UserCategoryCode.Contains(key)
                                     orderby new { a.UserCategoryCode }
                                     select new UserCategoryModel
                                     {
                                         UserCategoryID = a.ProductID,
                                         UserCategoryCode = a.UserCategoryCode,
                                     }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddUserCategory(UserCategoryModel UserCategoryModel, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.UserCategories.Where(a => a.UserCategoryCode == UserCategoryModel.UserCategoryCode && a.ProductID == UserCategoryModel.Product.ID && a.IsDeleted == false).Any())
                {
                    UserCategory userCategory = new UserCategory();
                    userCategory.UserCategoryCode = UserCategoryModel.UserCategoryCode;
                    userCategory.MaxTransaction = UserCategoryModel.MaxTransaction;
                    userCategory.MinTransaction = UserCategoryModel.MinTransaction;
                    userCategory.IsDeleted = false;
                    userCategory.CreateDate = DateTime.UtcNow;
                    userCategory.CreateBy = currentUser.GetCurrentUser().DisplayName;
                    userCategory.UserGroup = UserCategoryModel.UserGroup;
                    userCategory.Unlimited = UserCategoryModel.Unlimited;
                    if (!string.IsNullOrEmpty(UserCategoryModel.Product.Name))
                    {
                        userCategory.ProductID = UserCategoryModel.Product.ID;
                    }
                    //if (!string.IsNullOrEmpty(UserCategoryModel.Currency.Code))
                    //{
                    userCategory.CurrencyID = UserCategoryModel.Currency.ID;
                    //}
                    context.UserCategories.Add(userCategory);
                    context.SaveChanges();
                    isSuccess = true;
                }
                else
                {
                    message = string.Format("User category data Mode Name {0} is already exist.", UserCategoryModel.UserCategoryCode);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateUserCategory(int id, UserCategoryModel UserCategoryModel, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.UserCategory data = context.UserCategories.Where(a => a.UserCategoryID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.UserCategories.Where(a => a.UserCategoryID != UserCategoryModel.UserCategoryID && a.UserCategoryCode == UserCategoryModel.UserCategoryCode && a.ProductID == UserCategoryModel.Product.ID && a.IsDeleted == false).Count() >= 1)
                    {
                        message = string.Format("Mode Name {0} is exist.", UserCategoryModel.UserCategoryCode);
                    }
                    else
                    {
                        data.UserCategoryCode = UserCategoryModel.UserCategoryCode;
                        data.MaxTransaction = UserCategoryModel.MaxTransaction;
                        data.MinTransaction = UserCategoryModel.MinTransaction;
                        data.IsDeleted = false;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        data.UserGroup = UserCategoryModel.UserGroup;
                        data.Unlimited = UserCategoryModel.Unlimited;
                        if (!string.IsNullOrEmpty(UserCategoryModel.Product.Name))
                        {
                            data.ProductID = UserCategoryModel.Product.ID;
                        }
                        if (!string.IsNullOrEmpty(UserCategoryModel.Currency.Code))
                        {
                            data.CurrencyID = UserCategoryModel.Currency.ID;
                        }
                        context.SaveChanges();
                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("User Category data for Mode ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteUserCategory(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.UserCategory data = context.UserCategories.Where(a => a.UserCategoryID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("User Category data for Mode ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        //public bool GetUserCategoryIPE(ref IList<UserCategoryModel> UserCategoryModel, ref string message)
        //{
        //    bool isSuccess = false;

        //    try
        //    {
        //        using (DBSEntities context = new DBSEntities())
        //        {
        //            UserCategoryModel = (from a in context.UserCategories
        //                                 join b in context.Products on a.ProductID equals b.ProductID
        //                                 join c in context.Currencies on a.CurrencyID equals c.CurrencyID
        //                                 join d in context.Employees on a.UserCategoryCode equals d.UserCategoryCode
        //                                 where a.IsDeleted == false && d.EmployeeUsername.Equals("i:0#.f|dbsmembership|paymentchecker1") && a.UserGroup.Equals("PaymentChecker") && b.ProductID.Equals(2)
        //                                 orderby new { a.UserCategoryCode }
        //                                 select new UserCategoryModel
        //                                 {
        //                                     UserCategoryID = a.UserCategoryID,
        //                                     UserCategoryCode = a.UserCategoryCode,
        //                                     Product = new ProductModel()
        //                                     {
        //                                         ID = (a.Product != null) ? a.Product.ProductID : 0,
        //                                         Name = (a.Product != null) ? a.Product.ProductName : "-"
        //                                     },
        //                                     Currency = new CurrencyModel()
        //                                     {
        //                                         ID = (a.Currency != null) ? a.Currency.CurrencyID : 0,
        //                                         Code = (a.Currency != null) ? a.Currency.CurrencyCode : "-"
        //                                     },
        //                                     MaxTransaction = a.MaxTransaction,
        //                                     MinTransaction = a.MinTransaction,
        //                                     CreateDate = a.CreateDate,
        //                                     CreateBy = a.CreateBy,
        //                                     UpdateDate = a.UpdateDate,
        //                                     UpdateBy = a.UpdateBy
        //                                 }
        //                                 ).ToList();
        //            //UserCategoryModel = UserCategoryModel.GroupBy(p => p.UserCategoryCode).Select(g => g.First()).ToList();
        //        }

        //        isSuccess = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        message = ex.Message;
        //    }

        //    return isSuccess;
        //}


        public bool GetUserCategoryIPE(ref IList<UserCategoryModel> UserCategoryModel, int id, string name, string text, ref string message)
        {
            bool isSuccess = false;
            name = "i:0#" + name;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    UserCategoryModel = (from a in context.UserCategories
                                         join b in context.Products on a.ProductID equals b.ProductID
                                         join c in context.Currencies on a.CurrencyID equals c.CurrencyID
                                         join d in context.Employees on a.UserCategoryCode equals d.UserCategoryCode
                                         //where a.IsDeleted == false && d.EmployeeUsername.Equals("i:0#.f|dbsmembership|paymentchecker1") && a.UserGroup.Equals("PaymentChecker") && b.ProductID.Equals(1)
                                         where a.IsDeleted == false && d.EmployeeUsername.ToLower().Equals(name) && a.UserGroup.Equals(text) && b.ProductID.Equals(id)
                                         orderby new { a.UserCategoryCode }
                                         select new UserCategoryModel
                                         {
                                             UserCategoryID = a.UserCategoryID,
                                             UserCategoryCode = a.UserCategoryCode,
                                             Product = new ProductModel()
                                             {
                                                 ID = (a.Product != null) ? a.Product.ProductID : 0,
                                                 Name = (a.Product != null) ? a.Product.ProductName : "-"
                                             },
                                             Currency = new CurrencyModel()
                                             {
                                                 ID = (a.Currency != null) ? a.Currency.CurrencyID : 0,
                                                 Code = (a.Currency != null) ? a.Currency.CurrencyCode : "-"
                                             },
                                             MaxTransaction = a.MaxTransaction,
                                             MinTransaction = a.MinTransaction,
                                             CreateDate = a.CreateDate,
                                             CreateBy = a.CreateBy,
                                             UpdateDate = a.UpdateDate,
                                             UpdateBy = a.UpdateBy,
                                             UserGroup = a.UserGroup,
                                             Unlimited = a.Unlimited
                                         }
                                         ).ToList();
                    //UserCategoryModel = UserCategoryModel.GroupBy(p => p.UserCategoryCode).Select(g => g.First()).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
    }
    #endregion

}