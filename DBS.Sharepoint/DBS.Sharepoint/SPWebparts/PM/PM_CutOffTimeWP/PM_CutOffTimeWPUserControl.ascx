﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PM_CutOffTimeWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.PM.PM_CutOffTimeWP.PM_CutOffTimeWPUserControl" %>

<link rel="stylesheet" href="/_catalogs/masterpage/Ace/assets/css/bootstrap-timepicker.css" />

<h1 class="header smaller lighter dark">Cut Off Time (Backup Purpose)</h1>

<div class="modal-body overflow-visible">
    <div class="row">
        <div class="col-xs-12">
            <div id="customer-form" class="form-horizontal" role="form">

                <div class="form-group no-margin-bottom no-padding-bottom">
                    <label class="col-sm-2" for="IsCutOffApplied">
                        Apply Cut Off Time
                    </label>

                    <label class="col-sm-2">
                        <input name="IsCutOffApplied" data-bind="checked: $root.IsCutOffApplied, enable: $root.IsRoleMaker()" class="ace ace-switch ace-switch-6" type="checkbox" />
                        <span class="lbl"></span>
                    </label>
                </div>

                <div class="form-group no-margin-bottom no-padding-bottom">
                    <label class="col-sm-2" for="CutOffTime">
                        Cut Off Time (hh:mm)
                    </label>

                    <div class="col-sm-2">
                        <div class="input-group bootstrap-timepicker">
                            <input type="text" id="timepicker" class="form-control time-picker valid" data-bind="timepicker:true, value: CutOffTime, enable: $root.IsRoleMaker()" />
                            <span class="input-group-addon">
                                <i class="icon-time bigger-110"></i>
                            </span>
                        </div>
                    </div>

                    <div>
                        <button id="btnUpdateCutOffTime" class="btn btn-sm btn-primary" data-bind="click: $root.UpdateCutOffTime, visible: $root.IsRoleMaker()">
                            <i class="icon-save"></i>
                            Save
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!--<div class="space-4"></div>
<div class="row">
    <div class="col-sm-12">
        <pre data-bind="text: JSON.stringify(ko.toJS(IsCutOffAppliedData), null, 2)"></pre>
    </div>
</div>-->

<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/date-time/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/date-time/moment.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/date-time/daterangepicker.min.js"></script>