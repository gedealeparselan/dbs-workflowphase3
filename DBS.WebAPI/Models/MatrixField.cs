﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Transactions;
using DBS.Entity;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Data.Entity.Validation;
using System.Diagnostics;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;

namespace DBS.WebAPI.Models
{
    #region Property   
    [ModelName("MatrixField")]
    public class MatrixFieldModel
    {
        /// <summary>
        /// Matrix Field ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Matrix Field Name
        /// </summary>
        public string Name { get; set; }
    }
    

    #endregion


    #region Interface
    public interface IMatrixFieldRepository : IDisposable
    {
        bool GetMatrixField(ref IList<MatrixFieldModel> fields, ref string message);
    }
    #endregion

    #region Repository
    public class MatrixFieldRepository : IMatrixFieldRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        
        public bool GetMatrixField(ref IList<MatrixFieldModel> fields, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    fields = (from a in context.MatrixFields
                              where a.IsDeleted.Equals(false)
                              select new MatrixFieldModel
                              {
                                  ID = a.FieldID,
                                  Name = a.MatrixDescription
                              }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }      


    }
    #endregion
}