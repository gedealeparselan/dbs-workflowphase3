﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace DBS.Sharepoint
{
    #region Interface

    [ServiceContract]
    public interface IUploadFileService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        UploadFileAllInfo UploadFile(UploadFileAllModel data);

        //add aridya 20161018 for skn bulk excel ~OFFLINE~
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        UploadSKNBulkExcelInfo ExcelSKNBulkData(UploadSKNBulkExcelModel excelSknBulkModel);
        //end add
    }
    #endregion
    #region Properties
    //add aridya 20161018 for skn bulk excel ~OFFLINE~
    public class TransactionSKNBulkModel
    {
        public long ID { get; set; }
        public string AccountNumber { get; set; }
        public string CustomerName { get; set; }
        public DateTime? ValueDate { get; set; }
        public string PaymentAmount { get; set; }
        public string BeneficiaryName { get; set; }
        public string BeneficiaryAccountNumber { get; set; }
        public string BankCode { get; set; }
        public string BranchCode { get; set; }
        public string Citizen { get; set; }
        public string Resident { get; set; }
        public string PaymentDetails { get; set; }
        public string Charges { get; set; }
        public string Type { get; set; }
        public long ParentTransactionID { get; set; }
        public Guid ParentWorkflowInstanceID { get; set; }
        public bool IsChanged { get; set; }
        public string BeneficiaryCustomer { get; set; }
    }

    public class TransactionSKNBulkModelRaw
    {
        public long ID { get; set; }
        public string AccountNumber { get; set; }
        public string CustomerName { get; set; }
        public string ValueDate { get; set; }
        public string PaymentAmount { get; set; }
        public string BeneficiaryName { get; set; }
        public string BeneficiaryAccountNumber { get; set; }
        public string BankCode { get; set; }
        public string BranchCode { get; set; }
        public string Citizen { get; set; }
        public string Resident { get; set; }
        public string PaymentDetails { get; set; }
        public string Charges { get; set; }
        public string Type { get; set; }
        public long ParentTransactionID { get; set; }
        public Guid ParentWorkflowInstanceID { get; set; }
        public bool IsChanged { get; set; }
        public string BeneficiaryCustomer { get; set; }
    }

    [DataContract]
    public class UploadSKNBulkExcelInfo
    {
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public IList<TransactionSKNBulkModel> output { get; set; }
        [DataMember]
        public IList<TransactionSKNBulkModelRaw> outputRaw { get; set; }
    }

    [DataContract]
    public class UploadSKNBulkExcelModel
    {
        [DataMember]
        public string Path { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string UploadType { get; set; }
        [DataMember]
        public Guid instanceID { get; set; }
        [DataMember]
        public long transactionID { get; set; }
        //[DataMember]
        //public string fileName { get; set; }
        //[DataMember]
        //public string filePath { get; set; }
        //[DataMember]
        //public string pass { get; set; }
        //[DataMember]
        //public string loginUser { get; set; }
    }
    //end add

    [DataContract]
    public class UploadFileAllModel
    {
        [DataMember]
        public string Path { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string UploadType { get; set; }
    }
    [DataContract]
    public class UploadFileAllInfo
    {
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public int RowsAffected { get; set; }
        [DataMember]
        public string Filepath { get; set; }
        [DataMember]
        public int ActivityHistoryID { get; set; }
        [DataMember]
        public int FailedRows { get; set; }
        [DataMember]
        public IList<RetModel> ReturnRows { get; set; }
    }
    public class RetModel
    {
        public string Column1 { get; set; }
        public string Column2 { get; set; }
    }
    #endregion
}
