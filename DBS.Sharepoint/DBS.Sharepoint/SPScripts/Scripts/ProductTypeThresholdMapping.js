﻿var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;
    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);

    //Model ProductType
    var ProductTypeModel = function (id, producttype) {
        var self = this;
        self.ID = ko.observable(id);
        self.Code = ko.observable(producttype);
    }

    //Declare observable which will be bind with UI
    self.ID = ko.observable(0);
    self.ProductTypeID = ko.observable(0);
    self.ThresholdGroupID = ko.observable(0);
    self.GroupName = ko.observable("");
    self.ProductType = ko.observable(new ProductTypeModel('', ''));
    self.TransactionType = ko.observable("");
    self.SelectedTransactionType = ko.observableArray([{ ID: 'S', Name: "Sell" }, { ID: 'B', Name: "Buy" }]);
    self.ResidentType = ko.observable("");
    
    self.SelectedResidentType = ko.observableArray([{ ID: 'N', Name: "Non Resident" }, { ID: 'R', Name: "Resident" }, { ID: 'A', Name: "All" }]);
    self.ThresholdType = ko.observable("");
    self.SelectedThresholdType = ko.observableArray([{ ID: 'A', Name: "Accumulation" }, { ID: 'T', Name: "Transaction" }]);
    self.ThresholdValue = ko.observable(0.0);
    self.RoundingValue = ko.observable(0.0);
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");

    self.TransactionType2 = ko.observable("");
    self.ResidentType2 = ko.observable("");
    self.ThresholdType2 = ko.observable("");
    //ddl Product Type Code
    self.ddlProductTypeCode = ko.observableArray([]);

    // filter
    self.FilterGroupName = ko.observable("");
    self.FilterProductTypeCode = ko.observable("");
    self.FilterTransactionType = ko.observable("");
    self.FilterResidentType = ko.observable("");
    self.FilterThresholdType = ko.observable("");
    self.FilterThresholdValue = ko.observable("");
    self.FilterRoundingValue = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");

    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    // New Data flag
    self.IsNewData = ko.observable(false);
    self.HeaderTitle = ko.observable(false);
    //self.ProductTypeId = ko.observable(0);

    // Declare an ObservableArray for Storing the JSON Response
    self.ProductTypeThresholdMappings = ko.observableArray([]);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    // set default sorting
    self.GridProperties().SortColumn("GroupName");
    self.GridProperties().SortOrder("ASC");

    // bind clear filters
    self.ClearFilters = function () {
        self.FilterGroupName("");
        self.FilterProductTypeCode("");
        self.FilterTransactionType("");
        self.FilterResidentType("");
        self.FilterThresholdType("");
        self.FilterThresholdValue("");
        self.FilterRoundingValue("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");
        GetData();
    };

    self.FormatNumber = function (num) {
        if (num != null) {
            var n = num.toString(), p = n.indexOf('.');
            return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
                return p < 0 || i < p ? ($0 + ',') : $0;
            });
        } else { return 0; }
    }

    var formatNumber = function (num) {
        if (num != null) {
            //num = num.toString().replace(/,/g, '');
            //num = parseFloat(num).toFixed(2);
            var n = num.toString(), p = n.indexOf('.');
            return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
                return p < 0 || i < p ? ($0 + ',') : $0;
            });
        } else { return 0; }
    }


    // bind get data function to view
    self.GetData = function () {
        GetData();
    };

    //bind get dropdown function to view
    self.GetDropdown = function () {
        GetDropdown();
    };

    //The Object which stored data entered in the observables
    var ThresholdGroup = {
        ID: self.ID,
        ProductTypeID: self.ProductTypeID,
        ThresholdGroupID: self.ThresholdGroupID,
        GroupName: self.GroupName,
        ProductType: self.ProductType,
        TransactionType: self.TransactionType,
        ResidentType: self.ResidentType,
        ThresholdType: self.ThresholdType,
        ThresholdValue: self.ThresholdValue,
        RoundingValue: self.RoundingValue,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
    }

    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    console.log("spUser Roles : " + ko.toJSON(spUser.Roles));
                    console.log("spUser Permited : " + data);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    // Function to get Product Type Code  for Dropdownlist
    function GetDropdown() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.parameter + '?select=ProductType',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    console.log(data['ProductType']);
                    if (data['ProductType'] != null) {
                        self.ddlProductTypeCode(data['ProductType']);
                        console.log(self.ddlProductTypeCode());
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetSelectedProductType() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.parameter + '?select=ProductType',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    // console.log(data['ProductType']);
                    if (data['ProductType'] != null) {
                        self.ddlProductTypeCode(data['ProductType']);
                        //console.log(self.ddlProductTypeCode());
                        if (self.ddlProductTypeCode() != undefined) {
                            if (self.ProductType().ID != undefined) {
                                self.ProductType().ID = self.ProductType().ID;
                                self.ProductType().Code = self.ProductType().Code;
                                console.log(self.ProductType());
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    self.save = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {

                    //Ajax call to insert the ProductTypeThresholdMappings
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.producttypethresholdmapping,
                        data: ko.toJSON(ThresholdGroup), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.update = function () {
        // validation
        //console.log(self.ID());
        //console.log(ko.toJSON(self.ProductType()));
        //return;
        var form = $("#aspnetForm");
        form.validate();
        if (form.valid()) {
            // hide current popup window
            //$("#modal-form").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    //Ajax call to update the Product Type Threshold Mapping
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.producttypethresholdmapping + "/" + self.ID(),
                        data: ko.toJSON(ThresholdGroup),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status == 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.delete = function () {
        // hide current popup window
        //$("#modal-form").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.producttypethresholdmapping + "/" + self.ID(),
                    data: ko.toJSON(ThresholdGroup),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    self.OnChangeProductType = function () {
        var indexOption = $('#ProductTypeID option:selected').index();
    };

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-18
    };

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        self.IsNewData(false);
        
        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');

        self.ID(data.ID);
        self.GroupName(data.GroupName);
        self.ProductType(new ProductTypeModel(data.ProductType.ID, ""));
        
        self.ProductTypeID(data.ProductType.ID);
        self.TransactionType(data.TransactionType);

        self.ResidentType(data.ResidentType);
        self.ThresholdType(data.ThresholdType);
        self.ThresholdValue(data.ThresholdValue);
        self.RoundingValue(data.RoundingValue);
        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);

        GetSelectedProductType();
    };

    //insert new
    self.NewData = function () {
        // flag as new Data
        self.IsNewData(true);
        self.Readonly(false);
        // bind empty data
        self.ID(0);
        self.GroupName('');
        self.ProductType(new ProductTypeModel("", ""));
        self.TransactionType('');
        self.ResidentType('');
        self.ThresholdType('');
        self.ThresholdValue('');
        self.RoundingValue('');
    };

    //Function to Read All Customers
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.producttypethresholdmapping,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }

    //Function to validation dynamic field

    function IsvalidField() { /*
     $("[name^=days]").each(function () {
     $(this).rules('add', {
     required: true,
     maxlength: 2,
     number: true,
     messages: {
     required: "Please provide a valid Day.",
     maxlength: "Please provide maximum 2 number valid day value.",
     number: "Please provide number"
     }
     });
     }); */
        return true;
    }

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterGroupName() != "") filters.push({ Field: 'GroupName', Value: self.FilterGroupName() });
        if (self.FilterProductTypeCode() != "") filters.push({ Field: 'ProductTypeCode', Value: self.FilterProductTypeCode() });
        if (self.FilterTransactionType() != "") filters.push({ Field: 'TransactionType', Value: self.FilterTransactionType() });
        if (self.FilterResidentType() != "") filters.push({ Field: 'ResidentType', Value: self.FilterResidentType() });
        if (self.FilterThresholdType() != "") filters.push({ Field: 'ThresholdType', Value: self.FilterThresholdType() });
        if (self.FilterThresholdValue() != "") filters.push({ Field: 'ThresholdValue', Value: self.FilterThresholdValue() });
        if (self.FilterRoundingValue() != "") filters.push({ Field: 'RoundingValue', Value: self.FilterRoundingValue() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

        return filters;
    };

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            console.log("Get Data : ");
            console.log(data.Rows);
            self.ProductTypeThresholdMappings(data.Rows);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }

    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });
};