﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Transactions;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Data.Entity;

namespace DBS.WebAPI.Models
{
    #region Property
    // [ModelName("Customer")]
    public class CustomerModel
    {
        /// <summary>
        /// Customer Indentification File (unique).
        /// </summary>
        //[MaxLength(10, ErrorMessage = "CIF is must be in {1} characters.")]
        public string CIF { get; set; }


        /// <summary>
        /// Customer Name.
        /// </summary>
        // [MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Name { get; set; }

        /// <summary>
        /// Customer POA Name.
        /// </summary>
        // [MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string POAName { get; set; }

        /// <summary>
        /// Biz Segment Information
        /// </summary>
        public BizSegmentModel BizSegment { get; set; }

        /// <summary>
        /// Customer Branch
        /// </summary>
        public BranchModel Branch { get; set; }

        /// <summary>
        /// Customer Type
        /// </summary>
        public CustomerTypeModel Type { get; set; }

        /// <summary>
        /// Relationship Manager (RM)
        /// </summary>
        public RMModel RM { get; set; }

        /// <summary>
        /// Customer Contacts
        /// </summary>
        public IList<CustomerContactModel> Contacts { get; set; }

        /// <summary>
        /// POA Functions
        /// </summary>
        public IList<POAFunctionModel> Functions { get; set; }

        /// <summary>
        /// Customer Account
        /// </summary>
        public IList<CustomerAccountModel> Accounts { get; set; }

        /// <summary>
        /// Customer Underlying
        /// </summary>
        public IList<CustomerUnderlyingModel> Underlyings { get; set; }

        /// <summary>
        /// Last Modified Date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last Modified By User
        /// </summary>
        public string LastModifiedBy { get; set; }
        public int? CustomerTypeID { get; set; }//add by fandi
        public int? CustomerCategoryID { get; set; }//add fandi

        public bool? IsCitizen { get; set; }

        public bool? IsResident { get; set; }

        public bool? IsBeneficiaryResident { get; set; }

        public bool? IsDormant { get; set; }

        public bool? IsFreeze { get; set; }

    }

    public class RMModel
    {

        public long? ID { get; set; }

        public string Name { get; set; }
        public int? RankID { get; set; }

        public int BranchID { get; set; }

        public int? SegmentID { get; set; }
        public string Email { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public string LastModifiedBy { get; set; }
    }

    //Add by Rizki
    public class CustomerTableModel
    {
        public string CIF { get; set; }

        public string Name { get; set; }

        public string BizSegment { get; set; }

        public string Branch { get; set; }

        public string Type { get; set; }

        public DateTime LastModifiedDate { get; set; }

        public string LastModifiedBy { get; set; }
    }

    public class CustomerRow : CustomerTableModel
    {
        public int RowID { get; set; }
    }

    public class CustomerGrid : Grid
    {
        public IList<CustomerRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class CustomerFilter : Filter
    {
    }
    #endregion

    #region Interface
    public interface ICustomerRepository : IDisposable
    {
        bool GetCustomerByCIF(string cif, ref CustomerModel customer, ref string message);
        bool GetCustomer(ref IList<CustomerModel> customers, int limit, int index, ref string message);
        bool GetCustomer(int page, int size, IList<CustomerFilter> filters, string sortColumn, string sortOrder, ref CustomerGrid customer, ref string message);
        bool GetCustomer(string key, int limit, ref IList<CustomerModel> customers, ref string message);
        bool AddCustomer(CustomerModel customer, ref string message);
        bool UpdateCustomer(string cif, CustomerModel customer, ref string message);
        bool DeleteCustomer(string cif, ref string message);
        bool GetCustomerByTransaction(long TransactionID, ref CustomerModel customer, ref string message);
        bool GetCustomerByTransactionDeal(long TransactionID, ref CustomerModel customer, ref string message);
        bool GetCustomerByTMONetting(long TMONettingID, ref CustomerModel customer, ref string message);       
        bool GetCustomerByTransactionTMOIPEDeal(long TransactionDealID, long TransactionID, ref CustomerModel customer, ref string message);
        bool GetCustomerByTransactionFXDealChecker(long TransactionID, ref CustomerModel customer, ref string message);
        bool GetCustomerByTransactionDealChecker(long TransactionDealID, long TransactionID, ref CustomerModel customer, ref string message);      
        bool GetCustomerByTransactionReviseMaker(long TransactionID, ref CustomerModel customer, ref string message);
        bool GetCustomerByTransactionReviseDealMaker(long TransactionID, ref CustomerModel customer, ref string message);
        bool GetCustomerUnderlyingsByCIF(long transactionDealID, ref CustomerModel customer, ref string message);
        #region basri
        bool GetCustomerByTransactionNetting(long TransactionID, ref CustomerModel customer, ref string message);
        #endregion
    }
    #endregion

    #region Repository
    public class CustomerRepository : ICustomerRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetCustomerByCIF(string cif, ref CustomerModel customer, ref string message)
        {
            bool isSuccess = false;
            DateTime currentdate = DateTime.Now;
            try
            {
                customer = (from a in context.Customers
                            where a.IsDeleted.Equals(false)
                            && a.CIF.Equals(cif)
                            select new CustomerModel
                            {
                                CIF = a.CIF,
                                Name = a.CustomerName,
                                POAName = a.POAName,
                                CustomerTypeID = a.CustomerTypeID,//add by fandi                               
                                Branch = a.Location != null ? new BranchModel()
                                {
                                    ID = a.Location.LocationID,
                                    Name = a.Location.LocationName,
                                    SolID = a.Location.SolID,//add by fandi
                                    LastModifiedBy = a.Location.UpdateDate == null ? a.Location.CreateBy : a.Location.UpdateBy,
                                    LastModifiedDate = a.Location.UpdateDate == null ? a.Location.CreateDate : a.Location.UpdateDate.Value
                                } : new BranchModel()
                                {
                                    ID = 0,
                                    Name = "",
                                    SolID = "",//add by fandi
                                    LastModifiedBy = a.Location.CreateBy,
                                    LastModifiedDate = a.Location.CreateDate
                                },
                                BizSegment = new BizSegmentModel()
                                {
                                    ID = a.BizSegment.BizSegmentID,
                                    Name = a.BizSegment.BizSegmentName,
                                    Description = a.BizSegment.BizSegmentDescription,
                                    LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                    LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                },

                                Type = a.CustomerType != null ? new CustomerTypeModel()
                                {
                                    ID = a.CustomerType.CustomerTypeID,
                                    Name = a.CustomerType.CustomerTypeName,
                                    Description = a.CustomerType.CustomerTypeDescription,
                                    LastModifiedBy = a.CustomerType.UpdateDate == null ? a.CustomerType.CreateBy : a.CustomerType.UpdateBy,
                                    LastModifiedDate = a.CustomerType.UpdateDate == null ? a.CustomerType.CreateDate : a.CustomerType.UpdateDate.Value
                                } : new CustomerTypeModel()
                                {
                                    ID = 0,
                                    Name = "",
                                    Description = "",
                                    LastModifiedBy = a.CustomerType.UpdateDate == null ? a.CustomerType.CreateBy : a.CustomerType.UpdateBy,
                                    LastModifiedDate = a.CustomerType.UpdateDate == null ? a.CustomerType.CreateDate : a.CustomerType.UpdateDate.Value
                                },
                                RM = a.Employee != null ? new RMModel()
                                {

                                    ID = a.Employee.EmployeeID,
                                    Name = a.Employee.EmployeeName,
                                    RankID = a.Employee.RankID,
                                    BranchID = a.Employee.LocationID,
                                    SegmentID = a.Employee.BizSegmentID,
                                    Email = a.Employee.EmployeeEmail,
                                    LastModifiedBy = a.Employee.UpdateDate == null ? a.Employee.CreateBy : a.Employee.UpdateBy,
                                    LastModifiedDate = a.Employee.UpdateDate == null ? a.Employee.CreateDate : a.Employee.UpdateDate.Value
                                } : new RMModel()
                                {
                                    ID = 0,
                                    Name = "",
                                    RankID = 0,
                                    BranchID = 0,
                                    SegmentID = 0,
                                    Email = "",
                                    LastModifiedBy = a.Employee.CreateBy,
                                    LastModifiedDate = a.Employee.CreateDate
                                },
                                /*Accounts = a.CustomerAccounts.Where(x => x.IsDeleted.Equals(false)).Select(x => new CustomerAccountModel()
                                {
                                    AccountNumber = x.AccountNumber,
                                    Currency = new CurrencyModel()
                                    {
                                        ID = x.Currency.CurrencyID,
                                        Code = x.Currency.CurrencyCode,
                                        Description = x.Currency.CurrencyDescription,
                                        LastModifiedBy = x.Currency.UpdateDate == null ? x.Currency.CreateBy : x.Currency.UpdateBy,
                                        LastModifiedDate = x.Currency.UpdateDate == null ? x.Currency.CreateDate : x.Currency.UpdateDate.Value
                                    },
                                    LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                    LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                }).ToList(),*/
                                Functions = a.CustomerFunctions.Where(x => x.IsDeleted.Equals(false)).Select(x => new POAFunctionModel()
                                {
                                    ID = x.POAFunction.POAFunctionID,
                                    Name = x.POAFunction.POAFunctionName,
                                    Description = x.POAFunction.POAFunctionDescription,
                                    LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                    LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                }).ToList(),
                                Underlyings = a.CustomerUnderlyings.Where(yz => yz.IsDeleted.Equals(false) && DbFunctions.TruncateTime(yz.ExpiredDate) >= DbFunctions.TruncateTime(currentdate)).Select(x => new CustomerUnderlyingModel()
                                {
                                    ID = x.UnderlyingID,
                                    StatementLetter = new StatementLetterModel()
                                    {
                                        ID = x.StatementLetter.StatementLetterID,
                                        Name = x.StatementLetter.StatementLetterName,
                                        LastModifiedBy = x.StatementLetter.UpdateDate == null ? x.StatementLetter.CreateBy : x.StatementLetter.UpdateBy,
                                        LastModifiedDate = x.StatementLetter.UpdateDate == null ? x.StatementLetter.CreateDate : x.StatementLetter.UpdateDate.Value
                                    },
                                    Amount = x.Amount,
                                    Rate = x.Rate,
                                    AmountUSD = x.AmountUSD,
                                    AvailableAmount = x.AvailableAmountUSD,
                                    UtilizeAmountDeal = x.AvailableAmountUSD,
                                    AvailableAmountDeal = x.AvailableAmountUSD,
                                    AttachmentNo = x.AttachmentNo,
                                    Currency = new CurrencyModel()
                                    {
                                        ID = x.Currency.CurrencyID,
                                        Code = x.Currency.CurrencyCode,
                                        Description = x.Currency.CurrencyDescription,
                                        LastModifiedBy = x.Currency.UpdateDate == null ? x.Currency.CreateBy : x.Currency.UpdateBy,
                                        LastModifiedDate = x.Currency.UpdateDate == null ? x.Currency.CreateDate : x.Currency.UpdateDate.Value
                                    },
                                    DateOfUnderlying = x.DateOfUnderlying,
                                    DocumentType = new DocumentTypeModel()
                                    {
                                        ID = x.DocumentType.DocTypeID,
                                        Name = x.DocumentType.DocTypeName,
                                        Description = x.DocumentType.DocTypeDescription,
                                        LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                        LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                    },
                                    EndDate = x.EndDate,
                                    ExpiredDate = x.ExpiredDate,
                                    InvoiceNumber = x.InvoiceNumber,
                                    ReferenceNumber = x.ReferenceNumber,
                                    StartDate = x.StartDate,
                                    SupplierName = x.SupplierName,
                                    UnderlyingDocument = new UnderlyingDocModel()
                                    {
                                        ID = x.UnderlyingDocument.UnderlyingDocID,
                                        Name = x.UnderlyingDocument.UnderlyingDocName,
                                        LastModifiedBy = x.UnderlyingDocument.UpdateDate == null ? x.UnderlyingDocument.CreateBy : x.UnderlyingDocument.UpdateBy,
                                        LastModifiedDate = x.UnderlyingDocument.UpdateDate == null ? x.UnderlyingDocument.CreateDate : x.UnderlyingDocument.UpdateDate.Value
                                    },
                                    IsDeclarationOfException = x.IsDeclarationOfExecption,
                                    IsUtilize = x.IsUtilize,
                                    IsEnable = false,
                                    LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                    LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                }).ToList(),
                                IsCitizen = a.IsCitizen == null ? false : a.IsCitizen,
                                IsResident = a.IsResident == null ? false : a.IsResident,
                                IsDormant = a.IsDormant == null ? false : a.IsDormant,
                                IsFreeze = a.IsFreeze == null ? false : a.IsFreeze,
                                LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                            }).SingleOrDefault();
                if (customer != null && customer.CIF != null)
                {
                    var accountData = GetJointAccount(customer.CIF);
                    customer.Accounts = accountData;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public List<CustomerAccountModel> GetJointAccount(string cif)
        {
            try
            {
                var data = (from j in context.SP_GETJointAccount(cif)
                            select new CustomerAccountModel
                            {
                                AccountNumber = j.AccountNumber,
                                Currency = new CurrencyModel
                                {
                                    ID = j.CurrencyID.Value,
                                    Code = j.CurrencyCode,
                                    Description = j.CurrencyDescription
                                },
                                CustomerName = j.CustomerName,
                                IsJointAccount = j.IsJointAccount
                            }).ToList();
                return data;
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                return null;
            }
        }
        public bool GetCustomer(ref IList<CustomerModel> customers, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;

                customers = (from a in context.Customers
                             where a.IsDeleted.Equals(false)
                             orderby new { a.CIF, a.CustomerName }
                             select new CustomerModel
                             {
                                 CIF = a.CIF,
                                 Name = a.CustomerName,
                                 POAName = a.POAName,
                                 Branch = a.Location != null ? new BranchModel()
                                 {
                                     ID = a.Location.LocationID,
                                     Name = a.Location.LocationName,
                                     SolID = a.Location.SolID,
                                     LastModifiedBy = a.Location.UpdateDate == null ? a.Location.CreateBy : a.Location.UpdateBy,
                                     LastModifiedDate = a.Location.UpdateDate == null ? a.Location.CreateDate : a.Location.UpdateDate.Value
                                 } : new BranchModel()
                                 {
                                     ID = 0,
                                     Name = "",
                                     SolID = "",
                                     LastModifiedBy = a.Location.CreateBy,
                                     LastModifiedDate = a.Location.CreateDate
                                 },
                                 BizSegment = new BizSegmentModel()
                                 {
                                     ID = a.BizSegment.BizSegmentID,
                                     Name = a.BizSegment.BizSegmentName,
                                     Description = a.BizSegment.BizSegmentDescription,
                                     LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                     LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                 },
                                 Type = new CustomerTypeModel()
                                 {
                                     ID = a.CustomerType.CustomerTypeID,
                                     Name = a.CustomerType.CustomerTypeName,
                                     Description = a.CustomerType.CustomerTypeDescription,
                                     LastModifiedBy = a.CustomerType.UpdateDate == null ? a.CustomerType.CreateBy : a.CustomerType.UpdateBy,
                                     LastModifiedDate = a.CustomerType.UpdateDate == null ? a.CustomerType.CreateDate : a.CustomerType.UpdateDate.Value
                                 },
                                 RM = a.CustomerMappings.Where(x => x.IsDeleted.Equals(false)).Select(x => new RMModel()
                                 {
                                     ID = x.Employee.EmployeeID,
                                     Name = x.Employee.EmployeeName,
                                     SegmentID = x.Employee.BizSegmentID,
                                     BranchID = x.Employee.LocationID,
                                     RankID = x.Employee.RankID,
                                     Email = x.Employee.EmployeeEmail,
                                     LastModifiedBy = x.Employee.UpdateDate == null ? x.Employee.CreateBy : x.Employee.UpdateBy,
                                     LastModifiedDate = x.Employee.UpdateDate == null ? x.Employee.CreateDate : x.Employee.UpdateDate.Value
                                 }).FirstOrDefault(),
                                 Contacts = a.CustomerContacts.Where(x => x.IsDeleted.Equals(false)).Select(x => new CustomerContactModel()
                                 {
                                     ID = x.ContactID,
                                     Name = x.ContactName,
                                     IDNumber = x.IdentificationNumber,
                                     DateOfBirth = x.DateOfBirth,
                                     //Address = x.Address,
                                     PhoneNumber = x.PhoneNumber,
                                     OccupationInID = x.OccupationInId,
                                     PlaceOfBirth = x.PlaceOfBirth,
                                     EffectiveDate = x.EffectiveDate,
                                     CancellationDate = x.CancellationDate,
                                     LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                     LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                 }).ToList(),
                                 Accounts = a.CustomerAccounts.Where(x => x.IsDeleted.Equals(false)).Select(x => new CustomerAccountModel()
                                 {
                                     AccountNumber = x.AccountNumber,
                                     Currency = new CurrencyModel()
                                     {
                                         ID = x.Currency.CurrencyID,
                                         Code = x.Currency.CurrencyCode,
                                         Description = x.Currency.CurrencyDescription,
                                         LastModifiedBy = x.Currency.UpdateDate == null ? x.Currency.CreateBy : x.Currency.UpdateBy,
                                         LastModifiedDate = x.Currency.UpdateDate == null ? x.Currency.CreateDate : x.Currency.UpdateDate.Value
                                     },
                                     LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                     LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                 }).ToList(),
                                 /*  Functions = a.CustomerFunctions.Where(x => x.IsDeleted.Equals(false)).Select(x => new POAFunctionModel()
                                   {
                                       ID = x.POAFunction.POAFunctionID,
                                       Name = x.POAFunction.POAFunctionName,
                                       Description = x.POAFunction.POAFunctionDescription,
                                       LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                       LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                   }).ToList(),
                                   Underlyings = a.CustomerUnderlyings.Select(x => new CustomerUnderlyingModel()
                                   {
                                       ID = x.UnderlyingID,
                                       Amount = x.Amount,
                                       AttachmentNo = x.AttachmentNo,
                                       Currency = new CurrencyModel()
                                       {
                                           ID = x.Currency.CurrencyID,
                                           Code = x.Currency.CurrencyCode,
                                           Description = x.Currency.CurrencyDescription,
                                           LastModifiedBy = x.Currency.UpdateDate == null ? x.Currency.CreateBy : x.Currency.UpdateBy,
                                           LastModifiedDate = x.Currency.UpdateDate == null ? x.Currency.CreateDate : x.Currency.UpdateDate.Value
                                       },
                                       DateOfUnderlying = x.DateOfUnderlying,
                                       DocumentType = new DocumentTypeModel()
                                       {
                                           ID = x.DocumentType.DocTypeID,
                                           DocTypeName = x.DocumentType.DocTypeName,
                                           DocTypeDescription = x.DocumentType.DocTypeDescription,
                                           LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                           LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                       },
                                       EndDate = x.EndDate,
                                       ExpiredDate = x.ExpiredDate,
                                       InvoiceNumber = x.InvoiceNumber,
                                       ReferenceNumber = x.ReferenceNumber,
                                       StartDate = x.StartDate,
                                       SupplierName = x.SupplierName,
                                       UnderlyingDocument = new UnderlyingDocModel()
                                       {
                                           ID = x.UnderlyingDocument.UnderlyingDocID,
                                           Name = x.UnderlyingDocument.UnderlyingDocName,
                                           LastModifiedBy = x.UnderlyingDocument.UpdateDate == null ? x.UnderlyingDocument.CreateBy : x.UnderlyingDocument.UpdateBy,
                                           LastModifiedDate = x.UnderlyingDocument.UpdateDate == null ? x.UnderlyingDocument.CreateDate : x.UnderlyingDocument.UpdateDate.Value
                                       },
                                       IsDeclarationOfException = x.IsDeclarationOfExecption,
                                       StatementLetter = new StatementLetterModel()
                                       {
                                           ID = x.StatementLetter.StatementLetterID,
                                           Name = x.StatementLetter.StatementLetterName,
                                           LastModifiedBy = x.StatementLetter.UpdateDate == null ? x.StatementLetter.CreateBy : x.StatementLetter.UpdateBy,
                                           LastModifiedDate = x.StatementLetter.UpdateDate == null ? x.StatementLetter.CreateDate : x.StatementLetter.UpdateDate.Value
                                       },
                                       LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                       LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                   }).ToList(), */
                                 LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                 LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                             }).Skip(skip).Take(limit).ToList();


                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomer(int page, int size, IList<CustomerFilter> filters, string sortColumn, string sortOrder, ref CustomerGrid customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;

                CustomerTableModel filter = new CustomerTableModel();

                string filterModifiedDate = "";
                if (filters != null)
                {
                    filter.CIF = filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.Name = filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.BizSegment = filters.Where(a => a.Field.Equals("BizSegment")).Select(a => a.Value).SingleOrDefault();
                    filter.Branch = filters.Where(a => a.Field.Equals("Branch")).Select(a => a.Value).SingleOrDefault();
                    filter.Type = filters.Where(a => a.Field.Equals("Type")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    filterModifiedDate = filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault();
                }

                using (DBSEntities context1 = new DBSEntities())
                {
                    var data = (from a in context1.SP_GetCustomerList(filter.CIF,
                                                                      filter.Name,
                                                                      filter.Branch,
                                                                      filter.BizSegment,
                                                                      filter.Type,
                                                                      filter.LastModifiedBy,
                                                                      filterModifiedDate)

                                select new CustomerTableModel
                                {
                                    CIF = a.CIF,
                                    Name = a.CustomerName,
                                    BizSegment = a.BizSegmentName,
                                    Branch = a.LocationName,
                                    Type = a.CustomerTypeName,
                                    LastModifiedDate = a.UpdateDate,
                                    LastModifiedBy = a.UpdateBy
                                }).ToList();

                    customer.Page = page;
                    customer.Size = size;
                    customer.Total = data.Count();
                    customer.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    customer.Rows = data.AsQueryable().OrderBy(orderBy).Select((a, i) => new CustomerRow()
                    {
                        RowID = i + 1,
                        CIF = a.CIF,
                        Name = a.Name,
                        BizSegment = a.BizSegment,
                        Branch = a.Branch,
                        Type = a.Type,
                        LastModifiedDate = a.LastModifiedDate,
                        LastModifiedBy = a.LastModifiedBy
                    }).Skip(skip).Take(size).ToList();

                    filter = null;

                    isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomer(string key, int limit, ref IList<CustomerModel> customers, ref string message)
        {
            bool isSuccess = false;

            try
            {
                customers = (from a in context.Customers
                             where a.IsDeleted.Equals(false)
                                 && (a.CIF.Contains(key) || a.CustomerName.Contains(key))
                             orderby new { a.CIF, a.CustomerName }
                             select new CustomerModel
                             {
                                 CIF = a.CIF,
                                 Name = a.CustomerName,
                                 POAName = a.POAName,
                                 Branch = a.Location != null ? new BranchModel()
                                 {
                                     ID = a.Location.LocationID,
                                     Name = a.Location.LocationName,
                                     LastModifiedBy = a.Location.UpdateDate == null ? a.Location.CreateBy : a.Location.UpdateBy,
                                     LastModifiedDate = a.Location.UpdateDate == null ? a.Location.CreateDate : a.Location.UpdateDate.Value
                                 } : new BranchModel()
                                 {
                                     ID = 0,
                                     Name = "",
                                     LastModifiedBy = a.Location.UpdateDate == null ? a.Location.CreateBy : a.Location.UpdateBy,
                                     LastModifiedDate = a.Location.UpdateDate == null ? a.Location.CreateDate : a.Location.UpdateDate.Value
                                 },
                                 BizSegment = new BizSegmentModel()
                                 {
                                     ID = a.BizSegment.BizSegmentID,
                                     Name = a.BizSegment.BizSegmentName,
                                     Description = a.BizSegment.BizSegmentDescription,
                                     LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                     LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                 },

                                 Type = a.CustomerType != null ? new CustomerTypeModel()
                                 {
                                     ID = a.CustomerType.CustomerTypeID,
                                     Name = a.CustomerType.CustomerTypeName,
                                     Description = a.CustomerType.CustomerTypeDescription,
                                     LastModifiedBy = a.CustomerType.UpdateDate == null ? a.CustomerType.CreateBy : a.CustomerType.UpdateBy,
                                     LastModifiedDate = a.CustomerType.UpdateDate == null ? a.CustomerType.CreateDate : a.CustomerType.UpdateDate.Value
                                 } : new CustomerTypeModel()
                                 {
                                     ID = 0,
                                     Name = "",
                                     Description = "",
                                     LastModifiedBy = a.CustomerType.UpdateDate == null ? a.CustomerType.CreateBy : a.CustomerType.UpdateBy,
                                     LastModifiedDate = a.CustomerType.UpdateDate == null ? a.CustomerType.CreateDate : a.CustomerType.UpdateDate.Value
                                 },
                                 RM = a.Employee != null ? new RMModel()
                                 {

                                     ID = a.Employee.EmployeeID,
                                     Name = a.Employee.EmployeeName,
                                     RankID = a.Employee.RankID,
                                     BranchID = a.Employee.LocationID,
                                     SegmentID = a.Employee.BizSegmentID,
                                     Email = a.Employee.EmployeeEmail,
                                     LastModifiedBy = a.Employee.UpdateDate == null ? a.Employee.CreateBy : a.Employee.UpdateBy,
                                     LastModifiedDate = a.Employee.UpdateDate == null ? a.Employee.CreateDate : a.Employee.UpdateDate.Value
                                 } : new RMModel()
                                 {
                                     ID = 0,
                                     Name = "",
                                     RankID = 0,
                                     BranchID = 0,
                                     SegmentID = 0,
                                     Email = "",
                                     LastModifiedBy = a.Employee.CreateBy,
                                     LastModifiedDate = a.Employee.CreateDate
                                 },
                                 //Accounts = a.CustomerAccounts.Where(x => x.IsDeleted.Equals(false)).Select(x => new CustomerAccountModel()
                                 // {
                                 //     AccountNumber = x.AccountNumber,
                                 //     Currency = new CurrencyModel()
                                 //     {
                                 //         ID = x.Currency.CurrencyID,
                                 //         Code = x.Currency.CurrencyCode,
                                 //         Description = x.Currency.CurrencyDescription,
                                 //         LastModifiedBy = x.Currency.UpdateDate == null ? x.Currency.CreateBy : x.Currency.UpdateBy,
                                 //         LastModifiedDate = x.Currency.UpdateDate == null ? x.Currency.CreateDate : x.Currency.UpdateDate.Value
                                 //     },
                                 //     LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                 //     LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                 // }).ToList(),
                                 IsResident = a.IsResident.HasValue ? a.IsResident.Value : false,

                                 Accounts = (from x in context.CustomerAccounts
                                             join y in context.CustomerAccountMappings.Where(xy => xy.IsDeleted == false) on x.AccountNumber equals y.AccountNumber into temp
                                             from z in temp.DefaultIfEmpty()
                                             where (z.CIF == a.CIF)
                                             select new CustomerAccountModel
                                             {
                                                 AccountNumber = x.AccountNumber,
                                                 Currency = new CurrencyModel()
                                                 {
                                                     ID = x.Currency.CurrencyID,
                                                     Code = x.Currency.CurrencyCode,
                                                     Description = x.Currency.CurrencyDescription,
                                                     LastModifiedBy = x.Currency.UpdateDate == null ? x.Currency.CreateBy : x.Currency.UpdateBy,
                                                     LastModifiedDate = x.Currency.UpdateDate == null ? x.Currency.CreateDate : x.Currency.UpdateDate.Value
                                                 },
                                                 LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                 LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                             }).ToList(),
                             }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddCustomer(CustomerModel customer, ref string message)
        {
            bool isSuccess = false;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (!context.Customers.Where(a => a.CIF.Equals(customer.CIF)).Any())
                    {
                        context.Customers.Add(new Entity.Customer()
                        {
                            CIF = customer.CIF,
                            CustomerName = customer.Name,
                            POAName = customer.POAName,
                            BizSegmentID = customer.BizSegment.ID,
                            LocationID = customer.Branch.ID,
                            CustomerTypeID = customer.Type.ID,
                            EmployeeID = customer.RM.ID,
                            SourceID = 2,
                            IsDeleted = false,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().DisplayName
                        });
                        context.SaveChanges();

                    }
                    else
                    {
                        message = string.Format("Customer data for CIF number {0} is already exist.", customer.CIF);
                    }
                    if (customer.RM.ID != null)
                    {
                        context.CustomerMappings.Add(new Entity.CustomerMapping()
                        {
                            CIF = customer.CIF,
                            EmployeeID = (long)customer.RM.ID,
                            IsDeleted = false,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().DisplayName
                        });
                    }

                    isSuccess = true;
                    ts.Complete();
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }

            return isSuccess;
        }

        public bool UpdateCustomer(string cif, CustomerModel customer, ref string message)
        {
            bool isSuccess = false;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    bool isUpdateCustomerMapping = false;
                    long? oldIDEmplyee = null;
                    Entity.Customer data = context.Customers.Where(a => a.CIF == customer.CIF).FirstOrDefault();

                    if (data != null)
                    {
                        if (customer.RM.ID != null & data.EmployeeID != customer.RM.ID)
                        {
                            isUpdateCustomerMapping = true;
                            oldIDEmplyee = data.EmployeeID;
                        }
                        //data.CIF = customer.CIF;
                        data.CustomerName = customer.Name;
                        data.POAName = customer.POAName;
                        data.BizSegmentID = customer.BizSegment.ID;
                        data.LocationID = customer.Branch.ID;
                        data.EmployeeID = customer.RM.ID;
                        data.CustomerTypeID = customer.Type.ID;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();
                    }
                    else
                    {
                        message = string.Format("Customer data for CIF number {0} is does not exist.", customer.CIF);
                    }
                    if (isUpdateCustomerMapping && customer.RM.ID != null)
                    {
                        context.CustomerMappings.Add(new Entity.CustomerMapping()
                        {
                            CIF = customer.CIF,
                            EmployeeID = (long)customer.RM.ID,
                            IsDeleted = false,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().DisplayName
                        });
                        context.SaveChanges(); // insert data

                        if (oldIDEmplyee != null)
                        {
                            Entity.CustomerMapping dataMapping = context.CustomerMappings.Where(x => x.CIF == customer.CIF && x.EmployeeID == oldIDEmplyee && x.IsDeleted == false).FirstOrDefault();
                            // var dataMapping = context.CustomerMappings.Where(x => x.CIF == customer.CIF && x.EmployeeID.Equals(customer.RM.ID) && x.IsDeleted.Equals(false)).FirstOrDefault();
                            if (dataMapping != null)
                            {
                                dataMapping.IsDeleted = true;
                                dataMapping.UpdateDate = DateTime.UtcNow;
                                dataMapping.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                context.SaveChanges();
                            }
                        }
                    }
                    isSuccess = true;
                    ts.Complete();
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }
            return isSuccess;
        }

        public bool DeleteCustomer(string cif, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Customer data = context.Customers.Where(a => a.CIF.Equals(cif)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Customer data for CIF number {0} is does not exist.", cif);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


        public bool GetCustomerUnderlyingsByCIF(long TransactionDealID, ref CustomerModel customer, ref string message)
        {
            bool isSuccess = false;
            DateTime currentdate = DateTime.Now;
            try
            {
                if (GetCustomerByTransactionReviseDealMaker(TransactionDealID, ref customer, ref message))
                {
                    isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                        context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public bool GetCustomerByTransaction(long TransactionID, ref CustomerModel customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                customer = (from a in context.Customers
                            join b in context.Transactions on a.CIF equals b.CIF
                            where a.IsDeleted.Equals(false)
                            && b.TransactionID == TransactionID
                            select new CustomerModel
                            {
                                CIF = a.CIF,
                                Name = a.CustomerName,
                                POAName = a.POAName,

                                BizSegment = new BizSegmentModel()
                                {
                                    ID = a.BizSegment.BizSegmentID,
                                    Name = a.BizSegment.BizSegmentName,
                                    Description = a.BizSegment.BizSegmentDescription,
                                    LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                    LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                },

                                RM = a.CustomerMappings.Where(x => x.IsDeleted.Equals(false)).Select(x => new RMModel()
                                {
                                    ID = x.Employee.EmployeeID,
                                    Name = x.Employee.EmployeeName,
                                    SegmentID = x.Employee.BizSegmentID,
                                    BranchID = x.Employee.LocationID,
                                    RankID = x.Employee.RankID,
                                    Email = x.Employee.EmployeeEmail,
                                    LastModifiedBy = x.Employee.UpdateDate == null ? x.Employee.CreateBy : x.Employee.UpdateBy,
                                    LastModifiedDate = x.Employee.UpdateDate == null ? x.Employee.CreateDate : x.Employee.UpdateDate.Value
                                }).FirstOrDefault(),

                                Underlyings = context.TransactionUnderlyings.Where(xx => xx.TransactionID == b.TransactionID && xx.IsDeleted == false).Join(context.CustomerUnderlyings, tu => tu.UnderlyingID, cu => cu.UnderlyingID, (tu, cu) => new CustomerUnderlyingModel()
                                {
                                    ID = cu.UnderlyingID,
                                    StatementLetter = new StatementLetterModel()
                                    {
                                        ID = cu.StatementLetter.StatementLetterID,
                                        Name = cu.StatementLetter.StatementLetterName,
                                        LastModifiedBy = cu.StatementLetter.UpdateDate == null ? cu.StatementLetter.CreateBy : cu.StatementLetter.UpdateBy,
                                        LastModifiedDate = cu.StatementLetter.UpdateDate == null ? cu.StatementLetter.CreateDate : cu.StatementLetter.UpdateDate.Value
                                    },
                                    Amount = cu.Amount,
                                    Rate = cu.Rate,
                                    AmountUSD = cu.AmountUSD,
                                    AvailableAmount = cu.AvailableAmountUSD,
                                    AttachmentNo = cu.AttachmentNo,
                                    Currency = new CurrencyModel()
                                    {
                                        ID = cu.Currency.CurrencyID,
                                        Code = cu.Currency.CurrencyCode,
                                        Description = cu.Currency.CurrencyDescription,
                                        LastModifiedBy = cu.Currency.UpdateDate == null ? cu.Currency.CreateBy : cu.Currency.UpdateBy,
                                        LastModifiedDate = cu.Currency.UpdateDate == null ? cu.Currency.CreateDate : cu.Currency.UpdateDate.Value
                                    },
                                    DateOfUnderlying = cu.DateOfUnderlying,
                                    DocumentType = new DocumentTypeModel()
                                    {
                                        ID = cu.DocumentType.DocTypeID,
                                        Name = cu.DocumentType.DocTypeName,
                                        Description = cu.DocumentType.DocTypeDescription,
                                        LastModifiedBy = cu.DocumentType.UpdateDate == null ? cu.DocumentType.CreateBy : cu.DocumentType.UpdateBy,
                                        LastModifiedDate = cu.DocumentType.UpdateDate == null ? cu.DocumentType.CreateDate : cu.DocumentType.UpdateDate.Value
                                    },
                                    EndDate = cu.EndDate,
                                    ExpiredDate = cu.ExpiredDate,
                                    InvoiceNumber = cu.InvoiceNumber,
                                    ReferenceNumber = cu.ReferenceNumber,
                                    StartDate = cu.StartDate,
                                    SupplierName = cu.SupplierName,
                                    UnderlyingDocument = new UnderlyingDocModel()
                                    {
                                        ID = cu.UnderlyingDocument.UnderlyingDocID,
                                        Code = cu.UnderlyingDocument.UnderlyingDocCode,
                                        Name = cu.UnderlyingDocument.UnderlyingDocName,
                                        LastModifiedBy = cu.UnderlyingDocument.UpdateDate == null ? cu.UnderlyingDocument.CreateBy : cu.UnderlyingDocument.UpdateBy,
                                        LastModifiedDate = cu.UnderlyingDocument.UpdateDate == null ? cu.UnderlyingDocument.CreateDate : cu.UnderlyingDocument.UpdateDate.Value
                                    },
                                    IsDeclarationOfException = cu.IsDeclarationOfExecption,
                                    IsUtilize = cu.IsUtilize,
                                    IsEnable = false,
                                    IsProforma = cu.IsProforma,
                                    IsBulkUnderlying = cu.IsBulkUnderlying.Value,
                                    IsJointAccount = cu.IsJointAccount.HasValue ? cu.IsJointAccount.Value : false,
                                    IsTMO = cu.IsTMO.HasValue ? cu.IsTMO.Value : false,
                                    AccountNumber = cu.AccountNumber,
                                    ProformaDetails = (from cuy in context.CustomerUnderlyings
                                                       join cm in context.MappingUnderlyings.Where(x => x.IsDeleted.Equals(false)) on cuy.UnderlyingID equals cm.UnderlyingID
                                                       where cm.ParentID == cu.UnderlyingID
                                                       select new CustomerUnderlyingDetailModel
                                                       {
                                                           StatementLetter = new StatementLetterModel()
                                                           {
                                                               ID = cuy.StatementLetter.StatementLetterID,
                                                               Name = cuy.StatementLetter.StatementLetterName,
                                                               LastModifiedBy = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateBy : cuy.StatementLetter.UpdateBy,
                                                               LastModifiedDate = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateDate : cuy.StatementLetter.UpdateDate.Value
                                                           },
                                                           Amount = cuy.Amount,
                                                           Rate = cuy.Rate,
                                                           AmountUSD = cuy.AmountUSD,
                                                           AvailableAmount = cuy.AvailableAmountUSD,
                                                           AttachmentNo = cuy.AttachmentNo,
                                                           Currency = new CurrencyModel()
                                                           {
                                                               ID = cuy.Currency.CurrencyID,
                                                               Code = cuy.Currency.CurrencyCode,
                                                               Description = cuy.Currency.CurrencyDescription,
                                                               LastModifiedBy = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateBy : cuy.Currency.UpdateBy,
                                                               LastModifiedDate = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateDate : cuy.Currency.UpdateDate.Value
                                                           },
                                                           DateOfUnderlying = cuy.DateOfUnderlying,
                                                           DocumentType = new DocumentTypeModel()
                                                           {
                                                               ID = cuy.DocumentType.DocTypeID,
                                                               Name = cuy.DocumentType.DocTypeName,
                                                               Description = cuy.DocumentType.DocTypeDescription,
                                                               LastModifiedBy = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateBy : cuy.DocumentType.UpdateBy,
                                                               LastModifiedDate = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateDate : cuy.DocumentType.UpdateDate.Value
                                                           },
                                                           InvoiceNumber = cuy.InvoiceNumber,
                                                           ReferenceNumber = cuy.ReferenceNumber,
                                                           SupplierName = cuy.SupplierName,
                                                           ExpiredDate = cuy.ExpiredDate,
                                                           UnderlyingDocument = new UnderlyingDocModel()
                                                           {
                                                               ID = cuy.UnderlyingDocument.UnderlyingDocID,
                                                               Code = cuy.UnderlyingDocument.UnderlyingDocCode,
                                                               Name = cuy.UnderlyingDocument.UnderlyingDocName,
                                                               LastModifiedBy = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateBy : cuy.UnderlyingDocument.UpdateBy,
                                                               LastModifiedDate = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateDate : cuy.UnderlyingDocument.UpdateDate.Value
                                                           }
                                                       }).ToList(),
                                    BulkDetails = (from cuy in context.CustomerUnderlyings
                                                   join cm in context.MappingBulkUnderlyings.Where(x => x.IsDeleted.Equals(false)) on cuy.UnderlyingID equals cm.UnderlyingID
                                                   where cm.ParentID == cu.UnderlyingID
                                                   select new CustomerUnderlyingDetailModel
                                                   {
                                                       StatementLetter = new StatementLetterModel()
                                                       {
                                                           ID = cuy.StatementLetter.StatementLetterID,
                                                           Name = cuy.StatementLetter.StatementLetterName,
                                                           LastModifiedBy = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateBy : cuy.StatementLetter.UpdateBy,
                                                           LastModifiedDate = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateDate : cuy.StatementLetter.UpdateDate.Value
                                                       },
                                                       Amount = cuy.Amount,
                                                       Rate = cuy.Rate,
                                                       AmountUSD = cuy.AmountUSD,
                                                       AvailableAmount = cuy.AvailableAmountUSD,
                                                       AttachmentNo = cuy.AttachmentNo,
                                                       Currency = new CurrencyModel()
                                                       {
                                                           ID = cuy.Currency.CurrencyID,
                                                           Code = cuy.Currency.CurrencyCode,
                                                           Description = cuy.Currency.CurrencyDescription,
                                                           LastModifiedBy = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateBy : cuy.Currency.UpdateBy,
                                                           LastModifiedDate = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateDate : cuy.Currency.UpdateDate.Value
                                                       },
                                                       DateOfUnderlying = cuy.DateOfUnderlying,
                                                       DocumentType = new DocumentTypeModel()
                                                       {
                                                           ID = cuy.DocumentType.DocTypeID,
                                                           Name = cuy.DocumentType.DocTypeName,
                                                           Description = cuy.DocumentType.DocTypeDescription,
                                                           LastModifiedBy = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateBy : cuy.DocumentType.UpdateBy,
                                                           LastModifiedDate = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateDate : cuy.DocumentType.UpdateDate.Value
                                                       },
                                                       InvoiceNumber = cuy.InvoiceNumber,
                                                       ReferenceNumber = cuy.ReferenceNumber,
                                                       SupplierName = cuy.SupplierName,
                                                       ExpiredDate = cuy.ExpiredDate,
                                                       UnderlyingDocument = new UnderlyingDocModel()
                                                       {
                                                           ID = cuy.UnderlyingDocument.UnderlyingDocID,
                                                           Code = cuy.UnderlyingDocument.UnderlyingDocCode,
                                                           Name = cuy.UnderlyingDocument.UnderlyingDocName,
                                                           LastModifiedBy = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateBy : cuy.UnderlyingDocument.UpdateBy,
                                                           LastModifiedDate = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateDate : cuy.UnderlyingDocument.UpdateDate.Value
                                                       }
                                                   }).ToList(),
                                    LastModifiedBy = cu.UpdateDate == null ? cu.CreateBy : cu.UpdateBy,
                                    LastModifiedDate = cu.UpdateDate == null ? cu.CreateDate : cu.UpdateDate.Value
                                }).ToList(),
                                LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                            }).SingleOrDefault();

                if (customer != null)
                {
                    string _CIF = customer.CIF;
                    var CustomerModel = (from a in context.Customers
                                         where a.CIF == _CIF
                                         select a).SingleOrDefault();
                    if (CustomerModel.CustomerType != null)
                    {
                        customer.Type = new CustomerTypeModel()
                        {
                            ID = CustomerModel.CustomerType.CustomerTypeID,
                            Name = CustomerModel.CustomerType.CustomerTypeName,
                            Description = CustomerModel.CustomerType.CustomerTypeDescription,
                            LastModifiedBy = CustomerModel.CustomerType.UpdateDate == null ? CustomerModel.CustomerType.CreateBy : CustomerModel.CustomerType.UpdateBy,
                            LastModifiedDate = CustomerModel.CustomerType.UpdateDate == null ? CustomerModel.CustomerType.CreateDate : CustomerModel.CustomerType.UpdateDate.Value
                        };
                    }

                    if (CustomerModel.Location != null)
                    {
                        customer.Branch = new BranchModel()
                        {
                            ID = CustomerModel.Location.LocationID != null ? CustomerModel.Location.LocationID : 0,//add by fandi
                            Name = CustomerModel.Location.LocationName != null ? CustomerModel.Location.LocationName : "-",//add by fandi 
                            LastModifiedBy = CustomerModel.Location.UpdateDate == null ? CustomerModel.Location.CreateBy : CustomerModel.Location.UpdateBy,
                            LastModifiedDate = CustomerModel.Location.UpdateDate == null ? CustomerModel.Location.CreateDate : CustomerModel.Location.UpdateDate.Value
                        };
                    }
                    //1-3-2016 by dani
                    else
                    {
                        customer.Branch = new BranchModel()
                        {
                            ID = 0,//add by dani
                            Name = "-",//add by dani 
                            LastModifiedBy = "",
                            LastModifiedDate = null
                        };
                    }
                    customer.Contacts = (from cc in context.SP_GETContactCallBack(_CIF)
                                         select new CustomerContactModel()
                                         {
                                             ID = cc.ContactID,
                                             Name = cc.ContactName,
                                             IDNumber = cc.IdentificationNumber,
                                             DateOfBirth = cc.DateOfBirth,
                                             CIF = cc.CIF,
                                             PhoneNumber = cc.PhoneNumber,
                                             OccupationInID = cc.OccupationInID,
                                             //POAFunction = context.POAFunctions.Where(pc => pc.IsDeleted.Equals(false) && pc.POAFunctionID.Equals(cc.POAFunctionID.Value)).Select(pc => new POAFunctionModel()
                                             //{
                                             //    ID = pc.POAFunctionID,
                                             //    Name = pc.POAFunctionName,
                                             //    Description = (pc.POAFunctionID == 9 ? cc.POAFunctionOther : pc.POAFunctionDescription)
                                             //}).FirstOrDefault(),
                                             POAFunctionVW = cc.POADescription,
                                             PlaceOfBirth = cc.PlaceOfBirth,
                                             EffectiveDate = cc.EffectiveDate,
                                             CancellationDate = cc.CancellationDate,
                                             LastModifiedBy = cc.UpdateDate == null ? cc.CreateBy : cc.UpdateBy,
                                             LastModifiedDate = cc.UpdateDate == null ? cc.CreateDate : cc.UpdateDate.Value,
                                         }).ToList();

                    customer.Accounts = (from j in context.SP_GETJointAccount(_CIF)
                                         select new CustomerAccountModel
                                         {
                                             AccountNumber = j.AccountNumber,
                                             Currency = new CurrencyModel
                                             {
                                                 ID = j.CurrencyID.Value,
                                                 Code = j.CurrencyCode,
                                                 Description = j.CurrencyDescription
                                             },
                                             CustomerName = j.CustomerName,
                                             IsJointAccount = j.IsJointAccount
                                         }).ToList();

                }
                else
                {
                    message = "Customer empty data or has been deleted.";
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerByTransactionReviseMaker(long TransactionID, ref CustomerModel customer, ref string message)
        {
            bool isSuccess = false;
            DateTime currentdate = DateTime.Now;
            try
            {
                var TransactionUnderlying = context.TransactionUnderlyings.Where(x => x.TransactionID == TransactionID && x.IsDeleted == false);
                customer = (from a in context.Customers
                            join b in context.Transactions on a.CIF equals b.CIF
                            where a.IsDeleted.Equals(false)
                            && b.TransactionID == TransactionID
                            select new CustomerModel
                            {
                                CIF = a.CIF,
                                Name = a.CustomerName,
                                POAName = a.POAName,

                                BizSegment = new BizSegmentModel()
                                {
                                    ID = a.BizSegment.BizSegmentID,
                                    Name = a.BizSegment.BizSegmentName,
                                    Description = a.BizSegment.BizSegmentDescription,
                                    LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                    LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                },

                                RM = a.CustomerMappings.Where(x => x.IsDeleted.Equals(false)).Select(x => new RMModel()
                                {
                                    ID = x.Employee.EmployeeID,
                                    Name = x.Employee.EmployeeName,
                                    SegmentID = x.Employee.BizSegmentID,
                                    BranchID = x.Employee.LocationID,
                                    RankID = x.Employee.RankID,
                                    Email = x.Employee.EmployeeEmail,
                                    LastModifiedBy = x.Employee.UpdateDate == null ? x.Employee.CreateBy : x.Employee.UpdateBy,
                                    LastModifiedDate = x.Employee.UpdateDate == null ? x.Employee.CreateDate : x.Employee.UpdateDate.Value
                                }).FirstOrDefault(),
                                LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                            }).SingleOrDefault();

                if (customer != null)
                {

                    string _CIF = customer.CIF;
                    var CustomerModel = (from a in context.Customers
                                         where a.CIF == _CIF
                                         select a).SingleOrDefault();
                    if (CustomerModel.CustomerType != null)
                    {
                        customer.Type = new CustomerTypeModel()
                        {
                            ID = CustomerModel.CustomerType.CustomerTypeID,
                            Name = CustomerModel.CustomerType.CustomerTypeName,
                            Description = CustomerModel.CustomerType.CustomerTypeDescription,
                            LastModifiedBy = CustomerModel.CustomerType.UpdateDate == null ? CustomerModel.CustomerType.CreateBy : CustomerModel.CustomerType.UpdateBy,
                            LastModifiedDate = CustomerModel.CustomerType.UpdateDate == null ? CustomerModel.CustomerType.CreateDate : CustomerModel.CustomerType.UpdateDate.Value
                        };
                    }

                    if (CustomerModel.Location != null)
                    {
                        customer.Branch = new BranchModel()
                        {
                            ID = CustomerModel.Location.LocationID,
                            Name = CustomerModel.Location.LocationName,
                            LastModifiedBy = CustomerModel.Location.UpdateDate == null ? CustomerModel.Location.CreateBy : CustomerModel.Location.UpdateBy,
                            LastModifiedDate = CustomerModel.Location.UpdateDate == null ? CustomerModel.Location.CreateDate : CustomerModel.Location.UpdateDate.Value
                        };
                    }

                    if (CustomerModel.CustomerContacts != null)
                    {
                        customer.Contacts = new List<CustomerContactModel>();

                        foreach (var item in CustomerModel.CustomerContacts)
                        {
                            CustomerContactModel contact = new CustomerContactModel();
                            contact.ID = item.ContactID;
                            contact.Name = item.ContactName;
                            contact.IDNumber = item.IdentificationNumber;
                            contact.DateOfBirth = item.DateOfBirth;
                            contact.CIF = item.CIF;
                            //Address = x.Address,
                            contact.PhoneNumber = item.PhoneNumber;
                            contact.OccupationInID = item.OccupationInId;
                            contact.POAFunction = item.POAFunctionID != null ? new POAFunctionModel
                            {
                                ID = item.POAFunction.POAFunctionID,
                                Name = item.POAFunction.POAFunctionName,
                                Description = (item.POAFunction.POAFunctionID == 9 ? item.POAFunctionOther : item.POAFunction.POAFunctionDescription),//item.POAFunction.POAFunctionDescription
                            } : new POAFunctionModel();
                            contact.PlaceOfBirth = item.PlaceOfBirth;
                            contact.EffectiveDate = item.EffectiveDate;
                            contact.CancellationDate = item.CancellationDate;
                            contact.LastModifiedBy = item.UpdateDate == null ? item.CreateBy : item.UpdateBy;
                            contact.LastModifiedDate = item.UpdateDate == null ? item.CreateDate : item.UpdateDate.Value;
                            customer.Contacts.Add(contact);
                        }
                    }
                    customer.Accounts = (from j in context.SP_GETJointAccount(_CIF)
                                         select new CustomerAccountModel
                                         {
                                             AccountNumber = j.AccountNumber,
                                             Currency = new CurrencyModel
                                             {
                                                 ID = j.CurrencyID.Value,
                                                 Code = j.CurrencyCode,
                                                 Description = j.CurrencyDescription
                                             },
                                             CustomerName = j.CustomerName,
                                             IsJointAccount = j.IsJointAccount.HasValue ? j.IsJointAccount.Value : false
                                         }).ToList();
                    //}

                    IList<string> accountNumber = new List<string>();
                    if (customer.Accounts != null)
                    {
                        accountNumber = customer.Accounts.Where(x => x.IsJointAccount.Equals(true)).Select(x => x.AccountNumber).ToList();
                    }

                    customer.Underlyings = (from cu in context.CustomerUnderlyings.Where(x => x.CIF.Equals(_CIF) || (accountNumber.Count != 0 ? accountNumber.Contains(x.AccountNumber) : false))
                                            join trans in TransactionUnderlying.Where(x => x.TransactionID.Equals(TransactionID)) on cu.UnderlyingID equals trans.UnderlyingID into grp
                                            from subTrans in grp.DefaultIfEmpty()
                                            where cu.IsDeleted.Equals(false) && (DbFunctions.TruncateTime(cu.ExpiredDate) >= DbFunctions.TruncateTime(currentdate) || subTrans != null)
                                            && (cu.AvailableAmountUSD > 0 || subTrans != null)
                                            && (cu.IsSelectedBulk == null || cu.IsSelectedBulk.Value == false)
                                            select new CustomerUnderlyingModel()
                                            {
                                                ID = cu.UnderlyingID,
                                                StatementLetter = new StatementLetterModel()
                                                {
                                                    ID = cu.StatementLetter.StatementLetterID,
                                                    Name = cu.StatementLetter.StatementLetterName,
                                                    LastModifiedBy = cu.StatementLetter.UpdateDate == null ? cu.StatementLetter.CreateBy : cu.StatementLetter.UpdateBy,
                                                    LastModifiedDate = cu.StatementLetter.UpdateDate == null ? cu.StatementLetter.CreateDate : cu.StatementLetter.UpdateDate.Value
                                                },
                                                Amount = cu.Amount,
                                                Rate = cu.Rate,
                                                AmountUSD = cu.AmountUSD,
                                                //AvailableAmount = cu.AvailableAmountUSD + (subTrans == null ? 0 : subTrans.Amount),
                                                AvailableAmount = cu.AvailableAmountUSD,
                                                AttachmentNo = cu.AttachmentNo,
                                                Currency = new CurrencyModel()
                                                {
                                                    ID = cu.Currency.CurrencyID,
                                                    Code = cu.Currency.CurrencyCode,
                                                    Description = cu.Currency.CurrencyDescription,
                                                    LastModifiedBy = cu.Currency.UpdateDate == null ? cu.Currency.CreateBy : cu.Currency.UpdateBy,
                                                    LastModifiedDate = cu.Currency.UpdateDate == null ? cu.Currency.CreateDate : cu.Currency.UpdateDate.Value
                                                },
                                                DateOfUnderlying = cu.DateOfUnderlying,
                                                DocumentType = new DocumentTypeModel()
                                                {
                                                    ID = cu.DocumentType.DocTypeID,
                                                    Name = cu.DocumentType.DocTypeName,
                                                    Description = cu.DocumentType.DocTypeDescription,
                                                    LastModifiedBy = cu.DocumentType.UpdateDate == null ? cu.DocumentType.CreateBy : cu.DocumentType.UpdateBy,
                                                    LastModifiedDate = cu.DocumentType.UpdateDate == null ? cu.DocumentType.CreateDate : cu.DocumentType.UpdateDate.Value
                                                },
                                                EndDate = cu.EndDate,
                                                ExpiredDate = cu.ExpiredDate,
                                                InvoiceNumber = cu.InvoiceNumber,
                                                ReferenceNumber = cu.ReferenceNumber,
                                                StartDate = cu.StartDate,
                                                SupplierName = cu.SupplierName,
                                                UnderlyingDocument = new UnderlyingDocModel()
                                                {
                                                    ID = cu.UnderlyingDocument.UnderlyingDocID,
                                                    Code = cu.UnderlyingDocument.UnderlyingDocCode,
                                                    Name = cu.UnderlyingDocument.UnderlyingDocName,
                                                    LastModifiedBy = cu.UnderlyingDocument.UpdateDate == null ? cu.UnderlyingDocument.CreateBy : cu.UnderlyingDocument.UpdateBy,
                                                    LastModifiedDate = cu.UnderlyingDocument.UpdateDate == null ? cu.UnderlyingDocument.CreateDate : cu.UnderlyingDocument.UpdateDate.Value
                                                },
                                                IsDeclarationOfException = cu.IsDeclarationOfExecption,
                                                IsUtilize = cu.IsUtilize,
                                                IsProforma = cu.IsProforma,
                                                IsEnable = (subTrans == null ? false : true),
                                                IsJointAccount = cu.IsJointAccount.HasValue ? cu.IsJointAccount.Value : false,
                                                IsTMO = cu.IsTMO.HasValue ? cu.IsTMO.Value : false,
                                                AccountNumber = cu.AccountNumber,
                                                IsBulkUnderlying = cu.IsBulkUnderlying.Value,
                                                ProformaDetails = (from cuy in context.CustomerUnderlyings
                                                                   join cm in context.MappingUnderlyings.Where(x => x.IsDeleted.Equals(false)) on cuy.UnderlyingID equals cm.UnderlyingID
                                                                   where cm.ParentID == cu.UnderlyingID
                                                                   select new CustomerUnderlyingDetailModel
                                                                   {
                                                                       StatementLetter = new StatementLetterModel()
                                                                       {
                                                                           ID = cuy.StatementLetter.StatementLetterID,
                                                                           Name = cuy.StatementLetter.StatementLetterName,
                                                                           LastModifiedBy = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateBy : cuy.StatementLetter.UpdateBy,
                                                                           LastModifiedDate = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateDate : cuy.StatementLetter.UpdateDate.Value
                                                                       },
                                                                       Amount = cuy.Amount,
                                                                       Rate = cuy.Rate,
                                                                       AmountUSD = cuy.AmountUSD,
                                                                       AvailableAmount = cuy.AvailableAmountUSD,
                                                                       AttachmentNo = cuy.AttachmentNo,
                                                                       Currency = new CurrencyModel()
                                                                       {
                                                                           ID = cuy.Currency.CurrencyID,
                                                                           Code = cuy.Currency.CurrencyCode,
                                                                           Description = cuy.Currency.CurrencyDescription,
                                                                           LastModifiedBy = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateBy : cuy.Currency.UpdateBy,
                                                                           LastModifiedDate = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateDate : cuy.Currency.UpdateDate.Value
                                                                       },
                                                                       DateOfUnderlying = cuy.DateOfUnderlying,
                                                                       DocumentType = new DocumentTypeModel()
                                                                       {
                                                                           ID = cuy.DocumentType.DocTypeID,
                                                                           Name = cuy.DocumentType.DocTypeName,
                                                                           Description = cuy.DocumentType.DocTypeDescription,
                                                                           LastModifiedBy = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateBy : cuy.DocumentType.UpdateBy,
                                                                           LastModifiedDate = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateDate : cuy.DocumentType.UpdateDate.Value
                                                                       },
                                                                       InvoiceNumber = cuy.InvoiceNumber,
                                                                       ReferenceNumber = cuy.ReferenceNumber,
                                                                       SupplierName = cuy.SupplierName,
                                                                       ExpiredDate = cuy.ExpiredDate,
                                                                       UnderlyingDocument = new UnderlyingDocModel()
                                                                       {
                                                                           ID = cuy.UnderlyingDocument.UnderlyingDocID,
                                                                           Code = cuy.UnderlyingDocument.UnderlyingDocCode,
                                                                           Name = cuy.UnderlyingDocument.UnderlyingDocName,
                                                                           LastModifiedBy = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateBy : cuy.UnderlyingDocument.UpdateBy,
                                                                           LastModifiedDate = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateDate : cuy.UnderlyingDocument.UpdateDate.Value
                                                                       }
                                                                   }).ToList(),
                                                BulkDetails = (from cuy in context.CustomerUnderlyings
                                                               join cm in context.MappingBulkUnderlyings.Where(x => x.IsDeleted.Equals(false)) on cuy.UnderlyingID equals cm.UnderlyingID
                                                               where cm.ParentID == cu.UnderlyingID
                                                               select new CustomerUnderlyingDetailModel
                                                               {
                                                                   StatementLetter = new StatementLetterModel()
                                                                   {
                                                                       ID = cuy.StatementLetter.StatementLetterID,
                                                                       Name = cuy.StatementLetter.StatementLetterName,
                                                                       LastModifiedBy = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateBy : cuy.StatementLetter.UpdateBy,
                                                                       LastModifiedDate = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateDate : cuy.StatementLetter.UpdateDate.Value
                                                                   },
                                                                   Amount = cuy.Amount,
                                                                   Rate = cuy.Rate,
                                                                   AmountUSD = cuy.AmountUSD,
                                                                   AvailableAmount = cuy.AvailableAmountUSD,
                                                                   AttachmentNo = cuy.AttachmentNo,
                                                                   Currency = new CurrencyModel()
                                                                   {
                                                                       ID = cuy.Currency.CurrencyID,
                                                                       Code = cuy.Currency.CurrencyCode,
                                                                       Description = cuy.Currency.CurrencyDescription,
                                                                       LastModifiedBy = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateBy : cuy.Currency.UpdateBy,
                                                                       LastModifiedDate = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateDate : cuy.Currency.UpdateDate.Value
                                                                   },
                                                                   DateOfUnderlying = cuy.DateOfUnderlying,
                                                                   DocumentType = new DocumentTypeModel()
                                                                   {
                                                                       ID = cuy.DocumentType.DocTypeID,
                                                                       Name = cuy.DocumentType.DocTypeName,
                                                                       Description = cuy.DocumentType.DocTypeDescription,
                                                                       LastModifiedBy = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateBy : cuy.DocumentType.UpdateBy,
                                                                       LastModifiedDate = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateDate : cuy.DocumentType.UpdateDate.Value
                                                                   },
                                                                   InvoiceNumber = cuy.InvoiceNumber,
                                                                   ReferenceNumber = cuy.ReferenceNumber,
                                                                   SupplierName = cuy.SupplierName,
                                                                   ExpiredDate = cuy.ExpiredDate,
                                                                   UnderlyingDocument = new UnderlyingDocModel()
                                                                   {
                                                                       ID = cuy.UnderlyingDocument.UnderlyingDocID,
                                                                       Code = cuy.UnderlyingDocument.UnderlyingDocCode,
                                                                       Name = cuy.UnderlyingDocument.UnderlyingDocName,
                                                                       LastModifiedBy = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateBy : cuy.UnderlyingDocument.UpdateBy,
                                                                       LastModifiedDate = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateDate : cuy.UnderlyingDocument.UpdateDate.Value
                                                                   }
                                                               }).ToList(),
                                                LastModifiedBy = cu.UpdateDate == null ? cu.CreateBy : cu.UpdateBy,
                                                LastModifiedDate = cu.UpdateDate == null ? cu.CreateDate : cu.UpdateDate.Value
                                            }).ToList();


                }
                else
                {
                    message = "Customer empty data or has been deleted.";
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerByTransactionReviseDealMaker(long TransactionDealID, ref CustomerModel customer, ref string message)
        {
            bool isSuccess = false;
            DateTime currentdate = DateTime.Now;
            try
            {
                var TransactionDealUnderlying = context.TransactionDealUnderlyings.Where(x => x.TransactionDealID == TransactionDealID && x.IsDeleted.Equals(false));
                customer = (from a in context.Customers
                            join b in context.TransactionDeals on a.CIF equals b.CIF
                            where a.IsDeleted.Equals(false)
                            && b.TransactionDealID == TransactionDealID
                            select new CustomerModel
                            {
                                CIF = a.CIF,
                                Name = a.CustomerName,
                                POAName = a.POAName,

                                BizSegment = new BizSegmentModel()
                                {
                                    ID = a.BizSegment.BizSegmentID,
                                    Name = a.BizSegment.BizSegmentName,
                                    Description = a.BizSegment.BizSegmentDescription,
                                    LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                    LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                },

                                RM = a.CustomerMappings.Where(x => x.IsDeleted.Equals(false)).Select(x => new RMModel()
                                {
                                    ID = x.Employee.EmployeeID,
                                    Name = x.Employee.EmployeeName,
                                    SegmentID = x.Employee.BizSegmentID,
                                    BranchID = x.Employee.LocationID,
                                    RankID = x.Employee.RankID,
                                    Email = x.Employee.EmployeeEmail,
                                    LastModifiedBy = x.Employee.UpdateDate == null ? x.Employee.CreateBy : x.Employee.UpdateBy,
                                    LastModifiedDate = x.Employee.UpdateDate == null ? x.Employee.CreateDate : x.Employee.UpdateDate.Value
                                }).FirstOrDefault(),
                                LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                            }).SingleOrDefault();

                if (customer != null)
                {
                    // customer.Accounts.Add(new CustomerAccountModel() { AccountNumber = "-", Currency = new CurrencyModel() { ID = 0, Code = "-" } });

                    string _CIF = customer.CIF;
                    var CustomerModel = (from a in context.Customers
                                         where a.CIF == _CIF
                                         select a).SingleOrDefault();
                    if (CustomerModel.CustomerType != null)
                    {
                        customer.Type = new CustomerTypeModel()
                        {
                            ID = CustomerModel.CustomerType.CustomerTypeID,
                            Name = CustomerModel.CustomerType.CustomerTypeName,
                            Description = CustomerModel.CustomerType.CustomerTypeDescription,
                            LastModifiedBy = CustomerModel.CustomerType.UpdateDate == null ? CustomerModel.CustomerType.CreateBy : CustomerModel.CustomerType.UpdateBy,
                            LastModifiedDate = CustomerModel.CustomerType.UpdateDate == null ? CustomerModel.CustomerType.CreateDate : CustomerModel.CustomerType.UpdateDate.Value
                        };
                    }

                    if (CustomerModel.Location != null)
                    {
                        customer.Branch = new BranchModel()
                        {
                            ID = CustomerModel.Location.LocationID,
                            Name = CustomerModel.Location.LocationName,
                            LastModifiedBy = CustomerModel.Location.UpdateDate == null ? CustomerModel.Location.CreateBy : CustomerModel.Location.UpdateBy,
                            LastModifiedDate = CustomerModel.Location.UpdateDate == null ? CustomerModel.Location.CreateDate : CustomerModel.Location.UpdateDate.Value
                        };
                    }

                    if (CustomerModel.CustomerContacts != null)
                    {
                        customer.Contacts = new List<CustomerContactModel>();

                        foreach (var item in CustomerModel.CustomerContacts)
                        {
                            CustomerContactModel contact = new CustomerContactModel();
                            contact.ID = item.ContactID;
                            contact.Name = item.ContactName;
                            contact.IDNumber = item.IdentificationNumber;
                            contact.DateOfBirth = item.DateOfBirth;
                            contact.CIF = item.CIF;
                            //Address = x.Address,
                            contact.PhoneNumber = item.PhoneNumber;
                            contact.OccupationInID = item.OccupationInId;
                            contact.POAFunction = item.POAFunctionID != null ? new POAFunctionModel
                            {
                                ID = item.POAFunction.POAFunctionID,
                                Name = item.POAFunction.POAFunctionName,
                                Description = (item.POAFunction.POAFunctionID == 9 ? item.POAFunctionOther : item.POAFunction.POAFunctionDescription),//item.POAFunction.POAFunctionDescription
                            } : new POAFunctionModel();
                            contact.PlaceOfBirth = item.PlaceOfBirth;
                            contact.EffectiveDate = item.EffectiveDate;
                            contact.CancellationDate = item.CancellationDate;
                            contact.LastModifiedBy = item.UpdateDate == null ? item.CreateBy : item.UpdateBy;
                            contact.LastModifiedDate = item.UpdateDate == null ? item.CreateDate : item.UpdateDate.Value;
                            customer.Contacts.Add(contact);
                        }
                    }

                    //if (CustomerModel.CustomerAccounts != null)
                    //{
                    customer.Accounts = (from j in context.SP_GETJointAccount(_CIF)
                                         select new CustomerAccountModel
                                         {
                                             AccountNumber = j.AccountNumber,
                                             Currency = new CurrencyModel
                                             {
                                                 ID = j.CurrencyID.Value,
                                                 Code = j.CurrencyCode,
                                                 Description = j.CurrencyDescription
                                             },
                                             CustomerName = j.CustomerName,
                                             IsJointAccount = j.IsJointAccount
                                         }).ToList();
                    //}
                    IList<string> accountNumber = new List<string>();
                    if (customer.Accounts != null)
                    {
                        accountNumber = customer.Accounts.Where(x => x.IsJointAccount.Equals(true)).Select(x => x.AccountNumber).ToList();
                    }
                    customer.Underlyings = (from cu in context.CustomerUnderlyings.Where(x => x.CIF.Equals(_CIF) || (accountNumber.Count != 0 ? accountNumber.Contains(x.AccountNumber) : false))
                                            join trans in TransactionDealUnderlying on cu.UnderlyingID equals trans.UnderlyingID into grp
                                            from subTrans in grp.DefaultIfEmpty()
                                            where cu.IsDeleted.Equals(false) && (DbFunctions.TruncateTime(cu.ExpiredDate) >= DbFunctions.TruncateTime(currentdate) || subTrans != null)
                                            && (cu.AvailableAmountUSD > 0 || cu.StatementLetterID == 4 || subTrans != null)
                                            && (cu.IsSelectedBulk == null || cu.IsSelectedBulk.Value == false)
                                            select new CustomerUnderlyingModel()
                                            {
                                                ID = cu.UnderlyingID,
                                                StatementLetter = new StatementLetterModel()
                                                {
                                                    ID = cu.StatementLetter.StatementLetterID,
                                                    Name = cu.StatementLetter.StatementLetterName,
                                                    LastModifiedBy = cu.StatementLetter.UpdateDate == null ? cu.StatementLetter.CreateBy : cu.StatementLetter.UpdateBy,
                                                    LastModifiedDate = cu.StatementLetter.UpdateDate == null ? cu.StatementLetter.CreateDate : cu.StatementLetter.UpdateDate.Value
                                                },
                                                Amount = cu.Amount,
                                                Rate = cu.Rate,
                                                AmountUSD = cu.AmountUSD,
                                                AvailableAmount = cu.AvailableAmountUSD, //+ (subTrans == null ? 0 : subTrans.Amount),
                                                AttachmentNo = cu.AttachmentNo,
                                                Currency = new CurrencyModel()
                                                {
                                                    ID = cu.Currency.CurrencyID,
                                                    Code = cu.Currency.CurrencyCode,
                                                    Description = cu.Currency.CurrencyDescription,
                                                    LastModifiedBy = cu.Currency.UpdateDate == null ? cu.Currency.CreateBy : cu.Currency.UpdateBy,
                                                    LastModifiedDate = cu.Currency.UpdateDate == null ? cu.Currency.CreateDate : cu.Currency.UpdateDate.Value
                                                },
                                                DateOfUnderlying = cu.DateOfUnderlying,
                                                DocumentType = new DocumentTypeModel()
                                                {
                                                    ID = cu.DocumentType.DocTypeID,
                                                    Name = cu.DocumentType.DocTypeName,
                                                    Description = cu.DocumentType.DocTypeDescription,
                                                    LastModifiedBy = cu.DocumentType.UpdateDate == null ? cu.DocumentType.CreateBy : cu.DocumentType.UpdateBy,
                                                    LastModifiedDate = cu.DocumentType.UpdateDate == null ? cu.DocumentType.CreateDate : cu.DocumentType.UpdateDate.Value
                                                },
                                                EndDate = cu.EndDate,
                                                ExpiredDate = cu.ExpiredDate,
                                                InvoiceNumber = cu.InvoiceNumber,
                                                ReferenceNumber = cu.ReferenceNumber,
                                                StartDate = cu.StartDate,
                                                SupplierName = cu.SupplierName,
                                                UnderlyingDocument = new UnderlyingDocModel()
                                                {
                                                    ID = cu.UnderlyingDocument.UnderlyingDocID,
                                                    Code = cu.UnderlyingDocument.UnderlyingDocCode,
                                                    Name = cu.UnderlyingDocument.UnderlyingDocName,
                                                    LastModifiedBy = cu.UnderlyingDocument.UpdateDate == null ? cu.UnderlyingDocument.CreateBy : cu.UnderlyingDocument.UpdateBy,
                                                    LastModifiedDate = cu.UnderlyingDocument.UpdateDate == null ? cu.UnderlyingDocument.CreateDate : cu.UnderlyingDocument.UpdateDate.Value
                                                },
                                                IsDeclarationOfException = cu.IsDeclarationOfExecption,
                                                IsUtilize = cu.IsUtilize,
                                                IsProforma = cu.IsProforma,
                                                IsEnable = (subTrans == null ? false : true),
                                                IsJointAccount = cu.IsJointAccount.HasValue ? cu.IsJointAccount.Value : false,
                                                IsTMO = cu.IsTMO.HasValue ? cu.IsTMO.Value : false,
                                                AccountNumber = cu.AccountNumber,
                                                IsBulkUnderlying = cu.IsBulkUnderlying.Value,
                                                ProformaDetails = (from cuy in context.CustomerUnderlyings
                                                                   join cm in context.MappingUnderlyings.Where(x => x.IsDeleted.Equals(false)) on cuy.UnderlyingID equals cm.UnderlyingID
                                                                   where cm.ParentID == cu.UnderlyingID
                                                                   select new CustomerUnderlyingDetailModel
                                                                   {
                                                                       StatementLetter = new StatementLetterModel()
                                                                       {
                                                                           ID = cuy.StatementLetter.StatementLetterID,
                                                                           Name = cuy.StatementLetter.StatementLetterName,
                                                                           LastModifiedBy = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateBy : cuy.StatementLetter.UpdateBy,
                                                                           LastModifiedDate = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateDate : cuy.StatementLetter.UpdateDate.Value
                                                                       },
                                                                       Amount = cuy.Amount,
                                                                       Rate = cuy.Rate,
                                                                       AmountUSD = cuy.AmountUSD,
                                                                       AvailableAmount = cuy.AvailableAmountUSD,
                                                                       AttachmentNo = cuy.AttachmentNo,
                                                                       Currency = new CurrencyModel()
                                                                       {
                                                                           ID = cuy.Currency.CurrencyID,
                                                                           Code = cuy.Currency.CurrencyCode,
                                                                           Description = cuy.Currency.CurrencyDescription,
                                                                           LastModifiedBy = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateBy : cuy.Currency.UpdateBy,
                                                                           LastModifiedDate = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateDate : cuy.Currency.UpdateDate.Value
                                                                       },
                                                                       DateOfUnderlying = cuy.DateOfUnderlying,
                                                                       DocumentType = new DocumentTypeModel()
                                                                       {
                                                                           ID = cuy.DocumentType.DocTypeID,
                                                                           Name = cuy.DocumentType.DocTypeName,
                                                                           Description = cuy.DocumentType.DocTypeDescription,
                                                                           LastModifiedBy = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateBy : cuy.DocumentType.UpdateBy,
                                                                           LastModifiedDate = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateDate : cuy.DocumentType.UpdateDate.Value
                                                                       },
                                                                       InvoiceNumber = cuy.InvoiceNumber,
                                                                       ReferenceNumber = cuy.ReferenceNumber,
                                                                       SupplierName = cuy.SupplierName,
                                                                       ExpiredDate = cuy.ExpiredDate,
                                                                       UnderlyingDocument = new UnderlyingDocModel()
                                                                       {
                                                                           ID = cuy.UnderlyingDocument.UnderlyingDocID,
                                                                           Code = cuy.UnderlyingDocument.UnderlyingDocCode,
                                                                           Name = cuy.UnderlyingDocument.UnderlyingDocName,
                                                                           LastModifiedBy = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateBy : cuy.UnderlyingDocument.UpdateBy,
                                                                           LastModifiedDate = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateDate : cuy.UnderlyingDocument.UpdateDate.Value
                                                                       }
                                                                   }).ToList(),
                                                BulkDetails = (from cuy in context.CustomerUnderlyings
                                                               join cm in context.MappingBulkUnderlyings.Where(x => x.IsDeleted.Equals(false)) on cuy.UnderlyingID equals cm.UnderlyingID
                                                               where cm.ParentID == cu.UnderlyingID
                                                               select new CustomerUnderlyingDetailModel
                                                               {
                                                                   StatementLetter = new StatementLetterModel()
                                                                   {
                                                                       ID = cuy.StatementLetter.StatementLetterID,
                                                                       Name = cuy.StatementLetter.StatementLetterName,
                                                                       LastModifiedBy = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateBy : cuy.StatementLetter.UpdateBy,
                                                                       LastModifiedDate = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateDate : cuy.StatementLetter.UpdateDate.Value
                                                                   },
                                                                   Amount = cuy.Amount,
                                                                   Rate = cuy.Rate,
                                                                   AmountUSD = cuy.AmountUSD,
                                                                   AvailableAmount = cuy.AvailableAmountUSD,
                                                                   AttachmentNo = cuy.AttachmentNo,
                                                                   Currency = new CurrencyModel()
                                                                   {
                                                                       ID = cuy.Currency.CurrencyID,
                                                                       Code = cuy.Currency.CurrencyCode,
                                                                       Description = cuy.Currency.CurrencyDescription,
                                                                       LastModifiedBy = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateBy : cuy.Currency.UpdateBy,
                                                                       LastModifiedDate = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateDate : cuy.Currency.UpdateDate.Value
                                                                   },
                                                                   DateOfUnderlying = cuy.DateOfUnderlying,
                                                                   DocumentType = new DocumentTypeModel()
                                                                   {
                                                                       ID = cuy.DocumentType.DocTypeID,
                                                                       Name = cuy.DocumentType.DocTypeName,
                                                                       Description = cuy.DocumentType.DocTypeDescription,
                                                                       LastModifiedBy = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateBy : cuy.DocumentType.UpdateBy,
                                                                       LastModifiedDate = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateDate : cuy.DocumentType.UpdateDate.Value
                                                                   },
                                                                   InvoiceNumber = cuy.InvoiceNumber,
                                                                   ReferenceNumber = cuy.ReferenceNumber,
                                                                   SupplierName = cuy.SupplierName,
                                                                   ExpiredDate = cuy.ExpiredDate,
                                                                   UnderlyingDocument = new UnderlyingDocModel()
                                                                   {
                                                                       ID = cuy.UnderlyingDocument.UnderlyingDocID,
                                                                       Code = cuy.UnderlyingDocument.UnderlyingDocCode,
                                                                       Name = cuy.UnderlyingDocument.UnderlyingDocName,
                                                                       LastModifiedBy = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateBy : cuy.UnderlyingDocument.UpdateBy,
                                                                       LastModifiedDate = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateDate : cuy.UnderlyingDocument.UpdateDate.Value
                                                                   }
                                                               }).ToList(),
                                                LastModifiedBy = cu.UpdateDate == null ? cu.CreateBy : cu.UpdateBy,
                                                LastModifiedDate = cu.UpdateDate == null ? cu.CreateDate : cu.UpdateDate.Value
                                            }).ToList();
                }

                else
                {
                    message = "Customer empty data or has been deleted.";
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


        public bool GetCustomerByTransactionDeal(long TransactionID, ref CustomerModel customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                customer = (from a in context.Customers
                            join b in context.TransactionDeals on a.CIF equals b.CIF
                            where a.IsDeleted.Equals(false)
                            && b.TransactionDealID == TransactionID
                            select new CustomerModel
                            {
                                CIF = a.CIF,
                                Name = a.CustomerName,
                                POAName = a.POAName,

                                BizSegment = new BizSegmentModel()
                                {
                                    ID = a.BizSegment.BizSegmentID,
                                    Name = a.BizSegment.BizSegmentName,
                                    Description = a.BizSegment.BizSegmentDescription,
                                    LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                    LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                },

                                RM = a.CustomerMappings.Where(x => x.IsDeleted.Equals(false)).Select(x => new RMModel()
                                {
                                    ID = x.Employee.EmployeeID,
                                    Name = x.Employee.EmployeeName,
                                    SegmentID = x.Employee.BizSegmentID,
                                    BranchID = x.Employee.LocationID,
                                    RankID = x.Employee.RankID,
                                    Email = x.Employee.EmployeeEmail,
                                    LastModifiedBy = x.Employee.UpdateDate == null ? x.Employee.CreateBy : x.Employee.UpdateBy,
                                    LastModifiedDate = x.Employee.UpdateDate == null ? x.Employee.CreateDate : x.Employee.UpdateDate.Value
                                }).FirstOrDefault(),
                                LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                            }).SingleOrDefault();

                if (customer != null)
                {
                    string _CIF = customer.CIF;
                    var CustomerModel = (from a in context.Customers
                                         where a.CIF == _CIF
                                         select a).SingleOrDefault();
                    if (CustomerModel.CustomerType != null)
                    {
                        customer.Type = new CustomerTypeModel()
                        {
                            ID = CustomerModel.CustomerType.CustomerTypeID,
                            Name = CustomerModel.CustomerType.CustomerTypeName,
                            Description = CustomerModel.CustomerType.CustomerTypeDescription,
                            LastModifiedBy = CustomerModel.CustomerType.UpdateDate == null ? CustomerModel.CustomerType.CreateBy : CustomerModel.CustomerType.UpdateBy,
                            LastModifiedDate = CustomerModel.CustomerType.UpdateDate == null ? CustomerModel.CustomerType.CreateDate : CustomerModel.CustomerType.UpdateDate.Value
                        };
                    }

                    if (CustomerModel.Location != null)
                    {
                        customer.Branch = new BranchModel()
                        {
                            ID = CustomerModel.Location.LocationID,
                            Name = CustomerModel.Location.LocationName,
                            LastModifiedBy = CustomerModel.Location.UpdateDate == null ? CustomerModel.Location.CreateBy : CustomerModel.Location.UpdateBy,
                            LastModifiedDate = CustomerModel.Location.UpdateDate == null ? CustomerModel.Location.CreateDate : CustomerModel.Location.UpdateDate.Value
                        };
                    }

                    customer.Contacts = (from cc in context.CustomerContacts
                                         where cc.IsDeleted.Equals(false) && cc.CIF.Equals(_CIF)
                                         select new CustomerContactModel()
                                         {
                                             ID = cc.ContactID,
                                             Name = cc.ContactName,
                                             IDNumber = cc.IdentificationNumber,
                                             DateOfBirth = cc.DateOfBirth,
                                             CIF = cc.CIF,
                                             PhoneNumber = cc.PhoneNumber,
                                             OccupationInID = cc.OccupationInId,
                                             POAFunction = context.POAFunctions.Where(pc => pc.IsDeleted.Equals(false) && pc.POAFunctionID.Equals(cc.POAFunctionID.Value)).Select(pc => new POAFunctionModel()
                                             {
                                                 ID = pc.POAFunctionID,
                                                 Name = pc.POAFunctionName,
                                                 Description = (pc.POAFunctionID == 9 ? cc.POAFunctionOther : pc.POAFunctionDescription)
                                             }).FirstOrDefault(),
                                             PlaceOfBirth = cc.PlaceOfBirth,
                                             EffectiveDate = cc.EffectiveDate,
                                             CancellationDate = cc.CancellationDate,
                                             LastModifiedBy = cc.UpdateDate == null ? cc.CreateBy : cc.UpdateBy,
                                             LastModifiedDate = cc.UpdateDate == null ? cc.CreateDate : cc.UpdateDate.Value,
                                         }).ToList();

                    customer.Accounts = (from j in context.SP_GETJointAccount(_CIF)
                                         select new CustomerAccountModel
                                         {
                                             AccountNumber = j.AccountNumber,
                                             Currency = new CurrencyModel
                                             {
                                                 ID = j.CurrencyID.Value,
                                                 Code = j.CurrencyCode,
                                                 Description = j.CurrencyDescription
                                             },
                                             CustomerName = j.CustomerName,
                                             IsJointAccount = j.IsJointAccount
                                         }).ToList();

                    customer.Underlyings = (from cu in context.SP_GetUnderlyingMaker(TransactionID)
                                            select new CustomerUnderlyingModel()
                                            {
                                                ID = cu.UnderlyingID.Value,
                                                //Amount = cu.Amount.Value,
                                                //Rate = cu.Rate,
                                                //AmountUSD = cu.AmountUSD,
                                                ////AvailableAmount = cu.AvailableAmountUSD,
                                                //AvailableAmount = tu.Amount != null ? (cu.AvailableAmountUSD.HasValue ? cu.AvailableAmountUSD.Value + tu.Amount : 0) : cu.AvailableAmountUSD.HasValue ? cu.AvailableAmountUSD.Value : 0,
                                                //UtilizeAmountDeal = tu.AvailableAmount,
                                                //AvailableAmountDeal = tu.AvailableAmount,
                                                Amount = cu.Amount.Value,
                                                Rate = cu.Rate,
                                                AmountUSD = cu.AmountUSD,
                                                AvailableAmount = cu.AvailableAmount,
                                                UtilizeAmountDeal = cu.UtilizeAmountDeal,
                                                AvailableAmountDeal = cu.Amount,

                                                AttachmentNo = cu.AttachmentNo,
                                                DateOfUnderlying = cu.DateOfUnderlying,
                                                EndDate = cu.EndDate,
                                                ExpiredDate = cu.ExpiredDate,
                                                InvoiceNumber = cu.InvoiceNumber,
                                                ReferenceNumber = cu.ReferenceNumber,
                                                StartDate = cu.StartDate,
                                                SupplierName = cu.SupplierName,
                                                IsDeclarationOfException = cu.IsDeclarationOfExecption.Value,
                                                IsUtilize = cu.IsUtilize.Value,
                                                IsJointAccount = cu.IsJointAccount.HasValue ? cu.IsJointAccount.Value : false,
                                                IsTMO = cu.IsTMO.HasValue ? cu.IsTMO.Value : false,
                                                AccountNumber = cu.AccountNumber,
                                                IsEnable = true,
                                                IsProforma = cu.IsProforma.Value,
                                                IsBulkUnderlying = cu.IsBulkUnderlying.Value,
                                                CreatedBy = cu.CreateBy,
                                                CreatedDate = cu.CreateDate.Value,
                                                LastModifiedBy = cu.UpdateDate == null ? cu.CreateBy : cu.UpdateBy,
                                                LastModifiedDate = cu.UpdateDate == null ? cu.CreateDate : cu.UpdateDate.Value,
                                                StatementLetter = new StatementLetterModel()
                                                 {
                                                     ID = cu.StatementLetterID.Value,
                                                     Name = cu.StatementLetterName,
                                                     LastModifiedBy = cu.UpdateDateSL == null ? cu.CreateBySL : cu.UpdateBySL,
                                                     LastModifiedDate = cu.UpdateDateSL == null ? cu.CreateDateSL : cu.UpdateDateSL
                                                 },
                                                 Currency = new CurrencyModel()
                                                 {
                                                     ID = cu.CurrencyID.Value,
                                                     Code = cu.CurrencyCode,
                                                     Description = cu.CurrencyDescription,
                                                     LastModifiedBy = cu.UpdateDateCcy == null ? cu.CreateByCcy : cu.UpdateByCcy,
                                                     LastModifiedDate = cu.UpdateDateCcy == null ? cu.CreateDateCcy : cu.UpdateDateCcy
                                                 },
                                                 DocumentType = new DocumentTypeModel()
                                                 {
                                                     ID = cu.DocTypeID.Value,
                                                     Name = cu.DocTypeName,
                                                     Description = cu.DocTypeDescription,
                                                     LastModifiedBy = cu.UpdateDateDC == null ? cu.CreateByDC : cu.UpdateByDC,
                                                     LastModifiedDate = cu.UpdateDateDC == null ? cu.CreateDateDC : cu.UpdateDateDC
                                                 },
                                                 UnderlyingDocument = new UnderlyingDocModel()
                                                 {
                                                     ID = cu.UnderlyingDocID.Value,
                                                     Code = cu.UnderlyingDocCode,
                                                     Name = cu.UnderlyingDocName,
                                                     LastModifiedBy = cu.UpdateDateUD == null ? cu.CreateByUD : cu.UpdateByUD,
                                                     LastModifiedDate = cu.UpdateDateUD == null ? cu.CreateDateUD : cu.UpdateDateUD
                                                 },
                                                ProformaDetails = (from cuy in context.CustomerUnderlyings
                                                                   join cm in context.MappingUnderlyings.Where(x => x.IsDeleted.Equals(false)) on cuy.UnderlyingID equals cm.UnderlyingID
                                                                   where cm.ParentID == cu.UnderlyingID
                                                                   select new CustomerUnderlyingDetailModel
                                                                   {
                                                                       StatementLetter = new StatementLetterModel()
                                                                       {
                                                                           ID = cuy.StatementLetter.StatementLetterID,
                                                                           Name = cuy.StatementLetter.StatementLetterName,
                                                                           LastModifiedBy = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateBy : cuy.StatementLetter.UpdateBy,
                                                                           LastModifiedDate = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateDate : cuy.StatementLetter.UpdateDate.Value
                                                                       },
                                                                       Amount = cuy.Amount,
                                                                       Rate = cuy.Rate,
                                                                       AmountUSD = cuy.AmountUSD,
                                                                       AvailableAmount = cuy.AvailableAmountUSD,
                                                                       AttachmentNo = cuy.AttachmentNo,
                                                                       Currency = new CurrencyModel()
                                                                       {
                                                                           ID = cuy.Currency.CurrencyID,
                                                                           Code = cuy.Currency.CurrencyCode,
                                                                           Description = cuy.Currency.CurrencyDescription,
                                                                           LastModifiedBy = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateBy : cuy.Currency.UpdateBy,
                                                                           LastModifiedDate = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateDate : cuy.Currency.UpdateDate.Value
                                                                       },
                                                                       DateOfUnderlying = cuy.DateOfUnderlying,
                                                                       DocumentType = new DocumentTypeModel()
                                                                       {
                                                                           ID = cuy.DocumentType.DocTypeID,
                                                                           Name = cuy.DocumentType.DocTypeName,
                                                                           Description = cuy.DocumentType.DocTypeDescription,
                                                                           LastModifiedBy = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateBy : cuy.DocumentType.UpdateBy,
                                                                           LastModifiedDate = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateDate : cuy.DocumentType.UpdateDate.Value
                                                                       },
                                                                       InvoiceNumber = cuy.InvoiceNumber,
                                                                       ReferenceNumber = cuy.ReferenceNumber,
                                                                       SupplierName = cuy.SupplierName,
                                                                       ExpiredDate = cuy.ExpiredDate,
                                                                       UnderlyingDocument = new UnderlyingDocModel()
                                                                       {
                                                                           ID = cuy.UnderlyingDocument.UnderlyingDocID,
                                                                           Code = cuy.UnderlyingDocument.UnderlyingDocCode,
                                                                           Name = cuy.UnderlyingDocument.UnderlyingDocName,
                                                                           LastModifiedBy = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateBy : cuy.UnderlyingDocument.UpdateBy,
                                                                           LastModifiedDate = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateDate : cuy.UnderlyingDocument.UpdateDate.Value
                                                                       }
                                                                   }).ToList(),
                                                BulkDetails = (from cuy in context.CustomerUnderlyings
                                                               join cm in context.MappingBulkUnderlyings.Where(x => x.IsDeleted.Equals(false)) on cuy.UnderlyingID equals cm.UnderlyingID
                                                               where cm.ParentID == cu.UnderlyingID
                                                               select new CustomerUnderlyingDetailModel
                                                               {
                                                                   StatementLetter = new StatementLetterModel()
                                                                   {
                                                                       ID = cuy.StatementLetter.StatementLetterID,
                                                                       Name = cuy.StatementLetter.StatementLetterName,
                                                                       LastModifiedBy = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateBy : cuy.StatementLetter.UpdateBy,
                                                                       LastModifiedDate = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateDate : cuy.StatementLetter.UpdateDate.Value
                                                                   },
                                                                   Amount = cuy.Amount,
                                                                   Rate = cuy.Rate,
                                                                   AmountUSD = cuy.AmountUSD,
                                                                   AvailableAmount = cuy.AvailableAmountUSD,
                                                                   AttachmentNo = cuy.AttachmentNo,
                                                                   Currency = new CurrencyModel()
                                                                   {
                                                                       ID = cuy.Currency.CurrencyID,
                                                                       Code = cuy.Currency.CurrencyCode,
                                                                       Description = cuy.Currency.CurrencyDescription,
                                                                       LastModifiedBy = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateBy : cuy.Currency.UpdateBy,
                                                                       LastModifiedDate = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateDate : cuy.Currency.UpdateDate.Value
                                                                   },
                                                                   DateOfUnderlying = cuy.DateOfUnderlying,
                                                                   DocumentType = new DocumentTypeModel()
                                                                   {
                                                                       ID = cuy.DocumentType.DocTypeID,
                                                                       Name = cuy.DocumentType.DocTypeName,
                                                                       Description = cuy.DocumentType.DocTypeDescription,
                                                                       LastModifiedBy = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateBy : cuy.DocumentType.UpdateBy,
                                                                       LastModifiedDate = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateDate : cuy.DocumentType.UpdateDate.Value
                                                                   },
                                                                   InvoiceNumber = cuy.InvoiceNumber,
                                                                   ReferenceNumber = cuy.ReferenceNumber,
                                                                   SupplierName = cuy.SupplierName,
                                                                   ExpiredDate = cuy.ExpiredDate,
                                                                   UnderlyingDocument = new UnderlyingDocModel()
                                                                   {
                                                                       ID = cuy.UnderlyingDocument.UnderlyingDocID,
                                                                       Code = cuy.UnderlyingDocument.UnderlyingDocCode,
                                                                       Name = cuy.UnderlyingDocument.UnderlyingDocName,
                                                                       LastModifiedBy = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateBy : cuy.UnderlyingDocument.UpdateBy,
                                                                       LastModifiedDate = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateDate : cuy.UnderlyingDocument.UpdateDate.Value
                                                                   }
                                                               }).ToList()
                                            }).ToList();
                }
                else
                {
                    message = "Customer empty data or has been deleted.";
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerByTransactionTMOIPEDeal(long TransactionDealID, long TransactionID, ref CustomerModel customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                customer = (from a in context.Customers
                            join b in context.Transactions on a.CIF equals b.CIF
                            where a.IsDeleted.Equals(false)
                            && b.TransactionID == TransactionID
                            select new CustomerModel
                            {
                                CIF = a.CIF,
                                Name = a.CustomerName,
                                POAName = a.POAName,

                                BizSegment = new BizSegmentModel()
                                {
                                    ID = a.BizSegment.BizSegmentID,
                                    Name = a.BizSegment.BizSegmentName,
                                    Description = a.BizSegment.BizSegmentDescription,
                                    LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                    LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                },

                                RM = a.CustomerMappings.Where(x => x.IsDeleted.Equals(false)).Select(x => new RMModel()
                                {
                                    ID = x.Employee.EmployeeID,
                                    Name = x.Employee.EmployeeName,
                                    SegmentID = x.Employee.BizSegmentID,
                                    BranchID = x.Employee.LocationID,
                                    RankID = x.Employee.RankID,
                                    Email = x.Employee.EmployeeEmail,
                                    LastModifiedBy = x.Employee.UpdateDate == null ? x.Employee.CreateBy : x.Employee.UpdateBy,
                                    LastModifiedDate = x.Employee.UpdateDate == null ? x.Employee.CreateDate : x.Employee.UpdateDate.Value
                                }).FirstOrDefault(),
                                LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                            }).SingleOrDefault();

                if (customer != null)
                {
                    string _CIF = customer.CIF;
                    var CustomerModel = (from a in context.Customers
                                         where a.CIF == _CIF
                                         select a).SingleOrDefault();
                    if (CustomerModel.CustomerType != null)
                    {
                        customer.Type = new CustomerTypeModel()
                        {
                            ID = CustomerModel.CustomerType.CustomerTypeID,
                            Name = CustomerModel.CustomerType.CustomerTypeName,
                            Description = CustomerModel.CustomerType.CustomerTypeDescription,
                            LastModifiedBy = CustomerModel.CustomerType.UpdateDate == null ? CustomerModel.CustomerType.CreateBy : CustomerModel.CustomerType.UpdateBy,
                            LastModifiedDate = CustomerModel.CustomerType.UpdateDate == null ? CustomerModel.CustomerType.CreateDate : CustomerModel.CustomerType.UpdateDate.Value
                        };
                    }

                    if (CustomerModel.Location != null)
                    {
                        customer.Branch = new BranchModel()
                        {
                            ID = CustomerModel.Location.LocationID,
                            Name = CustomerModel.Location.LocationName,
                            LastModifiedBy = CustomerModel.Location.UpdateDate == null ? CustomerModel.Location.CreateBy : CustomerModel.Location.UpdateBy,
                            LastModifiedDate = CustomerModel.Location.UpdateDate == null ? CustomerModel.Location.CreateDate : CustomerModel.Location.UpdateDate.Value
                        };
                    }

                    customer.Contacts = (from cc in context.SP_GETContactCallBack(_CIF)
                                         select new CustomerContactModel()
                                         {
                                             ID = cc.ContactID,
                                             Name = cc.ContactName,
                                             IDNumber = cc.IdentificationNumber,
                                             DateOfBirth = cc.DateOfBirth,
                                             CIF = cc.CIF,
                                             PhoneNumber = cc.PhoneNumber,
                                             OccupationInID = cc.OccupationInID,
                                             //POAFunction = context.POAFunctions.Where(pc => pc.IsDeleted.Equals(false) && pc.POAFunctionID.Equals(cc.POAFunctionID.Value)).Select(pc => new POAFunctionModel()
                                             //{
                                             //    ID = pc.POAFunctionID,
                                             //    Name = pc.POAFunctionName,
                                             //    Description = (pc.POAFunctionID == 9 ? cc.POAFunctionOther : pc.POAFunctionDescription)
                                             //}).FirstOrDefault(),
                                             POAFunctionVW = cc.POADescription,
                                             PlaceOfBirth = cc.PlaceOfBirth,
                                             EffectiveDate = cc.EffectiveDate,
                                             CancellationDate = cc.CancellationDate,
                                             LastModifiedBy = cc.UpdateDate == null ? cc.CreateBy : cc.UpdateBy,
                                             LastModifiedDate = cc.UpdateDate == null ? cc.CreateDate : cc.UpdateDate.Value,
                                         }).ToList();

                    customer.Accounts = (from j in context.SP_GETJointAccount(_CIF)
                                         select new CustomerAccountModel
                                         {
                                             AccountNumber = j.AccountNumber,
                                             Currency = new CurrencyModel
                                             {
                                                 ID = j.CurrencyID.Value,
                                                 Code = j.CurrencyCode,
                                                 Description = j.CurrencyDescription
                                             },
                                             CustomerName = j.CustomerName,
                                             IsJointAccount = j.IsJointAccount
                                         }).ToList();

                    customer.Underlyings = (from cu in context.SP_GetUnderlyingTMOMaker(TransactionDealID, TransactionID)
                                            select new CustomerUnderlyingModel()
                                            {
                                                ID = cu.UnderlyingID.Value,
                                                Amount = cu.Amount.Value,
                                                Rate = cu.Rate,
                                                AmountUSD = cu.AmountUSD,
                                                AvailableAmount = cu.AvailableAmount,
                                                UtilizeAmountDeal = cu.UtilizeAmountDeal,
                                                AvailableAmountDeal = cu.Amount,
                                                AttachmentNo = cu.AttachmentNo,
                                                DateOfUnderlying = cu.DateOfUnderlying,
                                                EndDate = cu.EndDate,
                                                ExpiredDate = cu.ExpiredDate,
                                                InvoiceNumber = cu.InvoiceNumber,
                                                ReferenceNumber = cu.ReferenceNumber,
                                                StartDate = cu.StartDate,
                                                SupplierName = cu.SupplierName,
                                                IsDeclarationOfException = cu.IsDeclarationOfExecption.Value,
                                                IsUtilize = cu.IsUtilize.Value,
                                                IsJointAccount = cu.IsJointAccount.HasValue ? cu.IsJointAccount.Value : false,
                                                IsTMO = cu.IsTMO.HasValue ? cu.IsTMO.Value : false,
                                                AccountNumber = cu.AccountNumber,
                                                IsEnable = true,
                                                IsProforma = cu.IsProforma.Value,
                                                IsBulkUnderlying = cu.IsBulkUnderlying.Value,
                                                CreatedBy = cu.CreateBy,
                                                CreatedDate = cu.CreateDate.Value,
                                                LastModifiedBy = cu.UpdateDate == null ? cu.CreateBy : cu.UpdateBy,
                                                LastModifiedDate = cu.UpdateDate == null ? cu.CreateDate : cu.UpdateDate.Value,
                                                StatementLetter = new StatementLetterModel()
                                                {
                                                    ID = cu.StatementLetterID.Value,
                                                    Name = cu.StatementLetterName,
                                                    LastModifiedBy = cu.UpdateDateSL == null ? cu.CreateBySL : cu.UpdateBySL,
                                                    LastModifiedDate = cu.UpdateDateSL == null ? cu.CreateDateSL : cu.UpdateDateSL
                                                },
                                                Currency = new CurrencyModel()
                                                {
                                                    ID = cu.CurrencyID.Value,
                                                    Code = cu.CurrencyCode,
                                                    Description = cu.CurrencyDescription,
                                                    LastModifiedBy = cu.UpdateDateCcy == null ? cu.CreateByCcy : cu.UpdateByCcy,
                                                    LastModifiedDate = cu.UpdateDateCcy == null ? cu.CreateDateCcy : cu.UpdateDateCcy
                                                },
                                                DocumentType = new DocumentTypeModel()
                                                {
                                                    ID = cu.DocTypeID.Value,
                                                    Name = cu.DocTypeName,
                                                    Description = cu.DocTypeDescription,
                                                    LastModifiedBy = cu.UpdateDateDC == null ? cu.CreateByDC : cu.UpdateByDC,
                                                    LastModifiedDate = cu.UpdateDateDC == null ? cu.CreateDateDC : cu.UpdateDateDC
                                                },
                                                UnderlyingDocument = new UnderlyingDocModel()
                                                {
                                                    ID = cu.UnderlyingDocID.Value,
                                                    Code = cu.UnderlyingDocCode,
                                                    Name = cu.UnderlyingDocName,
                                                    LastModifiedBy = cu.UpdateDateUD == null ? cu.CreateByUD : cu.UpdateByUD,
                                                    LastModifiedDate = cu.UpdateDateUD == null ? cu.CreateDateUD : cu.UpdateDateUD
                                                }
                                            }).ToList();
                }
                else
                {
                    message = "Customer empty data or has been deleted.";
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerByTMONetting(long TMONettingID, ref CustomerModel customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                customer = (from a in context.Customers
                            join b in context.TMONettings on a.CIF equals b.CIF
                            where a.IsDeleted.Equals(false)
                            && b.TMONettingID.Equals(TMONettingID)
                            select new CustomerModel
                            {
                                CIF = a.CIF,
                                Name = a.CustomerName,
                                POAName = a.POAName,

                                BizSegment = new BizSegmentModel()
                                {
                                    ID = a.BizSegment.BizSegmentID,
                                    Name = a.BizSegment.BizSegmentName,
                                    Description = a.BizSegment.BizSegmentDescription,
                                    LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                    LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                },

                                RM = a.CustomerMappings.Where(x => x.IsDeleted.Equals(false)).Select(x => new RMModel()
                                {
                                    ID = x.Employee.EmployeeID,
                                    Name = x.Employee.EmployeeName,
                                    SegmentID = x.Employee.BizSegmentID,
                                    BranchID = x.Employee.LocationID,
                                    RankID = x.Employee.RankID,
                                    Email = x.Employee.EmployeeEmail,
                                    LastModifiedBy = x.Employee.UpdateDate == null ? x.Employee.CreateBy : x.Employee.UpdateBy,
                                    LastModifiedDate = x.Employee.UpdateDate == null ? x.Employee.CreateDate : x.Employee.UpdateDate.Value
                                }).FirstOrDefault(),
                                LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                            }).SingleOrDefault();

                if (customer != null)
                {
                    string _CIF = customer.CIF;
                    var CustomerModel = (from a in context.Customers
                                         where a.CIF == _CIF
                                         select a).SingleOrDefault();
                    if (CustomerModel.CustomerType != null)
                    {
                        customer.Type = new CustomerTypeModel()
                        {
                            ID = CustomerModel.CustomerType.CustomerTypeID,
                            Name = CustomerModel.CustomerType.CustomerTypeName,
                            Description = CustomerModel.CustomerType.CustomerTypeDescription,
                            LastModifiedBy = CustomerModel.CustomerType.UpdateDate == null ? CustomerModel.CustomerType.CreateBy : CustomerModel.CustomerType.UpdateBy,
                            LastModifiedDate = CustomerModel.CustomerType.UpdateDate == null ? CustomerModel.CustomerType.CreateDate : CustomerModel.CustomerType.UpdateDate.Value
                        };
                    }

                    if (CustomerModel.Location != null)
                    {
                        customer.Branch = new BranchModel()
                        {
                            ID = CustomerModel.Location.LocationID,
                            Name = CustomerModel.Location.LocationName,
                            LastModifiedBy = CustomerModel.Location.UpdateDate == null ? CustomerModel.Location.CreateBy : CustomerModel.Location.UpdateBy,
                            LastModifiedDate = CustomerModel.Location.UpdateDate == null ? CustomerModel.Location.CreateDate : CustomerModel.Location.UpdateDate.Value
                        };
                    }

                    customer.Contacts = (from cc in context.CustomerContacts
                                         where cc.IsDeleted.Equals(false) && cc.CIF.Equals(_CIF)
                                         select new CustomerContactModel()
                                         {
                                             ID = cc.ContactID,
                                             Name = cc.ContactName,
                                             IDNumber = cc.IdentificationNumber,
                                             DateOfBirth = cc.DateOfBirth,
                                             CIF = cc.CIF,
                                             PhoneNumber = cc.PhoneNumber,
                                             OccupationInID = cc.OccupationInId,
                                             POAFunction = context.POAFunctions.Where(pc => pc.IsDeleted.Equals(false) && pc.POAFunctionID.Equals(cc.POAFunctionID.Value)).Select(pc => new POAFunctionModel()
                                             {
                                                 ID = pc.POAFunctionID,
                                                 Name = pc.POAFunctionName,
                                                 Description = (pc.POAFunctionID == 9 ? cc.POAFunctionOther : pc.POAFunctionDescription)
                                             }).FirstOrDefault(),
                                             PlaceOfBirth = cc.PlaceOfBirth,
                                             EffectiveDate = cc.EffectiveDate,
                                             CancellationDate = cc.CancellationDate,
                                             LastModifiedBy = cc.UpdateDate == null ? cc.CreateBy : cc.UpdateBy,
                                             LastModifiedDate = cc.UpdateDate == null ? cc.CreateDate : cc.UpdateDate.Value,
                                         }).ToList();

                    customer.Accounts = (from j in context.SP_GETJointAccount(_CIF)
                                         select new CustomerAccountModel
                                         {
                                             AccountNumber = j.AccountNumber,
                                             Currency = new CurrencyModel
                                             {
                                                 ID = j.CurrencyID.Value,
                                                 Code = j.CurrencyCode,
                                                 Description = j.CurrencyDescription
                                             },
                                             CustomerName = j.CustomerName,
                                             IsJointAccount = j.IsJointAccount
                                         }).ToList();

                    customer.Underlyings = (from cu in context.SP_GetUnderlyingNettingChecker(TMONettingID)
                                            select new CustomerUnderlyingModel()
                                            {
                                                ID = cu.UnderlyingID.Value,
                                                Amount = cu.Amount.Value,
                                                Rate = cu.Rate,
                                                AmountUSD = cu.AmountUSD,
                                                AvailableAmount = cu.AvailableAmount,
                                                UtilizeAmountDeal = cu.UtilizeAmountDeal,
                                                AvailableAmountDeal = cu.Amount,
                                                AttachmentNo = cu.AttachmentNo,
                                                DateOfUnderlying = cu.DateOfUnderlying,
                                                EndDate = cu.EndDate,
                                                ExpiredDate = cu.ExpiredDate,
                                                InvoiceNumber = cu.InvoiceNumber,
                                                ReferenceNumber = cu.ReferenceNumber,
                                                StartDate = cu.StartDate,
                                                SupplierName = cu.SupplierName,
                                                IsDeclarationOfException = cu.IsDeclarationOfExecption.Value,
                                                IsUtilize = cu.IsUtilize.Value,
                                                IsJointAccount = cu.IsJointAccount.HasValue ? cu.IsJointAccount.Value : false,
                                                IsTMO = cu.IsTMO.HasValue ? cu.IsTMO.Value : false,
                                                AccountNumber = cu.AccountNumber,
                                                IsEnable = true,
                                                IsProforma = cu.IsProforma.Value,
                                                IsBulkUnderlying = cu.IsBulkUnderlying.Value,
                                                CreatedBy = cu.CreateBy,
                                                CreatedDate = cu.CreateDate.Value,
                                                LastModifiedBy = cu.UpdateDate == null ? cu.CreateBy : cu.UpdateBy,
                                                LastModifiedDate = cu.UpdateDate == null ? cu.CreateDate : cu.UpdateDate.Value,
                                                StatementLetter = new StatementLetterModel()
                                                {
                                                    ID = cu.StatementLetterID.Value,
                                                    Name = cu.StatementLetterName,
                                                    LastModifiedBy = cu.UpdateDateSL == null ? cu.CreateBySL : cu.UpdateBySL,
                                                    LastModifiedDate = cu.UpdateDateSL == null ? cu.CreateDateSL : cu.UpdateDateSL
                                                },
                                                Currency = new CurrencyModel()
                                                {
                                                    ID = cu.CurrencyID.Value,
                                                    Code = cu.CurrencyCode,
                                                    Description = cu.CurrencyDescription,
                                                    LastModifiedBy = cu.UpdateDateCcy == null ? cu.CreateByCcy : cu.UpdateByCcy,
                                                    LastModifiedDate = cu.UpdateDateCcy == null ? cu.CreateDateCcy : cu.UpdateDateCcy
                                                },
                                                DocumentType = new DocumentTypeModel()
                                                {
                                                    ID = cu.DocTypeID.Value,
                                                    Name = cu.DocTypeName,
                                                    Description = cu.DocTypeDescription,
                                                    LastModifiedBy = cu.UpdateDateDC == null ? cu.CreateByDC : cu.UpdateByDC,
                                                    LastModifiedDate = cu.UpdateDateDC == null ? cu.CreateDateDC : cu.UpdateDateDC
                                                },
                                                UnderlyingDocument = new UnderlyingDocModel()
                                                {
                                                    ID = cu.UnderlyingDocID.Value,
                                                    Code = cu.UnderlyingDocCode,
                                                    Name = cu.UnderlyingDocName,
                                                    LastModifiedBy = cu.UpdateDateUD == null ? cu.CreateByUD : cu.UpdateByUD,
                                                    LastModifiedDate = cu.UpdateDateUD == null ? cu.CreateDateUD : cu.UpdateDateUD
                                                }
                                            }).ToList();
                }
                else
                {
                    message = "Customer empty data or has been deleted.";
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerByTransactionDealChecker(long TransactionDealID, long TransactionID, ref CustomerModel customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (TransactionDealID != 0)
                {
                    customer = (from a in context.Customers
                                join b in context.TransactionDeals on a.CIF equals b.CIF
                                where a.IsDeleted.Equals(false)
                                && b.TransactionDealID == TransactionDealID
                                select new CustomerModel
                                {
                                    CIF = a.CIF,
                                    Name = a.CustomerName,
                                    POAName = a.POAName,

                                    BizSegment = new BizSegmentModel()
                                    {
                                        ID = a.BizSegment.BizSegmentID,
                                        Name = a.BizSegment.BizSegmentName,
                                        Description = a.BizSegment.BizSegmentDescription,
                                        LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                        LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                    },

                                    RM = a.CustomerMappings.Where(x => x.IsDeleted.Equals(false)).Select(x => new RMModel()
                                    {
                                        ID = x.Employee.EmployeeID,
                                        Name = x.Employee.EmployeeName,
                                        SegmentID = x.Employee.BizSegmentID,
                                        BranchID = x.Employee.LocationID,
                                        RankID = x.Employee.RankID,
                                        Email = x.Employee.EmployeeEmail,
                                        LastModifiedBy = x.Employee.UpdateDate == null ? x.Employee.CreateBy : x.Employee.UpdateBy,
                                        LastModifiedDate = x.Employee.UpdateDate == null ? x.Employee.CreateDate : x.Employee.UpdateDate.Value
                                    }).FirstOrDefault(),
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                }).SingleOrDefault();
                }
                else
                {
                     customer = (from a in context.Customers
                                join b in context.Transactions on a.CIF equals b.CIF
                                where a.IsDeleted.Equals(false)
                                && b.TransactionID == TransactionID
                                select new CustomerModel
                                {
                                    CIF = a.CIF,
                                    Name = a.CustomerName,
                                    POAName = a.POAName,

                                    BizSegment = new BizSegmentModel()
                                    {
                                        ID = a.BizSegment.BizSegmentID,
                                        Name = a.BizSegment.BizSegmentName,
                                        Description = a.BizSegment.BizSegmentDescription,
                                        LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                        LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                    },

                                    RM = a.CustomerMappings.Where(x => x.IsDeleted.Equals(false)).Select(x => new RMModel()
                                    {
                                        ID = x.Employee.EmployeeID,
                                        Name = x.Employee.EmployeeName,
                                        SegmentID = x.Employee.BizSegmentID,
                                        BranchID = x.Employee.LocationID,
                                        RankID = x.Employee.RankID,
                                        Email = x.Employee.EmployeeEmail,
                                        LastModifiedBy = x.Employee.UpdateDate == null ? x.Employee.CreateBy : x.Employee.UpdateBy,
                                        LastModifiedDate = x.Employee.UpdateDate == null ? x.Employee.CreateDate : x.Employee.UpdateDate.Value
                                    }).FirstOrDefault(),
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                }).SingleOrDefault();
                }
                if (customer != null)
                {
                   string _CIF = customer.CIF;
                    var CustomerModel = (from a in context.Customers
                                         where a.CIF == _CIF
                                         select a).SingleOrDefault();
                    if (CustomerModel.CustomerType != null)
                    {
                        customer.Type = new CustomerTypeModel()
                        {
                            ID = CustomerModel.CustomerType.CustomerTypeID,
                            Name = CustomerModel.CustomerType.CustomerTypeName,
                            Description = CustomerModel.CustomerType.CustomerTypeDescription,
                            LastModifiedBy = CustomerModel.CustomerType.UpdateDate == null ? CustomerModel.CustomerType.CreateBy : CustomerModel.CustomerType.UpdateBy,
                            LastModifiedDate = CustomerModel.CustomerType.UpdateDate == null ? CustomerModel.CustomerType.CreateDate : CustomerModel.CustomerType.UpdateDate.Value
                        };
                    }

                    if (CustomerModel.Location != null)
                    {
                        customer.Branch = new BranchModel()
                        {
                            ID = CustomerModel.Location.LocationID,
                            Name = CustomerModel.Location.LocationName,
                            LastModifiedBy = CustomerModel.Location.UpdateDate == null ? CustomerModel.Location.CreateBy : CustomerModel.Location.UpdateBy,
                            LastModifiedDate = CustomerModel.Location.UpdateDate == null ? CustomerModel.Location.CreateDate : CustomerModel.Location.UpdateDate.Value
                        };
                    }

                    customer.Contacts = (from cc in context.SP_GETContactCallBack(_CIF)
                                         select new CustomerContactModel()
                                         {
                                             ID = cc.ContactID,
                                             Name = cc.ContactName,
                                             IDNumber = cc.IdentificationNumber,
                                             DateOfBirth = cc.DateOfBirth,
                                             CIF = cc.CIF,
                                             PhoneNumber = cc.PhoneNumber,
                                             OccupationInID = cc.OccupationInID,
                                             //POAFunction = context.POAFunctions.Where(pc => pc.IsDeleted.Equals(false) && pc.POAFunctionID.Equals(cc.POAFunctionID.Value)).Select(pc => new POAFunctionModel()
                                             //{
                                             //    ID = pc.POAFunctionID,
                                             //    Name = pc.POAFunctionName,
                                             //    Description = (pc.POAFunctionID == 9 ? cc.POAFunctionOther : pc.POAFunctionDescription)
                                             //}).FirstOrDefault(),
                                             POAFunctionVW = cc.POADescription,
                                             PlaceOfBirth = cc.PlaceOfBirth,
                                             EffectiveDate = cc.EffectiveDate,
                                             CancellationDate = cc.CancellationDate,
                                             LastModifiedBy = cc.UpdateDate == null ? cc.CreateBy : cc.UpdateBy,
                                             LastModifiedDate = cc.UpdateDate == null ? cc.CreateDate : cc.UpdateDate.Value,
                                         }).ToList();
                    customer.Accounts = (from j in context.SP_GETJointAccount(_CIF)
                                         select new CustomerAccountModel
                                         {
                                             AccountNumber = j.AccountNumber,
                                             Currency = new CurrencyModel
                                             {
                                                 ID = j.CurrencyID.Value,
                                                 Code = j.CurrencyCode,
                                                 Description = j.CurrencyDescription
                                             },
                                             CustomerName = j.CustomerName,
                                             IsJointAccount = j.IsJointAccount
                                         }).ToList();

                     customer.Underlyings = (from cu in context.SP_GetUnderlyingChecker(TransactionDealID, TransactionID)
                                             select new CustomerUnderlyingModel()
                                             {
                                                 ID = cu.UnderlyingID.Value,
                                                 Amount = cu.Amount.Value,
                                                 Rate = cu.Rate,
                                                 AmountUSD = cu.AmountUSD,
                                                 AvailableAmount = cu.AvailableAmount,
                                                 UtilizeAmountDeal = cu.UtilizeAmountDeal,
                                                 AvailableAmountDeal = cu.Amount,
                                                 AttachmentNo = cu.AttachmentNo,
                                                 DateOfUnderlying = cu.DateOfUnderlying,
                                                 EndDate = cu.EndDate,
                                                 ExpiredDate = cu.ExpiredDate,
                                                 InvoiceNumber = cu.InvoiceNumber,
                                                 ReferenceNumber = cu.ReferenceNumber,
                                                 StartDate = cu.StartDate,
                                                 SupplierName = cu.SupplierName,
                                                 IsDeclarationOfException = cu.IsDeclarationOfExecption.Value,
                                                 IsUtilize = cu.IsUtilize.Value,
                                                 IsJointAccount = cu.IsJointAccount.HasValue ? cu.IsJointAccount.Value : false,
                                                 IsTMO = cu.IsTMO.HasValue ? cu.IsTMO.Value : false,
                                                 AccountNumber = cu.AccountNumber,
                                                 IsEnable = true,
                                                 IsProforma = cu.IsProforma.Value,
                                                 IsBulkUnderlying = cu.IsBulkUnderlying.Value,
                                                 CreatedBy = cu.CreateBy,
                                                 CreatedDate = cu.CreateDate.Value,
                                                 LastModifiedBy = cu.UpdateDate == null ? cu.CreateBy : cu.UpdateBy,
                                                 LastModifiedDate = cu.UpdateDate == null ? cu.CreateDate : cu.UpdateDate.Value,
                                                 StatementLetter = new StatementLetterModel()
                                                 {
                                                     ID = cu.StatementLetterID.Value,
                                                     Name = cu.StatementLetterName,
                                                     LastModifiedBy = cu.UpdateDateSL == null ? cu.CreateBySL : cu.UpdateBySL,
                                                     LastModifiedDate = cu.UpdateDateSL == null ? cu.CreateDateSL : cu.UpdateDateSL
                                                 },
                                                 Currency = new CurrencyModel()
                                                 {
                                                     ID = cu.CurrencyID.Value,
                                                     Code = cu.CurrencyCode,
                                                     Description = cu.CurrencyDescription,
                                                     LastModifiedBy = cu.UpdateDateCcy == null ? cu.CreateByCcy : cu.UpdateByCcy,
                                                     LastModifiedDate = cu.UpdateDateCcy == null ? cu.CreateDateCcy : cu.UpdateDateCcy
                                                 },
                                                 DocumentType = new DocumentTypeModel()
                                                 {
                                                     ID = cu.DocTypeID.Value,
                                                     Name = cu.DocTypeName,
                                                     Description = cu.DocTypeDescription,
                                                     LastModifiedBy = cu.UpdateDateDC == null ? cu.CreateByDC : cu.UpdateByDC,
                                                     LastModifiedDate = cu.UpdateDateDC == null ? cu.CreateDateDC : cu.UpdateDateDC
                                                 },
                                                 UnderlyingDocument = new UnderlyingDocModel()
                                                 {
                                                     ID = cu.UnderlyingDocID.Value,
                                                     Code = cu.UnderlyingDocCode,
                                                     Name = cu.UnderlyingDocName,
                                                     LastModifiedBy = cu.UpdateDateUD == null ? cu.CreateByUD : cu.UpdateByUD,
                                                     LastModifiedDate = cu.UpdateDateUD == null ? cu.CreateDateUD : cu.UpdateDateUD
                                                 }
                                             }).ToList();
                }
                else
                {
                    message = "Customer empty data or has been deleted.";
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerByTransactionFXDealChecker(long TransactionID, ref CustomerModel customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                customer = (from a in context.Customers
                            join b in context.TransactionDeals on a.CIF equals b.CIF
                            where a.IsDeleted.Equals(false)
                            && b.TransactionDealID == TransactionID
                            select new CustomerModel
                            {
                                CIF = a.CIF,
                                Name = a.CustomerName,
                                POAName = a.POAName,

                                BizSegment = new BizSegmentModel()
                                {
                                    ID = a.BizSegment.BizSegmentID,
                                    Name = a.BizSegment.BizSegmentName,
                                    Description = a.BizSegment.BizSegmentDescription,
                                    LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                    LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                },

                                RM = a.CustomerMappings.Where(x => x.IsDeleted.Equals(false)).Select(x => new RMModel()
                                {
                                    ID = x.Employee.EmployeeID,
                                    Name = x.Employee.EmployeeName,
                                    SegmentID = x.Employee.BizSegmentID,
                                    BranchID = x.Employee.LocationID,
                                    RankID = x.Employee.RankID,
                                    Email = x.Employee.EmployeeEmail,
                                    LastModifiedBy = x.Employee.UpdateDate == null ? x.Employee.CreateBy : x.Employee.UpdateBy,
                                    LastModifiedDate = x.Employee.UpdateDate == null ? x.Employee.CreateDate : x.Employee.UpdateDate.Value
                                }).FirstOrDefault(),
                                LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                            }).SingleOrDefault();

                if (customer != null)
                {
                    string _CIF = customer.CIF;
                    var CustomerModel = (from a in context.Customers
                                         where a.CIF == _CIF
                                         select a).SingleOrDefault();
                    if (CustomerModel.CustomerType != null)
                    {
                        customer.Type = new CustomerTypeModel()
                        {
                            ID = CustomerModel.CustomerType.CustomerTypeID,
                            Name = CustomerModel.CustomerType.CustomerTypeName,
                            Description = CustomerModel.CustomerType.CustomerTypeDescription,
                            LastModifiedBy = CustomerModel.CustomerType.UpdateDate == null ? CustomerModel.CustomerType.CreateBy : CustomerModel.CustomerType.UpdateBy,
                            LastModifiedDate = CustomerModel.CustomerType.UpdateDate == null ? CustomerModel.CustomerType.CreateDate : CustomerModel.CustomerType.UpdateDate.Value
                        };
                    }

                    if (CustomerModel.Location != null)
                    {
                        customer.Branch = new BranchModel()
                        {
                            ID = CustomerModel.Location.LocationID,
                            Name = CustomerModel.Location.LocationName,
                            LastModifiedBy = CustomerModel.Location.UpdateDate == null ? CustomerModel.Location.CreateBy : CustomerModel.Location.UpdateBy,
                            LastModifiedDate = CustomerModel.Location.UpdateDate == null ? CustomerModel.Location.CreateDate : CustomerModel.Location.UpdateDate.Value
                        };
                    }

                    //customer.Contacts = (from cc in context.SP_GETContactCallBack(_CIF)
                    //                     select new CustomerContactModel()
                    //                     {
                    //                         ID = cc.ContactID,
                    //                         Name = cc.ContactName,
                    //                         IDNumber = cc.IdentificationNumber,
                    //                         DateOfBirth = cc.DateOfBirth,
                    //                         CIF = cc.CIF,
                    //                         PhoneNumber = cc.PhoneNumber,
                    //                         OccupationInID = cc.OccupationInID,
                    //                         //POAFunction = context.POAFunctions.Where(pc => pc.IsDeleted.Equals(false) && pc.POAFunctionID.Equals(cc.POAFunctionID.Value)).Select(pc => new POAFunctionModel()
                    //                         //{
                    //                         //    ID = pc.POAFunctionID,
                    //                         //    Name = pc.POAFunctionName,
                    //                         //    Description = (pc.POAFunctionID == 9 ? cc.POAFunctionOther : pc.POAFunctionDescription)
                    //                         //}).FirstOrDefault(),
                    //                         POAFunctionVW = cc.POADescription,
                    //                         PlaceOfBirth = cc.PlaceOfBirth,
                    //                         EffectiveDate = cc.EffectiveDate,
                    //                         CancellationDate = cc.CancellationDate,
                    //                         LastModifiedBy = cc.UpdateDate == null ? cc.CreateBy : cc.UpdateBy,
                    //                         LastModifiedDate = cc.UpdateDate == null ? cc.CreateDate : cc.UpdateDate.Value,
                    //                     }).ToList();

                    customer.Contacts = (from cc in context.CustomerContacts
                                         where cc.IsDeleted.Equals(false) && cc.CIF.Equals(_CIF)
                                         select new CustomerContactModel()
                                         {
                                             ID = cc.ContactID,
                                             Name = cc.ContactName,
                                             IDNumber = cc.IdentificationNumber,
                                             DateOfBirth = cc.DateOfBirth,
                                             CIF = cc.CIF,
                                             PhoneNumber = cc.PhoneNumber,
                                             OccupationInID = cc.OccupationInId,
                                             POAFunction = context.POAFunctions.Where(pc => pc.IsDeleted.Equals(false) && pc.POAFunctionID.Equals(cc.POAFunctionID.Value)).Select(pc => new POAFunctionModel()
                                             {
                                                 ID = pc.POAFunctionID,
                                                 Name = pc.POAFunctionName,
                                                 Description = (pc.POAFunctionID == 9 ? cc.POAFunctionOther : pc.POAFunctionDescription)
                                             }).FirstOrDefault(),
                                             PlaceOfBirth = cc.PlaceOfBirth,
                                             EffectiveDate = cc.EffectiveDate,
                                             CancellationDate = cc.CancellationDate,
                                             LastModifiedBy = cc.UpdateDate == null ? cc.CreateBy : cc.UpdateBy,
                                             LastModifiedDate = cc.UpdateDate == null ? cc.CreateDate : cc.UpdateDate.Value,
                                         }).ToList();

                    customer.Accounts = (from j in context.SP_GETJointAccount(_CIF)
                                         select new CustomerAccountModel
                                         {
                                             AccountNumber = j.AccountNumber,
                                             Currency = new CurrencyModel
                                             {
                                                 ID = j.CurrencyID.Value,
                                                 Code = j.CurrencyCode,
                                                 Description = j.CurrencyDescription
                                             },
                                             CustomerName = j.CustomerName,
                                             IsJointAccount = j.IsJointAccount
                                         }).ToList();

                    customer.Underlyings = (from cu in context.SP_GetUnderlyingFXChecker(TransactionID)
                                            select new CustomerUnderlyingModel()
                                            {
                                                ID = cu.UnderlyingID.Value,
                                                Amount = cu.Amount.Value,
                                                Rate = cu.Rate,
                                                AmountUSD = cu.AmountUSD,
                                                AvailableAmount = cu.AvailableAmount,
                                                UtilizeAmountDeal = cu.UtilizeAmountDeal,
                                                AvailableAmountDeal = cu.Amount,

                                                AttachmentNo = cu.AttachmentNo,
                                                DateOfUnderlying = cu.DateOfUnderlying,
                                                EndDate = cu.EndDate,
                                                ExpiredDate = cu.ExpiredDate,
                                                InvoiceNumber = cu.InvoiceNumber,
                                                ReferenceNumber = cu.ReferenceNumber,
                                                StartDate = cu.StartDate,
                                                SupplierName = cu.SupplierName,
                                                IsDeclarationOfException = cu.IsDeclarationOfExecption.Value,
                                                IsUtilize = cu.IsUtilize.Value,
                                                IsJointAccount = cu.IsJointAccount.HasValue ? cu.IsJointAccount.Value : false,
                                                IsTMO = cu.IsTMO.HasValue ? cu.IsTMO.Value : false,
                                                AccountNumber = cu.AccountNumber,
                                                IsEnable = true,
                                                IsProforma = cu.IsProforma.Value,
                                                IsBulkUnderlying = cu.IsBulkUnderlying.Value,
                                                CreatedBy = cu.CreateBy,
                                                CreatedDate = cu.CreateDate.Value,
                                                LastModifiedBy = cu.UpdateDate == null ? cu.CreateBy : cu.UpdateBy,
                                                LastModifiedDate = cu.UpdateDate == null ? cu.CreateDate : cu.UpdateDate.Value,
                                                StatementLetter = new StatementLetterModel()
                                                {
                                                    ID = cu.StatementLetterID.Value,
                                                    Name = cu.StatementLetterName,
                                                    LastModifiedBy = cu.UpdateDateSL == null ? cu.CreateBySL : cu.UpdateBySL,
                                                    LastModifiedDate = cu.UpdateDateSL == null ? cu.CreateDateSL : cu.UpdateDateSL
                                                },
                                                Currency = new CurrencyModel()
                                                {
                                                    ID = cu.CurrencyID.Value,
                                                    Code = cu.CurrencyCode,
                                                    Description = cu.CurrencyDescription,
                                                    LastModifiedBy = cu.UpdateDateCcy == null ? cu.CreateByCcy : cu.UpdateByCcy,
                                                    LastModifiedDate = cu.UpdateDateCcy == null ? cu.CreateDateCcy : cu.UpdateDateCcy
                                                },
                                                DocumentType = new DocumentTypeModel()
                                                {
                                                    ID = cu.DocTypeID.Value,
                                                    Name = cu.DocTypeName,
                                                    Description = cu.DocTypeDescription,
                                                    LastModifiedBy = cu.UpdateDateDC == null ? cu.CreateByDC : cu.UpdateByDC,
                                                    LastModifiedDate = cu.UpdateDateDC == null ? cu.CreateDateDC : cu.UpdateDateDC
                                                },
                                                UnderlyingDocument = new UnderlyingDocModel()
                                                {
                                                    ID = cu.UnderlyingDocID.Value,
                                                    Code = cu.UnderlyingDocCode,
                                                    Name = cu.UnderlyingDocName,
                                                    LastModifiedBy = cu.UpdateDateUD == null ? cu.CreateByUD : cu.UpdateByUD,
                                                    LastModifiedDate = cu.UpdateDateUD == null ? cu.CreateDateUD : cu.UpdateDateUD
                                                },
                                                ProformaDetails = (from cuy in context.CustomerUnderlyings
                                                                   join cm in context.MappingUnderlyings.Where(x => x.IsDeleted.Equals(false)) on cuy.UnderlyingID equals cm.UnderlyingID
                                                                   where cm.ParentID == cu.UnderlyingID
                                                                   select new CustomerUnderlyingDetailModel
                                                                   {
                                                                       StatementLetter = new StatementLetterModel()
                                                                       {
                                                                           ID = cuy.StatementLetter.StatementLetterID,
                                                                           Name = cuy.StatementLetter.StatementLetterName,
                                                                           LastModifiedBy = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateBy : cuy.StatementLetter.UpdateBy,
                                                                           LastModifiedDate = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateDate : cuy.StatementLetter.UpdateDate.Value
                                                                       },
                                                                       Amount = cuy.Amount,
                                                                       Rate = cuy.Rate,
                                                                       AmountUSD = cuy.AmountUSD,
                                                                       AvailableAmount = cuy.AvailableAmountUSD,
                                                                       AttachmentNo = cuy.AttachmentNo,
                                                                       Currency = new CurrencyModel()
                                                                       {
                                                                           ID = cuy.Currency.CurrencyID,
                                                                           Code = cuy.Currency.CurrencyCode,
                                                                           Description = cuy.Currency.CurrencyDescription,
                                                                           LastModifiedBy = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateBy : cuy.Currency.UpdateBy,
                                                                           LastModifiedDate = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateDate : cuy.Currency.UpdateDate.Value
                                                                       },
                                                                       DateOfUnderlying = cuy.DateOfUnderlying,
                                                                       DocumentType = new DocumentTypeModel()
                                                                       {
                                                                           ID = cuy.DocumentType.DocTypeID,
                                                                           Name = cuy.DocumentType.DocTypeName,
                                                                           Description = cuy.DocumentType.DocTypeDescription,
                                                                           LastModifiedBy = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateBy : cuy.DocumentType.UpdateBy,
                                                                           LastModifiedDate = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateDate : cuy.DocumentType.UpdateDate.Value
                                                                       },
                                                                       InvoiceNumber = cuy.InvoiceNumber,
                                                                       ReferenceNumber = cuy.ReferenceNumber,
                                                                       SupplierName = cuy.SupplierName,
                                                                       ExpiredDate = cuy.ExpiredDate,
                                                                       UnderlyingDocument = new UnderlyingDocModel()
                                                                       {
                                                                           ID = cuy.UnderlyingDocument.UnderlyingDocID,
                                                                           Code = cuy.UnderlyingDocument.UnderlyingDocCode,
                                                                           Name = cuy.UnderlyingDocument.UnderlyingDocName,
                                                                           LastModifiedBy = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateBy : cuy.UnderlyingDocument.UpdateBy,
                                                                           LastModifiedDate = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateDate : cuy.UnderlyingDocument.UpdateDate.Value
                                                                       }
                                                                   }).ToList(),
                                                BulkDetails = (from cuy in context.CustomerUnderlyings
                                                               join cm in context.MappingBulkUnderlyings.Where(x => x.IsDeleted.Equals(false)) on cuy.UnderlyingID equals cm.UnderlyingID
                                                               where cm.ParentID == cu.UnderlyingID
                                                               select new CustomerUnderlyingDetailModel
                                                               {
                                                                   StatementLetter = new StatementLetterModel()
                                                                   {
                                                                       ID = cuy.StatementLetter.StatementLetterID,
                                                                       Name = cuy.StatementLetter.StatementLetterName,
                                                                       LastModifiedBy = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateBy : cuy.StatementLetter.UpdateBy,
                                                                       LastModifiedDate = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateDate : cuy.StatementLetter.UpdateDate.Value
                                                                   },
                                                                   Amount = cuy.Amount,
                                                                   Rate = cuy.Rate,
                                                                   AmountUSD = cuy.AmountUSD,
                                                                   AvailableAmount = cuy.AvailableAmountUSD,
                                                                   AttachmentNo = cuy.AttachmentNo,
                                                                   Currency = new CurrencyModel()
                                                                   {
                                                                       ID = cuy.Currency.CurrencyID,
                                                                       Code = cuy.Currency.CurrencyCode,
                                                                       Description = cuy.Currency.CurrencyDescription,
                                                                       LastModifiedBy = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateBy : cuy.Currency.UpdateBy,
                                                                       LastModifiedDate = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateDate : cuy.Currency.UpdateDate.Value
                                                                   },
                                                                   DateOfUnderlying = cuy.DateOfUnderlying,
                                                                   DocumentType = new DocumentTypeModel()
                                                                   {
                                                                       ID = cuy.DocumentType.DocTypeID,
                                                                       Name = cuy.DocumentType.DocTypeName,
                                                                       Description = cuy.DocumentType.DocTypeDescription,
                                                                       LastModifiedBy = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateBy : cuy.DocumentType.UpdateBy,
                                                                       LastModifiedDate = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateDate : cuy.DocumentType.UpdateDate.Value
                                                                   },
                                                                   InvoiceNumber = cuy.InvoiceNumber,
                                                                   ReferenceNumber = cuy.ReferenceNumber,
                                                                   SupplierName = cuy.SupplierName,
                                                                   ExpiredDate = cuy.ExpiredDate,
                                                                   UnderlyingDocument = new UnderlyingDocModel()
                                                                   {
                                                                       ID = cuy.UnderlyingDocument.UnderlyingDocID,
                                                                       Code = cuy.UnderlyingDocument.UnderlyingDocCode,
                                                                       Name = cuy.UnderlyingDocument.UnderlyingDocName,
                                                                       LastModifiedBy = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateBy : cuy.UnderlyingDocument.UpdateBy,
                                                                       LastModifiedDate = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateDate : cuy.UnderlyingDocument.UpdateDate.Value
                                                                   }
                                                               }).ToList()
                                            }).ToList();
                }
                else
                {
                    message = "Customer empty data or has been deleted.";
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerByTransactionNetting(long TransactionID, ref CustomerModel customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                customer = (from a in context.Customers
                            join b in context.TransactionNettings on a.CIF equals b.CIF
                            where a.IsDeleted.Equals(false)
                            && b.TransactionNettingID == TransactionID
                            select new CustomerModel
                            {
                                CIF = a.CIF,
                                Name = a.CustomerName,
                                POAName = a.POAName,

                                BizSegment = new BizSegmentModel()
                                {
                                    ID = a.BizSegment.BizSegmentID,
                                    Name = a.BizSegment.BizSegmentName,
                                    Description = a.BizSegment.BizSegmentDescription,
                                    LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                    LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                },

                                RM = a.CustomerMappings.Where(x => x.IsDeleted.Equals(false)).Select(x => new RMModel()
                                {
                                    ID = x.Employee.EmployeeID,
                                    Name = x.Employee.EmployeeName,
                                    SegmentID = x.Employee.BizSegmentID,
                                    BranchID = x.Employee.LocationID,
                                    RankID = x.Employee.RankID,
                                    Email = x.Employee.EmployeeEmail,
                                    LastModifiedBy = x.Employee.UpdateDate == null ? x.Employee.CreateBy : x.Employee.UpdateBy,
                                    LastModifiedDate = x.Employee.UpdateDate == null ? x.Employee.CreateDate : x.Employee.UpdateDate.Value
                                }).FirstOrDefault(),
                                Underlyings = context.TransactionNettingUnderlyings.Where(xx => xx.TransactionNettingID == b.TransactionNettingID && xx.IsDeleted.Equals(false)).Join(context.CustomerUnderlyings, tu => tu.UnderlyingID, cu => cu.UnderlyingID, (tu, cu) => new CustomerUnderlyingModel()
                                {
                                    ID = cu.UnderlyingID,
                                    StatementLetter = new StatementLetterModel()
                                    {
                                        ID = cu.StatementLetter.StatementLetterID,
                                        Name = cu.StatementLetter.StatementLetterName,
                                        LastModifiedBy = cu.StatementLetter.UpdateDate == null ? cu.StatementLetter.CreateBy : cu.StatementLetter.UpdateBy,
                                        LastModifiedDate = cu.StatementLetter.UpdateDate == null ? cu.StatementLetter.CreateDate : cu.StatementLetter.UpdateDate.Value
                                    },
                                    Amount = cu.Amount,
                                    Rate = cu.Rate,
                                    AmountUSD = cu.AmountUSD,
                                    AvailableAmount = cu.AvailableAmountUSD,
                                    AttachmentNo = cu.AttachmentNo,
                                    Currency = new CurrencyModel()
                                    {
                                        ID = cu.Currency.CurrencyID,
                                        Code = cu.Currency.CurrencyCode,
                                        Description = cu.Currency.CurrencyDescription,
                                        LastModifiedBy = cu.Currency.UpdateDate == null ? cu.Currency.CreateBy : cu.Currency.UpdateBy,
                                        LastModifiedDate = cu.Currency.UpdateDate == null ? cu.Currency.CreateDate : cu.Currency.UpdateDate.Value
                                    },
                                    DateOfUnderlying = cu.DateOfUnderlying,
                                    DocumentType = new DocumentTypeModel()
                                    {
                                        ID = cu.DocumentType.DocTypeID,
                                        Name = cu.DocumentType.DocTypeName,
                                        Description = cu.DocumentType.DocTypeDescription,
                                        LastModifiedBy = cu.DocumentType.UpdateDate == null ? cu.DocumentType.CreateBy : cu.DocumentType.UpdateBy,
                                        LastModifiedDate = cu.DocumentType.UpdateDate == null ? cu.DocumentType.CreateDate : cu.DocumentType.UpdateDate.Value
                                    },
                                    EndDate = cu.EndDate,
                                    ExpiredDate = cu.ExpiredDate,
                                    InvoiceNumber = cu.InvoiceNumber,
                                    ReferenceNumber = cu.ReferenceNumber,
                                    StartDate = cu.StartDate,
                                    SupplierName = cu.SupplierName,
                                    UnderlyingDocument = new UnderlyingDocModel()
                                    {
                                        ID = cu.UnderlyingDocument.UnderlyingDocID,
                                        Code = cu.UnderlyingDocument.UnderlyingDocCode,
                                        Name = cu.UnderlyingDocument.UnderlyingDocName,
                                        LastModifiedBy = cu.UnderlyingDocument.UpdateDate == null ? cu.UnderlyingDocument.CreateBy : cu.UnderlyingDocument.UpdateBy,
                                        LastModifiedDate = cu.UnderlyingDocument.UpdateDate == null ? cu.UnderlyingDocument.CreateDate : cu.UnderlyingDocument.UpdateDate.Value
                                    },
                                    IsDeclarationOfException = cu.IsDeclarationOfExecption,
                                    IsUtilize = cu.IsUtilize,
                                    IsJointAccount = cu.IsJointAccount.HasValue ? cu.IsJointAccount.Value : false,
                                    IsTMO = cu.IsTMO.HasValue ? cu.IsTMO.Value : false,
                                    AccountNumber = cu.AccountNumber,
                                    IsEnable = true,
                                    IsProforma = cu.IsProforma,
                                    IsBulkUnderlying = cu.IsBulkUnderlying.Value,
                                    ProformaDetails = (from cuy in context.CustomerUnderlyings
                                                       join cm in context.MappingUnderlyings.Where(x => x.IsDeleted.Equals(false)) on cuy.UnderlyingID equals cm.UnderlyingID
                                                       where cm.ParentID == cu.UnderlyingID
                                                       select new CustomerUnderlyingDetailModel
                                                       {
                                                           StatementLetter = new StatementLetterModel()
                                                           {
                                                               ID = cuy.StatementLetter.StatementLetterID,
                                                               Name = cuy.StatementLetter.StatementLetterName,
                                                               LastModifiedBy = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateBy : cuy.StatementLetter.UpdateBy,
                                                               LastModifiedDate = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateDate : cuy.StatementLetter.UpdateDate.Value
                                                           },
                                                           Amount = cuy.Amount,
                                                           Rate = cuy.Rate,
                                                           AmountUSD = cuy.AmountUSD,
                                                           AvailableAmount = cuy.AvailableAmountUSD,
                                                           AttachmentNo = cuy.AttachmentNo,
                                                           Currency = new CurrencyModel()
                                                           {
                                                               ID = cuy.Currency.CurrencyID,
                                                               Code = cuy.Currency.CurrencyCode,
                                                               Description = cuy.Currency.CurrencyDescription,
                                                               LastModifiedBy = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateBy : cuy.Currency.UpdateBy,
                                                               LastModifiedDate = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateDate : cuy.Currency.UpdateDate.Value
                                                           },
                                                           DateOfUnderlying = cuy.DateOfUnderlying,
                                                           DocumentType = new DocumentTypeModel()
                                                           {
                                                               ID = cuy.DocumentType.DocTypeID,
                                                               Name = cuy.DocumentType.DocTypeName,
                                                               Description = cuy.DocumentType.DocTypeDescription,
                                                               LastModifiedBy = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateBy : cuy.DocumentType.UpdateBy,
                                                               LastModifiedDate = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateDate : cuy.DocumentType.UpdateDate.Value
                                                           },
                                                           InvoiceNumber = cuy.InvoiceNumber,
                                                           ReferenceNumber = cuy.ReferenceNumber,
                                                           SupplierName = cuy.SupplierName,
                                                           ExpiredDate = cuy.ExpiredDate,
                                                           UnderlyingDocument = new UnderlyingDocModel()
                                                           {
                                                               ID = cuy.UnderlyingDocument.UnderlyingDocID,
                                                               Code = cuy.UnderlyingDocument.UnderlyingDocCode,
                                                               Name = cuy.UnderlyingDocument.UnderlyingDocName,
                                                               LastModifiedBy = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateBy : cuy.UnderlyingDocument.UpdateBy,
                                                               LastModifiedDate = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateDate : cuy.UnderlyingDocument.UpdateDate.Value
                                                           }
                                                       }).ToList(),
                                    BulkDetails = (from cuy in context.CustomerUnderlyings
                                                   join cm in context.MappingBulkUnderlyings.Where(x => x.IsDeleted.Equals(false)) on cuy.UnderlyingID equals cm.UnderlyingID
                                                   where cm.ParentID == cu.UnderlyingID
                                                   select new CustomerUnderlyingDetailModel
                                                   {
                                                       StatementLetter = new StatementLetterModel()
                                                       {
                                                           ID = cuy.StatementLetter.StatementLetterID,
                                                           Name = cuy.StatementLetter.StatementLetterName,
                                                           LastModifiedBy = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateBy : cuy.StatementLetter.UpdateBy,
                                                           LastModifiedDate = cuy.StatementLetter.UpdateDate == null ? cuy.StatementLetter.CreateDate : cuy.StatementLetter.UpdateDate.Value
                                                       },
                                                       Amount = cuy.Amount,
                                                       Rate = cuy.Rate,
                                                       AmountUSD = cuy.AmountUSD,
                                                       AvailableAmount = cuy.AvailableAmountUSD,
                                                       AttachmentNo = cuy.AttachmentNo,
                                                       Currency = new CurrencyModel()
                                                       {
                                                           ID = cuy.Currency.CurrencyID,
                                                           Code = cuy.Currency.CurrencyCode,
                                                           Description = cuy.Currency.CurrencyDescription,
                                                           LastModifiedBy = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateBy : cuy.Currency.UpdateBy,
                                                           LastModifiedDate = cuy.Currency.UpdateDate == null ? cuy.Currency.CreateDate : cuy.Currency.UpdateDate.Value
                                                       },
                                                       DateOfUnderlying = cuy.DateOfUnderlying,
                                                       DocumentType = new DocumentTypeModel()
                                                       {
                                                           ID = cuy.DocumentType.DocTypeID,
                                                           Name = cuy.DocumentType.DocTypeName,
                                                           Description = cuy.DocumentType.DocTypeDescription,
                                                           LastModifiedBy = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateBy : cuy.DocumentType.UpdateBy,
                                                           LastModifiedDate = cuy.DocumentType.UpdateDate == null ? cuy.DocumentType.CreateDate : cuy.DocumentType.UpdateDate.Value
                                                       },
                                                       InvoiceNumber = cuy.InvoiceNumber,
                                                       ReferenceNumber = cuy.ReferenceNumber,
                                                       SupplierName = cuy.SupplierName,
                                                       ExpiredDate = cuy.ExpiredDate,
                                                       UnderlyingDocument = new UnderlyingDocModel()
                                                       {
                                                           ID = cuy.UnderlyingDocument.UnderlyingDocID,
                                                           Code = cuy.UnderlyingDocument.UnderlyingDocCode,
                                                           Name = cuy.UnderlyingDocument.UnderlyingDocName,
                                                           LastModifiedBy = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateBy : cuy.UnderlyingDocument.UpdateBy,
                                                           LastModifiedDate = cuy.UnderlyingDocument.UpdateDate == null ? cuy.UnderlyingDocument.CreateDate : cuy.UnderlyingDocument.UpdateDate.Value
                                                       }
                                                   }).ToList(),
                                    LastModifiedBy = cu.UpdateDate == null ? cu.CreateBy : cu.UpdateBy,
                                    LastModifiedDate = cu.UpdateDate == null ? cu.CreateDate : cu.UpdateDate.Value
                                }).ToList(),
                                LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                            }).SingleOrDefault();

                if (customer != null)
                {
                    string _CIF = customer.CIF;
                    var CustomerModel = (from a in context.Customers
                                         where a.CIF == _CIF
                                         select a).SingleOrDefault();


                    //if (CustomerModel.CustomerAccounts != null)
                    //{
                    customer.Accounts = (from j in context.SP_GETJointAccount(_CIF)
                                         select new CustomerAccountModel
                                         {
                                             AccountNumber = j.AccountNumber,
                                             Currency = new CurrencyModel
                                             {
                                                 ID = j.CurrencyID.Value,
                                                 Code = j.CurrencyCode,
                                                 Description = j.CurrencyDescription
                                             },
                                             CustomerName = j.CustomerName,
                                             IsJointAccount = j.IsJointAccount
                                         }).ToList();
                    //}
                }
                else
                {
                    message = "Customer empty data or has been deleted.";
                }


                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


    }
    #endregion
}