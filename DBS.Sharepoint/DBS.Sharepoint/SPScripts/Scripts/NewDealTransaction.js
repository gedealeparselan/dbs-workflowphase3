// JavaScript source code

/**
 * Created by SPAdmin on 21/11/2014.
 */

var accessToken;

/* dodit@2014.11.08:add format function */
var formatNumber = function (num) {
    if (num != null) {
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    }
}

var $box;
var $remove = false;
var cifData = '0';
var customerNameData = '';
var DocumentModels = ko.observable({ FileName: '', DocumentPath: '' });
var DealModel = { cif: cifData, token: accessToken }
var idrrate = 0;

//#region Constanta Variable

var CONST_STATEMENT = {
    StatementA_ID: 1,
    StatementB_ID: 2,
    Dash_ID: 3,
    AnnualStatement_ID: 4
}
var CONST_MSG = {
    NotCompleteUpload: 'Please complete the upload form fields.',
    ActivityTitleNotFound: 'Something went worng, the activity title task not found.',
    ValidationFields: 'Mandatory field must be filled.',
    ValidationAnnualSL: 'Customer does not have annual statement.',
    ValidationEnter: 'Enter key is disabled for this form.',
    SuccessSave: 'Transaction data has been saved.',
    ValidationMinTotal: 'The Total Underlying Amount must be greater than Transaction Amount'
}
//#endregion

var AmountModel = {
    TotalAmountsUSD: ko.observable(0),
    RemainingBalance: ko.observable(0),
    TotalUtilization: ko.observable(0)
}


var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};

var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};

var StatementModel = {
    ID: ko.observable(0),
    Name: ko.observable("")
};

var BranchModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var TypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var RMModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    RankID: ko.observable(),
    Rank: ko.observable(),
    SegmentID: ko.observable(),
    Segment: ko.observable(),
    BranchID: ko.observable(),
    Branch: ko.observable(),
    Email: ko.observable()
};

var ContactModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    PhoneNumber: ko.observable(),
    DateOfBirth: ko.observable(),
    Address: ko.observable(),
    IDNumber: ko.observable()
};

var FunctionModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var CurrencyModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var ProductTypeModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    IsFlowValas: ko.observable(),
    Description: ko.observable()
};

var AccountModel = {
    AccountNumber: ko.observable(),
    CustomerName: ko.observable(),
    Currency: CurrencyModel,
    IsJointAccount: ko.observable()
};

var UnderlyingDocumentModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var DocumentTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var DocumentPurposeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var RateTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
}

var FXComplianceModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ChargesModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};


var UnderlyingModel = {
    ID: ko.observable(),
    UnderlyingDocument: ko.observable(UnderlyingDocumentModel),
    DocumentType: ko.observable(DocumentTypeModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    AttachmentNo: ko.observable(),
    IsStatementLetter: ko.observable(),
    IsDeclarationOfException: ko.observable(),
    StartDate: ko.observable(),
    EndDate: ko.observable(),
    DateOfUnderlying: ko.observable(),
    ExpiredDate: ko.observable(),
    ReferenceNumber: ko.observable(),
    SupplierName: ko.observable(),
    InvoiceNumber: ko.observable()
};

var DocumentModel = {
    ID: ko.observable(),
    Type: ko.observable(DocumentTypeModel),
    Purpose: ko.observable(DocumentPurposeModel),
    FileName: ko.observable(),
    DocumentPath: ko.observable(),
    LastModifiedDate: ko.observable(new Date),
    LastModifiedBy: ko.observable()
};

var CustomerModel = {
    CIF: ko.observable(),
    Name: ko.observable(),
    POAName: ko.observable(),
    Branch: ko.observable(BranchModel),
    Type: ko.observable(TypeModel),
    RM: ko.observable(RMModel),
    Contacts: ko.observableArray([ContactModel]),
    Functions: ko.observableArray([FunctionModel]),
    Accounts: ko.observableArray([AccountModel]),
    Underlyings: ko.observableArray([UnderlyingModel])
};

var TransactionDealModel = {
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    ValueDate: ko.observable(),
    Account: ko.observable(AccountModel),
    StatementLetter: ko.observable(StatementModel),
    TZRef: ko.observable(),
    RateType: ko.observable(RateTypeModel),
    Rate: ko.observable(0),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(0),
    AmountUSD: ko.observable(0),
    UnderlyingCurrency: ko.observable(CurrencyModel),
    UnderlyingAmount: ko.observable(0),
    UtilizedAmount: ko.observable(0),
    IsResident: ko.observable(false),
    CreateDate: ko.observable(new Date),
    TouchTimeStartDate: ko.observable(new Date().toUTCString())
};

var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};

var SelectModel = function (cif, id) {
    var self = this;
    self.ID = ko.observable(0);
    self.CIF = ko.observable(cif);
    self.UnderlyingID = ko.observable(id);
    self.ParentID = ko.observable(0);
}
var SelectBulkModel = function (cif, id) {
    var self = this;
    self.ID = ko.observable(0);
    self.CIF = ko.observable(cif);
    self.UnderlyingID = ko.observable(id);
    self.ParentID = ko.observable(0);
}

var UnderlyingDocumentModel2 = function (id, name, code) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Name = ko.observable(name);
    self.CodeName = ko.observable(code + ' (' + name + ')');
}

var DocumentTypeModel2 = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);

}

var StatementLetterModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}

var CurrencyModel2 = function (id, code, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Description = ko.observable(description);
}

var RateTypeModel2 = function (id, code, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Description = ko.observable(description);
}

var DocumentPurposeModel2 = function (id, name, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(description);
}

var CustomerUnderlyingMappingModel = function (FileID, UnderlyingID, attachNo) {
    var self = this;
    self.ID = ko.observable(0);
    self.UnderlyingID = ko.observable(UnderlyingID);
    self.UnderlyingFileID = ko.observable(FileID);
    self.AttachmentNo = ko.observable(attachNo);
}

var SelectUtillizeModel = function (id, enable, uSDAmount, statementLetter) {
    var self = this;
    self.ID = ko.observable(id);
    self.Enable = ko.observable(enable);
    self.USDAmount = ko.observable(uSDAmount);
    self.StatementLetter = ko.observable(statementLetter);
}
var today = Date.now();
var TransactionDealDetailModel = {
    TradeDate: ko.observable(moment(today).format(config.format.date)),
    ValueDate: ko.observable(moment(today).format(config.format.date)),
    AccountNumber: ko.observable(),
    StatementLetter: ko.observable(StatementModel),
    TZRef: ko.observable(),
    RateType: ko.observable(),
    ProductType: ko.observable(ProductTypeModel),
    Rate: ko.observable(),
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(0),
    AmountUSD: ko.observable(0),
    Account: ko.observable(AccountModel),
    Type: ko.observable(20),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]),
    DetailCompliance: ko.observable(),
    BeneName: ko.observable(),
    BeneAccNumber: ko.observable(),
    utilizationAmount: ko.observable(0.00),
    underlyings: ko.observableArray([]),
    TZReference: ko.observable(),
    IsBookDeal: ko.observable(false),
    bookunderlyingamount: ko.observable(),
    bookunderlyingcurrency: ko.observable(),
    bookunderlyingcode: ko.observable(),
    bookunderlyingdesc: ko.observable(),
    IsFxTransaction: ko.observable(false),
    OtherUnderlying: ko.observable(),
    IsOtherAccountNumber: ko.observable(false),
    OtherAccountNumber: ko.observable(),
    DebitCurrency: ko.observable(CurrencyModel),
    IsJointAccount: ko.observable(),
    IsResident: ko.observable(true),
    TouchTimeStartDate: ko.observable(new Date().toUTCString())
};

var Parameter = {
    Currencies: ko.observableArray([CurrencyModel]),
    ProductType: ko.observableArray([ProductTypeModel]),
    FXCompliances: ko.observableArray([FXComplianceModel]),
    DocumentTypes: ko.observableArray([DocumentTypeModel]),
    DocumentPurposes: ko.observableArray([DocumentPurposeModel]),
    RateType: ko.observableArray([RateTypeModel]),
    StatementLetter: ko.observableArray([StatementModel])
};

var SelectedModel = {
    Currency: ko.observable(),
    Account: ko.observable(),
    DocumentType: ko.observable(),
    DocumentPurpose: ko.observable(),
    NewCustomer: ko.observable({
        Currency: ko.observable()
    }),
    ProductType: ko.observable(),
    RateType: ko.observable(),
    StatementLetter: ko.observable(),
    DebitCurrency: ko.observable()
};

var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;
    self.IsRole = function (name) {
        var result = false;
        $.each($.cookie(api.cookie.spUser).Roles, function (index, item) {
            if (Const_RoleName[item.ID] == name) result = true; //if (item.Name == name) result = true;
        });
        return result;
    };

    // Sharepoint User
    self.SPUser = ko.observable(SPUser);

    // TresHold Value
    self.FCYIDRTresHold = ko.observable(0);

    self.IsEditTableUnderlying = ko.observable(true);

    // rate param
    self.Rate = ko.observable(0);

    // Is check attachments
    self.IsFXTransactionAttach = ko.observable(false);
    self.IsUploaded = ko.observable(false);
    self.IsUploading = ko.observable(false);
    self.SelectingUnderlying = ko.observable(false);
    // before value CurrencyID
    self.beforeValueCurrID = ko.observable(0);
    //bu group val
    self.isDealBUGroup = ko.observable(false);
    self.IsPermissionBookDeal = ko.observable(false);
    //new underlying type
    self.isNewUnderlying = ko.observable(false);
    self.underlyingCode = ko.observable();

    //is fx transaction
    self.t_IsFxTransaction = ko.observable(false);
    self.t_IsFxTransactionToIDR = ko.observable(false);

    //underlying declare
    self.AmountModel = ko.observable(AmountModel);

    // input form controls
    self.IsEditable = ko.observable(true);
    self.IsEnableSubmit = ko.observable(true);

    // Parameters
    self.Parameter = ko.observable(Parameter);

    // Main Model
    self.TransactionDealDetailModel = ko.observable(TransactionDealDetailModel);

    // Main Model
    self.TransactionDealModel = ko.observable(TransactionDealModel);

    // Double Transaction
    self.DoubleTransactions = ko.observableArray([]);

    // Validation User
    self.UserValidation = ko.observable({
        UserID: ko.observable(),
        Password: ko.observable(),
        Error: ko.observable()
    });
    self.IsResident = ko.observable(false);
    // ddl Joint Accounts
    //self.CIFJointAccounts = ko.observableArray([{ ID: false, Name: "Single" }, { ID: true, Name: "Join" }]);
    self.JointAccounts = ko.observableArray([{ ID: false, Name: "Single" }, { ID: true, Name: "Join" }]);
    self.DynamicAccounts = ko.observableArray([]);
    self.JointAccountNumbers = ko.observable([]);
    self.IsJointAccount = ko.observable(false); // disable dropdown Joint Account 
    self.IsHitThreshold = ko.observable(false);
    self.ThresholdType = ko.observable();
    self.ThresholdValue = ko.observable(0);
    self.IsNoThresholdValue = ko.observable(false);
    self.IsAnnualStatement = ko.observable(false);
    //self.TouchTimeStartDate = ko.observable(new Date().toUTCString());
    // Options selected value
    self.Selected = ko.observable(SelectedModel);
    // set Indicator Submited Statement letter
    self.Submitted = ko.observable();
    // Temporary documents
    self.Documents = ko.observableArray([]);
    self.DocumentPath = ko.observable();
    self.DocumentType = ko.observable();
    self.DocumentPurpose = ko.observable();
    self.DocumentFileName = ko.observable();
    self.IsEmptyAccountNumber = ko.observable(false);
    self.Rounding = ko.observable(0.00);
    self.ParameterRounding = ko.observable(0);
    self.TransactionDealDetailModel().IsResident = ko.observable(self.TransactionDealDetailModel().IsResident);
    self.TransactionDealDetailModel().IsResident.subscribe(function (newValue) {
        self.TransactionDealDetailModel().IsResident(newValue);
        GetTotalTransaction();
    });

    self.OnchangeProductType = function () {
        GetTotalTransaction();
    }
    self.OnchangeStatementLetter = function () {
        GetTotalTransaction();
    }
    self.OnTrnsCurrencyChange = function () {
        GetTotalTransaction();
    }

    // Uploading document
    self.UploadDocumentUnderlying = function () {

        // show the dialog task form
        $("#modal-form-Attach").modal('show');
        self.TempSelectedAttachUnderlying([]);
        self.ID_a(0);
        self.DocumentPurpose_a(new DocumentPurposeModel2('', '', ''));
        self.DocumentType_a(new DocumentTypeModel2('', ''));
        self.DocumentPath_a('');
        GetDataUnderlyingAttach();
        $('.remove').click();
    }

    self.UploadDocument = function () {
        self.DocumentPath(null);
        self.Selected().DocumentType(null);
        self.Selected().DocumentPurpose(null);
        self.DocumentType(null);
        self.DocumentPurpose(null);
        $("#modal-form-upload").modal('show');

    }

    // Add new document handler
    self.AddDocument = function () {
        var doc = {
            ID: 0,
            Type: self.DocumentType(),
            Purpose: self.DocumentPurpose(),
            FileName: self.DocumentFileName(),
            DocumentPath: self.DocumentPath(),
            LastModifiedDate: new Date(),
            LastModifiedBy: null
        };

        //alert(JSON.stringify(self.DocumentPath()))
        if (doc.Type == null || doc.Purpose == null || doc.DocumentPath == null || doc.DocumentPath == "") {
            alert("Please complete the upload form fields.")
        } else {
            self.IsFXTransactionAttach(true);
            self.Documents.push(doc);
            $('.remove').click();
            // hide upload dialog
            $("#modal-form-upload").modal('hide');
        }
    };

    self.EditDocument = function (data) {
        self.DocumentPath(data.DocumentPath);
        self.Selected().DocumentType(data.Type.ID);
        self.Selected().DocumentPurpose(data.Purpose.ID);

        // show upload dialog
        $("#modal-form-upload").modal('show');
    };
    self.CheckEmptyAccountNumber = function () {
        var AccountNumberSelected = $('#book-account option:selected').text().trim();
        if (AccountNumberSelected == '-') {
            self.IsEmptyAccountNumber(true);
            viewModel.TransactionDealDetailModel().AccountNumber(null);
            viewModel.TransactionDealDetailModel().IsOtherAccountNumber(true);
            var debitcurrency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().DebitCurrency(); });
            if (debitcurrency != null) {
                viewModel.TransactionDealDetailModel().DebitCurrency(debitcurrency);
                GetTotalTransaction();
            }
        } else {
            self.IsEmptyAccountNumber(false);
            viewModel.TransactionDealDetailModel().OtherAccountNumber(null);
            viewModel.TransactionDealDetailModel().IsOtherAccountNumber(false);
            GetTotalTransaction();
        }
    };

    self.onChangeAccountCCY = function () {
        var debitcurrency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().DebitCurrency(); });
        if (debitcurrency != null) {
            viewModel.TransactionDealDetailModel().DebitCurrency(debitcurrency);
            GetTotalTransaction();
        }
    };

    self.RemoveDocument = function (data) {
        //alert(JSON.stringify(data))
        self.Documents.remove(data);
    };

    // Save as draft handler
    self.Submit = function () {
        self.IsEnableSubmit(false);
        //self.TransactionDealDetailModel().CreateDate(self.TouchTimeStartDate());
        // validation
        self.IsDeal(false);
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            if (viewModel.Selected().StatementLetter() !== CONST_STATEMENT.AnnualStatement_ID) {
                GetThreshold(true);
            } else {
                GetAnnualStatement(cifData, true);
            }
            //ValidateTransaction();
        }
        else {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
            self.IsEnableSubmit(true);
        }
    };

    // User Validation process
    self.ValidationProcess = function () {
        UploadDocuments(); // start upload docs and save the transaction
    };

    // Continue editing
    self.ContinueEditing = function () {
        // enable form input controls
        self.IsEditable(true);
    };


    //--------------------------- set Underlying function & variable start
    self.IsUnderlyingMode = ko.observable(true);
    self.ID_u = ko.observable("");
    self.CIF_u = ko.observable(cifData);
    self.CustomerName_u = ko.observable(customerNameData);
    self.UnderlyingDocument_u = ko.observable(new UnderlyingDocumentModel2('', '', ''));
    self.ddlUnderlyingDocument_u = ko.observableArray([]);
    self.DocumentType_u = ko.observable(new DocumentTypeModel2('', ''));
    self.ddlDocumentType_u = ko.observableArray([]);

    self.AmountUSD_u = ko.observable(0.00);

    //self.Currency_u = ko.observableArray([new CurrencyModel2('', '', '')]);
    self.Currency_u = ko.observable(new CurrencyModel2('', '', ''));

    //self.RateType_u = ko.observableArray([new RateTypeModel2('', '', '')]);
    self.RateType_u = ko.observable(new RateTypeModel2('', '', ''));

    self.Rate_u = ko.observable(0);

    self.ddlCurrency_u = ko.observableArray([]);
    self.ddlRateType_u = ko.observableArray([]);

    //self.StatementLetter_u = ko.observableArray([new StatementLetterModel('', '')]);
    self.StatementLetter_u = ko.observable(new StatementLetterModel('', ''));
    self.ddlStatementLetter = ko.observableArray([]);
    self.ddlStatementLetter_u = ko.observableArray([]);
    self.Amount_u = ko.observable(0);
    self.AttachmentNo_u = ko.observable("");
    self.SelectUnderlyingDocument_u = ko.observable("");
    self.IsDeclarationOfException_u = ko.observable(false);
    self.StartDate_u = ko.observable('');
    self.EndDate_u = ko.observable('');
    self.DateOfUnderlying_u = ko.observable();
    self.ExpiredDate_u = ko.observable("");
    self.ReferenceNumber_u = ko.observable("");
    self.SupplierName_u = ko.observable("");
    self.InvoiceNumber_u = ko.observable("");
    self.IsStatementA = ko.observable(true);
    self.IsProforma_u = ko.observable(false);
    self.IsSelectedProforma = ko.observable(false);
    self.IsUtilize_u = ko.observable(false);
    self.IsJointAccount_u = ko.observable(false);
    self.AccountNumber_u = ko.observable("");
    // add bulkUnderlying // add 2015.03.09
    self.IsBulkUnderlying_u = ko.observable(false);
    self.IsSelectedBulk = ko.observable(false);
    // temp amount_u for bulk underlying
    self.tempAmountBulk = ko.observable(0);
    /*   self.Amounttmp_u = ko.computed({
           read: function () {
               var fromFormat = document.getElementById("Amount_u").value;
               fromFormat = fromFormat.toString().replace(/,/g, '');
               if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementA_ID) {
                   self.Currency_u().ID(1);
                   self.Amount_u(formatNumber(DataModel.ThresholdBuy));
                   // set default last day of month
                   var date = new Date();
                   var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                   var day = lastDay.getDate();
                   var month = lastDay.getMonth() + 1;
                   var year = lastDay.getFullYear()
                   var fixLastDay = year + "/" + month + "/" + day;
                   self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
                   SetDefaultValueStatementA();
               } else if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementB_ID && self.DateOfUnderlying_u() != "" && self.IsNewDataUnderlying()) {
                   self.ClearUnderlyingData();
                   if (self.LocalDate(self.DateOfUnderlying_u()) != "") {
                       var date = new Date(self.DateOfUnderlying_u());
                       var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                       var day = date.getDate();
                       var month = lastDay.getMonth() + 13;
                       var year = lastDay.getFullYear();
                       if (month > 12) {
                           month -= 12;
                           year += 1;
                       }
                       var fixLastDay = year + "/" + month + "/" + day;
                       if (!moment(fixLastDay, 'YYYY/MM/DD').isValid()) {
                           var LastDay_ = new Date(year, month, 0);
                           day = LastDay_.getDate();
                       }
                       self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
                   }
               }
   
               var e = document.getElementById("Amount_u").value;
               e = e.toString().replace(/,/g, '');
   
               var res = parseFloat(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
               res = Math.round(res * 100) / 100;
               res = isNaN(res) ? 0 : res; //avoid NaN
               self.AmountUSD_u(parseFloat(res).toFixed(2));
               self.Amount_u(formatNumber(e));
   
           },
           write: function (data) {
               var fromFormat = document.getElementById("Amount_u").value;
               fromFormat = fromFormat.toString().replace(/,/g, '');
   
   
               var res = parseInt(fromFormat) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
               res = Math.round(res * 100) / 100;
               res = isNaN(res) ? 0 : res; //avoid NaN
               self.AmountUSD_u(parseFloat(res).toFixed(2));
               self.Amount_u(formatNumber(fromFormat));
   
           }
       }, this); */

    self.CaluclateRate_u = ko.computed({
        read: function () {
            var CurrencyID = self.Currency_u().ID();
            var AccountID = self.Selected().Account;
            if (CurrencyID != '' && CurrencyID != undefined) {
                GetRateIDRAmount(CurrencyID);
            } else {
                self.Rate_u(0);
            }
            if (AccountID == '' && AccountID != undefined) {
                alert('test');
            }
        },
        write: function () { }
    });

    self.Proformas = ko.observableArray([new SelectModel('', '')]);
    self.BulkUnderlyings = ko.observableArray([new SelectBulkModel('', '')]); // add 2015.03.09
    self.CodeUnderlying = ko.computed({
        read: function () {
            if (self.UnderlyingDocument_u().ID() != undefined && self.UnderlyingDocument_u().ID() != '') {
                var item = ko.utils.arrayFirst(self.ddlUnderlyingDocument_u(), function (x) {
                    return self.UnderlyingDocument_u().ID() == x.ID();
                }
                )
                if (item != null) {
                    return item.Code();
                }
                else {
                    return ''
                }
            } else { return '' }
        },
        write: function (value) {
            return value;
        }
    });

    self.OtherUnderlying_u = ko.observable('');
    self.tmpOtherUnderlying = ko.observable('');
    self.IsSelected_u = ko.observable(false);
    self.LastModifiedBy_u = ko.observable("");
    self.LastModifiedDate_u = ko.observable("");

    // Attach Variable
    self.ID_a = ko.observable(0);
    self.FileName_a = ko.observable('');
    self.DocumentPurpose_a = ko.observable(new DocumentPurposeModel2('', '', ''));
    self.ddlDocumentPurpose_a = ko.observableArray([]);
    self.ddlDocumentPurposeNoFx = ko.observableArray([]);
    self.DocumentType_a = ko.observable(new DocumentTypeModel2('', ''));
    self.ddlDocumentType_a = ko.observableArray([]);
    self.DocumentRefNumber_a = ko.observable("");
    self.LastModifiedBy_a = ko.observable("");
    self.LastModifiedDate_a = ko.observable("");
    self.CustomerUnderlyingMappings_a = ko.observableArray([new CustomerUnderlyingMappingModel('', '', '')]);

    self.Documents = ko.observableArray([]);
    self.DocumentPath_a = ko.observable("");

    // filter Underlying
    self.UnderlyingFilterCIF = ko.observable("");
    self.UnderlyingFilterParentID = ko.observable("");
    self.UnderlyingFilterIsSelected = ko.observable(false);
    self.UnderlyingFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingFilterDocumentType = ko.observable("");
    self.UnderlyingFilterCurrency = ko.observable("");
    self.UnderlyingFilterStatementLetter = ko.observable("");
    self.UnderlyingFilterAmount = ko.observable("");
    self.UnderlyingFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingFilterExpiredDate = ko.observable("");
    self.UnderlyingFilterReferenceNumber = ko.observable("");
    self.UnderlyingFilterSupplierName = ko.observable("");
    self.UnderlyingFilterIsProforma = ko.observable("");
    self.UnderlyingFilterIsAvailable = ko.observable(false);
    self.UnderlyingFilterIsExpiredDate = ko.observable(false);
    self.UnderlyingFilterIsSelectedBulk = ko.observable(false);
    self.UnderlyingFilterAvailableAmount = ko.observable("");
    self.UnderlyingFilterAttachmentNo = ko.observable("");
    self.UnderlyingFilterAccountNumber = ko.observable("");
    self.UnderlyingFilterShow = ko.observable(1);

    // filter Attach Underlying File
    self.AttachFilterFileName = ko.observable("");
    self.AttachFilterDocumentPurpose = ko.observable("");
    self.AttachFilterModifiedDate = ko.observable("");
    self.AttachFilterDocumentRefNumber = ko.observable("");
    self.AttachFilterAttachmentReference = ko.observable("");
    self.AttachFilterAccountNumber = ko.observable("");

    // filter Underlying for Attach
    self.UnderlyingAttachFilterIsSelected = ko.observable("");
    self.UnderlyingAttachFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingAttachFilterDocumentType = ko.observable("");
    self.UnderlyingAttachFilterCurrency = ko.observable("");
    self.UnderlyingAttachFilterStatementLetter = ko.observable("");
    self.UnderlyingAttachFilterAmount = ko.observable("");
    self.UnderlyingAttachFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingAttachFilterExpiredDate = ko.observable("");
    self.UnderlyingAttachFilterReferenceNumber = ko.observable("");
    self.UnderlyingAttachFilterSupplierName = ko.observable("");
    self.UnderlyingAttachFilterAvailableAmount = ko.observable("");
    self.UnderlyingAttachFilterAccountNumber = ko.observable("");

    // filter Underlying for Attach
    self.ProformaFilterIsSelected = ko.observable(false);
    self.ProformaFilterUnderlyingDocument = ko.observable("");
    self.ProformaFilterDocumentType = ko.observable("Proforma");
    self.ProformaFilterCurrency = ko.observable("");
    self.ProformaFilterStatementLetter = ko.observable("");
    self.ProformaFilterAmount = ko.observable("");
    self.ProformaFilterDateOfUnderlying = ko.observable("");
    self.ProformaFilterExpiredDate = ko.observable("");
    self.ProformaFilterReferenceNumber = ko.observable("");
    self.ProformaFilterSupplierName = ko.observable("");
    self.ProformaFilterIsProforma = ko.observable(false);

    // filter Underlying for Attach // add 2015.03.09
    self.BulkFilterIsSelected = ko.observable(false);
    self.BulkFilterUnderlyingDocument = ko.observable("");
    self.BulkFilterDocumentType = ko.observable("");
    self.BulkFilterCurrency = ko.observable("");
    self.BulkFilterStatementLetter = ko.observable("");
    self.BulkFilterAmount = ko.observable("");
    self.BulkFilterDateOfUnderlying = ko.observable("");
    self.BulkFilterExpiredDate = ko.observable("");
    self.BulkFilterReferenceNumber = ko.observable("");
    self.BulkFilterSupplierName = ko.observable("");
    self.BulkFilterInvoiceNumber = ko.observable("");
    self.BulkFilterIsBulkUnderlying = ko.observable(false);

    //// start Checked Calculate for Attach Grid and Underlying Grid
    self.TempSelectedAttachUnderlying = ko.observableArray([]);

    //// start Checked Calculate for Underlying Proforma Grid
    self.TempSelectedUnderlyingProforma = ko.observableArray([]);

    //// start Checked Calculate for Underlying Bulk Grid // add 2015.03.09
    self.TempSelectedUnderlyingBulk = ko.observableArray([]);

    //// start Checked Calculate for Underlying Grid
    self.TempSelectedUnderlying = ko.observableArray([]);

    function ResetDataAttachment(index, isSelect) {
        self.CustomerAttachUnderlyings()[index].IsSelectedAttach = isSelect;
        var dataRows = self.CustomerAttachUnderlyings().slice(0);
        self.CustomerAttachUnderlyings([]);
        self.CustomerAttachUnderlyings(dataRows);
    }

    function ResetDataProforma(index, isSelect) {
        self.CustomerUnderlyingProformas()[index].IsSelectedProforma = isSelect;
        var dataRows = self.CustomerUnderlyingProformas().slice(0);
        self.CustomerUnderlyingProformas([]);
        self.CustomerUnderlyingProformas(dataRows);
    }

    function ResetBulkData(index, isSelect) { //add 2015.03.09
        self.CustomerBulkUnderlyings()[index].IsSelectedBulk = isSelect;
        var dataRows = self.CustomerBulkUnderlyings().slice(0);
        self.CustomerBulkUnderlyings([]);
        self.CustomerBulkUnderlyings(dataRows);
    }

    function ResetDataUnderlying(index, isSelect) {
        self.CustomerUnderlyings()[index].IsEnable = isSelect;
        var dataRows = self.CustomerUnderlyings().slice(0);
        self.CustomerUnderlyings([]);
        self.CustomerUnderlyings(dataRows);
    }

    function SetSelectedProforma(data) {
        // if(self.TempSelectedUnderlyingProforma().length>0) {
        for (var i = 0 ; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlyingProforma(), function (item) {
                return item.ID == data[i].ID;
            });
            if (selected != null) {
                data[i].IsSelectedProforma = true;
            }
            else {
                data[i].IsSelectedProforma = false;
            }
        }
    }

    function SetSelectedBulk(data) { //add 2015.03.09

        for (var i = 0 ; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlyingBulk(), function (item) {
                return item.ID == data[i].ID;
            });
            if (selected != null) {
                data[i].IsSelectedBulk = true;
            }
            else {
                data[i].IsSelectedBulk = false;
            }
        }
    }

    function TotalUtilize() {
        var total = 0;
        ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (item) {
            total += parseFloat(ko.utils.unwrapObservable(item.USDAmount()));
        });
        var fixTotal = parseFloat(total).toFixed(2);
        return fixTotal;
    }
    function SetDefaultValueStatementA() {
        var today = Date.now();
        var documentType = ko.utils.arrayFirst(self.ddlDocumentType_u(), function (item) { return item.Name == '-' });
        var underlyingDocument = ko.utils.arrayFirst(self.ddlUnderlyingDocument_u(), function (item) { return item.Code() == 998 });

        if (documentType != null) {
            self.DocumentType_u(new DocumentTypeModel2(documentType.ID, documentType.Name));
        }

        if (underlyingDocument != null) {
            self.UnderlyingDocument_u(new UnderlyingDocumentModel2(underlyingDocument.ID(), underlyingDocument.Name(), underlyingDocument.Code()));
        }
        self.DateOfUnderlying_u(self.LocalDate(today, true, false));
        self.SupplierName_u('-');
        self.InvoiceNumber_u('-');
    }
    function GetRateIDRAmount(CurrencyID) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
            params: {
            },
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    self.Rate_u(data.RupiahRate);
                    //if(CurrencyID!=1){ // if not USD

                    var fromFormat = document.getElementById("Amount_u").value;

                    fromFormat = fromFormat.toString().replace(/,/g, '');

                    res = parseFloat(fromFormat) * data.RupiahRate / idrrate;
                    res = Math.round(res * 100) / 100;
                    res = isNaN(res) ? 0 : res; //avoid NaN
                    self.AmountUSD_u(parseFloat(res).toFixed(2));

                    self.Amount_u(formatNumber(fromFormat));
                    //}
                }
            }
        });
    }

    function SetSelectedUnderlying(data) {
        for (var i = 0; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (item) {
                return item.ID() == data[i].ID;
            });
            if (selected != null) {
                data[i].IsEnable = true;
            }
            else {
                data[i].IsEnable = false;
            }
        }
        return data;
    }

    self.onSelectionAttach = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            //self.IsSelectedAttach(false);
            self.TempSelectedAttachUnderlying.remove(data);
            ResetDataAttachment(index, false);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        } else {
            //self.IsSelectedAttach(true);
            self.TempSelectedAttachUnderlying.push({
                ID: item.ID
            });
            ResetDataAttachment(index, true);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        }
    }

    self.onSelectionProforma = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlyingProforma(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            //self.IsSelectedAttach(false);
            self.TempSelectedUnderlyingProforma.remove(data);
            ResetDataProforma(index, false);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        } else {
            //self.IsSelectedAttach(true);
            self.TempSelectedUnderlyingProforma.push({
                ID: item.ID
            });
            ResetDataProforma(index, true);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        }
    }

    // add 2015.03.09
    self.OnCurrencyChange = function (CurrencyID) {
        if (CurrencyID != null) {
            var item = ko.utils.arrayFirst(self.ddlCurrency_u(), function (x) { return CurrencyID == x.ID(); });
            if (item != null) {
                self.Currency_u(new CurrencyModel2(item.ID(), item.Code(), item.Description()));
            }
            if (!self.IsNewDataUnderlying()) {
                GetSelectedBulkID(GetDataBulkUnderlying);
            } else {
                self.TempSelectedUnderlyingBulk([]);
                GetDataBulkUnderlying();
            }
        }
    }
    self.onSelectionBulk = function (index, item) {
        // var temp = self.Amount_u();
        var total = 0;
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlyingBulk(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            //self.IsSelectedAttach(false);
            self.TempSelectedUnderlyingBulk.remove(data);
            ResetBulkData(index, false);
            //console.log(ko.toJSON(self.TempSelectedAttachUnderlying()));
        } else {
            //self.IsSelectedAttach(true);
            self.TempSelectedUnderlyingBulk.push({
                ID: item.ID,
                Amount: item.Amount
            });
            ResetBulkData(index, true);
        }
        for (var i = 0; self.TempSelectedUnderlyingBulk.push() > i; i++) {
            total += self.TempSelectedUnderlyingBulk()[i].Amount;
        }
        if (total > 0) {
            //self.Amount_u(total);
            var e = total;
            var res = parseInt(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res).toFixed(2));
            self.Amount_u(formatNumber(e));
        } else {
            var e = self.tempAmountBulk();
            var res = parseInt(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res).toFixed(2));
            self.Amount_u(formatNumber(e));
            //self.Amount_u(self.tempAmountBulk());
        }

    }

    self.OnChangeJointAcc = function (obj, event) {
        if (obj.IsJointAccount != null) {
            var dataAccounts = ko.utils.arrayFilter(viewModel.TransactionDealDetailModel().Customer().Accounts(), function (dta) {
                var sAccount = dta.IsJointAccount == null ? false : dta.IsJointAccount;
                return sAccount == obj.IsJointAccount()
            });
            if (dataAccounts != null && dataAccounts.length > 0) {
                self.DynamicAccounts([]);
                if (obj.IsJointAccount() == false) {
                    dataAccounts = AddOtherAccounts(dataAccounts);
                    //single account               
                    self.UnderlyingFilterShow(1); // show single account
                    self.GetDataUnderlying();
                }
                self.DynamicAccounts(dataAccounts);
            } else {
                if (!obj.IsJointAccount()) {
                    self.DynamicAccounts([]);
                    if (obj.IsJointAccount() == false) {
                        dataAccounts = AddOtherAccounts(dataAccounts);
                    }
                    self.DynamicAccounts(dataAccounts);
                    //single account               
                    self.UnderlyingFilterShow(1); // show single account
                    self.GetDataUnderlying();
                }
            }
        }
    }
    self.OnChangeJointAccUnderlying = function () {
        if (!CustomerUnderlying.IsJointAccount()) {
            CustomerUnderlying.AccountNumber(null);
            //SetStatementA(false, null);
        }
    }
    self.OnchangeAccountNumber = function () {

    }
    self.TransactionDealDetailModel().bookunderlyingcode.subscribe(function (underlyingID) {
        //get underlying code based on selected
        if (underlyingID > 0) {
            GetUnderlyingDocName(underlyingID);
        }
    });
    self.GetTotalTransaction = function () {
        GetTotalTransaction();
    }
    function GetUnderlyingDocName(underlyingID) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.underlyingdoc + "/" + underlyingID,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if (data != null) {
                        self.isNewUnderlying(false); //return default disabled

                        // available type to input for new underlying name if code is 999
                        if (data.Code == '999') {
                            self.TransactionDealDetailModel().bookunderlyingdesc('');
                            self.isNewUnderlying(true);
                            $('#book-underlying-desc').focus();

                            $('#book-underlying-desc').attr('data-rule-required', true);
                        }
                        else {
                            self.isNewUnderlying(false);
                            self.TransactionDealDetailModel().OtherUnderlying('');

                            $('#book-underlying-desc').attr('data-rule-required', false);
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    self.onSelectionUtilize = function (index, item) {

        var data = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
            return x.ID() == item.ID;
        });

        if (data != null) { //uncheck condition

            //console.log('onSelectionUtilize data not null');

            self.TempSelectedUnderlying.remove(data);
            ResetDataUnderlying(index, false);
            setTotalUtilize();

        } else {
            /*if (self.Selected().Currency() != item.Currency.ID) {
                ResetDataUnderlying(index, false);
                ShowNotification("Form Validation Warning", "Please select same currency", 'gritter-warning', false);
            }*/

            var amount = document.getElementById("new-eqv-usd").value;
            if (amount != null) {
                amount = amount.replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '');
            }
            var statementA = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                return x.StatementLetter() == CONST_STATEMENT.StatementA_ID;
            });
            var statementB = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                return x.StatementLetter() == CONST_STATEMENT.StatementB_ID;
            });
            var annualState = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                return x.StatementLetter() == CONST_STATEMENT.AnnualStatement;
            });
            if (statementA != null && item.StatementLetter.ID == CONST_STATEMENT.StatementA_ID ||
                statementB != null && item.StatementLetter.ID == CONST_STATEMENT.StatementB_ID ||
                annualState != null && item.StatementLetter.ID == CONST_STATEMENT.AnnualStatement ||
                self.Selected().StatementLetter() == item.StatementLetter.ID) {
                self.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, false, item.AvailableAmount, item.StatementLetter.ID));
                ResetDataUnderlying(index, true);
                setTotalUtilize();
            } else {
                ResetDataUnderlying(index, false);
            }

        }

    }

    // New Data flag
    self.IsDeal = ko.observable(false);

    // New Data flag
    self.IsNewDataUnderlying = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyings = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyingProformas = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyingFiles = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerAttachUnderlyings = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerBulkUnderlyings = ko.observableArray([]); // add 2015.03.09

    // grid properties Underlying
    self.UnderlyingGridProperties = ko.observable();
    self.UnderlyingGridProperties(new GridPropertiesModel(GetDataUnderlying));

    // grid properties Attach Grid
    self.AttachGridProperties = ko.observable();
    self.AttachGridProperties(new GridPropertiesModel(GetDataAttachFile));

    // grid properties Underlying File
    self.UnderlyingAttachGridProperties = ko.observable();
    self.UnderlyingAttachGridProperties(new GridPropertiesModel(GetDataUnderlyingAttach));

    // grid properties Proforma
    self.UnderlyingProformaGridProperties = ko.observable();
    self.UnderlyingProformaGridProperties(new GridPropertiesModel(GetDataUnderlyingProforma));

    // grid properties Bulk // 2015.03.09
    self.UnderlyingBulkGridProperties = ko.observable();
    self.UnderlyingBulkGridProperties(new GridPropertiesModel(GetDataBulkUnderlying));

    // set default sorting for Underlying Grid
    self.UnderlyingGridProperties().SortColumn("ID");
    self.UnderlyingGridProperties().SortOrder("ASC");

    // set default sorting for Underlying File Grid
    self.AttachGridProperties().SortColumn("FileName");
    self.AttachGridProperties().SortOrder("ASC");

    // set default sorting for Attach Underlying Grid
    self.UnderlyingAttachGridProperties().SortColumn("ID");
    self.UnderlyingAttachGridProperties().SortOrder("ASC");

    // set default sorting for Proforma grid
    self.UnderlyingProformaGridProperties().SortColumn("IsSelectedProforma");
    self.UnderlyingProformaGridProperties().SortOrder("DESC");

    // set default sorting for Bulk grid //add 2015.03.09
    self.UnderlyingBulkGridProperties().SortColumn("IsSelectedBulk");
    self.UnderlyingBulkGridProperties().SortOrder("DESC");

    // bind clear filters
    self.UnderlyingClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.UnderlyingFilterIsSelected(false);
        self.UnderlyingFilterUnderlyingDocument("");
        self.UnderlyingFilterAvailableAmount("");
        self.UnderlyingFilterDocumentType("");
        self.UnderlyingFilterCurrency("");
        self.UnderlyingFilterStatementLetter("");
        self.UnderlyingFilterAmount("");
        self.UnderlyingFilterDateOfUnderlying("");
        self.UnderlyingFilterExpiredDate("");
        self.UnderlyingFilterReferenceNumber("");
        self.UnderlyingFilterSupplierName("");
        self.UnderlyingFilterIsProforma("");
        self.UnderlyingFilterAttachmentNo("");
        self.UnderlyingFilterIsAvailable(false);
        self.UnderlyingFilterIsExpiredDate(false);

        GetDataUnderlying();
    };

    self.ProformaClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.ProformaFilterIsSelected(false);
        self.ProformaFilterUnderlyingDocument("");
        self.ProformaFilterDocumentType("Proforma");
        self.ProformaFilterCurrency("");
        self.ProformaFilterStatementLetter("");
        self.ProformaFilterAmount("");
        self.ProformaFilterDateOfUnderlying("");
        self.ProformaFilterExpiredDate("");
        self.ProformaFilterReferenceNumber("");
        self.ProformaFilterSupplierName("");
        self.ProformaFilterIsProforma("");

        GetDataUnderlyingProforma();
    };
    //add 2015.03.09
    self.BulkClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.BulkFilterIsSelected(false);
        self.BulkFilterUnderlyingDocument("");
        self.BulkFilterDocumentType("");
        self.BulkFilterCurrency("");
        self.BulkFilterStatementLetter("");
        self.BulkFilterAmount("");
        self.BulkFilterDateOfUnderlying("");
        self.BulkFilterExpiredDate("");
        self.BulkFilterReferenceNumber("");
        self.BulkFilterInvoiceNumber("");
        self.BulkFilterSupplierName("");
        self.BulkFilterIsBulkUnderlying("");

        GetDataBulkUnderlying();
    };
    // bind clear filters
    self.AttachClearFilters = function () {
        self.AttachFilterFileName("");
        self.AttachFilterDocumentPurpose("");
        self.AttachFilterModifiedDate("");
        self.AttachFilterDocumentRefNumber("");
        self.AttachFilterAttachmentReference("");
        self.AttachFilterAccountNumber("");
        GetDataAttachFile();
    };

    // bind clear filters
    self.UnderlyingAttachClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.UnderlyingAttachFilterIsSelected(false);
        self.UnderlyingAttachFilterUnderlyingDocument("");
        self.UnderlyingAttachFilterDocumentType("");
        self.UnderlyingAttachFilterCurrency("");
        self.UnderlyingAttachFilterStatementLetter("");
        self.UnderlyingAttachFilterAmount("");
        self.UnderlyingAttachFilterDateOfUnderlying("");
        self.UnderlyingAttachFilterExpiredDate("");
        self.UnderlyingAttachFilterReferenceNumber("");
        self.UnderlyingAttachFilterSupplierName("");
        self.UnderlyingAttachFilterAccountNumber("");

        GetDataUnderlyingAttach();
    };

    self.GetApplicationID = function () { GetApplicationID(); };

    // bind get data function to view
    self.GetDataUnderlying = function () {
        GetDataUnderlying();
    };

    self.GetSelectedUtilizeID = function () {
        GetSelectedUtilizeID();
    }

    self.GetDataAttachFile = function () {
        GetDataAttachFile();
    }

    self.GetDataUnderlyingAttach = function () {
        GetDataUnderlyingAttach();
    }

    self.GetDataUnderlyingProforma = function () {
        GetDataUnderlyingProforma();
    }

    // 2015.03.09
    self.GetDataBulkUnderlying = function () {
        GetDataBulkUnderlying();
    }

    self.GetUnderlyingParameters = function (data) {
        GetUnderlyingParameters(data);
    }


    self.CalculateFX = function (amountUSD) {
        CalculateFX(amountUSD);
    }
    self.SetCalculateFX = function (amountUSD) {
        SetCalculateFX(amountUSD);
    }
    self.NewTransaction = function () {
        NewTransaction();
    }
    self.SetDynamicAccount = function () {
        SetDynamicAccount();
    }
    //The Object which stored data entered in the observables
    var CustomerUnderlying = {
        ID: self.ID_u,
        CIF: self.CIF_u,
        CustomerName: self.CustomerName_u,
        UnderlyingDocument: self.UnderlyingDocument_u,
        DocumentType: self.DocumentType_u,
        Currency: self.Currency_u,
        Amount: self.Amount_u,
        Rate: self.Rate_u,
        AmountUSD: self.AmountUSD_u,
        AvailableAmountUSD: self.AvailableAmount_u,
        AttachmentNo: self.AttachmentNo_u,
        StatementLetter: self.StatementLetter_u,
        IsDeclarationOfException: self.IsDeclarationOfException_u,
        StartDate: self.StartDate_u,
        EndDate: self.EndDate_u,
        DateOfUnderlying: self.DateOfUnderlying_u,
        ExpiredDate: self.ExpiredDate_u,
        ReferenceNumber: self.ReferenceNumber_u,
        SupplierName: self.SupplierName_u,
        InvoiceNumber: self.InvoiceNumber_u,
        IsSelectedProforma: self.IsSelectedProforma,
        IsProforma: self.IsProforma_u,
        IsUtilize: self.IsUtilize_u,
        IsEnable: self.IsEnable_u,
        Proformas: self.Proformas,
        IsSelectedBulk: self.IsSelectedBulk,
        IsBulkUnderlying: self.IsBulkUnderlying_u,
        BulkUnderlyings: self.BulkUnderlyings,
        OtherUnderlying: self.OtherUnderlying_u,
        IsJointAccount: self.IsJointAccount_u,
        AccountNumber: self.AccountNumber_u,
        LastModifiedBy: self.LastModifiedBy_u,
        LastModifiedDate: self.LastModifiedDate_u
    };

    var Documents = {
        Documents: self.Documents,
        DocumentPath: self.DocumentPath_a,
        Type: self.DocumentType_a,
        Purpose: self.DocumentPurpose_a
    }

    var CustomerUnderlyingFile = {
        ID: self.ID_a,
        FileName: DocumentModels().FileName,
        DocumentPath: DocumentModels().DocumentPath,
        DocumentPurpose: self.DocumentPurpose_a,
        DocumentType: self.DocumentType_a,
        DocumentRefNumber: self.DocumentRefNumber_a,
        LastModifiedBy: self.LastModifiedBy_a,
        LastModifiedDate: self.LastModifiedDate_a,
        CustomerUnderlyingMappings: self.CustomerUnderlyingMappings_a
    };

    function CalculateFX(amountUsd) {
        var t_TotalAmountsUSD = TotalDealModel.Total.IDRFCYDeal;
        var t_RemainingBalance = TotalDealModel.Total.RemainingBalance;
        viewModel.FCYIDRTresHold(TotalDealModel.FCYIDRTrashHold);
        viewModel.AmountModel().TotalUtilization(TotalDealModel.Total.UtilizationDeal);
        viewModel.TransactionDealDetailModel().AmountUSD(amountUsd);
        if (amountUsd != "" && amountUsd > 0) {
            var t_Treshold = TotalDealModel.TreshHold;
            var t_FCYTreshold = TotalDealModel.FCYIDRTrashHold;
            var t_FcyAmount = parseFloat(amountUsd);
            var t_IsFxTransaction = viewModel.t_IsFxTransaction();
            var t_IsFxTransactionToIDR = viewModel.t_IsFxTransactionToIDR();
            if (t_IsFxTransaction || t_IsFxTransactionToIDR) {
                t_TotalAmountsUSD = t_TotalAmountsUSD + parseFloat(amountUsd);
                t_TotalAmountsUSD = t_TotalAmountsUSD.toFixed(2);
            }
            if (viewModel.TransactionDealDetailModel().StatementLetter() != undefined) {
                if (viewModel.TransactionDealDetailModel().StatementLetter().ID == CONST_STATEMENT.StatementA_ID) {
                    if (t_TotalAmountsUSD <= t_Treshold) {
                        t_RemainingBalance = 0.00;
                    }
                } else {
                    t_RemainingBalance = t_RemainingBalance - t_FcyAmount;
                }
                if (t_IsFxTransactionToIDR) {
                    if (t_FcyAmount >= t_FCYTreshold && viewModel.TransactionDealDetailModel().ProductType().IsFlowValas == true) {
                        if (t_RemainingBalance == 0.00) {
                            t_RemainingBalance = t_RemainingBalance - t_FcyAmount;
                        }
                    }
                }
                //t_RemainingBalance = t_RemainingBalance.toFixed(2));
            }
        } else {
            var t_TotalAmountsUSD = TotalDealModel.Total.IDRFCYDeal;
            t_TotalAmountsUSD = t_TotalAmountsUSD.toFixed(2);
        }
        viewModel.AmountModel().TotalAmountsUSD(t_TotalAmountsUSD);
        viewModel.AmountModel().RemainingBalance(parseFloat(t_RemainingBalance).toFixed(2));
    }
    function GetApplicationID() {
        var applicationID = 'FX-' + viewModel.TransactionDealDetailModel().TZReference();
    };
    function GetRounding(totalUnderlying, parameterRounding) {
        if (totalUnderlying != null && parameterRounding != null) {
            var rounding = totalUnderlying % parameterRounding;
            if (rounding != null && rounding != 0) {
                return rounding = parseFloat(parameterRounding - rounding).toFixed(2);
            }
        }
        return 0;
    }
    function SetMappingAttachment() {
        CustomerUnderlyingFile.CustomerUnderlyingMappings([]);
        for (var i = 0 ; i < self.TempSelectedAttachUnderlying().length; i++) {
            CustomerUnderlyingFile.CustomerUnderlyingMappings.push(new CustomerUnderlyingMappingModel(0, self.TempSelectedAttachUnderlying()[i].ID, customerNameData + '-(A)'));
        }
    }

    function SetMappingProformas() {
        CustomerUnderlying.Proformas([]);
        for (var i = 0 ; i < self.TempSelectedUnderlyingProforma().length; i++) {
            CustomerUnderlying.Proformas.push(new SelectModel(cifData, self.TempSelectedUnderlyingProforma()[i].ID));
        }
    }

    function SetMappingBulks() { //add 2015.03.09

        CustomerUnderlying.BulkUnderlyings([]);
        for (var i = 0 ; i < self.TempSelectedUnderlyingBulk().length; i++) {
            CustomerUnderlying.BulkUnderlyings.push(new SelectBulkModel(cifData, self.TempSelectedUnderlyingBulk()[i].ID));
        }
    }

    function setTotalUtilize() {
        var total = 0;
        ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (item) {
            total += parseFloat(ko.utils.unwrapObservable(item.USDAmount()));
        });
        viewModel.TransactionDealDetailModel().utilizationAmount(parseFloat(total).toFixed(2));
        var sRounding = GetRounding(parseFloat(total), self.ParameterRounding());
        viewModel.Rounding(sRounding);
    }

    function setStatementA() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetStatementA/" + cifData,
            data: null, //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            }, success: function (data) {
                if (data.IsStatementA) {
                    self.StatementLetter_u(new StatementLetterModel(CONST_STATEMENT.StatementB_ID, 'Statement B'));
                    self.IsStatementA(false);
                } else { self.IsStatementA(true); }
            }
        });
    }
    function setRefID() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetRefID/" + cifData,
            data: null, //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            }, success: function (data) {
                self.ReferenceNumber_u(customerNameData + '-' + data.ID);
            }
        });

    }

    //add validation if statement B is choose then underlying doc select "tanpa underlying" by henggar wicaksana
    function IsValidUnderlying() {
        //var defaultvalue=0;
        //var tmpData = [];
        //for (var i = 0; i < viewModel.TransactionDealDetailModel().bookunderlyingcode(); i++) {
        //    if (viewModel.ddlUnderlyingDocument_u()[i].Name() == "tanpa underlying") {
        //        tmpData.push(viewModel.ddlUnderlyingDocument_u()[i]);
        //    }
        //}
        //var ArraySelected = i;
        //console.log(tmpData);
        //if (viewModel.Selected().StatementLetter() == Const_Underlying.SelectedStatement) {
        //    var SelectedUnderlying = Const_Underlying.SelectedUnderlying;
        //    if (ArraySelected == SelectedUnderlying) {
        //        ShowNotification("Form Validation Warning", "Please dont select 'tanpa undrlying' in Underlying Code", 'gritter-warning', true);
        //        defaultvalue = 1;
        //        return defaultvalue;
        //    }
        //}
        if (viewModel.Selected().StatementLetter() == Const_Underlying.SelectedStatement) {
            if (viewModel.TransactionDealDetailModel().bookunderlyingcode() == Const_Underlying.SelectedUnderlying) {
                ShowNotification("Form Validation Warning", "Please dont select 'tanpa undrlying' in Underlying Code", 'gritter-warning', true);
                return false;
            }
        }

        return true;
    }
    //end wicaksana

    // Save is book deal
    self.IsBooked = function () {
        viewModel.IsEditable(false);
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckAvailbaleBookNumber/" + viewModel.TransactionDealDetailModel().TZReference().trim(),
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (!data) {
                    self.IsDeal(true);
                    viewModel.TransactionDealDetailModel().IsFxTransaction(true);
                    /* if (viewModel.t_IsFxTransaction()) {
                         viewModel.TransactionDealDetailModel().IsFxTransaction(true);
                     }
                     else {
                         viewModel.TransactionDealDetailModel().IsFxTransaction(false);
                     }*/

                    $('#book-underlying-code').attr('data-rule-required', true);
                    $('#book-underlying-code').rules('add', 'required');

                    var applicationID = 'FX-' + viewModel.TransactionDealDetailModel().TZReference();
                    viewModel.TransactionDealDetailModel().ApplicationID(applicationID);

                    if (!IsValidUnderlying()) {
                        return false;
                    } else {
                        // validation
                        var form = $("#aspnetForm");
                        form.validate();

                        if (form.valid()) {
                            viewModel.IsEditable(false);
                            if (viewModel.t_IsFxTransaction() || viewModel.t_IsFxTransactionToIDR()) {
                                self.IsAnnualStatement(viewModel.Selected().StatementLetter() == CONST_STATEMENT.AnnualStatement_ID ? true : false);
                                if (viewModel.Selected().StatementLetter() !== CONST_STATEMENT.AnnualStatement_ID) {
                                    GetThreshold(true);
                                } else {
                                    GetAnnualStatement(cifData, true)
                                }
                            } else {
                                viewModel.IsEditable(true);
                                ShowNotification("Form Validation Warning", "Transaction FCY->FCY, please select IDR->FCY or FCY->IDR transaction", 'gritter-warning', false);
                            }
                        } else {
                            viewModel.IsEditable(true);
                            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
                            $('#book-underlying-code').attr('data-rule-required', false);
                            $('#book-underlying-code').rules('remove', 'required');
                        }
                    }

                    // validation
                    var form = $("#aspnetForm");
                    form.validate();


                }
                else {
                    ShowNotification("Form Validation Warning", "Booking Reference already exist. </b>Please view in FX Tracking.", 'gritter-warning', false);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);

            }
        });


    };

    self.save_u = function () {
        self.IsEditTableUnderlying(false);
        var form = $("#frmUnderlying");
        form.validate();

        if (form.valid()) {
            if (CustomerUnderlying.StatementLetter().ID() == CONST_STATEMENT.StatementB_ID && CustomerUnderlying.UnderlyingDocument().ID() == 36) {
                ShowNotification("Form Validation Warning", "Please Change Document Underlying other 998(tanpa underlying). ", 'gritter-warning', false);
                self.IsEditTableUnderlying(true);
                return false;
            }
            viewModel.Amount_u(viewModel.Amount_u().toString().replace(/,/g, ''));
            CustomerUnderlying.OtherUnderlying = self.CodeUnderlying() == '999' ? self.OtherUnderlying_u : '';
            SetMappingProformas();
            SetMappingBulks(); // add 2015.03.09
            CustomerUnderlying.AvailableAmountUSD = self.AmountUSD_u();
            //Ajax call to insert the CustomerUnderlyings
            $.ajax({
                type: "POST",
                url: api.server + api.url.customerunderlying + "/Save",
                data: ko.toJSON(CustomerUnderlying), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        $("#modal-form-Underlying").modal('hide');
                        self.IsEditTableUnderlying(true);
                        // refresh data
                        GetDataUnderlying();
                        GetTotalTransaction();
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                        self.IsEditTableUnderlying(true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    if (jqXHR.responseJSON.Message.toLowerCase().startsWith("double underlying")) {
                        ShowNotification("", jqXHR.responseJSON.Message, 'gritter-warning', true);
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    } self.IsEditTableUnderlying(true);
                }
            });
        } else {
            self.IsEditTableUnderlying(true);
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        }
    };

    function onSuccessRemainingBlnc() {
        //CalculateFX(viewModel.TransactionDealDetailModel().AmountUSD()); // changed PBI 
        var amountUSD = viewModel.TransactionDealDetailModel().AmountUSD();
        SetCalculateFX(amountUSD);
    }

    self.save_a = function () {

        var form = $("#frmUnderlying");
        form.validate();
        if (form.valid()) {
            viewModel.IsEditable(false);
            self.IsUploading(true)

            var FxWithUnderlying = (self.TempSelectedAttachUnderlying().length > 0);
            var FxWithNoUnderlying = (self.DocumentPurpose_a().ID() != 2);

            if (FxWithUnderlying || FxWithNoUnderlying) {
                var data = {
                    CIF: CustomerUnderlying.CIF,
                    Name: CustomerUnderlying.CustomerName,
                    Type: ko.utils.arrayFirst(self.ddlDocumentType_a(), function (item) {
                        return item.ID == self.DocumentType_a().ID();
                    }),
                    Purpose: ko.utils.arrayFirst(self.ddlDocumentPurpose_a(), function (item) {
                        return item.ID == self.DocumentPurpose_a().ID();
                    })
                };

                if (FxWithUnderlying) {
                    if (Documents.DocumentPath().name != null) {
                        UploadFileUnderlying(data, Documents, SaveDataFile);
                    } else {
                        ShowNotification("Form Validation Warning", "Please complete the upload form fields", 'gritter-warning', false);
                        //alert("Please complete the upload form fields.")
                        viewModel.IsEditable(true);
                        viewModel.IsUploading(false);
                        return;
                    }
                    // refresh data
                    GetDataUnderlying();
                }
                else if (FxWithNoUnderlying) {
                    self.IsFXTransactionAttach(true);

                    var doc = {
                        ID: 0,
                        Type: data.Type,
                        Purpose: data.Purpose,
                        FileName: Documents.DocumentPath().name.split('.')[Documents.DocumentPath().name.split('.').length - 2].replace(/[<>:"\/\\|?!@#$%^&*]+/g, '_') + "." + Documents.DocumentPath().name.split('.')[Documents.DocumentPath().name.split('.').length - 1], //aridya 20170131 untuk handle special char
                        DocumentPath: self.DocumentPath_a(),
                        LastModifiedDate: new Date(),
                        LastModifiedBy: null
                    };

                    //aridya add 20170131 untuk special char
                    var filename = Documents.DocumentPath().name.split('.')[Documents.DocumentPath().name.split('.').length - 2].replace(/[<>:"\/\\|?!@#$%^&*]+/g, '_');
                    var extension = Documents.DocumentPath().name.split('.')[Documents.DocumentPath().name.split('.').length - 1];
                    Documents.DocumentPath().name = filename + "." + extension;
                    //end aridya

                    //alert(JSON.stringify(self.DocumentPath()))
                    if (doc.Type == null || doc.Purpose == null || doc.DocumentPath == null || doc.DocumentPath == "") {
                        //alert("Please complete the upload form fields.")
                        ShowNotification("Form Validation Warning", "Please complete the upload form fields", 'gritter-warning', false);
                        viewModel.IsEditable(true);
                        viewModel.IsUploading(false);
                    } else {
                        self.Documents.push(doc);

                        $('.remove').click();

                        // hide upload dialog
                        $("#modal-form-Attach").modal('hide');
                        viewModel.IsEditable(true);
                        viewModel.IsUploading(false);
                    }
                }
            } else {
                viewModel.IsEditable(true);
                viewModel.IsUploading(false);
                //alert('Please select the underlying document');
                ShowNotification("Form Validation Warning", "Please select the underlying document", 'gritter-warning', false);

            }


        }
        else {
            viewModel.IsUploading(false);
        };
    };

    function SaveDataFile() {

        SetMappingAttachment();
        //Ajax call to insert the CustomerUnderlyings
        CustomerUnderlyingFile.FileName = DocumentModels().FileName;
        CustomerUnderlyingFile.DocumentPath = DocumentModels().DocumentPath;
        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlyingfile,
            data: ko.toJSON(CustomerUnderlyingFile), //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    viewModel.IsEditable(true);

                    // send notification
                    ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                    // hide current popup window
                    $("#modal-form-Attach").modal('hide');

                    viewModel.IsUploading(false);

                    // refresh data
                    GetDataAttachFile();
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    //viewModel.IsEditable(true);
                    viewModel.IsUploading(false);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                //viewModel.IsEditable(true);
                viewModel.IsUploading(false);
            }
        });
    }

    self.update_u = function () {
        var form = $("#frmUnderlying");
        form.validate();

        if (form.valid()) {
            if (CustomerUnderlying.StatementLetter().ID() == CONST_STATEMENT.StatementB_ID && CustomerUnderlying.UnderlyingDocument().ID() == 36) {
                ShowNotification("Form Validation Warning", "Please Change Document Underlying other 998(tanpa underlying). ", 'gritter-warning', false);
                self.IsEditTableUnderlying(true);
                return false;
            }
            viewModel.Amount_u(viewModel.Amount_u().toString().replace(/,/g, ''));
            // hide current popup window
            $("#modal-form-Underlying").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form-Underlying").modal('show');
                } else {
                    CustomerUnderlying.OtherUnderlying = self.CodeUnderlying() == '999' ? self.OtherUnderlying_u : '';
                    SetMappingProformas();
                    SetMappingBulks(); // add 2015.03.09
                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.customerunderlying + "/" + CustomerUnderlying.ID(),
                        data: ko.toJSON(CustomerUnderlying),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {

                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                $("#modal-form-Underlying").modal('hide');

                                // refresh data
                                GetDataUnderlying();


                                //new calculated
                                //DealModel.cif = cifData;
                                //GetRemainingBalance(DealModel, onSuccessRemainingBlnc, OnErrorDeal);
                                GetTotalTransaction();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            if (jqXHR.responseJSON.Message.toLowerCase().startsWith("double underlying")) {
                                ShowNotification("", jqXHR.responseJSON.Message, 'gritter-warning', true);
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                            }
                        }
                    });
                }
            });
        } else {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        }
    };

    self.delete_u = function () {
        // hide current popup window
        $("#modal-form-Underlying").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form-Underlying").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customerunderlying + "/remove/" + CustomerUnderlying.ID(),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {

                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                            // refresh data
                            GetDataUnderlying();

                            //new calculated
                            //DealModel.cif = cifData;
                            //GetRemainingBalance(DealModel, onSuccessRemainingBlnc, OnErrorDeal);
                            GetTotalTransaction();

                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    self.cancel_u = function () {
    }

    self.cancel_a = function () {
    }

    self.delete_a = function (item) {

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                //$("#modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customerunderlyingfile + "/" + item.ID,
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            DeleteFile(item.DocumentPath);
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetDataAttachFile();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    }

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);
        if (date == '1970/01/01 00:00:00') {
            return "";
        }

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    self.OnChangeStatementLetterUnderlying = function () {
        var fromFormat = document.getElementById("Amount_u").value;
        fromFormat = fromFormat.toString().replace(/,/g, '');
        if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementA_ID) {
            self.Currency_u().ID(1);
            self.Amount_u(formatNumber(DataModel.ThresholdBuy));
            // set default last day of month
            var date = new Date();
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var day = lastDay.getDate();
            var month = lastDay.getMonth() + 1;
            var year = lastDay.getFullYear()
            var fixLastDay = year + "/" + month + "/" + day;
            self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
            SetDefaultValueStatementA();
        } else if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementB_ID && self.IsNewDataUnderlying()) {
            self.ClearUnderlyingData();
            if (self.LocalDate(self.DateOfUnderlying_u()) != "") {
                var date = new Date(self.DateOfUnderlying_u());
                var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                var day = date.getDate();
                var month = lastDay.getMonth() + 13;
                var year = lastDay.getFullYear();
                if (month > 12) {
                    month -= 12;
                    year += 1;
                }
                var fixLastDay = year + "/" + month + "/" + day;
                if (!moment(fixLastDay, 'YYYY/MM/DD').isValid()) {
                    var LastDay_ = new Date(year, month, 0);
                    day = LastDay_.getDate();
                }
                self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
            }
        }

        var e = document.getElementById("Amount_u").value;
        e = e.toString().replace(/,/g, '');

        var res = parseFloat(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
        res = Math.round(res * 100) / 100;
        res = isNaN(res) ? 0 : res; //avoid NaN
        self.AmountUSD_u(parseFloat(res).toFixed(2));
        self.Amount_u(formatNumber(e));

    }
    self.onChangeDateOfUnderlying = function () {
        if (self.LocalDate(self.DateOfUnderlying_u()) != "") {
            if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementA_ID) {
                self.Currency_u().ID(1);
                self.Amount_u(formatNumber(DataModel.ThresholdBuy));
                var date = new Date();
                var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                var day = lastDay.getDate();
                var month = lastDay.getMonth() + 1;
                var year = lastDay.getFullYear()
                var fixLastDay = year + "/" + month + "/" + day;
                self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
                //SetDefaultValueStatementA();
            } else if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementB_ID && self.IsNewDataUnderlying()) {
                //self.ClearUnderlyingData();
                if (self.LocalDate(self.DateOfUnderlying_u()) != "") {
                    var date = new Date(self.DateOfUnderlying_u());
                    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                    var day = date.getDate();
                    var month = lastDay.getMonth() + 13;
                    var year = lastDay.getFullYear();
                    if (month > 12) {
                        month -= 12;
                        year += 1;
                    }
                    var fixLastDay = year + "/" + month + "/" + day;
                    if (!moment(fixLastDay, 'YYYY/MM/DD').isValid()) {
                        var LastDay_ = new Date(year, month, 0);
                        day = LastDay_.getDate();
                    }
                    self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
                }
            }
        }
    }
    //Function to Display record to be updated
    self.GetUnderlyingSelectedRow = function (data) {

        self.IsNewDataUnderlying(false);

        $("#modal-form-Underlying").modal('show');

        self.ID_u(data.ID);
        self.CustomerName_u(data.CustomerName);
        self.UnderlyingDocument_u(new UnderlyingDocumentModel2(data.UnderlyingDocument.ID, data.UnderlyingDocument.Name, data.UnderlyingDocument.Code));
        self.OtherUnderlying_u(data.OtherUnderlying);
        self.DocumentType_u(new DocumentTypeModel2(data.DocumentType.ID, data.DocumentType.Name));

        //self.Currency_u(data.Currency);
        self.Currency_u(new CurrencyModel2(data.Currency.ID, data.Currency.Code, data.Currency.Description));

        self.Amount_u(data.Amount);
        self.AttachmentNo_u(data.AttachmentNo);

        //self.StatementLetter_u(data.StatementLetter);
        self.StatementLetter_u(new StatementLetterModel(data.StatementLetter.ID, data.StatementLetter.Name));

        self.IsDeclarationOfException_u(data.IsDeclarationOfException);
        self.StartDate_u(self.LocalDate(data.StartDate, true, false));
        self.EndDate_u(self.LocalDate(data.EndDate, true, false));
        self.DateOfUnderlying_u(self.LocalDate(data.DateOfUnderlying, true, false));
        self.ExpiredDate_u(self.LocalDate(data.ExpiredDate, true, false));
        self.ReferenceNumber_u(data.ReferenceNumber);
        self.SupplierName_u(data.SupplierName);
        self.InvoiceNumber_u(data.InvoiceNumber);
        self.IsProforma_u(data.IsProforma);
        self.IsUtilize_u(data.IsUtilize);
        self.IsSelectedProforma(data.IsSelectedProforma);
        self.IsBulkUnderlying_u(data.IsBulkUnderlying); // add 2015.03.09
        self.IsSelectedBulk(data.IsSelectedBulk); // add 2015.03.09
        self.tempAmountBulk(data.Amount);
        self.IsJointAccount_u(data.IsJointAccount);
        self.AccountNumber_u(data.AccountNumber)
        self.LastModifiedBy_u(data.LastModifiedBy);
        self.LastModifiedDate_u(data.LastModifiedDate);
        //Call Grid Underlying for Check Proforma

        GetSelectedProformaID(GetDataUnderlyingProforma);
        GetSelectedBulkID(GetDataBulkUnderlying);

    };
    self.ClearUnderlyingData = function () {
        if (self.Selected().Currency() != self.Currency_u().ID && self.StatementLetter_u().ID != CONST_STATEMENT.StatementA_ID) {
            var cur = ko.utils.arrayFirst(self.ddlCurrency_u(), function (item) {
                return self.Selected().Currency() == item.ID();
            });

            if (cur != null) {
                self.Currency_u(new CurrencyModel2(cur.ID(), cur.Code(), cur.Description()));
            } else {
                self.Currency_u(new CurrencyModel2('', '', ''));
            }

        }
        self.UnderlyingDocument_u(new UnderlyingDocumentModel2('', '', ''));
        self.OtherUnderlying_u('');
        self.DocumentType_u(new DocumentTypeModel2('', ''));

        self.Amount_u(0);
        self.AttachmentNo_u();
        self.IsDeclarationOfException_u(false);
        self.StartDate_u('');
        self.EndDate_u('');
        self.ExpiredDate_u('');
        self.SupplierName_u('');
        self.InvoiceNumber_u('');
    }
    //Add Attach File
    self.NewAttachFile = function () {
        // show the dialog task form
        $("#modal-form-Attach").modal('show');
        self.TempSelectedAttachUnderlying([]);
        self.ID_a(0);
        self.DocumentPurpose_a(new DocumentPurposeModel2('', '', ''));
        self.DocumentType_a(new DocumentTypeModel2('', ''));
        self.DocumentPath_a('');
        GetDataUnderlyingAttach();
    }

    //insert new
    self.NewDataUnderlying = function () {
        //Remove required filed if show
        $('.form-group').removeClass('has-error').addClass('has-info');
        $('.help-block').remove();
        $("#modal-form-Underlying").modal('show');
        // flag as new Data
        self.IsNewDataUnderlying(true);

        // bind empty data
        self.ID_u(0);
        self.StatementLetter_u(new StatementLetterModel('', ''));
        self.ReferenceNumber_u('');
        self.DateOfUnderlying_u('');
        self.ClearUnderlyingData();
        self.IsProforma_u(false);
        self.IsSelectedProforma(false);
        self.IsUtilize_u(false);
        self.IsJointAccount_u(self.TransactionDealDetailModel().IsJointAccount());
        self.AccountNumber_u('');
        self.IsBulkUnderlying_u(false); //add 2015.03.09
        self.IsSelectedBulk(false); //add 2015.03.09
        if (self.IsJointAccount_u()) {
            self.AccountNumber_u(self.Selected().Account());
        }
        //Call Grid Underlying for Check Proforma
        GetDataUnderlyingProforma();
        GetDataBulkUnderlying();
        self.TempSelectedUnderlyingBulk([]);
        self.TempSelectedUnderlyingProforma([]);
        //setStatementA();
        setRefID();
    };

    // check permission for button book deal
    self.IsPermitedButtonBookDeal = function () {
        //Ajax call to delete the Customer
        var oSPUser = viewModel.SPUser();

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/Book Deal Transaction",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    for (i = 0; i < oSPUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[oSPUser.Roles[i].ID].toLowerCase() == data[j].toLowerCase()) { //if (oSPUser.Roles[i].Name.toLowerCase() == data[j].toLowerCase()) {
                                self.IsPermissionBookDeal(true);
                                return false;
                            }
                        }
                    }
                    self.IsPermissionBookDeal(false);
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    // Function to Read parameter Underlying
    function GetUnderlyingParameters(data) {

        var underlying = data['UnderltyingDoc'];
        for (var i = 0; i < underlying.length; i++) {
            self.ddlUnderlyingDocument_u.push(new UnderlyingDocumentModel2(underlying[i].ID, underlying[i].Name, underlying[i].Code));
        }

        self.ddlDocumentType_u(data['DocType']);
        self.ddlDocumentType_a(data['DocType']);
        ko.utils.arrayForEach(data['Currency'], function (item) {
            self.ddlCurrency_u.push(new CurrencyModel2(item.ID, item.Code, item.Description));
        });
        self.ddlDocumentPurpose_a(data['PurposeDoc']);
        self.ddlStatementLetter(data['StatementLetter']);
        ko.utils.arrayForEach(data['StatementLetter'], function (item) {
            if (item.ID == CONST_STATEMENT.StatementA_ID || item.ID == CONST_STATEMENT.StatementB_ID) {
                self.ddlStatementLetter_u.push(item);
            }
        });
        ko.utils.arrayForEach(data['PurposeDoc'], function (item) {
            if (item.ID != 2) { //tanpa underlying
                self.ddlDocumentPurposeNoFx.push(item);
            }
        });

        //self.ddlStatementLetter_u(data['StatementLetter']);

        self.ddlRateType_u(data['RateType']);
    }

    //Function to Read All Customers Underlying
    function GetDataUnderlying() {

        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/UnderlyingWithFilterExpiredDate",
            params: {
                page: self.UnderlyingGridProperties().Page(),
                size: self.UnderlyingGridProperties().Size(),
                sort_column: self.UnderlyingGridProperties().SortColumn(),
                sort_order: self.UnderlyingGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        self.UnderlyingFilterIsAvailable(true);
        self.UnderlyingFilterIsExpiredDate(true);
        var filters = GetUnderlyingFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        }
    }

    //Function to Read All Customers Underlying File
    function GetDataAttachFile() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlyingfile,
            params: {
                page: self.AttachGridProperties().Page(),
                size: self.AttachGridProperties().Size(),
                sort_column: self.AttachGridProperties().SortColumn(),
                sort_order: self.AttachGridProperties().SortOrder()
            },
            token: accessToken
        };
        self.AttachFilterAccountNumber(self.UnderlyingFilterAccountNumber());
        // get filtered columns
        var filters = GetAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        }
    }

    //Function to Read All Customers Underlying for Attach File
    function GetDataUnderlyingAttach() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Attach",
            params: {
                page: self.UnderlyingAttachGridProperties().Page(),
                size: self.UnderlyingAttachGridProperties().Size(),
                sort_column: self.UnderlyingAttachGridProperties().SortColumn(),
                sort_order: self.UnderlyingAttachGridProperties().SortOrder()
            },
            token: accessToken
        };
        self.UnderlyingAttachFilterAccountNumber(self.UnderlyingFilterAccountNumber());
        // get filtered columns
        var filters = GetUnderlyingAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingDataAttachFile, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingDataAttachFile, OnError, OnAlways);
        }
    }

    //Function to Read All Customers Underlying Proforma
    function GetDataUnderlyingProforma() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Proforma",
            params: {
                page: self.UnderlyingProformaGridProperties().Page(),
                size: self.UnderlyingProformaGridProperties().Size(),
                sort_column: self.UnderlyingProformaGridProperties().SortColumn(),
                sort_order: self.UnderlyingProformaGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetUnderlyingProformaFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingProforma, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingProforma, OnError, OnAlways);
        }
    }

    //Function to Read All Customers Bulk Underlying // add 2015.03.09
    function GetDataBulkUnderlying() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Bulk",
            params: {
                page: self.UnderlyingBulkGridProperties().Page(),
                size: self.UnderlyingBulkGridProperties().Size(),
                sort_column: self.UnderlyingBulkGridProperties().SortColumn(),
                sort_order: self.UnderlyingBulkGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns

        var filters = GetBulkUnderlyingFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingBulk, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingBulk, OnError, OnAlways);
        }
    }
    //Function Set Dynamic Account
    function SetDynamicAccount() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.customerAccount + "/" + cifData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TransactionDealDetailModel().Customer().Accounts(data);
                    self.DynamicAccounts([])
                    var dataAccount = ko.utils.arrayFilter(data, function (item) {
                        var sAccount = item.IsJointAccount == null ? false : item.IsJointAccount;
                        return sAccount == self.TransactionDealDetailModel().IsJointAccount();
                    });
                    if (!self.TransactionDealDetailModel().IsJointAccount()) {
                        dataAccount = AddOtherAccounts(dataAccount);
                    }
                    self.DynamicAccounts(dataAccount);

                    //check customer have joint account
                    var isJointAcc = ko.utils.arrayFilter(data, function (item) {
                        return true == item.IsJointAccount;
                    });
                    if (isJointAcc != null & isJointAcc.length == 0) {
                        self.IsJointAccount(false);
                    }
                    var jointAccount = ko.utils.arrayFilter(data, function (item) {
                        return true == item.IsJointAccount;
                    });
                    if (jointAccount != null && jointAccount.length > 0) {
                        self.JointAccountNumbers(jointAccount); // set drowpdownlist joint account underlying
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
    function NewTransaction() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckAvailbaleBookNumber/" + viewModel.TransactionDealDetailModel().TZReference().trim(),
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (!data) {
                    self.IsDeal(false);
                    var isFxTransaction = viewModel.t_IsFxTransaction();
                    var isFxTransactionToIDR = viewModel.t_IsFxTransactionToIDR();
                    var form = $("#aspnetForm");
                    form.validate();

                    var applicationID = 'FX-' + viewModel.TransactionDealDetailModel().TZReference();
                    viewModel.TransactionDealDetailModel().ApplicationID(applicationID);

                    if (!IsValidUnderlying()) {
                        return false;
                    } else {
                        if (form.valid()) {
                            if (isFxTransaction || isFxTransactionToIDR) {
                                var isSubmit = false;
                                self.IsAnnualStatement(viewModel.Selected().StatementLetter() == CONST_STATEMENT.AnnualStatement_ID ? true : false);
                                if (viewModel.Selected().StatementLetter() !== CONST_STATEMENT.AnnualStatement_ID) {
                                    GetThreshold(isSubmit);
                                } else {
                                    GetAnnualStatement(cifData, isSubmit)
                                }
                            } else {
                                viewModel.IsEditable(true);
                                ShowNotification("Form Validation Warning", "Transaction FCY->FCY, please select IDR->FCY or FCY->IDR transaction", 'gritter-warning', false);
                            }
                        } else {
                            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
                            $('#book-underlying-code').attr('data-rule-required', false);
                            $('#book-underlying-code').rules('remove', 'required');
                        }
                    }
                }
                else {
                    ShowNotification("Form Validation Warning", "Booking Reference already exist. </b>Please view in FX Tracking.", 'gritter-warning', false);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);

            }
        });


    }
    //#region Joint Account Function
    function UnCheckUnderlyingTable() {
        viewModel.TempSelectedUnderlying([]);
        var dataRows = [];
        ko.utils.arrayForEach(viewModel.CustomerUnderlyings(), function (item) {
            item.IsEnable = false;
            dataRows.push(item);
        });
        viewModel.CustomerUnderlyings([]);
        viewModel.CustomerUnderlyings(dataRows);
    }

    //#endregion
    //#region PBI Threshold 
    function GetThreshold(isSubmit) {
        var paramhreshold = {
            TzRef: viewModel.TransactionDealDetailModel().TZReference(),
            StatementLetterID: viewModel.TransactionDealDetailModel().StatementLetter().ID,
            ProductTypeID: viewModel.TransactionDealDetailModel().ProductType().ID,
            DebitCurrencyCode: viewModel.TransactionDealDetailModel().DebitCurrency().Code,
            TransactionCurrencyCode: viewModel.TransactionDealDetailModel().Currency().Code,
            IsResident: viewModel.TransactionDealDetailModel().IsResident(),
            AmountUSD: parseFloat(viewModel.TransactionDealDetailModel().AmountUSD()),
            CIF: viewModel.TransactionDealDetailModel().Customer().CIF(),
            AccountNumber: viewModel.TransactionDealDetailModel().IsOtherAccountNumber() ? null : viewModel.TransactionDealDetailModel().Account().AccountNumber,
            IsJointAccount: viewModel.TransactionDealDetailModel().IsJointAccount()
        }
        var options = {
            url: api.server + api.url.helper + "/GetThresholdFXValue",
            token: accessToken,
            params: {},
            data: ko.toJSON(paramhreshold)
        };
        Helper.Ajax.Post(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                if (IsValidationFX(data, paramhreshold.AmountUSD, isSubmit)) {
                    if (self.IsDeal()) {
                        SaveTransaction();
                        return;
                    }
                    if (!isSubmit) {
                        SuccessNewTransaction(data);
                    } else {
                        UploadDocuments();
                    }
                } else {
                    self.IsEditable(true);
                }
                // }
            }
        }, OnError);
    }
    function IsValidationFX(data, amountUSD, isSubmit) {
        var IsStatus = true;
        //var totalUnderlying = GetTotalUnderlyingRounding(data.RoundingValue);
        var totalUnderlying = GetTotalUnderlyingRounding(viewModel.Rounding());
        var statementLetterID = viewModel.TempSelectedUnderlying().length > 0 ? viewModel.TempSelectedUnderlying()[0].StatementLetter() : viewModel.TransactionDealDetailModel().StatementLetter().ID;
        if (self.IsNoThresholdValue() && statementLetterID != CONST_STATEMENT.Dash_ID) {
            viewModel.IsEditable(true);
            viewModel.IsEnableSubmit(true);
            ShowNotification("Form Validation Warning", "Transaction does not have threshold, Please Select (-) Statement Letter.", 'gritter-warning', false);
            return false;
        }
        if (!self.IsNoThresholdValue() && statementLetterID == CONST_STATEMENT.Dash_ID) {
            viewModel.IsEditable(true);
            viewModel.IsEnableSubmit(true);
            ShowNotification("Form Validation Warning", "Please Select Statement Letter.", 'gritter-warning', false);
            return false;
        }
        if (self.IsDeal() && statementLetterID == 1 && data.IsHitThreshold) {
            viewModel.IsEditable(true);
            viewModel.IsEnableSubmit(true);
            ShowNotification("Form Validation Warning", "Total Amount Statement A hit " + data.ThresholdValue + ", Please Select Statement B  ", 'gritter-warning', false);
            return false;
        }
        if (amountUSD <= totalUnderlying || !isSubmit || self.IsDeal() || self.IsNoThresholdValue()) {
            if (data.IsHitThreshold && statementLetterID == CONST_STATEMENT.StatementA_ID) {
                viewModel.IsEditable(true);
                viewModel.IsEnableSubmit(true);
                ShowNotification("Form Underlying Warning", "Total IDR->FCY Amount greater than " + data.ThresholdValue + " (USD), Please add statement B underlying", 'gritter-warning', false);
                return false;
            }
        } else {
            viewModel.IsEditable(true);
            viewModel.IsEnableSubmit(true);
            ShowNotification("Form Underlying Warning", "Total Underlying Amount must be equal greater than Transaction Amount", 'gritter-warning', false);
            IsStatus = false;
        }
        return IsStatus;
    }
    function GetTotalUnderlyingRounding(rounding) {
        var totalUnderlying;
        if (viewModel.TempSelectedUnderlying() != null && viewModel.TempSelectedUnderlying().length > 0) {
            switch (viewModel.TempSelectedUnderlying()[0].StatementLetter()) {
                case 2:
                    totalUnderlying = parseFloat(viewModel.TransactionDealDetailModel().utilizationAmount()) + parseFloat(rounding);
                    break;
                default:
                    totalUnderlying = parseFloat(viewModel.TransactionDealDetailModel().utilizationAmount());
                    break;
            }
        } else {
            totalUnderlying = parseFloat(viewModel.TransactionDealDetailModel().utilizationAmount());
        }
        return totalUnderlying;
    }
    function SuccessNewTransaction(data) {

        var isFxTransactionToIDR = viewModel.t_IsFxTransactionToIDR();
        var statementLeter = viewModel.Selected().StatementLetter();
        var amountUSD = parseFloat(viewModel.TransactionDealDetailModel().AmountUSD());

        /*if (isFxTransactionToIDR) {
            if (amountUSD >= data.ThresholdValue) {
                viewModel.Selected().StatementLetter(2);
            } else {
                viewModel.Selected().StatementLetter(3);
            }
        } else {
            if ((viewModel.TransactionDealDetailModel().Currency().Code != 'IDR' && statementLeter == 3)) {
                viewModel.IsEditable(true);
                ShowNotification("Form Validation Warning", "Please Select Statement Letter A, B or annual Statement letter.", 'gritter-warning', false);
                return;
            }
        } */
        var accountNumber = viewModel.Selected().Account() != null && viewModel.Selected().Account() != "";

        if (accountNumber && viewModel.TransactionDealDetailModel().IsJointAccount()) {
            viewModel.UnderlyingFilterAccountNumber(viewModel.Selected().Account());
            viewModel.UnderlyingFilterShow(2); // show data only joint account
            viewModel.GetDataUnderlying();
        } else {
            viewModel.UnderlyingFilterAccountNumber("");
            viewModel.UnderlyingFilterShow(1); // show data single account
            viewModel.GetDataUnderlying();
        }
        UnCheckUnderlyingTable();
        viewModel.TransactionDealDetailModel().utilizationAmount(0.00);
        viewModel.TempSelectedUnderlying([]);
        viewModel.GetDataUnderlying();
        viewModel.GetDataAttachFile();
        viewModel.IsDeal(false);
        $('#book-transaction').hide();
        $('#new-transaction').show();
        viewModel.IsEditable(true);
        $('#s4-workspace').animate({ scrollTop: 0 }, 'fast');
    }
    function GetTotalTransaction() {
        if (viewModel != null) {
            var paramhreshold = {
                ProductTypeID: viewModel.TransactionDealDetailModel().ProductType().ID,
                DebitCurrencyCode: viewModel.TransactionDealDetailModel().DebitCurrency().Code,
                TransactionCurrencyCode: viewModel.TransactionDealDetailModel().Currency().Code,
                IsResident: viewModel.TransactionDealDetailModel().IsResident(),
                AmountUSD: parseFloat(viewModel.TransactionDealDetailModel().AmountUSD()),
                CIF: viewModel.TransactionDealDetailModel().Customer().CIF(),
                AccountNumber: viewModel.TransactionDealDetailModel().IsOtherAccountNumber() ? null : viewModel.TransactionDealDetailModel().Account().AccountNumber,
                IsJointAccount: viewModel.TransactionDealDetailModel().IsJointAccount(),
                StatementLetterID: viewModel.TransactionDealDetailModel().StatementLetter().ID,
                TZReference: viewModel.TransactionDealDetailModel().TZReference,

            }
            var options = {
                url: api.server + api.url.helper + "/GetTotalTransactionIDRFCY",
                token: accessToken,
                params: {},
                data: ko.toJSON(paramhreshold)
            };
            Helper.Ajax.Post(options, function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    TotalDealModel.Total.IDRFCYDeal = data.TotalTransaction;
                    self.AmountModel().TotalAmountsUSD(data.TotalTransaction);
                    TotalDealModel.Total.UtilizationDeal = data.TotalUtilizationAmount;
                    self.AmountModel().TotalUtilization(data.TotalUtilizationAmount);
                    TotalDealModel.Total.RemainingBalance = data.AvailableUnderlyingAmount;
                    self.AmountModel().RemainingBalance(data.AvailableUnderlyingAmount);
                    self.ParameterRounding(data.RoundingValue);
                    self.ThresholdType(data.ThresholdType);
                    self.ThresholdValue(data.ThresholdValue);
                    self.IsNoThresholdValue(data.ThresholdValue == 0 ? true : false);
                    //SetHitThreshold(parseFloat(viewModel.TransactionDealDetailModel().AmountUSD()));
                    SetCalculateFX(paramhreshold.AmountUSD);
                    if (self.IsNoThresholdValue() && data.ThresholdType != null) {
                        self.Selected().StatementLetter(CONST_STATEMENT.Dash_ID);
                    }
                }
            }, OnError);
        }
    }
    function SetCalculateFX(amountUSD) {
        self.TransactionDealDetailModel().AmountUSD(amountUSD);
        if (viewModel.TransactionDealDetailModel().StatementLetter() != null && viewModel.TransactionDealDetailModel().StatementLetter().ID == 4) {
            self.AmountModel().RemainingBalance(0.00);
            self.AmountModel().TotalAmountsUSD(TotalDealModel.Total.IDRFCYDeal + parseFloat(amountUSD));
        } else if (viewModel.TransactionDealDetailModel().StatementLetter() != null && viewModel.TransactionDealDetailModel().StatementLetter().ID != 4) {
            var balanceAmount = parseFloat(TotalDealModel.Total.RemainingBalance) - parseFloat(amountUSD);
            self.AmountModel().RemainingBalance(parseFloat(balanceAmount).toFixed(2));
            self.AmountModel().TotalAmountsUSD(TotalDealModel.Total.IDRFCYDeal + parseFloat(amountUSD));
        }
        //SetHitThreshold(amountUSD);
    }
    function SetHitThreshold(amountUSD) {
        if (self.t_IsFxTransactionToIDR()) {

            self.IsHitThreshold(false);
            if (self.ThresholdValue() != null && self.ThresholdValue() != 0) {
                if (self.ThresholdType() == 'T') {
                    if (amountUSD > self.ThresholdValue()) {
                        self.IsHitThreshold(true);
                        viewModel.Selected().StatementLetter(CONST_STATEMENT.StatementB_ID);
                    } else {
                        viewModel.Selected().StatementLetter(CONST_STATEMENT.Dash_ID);
                        self.AmountModel().TotalAmountsUSD(0);
                    }
                } else {
                    if (TotalDealModel.Total.IDRFCYDeal > self.ThresholdValue()) {
                        self.IsHitThreshold(true);
                        viewModel.Selected().StatementLetter(CONST_STATEMENT.StatementB_ID);
                    } else {
                        viewModel.Selected().StatementLetter(CONST_STATEMENT.Dash_ID);
                        self.AmountModel().TotalAmountsUSD(0);
                    }
                }
            }

        }

    }
    function GetAnnualStatement(cif, isSubmit) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckAnnualStatement/" + cif,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }, success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if (data != null) {
                        if (data) {
                            if (self.IsDeal()) {
                                SaveTransaction();
                                return;
                            }
                            if (isSubmit) {
                                if (self.TempSelectedUnderlying() == null || (self.TempSelectedUnderlying() != null && self.TempSelectedUnderlying().length == 0)) {
                                    ShowNotification("Form Underlying Warning", "Please select utilize underlying data.", 'gritter-warning', false);
                                    viewModel.IsEditable(true);
                                    viewModel.IsEnableSubmit(true);
                                    return false;
                                }
                                UploadDocuments();
                            } else {
                                viewModel.TransactionDealDetailModel().utilizationAmount(0.00);
                                viewModel.TempSelectedUnderlying([]);
                                viewModel.GetDataUnderlying();
                                viewModel.GetDataAttachFile();
                                viewModel.IsDeal(false);
                                $('#book-transaction').hide();
                                $('#new-transaction').show();
                                viewModel.IsEditable(true);
                                $('#s4-workspace').animate({ scrollTop: 0 }, 'fast');
                            }
                        } else {
                            ShowNotification("Form Validation Warning", CONST_MSG.ValidationAnnualSL, 'gritter-warning', true);
                            viewModel.IsEditable(true);
                            viewModel.IsEnableSubmit(true);
                            return;
                        }
                    } else {
                        ShowNotification("Form Validation Warning", "Something went worng with function get annual statment", 'gritter-warning', true);
                        viewModel.IsEditable(true);
                        viewModel.IsEnableSubmit(true);
                        return;
                    }
                }
            }
        });

    }
    //#endregion

    function GetSelectedUtilizeID() {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });

        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SelectedUtilizeID",
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON(filters),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlying([]);
                    for (var i = 0; i < data.length; i++) {
                        self.TempSelectedUnderlying.push(new SelectUtillizeModel(data[i].ID, true, 0));
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetSelectedProformaID(callback) {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.ProformaFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.ProformaFilterIsSelected() });
        if (self.ProformaFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.ProformaFilterUnderlyingDocument() });
        if (self.ProformaFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.ProformaFilterDocumentType() });


        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SelectedProformaID",
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON(filters),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlyingProforma([]);
                    for (var i = 0; i < data.length; i++) {
                        self.TempSelectedUnderlyingProforma.push({ ID: data[i].ID });
                    }
                    if (callback != null) {
                        callback();
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    // Function to get All selected Bulk Underlying // add 2015.03.09
    function GetSelectedBulkID(callback) {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: self.CIF_u() });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.BulkFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.BulkFilterIsSelected() });
        if (self.BulkFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.BulkFilterUnderlyingDocument() });
        if (self.BulkFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.BulkFilterDocumentType() });


        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SelectedBulkID",
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON(filters),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlyingBulk([]);
                    for (var i = 0; i < data.length; i++) {
                        self.TempSelectedUnderlyingBulk.push({ ID: data[i].ID, Amount: data[i].Amount });
                    }
                    if (callback != null) {
                        callback();
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    // Get filtered columns value
    function GetUnderlyingFilteredColumns() {
        // define filter

        var filters = [];
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingFilterIsSelected() });
        if (self.UnderlyingFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingFilterUnderlyingDocument() });
        if (self.UnderlyingFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingFilterDocumentType() });
        if (self.UnderlyingFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingFilterCurrency() });
        if (self.UnderlyingFilterAvailableAmount() != "") filters.push({ Field: 'AvailableAmount', Value: self.UnderlyingFilterAvailableAmount() });
        if (self.UnderlyingFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingFilterStatementLetter() });
        if (self.UnderlyingFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingFilterAmount() });
        if (self.UnderlyingFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingFilterDateOfUnderlying() });
        if (self.UnderlyingFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingFilterExpiredDate() });
        if (self.UnderlyingFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingFilterReferenceNumber() });
        if (self.UnderlyingFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingFilterSupplierName() });
        if (self.UnderlyingFilterAttachmentNo() != "") filters.push({ Field: 'AttachmentNo', Value: self.UnderlyingFilterAttachmentNo() });
        if (self.UnderlyingFilterIsAvailable() != "") filters.push({ Field: 'IsAvailable', Value: self.UnderlyingFilterIsAvailable() });
        if (self.UnderlyingFilterIsExpiredDate() != "") filters.push({ Field: 'IsExpiredDate', Value: self.UnderlyingFilterIsExpiredDate() });
        if (self.UnderlyingFilterIsSelectedBulk() == false) filters.push({ Field: 'IsSelectedBulk', Value: self.UnderlyingFilterIsSelectedBulk() });
        if (self.UnderlyingFilterAccountNumber() != "") filters.push({ Field: 'AccountNumber', Value: self.UnderlyingFilterAccountNumber() });

        filters.push({ Field: 'StatusShowData', Value: self.UnderlyingFilterShow() });
        return filters;
    };

    // Get filtered columns value
    function GetUnderlyingAttachFilteredColumns() {
        var filters = [];
        self.UnderlyingFilterIsSelected(true); // flag for get all datas
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingAttachFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingAttachFilterIsSelected() });
        if (self.UnderlyingAttachFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingAttachFilterUnderlyingDocument() });
        if (self.UnderlyingAttachFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingAttachFilterDocumentType() });
        if (self.UnderlyingAttachFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingAttachFilterCurrency() });
        if (self.UnderlyingAttachFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingAttachFilterStatementLetter() });
        if (self.UnderlyingAttachFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingAttachFilterAmount() });
        if (self.UnderlyingAttachFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingAttachFilterDateOfUnderlying() });
        if (self.UnderlyingAttachFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingAttachFilterExpiredDate() });
        if (self.UnderlyingAttachFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingAttachFilterReferenceNumber() });
        if (self.UnderlyingAttachFilterAvailableAmount() != "") filters.push({ Field: 'AvailableAmount', Value: self.UnderlyingAttachFilterAvailableAmount() });
        if (self.UnderlyingAttachFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingAttachFilterSupplierName() });
        if (self.UnderlyingAttachFilterAccountNumber() != "") filters.push({ Field: 'AccountNumber', Value: self.UnderlyingAttachFilterAccountNumber() });

        filters.push({ Field: 'StatusShowData', Value: self.UnderlyingFilterShow() });
        return filters;

    }

    // Get filtered columns value
    function GetAttachFilteredColumns() {
        // define filter

        var filters = [];
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.AttachFilterFileName() != "") filters.push({ Field: 'FileName', Value: self.AttachFilterFileName() });
        if (self.AttachFilterDocumentPurpose() != "") filters.push({ Field: 'DocumentPurpose', Value: self.AttachFilterDocumentPurpose() });
        if (self.AttachFilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.AttachFilterModifiedDate() });
        if (self.AttachFilterDocumentRefNumber() != "") filters.push({ Field: 'DocumentRefNumber', Value: self.AttachFilterDocumentRefNumber() });
        if (self.AttachFilterAttachmentReference() != "") filters.push({ Field: 'AttachmentReference', Value: self.AttachFilterAttachmentReference() });
        if (self.AttachFilterAccountNumber() != "") filters.push({ Field: 'AccountNumber', Value: self.AttachFilterAccountNumber() });

        filters.push({ Field: 'StatusShowData', Value: self.UnderlyingFilterShow() });
        return filters;
    };

    // Get filtered columns value
    function GetUnderlyingProformaFilteredColumns() {
        // define filter
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.ProformaFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.ProformaFilterIsSelected() });
        if (self.ProformaFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.ProformaFilterUnderlyingDocument() });
        if (self.ProformaFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.ProformaFilterDocumentType() });
        if (self.ProformaFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.ProformaFilterCurrency() });
        if (self.ProformaFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.ProformaFilterStatementLetter() });
        if (self.ProformaFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.ProformaFilterAmount() });
        if (self.ProformaFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.ProformaFilterDateOfUnderlying() });
        if (self.ProformaFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.ProformaFilterExpiredDate() });
        if (self.ProformaFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.ProformaFilterReferenceNumber() });
        if (self.ProformaFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.ProformaFilterSupplierName() });

        return filters;
    };

    // Get filtered columns value // add 2015.03.09
    function GetBulkUnderlyingFilteredColumns() {
        // define filter
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.BulkFilterCurrency(self.Currency_u().Code() != "" && self.Currency_u().Code() != null ? self.Currency_u().Code() : "xyz");
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: self.CIF_u() });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.BulkFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.BulkFilterIsSelected() });
        if (self.BulkFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.BulkFilterUnderlyingDocument() });
        if (self.BulkFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.BulkFilterDocumentType() });
        if (self.BulkFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.BulkFilterCurrency() });
        if (self.BulkFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.BulkFilterStatementLetter() });
        if (self.BulkFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.BulkFilterAmount() });
        if (self.BulkFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.BulkFilterDateOfUnderlying() });
        if (self.BulkFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.BulkFilterExpiredDate() });
        if (self.BulkFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.BulkFilterReferenceNumber() });
        if (self.BulkFilterInvoiceNumber() != "") filters.push({ Field: 'InvoiceNumber', Value: self.BulkFilterInvoiceNumber() });
        if (self.BulkFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.BulkFilterSupplierName() });

        return filters;
    };

    // On success GetData callback
    function OnSuccessGetDataUnderlying(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            var dataunderlying = SetSelectedUnderlying(data.Rows);
            self.CustomerUnderlyings([]);
            self.CustomerUnderlyings(dataunderlying);
            self.UnderlyingGridProperties().Page(data['Page']);
            self.UnderlyingGridProperties().Size(data['Size']);
            self.UnderlyingGridProperties().Total(data['Total']);
            self.UnderlyingGridProperties().TotalPages(Math.ceil(self.UnderlyingGridProperties().Total() / self.UnderlyingGridProperties().Size()));
            ReSetTotalUnderlying();
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetUnderlyingDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {

            for (var i = 0 ; i < data.Rows.length; i++) {
                var selected = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (item) {
                    return item.ID == data.Rows[i].ID;
                });
                if (selected != null) {
                    data.Rows[i].IsSelectedAttach = true;
                }
                else {
                    data.Rows[i].IsSelectedAttach = false;
                }
            }
            self.CustomerAttachUnderlyings(data.Rows);

            self.UnderlyingAttachGridProperties().Page(data['Page']);
            self.UnderlyingAttachGridProperties().Size(data['Size']);
            self.UnderlyingAttachGridProperties().Total(data['Total']);
            self.UnderlyingAttachGridProperties().TotalPages(Math.ceil(self.UnderlyingAttachGridProperties().Total() / self.UnderlyingAttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerUnderlyingFiles(data.Rows);

            self.AttachGridProperties().Page(data['Page']);
            self.AttachGridProperties().Size(data['Size']);
            self.AttachGridProperties().Total(data['Total']);
            self.AttachGridProperties().TotalPages(Math.ceil(self.AttachGridProperties().Total() / self.AttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback // Add 2015.03.09
    function OnSuccessGetUnderlyingBulk(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            SetSelectedBulk(data.Rows);

            self.CustomerBulkUnderlyings(data.Rows);

            self.UnderlyingBulkGridProperties().Page(data['Page']);
            self.UnderlyingBulkGridProperties().Size(data['Size']);
            self.UnderlyingBulkGridProperties().Total(data['Total']);
            self.UnderlyingBulkGridProperties().TotalPages(Math.ceil(self.UnderlyingBulkGridProperties().Total() / self.UnderlyingBulkGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetUnderlyingProforma(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            SetSelectedProforma(data.Rows);

            self.CustomerUnderlyingProformas(data.Rows);

            self.UnderlyingProformaGridProperties().Page(data['Page']);
            self.UnderlyingProformaGridProperties().Size(data['Size']);
            self.UnderlyingProformaGridProperties().Total(data['Total']);
            self.UnderlyingProformaGridProperties().TotalPages(Math.ceil(self.UnderlyingProformaGridProperties().Total() / self.UnderlyingProformaGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }
    function ReSetTotalUnderlying() {
        self.TempSelectedUnderlying([]);
        ko.utils.arrayForEach(self.CustomerUnderlyings(), function (underlyingItem) {
            if (underlyingItem.IsEnable == true) {
                self.TempSelectedUnderlying.push(new SelectUtillizeModel(underlyingItem.ID, true, underlyingItem.AvailableAmount, underlyingItem.StatementLetter.ID))
            }
        });
        setTotalUtilize();
    }
    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
    //--------------------------- set Underlying Function & variable end
};

// Knockout View Model
var viewModel = new ViewModel();
function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

$.holdReady(true);
var intervalRoleName = setInterval(function () {
    if (Object.keys(Const_RoleName).length != 0) {
        $.holdReady(false);
        clearInterval(intervalRoleName);
    }
}, 100);

// jQuery Init
//$(document).on("RoleNameReady", function() {
$(document).ready(function () {
    //force input keyboard in numeric   
    $.fn.ForceNumericOnly = function () {

        return this.each(function () {

            $(this).keydown(function (e) {

                var key = e.charCode || e.keyCode || 0;

                return (

    				key == 8 ||
    				key == 9 ||
    				key == 13 ||
    				key == 46 ||
    				key == 110 ||
    				key == 190 ||
    				(key >= 35 && key <= 40) ||
    				(key >= 48 && key <= 57) ||
    				(key >= 96 && key <= 105));

            });

        });

    };
    $(".input-numericonly").ForceNumericOnly();
    //$('#modal-form-Underlying')

    // show book transaction, hide new transaction

    // ace file upload
    $('#document-path').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        thumbnail: false, //| true | large
        blacklist: 'exe|dll'
    });

    $('#document-path-upload').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });

    // Knockout custom file handler
    ko.bindingHandlers.file = {
        init: function (element, valueAccessor) {
            $(element).change(function () {
                var file = this.files[0];

                if (ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };


    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        focusCleanup: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });

    $('#aspnetForm').after('<form id="frmUnderlying"></form>');
    $("#modal-form-Underlying").appendTo("#frmUnderlying");
    $("#modal-form-Attach").appendTo("#frmUnderlying");

    $('#frmUnderlying').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        focusCleanup: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    var element = $("#book-customer-name");
    GetCustomerAutocompleted(element);

    // datepicker
    $('.date-picker').datepicker({
        autoclose: true,
        onSelect: function () {
            this.focus();
        }
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });


    // Knockout Datepicker custom binding handler
    ko.bindingHandlers.datepicker = {
        init: function (element) {
            $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).datepicker("refresh");
        }
    };

    // Dependant Observable for dropdown auto fill
    ko.dependentObservable(function () {
        var account = ko.utils.arrayFirst(viewModel.TransactionDealDetailModel().Customer().Accounts(), function (item) { return item.AccountNumber == viewModel.Selected().Account(); });

        if (viewModel.Selected().Account() == '-') {
            viewModel.IsEmptyAccountNumber(true);
        }
        else {
            if (account != null) {
                viewModel.TransactionDealDetailModel().DebitCurrency(account.Currency);
            }
        }

        var currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().Currency(); });
        var debitcurrency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().DebitCurrency(); });
        var docPurpose = ko.utils.arrayFirst(viewModel.Parameter().DocumentPurposes(), function (item) { return item.ID == viewModel.Selected().DocumentPurpose(); });
        var rateType = ko.utils.arrayFirst(viewModel.Parameter().RateType(), function (item) { return item.ID == viewModel.Selected().RateType(); });
        var statementLetter = ko.utils.arrayFirst(viewModel.Parameter().StatementLetter(), function (item) { return item.ID == viewModel.Selected().StatementLetter(); });
        var productType = ko.utils.arrayFirst(viewModel.Parameter().ProductType(), function (item) { return item.ID == viewModel.Selected().ProductType(); });
        var docType = ko.utils.arrayFirst(viewModel.Parameter().DocumentTypes(), function (item) { return item.ID == viewModel.Selected().DocumentType(); });

        if (viewModel.DocumentPurpose_a() != null) {
            if (viewModel.DocumentPurpose_a().ID() == 2) {
                viewModel.SelectingUnderlying(true);
            }
            else {
                viewModel.SelectingUnderlying(false);
            }
        }

        if (debitcurrency != null) {
            viewModel.TransactionDealDetailModel().DebitCurrency(debitcurrency);
            // viewModel.GetTotalTransaction();
        }

        if (productType != null) {
            viewModel.TransactionDealDetailModel().ProductType(productType);
        }
        // if currency is selected
        if (currency != null) {

            if (debitcurrency != null) {
                viewModel.TransactionDealDetailModel().DebitCurrency(debitcurrency);
            }
            viewModel.TransactionDealDetailModel().Currency(currency);

            if (currency.ID != viewModel.beforeValueCurrID()) {
                GetRateAmount(currency.ID);
                viewModel.beforeValueCurrID(currency.ID);
            }

            // set value of transaction 'IS FX TRANSACTION'
            var t_IsFxTransaction = (viewModel.TransactionDealDetailModel().Currency().Code != 'IDR' && viewModel.TransactionDealDetailModel().DebitCurrency().Code == 'IDR') ? true : false;
            var t_IsFxTransactionToIDR = (viewModel.TransactionDealDetailModel().Currency().Code == 'IDR' && viewModel.TransactionDealDetailModel().DebitCurrency().Code != 'IDR') ? true : false;

            viewModel.t_IsFxTransaction(t_IsFxTransaction);
            viewModel.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);
            //SetProductType();
        }



        if (account != null) {
            viewModel.TransactionDealDetailModel().DebitCurrency(account.Currency);
            viewModel.TransactionDealDetailModel().Account(account);
            // set value of transaction 'IS FX TRANSACTION'
            var t_IsFxTransaction = (viewModel.TransactionDealDetailModel().Currency().Code != 'IDR' && viewModel.TransactionDealDetailModel().DebitCurrency().Code == 'IDR') ? true : false;
            var t_IsFxTransactionToIDR = (viewModel.TransactionDealDetailModel().Currency().Code == 'IDR' && viewModel.TransactionDealDetailModel().DebitCurrency().Code != 'IDR') ? true : false;

            viewModel.t_IsFxTransaction(t_IsFxTransaction);
            viewModel.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);
            calculate();
            //SetProductType();
        }
        if (docType != null) viewModel.DocumentType(docType);
        if (docPurpose != null) viewModel.DocumentPurpose(docPurpose);
        if (rateType != null) viewModel.TransactionDealDetailModel().RateType(rateType);
        if (statementLetter != null) {
            viewModel.TransactionDealDetailModel().StatementLetter(statementLetter);

            if (viewModel.TransactionDealDetailModel().StatementLetter() != undefined) {
                if (viewModel.TransactionDealDetailModel().StatementLetter().ID == 1) {
                    viewModel.TransactionDealDetailModel().bookunderlyingcode(36);
                }
                else {
                    //viewModel.TransactionDealDetailModel().bookunderlyingcode('');
                }
            }

            calculate();
        }

    });

    // Token Validation
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(OnSuccessToken, OnError);
    } else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);


            viewModel.GetApplicationID();
            viewModel.SPUser(ko.mapping.toJS(spUser));
            viewModel.isDealBUGroup(true);
            viewModel.IsPermitedButtonBookDeal(); //set Permission
            $.ajax({
                type: "GET",
                url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + $.cookie(api.cookie.name)
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        //("spUser Roles : " + ko.toJSON(spUser.Roles));
                        // console.log("spUser Permited : " + data);
                        //compare user role to page permission to get checker validation
                        viewModel.isDealBUGroup(false);
                        for (i = 0; i < spUser.Roles.length; i++) {
                            for (j = 0; j < data.length; j++) {
                                if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {

                                    if (Const_RoleName[spUser.Roles[i].ID].toLowerCase().startsWith("dbs fx") && Const_RoleName[spUser.Roles[i].ID].toLowerCase().endsWith("maker")) { // develop//if (spUser.Roles[i].Name.toLowerCase().startsWith("dbs fx") && spUser.Roles[i].Name.toLowerCase().endsWith("maker")) { // develop
                                        viewModel.isDealBUGroup(true);
                                        // return;
                                    }
                                    /*else
                                    {
                                        viewModel.isDealBUGroup(true);
                                    } */
                                }
                            }
                        }
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }
        // call get data inside view model
        //DealModel.token = accessToken;
        //GetParameterData(DealModel, OnSuccessGetTotal, OnErrorDeal);
        DealModel.token = accessToken;
        GetThresholdParameter(DealModel, OnSuccessThresholdPrm, OnErrorDeal);
        GetParameters();
    }

    // Knockout Bindings
    ko.applyBindings(viewModel);

    //================================= Eqv Calculation ====================================

    var x = document.getElementById("book-amount");
    var y = document.getElementById("book-rate");
    var z = document.getElementById("book-currency");
    var d = document.getElementById("book-eqv-usd");
    var xstored = x.getAttribute("data-in");
    var ystored = y.getAttribute("data-in");

    setInterval(function () {
        if (x == document.activeElement) {
            var temp = x.value;
            if (xstored != temp) {
                xstored = temp;
                x.setAttribute("data-in", temp);
                calculate();
            }
        }
        if (y == document.activeElement) {
            var temp = y.value;
            if (ystored != temp) {
                ystored = temp;
                y.setAttribute("data-in", temp);
                calculate();
            }
        }
        /* dodit@2014.11.08:add posibility calculate if currency changed */
        if (z == document.activeElement) { // add by dodit 2014/11/8
            calculate();
        }
    }, 100);

    function calculate() {
        /* dodit@2014.11.08:altering calculate process to avoid NaN, avoid formula on USD, and formating feature */

        x.value = x.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        y.value = y.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        //y.value = y.value==null || y.value==''?0: y.value;
        viewModel.TransactionDealDetailModel().Amount(x.value);
        viewModel.TransactionDealDetailModel().Rate(y.value);
        res = x.value;
        ret = y.value;

        x.value = formatNumber(x.value);
        y.value = formatNumber(y.value);

        currency = viewModel.Selected().Currency();

        if (currency != 1) { // if not USD
            res = res * ret / idrrate;
            res = Math.round(res * 100) / 100;
        }

        res = isNaN(res) ? 0 : res; //avoid NaN
        res = res != null && res != "" ? res : 0;
        viewModel.SetCalculateFX(res);
        //viewModel.CalculateFX(res);
    }

    x.onblur = calculate;
    calculate();

    $('#book-transaction').show();
    $('#new-transaction').hide();

    //  $('#book-underlying-currency').attr('data-rule-required', false);
    //	$('#book-underlying-amount').attr('data-rule-required', false);
    $('#book-underlying-code').attr('data-rule-required', false);

    //		$('#book-underlying-currency').rules('remove', 'required');
    //		$('#book-underlying-amount').rules('remove', 'required');
    $('#book-underlying-code').rules('remove', 'required');

});

function GetRateAmount(CurrencyID) {
    var options = {
        url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
        params: {
        },
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetRateAmount, OnError, OnAlways);
}
function SetProductType() {
    if (viewModel.t_IsFxTransactionToIDR()) {
        var productType = ko.utils.arrayFirst(viewModel.Parameter().ProductType(), function (item) { return item.ID == 0; }); // ID 0 = null product type
        if (productType != null) {
            viewModel.TransactionDealDetailModel().ProductType(productType);
        }
    }
}
function AddOtherAccounts(accounts) {
    if (accounts != null) {
        var accountData = {
            AccountNumber: "-",
            CustomerName: null,
            IsJointAccount: false,
            Currency: null
        };
        accounts.push(accountData);
    }
    return accounts;
}
function OnSuccessGetRateAmount(data, textStatus, jqXHR) {

    if (jqXHR.status = 200) {
        // bind result to observable array
        viewModel.TransactionDealDetailModel().Rate(data.RupiahRate);
        viewModel.Rate(data.RupiahRate);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function read_u() {
    var e = document.getElementById("Amount_u").value;
    e = e.toString().replace(/,/g, '');
    var res = parseInt(e) * parseFloat(viewModel.Rate_u()) / parseFloat(idrrate);
    res = Math.round(res * 100) / 100;
    res = isNaN(res) ? 0 : res; //avoid NaN
    viewModel.AmountUSD_u(parseFloat(res).toFixed(2));
    viewModel.Amount_u(formatNumber(e));
}
function BookTransactioin() {
    // hide book transaction, show new transaction

    viewModel.Documents([]);

    $('#book-transaction').show();
    $('#new-transaction').hide();

    $('#s4-workspace').animate({ scrollTop: 0 }, 'fast');
}
function GetCustomerAutocompleted(element) {
    // autocomplete
    element.autocomplete({
        source: function (request, response) {
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.customer + "/Search",
                data: {
                    query: request.term,
                    limit: 20
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
        },
        minLength: 2,
        select: function (event, ui) {
            // set customer data model
            if (ui.item.data != undefined || ui.item.data != null) {

                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                viewModel.TransactionDealDetailModel().Customer(ko.mapping.fromJS(ui.item.data, mapping));
                viewModel.TransactionDealDetailModel().IsResident(ui.item.data.IsResident);
                viewModel.TransactionDealDetailModel().IsJointAccount(false);
                viewModel.IsJointAccount(true); // set default value
                /*  var dataAccounts = ko.utils.arrayFilter(viewModel.TransactionDealDetailModel().Customer().Accounts, function (dta) {
                      var sAccount = dta.IsJointAccount == null ? false : dta.IsJointAccount
                      return sAccount == false
                  });
                var isJointAcc = ko.utils.arrayFilter(viewModel.TransactionDealDetailModel().Customer().Accounts, function (dta) {                   
                      return dta.IsJointAccount == true
                });
                  viewModel.DynamicAccounts([]);
                  if (dataAccounts != null && dataAccounts.length > 0) {                  
                      dataAccounts = AddOtherAccounts(dataAccounts);
                  } else {
                      dataAccounts = AddOtherAccounts(dataAccounts);
                  }
                  viewModel.DynamicAccounts(dataAccounts);
                  if (isJointAcc != null && isJointAcc.length == 0) {
                      viewModel.IsJointAccount(false);
                  } */


                cifData = ui.item.data.CIF;
                DealModel.cif = cifData;
                customerNameData = ui.item.data.Name;
                //GetTotalDeal(DealModel, OnSuccessTotal, OnErrorDeal);
                viewModel.SetDynamicAccount();
                //viewModel.CalculateFX(0);               
            }
            else
                viewModel.TransactionDealDetailModel().Customer(null);
        }
    });
}
function OnSuccessTotal() {
    var amountUSD = viewModel.TransactionDealDetailModel().AmountUSD() != null ? parseFloat(viewModel.TransactionDealDetailModel().AmountUSD()) : 0;
    viewModel.CalculateFX(amountUSD);
}
function OnErrorDeal(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}


// reset model
function ResetModel() {
    viewModel = new ViewModel();
}

// get all parameters need
function GetParameters() {
    var options = {
        url: api.server + api.url.parameter,
        params: {
            select: "Currency,AgentCharge,FXCompliance,ChargesType,DocType,PurposeDoc,statementletter,underlyingdoc,customer,ratetype,producttype"
        },
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
}

function SaveTransaction() {
    if (viewModel.TransactionDealDetailModel().Documents().length == viewModel.Documents().length) {
        viewModel.TransactionDealDetailModel().IsBookDeal(viewModel.IsDeal());
        viewModel.TransactionDealDetailModel().IsFxTransaction(true);
        if (viewModel.TransactionDealDetailModel().Underlyings == null) {
            viewModel.TransactionDealDetailModel().Underlyings = ko.observableArray([]);
        } else {
            viewModel.TransactionDealDetailModel().Underlyings([]);
        }

        ko.utils.arrayForEach(viewModel.TempSelectedUnderlying(), function (item) {
            viewModel.TransactionDealDetailModel().Underlyings().push(new SelectUtillizeModel(item.ID(), false, item.USDAmount(), ''));
        });
        var options = {
            url: api.server + api.url.transactiondeal,
            token: accessToken,
            data: ko.toJSON(viewModel.TransactionDealDetailModel())
        };
        Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
    }
}


function AddListItem() {
    var body = {
        Title: viewModel.TransactionDealDetailModel().ApplicationID(),
        __metadata: {
            type: config.sharepoint.metadata.listDeal
        }
    };

    //console.log('body: ' + body);

    var options = {
        url: config.sharepoint.url.api + "/Lists(guid'" + config.sharepoint.listIdDeal + "')/Items",
        data: JSON.stringify(body),
        digest: jQuery("#__REQUESTDIGEST").val()
    };

    //console.log('option: ' + options);

    Helper.Sharepoint.List.Add(options, OnSuccessAddListItem, OnError, OnAlways);
}

function ValidateTransaction() {
    UploadDocuments();
}

function UploadDocuments() {
    // uploading documents. after upload completed, see SaveTransaction()
    var data = {
        ApplicationID: viewModel.TransactionDealDetailModel().ApplicationID(),
        CIF: viewModel.TransactionDealDetailModel().Customer().CIF,
        Name: viewModel.TransactionDealDetailModel().Customer().Name
    };

    //if (viewModel.Documents().length > 0 && !(viewModel.TransactionDealDetailModel().Currency().Code != 'IDR' && viewModel.TransactionDealDetailModel().DebitCurrency().Code == 'IDR')) {
    if (viewModel.Documents().length > 0) {
        for (var i = 0; i < viewModel.Documents().length; i++) {
            UploadFile(data, viewModel.Documents()[i], SaveTransaction);
        }
    }
    else if (viewModel.IsFXTransactionAttach() && !(viewModel.IsUploaded())) {
        if (viewModel.Documents().length > 0) {
            for (var i = 0; i < viewModel.Documents().length; i++) {
                UploadFile(data, viewModel.Documents()[i], SaveTransaction);
            }
        } else {
            SaveTransaction();
        }
    }
    else {
        SaveTransaction();
    }
}

// Event handlers declaration start
function OnSuccessUpload(data, textStatus, jqXHR) {
    ShowNotification("Upload Success", JSON.stringify(data), "gritter-success", true);
}

function OnSuccessGetParameters(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        // bind result to observable array
        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };

        viewModel.Parameter().Currencies(ko.mapping.toJS(data.Currency, mapping));
        viewModel.Parameter().ProductType(ko.mapping.toJS(data.ProductType, mapping));
        viewModel.Parameter().FXCompliances(ko.mapping.toJS(data.FXCompliance, mapping));
        viewModel.Parameter().DocumentTypes(ko.mapping.toJS(data.DocType, mapping));
        viewModel.Parameter().DocumentPurposes(ko.mapping.toJS(data.PurposeDoc, mapping));
        viewModel.Parameter().RateType(ko.mapping.toJS(data.RateType, mapping));
        viewModel.Parameter().StatementLetter(ko.mapping.toJS(data.StatementLetter, mapping));

        // underlying parameter
        viewModel.GetUnderlyingParameters(data);

        // enabling form input controls
        viewModel.IsEditable(true);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function OnSuccessUpdateUnderlying(data, textStatus, jqXHR) {
    if (jqXHR.status != 200) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-success', true);
    }
}

function OnSuccessAddListItem(data, textStatus, jqXHR) {
    ShowNotification("Book Success", JSON.stringify(data.Message), "gritter-success", true);
    viewModel.IsEditable(false);
    viewModel.IsEnableSubmit(true);
    window.location = "/home";
}

function OnSuccessSaveAPI(data, textStatus, jqXHR) {
    // Get transaction id from api result

    if (data.ID != null || data.ID != undefined) {
        viewModel.TransactionDealDetailModel().ID(data.ID);
        viewModel.TransactionDealDetailModel().bookunderlyingamount("");

        if (viewModel.IsDeal() == false) {
            // insert to sharepoint list item
            AddListItem();
        }
        else {
            //viewModel.isEditable(false);
            ShowNotification("Book Success", JSON.stringify(data.Message), "gritter-success", true);
            window.location = "/home";
        }
    }
}

// Autocomplete
function OnSuccessAutoComplete(response, data, textStatus, jqXHR) {

    response($.map(data, function (item) {

        return {
            // default autocomplete object
            label: item.CIF + " - " + item.Name,
            value: item.Name,

            // custom object binding
            data: {
                CIF: item.CIF,
                Name: item.Name,
                POAName: item.POAName,
                Accounts: item.Accounts,
                IsResident: item.IsResident,
                Branch: item.Branch,
                Contacts: item.Contacts,
                Functions: item.Functions,
                RM: item.RM,
                Type: item.Type,
                Underlyings: item.Underlyings,
                LastModifiedBy: item.LastModifiedBy,
                LastModifiedDate: item.LastModifiedDate
            }

        }

    })
    );



}

// Token validation On Success function
function OnSuccessToken(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // read spuser from cookie
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);
        viewModel.SPUser(ko.mapping.toJS(spUser));
    }
    // get parameter values
    //DealModel.token = accessToken;
    //GetParameterData(DealModel, OnSuccessGetTotal, OnErrorDeal);
    DealModel.token = accessToken;
    GetThresholdParameter(DealModel, OnSuccessThresholdPrm, OnErrorDeal);
    GetParameters();

}

// delete & upload document underlying start
function DeleteFile(documentPath) {
    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Delete the file to the SharePoint folder.
    var deleteFile = deleteFileToFolder();
    deleteFile.done(function (file, status, xhr) {
        output = file.d;
    });
    deleteFile.fail(OnError);
    // Call function delete File Shrepoint Folder
    function deleteFileToFolder() {
        return $.ajax({
            url: serverUrl + "/_api/web/GetFileByServerRelativeUrl('" + documentPath + "')",
            type: "POST",
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "X-HTTP-Method": "DELETE",
                "IF-MATCH": "*"
                //"content-length": arrayBuffer.byteLength
            }
        });
    }
}

function UploadFileUnderlying(context, document, callBack) {

    // Define the folder path for this example.
    var serverRelativeUrlToFolder = '/Underlying Documents';

    // Get the file name from the file input control on the page.
    var parts = document.DocumentPath().name.split('.');

    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBufferUnderlying();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolderUnderlying(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItemUnderlying(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                // Change the display name and title of the list item.
                var changeItem = updateListItemUnderlying(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    DocumentModels({ "FileName": document.DocumentPath().name, "DocumentPath": output.ServerRelativeUrl });
                    //viewModel.TransactionDealDetailModel().Documents.push(kodok);

                    //self.Documents.push(kodok);
                    callBack();
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBufferUnderlying() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }

        //reader.readAsArrayBuffer(fileInput[0].files[0]);
        reader.readAsArrayBuffer(document.DocumentPath());
        //reader.readAsDataURL(document.DocumentPath());
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolderUnderlying(arrayBuffer) {

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                //"content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItemUnderlying(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItemUnderlying(itemMetadata) {

        // Define the list item changes. Use the FileLeafRef property to change the display name.
        // For simplicity, also use the name as the title.
        // The example gets the list item type from the item's metadata, but you can also get it from the
        // ListItemEntityTypeFullName property of the list.
        var body = {
            Title: document.DocumentPath().name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: context.Type.DocTypeName,
            Document_x0020_Purpose: context.Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}

// Upload the file
function UploadFile(context, document, callBack) {

    var serverRelativeUrlToFolder = '/Instruction Documents';
    var parts = document.DocumentPath.name.split('.');
    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp + "." + fileExtension;

    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Get the local file as an array buffer.
    var getFile = getFileBuffer();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolder(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                // Change the display name and title of the list item.
                var changeItem = updateListItem(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {

                    //return output;
                    var kodok = {
                        ID: 0,
                        Type: document.Type,
                        Purpose: document.Purpose,
                        LastModifiedDate: null,
                        LastModifiedBy: null,
                        DocumentPath: output.ServerRelativeUrl,
                        FileName: document.DocumentPath.name
                    };
                    viewModel.TransactionDealDetailModel().Documents.push(kodok);

                    callBack();
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBuffer() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }

        reader.readAsArrayBuffer(document.DocumentPath);
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(arrayBuffer) {

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItem(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItem(itemMetadata) {
        var body = {
            Title: document.DocumentPath.name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: document.Type.Name,
            Document_x0020_Purpose: document.Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}

// AJAX On Error Callback
function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}
function OnSuccessThresholdPrm() {
    idrrate = DataModel.RateIDR;
}
function OnSuccessGetTotal(dataCall) {
    idrrate = dataCall.RateIDR;
    //viewModel.FCYIDRTreshold(dataCall.FCYIDRTrashHold[0]);
    //viewModel.Treshold(dataCall.TreshHold[0]);
}
// AJAX On Alway Callback
function OnAlways() {
    // enabling form input controls
    // viewModel.save(true);
}