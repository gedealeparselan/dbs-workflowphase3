﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace DBS.WebAPI.Models
{
    #region Property
    //  [ModelName("Branch")]

    public class BranchModel
    {
        /// <summary>
        /// Branch ID.
        /// </summary>
        /// 
        public int ID { get; set; }

        /// <summary>
        /// Branch Code
        /// </summary>
        //[MaxLength(50, ErrorMessage= "Branch Code is must be in {1} characters.")]      
        public string Code { get; set; }

        /// <summary>
        /// Branch Name
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Branch Name is must be in {1} characters.")]     
        public string Name { get; set; }

        /// <summary>
        /// Pick up Region ID and Region Name
        /// </summary>  
        public RegionModel Region { get; set; }


        /// <summary>
        /// SolID        /// </summary>      
        public bool? IsHO { get; set; }

        /// <summary>
        /// IsUpcountryBranch
        /// </summary>      
        public bool? IsUpcountryBranch { get; set; }

        /// <summary>
        /// IsUpcountryBranch
        /// </summary>      
        public bool? IsJakartaBranch { get; set; }
        /// <summary>
        /// SolID
        /// </summary>
        public string SolID { get; set; } //add by fandi 

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>

        public string LastModifiedBy { get; set; }

        public int? CityID { get; set; }

        public string CityDescription { get; set; }

        public int ProvinceID { get; set; }
        public string CityCode { get; set; }

    }

    public class BranchRow : BranchModel
    {
        public int RowID { get; set; }

    }


    public class BranchGrid : Grid
    {
        public IList<BranchRow> Rows { get; set; }
    }

    #endregion

    #region Filter

    public class BranchFilter : Filter { }
    #endregion

    #region Interface
    public interface IBranchRepository : IDisposable
    {

        bool GetBranch(ref IList<BranchModel> branch, ref string message);
        bool GetBranchByID(int id, ref BranchModel branch, ref string message);
        bool GetBranch(ref IList<BranchModel> branchs, int limit, int index, ref string message);
        bool GetBranch(int page, int size, IList<BranchFilter> filters, string sortColumn, string sortOrder, ref BranchGrid branch, ref string message);
        bool GetBranch(BranchFilter filter, ref IList<BranchModel> branchs, ref string message);
        bool GetBranchDetail(string key, int limit, ref IList<BranchModel> branchs, ref string message);
        bool GetBankBranchSelected(string key, int limit, int id, ref IList<BranchModel> branch, ref string message);
        bool AddBranch(BranchModel branch, ref string message);
        bool UpdateBranch(int id, BranchModel branch, ref string message);
        bool DeleteBranch(int id, ref string message);
        bool GetSolID(string key, ref IList<BranchModel> branch, ref string Message);
    }
    #endregion

    #region Repository
    public class BranchRepository : IBranchRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetBranchByID(int id, ref BranchModel branch, ref string message)
        {
            bool isSuccess = false;

            try
            {
                branch = (from a in context.Locations
                          where a.IsDeleted.Equals(false) && a.LocationID.Equals(id)
                          select new BranchModel
                          {
                              ID = a.LocationID,
                              Code = a.LocationCode,
                              Name = a.LocationName,
                              Region = a.Region != null ? new RegionModel { ID = a.Region.RegionID, Name = a.Region.RegionName } :
                                       new RegionModel { ID = 0, Name = "" },
                              SolID = a.SolID,
                              IsHO = a.IsHO,
                              IsJakartaBranch = a.IsJakartaBranch,
                              IsUpcountryBranch = a.IsUpcountryBranch,
                              LastModifiedDate = a.CreateDate,
                              LastModifiedBy = a.CreateBy
                          }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBranch(ref IList<BranchModel> branch, ref string message)
        {
            bool isSuccess = false;
            try
            {

                using (DBSEntities context = new DBSEntities())
                {
                    branch = (from a in context.Locations
                              where a.IsDeleted.Equals(false)
                              orderby new { a.LocationName }
                              select new BranchModel
                              {
                                  ID = a.LocationID,
                                  Code = a.LocationCode,
                                  Name = a.LocationName,
                                  Region = a.Region != null ? new RegionModel { ID = a.Region.RegionID, Name = a.Region.RegionName } :
                                           new RegionModel { ID = 0, Name = "" },
                                  SolID = a.SolID,
                                  IsHO = a.IsHO,
                                  IsJakartaBranch = a.IsJakartaBranch,
                                  IsUpcountryBranch = a.IsUpcountryBranch,
                                  LastModifiedBy = a.CreateBy,
                                  LastModifiedDate = a.CreateDate
                              }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;

        }
        public bool GetBranch(ref IList<BranchModel> branch, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    branch = (from a in context.Locations
                              where a.IsDeleted.Equals(false)
                              orderby new { a.LocationName }
                              select new BranchModel
                              {
                                  ID = a.LocationID,
                                  Code = a.LocationCode,
                                  Name = a.LocationName,
                                  Region = a.Region != null ? new RegionModel { ID = a.Region.RegionID, Name = a.Region.RegionName } :
                                           new RegionModel { ID = 0, Name = "" },
                                  SolID = a.SolID,
                                  IsHO = a.IsHO,
                                  IsJakartaBranch = a.IsJakartaBranch,
                                  IsUpcountryBranch = a.IsUpcountryBranch,
                                  LastModifiedBy = a.CreateBy,
                                  LastModifiedDate = a.CreateDate
                              }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBranch(int page, int size, IList<BranchFilter> filters, string sortColumn, string sortOrder, ref BranchGrid branchGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                BranchModel filter = new BranchModel();
                filter.Region = new RegionModel { Name = string.Empty };
                if (filters != null)
                {
                    //filter.Code = (string)filters.Where(a => a.Field.Equals("Code")).Select(a => a.Value).SingleOrDefault();
                    filter.SolID = (string)filters.Where(a => a.Field.Equals("SolID")).Select(a => a.Value).SingleOrDefault();
                    filter.Name = (string)filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.Region.Name = (string)filters.Where(a => a.Field.Equals("Region")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {

                    var data = (from a in context.Locations
                                //let isCode = !string.IsNullOrEmpty(filter.Code)
                                let isSolID = !string.IsNullOrEmpty(filter.SolID)
                                let isName = !string.IsNullOrEmpty(filter.Name)
                                let isRegion = !string.IsNullOrEmpty(filter.Region.Name)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    //(isCode ? a.LocationCode.Contains(filter.Code) : true) &&
                                    (isSolID ? a.SolID.Contains(filter.SolID):true) &&
                                    (isName ? a.LocationName.Contains(filter.Name) : true) &&
                                    (isRegion ? a.Region.RegionName.Contains(filter.Region.Name) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)

                                select new BranchModel
                                {
                                    ID = a.LocationID,
                                    Code = a.LocationCode,
                                    Name = a.LocationName,
                                    Region = a.Region != null ? new RegionModel { ID = a.Region.RegionID, Name = a.Region.RegionName } :
                                             new RegionModel { ID = 0, Name = "" },
                                    SolID = a.SolID,
                                    IsHO = a.IsHO,
                                    IsJakartaBranch = a.IsJakartaBranch,
                                    IsUpcountryBranch = a.IsUpcountryBranch,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    branchGrid.Page = page;
                    branchGrid.Size = size;
                    branchGrid.Total = data.Count();
                    branchGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    branchGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new BranchRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Code = a.Code,
                            Name = a.Name,
                            Region = a.Region,
                            SolID = a.SolID,
                            IsJakartaBranch = a.IsJakartaBranch,
                            IsUpcountryBranch = a.IsUpcountryBranch,
                            IsHO = a.IsHO,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBranch(BranchFilter filter, ref IList<BranchModel> branchs, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetBranchDetail(string key, int limit, ref IList<BranchModel> branchs, ref string message)
        {
            bool isSuccess = false;

            try
            {
                branchs = (from a in context.Locations
                           join b in context.Regions
                           on a.RegionID equals b.RegionID
                           where a.IsDeleted.Equals(false) && b.IsDeleted.Equals(false) &&
                               a.LocationName.Contains(key)
                           orderby new { a.LocationName }
                           select new BranchModel
                           {
                               Name = a.LocationName
                           }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddBranch(BranchModel branch, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.Locations.Where(a => a.LocationName.Equals(branch.Name) && a.IsDeleted.Equals(false)).Any())
                {
                    context.Locations.Add(new Entity.Location()
                    {
                        LocationCode = branch.Code,
                        SolID = branch.Code,
                        LocationName = branch.Name,
                        RegionID = branch.Region.ID,
                        IsHO = branch.IsHO,
                        IsJakartaBranch = branch.IsJakartaBranch,
                        IsUpcountryBranch = branch.IsUpcountryBranch,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Branch data for Branch Name {0} is already exist.", branch.Name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateBranch(int id, BranchModel branch, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Location data = context.Locations.Where(a => a.LocationID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.Locations.Where(a => a.LocationID != branch.ID && a.LocationName.Equals(branch.Name) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Branch Name {0} is exist.", branch.Name);
                    }
                    else
                    {
                        data.LocationName = branch.Name;
                        data.RegionID = branch.Region.ID;
                        data.SolID = branch.SolID;
                        data.IsHO = branch.IsHO;
                        data.IsJakartaBranch = branch.IsJakartaBranch;
                        data.IsUpcountryBranch = branch.IsUpcountryBranch;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Branch data for Branch ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteBranch(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Location data = context.Locations.Where(a => a.LocationID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Branch data for Branch ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        currentUser = null;
                        context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public bool GetSolID(string key, ref IList<BranchModel> branch, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities cont = new DBSEntities())
                {
                    branch = (from loc in cont.Locations
                              where loc.IsDeleted.Equals(false) && (loc.SolID.Contains(key) || loc.LocationName.Contains(key))
                              select new BranchModel
                              {
                                  Code = loc.LocationCode,
                                  Name = loc.LocationName,
                                  ID = loc.LocationID,
                                  SolID = loc.SolID,
                                  IsHO = loc.IsHO,
                                  IsJakartaBranch = loc.IsJakartaBranch,
                                  IsUpcountryBranch = loc.IsUpcountryBranch,
                                  Region = new RegionModel { ID = (int)loc.RegionID, Name = loc.Region.RegionName }
                              }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception e)
            {
                Message = e.Message;
            }
            return isSuccess;
        }



        public bool GetBankBranchSelected(string key, int limit, int id, ref IList<BranchModel> branch, ref string message)
        {
            bool isSuccess = false;

            try
            {
                branch = (from ab in context.Branches
                          join b in context.Banks on ab.BankID equals b.BankID
                          join c in context.Cities on ab.CityID equals c.CityID
                          where (ab.BranchCode.Contains(key) || ab.CommonName.Contains(key)) && b.BankID.Equals(id)
                          select new BranchModel
                          {
                              ID = ab.BranchID,
                              Name = ab.CommonName,
                              Code = ab.BranchCode,
                              ProvinceID = c.ProvinceID.Value,
                              CityID = c.CityID,
                              CityCode = c.CityCode,
                              CityDescription = c.Description
                          }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
    }
    #endregion
}