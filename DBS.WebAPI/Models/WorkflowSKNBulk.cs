﻿using DBS.Entity;
using DBS.WebAPI.Models;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Transactions;
using System.Web;
using System.IO;
using System.Data.Entity.SqlServer;
using System.Data;
using FastMember;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace DBS.WebAPI.Models
{
    #region Property
    public enum TabelSKNBulk
    {
        TransactionSKNBulk = 1,
        TransactionSKNBulkMaker = 2,
        TransactionSKNBulkExcel = 3,
        TransactionSKNBulkTemp = 4
    }

    public class DataForCheckingDoubleTransactionsModel
    {
        public IList<ColumnComparisonModel> columnToCheck { get; set; }
        public IList<TransactionSKNBulkModel> dataToCheck { get; set; }
    }

    public class ReturnDoubleTransactions
    {
        public List<int> LineDuplicates { get; set; }
        public List<string[]> Values { get; set; }
        public List<string[]> Columns { get; set; }
    }
    public class DoubleTransactions
    {
        public string Line { get; set; }
        public string ColName { get; set; }
        public string Value { get; set; }
        public string Duplicates { get; set; }
    }

    public class PaymentCheckerSKNBulkDetailModel
    {
        public IList<TransactionSKNBulkModel> sknBulkData { get; set; }
        public IList<TransactionSKNBulkModel> sknBulkDataOriginal { get; set; }
    }

    public class PaymentMakerSKNBulkDetailModel
    {
        public IList<TransactionSKNBulkModel> transactionSKNBulk { get; set; }
        public IList<TransactionSKNBulkModel> transactionSKNBulkOriginal { get; set; }
    }

    public class ExcelSKNBulkModel
    {
        public Guid instanceID { get; set; }
        public long transactionID { get; set; }
        public string fileName { get; set; }
        public string filePath { get; set; }
        public string pass { get; set; }
        public string siteURL { get; set; }
    }
    public class TransactionSKNBulkModelPrettified
    {
        public long ID { get; set; }
        public string AccountNumber { get; set; }
        public string CustomerName { get; set; }
        public string ValueDate { get; set; }
        public string PaymentAmount { get; set; }
        public string BeneficiaryName { get; set; }
        public string BeneficiaryAccountNumber { get; set; }
        public string BankCode { get; set; }
        public string BranchCode { get; set; }
        public string Citizen { get; set; }
        public string Resident { get; set; }
        public string PaymentDetails { get; set; }
        public string Charges { get; set; }
        public string Type { get; set; }
        public long ParentTransactionID { get; set; }
        public Guid ParentWorkflowInstanceID { get; set; }
        public bool IsChanged { get; set; }
        public string BeneficiaryCustomer { get; set; }
    }
    public class TransactionSKNBulkModel
    {
        public long ID { get; set; }
        public string AccountNumber { get; set; }
        public string CustomerName { get; set; }
        public DateTime? ValueDate { get; set; }
        public string PaymentAmount { get; set; }
        public string BeneficiaryName { get; set; }
        public string BeneficiaryAccountNumber { get; set; }
        public string BankCode { get; set; }
        public string BranchCode { get; set; }
        public string Citizen { get; set; }
        public string Resident { get; set; }
        public string PaymentDetails { get; set; }
        public string Charges { get; set; }
        public string Type { get; set; }
        public long ParentTransactionID { get; set; }
        public Guid ParentWorkflowInstanceID { get; set; }
        public long TransactionDocumentID { get; set; }
        public bool IsChanged { get; set; }
        public string BeneficiaryCustomer { get; set; }
    }
    #endregion

    #region Filter
    #endregion

    #region Interface
    public interface IWorkflowSKNBulkRepository : IDisposable
    {
        bool GetTransactionSKNBulk(Guid workflowInstanceID, long transactionID, ref IList<TransactionSKNBulkModel> output, ref string message);
        bool AddTransactionSKNBulk(Guid workflowInstanceID, long transactionID, long documentID, IList<TransactionSKNBulkModel> excelSknBulkModel, ref string message);
        bool UpdateTransactionSKNBulk(Guid workflowInstanceID, long transactionID, long documentID, PaymentMakerSKNBulkDetailModel paymentMakerSKNBulkDetail, ref string message);
        bool GetTransactionSKNBulkChecker(Guid workflowInstanceID, long transactionID, ref PaymentCheckerSKNBulkDetailModel output, ref string message);
        bool CheckDoubleTransactionSKNBulk(long transactionID, DataForCheckingDoubleTransactionsModel data, ref ReturnDoubleTransactions output, ref string message);
    }
    #endregion

    #region Repository
    public class WorkflowSKNBulkRepository : IWorkflowSKNBulkRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        private static string connString = System.Configuration.ConfigurationManager.ConnectionStrings["DBSEntitiesSP"].ToString();

        public bool GetTransactionSKNBulk(Guid workflowInstanceID, long transactionID, ref IList<TransactionSKNBulkModel> output, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                output = context.TransactionSKNBulks.Where(a => a.ParentTransactionID == transactionID)
                    .Select(a => new TransactionSKNBulkModel
                    {
                        ID = a.ID,
                        AccountNumber = a.AccountNumber,
                        CustomerName = a.CustomerName,
                        ValueDate = a.ValueDate,
                        PaymentAmount = a.PaymentAmount.HasValue ? SqlFunctions.StringConvert(a.PaymentAmount.Value) : "",
                        BeneficiaryName = a.BeneficiaryName,
                        BeneficiaryAccountNumber = a.BeneficiaryAccountNumber,
                        BankCode = a.BankCode,
                        BranchCode = a.BranchCode,
                        Citizen = a.Citizen.HasValue ? (a.Citizen.Value ? "YES" : "NO") : "NO",
                        Resident = a.Resident.HasValue ? (a.Resident.Value ? "YES" : "NO") : "NO",
                        PaymentDetails = a.PaymentDetails,
                        Charges = a.Charges,
                        Type = a.Type,
                        BeneficiaryCustomer = a.BeneficiaryCustomer,
                        ParentTransactionID = a.ParentTransactionID,
                        ParentWorkflowInstanceID = a.ParentWorkflowInstanceID.Value,
                        TransactionDocumentID = a.TransactionDocumentID.HasValue ? a.TransactionDocumentID.Value : 0,
                        IsChanged = a.IsChanged.HasValue ? a.IsChanged.Value : false
                    }).ToList();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return IsSuccess;
        }

        public bool AddTransactionSKNBulk(Guid workflowInstanceID, long transactionID, long documentID, IList<TransactionSKNBulkModel> excelSknBulkModel, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                var dataSKNBulkTransaction = context.TransactionSKNBulkExcels.Where(t => t.ParentTransactionID == transactionID).ToList();
                var dataSKNBulkExcel = context.TransactionSKNBulkExcels.Where(t => t.ParentTransactionID == transactionID && t.TransactionDocumentID == documentID).ToList();
                IList<TransactionSKNBulk> dataSKNBulkToBulkCopy = new List<TransactionSKNBulk>();
                IList<TransactionSKNBulkMaker> dataSKNBulkMakerToBulkCopy = new List<TransactionSKNBulkMaker>();
                IList<TransactionSKNBulkExcel> dataSKNBulkExcelToBulkCopy = new List<TransactionSKNBulkExcel>();

                //only insert data if none found in the db
                //cek sudah ada data excel untuk suatu transaksi, jika belum ada maka pasti pertama kali, insert baru ke 3 tabel skn bulk
                if (dataSKNBulkTransaction.Count == 0)
                {
                    foreach (TransactionSKNBulkModel item in excelSknBulkModel)
                    {
                        TransactionSKNBulk dataSKNBulkToInsert = new TransactionSKNBulk();

                        dataSKNBulkToInsert.AccountNumber = item.AccountNumber;
                        dataSKNBulkToInsert.CustomerName = item.CustomerName;
                        dataSKNBulkToInsert.ValueDate = item.ValueDate;
                        dataSKNBulkToInsert.PaymentAmount = item.PaymentAmount == null || string.IsNullOrEmpty(item.PaymentAmount) ? 0 : decimal.Parse(item.PaymentAmount);
                        dataSKNBulkToInsert.BeneficiaryName = item.BeneficiaryName;
                        dataSKNBulkToInsert.BeneficiaryAccountNumber = item.BeneficiaryAccountNumber;
                        dataSKNBulkToInsert.BankCode = item.BankCode;
                        dataSKNBulkToInsert.BranchCode = item.BranchCode;
                        dataSKNBulkToInsert.Citizen = item.Citizen.Trim().ToLower() == "yes" ? true : false;
                        dataSKNBulkToInsert.Resident = item.Resident.Trim().ToLower() == "yes" ? true : false;
                        dataSKNBulkToInsert.PaymentDetails = item.PaymentDetails;
                        dataSKNBulkToInsert.Charges = item.Charges;
                        dataSKNBulkToInsert.Type = item.Type;
                        dataSKNBulkToInsert.BeneficiaryCustomer = item.BeneficiaryCustomer;
                        dataSKNBulkToInsert.ParentTransactionID = item.ParentTransactionID;
                        dataSKNBulkToInsert.ParentWorkflowInstanceID = item.ParentWorkflowInstanceID;
                        dataSKNBulkToInsert.TransactionDocumentID = documentID;

                        dataSKNBulkToBulkCopy.Add(dataSKNBulkToInsert);
                    }

                    using (SqlConnection connection = new SqlConnection(connString))
                    {
                        connection.Open();
                        DataTable dt = DataTableConverter.CreateDataTableFromList(dataSKNBulkToBulkCopy);
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                        {
                            SetBulkCopyColumnMappings(bulkCopy);
                            bulkCopy.DestinationTableName = "Data.TransactionSKNBulk";
                            bulkCopy.WriteToServer(dt);
                            bulkCopy.DestinationTableName = "Data.TransactionSKNBulkMaker";
                            bulkCopy.WriteToServer(dt);
                            bulkCopy.DestinationTableName = "Data.TransactionSKNBulkExcel";
                            bulkCopy.WriteToServer(dt);
                        }
                    }
                }
                else
                {
                    //aridya 20161228 ilangin kondisi documentID karena selalu berubah jadi setiap upload ulang
                    //untuk sebuah transaksi yang sudah ada pasti hapus semua dan insert ulang
                    var dataTemp = context.TransactionSKNBulks.Where(t => t.ParentTransactionID == transactionID).ToList();
                    var dataTempMaker = context.TransactionSKNBulkMakers.Where(t => t.ParentTransactionID == transactionID).ToList();
                    var dataTempExcel = context.TransactionSKNBulkExcels.Where(t => t.ParentTransactionID == transactionID).ToList();
                    context.SP_DeleteTransactionSKNBulk(transactionID, (int)TabelSKNBulk.TransactionSKNBulk);
                    context.SP_DeleteTransactionSKNBulk(transactionID, (int)TabelSKNBulk.TransactionSKNBulkMaker);
                    context.SP_DeleteTransactionSKNBulk(transactionID, (int)TabelSKNBulk.TransactionSKNBulkExcel);

                    foreach (TransactionSKNBulkModel item in excelSknBulkModel)
                    {
                        TransactionSKNBulk dataSKNBulkToInsert = new TransactionSKNBulk();

                        dataSKNBulkToInsert.AccountNumber = item.AccountNumber;
                        dataSKNBulkToInsert.CustomerName = item.CustomerName;
                        dataSKNBulkToInsert.ValueDate = item.ValueDate;
                        dataSKNBulkToInsert.PaymentAmount = item.PaymentAmount == null || string.IsNullOrEmpty(item.PaymentAmount) ? 0 : decimal.Parse(item.PaymentAmount);
                        dataSKNBulkToInsert.BeneficiaryName = item.BeneficiaryName;
                        dataSKNBulkToInsert.BeneficiaryAccountNumber = item.BeneficiaryAccountNumber;
                        dataSKNBulkToInsert.BankCode = item.BankCode;
                        dataSKNBulkToInsert.BranchCode = item.BranchCode;
                        dataSKNBulkToInsert.Citizen = item.Citizen.Trim().ToLower() == "yes" ? true : false;
                        dataSKNBulkToInsert.Resident = item.Resident.Trim().ToLower() == "yes" ? true : false;
                        dataSKNBulkToInsert.PaymentDetails = item.PaymentDetails;
                        dataSKNBulkToInsert.Charges = item.Charges;
                        dataSKNBulkToInsert.Type = item.Type;
                        dataSKNBulkToInsert.BeneficiaryCustomer = item.BeneficiaryCustomer;
                        dataSKNBulkToInsert.ParentTransactionID = item.ParentTransactionID;
                        dataSKNBulkToInsert.ParentWorkflowInstanceID = item.ParentWorkflowInstanceID;
                        dataSKNBulkToInsert.TransactionDocumentID = documentID;
                        dataSKNBulkToBulkCopy.Add(dataSKNBulkToInsert);
                    }
                    using (SqlConnection connection = new SqlConnection(connString))
                    {
                        connection.Open();
                        DataTable dt = DataTableConverter.CreateDataTableFromList(dataSKNBulkToBulkCopy);
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                        {
                            SetBulkCopyColumnMappings(bulkCopy);
                            bulkCopy.DestinationTableName = "Data.TransactionSKNBulk";
                            bulkCopy.WriteToServer(dt);
                            bulkCopy.DestinationTableName = "Data.TransactionSKNBulkMaker";
                            bulkCopy.WriteToServer(dt);
                            bulkCopy.DestinationTableName = "Data.TransactionSKNBulkExcel";
                            bulkCopy.WriteToServer(dt);
                        }
                    }

                    //unused as of 20161228 aridya
                    ////masih sama excel di attachment tapi bisa aja payment ganti-ganti excel
                    //if (dataSKNBulkExcel.Count != 0) //(dataSKNBulkTransaction.Count != excelSknBulkModel.Count)
                    //{
                    //    var dataTemp = context.TransactionSKNBulks.Where(t => t.ParentTransactionID == transactionID).ToList();
                    //    var dataTempMaker = context.TransactionSKNBulkMakers.Where(t => t.ParentTransactionID == transactionID).ToList();
                    //    var dataTempExcel = context.TransactionSKNBulkExcels.Where(t => t.ParentTransactionID == transactionID).ToList();
                    //    context.SP_DeleteTransactionSKNBulk(transactionID, (int)TabelSKNBulk.TransactionSKNBulk);
                    //    context.SP_DeleteTransactionSKNBulk(transactionID, (int)TabelSKNBulk.TransactionSKNBulkMaker);
                    //    context.SP_DeleteTransactionSKNBulk(transactionID, (int)TabelSKNBulk.TransactionSKNBulkExcel);

                    //    foreach (TransactionSKNBulkModel item in excelSknBulkModel)
                    //    {
                    //        TransactionSKNBulk dataSKNBulkToInsert = new TransactionSKNBulk();

                    //        dataSKNBulkToInsert.AccountNumber = item.AccountNumber;
                    //        dataSKNBulkToInsert.CustomerName = item.CustomerName;
                    //        dataSKNBulkToInsert.ValueDate = item.ValueDate;
                    //        dataSKNBulkToInsert.PaymentAmount = item.PaymentAmount == null || string.IsNullOrEmpty(item.PaymentAmount) ? 0 : decimal.Parse(item.PaymentAmount);
                    //        dataSKNBulkToInsert.BeneficiaryName = item.BeneficiaryName;
                    //        dataSKNBulkToInsert.BeneficiaryAccountNumber = item.BeneficiaryAccountNumber;
                    //        dataSKNBulkToInsert.BankCode = item.BankCode;
                    //        dataSKNBulkToInsert.BranchCode = item.BranchCode;
                    //        dataSKNBulkToInsert.Citizen = item.Citizen.Trim().ToLower() == "yes" ? true : false;
                    //        dataSKNBulkToInsert.Resident = item.Resident.Trim().ToLower() == "yes" ? true : false;
                    //        dataSKNBulkToInsert.PaymentDetails = item.PaymentDetails;
                    //        dataSKNBulkToInsert.Charges = item.Charges;
                    //        dataSKNBulkToInsert.Type = item.Type;
                    //        dataSKNBulkToInsert.BeneficiaryCustomer = item.BeneficiaryCustomer;
                    //        dataSKNBulkToInsert.ParentTransactionID = item.ParentTransactionID;
                    //        dataSKNBulkToInsert.ParentWorkflowInstanceID = item.ParentWorkflowInstanceID;
                    //        dataSKNBulkToInsert.TransactionDocumentID = documentID;
                    //        dataSKNBulkToBulkCopy.Add(dataSKNBulkToInsert);
                    //    }
                    //    using (SqlConnection connection = new SqlConnection(connString))
                    //    {
                    //        connection.Open();
                    //        DataTable dt = DataTableConverter.CreateDataTableFromList(dataSKNBulkToBulkCopy);
                    //        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                    //        {
                    //            SetBulkCopyColumnMappings(bulkCopy);
                    //            bulkCopy.DestinationTableName = "Data.TransactionSKNBulk";
                    //            bulkCopy.WriteToServer(dt);
                    //            bulkCopy.DestinationTableName = "Data.TransactionSKNBulkMaker";
                    //            bulkCopy.WriteToServer(dt);
                    //            bulkCopy.DestinationTableName = "Data.TransactionSKNBulkExcel";
                    //            bulkCopy.WriteToServer(dt);
                    //        }
                    //    }
                    //}
                    ////cek jika document id untuk sebuah transaksi sudah ada atau belum
                    ////jika transaksi sudah ada tapi dokumen belum ada artinya ganti dokumen
                    ////hapus data dokumen lama, masukkan data baru ke TransactionSKNBulkExcel
                    ////jika transaksi ada dan dokumen ada, berarti dokumen masih sama
                    ////tidak melakukan apa-apa
                    //if (dataSKNBulkExcel.Count == 0)
                    //{
                    //    context.SP_DeleteTransactionSKNBulk(transactionID, (int)TabelSKNBulk.TransactionSKNBulkExcel);

                    //    foreach (TransactionSKNBulkModel item in excelSknBulkModel)
                    //    {
                    //        TransactionSKNBulkExcel dataSKNBulkToInsert = new TransactionSKNBulkExcel();
                    //        dataSKNBulkToInsert.AccountNumber = item.AccountNumber;
                    //        dataSKNBulkToInsert.CustomerName = item.CustomerName;
                    //        dataSKNBulkToInsert.ValueDate = item.ValueDate;
                    //        dataSKNBulkToInsert.PaymentAmount = item.PaymentAmount == null || string.IsNullOrEmpty(item.PaymentAmount) ? 0 : decimal.Parse(item.PaymentAmount);
                    //        dataSKNBulkToInsert.BeneficiaryName = item.BeneficiaryName;
                    //        dataSKNBulkToInsert.BeneficiaryAccountNumber = item.BeneficiaryAccountNumber;
                    //        dataSKNBulkToInsert.BankCode = item.BankCode;
                    //        dataSKNBulkToInsert.BranchCode = item.BranchCode;
                    //        dataSKNBulkToInsert.Citizen = item.Citizen.Trim().ToLower() == "yes" ? true : false;
                    //        dataSKNBulkToInsert.Resident = item.Resident.Trim().ToLower() == "yes" ? true : false;
                    //        dataSKNBulkToInsert.PaymentDetails = item.PaymentDetails;
                    //        dataSKNBulkToInsert.Charges = item.Charges;
                    //        dataSKNBulkToInsert.Type = item.Type;
                    //        dataSKNBulkToInsert.BeneficiaryCustomer = item.BeneficiaryCustomer;
                    //        dataSKNBulkToInsert.ParentTransactionID = item.ParentTransactionID;
                    //        dataSKNBulkToInsert.ParentWorkflowInstanceID = item.ParentWorkflowInstanceID;
                    //        dataSKNBulkToInsert.TransactionDocumentID = documentID;
                    //        dataSKNBulkExcelToBulkCopy.Add(dataSKNBulkToInsert);
                    //    }
                    //    using (SqlConnection connection = new SqlConnection(connString))
                    //    {
                    //        connection.Open();
                    //        DataTable dt = DataTableConverter.CreateDataTableFromList(dataSKNBulkExcelToBulkCopy);
                    //        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                    //        {
                    //            SetBulkCopyColumnMappings(bulkCopy);
                    //            bulkCopy.DestinationTableName = "Data.TransactionSKNBulkExcel";
                    //            bulkCopy.WriteToServer(dt);
                    //        }
                    //    }
                    //}
                    //end of unused
                }              

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return IsSuccess;
        }

        public bool UpdateTransactionSKNBulk(Guid workflowInstanceID, long transactionID, long documentID, PaymentMakerSKNBulkDetailModel paymentMakerSKNBulkDetail, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                var data = context.TransactionSKNBulks.Where(t => t.ParentTransactionID == transactionID && t.TransactionDocumentID == documentID).ToList();
                IList<TransactionSKNBulk> dataSKNBulkToBulkCopy = new List<TransactionSKNBulk>();
                IList<TransactionSKNBulkMaker> dataSKNBulkMakerToBulkCopy = new List<TransactionSKNBulkMaker>();
                IList<TransactionSKNBulkExcel> dataSKNBulkExcelToBulkCopy = new List<TransactionSKNBulkExcel>();

                //kalau masih sama excel
                if (data.Count != 0)
                {
                    //update data transaction skn bulk
                    //set all is changed to false
                    //aridya 20170208 disable untuk catet perubahan -1
                    foreach (TransactionSKNBulkModel item in paymentMakerSKNBulkDetail.transactionSKNBulk)
                    {
                        TransactionSKNBulk dataSKNBulk = context.TransactionSKNBulks.Where(t => t.ParentTransactionID == transactionID && t.ID == item.ID).FirstOrDefault();
                        if (dataSKNBulk != null)
                        {
                            dataSKNBulk.IsChanged = false;
                        }
                    }

                    foreach (TransactionSKNBulkModel item in paymentMakerSKNBulkDetail.transactionSKNBulk)
                    {
                        TransactionSKNBulk dataSKNBulk = context.TransactionSKNBulks.Where(t => t.ParentTransactionID == transactionID && t.ID == item.ID).FirstOrDefault();
                        if (dataSKNBulk != null)
                        {
                            if (dataSKNBulk.BankCode != item.BankCode || dataSKNBulk.BranchCode != item.BranchCode)
                            {
                                dataSKNBulk.IsChanged = true;
                            }
                            //aridya 20170208 disable untuk catet perubahan -1
                            else
                            {
                                dataSKNBulk.IsChanged = false;
                            }
                            dataSKNBulk.BranchCode = item.BranchCode;
                            dataSKNBulk.BankCode = item.BankCode;
                        }
                    }

                    context.SaveChanges();

                    if (paymentMakerSKNBulkDetail.transactionSKNBulkOriginal != null) //ganti 20161227 aridya
                    {
                        var dataMaker = context.TransactionSKNBulkMakers.Where(t => t.ParentTransactionID == transactionID && t.TransactionDocumentID == documentID).ToList();
                        //cek data yang tersimpan di transaction skn bulk maker apakah dokumennya sudah yang terbaru
                        //kasus flow ppu -> payment maker -> payment checker -> ppu ubah dokumen -> payment maker -> payment checker -> payment maker
                        //kalau sudah dokumen terbaru maka update berdasarkan id
                        //jika belum maka hapus semua data di maker terkait transaction id tersebut dan masukkan data latest -1
                        if (dataMaker.Count != 0)
                        {
                            foreach (TransactionSKNBulkModel item in paymentMakerSKNBulkDetail.transactionSKNBulkOriginal)
                            {
                                TransactionSKNBulkMaker dataSKNBulkMaker = context.TransactionSKNBulkMakers.Where(t => t.ParentTransactionID == transactionID && t.ID == item.ID).FirstOrDefault();
                                if (dataSKNBulkMaker != null)
                                {
                                    dataSKNBulkMaker.BranchCode = item.BranchCode;
                                    dataSKNBulkMaker.BankCode = item.BankCode;
                                }
                            }
                        }
                        else
                        {
                            List<TransactionSKNBulkMaker> dataTrxMaker = context.TransactionSKNBulkMakers.Where(t => t.ParentTransactionID == transactionID).ToList();
                            if (dataTrxMaker != null)
                            {
                                context.SP_DeleteTransactionSKNBulk(transactionID, (int)TabelSKNBulk.TransactionSKNBulkMaker);
                            }

                            foreach (TransactionSKNBulkModel item in paymentMakerSKNBulkDetail.transactionSKNBulkOriginal)
                            {
                                TransactionSKNBulkMaker dataSKNBulkToInsert = new TransactionSKNBulkMaker();
                                dataSKNBulkToInsert.AccountNumber = item.AccountNumber;
                                dataSKNBulkToInsert.CustomerName = item.CustomerName;
                                dataSKNBulkToInsert.ValueDate = item.ValueDate;
                                dataSKNBulkToInsert.PaymentAmount = item.PaymentAmount == null || string.IsNullOrEmpty(item.PaymentAmount) ? 0 : decimal.Parse(item.PaymentAmount);
                                dataSKNBulkToInsert.BeneficiaryName = item.BeneficiaryName;
                                dataSKNBulkToInsert.BeneficiaryAccountNumber = item.BeneficiaryAccountNumber;
                                dataSKNBulkToInsert.BankCode = item.BankCode;
                                dataSKNBulkToInsert.BranchCode = item.BranchCode;
                                dataSKNBulkToInsert.Citizen = item.Citizen.Trim().ToLower() == "yes" ? true : false;
                                dataSKNBulkToInsert.Resident = item.Resident.Trim().ToLower() == "yes" ? true : false;
                                dataSKNBulkToInsert.PaymentDetails = item.PaymentDetails;
                                dataSKNBulkToInsert.Charges = item.Charges;
                                dataSKNBulkToInsert.Type = item.Type;
                                dataSKNBulkToInsert.BeneficiaryCustomer = item.BeneficiaryCustomer;
                                dataSKNBulkToInsert.ParentTransactionID = item.ParentTransactionID;
                                dataSKNBulkToInsert.ParentWorkflowInstanceID = item.ParentWorkflowInstanceID;
                                dataSKNBulkToInsert.TransactionDocumentID = documentID;
                                dataSKNBulkMakerToBulkCopy.Add(dataSKNBulkToInsert);
                            }
                            using (SqlConnection connection = new SqlConnection(connString))
                            {
                                connection.Open();
                                DataTable dt = DataTableConverter.CreateDataTableFromList(dataSKNBulkMakerToBulkCopy);
                                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                                {
                                    SetBulkCopyColumnMappings(bulkCopy);
                                    bulkCopy.DestinationTableName = "Data.TransactionSKNBulkMaker";
                                    bulkCopy.WriteToServer(dt);
                                }
                            }
                        }
                    }                    
                }
                //ganti excel
                else
                {
                    List<TransactionSKNBulk> dataOriginal = context.TransactionSKNBulks.Where(t => t.ParentTransactionID == transactionID).ToList();

                    foreach(TransactionSKNBulk item in dataOriginal)
                    {
                        TransactionSKNBulkMaker dataMaker = context.TransactionSKNBulkMakers.Where(t => t.ParentTransactionID == transactionID && t.ID == item.ID).FirstOrDefault();

                        if (dataMaker != null)
                        {
                            dataMaker.BranchCode = item.BranchCode;
                            dataMaker.BankCode = item.BankCode;
                        }
                    }

                    context.SP_DeleteTransactionSKNBulk(transactionID, (int)TabelSKNBulk.TransactionSKNBulk);

                    foreach (TransactionSKNBulkModel item in paymentMakerSKNBulkDetail.transactionSKNBulk)
                    {
                        TransactionSKNBulk dataSKNBulkToInsert = new TransactionSKNBulk();
                        dataSKNBulkToInsert.AccountNumber = item.AccountNumber;
                        dataSKNBulkToInsert.CustomerName = item.CustomerName;
                        dataSKNBulkToInsert.ValueDate = item.ValueDate;
                        dataSKNBulkToInsert.PaymentAmount = item.PaymentAmount == null || string.IsNullOrEmpty(item.PaymentAmount) ? 0 : decimal.Parse(item.PaymentAmount);
                        dataSKNBulkToInsert.BeneficiaryName = item.BeneficiaryName;
                        dataSKNBulkToInsert.BeneficiaryAccountNumber = item.BeneficiaryAccountNumber;
                        dataSKNBulkToInsert.BankCode = item.BankCode;
                        dataSKNBulkToInsert.BranchCode = item.BranchCode;
                        dataSKNBulkToInsert.Citizen = item.Citizen.Trim().ToLower() == "yes" ? true : false;
                        dataSKNBulkToInsert.Resident = item.Resident.Trim().ToLower() == "yes" ? true : false;
                        dataSKNBulkToInsert.PaymentDetails = item.PaymentDetails;
                        dataSKNBulkToInsert.Charges = item.Charges;
                        dataSKNBulkToInsert.Type = item.Type;
                        dataSKNBulkToInsert.BeneficiaryCustomer = item.BeneficiaryCustomer;
                        dataSKNBulkToInsert.ParentTransactionID = item.ParentTransactionID;
                        dataSKNBulkToInsert.ParentWorkflowInstanceID = item.ParentWorkflowInstanceID;
                        dataSKNBulkToInsert.TransactionDocumentID = documentID;
                        dataSKNBulkToBulkCopy.Add(dataSKNBulkToInsert);
                        //context.TransactionSKNBulks.Add(dataSKNBulkToInsert);
                    }
                    using (SqlConnection connection = new SqlConnection(connString))
                    {
                        connection.Open();
                        DataTable dt = DataTableConverter.CreateDataTableFromList(dataSKNBulkToBulkCopy);
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                        {
                            SetBulkCopyColumnMappings(bulkCopy);
                            bulkCopy.DestinationTableName = "Data.TransactionSKNBulk";
                            bulkCopy.WriteToServer(dt);
                        }
                    }

                    TransactionSKNBulkExcel[] dataExcelOri = context.TransactionSKNBulkExcels.Where(t => t.ParentTransactionID == transactionID).ToArray();
                    TransactionSKNBulk[] dataSKNBulk = context.TransactionSKNBulks.Where(t => t.ParentTransactionID == transactionID).ToArray();

                    bool isDataChanged = false;

                    if (dataExcelOri.Length == dataSKNBulk.Length)
                    {
                        for (int ii = 0; ii < dataSKNBulk.Length; ii++)
                        {
                            TransactionSKNBulk dataSKNBulkUpdate = context.TransactionSKNBulks.Where(t => t.ParentTransactionID == transactionID && t.ID == dataSKNBulk[ii].ID).FirstOrDefault();
                            if (dataExcelOri[ii].BankCode != dataSKNBulk[ii].BankCode || dataExcelOri[ii].BranchCode != dataSKNBulk[ii].BranchCode)
                            {
                                dataSKNBulkUpdate.IsChanged = true;
                                isDataChanged = true;
                            }
                        }

                        if (isDataChanged)
                        {
                            List<TransactionSKNBulkMaker> dataSKNMaker = context.TransactionSKNBulkMakers.Where(t => t.ParentTransactionID == transactionID).ToList();

                            context.SP_DeleteTransactionSKNBulk(transactionID, (int)TabelSKNBulk.TransactionSKNBulkMaker);

                            for (int ii = 0; ii < dataExcelOri.Length; ii++)
                            {
                                TransactionSKNBulkMaker dataSKNBulkToInsert = new TransactionSKNBulkMaker();
                                dataSKNBulkToInsert.AccountNumber = dataExcelOri[ii].AccountNumber;
                                dataSKNBulkToInsert.CustomerName = dataExcelOri[ii].CustomerName;
                                dataSKNBulkToInsert.ValueDate = dataExcelOri[ii].ValueDate;
                                dataSKNBulkToInsert.PaymentAmount = dataExcelOri[ii].PaymentAmount;
                                dataSKNBulkToInsert.BeneficiaryName = dataExcelOri[ii].BeneficiaryName;
                                dataSKNBulkToInsert.BeneficiaryAccountNumber = dataExcelOri[ii].BeneficiaryAccountNumber;
                                dataSKNBulkToInsert.BankCode = dataExcelOri[ii].BankCode;
                                dataSKNBulkToInsert.BranchCode = dataExcelOri[ii].BranchCode;
                                dataSKNBulkToInsert.Citizen = dataExcelOri[ii].Citizen;
                                dataSKNBulkToInsert.Resident = dataExcelOri[ii].Resident;
                                dataSKNBulkToInsert.PaymentDetails = dataExcelOri[ii].PaymentDetails;
                                dataSKNBulkToInsert.Charges = dataExcelOri[ii].Charges;
                                dataSKNBulkToInsert.Type = dataExcelOri[ii].Type;
                                dataSKNBulkToInsert.BeneficiaryCustomer = dataExcelOri[ii].BeneficiaryCustomer;
                                dataSKNBulkToInsert.ParentTransactionID = dataExcelOri[ii].ParentTransactionID;
                                dataSKNBulkToInsert.ParentWorkflowInstanceID = dataExcelOri[ii].ParentWorkflowInstanceID;
                                dataSKNBulkToInsert.TransactionDocumentID = documentID;
                                dataSKNBulkMakerToBulkCopy.Add(dataSKNBulkToInsert);
                                //context.TransactionSKNBulkMakers.Add(dataSKNBulkToInsert);
                            }
                            using (SqlConnection connection = new SqlConnection(connString))
                            {
                                connection.Open();
                                DataTable dt = DataTableConverter.CreateDataTableFromList(dataSKNBulkMakerToBulkCopy);
                                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                                {
                                    SetBulkCopyColumnMappings(bulkCopy);
                                    bulkCopy.DestinationTableName = "Data.TransactionSKNBulkMaker";
                                    bulkCopy.WriteToServer(dt);
                                }
                            }
                        }
                    }
                }
                context.SaveChanges();

                IsSuccess = true;
                
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        public bool GetTransactionSKNBulkChecker(Guid workflowInstanceID, long transactionID, ref PaymentCheckerSKNBulkDetailModel output, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                //output.sknBulkData = new List<TransactionSKNBulkModelPrettified>();
                //IList<TransactionSKNBulkModel>
                output.sknBulkData = context.TransactionSKNBulks.Where(a => a.ParentTransactionID == transactionID)
                    .Select(a => new TransactionSKNBulkModel
                    {
                        ID = a.ID,
                        AccountNumber = a.AccountNumber,
                        CustomerName = a.CustomerName,
                        ValueDate = a.ValueDate,
                        PaymentAmount = a.PaymentAmount.HasValue ? SqlFunctions.StringConvert(a.PaymentAmount.Value) : "",
                        BeneficiaryName = a.BeneficiaryName,
                        BeneficiaryAccountNumber = a.BeneficiaryAccountNumber,
                        BankCode = a.BankCode,
                        BranchCode = a.BranchCode,
                        Citizen = a.Citizen.HasValue ? (a.Citizen.Value ? "YES" : "NO") : "NO",
                        Resident = a.Resident.HasValue ? (a.Resident.Value ? "YES" : "NO") : "NO",
                        PaymentDetails = a.PaymentDetails,
                        Charges = a.Charges,
                        Type = a.Type,
                        BeneficiaryCustomer = a.BeneficiaryCustomer,
                        ParentTransactionID = a.ParentTransactionID,
                        ParentWorkflowInstanceID = a.ParentWorkflowInstanceID.Value,
                        TransactionDocumentID = a.TransactionDocumentID.HasValue ? a.TransactionDocumentID.Value : 0,
                        IsChanged = a.IsChanged.HasValue ? a.IsChanged.Value : false
                    }).ToList();

                //foreach (TransactionSKNBulkModel item in sknBulkData)
                //{
                //    TransactionSKNBulkModelPrettified newItem = new TransactionSKNBulkModelPrettified();
                //    newItem.ID = item.ID;
                //    newItem.AccountNumber = item.AccountNumber;
                //    newItem.CustomerName = item.CustomerName;
                //    newItem.ValueDate = item.ValueDate.Value.ToString("yyyy-MM-dd");
                //    newItem.PaymentAmount = item.PaymentAmount;
                //    newItem.BeneficiaryName = item.BeneficiaryName;
                //    newItem.BeneficiaryAccountNumber = item.BeneficiaryAccountNumber;
                //    newItem.BankCode = item.BankCode;
                //    newItem.BranchCode = item.BranchCode;
                //    newItem.Citizen = item.Citizen;
                //    newItem.Resident = item.Resident;
                //    newItem.PaymentDetails = item.PaymentDetails;
                //    newItem.Charges = item.Charges;
                //    newItem.Type = item.Type;
                //    newItem.ParentTransactionID = item.ParentTransactionID;
                //    newItem.ParentWorkflowInstanceID = item.ParentWorkflowInstanceID;
                //    newItem.IsChanged = item.IsChanged;
                //    output.sknBulkData.Add(newItem);
                //}

                //nilai original tidak akan null jika dan hanya jika di transaction skn bulk ada yang IsChanged
                //atau transactiondocumentid di transactionsknbulk dan transactionsknbulkmaker tidak sama
                int changedCount = context.TransactionSKNBulks.Where(a => a.ParentTransactionID == transactionID && a.IsChanged == true).Count();

                long? documentIDTrx = context.TransactionSKNBulks.Where(a => a.ParentTransactionID == transactionID).Select(a => a.TransactionDocumentID).FirstOrDefault();
                long? documentIDTrxMaker = context.TransactionSKNBulkMakers.Where(a => a.ParentTransactionID == transactionID).Select(a => a.TransactionDocumentID).FirstOrDefault();

                int rolemaker = currentUser.GetCurrentUser().Roles.Where(r => r.Name.ToLower().Contains("payment maker")).Count();
                int rolechecker = currentUser.GetCurrentUser().Roles.Where(r => r.Name.ToLower().Contains("payment checker")).Count();

                if (rolemaker > 0)
                {
                    output.sknBulkDataOriginal = context.TransactionSKNBulkMakers.Where(a => a.ParentTransactionID == transactionID)
                    .Select(a => new TransactionSKNBulkModel
                    {
                        ID = a.ID,
                        AccountNumber = a.AccountNumber,
                        CustomerName = a.CustomerName,
                        ValueDate = a.ValueDate,
                        PaymentAmount = a.PaymentAmount.HasValue ? SqlFunctions.StringConvert(a.PaymentAmount.Value) : "",
                        BeneficiaryName = a.BeneficiaryName,
                        BeneficiaryAccountNumber = a.BeneficiaryAccountNumber,
                        BankCode = a.BankCode,
                        BranchCode = a.BranchCode,
                        Citizen = a.Citizen.HasValue ? (a.Citizen.Value ? "YES" : "NO") : "NO",
                        Resident = a.Resident.HasValue ? (a.Resident.Value ? "YES" : "NO") : "NO",
                        PaymentDetails = a.PaymentDetails,
                        Charges = a.Charges,
                        Type = a.Type,
                        BeneficiaryCustomer = a.BeneficiaryCustomer,
                        ParentTransactionID = a.ParentTransactionID,
                        ParentWorkflowInstanceID = a.ParentWorkflowInstanceID.Value,
                        TransactionDocumentID = a.TransactionDocumentID.HasValue ? a.TransactionDocumentID.Value : 0,
                        IsChanged = a.IsChanged.HasValue ? a.IsChanged.Value : false
                    }).ToList();
                }
                else //if(rolechecker > 0) aridya 20170219 changed for monitoring
                {
                    if (changedCount > 0)// || documentIDTrx != documentIDTrxMaker)  aridya 20170219 changed for monitoring
                    {
                        //output.sknBulkDataOriginal = new List<TransactionSKNBulkModelPrettified>();
                        //IList<TransactionSKNBulkModel>
                        output.sknBulkDataOriginal = context.TransactionSKNBulkMakers.Where(a => a.ParentTransactionID == transactionID)
                        .Select(a => new TransactionSKNBulkModel
                        {
                            ID = a.ID,
                            AccountNumber = a.AccountNumber,
                            CustomerName = a.CustomerName,
                            ValueDate = a.ValueDate,
                            PaymentAmount = a.PaymentAmount.HasValue ? SqlFunctions.StringConvert(a.PaymentAmount.Value) : "",
                            BeneficiaryName = a.BeneficiaryName,
                            BeneficiaryAccountNumber = a.BeneficiaryAccountNumber,
                            BankCode = a.BankCode,
                            BranchCode = a.BranchCode,
                            Citizen = a.Citizen.HasValue ? (a.Citizen.Value ? "YES" : "NO") : "NO",
                            Resident = a.Resident.HasValue ? (a.Resident.Value ? "YES" : "NO") : "NO",
                            PaymentDetails = a.PaymentDetails,
                            Charges = a.Charges,
                            Type = a.Type,
                            BeneficiaryCustomer = a.BeneficiaryCustomer,
                            ParentTransactionID = a.ParentTransactionID,
                            ParentWorkflowInstanceID = a.ParentWorkflowInstanceID.Value,
                            TransactionDocumentID = a.TransactionDocumentID.HasValue ? a.TransactionDocumentID.Value : 0,
                            IsChanged = a.IsChanged.HasValue ? a.IsChanged.Value : false
                        }).ToList();

                        //foreach (TransactionSKNBulkModel item in sknBulkDataOriginal)
                        //{
                        //    TransactionSKNBulkModelPrettified newItem = new TransactionSKNBulkModelPrettified();
                        //    newItem.ID = item.ID;
                        //    newItem.AccountNumber = item.AccountNumber;
                        //    newItem.CustomerName = item.CustomerName;
                        //    newItem.ValueDate = item.ValueDate.Value.ToString("yyyy-MM-dd");
                        //    newItem.PaymentAmount = item.PaymentAmount;
                        //    newItem.BeneficiaryName = item.BeneficiaryName;
                        //    newItem.BeneficiaryAccountNumber = item.BeneficiaryAccountNumber;
                        //    newItem.BankCode = item.BankCode;
                        //    newItem.BranchCode = item.BranchCode;
                        //    newItem.Citizen = item.Citizen;
                        //    newItem.Resident = item.Resident;
                        //    newItem.PaymentDetails = item.PaymentDetails;
                        //    newItem.Charges = item.Charges;
                        //    newItem.Type = item.Type;
                        //    newItem.ParentTransactionID = item.ParentTransactionID;
                        //    newItem.ParentWorkflowInstanceID = item.ParentWorkflowInstanceID;
                        //    newItem.IsChanged = item.IsChanged;
                        //    output.sknBulkDataOriginal.Add(newItem);
                        //}
                    }
                    else
                    {
                        output.sknBulkDataOriginal = null;
                    }
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        public bool CheckDoubleTransactionSKNBulk(long transactionID, DataForCheckingDoubleTransactionsModel data, ref ReturnDoubleTransactions output, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                if (data.columnToCheck.Count > 0)
                {
                    //List<string> columnNoSpace = new List<string>();
                    string pattern = @"\s+"; //@"(/\s/g)";
                    List<string> columns = new List<string>();
                    string columnsToSelectAsColName = "";
                    string columnsToSelectAsValue = "";
                    string columnsToSelectForSub1 = "";
                    string columnsToFrom = "";
                    string columnsToWhere = "";
                    string columnsToOrder = "";
                    string tableName = "[STG].[TransactionSKNBulkTemp]";
                    int jj = 0;
                    foreach (ColumnComparisonModel column in data.columnToCheck)
                    {
                        string columnNoSpace = "";

                        if (column.ColumnName.Contains("Debit") || column.ColumnName.Contains("DebitAccount")) //special case
                        {
                            columnNoSpace = "AccountNumber";
                        }
                        else
                        {
                            columnNoSpace = Regex.Replace(column.ColumnName, pattern, ""); //new Regex(pattern).Replace(column.ColumnName, "");
                        }
                        columns.Add(columnNoSpace);
                        columnsToSelectAsColName = string.Format("{0}{1}", columnsToSelectAsColName, column.ColumnName);
                        columnsToSelectAsValue = string.Format("{0}Main.[{1}]", columnsToSelectAsValue, columnNoSpace); //+= "Main.[" + column.ColumnName + "]";
                        columnsToSelectForSub1 = string.Format("{0}RTRIM(LTRIM(ST2.[{1}])) AS [{1}]", columnsToSelectForSub1, columnNoSpace); //+= "RTRIM(LTRIM(ST2.[" + column.ColumnName + "])) AS [" + column.ColumnName + "]";
                        columnsToFrom = string.Format("{0}[{1}]", columnsToFrom, columnNoSpace); //+= "[" + column.ColumnName +"]";
                        columnsToWhere = string.Format("{0}ST1.[{1}] = ST2.[{1}]", columnsToWhere, columnNoSpace); //+= "ST1.[" + column.ColumnName + "] = ST2.[" + column.ColumnName + "]";
                        columnsToOrder = string.Format("{0}ST1.[{1}]", columnsToOrder, columnNoSpace);

                        if (jj != data.columnToCheck.Count - 1)
                        {
                            columnsToSelectAsColName = string.Format("{0}{1}", columnsToSelectAsColName, ",");
                            columnsToSelectAsValue = string.Format("{0}{1}", columnsToSelectAsValue, "+'|'+"); //+= "|";
                            columnsToSelectForSub1 = string.Format("{0}{1}", columnsToSelectForSub1, ", "); //+= ", ";
                            columnsToFrom = string.Format("{0}{1}", columnsToFrom, ","); //+= ",";
                            columnsToWhere = string.Format("{0}{1}", columnsToWhere, " AND "); //+= "  AND ";
                            columnsToOrder = string.Format("{0}{1}", columnsToOrder, ", ");
                        }
                        jj++;
                    }
                    output.Columns.Add(columns.ToArray());
                    //masuk ke tabel temporary
                    List<TransactionSKNBulkTemp> dataToTable = new List<TransactionSKNBulkTemp>();

                    //context.SP_DeleteTransactionSKNBulk(transactionID, (int)TabelSKNBulk.TransactionSKNBulkMaker);

                    foreach (TransactionSKNBulkModel item in data.dataToCheck)
                    {
                        TransactionSKNBulkTemp dataSKNBulkToInsert = new TransactionSKNBulkTemp();
                        dataSKNBulkToInsert.AccountNumber = item.AccountNumber;
                        dataSKNBulkToInsert.CustomerName = item.CustomerName;
                        dataSKNBulkToInsert.ValueDate = item.ValueDate;
                        dataSKNBulkToInsert.PaymentAmount = Decimal.Parse(item.PaymentAmount);
                        dataSKNBulkToInsert.BeneficiaryName = item.BeneficiaryName;
                        dataSKNBulkToInsert.BeneficiaryAccountNumber = item.BeneficiaryAccountNumber;
                        dataSKNBulkToInsert.BankCode = item.BankCode;
                        dataSKNBulkToInsert.BranchCode = item.BranchCode;
                        dataSKNBulkToInsert.Citizen = item.Citizen.Trim().ToLower() == "yes";
                        dataSKNBulkToInsert.Resident = item.Resident.Trim().ToLower() == "yes";
                        dataSKNBulkToInsert.PaymentDetails = item.PaymentDetails;
                        dataSKNBulkToInsert.Charges = item.Charges;
                        dataSKNBulkToInsert.Type = item.Type;
                        dataSKNBulkToInsert.BeneficiaryCustomer = item.BeneficiaryCustomer;
                        dataSKNBulkToInsert.ParentTransactionID = item.ParentTransactionID;
                        dataToTable.Add(dataSKNBulkToInsert);
                    }
                    using (SqlConnection connection = new SqlConnection(connString))
                    {
                        connection.Open();
                        DataTable dt = DataTableConverter.CreateDataTableFromList(dataToTable);
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                        {
                            SetBulkCopyColumnMappingsForTemp(bulkCopy);
                            bulkCopy.DestinationTableName = "STG.TransactionSKNBulkTemp";
                            bulkCopy.WriteToServer(dt);
                        }
                    }
                    //end masuk ke table

                    //jalankan check double transaction
                    string sqlTemp = @"SELECT 
                                [Line], 
                                '{0}' [ColName],
                                {1} as Value,
                                CONVERT(VARCHAR(250),Left(Main.ByCode,Len(Main.ByCode)-1),0) AS [Duplicates]
                                FROM (
                                    SELECT DISTINCT {2},
                                    (
                                        SELECT LTRIM(STR(ST1.RowNum)) + ',' AS [text()]
		                                FROM (SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) RowNum,{3}
		                                FROM {4} WHERE {4}.[ParentTransactionID] = {5} ) ST1
		                                WHERE {6}
		                                ORDER BY {7}
		                                FOR XML PATH ('')
                                    ) [ByCode],
                                    (
                                        SELECT TOP 1 ST1.RowNum AS [text()]
		                                FROM (SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) RowNum,{3}
		                                FROM {4} WHERE {4}.[ParentTransactionID] = {5} ) ST1
		                                WHERE {6}
		                                ORDER BY {7}
		                                FOR XML PATH ('')
                                    ) [Line],
                                    (
                                        SELECT '.' AS [text()]
		                                FROM (SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) RowNum,{3}
		                                FROM {4} WHERE {4}.[ParentTransactionID] = {5} ) ST1
		                                WHERE {6}
		                                ORDER BY {7}
		                                FOR XML PATH ('')
                                    ) [Count]
                                    FROM (
                                        SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) RowNum,{3}
		                                FROM {4}
                                        WHERE {4}.[ParentTransactionID] = {5}
                                    ) ST2
                                ) [Main] WHERE LEN([Count])>1
                        ";
                    //0 = columnsToSelectAsColName
                    //1 = columnsToSelectAsValue
                    //2 = columnsToSelectForSub1
                    //3 = columnsToFrom
                    //4 = tableName
                    //5 = transactionID
                    //6 = columnsToWhere
                    //7 = columnsToOrder

                    string sql = string.Format(sqlTemp, columnsToSelectAsColName, columnsToSelectAsValue, columnsToSelectForSub1, columnsToFrom,
                        tableName, transactionID, columnsToWhere, columnsToOrder);
                    context.Database.CommandTimeout = 600;
                    IList<DoubleTransactions> resultDouble = context.Database.SqlQuery<DoubleTransactions>(sql).ToList();

                    if (resultDouble.Count > 0)
                    {
                        foreach (DoubleTransactions item in resultDouble)
                        {
                            string[] lines = item.Duplicates.Split(',');
                            for (int ii = 0; ii < lines.Length; ii++)
                            {
                                output.LineDuplicates.Add(Int32.Parse(lines[ii]));
                            }
                            string[] values = item.Value.Split('|');
                            output.Values.Add(values);
                        }
                    }

                    //kosongin tabel temp
                    context.SP_DeleteTransactionSKNBulk(transactionID, (int)TabelSKNBulk.TransactionSKNBulkTemp);
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                //kosongin tabel temp
                context.SP_DeleteTransactionSKNBulk(transactionID, (int)TabelSKNBulk.TransactionSKNBulkTemp);
            }
            return IsSuccess;
        }

        private void SetBulkCopyColumnMappings(SqlBulkCopy bulkCopy)
        {
            bulkCopy.ColumnMappings.Add("AccountNumber", "AccountNumber");
            bulkCopy.ColumnMappings.Add("CustomerName", "CustomerName");
            bulkCopy.ColumnMappings.Add("ValueDate", "ValueDate");
            bulkCopy.ColumnMappings.Add("PaymentAmount", "PaymentAmount");
            bulkCopy.ColumnMappings.Add("BeneficiaryName", "BeneficiaryName");
            bulkCopy.ColumnMappings.Add("BeneficiaryAccountNumber", "BeneficiaryAccountNumber");
            bulkCopy.ColumnMappings.Add("BankCode", "BankCode");
            bulkCopy.ColumnMappings.Add("BranchCode", "BranchCode");
            bulkCopy.ColumnMappings.Add("Citizen", "Citizen");
            bulkCopy.ColumnMappings.Add("Resident", "Resident");
            bulkCopy.ColumnMappings.Add("PaymentDetails", "PaymentDetails");
            bulkCopy.ColumnMappings.Add("Charges", "Charges");
            bulkCopy.ColumnMappings.Add("Type", "Type");
            bulkCopy.ColumnMappings.Add("ParentTransactionID", "ParentTransactionID");
            bulkCopy.ColumnMappings.Add("ParentWorkflowInstanceID", "ParentWorkflowInstanceID");
            bulkCopy.ColumnMappings.Add("TransactionDocumentID", "TransactionDocumentID");
            bulkCopy.ColumnMappings.Add("BeneficiaryCustomer", "BeneficiaryCustomer");
        }

        private void SetBulkCopyColumnMappingsForTemp(SqlBulkCopy bulkCopy)
        {
            bulkCopy.ColumnMappings.Add("AccountNumber", "AccountNumber");
            bulkCopy.ColumnMappings.Add("CustomerName", "CustomerName");
            bulkCopy.ColumnMappings.Add("ValueDate", "ValueDate");
            bulkCopy.ColumnMappings.Add("PaymentAmount", "PaymentAmount");
            bulkCopy.ColumnMappings.Add("BeneficiaryName", "BeneficiaryName");
            bulkCopy.ColumnMappings.Add("BeneficiaryAccountNumber", "BeneficiaryAccountNumber");
            bulkCopy.ColumnMappings.Add("BankCode", "BankCode");
            bulkCopy.ColumnMappings.Add("BranchCode", "BranchCode");
            bulkCopy.ColumnMappings.Add("Citizen", "Citizen");
            bulkCopy.ColumnMappings.Add("Resident", "Resident");
            bulkCopy.ColumnMappings.Add("PaymentDetails", "PaymentDetails");
            bulkCopy.ColumnMappings.Add("Charges", "Charges");
            bulkCopy.ColumnMappings.Add("Type", "Type");
            bulkCopy.ColumnMappings.Add("ParentTransactionID", "ParentTransactionID");
            bulkCopy.ColumnMappings.Add("BeneficiaryCustomer", "BeneficiaryCustomer");
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.Collect();
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}