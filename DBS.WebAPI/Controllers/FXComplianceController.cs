﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class FXComplianceController : ApiController
    {
        private string message = string.Empty;

        private readonly IFXComplianceRepository repository = null;

        public FXComplianceController()
        {
            this.repository = new FXComplianceRepository();
        }

        public FXComplianceController(IFXComplianceRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all FX Compliances.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<FXComplianceModel>))]
        public HttpResponseMessage Get()
        {
            IList<FXComplianceModel> output = new List<FXComplianceModel>();

            if (!repository.GetFXCompliance(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<FXComplianceModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get FX Compliances with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<FXComplianceModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<FXComplianceModel> output = new List<FXComplianceModel>();

            if (!repository.GetFXCompliance(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<FXComplianceModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get FX Compliances for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(FXComplianceGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<FXComplianceFilter> filters)
        {
            FXComplianceGrid output = new FXComplianceGrid();

            if (!repository.GetFXCompliance(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<FXComplianceGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find FX Compliances using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<FXComplianceModel>))]
        [HttpGet]
        [Route("api/FXCompliance/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<FXComplianceModel> output = new List<FXComplianceModel>();

            if (!repository.GetFXCompliance(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<FXComplianceModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get FX Compliance by ID.
        /// </summary>
        /// <param name="id">FX Compliance ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(FXComplianceModel))]
        [Route("api/FXCompliance/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            FXComplianceModel output = new FXComplianceModel();

            if (!repository.GetFXComplianceByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<FXComplianceModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new FX Compliance.
        /// </summary>
        /// <param name="FXCompliance">FX Compliance data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(FXComplianceModel FXCompliance)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddFXCompliance(FXCompliance, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New FX Compliance data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing FX Compliance.
        /// </summary>
        /// <param name="id">ID of FXCompliance to be modify.</param>
        /// <param name="FXCompliance">FX Compliance data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/FXCompliance/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, FXComplianceModel FXCompliance)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateFXCompliance(id, FXCompliance, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "FX Compliance data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting FX Compliance.
        /// </summary>
        /// <param name="id">ID of FX Compliance to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/FXCompliance/{id}")]
        [Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteFXCompliance(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "FXCompliance data has been deleted." });
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}