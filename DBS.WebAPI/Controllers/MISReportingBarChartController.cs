﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;
using DBS.Entity;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class MISReportingBarChartController : ApiController
    {
        private string message = string.Empty;
        private readonly IMISReportingBarChart repositoryMISReportingBarChart = null;

        public MISReportingBarChartController()
        {
            this.repositoryMISReportingBarChart = new MISReportingBarChartRepository();
        }
        
        public MISReportingBarChartController(IMISReportingBarChart repositoryMISReportingBarChart)
        {
            this.repositoryMISReportingBarChart = repositoryMISReportingBarChart;
        }

        [ResponseType(typeof(MISReportingBarChartModel))]
        [AcceptVerbs("GET","POST")]
        [Route("api/MISReportingBarChart")]
        public HttpResponseMessage Get(int TransactionType, int Segmentation, int Channel, int Branch, DateTime StartDate, DateTime EndDate, DateTime ClientDateTime)
        {
        
            IList<MISReportingBarChartModel> Output = new List<MISReportingBarChartModel>();
            if (!repositoryMISReportingBarChart.GetMISReportingBarChart(TransactionType, Segmentation, Channel, Branch, StartDate, EndDate, ClientDateTime, ref Output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<MISReportingBarChartModel>>(HttpStatusCode.OK, Output);
            }  
            
        }

    }
}
