﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Tunning_HomeWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.Tuning.Tunning_HomeWP.Tunning_HomeWPUserControl" %>


<style>
    #transaction-data > .row > .col-xs-10 {
        margin-left: 60px;
    }

    .form-line-custom {
        padding-top: 4px;
        padding-bottom: 4px;
        border-bottom: 1px solid #EEE;
    }
</style>
<link rel="stylesheet" href="/_catalogs/masterpage/Ace/assets/css/bootstrap-timepicker.css" />
<link rel="stylesheet" href="/SiteAssets/Scripts/w2ui/w2ui-1.5.rc1.min.css" />
<h1 class="header smaller no-margin-top lighter dark">Home</h1>

<div id="home-transaction">

    <!-- widget box start -->
    <div id="widget-box" class="widget-box">
        <!-- widget header start -->
        <div class="widget-header widget-hea1der-small header-color-dark">
            <h6>Incoming Tasks</h6>

            <div class="widget-toolbar">
                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                    <i class="blue icon-filter"></i>
                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridProperties().AllowFilter" />
                    <span class="lbl"></span>
                </label>
                <a href="#" data-bind="click: GetData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                    <i class="blue icon-refresh"></i>
                </a>
            </div>
        </div>
        <!-- widget header end -->
        <!-- widget body start -->
        <div class="widget-body">
            <!-- widget main start -->
            <div class="widget-main padding-0">
                <!-- widget slim control start -->
                <div class="slim-scroll" data-height="400">
                    <!-- widget content start -->
                    <div class="content">

                        <!-- table responsive start -->
                        <div class="table-responsive" id="form-menu-custom" form="Home">
                            <div class="dataTables_wrapper" role="grid">
                                <table id="workflow-task-table" class="table table-striped table-bordered table-hover dataTable"
                                    workflow-state="running"
                                    workflow-outcome="pending"
                                    workflow-custom-outcome=""
                                    workflow-show-active-task="true"
                                    workflow-show-contribute="false">
                                    <thead>
                                        <tr data-bind="with: GridProperties">
                                            <th>No.</th>
                                            <th data-bind="click: function () { Sorting('BranchCode') }, css: GetSortedColumn('BranchCode')">Branch Code</th>
                                            <th data-bind="click: function () { Sorting('BranchName') }, css: GetSortedColumn('BranchName')">User Location</th>
                                            <th data-bind="click: function () { Sorting('ApplicationID') }, css: GetSortedColumn('ApplicationID')">Application ID</th>
                                            <th data-bind="click: function () { Sorting('Customer') }, css: GetSortedColumn('Customer')">Customer Name</th>
                                            <th data-bind="click: function () { Sorting('Product') }, css: GetSortedColumn('Product')">Product</th>
                                            <th data-bind="click: function () { Sorting('Currency') }, css: GetSortedColumn('Currency')">Currency</th>
                                            <th data-bind="click: function () { Sorting('Amount') }, css: GetSortedColumn('Amount')">Trxn Amount</th>
                                            <th data-bind="click: function () { Sorting('AccountNumber') }, css: GetSortedColumn('AccountNumber')">Debit Acc Number</th>
                                            <th data-bind="click: function () { Sorting('IsFXTransaction') }, css: GetSortedColumn('IsFXTransaction')">FX Transaction</th>
                                            <th data-bind="click: function () { Sorting('IsTopUrgent') }, css: GetSortedColumn('IsTopUrgent')">Urgency</th>
                                            <th data-bind="click: function () { Sorting('TransactionStatus') }, css: GetSortedColumn('TransactionStatus')">Transaction Status</th>
                                            <th data-bind="click: function () { Sorting('LastModifiedBy') }, css: GetSortedColumn('LastModifiedBy')">Modified By</th>
                                            <th data-bind="click: function () { Sorting('LastModifiedDate') }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
                                        </tr>
                                    </thead>
                                    <thead data-bind="visible: GridProperties().AllowFilter" class="table-filter">
                                        <tr>
                                            <th class="clear-filter">
                                                <a href="#" data-bind="click: ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                    <i class="green icon-trash"></i>
                                                </a>
                                            </th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterBranchCode, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterBranchName, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterApplicationID, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCustomer, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterProduct, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCurrency, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterAmount, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterDebitAccNumber, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterFXTransaction, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterTopUrgent, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterTransactionStatus, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterUser, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterEntryTime, event: { change: GridProperties().Filter }" /></th>
                                        </tr>
                                    </thead>
                                    <tbody data-bind="foreach: Tasks">
                                        <tr data-bind="click: $root.GetSelectedRow" data-toggle="modal">
                                            <td><span data-bind="text: RowID"></span></td>
                                            <td><span data-bind="text: Transaction.BranchCode"></span></td>
                                            <td><span data-bind="text: Transaction.BranchName"></span></td>
                                            <td style="white-space: nowrap"><span data-bind="text: Transaction.ApplicationID"></span></td>
                                            <td><span data-bind="text: Transaction.Customer"></span></td>
                                            <td><span data-bind="text: Transaction.Product"></span></td>
                                            <td><span data-bind="text: Transaction.Currency"></span></td>
                                            <td align="right"><span data-bind="text: formatNumber(Transaction.Amount)"></span></td>
                                            <td><span data-bind="text: Transaction.DebitAccNumber"></span></td>
                                            <td><span data-bind="text: Transaction.IsFXTransactionValue"></span></td>
                                            <td><span data-bind="text: Transaction.IsTopUrgentValue"></span></td>
                                            <td><span data-bind="text: Transaction.TransactionStatus"></span></td>
                                            <td><span data-bind="text: Transaction.LastModifiedBy"></span></td>
                                            <td><span data-bind="text: $root.LocalDate(Transaction.LastModifiedDate)"></span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- table responsive end -->
                    </div>
                    <!-- widget content end -->
                </div>
                <!-- widget slim control end -->
                <!-- widget footer start -->
                <div class="widget-toolbox padding-8 clearfix">
                    <div class="row" data-bind="with: GridProperties">
                        <!-- pagination size start -->
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                Showing
                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                rows of <span data-bind="text: Total"></span>
                                entries
                            </div>
                        </div>
                        <!-- pagination size end -->
                        <!-- pagination page jump start -->
                        <div class="col-sm-3">
                            <div class="dataTables_paginate paging_bootstrap">
                                Page
                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                of <span data-bind="text: TotalPages"></span>
                            </div>
                        </div>
                        <!-- pagination page jump end -->
                        <!-- pagination navigation start -->
                        <div class="col-sm-3">
                            <div class="dataTables_paginate paging_bootstrap">
                                <ul class="pagination">
                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                            <i class="icon-double-angle-left"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                            <i class="icon-angle-left"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                            <i class="icon-angle-right"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                            <i class="icon-double-angle-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- pagination navigation end -->

                    </div>
                </div>
                <!-- widget footer end -->

            </div>
            <!-- widget main end -->
        </div>
        <!-- widget body end -->
    </div>
    <!-- widget box end -->
    <!-- modal form start -->
    <div id="modal-form" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 100%;">
            <div id="backDrop" class="absCustomBackDrop topCustomBackDrop darkCustomBackDrop hideCustomBackDrop fullHeightCustomBackDrop fullWidthCustomBackDrop centerCustomBackDrop"></div>
            <div class="modal-content">
                <!-- modal header start
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="blue bigger">
                        <span data-bind="text: $root.Title"></span>
                    </h4>
                </div> -->
                <div class="alert alert-danger" data-bind="visible: !$root.IsAuthorizedNintex()">
                    <!-- <button type="button" class="close" data-dismiss="alert">
                        <i class="icon-remove"></i>
                    </button> -->
                    <strong>
                        <i class="icon-remove"></i>
                        Unauthorized!
                    </strong>
                    <span data-bind="text: $root.MessageNintex"></span>
                </div>
                <div class="alert alert-danger" data-bind="visible: $root.IsAuthorizedNintex() && !$root.IsNotInitiator()">
                    <span>You can't complete this task because you are creator of this transaction</span>
                </div>
                <!-- modal header end -->
                <!-- modal body start -->
                <div class="modal-body overflow-visible">
                    <button type="button" class="bootbox-close-button close" data-dismiss="modal" style="margin-top: -10px; font-size: 30px; margin-right: -8px;" data-bind="click: $root.OnCloseApproval">×</button>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div class="tabbable">
                                <ul class="nav nav-tabs" id="myTab">
                                    <li class="active">
                                        <a data-toggle="tab" href="#transaction">
                                            <i class="blue icon-credit-card bigger-110"></i>
                                            <span data-bind="text: ActivityTitle"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#history">
                                            <i class="blue icon-comments bigger-110"></i>
                                            Workflow History
                                        </a>
                                    </li>
                                    <%-- add by fandi transaction history --%>
                                    <li class="TransHistory">
                                        <a data-toggle="tab" href="#Transactionhistory">
                                            <i class="blue icon-list bigger-110"></i>
                                            Transaction History
                                        </a>
                                    </li>
                                    <%-- end  --%>
                                </ul>

                                <div class="tab-content">
                                    <div id="transaction" class="tab-pane in active">
                                        <div id="transaction-progress" class="col-sm-4 col-sm-offset-4" style="display: none">
                                            <h4 class="smaller blue">Please wait...</h4>
                                            <span class="space-4"></span>
                                            <div class="progress progress-small progress-striped active">
                                                <div class="progress-bar progress-bar-info" style="width: 100%"></div>
                                            </div>
                                        </div>

                                        <div id="transaction-data" data-bind="template: { name: 'CommonTemplate', data: ApprovalData() }"></div>
                                    </div>

                                    <div id="history" class="tab-pane">
                                        <div id="timeline-2" data-bind="with: ApprovalData()">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                                                    <div class="timeline-container timeline-style2">
                                                        <!--<span class="timeline-label">
                                                            <b>Today</b>
                                                        </span>-->

                                                        <div data-bind="if: $root.IsTimelines">

                                                            <div class="timeline-items" data-bind="foreach: Timelines">
                                                                <div class="timeline-item clearfix">
                                                                    <div class="timeline-info">
                                                                        <span class="timeline-date" data-bind="text: $root.LocalDate(Time)"></span>

                                                                        <i class="timeline-indicator btn btn-info no-hover"></i>
                                                                    </div>

                                                                    <div class="widget-box transparent">
                                                                        <div class="widget-body">
                                                                            <div class="widget-main no-padding">
                                                                                <span class="bigger-110">
                                                                                    <span class="blue bolder" data-bind="text: DisplayName"></span>
                                                                                    <!-- ko if: IsGroup -->
                                                                                    (<span data-bind="text: UserOrGroup"></span>)
                                                                                    <!-- /ko -->
                                                                                </span>

                                                                                <span class="bolder" data-bind="text: Outcome"></span>
                                                                                <span data-bind="text: Activity"></span>

                                                                                <!-- ko ifnot: ApproverID == 0 -->
                                                                                <br />
                                                                                <i class="icon-comments-alt bigger-110 blue"></i>
                                                                                <span data-bind="text: Comment"></span>
                                                                                <!-- /ko -->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <%-- add by fandi --%>
                                    <div id="Transactionhistory" class="tab-pane">
                                        <div id="transaction-form2" class="form-horizontal" role="form" data-bind="if: TransactionHistory().Transaction != null || TransactionHistory().Payment != null">

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label bolder no-padding-right">
                                                    <span data-bind="text: $root.LocalDate($root.CreateDateHeader, true, true)"></span>
                                                </label>

                                                <label class="col-sm-4 pull-right">
                                                    <b>Application ID</b> : <span data-bind="text: $root.ApplicationIDHeader"></span>
                                                    <br />
                                                    <b>User Name</b> : <span data-bind="text: $root.SPUser.DisplayName"></span>
                                                </label>
                                            </div>

                                            <div class="form-group form-line no-margin-bottom no-padding-bottom">
                                                <label class="col-sm-2 control-label bolder no-padding-right" for="customer-name">Customer Name</label>

                                                <label class="col-sm-8">
                                                    <span name="customer-name" id="customer-name" data-bind="text: $root.CustomerNameHeader"></span>
                                                    <span class="label label-warning arrowed" data-bind="if: $root.IsNewCustomerHeader">
                                                        <i class="icon-warning-sign bigger-120"></i>
                                                        New Customer
                                                    </span>
                                                </label>
                                            </div>

                                            <div class="form-group form-line no-margin-bottom no-padding-bottom">
                                                <label class="col-sm-2 control-label bolder no-padding-right" for="cif">CIF</label>

                                                <label class="col-sm-9">
                                                    <span name="cif" id="cif" data-bind="text: $root.CIFHeaderHistory"></span>
                                                </label>
                                            </div>

                                            <div class="form-group form-line no-margin-bottom no-padding-bottom">
                                                <label class="col-sm-2 control-label bolder no-padding-right" for="product-code">Product</label>

                                                <label class="col-sm-9">
                                                    <span id="product-code" name="product" data-bind="text: $root.ProductCodeHeader"></span>
                                                    (<span id="product-name" name="product" data-bind="text: $root.ProductNamerHeader"></span>)
                                                </label>
                                            </div>

                                            <div class="form-group form-line no-margin-bottom no-padding-bottom">
                                                <label class="col-sm-2 control-label bolder no-padding-right" for="currency-code">Currency</label>

                                                <label class="col-sm-8">
                                                    <span id="currency-code" name="currency" data-bind="text: $root.CurrencyCodeHeader"></span>
                                                    (<span id="currency-desc" name="currency" data-bind="text: $root.CurrencyDescriptionHeader"></span>) 
                                                </label>
                                            </div>

                                            <div class="form-group form-line no-margin-bottom no-padding-bottom">
                                                <label class="col-sm-2 control-label bolder no-padding-right" for="channel">Channel</label>

                                                <label class="col-sm-8">
                                                    <span id="channel" name="channel" data-bind="text: $root.ChannelNameHeader"></span>
                                                </label>
                                            </div>

                                            <div class="form-group form-line no-margin-bottom no-padding-bottom">
                                                <label class="col-sm-2 control-label bolder no-padding-right" for="application-date">Application Date</label>

                                                <label class="col-sm-9">
                                                    <span id="application-date" name="application-date" data-bind="text: $root.ApplicationDateHeader"></span>
                                                </label>
                                            </div>

                                            <div class="form-group form-line no-margin-bottom no-padding-bottom">
                                                <label class="col-sm-2 control-label bolder no-padding-right" for="biz-segment-code">Biz Segment</label>

                                                <label class="col-sm-8">
                                                    <span id="biz-segment-code" name="biz-segment" data-bind="text: $root.BizSegmentDescriptionDescHeader"></span>
                                                    (<span id="biz-segment-desc" name="biz-segment" data-bind="text: $root.BizSegmentNameDescHeader"></span>)
                                                </label>
                                            </div>

                                            <div class="space-6"></div>

                                            <div class="dataTables_wrapper" role="grid" style="overflow-x: auto">
                                                <table class="table table-striped table-bordered table-hover dataTable">
                                                    <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Trxn Amount</th>
                                                            <th>Rate</th>
                                                            <th>Eqv. USD</th>
                                                            <th>Debit Acc Number</th>
                                                            <th>Debit Acc CCY</th>
                                                            <th>Debit Sundry</th>
                                                            <th>Bene Name</th>
                                                            <th>Bene Bank</th>
                                                            <th>Bene Acc Number</th>
                                                            <th>Swift Code/ Bank Code</th>
                                                            <th>Bank Charges</th>
                                                            <th>Agent Charges</th>
                                                            <th>Citizen</th>
                                                            <th>Resident</th>
                                                            <th>LLD Code</th>
                                                            <th>Payment Details</th>
                                                            <th>Modifed By</th>
                                                            <th>Modifed Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody data-bind="foreach: $root.TransactionHistory().TransactionMaker, visible: TransactionHistory().TransactionMaker.length > 0">
                                                        <tr>
                                                            <td><span data-bind="text: $index() + 1"></span></td>
                                                            <td align="right"><span data-bind="text: formatNumber_r(Amount)"></span></td>
                                                            <td align="right"><span data-bind="text: formatNumber_r(Rate)"></span></td>
                                                            <td align="right"><span data-bind="text: formatNumber_r(AmountUSD)"></span></td>
                                                            <td><span data-bind="text: DebitAccNumber"></span></td>
                                                            <td><span data-bind="text: CurrencyCode"></span></td>
                                                            <td><span data-bind="text: DebitSundry"></span></td>
                                                            <td><span data-bind="text: BeneName"></span></td>
                                                            <td><span data-bind="text: BeneBankName"></span></td>
                                                            <td><span data-bind="text: BeneAccNumber"></span></td>
                                                            <td><span data-bind="text: BeneBankSwift"></span></td>
                                                            <td align="center"><span data-bind="text: BankChargesCode"></span></td>
                                                            <td align="center"><span data-bind="text: AgentChargesCode"></span></td>
                                                            <td align="center"><span data-bind="text: IsCitizen ? 'Yes' : 'No'"></span></td>
                                                            <td align="center"><span data-bind="text: IsResident ? 'Yes' : 'No'"></span></td>
                                                            <td><span data-bind="text: LLDDescription"></span></td>
                                                            <td><span data-bind="text: PaymentDetails"></span></td>
                                                            <td><span data-bind="text: LastModifiedBy"></span></td>
                                                            <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- end by fandi --%>

                                    <div id="context" class="tab-pane">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Parameter</th>
                                                    <th>Value</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Web ID</td>
                                                    <td><span data-bind="text: SPWebID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Site ID</td>
                                                    <td><span data-bind="text: SPSiteID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>List ID</td>
                                                    <td><span data-bind="text: SPListID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>List Item ID</td>
                                                    <td><span data-bind="text: SPListItemID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Task List ID</td>
                                                    <td><span data-bind="text: SPTaskListID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Task List Item ID</td>
                                                    <td><span data-bind="text: SPTaskListItemID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Initiator</td>
                                                    <td><span data-bind="text: Initiator"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Workflow Instance ID</td>
                                                    <td><span data-bind="text: WorkflowInstanceID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Workflow ID</td>
                                                    <td><span data-bind="text: WorkflowID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Workflow Name</td>
                                                    <td><span data-bind="text: WorkflowName"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Start Time</td>
                                                    <td><span data-bind="text: LocalDate(StartTime())"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>State ID </td>
                                                    <td><span data-bind="text: StateID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>State Description</td>
                                                    <td><span data-bind="text: StateDescription"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Is Authorized</td>
                                                    <td><span data-bind="text: IsAuthorized"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Task Type</td>
                                                    <td><span data-bind="text: TaskTypeDescription"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Title</td>
                                                    <td><span data-bind="text: Title"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Activity Title</td>
                                                    <td><span data-bind="text: ActivityTitle"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>User</td>
                                                    <td><span data-bind="text: User"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Is Sharepoint Group</td>
                                                    <td><span data-bind="text: IsSPGroup"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Entry Time</td>
                                                    <td><span data-bind="text: LocalDate(EntryTime())"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Exit Time</td>
                                                    <td><span data-bind="text: LocalDate(ExitTime())"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Outcome ID</td>
                                                    <td><span data-bind="text: OutcomeID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Outcome Description</td>
                                                    <td><span data-bind="text: OutcomeDescription"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Custom Outcome ID</td>
                                                    <td><span data-bind="text: CustomOutcomeID"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Custom Outcom Description</td>
                                                    <td><span data-bind="text: CustomOutcomeDescription"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Comments</td>
                                                    <td><span data-bind="text: Comments"></span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div id="ko-model" class="tab-pane">
                                        <pre data-bind="text: ko.toJSON($root.ApprovalData, null, 2)"></pre>
                                        <!--<pre data-bind="text: JSON.stringify(ko.toJS(ApprovalData), null, 2)"></pre>-->
                                    </div>
                                </div>
                            </div>
                            <!-- modal body form end -->
                        </div>
                    </div>
                </div>
                <!-- modal body end -->
                <!-- modal footer start -->
                <div class="modal-footer">
                    <div class="row" data-bind="visible: $root.IsAuthorizedNintex()">
                        <div class="form-group">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="workflow-comments">Comments</label>

                            <div class="col-xs-9 align-left">
                                <div class="clearfix">
                                    <textarea class="col-lg-12" id="workflow-comments" name="workflow-comments" rows="4" data-bind="value: $root.Comments, enable: $root.IsPendingNintex()" placeholder="Type any comment here..."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="space-8"></div>

                    <!-- dynamic outcomes button start -->
                    <span data-bind="foreach: Outcomes, visible: $root.IsAuthorizedNintex()">
                        <button data-bind="click: $root.ApprovalProcess, enable: $root.IsPendingNintex(), visible: $root.IsNotInitiator(), visible: $root.IsTaskOpened()" class="btn btn-sm btn-primary">
                            <i class="icon-approve"></i>
                            <span data-bind="text: $root.ApprovalButton(Name)"></span>
                        </button>
                    </span>
                    <!-- dynamic outcomes button end -->
                    <button class="btn btn-sm btn-success" data-dismiss="modal" data-bind="click: $root.PrintPage, visible: $root.IsPrintVisible()">
                        <i class="icon-approve"></i>
                        Print
                    </button>
                    <button class="btn btn-sm btn-success" data-dismiss="modal" data-bind="click: $root.OnCloseApproval">
                        <i class="icon-remove"></i>
                        Close
                    </button>
                </div>
                <!-- modal footer end -->

            </div>
        </div>
    </div>
    <!-- modal form end -->
    <!-- modal form child -->
    <div id="modal-form-child" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="0" role="dialog">
        <div class="modal-dialog" style="width: 80%">
            <div class="modal-content">

                <!-- modal header start -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span data-bind="if: $root.IsNewData()">Create a new Customer Contact Parameter</span>
                        <span data-bind="if: !$root.IsNewData()">Modify Customer Contact Parameter</span>
                    </h4>
                </div>
                <!-- modal header end -->
                <!-- modal body start -->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div id="customer-form" class="form-horizontal" role="form">

                                <div class="form-group" data-bind="visible: false">
                                    <label class="col-sm-3 control-label no-padding-right" for="id">
                                        ID
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="id" name="id" data-bind="value: $root.ID_c" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="Name">
                                        Contact Name
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="Name" name="Name" autocomplete="off" data-bind="value: $root.Name_c" class="col-xs-10 col-sm-5" data-rule-required="true" />
                                            <label class="control-label bolder text-danger" for="currency">*</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="PhoneNumber">
                                        Phone Number
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="PhoneNumber" name="PhoneNumber" autocomplete="off" data-bind="value: $root.PhoneNumber_c" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="DateOfBirth">
                                        Date of Birth
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix input-group col-sm-3 no-padding">
                                            <input class="form-control date-picker" type="text" data-date-format="dd-M-yyyy" id="DateOfBirth" name="DateOfBirth" data-bind="value: $root.DateOfBirth_c" data-rule-value="true" />
                                            <span class="input-group-addon">
                                                <i class="icon-calendar bigger-110"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="IDNumber">
                                        IDNumber
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="IDNumber" name="IDNumber" autocomplete="off" data-bind="value: $root.IDNumber_c" class="col-xs-10 col-sm-5" data-rule-required="true" data-rule-number="true" />
                                            <label class="control-label bolder text-danger" for="currency">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="poafunction">
                                        POA Function
                                    </label>
                                    <div class="col-sm-8">
                                        <div class="clearfix">
                                            <select class="col-sm-11" id="poa-function" name="poa-function" data-rule-required="true" data-rule-value="true"
                                                data-bind="options: $root.POAFunctions,
    optionsText: function (item) { if (item.Name != null) return item.Name + ' (' + item.Description + ')' },
    optionsValue: 'ID',
    optionsCaption: 'Please Select...',
    value: $root.Selected().POAFunction,
    event: {
        change: function () {
            var data = ko.utils.arrayFirst($root.POAFunctions(),
                function (item) {
                    return item.ID == $root.Selected().POAFunction();
                });
            if (data != undefined && data != null) { $root.POAFunction_c(data); }
        }
    }">
                                            </select><label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" data-bind="visible: $root.Selected().POAFunction == 9">
                                    <label class="col-sm-3 control-label no-padding-right" for="POAFunction">
                                        POA Function Other
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="functionOther" name="functionOther" autocomplete="off" data-bind="value: $root.POAFunctionOther_c" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="OccupationInID">
                                        OccupationIn ID
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="OccupationInID" name="OccupationInID" autocomplete="off" data-bind="value: $root.OccupationInID_c" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="PlaceofBirth">
                                        Place of Birth
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="PlaceOfBirth" name="PlaceOfBirth" autocomplete="off" data-bind="value: $root.PlaceOfBirth_c" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="EffectiveDate">
                                        Effective Date
                                    </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix input-group col-sm-3 no-padding">
                                            <input class="form-control date-picker" type="text" data-date-format="dd-M-yyyy" id="EffectiveDate" name="EffectiveDate" data-bind="value: $root.EffectiveDate_c" />
                                            <span class="input-group-addon">
                                                <i class="icon-calendar bigger-110"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="CancellationDate">
                                        Cancellation Date
                                    </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix input-group col-sm-3 no-padding">
                                            <input class="form-control date-picker" type="text" data-date-format="dd-M-yyyy" id="cancellationDate" name="cancellationDate" data-bind="value: $root.CancellationDate_c" />
                                            <span class="input-group-addon">
                                                <i class="icon-calendar bigger-110"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- modal body form end -->
                        </div>
                    </div>
                </div>
                <!-- modal body end -->
                <!-- modal footer start -->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.save_c, visible: $root.IsNewData()">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.update_c, visible: !$root.IsNewData()">
                        <i class="icon-edit"></i>
                        Update
                    </button>
                    <button class="btn btn-sm btn-warning" data-bind="click: $root.delete_c, visible: !$root.IsNewData()">
                        <i class="icon-trash"></i>
                        Delete
                    </button>
                    <button class="btn btn-sm" data-bind="click: $root.Close">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
                <!-- modal footer end -->

            </div>
        </div>
    </div>
    <!-- modal form child end-->

    <!-- modal form start Underlying Form -->
    <div id="modal-form-Underlying" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 95%">
            <div class="modal-content">
                <!-- modal header start Underlying Form -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span>Modify a Customer Underlying Parameter</span>
                    </h4>
                </div>
                <!-- modal header end -->
                <!-- modal body start -->
                <div class="modal-body overflow-visible">
                    <div id="modalUnderlyingtest">
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- modal body form start -->

                                <div id="customerUnderlyingform" class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="JointAccountID">
                                            Joint Account</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="book-joinAcc" name="book-statement" data-bind="value: $root.IsJointAccount_u,
    options: $root.CIFJointAccounts,
    optionsText: 'Name',
    optionsValue: 'ID' "
                                                    class="col-xs-3" disabled="disabled">
                                                </select>&nbsp;
                                                <input type="text" id="accountNumber" name="documentType" data-bind="value: $root.AccountNumber_u, visible: $root.IsJointAccount_u" class="col-xs-3" disabled="disabled" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="StatementLetterID">
                                            Statement Letter
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="statementLetter" name="statementLetter" data-bind="value: $root.StatementLetter_u().ID, options: $root.StatementLetters, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7" disabled="disabled"></select>
                                                <label class="control-label bolder text-danger" for="StatementLetter_u">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="UnderlyingDocument">
                                            Underlying Document
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="underlyingDocument" name="underlyingDocument" data-bind="value: $root.UnderlyingDocument_u().ID, options: $root.UnderlyingDocs, optionsText: function (item) { if (item.ID != null) return item.Code + ' (' + item.Name + ')' }, optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger" for="UnderlyingDocument_u">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div data-bind="visible: UnderlyingDocument_u().ID == '50'" class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="otherUnderlying">
                                            Other Underlying
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" id="otherUnderlying" autocomplete="off" name="otherUnderlying" data-bind="value: $root.OtherUnderlying_u" class="col-xs-7" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="DocumentType">
                                            Type of Document
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="documentType" name="documentType" data-bind="value: $root.DocumentType_u().ID, options: $root.DocumentTypes, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger" for="DocumentType_u">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Currency">
                                            Transaction Currency
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="currency_u" name="currency_u" data-bind="value: $root.Currency_u().ID, disable: $root.StatementLetter_u().ID == 1, options: $root.Currencies, optionsText: 'Code', optionsValue: 'ID', optionsCaption: 'Please Select...', event: { change: function (data) { OnCurrencyChange(Currency_u().ID) } }, disable: true" data-rule-required="true" data-rule-value="true" class="col-xs-2"></select>
                                                <label class="control-label bolder text-danger" for="Currency_u">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Amount">
                                            Invoice Amount
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" id="Amount_u" name="Amount_u" autocomplete="off" data-in="" onkeyup="OnKeyUp('AmountUnderlying')" data-bind="value: $root.Amount_u(), enable: $root.EditUnderlying" data-rule-required="true" class="col-xs-4 align-right" />
                                                <%--$root.StatementLetter_u().ID == 1--%>
                                                <label class="control-label bolder text-danger" for="Amount_u">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label bolder no-padding-right" for="rate">
                                            Rate</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" class="col-sm-4 align-right" data-in="" name="rate_u" id="rate_u" disabled="disabled" data-rule-required="true" data-rule-number="true" data-bind="value: formatNumber($root.Rate_u())" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label bolder no-padding-right" for="eqv-usd">
                                            Eqv. USD</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" class="col-sm-4 align-right" disabled="disabled" name="eqv-usd_u" id="eqv-usd_u" data-rule-required="true" data-rule-number="true" data-bind="value: formatNumber($root.AmountUSD_u())" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="DateOfUnderlying">
                                            Date of Underlying
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <input type="text" id="DateOfUnderlying" name="DateOfUnderlying_u" data-bind="value: DateOfUnderlying_u" autocomplete="off" data-date-format="dd-M-yyyy" class="form-control date-picker col-xs-6" data-rule-required="true" />
                                                <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                <label class="control-label bolder text-danger starremove" for="DateOfUnderlying_u" style="position: absolute;">*</label>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label no-padding-right" for="ExpiredDate">
                                            Expiry Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <input type="text" id="ExpiredDate" name="ExpiredDate" data-bind="value: ExpiredDate_u" autocomplete="off" data-date-format="dd-M-yyyy" class="form-control date-picker col-xs-6" data-rule-required="true" />
                                                <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                <label class="control-label bolder text-danger starremove" for="ExpiredDate_u" style="position: absolute;">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" data-bind="visible: false">
                                        <label class="col-sm-3 control-label no-padding-right" for="IsDeclarationOfException">
                                            Declaration of Exception
                                        </label>
                                        <div class="col-sm-9">
                                            <label>
                                                <input name="switch-field-1" id="IsDeclarationOfException" data-bind="checked: $root.IsDeclarationOfException_u" class="ace ace-switch ace-switch-6" type="checkbox" />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group" data-bind="visible: false">
                                        <label class="col-sm-3 control-label no-padding-right" for="StartDate">
                                            Start Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <input id="StartDate" name="StartDate" data-date-format="dd-M-yyyy" data-bind="value: StartDate_u" class="form-control date-picker col-xs-6" type="text" />
                                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label no-padding-right" for="EndDate">
                                            End Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <input type="text" id="EndDate" name="EndDate" data-date-format="dd-M-yyyy" data-bind="value: EndDate_u" class="form-control date-picker col-xs-6" />
                                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="ReferenceNumber">
                                            Reference Number
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" id="ReferenceNumber" name="ReferenceNumber" data-bind="value: $root.ReferenceNumber_u" disabled="disabled" class="col-xs-9" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="SupplierName">
                                            Supplier Name
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" id="SupplierName" name="SupplierName" data-bind="value: $root.SupplierName_u" autocomplete="off" class="col-xs-8" data-rule-required="true" />
                                                <label class="control-label bolder text-danger" for="SupplierName_u">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="InvoiceNumber">
                                            Invoice Number
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" id="invoiceNumber" name="invoiceNumber" data-bind="value: $root.InvoiceNumber_u" autocomplete="off" class="col-xs-8" data-rule-required="true" />
                                                <label class="control-label bolder text-danger" for="InvoiceNumber_u">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="AttachmentNo">
                                            Is This doc Purposed to replace the proforma doc
                                        </label>
                                        <div class="col-sm-9">
                                            <label>
                                                <input name="switch-field-1" id="IsProforma" data-bind="checked: IsProforma_u, disable: DocumentType_u().ID == 2" class="ace ace-switch ace-switch-6" type="checkbox" />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <!-- grid proforma begin -->

                                    <!-- widget box start -->
                                    <div id="widget-box" class="widget-box" data-bind="visible: IsProforma_u()">
                                        <!-- widget header start -->
                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                            <h6>Customer Underlying Proforma Table</h6>
                                            <div class="widget-toolbar">
                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                    <i class="blue icon-filter"></i>
                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingProformaGridProperties().AllowFilter" />
                                                    <span class="lbl"></span>
                                                </label>
                                                <a href="#" data-bind="click: GetDataUnderlyingProforma" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                    <i class="blue icon-refresh"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- widget header end -->
                                        <!-- widget body start -->
                                        <div class="widget-body">
                                            <!-- widget main start -->
                                            <div class="widget-main padding-0">
                                                <!-- widget slim control start -->
                                                <div class="slim-scroll" data-height="400">
                                                    <!-- widget content start -->
                                                    <div class="content">
                                                        <!-- table responsive start -->
                                                        <div class="table-responsive">
                                                            <div class="dataTables_wrapper" role="grid">
                                                                <table id="Customer Underlying-table2" class="table table-striped table-bordered table-hover dataTable">
                                                                    <thead>
                                                                        <tr data-bind="with: UnderlyingProformaGridProperties">
                                                                            <th style="width: 50px">No.</th>
                                                                            <th style="width: 50px">Select</th>
                                                                            <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                            <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                            <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                            <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                            <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                            <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                            <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                            <th data-bind="click: function () { Sorting('InvoiceNumber'); }, css: GetSortedColumn('InvoiceNumber')">Invoice Number</th>
                                                                            <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <thead data-bind="visible: UnderlyingProformaGridProperties().AllowFilter" class="table-filter">
                                                                        <tr>
                                                                            <th class="clear-filter">
                                                                                <a href="#" data-bind="click: ProformaClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                    <i class="green icon-trash"></i>
                                                                                </a>
                                                                            </th>
                                                                            <th class="clear-filter"></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterStatementLetter, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterUnderlyingDocument, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterDocumentType, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterCurrency, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterAmount, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.ProformaFilterDateOfUnderlying, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterSupplierName, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterInvoiceNumber, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.ProformaFilterExpiredDate, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody data-bind="foreach: { data: CustomerUnderlyingProformas, as: 'ProformasData' }, visible: CustomerUnderlyingProformas().length > 0">
                                                                        <tr data-toggle="modal">
                                                                            <td><span data-bind="text: RowID"></span></td>
                                                                            <td align="center">
                                                                                <input type="checkbox" id="isSelectedProforma" name="isSelect" data-bind="checked: ProformasData.IsSelectedProforma, event: { change: function (data) { $root.onSelectionProforma($index(), $data) } }" /></td>
                                                                            <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                            <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                            <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                            <td><span data-bind="text: Currency.Code"></span></td>
                                                                            <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                                                            <td><span data-bind="text: $root.LocalDate(DateOfUnderlying, true, false)"></span></td>
                                                                            <td><span data-bind="text: SupplierName"></span></td>
                                                                            <td><span data-bind="text: InvoiceNumber"></span></td>
                                                                            <td><span data-bind="text: $root.LocalDate(ExpiredDate, true, false)"></span></td>
                                                                        </tr>
                                                                    </tbody>
                                                                    <tbody data-bind="visible: CustomerUnderlyingProformas().length == 0">
                                                                        <tr>
                                                                            <td colspan="11" class="text-center">No entries available.
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <!-- table responsive end -->
                                                    </div>
                                                    <!-- widget content end -->
                                                </div>
                                                <!-- widget slim control end -->
                                                <!-- widget footer start -->
                                                <div class="widget-toolbox padding-8 clearfix">
                                                    <div class="row" data-bind="with: UnderlyingProformaGridProperties">
                                                        <!-- pagination size start -->
                                                        <div class="col-sm-6">
                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                Showing
                                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                rows of <span data-bind="text: Total"></span>
                                                                entries
                                                            </div>
                                                        </div>
                                                        <!-- pagination size end -->
                                                        <!-- pagination page jump start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                Page
                                                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                of <span data-bind="text: TotalPages"></span>
                                                            </div>
                                                        </div>
                                                        <!-- pagination page jump end -->
                                                        <!-- pagination navigation start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                <ul class="pagination">
                                                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                            <i class="icon-double-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                            <i class="icon-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                            <i class="icon-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                            <i class="icon-double-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <!-- pagination navigation end -->

                                                    </div>
                                                </div>
                                                <!-- widget footer end -->
                                            </div>
                                            <!-- widget main end -->
                                        </div>
                                        <!-- widget body end -->
                                    </div>
                                    <!-- widget box end -->
                                    <!-- grid proforma end -->
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="IsBulkUnderlying">
                                            Is This Bulk Underlying</label>
                                        <div class="col-sm-9">
                                            <label>
                                                <input name="switch-field-1" id="IsBulkUnderlying" data-bind="checked: IsBulkUnderlying_u" class="ace ace-switch ace-switch-6" type="checkbox" />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <!-- grid Bulk begin -->
                                    <!-- widget box start -->
                                    <div class="widget-box" data-bind="visible: IsBulkUnderlying_u()">
                                        <!-- widget header start -->
                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                            <h6>Customer Bulk Underlying
													Table</h6>

                                            <div class="widget-toolbar">
                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                    <i class="blue icon-filter"></i>
                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingBulkGridProperties().AllowFilter" />
                                                    <span class="lbl"></span>
                                                </label>
                                                <a href="#" data-bind="click: GetDataBulkUnderlying" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                    <i class="blue icon-refresh"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- widget header end -->

                                        <!-- widget body start -->
                                        <div class="widget-body">
                                            <!-- widget main start -->
                                            <div class="widget-main padding-0">
                                                <!-- widget slim control start -->
                                                <div class="slim-scroll" data-height="400">
                                                    <!-- widget content start -->
                                                    <div class="content">

                                                        <!-- table responsive start -->
                                                        <div class="table-responsive">
                                                            <div class="dataTables_wrapper" role="grid">
                                                                <table id="Customer Underlying-table1" class="table table-striped table-bordered table-hover dataTable">
                                                                    <thead>
                                                                        <tr data-bind="with: UnderlyingBulkGridProperties">
                                                                            <th style="width: 50px">No.</th>
                                                                            <th style="width: 50px">Select</th>
                                                                            <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                            <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                            <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                            <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                            <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                            <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                            <th data-bind="click: function () { Sorting('InvoiceNumber'); }, css: GetSortedColumn('InvoiceNumber')">Invoice Number</th>
                                                                            <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                            <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <thead data-bind="visible: UnderlyingBulkGridProperties().AllowFilter" class="table-filter">
                                                                        <tr>
                                                                            <th class="clear-filter">
                                                                                <a href="#" data-bind="click: BulkClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                    <i class="green icon-trash"></i>
                                                                                </a>
                                                                            </th>
                                                                            <th class="clear-filter"></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterStatementLetter, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterUnderlyingDocument, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterDocumentType, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterCurrency, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterSupplierName, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterAmount, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterInvoiceNumber, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.BulkFilterDateOfUnderlying, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.BulkFilterExpiredDate, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody data-bind="foreach: { data: CustomerBulkUnderlyings, as: 'BulkDatas' }, visible: CustomerBulkUnderlyings().length > 0">
                                                                        <tr data-toggle="modal">
                                                                            <td><span data-bind="text: RowID"></span></td>
                                                                            <td align="center">
                                                                                <input type="checkbox" id="isSelectedBulk" name="isSelect" data-bind="checked: BulkDatas.IsSelectedBulk, event: { change: function (data) { $root.onSelectionBulk($index(), $data) } }" /></td>
                                                                            <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                            <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                            <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                            <td><span data-bind="text: Currency.Code"></span></td>
                                                                            <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                                                            <td><span data-bind="text: SupplierName"></span></td>
                                                                            <td><span data-bind="text: InvoiceNumber"></span></td>
                                                                            <td><span data-bind="text: $root.LocalDate(DateOfUnderlying, true, false)"></span></td>
                                                                            <td><span data-bind="text: $root.LocalDate(ExpiredDate, true, false)"></span></td>
                                                                        </tr>
                                                                    </tbody>
                                                                    <tbody data-bind="visible: CustomerBulkUnderlyings().length == 0">
                                                                        <tr>
                                                                            <td colspan="11" class="text-center">No entries available.
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <!-- table responsive end -->
                                                    </div>
                                                    <!-- widget content end -->
                                                </div>
                                                <!-- widget slim control end -->

                                                <!-- widget footer start -->
                                                <div class="widget-toolbox padding-8 clearfix">
                                                    <div class="row" data-bind="with: UnderlyingBulkGridProperties">
                                                        <!-- pagination size start -->
                                                        <div class="col-sm-6">
                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                Showing
                                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                rows of <span data-bind="text: Total"></span>
                                                                entries
                                                            </div>
                                                        </div>
                                                        <!-- pagination size end -->

                                                        <!-- pagination page jump start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                Page
                                                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                of <span data-bind="text: TotalPages"></span>
                                                            </div>
                                                        </div>
                                                        <!-- pagination page jump end -->

                                                        <!-- pagination navigation start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                <ul class="pagination">
                                                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                            <i class="icon-double-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                            <i class="icon-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                            <i class="icon-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                            <i class="icon-double-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <!-- pagination navigation end -->

                                                    </div>
                                                </div>
                                                <!-- widget footer end -->

                                            </div>
                                            <!-- widget main end -->
                                        </div>
                                        <!-- widget body end -->

                                    </div>
                                    <!-- widget box end -->
                                    <!-- grid Bulk end -->

                                    <!-- modal body form end -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal body end -->
                </div>
                <!-- modal footer start -->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.update_u, visible: !$root.IsUtilize_u()">
                        <i class="icon-edit"></i>
                        Update
                    </button>
                    <button class="btn btn-sm btn-warning" data-bind="click: $root.delete_u, visible: !$root.IsUtilize_u()">
                        <i class="icon-trash"></i>
                        Delete
                    </button>
                    <button class="btn btn-sm" data-bind="click: $root.cancel_u" data-dismiss="modal">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
                <!-- modal footer end -->
            </div>
        </div>
    </div>
    <!-- modal form end Underlying Form -->
    <!-- End -->
    <div id="modal-form-UnderlyingTMO" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">

        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <div class="modal-body overflow-visible">
                    <div id="modalUnderlyingtmo">
                        <div class="row">
                            <div class="col-xs-12">
                                <div id="customerUnderlyingformtmo" class="form-horizontal" role="form">
                                    <h3 class="header smaller lighter dark">Underlying</h3>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label bolder no-padding-right" for="eqv-usd">DF Amount</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" class="col-sm-3 align-right" name="dfamount2" id="dfamount2" disabled="disabled" data-bind="value: $root.DFAmount()" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label bolder no-padding-right" for="eqv-usd">Total Utilizations</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" class="col-sm-3 align-right" name="utilizations" id="utilizations" disabled="disabled" data-bind="value: $root.TotalUtilization()" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label bolder no-padding-right" for="eqv-usd">Rounding</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" class="col-sm-3 align-right" name="utilizations" id="utilizations" disabled="disabled" data-bind="value: $root.TotalUtilization()" />
                                            </div>
                                        </div>
                                    </div>
                                    <h3 class="header smaller lighter dark">Un-utilize Underlying Documents<button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.NewDataUnderlyingTMO"><i class="icon-plus"></i>Add Underlying</button></h3>                              
                                        <!-- widget box start -->
                                    <div id="widget-box" class="widget-box">
                                        <!-- widget header start -->
                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                            <h6>Underlying Table</h6>

                                            <div class="widget-toolbar">
                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                    <i class="blue icon-filter"></i>
                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: $root.UnderlyingGridProperties().AllowFilter" />

                                                    <span class="lbl"></span>
                                                </label>
                                                <a href="#" data-bind="click: $root.GetDataUnderlying" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                    <i class="blue icon-refresh"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- widget header end -->
                                        <!-- widget body start -->
                                        <div class="widget-body">
                                            <!-- widget main start -->
                                            <div class="widget-main padding-0">
                                                <!-- widget slim control start -->
                                                <div class="slim-scroll" data-height="200">
                                                    <!-- widget content start -->
                                                    <div class="content">

                                                        <!-- table responsive start -->
                                                        <div class="table-responsive">
                                                            <div class="dataTables_wrapper" role="grid">
                                                                <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                                    <thead>
                                                                        <tr data-bind="with: $root.UnderlyingGridProperties">
                                                                            <th style="width: 50px">No.</th>
                                                                            <th style="width: 50px">Utilize</th>
                                                                            <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                            <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                            <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                            <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                            <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                            <th data-bind="click: function () { Sorting('AvailableAmount'); }, css: GetSortedColumn('AvailableAmount')">Available Amount in USD</th>
                                                                            <th data-bind="click: function () { Sorting('AvailableAmount'); }, css: GetSortedColumn('AvailableAmount')">Utilize Amount Deal</th>
                                                                            <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                            <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                            <th data-bind="click: function () { Sorting('AttachmentNo'); }, css: GetSortedColumn('AttachmentNo')">Attachment No</th>
                                                                            <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th>
                                                                            <th style="width: 50px">Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <thead data-bind="visible: $root.UnderlyingGridProperties().AllowFilter" class="table-filter">
                                                                        <tr>
                                                                            <th class="clear-filter">
                                                                                <a href="#" data-bind="click: $root.UnderlyingClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                    <i class="green icon-trash"></i>
                                                                                </a>
                                                                            </th>
                                                                            <th class="clear-filter"></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterStatementLetter, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterUnderlyingDocument, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterDocumentType, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterCurrency, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 input-numericonly" data-bind="value: $root.UnderlyingFilterAmount, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 input-numericonly" data-bind="value: $root.UnderlyingFilterAvailableAmount, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 input-numericonly" data-bind="value: $root.UnderlyingFilterAvailableAmount, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.UnderlyingFilterDateOfUnderlying, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterSupplierName, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterAttachmentNo, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterReferenceNumber, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                                            <th class="clear-filter"></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody data-bind="foreach: $root.CustomerUnderlyingsTMOALL(), visible: $root.CustomerUnderlyingsTMOALL().length > 0">
                                                                        <tr>
                                                                            <td><span data-bind="text: $index() + 1"></span></td>
                                                                            <td align="center">
                                                                                <input type="checkbox" id="isUtilize" name="isUtilize" data-bind="checked: IsEnable, event: { change: function (data) { $root.onSelectionUtilizeAll($index(), $data) } }" /></td>
                                                                            <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                            <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                            <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                            <td><span data-bind="text: Currency.Code"></span></td>
                                                                            <td><span data-bind="text: formatNumber(Amount)"></span></td>
                                                                            <td><span data-bind="text: formatNumber(AvailableAmount)"></span></td>
                                                                            <td align="right">
                                                                                <input type="text" class="input-sm col-xs-15" data-bind="value: UtilizeAmountDeal, attr: { id: 'PositionAll' + $index(), name: 'PositionAll' + $index() }, event: { 'keyup': $root.onChangeUtilizeAmountDealAll }" /></td>
                                                                            <td><span data-bind="text: $root.LocalDate(DateOfUnderlying, true, false)"></span></td>
                                                                            <td><span data-bind="text: SupplierName"></span></td>
                                                                            <td><span data-bind="text: AttachmentNo"></span></td>
                                                                            <td><span data-bind="text: ReferenceNumber"></span></td>
                                                                            <td><span class="label label-info arrowed-in-right arrowed" data-toggle="modal" data-target="#modal-form-Underlying" data-bind="click: $root.GetUnderlyingSelectedRow, text: IsUtilize ? 'view' : 'Update'"></span></td>
                                                                        </tr>
                                                                    </tbody>
                                                                    <tbody data-bind="visible: $root.CustomerUnderlyingsTMOALL().length == 0">
                                                                        <tr>
                                                                            <td colspan="13" class="text-center">No entries available.
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <!-- table responsive end -->
                                                    </div>
                                                    <!-- widget content end -->
                                                </div>
                                                <!-- widget slim control end -->
                                                <!-- widget footer start -->

                                                <div class="widget-toolbox padding-8 clearfix">
                                                    <div class="row" data-bind="with: $root.UnderlyingGridProperties">
                                                        <!-- pagination size start -->
                                                        <div class="col-sm-6">
                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                Showing
                                            <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                rows of <span data-bind="text: Total"></span>
                                                                entries
                                                            </div>
                                                        </div>
                                                        <!-- pagination size end -->
                                                        <!-- pagination page jump start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                Page
                                            <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                of <span data-bind="text: TotalPages"></span>
                                                            </div>
                                                        </div>
                                                        <!-- pagination page jump end -->
                                                        <!-- pagination navigation start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                <ul class="pagination">
                                                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                            <i class="icon-double-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                            <i class="icon-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                            <i class="icon-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                            <i class="icon-double-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <!-- pagination navigation end -->

                                                    </div>
                                                </div>

                                                <!-- widget footer end -->

                                            </div>
                                            <!-- widget main end -->
                                        </div>
                                        <!-- widget body end -->
                                        <div class="modal-footer">
                                            <button class="btn btn-sm btn-primary" data-bind="click: $root.save_utmo">
                                                <i class="icon-save"></i>
                                                Save
                                            </button>
                                            <button class="btn btn-sm" data-bind="click: $root.cancel_u" data-dismiss="modal">
                                                <i class="icon-remove"></i>
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End -->

    <!-- modal form callbacktime -->
    <div id="modal-form-callbacktime" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="0" role="dialog">
        <div class="modal-dialog" style="width: 50%">
            <div class="modal-content">

                <!-- modal header start -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span>Call Back Time</span>
                    </h4>
                </div>
                <!-- modal header end -->
                <!-- modal body start -->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div id="customer-form" class="form-horizontal" role="form">

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="NameCallback">
                                        Contact Name
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="NameCallback" name="NameCallback" class="col-xs-10 col-sm-5" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="TimePickerCallBack">
                                        Call back time
                                    </label>
                                    <div class="col-sm-4">
                                        <div class="clearfix">
                                            <div class="input-group bootstrap-timepicker">
                                                <input type="text" id="TimePickerCallBack" class="form-control time-picker" data-bind="timepicker: true" data-rule-required="true" data-rule-value="true" />
                                                <span class="input-group-addon">
                                                    <i class="icon-time bigger-110"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="IsUtc">
                                        Is UTC
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input id="IsUtc" class="ace ace-switch ace-switch-6" type="checkbox" />
                                            <span class="lbl"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <!--<div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Remarks
                                    </label>

                                    <div class="col-sm-9">
                                        <textarea name="RemarksCallBack" id="remarks" class="col-sm-12" rows="5"></textarea>
                                    </div>
                                </div> -->

                            </div>
                            <!-- modal body form end -->
                        </div>
                    </div>
                </div>
                <!-- modal body end -->
                <!-- modal footer start -->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.SaveCallBackTime">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-bind="click: $root.CloseCallBackTime">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
                <!-- modal footer end -->

            </div>
        </div>
    </div>
    <!-- modal form callbacktime end-->

    <!-- modal form callbacktime cif -->
    <div id="modal-form-callbacktime-cif" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="0" role="dialog">
        <div class="modal-dialog" style="width: 50%">
            <div class="modal-content">

                <!-- modal header start -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span>Call Back Time</span>
                    </h4>
                </div>
                <!-- modal header end -->
                <!-- modal body start -->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div id="customer-form-cif" class="form-horizontal" role="form">

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="NameCallback">
                                        Contact Name
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="NameCallbackCIF" name="NameCallback" class="col-xs-10 col-sm-5" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="TimePickerCallBack">
                                        Call back time
                                    </label>
                                    <div class="col-sm-4">
                                        <div class="clearfix">
                                            <div class="input-group bootstrap-timepicker">
                                                <input type="text" id="TimePickerCallBackCIF" class="form-control time-picker" data-bind="timepicker: true" data-rule-required="true" data-rule-value="true" />
                                                <span class="input-group-addon">
                                                    <i class="icon-time bigger-110"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="IsUtc">
                                        Is UTC
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input id="IsUtcCIF" class="ace ace-switch ace-switch-6" type="checkbox" data-bind="event: { change: function () { $root.IsUtcChange(); } }" />
                                            <span class="lbl"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Reason
                                    </label>
                                    <div class="col-sm-9">
                                        <select id="select-reason" name="select-reason" data-bind="options: $root.ParameterCIF().ddlCallbackTimeReason, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().CIFReason, enable: $root.IsUtcChanged() == true" class="col-xs-7"></select>
                                    </div>
                                </div>
                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Remarks
                                    </label>

                                    <div class="col-sm-9">
                                        <textarea name="RemarksCallBack" id="remarks-cif" class="col-sm-12" rows="5"></textarea>
                                    </div>
                                </div>

                            </div>
                            <!-- modal body form end -->
                        </div>
                    </div>
                </div>
                <!-- modal body end -->
                <!-- modal footer start -->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.SaveCallBackTimeCif">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-bind="click: $root.CloseCallBackTimeCif">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
                <!-- modal footer end -->

            </div>
        </div>
    </div>
    <!-- modal form callbacktime cif end -->

    <!-- modal form start Attach Document Underlying Form start -->
    <div id="modal-form-Attach" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="0" role="dialog">
        <div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
                <!-- modal header start Attach FIle Form -->
                <div class="modal-header">
                    <h5 class="blue bigger">
                        <span>Add Attachment File</span>
                    </h5>
                </div>
                <!-- modal header end Attach file -->
                <!-- modal body start Attach File -->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div id="customer-form" class="form-horizontal" role="form">
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="Name">
                                        Purpose of Doc
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentPurpose" name="documentPurpose" data-bind="value: $root.Selected().DocumentPurpose_a, options: ddlDocumentPurpose_a, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...', event: { change: OnChangePropose }" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="description">
                                        Type of Doc
                                    </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentType" name="documentType" data-bind="value: $root.Selected().DocumentType_a, options: ddlDocumentType_a, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-path-upload">Document</label>

                                    <div class="col-sm-7">
                                        <div class="clearfix">
                                            <input type="file" id="document-path-upload" name="document-path-upload" data-bind="file: DocumentPath_a" class="col-xs-7" /><!---->
                                        </div>
                                    </div>
                                    <label class="control-label bolder text-danger">*</label>
                                </div>
                            </div>
                            <!-- modal body form Attach File End -->
                            <!-- widget box Underlying Attach start -->
                            <div class="dataTables_wrapper" role="grid">
                                <table class="table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <!-- <th style="width:50px">Select</th> -->
                                            <th>No.</th>
                                            <th>Select</th>
                                            <th>Statement Letter</th>
                                            <th>Underlying Document</th>
                                            <th>Type of Doc</th>
                                            <th>Currency</th>
                                            <th>Amount</th>
                                            <th>Date</th>
                                            <th>Supplier Name</th>
                                            <th>Reference Number</th>
                                            <th>Expiry Date</th>
                                        </tr>
                                    </thead>
                                    <tbody data-bind="foreach: { data: $root.CustomerAttachUnderlyings, as: 'AttachData' }, visible: $root.CustomerAttachUnderlyings().length > 0">
                                        <tr>
                                            <td><span data-bind="text: $index() + 1"></span></td>
                                            <td align="center">
                                                <input type="checkbox" id="isSelected2" name="isSelect2" data-bind="checked: IsSelectedAttach, event: { change: function (data) { $root.onSelectionAttach($index(), $data) } }, disable: !($root.SelectingUnderlying())" /></td>
                                            <td><span data-bind="text: StatementLetter.Name"></span></td>
                                            <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                            <td><span data-bind="text: DocumentType.Name"></span></td>
                                            <td><span data-bind="text: Currency.Code"></span></td>
                                            <td><span data-bind="text: FormatNumber(Amount)"></span></td>
                                            <td><span data-bind="text: $root.LocalDate(DateOfUnderlying, true, false)"></span></td>
                                            <td><span data-bind="text: SupplierName"></span></td>
                                            <td><span data-bind="text: ReferenceNumber"></span></td>
                                            <td><span data-bind="text: $root.LocalDate(ExpiredDate, true, false)"></span></td>
                                        </tr>
                                    </tbody>
                                    <tbody data-bind="visible: $root.CustomerAttachUnderlyings().length == 0">
                                        <tr>
                                            <td colspan="11" class="text-center">No entries available.
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- modal body end Attach File -->
                <!-- modal footer start Attach File-->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.save_a, disable: $root.IsUploading()">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.cancel_a">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
                <!-- modal footer end attach file -->
            </div>
        </div>
    </div>
    <!-- modal form end Attach Document Underlying Form end -->
    <!-- modal form start Attach Document Underlying Form start -->
    <div id="modal-form-Attach1" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="0" role="dialog">
        <div class="modal-dialog" style="width: 80%">
            <div class="modal-content">
                <!-- modal header start Attach FIle Form -->
                <div class="modal-header">
                    <h5 class="blue bigger">
                        <span>Add Attachment File</span>
                    </h5>
                </div>
                <!-- modal header end Attach file -->
                <!-- modal body start Attach File -->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div id="customer-form1" class="form-horizontal" role="form">
                                <div class="space-4"></div>
                                <div class="form-group" data-bind="visible: $root.ActivityTitle() != 'PPU Checker After PPU Caller Task'">
                                    <label class="col-sm-3 control-label no-padding-right" for="Name">
                                        Purpose of Doc
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentPurpose1" name="documentPurpose" data-bind="value: $root.Selected().DocumentPurpose_a, options: ddlDocumentPurpose_a, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group" data-bind="visible: $root.ActivityTitle() != 'PPU Checker After PPU Caller Task'">
                                    <label class="col-sm-3 control-label no-padding-right" for="description">
                                        Type of Doc
                                    </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentType2" name="documentType" data-bind="value: $root.Selected().DocumentType_a, options: ddlDocumentType_a, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-path-upload1">Document</label>

                                    <div class="col-sm-7">
                                        <div class="clearfix">
                                            <input type="file" id="document-path-upload1" name="document-path-upload1" data-bind="file: DocumentPath_a" class="col-xs-7" />
                                        </div>
                                    </div>
                                    <label class="control-label bolder text-danger">*</label>
                                </div>
                            </div>
                            <!-- modal body form Attach File End -->

                        </div>
                    </div>
                </div>
                <!-- modal body end Attach File -->
                <!-- modal footer start Attach File-->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.save_a, disable: $root.IsUploading()">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.cancel_a">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
                <!-- modal footer end attach file -->
            </div>
        </div>
    </div>
    <!-- modal form end Attach Document Underlying Form end -->

    <div id="modal-form-Attach2" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="0" role="dialog">
        <div class="modal-dialog" style="width:80%">
            <div class="modal-content">
                <!-- modal header start Attach FIle Form -->
                <div class="modal-header">
                    <h5 class="blue bigger">
                        <span>Add Attachment File</span>
                    </h5>
                </div>
                <!-- modal header end Attach file -->
                <!-- modal body start Attach File -->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div id="customer-form-checker" class="form-horizontal" role="form">
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="Name">
                                        Purpose of Doc
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentPurposechecker" name="documentPurpose" data-bind="value: $root.Selected().DocumentPurpose_a, options: ddlDocumentPurpose_a, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="description">
                                        Type of Doc
                                    </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentTypechecker" name="documentType" data-bind="value: $root.Selected().DocumentType_a, options: ddlDocumentType_a, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-path-upload1">Document</label>

                                    <div class="col-sm-7">
                                        <div class="clearfix">
                                            <input type="file" id="document-path-upload2" name="document-path-upload1" data-bind="file: DocumentPath_a" />
                                        </div>
                                    </div>
                                    <label class="control-label bolder text-danger">*</label>
                                </div>
                            </div>
                            <!-- modal body form Attach File End -->

                        </div>
                    </div>
                </div>
                <!-- modal body end Attach File -->
                <!-- modal footer start Attach File-->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.save_Checker, disable: $root.IsUploading()">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.cancel_c">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
                <!-- modal footer end attach file -->
            </div>
        </div>
    </div>

    <!-- Modal upload form start -->
    <div id="modal-form-upload" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="0" role="dialog">
        <div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
                <!-- modal header start Attach FIle Form -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span>Added Attachment File</span>
                    </h4>
                </div>
                <!-- modal header end Attach file -->
                <!-- modal body start Attach File -->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="document-type">Document Type</label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="document-type" name="document-type" data-rule-required="true" data-rule-value="true"
                                            data-bind="options: $root.DocumentTypes,
    optionsText: function (item) { if (item.ID != null) return item.Name + ' (' + item.Description + ')' },
    optionsValue: 'ID',
    optionsCaption: 'Please Select...',
    value: $root.Selected().DocumentType ">
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="document-purpose">Purpose of Docs</label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="document-purpose" name="document-purpose" data-rule-required="true" data-rule-value="true"
                                            data-bind="options: $root.DocumentPurposes,
    optionsText: function (item) { if (item.ID != null) return item.Name + ' (' + item.Description + ')' },
    optionsValue: 'ID',
    optionsCaption: 'Please Select...',
    value: $root.Selected().DocumentPurpose ">
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="document-path">Document</label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="file" id="document-path" name="document-path" data-bind="file: DocumentPath" data-rule-required="true" data-rule-value="true" /><!---->
                                        <!--<span data-bind="text: DocumentPath"></span>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.AddDocument">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm" data-bind="click: $root.Close">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
        </div>
    </div>
    <!-- Modal upload form end -->
    <!-- modal form Tz Information -->
    <div id="modal-form-Tz" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <!-- modal header start Underlying Form -->
                <div class="modal-header">
                    <button type="button" class="bootbox-close-button close" data-dismiss="modal" style="margin-top: -10px; font-size: 30px; margin-right: -8px;" data-bind="click: $root.btnCloseTZ">×</button>
                    <h5 class="blue bigger">
                        <span>Please Choose TZ Number</span>
                    </h5>
                </div>
                <!-- modal header end -->
                <!-- modal body start -->
                <div class="modal-body overflow-visible">
                    <div id="modalformTz">
                        <div class="row">
                            <div class="col-xs-12">

                                <!-- modal body form start -->
                                <div class="dataTables_wrapper" role="grid">
                                    <table class="table table-striped table-bordered table-hover dataTable">
                                        <thead>
                                            <tr>
                                                <!--<th>Choose</th>-->
                                                <th>No</th>
                                                <th>Booking Reference</th>
                                                <th>Customer Name</th>
                                                <th>Product Type</th>
                                                <th>Trade Date</th>
                                                <th>Currency</th>
                                                <th>Trxn Amount</th>
                                                <th>Account Number</th>
                                                <th>Exchange Rate</th>
                                                <th>Fx Transaction</th>
                                            </tr>
                                        </thead>
                                        <tbody data-bind="foreach: $root.TZInformations, visible: $root.TZInformations().length > 0">
                                            <tr data-bind="click: $root.SelectedTZ, event: { dblclick: $root.SelecteddbTZ }">
                                                <!--<td align="center"><input type="checkbox" id="tzSelection" name="tzSelection" data-bind="checked: IsTZNumber, event: { change: function (data) { $root.onSelectionTZ($index(), $data) } }" /></td> -->
                                                <!--<td class="center"><label><input type="checkbox" data-bind="event: { change: $root.SelectedTZ }" /></label></td> -->
                                                <td><span data-bind="text: RowID"></span></td>
                                                <td><span data-bind="text: TZRef"></span></td>
                                                <td><span data-bind="text: CustomerName"></span></td>
                                                <td><span data-bind="text: ProductType.Code"></span></td>
                                                <td><span data-bind="text: $root.LocalDate(TradeDate, true, false)"></span></td>
                                                <td><span data-bind="text: Currency.Code"></span></td>
                                                <td><span data-bind="text: $root.FormatNumber(Amount)"></span></td>
                                                <td><span data-bind="text: AccountNumber"></span></td>
                                                <td><span data-bind="text: Rate"></span></td>
                                                <td><span data-bind="text: IsFxTransaction"></span></td>
                                            </tr>
                                        </tbody>
                                        <tbody data-bind="visible: $root.TZInformations().length == 0">
                                            <tr>
                                                <td colspan="13" class="text-center">No entries available.
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- modal body form end -->

                                <!-- widget footer start -->
                                <div class="widget-toolbox padding-8 clearfix">
                                    <div class="row" data-bind="with: TZGridProperties">
                                        <!-- pagination size start -->
                                        <div class="col-sm-6">
                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                Showing
                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                rows of <span data-bind="text: Total"></span>
                                                entries
                                            </div>
                                        </div>
                                        <!-- pagination size end -->
                                        <!-- pagination page jump start -->
                                        <div class="col-sm-3">
                                            <div class="dataTables_paginate paging_bootstrap">
                                                Page
                                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                of <span data-bind="text: TotalPages"></span>
                                            </div>
                                        </div>
                                        <!-- pagination page jump end -->
                                        <!-- pagination navigation start -->
                                        <div class="col-sm-3">
                                            <div class="dataTables_paginate paging_bootstrap">
                                                <ul class="pagination">
                                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                            <i class="icon-double-angle-left"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                            <i class="icon-angle-left"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                            <i class="icon-angle-right"></i>
                                                        </a>
                                                    </li>
                                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                            <i class="icon-double-angle-right"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- pagination navigation end -->

                                    </div>
                                </div>
                                <!-- widget footer end -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- modal body end -->
                <!-- modal footer start -->
                <div class="modal-footer">
                    <button class="btn btn-sm" data-bind="click: $root.btnCloseTZ" data-dismiss="modal">
                        <i class="icon-remove"></i>
                        Close
                    </button>
                </div>
                <!-- modal footer end -->
            </div>
        </div>
    </div>
    <!-- modal form enderlying end -->
    <!--Basri Modal Details form start-->
    <div id="modal-form-detail-loan" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="height: 100%; width: 100%;">
            <div id="backDrop" class="absCustomBackDrop topCustomBackDrop darkCustomBackDrop hideCustomBackDrop fullHeightCustomBackDrop fullWidthCustomBackDrop centerCustomBackDrop"></div>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="blue bigger"></h4>
                </div>
                <div class="modal-body overflow-visible">
                    <div class="row" data-bind="with: TransactionCheckerLoanWorkSheetDetails()">
                        <div class="row" data-bind="ifnot: $data == null">
                            <div class="col-xs-12">
                                <div id="transaction-form2" class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label bolder no-padding-right" for="top-urgent">Top Urgent</label>

                                        <label class="col-sm-1">
                                            <input id="top-urgent" name="top-urgent" class="ace ace-switch ace-switch-6" type="checkbox" disabled="disabled" data-bind="checked: Transaction.IsTopUrgent" />
                                            <span class="lbl"></span>
                                        </label>

                                        <label class="col-sm-2 control-label bolder no-padding-right" for="top-urgent">Top Urgent Chain</label>

                                        <label class="col-sm-1">
                                            <input id="top-urgent" name="top-urgent" class="ace ace-switch ace-switch-6" type="checkbox" disabled="disabled" data-bind="checked: Transaction.IsTopUrgentChain" />
                                            <span class="lbl"></span>
                                        </label>
                                        <label class="col-sm-2 pull-left">
                                            <b>Application ID</b> : <span data-bind="text: Transaction.ApplicationID"></span>
                                        </label>

                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-3 control-label bolder no-padding-right">Customer Name</label>

                                        <div class="col-sm-6">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" class="col-sm-12" name="customer-name" id="customer-name" disabled="disabled" data-rule-required="true" data-bind="value: Transaction.Customer.Name" />
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label bolder no-padding-right">CIF</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" class="col-sm-3" name="cif" id="cif" disabled="disabled" data-bind="value: Transaction.Customer.CIF" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label bolder no-padding-right">Product</label>

                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="product" name="product" disabled="disabled" data-bind="options: $root.Products, optionsText: function (item) { if (item.Code != null) return item.Code + ' (' + item.Name + ')' }, optionsValue: 'ID', optionsCaption: 'Please Select...', value: Transaction.Product.ID"></select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label bolder no-padding-right" for="biz-segment">IBG Segment</label>

                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="bizsegment" name="bizsegment" disabled="disabled"
                                                    data-bind="options: $root.BizSegments,
    optionsText: function (item) { if (item.ID != null) return item.Name + ' (' + item.Description + ')' },
    optionsValue: 'ID',
    optionsCaption: 'Please Select...', value: Transaction.BizSegment.ID">
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label bolder no-padding-right" for="currency">Transaction Currency</label>

                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="currency" name="currency" disabled="disabled"
                                                    data-bind="options: $root.Currencies,
    optionsText: function (item) { if (item.Code != null) return item.Code + ' (' + item.Description + ')' },
    optionsValue: 'ID',
    optionsCaption: 'Please Select...', value: $root.Selected().Currency">
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label bolder no-padding-right" for="trxn-amount">Transaction Amount</label>

                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" class="col-sm-4 align-right" name="trxn-amount" id="trxn-amount" disabled="disabled" data-rule-required="true" data-rule-number="true" data-bind="value: Transaction.Amount" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label bolder no-padding-right" for="channel">Channel</label>

                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="channel" name="channel" disabled="disabled" data-bind="options: $root.Channels, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().Channel"></select>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="space-4"></div>

                                    <div>
                                        <h3 class="header smaller lighter dark">Attachment</h3>

                                        <div class="dataTables_wrapper" role="grid">
                                            <table class="table table-striped table-bordered table-hover dataTable">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>File Name</th>
                                                        <th>Document Type</th>
                                                        <th>Modified</th>
                                                    </tr>
                                                </thead>
                                                <tbody data-bind="foreach: Transaction.Documents, visible: Transaction.Documents != undefined">
                                                    <tr>
                                                        <td><span data-bind="text: $index() + 1"></span></td>
                                                        <td><a data-bind="attr: { href: DocumentPath, target: '_blank' }, text: FileName"></a></td>
                                                        <td><span data-bind="text: Purpose.Name"></span></td>
                                                        <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
                                                    </tr>
                                                </tbody>
                                                <tbody data-bind="visible: Transaction.Documents == undefined">
                                                    <tr>
                                                        <td colspan="13" class="text-center">No entries available.
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button class="btn btn-sm" data-bind="click: $root.CloseFormLoanDetails">
                            <i class="icon-remove"></i>
                            Close
                        </button>
                    </div>
                </div>
            </div>
            <!-- modal footer end -->

        </div>
    </div>

    <!-- modal form end -->

    <!--Agung UT form start-->
    <div id="modal-form-fdaddjoinfna" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog" style="height: 100%; width: 70%;">
            <div id="backDrop1" class="absCustomBackDrop topCustomBackDrop darkCustomBackDrop hideCustomBackDrop fullHeightCustomBackDrop fullWidthCustomBackDrop centerCustomBackDrop"></div>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span data-bind="if: $root.IsNewDataUT()">Add Join</span>
                        <span data-bind="if: !$root.IsNewDataUT()">Edit Join</span>
                    </h4>
                </div>
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="customer-form3" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="customer-name">Customer Name</label>

                                    <div class="col-sm-6">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-11" name="customer-nameJoinFna" id="customer-nameJoinFna" data-rule-required="true" data-bind="value: $root.AccountJoin_().CustomerJoin().Name" />
                                            <label class="control-label bolder text-danger" for="customer-nameJoinFna">*</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="cif">CIF</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-3" name="cifUTJoinFna" id="cifUTJoinFna" data-bind="value: $root.AccountJoin_().CustomerJoin().CIF, disable: true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" data-bind="">
                                    <label class="col-sm-3 control-label bolder no-padding-right">Customer Risk Effective Date</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix input-group col-sm-3 no-padding">
                                            <input class="form-control date-picker" type="text" autocomplete="off" data-date-format="dd-M-yyyy" id="value-date" name="value-date" data-rule-required="false" data-rule-value="true" data-bind="datepicker: true, value: $root.AccountJoin_().CustomerRiskEffectiveDateJoin" />
                                            <span class="input-group-addon">
                                                <i class="icon-calendar bigger-110"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" data-bind="">
                                    <label class="col-sm-3 control-label bolder no-padding-right">Risk Score</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-4 align-right" name="irate" id="irate" data-in="" data-rule-required="false" data-rule-number="true" data-bind="value: $root.AccountJoin_().RiskScoreJoin" />
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group" data-bind="">
                                    <label class="col-sm-3 control-label bolder no-padding-right">Risk Profile Expiry Date</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix input-group col-sm-3 no-padding">
                                            <input class="form-control date-picker" type="text" autocomplete="off" data-date-format="dd-M-yyyy" id="maturity-date" name="maturity-date" data-rule-required="false" data-rule-value="true" data-bind="datepicker: true, value: $root.AccountJoin_().RiskProfileExpiryDateJoin">
                                            <span class="input-group-addon">
                                                <i class="icon-calendar bigger-110"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" data-bind="">
                                    <label class="col-sm-3 control-label bolder no-padding-right">Join Type</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="JoinType" name="JoinType" data-bind="options: $root.Joins_, optionsText: function (item) { if (item.ID != null) return item.Name }, optionsValue: 'ID', value: $root.Selected().Join_" class="col-sm-5" data-rule-required="true"></select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.AddJoinFna, visible: $root.IsNewDataUT()">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.UpdateJoinFna, visible: !$root.IsNewDataUT()">
                        <i class="icon-edit"></i>
                        Update
                    </button>
                    <button class="btn btn-sm" data-bind="click: $root.CloseJoin">
                        <i class="icon-remove"></i>
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-form-fdaddjoinnonfna" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" data-bind="visible: ($root.IsIN_() && $root.IsJoin_() && $root.IsNOFNA_())">
        <div class="modal-dialog" style="height: 100%; width: 70%;">
            <div id="backDrop2" class="absCustomBackDrop topCustomBackDrop darkCustomBackDrop hideCustomBackDrop fullHeightCustomBackDrop fullWidthCustomBackDrop centerCustomBackDrop"></div>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span data-bind="if: $root.IsNewDataUT()">Add Join</span>
                        <span data-bind="if: !$root.IsNewDataUT()">Edit Join</span>
                    </h4>
                </div>
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="customer-form2" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="customer-name">Customer Name</label>

                                    <div class="col-sm-6">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-11" name="customer-nameJoinNonFna" id="customer-nameJoinNonFna" data-rule-required="true" data-bind="value: $root.AccountJoin_().CustomerJoin().Name" />
                                            <label class="control-label bolder text-danger" for="customer-nameJoinFna">*</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="cif">CIF</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-3" name="cifUTJoinNonFna" id="cifUTJoinNonFna" data-bind="value: $root.AccountJoin_().CustomerJoin().CIF, disable: true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" data-bind="">
                                    <label class="col-sm-3 control-label bolder no-padding-right">SOL ID</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" class="col-sm-4 align-right" name="solidjoin" id="solidjoin" data-rule-required="false" data-rule-number="true" data-bind="value: $root.AccountJoin_().SolIDJoin" />
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group" data-bind="">
                                    <label class="col-sm-3 control-label bolder no-padding-right">Join Type</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="JoinType" name="JoinType" data-bind="options: $root.Joins_, optionsText: function (item) { if (item.ID != null) return item.Name }, optionsValue: 'ID', value: $root.Selected().Join_" class="col-sm-5" data-rule-required="true"></select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.AddJoinNonFna, visible: $root.IsNewDataUT()">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.UpdateJoinNonFna, visible: !$root.IsNewDataUT()">
                        <i class="icon-edit"></i>
                        Update
                    </button>
                    <button class="btn btn-sm" data-bind="click: $root.CloseJoin">
                        <i class="icon-remove"></i>
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div id="mutualfund-form" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" data-bind="visible: $root.IsSP_() || $root.IsUTP_()">
        <div class="modal-dialog" style="height: 100%; width: 70%;">
            <div id="backDrop2" class="absCustomBackDrop topCustomBackDrop darkCustomBackDrop hideCustomBackDrop fullHeightCustomBackDrop fullWidthCustomBackDrop centerCustomBackDrop"></div>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span data-bind="if: $root.IsNewDataUT()">Add Mutual</span>
                        <span data-bind="if: !$root.IsNewDataUT()">Edit Mutual</span>
                    </h4>
                </div>
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="customer-form4" class="form-horizontal" role="form">
                                <div class="form-group" data-bind="visible: $root.IsSP_() || $root.IsSubscription_() || $root.IsRedemption_()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="mutualfundnamesp">Mutual Fund Name</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" class="col-sm-6 autocomplete" name="MFund" data-bind="value: $root.MutualFund_().MutualFundList().FundName" id="MFund" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" data-bind="visible: $root.IsSP_() || $root.IsSubscription_() || $root.IsRedemption_()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="mutualfundnamesp">Mutual Fund Code</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-3" name="mfundcode" id="mfundcode" data-bind="value: $root.MutualFund_().MutualFundList().FundCode" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" data-bind="visible: $root.IsSP_()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="currency">Currency</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="currency" name="currency" data-bind="options: $root.Currencies, optionsText: function (item) { if (item.Code != null) return item.Code + ' (' + item.Description + ')' }, optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().Currency" class="col-sm-5" data-rule-required="true"></select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" data-bind="visible: $root.IsSP_() || $root.IsSubscription_()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="amountsp">Amount</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="amountsp" name="amountsp" data-bind="value: $root.MutualFund_().MutualAmount" data-rule-number="true" class="col-sm-3" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" data-bind="visible: $root.IsSwitching_()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="switchmutualfundname">Switch from Mutual Fund</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-6 autocomplete" name="MFundFrom" data-bind="value: $root.MutualFund_().MutualFundSwitchFrom().FundName" id="MFundFrom" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" data-bind="visible: $root.IsSwitching_()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="switchfromfundcode">Switch From Fund Code</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-3" name="mfundcodefrom" id="mfundcodefrom" data-bind="value: $root.MutualFund_().MutualFundSwitchFrom().FundCode" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" data-bind="visible: $root.IsSwitching_()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="switchtomutualfund">Switch To Mutual Fund</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-6 autocomplete" name="MFundTo" data-bind="value: $root.MutualFund_().MutualFundSwitchTo().FundName" id="MFundTo" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" data-bind="visible: $root.IsSwitching_()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="switchtomutualcode">Switch To Mutual Code</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-3" name="mfundcodeto" id="mfundcodeto" data-bind="value: $root.MutualFund_().MutualFundSwitchTo().FundCode" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" data-bind="visible: $root.IsRedemption_() || $root.IsSwitching_()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="partialfull">Is Partial</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="checkbox" id="partialfull" name="partialfull" data-bind="checked: $root.MutualFund_().MutualPartial" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" data-bind="visible: $root.IsRedemption_() || $root.IsSwitching_()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="numberofunit">Number Of Unit</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="numberofunit1" name="numberofunit1" class="col-sm-3" data-bind="decimalPlacement: $root.MutualFund_().MutualUnitNumber, value: $root.MutualFund_().MutualUnitNumber" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm" data-bind="click: $root.AddMutualFund, visible: $root.IsNewDataUT()">
                        Save
                    </button>
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.UpdateMutualFund, visible: !$root.IsNewDataUT()">
                        <i class="icon-edit"></i>
                        Update
                    </button>
                    <button class="btn btn-sm" data-bind="click: $root.CloseMutual">
                        <i class="icon-remove"></i>
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>

    <%--add modal henggar 15032017--%>
    <div id="mutualfund-form-detail" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" data-bind="visible: $root.IsSP_() || $root.IsUTP_()">
        <div class="modal-dialog" style="height: 100%; width: 70%;">
            <div id="backDrop2" class="absCustomBackDrop topCustomBackDrop darkCustomBackDrop hideCustomBackDrop fullHeightCustomBackDrop fullWidthCustomBackDrop centerCustomBackDrop"></div>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span data-bind="if: $root.IsNewUT()">Add Mutual</span>
                        <span data-bind="if: !$root.IsNewUT()">Edit Mutual</span>
                    </h4>
                </div>
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="customer-form4" class="form-horizontal" role="form">
                                <div class="form-group" data-bind="visible: viewModel.IsSubscription_() || viewModel.IsRedemption_()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="mutualfundnamesp">Mutual Fund Name</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" class="col-sm-6 autocomplete" disabled="disabled" name="MFund" data-bind="value: $root.MutualFund_().MutualFundList().FundName" id="MFund" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" data-bind="visible: viewModel.IsSubscription_() || viewModel.IsRedemption_()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="mutualfundnamesp">Mutual Fund Code</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-3" name="mfundcode" id="mfundcode" data-bind="value: $root.MutualFund_().MutualFundList().FundCode" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" data-bind="visible: viewModel.IsSubscription_()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="amountsp">Amount</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="amountsp" name="amountsp" disabled="disabled" data-bind="value: $root.MutualFund_().MutualAmount" data-rule-number="true" class="col-sm-3" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" data-bind="visible: $root.IsSwitching_()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="switchmutualfundname">Switch from Mutual Fund</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" disabled="disabled" class="col-sm-6 autocomplete" name="MFundFrom" data-bind="value: $root.MutualFund_().MutualFundSwitchFrom().FundName" id="MFundFrom" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" data-bind="visible: $root.IsSwitching_()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="switchfromfundcode">Switch From Fund Code</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-3" name="mfundcodefrom" id="mfundcodefrom" data-bind="value: $root.MutualFund_().MutualFundSwitchFrom().FundCode" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" data-bind="visible: $root.IsSwitching_()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="switchtomutualfund">Switch To Mutual Fund</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-6 autocomplete" disabled="disabled" name="MFundTo" data-bind="value: $root.MutualFund_().MutualFundSwitchTo().FundName" id="MFundTo" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" data-bind="visible: $root.IsSwitching_()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="switchtomutualcode">Switch To Mutual Code</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-3" name="mfundcodeto" id="mfundcodeto" data-bind="value: $root.MutualFund_().MutualFundSwitchTo().FundCode" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" data-bind="visible: viewModel.IsSwitching_() || viewModel.IsRedemption_()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="partialfull">Is Partial</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="checkbox" id="partialfull" disabled="disabled" name="partialfull" data-bind="checked: $root.MutualFund_().MutualPartial" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" data-bind="visible: viewModel.IsSwitching_() || viewModel.IsRedemption_()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="numberofunit">Number Of Unit</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="numberofunit1" name="numberofunit1" class="col-sm-3" disabled="disabled" data-bind="decimalPlacement: $root.MutualFund_().MutualUnitNumber, value: $root.MutualFund_().MutualUnitNumber" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" data-bind="visible: $root.IsUTNumberType()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="utnum">UT Number</label>

                                    <label class="col-sm-8">
                                        <input type="text" id="UTNumber" name="UTNumber" data-rule-required="true" data-bind="value: $root.MutualFund_().UTNumber, event: { change: $root.OnChangeUTNumberFilled }">
                                        <label data-bind="if: $root.ValidationDBandUTFilled" class="control-label bolder text-danger" for="utnum">*</label>
                                    </label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.UpdateUTProd, visible: $root.IsNewUT()">
                        Save
                    </button>
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.UpdateUTProd, visible: !$root.IsNewUT()">
                        <i class="icon-edit"></i>
                        Update
                    </button>
                    <button class="btn btn-sm" data-bind="click: $root.CloseMutualDetail">
                        <i class="icon-remove"></i>
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>
    <%--end--%>

    <%--add modal henggar 16032017--%>
    <div id="mutualfund-form-detail-SP" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" data-bind="visible: $root.IsSP_() || $root.IsUTP_()">
        <div class="modal-dialog" style="height: 100%; width: 70%;">
            <div id="backDrop2" class="absCustomBackDrop topCustomBackDrop darkCustomBackDrop hideCustomBackDrop fullHeightCustomBackDrop fullWidthCustomBackDrop centerCustomBackDrop"></div>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span data-bind="if: $root.IsNewUT()">Add Mutual</span>
                        <span data-bind="if: !$root.IsNewUT()">Edit Mutual</span>
                    </h4>
                </div>
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="customer-form4" class="form-horizontal" role="form">
                                <div>
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="mutualfundnamesp">Mutual Fund Name</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" class="col-sm-6 autocomplete" disabled="disabled" name="MFund" data-bind="value: $root.MutualFund_().MutualFundList().FundName" id="MFund" />
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="mutualfundnamesp">Mutual Fund Code</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-3" name="mfundcode" id="mfundcode" data-bind="value: $root.MutualFund_().MutualFundList().FundCode" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="currency">Currency</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="currency" name="currency" disabled="disabled" data-bind="value: $root.MutualFund_().MutualCurrency" data-rule-number="true" class="col-sm-3" />
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="amountsp">Amount</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="amountsp" name="amountsp" disabled="disabled" data-bind="value: $root.MutualFund_().MutualAmount" data-rule-number="true" class="col-sm-3" />
                                        </div>
                                    </div>
                                </div>

                                <div data-bind="visible: $root.IsDBNumberType()">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="dbnum">DB Number</label>

                                    <label class="col-sm-8">
                                        <input type="text" id="DBNumber" name="DBNumber" data-rule-required="true" data-bind="value: $root.MutualFund_().DBNumber, event: { change: $root.OnChangeDBNumberFilled }">
                                        <label data-bind="if: $root.ValidationDBandUTFilled" class="control-label bolder text-danger" for="dbnum">*</label>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.UpdateUTSp, visible: $root.IsNewUT()">
                        Save
                    </button>
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.UpdateUTSp, visible: !$root.IsNewUT()">
                        <i class="icon-edit"></i>
                        Update
                    </button>
                    <button class="btn btn-sm" data-bind="click: $root.CloseMutualDetailSP">
                        <i class="icon-remove"></i>
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>
    <%--end--%>

    <!-- Modal upload form CIF start -->
    <div id="modal-form-upload-CIF" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span data-bind="visible: $root.IsUploadAttachment() == true">Add Attachment File</span>
                        <span data-bind="visible: $root.IsUploadExcel() == true">Upload File</span>
                    </h4>
                </div>
                <div class="modal-body overflow-visible" id="upload-form-CIF">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="detail-upload-form-CIF" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-type" data-bind="visible: $root.IsUploadAttachment() == true">Document Type</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="document-type" name="document-type" data-rule-required="true" data-rule-value="true" data-bind="options: $root.DocumentTypes, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Options..', value: $root.Selected().DocumentType, visible: $root.IsUploadAttachment() == true"></select>
                                            <label class="control-label bolder text-danger" data-bind="visible: $root.IsUploadAttachment() == true">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group" data-bind="visible: false">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-purpose" data-bind="visible: $root.IsUploadAttachment() == true">Purpose of Docs</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="document-purpose" name="document-purpose" data-rule-required="true" data-rule-value="true" data-bind="options: $root.DocumentPurposes, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Options..', value: $root.Selected().DocumentPurpose, visible: $root.IsUploadAttachment() == true"></select>
                                            <label class="control-label bolder text-danger" data-bind="visible: $root.IsUploadAttachment() == true">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-path" data-bind="visible: $root.IsUploadAttachment() == true">Document</label>
                                    <label class="col-sm-3 control-label no-padding-right" for="document-path" data-bind="visible: $root.IsUploadExcel() == true">File Name</label>
                                    <div class="col-sm-8">
                                        <div class="clearfix">
                                            <input type="file" id="document-pathCIF" name="document-path" data-bind="file: DocumentPath" />
                                            <!---->
                                            <!--<span data-bind="text: DocumentPath"></span>-->
                                        </div>
                                    </div>
                                    <label class="control-label bolder text-danger">*</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="transaction-progress-CIF" class="col-sm-12" style="display: none">
                                <h4 class="smaller blue">Upload in progress, please wait...</h4>
                                <span class="space-4"></span>
                                <div class="progress progress-small progress-striped active">
                                    <div class="progress-bar progress-bar-info" style="width: 100%"></div>
                                </div>
                            </div>
                            <div id="transaction-result-CIF" class="col-sm-12" style="display: none">
                                <div class="dataTables_wrapper" role="grid">
                                    <h4><span data-bind="text: $root.resultHeader()" /></h4>
                                    <span data-bind="text: $root.resultMessage()" />
                                </div>
                            </div>
                            <div id="transaction-grid-CIF" class="col-sm-12" style="display: none">
                                <div class="dataTables_wrapper" role="grid">
                                    <table class="table table-striped table-bordered table-hover dataTable">
                                        <thead>
                                            <tr>
                                                <th style="width: 50px">No.</th>
                                                <th><span data-bind="text: $root.Column1Name"></th>
                                                <th><span data-bind="text: $root.Column2Name"></th>
                                            </tr>
                                        </thead>
                                        <tbody data-bind="foreach: $root.RowFailed, visible: $root.RowFailed().length > 0">
                                            <tr>
                                                <!--data-bind="click: $root.EditDocument"-->
                                                <td><span data-bind="text: $index() + 1"></span></td>
                                                <td><span data-bind="text: Column1"></span></td>
                                                <td><span data-bind="text: Column2"></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.AddDocumentCIF, visible: $root.IsUploadAttachment() == true">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.UploadProcessExcel, visible: $root.IsUploadExcel() == true, disable: $root.IsUploading() == true">
                        <i class="icon-save"></i>
                        Upload
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="visible: $root.IsUploadAttachment() == true, click: $root.CloseUploadCIF">
                        <i class="icon-remove"></i>
                        Close
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="visible: $root.IsUploadExcel() == true, click: $root.CloseUploadCIF">
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal upload form CIF end -->
    <!-- Modal update account number Start -->
    <div id="modal-form-accountnumber-CIF" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <!--Account Number Currency Start-->
                            <div id="account-number-currency" class="form-horizontal" role="form" data-bind="visible: $root.IsAccountNumberCurrencyCIF() == true">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="customer-name">Customer Name</label>
                                    <div class="col-sm-6">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-11" name="customer-nameCIF" id="customer-nameCIF" data-rule-required="true" data-bind="value: $root.AccountNumberCurrencyCIF().Name" />
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="cif">CIF</label>
                                    <div class="col-sm-6">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-3" name="customer-cif" id="customer-cif" data-bind="value: $root.AccountNumberCurrencyCIF().CIF" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Account Number Currency End-->
                            <!--Account Number Additional Account Start-->
                            <div id="account-number-account" class="form-horizontal" role="form" data-bind="visible: $root.IsAccountNumberAdditionalAccountCIF() == true">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="customer-name">Account Number</label>
                                    <div class="col-sm-6">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-11" name="account-numberCIF" id="account-numberCIF" data-rule-required="true" data-bind="value: $root.AccountNumberAdditionalAccountCIF().AccountNumber" />
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="cif">Currency</label>
                                    <div class="col-sm-6">
                                        <div class="clearfix">
                                            <select id="currency-account-number-cif" name="currency-account-number-cif"
                                                data-bind="options: $root.Currencies,
    optionsText: function (item) { if (item.Code != null) return item.Code + ' (' + item.Description + ')' },
    optionsValue: 'ID',
    optionsCaption: 'Please Select...', value: $root.Selected().CurrencyCIF, event: { change: OnChangeCurrencyAccountNumberCIF }">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Account Number Additional Account End-->
                            <!--Account Number FFD Account Start-->
                            <div id="account-number-ffdaccount" class="form-horizontal" role="form" data-bind="visible: $root.IsAccountNumberFFDAccountCIF() == true">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="customer-name">Account Number</label>
                                    <div class="col-sm-6">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-11" name="account-numberFFDAccountCIF" id="account-numberFFDAccountCIF" data-rule-required="true" data-bind="value: $root.AccountNumberFFDAccountCIF().AccountNumber" />
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="cif">FFD</label>
                                    <div class="col-sm-6">
                                        <div class="clearfix">
                                            <input type="checkbox" id="islinkFFDAccountNumberCIF" name="islinkFFDAccountNumberCIF" data-bind="checked: $root.AccountNumberFFDAccountCIF().IsAddFFDAccount" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Account Number FFD Account End-->
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.AddAccountNumberToGridCIF">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.CloseUploadCIF">
                        <i class="icon-remove"></i>
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal update account number End -->

    <!-- Modal -->
    <div id="modal-form-instruction" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 800px; top: 20px">
                <div class="modal-body overflow-visible">
                    <div class="table-responsive">
                        <div class="dataTables_wrapper" role="grid">
                            <table class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Application ID</th>
                                        <th>Customer Name</th>
                                        <th>CIF</th>
                                        <th>Product</th>
                                        <th>IBG Segment</th>
                                        <th>Currency</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: $root.ChooseInstruction(), visible: $root.ChooseInstruction().length > 0">
                                    <tr data-bind="click: $root.onSelectionChoseeInstruction">
                                        <td><span data-bind="text: $index() + 1"></span></td>
                                        <td><span data-bind="text: ApplicationID"></span></td>
                                        <td><span data-bind="text: CustomerName"></span></td>
                                        <td><span data-bind="text: CIF"></span></td>
                                        <td><span data-bind="text: ProductName"></span></td>
                                        <td><span data-bind="text: Transaction.bizSegmentID"></span></td>
                                        <td><span data-bind="text: CurrencyName"></span></td>
                                        <td><span data-bind="text: $root.FormatNumber(Transaction.transactionAmount)"></span></td>
                                    </tr>
                                </tbody>
                                <tbody data-bind="visible: $root.ChooseInstruction().length == 0">
                                    <tr>
                                        <td colspan="7" align="center">No entries available.</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer" style="width: 800px">
                <button class="btn btn-sm" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
        </div>
    </div>

    <div id="modal-form-uploadLoan" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="0" role="dialog">
        <div class="modal-dialog" style="width: 80%">
            <div class="modal-content">
                <!-- modal header start Attach FIle Form -->
                <div class="modal-header">
                    <h5 class="blue bigger">
                        <span>Add Attachment File</span>
                    </h5>
                </div>
                <!-- modal header end Attach file -->
                <!-- modal body start Attach File -->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div id="customer-formLoan" class="form-horizontal" role="form">
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="description">
                                        Document Type
                                    </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentType2" name="documentType" data-bind="value: $root.Selected().DocumentType_a, options: ddlDocumentType_a, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-path-upload1">Document</label>

                                    <div class="col-sm-7">
                                        <div class="clearfix">
                                            <input type="file" id="document-path-upload1" name="document-path-upload1" data-bind="file: DocumentPath_a" class="col-xs-7" />
                                        </div>
                                    </div>
                                    <label class="control-label bolder text-danger">*</label>
                                </div>
                            </div>
                            <!-- modal body form Attach File End -->

                        </div>
                    </div>
                </div>
                <!-- modal body end Attach File -->
                <!-- modal footer start Attach File-->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.save_LoanResubmit, disable: $root.IsUploading()">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.Close">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
                <!-- modal footer end attach file -->
            </div>
        </div>
    </div>
    <!-- End-->
    <div id="modal-form-validation-trxLimit" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="0" role="dialog">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="col-sm-3 control-label no-padding-right" for="description">
                        Message for User Limit Payment Checker
                    </label>
                </div>
                <div class="modal-body overflow-visible">
                    <div class="col-xs-12">
                        <label class="col-xs-6 control-label no-padding-right" for="description-text">
                            The Transaction can not approved!!!
                        </label>
                    </div>

                    <div class="col-xs-12">
                        <label class="col-sm-3 control-label no-padding-right" for="description-text">
                            Transaction is not in accordance with user approval limit
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--aridya 20161101 insert excel password-->
    <div id="modal-form-sknbulk" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="blue bigger">Insert Excel Password
                    </h3>
                </div>
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="excel-form" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-type">Excel Password</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="password" id="passwordExcel" name="passwordExcel" data-bind="value: $root.PasswordExcel" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.UploadExcelSknBulk">
                        <i class="icon-save"></i>
                        OK
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!--end aridya-->
    <!--aridya 20161109 handle excel loading-->
    <div id="modal-loading-excel" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <!--<div class="modal-header">
			    </div>-->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <h4 class="smaller blue">Please wait...</h4>
                        <span class="space-4"></span>
                        <div class="progress progress-small progress-striped active">
                            <div class="progress-bar progress-bar-info" style="width: 100%"></div>
                        </div>
                    </div>
                </div>
                <!--<div class="modal-footer">
			    </div>-->
            </div>
        </div>
    </div>
    <!--end aridya-->
    <!--aridya popup duplicate skn bulk data-->
    <div id="messageDuplicateDataSKNBulk" align="center" style="width: 100%" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="table-header">
                        <span><b>Can not Submit Transaction !!!</b></span><br />
                        <!-- style='color:#FF0000;'-->
                        <span><b>There are duplicate data in the SKN Bulk Worksheet</b></span>
                    </div>
                </div>
                <div class="modal-body overflow-visible" data-bind="ifnot: $data == null">
                    <div class="slim-scroll" data-height="300">
                        <table class="table table-striped table-bordered table-hover dataTable">
                            <thead>
                                <tr data-bind="foreach: $root.ColumnDuplicateData">
                                    <th><span data-bind="text: $data"></span></th>
                                </tr>
                            </thead>
                            <tbody data-bind="foreach: $root.DuplicateSKNBulkData">
                                <tr data-bind="foreach: $root.ColumnDuplicateData">
                                    <td data-bind="text: $root.DuplicateSKNBulkData()[$parentContext.$index()][$data]"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.ShowDuplicateData">
                        OK
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!--end aridya-->
    <!--aridya 20161109 handle loading proses cek validasi double data skn bulk-->
    <div id="modal-loading-duplicate-data" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <!--<div class="modal-header">
			    </div>-->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <h4 class="smaller blue">Please wait...</h4>
                        <br />
                        <h4 class="smaller blue">Checking for Duplicate Data In The SKN Bulk Worksheet</h4>
                        <span class="space-4"></span>
                        <div class="progress progress-small progress-striped active">
                            <div class="progress-bar progress-bar-info" style="width: 100%"></div>
                        </div>
                    </div>
                </div>
                <!--<div class="modal-footer">
			    </div>-->
            </div>
        </div>
    </div>
    <!--end aridya-->
    <!--aridya 20161207 handle loading proses cek validasi double data skn bulk-->
    <div id="modal-loading-update-sknbulk" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <!--<div class="modal-header">
			    </div>-->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <h4 class="smaller blue">Please wait...</h4>
                        <br />
                        <h4 class="smaller blue">Updating SKN Bulk Data In The Database</h4>
                        <span class="space-4"></span>
                        <div class="progress progress-small progress-striped active">
                            <div class="progress-bar progress-bar-info" style="width: 100%"></div>
                        </div>
                    </div>
                </div>
                <!--<div class="modal-footer">
			    </div>-->
            </div>
        </div>
    </div>
    <!--end aridya-->
    <div id="lienunliendialog" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="accLienUnlien" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">Account Number</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <label class="col-sm-6" id="account-number-lienunlien" data-bind="text: $root.AccountNumberLienUnLienData"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">Lien/Un-lien</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="lien-unlien" name="lien-unlien" class="col-sm-6" data-rule-required="true" data-bind="options: $root.LienUnlienList, value: $root.LienUnliendata, optionsText: 'Data', optionsValue: 'Data', optionsCaption: 'Please Select...'"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.AddAccountNumberLienUnlien">
                        <i class="icon-save"></i>
                        OK
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.CloseAmountLienUnlien">
                        <i class="icon-remove"></i>
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal upload form start -->
    <div id="modal-form-upload-df" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 50%">
            <div class="modal-content">
                <!-- modal header start Attach FIle Form -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span>Added Attachment File</span>
                    </h4>
                </div>
                <!-- modal header end Attach file -->
                <!-- modal body start Attach File -->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="document-type">Document Type</label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="document-type-df" name="document-type" data-rule-required="true" data-rule-value="true"
                                            data-bind="options: $root.DocumentTypes, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().DocumentType ">
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="document-purpose">Purpose of Docs</label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="document-purpose-df" name="document-purpose" data-rule-required="true" data-rule-value="true"
                                            data-bind="options: $root.DocumentPurposes, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().DocumentPurpose ">
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="document-path">Document</label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="file" id="document-path-df" name="document-path" data-bind="file: DocumentPath" data-rule-required="true" data-rule-value="true" />
                                        <!--<span data-bind="text: DocumentPath"></span>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.AddDocumentDF">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-bind="click: $root.CloseDF">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal upload form end -->

    <div id="modal-form-LoadCustomerCallback" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="0" role="dialog">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <!-- modal header start -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span>Customer Call Back</span>
                    </h4>
                </div>
                <!-- modal header end -->
                <!-- modal body start -->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="widget-box">
                                <div class="widget-header widget-hea1der-small header-color-dark">
                                    <h6>Call Back Table</h6>

                                    <div class="widget-toolbar">
                                        <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                            <i class="blue icon-filter"></i>
                                            <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: CustomerCallbackGridProperties().AllowFilter" />
                                            <span class="lbl"></span>
                                        </label>
                                        <a href="#" data-bind="click: GetCustomerCallback" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                            <i class="blue icon-refresh"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <!-- widget main start -->
                                    <div class="widget-main padding-0">
                                        <!-- widget slim control start -->
                                        <div class="slim-scroll" data-height="300">
                                            <!-- widget content start -->
                                            <div class="content">

                                                <!-- table responsive start -->
                                                <div class="table-responsive">
                                                    <div class="dataTables_wrapper" role="grid">
                                                        <table id="Customer-Callback-table" class="table table-striped table-bordered table-hover dataTable">
                                                            <thead>
                                                                <tr data-bind="with: CustomerCallbackGridProperties">
                                                                    <th style="width: 50px">No.</th>
                                                                    <th style="width: 50px">Select</th>
                                                                    <th data-bind="click: function () { Sorting('ContactName'); }, css: GetSortedColumn('ContactName')">Contacted Name</th>
                                                                    <th data-bind="click: function () { Sorting('ProductName'); }, css: GetSortedColumn('ProductName')">Product</th>
                                                                    <th data-bind="click: function () { Sorting('Time'); }, css: GetSortedColumn('Time')">Call Back Time</th>
                                                                    <th data-bind="click: function () { Sorting('IsUTC'); }, css: GetSortedColumn('IsUTC')">IsUTC</th>
                                                                    <th data-bind="click: function () { Sorting('Remark'); }, css: GetSortedColumn('Remark')">Remark</th>
                                                                    <th data-bind="click: function () { Sorting('LastModifiedBy'); }, css: GetSortedColumn('LastModifiedBy')">Modified By</th>
                                                                    <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
                                                                </tr>
                                                            </thead>
                                                            <thead data-bind="visible: CustomerCallbackGridProperties().AllowFilter" class="table-filter">
                                                                <tr>
                                                                    <th class="clear-filter">
                                                                        <a href="#" data-bind="click: ClearCustomerCallbackFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                            <i class="green icon-trash"></i>
                                                                        </a>
                                                                    </th>
                                                                    <th></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCallbackContactName, event: { change: CustomerCallbackGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCallbackProductName, event: { change: CustomerCallbackGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterCallbackTime, event: { change: CustomerCallbackGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCallbackIsUTC, event: { change: CustomerCallbackGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCallbackRemark, event: { change: CustomerCallbackGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCallbackModifiedBy, event: { change: CustomerCallbackGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterCallbackModifiedDate, event: { change: CustomerCallbackGridProperties().Filter }" /></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody data-bind="foreach: CustomerCallbacks, visible: CustomerCallbacks().length > 0">
                                                                <tr>
                                                                    <td><span data-bind="text: RowID"></span></td>
                                                                    <td align="center">
                                                                        <input type="checkbox" id="isCustomerCallbackSelected" name="isCustomerCallbackSelected" data-bind="checked: isSelected, event: { change: function (data) { $root.onSelectionCustomerCallback($index(), $data) } }" />
                                                                    </td>
                                                                    <td><span data-bind="text: ContactName"></span></td>
                                                                    <td><span data-bind="text: ProductName"></span></td>
                                                                    <td><span data-bind="text: $root.LocalDate(Time)"></span></td>
                                                                    <td><span data-bind="text: IsUTCDescription"></span></td>
                                                                    <td><span data-bind="text: Remark"></span></td>
                                                                    <td><span data-bind="text: LastModifiedBy"></span></td>
                                                                    <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
                                                                </tr>
                                                            </tbody>
                                                            <tbody data-bind="visible: CustomerCallbacks().length == 0">
                                                                <tr>
                                                                    <td colspan="9" class="text-center">No entries available.
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- table responsive end -->
                                            </div>
                                            <!-- widget content end -->
                                        </div>
                                        <!-- widget slim control end -->

                                        <!-- widget footer start -->
                                        <div class="widget-toolbox padding-8 clearfix">
                                            <div class="row" data-bind="with: CustomerCallbackGridProperties">
                                                <!-- pagination size start -->
                                                <div class="col-sm-6">
                                                    <div class="dataTables_paginate paging_bootstrap pull-left">
                                                        Showing
                                                        <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                        rows of <span data-bind="text: Total"></span>
                                                        entries
                                                    </div>
                                                </div>
                                                <!-- pagination size end -->

                                                <!-- pagination page jump start -->
                                                <div class="col-sm-3">
                                                    <div class="dataTables_paginate paging_bootstrap">
                                                        Page
                                                        <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                        of <span data-bind="text: TotalPages"></span>
                                                    </div>
                                                </div>
                                                <!-- pagination page jump end -->

                                                <!-- pagination navigation start -->
                                                <div class="col-sm-3">
                                                    <div class="dataTables_paginate paging_bootstrap">
                                                        <ul class="pagination">
                                                            <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                    <i class="icon-double-angle-left"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                    <i class="icon-angle-left"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                    <i class="icon-angle-right"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                    <i class="icon-double-angle-right"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- pagination navigation end -->

                                            </div>
                                        </div>
                                        <!-- widget footer end -->

                                    </div>
                                    <!-- widget main end -->
                                </div>
                            </div>

                            <%--<div class="widget-box">
                                <div class="widget-header widget-hea1der-small header-color-dark">
                                    <h6>Call Back Table</h6>

                                    <div class="widget-toolbar">
                                        <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                            <i class="blue icon-filter"></i>
                                            <input type="checkbox" class="ace ace-switch ace-switch-3" />
                                            <span class="lbl"></span>
                                        </label>
                                        <a href="#" data-bind="click: GetCustomerCallback" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                            <i class="blue icon-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                                                        
                                <div class="widget-body">
                                    <!-- widget main start -->
                                    <div class="widget-main padding-0">
                                        <!-- widget slim control start -->
                                        <div class="slim-scroll" data-height="300">
                                            <!-- widget content start -->
                                            <div class="content">

                                                <!-- table responsive start -->
                                                <div class="table-responsive">
                                                    <div class="dataTables_wrapper" role="grid">
                                                        <table id="Customer-Callback-table" class="table table-striped table-bordered table-hover dataTable">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width: 50px">No.</th>
                                                                    <th>Contacted Name</th>
                                                                    <th>Product</th>
                                                                    <th>Call Back Time</th>
                                                                    <th>IsUTC</th>
                                                                    <th>Remark</th>
                                                                    <th>Modified By</th>
                                                                    <th>Modified Date</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody data-bind="foreach: CustomerCallbacks, visible: CustomerCallbacks().length > 0">
                                                                <tr>
                                                                    <td><span data-bind="text: RowID"></span></td>
                                                                    <td><span data-bind="text: ContactName"></span></td>
                                                                    <td><span data-bind="text: ProductName"></span></td>
                                                                    <td><span data-bind="text: $root.LocalDate(Time)"></span></td>
                                                                    <td><span data-bind="text: IsUTCDescription"></span></td>
                                                                    <td><span data-bind="text: Remark"></span></td>
                                                                    <td><span data-bind="text: LastModifiedBy"></span></td>
                                                                    <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
                                                                </tr>
                                                            </tbody>
                                                            <tbody data-bind="visible: CustomerCallbacks().length == 0">
                                                                <tr>
                                                                    <td colspan="8" class="text-center">No entries available.
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- table responsive end -->
                                            </div>
                                            <!-- widget content end -->
                                        </div>
                                        <!-- widget slim control end -->

                                    </div>
                                    <!-- widget main end -->
                                </div>
                            </div>--%>
                        </div>
                    </div>
                </div>
                <!-- modal body end -->
                <!-- modal footer start -->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.SaveCustomerCallback">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-bind="click: $root.CloseCustomerCallback">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
                <!-- modal footer end -->

            </div>
        </div>
    </div>

</div>

<%--Approval Template Start--%>
<script type="text/html" id="CommonTemplate"></script>
<%--Approval Template End--%>

<!-- additional page scripts start -->
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/date-time/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/CalculatedFX.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/HomeTunning.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/w2ui/w2ui-1.5.rc1.min.js"></script>
<!-- additional page scripts end -->
