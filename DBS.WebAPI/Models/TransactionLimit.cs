﻿using DBS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//additional
using System.Linq.Dynamic;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using System.Text;

namespace DBS.WebAPI.Models
{
    #region property
    [ModelName("TransactionLimit")]
    public class TransactionLimitModel
    {
        public int ID { get; set; }
        public ProductModel Product { get; set; }
        public CurrencyModel Currency { get; set; }
        public decimal MinAmount { get; set; }
        public decimal MaxAmount { get; set; }
        public Nullable<bool> Unlimited { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }

    public class TransactionLimitRow : TransactionLimitModel
    {
        public int RowID { get; set; }
    }
    public class TransactionLimitGrid : Grid
    {
        public IList<TransactionLimitRow> Rows { get; set; }
    }
    #endregion

    #region filter
    public class TransactionLimitFilter : Filter { }
    #endregion

    #region interface
    public interface ITransactionLimitRepo : IDisposable
    {
        bool GetTransactionLimitByID(int id, ref TransactionLimitModel TransactionLimitID, ref string message);
        bool GetTransactionLimit(ref IList<TransactionLimitModel> transactionLimit, int limit, int index, ref string message);
        bool GetTransactionLimit(ref IList<TransactionLimitModel> transactionLimit, ref string message);
        bool GetTransactionLimit(int page, int size, IList<TransactionLimitFilter> filters, string sortColumn, string sortOrder, ref TransactionLimitGrid TransactionLimit, ref string message);
        bool GetTransactionLimit(TransactionLimitFilter filter, ref IList<TransactionLimitModel> TransactionLimit, ref string message);
        bool GetTransactionLimit(string key, int limit, ref IList<TransactionLimitModel> transactionLimit, ref string message);
        bool AddTransactionLimit(TransactionLimitModel transactionLimit, ref string message);
        bool UpdateTransactionLimit(int id, TransactionLimitModel transactionLimit, ref string message);
        bool DeleteTransactionLimit(int id, ref string message);
        bool GetTransactionLimitByProduct(ref IList<TransactionLimitModel> transactionLimit, int id, ref string message);

    }
    #endregion

    #region repository
    public class TransactionLimitRepository : ITransactionLimitRepo
    {

        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetTransactionLimitByProduct(ref IList<TransactionLimitModel> transactionLimit, int id, ref string message)
        {
            // throw new NotImplementedException();
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    transactionLimit = (from a in context.TransactionLimitProducts
                                        join b in context.Products on a.ProductID equals b.ProductID
                                        join c in context.Currencies on a.CurrencyID equals c.CurrencyID
                                        where a.IsDeleted == false && b.ProductID.Equals(id)
                                        select new TransactionLimitModel
                                        {
                                            ID = a.TransactionLimitProductID,
                                            Product = new ProductModel()
                                            {
                                                ID = (a.ProductID != null) ? a.ProductID : 0,
                                                Name = (a.ProductID != null) ? b.ProductName : "-"
                                            },
                                            Currency = new CurrencyModel()
                                            {
                                                ID = (a.CurrencyID != null) ? a.CurrencyID : 0,
                                                Code = (a.CurrencyID != null) ? c.CurrencyCode : "-"
                                            },
                                            MinAmount = a.MinAmount,
                                            MaxAmount = a.MaxAmount,
                                            Unlimited = a.Unlimited,
                                            LastModifiedDate = a.CreateDate,
                                            LastModifiedBy = a.CreateBy
                                        }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


        public bool GetTransactionLimitByID(int id, ref TransactionLimitModel TransactionLimitID, ref string message)
        {
            //throw new NotImplementedException();
            bool isSuccess = false;

            try
            {
                TransactionLimitID = (from a in context.TransactionLimitProducts
                                      join b in context.Products on a.ProductID equals b.ProductID
                                      join c in context.Currencies on a.CurrencyID equals c.CurrencyID
                                      where a.IsDeleted.Equals(false) && a.TransactionLimitProductID.Equals(id)
                                      select new TransactionLimitModel
                                      {
                                          ID = a.TransactionLimitProductID,
                                          Product = new ProductModel()
                                          {
                                              ID = (a.ProductID != null) ? a.ProductID : 0,
                                              Name = (a.ProductID != null) ? b.ProductName : "-"
                                          },
                                          Currency = new CurrencyModel()
                                          {
                                              ID = (a.CurrencyID != null) ? a.CurrencyID : 0,
                                              Code = (a.CurrencyID != null) ? c.CurrencyCode : "-"
                                          },
                                          MinAmount = a.MinAmount,
                                          MaxAmount = a.MaxAmount,
                                          LastModifiedDate = a.CreateDate,
                                          LastModifiedBy = a.CreateBy
                                      }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetTransactionLimit(ref IList<TransactionLimitModel> transactionLimit, int limit, int index, ref string message)
        {
            //throw new NotImplementedException();
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    transactionLimit = (from a in context.TransactionLimitProducts
                                        join b in context.Products on a.ProductID equals b.ProductID
                                        join c in context.Currencies on a.CurrencyID equals c.CurrencyID
                                        where a.IsDeleted.Equals(false)
                                        orderby new { a.ProductID }
                                        select new TransactionLimitModel
                                        {
                                            ID = a.TransactionLimitProductID,
                                            Product = new ProductModel()
                                            {
                                                ID = (a.ProductID != null) ? a.ProductID : 0,
                                                Name = (a.ProductID != null) ? b.ProductName : "-"
                                            },
                                            Currency = new CurrencyModel()
                                            {
                                                ID = (a.CurrencyID != null) ? a.CurrencyID : 0,
                                                Code = (a.CurrencyID != null) ? c.CurrencyCode : "-"
                                            },
                                            MinAmount = a.MinAmount,
                                            MaxAmount = a.MaxAmount,
                                            LastModifiedDate = a.CreateDate,
                                            LastModifiedBy = a.CreateBy
                                        }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetTransactionLimit(ref IList<TransactionLimitModel> transactionLimit, ref string message)
        {
            // throw new NotImplementedException();
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    transactionLimit = (from a in context.TransactionLimitProducts
                                        join b in context.Products on a.ProductID equals b.ProductID
                                        join c in context.Currencies on a.CurrencyID equals c.CurrencyID
                                        where a.IsDeleted.Equals(false)
                                        orderby new { a.ProductID }
                                        select new TransactionLimitModel
                                        {
                                            ID = a.TransactionLimitProductID,
                                            Product = new ProductModel()
                                            {
                                                ID = (a.ProductID != null) ? a.ProductID : 0,
                                                Name = (a.ProductID != null) ? b.ProductName : "-"
                                            },
                                            Currency = new CurrencyModel()
                                            {
                                                ID = (a.CurrencyID != null) ? a.CurrencyID : 0,
                                                Code = (a.CurrencyID != null) ? c.CurrencyCode : "-"
                                            },
                                            MinAmount = a.MinAmount,
                                            MaxAmount = a.MaxAmount,
                                            LastModifiedDate = a.CreateDate,
                                            LastModifiedBy = a.CreateBy
                                        }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetTransactionLimit(int page, int size, IList<TransactionLimitFilter> filters, string sortColumn, string sortOrder, ref TransactionLimitGrid TransactionLimitGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                TransactionLimitModel filter = new TransactionLimitModel();
                filter.Product = new ProductModel { Name = string.Empty };
                filter.Currency = new CurrencyModel { Code = string.Empty };
                if (filters != null)
                {

                    filter.MinAmount = Convert.ToInt32((string)filters.Where(a => a.Field.Equals("MinAmount")).Select(a => a.Value).SingleOrDefault());
                    filter.MaxAmount = Convert.ToInt32((string)filters.Where(a => a.Field.Equals("MaxAmount")).Select(a => a.Value).SingleOrDefault());
                    filter.Product = new ProductModel { Name = (string)filters.Where(a => a.Field.Equals("Product")).Select(a => a.Value).SingleOrDefault() };
                    filter.Currency = new CurrencyModel { Code = (string)filters.Where(a => a.Field.Equals("Currency")).Select(a => a.Value).SingleOrDefault() };

                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.TransactionLimitProducts
                                join b in context.Products on a.ProductID equals b.ProductID
                                join c in context.Currencies on a.CurrencyID equals c.CurrencyID

                                let isMinAmount = filter.MinAmount ==0 ? false : true
                                let isMaxAmount = filter.MaxAmount ==0? false : true
                                let isProduct = !string.IsNullOrEmpty(filter.Product.Name)
                                let isCurrency = !string.IsNullOrEmpty(filter.Currency.Code)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted == false &&

                                    (isMaxAmount ? a.MaxAmount == filter.MaxAmount : true) &&
                                    (isMinAmount ? a.MinAmount == filter.MinAmount : true) &&
                                    (isProduct ? b.ProductName.Contains(filter.Product.Name) : true) &&
                                    (isCurrency ? a.CurrencyID == filter.Currency.ID : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new TransactionLimitModel
                                {
                                    ID = a.TransactionLimitProductID,

                                    Product = new ProductModel()
                                    {
                                        ID = (a.ProductID != null) ? a.ProductID : 0,
                                        Name = (a.ProductID != null) ? b.ProductName : "-",

                                    },
                                    Currency = new CurrencyModel()
                                    {

                                        ID = (a.CurrencyID != null) ? a.CurrencyID : 0,
                                        Code = (a.CurrencyID != null) ? c.CurrencyCode : "-"
                                    },
                                    MaxAmount = a.MaxAmount,
                                    MinAmount = a.MinAmount,
                                    Unlimited = a.Unlimited,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    TransactionLimitGrid.Page = page;
                    TransactionLimitGrid.Size = size;
                    TransactionLimitGrid.Total = data.Count();
                    TransactionLimitGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    TransactionLimitGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new TransactionLimitRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Product = a.Product,
                            Currency = a.Currency,
                            MinAmount = a.MinAmount,
                            MaxAmount = a.MaxAmount,
                            Unlimited = a.Unlimited,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;

        }

        public bool GetTransactionLimit(TransactionLimitFilter filter, ref IList<TransactionLimitModel> TransactionLimit, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetTransactionLimit(string key, int limit, ref IList<TransactionLimitModel> transactionLimit, ref string message)
        {
            //throw new NotImplementedException();
            bool isSuccess = false;

            try
            {
                transactionLimit = (from a in context.TransactionLimitProducts
                                    where a.IsDeleted.Equals(false) &&
                                    a.CreateBy.Equals(key)
                                    orderby new { a.CreateBy }
                                    select new TransactionLimitModel
                                    {
                                        MinAmount = a.MinAmount,
                                        MaxAmount = a.MaxAmount,
                                        LastModifiedBy = a.CreateBy
                                    }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddTransactionLimit(TransactionLimitModel transactionLimit, ref string message)
        {
            //throw new NotImplementedException();
            bool isSuccess = false;

            try
            {
                if (!context.TransactionLimitProducts.Where(a => a.TransactionLimitProductID ==transactionLimit.ID && a.IsDeleted == false).Any())
                {
                    TransactionLimitProduct TransactionLimit = new TransactionLimitProduct();

                    TransactionLimit.MinAmount = transactionLimit.MinAmount;
                    TransactionLimit.MaxAmount = transactionLimit.MaxAmount;
                    TransactionLimit.IsDeleted = false;
                    TransactionLimit.Unlimited = transactionLimit.Unlimited;
                    TransactionLimit.CreateDate = DateTime.UtcNow;
                    TransactionLimit.CreateBy = currentUser.GetCurrentUser().DisplayName;
                    if (!string.IsNullOrEmpty(transactionLimit.Product.Name))
                    {
                        TransactionLimit.ProductID = transactionLimit.Product.ID;
                    }

                    TransactionLimit.CurrencyID = transactionLimit.Currency.ID;

                    context.TransactionLimitProducts.Add(TransactionLimit);
                    context.SaveChanges();
                    isSuccess = true;

                    //context.TransactionLimitProducts.Add(new Entity.TransactionLimitProduct()
                    //{
                    //    CurrencyID = transactionLimit.Currency,
                    //    ProductID = transactionLimit.Product,
                    //    MinAmount = transactionLimit.MinAmount,
                    //    MaxAmount = transactionLimit.MaxAmount,
                    //    IsDeleted = false,
                    //    CreatedDate = DateTime.UtcNow,
                    //    CreatedBy = currentUser.GetCurrentUser().DisplayName
                    //});
                    //context.SaveChanges();

                    //isSuccess = true;
                }
                else
                {
                    message = string.Format("Transaction Limit data for Transaction Limit  {0} is already exist.", transactionLimit.Product);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateTransactionLimit(int id, TransactionLimitModel transactionLimit, ref string message)
        {
            // throw new NotImplementedException();
            bool isSuccess = false;

            try
            {
                Entity.TransactionLimitProduct data = context.TransactionLimitProducts.Where(a => a.TransactionLimitProductID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.TransactionLimitProducts.Where(a => a.TransactionLimitProductID != transactionLimit.ID && a.MinAmount.Equals(transactionLimit.MinAmount) && a.MaxAmount.Equals(transactionLimit.MaxAmount) && a.Unlimited.HasValue.Equals(transactionLimit.Unlimited.HasValue) && a.IsDeleted == false).Count() >= 1)
                    {
                        message = string.Format("Transaction Limit {0} is exist.", transactionLimit.ID);
                    }
                    else
                    {
                        // data.ProductID = transactionLimit.Product;
                        data.MinAmount = transactionLimit.MinAmount;
                        data.MaxAmount = transactionLimit.MaxAmount;
                        data.Unlimited = transactionLimit.Unlimited;
                        //data.CurrencyID = transactionLimit.Currency;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        if (!string.IsNullOrEmpty(transactionLimit.Product.Name))
                        {
                            data.ProductID = transactionLimit.Product.ID;
                        }
                        if (transactionLimit.Currency.ID != 0)
                        {
                            data.CurrencyID = transactionLimit.Currency.ID;
                        }

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Transaction Limit with id {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteTransactionLimit(int id, ref string message)
        {
            //throw new NotImplementedException();
            bool isSuccess = false;

            try
            {
                Entity.TransactionLimitProduct data = context.TransactionLimitProducts.Where(a => a.TransactionLimitProductID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Transaction Limit data for TransactionLimit ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    #endregion
    }
}