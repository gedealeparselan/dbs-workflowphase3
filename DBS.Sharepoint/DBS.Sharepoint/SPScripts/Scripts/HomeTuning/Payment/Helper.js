$.cookie.json = true;

var connection;
var spUser;


//global var inside in this vHelper
var vSystem = {
    AmountResCalculate: null,
    vLLDAmount: null,
    vLLDRounding: null,
    LLDRounding: null,
    amSP: null,
    numUnit: null,
    idrrate: 0
};

var Helper = {
    Ajax: {
        Get: function (options, success, error, always) {
            AjaxRequest("GET", options, success, error, always);
        },
        Post: function (options, success, error, always) {
            AjaxRequest("POST", options, success, error, always);
        },
        Put: function (options, success, error, always) {
            AjaxRequest("PUT", options, success, error, always);
        },
        Delete: function (options, success, error, always) {
            AjaxRequest("DELETE", options, success, error, always);
        },
        AutoComplete: function (options, response, success, error) {
            AjaxRequestAutoComplete(options, response, success, error);
        }
    },
    Token: {
        Request: function (success, error) {
            CreateToken(success, error);
        }
    },
    Signal: {
        Connect: function (success, error, received) {
            SignalRConnection(success, error, received);
        }
    },
    Sharepoint: {
        Nintex: {
            Post: function (options, success, error, always) {
                AjaxRequestSharepoint("POST", options, success, error, always);
            }
        },
        Validate: {
            User: function (options, success, error, always) {
                AjaxRequestSharepoint("POST", options, success, error, always);
            }
        },
        List: {
            Add: function (options, success, error, always) {
                AjaxRequestSharepoint("POST", options, success, error, always);
            }
        }
    },
    GetToken: function (callback) {
        if ($.cookie(api.cookie.name) == undefined) {
            Helper.Token.Request(function (data, textStatus, jqXHR) {
                if (callback) callback(data.AccessToken);
                $.cookie(api.cookie.name, data.AccessToken);
                accessToken = $.cookie(api.cookie.name);
            }, function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
            });
        } else {
            if (callback) callback(api.cookie.name);
            accessToken = $.cookie(api.cookie.name);
        }
    },
     OnError: function(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}
};


function AjaxRequest(method, options, success, error, always) {
    var isValid = true;
    var Url = options.url;

    // validate & decode params
    if (options.params != null || options.params != undefined) {
        Url += DecodeParams(options.params);
    }

    // declare ajax options
    var ajaxOptions = {
        type: method,
        url: Url,
        contentType: "application/json; charset=utf-8", //; odata=verbose;
        dataType: "json",
        //data: ko.toJSON(options.data),
        headers: {
            "Authorization": "Bearer " + options.token
        },
        success: function (data, textStatus, jqXHR) {
            success(data, textStatus, jqXHR);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    };

    // set body request
    if (method != "GET") {
        // validate request body on POST
        if (options.data == null || options.data == undefined) {
            alert(method + " method does not contains request body");

            isValid = false;
        } else {
            ajaxOptions.data = options.data;
        }
    }

    // execute ajax request
    if (isValid) {
        $.ajax(ajaxOptions).always(function () {
            if (always != null || always != undefined)
                always();
        });
    } else {
        if (always != null || always != undefined)
            always();
    }
}

function AjaxRequestSharepoint(method, options, success, error, always) {
    var isValid = true;
    var Url = options.url;

    // validate & decode params
    if (options.params != null || options.params != undefined) {
        Url += DecodeParams(options.params);
    }

    // declare ajax options
    var ajaxOptions = {
        type: method,
        url: Url,
        contentType: "application/json; odata=verbose",
        dataType: "json",
        headers: {
            "accept": "application/json; odata=verbose",
            "X-RequestDigest": options.digest
        },
        success: function (data, textStatus, jqXHR) {
            success(data, textStatus, jqXHR);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    };

    // set body request
    if (method != "GET") {
        // validate request body on POST
        if (options.data == null || options.data == undefined) {
            alert(method + " method does not contains request body");

            isValid = false;
        } else {
            ajaxOptions.data = options.data;
        }
    }

    // execute ajax request
    if (isValid) {
        $.ajax(ajaxOptions).always(function () {
            if (always != null || always != undefined)
                always();
        });
    } else {
        if (always != null || always != undefined)
            always();
    }
}

function AjaxRequestAutoComplete(options, response, success, error) {
    // declare ajax options
    var ajaxOptions = {
        type: "GET",
        url: options.url,
        contentType: "application/json; charset=utf-8; odata=verbose;",
        dataType: "json",
        data: options.data,
        headers: {
            "Authorization": "Bearer " + options.token
        },
        success: function (data, textStatus, jqXHR) {
            success(response, data, textStatus, jqXHR);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    };

    // execute ajax request
    $.ajax(ajaxOptions);
}

function DecodeParams(params) {
    if (typeof (params) != 'object') {
        alert("The parameter value is not an object!")
        return false;
    }

    var output = "";

    for (var key in params) {
        if (output != "") {
            output += "&";
        } else {
            output += "?";
        }

        output += key + "=" + params[key];
    }

    return output;
}

function CreateToken(success, error) {
    // checking stored token on browser cookies
    if ($.cookie(api.cookie.name) == undefined) {
        // create rest api request to sharepoint
        $.ajax({
            type: "GET",
            url: "/_api/web/CurrentUser?$select=id,LoginName",
            headers: {
                "accept": "application/json;odata=verbose"
            },
            success: function (data, textStatus, jqXHR) {
                OnSuccessCurrentUser(data, success, error);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                error(jqXHR, textStatus, errorThrown);
            }
        });
    }
}

function OnSuccessCurrentUser(data, success, error) {
    $.ajax({
        type: "GET",
        url: "/_api/web/GetUserById(" + data.d.Id + ")?$select=Id,LoginName,Title,Email",
        headers: {
            "accept": "application/json;odata=verbose"
        },
        success: function (data, textStatus, jqXHR) {
            OnSuccessUserByID(data, success, error);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    });
}

function OnSuccessUserByID(datas, success, error) {
    // fill data to spUser
    spUser = {
        ID: datas.d.Id,
        LoginName: datas.d.LoginName,
        DisplayName: datas.d.Title,
        Email: datas.d.Email,
        Roles: {}
    };

    // get sharepoint user groups
    $.ajax({
        type: "GET",
        url: "/_api/web/GetUserById(" + datas.d.Id + ")/Groups?$select=Id,Title&$filter=startswith(Title, 'DBS')",
        headers: {
            "accept": "application/json; odata=verbose"
        },
        success: function (data, textStatus, jqXHR) {
            var roles = []
            for (i = 0; i < data.d.results.length; i++) {
                roles.push({
                    ID: data.d.results[i].Id,
                    Name: data.d.results[i].Title
                });
            }

            // adding roles from sp groups
            spUser['Roles'] = roles;

            // store spUser to cookie
            $.cookie(api.cookie.spUser, spUser);

            // call request api token function
            RequestAPIToken(success, error);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    });
}

// request new jwt token to DBS API
function RequestAPIToken(success, error) {
    $.ajax({
        type: "POST",
        url: api.server + api.url.auth,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(spUser),
        success: function (data, textStatus, jqXHR) {
            success(data, textStatus, jqXHR);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    });
}

// SignalR Helper
function SignalRConnection(success, error, received) {
    // build connection to server
    connection = $.connection(config.signal.server + "echo");

    // start connection
    connection.start().done(success).fail(error);

    // received callback
    connection.received(function (data) {
        received(data);
    });
}

// Gritter Notification
// class name : gritter-success, gritter-warning, gritter-error, gritter-info
function ShowNotification(title, text, className, isSticky) {
    $.gritter.add({
        title: title,
        text: text,
        class_name: className,
        sticky: false,
        time: 1000
    });
}
