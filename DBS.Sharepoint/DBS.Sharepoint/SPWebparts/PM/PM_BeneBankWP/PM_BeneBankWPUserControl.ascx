﻿<%@ Assembly Name="DBS.Sharepoint, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f75c72caf4809f4b" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, 

PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, 

PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, 

PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PM_BeneBankWPUserControl.ascx.cs" 

Inherits="DBS.Sharepoint.SPWebparts.PM.PM_BeneBankWP.PM_BeneBankWPUserControl" %>

<h1 class="header smaller lighter dark">Bene Bank</h1>
<!-- widget box start -->
<div id="widget-box" class="widget-box">
    <!-- widget header start -->
    <div class="widget-header widget-hea1der-small header-color-dark">
        <h6>Bene Bank Table</h6>
        <div class="widget-toolbar">
            <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                <i class="blue icon-filter"></i>
                <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridProperties().AllowFilter" />
                <span class="lbl"></span>
            </label>
            <a href="#" data-bind="click: GetData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                <i class="blue icon-refresh"></i>
            </a>
        </div>
    </div>
    <!-- widget header end -->
    <!-- widget body start -->
    <div class="widget-body">
        <!-- widget main start -->
        <div class="widget-main padding-0">
            <!-- widget slim control start -->
            <div class="slim-scroll" data-height="400">
                <!-- widget content start -->
                <div class="content">
                    <!-- table responsive start -->
                    <div class="table-responsive">
                        <div class="dataTables_wrapper" role="grid">
                            <table id="BeneBank-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr data-bind="with: GridProperties">
                                        <th style="width: 50px">No.</th>
                                        <th data-bind="click: function () { Sorting('Code'); }, css: GetSortedColumn('Code')">Bank Code </th>
                                        <th data-bind="click: function () { Sorting('BranchCode'); }, css: GetSortedColumn('BranchCode')">Branch Code </th>
                                        <th data-bind="click: function () { Sorting('SwiftCode'); }, css: GetSortedColumn('SwiftCode')">Swift Code</th>
                                        <th data-bind="click: function () { Sorting('BankAccount'); }, css: GetSortedColumn('BankAccount')">Bank A/C</th>
                                        <th data-bind="click: function () { Sorting('Description'); }, css: GetSortedColumn('Description')">Description</th>
                                        <th data-bind="click: function () { Sorting('CommonName'); }, css: GetSortedColumn('CommonName')">Common Name</th>
                                        <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                        <th data-bind="click: function () { Sorting('PGSL'); }, css: GetSortedColumn('PGSL')">PGSL</th>
                                        <th data-bind="click: function () { Sorting('IsRTGS'); }, css: GetSortedColumn('IsRTGS')">IsRTGS</th>
                                        <th data-bind="click: function () { Sorting('IsSKN'); }, css: GetSortedColumn('IsSKN')">IsSKN</th>
                                        <th data-bind="click: function () { Sorting('IsOTT'); }, css: GetSortedColumn('IsOTT')">IsOTT</th>
                                        <th data-bind="click: function () { Sorting('IsOVB'); }, css: GetSortedColumn('IsOVB')">IsOVB</th>
                                        <th data-bind="click: function () { Sorting('LastModifiedBy'); }, css: GetSortedColumn('LastModifiedBy')">Modified By</th>
                                        <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Modified 

Date</th>
                                    </tr>
                                </thead>
                                <thead data-bind="visible: GridProperties().AllowFilter" class="table-filter">
                                    <tr>
                                        <th class="clear-filter">
                                            <a href="#" data-bind="click: ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear 

Filters">
                                                <i class="green icon-trash"></i>
                                            </a>
                                        </th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCode, event: {
change: GridProperties().Filter

}" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterBranchCode, event: {
change: GridProperties

().Filter
}" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterSwiftCode, event: {
change: GridProperties

().Filter
}" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterBankAccount, event: {
change: GridProperties

().Filter
}" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterDescription, event: {
change: GridProperties

().Filter
}" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCommonName, event: {
change: GridProperties

().Filter
}" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCurrency, event: {
change: GridProperties

().Filter
}" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterPGSL, event: {
change: GridProperties().Filter

}" /></th>

                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterIsRTGS, event: {
change: GridProperties

().Filter
}" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterIsSKN, event: {
change: GridProperties().Filter

}" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterIsOTT, event: {
change: GridProperties().Filter

}" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterIsOVB, event: {
change: GridProperties().Filter

}" /></th>

                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterModifiedBy, event: {
change: GridProperties

().Filter
}" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value:

$root.FilterModifiedDate, event: { change: GridProperties().Filter }" /></th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: Banks, visible: Banks().length > 0">
                                    <tr data-bind="click: $root.GetSelectedRow" data-toggle="modal">
                                        <td><span data-bind="text: RowID"></span></td>
                                        <td><span data-bind="text: Code"></span></td>
                                        <td><span data-bind="text: BranchCode"></span></td>
                                        <td><span data-bind="text: SwiftCode"></span></td>
                                        <td><span data-bind="text: BankAccount"></span></td>
                                        <td><span data-bind="text: Description"></span></td>
                                        <td><span data-bind="text: CommonName"></span></td>
                                        <td><span data-bind="text: Currency"></span></td>
                                        <td><span data-bind="text: PGSL"></span></td>
                                        <td><span data-bind="text: (IsRTGS == true ? 'Yes' : 'No')"></span></td>
                                        <td><span data-bind="text: (IsSKN == true ? 'Yes' : 'No')"></span></td>
                                        <td><span data-bind="text: (IsOTT == true ? 'Yes' : 'No')"></span></td>
                                        <td><span data-bind="text: (IsOVB == true ? 'Yes' : 'No')"></span></td>
                                        <td><span data-bind="text: LastModifiedBy"></span></td>
                                        <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
                                    </tr>
                                </tbody>
                                <tbody data-bind="visible: Banks().length == 0">
                                    <tr>
                                        <td colspan="15" class="text-center">No entries available.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- table responsive end -->
                </div>
                <!-- widget content end -->
            </div>
            <!-- widget slim control end -->
            <!-- widget footer start -->
            <div class="widget-toolbox padding-8 clearfix">
                <div class="row" data-bind="with: GridProperties">
                    <!-- pagination size start -->
                    <div class="col-sm-6">
                        <div class="dataTables_paginate paging_bootstrap pull-left">
                            Showing
                            <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-

rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                            rows of <span data-bind="text: Total"></span>
                            entries
                        </div>
                    </div>
                    <!-- pagination size end -->
                    <!-- pagination page jump start -->
                    <div class="col-sm-3">
                        <div class="dataTables_paginate paging_bootstrap">
                            Page
                            <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" 

data-placement="bottom" title="Jump to Page" />
                            of <span data-bind="text: TotalPages"></span>
                        </div>
                    </div>
                    <!-- pagination page jump end -->
                    <!-- pagination navigation start -->
                    <div class="col-sm-3">
                        <div class="dataTables_paginate paging_bootstrap">
                            <ul class="pagination">
                                <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                        <i class="icon-double-angle-left"></i>
                                    </a>
                                </li>
                                <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                        <i class="icon-angle-left"></i>
                                    </a>
                                </li>
                                <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                        <i class="icon-angle-right"></i>
                                    </a>
                                </li>
                                <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                        <i class="icon-double-angle-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- pagination navigation end -->

                </div>
            </div>
            <!-- widget footer end -->
        </div>
        <!-- widget main end -->
    </div>
    <!-- widget body end -->
</div>
<!-- widget box end -->
<div data-bind="if: $root.IsRoleMaker()">
    <h4 class="pink">
        <i class="icon-hand-right green"></i>
        <a href="#modal-forms" role="button" class="blue" data-toggle="modal" data-bind="click: NewData">New Bene Bank</a>
    </h4>
</div>

<!-- modal form start -->
<div id="modal-forms" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 1350px">
        <div class="modal-content">
            <!-- modal header start -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span data-bind="if: $root.IsNewData()">Create a new Bene Bank Parameter</span>
                    <span data-bind="if: !$root.IsNewData()">Modify a Bene Bank Parameter</span>
                </h4>
            </div>
            <!-- modal header end -->
            <!-- modal body start -->
            <div class="modal-body overflow-visible">

                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->

                        <div id="customer-form" class="form-horizontal" role="form">
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="code">
                                    Bank Code
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="code" name="code" data-bind="value: $root.Code, disable: $root.Readonly" class="col-xs-10 col-sm-5"  

data-rule-maxlength="50" />
                                        <!-- <label class="control-label bolder text-danger" for="code">*</label> -->
					 <label data-bind="if: $root.IsCheckOTT" class="control-label bolder text-danger">*</label>

                                    </div>
                                </div>
                            </div>
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="branchcode">
                                    Branch  Code
                                </label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="branchcode" name="branchcode" data-bind="value: $root.BranchCode, disable: $root.Readonly" class="col-

xs-10 col-sm-5" data-rule-maxlength="50" />

                                    </div>
                                </div>
                            </div>
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="swiftcode">
                                    Swift Code
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="swiftCode" name="swiftCode" data-bind="value: $root.SwiftCode, disable: $root.Readonly" class="col-xs-8" 

data-rule-maxlength="50" />
                                    </div>
                                </div>
                            </div>
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="bankAccount">
                                    Bank Account
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="code" name="bankAccount" data-bind="value: $root.BankAccount, disable: $root.Readonly" class="col-xs-8" 

data-rule-maxlength="50" />
                                    </div>
                                </div>
                            </div>
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="description">
                                    Description
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="description" name="description" data-bind="value: $root.Description, disable: $root.Readonly" 

class="col-xs-10" data-rule-required="true" data-rule-maxlength="255" />
                                        <label class="control-label bolder text-danger" for="Description">*</label>
                                    </div>
                                </div>
                            </div>
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="commonName">
                                    Common Name
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="commonName" name="commonName" data-bind="value: $root.CommonName, disable: $root.Readonly" class="col-

xs-10" data-rule-maxlength="255" />
                                    </div>
                                </div>
                            </div>

                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="currency">
                                    Currency
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="currency" name="currency" data-bind="value: $root.Currency, options: ddlCurrency, optionsValue: 'ID', optionsText:

'CodeDescription', optionsCaption: 'Please Select...', disable: $root.Readonly, event: { change: $root.OnChangeCurrency }, visible: $root.Readonly() == false" 

class="col-xs-8"></select>
                                        <select id="currencyhidden" name="currencyhidden" data-bind="value: $root.Currency, options: ddlCurrency, optionsValue: 'ID',

    optionsText: 'CodeDescription', optionsCaption: 'Please Select...', visible: $root.Readonly() == true, disable: $root.Readonly" class="col-xs-8"></select>
                                    </div>
                                </div>
                            </div>

                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="PGSL">
                                    PGSL
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="PGSL" name="PGSL" data-bind="value: $root.PGSL, disable: $root.Readonly" class="col-xs-10 col-sm-5" 

data-rule-maxlength="50" />
                                    </div>
                                </div>
                            </div>

                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="checkbox">
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <div>
                                            <input class="ace" id="RTGS" name="RTGS" type="checkbox" data-bind="checked: $root.IsRTGS, disable: $root.Readonly" 

title="IsRTGS" /><span class="lbl">RTGS</span>
                                        </div>
                                        <div>
                                            <input class="ace" id="SKN" name="SKN" type="checkbox" data-bind="checked: $root.IsSKN, disable: $root.Readonly" 

title="IsSKN" /><span class="lbl">SKN</span>
                                        </div>
                                        <div>
                                            <input class="ace" id="OTT" name="OTT" type="checkbox" data-bind="checked: $root.IsOTT, disable: $root.Readonly" 

title="IsOTT" /><span class="lbl">OTT</span>
                                        </div>
                                        <div>
                                            <input class="ace" id="OVB" name="OVB" type="checkbox" data-bind="checked: $root.IsOVB, disable: $root.Readonly" 

title="IsOVB" /><span class="lbl">OVB</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" style="margin-left: 20px; margin-right: 20px">
                                <div id="widget-boxs" class="widget-box" data-bind="visible: !$root.IsNewData()">
                                    <!-- widget header start -->
                                    <div class="widget-header widget-hea1der-small header-color-dark">
                                        <h6>Branch Bank </h6>
                                        <div class="widget-toolbar">
                                            <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                <i class="blue icon-filter"></i>
                                                <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridBranchBankProperties().AllowFilter" 

/>
                                                <span class="lbl"></span>
                                            </label>
                                            <a href="#" data-bind="click: $root.GetDataBranchBank" class="tooltip-info" data-rel="tooltip" data-placement="top" 

title="Reload Content">
                                                <i class="blue icon-refresh"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- widget header end -->
                                    <!-- widget body start -->
                                    <div class="widget-body">
                                        <!-- widget main start -->
                                        <div class="widget-main padding-0">
                                            <!-- widget slim control start -->
                                            <div class="slim-scroll" data-height="400">
                                                <!-- widget content start -->
                                                <div class="content">
                                                    <!-- table responsive start -->
                                                    <div class="table-responsive">
                                                        <div class="dataTables_wrapper" role="grid">
                                                            <table id="BranchBank-table" class="table table-striped table-bordered table-hover dataTable">
                                                                <!--visible:$root.TableBranch()-->
                                                                <thead>
                                                                    <tr data-bind="with: GridBranchBankProperties">
                                                                        <th style="width: 50px">No.</th>
                                                                        <th data-bind="click: function () { Sorting('BranchCode'); }, css: GetSortedColumn

('BranchCode')">Branch Code</th>
                                                                        <th data-bind="click: function () { Sorting('BankAccount'); }, css: GetSortedColumn

('BankAccount')">Bank A/C</th>
                                                                        <th data-bind="click: function () { Sorting('CommonName'); }, css: GetSortedColumn

('CommonName')">Common Name</th>
                                                                        <!--<th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn

('Currency')">Currency</th>-->
                                                                        <th data-bind="click: function () { Sorting('PGSL'); }, css: GetSortedColumn('PGSL')">PGSL</th>
                                                                        <th data-bind="click: function () { Sorting('City'); }, css: GetSortedColumn('City')">City 

</th>
                                                                        <th>Action </th>

                                                                        <!--<th data-bind="click: function () { Sorting('Code'); }, css: GetSortedColumn('Code')">Bank 

Code </th>
                                                                        <th data-bind="click: function () { Sorting('SwiftCode'); }, css: GetSortedColumn

('SwiftCode')">Swift Code</th>
                                                                        <th data-bind="click: function () { Sorting('LastModifiedBy'); }, css: GetSortedColumn

('LastModifiedBy')">Modified By</th>
                                                                         <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn

('LastModifiedDate')">Modified Date</th>-->
                                                                    </tr>
                                                                </thead>
                                                                <thead data-bind="visible: GridBranchBankProperties().AllowFilter" class="table-filter">
                                                                    <tr>
                                                                        <th class="clear-filter">
                                                                            <a href="#" data-bind="click: ClearFilterBranchBank" class="tooltip-info" data-

rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                <i class="green icon-trash"></i>
                                                                            </a>
                                                                        </th>
                                                                        <th>
                                                                            <input type="text" class="input-sm col-xs-12" data-bind="value:

$root.FilterBranch_BranchCode, event: { change: GridBranchBankProperties().Filter }" /></th>
                                                                        <th>
                                                                            <input type="text" class="input-sm col-xs-12" data-bind="value:

$root.FilterBranch_BankAccount, event: { change: GridBranchBankProperties().Filter }" /></th>
                                                                        <th>
                                                                            <input type="text" class="input-sm col-xs-12" data-bind="value:

$root.FilterBranch_CommonName, event: { change: GridBranchBankProperties().Filter }" /></th>
                                                                        <th>
                                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCurrency,

    event: { change: GridBranchBankProperties().Filter }" /></th>
                                                                        <th>
                                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterBranch_PGSL,

    event: { change: GridBranchBankProperties().Filter }" /></th>
                                                                        <th>
                                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCity, event: {

    change: GridBranchBankProperties().Filter
}" /></th>
                                                                        <th></th>

                                                                        <!--<th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterSwiftCode, 

event: { change: GridProperties().Filter }" /></th>
                                                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterDescription, 

event: { change: GridProperties().Filter }" /></th>
                                                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterModifiedBy, 

event: { change: GridProperties().Filter }" /></th>
                                                                        <th><input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" 

data-bind="value: $root.FilterModifiedDate, event: { change: GridProperties().Filter }" /></th>-->
                                                                    </tr>
                                                                </thead>
                                                                <tbody data-bind="foreach: BranchBanks, visible: BranchBanks().length > 0">
                                                                    <tr data-bind="click: $root.GetSelectedRowBranchBank" data-toggle="modal">
                                                                        <td><span data-bind="text: RowID"></span></td>
                                                                        <td><span data-bind="text: BranchCode"></span></td>
                                                                        <td><span data-bind="text: BankAccount"></span></td>
                                                                        <td><span data-bind="text: CommonName"></span></td>
                                                                        <!--<td><span data-bind="text: Currency"></span></td>-->
                                                                        <td><span data-bind="text: PGSL"></span></td>
                                                                        <td><span data-bind="text: City"></span></td>
                                                                        <td></td>
                                                                    </tr>
                                                                </tbody>
                                                                <tbody data-bind="visible: BranchBanks().length == 0">
                                                                    <tr>
                                                                        <td colspan="7" class="text-center">No entries available.
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!-- table responsive end -->
                                                </div>
                                                <!-- widget content end -->
                                            </div>
                                            <!-- widget slim control end -->
                                            <!-- widget footer start -->
                                            <div class="widget-toolbox padding-8 clearfix">
                                                <div class="row" data-bind="with: GridBranchBankProperties">
                                                    <!-- pagination size start -->
                                                    <div class="col-sm-6">
                                                        <div class="dataTables_paginate paging_bootstrap pull-left">
                                                            Showing
                                                            <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" 

class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                            rows of <span data-bind="text: Total"></span>
                                                            entries
                                                        </div>
                                                    </div>
                                                    <!-- pagination size end -->
                                                    <!-- pagination page jump start -->
                                                    <div class="col-sm-3">
                                                        <div class="dataTables_paginate paging_bootstrap">
                                                            Page
                                                            <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" 

class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                            of <span data-bind="text: TotalPages"></span>
                                                        </div>
                                                    </div>
                                                    <!-- pagination page jump end -->
                                                    <!-- pagination navigation start -->
                                                    <div class="col-sm-3">
                                                        <div class="dataTables_paginate paging_bootstrap">
                                                            <ul class="pagination">
                                                                <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                        <i class="icon-double-angle-left"></i>
                                                                    </a>
                                                                </li>
                                                                <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                        <i class="icon-angle-left"></i>
                                                                    </a>
                                                                </li>
                                                                <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                        <i class="icon-angle-right"></i>
                                                                    </a>
                                                                </li>
                                                                <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                        <i class="icon-double-angle-right"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <!-- pagination navigation end -->

                                                </div>
                                            </div>
                                            <!-- widget footer end -->
                                        </div>
                                        <!-- widget main end -->
                                    </div>
                                    <!-- widget body end -->
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <!-- modal body end -->
            <!-- modal footer start -->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.save, visible: $root.IsNewData() && $root.IsRoleMaker()">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm btn-primary" data-bind="click: $root.update, visible: !$root.IsNewData() && $root.IsRoleMaker()">
                    <i class="icon-edit"></i>
                    Update
                </button>
                <button class="btn btn-sm btn-warning" data-bind="click: $root.delete, visible: !$root.IsNewData() && $root.IsRoleMaker()">
                    <i class="icon-trash"></i>
                    Delete
                </button>
                <button class="btn btn-sm" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Close
                </button>
            </div>
            <!-- modal footer end -->
        </div>
    </div>
</div>
<!-- modal form end -->
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
