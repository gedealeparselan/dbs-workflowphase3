﻿using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Web;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("MatrixDOA")]
    public class MatrixDOAModel
    {
        public int ID { get; set; }
        public int ProductID { get; set; }

        public string ProductName { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }

        public string ApprovalDOAHeader { get; set; }

        public string MatrixCondition { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public string LastModifiedBy { get; set; }

        public IList<MatrixDOAApproverModel> ApproverDOA { get; set; }
        public IList<MatrixDOADetailModel> MatrixDetailDOA { get; set; }

    }

    public class MatrixDOAApproverModel
    {
        public long ID { get; set; }
        public int MatrixID { get; set; }

        public string ApproverPosition { get; set; }
        public bool IsDeleted { get; set; }
        public int ApproverCount { get; set; }
        public string Relation { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }

    public class MatrixDOADetailModel
    {
        public long ID { get; set; }
        public int MatrixID { get; set; }
        public int MatrixFieldID { get; set; }
        public int OperatorID { get; set; }
        public string Value1 { get; set; }
        public string Value1Id { get; set; }
        public bool IsFieldComparer { get; set; }
        public string Value2 { get; set; }
        public string Value2Id { get; set; }
        public string Relation { get; set; }
        public int? LevelRelation { get; set; }
        public int? OrderId { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }

    }


    public class MatrixDOARow : MatrixDOAModel
    {
        public int RowID { get; set; }

    }

    public class MatrixDOAGrid : Grid
    {
        public IList<MatrixDOARow> Rows { get; set; }
    }
    #endregion

    #region Filter
    public class MatrixDOAFilter : Filter { }
    #endregion

    #region Interface
    public interface IMatrixDOARepository : IDisposable
    {
        bool GetMatrixDOAByID(int id, ref MatrixDOAModel matrixDOA, ref string message);
        bool GetMatrixDOA(ref IList<MatrixDOAModel> Matrices, int limit, int index, ref string message);
        bool GetMatrixDOA(ref IList<MatrixDOAModel> Matrices, ref string message);
        bool GetMatrixDOA(int page, int size, IList<MatrixDOAFilter> filters, string sortColumn, string sortOrder, ref MatrixDOAGrid matrix, ref string message);
        bool GetMatrixDOA(MatrixFilter filter, ref IList<MatrixDOAModel> Matrices, ref string message);
        bool GetMatrixDOA(string key, int limit, ref IList<MatrixDOAModel> Matrices, ref string message);
        bool AddMatrixDOA(MatrixDOAModel matrix, ref string message);
        bool UpdateMatrixDOA(int id, MatrixDOAModel matrix, ref string message);
        bool DeleteMatrixDOA(int id, ref string message);
        bool GetParameters(ref IList<MatrixDOA> parameters, ref string message);

    }
    #endregion

    #region Repository
    public class MatrixDOARepository : IMatrixDOARepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetParameters(ref IList<MatrixDOA> parameters, ref string message)
        {
            bool isSuccess = false;

            try
            {
                parameters = (from a in context.MatrixDOAs select a).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetMatrixDOAByID(int id, ref MatrixDOAModel matrixDOA, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetMatrixDOA(ref IList<MatrixDOAModel> Matrices, int limit, int index, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetMatrixDOA(ref IList<MatrixDOAModel> Matrices, ref string message)
        {
            bool isSuccess = false;

            try
            {

                using (DBSEntities context = new DBSEntities())
                {
                    var result = (from a in context.MatrixDOAs
                                  join d in context.Products on a.ProductID equals d.ProductID
                                  where a.IsDeleted == false
                                  orderby new { a.MatrixName }
                                  select new MatrixDOAModel
                                  {
                                      ID = a.MatrixID,
                                      Name = a.MatrixName,
                                      Description = a.MatrixDescription,
                                      ApprovalDOAHeader = a.ApprovalDOAHeader,
                                      ProductID = a.ProductID,
                                      ProductName = d.ProductName,
                                      LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                      LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                      ApproverDOA = context.MatrixDOAApprovers.Where(b => b.MatrixID == a.MatrixID && b.IsDeleted == false).Select(b => new MatrixDOAApproverModel
                                      {
                                          ID = b.MatrixApproverID,
                                          MatrixID = b.MatrixID,
                                          ApproverPosition = b.ApproverPosition,
                                          ApproverCount = b.ApproverCount.Value
                                      }).ToList(),
                                      MatrixDetailDOA = context.MatrixDOADetails.Where(c => c.MatrixID == a.MatrixID && c.IsDeleted == false).Select(c => new MatrixDOADetailModel
                                      {
                                          ID = c.MatrixDetailID,
                                          MatrixID = c.MatrixID,
                                          MatrixFieldID = c.MatrixFieldID,
                                          OperatorID = c.OperatorID,
                                          Value1 = c.Value1,
                                          Value1Id = c.Value1Id,
                                          IsFieldComparer = c.IsFieldComparer.Value,
                                          Value2 = c.Value2,
                                          Value2Id = c.Value2Id,
                                          Relation = c.Relation,
                                          LevelRelation = c.LevelRelation.Value,
                                          OrderId = c.OrderId.Value
                                      }).ToList()
                                  }).ToList();
                    Matrices = result.ToList<MatrixDOAModel>();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetMatrixDOA(MatrixFilter filter, ref IList<MatrixDOAModel> Matrices, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetMatrixDOA(string key, int limit, ref IList<MatrixDOAModel> Matrices, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool AddMatrixDOA(MatrixDOAModel matrix, ref string message)
        {
            bool isSuccess = false;
            string strvalue = string.Empty;
            if (matrix != null)
            {
                try
                {
                    if (!context.MatrixDOAs.Where(a => a.MatrixID.Equals(matrix.ID) &&
                                                               a.IsDeleted == false).Any())
                    {
                        using (DBSEntities context_ts = new DBSEntities())
                        {
                            using (TransactionScope ts = new TransactionScope())
                            {
                                try
                                {
                                    Entity.MatrixDOA md = new Entity.MatrixDOA()
                                    {
                                        MatrixName = matrix.Name,
                                        MatrixDescription = matrix.Description,
                                        ProductID = matrix.ProductID,
                                        ApprovalDOAHeader = matrix.ApprovalDOAHeader,
                                        IsDeleted = false,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName
                                    };
                                    context_ts.MatrixDOAs.Add(md);
                                    context_ts.SaveChanges();
                                    
                                    int mID = md.MatrixID;
                                    List<Entity.MatrixDOADetail> Detail = (from d in matrix.MatrixDetailDOA
                                                                           select new Entity.MatrixDOADetail()
                                                                           {
                                                                               MatrixID = mID,
                                                                               OrderId = d.OrderId,
                                                                               OperatorID = d.OperatorID,
                                                                               MatrixFieldID = d.MatrixFieldID,
                                                                               IsFieldComparer = d.IsFieldComparer,
                                                                               Value1 = d.Value1,
                                                                               Relation = d.Relation,
                                                                               LevelRelation = d.LevelRelation,
                                                                               IsDeleted = false,
                                                                               CreateDate = DateTime.UtcNow,
                                                                               CreateBy = currentUser.GetCurrentUser().LoginName
                                                                           }).ToList();
                                    context_ts.MatrixDOADetails.AddRange(Detail);
                                    context_ts.SaveChanges();

                                    List<Entity.MatrixDOAApprover> Approver = (from d in matrix.ApproverDOA
                                                                               select new Entity.MatrixDOAApprover()
                                                                             {
                                                                                 MatrixID = mID,
                                                                                 ApproverPosition = d.ApproverPosition,
                                                                                 ApproverCount = d.ApproverCount,
                                                                                 Relation = d.Relation,
                                                                                 IsDeleted = false,
                                                                                 CreateDate = DateTime.UtcNow,
                                                                                 CreateBy = currentUser.GetCurrentUser().LoginName
                                                                             }).ToList();
                                    context_ts.MatrixDOAApprovers.AddRange(Approver);
                                    context_ts.SaveChanges();

                                    string UpdateMatrix = context.SP_WF_GenerateMatrixDOACondition(mID).Select(a => a).SingleOrDefault();

                                    if (UpdateMatrix != null)
                                    {
                                        Entity.MatrixDOA TMO = context_ts.MatrixDOAs.Where(a => a.MatrixID.Equals(mID)).SingleOrDefault();
                                        if (TMO != null)
                                        {
                                            TMO.MatrixCondition = UpdateMatrix;
                                            context_ts.SaveChanges();
                                        }
                                    }

                                    ts.Complete();
                                    isSuccess = true;
                                }
                                catch (Exception ex)
                                {
                                    ts.Dispose();
                                    message = ex.Message;
                                }
                            }
                        }
                    }
                    else
                    {
                        message = string.Format("Exception Handling data for Exception Handling {0},{1} is already exist.", matrix.Name, matrix.ApprovalDOAHeader);
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }

            return isSuccess;
        }

        public bool UpdateMatrixDOA(int id, MatrixDOAModel matrix, ref string message)
        {
            bool isSuccess = false;
            string strvalue = string.Empty;

            try
            {
                Entity.MatrixDOA data = context.MatrixDOAs.Where(a => a.MatrixID.Equals(id)).SingleOrDefault();

                if (data != null)
                {

                    using (TransactionScope ts = new TransactionScope())
                    {
                        try
                        {
                            data.ApprovalDOAHeader = matrix.ApprovalDOAHeader;
                            data.MatrixName = matrix.Name;
                            data.ProductID = matrix.ProductID;
                            data.UpdateDate = DateTime.UtcNow;
                            data.UpdateBy = currentUser.GetCurrentUser().LoginName;
                            context.SaveChanges();

                            var datadetail = context.MatrixDOADetails.Where(a => a.MatrixID.Equals(id) && a.IsDeleted == false).ToList();
                            if (datadetail != null && datadetail.Count > 0)
                            {
                                datadetail.ForEach(a =>
                                {
                                    a.IsDeleted = true;

                                }
                                );
                                context.SaveChanges();
                            }

                            List<Entity.MatrixDOADetail> Detail = (from d in matrix.MatrixDetailDOA
                                                                   select new Entity.MatrixDOADetail()
                                                                   {
                                                                       MatrixID = id,
                                                                       OrderId = d.OrderId,
                                                                       OperatorID = d.OperatorID,
                                                                       MatrixFieldID = d.MatrixFieldID,
                                                                       IsFieldComparer = d.IsFieldComparer,
                                                                       Value1 = d.Value1,
                                                                       Relation = d.Relation,
                                                                       IsDeleted = false,
                                                                       CreateDate = DateTime.UtcNow,
                                                                       CreateBy = currentUser.GetCurrentUser().LoginName
                                                                   }).ToList();
                            context.MatrixDOADetails.AddRange(Detail);
                            context.SaveChanges();

                            var dataApprover = context.MatrixDOAApprovers.Where(a => a.MatrixID.Equals(id) && a.IsDeleted == false).ToList();
                            if (dataApprover != null && dataApprover.Count > 0)
                            {
                                dataApprover.ForEach(a =>
                                {
                                    a.IsDeleted = true;

                                }
                                );
                                context.SaveChanges();
                            }

                            List<Entity.MatrixDOAApprover> Approver = (from d in matrix.ApproverDOA
                                                                       select new Entity.MatrixDOAApprover()
                                                                     {
                                                                         MatrixID = id,
                                                                         ApproverPosition = d.ApproverPosition,
                                                                         ApproverCount = d.ApproverCount,
                                                                         Relation = d.Relation,
                                                                         IsDeleted = false,
                                                                         CreateDate = DateTime.UtcNow,
                                                                         CreateBy = currentUser.GetCurrentUser().LoginName
                                                                     }).ToList();
                            context.MatrixDOAApprovers.AddRange(Approver);
                            context.SaveChanges();

                            using (context)
                            {
                                using (context.Database.Connection)
                                {
                                    context.Database.Connection.Open();
                                    DbCommand cmd = context.Database.Connection.CreateCommand();
                                    cmd.CommandText = "SP_WF_GenerateMatrixDOACondition";
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.Add(new SqlParameter("MatrixDOAID", id));
                                    cmd.ExecuteNonQuery();
                                    context.Database.Connection.Close();

                                }

                            }


                            ts.Complete();
                            isSuccess = true;
                        }
                        catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                        {
                            Exception raise = dbEx;
                            foreach (var validationErrors in dbEx.EntityValidationErrors)
                            {
                                foreach (var validationError in validationErrors.ValidationErrors)
                                {
                                    string _message = string.Format("{0}:{1}",
                                        validationErrors.Entry.Entity.ToString(),
                                        validationError.ErrorMessage);
                                    // raise a new exception nesting
                                    // the current instance as InnerException
                                    raise = new InvalidOperationException(_message, raise);
                                }
                            }
                            throw raise;
                        }
                        catch (Exception ex)
                        {
                            ts.Dispose();
                            message = ex.Message;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool DeleteMatrixDOA(int id, ref string message)
        {
            bool isSuccess = false;
            string strvalue = string.Empty;

            try
            {
                Entity.MatrixDOA data = context.MatrixDOAs.Where(a => a.MatrixID.Equals(id)).SingleOrDefault();

                if (data != null)
                {

                    using (TransactionScope ts = new TransactionScope())
                    {
                        try
                        {
                            data.UpdateDate = DateTime.UtcNow;
                            data.UpdateBy = currentUser.GetCurrentUser().LoginName;
                            data.IsDeleted = true;
                            context.SaveChanges();

                            var datadetail = context.MatrixDOADetails.Where(a => a.MatrixID.Equals(id) && a.IsDeleted == false).ToList();
                            if (datadetail != null && datadetail.Count > 0)
                            {
                                datadetail.ForEach(a =>
                                {
                                    a.IsDeleted = true;

                                }
                                );
                                context.SaveChanges();
                            }

                            var dataApprover = context.MatrixDOAApprovers.Where(a => a.MatrixID.Equals(id) && a.IsDeleted == false).ToList();
                            if (dataApprover != null && dataApprover.Count > 0)
                            {
                                dataApprover.ForEach(a =>
                                {
                                    a.IsDeleted = true;

                                }
                                );
                                context.SaveChanges();
                            }

                            ts.Complete();
                            isSuccess = true;
                        }
                        catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                        {
                            Exception raise = dbEx;
                            foreach (var validationErrors in dbEx.EntityValidationErrors)
                            {
                                foreach (var validationError in validationErrors.ValidationErrors)
                                {
                                    string _message = string.Format("{0}:{1}",
                                        validationErrors.Entry.Entity.ToString(),
                                        validationError.ErrorMessage);
                                    // raise a new exception nesting
                                    // the current instance as InnerException
                                    raise = new InvalidOperationException(_message, raise);
                                }
                            }
                            throw raise;
                        }
                        catch (Exception ex)
                        {
                            ts.Dispose();
                            message = ex.Message;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public bool GetMatrixDOA(int page, int size, IList<MatrixDOAFilter> filters, string sortColumn, string sortOrder, ref MatrixDOAGrid matrixGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                MatrixDOAModel filter = new MatrixDOAModel();
                filter.Description = string.Empty;

                if (filters != null)
                {
                    filter.ProductName = (string)filters.Where(a => a.Field.Equals("ProductName")).Select(a => a.Value).SingleOrDefault();
                    filter.Name = (string)filters.Where(a => a.Field.Equals("MatrixName")).Select(a => a.Value).SingleOrDefault();
                    //filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.ApprovalDOAHeader = (string)filters.Where(a => a.Field.Equals("ApprovalDOAHeader")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.MatrixDOAs
                                join b in context.Products on a.ProductID equals b.ProductID
                                let isProdcutName = !string.IsNullOrEmpty(filter.ProductName)
                                let isMatrixName = !string.IsNullOrEmpty(filter.Name)
                                //let isDescription = !string.IsNullOrEmpty(filter.Description)
                                let isApprovalDOAHeader = !string.IsNullOrEmpty(filter.ApprovalDOAHeader)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted == false &&
                                    //(isDescription ? a.MatrixDescription.Contains(filter.Description) : true) &&
                                    (isProdcutName ? b.ProductName.Contains(filter.ProductName) : true) &&
                                    (isMatrixName ? a.MatrixName.Contains(filter.Name) : true) &&
                                    (isApprovalDOAHeader ? a.ApprovalDOAHeader.Contains(filter.ApprovalDOAHeader) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new MatrixDOAModel
                                {
                                    ID = a.MatrixID,
                                    ProductID = a.ProductID,
                                    ProductName = b.ProductName,
                                    Name = a.MatrixName,
                                    ApprovalDOAHeader = a.ApprovalDOAHeader,
                                    Description = a.MatrixDescription,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    matrixGrid.Page = page;
                    matrixGrid.Size = size;
                    matrixGrid.Total = data.Count();
                    matrixGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    matrixGrid.Rows = data.AsEnumerable()
                        .Select((a, i) => new MatrixDOARow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            ProductID = a.ProductID,
                            ProductName = a.ProductName,
                            Name = a.Name,
                            ApprovalDOAHeader = a.ApprovalDOAHeader,
                            Description = a.Description,
                            MatrixDetailDOA = context.MatrixDOADetails.Where(e => e.MatrixID == a.ID && e.IsDeleted == false).Select(e => new MatrixDOADetailModel
                            {
                                ID = e.MatrixDetailID,
                                IsFieldComparer = e.IsFieldComparer.Value,
                                LevelRelation = e.LevelRelation,
                                MatrixFieldID = e.MatrixFieldID,
                                MatrixID = e.MatrixID,
                                OperatorID = e.OperatorID,
                                OrderId = e.OrderId,
                                Relation = e.Relation,
                                Value1 = e.Value1,
                                Value1Id = e.Value1Id,
                                Value2 = e.Value2,
                                Value2Id = e.Value2Id
                            }).ToList(),
                            ApproverDOA = context.MatrixDOAApprovers.Where(p => p.MatrixID == a.ID && p.IsDeleted == false).Select(p => new MatrixDOAApproverModel
                            {
                                ID = p.MatrixApproverID,
                                ApproverPosition = p.ApproverPosition,
                                ApproverCount = p.ApproverCount.Value,
                                Relation = p.Relation

                            }).ToList(),
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
    }
    #endregion
}