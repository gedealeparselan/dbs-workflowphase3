var accessToken;
var $box;
var $remove = false;

var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;

    //Declare observable which will be bind with UI
    self.CIF = ko.observable();
    self.Name = ko.observable();
    self.POAName = ko.observable();
    //self.BizSegment = ko.observable();
    self.BizSegmentID = ko.observable();
    //self.Branch = ko.observable();
    self.BranchID = ko.observable();
    //self.Type = ko.observable();
    self.TypeID = ko.observable();
    self.RMCode = ko.observable();
    self.RMName = ko.observable();
    self.LastModifiedBy = ko.observable();
    self.LastModifiedDate = ko.observable();

    // Customer Function
    self.CustomerFunctionID = ko.observable();

    // filter
    self.FilterCIF = ko.observable("");
    self.FilterName = ko.observable("");
    self.FilterPOAName = ko.observable("");
    self.FilterBizSegment = ko.observable("");
    self.FilterBranch = ko.observable("");
    self.FilterType = ko.observable("");
    self.FilterLastModifiedBy = ko.observable("");
    self.FilterLastModifiedDate = ko.observable("");

    // sorting
    self.SortColumn = ko.observable("CIF");
    self.SortOrder = ko.observable("ASC");

    // grid properties
    self.allowFilter = ko.observable(false);
    self.availableSizes = ko.observableArray([10, 25, 50, 75, 100]);
    self.Page = ko.observable(1);
    self.Size = ko.observableArray([10]);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    // New Customer flag
    self.IsNewCustomer = ko.observable(false);
    self.IsNewCustomerFunction = ko.observable(false);
    self.IsFunctionVisible = ko.observable(false);
    self.IsNewCustomerContact = ko.observable(false);
    self.IsNewCustomerUnderlying = ko.observable(false);

    //The Object which stored data entered in the observables
    var Customer = {
        CIF: self.CIF,
        Name: self.Name,
        POAName: self.POAName,
        BizSegment: self.BizSegment,
        BizSegmentID: self.BizSegmentID,
        Branch: self.Branch,
        BranchID: self.BranchID,
        Type: self.Type,
        TypeID: self.TypeID
    };

    //Declare an ObservableArray for Storing the JSON Response
    self.Customers = ko.observableArray([]);
    self.CustomerAccounts = ko.observableArray([]);
    self.CustomerFunctions = ko.observableArray([]);
    self.CustomerContacts = ko.observableArray([]);
    self.CustomerUnderlyings = ko.observableArray([]);

    // parameters
    self.Branchs = ko.observableArray([]);
    self.BizSegments = ko.observableArray([]);
    self.Types = ko.observableArray([]);
    self.POAFunctions = ko.observableArray([]);

    // get all parameters need
    function GetParameters(){
        // widget reloader function start
        if($box.css('position') == 'static') {$remove = true; $box.addClass('position-relative');}
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function() {
            $box.find('.widget-box-overlay').remove();
            if($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        var options = {
            url: api.server + api.url.parameter,
            params: {
                select: "Branch,BizSegment,CustomerType,POAFunction"
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
    }

    // bind get Customers function to view
    self.GetCustomers = function () {
        GetCustomers();
    }
    self.GetParameters = function(){
        GetParameters();
    }

    // event handlers
    self.CustomerFunctionAdd = function(){
        self.IsFunctionVisible(true);
        self.IsNewCustomerFunction(true);

        self.CustomerFunctionID(null);
    };

    self.CustomerFunctionEdit = function(data){
        // validate if editor is active
        if(self.IsFunctionVisible()){
            var action = self.IsNewCustomerFunction() ? "New" : "Modify";
            if(!confirm("Editor for "+ action + " Function is active, Do you want discard the changes?")){
                return false;
            }
        }

        self.IsFunctionVisible(true);
        self.IsNewCustomerFunction(false);

        self.CustomerFunctionID(data.ID);
    };

    self.CustomerFunctionSave = function(){
        self.IsFunctionVisible(false);
        self.IsNewCustomerFunction(false);

        self.CustomerFunctionID(null);
    };

    self.CustomerFunctionUpdate = function(){
        var custFunction = {
            ID: self.CustomerFunctionID()
        };

        var options = {
            url: api.server + api.url.customer + "/" + self.CIF() + api.url.customerFunction + "/" + self.CustomerFunctionID(),
            token: accessToken,
            data: ko.toJSON(custFunction)
        };

        Helper.Ajax.Put(options, OnSuccessUpdateFunction, OnError, null);

        // set flag
        self.IsFunctionVisible(false);
        self.IsNewCustomerFunction(false);

        // clearing objects
        self.CustomerFunctionID(null);
    }

    self.CustomerFunctionDiscard = function(){
        self.IsFunctionVisible(false);
        self.IsNewCustomerFunction(false);

        self.CustomerFunctionID(null);
    };

    self.CustomerFunctionDelete = function(){
        self.IsFunctionVisible(false);
        self.IsNewCustomerFunction(false);
    };

    self.AddCustomerContact = function(){
        alert("jh")
    }

    self.AddCustomerUnderlying = function(){
        alert("jh")
    }

    self.save = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            //Ajax call to insert the Customer
            $.ajax({
                type: "POST",
                url: api.server + api.url.customer,
                data: ko.toJSON(Customer), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        // hide current popup window
                        $("#modal-form").modal('hide');

                        // refresh data
                        GetCustomers();
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }
    };

    self.update = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            // hide current popup window
            $("#modal-form").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.customer + "/" + Customer.CIF(),
                        data: ko.toJSON(Customer),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            // send notification
                            if (jqXHR.status = 200) {
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // refresh data
                                GetCustomers();
                            } else
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.delete = function () {
        // hide current popup window
        $("#modal-form").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customer + "/" + Customer.CIF(),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        if (jqXHR.status = 200) {
                            // send notification
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetCustomers();
                        } else {
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    //Function to Read All Customers
    function GetCustomers() {
        // widget reloader function start
        if($box.css('position') == 'static') {$remove = true; $box.addClass('position-relative');}
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function() {
            $box.find('.widget-box-overlay').remove();
            if($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // define filter
        var filters = [];
        if (self.FilterCIF() != "") filters.push({ Field: 'CIF', Value: self.FilterCIF() });
        if (self.FilterName() != "") filters.push({ Field: 'Name', Value: self.FilterName() });
        if (self.FilterPOAName() != "") filters.push({ Field: 'POAName', Value: self.FilterPOAName() });
        if (self.FilterBizSegment() != "") filters.push({ Field: 'BizSegment', Value: self.FilterBizSegment() });
        if (self.FilterBranch() != "") filters.push({ Field: 'Branch', Value: self.FilterBranch() });
        if (self.FilterType() != "") filters.push({ Field: 'Type', Value: self.FilterType() });
        if (self.FilterLastModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterLastModifiedBy() });
        if (self.FilterLastModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterLastModifiedDate() });

        //Ajax Call Get All Customer Records
        /*$.ajax({
            type: "POST",
            url: api.server + api.url.customer + "?page=" + self.Page() + "&size=" + self.Size() + "&sort_column=" + self.SortColumn() + "&sort_order=" + self.SortOrder(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: ko.toJSON(filters),
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            crossDomain: true,
            cache: false,
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.Customers(data.Rows); //Put the response in ObservableArray

                    self.Page(data['Page']);
                    self.Size([data['Size']]);
                    self.Total(data['Total']);
                    self.TotalPages(Math.ceil(self.Total() / self.Size()));
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
            }
        });*/

        var options = {
            url:api.server + api.url.customer,
            params: {
                page: self.Page(),
                size: self.Size(),
                sort_column: self.SortColumn(),
                sort_order: self.SortOrder()
            },
            token: accessToken
        };

        // define method GET / POST
        if(filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        }
        else
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
    }

    //Function to Display record to be updated
    self.getSelectedCustomer = function (data) {
        // reset validation
        var validator = $("#aspnetForm").validate();
        validator.resetForm();

        // clear has-error class
        $('.form-group').removeClass('has-error');

        // fill properties
        self.IsNewCustomer(false);

        self.CIF(data.CIF);
        self.Name(data.Name);
        self.POAName(data.POAName);
        //self.BizSegment(data.BizSegment);
        self.BizSegmentID(data.BizSegment.ID);
        //self.Branch(data.Branch);
        self.BranchID(data.Branch.ID);
        //self.Type(data.Type);
        self.TypeID(data.Type.ID);
        if(data.RM != null) {
            self.RMCode(data.RM.ID);
            self.RMName(data.RM.Name);
        }

        // fill Observable Arrays object
        self.CustomerAccounts(data.Accounts);
        self.CustomerFunctions(data.Functions);
        self.CustomerContacts(data.Contacts);
        self.CustomerUnderlyings(data.Underlyings);

        $("#modal-form").modal('show');
    };

    //insert new
    self.newCustomer = function () {
        //self.Employees.push({ CIF: "f", Name: "dsd" });

        // flag as new Customer
        self.IsNewCustomer(true);

        // bind empty data
        self.CIF('');
        self.Name('');
    };

    self.onPageSizeChange = function () {
        self.Page(1);

        GetCustomers();
    };

    self.onPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        GetCustomers();
    };

    self.nextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            GetCustomers();
        }
    }

    self.previousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            GetCustomers();
        }
    }

    self.firstPage = function () {
        self.Page(1);

        GetCustomers();
    }

    self.lastPage = function () {
        self.Page(self.TotalPages());

        GetCustomers();
    }

    self.filter = function () {
        self.Page(1);

        GetCustomers();
    }

    self.sorting = function (column) {
        //alert(column)

        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        GetCustomers();
    }

    // Local Date
    self.LocalDate = function (date) {
        if (moment(date).isValid()) {
            var localDate = new Date(date);

            return moment(localDate).format(config.format.date);
        } else {
            return ""
        }
    }

    // bind clear filters
    self.ClearFilters = function () {
        self.FilterCIF("");
        self.FilterName("");
        self.FilterPOAName("");
        self.FilterBizSegment("");
        self.FilterBranch("");
        self.FilterType("");
        self.FilterLastModifiedBy("");
        self.FilterLastModifiedDate("");

        GetCustomers();
    }

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    }

    function OnSuccessGetParameters(data, textStatus, jqXHR){
        if(jqXHR.status = 200){
            // bind result to observable array
            self.Branchs(data.Branch);
            self.BizSegments(data.BizSegment);
            self.Types(data.CustomerType);
            self.POAFunctions(data.POAFunction);
        }else{
            ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    function OnSuccessGetData(data, textStatus, jqXHR){
        if (jqXHR.status = 200) {
            self.Customers(data.Rows); //Put the response in ObservableArray

            self.Page(data['Page']);
            self.Size([data['Size']]);
            self.Total(data['Total']);
            self.TotalPages(Math.ceil(self.Total() / self.Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    function OnSuccessUpdateFunction(data, textStatus, jqXHR){
        if (jqXHR.status = 200) {
            ShowNotification("Update Function Success", jqXHR.responseJSON.Message, 'gritter-success', false);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }
};

$(document).ready(function () {
    // widget loader
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    // Knockout Bindings
    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);

    // Token Validation
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(TokenOnSuccess, TokenOnError);
    } else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // call function inside view model
        viewModel.GetParameters();
        viewModel.GetCustomers();
    }

    // Token validation On Success function
    function TokenOnSuccess(data, textStatus, jqXHR) {
        // store token on browser cookies
        $.cookie(api.cookie.name, data.AccessToken);
        accessToken = $.cookie(api.cookie.name);

        // call get data inside view model
        viewModel.GetCustomers();
    }

    // Token validation On Error function
    function TokenOnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    $('.date-picker').datepicker({autoclose: true}).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    /// block enter key from user to prevent submitted data.
    $("input").keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

            return false;
        }
    });

    //$("#aspnetForm").validate({onsubmit: false});
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,

        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });

    // autocomplete
    $("#rm-code").autocomplete({
        source: function (request, response) {
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.userapproval + "/Search",
                data: {
                    query: request.term,
                    limit: 10
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
        },
        minLength: 2,
        select: function (event, ui) {
            $("#rm-name").val(ui.item.Name);
        },
        change: function (event, ui) {
            if($("#rm-code").val() == "")
                $("#rm-name").val("");
        }
    });
});

// Function to display notification.
// class name : gritter-success, gritter-warning, gritter-error, gritter-info
function ShowNotification(title, text, className, isSticky) {
    $.gritter.add({
        title: title,
        text: text,
        class_name: className,
        sticky: isSticky
    });
}

// Event handlers declaration start
function OnSuccessAutoComplete(response, data, textStatus, jqXHR) {
    response($.map(data, function(item) {
            return {
                // default autocomplete object
                label: item.ID + " - " + item.Name + " (" + item.Email + ")",
                value: item.ID,

                // custom object binding
                ID: item.ID,
                Name: item.Name
            }
        })
    );
}

function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

function OnAlways(){
    $box.trigger('reloaded.ace.widget');
}