﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Data.Entity.Validation;
using System.Diagnostics;


namespace DBS.WebAPI.Models
{
    #region property
    [ModelName("ExceptionHandlingOperator")]
    public class ExceptionHandlingOperatorModel
    {
        /// <summary>
        /// Exception Handling Operator ID
        /// </summary>
        public int ID { get; set; }



        /// <summary>
        /// Exception Handling Operator Code
        /// </summary>
        public string Code { get; set; }


        /// <summary>
        /// Exception Handling Operator Name
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>

        public string LastModifiedBy { get; set; }
    }

    public class ExceptionHandlingOperatorRow: ExceptionHandlingOperatorModel
    {
        public int RowID { get; set; }
    }

    public class ExceptionHandlingOperatorGrid: Grid
    {
        public IList<ExceptionHandlingOperatorRow> Rows { get; set; }
    }
    #endregion

    #region Filter

    public class ExceptionHandlingOperatorFilter : Filter { }
    #endregion

    #region Interface
    public interface IExceptionHandlingOperatorRepository: IDisposable
    {
        bool GetExceptionHandlingOperator(ref IList<ExceptionHandlingOperatorModel> exceptionhandlingoperator, ref string message);
    }
    #endregion

    #region Repository
    public class ExceptionHandlingOperatorRepository: IExceptionHandlingOperatorRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetExceptionHandlingOperator(ref IList<ExceptionHandlingOperatorModel> exceptionhandlingoperator, ref string message)
        {
            bool isSuccess = false;
            try
            {

                using (DBSEntities context = new DBSEntities())
                {
                    exceptionhandlingoperator = (from a in context.ExceptionHandlingOperators
                                                  orderby new { a.Name }
                                                  select new ExceptionHandlingOperatorModel
                                                  {
                                                      ID = a.ExceptionHandlingOperatorID,
                                                      Code = a.Code,
                                                      Name = a.Name,                                  
                                                      LastModifiedBy = a.CreateBy,
                                                      LastModifiedDate = a.CreateDate
                                                  }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;

        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        currentUser = null;
                        context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}