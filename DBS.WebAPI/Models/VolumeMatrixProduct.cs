﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;
using System.Data.Entity;


namespace DBS.WebAPI.Models
{
    [ModelName("VolumeMatrixProduct")]
    [DataContract]
    public class VolumeMatrixProductModel
    {
        ///<summary>
        /// Date Period
        ///</summary>        
        public DateTime? period { get; set; }

        ///<summary>
        ///Product Name
        ///</summary>
        [DataMember]
        public string productName { get; set; }

        /// <summary>
        /// Transactions Date
        /// </summary>
        [DataMember]
        public DateTime? trxDate { get; set; }

        /// <summary>
        /// Actual Date
        /// </summary>        
        public DateTime? actualDate { get; set; }

        /// <summary>
        /// SLA Time
        /// </summary>
        public int SLATime { get; set; }

        /// <summary>
        /// TAT
        /// </summary>
        public int? TAT { get; set; }

        /// <summary>
        /// Cumulative Exceptional
        /// </summary>
        public int ExceptionalCount { get; set; }

        /// <summary>
        /// State ID  1 = In Progress, 2 = Completed, 3 = Cancelled
        /// </summary>
        public int StateID { get; set; }

        /// <summary>
        /// String Period
        /// </summary>
        [DataMember(Name = "period")]
        private string strPeriod { get; set; }

        /// <summary>
        /// String For Actual Date
        /// </summary>
        [DataMember(Name = "actualDate")]
        private string strActualDate { get; set; }

        /// <summary>
        /// String For SLA Time
        /// </summary>
        [DataMember(Name = "SLATime")]
        private string strSLATime { get; set; }

        /// <summary>
        /// String For TAT
        /// </summary>
        [DataMember(Name = "TAT")]
        private string strTAT { get; set; }


        /// <summary>
        /// String For TAT
        /// </summary>
        [DataMember(Name = "ExceptionalCount")]
        private string strExceptionalCount { get; set; }

        /// <summary>
        /// String For State ID
        /// </summary>
        /// <param name="context"></param>
        [DataMember(Name = "StateID")]
        private string strStateID { get; set; }

        [OnSerializing]
        void OnSerializing(StreamingContext context)
        {
            this.strPeriod = this.period.ToString();
            this.strActualDate = this.actualDate.ToString();
            this.strSLATime = this.SLATime.ToString();
            this.strStateID = this.StateID.ToString();
            this.strTAT = this.TAT.ToString();
            this.strExceptionalCount = this.ExceptionalCount.ToString();
        }

    }


    public interface IVolumeMatrixProduct : IDisposable
    {
        bool GetVolumeMatrixDashboardProduct(string productTypeDetailID, string isTAT, Nullable<DateTime> startDate, Nullable<DateTime> endDate, DateTime clientDateTime, string paymentMode, ref IList<VolumeMatrixProductModel> allDataProduct, ref string message);


    }
    public class VolumeMatrixProductRepository : IVolumeMatrixProduct
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetVolumeMatrixDashboardProduct(string productTypeDetailID, string isTAT, Nullable<DateTime> startDate, Nullable<DateTime> endDate, DateTime clientDateTime, string paymentMode, ref IList<VolumeMatrixProductModel> allDataProduct, ref string message)
        {
            bool isSuccess = false;
            DateTime serverUTC = DateTime.UtcNow;
            int hourDiff, oneDayHour = 24;
            string[] productTypeDetail = productTypeDetailID.Split(',');
            IList<int> productTypeDetailInt = new List<int>();

            if (clientDateTime.Hour > serverUTC.Hour)
            {
                hourDiff = clientDateTime.Hour - serverUTC.Hour;
            }
            else if (clientDateTime.Hour < serverUTC.Hour)
            {
                hourDiff = oneDayHour + clientDateTime.Hour - serverUTC.Hour;
            }
            else
            {
                hourDiff = 0;
            }

            foreach (string id in productTypeDetail)
            {
                productTypeDetailInt.Add(int.Parse(id));
            }

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    if (paymentMode == "BCP2")
                    {
                        if (isTAT == "true")
                        {

                            allDataProduct = (from products in context.Products
                                              join transactions in
                                                  (
                                                          from transaction in context.Transactions
                                                          join slas in context.SLAs
                                                          on transaction.ProductID equals slas.ProductID
                                                          join progress in context.ProgressStates
                                                          on transaction.StateID equals progress.StateID
                                                          join workflowhistory in context.V_VolumeMatrixTransaction
                                                          on transaction.WorkflowInstanceID equals workflowhistory.WorkflowInstanceID
                                                          let utcToLocal = DbFunctions.AddHours(transaction.CreateDate, hourDiff)
                                                          let MaxApprovalID = (from workflowhistorie in context.V_VolumeMatrixTransaction
                                                                               join transactions in context.Transactions
                                                                               on workflowhistorie.WorkflowInstanceID equals transactions.WorkflowInstanceID
                                                                               group workflowhistorie by workflowhistorie.WorkflowInstanceID into grp
                                                                               select grp.Max(x => x.WorkflowApproverID)).ToList()
                                                          where (utcToLocal >= startDate) &&
                                                                  (utcToLocal <= endDate) &&
                                                                  (MaxApprovalID.Contains(workflowhistory.WorkflowApproverID)) &&
                                                                  (transaction.StateID != 2) &&
                                                                  !(transaction.IsExceptionHandling.Value.Equals(true)) &&
                                                                  (transaction.Mode.Equals("BCP2"))
                                                          select new
                                                          {
                                                              StateID = transaction.StateID,
                                                              ProductID = transaction.ProductID,
                                                              CreateDate = transaction.CreateDate,
                                                              UpdateDate = workflowhistory.WorkflowTaskExitTime,
                                                              IsDeleted = slas.IsDeleted,
                                                              SLATime = slas.SLATime,
                                                              IsExceptionHandling = transaction.IsExceptionHandling.Value
                                                          }
                                                  )
                                              on products.ProductID equals transactions.ProductID into group1
                                              from gr1 in group1.DefaultIfEmpty()
                                              let utcToLocalTransactions = gr1.CreateDate != null ? DbFunctions.AddHours(gr1.CreateDate, hourDiff) : DbFunctions.AddHours(startDate, hourDiff)
                                              where (gr1.IsDeleted.Equals(false))
                                                      && (products.IsDeleted.Equals(false))
                                                      && (productTypeDetailInt.Contains(products.ProductID))
                                                      && !(gr1.IsExceptionHandling.Equals(true))
                                                      && ((utcToLocalTransactions >= startDate)
                                                      && (utcToLocalTransactions <= endDate))
                                                      && (gr1.StateID != 2)
                                              //|| (from product in context.Products select product.ProductID).Contains(gr1.ProductID)
                                              select new VolumeMatrixProductModel
                                              {
                                                  period = clientDateTime,
                                                  productName = products.ProductName,
                                                  trxDate = utcToLocalTransactions.Value,
                                                  actualDate = clientDateTime,
                                                  SLATime = gr1.SLATime,
                                                  TAT = DbFunctions.DiffSeconds(utcToLocalTransactions.Value, DateTime.Now),
                                                  StateID = gr1.StateID,

                                              }).ToList();
                        }
                        else
                        {
                            allDataProduct = (from products in context.Products
                                              join slas in context.SLAs
                                              on products.ProductID equals slas.ProductID
                                              where (products.IsDeleted.Equals(false))
                                                    && (productTypeDetailInt.Contains(products.ProductID))
                                              select new VolumeMatrixProductModel
                                              {
                                                  period = clientDateTime,
                                                  productName = products.ProductName,
                                                  actualDate = clientDateTime,
                                                  SLATime = slas.SLATime,
                                                  ExceptionalCount = (from transaction in context.Transactions
                                                                      let utcToLocalTransaction = DbFunctions.AddHours(transaction.CreateDate, hourDiff)
                                                                      where
                                                                            ((utcToLocalTransaction.Value.Day == clientDateTime.Day) &&
                                                                            (utcToLocalTransaction.Value.Month == clientDateTime.Month) &&
                                                                            (utcToLocalTransaction.Value.Year == clientDateTime.Year)) &&
                                                                            (transaction.IsExceptionHandling.Value.Equals(true))
                                                                      select transaction).Count()
                                              }).ToList();
                        }
                    }
                    else if (paymentMode == "IPE")
                    {
                        if (isTAT == "true")
                        {

                            allDataProduct = (from products in context.Products
                                              join transactions in
                                                  (
                                                          from transaction in context.Transactions
                                                          join slas in context.SLAs
                                                          on transaction.ProductID equals slas.ProductID
                                                          join progress in context.ProgressStates
                                                          on transaction.StateID equals progress.StateID
                                                          join workflowhistory in context.V_VolumeMatrixTransaction
                                                          on transaction.WorkflowInstanceID equals workflowhistory.WorkflowInstanceID
                                                          let utcToLocal = DbFunctions.AddHours(transaction.CreateDate, hourDiff)
                                                          let MaxApprovalID = (from workflowhistorie in context.V_VolumeMatrixTransaction
                                                                               join transactions in context.Transactions
                                                                               on workflowhistorie.WorkflowInstanceID equals transactions.WorkflowInstanceID
                                                                               group workflowhistorie by workflowhistorie.WorkflowInstanceID into grp
                                                                               select grp.Max(x => x.WorkflowApproverID)).ToList()
                                                          where (utcToLocal >= startDate) &&
                                                                  (utcToLocal <= endDate) &&
                                                                  (MaxApprovalID.Contains(workflowhistory.WorkflowApproverID)) &&
                                                                  (transaction.StateID != 2) &&
                                                                  !(transaction.IsExceptionHandling.Value.Equals(true)) &&
                                                                  ((!transaction.Mode.Equals("BCP2")) || transaction.Mode.Equals(null))
                                                          select new
                                                          {
                                                              StateID = transaction.StateID,
                                                              ProductID = transaction.ProductID,
                                                              CreateDate = transaction.CreateDate,
                                                              UpdateDate = workflowhistory.WorkflowTaskExitTime,
                                                              IsDeleted = slas.IsDeleted,
                                                              SLATime = slas.SLATime,
                                                              IsExceptionHandling = transaction.IsExceptionHandling.Value
                                                          }
                                                  )
                                              on products.ProductID equals transactions.ProductID into group1
                                              from gr1 in group1.DefaultIfEmpty()
                                              let utcToLocalTransactions = gr1.CreateDate != null ? DbFunctions.AddHours(gr1.CreateDate, hourDiff) : DbFunctions.AddHours(startDate, hourDiff)
                                              where (gr1.IsDeleted.Equals(false))
                                                      && (products.IsDeleted.Equals(false))
                                                      && (productTypeDetailInt.Contains(products.ProductID))
                                                      && !(gr1.IsExceptionHandling.Equals(true))
                                                      && ((utcToLocalTransactions >= startDate)
                                                      && (utcToLocalTransactions <= endDate))
                                                      && (gr1.StateID != 2)
                                              //|| (from product in context.Products select product.ProductID).Contains(gr1.ProductID)
                                              select new VolumeMatrixProductModel
                                              {
                                                  period = clientDateTime,
                                                  productName = products.ProductName,
                                                  trxDate = utcToLocalTransactions.Value,
                                                  actualDate = clientDateTime,
                                                  SLATime = gr1.SLATime,
                                                  TAT = DbFunctions.DiffSeconds(utcToLocalTransactions.Value, DateTime.Now),
                                                  StateID = gr1.StateID,

                                              }).ToList();
                        }
                        else
                        {
                            allDataProduct = (from products in context.Products
                                              join slas in context.SLAs
                                              on products.ProductID equals slas.ProductID
                                              where (products.IsDeleted.Equals(false))
                                                    && (productTypeDetailInt.Contains(products.ProductID))
                                              select new VolumeMatrixProductModel
                                              {
                                                  period = clientDateTime,
                                                  productName = products.ProductName,
                                                  actualDate = clientDateTime,
                                                  SLATime = slas.SLATime,
                                                  ExceptionalCount = (from transaction in context.Transactions
                                                                      let utcToLocalTransaction = DbFunctions.AddHours(transaction.CreateDate, hourDiff)
                                                                      where
                                                                            ((utcToLocalTransaction.Value.Day == clientDateTime.Day) &&
                                                                            (utcToLocalTransaction.Value.Month == clientDateTime.Month) &&
                                                                            (utcToLocalTransaction.Value.Year == clientDateTime.Year)) &&
                                                                            (transaction.IsExceptionHandling.Value.Equals(true))
                                                                      select transaction).Count()
                                              }).ToList();
                        }
                    }
                    else
                    {
                        if (isTAT == "true")
                        {

                            allDataProduct = (from products in context.Products
                                              join transactions in
                                                  (
                                                          from transaction in context.Transactions
                                                          join slas in context.SLAs
                                                          on transaction.ProductID equals slas.ProductID
                                                          join progress in context.ProgressStates
                                                          on transaction.StateID equals progress.StateID
                                                          join workflowhistory in context.V_VolumeMatrixTransaction
                                                          on transaction.WorkflowInstanceID equals workflowhistory.WorkflowInstanceID
                                                          let utcToLocal = DbFunctions.AddHours(transaction.CreateDate, hourDiff)
                                                          let MaxApprovalID = (from workflowhistorie in context.V_VolumeMatrixTransaction
                                                                               join transactions in context.Transactions
                                                                               on workflowhistorie.WorkflowInstanceID equals transactions.WorkflowInstanceID
                                                                               group workflowhistorie by workflowhistorie.WorkflowInstanceID into grp
                                                                               select grp.Max(x => x.WorkflowApproverID)).ToList()
                                                          where (utcToLocal >= startDate) &&
                                                                  (utcToLocal <= endDate) &&
                                                                  (MaxApprovalID.Contains(workflowhistory.WorkflowApproverID)) &&
                                                                  (transaction.StateID != 2) &&
                                                                  !(transaction.IsExceptionHandling.Value.Equals(true))
                                                          select new
                                                          {
                                                              StateID = transaction.StateID,
                                                              ProductID = transaction.ProductID,
                                                              CreateDate = transaction.CreateDate,
                                                              UpdateDate = workflowhistory.WorkflowTaskExitTime,
                                                              IsDeleted = slas.IsDeleted,
                                                              SLATime = slas.SLATime,
                                                              IsExceptionHandling = transaction.IsExceptionHandling.Value
                                                          }
                                                  )
                                              on products.ProductID equals transactions.ProductID into group1
                                              from gr1 in group1.DefaultIfEmpty()
                                              let utcToLocalTransactions = gr1.CreateDate != null ? DbFunctions.AddHours(gr1.CreateDate, hourDiff) : DbFunctions.AddHours(startDate, hourDiff)
                                              where (gr1.IsDeleted.Equals(false))
                                                      && (products.IsDeleted.Equals(false))
                                                      && (productTypeDetailInt.Contains(products.ProductID))
                                                      && !(gr1.IsExceptionHandling.Equals(true))
                                                      && ((utcToLocalTransactions >= startDate)
                                                      && (utcToLocalTransactions <= endDate))
                                                      && (gr1.StateID != 2)
                                              //|| (from product in context.Products select product.ProductID).Contains(gr1.ProductID)
                                              select new VolumeMatrixProductModel
                                              {
                                                  period = clientDateTime,
                                                  productName = products.ProductName,
                                                  trxDate = utcToLocalTransactions.Value,
                                                  actualDate = clientDateTime,
                                                  SLATime = gr1.SLATime,
                                                  TAT = DbFunctions.DiffSeconds(utcToLocalTransactions.Value, DateTime.Now),
                                                  StateID = gr1.StateID,

                                              }).ToList();
                        }
                        else
                        {
                            allDataProduct = (from products in context.Products
                                              join slas in context.SLAs
                                              on products.ProductID equals slas.ProductID
                                              where (products.IsDeleted.Equals(false))
                                                    && (productTypeDetailInt.Contains(products.ProductID))
                                              select new VolumeMatrixProductModel
                                              {
                                                  period = clientDateTime,
                                                  productName = products.ProductName,
                                                  actualDate = clientDateTime,
                                                  SLATime = slas.SLATime,
                                                  ExceptionalCount = (from transaction in context.Transactions
                                                                      let utcToLocalTransaction = DbFunctions.AddHours(transaction.CreateDate, hourDiff)
                                                                      where
                                                                            ((utcToLocalTransaction.Value.Day == clientDateTime.Day) &&
                                                                            (utcToLocalTransaction.Value.Month == clientDateTime.Month) &&
                                                                            (utcToLocalTransaction.Value.Year == clientDateTime.Year)) &&
                                                                            (transaction.IsExceptionHandling.Value.Equals(true))
                                                                      select transaction).Count()
                                              }).ToList();
                        }
                    }
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }


        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

}