﻿using Excel;
using Microsoft.SharePoint;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBS.Sharepoint.Helpers
{
    public static class FileHelper
    {
        public static string DataTableToCSV(DataTable table, string delimiter, bool includeHeader)
        {
            StringBuilder result = new StringBuilder();

            if (includeHeader)
            {
                foreach (DataColumn column in table.Columns)
                {
                    result.Append(column.ColumnName);
                    result.Append(delimiter);
                }

                result.Remove(result.Length, 0);
                result.Append(Environment.NewLine);
            }

            foreach (DataRow row in table.Rows)
            {
                foreach (object item in row.ItemArray)
                {
                    if (item is DBNull)
                        result.Append(delimiter);
                    else
                    {
                        string itemAsString;

                        if (item.GetType() == typeof(DateTime))
                        {
                            itemAsString = DateTime.Parse(item.ToString()).ToString("yyyyMMdd");
                        }
                        else
                        {
                            itemAsString = item.ToString();
                        }

                        // Double up all embedded double quotes
                        itemAsString = itemAsString.Replace("\"", "\"\"");

                        // To keep things simple, always delimit with double-quotes
                        // so we don't have to determine in which cases they're necessary
                        // and which cases they're not.
                        //itemAsString = "\"" + itemAsString + "\"";

                        result.Append(itemAsString + delimiter);
                    }
                }

                result.Remove(result.Length, 0);
                result.Append(Environment.NewLine);
            }

            return result.ToString();
        }

        public static Stream GenerateStreamFromString(string data)
        {
            MemoryStream memoryStream = new MemoryStream();
            StreamWriter streamWriter = new StreamWriter(memoryStream);
            streamWriter.Write(data);
            streamWriter.Flush();
            memoryStream.Position = 0;
            return memoryStream;
        }

        public static bool AddToLibrary(SPWeb web, Stream stream, string fileName, string libraryName, bool replaceExisiting, ref long insertedID, ref string insertedURL, ref bool result, ref string errorMessage)
        {
            result = false;

            try
            {
                //SPFolder libFolder = web.Folders[libraryName]; --> BUG (if library name is not equals to lib folder)
                SPFolder libFolder = web.Lists[libraryName].RootFolder;

                //Check the existing File out if the Library Requires CheckOut
                if (libFolder.RequiresCheckout)
                {
                    try
                    {
                        SPFile fileOld = libFolder.Files[fileName];
                        fileOld.CheckOut();
                    }
                    catch (Exception ex)
                    {
                        errorMessage = ex.InnerException.ToString();
                    }
                }

                // read bytes
                byte[] contents = new byte[stream.Length];
                stream.Read(contents, 0, (int)stream.Length);
                stream.Close();

                //Property
                Hashtable properties = new Hashtable();
                properties.Add("Name", "Name");

                // Upload document
                SPFile spfile = libFolder.Files.Add(fileName, contents, properties, replaceExisiting);
                insertedID = spfile.Item.ID;
                insertedURL = string.Format("{0}/{1}", web.Url, spfile.Item.Url);

                // Commit 
                libFolder.Update();

                result = true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.InnerException.ToString();
            }
            finally
            {
                web.Dispose();
                stream.Dispose();
            }


            return result;
        }

        public static string Left(string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            maxLength = Math.Abs(maxLength);

            return (value.Length <= maxLength
                   ? value
                   : value.Substring(0, maxLength)
                   );
        }

        public static string Right(this string value, int length)
        {
            return value.Substring(value.Length - length);
        }

        //Create RTGS
        public static string RTGSFormat(DataTable dtTransaction)
        {
            string result = string.Empty;
            StringBuilder sbFile = new StringBuilder();

            for (int i = 0; i < dtTransaction.Rows.Count; i++)
            {
                var PaymentAmount = dtTransaction.Rows[i][3].ToString().Contains(".") ? Math.Round(Convert.ToDecimal(dtTransaction.Rows[i][3].ToString())).ToString() : dtTransaction.Rows[i][3].ToString();
                //var PaymentAmount = dtTransaction.Rows[i][3].ToString().Contains(".") ? String.Format("{0:0.00}", Convert.ToDecimal(dtTransaction.Rows[i][3].ToString())) : dtTransaction.Rows[i][3].ToString();

                sbFile.Append(
                    //Identifier
                    "R^^^"
                    //Cust Ref No (CIF)
                    + dtTransaction.Rows[i][0].ToString() + "^"
                    //Debit Acc Number (AccountNumber)
                    + dtTransaction.Rows[i][1].ToString() + "^"
                    //Value Date (ApplicationDate)
                    + String.Format("{0:ddMMyyyy}", Convert.ToDateTime(dtTransaction.Rows[i][2].ToString())) + "^"
                    //Payment Amount (Amount)
                    + PaymentAmount + "^"
                    //Bene Name (BeneName)
                    + dtTransaction.Rows[i][4].ToString() + "^^"
                    //Bene Acc Number (BankAccount)
                    + dtTransaction.Rows[i][5].ToString() + "^"
                    //Bank Code (BankCode)
                    + dtTransaction.Rows[i][6].ToString() + "^"
                    //Branch Code (BranchCode)
                    + dtTransaction.Rows[i][7].ToString() + "^"
                    //Bene Bank Name (BankDescription)
                    + Left(dtTransaction.Rows[i][8].ToString(),35) + "^^^^"  // request bene bank  max 35 char
                    //Customer Name (CustomerName)
                    + dtTransaction.Rows[i][9].ToString() + "^"
                    //Payment Details (PaymentDetails)
                    + dtTransaction.Rows[i][10].ToString() + "^^^^^^^^^^^^^^^"
                    //Resident (IsResident)
                    + (dtTransaction.Rows[i][11].ToString() == "True" ? "YES" : "NO") + "^"
                    //Citizen (IsCitizen)
                    + (dtTransaction.Rows[i][12].ToString() == "True" ? "YES" : "NO") + "^"
                    //Bank Charges (BankCharges)
                    + dtTransaction.Rows[i][13].ToString() + "^"
                    //Agent Charges (AgentCharges)
                    + dtTransaction.Rows[i][14].ToString()
                );
                sbFile.Append(Environment.NewLine);
            }
            result = sbFile.ToString();

            return result;
        }

        //Create SKN
        public static string SKNFormat(DataTable dtTransaction)
        {
            #region OldCode
            /*string result = string.Empty;
            StringBuilder sbFile = new StringBuilder();
            char pad = ' ';
            List<string> totalPaymentAmountTemp = new List<string>();

            for (int i = 0; i < dtTransaction.Rows.Count; i++)
            {
                //Initialize Data

                //Debit Acc Number (AccountNumber)
                string DebtAccNo = dtTransaction.Rows[i][1].ToString();
                //Value Date (ApplicationDate)
                string ValueDate = String.Format("{0:yyyyMMdd}", Convert.ToDateTime(dtTransaction.Rows[i][2].ToString()));
                //Payment Amount (Amount)
                string PaymentAmount = dtTransaction.Rows[i][3].ToString();
                //Bene Name (BeneName)
                string BenefName = dtTransaction.Rows[i][4].ToString();
                //Bene Acc Number (BankAccount)
                string BenefAccNo = dtTransaction.Rows[i][5].ToString();
                //Bank Code (BankCode)
                string BankCode = dtTransaction.Rows[i][6].ToString();
                //Branch Code (BranchCode)
                string BranchCode = dtTransaction.Rows[i][7].ToString();
                //Customer Name (CustomerName)
                string CustName = dtTransaction.Rows[i][9].ToString();
                //Citizen (IsCitizen)
                int Citizen = dtTransaction.Rows[i][12].ToString() == "True" ? 0 : 1;
                //Resident (IsResident)
                int Resident = dtTransaction.Rows[i][11].ToString() == "True" ? 1 : 2;
                //Payment Details (PaymentDetails)
                string PaymentDetails = dtTransaction.Rows[i][10].ToString();
                //Bank Charges (BankCharges)
                string Charges = dtTransaction.Rows[i][13].ToString();
                //Bene Bank Name (BankDescription)
                string BenefBankName = dtTransaction.Rows[i][8].ToString();
                //ChargesType
                string TrxType = "20";

                //Header
                if (i == 0)
                {
                    sbFile.Append("0" + ValueDate.PadRight(10, pad) + DebtAccNo.PadRight(35, pad) + CustName.PadRight(35, pad) + "1111188888888" + string.Empty.PadRight(2220, pad));
                    sbFile.Append(Environment.NewLine);
                }

                //Content
                sbFile.Append(
                        "11"
                        + String.Empty.PadRight(19, pad)
                        + Left(BankCode + "    ", 4) + Left(BranchCode + "    ", 4)
                        + Left(BankCode + BranchCode + String.Empty.PadRight(11, pad), 11)
                        + Left(BenefAccNo + String.Empty.PadRight(35, pad), 35)
                        + Left(BenefName + String.Empty.PadRight(50, pad), 50)
                        + String.Empty.PadRight(140, pad) + Resident + Citizen + "  " + TrxType
                        + Right(String.Empty.PadRight(17, '0') + (PaymentAmount.ToString().Contains(".") ? Math.Round(Convert.ToDecimal(PaymentAmount)).ToString() : PaymentAmount), 17)
                        + "00" + Charges + String.Empty.PadRight(16, pad)
                        + Left(PaymentDetails + String.Empty.PadRight(210, pad), 210)
                        + String.Empty.PadRight(295, pad)
                        + String.Empty.PadRight(1500, pad)
                    );
                sbFile.Append(Environment.NewLine);

                //Add Amount to List
                totalPaymentAmountTemp.Add(PaymentAmount);
            }

            //Count total amount
            Decimal totalAmount = totalPaymentAmountTemp.Sum(a => Convert.ToDecimal(a));

            //Footer
            if (dtTransaction.Rows.Count > 1)
            {
                sbFile.Append("9" + Right(String.Empty.PadRight(8, '0') + dtTransaction.Rows.Count.ToString(), 8) + Right(String.Empty.PadRight(20, '0') + Math.Round(totalAmount).ToString(), 20) + "00" + String.Empty.PadRight(8, '0') + String.Empty.PadRight(22, '0') + String.Empty.PadRight(2253, pad));
            }

            result = sbFile.ToString();

            return result;*/
            #endregion

            string result = string.Empty;
            StringBuilder sbFile = new StringBuilder();
            char pad = ' ';
            List<string> totalPaymentAmountTemp = new List<string>();
            string CIF = string.Empty;
            int TotalRow = 0;

            for (int i = 0; i < dtTransaction.Rows.Count; i++)
            {
                //Initialize Data

                //Debit Acc Number (AccountNumber)
                string DebtAccNo = dtTransaction.Rows[i][1].ToString();
                //Value Date (ApplicationDate)
                string ValueDate = String.Format("{0:yyyyMMdd}", Convert.ToDateTime(dtTransaction.Rows[i][2].ToString()));
                //Payment Amount (Amount)
                string PaymentAmount = dtTransaction.Rows[i][3].ToString();
                //Bene Name (BeneName)
                string BenefName = dtTransaction.Rows[i][4].ToString();
                //Bene Acc Number (BankAccount)
                string BenefAccNo = dtTransaction.Rows[i][5].ToString().Replace("\n", string.Empty);
                //Bank Code (BankCode)
                string BankCode = dtTransaction.Rows[i][6].ToString();
                //Branch Code (BranchCode)
                string BranchCode = dtTransaction.Rows[i][7].ToString();
                //Customer Name (CustomerName)
                string CustName = dtTransaction.Rows[i][9].ToString();
                //Citizen (IsCitizen)
                int Citizen = dtTransaction.Rows[i][12].ToString() == "True" ? 1 : 0;
                //Resident (IsResident)
                //int Resident = dtTransaction.Rows[i][11].ToString() == "True" ? 1 : 2;
                int Resident = dtTransaction.Rows[i][17].ToString() == "True" ? 1 : 0; //Perubahan SKN 2016-04-26
                //Payment Details (PaymentDetails)
                string PaymentDetails = dtTransaction.Rows[i][10].ToString();
                //Bank Charges (BankCharges)
                string Charges = dtTransaction.Rows[i][13].ToString();
                //Bene Bank Name (BankDescription)
                string BenefBankName = dtTransaction.Rows[i][8].ToString();
                //ChargesType
                string TrxType = Math.Round(Convert.ToDecimal(dtTransaction.Rows[i][15].ToString())).ToString();
                string BeneBusinessType = dtTransaction.Rows[i][16].ToString(); //Perubahan SKN 2016-04-26
                //Header
                if (CIF != dtTransaction.Rows[i][0].ToString())
                {
                    CIF = dtTransaction.Rows[i][0].ToString();
                    sbFile.Append("0" + Left(ValueDate.PadRight(10, pad), 10) + Left(DebtAccNo.PadRight(35, pad), 35) + Left(CustName.PadRight(35, pad), 35) + "1111188888888" + string.Empty.PadRight(2220, pad));
                    sbFile.Append(Environment.NewLine);
                }

                //Content
                sbFile.Append(
                        "11"
                        + String.Empty.PadRight(19, pad)
                        + Left(BankCode + "    ", 4) + Left(BranchCode + "    ", 4)
                        + Left(BankCode + BranchCode + String.Empty.PadRight(11, pad), 11)
                        + Left(BenefAccNo + String.Empty.PadRight(35, pad), 35)
                        + Left(BenefName + String.Empty.PadRight(70, pad), 70) //Rizki - error in Production 20160229, Change from 50 to 70 20160725 
                        + String.Empty.PadRight(140, pad) + Resident + Citizen + " " + BeneBusinessType + TrxType //Perubahan SKN 2016-04-26
                        + Right(String.Empty.PadRight(17, '0') + (PaymentAmount.ToString().Contains(".") ? Math.Round(Convert.ToDecimal(PaymentAmount)).ToString() : PaymentAmount), 17)
                        + "00" + Charges + String.Empty.PadRight(16, pad)
                        + Left(PaymentDetails + String.Empty.PadRight(210, pad), 210)
                        + String.Empty.PadRight(295, pad)
                        + String.Empty.PadRight(1500, pad)
                    );
                sbFile.Append(Environment.NewLine);

                //Add Amount to List
                totalPaymentAmountTemp.Add(PaymentAmount);
                TotalRow = TotalRow + 1;

                if ((dtTransaction.Rows.Count - 1) == i)
                {
                    //Count total amount
                    Decimal totalAmount = totalPaymentAmountTemp.Sum(a => Convert.ToDecimal(a));

                    //Footer
                    if (dtTransaction.Rows.Count > 0)
                    {
                        sbFile.Append("9" + Right(String.Empty.PadRight(8, '0') + TotalRow, 8) + Right(String.Empty.PadRight(20, '0') + Math.Round(totalAmount).ToString(), 20) + "00" + String.Empty.PadRight(8, '0') + String.Empty.PadRight(22, '0') + String.Empty.PadRight(2253, pad));
                    }

                    totalPaymentAmountTemp.Clear();
                    TotalRow = 0;
                }
                else if (CIF != dtTransaction.Rows[i + 1][0].ToString())
                {
                    //Count total amount
                    Decimal totalAmount = totalPaymentAmountTemp.Sum(a => Convert.ToDecimal(a));

                    //Footer
                    if (dtTransaction.Rows.Count > 0)
                    {
                        sbFile.Append("9" + Right(String.Empty.PadRight(8, '0') + TotalRow, 8) + Right(String.Empty.PadRight(20, '0') + Math.Round(totalAmount).ToString(), 20) + "00" + String.Empty.PadRight(8, '0') + String.Empty.PadRight(22, '0') + String.Empty.PadRight(2253, pad));
                        sbFile.Append(Environment.NewLine);
                    }

                    totalPaymentAmountTemp.Clear();
                    TotalRow = 0;
                }
            }

            result = sbFile.ToString();

            return result;
        }

        //Create OVB
        public static string OVBFormat(DataTable dtTransaction)
        {
            string result = string.Empty;
            StringBuilder sbFile = new StringBuilder();
            char pad = ' ';

            for (int i = 0; i < dtTransaction.Rows.Count; i++)
            {
                string AccNumber = dtTransaction.Rows[i][0].ToString();
                string Currencycode = dtTransaction.Rows[i][1].ToString();
                string PartTranType = "D";
                string TranAmount = dtTransaction.Rows[i][4].ToString();
                string PaymentDetail = dtTransaction.Rows[i][5].ToString();
                string CurrencyID = dtTransaction.Rows[i][6].ToString();
                string DebitCurrID = dtTransaction.Rows[i][7].ToString();
                string RefAmount = "";
                string RefCurrencyCode = "";
                string RateCode = "";
                string ValueDate = "";
                string rate = "";
                if (CurrencyID != DebitCurrID)
                {
                    RefAmount = TranAmount;
                    RefCurrencyCode = Currencycode;
                    RateCode = "TTB";
                    if (!dtTransaction.Rows[i][8].Equals(System.DBNull.Value) && !dtTransaction.Rows[i][4].Equals(System.DBNull.Value))
                    {
                        if (Convert.ToDateTime(dtTransaction.Rows[i][8].ToString()) < Convert.ToDateTime(dtTransaction.Rows[i][4].ToString()))
                            ValueDate = String.Format("{0:dd-MM-yyyy}", dtTransaction.Rows[i][8]).ToUpper();

                        DateTime dtValuedate = Convert.ToDateTime(dtTransaction.Rows[i][8].ToString());
                        ValueDate = Right(String.Empty.PadRight(2, '0') + (dtValuedate.Day).ToString(), 2) + "-" + Right(String.Empty.PadRight(2, '0') + (dtValuedate.Month).ToString(), 2) + "-" + dtValuedate.Year.ToString();

                    }


                    rate = Right(String.Empty.PadRight(15, '0') + (rate.ToString().Contains(".") ? Math.Round(Convert.ToDecimal(TranAmount)).ToString() : rate), 15);
                }

                sbFile.Append(
                    Left(AccNumber + String.Empty.PadRight(16, pad), 16) +
                    Left(Currencycode + String.Empty.PadRight(3, pad), 3) +
                    String.Empty.PadRight(8, pad) +
                    PartTranType +
                    Right(String.Empty.PadRight(17, '0') + (TranAmount.ToString().Contains(".") ? Math.Round(Convert.ToDecimal(TranAmount)).ToString() : TranAmount), 17) +
                    Left(PaymentDetail + String.Empty.PadRight(30, pad), 30) +
                    String.Empty.PadRight(5, pad) +
                    String.Empty.PadRight(20, pad) +
                    String.Empty.PadRight(5, pad) +
                    String.Empty.PadRight(10, pad) +
                    String.Empty.PadRight(6, pad) +
                    String.Empty.PadRight(16, pad) +
                    String.Empty.PadRight(1, pad)
                    );

                if (!string.IsNullOrEmpty(RefAmount))
                    sbFile.Append(Right(String.Empty.PadRight(17, '0') + (TranAmount.ToString().Contains(".") ? Math.Round(Convert.ToDecimal(TranAmount)).ToString() : TranAmount), 17));
                else
                    sbFile.Append(String.Empty.PadRight(17, pad));

                if (!string.IsNullOrEmpty(RefCurrencyCode))
                    sbFile.Append(Left(Currencycode + String.Empty.PadRight(3, pad), 3));
                else
                    //sbFile.Append(String.Empty.PadRight(17, pad));
                    sbFile.Append(String.Empty.PadRight(3, pad));

                if (!string.IsNullOrEmpty(RateCode))
                    sbFile.Append(Left(RateCode + String.Empty.PadRight(5, pad), 5));
                else
                    sbFile.Append(String.Empty.PadRight(5, pad));
                sbFile.Append(Left(RateCode + String.Empty.PadRight(5, pad), 5));

                if (!string.IsNullOrEmpty(rate))
                    sbFile.Append(Left(rate + String.Empty.PadRight(15, pad), 15));
                else
                    sbFile.Append(String.Empty.PadRight(15, pad));

                if (!string.IsNullOrEmpty(ValueDate))
                    sbFile.Append(Left(ValueDate + String.Empty.PadRight(10, pad), 10));
                else
                    sbFile.Append(String.Empty.PadRight(10, pad));

                sbFile.Append(
                        String.Empty.PadRight(10, pad) +
                        String.Empty.PadRight(5, pad) +
                        String.Empty.PadRight(6, pad) +
                        String.Empty.PadRight(6, pad) +
                        String.Empty.PadRight(2, pad) +
                        String.Empty.PadRight(1, pad) +
                        String.Empty.PadRight(12, pad) +
                        String.Empty.PadRight(10, pad) +
                        String.Empty.PadRight(20, pad) +
                        String.Empty.PadRight(5, pad) +
                        String.Empty.PadRight(30, pad) +
                        String.Empty.PadRight(40, pad) +
                        String.Empty.PadRight(40, pad) +
                        String.Empty.PadRight(40, pad) +
                        String.Empty.PadRight(40, pad) +
                        String.Empty.PadRight(40, pad) +
                        String.Empty.PadRight(17, pad) +
                        String.Empty.PadRight(17, pad) +
                        String.Empty.PadRight(17, pad) +
                        String.Empty.PadRight(17, pad) +
                        String.Empty.PadRight(17, pad) +
                        String.Empty.PadRight(30, pad) +
                        String.Empty.PadRight(16, pad) +
                        String.Empty.PadRight(12, pad) +
                        String.Empty.PadRight(10, pad) +
                        String.Empty.PadRight(10, pad) +
                        String.Empty.PadRight(9, pad) +
                        String.Empty.PadRight(4, pad) +
                        String.Empty.PadRight(34, pad) +
                        String.Empty.PadRight(256, pad) +
                        String.Empty.PadRight(16, pad) +
                        String.Empty.PadRight(5, pad) +
                        String.Empty.PadRight(5, pad)
                    );
                sbFile.Append(Environment.NewLine);
            }
            result = sbFile.ToString();
            return result;
        }

        
        public static string POAFormat (DataTable dtContents)
        {
            string result = string.Empty;

            StringBuilder sbFile = new StringBuilder();

            for (int i = 0; i < dtContents.Rows.Count; i++)
            {

                sbFile.Append(

                    dtContents.Rows[i][0].ToString().Trim() + dtContents.Rows[i][1].ToString().Trim() + dtContents.Rows[i][2].ToString().Trim() + dtContents.Rows[i][3].ToString().Trim() + dtContents.Rows[i][4].ToString().Trim()
                );
                sbFile.Append(Environment.NewLine);
            }
            result = sbFile.ToString();
            return result;

        }
        //Create XML by adi
        public static string XMLFormat(DataTable dtContents)
        {
            string result = string.Empty;

            StringBuilder sbFile = new StringBuilder();

            for (int i = 0; i < dtContents.Rows.Count; i++)
            {

                sbFile.Append(
                    //transactionID
                    //int TransactionID = dtContents.Rows[i][].ToString()
                    //.Trim(),
                    //Contents 
                    dtContents.Rows[i][0].ToString().Trim()
                );
                sbFile.Append(Environment.NewLine);
            }
            result = sbFile.ToString();
            return result;
        }

        //Read Data From txt file SKN
        public static void ReadSKN(UploadFileModel model, ref string message)
        {
            try
            {
                #region Code
                using (SPSite site = new SPSite(SPContext.Current.Site.RootWeb.Url))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        SPList myList = web.Lists["Instruction Documents"];
                        SPListItem myListItem = myList.GetItemById(model.ID);

                        SPFile attchFile = web.GetFile(site.Url + "/" + myListItem.Url);

                        Stream stream = attchFile.OpenBinaryStream();

                        string line;
                        StringBuilder sb = new StringBuilder();

                        //Read data from txt
                        StreamReader file = new StreamReader(stream);
                        //System.IO.StreamReader file = new System.IO.StreamReader(path);

                        string header = string.Empty;

                        while ((line = file.ReadLine()) != null)
                        {
                            string code = line.Substring(0, 1);

                            switch (code)
                            {
                                case "0":
                                    header = line;

                                    break;
                                case "1":

                                    //break down line header
                                    #region Header
                                    string var1 = header.Substring(1, 10).TrimEnd();
                                    DateTime ValueDate = DateTime.ParseExact(var1, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);

                                    string DebtAccNo = header.Substring(11, 35).TrimEnd();

                                    string var2 = header.Substring(46, 35).TrimEnd();
                                    string sqlCustomer = "SELECT * FROM [Customer].[Customer] WHERE CustomerName = '" + var2 + "'";
                                    DataTable dtCustomer = new DataTable();
                                    dtCustomer = DataHelper.ExecuteDataTable(sqlCustomer);
                                    var Cif = dtCustomer != null ? dtCustomer.Rows[0][0].ToString() : null;

                                    string IsNewCustomer = Cif == null ? "1" : "0";
                                    #endregion

                                    //break down content
                                    #region Content
                                    string var3 = line.Substring(21, 4).TrimEnd();
                                    string sqlBank = "SELECT * FROM [Master].[Bank] WHERE BankCode = '" + var3 + "'";
                                    DataTable dtBank = new DataTable();
                                    dtBank = DataHelper.ExecuteDataTable(sqlBank);
                                    var BankId = dtBank != null ? dtBank.Rows[0][0].ToString() : null;

                                    string BranchCode = line.Substring(25, 4).TrimEnd();

                                    string BenefAccNo = line.Substring(40, 35).TrimEnd();

                                    string BenefName = line.Substring(75, 50).TrimEnd();

                                    string Resident = line.Substring(264, 1).TrimEnd() == "1" ? "1" : "0";

                                    string Citizen = line.Substring(265, 1).TrimEnd() == "0" ? "1" : "0";

                                    string TrxType = line.Substring(269, 2).TrimEnd();

                                    string PaymentAmount = line.Substring(271, 17).TrimStart('0');

                                    string var4 = line.Substring(290, 16).TrimEnd();
                                    string sqlBankCharges = "SELECT * FROM [Master].[Charges] WHERE ChargesCode = '" + var4 + "'";
                                    DataTable dtBankCharges = new DataTable();
                                    dtBankCharges = DataHelper.ExecuteDataTable(sqlBankCharges);
                                    var Charges = dtBankCharges != null ? dtBankCharges.Rows[0][0].ToString() : null;

                                    string PaymentDetails = line.Substring(309, 210).TrimEnd();
                                    #endregion

                                    //query
                                    #region Query
                                    sb.Append("INSERT [Data].[TransactionDraft] ( "
                                    + "[CIF], "
                                        //+ "[BizSegmentID], "
                                    + "[AccountNumber], "
                                    + "[ApplicationDate], "
                                    + "[Amount], "
                                    + "[BeneName], "
                                    + "[BeneAccNumber], "
                                    + "[BankID], "
                                    + "[PaymentDetails], "
                                    + "[IsResident], "
                                    + "[IsCitizen], "
                                    + "[BankChargesID], "
                                        //+ "[AgentChargesID], "
                                    + "[Type], "
                                    + "[IsTopUrgent], "
                                    + "[IsNewCustomer], "
                                        //+ "[IsSignatureVerified], "
                                        //+ "[IsDormantAccount], "
                                        //+ "[IsFrezeAccount], "
                                        //+ "[IsDocumentComplete], "
                                    + "[IsDraft], "
                                    + "[CreateDate], "
                                    + "[CreateBy]"
                                    + ") VALUES (");

                                    sb.Append(Cif + ", '" +
                                              DebtAccNo + "', '" +
                                              ValueDate + "', " +
                                              PaymentAmount + ", '" +
                                              BenefName + "', " +
                                              BenefAccNo + ", " +
                                              BankId + ", '" +
                                              PaymentDetails + "', " +
                                              Resident + ", " +
                                              Citizen + ", " +
                                              Charges + ", " +
                                              TrxType + ", " +
                                              0 + ", " +
                                              IsNewCustomer + ", " +
                                        //0 + ", " +
                                        //0 + ", " +
                                        //0 + ", " +
                                        //0 + ", " +
                                              1 + ", '" +
                                              DateTime.UtcNow + "', '" +
                                              "" + "Unknown" + "')"
                                        );
                                    #endregion

                                    break;
                                case "9":

                                    break;
                            }
                        }

                        //Close file
                        stream.Close();
                        file.Close();

                        //Execute string and insert data into database
                        DataHelper.ExecuteNonQuery(sb.ToString());
                        //Console.WriteLine(sb.ToString());
                        //Console.ReadLine();
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
        }

        //Read Data From txt file RTGS
        public static void ReadRTGS(UploadFileModel model, ref string message)
        {
            try
            {
                using (SPSite site = new SPSite(SPContext.Current.Site.RootWeb.Url))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        #region Code
                        SPList myList = web.Lists["Instruction Documents"];
                        SPListItem myListItem = myList.GetItemById(model.ID);

                        SPFile attchFile = web.GetFile(site.Url + "/" + myListItem.Url);

                        Stream stream = attchFile.OpenBinaryStream();

                        string line;
                        StringBuilder sb = new StringBuilder();

                        //Read data from txt
                        StreamReader file = new StreamReader(stream);
                        //System.IO.StreamReader file = new System.IO.StreamReader(path);

                        //Initialize index of data
                        string[] dataindex = { "3", "4", "5", "6", "7", "9", "10", "17", "32", "33", "34", "35" };

                        //Iterate data and build sql string
                        while ((line = file.ReadLine()) != null)
                        {
                            sb.Append("INSERT [Data].[TransactionDraft] ( "
                                    + "[CIF], "
                                    + "[BizSegmentID], "
                                    + "[AccountNumber], "
                                    + "[ApplicationDate], "
                                    + "[Amount], "
                                    + "[BeneName], "
                                    + "[BeneAccNumber], "
                                    + "[BankID], "
                                    + "[PaymentDetails], "
                                    + "[IsResident], "
                                    + "[IsCitizen], "
                                    + "[BankChargesID], "
                                    + "[AgentChargesID], "
                                    + "[Type], "
                                    + "[IsTopUrgent], "
                                    + "[IsNewCustomer], "
                                //+ "[IsSignatureVerified], "
                                //+ "[IsDormantAccount], "
                                //+ "[IsFrezeAccount], "
                                //+ "[IsDocumentComplete], "
                                    + "[IsDraft], "
                                    + "[CreateDate], "
                                    + "[CreateBy]"
                                    + ") VALUES (");

                            //Split data each line
                            string[] splitline = line.Split('^');

                            //Iterate each column in a row
                            for (int i = 0; i < splitline.Length; i++)
                            {
                                if (Array.IndexOf(dataindex, i.ToString()) >= 0)
                                {
                                    switch (i)
                                    {
                                        case 3:
                                            string sqlCustomer = "SELECT * FROM [Customer].[Customer] WHERE CIF = '" + splitline[i] + "'";
                                            DataTable dtCustomer = new DataTable();
                                            dtCustomer = DataHelper.ExecuteDataTable(sqlCustomer);
                                            sb.Append("" + dtCustomer.Rows[0][0] + ", " + dtCustomer.Rows[0][3] + ", ");
                                            break;
                                        case 5:
                                            string date = splitline[i].Substring(0, 2) + "/" + splitline[i].Substring(2, 2) + "/" + splitline[i].Substring(4, 4);
                                            sb.Append("convert(datetime, '" + date + "', 103), ");
                                            break;
                                        case 6:
                                            sb.Append("" + splitline[i] + ", ");
                                            break;
                                        case 10:
                                            string sqlBank = "SELECT * FROM [Master].[Bank] WHERE BankCode = '" + splitline[i] + "'";
                                            DataTable dtBank = new DataTable();
                                            dtBank = DataHelper.ExecuteDataTable(sqlBank);
                                            sb.Append("" + dtBank.Rows[0][0] + ", ");
                                            break;
                                        case 32:
                                            string isResident = splitline[i].ToLower().Equals("true") ? "1" : "0";
                                            sb.Append(isResident + ", ");
                                            break;
                                        case 33:
                                            string isCitizen = splitline[i].ToLower().Equals("true") ? "1" : "0";
                                            sb.Append(isCitizen + ", ");
                                            break;
                                        case 34:
                                            string sqlBankCharges = "SELECT * FROM [Master].[Charges] WHERE ChargesCode = '" + splitline[i] + "'";
                                            DataTable dtBankCharges = new DataTable();
                                            dtBankCharges = DataHelper.ExecuteDataTable(sqlBankCharges);
                                            sb.Append("" + dtBankCharges.Rows[0][0] + ", ");
                                            break;
                                        case 35:
                                            string sqlAgentCharges = "SELECT * FROM [Master].[Charges] WHERE ChargesCode = '" + splitline[i] + "'";
                                            DataTable dtAgentCharges = new DataTable();
                                            dtAgentCharges = DataHelper.ExecuteDataTable(sqlAgentCharges);
                                            sb.Append("" + dtAgentCharges.Rows[0][0] + ", ");
                                            break;
                                        default:
                                            sb.Append("'" + splitline[i] + "', ");
                                            break;
                                    }
                                }
                            }

                            sb.Append("20, 0, 0, 1, CAST('" + DateTime.UtcNow + "' AS DateTime), '" + "Unknown" + "')");
                            sb.AppendLine(Environment.NewLine);
                        }

                        //Close file
                        stream.Close();
                        file.Close();

                        //Execute string and insert data into database
                        DataHelper.ExecuteNonQuery(sb.ToString());
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
        }

    }
}
