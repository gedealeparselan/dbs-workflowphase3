﻿using System.ServiceModel;
using System.ServiceModel.Web;

namespace DBS.Sharepoint.Model
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMailingService" in both code and config file together.
    [ServiceContract]
    public interface IEmailNotification
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "GetEmailNotification")]
        string GetEmailNotification(EmailProperty properties);
    }
}
