//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    
    public partial class SP_ReportTransactionDocument_Result
    {
        public string CustomerName { get; set; }
        public string CIF { get; set; }
        public Nullable<int> StatementLetterID { get; set; }
        public string StatementLetterName { get; set; }
        public string UnderlyingDocName { get; set; }
        public string DocTypeName { get; set; }
        public string CurrencyCode { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<System.DateTime> DateOfUnderlying { get; set; }
        public Nullable<System.DateTime> ExpiredDate { get; set; }
        public string SupplierName { get; set; }
        public string InvoiceNumber { get; set; }
        public string Document_Reference { get; set; }
    }
}
