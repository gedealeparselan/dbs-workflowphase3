﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class ChargesTypeController : ApiController
    {
        private string message = string.Empty;

        private readonly IChargesTypeRepository repository = null;

        public ChargesTypeController()
        {
            this.repository = new ChargesTypeRepository();
        }

        public ChargesTypeController(IChargesTypeRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all chargesType.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<ChargesTypeModel>))]
        public HttpResponseMessage Get()
        {
            IList<ChargesTypeModel> output = new List<ChargesTypeModel>();

            if (!repository.GetChargesType(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ChargesTypeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get chargesType with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<ChargesTypeModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<ChargesTypeModel> output = new List<ChargesTypeModel>();

            if (!repository.GetChargesType(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<ChargesTypeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get chargesType for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(ChargesTypeGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<ChargesTypeFilter> filters)
        {
            ChargesTypeGrid output = new ChargesTypeGrid();

            if (!repository.GetChargesType(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<ChargesTypeGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find chargesType using criterias Code or Description.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<ChargesTypeModel>))]
        [HttpGet]
        [Route("api/ChargesType/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<ChargesTypeModel> output = new List<ChargesTypeModel>();

            if (!repository.GetChargesType(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ChargesTypeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get chargesType by ID.
        /// </summary>
        /// <param name="id">ChargesType ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(ChargesTypeModel))]
        [Route("api/ChargesType/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            ChargesTypeModel output = new ChargesTypeModel();

            if (!repository.GetChargesTypeByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<ChargesTypeModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new chargesType.
        /// </summary>
        /// <param name="chargesType">Cumrrency data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(ChargesTypeModel chargesType)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddChargesType(chargesType, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Charges Type data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing chargesType.
        /// </summary>
        /// <param name="id">ID of chargesType to be modify.</param>
        /// <param name="customer">ChargesType data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/ChargesType/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, ChargesTypeModel chargesType)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateChargesType(id, chargesType, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Charges Type data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting chargesType.
        /// </summary>
        /// <param name="id">ID of chargesType to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/ChargesType/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteChargesType(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Charges Type data has been deleted." });
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}