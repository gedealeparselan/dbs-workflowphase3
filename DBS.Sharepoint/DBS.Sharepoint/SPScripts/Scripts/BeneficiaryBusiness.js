﻿var ViewModel = function () {
    var self = this;

    //Model
    //===================
    //added properti by azam
    self.updateCallback = function () { };

    self.SetUpdateCallback = function (callback) {
        self.updateCallback = callback;
    };

    //Properties
    self.BeneficiaryBusinessTypeID = ko.observable(0);
    self.BeneficiaryBusinessTypeCode = ko.observable("");
    self.BeneficiaryBusinessTypeName = ko.observable("");
    self.BeneficiaryBusinessTypeDescription = ko.observable("");
    self.BeneficiaryBusinessTypeIpeCode = ko.observable("");
    self.BeneficiaryBusinessTypeIpeDescription = ko.observable("");
    self.OutputFileGeneration = ko.observable("");
    self.OutputFileGenerationDescription = ko.observable("");
    self.OutputFileXml = ko.observable("");
    self.OutputFileXmlDescription = ko.observable("");
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");   

    //Filter
    self.FilterBeneficiaryBusinessTypeCode = ko.observable("");
    self.FilterBeneficiaryBusinessTypeName = ko.observable("");
    self.FilterBeneficiaryBusinessTypeDescription = ko.observable("");
    self.FilterBeneficiaryBusinessTypeIpeCode = ko.observable("");
    self.FilterBeneficiaryBusinessTypeIpeDescription = ko.observable("");
    self.FilterOutputFileGeneration = ko.observable("");
    self.FilterOutputFileGenerationDescription = ko.observable("");
    self.FilterOutputFileXml = ko.observable("");
    self.FilterOutputFileXmlDescription = ko.observable("");
    self.FilterLastModifiedBy = ko.observable("");
    self.FilterLastModifiedDate = ko.observable("");

    // grid properties
    //self.allowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray([10, 25, 50, 75, 100]);
    self.Page = ko.observable(1);
    self.Size = ko.observableArray([10]);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));
    self.GridProperties().SortColumn("BeneficiaryBusinessTypeCode");
    self.GridProperties().SortColumn("BeneficiaryBusinessTypeIpeCode");
    self.GridProperties().SortColumn("BeneficiaryBusinessTypeIpeDescription");
    self.GridProperties().SortColumn("OutputFileGeneration");
    self.GridProperties().SortColumn("OutputFileGenerationDescription");
    self.GridProperties().SortColumn("OutputFileXml");
    self.GridProperties().SortColumn("OutputFileXmlDescription");
    self.GridProperties().SortOrder("ASC");

    //bind clear data
    self.ClearFilters = function () {
        self.FilterBeneficiaryBusinessTypeCode = ko.observable("");
        self.FilterBeneficiaryBusinessTypeName = ko.observable("");
        self.FilterBeneficiaryBusinessTypeDescription = ko.observable("");
        self.FilterBeneficiaryBusinessTypeIpeCode = ko.observable("");
        self.FilterBeneficiaryBusinessTypeIpeDescription = ko.observable("");
        self.FilterOutputFileGeneration = ko.observable("");
        self.FilterOutputFileGenerationDescription = ko.observable("");
        self.FilterOutputFileXml = ko.observable("");
        self.FilterOutputFileXmlDescription = ko.observable("");
        self.FilterLastModifiedBy = ko.observable("");
        self.FilterLastModifiedDate = ko.observable("");
        GetData();
    };

    // flag
    //self.BarLoad = ko.observable(false);
    self.IsNewData = ko.observable(false);
    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);
    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    //object for storing data from view while entered
    var beneficiiaryBusiness = {
        ID: self.BeneficiaryBusinessTypeID,
        BeneficiaryBusinessTypeCode: self.BeneficiaryBusinessTypeCode,
        BeneficiaryBusinessTypeName: self.BeneficiaryBusinessTypeName,
        BeneficiaryBusinessTypeDescription: self.BeneficiaryBusinessTypeDescription,
        BeneficiaryBusinessTypeIpeCode: self.BeneficiaryBusinessTypeIpeCode,
        BeneficiaryBusinessTypeIpeDescription: self.BeneficiaryBusinessTypeIpeDescription,
        OutputFileGeneration: self.OutputFileGeneration,
        OutputFileGenerationDescription: self.OutputFileGenerationDescription,
        OutputFileXml: self.OutputFileXml,
        OutputFileXmlDescription : self.OutputFileXmlDescription,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
        
    };
    self.GetData = function () {
        GetData();
    };

    self.Beneficiaries = ko.observableArray([]);

    //Get Data from web api set to grid
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.beneficiarybusiness,//
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken           
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    };

    
    // Get filtered columns value
    function GetFilteredColumns() {
        //define filter
        var filters = [];

        if (self.FilterBeneficiaryBusinessTypeCode() != "")
            filters.push({ Field: 'BeneficiaryBusinessTypeCode', Value: self.FilterBeneficiaryBusinessTypeCode() });
        if (self.FilterBeneficiaryBusinessTypeName() != "")
            filters.push({ Field: 'BeneficiaryBusinessTypeName', Value: self.FilterBeneficiaryBusinessTypeName() });
        if (self.FilterBeneficiaryBusinessTypeDescription() != "")
            filters.push({ Field: 'BeneficiaryBusinessTypeDescription', Value: self.FilterBeneficiaryBusinessTypeDescription() });
        if (self.FilterBeneficiaryBusinessTypeIpeCode() != "")
            filters.push({ Field: 'BeneficiaryBusinessTypeIpeCode', Value: self.FilterBeneficiaryBusinessTypeIpeCode() });
        if (self.FilterBeneficiaryBusinessTypeIpeDescription() != "")
            filters.push({ Field: 'BeneficiaryBusinessTypeIpeDescription', Value: self.FilterBeneficiaryBusinessTypeIpeDescription() });
        if (self.FilterOutputFileGeneration() != "")
            filters.push({ Field: 'OutputFileGeneration', Value: self.FilterOutputFileGeneration() });
        if (self.FilterOutputFileGenerationDescription() != "")
            filters.push({ Field: 'OutputFileGenerationDescription', Value: self.FilterOutputFileGenerationDescription() });
        if (self.FilterLastModifiedBy() != "")
            filters.push({ Field: 'ModifiedBy', Value: self.FilterLastModifiedBy() });
        if (self.FilterLastModifiedDate() != "")
            filters.push({ Field: 'ModifiedDate', Value: self.FilterLastModifiedDate() });
        return filters;
    };

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            //console.log(data);
            self.Beneficiaries(data.Rows);
            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }

    //insert new
    self.NewData = function () {
        // flag as new Data
        self.IsNewData(true);
        self.Readonly(false);
        // bind empty data
        self.BeneficiaryBusinessTypeID(0);        
        self.BeneficiaryBusinessTypeCode("");
        self.BeneficiaryBusinessTypeName("");
        self.BeneficiaryBusinessTypeDescription("");
        self.BeneficiaryBusinessTypeIpeCode("");
        self.BeneficiaryBusinessTypeIpeDescription("");
        self.OutputFileGeneration("");
        self.OutputFileGenerationDescription("");
        self.OutputFileXml("");
        self.OutputFileXmlDescription("");
        self.LastModifiedBy("");
        self.LastModifiedDate("");       
    };

    //update
    self.update = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.beneficiarybusiness + "/" + beneficiiaryBusiness.ID(),
                        data: ko.toJSON(beneficiiaryBusiness),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status == 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            })
        }

    };

    //Save Data
    self.save = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();


        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    //console.log(ko.toJSON(Fund));

                    //Ajax call to insert 
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.beneficiarybusiness, //panggil apiConfig
                        data: ko.toJSON(beneficiiaryBusiness), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status == 200) {
                                // send notification
                                ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                   
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    //delete
    self.delete = function () {
        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {

                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.beneficiarybusiness + "/" + beneficiiaryBusiness.ID(),
                    data: ko.toJSON(beneficiiaryBusiness),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status == 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    //permission group / role
    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    //compare user role to page permission to get checker validation
                    console.log("spUser Roles : " + ko.toJSON(spUser.Roles));
                    console.log("spUser Permited : " + data);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                    console.log("IsRoleMaker : " + self.IsRoleMaker());
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }    

    //Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        self.IsNewData(false);
        //console.log(data);
        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');

        self.BeneficiaryBusinessTypeID(data.BeneficiaryBusinessTypeID);
        self.BeneficiaryBusinessTypeCode(data.BeneficiaryBusinessTypeCode);
        self.BeneficiaryBusinessTypeName(data.BeneficiaryBusinessTypeName);
        self.BeneficiaryBusinessTypeDescription(data.BeneficiaryBusinessTypeDescription);
        self.BeneficiaryBusinessTypeIpeCode(data.BeneficiaryBusinessTypeIpeCode);
        self.BeneficiaryBusinessTypeIpeDescription(data.BeneficiaryBusinessTypeIpeDescription);
        self.OutputFileGeneration(data.OutputFileGeneration);
        self.OutputFileGenerationDescription(data.OutputFileGenerationDescription);
        self.OutputFileXml(data.OutputFileXml);
        self.OutputFileXmlDescription(data.OutputFileXmlDescription);
        self.LastModifiedBy(data.ModifiedBy);
        self.LastModifiedDate(moment(data.ModifiedDate).format(config.format.date));        
    };
}