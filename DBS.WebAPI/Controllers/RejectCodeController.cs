﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class RejectCodeController : ApiController
    {
        private string message = string.Empty;

        private readonly IRejectCodeRepo repository = null;

        public RejectCodeController()
        {
            this.repository = new RejectCodeRepository();
        }

        public RejectCodeController(IRejectCodeRepo repo)
        {
            this.repository = repo;
        }


        /// Get all RejectCode

        [ResponseType(typeof(IList<RejectCodeModel>))]
        public HttpResponseMessage Get()
        {
            IList<RejectCodeModel> output = new List<RejectCodeModel>();

            if (!repository.GetRejectCode(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<RejectCodeModel>>(HttpStatusCode.OK, output);
            }
        }


        /// Get Reject Code with maximum rows will be retreive and select page index to display.

        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<RejectCodeModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<RejectCodeModel> output = new List<RejectCodeModel>();

            if (!repository.GetRejectCode(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<RejectCodeModel>>(HttpStatusCode.OK, output);
            }
        }


        /// <summary>
        /// Get Reject Code for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(RejectCodeModel))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<RejectCodeFilter> filters)
        {
            RejectCodeGrid output = new RejectCodeGrid();

            if (!repository.GetRejectCode(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<RejectCodeGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find Reject Code using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<FXComplianceModel>))]
        [HttpGet]
        [Route("api/RejectCode/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<RejectCodeModel> output = new List<RejectCodeModel>();

            if (!repository.GetRejectCode(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<RejectCodeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get FX Compliance by ID.
        /// </summary>
        /// <param name="id">FX Compliance ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(RejectCodeModel))]
        [Route("api/RejectCode/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            RejectCodeModel output = new RejectCodeModel();

            if (!repository.GetRejectedCodeByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<RejectCodeModel>(HttpStatusCode.OK, output);
            }
        }


        /// <summary>
        /// Create a new reject Code.
        /// </summary>
        /// <param name="FXCompliance">FX Compliance data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(RejectCodeModel RejectCode)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddRejectCode(RejectCode, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Reject Code data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing Reject Code.
        /// </summary>
        /// <param name="id">ID of RejectCode to be modify.</param>
        /// <param name="FXCompliance">RejectCode data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/RejectCode/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, RejectCodeModel RejectCode)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateRejectCode(id, RejectCode, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Reject Code data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting Reject Code.
        /// </summary>
        /// <param name="id">ID of Reject Code to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/RejectCode/{id}")]
        [Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteRejectCode(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "RejectCode data has been deleted." });
                }
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }

    }
}
