﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBS.Sharepoint.Model
{
    public class EmailProperty
    {
        public string EmailBody { get; set; }
        public string Subject { get; set; }
        public string UserTo { get; set; }
    }
}
