﻿//var api_server = "http://localhost/dbs.webapi/";
var accessToken;
var ca;
var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;


    //Declare observable to Checkbox Control
    self.selectedTopUrgent = ko.observable();
    self.selectedNewCustomer = ko.observable(false);
    self.selectedSignVerification = ko.observable();
    self.selectedDormantAccount = ko.observable();
    self.selectedFreezeAccount = ko.observable();

    //Declare observable to Selected
    self.Products = ko.observableArray([]);
    self.selectedProduct = ko.observable("");

    self.Currencies = ko.observableArray([]);
    self.selectedCurrency = ko.observable("");
    
    self.DebitAccCcy = ko.observableArray([]);
    self.selectedDebitAccCcy = ko.observable("");

    self.Channels = ko.observableArray([]);
    self.selectedChannel = ko.observable("");

    self.BeneSegments = ko.observableArray([]);
    self.selectedBeneSegment = ko.observable("");

    self.DocTypes = ko.observableArray([]);
    self.selectedDocType = ko.observable("");

    self.BeneBanks = ko.observableArray([]);
    self.selectedBeneBank = ko.observable();

    //Declare an ObservableArray for Storing the JSON Response
    self.AllLampiran = ko.observableArray([]);
    self.Lampiran = ko.observableArray([]);
    self.selected = ko.observable(self.Lampiran()[0]);

    //Declare observable which will be bind with UI underlying transaction
    self.ID = ko.observable(0);
    //self.workflowInstanceID = ko.observable(00000000-0000-0000-0000-000000000000);
    self.applicationID = ko.observable("");
    self.customerName = ko.observable("");
    self.cif = ko.observable("");
    self.productID = ko.observable("");
    self.currencyID = ko.observable("");
    self.rate = ko.observable("");
    self.transactionAmount = ko.observable("");
    self.eqvUsd = ko.observable("");
    self.channelID = ko.observable("");
    self.debitAccNumber = ko.observable("");
    self.debitAccCcy = ko.observable("");
    self.applicationDate = ko.observable("");
    self.beneSegmentID = ko.observable("");
    self.lldCode = ko.observable("");
    self.lldInfo = ko.observable("");
    self.topUrgent = ko.observable(false);
    self.newCustomer = ko.observable(false);
    self.signatureVerification = ko.observable(false);
    self.dormantAccount = ko.observable(false);
    self.freezeAccount = ko.observable(false);
    self.isDraft = ko.observable(false);
    self.others = ko.observable("");
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");
    self.fxTransaction = ko.observable("");
    self.currencyDesc = ko.observable("");
    self.productName = ko.observable("");
    self.stateID = ko.observable(0);
    self.topUrgentDesc = ko.observable("");
    self.tzNumber = ko.observable("");

    //new variable 20140915
    self.beneName = ko.observable("");
    self.beneBankID = ko.observable("");
    self.bankCode = ko.observable("");
    self.beneAccNumber = ko.observable("");
    self.detailCompliance = ko.observable("");

    //Declare observable which will be bind with UI Attachment
    self.transactionDocumentID = ko.observable(0);
    self.docTypeID = ko.observable("");
    self.documentPath = ko.observable("");
    self.lastModifiedByAttachment = ko.observable("");
    self.lastModifiedDateAttachment = ko.observable("");

    //Declare observable for submit transacion
    self.url = ko.observable("_api/web/lists/getbytitle('listtransaction')/items");

    //Declare observable for customer
    //self.Customers = ko.observableArray([]);

    // grid properties
    self.availableSizes = ko.observableArray([10, 25, 50, 75, 100]);
    self.Page = ko.observable(1);
    self.Size = ko.observableArray([10]);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    // filter
    self.FilterDocTypeID = ko.observable("");
    self.FilterDocumentPath = ko.observable("");
    self.FilterLastModifiedByAttchment = ko.observable("");
    self.FilterlastModifiedDateAttchment = ko.observable("");

    // sorting
    self.SortColumn = ko.observable("TransactionDocumentID");
    self.SortOrder = ko.observable("ASC");

    // New Data flag
    self.IsNewData = ko.observable(false);

    var a = {
        productID: self.selectedProduct()['ID'],
        productName: self.selectedProduct()['Name'],
        curID: self.selectedCurrency()['ID'],
        curName: self.selectedCurrency()['Description'],
        channelID: self.selectedChannel()['ID'],
        channelName: self.selectedChannel()['ChannelName'],
        beneID: self.selectedBeneSegment()['ID'],
        beneName: self.selectedBeneSegment()['Name']
    };

    //The Object which stored data entered in the observables
    var PpuMakerTransaction = {
        ID: self.ID,
        workflowInstanceID: self.workflowInstanceID,
        applicationID: self.applicationID,
        customerName: self.customerName,
        cif: self.cif,
        productID: self.productID,
        currencyID: self.currencyID,
        rate: self.rate,
        transactionAmount: self.transactionAmount,
        eqvUsd: self.eqvUsd,
        channelID: self.channelID,
        debitAccNumber: self.debitAccNumber,
        debitAccCcy: self.debitAccCcy,
        applicationDate: self.applicationDate,
        beneSegmentID: self.beneSegmentID,
        lldCode: self.lldCode,
        lldInfo: self.lldInfo,
        topUrgent: self.topUrgent,
        newCustomer: self.selectedNewCustomer,
        signatureVerification: self.selectedSignVerification,
        dormantAccount: self.selectedDormantAccount,
        freezeAccount: self.selectedFreezeAccount,
        isDraft: self.isDraft,
        others: self.others,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate,
        transactionDocument: self.Lampiran,

        fxTransaction: self.fxTransaction,
        currencyDesc: self.currencyDesc,
        productName: self.productName,
        stateID: self.stateID,
        topUrgentDesc: self.topUrgentDesc,
        tzNumber: self.tzNumber,

        beneName: self.beneName,
        bankID: self.beneBankID,
        detailCompliance: self.detailCompliance
    };

    //---------------------------------- Start Contol Checkbox ----------------------------------

    //Checkbox Control Top Urgent
    //self.selectedTopUrgent = ko.observable();
    self.topUrgent = ko.computed({
        read: function () {
            return self.selectedTopUrgent() == 1 ? true : false;
        },
        write: function (newValue) {
            self.selectedTopUrgent(newValue ? 1 : 0);
            //alert(self.selectedTopUrgent());
        }
    });

    //Checkbox Control New Customer
    //self.selectedNewCustomer = ko.observable(false);
    self.newCustomer = ko.computed({
        read: function () {
            return self.selectedNewCustomer() == 1 ? true : false;
        },
        write: function (newValue) {
            self.selectedNewCustomer(newValue ? 1 : 0);
            $("#customerName").val("");
            $("#CIF").val("");
            this.Customers("");
        }
    });

    //Checkbox Control Signature Verification
    //self.selectedSignVerification = ko.observable();
    self.signVerification = ko.computed({
        read: function () {
            return self.selectedSignVerification() == 1 ? true : false;
        },
        write: function (newValue) {
            self.selectedSignVerification(newValue ? 1 : 0);
            //alert(self.selectedSignVerification());
        }
    });

    //Checkbox Control Dormant Account
    //self.selectedDormantAccount = ko.observable();
    self.dormantAccount = ko.computed({
        read: function () {
            return self.selectedDormantAccount() == 1 ? true : false;
        },
        write: function (newValue) {
            self.selectedDormantAccount(newValue ? 1 : 0);
            //alert(self.selectedDormantAccount());
        }
    });

    //Checkbox Control Freeze Account
    //self.selectedFreezeAccount = ko.observable();
    self.freezeAccount = ko.computed({
        read: function () {
            return self.selectedFreezeAccount() == 1 ? true : false;
        },
        write: function (newValue) {
            self.selectedFreezeAccount(newValue ? 1 : 0);
            //alert(self.selectedFreezeAccount());
        }
    });

    //---------------------------------- End Contol Checkbox ----------------------------------

    //---------------------------------- Start Contol DropdownList ----------------------------------

    //Declare an ObservableArray for Storing the JSON Response
    //self.Products = ko.observableArray([]);
    //self.selectedProduct = ko.observable("");
    self.selProduct = ko.dependentObservable(function () {
        var value = self.selectedProduct();
        return ko.utils.arrayFirst(self.Products, function (product) {
            return product.ID === value;
        });
    });
    //alert(self.selectedProduct()[].ID);

    GetProducts();

    function GetProducts() {
        //alert('GetProducts');
        //Ajax Call Get All Products Records
        $.ajax({
            type: "GET",
            url: api.server + api.url.product,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.Products(data); //Put the response in ObservableArray
                } else {
                    //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    //Declare an ObservableArray for Storing the JSON Response
    //self.Currencies = ko.observableArray([]);
    //self.selectedCurrency = ko.observable("");
    self.selCurrency = ko.dependentObservable(function () {
        var value = self.selectedCurrency();
        return ko.utils.arrayFirst(self.Currencies, function (currency) {
            return currency.ID === value;
        });
    });

    GetCurrencies();

    //Function to Get All Currencies
    function GetCurrencies() {
        //alert('GetCurrencies');
        $.ajax({
            type: "GET",
            url: api.server + api.url.currency,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.Currencies(data);
                    self.DebitAccCcy(data);
                } else {
                    //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    //Declare an ObservableArray for Storing the JSON Response
    //self.Channels = ko.observableArray([]);
    //self.selectedChannel = ko.observable("");
    self.selChannel = ko.dependentObservable(function () {
        var value = self.selectedChannel();
        return ko.utils.arrayFirst(self.Channels, function (channel) {
            return channel.ID === value;
        });
    });

    GetChannels();

    //Function to Get All Channel
    function GetChannels() {
        //alert('GetChannels');
        $.ajax({
            type: "GET",
            url: api.server + api.url.channel,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.Channels(data); //Put the response in ObservableArray
                } else {
                    //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    //Declare an ObservableArray for Storing the JSON Response
    //self.BeneSegments = ko.observableArray([]);
    //self.selectedBeneSegment = ko.observable("");
    self.selSegment = ko.dependentObservable(function () {
        var value = self.selectedBeneSegment();
        return ko.utils.arrayFirst(self.BeneSegments, function (segment) {
            return segment.ID === value;
        });
    });

    GetBeneSegments();

    //Function to Get All Bene Segment
    function GetBeneSegments() {
        //alert('GetBeneSegments');
        $.ajax({
            type: "GET",
            url: api.server + api.url.benesegment,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.BeneSegments(data); //Put the response in ObservableArray
                } else {
                    //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    //Declare an ObservableArray for Storing the JSON Response
    //self.DocTypes = ko.observableArray([]);
    //self.selectedDocType = ko.observable("");
    self.selDocType = ko.dependentObservable(function () {
        var value = self.selectedDocType();
        return ko.utils.arrayFirst(self.DocTypes, function (segment) {
            return segment.ID === value;
        });
    });

    GetDocTypes();

    //Function to Get All DocTypes
    function GetDocTypes() {
        //alert("doctypes");
        $.ajax({
            type: "GET",
            url: api.server + api.url.doctype,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.DocTypes(data); //Put the response in ObservableArray
                } else {
                    //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    //Declare an ObservableArray for Storing the JSON Response
    //self.DocTypes = ko.observableArray([]);
    //self.selectedDocType = ko.observable("");
    self.selBeneBank = ko.dependentObservable(function() {
    	var value = self.selectedBeneBank();
    	return ko.utils.arrayFirst(self.BeneBanks, function(benebank) {
    		return benebank.ID === value.Code;
     	});
    });

    self.onChangeBeneBank = function () {
    	var a = this.selectedBeneBank();
        var obj;
        for (var i=0; i<self.BeneBanks().length; i++) {
            if ( self.BeneBanks()[i].ID == a ) {
                //alert("result");
                obj = self.BeneBanks()[i];
                break;
            }
        }
        if (typeof a == 'undefined') { $("#bankID").val("") }
        else { $("#bankID").val(obj.Code); }
    }
    
    GetBanks();

    //Function to Read All Bank
    function GetBanks() {
        //Ajax Call Get All Currencies Records
        $.ajax({
            type: "GET",
            url: api.server + api.url.bank,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.BeneBanks(data);
                    //alert(ko.toJSON(data));
                } else {
                    //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    //---------------------------------- End Contol Dropdownlist ----------------------------------
    

    //---------------------------------- Start Contol Lampiran ----------------------------------
    self.addLampiran = function () {
        self.Lampiran.push({
            TransactionDocumentID: self.transactionDocumentID(),
            TransactionID: self.ID(),
            DocTypeID: self.selectedDocType(),
            DocumentPath: self.documentPath(),
            LastModifiedBy: self.lastModifiedByAttachment(),
            LastModifiedDate: self.lastModifiedDateAttachment
        });

        $("#modal-form").modal('hide');

        self.Lampiran();
        //console.log(JSON.stringify(self.Lampiran()));
        //alert(ko.toJSON(self.documentPath()));
    }

    self.rowIndex = ko.observable(0);
    self.updateLampiran = function (data) {
        self.Lampiran.replace(self.Lampiran()[self.rowIndex], {
            TransactionDocumentID: self.transactionDocumentID(),
            TransactionID: self.ID(),
            DocTypeID: self.selectedDocType(),
            DocumentPath: self.documentPath(),
            LastModifiedBy: self.lastModifiedByAttachment(),
            LastModifiedDate: self.lastModifiedDateAttachment
        });
        $("#modal-form").modal('hide');
    };

    self.select = function (item) {
        self.IsNewData(false);
        self.rowIndex = self.Lampiran.indexOf(item);
        //alert(self.rowIndex);

        $("#modal-form").modal('show');

        //set value modal form
        self.selected(item);
        self.transactionDocumentID(self.selected().TransactionDocumentID);
        self.ID(self.selected().TransactionID);
        self.selectedDocType(self.selected().DocTypeID);
        self.documentPath(self.selected().DocumentPath);
    };

    self.removeLampiran = function (lampiran) {
        $("#modal-form").modal('hide');
        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                //$("#modal-form").modal('show');
            } else {
                self.Lampiran.remove(lampiran);
            }
        });
    };

    GetLampiran();

    //Get Upload Instruction form and docs
    function GetLampiran() {
        // define filter
        var filters = [];
        if (self.FilterDocTypeID() != "") filters.push({ Field: 'DocTypeID', Value: self.FilterDocTypeID() });
        if (self.FilterDocumentPath() != "") filters.push({ Field: 'DocumentPath', Value: self.FilterDocumentPath() });
        if (self.FilterLastModifiedByAttchment() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterLastModifiedByAttchment() });
        if (self.FilterlastModifiedDateAttchment() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterlastModifiedDateAttchment() });

        if (self.ID() == "") {
            return;
        }

        //Ajax Call Get All Attachments Records
        $.support.cors = true;
        $.ajax({
            type: filters.length > 0 ? "POST" : "GET",
            url: api.server + api.url.transaction + "?transactionID=" + self.ID() + "&page=" + self.Page() + "&size=" + self.Size() + "&sort_column=" + self.SortColumn() + "&sort_order=" + self.SortOrder(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: ko.toJSON(filters),
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.Lampiran(data.Rows); //Put the response in ObservableArray
                    //console.log(data.Rows);
                    //alert(self.ID());

                    self.Page(data['Page']);
                    self.Size([data['Size']]);
                    self.Total(data['Total']);
                    self.TotalPages(Math.ceil(self.Total() / self.Size()));
                } else {
                    //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetAllLampiran() {
        //alert('GetProducts');
        //Ajax Call Get All Products Records
        $.ajax({
            type: "GET",
            url: api.server + api.url.transaction + "/transactionID=" + self.ID(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.AllLampiran(data); //Put the response in ObservableArray
                } else {
                    //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        self.IsNewData(false);

        $("#modal-form").modal('show');

        self.transactionDocumentID(data.TransactionDocumentID);
        //self.transactionID(data.TransactionID);
        self.docTypeID(data.DocTypeID);
        self.documentPath(data.DocumentPath);
        //self.lastModifiedByAttchment(data.LastModifiedBy);
        //self.lastModifiedDateAttchment(data.LastModifiedDate);
    };

    //insert new
    self.NewData = function () {
        // flag as new Data
        self.IsNewData(true);

        // bind empty data
        //self.transactionDocumentID= 0;
        //$('#docTypeID').val(self.selectedDocType());
        //$('#documentPath').val("");
        //self.documentPath= "";
    };

    self.onPageSizeChange = function () {
        self.Page(1);

        GetLampiran();
    };

    self.onPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        GetLampiran();
    };

    self.nextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            GetLampiran();
        }
    }

    self.previousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            GetLampiran();
        }
    }

    self.firstPage = function () {
        self.Page(1);

        GetLampiran();
    }

    self.lastPage = function () {
        self.Page(self.TotalPages());

        GetLampiran();
    }

    self.filter = function () {
        self.Page(1);

        GetLampiran();
    }

    self.sorting = function (column) {
        //alert(column)

        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        GetLampiran();
    }

    function Confirm(text) {
        bootbox.confirm(text, function (result) {
            return result;
        });
    }

    //---------------------------------- End Contol Lampiran ----------------------------------

    //---------------------------------- Start Contol Save & Submit ----------------------------------
    //Save Transaction PPUMaker
    self.save = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

		PpuMakerTransaction.customerName= $("#customerName").val();
        PpuMakerTransaction.cif= $("#CIF").val();
        PpuMakerTransaction.eqvUsd = $("#eqvUsd").val();
        PpuMakerTransaction.productID = self.selectedProduct();
        PpuMakerTransaction.currencyID = self.selectedCurrency();
        PpuMakerTransaction.channelID = self.selectedChannel();
        PpuMakerTransaction.beneSegmentID = self.selectedBeneSegment();
        PpuMakerTransaction.bankID = self.selectedBeneBank();
		
        PpuMakerTransaction.topUrgent = self.topUrgent;
        PpuMakerTransaction.newCustomer = self.newCustomer;
        PpuMakerTransaction.signatureVerification = self.signVerification;
        PpuMakerTransaction.dormantAccount = self.selectedDormantAccount;
        PpuMakerTransaction.freezeAccount = self.freezeAccount;
        
        if( self.newCustomer() == true ) {
        	PpuMakerTransaction.debitAccNumber = self.debitAccNumber();
        	PpuMakerTransaction.debitAccCcy = self.selectedDebitAccCcy();
        }
        else {
        	PpuMakerTransaction.debitAccNumber = this.selectedCustomer;
        	PpuMakerTransaction.debitAccCcy = $("#debitAccCcyId").val();
        }

        // Converting the JSON string with JSON.stringify
        // then saving with localStorage in the name of session
        if (self.Lampiran().length < 1) { 
        	self.LampiranTemp = ko.observableArray([]);
	        self.LampiranTemp.push({
	            TransactionDocumentID: 0,
	            TransactionID: 0,
	            DocTypeID: 0,
	            DocumentPath: null,
	            LastModifiedBy: null,
	            LastModifiedDate: null
	        }); 
	        PpuMakerTransaction.transactionDocument = self.LampiranTemp();
	        console.log(ko.toJSON(PpuMakerTransaction));
        }
        else {
        	localStorage.setItem('session', JSON.stringify(self.Lampiran()));
        	
        	// transform the String generated through
	        // JSON.stringify and saved in localStorage in JSON object again
	        var restoredSession = JSON.parse(localStorage.getItem('session'));
	
	        PpuMakerTransaction.transactionDocument = restoredSession;
        }

        if (form.valid()) {
            //Ajax call to insert the BeneSegment
            $.ajax({
                type: "POST",
                url: api.server + api.url.transaction,
                data: ko.toJSON(PpuMakerTransaction), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + $.cookie(api.cookie.name)
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        //ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                        self.ID(data.ID);
                        //console.log(data);

                        GetLampiran();
                        alert(self.ID());
                    } else {
                        //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }
    };

    self.Update = function (ca) {
        // validation
        var form = $("#aspnetForm");
        form.validate();

		PpuMakerTransaction.applicationID = ca;
        PpuMakerTransaction.customerName= $("#customerName").val();
        PpuMakerTransaction.cif= $("#CIF").val();
        PpuMakerTransaction.eqvUsd = $("#eqvUsd").val();
        PpuMakerTransaction.productID = self.selectedProduct();
        PpuMakerTransaction.currencyID = self.selectedCurrency();
        PpuMakerTransaction.channelID = self.selectedChannel();
        PpuMakerTransaction.beneSegmentID = self.selectedBeneSegment();
        PpuMakerTransaction.bankID = self.selectedBeneBank();
		
        PpuMakerTransaction.topUrgent = self.topUrgent;
        PpuMakerTransaction.newCustomer = self.newCustomer;
        PpuMakerTransaction.signatureVerification = self.signVerification;
        PpuMakerTransaction.dormantAccount = self.selectedDormantAccount;
        PpuMakerTransaction.freezeAccount = self.freezeAccount;
        
        if( self.newCustomer() == true ) {
        	PpuMakerTransaction.debitAccNumber = self.debitAccNumber();
        	PpuMakerTransaction.debitAccCcy = self.selectedDebitAccCcy();
        }
        else {
        	PpuMakerTransaction.debitAccNumber = this.selectedCustomer;
        	PpuMakerTransaction.debitAccCcy = $("#debitAccCcyId").val();
        }

        // Converting the JSON string with JSON.stringify
        // then saving with localStorage in the name of session
        if (self.Lampiran().length < 1) { 
        	self.LampiranTemp = ko.observableArray([]);
	        self.LampiranTemp.push({
	            TransactionDocumentID: 0,
	            TransactionID: 0,
	            DocTypeID: 0,
	            DocumentPath: null,
	            LastModifiedBy: null,
	            LastModifiedDate: null
	        }); 
	        PpuMakerTransaction.transactionDocument = self.LampiranTemp();
	        console.log(ko.toJSON(PpuMakerTransaction));
        }
        else {
        	localStorage.setItem('session', JSON.stringify(self.Lampiran()));
        	
        	// transform the String generated through
	        // JSON.stringify and saved in localStorage in JSON object again
	        var restoredSession = JSON.parse(localStorage.getItem('session'));
	
	        PpuMakerTransaction.transactionDocument = restoredSession;
        }

        if (form.valid()) {
            //Ajax call to insert the BeneSegment
            $.ajax({
                type: "POST",
                url: api.server + api.url.transaction,
                data: ko.toJSON(PpuMakerTransaction), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + $.cookie(api.cookie.name)
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        //ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                        self.ID(data.ID);
                        //console.log(data);

                        GetLampiran();
                        alert(self.ID());
                    } else {
                        //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }
    };


    var submitTest = {
        Title: 'Test',
        ApplicationID: '99',
        //Username: 'Test Username',
        TopUrgent: 'Test TopUrgent',
        CustomerName: 'Test CustName',
        CIF: '67676767676',
        Product: 'Test Product',
        Currency: 'Test Currency',
        //TransactionAmount: '1',
        Rate: '1',
        EqvUsd: '1',
        Channel: '1',
        DebitAccNumber: '1',
        DebitAccCcy: '1',
        ApplicationDate: '9/9/2014',
        //BeneBank: 'Test Benebank',
        //BeneName: 'Test Benename',
        //BankCode: 'Test Bankcode',
        //BeneAccNumber: 'Test Beneaccnumber',
        BeneSegment: 'Test Benesegment',
        LldCode: 'Test LldCode',
        lldInfo: 'Test LldInfo',
        SignatureVerification: true,
        DormantAccount: true,
        FreezeAccount: true,
        Others: 'Test Others',
        //Created: '9/9/2014',
        //Author: 'Test Author',

        __metadata: {
            type: 'SP.Data.ListTransactionListItem'
        }
    }

    var submit = {
        Title: 'PPU Maker',
        ApplicationID: self.applicationID,
        //Username: PpuMakerTransaction,
        TopUrgent: self.topUrgent,
        CustomerName: self.customerName,
        CIF: self.cif,
        Product: self.selectedProducts,
        Currency: self.selectedCurrency(),
        //TransactionAmount: PpuMakerTransaction,
        Rate: self.rate,
        EqvUsd: self.eqvUsd,
        Channel: self.selectedChannel(),
        DebitAccNumber: self.debitAccNumber,
        DebitAccCcy: self.debitAccCcy,
        ApplicationDate: self.applicationDate,
        //BeneBank: PpuMakerTransaction,
        //BeneName: PpuMakerTransaction,
        //BankCode: PpuMakerTransaction,
        //BeneAccNumber: PpuMakerTransaction,
        BeneSegment: self.selectedBeneSegment(),
        LldCode: self.lldCode,
        lldInfo: self.lldInfo,
        SignatureVerification: true,
        DormantAccount: true,
        FreezeAccount: true,
        Others: self.others,
        //BeneBank:
        //Created: '9/9/2014',
        //Author: 'Test Author',

        __metadata: {
            type: 'SP.Data.ListTransactionListItem'
        }
    }

    function GetApplicationID() {
        $.support.cors = true;
        //alert(self.selectedProduct());
        $.ajax({
            type: "GET",
            url: api.server + api.url.transaction + "/productid=" + self.selectedProduct(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    SubmitNew(data.ApplicationID);
                    //ca = data.ApplicationID;
                    //alert('data'+data.ApplicationID);
                } else {
                    //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                alert('error ');
            }
        });
    }

    self.onSubmit = function () {
        GetApplicationID();
    }

    function SubmitNew(ca) {
        self.Update(ca);
        //alert(ca);
        var productText = document.getElementById("product");
        var currencyText = document.getElementById("currency");
        var channelText = document.getElementById("channel");
        var beneSegmentText = document.getElementById("beneSegment");
        var beneBankText = document.getElementById("beneBank");

        //set value
        submit.ApplicationID = ca;
        submit.Product = productText.options[productText.selectedIndex].text;
        submit.Currency = currencyText.options[currencyText.selectedIndex].text;
        submit.Channel = channelText.options[channelText.selectedIndex].text;
        submit.BeneSegment = beneSegmentText.options[beneSegmentText.selectedIndex].text;
        //submit.BeneSegment = beneSegmentText.options[beneBankText.selectedIndex].text;
        if (self.topUrgent() == true) {
            submit.TopUrgent = "Top Urgent";
        }
        else {
            submit.TopUrgent = "Normal";
        }

        alert(ko.toJSON(submit));

        jQuery.ajax({
            //url: appweburl + self.url(),
            url: "/" + self.url(),
            type: "POST",
            data: ko.toJSON(submit),
            contentType: "application/json;odata=verbose",
            //processData: true,
            headers: {
                "accept": "application/json;odata=verbose",
                //"Content-Type": "application/json;odata=verbose",
                //"X-RequestDigest": formDigestValue
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
            },
            success: "",
            error: ""
        });
    }


    //---------------------------------- End Contol Save & Submit ----------------------------------

};

function upload() {
    var form = $("#aspnetForm");
    form.validate();

    /*if(form.validate()) {
     if( $( "#id-input-file-2" ).val() != null ) {
     UploadFile();
     alert($( "#id-input-file-2" ).val());
     }
     }*/
}

// Upload the file.
// You can upload files up to 2 GB with the REST API.
function uploadFile() {

    // Define the folder path for this example.
    var serverRelativeUrlToFolder = '/Instruction Document Library';

    // Get test values from the file input and text input page controls.
    var fileInput = jQuery('#id-input-file-2');
    var newName = jQuery('#id-input-file-2').val().split('\\').pop();
    var customerName = jQuery('#customerName').val();

    // Get the server URL.
    //var serverUrl = _spPageContextInfo.webAbsoluteUrl;
    var serverUrl = "http://romansantosa-pc:2828";

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBuffer();
    getFile.done(function (arrayBuffer) {
        //alert('alert1');
        // Add the file to the SharePoint folder.
        var addFile = addFileToFolder(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            //alert('alert2');
            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {
                //alert('alert3');
                // Change the display name and title of the list item.
                var changeItem = updateListItem(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    alert('file uploaded and updated');
                });
                changeItem.fail(onError);
            });
            getItem.fail(onError);
        });
        addFile.fail(onError);
    });
    getFile.fail(onError);

    // Get the local file as an array buffer.
    function getFileBuffer() {
        var deferred = jQuery.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }
        reader.readAsArrayBuffer(fileInput[0].files[0]);
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(arrayBuffer) {

        // Get the file name from the file input control on the page.
        var parts = fileInput[0].value.split('\\');
        var fileName = parts[parts.length - 1];

        // Construct the endpoint.
        // var fileCollectionEndpoint = String.format("{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" + "/add(overwrite=true, url='{2}')",serverUrl, serverRelativeUrlToFolder, fileName);
        var fileCollectionEndpoint = "" + serverUrl + "/_api/web/getfolderbyserverrelativeurl('" + serverRelativeUrlToFolder + "')/files/add(overwrite=true, url='" + fileName + "')";
        // Send the request and return the response.
        // This call returns the SharePoint file.
        return jQuery.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItem(fileListItemUri) {

        // Send the request and return the response.
        return jQuery.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItem(itemMetadata) {

        // Define the list item changes. Use the FileLeafRef property to change the display name. 
        // For simplicity, also use the name as the title. 
        // The example gets the list item type from the item's metadata, but you can also get it from the
        // ListItemEntityTypeFullName property of the list.
        var body = String.format("{{'__metadata':{{'type':'{0}'}},'FileLeafRef':'{1}','Title':'{2}','CustomerName':'{3}'}}",
            itemMetadata.type, newName, newName, customerName);

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return jQuery.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: body,
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                "content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}

// Display error messages. 
function onError(error) {
    alert(error.responseText);
}

$(document).ready(function () {
    accessToken = $.cookie(api.cookie.name);
    ko.applyBindings(new ViewModel());

    /*
     $('#documentPath1 , #id-input-file-2').ace_file_input({
     no_file:'Attach file ...',
     btn_choose:'Choose',
     btn_change:null,
     droppable:false,
     onchange:null,
     thumbnail:false //| true | large
     //whitelist:'gif|png|jpg|jpeg'
     //blacklist:'exe|php'
     //onchange:''
     //
     });*/

    // autocomplete
    $("#customerName").autocomplete({
        source: function (request, response) {
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.customer + "/Search?query=" + request.term + "&limit=10",
                data: {
                    query: request.term,
                    limit: 10
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
        },
        minLength: 2,
        select: function (event, ui) {
            $("#customerName").val(ui.item.Name);
            $("#CIF").val(ui.item.ID);
            GetCustomers($("#CIF").val())
            GetAccount($("#CIF").val());
        }
    });

    $('.date-picker').datepicker({autoclose: true}).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });  
    
	//================================= Eqv Calculation ====================================
	
	var x = document.getElementById("trxnAmount");
	var y = document.getElementById("rate");
	var d = document.getElementById("eqvUsd");
	var xstored = x.getAttribute("data-in");
	var ystored = y.getAttribute("data-in");
	setInterval(function(){
		if( x == document.activeElement ){
			var temp = x.value;
	 		if( xstored != temp ){
			   xstored = temp;
			   x.setAttribute("data-in",temp);
			   calculate();
	 		}
		}
		if( y == document.activeElement ){
		 	var temp = y.value;
		 	if( ystored != temp ){
			   ystored = temp;
			   y.setAttribute("data-in",temp);
			   calculate();
			}
		}
	},50);

	function calculate(){
	 	var res = x.value / y.value;
	 	$("#eqvUsd").val(res);
	}
	x.onblur = calculate;
	calculate();
	
	//================================= End Eqv Calculation ====================================  
});

//Function to Get All DocTypes
function GetAccount(cif) {
    $.ajax({
        type: "GET",
        url: api.server + api.url.customer + "/" + cif + api.url.customeraccount,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + accessToken
        },
        success: function (data, textStatus, jqXHR) {
            //alert(JSON.stringify(data))
            if (jqXHR.status = 200) {
            	$("#debitAccCcy").val(data.Currency.Code);
            	$("#debitAccCcyId").val(data.Currency.ID);
            } else {
                //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // send notification
            //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    });
}

this.Customers = ko.observableArray([]);
this.selectedCustomer = ko.observable("");

function GetCustomers(cif) {
    //Ajax Call Get All Currencies Records
    $.ajax({
        type: "GET",
        url: api.server + api.url.customer + "/" + cif,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + accessToken
        },
        success: function (data, textStatus, jqXHR) {
            if (jqXHR.status = 200) {
                self.Customers(data);
            } else {
                //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // send notification
            //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    });
}

function OnSuccessAutoComplete(response, data, textStatus, jqXHR) {
    response($.map(data, function (item) {
            return {
                // default autocomplete object
                label: item.CIF + " - " + item.Name,
                value: item.Name,

                // custom object binding
                ID: item.CIF,
                Name: item.Name
            }
        })
    );
}




