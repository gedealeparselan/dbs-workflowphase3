﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("TypeOfTransaction")]
    public class TypeOfTransactionModel
    {
        public int ID { get; set; }
        public int ProductID { get; set; }
        public string TransactionType { get; set; }
        public string ParameterTypeDisplay { get; set; }

        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }

    public class TypeOfTransactionRow : TypeOfTransactionModel
    {
        public int RowID { get; set; }

    }

    public class TypeOfTransactionGrid : Grid
    {
        public IList<TypeOfTransactionRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class TypeOfTransactionFilter : Filter { }
    #endregion

    #region Interface
    public interface ITypeOfTransactionRepository : IDisposable
    {
        bool GetTypeOfTransactionByID(int id, ref TypeOfTransactionModel TypeOfTransaction, ref string message);
        bool GetTypeOfTransaction(ref IList<TypeOfTransactionModel> TypeOfTransactionModels, int limit, int index, ref string message);
        bool GetTypeOfTransaction(ref IList<TypeOfTransactionModel> TypeOfTransactionModels, ref string message);
        bool GetTypeOfTransaction(int page, int size, IList<TypeOfTransactionFilter> filters, string sortColumn, string sortOrder, ref TypeOfTransactionGrid TypeOfTransaction, ref string message);
        bool GetTypeOfTransaction(TypeOfTransactionFilter filter, ref IList<TypeOfTransactionModel> TypeOfTransactionModels, ref string message);
        bool GetTypeOfTransaction(string key, int limit, ref IList<TypeOfTransactionModel> TypeOfTransactionModels, ref string message);
        bool AddTypeOfTransaction(TypeOfTransactionModel TypeOfTransaction, ref string message);
        bool UpdateTypeOfTransaction(int id, TypeOfTransactionModel TypeOfTransaction, ref string message);
        bool DeleteTypeOfTransaction(int id, ref string message);
    }
    #endregion

    #region Repository
    public class TypeOfTransactionRepository : ITypeOfTransactionRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetTypeOfTransactionByID(int id, ref TypeOfTransactionModel TypeOfTransaction, ref string message)
        {
            bool isSuccess = false;

            try
            {
                TypeOfTransaction = (from a in context.TransactionTypes
                                     where a.IsDeleted.Equals(false) && a.TransTypeID.Equals(id)
                           select new TypeOfTransactionModel
                           {
                               ID = a.TransTypeID,
                               TransactionType = a.TransactionType1,
                               ParameterTypeDisplay = a.ParameterTypeDisplay,
                               LastModifiedDate = a.CreateDate,
                               LastModifiedBy = a.CreateBy
                           }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetTypeOfTransaction(ref IList<TypeOfTransactionModel> TypeOfTransactions, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    TypeOfTransactions = (from a in context.TransactionTypes
                                where a.IsDeleted.Equals(false)
                                          orderby new { a.TransactionType1 }
                                select new TypeOfTransactionModel
                                {
                                    ID = a.TransTypeID,
                                    TransactionType = a.TransactionType1,
                                    ParameterTypeDisplay =a.ParameterTypeDisplay,
                                    LastModifiedBy = a.CreateBy,
                                    LastModifiedDate = a.CreateDate
                                }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetTypeOfTransaction(ref IList<TypeOfTransactionModel> TypeOfTransactions, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    TypeOfTransactions = (from a in context.TransactionTypes
                                where a.IsDeleted.Equals(false)
                                orderby new { a.TransactionType1 }
                                select new TypeOfTransactionModel
                                {
                                    ID = a.TransTypeID,
                                    TransactionType = a.TransactionType1,
                                    ParameterTypeDisplay =a.ParameterTypeDisplay,
                                    LastModifiedBy = a.CreateBy,
                                    LastModifiedDate = a.CreateDate
                                }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetTypeOfTransaction(int page, int size, IList<TypeOfTransactionFilter> filters, string sortColumn, string sortOrder, ref TypeOfTransactionGrid customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                TypeOfTransactionModel filter = new TypeOfTransactionModel();
                if (filters != null)
                {
                    //filter.ProductID = (int)DBSProductID.DFProductIDCons;
                    filter.TransactionType = filters.Where(a => a.Field.Equals("TransactionType")).Select(a => a.Value).SingleOrDefault();
                    filter.ParameterTypeDisplay = filters.Where(a => a.Field.Equals("ParameterTypeDisplay")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.TransactionTypes
                                let isTransactionType = !string.IsNullOrEmpty(filter.TransactionType)
                                let isParameterTypeDisplay = !string.IsNullOrEmpty(filter.ParameterTypeDisplay)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) && 
                                    a.ProductID.Equals((int)DBSProductID.DFProductIDCons) &&
                                    (isTransactionType ? a.TransactionType1.Contains(filter.TransactionType) : true) &&
                                    (isParameterTypeDisplay ? a.ParameterTypeDisplay.Contains(filter.ParameterTypeDisplay) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new TypeOfTransactionModel
                                {
                                    ID = a.TransTypeID,
                                    TransactionType = a.TransactionType1,
                                    ParameterTypeDisplay = a.ParameterTypeDisplay,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    customer.Page = page;
                    customer.Size = size;
                    customer.Total = data.Count();
                    customer.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    customer.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new TypeOfTransactionRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            TransactionType = a.TransactionType,
                            ParameterTypeDisplay = a.ParameterTypeDisplay,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetTypeOfTransaction(TypeOfTransactionFilter filter, ref IList<TypeOfTransactionModel> TypeOfTransactions, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetTypeOfTransaction(string key, int limit, ref IList<TypeOfTransactionModel> TypeOfTransactions, ref string message)
        {
            bool isSuccess = false;

            try
            {
                TypeOfTransactions = (from a in context.TransactionTypes
                            where a.IsDeleted.Equals(false) &&
                                a.TransactionType1.Contains(key)
                            orderby new { a.TransactionType1 }
                            select new TypeOfTransactionModel
                            {
                                TransactionType = a.TransactionType1,

                            }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddTypeOfTransaction(TypeOfTransactionModel TypeOfTransaction, ref string message)
        {
            bool isSuccess = false;
            int prodId = 0;
            //int.TryParse(DBSProductID.DFProductIDCons.ToString(),out prodId);
            try
            {
                if (!context.TransactionTypes.Where(a => a.TransactionType1.Equals(TypeOfTransaction.TransactionType) && a.IsDeleted.Equals(false)).Any())
                {
                    context.TransactionTypes.Add(new Entity.TransactionType()
                    {
                        ProductID = (int)DBSProductID.DFProductIDCons,
                        TransactionType1 = TypeOfTransaction.TransactionType,
                        ParameterTypeDisplay = TypeOfTransaction.ParameterTypeDisplay,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("TypeOfTransaction  data for transaction type {0} is already exist.", TypeOfTransaction.TransactionType);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


        public bool UpdateTypeOfTransaction(int id, TypeOfTransactionModel TypeOfTransaction, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.TransactionType data = context.TransactionTypes.Where(a => a.TransTypeID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.TransactionTypes.Where(a => a.TransTypeID != TypeOfTransaction.ID && a.TransactionType1.Equals(TypeOfTransaction.TransactionType) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("TypeOfTransaction name {0} is exist.", TypeOfTransaction.TransactionType);
                    }
                    else
                    {
                        data.TransactionType1 = TypeOfTransaction.TransactionType;
                        data.ParameterTypeDisplay = TypeOfTransaction.ParameterTypeDisplay;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("TypeOfTransaction data for Transaction Type ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteTypeOfTransaction(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.TransactionType data = context.TransactionTypes.Where(a => a.TransTypeID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("TypeOfTransaction data for Transaction Type ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}