﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class SPUserTaskController : ApiController
    {
        private string message = string.Empty;

        private readonly ISPUserTaskRepository repository = null;

        #region Default Workflow Values
        private const string _state = "Running, Complete, Cancelled, Error";
        private const string _outcome = "None, Approved, Rejected, Pending, Cancelled, NotRequired, Continue, Delegated, Custom, Override Approved/Rejected/Continue";
        private const string _customOutcome = "Approve, Reject, Pending, Cancel, Revise, Submit, Revise to Payment Maker, Revise to PPU Maker, UTC, Submit Revise";
        #endregion

        public SPUserTaskController()
        {
            this.repository = new SPUserTaskRepository();
        }

        public SPUserTaskController(ISPUserTaskRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all user tasks by multiple workflow id.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(SPUserTaskGrid))]
        [AcceptVerbs("GET", "POST")]
        
        public HttpResponseMessage Get(Guid webID, Guid siteID, string workflowIDs, string sort_column, string sort_order, IList<SPUserTaskFilter> filters, int? page = 1, int? size = 10, string state = _state, string outcome = _outcome, string customOutcome = _customOutcome, bool showContribute = false, bool showActiveTask = true)
        {
            IList<Guid> _workflowIDs = new List<Guid>();
            IList<string> _state = new List<string>();
            IList<string> _outcome = new List<string>();
            IList<string> _customOutcome = new List<string>();

            try
            {
                #region Workflow Parameters Validation
                // validate workflow id
                if (!string.IsNullOrEmpty(workflowIDs))
                {
                    string[] strWorkflowIDs = workflowIDs.Split(',');
                    foreach (var item in strWorkflowIDs)
                    {
                        _workflowIDs.Add(Guid.Parse(item));
                    }
                }

                // validate state id
                if (!string.IsNullOrEmpty(state))
                {
                    string[] strStateIDs = state.Split(',');
                    foreach (var item in strStateIDs)
                    {
                        _state.Add(item.Trim());
                    }
                }

                // validate oucome id
                if (!string.IsNullOrEmpty(outcome))
                {
                    string[] strOutcomeIDs = outcome.Split(',');
                    foreach (var item in strOutcomeIDs)
                    {
                        _outcome.Add(item.Trim());
                    }
                }

                // validate custom outcome id
                if (!string.IsNullOrEmpty(customOutcome))
                {
                    string[] strCustomOutcomeIDs = customOutcome.Split(',');
                    foreach (var item in strCustomOutcomeIDs)
                    {
                        _customOutcome.Add(item.Trim());
                    }
                }
                #endregion
                
                SPUserTaskGrid output = new SPUserTaskGrid();

                if (!repository.GetUserTasks(webID, siteID, _workflowIDs, _state, _outcome, _customOutcome, showContribute, showActiveTask, page, size, sort_column, sort_order, filters, ref output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse<SPUserTaskGrid>(HttpStatusCode.OK, output);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ex.Message });
            }
            finally
            {
                _workflowIDs = null;
            }
        }

        /// <summary>
        /// Get My User Task
        /// </summary>
        /// <returns></returns>    
        [ResponseType(typeof(SPUserTaskGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/SPUserTask/MyUserTask")]
        public HttpResponseMessage GetMyTask(Guid webID, Guid siteID, string workflowIDs, string sort_column, string sort_order, IList<SPUserTaskFilter> filters, int? page = 1, int? size = 10, string state = _state, string outcome = _outcome, string customOutcome = _customOutcome, bool showContribute = false, bool showActiveTask = true)
        {
            IList<Guid> _workflowIDs = new List<Guid>();
            IList<string> _state = new List<string>();
            IList<string> _outcome = new List<string>();
            IList<string> _customOutcome = new List<string>();

            try
            {
                #region Workflow Parameters Validation
                // validate workflow id
                if (!string.IsNullOrEmpty(workflowIDs))
                {
                    string[] strWorkflowIDs = workflowIDs.Split(',');
                    foreach (var item in strWorkflowIDs)
                    {
                        _workflowIDs.Add(Guid.Parse(item));
                    }
                }

                // validate state id
                if (!string.IsNullOrEmpty(state))
                {
                    string[] strStateIDs = state.Split(',');
                    foreach (var item in strStateIDs)
                    {
                        _state.Add(item.Trim());
                    }
                }

                // validate oucome id
                if (!string.IsNullOrEmpty(outcome))
                {
                    string[] strOutcomeIDs = outcome.Split(',');
                    foreach (var item in strOutcomeIDs)
                    {
                        _outcome.Add(item.Trim());
                    }
                }

                // validate custom outcome id
                if (!string.IsNullOrEmpty(customOutcome))
                {
                    string[] strCustomOutcomeIDs = customOutcome.Split(',');
                    foreach (var item in strCustomOutcomeIDs)
                    {
                        _customOutcome.Add(item.Trim());
                    }
                }
                #endregion

                SPUserTaskGrid output = new SPUserTaskGrid();

                if (!repository.GetMyUserTasks(webID, siteID, _workflowIDs, _state, _outcome, _customOutcome, showContribute, showActiveTask, page, size, sort_column, sort_order, filters, ref output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse<SPUserTaskGrid>(HttpStatusCode.OK, output);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ex.Message });
            }
            finally
            {
                _workflowIDs = null;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            
            base.Dispose(disposing);
        }
    }
}