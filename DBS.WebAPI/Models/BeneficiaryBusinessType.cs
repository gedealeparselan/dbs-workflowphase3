﻿using System;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("BeneficiaryBusinessType")]
    public class BeneficiaryBusinessTypeModel
    {
        public int BeneficiaryBusinessTypeID { get; set; }
        public string BeneficiaryBusinessTypeCode { get; set; }
        public string BeneficiaryBusinessTypeName { get; set; }
        public string BeneficiaryBusinessTypeDescription { get; set; }
        public string BeneficiaryBusinessTypeIpeCode { get; set; }
        public string BeneficiaryBusinessTypeIpeDescription { get; set; }
        public string OutputFileGeneration { get; set; }
        public string OutputFileGenerationDescription { get; set; }
        public string OutputFileXml { get; set; }
        public string OutputFileXmlDescription { get; set; }
        public bool IsDeleted { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }

    public class BeneficiaryBusinessRow : BeneficiaryBusinessTypeModel
    {
        public int RowID { get; set; }
    }
    public class BeneficiaryBusinessTypeGrid : Grid
    {
        public IList<BeneficiaryBusinessRow> Rows { get; set; }
    }
    #endregion
    #region Filter
    public class BeneficiaryBusinessTypeFilter : Filter
    {

    }
    #endregion
    #region Interface
    public interface IBeneficiaryBusinessType : IDisposable
    {
        bool GetBeneficiaryByID(int Id, ref BeneficiaryBusinessTypeModel BeneficiaryBusiness, ref string Message);
        bool GetBeneficiary(ref IList<BeneficiaryBusinessTypeModel> beneficiaryList, int index, int limit, ref string message);
        bool GetBeneficiary(ref IList<BeneficiaryBusinessTypeModel> beneficiaryList, ref string message);
        bool GetBeneficiary(BeneficiaryBusinessTypeFilter beneficiaryFilter, ref IList<BeneficiaryBusinessTypeModel> beneficiaryModel, ref string message);
        bool GetBeneficiary(int page, int size, IList<BeneficiaryBusinessTypeFilter> filters, string sortColumn, string sortOrder, ref BeneficiaryBusinessTypeGrid BeneficiaryGrid, ref string message);
        bool GetBeneficiary(string Key, int Limit, ref IList<BeneficiaryBusinessTypeModel> BeneficiaryBusiness, ref string Message);
        bool AddBeneficiary(BeneficiaryBusinessTypeModel beneficiaryModel, ref string message);
        bool UpdateBeneficiary(int id, BeneficiaryBusinessTypeModel beneficiaryModel, ref string message);
        bool DeleteBeneficiary(int id, ref string message);
        bool GetBeneficiaryBusinessParams(string key, ref IList<BeneficiaryBusinessTypeModel> BeneficiaryBusiness, ref string Message);
        bool GetBeneficiaryBusinessParamsBusiness(string key, string pid, ref IList<BeneficiaryBusinessTypeModel> BeneficiaryBusiness, ref string Message);
    }

    #endregion
    #region Repository
    public class BeneficiaryRepository : IBeneficiaryBusinessType
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetBeneficiary(ref IList<BeneficiaryBusinessTypeModel> beneficiaryList, ref string message)
        {
            bool isSucces = false;
            try
            {
                using (DBSEntities contex = new DBSEntities())
                {
                    beneficiaryList = (from beneficiary in contex.BeneficiaryBusinessTypes
                                       where beneficiary.IsDeleted.Equals(false)
                                       orderby new { beneficiary.BeneficiaryBusinessTypeName }
                                       select new BeneficiaryBusinessTypeModel
                                       {
                                           BeneficiaryBusinessTypeID = beneficiary.BeneficiaryBusinessTypeID,
                                           BeneficiaryBusinessTypeCode = beneficiary.BeneficiaryBusinessTypeCode,
                                           BeneficiaryBusinessTypeName = beneficiary.BeneficiaryBusinessTypeName,
                                           BeneficiaryBusinessTypeDescription = beneficiary.BeneficiaryBusinessTypeDescription,
                                           BeneficiaryBusinessTypeIpeCode = beneficiary.BeneficiaryBusinessTypeIpeCode,
                                           BeneficiaryBusinessTypeIpeDescription = beneficiary.BeneficiaryBusinessTypeIpeDescription,
                                           OutputFileGeneration = beneficiary.OutPutFileGeneration,
                                           OutputFileGenerationDescription = beneficiary.OutPutFileGenerationDescription,
                                           OutputFileXml = beneficiary.OutPutFileXml,
                                           OutputFileXmlDescription = beneficiary.OutPutFileXmlDescription,
                                           ModifiedBy = beneficiary.UpdateBy == null ? beneficiary.CreateBy : beneficiary.UpdateBy,
                                           ModifiedDate = beneficiary.UpdateDate == null ? beneficiary.CreateDate : beneficiary.UpdateDate
                                       }).ToList();
                }
                isSucces = true;
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return isSucces;
        }

        public bool AddBeneficiary(BeneficiaryBusinessTypeModel beneficiaryModel, ref string message)
        {
            bool isSucces = false;
            try
            {
                if (!context.BeneficiaryBusinessTypes.Where(a => a.BeneficiaryBusinessTypeName.ToLower() == beneficiaryModel.BeneficiaryBusinessTypeName.ToLower() && a.IsDeleted.Equals(false)).Any())
                {
                    context.BeneficiaryBusinessTypes.Add(new Entity.BeneficiaryBusinessType()
                    {
                        BeneficiaryBusinessTypeName = beneficiaryModel.BeneficiaryBusinessTypeName,
                        BeneficiaryBusinessTypeCode = beneficiaryModel.BeneficiaryBusinessTypeCode,
                        BeneficiaryBusinessTypeDescription = beneficiaryModel.BeneficiaryBusinessTypeDescription,
                        BeneficiaryBusinessTypeIpeCode = beneficiaryModel.BeneficiaryBusinessTypeIpeCode,
                        BeneficiaryBusinessTypeIpeDescription = beneficiaryModel.BeneficiaryBusinessTypeIpeDescription,
                        OutPutFileGeneration = beneficiaryModel.OutputFileGeneration,
                        OutPutFileGenerationDescription = beneficiaryModel.OutputFileGenerationDescription,
                        OutPutFileXml = beneficiaryModel.OutputFileXml,
                        OutPutFileXmlDescription = beneficiaryModel.OutputFileXmlDescription,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().LoginName
                    });
                    context.SaveChanges();
                    isSucces = true;
                }
                else
                {
                    message = string.Format("Beneficialy data for name {0} are already exist.", beneficiaryModel.BeneficiaryBusinessTypeName);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return isSucces;
        }

        public bool UpdateBeneficiary(int id, BeneficiaryBusinessTypeModel beneficiaryModel, ref string message)
        {
            bool isSucces = false;
            try
            {
                Entity.BeneficiaryBusinessType data = context.BeneficiaryBusinessTypes.Where(a => a.BeneficiaryBusinessTypeID == id).SingleOrDefault();
                if (data != null)
                {
                    if (context.BeneficiaryBusinessTypes.Where(a => a.BeneficiaryBusinessTypeName.ToLower() == beneficiaryModel.BeneficiaryBusinessTypeName.ToLower() && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Beneficialy for {0} are already exist", beneficiaryModel.BeneficiaryBusinessTypeName);
                    }
                    else
                    {
                        data.BeneficiaryBusinessTypeName = beneficiaryModel.BeneficiaryBusinessTypeName;
                        data.BeneficiaryBusinessTypeCode = beneficiaryModel.BeneficiaryBusinessTypeCode;
                        data.BeneficiaryBusinessTypeDescription = beneficiaryModel.BeneficiaryBusinessTypeDescription;
                        data.BeneficiaryBusinessTypeIpeCode = beneficiaryModel.BeneficiaryBusinessTypeIpeCode;
                        data.BeneficiaryBusinessTypeIpeDescription = beneficiaryModel.BeneficiaryBusinessTypeIpeDescription;
                        data.OutPutFileGeneration = beneficiaryModel.OutputFileGeneration;
                        data.OutPutFileGenerationDescription = beneficiaryModel.OutputFileGenerationDescription;
                        data.OutPutFileXml = beneficiaryModel.OutputFileXml;
                        data.OutPutFileXmlDescription = beneficiaryModel.OutputFileXmlDescription;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        context.SaveChanges();
                        isSucces = true;
                    }
                }
                else
                {
                    message = string.Format("Beneficialy data for ID {0} is does not exist.", id);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return isSucces;
        }

        public bool DeleteBeneficiary(int id, ref string message)
        {
            bool isSuccces = false;
            try
            {
                Entity.BeneficiaryBusinessType data = context.BeneficiaryBusinessTypes.Where(a => a.BeneficiaryBusinessTypeID == id).SingleOrDefault();
                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateBy = currentUser.GetCurrentUser().LoginName;
                    data.UpdateDate = DateTime.UtcNow;
                    context.SaveChanges();
                    isSuccces = true;
                }
                else
                {
                    message = string.Format("Beneficialy for ID {0} is does not exist", id);

                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return isSuccces;
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public bool GetBeneficiary(int page, int size, IList<BeneficiaryBusinessTypeFilter> filters, string sortColumn, string sortOrder, ref BeneficiaryBusinessTypeGrid BeneficiaryGrid, ref string message)
        {
            bool isSucces = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();
                BeneficiaryBusinessTypeModel filter = new BeneficiaryBusinessTypeModel();
                if (filters != null)
                {

                    filter.BeneficiaryBusinessTypeCode = filters.Where(a => a.Field.Equals("BeneficiaryBusinessTypeCode")).Select(a => a.Value).SingleOrDefault();
                    filter.BeneficiaryBusinessTypeName = filters.Where(a => a.Field.Equals("BeneficiaryBusinessTypeName")).Select(a => a.Value).SingleOrDefault();
                    filter.BeneficiaryBusinessTypeDescription = filters.Where(a => a.Field.Equals("BeneficiaryBusinessTypeDescription")).Select(a => a.Value).SingleOrDefault();
                    filter.BeneficiaryBusinessTypeIpeCode = filters.Where(a => a.Field.Equals("BeneficiaryBusinessTypeIpeCode")).Select(a => a.Value).SingleOrDefault();
                    filter.BeneficiaryBusinessTypeIpeDescription = filters.Where(a => a.Field.Equals("BeneficiaryBusinessTypeIpeDescription")).Select(a => a.Value).SingleOrDefault();
                    filter.OutputFileGeneration = filters.Where(a => a.Field.Equals("OutputFileGeneration")).Select(a => a.Value).SingleOrDefault();
                    filter.OutputFileGenerationDescription = filters.Where(a => a.Field.Equals("OutputFileGenerationDescription")).Select(a => a.Value).SingleOrDefault();
                    filter.OutputFileXml = filters.Where(a => a.Field.Equals("OutputFileXml")).Select(a => a.Value).SingleOrDefault();
                    filter.OutputFileXmlDescription = filters.Where(a => a.Field.Equals("OutputFileXmlDescription")).Select(a => a.Value).SingleOrDefault();
                    filter.ModifiedBy = filters.Where(a => a.Field.Equals("ModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("ModifiedDate")).Any())
                    {
                        filter.ModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("ModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.ModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities contex = new DBSEntities())
                {
                    var data = (from Beneficiary in contex.BeneficiaryBusinessTypes
                                join EmployeeUpdate in contex.Employees on Beneficiary.UpdateBy equals EmployeeUpdate.EmployeeUsername into zu
                                from EmployeeUpdate in zu.DefaultIfEmpty()
                                join EmployeeCreated in contex.Employees on Beneficiary.CreateBy equals EmployeeCreated.EmployeeUsername into zc
                                from EmployeeCreated in zc.DefaultIfEmpty()
                                let isCode = !string.IsNullOrEmpty(filter.BeneficiaryBusinessTypeCode)
                                let isName = !string.IsNullOrEmpty(filter.BeneficiaryBusinessTypeName)
                                let isDescription = !string.IsNullOrEmpty(filter.BeneficiaryBusinessTypeDescription)
                                let isIpeCode = !string.IsNullOrEmpty(filter.BeneficiaryBusinessTypeIpeCode)
                                let isIpeDescription = !string.IsNullOrEmpty(filter.BeneficiaryBusinessTypeIpeDescription)
                                let isOutputFileGeneration = !string.IsNullOrEmpty(filter.OutputFileGeneration)
                                let isOutputFileGenerationDescription = !string.IsNullOrEmpty(filter.OutputFileGenerationDescription)
                                let isOutputFileXml = !string.IsNullOrEmpty(filter.OutputFileXml)
                                let isOutputFileXmlDescription = !string.IsNullOrEmpty(filter.OutputFileXmlDescription)
                                let isModifiedBy = !string.IsNullOrEmpty(filter.ModifiedBy)
                                let isModifiedDate = filter.ModifiedDate.HasValue ? true : false
                                //let isCreatedBy = !string.IsNullOrEmpty(filter.)
                                where Beneficiary.IsDeleted.Equals(false) &&
                                    (isCode ? Beneficiary.BeneficiaryBusinessTypeCode.Contains(filter.BeneficiaryBusinessTypeCode) : true) &&
                                    (isName ? Beneficiary.BeneficiaryBusinessTypeName.Contains(filter.BeneficiaryBusinessTypeName) : true) &&
                                    (isDescription ? Beneficiary.BeneficiaryBusinessTypeDescription.Contains(filter.BeneficiaryBusinessTypeDescription) : true) &&
                                    (isIpeCode ? Beneficiary.BeneficiaryBusinessTypeIpeCode.Contains(filter.BeneficiaryBusinessTypeIpeCode) : true) &&
                                    (isIpeDescription ? Beneficiary.BeneficiaryBusinessTypeIpeDescription.Contains(filter.BeneficiaryBusinessTypeIpeDescription) : true) &&
                                    (isOutputFileGeneration ? Beneficiary.OutPutFileGeneration.Contains(filter.OutputFileGeneration) : true) &&
                                    (isOutputFileGenerationDescription ? Beneficiary.OutPutFileGenerationDescription.Contains(filter.OutputFileGenerationDescription) : true) &&
                                    (isModifiedBy ? (Beneficiary.UpdateBy == null ? EmployeeCreated.EmployeeUsername.Contains(filter.ModifiedBy) : EmployeeUpdate.EmployeeName.Contains(filter.ModifiedBy)) : true) &&
                                    (isModifiedDate ? ((Beneficiary.UpdateDate == null ? Beneficiary.CreateDate : Beneficiary.UpdateDate) > filter.ModifiedDate.Value && ((Beneficiary.UpdateDate == null ? Beneficiary.CreateDate : Beneficiary.UpdateDate.Value) < maxDate)) : true)

                                select new BeneficiaryBusinessTypeModel
                                {
                                    BeneficiaryBusinessTypeID = Beneficiary.BeneficiaryBusinessTypeID,
                                    BeneficiaryBusinessTypeCode = Beneficiary.BeneficiaryBusinessTypeCode,
                                    BeneficiaryBusinessTypeDescription = Beneficiary.BeneficiaryBusinessTypeDescription,
                                    BeneficiaryBusinessTypeIpeCode = Beneficiary.BeneficiaryBusinessTypeIpeCode,
                                    BeneficiaryBusinessTypeIpeDescription = Beneficiary.BeneficiaryBusinessTypeIpeDescription,
                                    OutputFileGeneration = Beneficiary.OutPutFileGeneration,
                                    OutputFileGenerationDescription = Beneficiary.OutPutFileGenerationDescription,
                                    OutputFileXml = Beneficiary.OutPutFileXml,
                                    OutputFileXmlDescription = Beneficiary.OutPutFileXmlDescription,
                                    BeneficiaryBusinessTypeName = Beneficiary.BeneficiaryBusinessTypeName,
                                    ModifiedBy = Beneficiary.UpdateBy == null ? EmployeeCreated.EmployeeName : EmployeeUpdate.EmployeeName,
                                    ModifiedDate = Beneficiary.UpdateDate == null ? Beneficiary.CreateDate : Beneficiary.UpdateDate
                                });
                    BeneficiaryGrid.Page = page;
                    BeneficiaryGrid.Size = size;
                    BeneficiaryGrid.Total = data.Count();
                    BeneficiaryGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    BeneficiaryGrid.Rows = data.OrderBy(orderBy).AsEnumerable().
                        Select((a, i) => new BeneficiaryBusinessRow
                        {
                            RowID = i + 1,
                            BeneficiaryBusinessTypeID = a.BeneficiaryBusinessTypeID,
                            BeneficiaryBusinessTypeDescription = a.BeneficiaryBusinessTypeDescription,
                            BeneficiaryBusinessTypeCode = a.BeneficiaryBusinessTypeCode,
                            BeneficiaryBusinessTypeIpeCode = a.BeneficiaryBusinessTypeIpeCode,
                            BeneficiaryBusinessTypeIpeDescription = a.BeneficiaryBusinessTypeIpeDescription,
                            OutputFileGeneration = a.OutputFileGeneration,
                            OutputFileGenerationDescription = a.OutputFileGenerationDescription,
                            OutputFileXml = a.OutputFileXml,
                            OutputFileXmlDescription = a.OutputFileXmlDescription,
                            BeneficiaryBusinessTypeName = a.BeneficiaryBusinessTypeName,
                            ModifiedBy = a.ModifiedBy,
                            ModifiedDate = a.ModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .Distinct()
                        .ToList();
                }
                isSucces = true;
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return isSucces;
        }


        public bool GetBeneficiary(string Key, int Limit, ref IList<BeneficiaryBusinessTypeModel> BeneficiaryBusiness, ref string Message)
        {
            bool isSuccess = false;

            try
            {
                BeneficiaryBusiness = (from beneficiary in context.BeneficiaryBusinessTypes
                                       where beneficiary.IsDeleted.Equals(false) &&
                                       beneficiary.BeneficiaryBusinessTypeCode.ToString().Contains(Key) || beneficiary.BeneficiaryBusinessTypeName.Contains(Key) || beneficiary.BeneficiaryBusinessTypeDescription.Contains(Key) || beneficiary.BeneficiaryBusinessTypeIpeCode.ToString().Contains(Key) || beneficiary.BeneficiaryBusinessTypeIpeDescription.Contains(Key) || beneficiary.UpdateBy.Contains(Key)
                                       orderby new { beneficiary.BeneficiaryBusinessTypeCode, beneficiary.BeneficiaryBusinessTypeName }
                                       select new BeneficiaryBusinessTypeModel
                                       {
                                           BeneficiaryBusinessTypeID = beneficiary.BeneficiaryBusinessTypeID,
                                           BeneficiaryBusinessTypeCode = beneficiary.BeneficiaryBusinessTypeCode,
                                           BeneficiaryBusinessTypeDescription = beneficiary.BeneficiaryBusinessTypeDescription,
                                           BeneficiaryBusinessTypeIpeCode = beneficiary.BeneficiaryBusinessTypeIpeCode,
                                           BeneficiaryBusinessTypeIpeDescription = beneficiary.BeneficiaryBusinessTypeIpeDescription,
                                           OutputFileGeneration = beneficiary.OutPutFileGeneration,
                                           OutputFileGenerationDescription = beneficiary.OutPutFileGenerationDescription,
                                           OutputFileXml = beneficiary.OutPutFileXml,
                                           OutputFileXmlDescription = beneficiary.OutPutFileXmlDescription,
                                           ModifiedBy = beneficiary.UpdateBy == null ? beneficiary.CreateBy : beneficiary.UpdateBy,
                                           ModifiedDate = beneficiary.UpdateDate == null ? beneficiary.CreateDate : beneficiary.UpdateDate
                                       }).Take(Limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBeneficiaryByID(int Id, ref BeneficiaryBusinessTypeModel BeneficiaryBusiness, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                BeneficiaryBusiness = (from beneficiary in context.BeneficiaryBusinessTypes
                                       where beneficiary.IsDeleted.Equals(false) && beneficiary.BeneficiaryBusinessTypeID.Equals(Id)
                                       select new BeneficiaryBusinessTypeModel
                                       {
                                           BeneficiaryBusinessTypeID = beneficiary.BeneficiaryBusinessTypeID,
                                           BeneficiaryBusinessTypeCode = beneficiary.BeneficiaryBusinessTypeCode,
                                           BeneficiaryBusinessTypeDescription = beneficiary.BeneficiaryBusinessTypeDescription,
                                           BeneficiaryBusinessTypeIpeCode = beneficiary.BeneficiaryBusinessTypeIpeCode,
                                           BeneficiaryBusinessTypeIpeDescription = beneficiary.BeneficiaryBusinessTypeIpeDescription,
                                           OutputFileGeneration = beneficiary.OutPutFileGeneration,
                                           OutputFileGenerationDescription = beneficiary.OutPutFileGenerationDescription,
                                           OutputFileXml = beneficiary.OutPutFileXml,
                                           OutputFileXmlDescription = beneficiary.OutPutFileXmlDescription,
                                           ModifiedBy = beneficiary.UpdateBy == null ? beneficiary.CreateBy : beneficiary.UpdateBy,
                                           ModifiedDate = beneficiary.UpdateDate == null ? beneficiary.CreateDate : beneficiary.UpdateDate
                                       }).SingleOrDefault();
                isSuccess = true;
            }
            catch (Exception e)
            {
                Message = e.Message;
            }
            return isSuccess;
        }


        public bool GetBeneficiaryBusinessParams(string key, ref IList<BeneficiaryBusinessTypeModel> BeneficiaryBusiness, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities cont = new DBSEntities())
                {
                    BeneficiaryBusiness = (from beneficiary in cont.BeneficiaryBusinessTypes
                                           where beneficiary.IsDeleted.Equals(false) && (beneficiary.BeneficiaryBusinessTypeName.Contains(key) || beneficiary.BeneficiaryBusinessTypeCode.Contains(key))
                                           orderby new { beneficiary.BeneficiaryBusinessTypeCode, beneficiary.BeneficiaryBusinessTypeName }
                                           select new BeneficiaryBusinessTypeModel
                                           {
                                               BeneficiaryBusinessTypeID = beneficiary.BeneficiaryBusinessTypeID,
                                               BeneficiaryBusinessTypeCode = beneficiary.BeneficiaryBusinessTypeCode,
                                               BeneficiaryBusinessTypeDescription = beneficiary.BeneficiaryBusinessTypeDescription,
                                               BeneficiaryBusinessTypeIpeCode = beneficiary.BeneficiaryBusinessTypeIpeCode,
                                               BeneficiaryBusinessTypeIpeDescription = beneficiary.BeneficiaryBusinessTypeIpeDescription,
                                               OutputFileGeneration = beneficiary.OutPutFileGeneration,
                                               OutputFileGenerationDescription = beneficiary.OutPutFileGenerationDescription,
                                               OutputFileXml = beneficiary.OutPutFileXml,
                                               OutputFileXmlDescription = beneficiary.OutPutFileXmlDescription,
                                               ModifiedBy = beneficiary.UpdateBy == null ? beneficiary.CreateBy : beneficiary.UpdateBy,
                                               ModifiedDate = beneficiary.UpdateDate == null ? beneficiary.CreateDate : beneficiary.UpdateDate
                                           }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception e)
            {
                Message = e.Message;
            }
            return isSuccess;
        }


        public bool GetBeneficiaryBusinessParamsBusiness(string key, string pid, ref IList<BeneficiaryBusinessTypeModel> BeneficiaryBusiness, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities cont = new DBSEntities())
                {
                    int intPID = int.Parse(pid);
                    if (intPID == 0)
                    {
                        BeneficiaryBusiness = (from beneficiary in cont.BeneficiaryBusinessTypes
                                               where beneficiary.IsDeleted.Equals(false) && (beneficiary.BeneficiaryBusinessTypeName.StartsWith(key) || beneficiary.BeneficiaryBusinessTypeCode.StartsWith(key))
                                               orderby new { beneficiary.BeneficiaryBusinessTypeCode, beneficiary.BeneficiaryBusinessTypeName }
                                               select new BeneficiaryBusinessTypeModel
                                               {
                                                   BeneficiaryBusinessTypeID = beneficiary.BeneficiaryBusinessTypeID,
                                                   BeneficiaryBusinessTypeCode = beneficiary.BeneficiaryBusinessTypeCode,
                                                   BeneficiaryBusinessTypeDescription = beneficiary.BeneficiaryBusinessTypeDescription,
                                                   BeneficiaryBusinessTypeIpeCode = beneficiary.BeneficiaryBusinessTypeIpeCode,
                                                   BeneficiaryBusinessTypeIpeDescription = beneficiary.BeneficiaryBusinessTypeIpeDescription,
                                                   OutputFileGeneration = beneficiary.OutPutFileGeneration,
                                                   OutputFileGenerationDescription = beneficiary.OutPutFileGenerationDescription,
                                                   OutputFileXml = beneficiary.OutPutFileXml,
                                                   OutputFileXmlDescription = beneficiary.OutPutFileXmlDescription,
                                                   ModifiedBy = beneficiary.UpdateBy == null ? beneficiary.CreateBy : beneficiary.UpdateBy,
                                                   ModifiedDate = beneficiary.UpdateDate == null ? beneficiary.CreateDate : beneficiary.UpdateDate
                                               }).ToList();
                    }
                    else
                    {
                        BeneficiaryBusiness = (from beneficiary in cont.BeneficiaryBusinessTypes
                                               where beneficiary.IsDeleted.Equals(false) && (beneficiary.BeneficiaryBusinessTypeName.StartsWith(key) || beneficiary.BeneficiaryBusinessTypeCode.StartsWith(key))
                                               orderby new { beneficiary.BeneficiaryBusinessTypeCode, beneficiary.BeneficiaryBusinessTypeName }
                                               select new BeneficiaryBusinessTypeModel
                                               {
                                                   BeneficiaryBusinessTypeID = beneficiary.BeneficiaryBusinessTypeID,
                                                   BeneficiaryBusinessTypeCode = beneficiary.BeneficiaryBusinessTypeCode,
                                                   BeneficiaryBusinessTypeDescription = beneficiary.BeneficiaryBusinessTypeDescription,
                                                   BeneficiaryBusinessTypeIpeCode = beneficiary.BeneficiaryBusinessTypeIpeCode,
                                                   BeneficiaryBusinessTypeIpeDescription = beneficiary.BeneficiaryBusinessTypeIpeDescription,
                                                   OutputFileGeneration = beneficiary.OutPutFileGeneration,
                                                   OutputFileGenerationDescription = beneficiary.OutPutFileGenerationDescription,
                                                   OutputFileXml = beneficiary.OutPutFileXml,
                                                   OutputFileXmlDescription = beneficiary.OutPutFileXmlDescription,
                                                   ModifiedBy = beneficiary.UpdateBy == null ? beneficiary.CreateBy : beneficiary.UpdateBy,
                                                   ModifiedDate = beneficiary.UpdateDate == null ? beneficiary.CreateDate : beneficiary.UpdateDate
                                               }).ToList();
                    }
                }
                isSuccess = true;
            }
            catch (Exception e)
            {
                Message = e.Message;
            }
            return isSuccess;
        }


        public bool GetBeneficiary(ref IList<BeneficiaryBusinessTypeModel> beneficiaryList, int index, int limit, ref string message)
        {
            bool isSuccess = false;
            try
            {
                int skip = limit * index;
                using (DBSEntities cont = new DBSEntities())
                {
                    beneficiaryList = (from beneficiary in cont.BeneficiaryBusinessTypes
                                       where beneficiary.IsDeleted.Equals(false)
                                       orderby new { beneficiary.BeneficiaryBusinessTypeCode, beneficiary.BeneficiaryBusinessTypeName }
                                       select new BeneficiaryBusinessTypeModel
                                       {
                                           BeneficiaryBusinessTypeID = beneficiary.BeneficiaryBusinessTypeID,
                                           BeneficiaryBusinessTypeCode = beneficiary.BeneficiaryBusinessTypeCode,
                                           BeneficiaryBusinessTypeDescription = beneficiary.BeneficiaryBusinessTypeDescription,
                                           BeneficiaryBusinessTypeIpeCode = beneficiary.BeneficiaryBusinessTypeIpeCode,
                                           BeneficiaryBusinessTypeIpeDescription = beneficiary.BeneficiaryBusinessTypeIpeDescription,
                                           OutputFileGeneration = beneficiary.OutPutFileGeneration,
                                           OutputFileGenerationDescription = beneficiary.OutPutFileGenerationDescription,
                                           OutputFileXml = beneficiary.OutPutFileXml,
                                           OutputFileXmlDescription = beneficiary.OutPutFileXmlDescription,
                                           ModifiedBy = beneficiary.UpdateBy == null ? beneficiary.CreateBy : beneficiary.UpdateBy,
                                           ModifiedDate = beneficiary.UpdateDate == null ? beneficiary.CreateDate : beneficiary.UpdateDate
                                       }).Skip(skip).Take(limit).ToList();
                }
                isSuccess = true;
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return isSuccess;
        }

        public bool GetBeneficiary(BeneficiaryBusinessTypeFilter beneficiaryFilter, ref IList<BeneficiaryBusinessTypeModel> beneficiaryModel, ref string message)
        {
            throw new NotImplementedException();
        }
    }
    #endregion
}