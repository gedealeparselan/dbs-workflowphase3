﻿var ViewModel = function () {
    var self = this;
    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);
    // New Data flag
    self.IsNewData = ko.observable(false);
    self.IsApprovalPage = ko.observable(false);
    //Parameter
    self.SingleValue = ko.observable("");
    self.SingleValueApproval = ko.observable("");

    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");

    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    var SingleValueParameterData = {
        ID: 0,
        Name: "SMS",
        Value: self.SingleValue,
        LastModifiedDate: new Date(),
        LastModifiedBy: "Unknown"
    }

    //Body
    self.SingleValueParameter = ko.observable(new SingleValueParameter(SingleValueParameterData));

    //Get Value
    self.GetData = function () { GetData(); }

    function GetData() {
        var options = {
            url: api.server + api.url.sms,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGet, OnError, OnAlways);
    }
    function OnSuccessGet(data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            if ($('#editor1').length) {
                CKEDITOR.instances['editor1'].setData(data);
            }


        } else {
            // bind result to observable array
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }
    self.GetSelectedRow = function (data) {
        self.IsNewData(false);
        //console.log(data.Value);
        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');

        self.SingleValueApproval(data.Value);
        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);
        if ($('#editorapproval1').length)
            $('#editorapproval1')[0].innerHTML = data.Value;
    };

    //
    self.UpdateValue = function () { UpdateValue(); }

    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker VALIDATION
                    console.log("spUser Roles : " + ko.toJSON(spUser.Roles));
                    console.log("spUser Permited : " + data);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function UpdateValue() {
        // validation
        var form = $("#aspnetForm");
        form.validate();
        //console.log(ko.toJSON(self.SingleValueParameter()));

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    self.SingleValue(CKEDITOR.instances['editor1'].getData());
                    var options = {
                        url: api.server + api.url.singlevalueparameter + "/" + "SMS",
                        data: ko.toJSON(self.SingleValueParameter()),
                        params: {
                        },
                        token: accessToken
                    };

                    Helper.Ajax.Put(options, OnSuccessUpdate, OnError, OnAlways);
                    GetData();
                }
            });
        }
    }

    function OnSuccessUpdate(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-success', false);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });
    // text editor (Body Email from Exception Handling)
    function showErrorAlert(reason, detail) {
        var msg = '';
        if (reason === 'unsupported-file-type') { msg = "Unsupported format " + detail; }
        else {
            console.log("error uploading file", reason, detail);
        }
        $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
    }
    $('[data-toggle="buttons"] .btn').on('click', function (e) {
        var target = $(this).find('input[type=radio]');
        var which = parseInt(target.val());
        var toolbar = $('#editor1').prev().get(0);
        if (which == 1 || which == 2 || which == 3) {
            toolbar.className = toolbar.className.replace(/wysiwyg\-style(1|2)/g, '');
            if (which == 1) $(toolbar).addClass('wysiwyg-style1');
            else if (which == 2) $(toolbar).addClass('wysiwyg-style2');
        }
    });


    if (typeof jQuery.ui !== 'undefined' && /applewebkit/.test(navigator.userAgent.toLowerCase())) {

        var lastResizableImg = null;
        function destroyResizable() {
            if (lastResizableImg == null) return;
            lastResizableImg.resizable("destroy");
            lastResizableImg.removeData('resizable');
            lastResizableImg = null;
        }

        var enableImageResize = function () {
            $('.wysiwyg-editor')
                .on('mousedown', function (e) {
                    var target = $(e.target);
                    if (e.target instanceof HTMLImageElement) {
                        if (!target.data('resizable')) {
                            target.resizable({
                                aspectRatio: e.target.width / e.target.height,
                            });
                            target.data('resizable', true);

                            if (lastResizableImg != null) {//disable previous resizable image
                                lastResizableImg.resizable("destroy");
                                lastResizableImg.removeData('resizable');
                            }
                            lastResizableImg = target;
                        }
                    }
                })
                .on('click', function (e) {
                    if (lastResizableImg != null && !(e.target instanceof HTMLImageElement)) {
                        destroyResizable();
                    }
                })
                .on('keydown', function () {
                    destroyResizable();
                });
        }
        enableImageResize();
    }
};
var accessToken;

$.holdReady(true);
var intervalRoleName = setInterval(function () {
    if (Object.keys(Const_RoleName).length != 0) {
        $.holdReady(false);
        clearInterval(intervalRoleName);
    }
}, 100);

//$(document).on("RoleNameReady", function() {
$(document).ready(function () {

    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(TokenOnSuccess, TokenOnError);
    } else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // call spuser function
        GetCurrentUser();


    }
    // Get SPUser from cookies
    function GetCurrentUser(vm) {
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);


        }
    }


    // Token validation On Success function
    function TokenOnSuccess(data, textStatus, jqXHR) {
        // store token on browser cookies
        $.cookie(api.cookie.name, data.AccessToken);
        accessToken = $.cookie(api.cookie.name);

        // call spuser function
        GetCurrentUser();

    }

    // Token validation On Error function
    function TokenOnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }






});