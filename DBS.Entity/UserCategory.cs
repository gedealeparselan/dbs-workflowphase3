//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserCategory
    {
        public int UserCategoryID { get; set; }
        public string UserCategoryCode { get; set; }
        public int ProductID { get; set; }
        public int CurrencyID { get; set; }
        public decimal MinTransaction { get; set; }
        public decimal MaxTransaction { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public string UserGroup { get; set; }
        public Nullable<bool> Unlimited { get; set; }
    
        public virtual Currency Currency { get; set; }
        public virtual Product Product { get; set; }
    }
}
