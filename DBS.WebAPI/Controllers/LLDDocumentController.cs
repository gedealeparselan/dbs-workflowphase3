﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    public class LLDDocumentController : ApiController
    {
        private string message = string.Empty;

        private readonly LDDDocumentRepository repository = null;

         public LLDDocumentController()
        {
            this.repository = new LDDDocumentRepository();
        }

         public LLDDocumentController(LDDDocumentRepository repository)
        {
            this.repository = repository;
        }

        //Data Grid
         [ResponseType(typeof(LLDDocumentGrid))]
         [AcceptVerbs("GET", "POST")]
         [Route("api/LLDDocument")]
         public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<LLDDocumentFilter> filters)
         {
             LLDDocumentGrid output = new LLDDocumentGrid();

             if (!repository.GetLLDDocument(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
             {
                 return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
             }
             else
             {
                 return Request.CreateResponse<LLDDocumentGrid>(HttpStatusCode.OK, output);
             }
         }

        //Add Data LLD Document
         [HttpPost]
         [ResponseType(typeof(string))]
         [Authorize(Roles = "DBS Admin")]
         //[Route("api/City")]
        [Route("api/LLDDocument")]
         public HttpResponseMessage Post(LLDDocumentModel LLDDocument)
         {
             if (!ModelState.IsValid)
             {
                 return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
             }
             else
             {
                 if (!repository.AddLLDDocument(LLDDocument, ref message))
                 {
                     return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                 }
                 else
                 {
                     return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New LLD Document data has been created." });
                 }
             }
         }

         /// <summary>
         /// Modify existing LLDDocument.
       
         [HttpPut]
         [ResponseType(typeof(string))]
         [Route("api/LLDDocument/{id}")]
         [Authorize(Roles = "DBS Admin")]
         public HttpResponseMessage Put(int id, LLDDocumentModel LLDDoc)
         {
             if (!ModelState.IsValid)
             {
                 return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
             }
             else
             {
                 if (!repository.UpdateLLDDocument(id, LLDDoc, ref message))
                 {
                     return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                 }
                 else
                 {
                     return Request.CreateResponse(HttpStatusCode.OK, new { Message = "LLDDocument data has been updated." });
                 }
             }
         }

         /// <summary>
         /// Remove exisiting LLD Document.
         /// </summary>
         /// <param name="id">ID of LLDDocument to be deleted.</param>
         /// <returns></returns>
         [HttpDelete]
         [Route("api/LLDDocument/{id}")]
         [ResponseType(typeof(string))]
         [Authorize(Roles = "DBS Admin")]
         public HttpResponseMessage Delete(int id)
         {
             if (!ModelState.IsValid)
             {
                 return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
             }
             else
             {
                 if (!repository.DeleteLLDDocument(id, ref message))
                 {
                     return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                 }
                 else
                 {
                     return Request.CreateResponse(HttpStatusCode.OK, new { Message = "LLDDocument data has been deleted." });
                 }
             }
         }

         protected override void Dispose(bool disposing)
         {
             if (repository != null)
                 repository.Dispose();
             base.Dispose(disposing);
         }

    }
}
