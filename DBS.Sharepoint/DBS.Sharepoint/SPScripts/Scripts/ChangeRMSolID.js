﻿var $box;
var $remove = false;
var accessToken;
var ActivityHistoryID = [];
var ArrCIFUploaded = [];

var UploadTypeCRM = {
    ID: 1,
    Name: "CIF_UPLOAD_CRM"
};

var ConstProduct = {
    Product: "Retail CIF"
};

var ConstRequestType = {
    Maintenance: "Maintenance"
};

var ConstMaintenanceType = {
    ChangeRMSegmentSOLID: "Change RM/Segment/SOL ID"
};

var DecodeLogin = function (username) {
    var index = username.lastIndexOf("|");
    var str = username.substring(index + 1);
    return str;
};

var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};

var ParameterSystemCIFModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};

var BizSegmentModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var BranchModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var TypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ProductModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    WorkflowID: ko.observable(),
    Workflow: ko.observable()
};

var RMModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    RankID: ko.observable(),
    Rank: ko.observable(),
    SegmentID: ko.observable(),
    Segment: ko.observable(),
    BranchID: ko.observable(),
    Branch: ko.observable(),
    Email: ko.observable()
};

var ContactModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    PhoneNumber: ko.observable(),
    DateOfBirth: ko.observable(),
    Address: ko.observable(),
    IDNumber: ko.observable()
};

var FunctionModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var AccountModel = {
    AccountNumber: ko.observable(),
    CustomerName: ko.observable(),
    Currency: ko.observable(CurrencyModel),
    IsJointAccount: ko.observable()
};

var CurrencyModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var MaintenanceTypeModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var TransactionTypeModel = {
    TransTypeID: ko.observable(),
    TransactionTypeName: ko.observable(),
    ProductID: ko.observable(),
    ProductName: ko.observable()
};

var TransactionSubTypeModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var DocumentTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var DocumentPurposeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var DocumentModel = {
    ID: ko.observable(),
    Type: ko.observable(DocumentTypeModel),
    Purpose: ko.observable(DocumentPurposeModel),
    FileName: ko.observable(),
    DocumentPath: ko.observable(),
    LastModifiedDate: ko.observable(new Date),
    LastModifiedBy: ko.observable()

};

var CustomerModel = {
    CIF: ko.observable(),
    Name: ko.observable(),
    POAName: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Branch: ko.observable(BranchModel),
    Type: ko.observable(TypeModel),
    RM: ko.observable(RMModel),
    Contacts: ko.observableArray([ContactModel]),
    Functions: ko.observableArray([FunctionModel]),
    Accounts: ko.observableArray([AccountModel])
};

var ChangeRMModel = {
    ChangeRMTransactionID: ko.observable(),
    TransactionID: ko.observable(),
    No: ko.observable(),
    CIF: ko.observable(),
    CustomerName: ko.observable(),
    ChangeSegmentFrom: ko.observable(),
    ChangeSegmentTo: ko.observable(),
    ChangeFromBranchCode: ko.observable(),
    ChangeFromRMCode: ko.observable(),
    ChangeFromBU: ko.observable(),
    ChangeToBranchCode: ko.observable(),
    ChangeToRMCode: ko.observable(),
    ChangeToBU: ko.observable(),
    Account: ko.observable(),
    PCCode: ko.observable(),
    EATPB: ko.observable(),
    Remarks: ko.observable(),
    ActivityHistoryID: ko.observable(),
    Account: ko.observable(),
    CreateDate: ko.observable(),
    CreateBy: ko.observable(),
    Selected: ko.observable(),
    UpdateDate: ko.observable(),
    UpdateBy: ko.observable()
};

var SelectedModel = {
    Product: ko.observable(),
    MaintenanceType: ko.observable(),
    RequestType: ko.observable(),
    DocumentType: ko.observable(),
    DocumentPurpose: ko.observable()
};

var CheckboxGroupModel = {
    IsChangeRM: ko.observable(false),
    IsSegment: ko.observable(false),
    IsSOLID: ko.observable(false)
};

var ParameterModel = {
    ID: ko.observable(),
    CatagoryName: ko.observable(),
    Name: ko.observable(),
    Itemvalue: ko.observable()
};

var TransactionMakerCBOModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    Currency: ko.observable(CurrencyModel),
    AccountNumber: ko.observable(),
    TransactionType: ko.observable(TransactionTypeModel),
    MaintenanceType: ko.observable(MaintenanceTypeModel),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]),
    ChangeRM: ko.observableArray([ChangeRMModel]),
    BranchName: ko.observable(),
    IsChangeRM: ko.observable(false),
    IsSegment: ko.observable(false),
    IsSOLID: ko.observable(false),
    IsDraft: ko.observable(false),
    IsBringupTask: ko.observable(false)
};

var ViewModel = function () {
    var self = this;

    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);
        if (date == '1970/01/01 00:00:00' || date == null) {
            return "";
        }

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    self.SPUser = ko.observable();
    self.IsTimelines = ko.observable(false);

    self.ddlProducts = ko.observableArray([]);
    self.ddlRequestTypes = ko.observableArray([]);
    self.ddlMaintenanceTypes = ko.observableArray([]);
    self.ddlDocumentTypes = ko.observableArray([]);

    self.ProductID = ko.observable();

    self.IsHO = ko.observable();
    self.IsJakartaBranch = ko.observable();
    self.IsUpcountryBranch = ko.observable();
    self.BranchName = ko.observable();
    self.IsDraftForm = ko.observable();
    self.IsLoadDraft = ko.observable(false);

    self.Selected = ko.observable(SelectedModel);
    self.CheckboxGroup = ko.observable(CheckboxGroupModel);

    self.IsBringUp = ko.observable(false);
    self.CutOffTime = ko.observable();
    self.IsCutOff = ko.observable(false);
    self.strCutOffTime = ko.observable('');
    self.NotifHeader = ko.observable();
    self.NotifTitle = ko.observable();
    self.NotifMessage = ko.observable();
    self.NotifOK = function () {
        if (viewModel.IsCutOff() == true) {
            window.location = "/home/bring-up";
        } else {
            window.location = "/home";
        }
    }

    self.IsEditable = ko.observable(false);
    self.IsSubmitted = ko.observable(false);
    self.ApplicationIDColl = ko.observableArray([]);
    self.RetIDColl = ko.observableArray([]);

    self.ID = ko.observable();
    self.IsCustomerValid = ko.observable(false);
    self.ArrCIFUploaded = ko.observableArray([]);

    self.MakerDocuments = ko.observableArray([]);
    self.Documents = ko.observableArray([]);
    self.DocumentPath = ko.observable();
    self.DocumentType = ko.observable();
    self.DocumentPurpose = ko.observable();
    self.DocumentFileName = ko.observable();

    self.resultMessage = ko.observable("");
    self.resultHeader = ko.observable("");
    self.RowFailed = ko.observableArray([]);
    self.IsUploading = ko.observable(false);
    self.IsUploadAttachment = ko.observable(false);
    self.IsUploadExcel = ko.observable(false);
    self.Column1Name = ko.observable("");
    self.Column2Name = ko.observable("");
    self.CloseProgres = function () {
        $("#modal-form-upload-excel").modal('hide');
    }

    self.TransactionMakerCBO = ko.observable(TransactionMakerCBOModel);
    self.UploadDocument = function () {
        self.DocumentPath(null);
        self.Selected().DocumentType(null);
        self.DocumentType(null);
        $("#modal-form-upload").modal('show');
        $('#transaction-result').hide()
        $('#upload-form').show();
        viewModel.IsUploadAttachment(true);
        viewModel.IsUploadExcel(false);
        $('.remove').click();
    };

    self.AddDocument = function () {
        var purposeDoc = {
            ID: 1,
            Name: "Instruction",
            Description: "Instruction"
        };

        var doc = {
            ID: 0,
            Type: self.DocumentType(),
            Purpose: purposeDoc,
            IsNewDocument: true,
            FileName: self.DocumentFileName(),
            DocumentPath: self.DocumentPath(),
            LastModifiedDate: new Date(),
            LastModifiedBy: null
        };

        if (doc.Type == null || doc.DocumentPath == null) {
            alert("Please complete the upload form fields.")
        } else {
            self.Documents.push(doc);
            self.MakerDocuments.push(doc);
            $('.remove').click();
            $('#modal-form-upload').modal('hide');
        }

    };

    self.RemoveDocument = function (data) {
        self.Documents.remove(data);
        self.MakerDocuments([]);
        ko.utils.arrayForEach(self.Documents(), function (item) {
            if (item.IsNewDocument == true) {
                self.MakerDocuments.push(item);
            }
        });

    };
    self.UploadExcel = function () {
        $("#modal-form-upload").modal('show');
        $("#upload-form").show();
        $("#transaction-progress").hide();
        $("#transaction-result").hide();
        $("#transaction-grid").hide();
        $('.remove').click();
        viewModel.IsUploading(false);
        viewModel.IsUploadExcel(true);
        viewModel.IsUploadAttachment(false);
    };
    self.UploadProcessExcel = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        var UploadFileModel = {
            Path: $('input[type=file]')[0].files[0] != null ? $('input[type=file]')[0].files[0].name : "",
            Type: $('input[type=file]')[0].files[0] != null ? $('input[type=file]')[0].files[0].type : ""
        }
        var UploadFilePath = $('input[type=file]')[0].files[0];

        if (UploadFilePath != null) {
            self.resultHeader('');
            self.resultMessage('');
            self.RowFailed([]);
            viewModel.IsUploading(true);
            $("#upload-form").hide();
            $("#transaction-progress").show();
            UploadFile(UploadFileModel, UploadedProcess);
        } else {
            ShowNotification("Upload Process Rejected", "Please select file to uploaded", "gritter-warning", true);
        }
    }

    //save Transaction and Draf
    self.SaveDraftChangeRM = function () {
        self.TransactionMakerCBO().IsDraft(true);
        self.TransactionMakerCBO().IsChangeRM(self.CheckboxGroup().IsChangeRM);
        self.TransactionMakerCBO().IsSegment(self.CheckboxGroup().IsSegment);
        self.TransactionMakerCBO().IsSOLID(self.CheckboxGroup().IsSOLID);
        var IsSelectedChangeRM = [];
        var CheckedCounter = [];
        $("input.htCheckboxRendererInput:checked").each(function () {
            IsSelectedChangeRM.push($(this).val());
        });
        CheckedCounter = IsSelectedChangeRM.join(',') + ',';
        if (CheckedCounter.length > 1) {
            Save();
        } else {
            ShowNotification("Attention", "Please select customers on grid table", "gritter-warning", true);
        }

    };

    self.SaveChangeRM = function () {
        if (self.CheckboxGroup().IsChangeRM() == false && self.CheckboxGroup().IsSegment() == false && self.CheckboxGroup().IsSOLID() == false) {
            ShowNotification("Attention", "Please select Change RM or Segment or Sol ID to Submit Transaction", "gritter-warning", true);
            return;
        }
        self.TransactionMakerCBO().IsDraft(false);
        self.TransactionMakerCBO().IsChangeRM(self.CheckboxGroup().IsChangeRM);
        self.TransactionMakerCBO().IsSegment(self.CheckboxGroup().IsSegment);
        self.TransactionMakerCBO().IsSOLID(self.CheckboxGroup().IsSOLID);
        var IsSelectedChangeRM = [];
        var CheckedCounter = [];
        $("input.htCheckboxRendererInput:checked").each(function () {
            IsSelectedChangeRM.push($(this).val());
        });
        CheckedCounter = IsSelectedChangeRM.join(',') + ',';
        if (CheckedCounter.length > 1) {
            Save();
        } else {
            ShowNotification("Attention", "Please select customers on grid table", "gritter-warning", true);
        }

    };

    self.RedirectHome = function () {
        window.location = "/home";
    };

    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });

    function Save() {
        var form = $("#aspnetForm");
        form.validate();
        if (form.valid()) {
            var data = {
                ApplicationID: viewModel.TransactionMakerCBO().ApplicationID(),
                CIF: null,
                Name: null
            };
            if (IsvalidChangeRM() == true) {
                self.TransactionMakerCBO().ID(0);
                if (ko.toJS(viewModel.IsBringUp()) == false) {
                    //for (var i = 0; i < viewModel.Documents().length ; i++) {

                    if (viewModel.Documents().length > 0) {
                        UploadFileRecuresive(data, viewModel.Documents(), SaveTransaction, viewModel.Documents().length);
                    }
                    //}
                } else {

                    if (viewModel.MakerDocuments().length > 0) {
                        UploadFileRecuresive(data, viewModel.MakerDocuments(), SaveTransaction, viewModel.MakerDocuments().length);
                    } else {
                        SaveTransaction();

                    }

                }
            }


        }

    }

    function UploadFileRecuresive(context, document, callBack, numFile) {
        var indexDocument = document.length - numFile;
        var IsDraft;

        IsDraft = viewModel.TransactionMakerCBO().IsDraft();
        var serverRelativeUrlToFolder = '';
        if (IsDraft == true) {
            serverRelativeUrlToFolder = '/DraftDocument'
        } else {
            serverRelativeUrlToFolder = '/Instruction Documents'
        }

        var parts = document[indexDocument].DocumentPath.name.split('.');
        var fileExtension = parts[parts.length - 1];
        var timeStamp = new Date().getTime();
        var fileName = timeStamp + "." + fileExtension;

        var serverUrl = _spPageContextInfo.webAbsoluteUrl;
        var output;

        // Initiate method calls using jQuery promises.
        // Get the local file as an array buffer.
        var getFile = getFileBuffer();
        getFile.done(function (arrayBuffer) {

            // Add the file to the SharePoint folder.
            var addFile = addFileToFolder(arrayBuffer);
            addFile.done(function (file, status, xhr) {
                output = file.d;

                // Get the list item that corresponds to the uploaded file.
                var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
                getItem.done(function (listItem, status, xhr) {

                    // Change the display name and title of the list item.
                    var changeItem = updateListItem(listItem.d.__metadata);
                    changeItem.done(function (data, status, xhr) {
                        //alert('file uploaded and updated');
                        //return output;
                        var newDoc = {
                            ID: 0,
                            Type: document[indexDocument].Type,
                            Purpose: document[indexDocument].Purpose,
                            LastModifiedDate: null,
                            LastModifiedBy: null,
                            DocumentPath: output.ServerRelativeUrl,
                            FileName: document[indexDocument].DocumentPath.name
                        };

                        viewModel.TransactionMakerCBO().Documents.push(newDoc);

                        if (numFile > 1) {
                            UploadFileRecuresive(context, document, callBack, numFile - 1); // recursive function
                        }
                        callBack();
                    });
                    changeItem.fail(OnError);
                });
                getItem.fail(OnError);
            });
            addFile.fail(OnError);
        });
        getFile.fail(OnError);
        // Get the local file as an array buffer.
        function getFileBuffer() {
            var deferred = $.Deferred();
            var reader = new FileReader();
            reader.onloadend = function (e) {
                deferred.resolve(e.target.result);
            }
            reader.onerror = function (e) {
                deferred.reject(e.target.error);
            }

            if (document[indexDocument].DocumentPath.type == Util.spitemdoc) {
                var fileURI = serverUrl + document[indexDocument].DocumentPath.DocPath;
                // load file:
                fetch(fileURI, convert, alert);

                function convert(buffer) {
                    var blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });

                    var domURL = self.URL || self.webkitURL || self,
                      url = domURL.createObjectURL(blob),
                      img = new Image;

                    img.onload = function () {
                        domURL.revokeObjectURL(url); // clean up
                        //document.body.appendChild(this);
                        // this = image
                    };
                    img.src = url;
                    reader.readAsArrayBuffer(blob);
                }

                function fetch(url, callback, error) {

                    var xhr = new XMLHttpRequest();
                    try {
                        xhr.open("GET", url);
                        xhr.responseType = "arraybuffer";
                        xhr.onerror = function () {
                            error("Network error")
                        };
                        xhr.onload = function () {
                            if (xhr.status === 200) callback(xhr.response);
                            else error(xhr.statusText);
                        };
                        xhr.send();
                    } catch (err) {
                        error(err.message)
                    }
                }


            } else {
                reader.readAsArrayBuffer(document[indexDocument].DocumentPath);
            }
            //bangkit

            //reader.readAsDataURL(document.DocumentPath);
            return deferred.promise();
        }

        // Add the file to the file collection in the Shared Documents folder.
        function addFileToFolder(arrayBuffer) {

            // Construct the endpoint.
            var fileCollectionEndpoint = String.format(
                    "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                    "/add(overwrite=false, url='{2}')",
                serverUrl, serverRelativeUrlToFolder, fileName);

            // Send the request and return the response.
            // This call returns the SharePoint file.
            return $.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val()
                    //"content-length": arrayBuffer.byteLength
                }
            });
        }

        // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
        function getListItem(fileListItemUri) {

            // Send the request and return the response.
            return $.ajax({
                url: fileListItemUri,
                type: "GET",
                headers: { "accept": "application/json;odata=verbose" }
            });
        }

        // Change the display name and title of the list item.
        function updateListItem(itemMetadata) {
            var body = {
                Title: document[indexDocument].DocumentPath.name,
                Application_x0020_ID: context.ApplicationID,
                CIF: context.CIF,
                Customer_x0020_Name: context.Name,
                Document_x0020_Type: document[indexDocument].Type.Name,
                Document_x0020_Purpose: document[indexDocument].Purpose.Name,
                __metadata: {
                    type: itemMetadata.type,
                    FileLeafRef: fileName,
                    Title: fileName
                }
            };

            // Send the request and return the promise.
            // This call does not return response content from the server.
            return $.ajax({
                url: itemMetadata.uri,
                type: "POST",
                data: ko.toJSON(body),
                headers: {
                    "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                    "content-type": "application/json;odata=verbose",
                    //"content-length": body.length,
                    "IF-MATCH": itemMetadata.etag,
                    "X-HTTP-Method": "MERGE"
                }
            });
        }


    }
    function GetCIFFromUploadedData() {
        var endPointURL = api.server + api.url.helper + "/CheckChangeRMCustomerUploaded";
        if (ko.toJSON(self.ArrCIFUploaded) != null || ko.toJSON(self.ArrCIFUploaded) != undefined) {
            //console.log(ko.toJS(self.ArrCIFUploaded));
            $.ajax({
                url: endPointURL,
                async: false,
                type: "POST",
                data: ko.toJSON(self.ArrCIFUploaded),
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status == 200 || jqXHR.status == 201) {
                        //console.log(data);
                        OnSuccessGetCIFFromUploadedData(data, textStatus, jqXHR);
                    }
                },
                error: function (jqXHR, jqXHR, errorThrown) {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }

    }

    function OnSuccessGetCIFFromUploadedData(data, textStatus, jqXHR) {
        //console.log(data);
        if (data != null || data != "" || data != undefined) {
            self.IsCustomerValid(true);
            self.TransactionMakerCBO().Customer().CIF(data);
        }
    }

    function SaveTransaction() {
        var cutOFFParameter = '';
        cutOFFParameter = ConsPARSYS.cifCuttof;

        var options = {
            url: api.server + api.url.parametersystem + "/IsCutOff/" + cutOFFParameter,
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessCheckCutOff, OnError, OnAlways);


    }
    function OnSuccessCheckCutOff(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            if (data != null) {
                viewModel.IsCutOff(data.IsCutOff);
                viewModel.strCutOffTime(data.CuttofValue);
                if (viewModel.IsCutOff() == true) {
                    self.TransactionMakerCBO().IsBringupTask(true);
                    self.IsEditable(false);
                    SaveTransactionChangeRM();
                    viewModel.IsCutOff(true);
                }
                else {
                    self.TransactionMakerCBO().IsDraft(false);
                    self.TransactionMakerCBO().IsBringupTask(false);
                    SaveTransactionChangeRM();
                    viewModel.IsCutOff(false);
                }

            }
            else
                ShowNotification('Cut Off Empty!!!', jqXHR.responseText, 'gritter-error', true);

        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }
    function CheckEmailAppCIF() {
        var docs = viewModel.Documents();
        var EmailApp = config.validate.approval.emailapp;
        var isAttachedEmail = false;
        for (var i = 0; i < docs.length; i++) {
            if (EmailApp.indexOf(docs[i].Type["Name"]) > -1) {
                isAttachedEmail = true;
            }
        }
        return isAttachedEmail;
    }

    function SaveTransactionChangeRM() {
        var options = {
            url: api.server + api.url.transactioncif + "/ChangeRM",
            token: accessToken,
            data: ko.toJSON(viewModel.TransactionMakerCBO()),

        };
        if (ko.toJS(self.IsBringUp()) == true) {
            if (viewModel.IsCutOff() == true) {
                if (viewModel.TransactionMakerCBO().ID() != null) {
                    if (viewModel.TransactionMakerCBO().Documents().length == viewModel.Documents().length) {
                        var isAttachedEmail = CheckEmailAppCIF();
                        if (isAttachedEmail == true) {
                            viewModel.IsSubmitted(true);
                            viewModel.TransactionMakerCBO().IsDraft(false);
                            options.data = ko.toJSON(viewModel.TransactionMakerCBO());
                            Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
                            return;
                        } else {
                            ShowNotification("Attention", "You need to attach email approval to continue cut off transaction.", 'gritter-warning', true);
                            return;

                        }
                    }
                    return;
                }

            }

            if (viewModel.TransactionMakerCBO().Documents().length == viewModel.Documents().length) {
                Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
            }

        } else {
            if (ko.toJS(self.IsCustomerValid()) == true) {
                self.IsSubmitted(true);
                if (viewModel.IsCutOff() == true) {
                    if (viewModel.TransactionMakerCBO().ID() != null) {
                        if (viewModel.TransactionMakerCBO().Documents().length == viewModel.Documents().length) {
                            viewModel.TransactionMakerCBO().IsDraft(true);
                            options.data = ko.toJSON(viewModel.TransactionMakerCBO())
                            Helper.Ajax.Post(options, OnSuccessSaveDraft, OnError, OnAlways);
                        }
                    }
                    return;
                }

                if (viewModel.TransactionMakerCBO().Documents().length == viewModel.Documents().length) {
                    Helper.Ajax.Post(options, OnSuccessSaveAPI, OnError, OnAlways);
                }

            } else {
                ShowNotification("Attention", "There is no match CIF between uploaded data and customer lists", "gritter-warning", true);
            }
        }
    }
    function OnSuccessSaveDraft(data, textStatus, jqXHR) {
        if (viewModel.IsCutOff() == true) {
            viewModel.NotifHeader("Cut Off " + viewModel.strCutOffTime());
            viewModel.NotifTitle("Attention");
            viewModel.NotifMessage("Transaction has exceeded cut off time, you need to attach email approval to continue cut off transaction. Please open Bring up file menu.");
            $("#modal-form-Notif").modal('show');
        }
        else {
            ShowNotification("Transaction Draft Success", "Transaction draft save attachments and underlying", "gritter-success", true);
            window.location = "/home/draft-transactions";
        }

    }
    function OnSuccessSaveAPI(data, textStatus, jqXHR) {
        var AppID = {};
        var DraftID;
        if (data.ID != null || data.ID != undefined) {
            viewModel.RetIDColl([]);
            AppID = {
                TransactionID: data.ID,
                ApplicationID: data.AppID
            };
            viewModel.RetIDColl.push(AppID);
            viewModel.TransactionMakerCBO().ApplicationID(data.AppID);


            if (viewModel.IsBringUp() == true) {
                DraftID = viewModel.ID();
                DeleteDraft(DraftID, data.ID);
            } else {
                viewModel.TransactionMakerCBO().ID(data.ID);
                DraftID = viewModel.TransactionMakerCBO().ID();
                AddListItem();
            }

        }
    }


    function DeleteDraft(id, transactionID) {
        //Ajax call to delete the Customer
        $.ajax({
            type: "DELETE",
            url: api.server + api.url.transactioncif + "/ChangeRM/Draft/Delete/" + id,
            //data: ko.toJSON(Product),
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                // send notification
                if (jqXHR.status = 200) {
                    viewModel.TransactionMakerCBO().ID(transactionID);
                    AddListItem();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });


    };

    function AddListItem() {
        var body = {};
        var listBody = {
            urlTransaction: config.sharepoint.listIdRetailCIF,
            title: viewModel.TransactionMakerCBO().ApplicationID(),
            initiatorGroup: GetUserRole(viewModel.SPUser().Roles),
            transactionID: viewModel.TransactionMakerCBO().ID(),
            applicationID: viewModel.TransactionMakerCBO().ApplicationID(),
            type: config.sharepoint.metadata.listRetailCIF
        }
        body = {
            Title: listBody.title,
            Initiator_x0020_GroupId: listBody.initiatorGroup,
            Transaction_x0020_ID: listBody.transactionID,
            Application_x0020_ID: listBody.applicationID,
            __metadata: {
                type: listBody.type
            }
        };

        var options = {
            url: config.sharepoint.url.api + "/Lists(guid'" + listBody.urlTransaction + "')/Items",
            data: JSON.stringify(body),
            digest: jQuery("#__REQUESTDIGEST").val()
        };

        Helper.Sharepoint.List.Add(options, OnSuccessAddListWorkflow, OnError, OnAlways);
    }

    function OnSuccessAddListWorkflow(data, textStatus, jqXHR) {
        var AppIDColl = viewModel.RetIDColl();
        var Cust;
        if (AppIDColl != null) {
            Cust = viewModel.TransactionMakerCBO().Customer();
            viewModel.TransactionMakerCBO().ApplicationID(AppIDColl[0].ApplicationID);
            viewModel.TransactionMakerCBO().ID(AppIDColl[0].TransactionID);

            viewModel.ApplicationIDColl([]);
            var appColl = {
                TransactionID: AppIDColl[0].TransactionID,
                ApplicationID: AppIDColl[0].ApplicationID,
                Customer: Cust,
            }

            viewModel.ApplicationIDColl.push(appColl);
            ShowNotification("Submit Transaction Success", "", "gritter-success", true);
            $("#modal-form-applicationID").modal('show');

        }
        //window.location = "/home";

    }

    function GetUserRole(userData) {
        var sValue;
        if (userData != undefined && userData.length > 0) {
            $.each(userData, function (index, item) {
                if (Const_RoleName[item.ID].toLowerCase().startsWith("dbs ppu") && Const_RoleName[item.ID].toLowerCase().endsWith("maker")) {
                    sValue = item.ID;
                }
            });
        }
        if (sValue == undefined) {
            sValue = userData[0].ID;
        }
        return sValue;
    }

    function UploadFile(document, callback) {
        var serverRelativeUrlToFolder = '/UploadedDocuments';
        var parts = document.Path.split('.');
        var fileExtension = parts[parts.length - 1];
        var timeStamp = new Date().getTime();
        var fileName = timeStamp + "." + fileExtension;

        var serverUrl = _spPageContextInfo.webAbsoluteUrl;

        var output;

        var getFile = getFileBuffer();
        getFile.done(function (arrayBuffer) {

            var addFile = addFileToFolder(arrayBuffer);
            addFile.done(function (file, status, xhr) {
                output = file.d;

                var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
                getItem.done(function (listItem, status, xhr) {
                    var UploadFileModel = {
                        Path: output.ServerRelativeUrl,
                        Type: document.Type,
                        ID: listItem.d.ID,
                        UploadType: UploadTypeCRM.Name
                    }
                    callback(UploadFileModel);
                });
                getItem.fail(OnError);
            });
            addFile.fail(OnError);
        });
        getFile.fail(OnError);

        // Get the local file as an array buffer.
        function getFileBuffer() {
            var deferred = $.Deferred();
            var reader = new FileReader();
            reader.onloadend = function (e) {
                deferred.resolve(e.target.result);
            }
            reader.onerror = function (e) {
                deferred.reject(e.target.error);
            }
            reader.readAsArrayBuffer($('input[type=file]')[0].files[0]);
            return deferred.promise();
        }

        // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
        function getListItem(fileListItemUri) {

            // Send the request and return the response.
            return $.ajax({
                url: fileListItemUri,
                type: "GET",
                headers: { "accept": "application/json;odata=verbose" }
            });
        }

        function addFileToFolder(arrayBuffer) {

            var fileCollectionEndpoint = String.format(
                    "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                    "/add(overwrite=false, url='{2}')",
                serverUrl, serverRelativeUrlToFolder, fileName);
            return $.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val()
                }
            });
        }
    }

    function UploadedProcess(UploadFileModel) {
        var options = {
            url: "/_vti_bin/DBSUploadFile/UploadFileService.svc/UploadFile",
            data: ko.toJSON(UploadFileModel)
        };
        Helper.Sharepoint.Nintex.Post(options, OnSuccessReadFile, OnError, OnAlways);
    }

    function OnSuccessReadFile(data, textStatus, jqXHR) {

        if (data.Message == "Upload File Success.") {
            if (data.FailedRows == 0) {
                self.resultHeader(data.Message);
                self.resultMessage('Rows Affected:' + data.RowsAffected);
                $("#transaction-progress").hide();
                $("#transaction-result").show();

            }
            else {
                var IsNeedReturn = true;
                var ResultHeader = '';
                var ResultMsg = 'Rows Failed:' + data.FailedRows;
                var Col1Name = '';
                var Col2Name = '';
                switch (UploadTypeCRM) {
                    case "CIF_UPLOAD_CRM":
                        ResultHeader = "Failed to upload";
                        Col1Name = "CIF";
                        Col2Name = "Customer Name";
                        break;
                    default:
                        IsNeedReturn = false;
                        break;
                }

                if (IsNeedReturn) {
                    self.resultHeader(ResultHeader);
                    self.resultMessage(ResultMsg);
                    self.Column1Name(Col1Name);
                    self.Column2Name(Col2Name);
                    self.RowFailed(data.ReturnRows);
                    $("#transaction-progress").hide();
                    $("#transaction-result").show();
                    $("#transaction-grid").show();
                }
                else {
                    self.resultHeader(ResultHeader);
                    self.resultMessage(ResultMsg);
                    $("#transaction-progress").hide();
                    $("#transaction-result").show();
                    $("#transaction-grid").hide();
                }
            }
            ActivityHistoryID.push(data.ActivityHistoryID);

            if (ActivityHistoryID != null || ActivityHistoryID != undefined) {
                if (ko.toJS(self.IsBringUp) == true) {
                    GetDataChangeRMAccumulate(ActivityHistoryID, ko.toJS(viewModel.ID()));
                } else {
                    GetDataChangeRM(ActivityHistoryID);
                }
            }
        } else {
            self.resultHeader(data.Message);
            self.resultMessage('Rows Affected:' + 0);
            $("#transaction-progress").hide();
            $("#transaction-result").show();
            $("#transaction-grid").hide();
        }
    }
    function GetDataChangeRMAccumulate(activityHistoryID, transactionID) {
        var endPointURL = api.server + api.url.workflowcif.ChangeRMDetailsReupload(activityHistoryID, transactionID, true);

        if (activityHistoryID != null) {
            $.ajax({
                url: endPointURL,
                async: false,
                type: "GET",
                data: {},
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {

                    if (jqXHR.status == 200) {
                        self.ArrCIFUploaded([]);
                        ko.utils.arrayForEach(data, function (item) {
                            self.ArrCIFUploaded.push(item.CIF);
                        });
                        //console.log(data);
                        GetDataChangeRMDetails(data);
                        if (ko.toJS(viewModel.IsBringUp()) == false) {
                            GetCIFFromUploadedData();
                        }
                    }
                },
                error: function (jqXHR, jqXHR, errorThrown) {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        } else {
            GetDataChangeRMAccumulateInitialize(data);
        }
    }
    function GetDataChangeRMAccumulateInitialize(data) {
        var $container = $("#dataExcelChangeRM");
        $container.handsontable({
            data: data,
            minSpareCols: 1,
            //fixedColumnsLeft: 3,
            //rowHeaders: true,
            colHeaders: ["", "NO", "CIF", "CUSTOMER NAME", "CHANGE SEGMENT FROM", "CHANGE SEGMENT TO", "CHANGE FROM BRANCH CODE",
	            "CHANGE FROM RM CODE", "CHANGE FROM BU", "CHANGE TO BRANCH CODE", "CHANGE TO RM CODE", "CHANGE TO BU", "ACCOUNT",
	            "PC CODE", "FREE TEXT 5 (EA/TPB EA/TPB)", "REMARKS / REASON"],
            contextMenu: false,
            columns: [
	                    {
	                        data: 'Selected',
	                    },
	                    {
	                        data: 'No'
	                    },
	                    {
	                        data: 'CIF',
	                        editor: false
	                    },
	                    {
	                        data: 'CustomerName'
	                    },
	                    {
	                        data: 'ChangeSegmentFrom'
	                    },
	                    {
	                        data: 'ChangeSegmentTo'
	                    },
	                    {
	                        data: 'ChangeFromBranchCode'
	                    },
	                    {
	                        data: 'ChangeFromRMCode'
	                    },
	                    {
	                        data: 'ChangeFromBU'
	                    },
	                    {
	                        data: 'ChangeToBranchCode'
	                    },
	                    {
	                        data: 'ChangeToRMCode'
	                    },
	                    {
	                        data: 'ChangeToBU'
	                    },
	                    {
	                        data: 'Account'
	                    },
	                    {
	                        data: 'PCCode'
	                    },
	                    {
	                        data: 'EATPB'
	                    },
	                    {
	                        data: 'Remarks'
	                    }

            ]
        });
        $('div.ht_clone_top.handsontable').css('display', 'none');
        $('div.ht_master.handsontable>div.wtHolder').css('width', 1290 + 'px');

        var handsontable = $container.data('handsontable');
        //    Hide();    

    }

    function IsvalidChangeRM() {
        if (viewModel.Documents() == null || viewModel.Documents().length == 0 || viewModel.Documents() === undefined) {
            ShowNotification("Attention", "You need to upload attachment to continue transaction.", 'gritter-warning', true);
            return false;
        }
        return true;
    }

    function GetDataChangeRM(activityHistoryID) {
        var endPointURL = api.server + api.url.workflowcif.ChangeRMDetails
        if (activityHistoryID != null) {
            $.ajax({
                url: endPointURL,
                async: false,
                type: "POST",
                data: ko.toJSON(activityHistoryID),
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {

                    if (jqXHR.status == 200) {
                        self.ArrCIFUploaded([]);
                        ko.utils.arrayForEach(data, function (item) {
                            self.ArrCIFUploaded.push(item.CIF);
                        });
                        GetDataChangeRMDetails(data);
                        if (ko.toJS(viewModel.IsBringUp()) == false) {
                            GetCIFFromUploadedData();
                        }
                    }
                },
                error: function (jqXHR, jqXHR, errorThrown) {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        } else {
            GetDataChangeRMDetailsInitialize(data);
        }
    }

    function GetDataChangeRMDetails(data) {
        var $container = $("#dataTable");
        $container.handsontable({
            data: data,
            minSpareCols: 1,
            //fixedColumnsLeft: 3,
            //rowHeaders: true,
            colHeaders: ["", "NO", "CIF", "CUSTOMER NAME", "CHANGE SEGMENT FROM", "CHANGE SEGMENT TO", "CHANGE FROM BRANCH CODE",
                "CHANGE FROM RM CODE", "CHANGE FROM BU", "CHANGE TO BRANCH CODE", "CHANGE TO RM CODE", "CHANGE TO BU", "ACCOUNT",
                "PC CODE", "FREE TEXT 5 (EA/TPB EA/TPB)", "REMARKS / REASON"],
            contextMenu: false,
            columns: [
                        {
                            data: 'Selected',
                            type: 'checkbox'
                        },
                        {
                            data: 'No'
                        },
                        {
                            data: 'CIF',
                            editor: false
                        },
                        {
                            data: 'CustomerName'
                        },
                        {
                            data: 'ChangeSegmentFrom'
                        },
                        {
                            data: 'ChangeSegmentTo'
                        },
                        {
                            data: 'ChangeFromBranchCode'
                        },
                        {
                            data: 'ChangeFromRMCode'
                        },
                        {
                            data: 'ChangeFromBU'
                        },
                        {
                            data: 'ChangeToBranchCode'
                        },
                        {
                            data: 'ChangeToRMCode'
                        },
                        {
                            data: 'ChangeToBU'
                        },
                        {
                            data: 'Account'
                        },
                        {
                            data: 'PCCode'
                        },
                        {
                            data: 'EATPB'
                        },
                        {
                            data: 'Remarks'
                        }

            ]
        });
        $('div.ht_clone_top.handsontable').css('display', 'none');
        $('div.ht_master.handsontable>div.wtHolder').css('width', 1290 + 'px');
        $('div.ht_master.handsontable>div.wtHolder').css('min-height', 400 + 'px');
        $('div.ht_master.handsontable>div.wtHolder').css('max-height', 400 + 'px');
        var handsontable = $container.data('handsontable');
        self.TransactionMakerCBO().ChangeRM($container.data('handsontable').getData());
        Hide();

    }
    function Hide() {
        $('#dataTable td:nth-child(2),#dataTable th:nth-child(2)').hide();
    }
    function GetDataChangeRMDetailsInitialize(data) {
        var $container = $("#dataTable");
        $container.handsontable({
            data: data,
            minSpareCols: 1,
            //fixedColumnsLeft: 3,
            //rowHeaders: true,
            colHeaders: ["", "NO", "CIF", "CUSTOMER NAME", "CHANGE SEGMENT FROM", "CHANGE SEGMENT TO", "CHANGE FROM BRANCH CODE",
	            "CHANGE FROM RM CODE", "CHANGE FROM BU", "CHANGE TO BRANCH CODE", "CHANGE TO RM CODE", "CHANGE TO BU", "ACCOUNT",
	            "PC CODE", "FREE TEXT 5 (EA/TPB EA/TPB)", "REMARKS / REASON"],
            contextMenu: false,
            columns: [
	                    {
	                        data: 'Selected',
	                    },
	                    {
	                        data: 'No'
	                    },
	                    {
	                        data: 'CIF',
	                        editor: false
	                    },
	                    {
	                        data: 'CustomerName'
	                    },
	                    {
	                        data: 'ChangeSegmentFrom'
	                    },
	                    {
	                        data: 'ChangeSegmentTo'
	                    },
	                    {
	                        data: 'ChangeFromBranchCode'
	                    },
	                    {
	                        data: 'ChangeFromRMCode'
	                    },
	                    {
	                        data: 'ChangeFromBU'
	                    },
	                    {
	                        data: 'ChangeToBranchCode'
	                    },
	                    {
	                        data: 'ChangeToRMCode'
	                    },
	                    {
	                        data: 'ChangeToBU'
	                    },
	                    {
	                        data: 'Account'
	                    },
	                    {
	                        data: 'PCCode'
	                    },
	                    {
	                        data: 'EATPB'
	                    },
	                    {
	                        data: 'Remarks'
	                    }

            ]
        });
        $('div.ht_clone_top.handsontable').css('display', 'none');
        $('div.ht_master.handsontable>div.wtHolder').css('width', 1290 + 'px');

        var handsontable = $container.data('handsontable');
        Hide();
    }


};

var viewModel = new ViewModel();
$.holdReady(true);
var intervalRoleName = setInterval(function () {
    if (Object.keys(Const_RoleName).length != 0) {
        $.holdReady(false);
        clearInterval(intervalRoleName);
    }
}, 100);
//$(document).on("RoleNameReady", function() {
$(document).ready(function () {
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    $(".modal-dialog.progress").hide();

    // ace file upload
    $('#document-path').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });

    // Knockout custom file handler
    ko.bindingHandlers.file = {
        init: function (element, valueAccessor) {
            $(element).change(function () {
                var file = this.files[0];

                if (ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };

    ko.dependentObservable(function () {

        var product = ko.utils.arrayFirst(viewModel.ddlProducts(), function (item) {
            return item.ID == viewModel.Selected().Product();
        });

        var requestType = ko.utils.arrayFirst(viewModel.ddlRequestTypes(), function (item) {
            return item.ID == viewModel.Selected().RequestType();

        });

        var maintenanceType = ko.utils.arrayFirst(viewModel.ddlMaintenanceTypes(), function (item) {
            return item.ID == viewModel.Selected().MaintenanceType();
        });

        var docType = ko.utils.arrayFirst(viewModel.ddlDocumentTypes(), function (item) {
            return item.ID == viewModel.Selected().DocumentType();

        });

        if (product != null)
            viewModel.TransactionMakerCBO().Product(product);

        if (requestType != null)
            viewModel.TransactionMakerCBO().TransactionType(requestType);

        if (maintenanceType != null)
            viewModel.TransactionMakerCBO().MaintenanceType(maintenanceType);

        if (docType != null)
            viewModel.DocumentType(docType);


    });

    // Token Validation
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(OnSuccessToken, OnError);
    } else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);
            viewModel.SPUser(ko.mapping.toJS(spUser));
        }

        // call get data inside view model
        GetParameters();
        GetEmployeeLocation();
        GetDataChangeRMDetailsInitialize(null);
    }

    // Knockout Bindings
    ko.applyBindings(viewModel);




});
function GetDataChangeRMDetails(data) {
    var $container = $("#dataTable");
    $container.handsontable({
        data: data,
        minSpareCols: 1,
        //fixedColumnsLeft: 3,
        //rowHeaders: true,
        colHeaders: ["", "NO", "CIF", "CUSTOMER NAME", "CHANGE SEGMENT FROM", "CHANGE SEGMENT TO", "CHANGE FROM BRANCH CODE",
            "CHANGE FROM RM CODE", "CHANGE FROM BU", "CHANGE TO BRANCH CODE", "CHANGE TO RM CODE", "CHANGE TO BU", "ACCOUNT",
            "PC CODE", "FREE TEXT 5 (EA/TPB EA/TPB)", "REMARKS / REASON"],
        contextMenu: false,
        columns: [
                    {
                        data: 'Selected',
                        type: 'checkbox'
                    },
                    {
                        data: 'No'
                    },
                    {
                        data: 'CIF',
                        editor: false
                    },
                    {
                        data: 'CustomerName'
                    },
                    {
                        data: 'ChangeSegmentFrom'
                    },
                    {
                        data: 'ChangeSegmentTo'
                    },
                    {
                        data: 'ChangeFromBranchCode'
                    },
                    {
                        data: 'ChangeFromRMCode'
                    },
                    {
                        data: 'ChangeFromBU'
                    },
                    {
                        data: 'ChangeToBranchCode'
                    },
                    {
                        data: 'ChangeToRMCode'
                    },
                    {
                        data: 'ChangeToBU'
                    },
                    {
                        data: 'Account'
                    },
                    {
                        data: 'PCCode'
                    },
                    {
                        data: 'EATPB'
                    },
                    {
                        data: 'Remarks'
                    }

        ]
    });
    $('div.ht_clone_top.handsontable').css('display', 'none');
    $('div.ht_master.handsontable>div.wtHolder').css('width', 1290 + 'px');
    $('div.ht_master.handsontable>div.wtHolder').css('min-height', 400 + 'px');
    $('div.ht_master.handsontable>div.wtHolder').css('max-height', 400 + 'px');
    var handsontable = $container.data('handsontable');
    viewModel.TransactionMakerCBO().ChangeRM($container.data('handsontable').getData());
    //Hide();

}

function GetDataChangeRMDetailsInitialize(data) {
    var $container = $("#dataTable");
    $container.handsontable({
        data: data,
        minSpareCols: 1,
        //fixedColumnsLeft: 3,
        //rowHeaders: true,
        colHeaders: ["", "NO", "CIF", "CUSTOMER NAME", "CHANGE SEGMENT FROM", "CHANGE SEGMENT TO", "CHANGE FROM BRANCH CODE",
            "CHANGE FROM RM CODE", "CHANGE FROM BU", "CHANGE TO BRANCH CODE", "CHANGE TO RM CODE", "CHANGE TO BU", "ACCOUNT",
            "PC CODE", "FREE TEXT 5 (EA/TPB EA/TPB)", "REMARKS / REASON"],
        contextMenu: false,
        columns: [
                    {
                        data: 'Selected',
                    },
                    {
                        data: 'No'
                    },
                    {
                        data: 'CIF',
                        editor: false
                    },
                    {
                        data: 'CustomerName'
                    },
                    {
                        data: 'ChangeSegmentFrom'
                    },
                    {
                        data: 'ChangeSegmentTo'
                    },
                    {
                        data: 'ChangeFromBranchCode'
                    },
                    {
                        data: 'ChangeFromRMCode'
                    },
                    {
                        data: 'ChangeFromBU'
                    },
                    {
                        data: 'ChangeToBranchCode'
                    },
                    {
                        data: 'ChangeToRMCode'
                    },
                    {
                        data: 'ChangeToBU'
                    },
                    {
                        data: 'Account'
                    },
                    {
                        data: 'PCCode'
                    },
                    {
                        data: 'EATPB'
                    },
                    {
                        data: 'Remarks'
                    }

        ]
    });
    $('div.ht_clone_top.handsontable').css('display', 'none');
    $('div.ht_master.handsontable>div.wtHolder').css('width', 1290 + 'px');

    var handsontable = $container.data('handsontable');
    Hide();
}

function Hide() {
    $('#dataTable td:nth-child(2),#dataTable th:nth-child(2)').hide();
}


function GetEmployeeLocation() {
    var LogName = DecodeLogin(spUser.LoginName);
    $.ajax({
        type: "GET",
        url: api.server + api.url.userapproval + "/Loc/" + LogName.trim(),
        contentType: "application/json; charset=utf-8",
        data: ko.toJSON(BranchModel),
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + accessToken
        },
        success: function (data, textStatus, jqXHR) {
            if (jqXHR.status = 200) {
                if (data != null) {
                    viewModel.IsHO(data["IsHO"]);
                    viewModel.IsJakartaBranch(data["IsJakartaBranch"]);
                    viewModel.IsUpcountryBranch(data["IsUpcountryBranch"]);
                    viewModel.BranchName(data["Name"]);
                    viewModel.TransactionMakerCBO().BranchName(data["Name"]);
                }
                else {
                    viewModel.IsHO(true);
                    viewModel.IsJakartaBranch(false);
                    viewModel.IsUpcountryBranch(false);
                    viewModel.BranchName(null);
                    viewModel.TransactionMakerCBO().BranchName();
                }
            } else {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // send notification
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    });
}
// Token validation On Success function
function OnSuccessToken(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // read spuser from cookie
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);
        viewModel.SPUser(ko.mapping.toJS(spUser));
    }

    // get parameter values
    GetParameters();
    GetEmployeeLocation();
}

// get all parameters need
function GetParameters() {
    var options = {
        url: api.server + api.url.parameter,
        params: {
            select: "TransactionType,MaintenanceType,Product,DocType,PurposeDoc"
        },
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
}

function OnSuccessGetParameters(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        // bind result to observable array

        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };

        ko.utils.arrayFilter(data.Product, function (item) {
            if (item.Name == ConstProduct.Product) {
                viewModel.ddlProducts.push(item);
                viewModel.Selected().Product(item.ID);
            }
        });

        ko.utils.arrayFilter(data.TransactionType, function (item) {
            if (item.Name == ConstRequestType.Maintenance) {
                viewModel.ddlRequestTypes.push(item);
                viewModel.Selected().RequestType(item.ID);
            }
        });

        ko.utils.arrayFilter(data.MaintenanceType, function (item) {
            if (item.Name == ConstMaintenanceType.ChangeRMSegmentSOLID) {
                viewModel.ddlMaintenanceTypes.push(item);
                viewModel.Selected().MaintenanceType(item.ID);
            }
        });

        viewModel.ddlDocumentTypes(ko.mapping.toJS(data.DocType, mapping));

        var items = ko.utils.arrayFilter(data.PurposeDoc, function (item) {
            return item.ID != 2;
        });

        // enabling form input controls
        viewModel.IsEditable(true);
        LoadDraft();
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}
function LoadDraft() {
    var uri = '';
    ar = window.location.hash.split('#');
    if (ar.length < 2) {
        viewModel.IsDraftForm(false);
        return;
    }
    viewModel.IsDraftForm(true);
    uri = api.server + api.url.transactioncif + '/ChangeRM/Draft/' + ar[1];
    var options = {
        url: uri,
        token: accessToken
    };
    Helper.Ajax.Get(options, OnSuccessLoadDraft, OnError, OnAlways);


}
function OnSuccessLoadDraft(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        viewModel.IsLoadDraft(true);
        if (data == null) return;
        viewModel.IsBringUp(true);
        LoadDraftChangeRM(data);
        GetDataChangeRMDetails(data.ChangeRM);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
    }
}
function LoadDraftChangeRM(data) {
    var mapping = {

    };

    viewModel.TransactionMakerCBO().Customer(data.Customer);
    viewModel.ID(data.ID);
    viewModel.TransactionMakerCBO().AccountNumber(undefined);
    viewModel.TransactionMakerCBO().ApplicationID(undefined);
    viewModel.CheckboxGroup().IsChangeRM(data.IsChangeRM);
    viewModel.CheckboxGroup().IsSOLID(data.IsSOLID);
    viewModel.CheckboxGroup().IsSegment(data.IsSegment);
    viewModel.TransactionMakerCBO().BranchName(viewModel.BranchName());
    //console.log(data);
    var DocPath = '';
    ko.utils.arrayForEach(data.Documents, function (item) {
        DocPath = item.DocumentPath.DocPath;
        item.IsNewDocument = false;
        viewModel.Documents.push(item);
        item.DocumentPath = DocPath;
        viewModel.TransactionMakerCBO().Documents.push(item);
    });
    //viewModel.TransactionMakerCBO().Documents(data.Documents);

}
function GetAllParameterSystems() {
    var RequestTypes = {
        url: api.server + api.url.transactiontype + "/Product/" + viewModel.ProductID(),
        params: {
        },
        token: accessToken
    };

    Helper.Ajax.Get(RequestTypes, OnSuccessGetTransactionTypesCIF, OnError, OnAlways);


}
function OnSuccessGetTransactionTypesCIF(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        viewModel.ddlRequestTypes(ko.mapping.toJS(data.TransactionType));
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

}


// AJAX On Error Callback
function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

// AJAX On Alway Callback
function OnAlways() {
    // $box.trigger('reloaded.ace.widget');

    // enabling form input controls
    viewModel.IsEditable(true);
}