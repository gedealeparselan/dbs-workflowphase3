//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class MISReportingTableSummary
    {
        public long WorkflowApproverID { get; set; }
        public Nullable<System.DateTime> WorkflowTaskExitTime { get; set; }
        public Nullable<System.Guid> WorkflowInstanceID { get; set; }
        public Nullable<int> ProductID { get; set; }
        public Nullable<int> BizSegmentID { get; set; }
        public Nullable<int> LocationID { get; set; }
        public Nullable<int> ChannelID { get; set; }
        public string CIF { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<int> SLATime { get; set; }
        public Nullable<int> StateID { get; set; }
        public Nullable<long> TransactionID { get; set; }
        public Nullable<bool> IsExceptionHandling { get; set; }
        public Nullable<int> TATTime { get; set; }
    }
}
