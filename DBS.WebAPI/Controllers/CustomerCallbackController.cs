﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class CustomerCallbackController : ApiController
    {
        private string message = string.Empty;

        private readonly ICustomerCallbackRepository repository = null;

        public CustomerCallbackController()
        {
            this.repository = new CustomerCallbackRepository();
        }

        public CustomerCallbackController(ICustomerCallbackRepository repository)
        {
            this.repository = repository;
        }

        [ResponseType(typeof(CustomerCallbackGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<CustomerCallbackFilter> filters)
        {
            CustomerCallbackGrid output = new CustomerCallbackGrid();

            if (!repository.GetCustomerCallback(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "ContactName" : sort_column, string.IsNullOrEmpty(sort_order) ? "ASC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerCallbackGrid>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(CustomerCallbackGrid))]
        [Route("api/CustomerCallback/Product/")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetCustomerCallbackByProductID(int? page, int? size, string sort_column, string sort_order, IList<CustomerCallbackFilter> filters)
        {
            CustomerCallbackGrid output = new CustomerCallbackGrid();

            if (!repository.GetCustomerCallbackByProductID(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "ContactName" : sort_column, string.IsNullOrEmpty(sort_order) ? "ASC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerCallbackGrid>(HttpStatusCode.OK, output);
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/CustomerCallback/Utilize/")]
        public HttpResponseMessage UtilizeCustomerCallback(IList<CustomerCallbackModel> CustomerCallback)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.Utilize(CustomerCallback, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "Customer Callback data has been updated." });
                }
            }
        }
    }
}
