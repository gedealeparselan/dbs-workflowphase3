﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class FundingMemoLTController : ApiController
    {
        private string message = string.Empty;

        private readonly IFundingMemoLTRepository repository = null;
        private readonly ICustomerRepository repoCus = null;

        public FundingMemoLTController()
        {
            this.repository = new FundingMemoLTRepository();
            this.repoCus = new CustomerRepository();
        }
        public FundingMemoLTController(IFundingMemoLTRepository repository)
        {
            this.repository = repository;
        }


        //[ResponseType(typeof(FundingMemoLTModel))]
        //[Route("api/FundingMemoLT/{id}")]
        //[HttpGet]
        //public HttpResponseMessage Get(int id)
        //{
        //    FundingMemoLTModel output = new FundingMemoLTModel();

        //    if (!repository.GetFundingMemoTLByID(id, ref output, ref message))
        //    {
        //        return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
        //    }
        //    else
        //    {
        //        if (output == null)
        //            return Request.CreateResponse(HttpStatusCode.NoContent);
        //        else
        //            return Request.CreateResponse<FundingMemoLTModel>(HttpStatusCode.OK, output);
        //    }
        //}
        [ResponseType(typeof(IList<FundingMemoLTModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<FundingMemoLTModel> output = new List<FundingMemoLTModel>();

            if (!repository.GetFundingMemoTL(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<FundingMemoLTModel>>(HttpStatusCode.OK, output);
            }
        }
        [ResponseType(typeof(FundingMemoLTGrid))]
        //[AcceptVerbs("GET")] comment  azam
        [AcceptVerbs("GET", "POST")] //added azam
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<FundingMemoLTFilter> filters)
        {
            FundingMemoLTGrid output = new FundingMemoLTGrid();
            IList<EmployeeFundingMemoLTModel> Employee = new List<EmployeeFundingMemoLTModel>();
            FundingMemoLTModel Document = new FundingMemoLTModel();

            if (!repository.GetFundingMemoTL(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "CIF" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (!repository.GetEmployeeCSO(ref Employee, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }

                else
                {
                    output.Employee = Employee;
                    output.Document = Document;
                    return Request.CreateResponse<FundingMemoLTGrid>(HttpStatusCode.OK, output);
                }

            }


        }
        [ResponseType(typeof(FundingMemoLTGrid))]
        [AcceptVerbs("GET")]
        [Route("api/FundingMemoLT/GetPopUp")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<FundingMemoLTFilter> filters, string CIF)
        {
            FundingMemoLTGrid output = new FundingMemoLTGrid();
            IList<EmployeeFundingMemoLTModel> Employee = new List<EmployeeFundingMemoLTModel>();
            FundingMemoLTModel Document = new FundingMemoLTModel();

            if (!repository.GetFundingMemoTLCSOApp(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "CIF" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message, CIF))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (!repository.GetEmployeeCSO(ref Employee, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }

                else
                {
                    output.Employee = Employee;
                    output.Document = Document;
                    return Request.CreateResponse<FundingMemoLTGrid>(HttpStatusCode.OK, output);
                }

            }


        }
        [ResponseType(typeof(IList<EmployeeFundingMemoLTModel>))]
        [HttpGet]
        [Route("api/FundingMemoLT/getcso")]
        public HttpResponseMessage GetCSOName(string query)
        {
            IList<EmployeeFundingMemoLTModel> result = new List<EmployeeFundingMemoLTModel>();

            if (!repository.GetCSOName(query, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<EmployeeFundingMemoLTModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(FundingMemoLTModel))]
        [HttpGet]
        [Route("api/FundingMemoLT/GetBaseRate")]
        public HttpResponseMessage GetBaseRate(string currency, string tenorCode)
        {
            FundingMemoLTModel result = new FundingMemoLTModel();

            if (!repository.GetBaseRate(currency, tenorCode, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<FundingMemoLTModel>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/FundingMemoLT/GetDOA")]
        public HttpResponseMessage GetDOA(FundingMemoLTModel funding)
        {
            long transactionID = 0;
            if (!repository.GetApprovalDOA(ref funding, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Created, new { Funding = funding, Message = "Get DOA." });
            }
        }

        [ResponseType(typeof(IList<FundingMemoLTModel>))]
        [HttpGet]
        [Route("api/FundingMemoLT/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<FundingMemoLTModel> output = new List<FundingMemoLTModel>();

            if (!repository.GetFundingMemoTL(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<FundingMemoLTModel>>(HttpStatusCode.OK, output);
            }
        }
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(FundingMemoLTModel funding)
        {


            if (!repository.AddFundingMemoTL(funding, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Funding data has been created." });
            }

        }
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/FundingMemoLT/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, FundingMemoLTModel Funding)
        {

            if (!repository.UpdateFundingMemoTL(id, Funding, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Message = "FundingMemo data has been updated." });
            }
        }
        [HttpGet]
        [ResponseType(typeof(string))]
        [Route("api/FundingMemoTL/Customer/Search")]
        //[Authorize(Roles = "DBS PPU Maker")]
        public HttpResponseMessage SearchCustomer(string query, int? limit) //documents
        {
            IList<CustomerLoanModel> output = new List<CustomerLoanModel>();

            if (!repository.GetCustomerLoan(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerLoanModel>>(HttpStatusCode.OK, output);
            }
        }

        [HttpDelete]
        [Route("api/FundingMemoLT/Delete/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {

            if (!repository.DeleteFundingMemoTL(id, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Message = "FundingMemo data has been deleted." });
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }

    }
}