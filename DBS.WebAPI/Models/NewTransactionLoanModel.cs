﻿using DBS.Entity;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;

namespace DBS.WebAPI.Models
{
    #region Property
    public class TransactionDraftLoanModel
    {
        public long ID { get; set; }
        public string ApplicationID { get; set; }
        public int? IsTopUrgent { get; set; }
        public CustomerModel Customer { get; set; }
        public bool? IsNewCustomer { get; set; }
        public ProductModel Product { get; set; }
        public CurrencyModel Currency { get; set; }
        public decimal? Amount { get; set; }
        public ChannelModel Channel { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public string CIF { get; set; }
        public BizSegmentModel BizSegment { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public string CreateBy { get; set; }
        public IList<TransactionDocumentDraftModel> Documents { get; set; }
        public int SourceID { get; set; }
    }
    #endregion

    #region Interface
    public interface INewTransactionLoanRepository : IDisposable
    {
        bool AddTransactionLoan(TransactionDetailModel transaction, ref long transactionID, ref string ApplicationID, ref string message);
        bool GetLoanDisburseDetails(Guid workflowInstanceID, ref TransactionDisbursementModel output, ref IList<TransactionRolloverModel> output2, ref IList<VerifyModel> verify, ref string message);
        bool GetLoanDisburseDetails(Int64 tranID, ref TransactionDisbursementModel output, ref string message);
        bool UpdateTransactionDraft(long id, TransactionDetailModel transaction, ref string message);
        bool GetTransactionDraftByID(long id, ref TransactionDraftLoanModel transaction, ref string message);
        bool DeleteTransactionDraftByID(int id, ref string Message);
    }
    #endregion

    #region Repository
    public class NewTransactionLoanRepository : INewTransactionLoanRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);

            GC.Collect();
            GC.SuppressFinalize(this);
        }
        public bool AddTransactionLoan(TransactionDetailModel transaction, ref long transactionID, ref string ApplicationID, ref string message)
        {
            bool IsSuccess = false;
            long TrIDInserted = 0;
            using (DBSEntities context_ts = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        if (transaction.IsDraft)
                        {
                            #region Saving to Draft
                            try
                            {
                                DBS.Entity.TransactionDraft data = new DBS.Entity.TransactionDraft()
                                {
                                    #region Non Used Field Not Null
                                    ApplicationDate = DateTime.Now,
                                    AmountUSD = 0.00M,
                                    Rate = 0.00M,
                                    DebitCurrencyID = 1,
                                    BizSegmentID = 1,
                                    BeneName = "-",
                                    BankID = 1,
                                    IsResident = false,
                                    IsCitizen = false,
                                    #endregion

                                    IsTopUrgent = (transaction.IsTopUrgentChain == 1 ? 2 : transaction.IsTopUrgent),
                                    CIF = transaction.Customer.CIF,
                                    IsDraft = transaction.IsDraft,
                                    ApplicationID = transaction.ApplicationID,
                                    Amount = transaction.Amount,
                                    IsNewCustomer = transaction.IsNewCustomer,
                                    StateID = (int)StateID.OnProgress,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName,
                                    ProductID = transaction.Product.ID
                                };

                                if (transaction.Channel.Name != null)
                                {
                                    data.ChannelID = transaction.Channel.ID;
                                }
                                if (transaction.Source != null)
                                {
                                    if (transaction.Source.Name != null)
                                    {
                                        data.SourceID = transaction.Source.ID;
                                    }
                                }

                                if (transaction.BizSegment.Name != null)
                                {
                                    data.BizSegmentID = transaction.BizSegment.ID;
                                }

                                if (!transaction.IsNewCustomer)
                                {
                                    data.CIF = transaction.Customer.CIF;

                                    if (transaction.Currency.Code != null)
                                    {
                                        data.CurrencyID = transaction.Currency.ID;
                                    }
                                }
                                else
                                {
                                    data.DraftCIF = transaction.CustomerDraft.DraftCIF;

                                    data.DraftCurrencyID = transaction.CustomerDraft.DraftAccountNumber.Currency.ID;
                                    data.DraftDebitCurrencyID = transaction.DebitCurrency.ID;
                                    if (transaction.Currency.Code != null)
                                    {
                                        data.CurrencyID = transaction.Currency.ID;
                                    }
                                }

                                context_ts.TransactionDrafts.Add(data);
                                context_ts.SaveChanges();
                                #region Save Document
                                if (transaction.Documents.Count > 0)
                                {
                                    foreach (var item in transaction.Documents)
                                    {
                                        Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                                        {
                                            TransactionID = data.TransactionID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context_ts.TransactionDocumentDrafts.Add(document);
                                    }
                                    context_ts.SaveChanges();
                                }
                                #endregion
                                ts.Complete();
                                return true;
                            }
                            catch (Exception ex)
                            {
                                message = ex.Message
                                    + " : " + ((ex.InnerException != null) ? ex.InnerException.Message : "")
                                    + " : " + ((ex.InnerException.InnerException != null) ? ex.InnerException.InnerException.Message : "") + transaction.ApplicationDate.ToString();
                                return false;
                            }


                            #endregion
                        }
                        else
                        {
                            #region Submit New Transaction
                            #region New Customer
                            if (transaction.IsNewCustomer)
                            {
                                context_ts.Customers.Add(new Customer()
                                {
                                    CIF = transaction.Customer.CIF,
                                    CustomerName = transaction.Customer.Name,
                                    BizSegmentID = 1,
                                    LocationID = 1,
                                    CustomerTypeID = 1,
                                    SourceID = 1,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });
                                context_ts.SaveChanges();

                                context_ts.CustomerAccounts.Add(new CustomerAccount()
                                {
                                    CIF = transaction.Customer.CIF,
                                    AccountNumber = transaction.Account.AccountNumber,
                                    CurrencyID = transaction.Account.Currency.ID,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });
                                context_ts.SaveChanges();
                            }
                            #endregion

                            DBS.Entity.Transaction data = new DBS.Entity.Transaction()
                            {
                                #region Non Used Field Not Null
                                AmountUSD = 0.00M,
                                Rate = 0.00M,
                                DebitCurrencyID = 1,
                                BeneName = "-",
                                BankID = 1,
                                IsResident = false,
                                IsCitizen = false,
                                IsSignatureVerified = false,
                                IsDormantAccount = false,
                                IsFrezeAccount = false,
                                #endregion

                                IsDocumentComplete = false,
                                IsDraft = transaction.IsDraft,
                                TransactionID = transaction.ID,
                                ApplicationID = transaction.ApplicationID,
                                IsTopUrgent = (transaction.IsTopUrgentChain == 1 ? 2 : transaction.IsTopUrgent.Value),
                                CIF = transaction.Customer.CIF,
                                ProductID = transaction.Product.ID,
                                CurrencyID = transaction.Currency.ID,
                                Amount = transaction.Amount,
                                ChannelID = transaction.Channel.ID,
                                //SourceID = transaction.Source.ID,
                                ApplicationDate = transaction.ApplicationDate,
                                BizSegmentID = transaction.Customer.BizSegment.ID,
                                BeneAccNumber = transaction.BeneAccNumber,
                                IsNewCustomer = transaction.IsNewCustomer,
                                StateID = (int)StateID.OnProgress,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName,
                                TouchTimeStartDate = transaction.CreateDate,
                                Mode = "UNSCHEDULED" //Rizki - 2017-07-02
                            };

                            if (transaction.Source != null)
                            {
                                if (transaction.Source.Name != null)
                                {
                                    data.SourceID = transaction.Source.ID;
                                }
                            }

                            context_ts.Transactions.Add(data);
                            context_ts.SaveChanges();

                            transactionID = data.TransactionID;
                            TrIDInserted = transactionID;

                            #region Saving Document

                            if (transaction.Documents.Count > 0)
                            {
                                foreach (var item in transaction.Documents)
                                {
                                    Entity.TransactionDocument document = new Entity.TransactionDocument()
                                    {
                                        TransactionID = data.TransactionID,
                                        DocTypeID = item.Type.ID,
                                        PurposeID = item.Purpose.ID,
                                        Filename = item.FileName,
                                        DocumentPath = item.DocumentPath,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName
                                    };

                                    context_ts.TransactionDocuments.Add(document);
                                }

                                context_ts.SaveChanges();
                            }
                            #endregion

                            ts.Complete();
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally { ts.Dispose(); }
                }
            }

            if (!transaction.IsDraft)
            {
                using (DBSEntities context = new DBSEntities())
                {
                    var getTR = (from TR in context.Transactions
                                 where TR.TransactionID == TrIDInserted
                                 select TR).SingleOrDefault();
                    ApplicationID = getTR.ApplicationID;
                    IsSuccess = true;
                }
            }
            return IsSuccess;
        }
        public bool GetLoanDisburseDetails(Guid workflowInstanceID, ref TransactionDisbursementModel output, ref IList<TransactionRolloverModel> output2, ref IList<VerifyModel> verify, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                // get main transaction data
                output = (from a in context.Transactions
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new TransactionDisbursementModel
                          {
                              ID = a.TransactionID,
                              workflowInstanceID = a.WorkflowInstanceID.Value,
                              applicationID = a.ApplicationID,
                              applicationDate = a.ApplicationDate,
                              ValueDate = a.ApplicationDate,
                              Customer = new CustomerModel()
                              {
                                  CIF = a.CIF
                              },
                              Product = new ProductModel()
                              {
                                  ID = a.Product.ProductID,
                                  Code = a.Product.ProductCode,
                                  Name = a.Product.ProductName
                              },
                              //bangkit
                              Currency = new CurrencyModel()
                              {
                                  ID = a.Currency.CurrencyID,
                                  Code = a.Currency.CurrencyCode,
                                  Description = a.Currency.CurrencyDescription,
                                  LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                  LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                              },
                              BizSegment = new BizSegmentModel()
                              {
                                  ID = a.BizSegment.BizSegmentID,
                                  Name = a.BizSegment.BizSegmentName,
                                  Description = a.BizSegment.BizSegmentDescription,
                                  LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                  LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                              },
                              Channel = new ChannelModel()
                              {
                                  ID = a.Channel.ChannelID,
                                  Name = a.Channel.ChannelName
                              },
                              transactionAmount = a.Amount,
                              LoanContractNo = a.LoanContractNo,
                              IsNewCustomer = a.IsNewCustomer,
                              CreateDate = a.CreateDate,
                              LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                              LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                          }).SingleOrDefault();
                verify = context.TransactionColumns.Where(x => x.IsDeleted.Equals(false))
                            .Select(x => new VerifyModel()
                            {
                                ID = x.TransactionColumnID,
                                Name = x.ColumnName,
                                IsVerified = false
                            })
                            .ToList();
                // fill payment data
                if (output != null)
                {
                    if (output.Product.ID == 20 || output.Product.ID == 2)
                    {
                        // get current payment data if exist
                        var disburse = context.SP_GetLoanDisbursement(output.ID).FirstOrDefault();
                        output.ParameterSystem = new ParameterSystemModelFunding();
                        output.ParameterSystem.CustomerCategory = new CustomerCategory() { CustomerCategoryID = disburse.CustomerCategoryID, CustomerCategoryName = disburse.CustomerCategoryName };
                        output.ParameterSystem.LoanTypeID = disburse.LoanTypeID;
                        output.ParameterSystem.LoanTypeName = disburse.LoanTypeName;
                        output.ParameterSystem.InterestFrequency = new InterestFrequency() { InterestFrequencyID = disburse.InterestFrequencyID, InterestFrequencyName = disburse.InterestFrequencyName };
                        output.SolID = disburse.SolID;
                        output.ParameterSystem.ProgramType = new ProgramType() { ProgramTypeID = disburse.ProgramTypeID, ProgramTypeName = disburse.ProgramTypeName };
                        output.ParameterSystem.FinacleScheme = new FinacleScheme() { FinacleSchemeCodeID = disburse.FinacleSchemeCodeID, FinacleSchemeCodeName = disburse.FinacleSchemeCodeName };
                        output.IsAdhoc = disburse.IsAdhoc.Value;
                        output.DebitingOperativeID = disburse.DebitingOperativeID.Value;
                        output.CreditingOperativeID = disburse.CreditingOperativeID.Value;
                        output.MaturityDate = disburse.MaturityDate.Value;
                        output.ParameterSystem.Interest = new Interest() { InterestCodeID = disburse.InterestCodeID, InterestCodeName = disburse.InterestCodeName };
                        output.BaseRate = disburse.BaseRate.Value;
                        output.AccountPreferencial = disburse.AccountPreferencial.Value;
                        output.AllInRate = disburse.AllInRate.Value;
                        output.ParameterSystem.RepricingPlan = new RepricingPlan() { RepricingPlanID = disburse.RepricingPlanID, RepricingPlanName = disburse.RepricingPlanName };
                        output.ApprovedMarginAsPerCM = disburse.ApprovedMarginAsPerCM.Value;
                        output.SpecialFTP = disburse.SpecialFTP.Value;
                        output.Margin = disburse.Margin.Value;
                        output.AllInSpecialRate = disburse.AllInSpecialRate.Value;
                        output.PeggingDate = disburse.PeggingDate.Value;
                        output.ParameterSystem.PeggingFrequency = new PeggingFrequency() { PeggingFrequencyID = disburse.PeggingFrequencyID, PeggingFrequencyName = disburse.PeggingFrequencyName };
                        output.PrincipalStartDate = disburse.PrincipalStartDate.Value;
                        output.InterestStartDate = disburse.InterestStartDate.Value;
                        output.NoOfInstallment = disburse.NoOfInstallment;
                        output.ParameterSystem.PrincipalFrequency = new PrincipalFrequency() { PrincipalFrequencyID = disburse.PrincipalFrequencyID, PricipalFrequencyName = disburse.PrincipalFrequencyName };
                        output.LimitIDinFin10 = disburse.LimitIDinFin10;
                        output.LimitExpiryDate = disburse.LimitExpiryDate.Value;
                        output.SanctionLimit = disburse.SanctionLimit.Value;
                        output.SanctionLimitCurr = new CurrencyModel() { ID = disburse.SanctionLimitCurrID.Value, Code = disburse.SanctionLimitCurrName };
                        output.utilization = disburse.Utilization.Value;
                        output.UtilizationCurr = new CurrencyModel() { ID = disburse.UtilizationCurrID.Value, Code = disburse.UtilizationCurrName };
                        output.Outstanding = disburse.Outstanding.Value;
                        output.OutstandingCurr = new CurrencyModel() { ID = disburse.OutstandingCurrID.Value, Code = disburse.OutstandingCurrName };
                        output.CSOName = disburse.CSOName;
                        if (output != null) IsSuccess = true;
                    }
                    else if (output.productID == 21)
                    {
                        var rollover = context.SP_GetLoanRollover(output.ID);
                        output2 = new List<TransactionRolloverModel>();
                        foreach (var r in rollover)
                        {
                            TransactionRolloverModel model = new TransactionRolloverModel()
                            {
                                SOLID = r.SOLID,
                                SchemeCode = r.SchemeCode,
                                CIF = r.CIF,
                                CustomerName = r.CustomerName,
                                LoanContractNo = r.LoanContractNo,
                                DueDate = r.DueDate.Value,
                                Currency = new CurrencyModel() { ID = r.CurrencyID.Value, Code = r.CurrencyCode },
                                AmountDue = r.AmountDue.Value,
                                LimitID = r.LimitID,
                                BizSegment = new BizSegmentModel() { ID = r.BizSegmentID.Value, Description = r.BizSegmentDescription },
                                RM = new EmployeeModel() { EmployeeID = r.EmployeeID.Value, EmployeeName = r.EmployeeName },
                                CSO = r.CSO,
                                IsSBLCSecured = r.IsSBLCSecured.Value,
                                IsHeavyEquipment = r.IsHeavyEquipment.Value,
                                ParameterSystem = new ParameterSystemModelFunding() { MaintenanceType = new LoanMaintenanceType() { MaintenanceTypeID = r.MaintenanceTypeID, MaintenanceTypeName = r.MaintenanceTypeName }, Interest = new Interest() { InterestCodeID = r.InterestRateCodeID, InterestCodeName = r.InterestRateCodeName } },
                                AmountRollover = r.AmountRollover.Value,
                                NextPrincipalDate = r.NextPrincipalDate.Value,
                                NextInterestDate = r.NextInterestDate.Value,
                                ApprovedMarginAsPerCM = r.ApprovedMarginAsPerCM.Value,
                                BaseRate = r.BaseRate.Value,
                                AllInLP = r.AllInLP.Value,
                                AllInrate = r.AllInFTPRate.Value,
                                AccountPreferencial = r.AccountPreferencial.Value,
                                SpecialFTP = r.SpecialFTP.Value,
                                SpecialRateMargin = r.SpecialRateMargin.Value,
                                ApprovalDOA = r.ApprovalDOA,
                                ApprovedBy = r.ApprovedBy,
                                Remarks = r.Remarks,
                                IsAdhoc = r.IsAdhoc.Value
                            };
                            output2.Add(model);
                        }
                        if (output2 != null) IsSuccess = true;
                    }


                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetLoanDisburseDetails(long tranID, ref TransactionDisbursementModel output, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                // get main transaction data
                output = (from a in context.Transactions
                          where a.TransactionID.Equals(tranID)
                          select new TransactionDisbursementModel
                          {
                              ID = a.TransactionID,
                              workflowInstanceID = a.WorkflowInstanceID.Value,
                              applicationID = a.ApplicationID,
                              applicationDate = a.ApplicationDate,
                              //bangkit
                              Currency = new CurrencyModel()
                              {
                                  ID = a.Currency.CurrencyID,
                                  Code = a.Currency.CurrencyCode,
                                  Description = a.Currency.CurrencyDescription,
                                  LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                  LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                              },
                              BizSegment = new BizSegmentModel()
                              {
                                  ID = a.BizSegment.BizSegmentID,
                                  Name = a.BizSegment.BizSegmentName,
                                  Description = a.BizSegment.BizSegmentDescription,
                                  LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                  LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                              },
                              IsNewCustomer = a.IsNewCustomer,
                              CreateDate = a.CreateDate,
                              LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                              LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                              debitAccNumber = a.AccountNumber

                          }).SingleOrDefault();

                // fill payment data
                if (output != null)
                {

                    // get current payment data if exist
                    var loan = (from a in context.TransactionLoans
                                where a.Transaction.TransactionID.Equals(tranID)
                                orderby a.TransactionLoanID descending
                                select a).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool UpdateTransactionDraft(long id, TransactionDetailModel transaction, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.TransactionDraft data = context.TransactionDrafts.Where(a => a.TransactionID.Equals(id)).SingleOrDefault();
                    if (data != null)
                    {
                        data.IsDraft = transaction.IsDraft;
                        data.ApplicationID = transaction.ApplicationID;
                        data.IsTopUrgent = transaction.IsTopUrgent;
                        data.Amount = transaction.Amount;
                        data.ApplicationDate = transaction.ApplicationDate;
                        data.IsNewCustomer = transaction.IsNewCustomer;
                        data.StateID = (int)StateID.OnProgress;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        if (transaction.Channel != null && transaction.Channel.Name != null)
                        {
                            data.ChannelID = transaction.Channel.ID;
                        }
                        if (transaction.Source != null && transaction.Source.Name != null)
                        {
                            data.SourceID = transaction.Source.ID;
                        }
                        data.BizSegmentID = 1;
                        if (transaction.Product != null && transaction.Product.Code != null)
                        {
                            data.ProductID = transaction.Product.ID;
                        }

                        if (!transaction.IsNewCustomer)
                        {
                            data.CIF = transaction.Customer.CIF;

                            if (transaction.Currency != null && transaction.Currency.Code != null)
                            {
                                data.CurrencyID = transaction.Currency.ID;
                            }

                            data.DebitCurrencyID = 1;
                        }
                        else
                        {
                            data.DraftCIF = transaction.CustomerDraft.DraftCIF;
                            data.DraftAccountNumber = transaction.CustomerDraft.DraftAccountNumber.AccountNumber;
                            data.DraftCustomerName = transaction.CustomerDraft.DraftCustomerName;
                            data.DraftCurrencyID = transaction.CustomerDraft.DraftAccountNumber.Currency.ID;

                            if (transaction.Currency != null && transaction.Currency.Code != null)
                            {
                                data.CurrencyID = transaction.Currency.ID;
                            }
                        }
                    }
                    context.SaveChanges();
                    #region Save Document
                    var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(transaction.ID)).ToList();
                    if (existingDocs != null)
                    {
                        if (existingDocs.Count() > 0)
                        {
                            foreach (var item in existingDocs)
                            {
                                var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                deleteDoc.IsDeleted = true;
                                context.SaveChanges();
                            }
                        }
                    }
                    if (transaction.Documents.Count > 0)
                    {
                        foreach (var item in transaction.Documents)
                        {
                            Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                            {
                                TransactionID = data.TransactionID,
                                DocTypeID = item.Type.ID,
                                PurposeID = item.Purpose.ID,
                                Filename = item.FileName,
                                DocumentPath = item.DocumentPath,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName
                            };
                            context.TransactionDocumentDrafts.Add(document);
                        }
                        context.SaveChanges();
                    }
                    #endregion
                    ts.Complete();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }

            return isSuccess;
        }
        public bool GetTransactionDraftByID(long id, ref TransactionDraftLoanModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();
                TransactionDraftLoanModel datatransaction = (from trDraft in context.TransactionDrafts
                                                             join Prd in context.Products on trDraft.ProductID equals (Prd.ProductID)
                                                             join Cust in context.Customers on trDraft.CIF equals (Cust.CIF)
                                                             where trDraft.TransactionID.Equals(id)
                                                             select new TransactionDraftLoanModel
                                                             {
                                                                 ID = trDraft.TransactionID,
                                                                 ApplicationID = trDraft.ApplicationID,
                                                                 ApplicationDate = trDraft.ApplicationDate,
                                                                 Amount = trDraft.Amount,
                                                                 IsTopUrgent = trDraft.IsTopUrgent,
                                                                 IsNewCustomer = trDraft.IsNewCustomer,
                                                                 CreateDate = trDraft.CreateDate,
                                                                 LastModifiedBy = trDraft.UpdateDate == null ? trDraft.CreateBy : trDraft.UpdateBy,
                                                                 LastModifiedDate = trDraft.UpdateDate == null ? trDraft.CreateDate : trDraft.UpdateDate.Value,
                                                                 Product = new ProductModel { ID = Prd.ProductID, Name = Prd.ProductName },
                                                                 CIF = trDraft.CIF,
                                                                 //SourceID = trDraft.SourceID.Value
                                                             }).SingleOrDefault();
                if (datatransaction != null)
                {
                    var data = context.TransactionDrafts.Where(x => x.TransactionID.Equals(datatransaction.ID)).Select(x => x).FirstOrDefault();
                    if (datatransaction.IsNewCustomer != null && datatransaction.IsNewCustomer != true)
                    {
                        string cif = context.TransactionDrafts.Where(a => a.TransactionID.Equals(id)).Select(a => a.CIF).FirstOrDefault();
                        if (!string.IsNullOrEmpty(cif))
                        {
                            ICustomerRepository customerRepo = new CustomerRepository();
                            CustomerModel customer = new CustomerModel();
                            if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                            {
                                datatransaction.Customer = customer;
                            }

                            customerRepo.Dispose();
                            customer = null;
                        }
                    }
                    if (data.Currency != null)
                    {
                        CurrencyModel c = new CurrencyModel();

                        c.ID = data.Currency.CurrencyID;
                        c.Code = data.Currency.CurrencyCode;
                        c.Description = data.Currency.CurrencyDescription;

                        datatransaction.Currency = c;
                    }
                    if (data.SourceID != null)
                    {
                        datatransaction.SourceID = data.SourceID.Value;
                    }

                    if (data.Channel != null)
                    {
                        ChannelModel cn = new ChannelModel();
                        cn.ID = data.Channel.ChannelID;
                        cn.Name = data.Channel.ChannelName;
                        datatransaction.Channel = cn;
                    }

                    if (data.BizSegment != null)
                    {
                        BizSegmentModel bz = new BizSegmentModel();
                        bz.ID = data.BizSegment.BizSegmentID;
                        bz.Name = data.BizSegment.BizSegmentName;
                        bz.Description = data.BizSegment.BizSegmentDescription;
                        datatransaction.BizSegment = bz;
                    }
                }

                var docDraft = (from doc in context.TransactionDocumentDrafts
                                where doc.TransactionID == id && doc.IsDeleted.Equals(false)
                                select new TransactionDocumentDraftModel
                                {
                                    ID = doc.TransactionDocumentID,
                                    IsDormant = doc.IsDormant,
                                    LastModifiedBy = doc.UpdateBy == null ? doc.CreateBy : doc.UpdateBy,
                                    LastModifiedDate = doc.UpdateDate == null ? doc.CreateDate : doc.CreateDate,
                                    Purpose = new DocumentPurposeModel { ID = doc.DocumentPurpose.PurposeID, Name = doc.DocumentPurpose.PurposeName },
                                    Type = new DocumentTypeModel { ID = doc.DocumentType.DocTypeID, Name = doc.DocumentType.DocTypeName },
                                    FileName = doc.Filename,
                                    DocumentPath = new DocumentPathModel { name = doc.Filename, lastModified = string.Empty, lastModifiedDate = DateTime.Now, DocPath = doc.DocumentPath, type = Util.ExistingDoctype }
                                }).ToList();

                if (docDraft != null)
                {
                    datatransaction.Documents = docDraft;
                }

                transaction = datatransaction;
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        public bool DeleteTransactionDraftByID(int id, ref string Message)
        {
            bool isDelete = false;
            using (DBSEntities context_ts = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        Entity.TransactionDraft data = context.TransactionDrafts.Where(a => a.TransactionID.Equals(id)).SingleOrDefault();
                        if (data != null)
                        {
                            var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(id)).ToList();
                            if (existingDocs != null)
                            {
                                if (existingDocs.Count() > 0)
                                {
                                    foreach (var item in existingDocs)
                                    {
                                        var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                        context.TransactionDocumentDrafts.Remove(deleteDoc);
                                        context.SaveChanges();
                                    }
                                }
                            }
                            context.TransactionDrafts.Remove(data);
                            context.SaveChanges();
                            isDelete = true;
                        }
                        else
                        {
                            Message = string.Format("Draft ID {0} is does not exist.", id);
                            isDelete = false;
                        }
                        ts.Complete();
                    }
                    catch (Exception ex)
                    {
                        ts.Dispose();
                        Message = ex.Message;
                    }
                }
            }
            return isDelete;
        }
    }
    #endregion
}