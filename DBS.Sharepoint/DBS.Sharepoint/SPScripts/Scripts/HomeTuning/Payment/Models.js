
//add aridya 20160921 for CityModel
var CityModel = {
    CityID: ko.observable(),
    CityCode: ko.observable(),
    Description: ko.observable(),
    ProvinceID: ko.observable(),
    IsDeleted: ko.observable(),
    CreateDate: ko.observable(),
    CreateBy: ko.observable(),
    UpdateDate: ko.observable(),
    UpdateBy: ko.observable()
}

var AmountModel = {
    TotalPPUAmountsUSD: ko.observable(0),
    TotalDealAmountsUSD: ko.observable(0),
    RemainingBalance: ko.observable(0),
    TotalPPUUtilization: ko.observable(0),
    TotalDealUtilization: ko.observable(0),
    TotalAmountsUSD: ko.observable(0),
}

var PeggingFrequency = {
    PeggingFrequencyID: ko.observable(),
    PeggingFrequencyName: ko.observable()
};
var RepricingPlan = {
    RepricingPlanID: ko.observable(),
    RepricingPlanName: ko.observable()
};
var InterestFrequency = {
    InterestFrequencyID: ko.observable(),
    InterestFrequencyName: ko.observable()
}
var PrincipalFrequency = {
    PrincipalFrequencyID: ko.observable(),
    PricipalFrequencyName: ko.observable()
}
var SanctionLimitModel = {
    ID: ko.observable(),
    CodeDescription: ko.observable()
};

var utilizationModel = {
    ID: ko.observable(),
    CodeDescription: ko.observable()
};

var OutstandingModel = {
    ID: ko.observable(),
    CodeDescription: ko.observable()

};
//add by fandi
var ProgramType = {
    ProgramTypeID: ko.observable(),
    ProgramTypeName: ko.observable()
};
var FinacleScheme = {
    FinacleSchemeCodeID: ko.observable(),
    FinacleSchemeCodeName: ko.observable()
};
var Interest = {//interest
    InterestID: ko.observable(),
    nterestCodeID: ko.observable(),
    InterestCodeName: ko.observable()
};

var ParameterSystemModelFunding = {//LoanID
    LoanTypeID: ko.observable(),
    LoanTypeName: ko.observable()
}
var FundingModel = {
    SolIDList: ko.observable(SolIDModel),
    Customer: ko.observable(CustomerModel),
    CIF: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Currency: ko.observable(CurrencyModel),
    Location: ko.observable(),
    CSOName: ko.observable(),
    SolID: ko.observable(),
    LoanID: ko.observable(ParameterSystemModelFunding),
    CustomerCategoryID: ko.observable(),
    AmountDisburseID: ko.observable(CurrencyModel),
    AmountDisburse: ko.observable(),
    Interest: ko.observable(Interest),
    PeggingFrequency: ko.observable(PeggingFrequency),
    RepricingPlan: ko.observable(RepricingPlan),
    CSOFundingMemoID: ko.observable(),
    TransactionFundingMemoID: ko.observable(),
    ValueDate: ko.observable(),
    IsAdhoc: ko.observable(),
    CreditingOperativeID: ko.observable(),
    DebitingOperativeID: ko.observable(),
    CreditingOperative: ko.observable(),
    DebitingOperative: ko.observable(),
    MaturityDate: ko.observable(),
    PrincipalStartDate: ko.observable(),//add
    InterestStartDate: ko.observable(),//add
    InterestFrequency: ko.observable(InterestFrequency),
    PrincipalFrequency: ko.observable(PrincipalFrequency),
    BaseRate: ko.observable(),
    AccountPreferencial: ko.observable(),
    AllInRate: ko.observable(),
    ApprovedMarginAsPerCM: ko.observable(),
    SpecialFTP: ko.observable(),
    Margin: ko.observable(),
    AllInSpecialRate: ko.observable(),
    PeggingDate: ko.observable(),
    NoOfInstallment: ko.observable(),
    LimitIDinFin10: ko.observable(),
    LimitExpiryDate: ko.observable(),
    SanctionLimitCurr: ko.observable(SanctionLimitModel),
    SanctionLimitAmount: ko.observable(),//add
    utilizationID: ko.observable(utilizationModel),
    utilizationAmount: ko.observable(),//add
    OutstandingID: ko.observable(OutstandingModel),
    OutstandingAmount: ko.observable(),//add
    OutstandingCurrID: ko.observable(),
    Remarks: ko.observable(),
    OtherRemarks: ko.observable(),
    ProgramType: ko.observable(ProgramType),
    FinacleScheme: ko.observable(FinacleScheme),
    LastModifiedBy: ko.observable(),
    LastModifiedDate: ko.observable(),
    fundingmemodocument: ko.observableArray([DocumentModel]),
};

self.ModeName = ko.observable("");

var optionModel = function (id, name) {
    var self = this;
    self.id = id;
    self.name = name;
}

//henggar
self.options = [
    new optionModel(1, "Resident"),
    new optionModel(0, "Non Resident")];

self.selectedOptionId = ko.observable("");
//end

self.OnChangeModeName = function () {
    var modeID;
    var modeName = $("#residentBene option:selected").text();

    if (modeName == "Please Select...") {
        modeID = null;
    }
    else if (modeName == "Resident") {
        modeID = 1;
    }
    else {
        modeID = 0;
    }
    viewModel.TransactionMaker().Transaction.IsBeneficiaryResident(modeID);
    //FormValidationTrxPayment();
};

/* dodit@2014.11.08:add format function */
var formatNumber = function (num) {
    if (num != null) {
        //num = num.toString().replace(/,/g, '');
        //num = parseFloat(num).toFixed(2);
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    } else { return 0; }
}

// henggar "wicak" 23 oktober 2016 copy func. format number for lld amount
var formatNumberLLD = function (num) {
    if (num != null) {
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    } else { return null; }
}

var SetPartialFull = function (partial) {
    if (partial == true) {
        return "Partial";
    }
    else
        return "Full";
}

//End Andi
var formatNumber_r = function (num) {
    if (num != null) {
        num = num.toString().replace(/,/g, '');
        num = parseFloat(num).toFixed(2);
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    } else { return 0; }
}

// henggar "wicak" 23 oktober 2016 copy function for lld amount
var formatNumber_l = function (num) {
    if (num != null) {
        num = num.toString().replace(/,/g, '');
        num = parseFloat(num).toFixed(2);
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    } else { return null; }
} //end

var resetNumber = function (num) {
    if (num != null) {
        num = num.toString().replace(/,/g, '');
        return parseFloat(num).toFixed(2);
    } else { return 0; }
}

var formatAccount = function (num) {

    num = num.replace(/-/g, '');

    sheet_a = (num.length > 0) ? num.substr(0, 4) : '';
    sheet_b = (num.length > 4) ? '-' + num.substr(4, 4) : '';
    sheet_c = (num.length > 8) ? '-' + num.substr(8) : '';

    return sheet_a + sheet_b + sheet_c;
}

var formatMakerDateApplication = function (task) {
    var date = document.getElementById("application-date").value;

    switch (task) {
        case "TransactionMaker":
            //console.log(moment(date).format('YYYY-MM-DD'));
            viewModel.TransactionMaker().Transaction.ApplicationDate = moment(date).format('YYYY-MM-DD');
            break;
    }
    viewModel.ApplicationDateMask(date);
}

var formatMakerDateExecution = function (task) {
    var date = document.getElementById("credit-value-date").value;

    switch (task) {
        case "TransactionMaker":
            //console.log(moment(date).format('YYYY-MM-DD'));
            viewModel.TransactionMaker().Transaction.ExecutionDate = moment(date).format('YYYY-MM-DD');
            break;
    }

    viewModel.ExecutionDateMask(date);
}

var formatMakerAccount = function (task) {
    //Transaction.BeneAccNumber
    var num = document.getElementById("bene-acc-number").value;
    num = num.replace(/-/g, '');

    switch (task) {
        case "PaymentMaker":
            viewModel.PaymentMaker().Payment.BeneAccNumber = num;
            break;
        case "TransactionMaker":
            viewModel.TransactionMaker().Transaction.BeneAccNumber = num;
            break;
        case "TransactionMakerAfterCaller":
            viewModel.TransactionMaker().Transaction.BeneAccNumber = num;
            break;
        case "TransactionCaller":
            viewModel.TransactionCallback().Transaction.BeneAccNumber = num;
            break;
        case "PaymentChecker":
            viewModel.PaymentChecker().Payment.BeneAccNumber = num;
            break;
    }

    sheet_a = (num.length > 0) ? num.substr(0, 4) : '';
    sheet_b = (num.length > 4) ? '-' + num.substr(4, 4) : '';
    sheet_c = (num.length > 8) ? '-' + num.substr(8) : '';

    viewModel.BeneAccNumberMask(sheet_a + sheet_b + sheet_c);
}

var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};

var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};

var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};

var TZModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable(),
    Class: ko.observable()
}

var BizSegmentModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var BranchModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

//Tambahan IPE
var BeneficiaryCountryModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};
var TransactionRelationshipModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};
var CBGCustomerModel = {
    ID: ko.observable(),
    Code: ko.observable()
};
var TransactionUsingDebitSundryModel = {
    ID: ko.observable(),
    Code: ko.observable()
};
//End

var TypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ProductModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    WorkflowID: ko.observable(),
    Workflow: ko.observable()
};

var ProductTypeModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var LLDModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var LLDDocument = {
    LLDDocumentID: ko.observable(),
    LLDDocumentCode: ko.observable(),
    Description: ko.observable()
};

var ChannelModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var BankModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    BranchCode: ko.observable(),
    SwiftCode: ko.observable(),
    BankAccount: ko.observable(),
    Description: ko.observable(),
    CommonName: ko.observable(),
    Currency: ko.observable(),
    PGSL: ko.observable(),
    IBranchBank: ko.observable()
};

var RMModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    RankID: ko.observable(),
    Rank: ko.observable(),
    SegmentID: ko.observable(),
    Segment: ko.observable(),
    BranchID: ko.observable(),
    Branch: ko.observable(),
    Email: ko.observable()
};

var ContactModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    PhoneNumber: ko.observable(),
    DateOfBirth: ko.observable(),
    Address: ko.observable(),
    IDNumber: ko.observable()
};

var FunctionModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var CurrencyModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};
//add henggar
var SundryModel = {
    ID: ko.observable(),
    OABAccNo: ko.observable()
};
//end
var DealUnderlyingModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var MidrateDataModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable(),
    RupiahRate: ko.observable(),
};

var StatementLetterModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var RateTypeModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var AccountModel = {
    AccountNumber: ko.observable(),
};

var UnderlyingDocumentModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable()
};

var DocumentTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var FXComplianceModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ChargesModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var NostroModel = {
    ID: ko.observable(),
    Currency: ko.observable(CurrencyModel),
    NostroUsed: ko.observable(),
    Code: ko.observable(),
    Bank: ko.observable()
};

var SundryModel = {
    ID: ko.observable(),
    UnitName: ko.observable(),
    Description: ko.observable(),
    OABAccNo: ko.observable(),
    OABAccName: ko.observable()
};

var ThresholdModel = {
    IsHitThreshold: ko.observable(),
    ThresholdValue: ko.observable(),
    RoundingValue: ko.observable(),
    TransactionType: ko.observable(),
    ResidentType: ko.observable()
}
var DocumentPurposeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var GenerateFileModel = {
    ItemID: ko.observable(),
    FileName: ko.observable(),
    URL: ko.observable(),
    ApplicationID: ko.observable(),
    CreateDate: ko.observable()
};

var LimitStatusModel = {
    UserCategoryCode: ko.observable(),
    MaxTransaction: ko.observable(),
    MinTransaction: ko.observable(),
    Unlimited: ko.observable(),
    Product: ko.observable(ProductModel),
    Currency: ko.observable(CurrencyModel)
}
var BranchBankModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    BranchCode: ko.observable(),
    SwiftCode: ko.observable(),
    BankAccount: ko.observable(),
    Description: ko.observable(),
    CommonName: ko.observable(),
    Currency: ko.observable(),
    PGSL: ko.observable(),
    IBranchBank: ko.observable(IBranchModel)
};

var IBranchModel = {
    ID: ko.observable(),
    CityID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    CityDescription: ko.observable(),
    CityCode: ko.observable()
};

var LoanCheckerUnsettlement = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    IsChainTopUrgent: ko.observable(false),
    IsNormal: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    LoanTransID: ko.observable(),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    Channel: ko.observable(ChannelModel),
    ValueDate: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    IsSignatureVerified: ko.observable(false),
    IsDormantAccount: ko.observable(false),
    IsFreezeAccount: ko.observable(false),
    Others: ko.observable(),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]), //DocumentModel
    FundingMemo: ko.observable(FundingModel),
    TransactionType: ko.observable(),
    MaintenanceType: ko.observable(),
    CreateDate: ko.observable(),
    outstanding: ko.observable(),
    sactionlimit: ko.observable(),
    utilazition: ko.observable(),
    CreateBy: ko.observable(),
    UpdateBy: ko.observable(),
    fundingmemodocument: ko.observableArray([DocumentModel]),
    LoanTransID: ko.observable(''),
}

var UTBranchCheckerModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    IsChainTopUrgent: ko.observable(false),
    IsNormal: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    Currency: ko.observable(CurrencyModel),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]), //DocumentModel    
    CreateDate: ko.observable(),
    CreateBy: ko.observable(),
    UpdateBy: ko.observable(),
    strfunctionType: ko.observable(),
    strAccountType: ko.observable(),

    FNACore: ko.observableArray([]),
    FunctionType: ko.observable([]),
    AccountType: ko.observable([]),

}
var UnderlyingModel = {
    ID: ko.observable(),
    UnderlyingDocument: ko.observable(UnderlyingDocumentModel),
    DocumentType: ko.observable(DocumentTypeModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    AttachmentNo: ko.observable(),
    IsStatementLetter: ko.observable(false),
    IsDeclarationOfException: ko.observable(false),
    StartDate: ko.observable(),
    EndDate: ko.observable(),
    DateOfUnderlying: ko.observable(),
    ExpiredDate: ko.observable(),
    ReferenceNumber: ko.observable(),
    SupplierName: ko.observable(),
    InvoiceNumber: ko.observable()
};

var DocumentModel = {
    ID: ko.observable(),
    Type: ko.observable(DocumentTypeModel),
    Purpose: ko.observable(DocumentPurposeModel),
    DocumentPath: ko.observable(),
    IsNewDocument: ko.observable(false),
    LastModifiedDate: ko.observable(new Date),
    LastModifiedBy: ko.observable(),
    IsDormant: ko.observable(false)
};

var CustomerModel = {
    CIF: ko.observable(),
    Name: ko.observable(),
    POAName: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Branch: ko.observable(BranchModel),
    Type: ko.observable(TypeModel),
    RM: ko.observable(RMModel),
    Contacts: ko.observableArray([ContactModel]),
    Functions: ko.observableArray([FunctionModel]),
    Accounts: ko.observableArray([AccountModel]),
    Underlyings: ko.observableArray([UnderlyingModel])
};

var TransactionModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    IsChainTopUrgent: ko.observable(false),
    IsNormal: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    IsNewCustomer: ko.observable(false),
    Product: ko.observable(ProductModel),
    ProductType: ko.observable(ProductTypeModel),
    DebitCurrency: ko.observable(CurrencyModel),
    DebitSundry: ko.observable(SundryModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    TrxRate: ko.observable(),
    AmountUSD: ko.observable(),
    Channel: ko.observable(ChannelModel),
    Account: ko.observable(AccountModel),
    ApplicationDate: ko.observable(),
    ExecutionDate: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Bank: ko.observable(BankModel),
    IsSignatureVerified: ko.observable(false),
    IsSyndication: ko.observable(false),
    IsDormantAccount: ko.observable(false),
    IsFreezeAccount: ko.observable(false),
    Others: ko.observable(),
    IsDraft: ko.observable(false),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]), //DocumentModel
    DetailCompliance: ko.observable(),
    BeneName: ko.observable(),
    OtherUnderlyingDoc: ko.observable(),
    TZNumber: ko.observable(),
    LoanContractNo: ko.observable(),
    LLD: ko.observable(LLDModel),
    LLDDocument: ko.observable(LLDDocument),
    LLDUnderlyingAmount: ko.observable(),
    IsOtherBeneBank: ko.observable(false),
    OtherBeneBankName: ko.observable(),
    OtherBeneBankSwift: ko.observable(),
    IsDocumentComplete: ko.observable(false),
    IsOtherAccountNumber: ko.observable(false),
    OtherAccountNumber: ko.observable(),
    IsJointAccount: ko.observable(false),
    //basri 29-09-2015
    IsLOIAvailable: ko.observable(),
    //end basri
    //add by fandi for exceptionHandling 
    IsExceptionHandling: ko.observable(false),
    ModePayment: ko.observable(false),
    PaymentDetails: ko.observable(),
    //end
    //add by adi for UTProduct
    Remarks: ko.observable(),
    AttachmentRemarks: ko.observable(),
    InvestmentID: ko.observable(UTModel),
    //end
    //Tambah Ipe
    BeneficiaryCountry: ko.observable(BeneficiaryCountryModel),
    TransactionRelationship: ko.observable(TransactionRelationshipModel),
    CBGCustomer: ko.observable(CBGCustomerModel),
    TransactionUsingDebitSundry: ko.observable(TransactionUsingDebitSundryModel),
    ChargingAccountCurrency: ko.observable(),
    ExecutionDate: ko.observable(),
};

//add by fandi
//add transaction history
//add by fandi for transactionHistory
var GetTransactionMakerModel = {
    ID: ko.observable(),
    TransactionID: ko.observable(),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    DebitAccNumber: ko.observable(),
    DebitSundry: ko.observable(),
    DebitCurrencyCode: ko.observable(),
    BeneName: ko.observable(),
    BeneBankName: ko.observable(),
    BeneAccNumber: ko.observable(),
    BeneBankSwift: ko.observable(),
    BankChargesCode: ko.observable(),
    AgentChargesCode: ko.observable(),
    IsResident: ko.observable(false),
    IsCitizen: ko.observable(false),
    LLDDescription: ko.observable(),
    PaymentDetails: ko.observable(),
    LastModifiedBy: ko.observable(),
    LastModifiedDate: ko.observable(),

    WorkflowInstanceID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    IsNormal: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    IsNewCustomer: ko.observable(false),
    Product: ko.observable(ProductModel),
    ProductType: ko.observable(ProductTypeModel),
    DebitCurrency: ko.observable(CurrencyModel),
    DebitSundry: ko.observable(SundryModel),
    Currency: ko.observable(CurrencyModel),
    Channel: ko.observable(ChannelModel),
    Account: ko.observable(AccountModel),
    ApplicationDate: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Bank: ko.observable(BankModel),
    IsSignatureVerified: ko.observable(false),
    IsDormantAccount: ko.observable(false),
    IsFreezeAccount: ko.observable(false),
    Others: ko.observable(),
    IsDraft: ko.observable(false),
    CreateDate: ko.observable(new Date),
    DetailCompliance: ko.observable(),
    TZNumber: ko.observable(),
    LLD: ko.observable(LLDModel),
    LLDDocument: ko.observable(LLDDocument),
    LLDUnderlyingAmount: ko.observable(),
    IsOtherBeneBank: ko.observable(false),
    OtherBeneBankName: ko.observable(),
    OtherBeneBankSwift: ko.observable(),
    IsDocumentComplete: ko.observable(false),
    IsOtherAccountNumber: ko.observable(false),
    OtherAccountNumber: ko.observable(),
    BankID: ko.observable(),
    BankCharges: ko.observable(),
    AgentCharges: ko.observable(),
    LLDID: ko.observable(),
    AttachmentRemark: ko.observable,
    //add aridya 20160921
    Branch: ko.observable(BranchModel),
    //City: ko.observable(CityModel)
};
//end
//end

var PaymentModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    IsNormal: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    IsNewCustomer: ko.observable(false),
    Product: ko.observable(ProductModel),
    ProductType: ko.observable(ProductTypeModel),
    Currency: ko.observable(CurrencyModel),
    DebitCurrency: ko.observable(CurrencyModel),
    DebitSundry: ko.observable(SundryModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    TrxRate: ko.observable(),
    AmountUSD: ko.observable(),
    Channel: ko.observable(ChannelModel),
    Account: ko.observable(AccountModel),
    ApplicationDate: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Bank: ko.observable(BankModel),
    BankCharges: ko.observable(ChargesModel),
    AgentCharges: ko.observable(ChargesModel),
    //add by fandi for paymaker nostro sundry
    Nostro: ko.observable(NostroModel),
    DynamicNostro: ko.observable(NostroModel),
    Sundry: ko.observable(SundryModel),
    //end
    Type: ko.observable(),
    IsCitizen: ko.observable(false),
    IsResident: ko.observable(false),
    /*LLDCode: ko.observable(),
     LLDInfo: ko.observable(),*/
    PaymentDetails: ko.observable(),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]), //DocumentModel
    DetailCompliance: ko.observable(),
    BeneName: ko.observable(),
    TZNumber: ko.observable(),
    FXCompliance: ko.observable(FXComplianceModel),
    LLD: ko.observable(LLDModel),
    LLDDocument: ko.observable(LLDDocument),
    LLDUnderlyingAmount: ko.observable(),
    IsOtherBeneBank: ko.observable(false),
    IsJointAccount: ko.observable(false),
    Branch: ko.observable(BranchModel), //add aridya 20160924
    CityCode: ko.observable(CityModel), //add aridya 20160924
};

var FundingMemoModel = {
    applicationID: ko.observable(),
    CSOFundingMemoID: ko.observable(),
    TransactionFundingMemoID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    LoanContractNo: ko.observable(),
    Currency: ko.observable(CurrencyModel),
    BizSegment: ko.observable(BizSegmentModel),
    Channel: ko.observable(ChannelModel),
    ValueDate: ko.observable(),
    Product: ko.observable(ProductModel),
    CustomerCategoryID: ko.observable(),
    CustomerCategoryName: ko.observable(),
    LoanTypeID: ko.observable(),
    LoanTypeName: ko.observable(),
    SolID: ko.observable(),
    Location: ko.observable(),
    ProgramTypeID: ko.observable(),
    ParameterSystem: ko.observable(),
    FinacleSchemeCodeID: ko.observable(),
    IsAdhoc: ko.observable(false),
    CreditingOperativeID: ko.observable(),
    DebitingOperativeID: ko.observable(),
    MaturityDate: ko.observable(),
    InterestCodeID: ko.observable(),
    BaseRate: ko.observable(),
    AccountPreferencial: ko.observable(),
    AllInRate: ko.observable(),
    RepricingPlanID: ko.observable(),
    ApprovedMarginAsPerCM: ko.observable(),
    SpecialFTP: ko.observable(),
    Margin: ko.observable(),
    AllInSpecialRate: ko.observable(),
    PeggingDate: ko.observable(),
    PeggingFrequencyID: ko.observable(),
    PrincipalStartDate: ko.observable(),
    InterestStartDate: ko.observable(),
    NoOfInstallment: ko.observable(),
    PrincipalFrequencyID: ko.observable(),
    InterestFrequencyID: ko.observable(),
    LimitIDinFin10: ko.observable(),
    LimitExpiryDate: ko.observable(),
    SanctionLimit: ko.observable(),
    SanctionLimitCurr: ko.observable(),
    utilization: ko.observable(),
    UtilizationCurr: ko.observable(),
    Outstanding: ko.observable(),
    OutstandingCurr: ko.observable(),
    CSOName: ko.observable(),
    Remarks: ko.observable(),
    OtherRemarks: ko.observable(),
    CreateDate: ko.observable(),
    CreateBy: ko.observable(),
    UpdateDate: ko.observable(),
    UpdateBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]),
}

var RolloverModel = {
    CSORolloverID: ko.observable(),
    TransactionRolloverID: ko.observable(),
    SOLID: ko.observable(),
    SchemeCode: ko.observable(),
    DueDate: ko.observable(),
    AmountDue: ko.observable(),
    LimitID: ko.observable(),
    BizSegmentID: ko.observable(),
    EmployeeID: ko.observable(),
    CSO: ko.observable(),
    IsSBLCSecured: ko.observable(),
    IsHeavyEquipment: ko.observable(),
    MaintenanceTypeID: ko.observable(),
    AmountRollover: ko.observable(),
    NextPrincipalDate: ko.observable(),
    NextInterestDate: ko.observable(),
    ApprovedMarginAsPerCM: ko.observable(),
    InterestRateCodeID: ko.observable(),
    BaseRate: ko.observable(),
    AllInLP: ko.observable(),
    AllInFTPRate: ko.observable(),
    AccountPreferencial: ko.observable(),
    SpecialFTP: ko.observable(),
    SpecialRateMargin: ko.observable(),
    AllInrate: ko.observable(),
    ApprovalDOA: ko.observable(),
    ApprovedBy: ko.observable(),
    Remarks: ko.observable(),
    IsAdhoc: ko.observable(),
    CSOName: ko.observable(),
    CreateDate: ko.observable(),
    CreateBy: ko.observable(),
    Updatedate: ko.observable(),
    UpdateBy: ko.observable(),
}

var InterestMaintenanceModel = {
    CSOInterestMaintenanceID: ko.observable(),
    TransactionInterestMaintenanceID: ko.observable(),
    SolID: ko.observable(),
    SchemeCode: ko.observable(),
    DueDate: ko.observable(),
    PreviousInterestRate: ko.observable(),
    CurrentInterestRate: ko.observable(),
    BizSegmentID: ko.observable(),
    EmployeeID: ko.observable(),
    CSO: ko.observable(),
    IsSBLCSecured: ko.observable(),
    IsHeavyEquipment: ko.observable(),
    MaintenanceTypeID: ko.observable(),
    AmountRollover: ko.observable(),
    NextPrincipalDate: ko.observable(),
    NextInterestDate: ko.observable(),
    ApprovedMarginAsPerCM: ko.observable(),
    InterestRateCodeID: ko.observable(),
    BaseRate: ko.observable(),
    AllinFTPRate: ko.observable(),
    AccountPreferencial: ko.observable(),
    SpecialFTP: ko.observable(),
    SpecialRate: ko.observable(),
    AllinRate: ko.observable(),
    ApprovalDOA: ko.observable(),
    ApprovedBy: ko.observable(),
    Remarks: ko.observable(),
    OtherRemarks: ko.observable(),
    IsAdhoc: ko.observable(),
    CSOName: ko.observable(),
    CreateDate: ko.observable(),
    CreateBy: ko.observable(),
    UpdateDate: ko.observable(),
    UpdateBy: ko.observable()
}

var ParameterSystemModel = {
    ID: ko.observable(),
    LoanTypeID: ko.observable(),
    LoanTypeName: ko.observable(),
    ProgramType: ko.observable(ProgramTypeModel),
    FinacleScheme: ko.observable(FinacleSchemeModel),
    Interest: ko.observable(InterestCodeModel),
    RepricingPlan: ko.observable(RepricingPlanModel),
    PeggingFrequency: ko.observable(PeggingFrequencyModel),
    InterestFrequency: ko.observable(InterestFrequencyModel),
    CustomerCategory: ko.observable(CustomerCategoryModel),
    PrincipalFrequency: ko.observable(PrincipalFrequencyModel),
    MaintenanceType: ko.observable(MaintenanceTypeModel)
}

var ProgramTypeModel = {
    ProgramTypeID: ko.observable(),
    ProgramTypeName: ko.observable(),
}

var FinacleSchemeModel = {
    FinacleSchemeCodeID: ko.observable(),
    FinacleSchemeCodeName: ko.observable(),
}

var InterestCodeModel = {
    InterestCodeID: ko.observable(),
    InterestCodeName: ko.observable(),
}

var RepricingPlanModel = {
    RepricingPlanID: ko.observable(),
    RepricingPlanName: ko.observable(),
}

var PeggingFrequencyModel = {
    PeggingFrequencyID: ko.observable(),
    PeggingFrequencyName: ko.observable(),
}

var InterestFrequencyModel = {
    InterestFrequencyID: ko.observable(),
    InterestFrequencyName: ko.observable(),
}

var CustomerCategoryModel = {
    CustomerCategoryID: ko.observable(),
    CustomerCategoryName: ko.observable(),
}

var PrincipalFrequencyModel = {
    PrincipalFrequencyID: ko.observable(),
    PricipalFrequencyName: ko.observable(),
}

var MaintenanceTypeModel = {
    MaintenanceTypeID: ko.observable(),
    MaintenanceTypeName: ko.observable(),
}

var LoanModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    IsNormal: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    IsNewCustomer: ko.observable(false),
    Product: ko.observable(ProductModel),
    ProductType: ko.observable(ProductTypeModel),
    Currency: ko.observable(CurrencyModel),
    DebitCurrency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    Channel: ko.observable(ChannelModel),
    Account: ko.observable(AccountModel),
    TransactionTypeID: ko.observable(),
    TransactionSubTypeID: ko.observable(),
    ValueDate: ko.observable(),
    LoanContractNo: ko.observable(),
    Remarks: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    FundingMemo: ko.observable(FundingMemoModel),
    Rollover: ko.observable(RolloverModel),
    InterestMaintenance: ko.observable(InterestMaintenanceModel)
};

//Basri 13.10.2015
var TMOModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    Channel: ko.observable(ChannelModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    DealNumber: ko.observable(),
    ValueDate: ko.observable(new Date),
    TMODetails: ko.observable(),
    Remarks: ko.observable(),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]),
};
//Agung
var FDModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    Channel: ko.observable(ChannelModel),
    TMODetails: ko.observable(),
    Remarks: ko.observable(),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]),
    Amount: ko.observable()
};
//End Agung
//Dani
//dani ut start
var CustomerDraftModel = {
    DraftCIF: ko.observable(),
    DraftCustomerName: ko.observable(),
    DraftAccountNumber: ko.observable(AccountModel)
}

var InvestmentListModel = {
    InvestmentID: ko.observable(),
    Investment: ko.observable(),
    CIF: ko.observable(),
    IsUT: ko.observable(),
    IsBond: ko.observable(),
    STNumber: ko.observable(),
    CheckIN: ko.observable(),
    CheckCIF: ko.observable(),
    CheckName: ko.observable(),
    CheckST: ko.observable(),
};
var FNACoreModel = {
    ID: ko.observable(0),
    Name: ko.observable("")
};
var FunctionTypeModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var AccountTypeModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var Transaction_TypeModel = {
    TransTypeID: ko.observable(),
    ProductID: ko.observable(),
    TransactionTypeName: ko.observable()
};
var FDTransactionTypeModel = {
    TransTypeID: ko.observable(),
    TransactionTypeName: ko.observable(),
    ProductID: ko.observable(),
    //ProductName: ko.observable()
}
var FDRemarksModel = {
    ID: ko.observable(),
    Name: ko.observable("")
};
//Andriyanto 2 Desember 2015
var LoanModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    TranID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    IsNormal: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    ProductType: ko.observable(ProductTypeModel),
    Currency: ko.observable(CurrencyModel),
    DebitCurrency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    Channel: ko.observable(ChannelModel),
    Account: ko.observable(AccountModel),
    CreditAccountNumber: ko.observable(),
    DebitAccNumber: ko.observable(),
    TransactionTypeID: ko.observable(),
    TransactionSubTypeID: ko.observable(),
    FDAccNumber: ko.observable(),
    InterestRate: ko.observable(),
    Tenor: ko.observable(),
    ValueDate: ko.observable(),
    MaturityDate: ko.observable(),
    TransactionRemarks: ko.observable(),
    Remarks: ko.observable(FDRemarksModel),
    TransactionType: ko.observable(FDTransactionTypeModel),
    BankNameAccNumber: ko.observable(),
    AttachmentRemarks: ko.observable(),
    IsSyndication: ko.observable(false),
    IsCallbackRequired: ko.observable(false),
    IsSignatureVerification: ko.observable(false),
    IsDormantAccount: ko.observable(false),
    IsFreezeAccount: ko.observable(false),
    IsLetterOfIndemnity: ko.observable(false),
    IsDocumentComplete: ko.observable(false),
    CreateDate: ko.observable(new Date),
    Documents: ko.observableArray([DocumentModel])
}
//End
//Andriyanto 2 Desember 2015 Loan
var CheckerLoanModel = {

    IsSignatureVerified: ko.observable(),
    IsDormantAccount: ko.observable(),
    IsFreezeAccount: ko.observable(),
    IsCallbackRequired: ko.observable(),
    IsLOIAvailable: ko.observable(),
    IsStatementLetterCopy: ko.observable(),
    IsDocComplete: ko.observable(),
    IsSyndication: ko.observable(),
}
//End Andriyanto 
var JoinAccountModel_ = {
    JoinOrder: ko.observable(),
    CustomerJoin: ko.observable(CustomerModel),
    CustomerRiskEffectiveDateJoin: ko.observable(),
    RiskScoreJoin: ko.observable(),
    RiskProfileExpiryDateJoin: ko.observable(),
    SolIDJoin: ko.observable(),
    ApproverID: ko.observable(),
    IsVerified: ko.observable(false),
    TransactionID: ko.observable(),
    JoinAccountBranchCheckerID: ko.observable(),
    JoinAccountID: ko.observable(),
    Flag: ko.observable()
}
var JoinAccountCBOModel_ = {
    JoinOrder: ko.observable(),
    CustomerJoin: ko.observable(CustomerModel),
    CustomerRiskEffectiveDateJoin: ko.observable(),
    RiskScoreJoin: ko.observable(),
    RiskProfileExpiryDateJoin: ko.observable(),
    SolIDJoin: ko.observable(),
    ApproverID: ko.observable(),
    IsVerified: ko.observable(false),
    TransactionID: ko.observable(),
    JoinAccountCBOCheckerID: ko.observable(),
    JoinAccountID: ko.observable(),
    Flag: ko.observable()
}
var FundListModel = {
    ID: ko.observable(),
    OffOnshoreModel: ko.observable(),
    FundCode: ko.observable(),
    FundName: ko.observable(),
    AccountNo: ko.observable(),
    SavingPlanDate: ko.observable(),
    IsSwitching: ko.observable(),
    IsSaving: ko.observable(),
    IsDeleted: ko.observable(),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
};
var UTModel = {
    //these needed in my form
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    FNACore: ko.observable(),
    FunctionType: ko.observable(),
    AccountType: ko.observable(),
    SolID: ko.observable(),
    CRiskEfectiveDate: ko.observable(),//ko.observable(moment(Date.now()).format(config.format.date)),
    RiskScore: ko.observable(),
    RiskProfileExpiryDate: ko.observable(),//ko.observable(moment(Date.now()).format(config.format.date)),
    OperativeAccount: ko.observable(),
    Transaction_Type: ko.observable(),
    Investment: ko.observable(),
    CustomerDraft: ko.observable(CustomerDraftModel),
    Remarks: ko.observable(""),
    IsDraft: ko.observable(false),
    IsNewCustomer: ko.observable(false),
    IsFNACore: ko.observable(),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]),

    MutualFundForms: ko.observableArray([]),
    MutualFund: ko.observable(),
    MutualFundCode: ko.observable(),
    MutualCurrency: ko.observable(),
    MutualAmount: ko.observable(),
    MutualSelected: ko.observable(),
    MutualPartial: ko.observable(),
    MutualUnitNumber: ko.observable(),
    MutualFundSwitchFrom: ko.observable(),
    MutualFundCodeSwitchFrom: ko.observable(),
    MutualFundSwitchTo: ko.observable(),
    MutualFundCodeSwitchTo: ko.observable(),

    UTJoin: ko.observableArray([]),
    CustomerJoin: ko.observable(CustomerModel),
    SolIDJoin: ko.observable(),
    CustomerRiskEffectiveDateJoin: ko.observable(),//ko.observable(moment(Date.now()).format(config.format.date)),
    RiskScoreJoin: ko.observable(),
    RiskProfileExpiryDateJoin: ko.observable(),//ko.observable(moment(Date.now()).format(config.format.date)),

    MutualFundList: ko.observable(FundListModel),
    MutualFundSwitchFrom: ko.observable(FundListModel),
    MutualFundSwitchTo: ko.observable(FundListModel),
    InvestmentList: ko.observable(InvestmentListModel),
    InvestmentID: ko.observable(),
    SolIDList: ko.observable(SolIDModel),
    AttacmentRemarks: ko.observable()
}

var SolIDModel = {
    SolID: ko.observable(),
    ID: ko.observable(),
    Name: ko.observable(),
    Code: ko.observable()
}

var UTResubmitBranchMakerModel = function () {
    this.Transaction = ko.observable(UTModel);
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
    this.MutualFund = ko.observable([MutualFundModel_]);
    this.JoinAccount = ko.observable([JoinAccountModel]);
};
var CustomerDraftModel = {
    DraftCIF: ko.observable(),
    DraftCustomerName: ko.observable(),
    DraftAccountNumber: ko.observable(AccountModel)
}
var FundListModel = {
    ID: ko.observable(),
    OffOnshoreModel: ko.observable(),
    FundCode: ko.observable(),
    FundName: ko.observable(),
    AccountNo: ko.observable(),
    SavingPlanDate: ko.observable(),
    IsSwitching: ko.observable(),
    IsSaving: ko.observable(),
    IsDeleted: ko.observable(),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
};
var InvestmentListModel = {
    InvestmentID: ko.observable(),
    Investment: ko.observable(),
    CIF: ko.observable(),
    IsUT: ko.observable(),
    IsBond: ko.observable(),
    STNumber: ko.observable(),
    CheckIN: ko.observable(),
    CheckCIF: ko.observable(),
    CheckName: ko.observable(),
    CheckST: ko.observable(),
};
var FNACoreModel = {
    ID: ko.observable(0),
    Name: ko.observable("")
};
var MutualFundModel_ = {
    MutualFundList: ko.observable(FundListModel),
    MutualFundSwitchFrom: ko.observable(FundListModel),
    MutualFundSwitchTo: ko.observable(FundListModel),
    MutualCurrency: ko.observable(CurrencyModel),
    MutualAmount: ko.observable(),
    MutualPartial: ko.observable(),
    MutualSelected: ko.observable(),
    MutualUnitNumber: ko.observable(),
    TransactionID: ko.observable(),
    MutualFundName: ko.observable(),
    ApproverID: ko.observable(),
    IsVerified: ko.observable(false),
    MutualFundTransactionBranchCheckerID: ko.observable(),
    MutualFundTransactionID: ko.observable(),
    Flag: ko.observable()
}

var MutualFundCBOModel_ = {
    MutualFundList: ko.observable(FundListModel),
    MutualFundSwitchFrom: ko.observable(FundListModel),
    MutualFundSwitchTo: ko.observable(FundListModel),
    MutualCurrency: ko.observable(CurrencyModel),
    MutualAmount: ko.observable(),
    MutualPartial: ko.observable(),
    MutualSelected: ko.observable(),
    MutualUnitNumber: ko.observable(),
    TransactionID: ko.observable(),
    MutualFundName: ko.observable(),
    ApproverID: ko.observable(),
    IsVerified: ko.observable(false),
    MutualFundTransactionCBOCheckerID: ko.observable(),
    MutualFundTransactionID: ko.observable(),
    Flag: ko.observable()
}
//dani ut end
var FunctionTypeModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var AccountTypeModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var FDTransactionTypeModel = {
    TransTypeID: ko.observable(),
    TransactionTypeName: ko.observable(),
    ProductID: ko.observable(),
    //ProductName: ko.observable()
}
/*var FDRemarksModel = {
    ID: ko.observable(0),
    Name: ko.observable("")
};*/
var FDModel_ = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    IsNormal: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    ProductType: ko.observable(ProductTypeModel),
    Currency: ko.observable(CurrencyModel),
    DebitCurrency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    Channel: ko.observable(ChannelModel),
    Account: ko.observable(AccountModel),
    CreditAccountNumber: ko.observable(),
    DebitAccNumber: ko.observable(),
    TransactionTypeID: ko.observable(),
    TransactionSubTypeID: ko.observable(),
    FDAccNumber: ko.observable(),
    InterestRate: ko.observable(),
    Tenor: ko.observable(),
    ValueDate: ko.observable(),
    MaturityDate: ko.observable(),
    TransactionRemarks: ko.observable(),
    Remarks: ko.observable(FDRemarksModel),
    TransactionType: ko.observable(FDTransactionTypeModel),
    BankNameAccNumber: ko.observable(),
    AttachmentRemarks: ko.observable(),
    IsCallbackRequired: ko.observable(false),
    IsSignatureVerification: ko.observable(false),
    IsDormantAccount: ko.observable(false),
    IsFreezeAccount: ko.observable(false),
    IsLetterOfIndemnity: ko.observable(false),
    IsDocumentComplete: ko.observable(false),
    CreateDate: ko.observable(new Date),
    Documents: ko.observableArray([DocumentModel])
};
//Andriyanto 2 Desember 2015 Loan
var CheckerLoanModel = {

    IsSignatureVerified: ko.observable(),
    IsDormantAccount: ko.observable(),
    IsFreezeAccount: ko.observable(),
    IsCallbackRequired: ko.observable(),
    IsLOIAvailable: ko.observable(),
    IsStatementLetterCopy: ko.observable(),
    IsDocComplete: ko.observable(),
    IsSyndication: ko.observable(),
}
//End Andriyanto 
var CheckerFDModel = {
    IsSignatureVerified: ko.observable(),
    IsDormantAccount: ko.observable(),
    IsFreezeAccount: ko.observable(),
    IsCallbackRequired: ko.observable(),
    IsLOIAvailable: ko.observable(),
    IsStatementLetterCopy: ko.observable()
}
//Andriyanto 2 Desember 2015 Loan
var TransactionCheckerLoanModel = function () {
    this.Transaction = ko.observable(LoanModel);
    this.Checker = ko.observable(CheckerLoanModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};
//end 
var TransactionCheckerFDModel = function () {
    this.Transaction = ko.observable(FDModel_);
    this.Checker = ko.observable(CheckerFDModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};
//End Dani
var TransactionMakerTMOModel = function () {
    this.Transaction = ko.observable(TMOModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var TransactionCheckerCallbackTMOModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    this.Calback = ko.observable();
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var TransactionCheckerTMOModel = function () {
    this.Transaction = ko.observable(TMOModel);
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var TMOMakerModel = function () {
    this.Transaction = ko.observable(TMOModel);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var TMOCheckerModel = function () {
    this.Transaction = ko.observable(TMOModel);
    this.Timelines = ko.observableArray([TimelineModel]);
    this.Verify = ko.observableArray([VerifyModel]);
};
//Agung
var FDCheckerModel = function () {
    this.Transaction = ko.observable(FDModel);
    this.Timelines = ko.observableArray([TimelineModel]);
    this.Verify = ko.observableArray([VerifyModel]);
};
//End Agung
//Andri Yanto Loan caller 08 Desember 2014
var TransactionCallbackLoanModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    this.Callbacks = ko.observableArray([CallBackModel]);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};
//endAndriyanto
var TransactionCallbackTMOModel = function () {
    this.Transaction = ko.observable(TMOModel);
    this.Callbacks = ko.observableArray([CallBackModel]);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};
//basri 18-11-2015
var TransactionCallbackFDModel = function () {
    this.Transaction = ko.observable(FDModel);
    this.Callbacks = ko.observableArray([CallBackModel]);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};
//end basri 18-11-2015
//basri 19-11-2015
var TransactionCheckerCallbackFDModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    this.Calback = ko.observable();
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var TransactionCallbackCIFModel = function () {
    this.Transaction = ko.observable(TransactionCBO);
    this.Callbacks = ko.observableArray([CallBackModel]);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

//end basri 19-11-2015
//basri 20-11-2015
var FDMakerModel = FDModel_;
var TransactionMakerFDModel = function () {
    this.Transaction = ko.observable(FDMakerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};
//end basri 20-11-2015
//basri 26-11-2015
var StatementModel = {
    ID: ko.observable(0),
    Name: ko.observable("")
};

var NettingPurposeModel = {
    ID: ko.observable(),
    name: ko.observable()
};

var TransactionMakerNettingModel = function () {
    this.Transaction = ko.observable(TransactionNettingModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var TransactionCheckerNettingModel = function () {
    this.Transaction = ko.observable(TransactionNettingModel);
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};


var TransactionNettingModel = {
    AccountNumber: ko.observable(),
    StatementLetter: ko.observable(StatementModel),
    Product: ko.observable(ProductModel),
    TZReference: ko.observable(),
    IDTZReference: ko.observable(),
    MurexNumber: ko.observable(),
    SwapDealNumber: ko.observable(),
    NettingTZReference: ko.observable(),
    IDNettingTZReference: ko.observable(),
    ExpectedDateStatementLetter: ko.observable(),
    ExpectedDateUnderlying: ko.observable(),
    ActualDateStatementLetter: ko.observable(),
    ActualDateUnderlying: ko.observable(),
    RateType: ko.observable(),
    ProductType: ko.observable(ProductTypeModel),
    Rate: ko.observable(),
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Currency: ko.observable(CurrencyModel),
    AmountUSD: ko.observable(0),
    Account: ko.observable(AccountModel),
    Type: ko.observable(20),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]),
    utilizationAmount: ko.observable(0.00),
    underlyings: ko.observableArray([]),
    IsDraft: ko.observable(false),
    OtherUnderlying: ko.observable(),
    bookunderlyingamount: ko.observable(),
    bookunderlyingcurrency: ko.observable(),
    bookunderlyingcode: ko.observable(),
    bookunderlyingdesc: ko.observable(),
    Remarks: ko.observable(),
    IsTMO: ko.observable(true),
    IsNettingTransaction: ko.observable(true),
    NettingPurpose: ko.observable(NettingPurposeModel)
};
//end basri

var CheckerModel = {
    ID: ko.observable(0),
    LLD: ko.observable(LLDModel)
};
//add by fandi for opened task >1
var ClienTaskDataModel = {
    ActivityTitle: ko.observable(),
    ApproverID: ko.observable(),
    ConnectionID: ko.observable(),
    DisplayName: ko.observable(),
    LoginName: ko.observable(),
    TaskID: ko.observable(),
    TransactionID: ko.observable(),
    WorkflowInstanceID: ko.observable()
};
//end

//bangkit 20141119
var CallBackModel = {
    ID: ko.observable(0),
    ApproverID: ko.observable(""),
    Contact: ko.observable(ContactModel),
    Time: ko.observable(new Date),
    Remark: ko.observable("")
};
//Chandra 2015.02.24
var DocumentPurposeModel = {
    ID: ko.observable(0),
    Name: ko.observable(""),
    Description: ko.observable("")
}

var VerifyModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    IsVerified: ko.observable()
};

var TimelineModel = {
    ApproverID: ko.observable(),
    Activity: ko.observable(),
    Time: ko.observable(),
    Outcome: ko.observable(),
    UserOrGroup: ko.observable(),
    IsGroup: ko.observable(),
    DisplayName: ko.observable(),
    Comment: ko.observable()
};

var TransactionDealModel = function () {
    this.Transaction = ko.observable(TransactionDealDetailModel);
    this.Timelines = ko.observableArray([TimelineModel]);
    this.Verify = ko.observableArray([VerifyModel]);
};

var TransactionMakerModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};
var TransactionMakerDetailModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    this.Payment = ko.observable(PaymentModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};
var ScheduledSettlementLoanModel = {
    SOLID: ko.observable(),
    SchemeCode: ko.observable(),
    CIF: ko.observable(),
    Customer: ko.observable(CustomerModel),
    LoanContractNo: ko.observable(),
    DueDate: ko.observable(),
    Currency: ko.observable(CurrencyModel),
    AmountDue: ko.observable(),
    LimitID: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    RM: ko.observable(),
    CSO: ko.observable(),
    IsSBLCSecured: ko.observable(),
    IsHeavyEquipment: ko.observable(),
    MaintenanceTypeID: ko.observable(),
    AmountRollover: ko.observable(),
    NextPrincipalDate: ko.observable(),
    NextInterestDate: ko.observable(),
    PeggingReviewDate: ko.observable(),
    ApprovedMarginAsPerCM: ko.observable(),
    InterestRateCodeID: ko.observable(),
    BaseRate: ko.observable(),
    AllInLP: ko.observable(),
    AllInFTPRate: ko.observable(),
    AccountPreferencial: ko.observable(),
    SpecialFTP: ko.observable(),
    SpecialRateMargin: ko.observable(),
    AllInrate: ko.observable(),
    ApprovalDOA: ko.observable(),
    ApprovedBy: ko.observable(),
    Remarks: ko.observable(),
    IsAdhoc: ko.observable(),
    CSOName: ko.observable(),
    Documents: ko.observableArray([DocumentModel])

};
var LoanScheduledSettlementModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    IsChainTopUrgent: ko.observable(false),
    IsNormal: ko.observable(false),
    Product: ko.observable(ProductModel),
    Channel: ko.observable(ChannelModel),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel])
};
//Andri UT 21 Nov 2015
var MutualFundModel = {
    Amount: ko.observable(),
    Currency: ko.observable(),
    ApproverID: ko.observable(),
    CreateBy: ko.observable(),
    CreateDate: ko.observable(),
    VerifiedMutualFund: ko.observable(false),
    MutualFundTransactionCBOCheckerID: ko.observable(),
    MutualFundTransactionID: ko.observable(),
    MutualFundName: ko.observable(),
    FromCodeswitchmutualfund: ko.observable(),
    FromStrswitchmutualFund: ko.observable(),
    TOCodeswitchmutualfund: ko.observable(),
    TOStrswitchmutualFund: ko.observable(),
    TransactionID: ko.observable(),
    partial: ko.observable(),
    NumberofUnit: ko.observable(),
    TOCodeswitchmutualfund: ko.observable(),
    TOStrswitchmutualFund: ko.observable(),
    UpdateBy: ko.observable(),
    UpdateDate: ko.observable()
}
var JoinAccountModel = {
    ApproverID: ko.observable(),
    CreateBy: ko.observable(),
    CreateDate: ko.observable(),
    IsVerified: ko.observable(false),
    JoinAccountBranchCheckerID: ko.observable(),
    JoinAccountID: ko.observable(),
    TransactionID: ko.observable(),
    UpdateBy: ko.observable(),
    UpdateDate: ko.observable(),
    CustomerName: ko.observable(),
    CIF: ko.observable(),
    CriskEfDate: ko.observable(),
    RiskScore: ko.observable(),
    RiskProfexpDate: ko.observable()

}
//End
//Andryanto 16 November 2015
var TransactionTypeModel = {
    TransTypeID: ko.observable(),
    TransactionTypeName: ko.observable()
}
var UTBranchCheckerDetailModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    IsChainTopUrgent: ko.observable(false),
    IsNormal: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    Currency: ko.observable(CurrencyModel),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]), //DocumentModel    
    CreateDate: ko.observable(),
    CreateBy: ko.observable(),
    UpdateBy: ko.observable(),
    strfunctionType: ko.observable(),
    strAccountType: ko.observable(),
    TransactionType: ko.observable(TransactionTypeModel),
    FNACore: ko.observableArray([]),
    FunctionType: ko.observable([]),
    AccountType: ko.observable([]),
    DBNumber: ko.observable(), // add by henggar db number
    UTNumber: ko.observable(), // add by henggar ut number
}
//endAndriyanto
//add fandi //kelinci
var TransactionLoanModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    SolIDList: ko.observable(SolIDModel),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    IsChainTopUrgent: ko.observable(false),
    IsNormal: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    Channel: ko.observable(ChannelModel),
    ValueDate: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    IsSignatureVerified: ko.observable(false),
    IsDormantAccount: ko.observable(false),
    IsFreezeAccount: ko.observable(false),
    Others: ko.observable(),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]), //DocumentModel
    FundingMemo: ko.observable(FundingModel)
};
var UTBranchCheckerModel = function () {
    this.Transaction = ko.observable(UTBranchCheckerDetailModel);
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
    this.MutualFund = ko.observable([MutualFundModel]);
    this.JoinAccount = ko.observable([JoinAccountModel]);
};
var TransactionLoanCheckerUnsettlementModel = function () {
    this.Transaction = ko.observable(LoanCheckerUnsettlement);
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};
//afif
var FNACoreModel = {
    FNACoreID: ko.observable(),
    FNACoreName: ko.observable()
};
var FunctionTypeModel = {
    FunctionTypeID: ko.observable(),
    FunctionTypeName: ko.observable()
};
var UTCBOUTMaker = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    IsChainTopUrgent: ko.observable(false),
    IsNormal: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    Currency: ko.observable(CurrencyModel),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]), //DocumentModel    
    CreateDate: ko.observable(),
    CreateBy: ko.observable(),
    UpdateBy: ko.observable(),
    ParameterSystemUT: ko.observable(ParameterUT)

}
var ChangeRMModel = {
    ChangeRMTransactionID: ko.observable(),
    TransactionID: ko.observable(),
    CIF: ko.observable(),
    CustomerName: ko.observable(),
    ChangeSegmentFrom: ko.observable(),
    ChangeSegmentTo: ko.observable(),
    ChangeFromBranchCode: ko.observable(),
    ChangeFromRMCode: ko.observable(),
    ChangeFromBU: ko.observable(),
    ChangeToBranchCode: ko.observable(),
    ChangeToRMCode: ko.observable(),
    ChangeToBU: ko.observable(),
    Account: ko.observable(),
    PCCode: ko.observable(),
    EATPB: ko.observable(),
    Remarks: ko.observable(),
};
var DispatchModeTypeModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var BrachRiskRatingModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var CIFSUMBERDANAModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var CIFASSETBERSIHModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var CIFPENDPATANBULANANModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var CIFPENGHASILANTAMBAHANModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var CIFPEKERJAANModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var CIFTUJUANPEMBUKAANREKENINGModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var CIFPERKIRAANDANAMASUKModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var CIFPERKIRAANDANAKELUARModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var CIFPERKIRAANTRANSAKSIKELUARModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var CIFJENISIDENTITASModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var ModificationData = [{ ID: 1, Name: 'Add' }, { ID: 2, Name: 'Edit' }, { ID: 3, Name: 'Delete' }]
var MaritalStatusData = [{ ID: 1, Name: 'Belum Menikah' }, { ID: 2, Name: 'Duda / Janda' }, { ID: 3, Name: 'Sudah Menikah' }]
var DispatchModeData = [{ ID: 1, Name: 'BSPL Delivery and Email' }, { ID: 2, Name: 'Collect by Person' }, { ID: 3, Name: 'Email' }, { ID: 4, Name: 'No Dispatch' }, { ID: 5, Name: 'Post' }, { ID: 6, Name: 'SSPL Delivery' }]
var RiskRatingResultData = [{ ID: 0, Name: 'Low Risk' }, { ID: 1, Name: 'Medium Risk' }, { ID: 2, Name: 'High Risk' }]
var RetailCIFCBO = {//retail
    RetailCIFCBOID: ko.observable(),
    TransactionID: ko.observable(),
    DispatchModeTypeID: ko.observable(),
    AccountNumbe: ko.observable(),
    RequestTypeID: ko.observable(),
    MaintenanceTypeID: ko.observable(),
    IsNameMaintenance: ko.observable(false),
    IsIdentityTypeMaintenance: ko.observable(false),
    IsNPWPMaintenance: ko.observable(false),
    IsMaritalStatusMaintenance: ko.observable(false),
    IsCorrespondenceMaintenance: ko.observable(false),
    IsIdentityAddressMaintenance: ko.observable(false),
    IsOfficeAddressMaintenance: ko.observable(false),
    IsCorrespondenseAddressMaintenance: ko.observable(false),
    IsPhoneFaxEmailMaintenance: ko.observable(false),
    IsNationalityMaintenance: ko.observable(false),
    IsFundSourceMaintenance: ko.observable(false),
    IsNetAssetMaintenance: ko.observable(false),
    IsMonthlyIncomeMaintenance: ko.observable(false),
    IsJobMaintenance: ko.observable(false),
    IsAccountPurposeMaintenance: ko.observable(false),//add
    Name: ko.observable(),
    IdentityTypeID: ko.observable(),
    IdentityNumber: ko.observable(),
    IdentityStartDate: ko.observable(),
    IdentityEndDate: ko.observable(),
    IdentityAddress: ko.observable(),
    IdentityKelurahan: ko.observable(),
    IdentityKecamatan: ko.observable(),
    IdentityCity: ko.observable(),
    IdentityProvince: ko.observable(),
    IdentityCountry: ko.observable(),
    IdentityPostalCode: ko.observable(),
    NPWPNumber: ko.observable(),
    IsNPWPReceived: ko.observable(false),
    MaritalStatusID: ko.observable(),
    SpouseName: ko.observable(),
    IsCorrespondenseToEmail: ko.observable(false),
    CorrespondenseAddress: ko.observable(),
    CorrespondenseKelurahan: ko.observable(),
    CorrespondenseKecamatan: ko.observable(),
    CorrespondenseCity: ko.observable(),
    CorrespondenseProvince: ko.observable(),
    CorrespondenseCountry: ko.observable(),
    CorrespondensePostalCode: ko.observable(),
    CellPhoneMethodID: ko.observable(),
    CellPhone: ko.observable(),
    UpdatedCellPhone: ko.observable(),
    HomePhoneMethodID: ko.observable(),
    HomePhone: ko.observable(),
    UpdatedHomePhone: ko.observable(),
    OfficePhoneMethodID: ko.observable(),
    OfficePhone: ko.observable(),
    UpdatedOfficePhone: ko.observable(),
    FaxMethodID: ko.observable(),
    Fax: ko.observable(),
    UpdatedFax: ko.observable(),
    EmailAddress: ko.observable(),
    OfficeAddress: ko.observable(),
    OfficeKelurahan: ko.observable(),
    OfficeKecamatan: ko.observable(),
    OfficeCity: ko.observable(),
    OfficeProvince: ko.observable(),
    OfficeCountry: ko.observable(),
    OfficePostalCode: ko.observable(),
    Nationality: ko.observable(),
    FundSource: ko.observable(),
    UBOName: ko.observable(),
    UBOIdentityType: ko.observable(),
    UBOPhone: ko.observable(),
    UBOJob: ko.observable(),
    NetAsset: ko.observable(),
    MonthlyIncome: ko.observable(),
    MonthlyExtraIncome: ko.observable(),
    Job: ko.observable(),
    CompanyName: ko.observable(),
    Position: ko.observable(),
    WorkPeriod: ko.observable(),
    IndustryType: ko.observable(),
    AccountPurpose: ko.observable(),
    IncomeForecast: ko.observable(),
    OutcomeForecast: ko.observable(),
    TransactionForecast: ko.observable(),
    SubType: ko.observable(),
    IdentityType: ko.observable(),
    MaritalStatus: ko.observable(),
    FundSources: ko.observable(),
    NetAssets: ko.observable(),
    MonthlyIncomes: ko.observable(),
    MonthlyExtraIncomes: ko.observable(),
    Jobs: ko.observable(),
    AccountPurposes: ko.observable(),
    IncomeForecasts: ko.observable(),
    OutcomeForecasts: ko.observable(),
    TransactionForecasts: ko.observable(),
    SubTypes: ko.observable(),
    DispatchModeType: ko.observable(),
    AtmNumber: ko.observable(),
    RiskRatingReportDate: ko.observable(),
    RiskRatingNextReviewDate: ko.observable(),
    RiskRatingResultID: ko.observable(),
    Remarks: ko.observable(),
    MaintenanceTypes: ko.observable(),
    RequestTypes: ko.observable(),
    RiskRatingResult: ko.observable(),
    CreateDate: ko.observable(),
    CreateBy: ko.observable(),
    UpdateDate: ko.observable(),
    UpdateBy: ko.observable(),
    //dani 8-4-2016
    SumberDanaLainnya: ko.observable(),
    PekerjaanProfesional: ko.observable(),
    PekerjaanLainnya: ko.observable(),
    TujuanBukaRekeningLainnya: ko.observable(),
    IsTujuanBukaRekeningLainnya: ko.observable(false),
    IsSumberDanaLainnya: ko.observable(false),
    IsPekerjaanProfesional: ko.observable(false),
    IsPekerjaanLainnya: ko.observable(false)
    //end dani
};

var TransactionSubType = {
    ID: ko.observable(),
    Name: ko.observable()
};

var TransactionCBOModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    IsChainTopUrgent: ko.observable(false),
    IsNormal: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    TransactionType: ko.observable(TransactionTypeModel),
    MaintenanceType: ko.observable(MaintenanceTypeModel),
    DispatchModeType: ko.observable(DispatchModeData),
    Currency: ko.observable(CurrencyModel),
    StaffID: ko.observable(),
    ATMNumber: ko.observable(),
    TransactionSubType: ko.observable(TransactionSubType),
    CIFSUMBERDANA: ko.observable(CIFSUMBERDANAModel),
    CIFASSETBERSIH: ko.observable(CIFASSETBERSIHModel),
    CIFPENDPATANBULANAN: ko.observable(CIFPENDPATANBULANANModel),
    CIFPENGHASILANTAMBAHAN: ko.observable(CIFPENGHASILANTAMBAHANModel),
    CIFPEKERJAAN: ko.observable(CIFPEKERJAANModel),
    CIFTUJUANPEMBUKAANREKENING: ko.observable(CIFTUJUANPEMBUKAANREKENINGModel),
    CIFPERKIRAANDANAMASUK: ko.observable(CIFPERKIRAANDANAMASUKModel),
    CIFPERKIRAANDANAKELUAR: ko.observable(CIFPERKIRAANDANAKELUARModel),
    CIFPERKIRAANTRANSAKSIKELUAR: ko.observable(CIFPERKIRAANTRANSAKSIKELUARModel),
    CIFJENISIDENTITAS: ko.observable(CIFJENISIDENTITASModel),
    StaffTagging: ko.observable(StaffTaggingModel),
    ValueDate: ko.observable(),
    IsSignatureVerified: ko.observable(false),
    IsDormantAccount: ko.observable(false),
    IsFreezeAccount: ko.observable(false),
    Others: ko.observable(),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]),
    //RetailCIFCBO: ko.observable(CustomerModel),
    RetailCIFCBO: ko.observable(RetailCIFCBO),
    ChangeRMModel: ko.observableArray([ChangeRMModel]),
    AddJoinTableCustomerCIF: ko.observableArray([CustomerCIFModel]),
    AddJoinTableFFDAccountCIF: ko.observableArray([FFDAccountCIFModel]),
    AddJoinTableAccountCIF: ko.observableArray([AccountCIFModel]),
    AddJoinTableDormantCIF: ko.observableArray([DormantAccountCIFModel]),
    AddJoinTableFreezeUnfreezeCIF: ko.observableArray([FreezeUnfreezeAccountCIFModel]),
    IsCommingPersonally: ko.observable(false),
    IsPersonallyKnown: ko.observable(false)
};

var CustomerCIFModel = {
    CIF: ko.observable(),
    Name: ko.observable()
};

var FFDAccountCIFModel = {
    AccountNumber: ko.observable(),
    IsAddFFDAccount: ko.observable(false)
};

var AccountCIFModel = {
    AccountNumber: ko.observable(),
    Currency: ko.observable()
};

var DormantAccountCIFModel = {
    AccountNumber: ko.observable(),
    Currency: ko.observable(false)
};

var FreezeUnfreezeAccountCIFModel = {
    AccountNumber: ko.observable(''),
    IsFreezeAccount: ko.observable(false)
};

var TransactionCBO = function () {
    this.Transaction = ko.observable(TransactionCBOModel);
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};
//Andri UT 21 Nov 2015
var MutualFundModel = {
    Amount: ko.observable(),
    Currency: ko.observable(),
    ApproverID: ko.observable(),
    CreateBy: ko.observable(),
    CreateDate: ko.observable(),
    IsVerified: ko.observable(false),
    MutualFundTransactionCBOCheckerID: ko.observable(),
    MutualFundTransactionID: ko.observable(),
    MutualFundName: ko.observable(),
    TransactionID: ko.observable(),
    UpdateBy: ko.observable(),
    UpdateDate: ko.observable()
}
var JoinAccountModel = {
    ApproverID: ko.observable(),
    CreateBy: ko.observable(),
    CreateDate: ko.observable(),
    IsVerified: ko.observable(false),
    JoinAccountBranchCheckerID: ko.observable(),
    JoinAccountID: ko.observable(),
    TransactionID: ko.observable(),
    UpdateBy: ko.observable(),
    UpdateDate: ko.observable(),
    CustomerName: ko.observable(),
    CIF: ko.observable(),
    CriskEfDate: ko.observable(),
    RiskScore: ko.observable(),
    RiskProfexpDate: ko.observable(),

}
//End
//afif
var AccountTypeModel = {
    AccountTypeID: ko.observable(),
    AccountTypeName: ko.observable()
};

var Transaction_TypeModel = {
    TransTypeID: ko.observable(),
    TransactionTypeName: ko.observable()
};
var ParameterUT = {
    FNACore: ko.observable(FNACoreModel),
    FunctionType: ko.observable(FunctionTypeModel),
    AccountType: ko.observable(AccountTypeModel),
    TransactionType: ko.observable(Transaction_TypeModel)
}

var UTCBOAccountMaker = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    IsChainTopUrgent: ko.observable(false),
    IsNormal: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    IsNewCustomer: ko.observable(false),
    Product: ko.observable(ProductModel),
    ProductType: ko.observable(ProductTypeModel),
    DebitCurrency: ko.observable(CurrencyModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    Channel: ko.observable(ChannelModel),
    Account: ko.observable(AccountModel),
    ApplicationDate: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]),
    SOLID: ko.observable(),
    RiskScore: ko.observable(),
    CustomerRiskEffectiveDate: ko.observable(),
    RiskProfileExpiryDate: ko.observable(),
    Remark: ko.observable(),
    InvestmentID: ko.observable(),
    ParameterSystemUT: ko.observable(ParameterUT)
};

var UTCBOAccountMakerModel = function () {
    this.Transaction = ko.observable(UTCBOAccountMaker);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var UTCBOUTMakerModel = function () {
    this.Transaction = ko.observable(UTCBOUTMaker);
    this.Timelines = ko.observableArray([TimelineModel]);
}

//afif

var TransactionLoanCSOModel = function () {
    this.Transaction = ko.observable(TransactionLoanModel);
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};
var TransactionCheckerModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
    //add by fandi for opened task >1
    this.ClienTaskData = ko.observable(ClienTaskDataModel);
    //end

};

//add by fandi transaction history
var TransactionHistoryModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    this.TransactionMaker = ko.observableArray([GetTransactionMakerModel]);
    //add
    this.Payment = ko.observable(TransactionModel);
};

var TransactionHistoryPaymentModel = function () {
    this.Payment = ko.observable(TransactionModel);
    this.TransactionMaker = ko.observableArray([GetTransactionMakerModel]);
};
//end 


var TransactionCheckerCallbackModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    this.Calback = ko.observable();
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

//bangkit 20141119
var TransactionCallbackModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    //this.Checker = ko.observable(CheckerModel);
    this.Callbacks = ko.observableArray([CallBackModel]);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var TransactionContactModel = function () {
    this.Contacts = ko.observableArray([ContactModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var PaymentMakerModel = function () {
    this.Payment = ko.observable(PaymentModel);
    this.Timelines = ko.observableArray([TimelineModel]);
    this.TransactionSKNBulk = ko.observableArray([TransactionSKNBulkModel]); //add aridya 20161025 for skn bulk ~OFFLINE~
    this.TransactionSKNBulkOriginal = ko.observableArray([TransactionSKNBulkModel]); //add aridya 20161025 for skn bulk ~OFFLINE~
};

var PaymentCheckerModel = function () {
    this.Payment = ko.observable(PaymentModel);
    this.Timelines = ko.observableArray([TimelineModel]);
    this.Verify = ko.observableArray([VerifyModel]);
    this.TransactionSKNBulk = ko.observableArray([TransactionSKNBulkModel]); //add aridya 20161025 for skn bulk ~OFFLINE~
};
var LoanMakerInterestMaintenanceModel = function () {
    this.Transaction = ko.observable(LoanScheduledSettlementModel);
    this.LoanCheckerIM = ko.observable(ScheduledSettlementLoanModel);
    this.Timelines = ko.observableArray([TimelineModel]);
}
var LoanMakerModel = function () {
    this.LoanDisbursement = ko.observable(FundingMemoModel);
    this.LoanRollover = ko.observable([RolloverModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};
//Andriyanto 19 Oktober 2015	
var LoanCheckerModel = function () {
    this.LoanDisbursement = ko.observable(FundingMemoModel);
    this.LoanRollover = ko.observable([RolloverModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};
//End
//basri get Excel
var DataCheckerRolloverModel = function () {
    this.Transaction = ko.observable(LoanScheduledSettlementModel);
    this.CheckerRollover = ko.observable(ScheduledSettlementLoanModel);
    this.Timelines = ko.observableArray([TimelineModel]);
}
var LoanScheduledRolloverCheckerModel = function () {
    this.Transaction = ko.observable(LoanScheduledSettlementModel);
    this.CheckerRollover = ko.observable(ScheduledSettlementLoanModel);
};
var TransactionCheckerLoanScheduledSettlementModel = function () {
    this.Transaction = ko.observable(LoanScheduledSettlementModel);
    this.LoanCheckerMakerRollover = ko.observable(ScheduledSettlementLoanModel);
    this.Timelines = ko.observableArray([TimelineModel]);
};
//popup excel
var TransactionCheckerLoanScheduledSettlementDetailsModel = function () {
    this.Transaction = ko.observable(LoanScheduledSettlementModel);
    this.LoanCheckerRollover = ko.observable(ScheduledSettlementLoanModel);
    this.ScheduledSettlementLoan = ko.observable(ScheduledSettlementLoanModel);
};
//end

//end basri

var Documents = {
    Documents: self.Documents,
    DocumentPath: self.DocumentPath_a,
    Type: self.DocumentType_a,
    Purpose: self.DocumentPurpose_a
}

var CustomerUnderlyingMappingModel = {
    ID: ko.observable(0),
    UnderlyingID: ko.observable(0),
    UnderlyingFileID: ko.observable(0),
    AttachmentNo: ko.observable("")
}

var ProductTypeModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    IsFlowValas: ko.observable(),
    Description: ko.observable()
};

var SelectUtillizeModel = function (id, enable, uSDAmount, statementLetter) {
    var self = this;
    self.ID = ko.observable(id);
    self.Enable = ko.observable(enable);
    self.USDAmount = ko.observable(uSDAmount);
    self.StatementLetter = ko.observable(statementLetter);
};

var TransactionDealDetailModel = {
    ValueDate: ko.observable(),
    AccountNumber: ko.observable(),
    StatementLetter: ko.observable(StatementLetterModel),
    ProductType: ko.observable(ProductTypeModel),
    TZNumber: ko.observable(),
    RateTypeID: ko.observable(),
    Rate: ko.observable(),
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Currency: ko.observable(CurrencyModel),
    DebitCurrency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    AmountUSD: ko.observable(),
    Account: ko.observable(AccountModel),
    ValueDate: ko.observable(),
    Type: ko.observable(20),
    CreateDate: ko.observable(new Date),
    //CreateBy: ko.observable(spUser.DisplayName),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]), //DocumentModel
    DetailCompliance: ko.observable(),
    BeneName: ko.observable(),
    BeneAccNumber: ko.observable(),
    TZRef: ko.observable(),
    utilizationAmount: ko.observable(0.00),
    underlyings: ko.observableArray([]),
    TZReference: ko.observable(),
    IsBookDeal: ko.observable(false),
    bookunderlyingamount: ko.observable(),
    bookunderlyingcurrency: ko.observable(),
    bookunderlyingcode: ko.observable(),
    CreateBy: ko.observable(),
    AccountCurrencyCode: ko.observable(),
    IsFxTransaction: ko.observable(false),
    OtherUnderlying: ko.observable(),
    bookunderlyingdoccode: ko.observable(),
    IsJointAccount: ko.observable(false)
    //TZRef: ko.observable()
    //UnderlyingGridProperties : self.UnderlyingGridProperties(new GridPropertiesModel(GetDataUnderlying))

};

var statusValueModel = function (value, status) {
    var self = this;
    self.Value = ko.observable(value);
    self.Status = ko.observable(status);
}

// Original Value PPU Maker -- chandra a

//var OriginalValueModel = function (amountOri, amountUSDOri, rateOri, beneNameOri, banksOri, beneAccNumberOri, swiftCodeOri, bankChargesOri, agentChargesOri, isCitizenOri, isResidentOri, titleOri) {
var OriginalValueModel = function (amountOri, amountUSDOri, rateOri, beneNameOri, banksOri, beneAccNumberOri, swiftCodeOri, bankChargesOri, agentChargesOri, isCitizenOri, isResidentOri, titleOri, branchOri, cityOri) {
    var self = this;
    self.AmountOri = ko.observable(amountOri);
    self.AmountUSDOri = ko.observable(amountUSDOri);
    self.RateOri = ko.observable(rateOri);
    self.BeneNameOri = ko.observable(beneNameOri);
    self.BanksOri = ko.observable(banksOri);
    self.BeneAccNumberOri = ko.observable(beneAccNumberOri);
    //self.AccountOri = ko.observable(accountOri);
    self.SwiftCodeOri = ko.observable(swiftCodeOri);
    self.BankChargesOri = ko.observable(bankChargesOri);
    self.AgentChargesOri = ko.observable(agentChargesOri);
    self.IsCitizenOri = ko.observable(isCitizenOri);
    self.IsResidentOri = ko.observable(isResidentOri);
    self.PaymentConfirmationTitle = ko.observable(titleOri); //Rizki 2016-01-21
    self.BranchOri = ko.observable(branchOri); //aridya 20160923
    self.CityOri = ko.observable(cityOri); //aridya 20160923
}

//dani ut start
var UTResubmitBranchMakerModel = function () {
    this.Transaction = ko.observable(UTModel);
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
    this.MutualFund = ko.observableArray([MutualFundModel_]);
    this.JoinAccount = ko.observableArray([JoinAccountModel_]);
};
//dani ut maker start
var UTResubmitCBOUTMakerModel = function () {
    this.Transaction = ko.observable(UTModel);
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
    this.MutualFund = ko.observableArray([MutualFundCBOModel_]);
    this.JoinAccount = ko.observableArray([JoinAccountCBOModel_]);
}
//dani ut maker end
//dani ut
var ParameterSystemCIFModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var ParameterCIF = {
    ddlCellPhoneMethodIDs: ko.observableArray([ParameterSystemCIFModel]),
    ddlHomePhoneMethodIDs: ko.observableArray([ParameterSystemCIFModel]),
    ddlOfficePhoneMethodIDs: ko.observableArray([ParameterSystemCIFModel]),
    ddlFaxMethodIDs: ko.observableArray([ParameterSystemCIFModel]),
    ddlRiskRatingResults: ko.observableArray([ParameterSystemCIFModel]),
    ddlDispatchModeTypes: ko.observableArray([ParameterSystemCIFModel]),
    ddlMaritalStatus: ko.observableArray([ParameterSystemCIFModel]),
    ddlAccountNumber: ko.observableArray([]),
    ddlCallbackTimeReason: ko.observableArray([ParameterSystemCIFModel])
};

//udin
var BankBranchModel = {
    CityCode: ko.observable(),
    CityDescription: ko.observable(),
    CityID: ko.observable(),
    Code: ko.observable(),
    ID: ko.observable(),
    Name: ko.observable(),
};
//end

//Tambahan CIf
var StaffTaggingModel = {
    ID: ko.observable(),
    Name: ko.observable(),
};

//aridya add 20160921
var LimitProductModel = {
    TransactionLimitProductID: ko.observable(),
    MinAmount: ko.observable(),
    MaxAmount: ko.observable(),
    Unlimited: ko.observable(),
    Product: ko.observable(ProductModel),
    Currency: ko.observable(CurrencyModel)
};
//end add

//aridya add 20161017 for skn bulk transactions ~OFFLINE~
var TransactionSKNBulkModel = {
    ID: ko.observable(),
    AccountNumber: ko.observable(),
    CustomerName: ko.observable(),
    ValueDate: ko.observable(),
    PaymentAmount: ko.observable(),
    BeneficiaryName: ko.observable(),
    BeneficiaryAccountNumber: ko.observable(),
    BankCode: ko.observable(),
    BranchCode: ko.observable(),
    Citizen: ko.observable(),
    Resident: ko.observable(),
    PaymentDetails: ko.observable(),
    Charges: ko.observable(),
    Type: ko.observable(),
    ParentTransactionID: ko.observable(),
    ParentWorkflowInstanceID: ko.observable(),
    IsChanged: ko.observable()
};
//end add aridya
