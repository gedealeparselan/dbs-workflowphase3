﻿using DBS.Entity;
using DBS.WebAPI.Models;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Transactions;
using System.Web;

namespace DBS.WebAPI.Models
{
    #region Property
    public class TransactionNettingMakerDetailModel
    {
        public TransactionNettingDetailModel Transaction { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public IList<VerifyDealModel> Verify { get; set; }
    }
    public class TransactionNettingCheckerDetailModel
    {
        public TransactionNettingDetailModel Transaction { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public IList<VerifyDealModel> Verify { get; set; }
    }
    public class TransactionNettingCheckerDataModel
    {
        public long ID { get; set; }


        public string Others { get; set; }

        /// <summary>
        /// Signature Verification
        /// </summary>
        public bool IsSignatureVerified { get; set; }


        /// Dormant Account
        /// </summary>
        public bool IsDormantAccount { get; set; }

        /// <summary>
        /// Freeze Account
        /// </summary>
        public bool IsFreezeAccount { get; set; }

        public bool IsCallbackRequired { get; set; }



        public bool IsLOIAvailable { get; set; }



        public bool IsStatementLetterCopy { get; set; }

    }
    public class TransactionNettingDocumentModel
    {
        /// <summary>
        /// Transaction Document ID
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Document Type details
        /// </summary>
        public DocumentTypeModel Type { get; set; }

        /// <summary>
        /// Document Type details
        /// </summary>
        public DocumentPurposeModel Purpose { get; set; }

        /// <summary>
        /// Document filename
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// DocumentPath
        /// </summary>
        public string DocumentPath { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

    }
    public class TransactionNettingDetailModel
    {
        /// <summary>
        /// CIF
        /// </summary>
        public string CIF { get; set; }
        /// <summary>
        /// StatementLetter
        /// </summary>
        public StatementLetterModel StatementLetter { get; set; }

        /// <summary>
        /// TZReference
        /// </summary>
        public string TZReference { get; set; }

        /// <summary>
        /// Netting TZReference
        /// </summary>
        public NettingTZReferenceModel NettingTZReference { get; set; }

        /// <summary>
        /// Product
        /// </summary>
        public ProductModel Product { get; set; }

        /// <summary>
        /// Product Type
        /// </summary>
        public ProductTypeModel ProductType { get; set; }

        /// <summary>
        /// RateType
        /// </summary>
        public RateTypeModel RateType { get; set; }

        /// <summary>
        /// Rate
        /// </summary>
        public string Rate { get; set; }

        /// <summary>
        /// Workflow Instance ID
        /// </summary>
        public Guid? WorkflowInstanceID { get; set; }

        /// <summary>
        /// Transaction Deal ID.
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Application ID
        /// </summary>
        public string ApplicationID { get; set; }

        /// <summary>
        /// Customer Data Details
        /// </summary>
        public CustomerModel Customer { get; set; }

        /// <summary>
        /// Currency Details
        /// </summary>
        public CurrencyModel Currency { get; set; }

        /// <summary>
        /// Transaction Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Eqv.USD
        /// </summary>
        public decimal AmountUSD { get; set; }

        /// <summary>
        /// Debit Acc Number
        /// </summary>
        public CustomerAccountModel Account { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        public decimal? Type { get; set; }

        /// <summary>
        /// Create Date
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// Actual Submission Date Statement Letter
        /// </summary>
        public DateTime? ActualDateStatementLetter { get; set; }

        /// <summary>
        /// Actual Submission Date Underlying
        /// </summary>
        public DateTime? ActualDateUnderlying { get; set; }

        /// <summary>
        /// Actual Submission Date Statement Letter
        /// </summary>
        public DateTime? ExpectedDateStatementLetter { get; set; }


        /// <summary>
        /// Expected Submission Date Underlying
        /// </summary>
        public DateTime? ExpectedDateUnderlying { get; set; }

        /// <summary>
        /// List Transaction Document Details
        /// </summary>
        public IList<TransactionNettingDocumentModel> Documents { get; set; }

        /// <summary>
        /// TZ Number
        /// </summary>
        public string TZNumber { get; set; }

        /// <summary>
        /// Is Draft
        /// </summary>
        public bool IsDraft { get; set; }

        public bool? IsNettingTransaction { get; set; }
        public bool? IsTMO { get; set; }
        public long? NettingTransactionID { get; set; }
        public NettingPurposeModel NettingPurpose { get; set; }

        /// <summary>
        /// Remarks
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// Underlyings
        /// </summary> 
        public IList<TransDetailUnderlyingModel> Underlyings { get; set; }

        /// <summary>
        /// Transaction Create By
        /// </summary> 
        public string CreateBy { get; set; }

        /// <summary>
        /// is book deal not new transaction
        /// </summary> 
        public bool IsBookDeal { get; set; }

        /// <summary>
        /// book underlying amount
        /// </summary>
        public decimal? bookunderlyingamount { get; set; }

        /// <summary>
        /// book underlying currency id
        /// </summary>
        public int? bookunderlyingcurrency { get; set; }

        /// <summary>
        /// book underlying id
        /// </summary>
        public long? bookunderlyingcode { get; set; }
        public string bookunderlyingdoccode { get; set; }
        public string bookunderlyingcurrencydesc { get; set; }
        public string bookunderlyingdesc { get; set; }
        public string AccountCurrencyCode { get; set; }
        public int RateTypeID { get; set; }
        public decimal? utilizationAmount { get; set; }
        public bool? IsFxTransaction { get; set; }
        public string OtherUnderlying { get; set; }
        public UnderlyingDocModel NettingUnderlying { get; set; }

        public bool? IsOtherAccountNumber { get; set; }
        public string OtherAccountNumber { get; set; }
        public CurrencyModel DebitCurrency { get; set; }
        public string TransactionStatus { get; set; }
        public string TzStatus { get; set; }
        public IList<ReviseDealDocumentModel> ReviseDealDocuments { get; set; }
        public string MurexNumber { get; set; }
        public string SwapDealNumber { get; set; }
        public bool? IsJointAccount { get; set; }
        public bool? IsResident { get; set; }
        public int? ProductTypeID { get; set; }

    }
    public class TransactionDealTMORow : TransactionNettingDetailModel
    {
        public int RowID { get; set; }

    }
    #endregion

    #region Filter
    public class TransactionDealTMOFilter : Filter { }

    #endregion

    #region Interface
    public interface IWorkflowDealTMORepository : IDisposable
    {
        bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineModel> timelines, ref string message);
        bool GetTransactionDetails(Guid instanceID, ref TransactionNettingDetailModel transaction, ref string message);
        bool GetTransactionCheckerDetails(Guid workflowInstanceID, long approverID, ref TransactionNettingCheckerDetailModel output, ref string message);
        bool AddTransactionChecker(Guid workflowInstanceID, long approverID, TransactionNettingCheckerDetailModel data, ref string message);
        bool UpdateTransactionMaker(Guid workflowInstanceID, long approverID, TransactionNettingMakerDetailModel data, ref string message);
        bool UpdateTransactionDealRevise(Guid workflowInstanceID, TransactionNettingCheckerDetailModel transactionChecker, ref string message);
        bool GetTransactionDealDetails(long id, ref TransactionDealDetailModel transaction, ref string message);

        #region IPE
        bool UpdateTransactionMakerIPE(Guid workflowInstanceID, long approverID, TransactionNettingMakerDetailModel data, ref string message);
        bool AddTransactionCheckerIPE(Guid workflowInstanceID, long approverID, TransactionNettingCheckerDetailModel data, ref string message);
        #endregion


    }

    #endregion

    #region Repository

    public class WorkflowDealTMORepository : IWorkflowDealTMORepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineModel> timelines, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                //Remark Rizki 2015-04-21, ganti dari View ke SP             
                timelines = (from a in context.SP_GetNintexTaskTimeline(workflowInstanceID)
                             select new TransactionTimelineModel
                             {
                                 ApproverID = a.WorkflowApproverID,
                                 Activity = a.ActivityTitle,
                                 Time = a.ActivityTime,
                                 UserOrGroup = a.UserOrGroup,
                                 IsGroup = a.IsSPGroup.Value,
                                 Outcome = a.Outcome,
                                 DisplayName = a.DisplayName,
                                 Comment = a.Comment
                             }).ToList();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        public bool GetTransactionDetails(Guid instanceID, ref TransactionNettingDetailModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();
                transaction = (from a in context.TransactionNettings
                               where a.WorkflowInstanceID.Value.Equals(instanceID)
                               select new TransactionNettingDetailModel
                               {
                                   ID = a.TransactionNettingID,
                                   ApplicationID = a.ApplicationID,
                                   CIF = a.CIF,
                                   TZReference = a.TZRef,
                                   OtherUnderlying = a.OtherUnderlying,

                                   StatementLetter = new StatementLetterModel()
                                   {
                                       ID = a.StatementLetter.StatementLetterID,
                                       Name = a.StatementLetter.StatementLetterName
                                   },
                                   ExpectedDateStatementLetter = a.ExpectedSubmissionDateSL,
                                   ExpectedDateUnderlying = a.ExpectedSubmissionDateUnd,
                                   ActualDateStatementLetter = a.ActualSubmissionDateSL,
                                   ActualDateUnderlying = a.ActualSubmissionDateUnd,
                                   NettingPurpose = new NettingPurposeModel()
                                   {
                                       ID = a.NettingPurposeID.Value,
                                       Name = context.ParameterSystems.Where(x => x.ParsysID == a.NettingPurposeID).Select(x => x.ParameterValue).FirstOrDefault()
                                   },
                                   NettingTransactionID = a.NettingTransactionID,
                                   NettingTZReference = new NettingTZReferenceModel()
                                   {
                                       TZReference = a.NettingDealNo
                                   },
                                   MurexNumber = a.MurexNumber,
                                   SwapDealNumber = a.SwapDealNumber,
                                   utilizationAmount = a.UtilizedAmount,
                                   Remarks = a.Remarks,
                                   IsDraft = a.IsDraft.Value,
                               }).SingleOrDefault();


                if (transaction != null)
                {
                    string DFNumber = transaction.TZReference;
                    string MurexNumber = transaction.MurexNumber;
                    long TransactionDealID = 0;
                    if (!string.IsNullOrEmpty(DFNumber))
                    {
                        TransactionDealID = context.TransactionDeals.Where(x => x.TZRef.Equals(DFNumber)).Select(x => x.TransactionDealID).FirstOrDefault();
                    }
                    else
                    {
                        TransactionDealID = context.TransactionDeals.Where(x => x.NB.Equals(MurexNumber)).Select(x => x.TransactionDealID).FirstOrDefault();
                    }
                    var deal = context.TransactionDeals.Where(a => a.TransactionDealID.Equals(TransactionDealID)).Select(a => a).FirstOrDefault();
                    string accountNumber = deal.AccountNumber;
                    long underlyingcode = Convert.ToInt32(transaction.bookunderlyingcode);

                    string cif = transaction.CIF;

                    transaction.AccountCurrencyCode = transaction.Account != null ? transaction.Account.Currency.Code : "";
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    bool isUnderlying = context.TransactionDealUnderlyings.Any(x => x.TransactionDealID.Equals(TransactionDealID));

                    if (isUnderlying)
                    {
                        if (customerRepo.GetCustomerByTransactionDeal(TransactionDealID, ref customer, ref message))
                        {
                            transaction.Customer = customer;
                            transaction.Documents = context.TransactionDealDocuments.Where(x => x.TransactionDealID.Equals(TransactionDealID) && x.IsDeleted.Equals(false)).Select(x => new TransactionNettingDocumentModel()
                            {
                                ID = x.TransactionDocumentID,
                                Type = new DocumentTypeModel()
                                {
                                    ID = x.DocumentType.DocTypeID,
                                    Name = x.DocumentType.DocTypeName,
                                    Description = x.DocumentType.DocTypeDescription,
                                    LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                    LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                },
                                Purpose = new DocumentPurposeModel()
                                {
                                    ID = x.DocumentPurpose.PurposeID,
                                    Name = x.DocumentPurpose.PurposeName,
                                    Description = x.DocumentPurpose.PurposeDescription,
                                    LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                    LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                },
                                FileName = x.Filename,
                                DocumentPath = x.DocumentPath,
                                LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                            }).ToList();
                            transaction.Currency = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(TransactionDealID)).Select(x => new CurrencyModel()
                            {
                                ID = x.CurrencyID,
                                Code = context.Currencies.Where(y => y.CurrencyID.Equals(x.CurrencyID)).Select(y => y.CurrencyCode).FirstOrDefault()
                            }).FirstOrDefault();
                            transaction.IsJointAccount = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(TransactionDealID)).Select(x => x.IsJointAccount).FirstOrDefault();
                            transaction.IsResident = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(TransactionDealID)).Select(x => x.IsResident).FirstOrDefault();
                            transaction.ProductTypeID = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(TransactionDealID)).Select(x => x.ProductTypeID).FirstOrDefault();
                            transaction.ProductType = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(TransactionDealID)).Select(x => new ProductTypeModel()
                            {
                                ID = x.ProductTypeID,
                                Code = context.ProductTypes.Where(y => y.ProductTypeID.Equals(x.ProductTypeID)).Select(y => y.ProductTypeCode).FirstOrDefault()
                            }).FirstOrDefault();
                        }
                    }
                    else
                    {
                        if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                        {
                            transaction.Customer = customer;
                            transaction.Customer.Underlyings = null;
                            transaction.Documents = context.TransactionDealDocuments.Where(x => x.TransactionDealID.Equals(TransactionDealID) && x.IsDeleted.Equals(false)).Select(x => new TransactionNettingDocumentModel()
                            {
                                ID = x.TransactionDocumentID,
                                Type = new DocumentTypeModel()
                                {
                                    ID = x.DocumentType.DocTypeID,
                                    Name = x.DocumentType.DocTypeName,
                                    Description = x.DocumentType.DocTypeDescription,
                                    LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                    LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                },
                                Purpose = new DocumentPurposeModel()
                                {
                                    ID = x.DocumentPurpose.PurposeID,
                                    Name = x.DocumentPurpose.PurposeName,
                                    Description = x.DocumentPurpose.PurposeDescription,
                                    LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                    LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                },
                                FileName = x.Filename,
                                DocumentPath = x.DocumentPath,
                                LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                            }).ToList();
                            transaction.Currency = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(TransactionDealID)).Select(x => new CurrencyModel()
                            {
                                ID = x.CurrencyID,
                                Code = context.Currencies.Where(y => y.CurrencyID.Equals(x.CurrencyID)).Select(y => y.CurrencyCode).FirstOrDefault()
                            }).FirstOrDefault();
                            transaction.IsJointAccount = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(TransactionDealID)).Select(x => x.IsJointAccount).FirstOrDefault();
                            transaction.IsResident = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(TransactionDealID)).Select(x => x.IsResident).FirstOrDefault();
                            transaction.ProductTypeID = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(TransactionDealID)).Select(x => x.ProductTypeID).FirstOrDefault();
                            transaction.ProductType = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(TransactionDealID)).Select(x => new ProductTypeModel()
                            {
                                ID = x.ProductTypeID,
                                Code = context.ProductTypes.Where(y => y.ProductTypeID.Equals(x.ProductTypeID)).Select(y => y.ProductTypeCode).FirstOrDefault()
                            }).FirstOrDefault();
                        }

                    }

                    if (deal.IsOtherAccountNumber != null && deal.IsOtherAccountNumber == true)
                    {
                        CustomerAccountModel Account = new CustomerAccountModel();
                        transaction.Account = Account;
                        transaction.OtherAccountNumber = deal.OtherAccountNumber;
                    }
                    else
                    {
                        transaction.Account = (from j in context.SP_GETJointAccount(cif)
                                               where j.AccountNumber.Equals(accountNumber)
                                               select new CustomerAccountModel
                                               {
                                                   AccountNumber = j.AccountNumber,
                                                   Currency = new CurrencyModel
                                                   {
                                                       ID = j.CurrencyID.Value,
                                                       Code = j.CurrencyCode,
                                                       Description = j.CurrencyDescription
                                                   },
                                                   CustomerName = j.CustomerName,
                                                   IsJointAccount = j.IsJointAccount
                                               }).FirstOrDefault();


                    }

                    CurrencyModel AccountCurrency = new CurrencyModel();
                    AccountCurrency.ID = deal.DebitCurrency.CurrencyID;
                    AccountCurrency.Code = deal.DebitCurrency.CurrencyCode;
                    AccountCurrency.Description = deal.DebitCurrency.CurrencyDescription;

                    transaction.DebitCurrency = AccountCurrency;
                    transaction.AccountCurrencyCode = AccountCurrency.Code;

                    customerRepo.Dispose();
                    customer = null;
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }

        public bool GetTransactionCheckerDetails(Guid workflowInstanceID, long approverID, ref TransactionNettingCheckerDetailModel output, ref string message)
        {
            bool IsSuccess = false;

            var transactionID = context.TransactionNettings
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionNettingID)
                        .SingleOrDefault();

            try
            {

                output.Verify = (from a in context.TransactionNettingCheckers.AsNoTracking()
                                 where a.TransactionNettingID.Equals(transactionID) && a.IsDeleted.Equals(false)
                                 select new VerifyDealModel()
                                 {
                                     ID = a.TransactionColumnID,
                                     Name = a.TransactionColumn.ColumnName,
                                     IsVerified = a.IsVerified
                                 }).ToList();
                if (!output.Verify.Any())
                {
                    output.Verify = context.TransactionColumns
                                    .Where(x => x.IsPPUCheckerNetting.Value.Equals(true) && x.IsDeleted.Equals(false))
                                    .Select(x => new VerifyDealModel()
                                    {
                                        ID = x.TransactionColumnID,
                                        Name = x.ColumnName,
                                        IsVerified = false
                                    }).ToList();


                }


                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        public bool AddTransactionChecker(Guid workflowInstanceID, long approverID, TransactionNettingCheckerDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionNettingID = context.TransactionNettings
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionNettingID)
                        .FirstOrDefault();
                    var dealNumber = data.Transaction.TZReference;
                    var nettingDealNumber = data.Transaction.NettingTZReference.TZReference;
                    if (transactionNettingID > 0)
                    {
                        IList<TransactionNettingChecker> Verify = context.TransactionNettingCheckers.Where(x => x.TransactionNettingID.Equals(transactionNettingID)).ToList();
                        if (Verify.Count() > 0)
                        {
                            foreach (var item in Verify)
                            {
                                item.IsDeleted = true;
                            }
                            context.SaveChanges();
                        }

                        foreach (var item in data.Verify)
                        {
                            context.TransactionNettingCheckers.Add(new TransactionNettingChecker()
                            {
                                ApproverID = approverID,
                                TransactionNettingID = transactionNettingID,
                                TransactionColumnID = item.ID,
                                IsVerified = item.IsVerified,
                                IsDeleted = false,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().DisplayName
                            });

                            context.SaveChanges();
                        }


                    }
                    if (dealNumber != null)
                    {
                        DBS.Entity.TransactionDeal dataDealNumber = context.TransactionDeals.Where(x => x.TZRef.Equals(dealNumber)).FirstOrDefault();
                        if (dataDealNumber != null)
                        {
                            dataDealNumber.IsNettingTransaction = true;
                            dataDealNumber.IsNettingComplete = true;

                        }
                        context.SaveChanges();
                    }

                    if (nettingDealNumber != null)
                    {
                        DBS.Entity.TransactionDeal dataNettingDealNumber = context.TransactionDeals.Where(x => x.TZRef.Equals(nettingDealNumber)).FirstOrDefault();
                        if (dataNettingDealNumber != null)
                        {
                            dataNettingDealNumber.IsNettingTransaction = true;

                            int? swapTypeNettingDealNumber = dataNettingDealNumber.SwapType;
                            if (swapTypeNettingDealNumber.Value != null || swapTypeNettingDealNumber.Value != 0)
                            {
                                DBS.Entity.TransactionDeal dataNettingDealNumberSwap = context.TransactionDeals.Where(x => x.SwapTZDealNo.Equals(dataNettingDealNumber.SwapTZDealNo) && !x.TZRef.Equals(nettingDealNumber)).FirstOrDefault();
                                if (dataNettingDealNumberSwap != null)
                                {
                                    dataNettingDealNumberSwap.IsNettingTransaction = true;
                                }
                            }
                        }
                        context.SaveChanges();

                    }
                    ts.Complete();
                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }

        public bool UpdateTransactionMaker(Guid workflowInstanceID, long approverID, TransactionNettingMakerDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionNettingID = context.TransactionNettings
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionNettingID)
                        .SingleOrDefault();

                    if (transactionNettingID > 0)
                    {
                        DBS.Entity.TransactionNetting datanetting = context.TransactionNettings.Where(x => x.TransactionNettingID.Equals(transactionNettingID)).SingleOrDefault();
                        if (datanetting != null)
                        {

                            datanetting.ExpectedSubmissionDateSL = data.Transaction.ExpectedDateStatementLetter;
                            datanetting.ExpectedSubmissionDateUnd = data.Transaction.ExpectedDateUnderlying;
                            datanetting.ActualSubmissionDateSL = data.Transaction.ExpectedDateStatementLetter;
                            datanetting.ActualSubmissionDateUnd = data.Transaction.ExpectedDateUnderlying;
                            datanetting.TZRef = data.Transaction.TZReference;
                            datanetting.NettingPurposeID = data.Transaction.NettingPurpose.ID;
                            datanetting.NettingTransactionID = data.Transaction.NettingTZReference.ID;
                            datanetting.NettingDealNo = data.Transaction.TZReference;
                            datanetting.UtilizedAmount = data.Transaction.utilizationAmount;
                            datanetting.Remarks = data.Transaction.Remarks;
                            datanetting.UpdateDate = DateTime.UtcNow;
                            datanetting.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        }
                        context.SaveChanges();
                    }

                    ts.Complete();
                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }

        public bool UpdateTransactionDealRevise(Guid workflowInstanceID, TransactionNettingCheckerDetailModel transactionChecker, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.TransactionNetting data = context.TransactionNettings.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).SingleOrDefault();
                    if (data != null)
                    {
                        long transactionID = data.TransactionNettingID;
                        isSuccess = RoolbackNettingCustomerUnderlying(transactionID, ref message);
                        if (!isSuccess)
                        {
                            ts.Dispose();
                            return isSuccess;
                        }
                        isSuccess = true;
                    }
                    ts.Complete();

                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }
            return isSuccess;
        }

        public bool RoolbackNettingCustomerUnderlying(long TranscationNettingID, ref string message)
        {
            bool isSuccess = false;

            try
            {
                List<Entity.TransactionNettingUnderlying> items = context.TransactionNettingUnderlyings.Where(a => a.TransactionNettingID.Equals(TranscationNettingID) && a.IsDeleted.Equals(false)).ToList();
                if (items != null && items.Count > 0)
                {
                    foreach (Entity.TransactionNettingUnderlying data in items)
                    {
                        // set update customer underlying
                        var dataCustomerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(data.UnderlyingID)).SingleOrDefault();
                        dataCustomerUnderlying.AvailableAmountUSD += data.Amount;
                        dataCustomerUnderlying.IsUtilize = dataCustomerUnderlying.AmountUSD == dataCustomerUnderlying.AvailableAmountUSD ? false : true;
                        dataCustomerUnderlying.UpdateDate = DateTime.UtcNow;
                        dataCustomerUnderlying.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        context.SaveChanges();
                        // set update transaction netting underlying
                        var dataTransactionNettingUnderlying = context.TransactionNettingUnderlyings.Where(a => a.TransactionNettingUnderlyingID.Equals(data.TransactionNettingUnderlyingID)).SingleOrDefault();
                        dataTransactionNettingUnderlying.IsDeleted = true;
                        dataTransactionNettingUnderlying.UpdateDate = DateTime.UtcNow;
                        dataTransactionNettingUnderlying.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        context.SaveChanges();
                    }
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {

                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTransactionDealDetails(long id, ref TransactionDealDetailModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                transaction = (from a in context.TransactionDeals
                               where a.TransactionDealID.Equals(id)
                               select new TransactionDealDetailModel
                               {
                                   ID = a.TransactionDealID,
                                   CIF = a.CIF,
                                   WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                   ApplicationID = a.ApplicationID,
                                   TradeDate = a.TradeDate,
                                   ValueDate = a.ValueDate,
                                   ProductType = new ProductTypeModel()
                                   {
                                       ID = a.ProductType.ProductTypeID,
                                       Code = a.ProductType.ProductTypeCode,
                                       Description = a.ProductType.ProductTypeDesc,
                                       IsFlowValas = a.ProductType.IsFlowValas
                                   },
                                   Rate = a.Rate,
                                   StatementLetter = new StatementLetterModel()
                                   {
                                       ID = a.StatementLetter.StatementLetterID,
                                       Name = a.StatementLetter.StatementLetterName
                                   },
                                   TZNumber = a.TZRef,
                                   Currency = new CurrencyModel()
                                   {
                                       ID = a.Currency.CurrencyID,
                                       Code = a.Currency.CurrencyCode,
                                       Description = a.Currency.CurrencyDescription,
                                       LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                       LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                   },
                                   Amount = a.Amount,
                                   AmountUSD = a.AmountUSD,
                                   AccountNumber = a.AccountNumber,
                                   IsResident = a.IsResident.HasValue ? a.IsResident.Value : false,
                                   Documents = a.TransactionDealDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDealDocumentModel()
                                   {
                                       ID = x.TransactionDocumentID,
                                       Type = new DocumentTypeModel()
                                       {
                                           ID = x.DocumentType.DocTypeID,
                                           Name = x.DocumentType.DocTypeName,
                                           Description = x.DocumentType.DocTypeDescription,
                                           LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                           LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                       },
                                       Purpose = new DocumentPurposeModel()
                                       {
                                           ID = x.DocumentPurpose.PurposeID,
                                           Name = x.DocumentPurpose.PurposeName,
                                           Description = x.DocumentPurpose.PurposeDescription,
                                           LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                           LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                       },
                                       FileName = x.Filename,
                                       DocumentPath = x.DocumentPath,
                                       LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                       LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                   }).ToList(),

                                   CreateDate = a.CreatedDate,
                                   LastModifiedBy = a.UpdateDate == null ? a.CreatedBy : a.UpdateBy,
                                   LastModifiedDate = a.UpdateDate == null ? a.CreatedDate : a.UpdateDate.Value,

                                   DealUnderlying = context.UnderlyingDocuments.Where(y => a.UnderlyingCodeID == (y.UnderlyingDocID)).Select(x => new UnderlyingDocModel()
                                   {
                                       ID = x.UnderlyingDocID,
                                       Name = x.UnderlyingDocName,
                                       Code = x.UnderlyingDocCode,
                                       Description = x.UnderlyingDocCode.Equals("999") ? a.OtherUnderlying : x.UnderlyingDocName
                                   }).FirstOrDefault(),

                                   bookunderlyingamount = a.UnderlyingAmount,
                                   bookunderlyingcode = a.UnderlyingCodeID,
                                   bookunderlyingcurrency = a.UnderlyingCurrencyID,
                                   bookunderlyingcurrencydesc = a.OtherUnderlying,
                                   IsOtherAccountNumber = a.IsOtherAccountNumber != null ? a.IsOtherAccountNumber : false,
                                   IsJointAccount = a.IsJointAccount.HasValue ? a.IsJointAccount.Value : false

                               }).SingleOrDefault();


                if (transaction != null)
                {
                    long TransactionDealID = transaction.ID;
                    string accountNumber = transaction.AccountNumber;
                    long underlyingcode = Convert.ToInt32(transaction.bookunderlyingcode);

                    string cif = transaction.CIF;
                    transaction.bookunderlyingdoccode = transaction.DealUnderlying != null ? transaction.DealUnderlying.Code : "";
                    transaction.AccountCurrencyCode = transaction.Account != null ? transaction.Account.Currency.Code : "";
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    bool isUnderlying = context.TransactionDealUnderlyings.Any(x => x.TransactionDealID.Equals(TransactionDealID));

                    var deal = context.TransactionDeals.Where(a => a.TransactionDealID.Equals(TransactionDealID)).Select(a => a).FirstOrDefault();

                    if (isUnderlying)
                    {
                        if (customerRepo.GetCustomerByTransactionDeal(TransactionDealID, ref customer, ref message))
                        {
                            transaction.Customer = customer;
                        }
                    }
                    else
                    {
                        if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                        {
                            transaction.Customer = customer;
                            transaction.Customer.Underlyings = null;
                        }

                    }

                    if (deal.IsOtherAccountNumber != null && deal.IsOtherAccountNumber == true)
                    {
                        CustomerAccountModel Account = new CustomerAccountModel();
                        transaction.Account = Account;
                        transaction.OtherAccountNumber = deal.OtherAccountNumber;
                    }
                    else
                    {
                        transaction.Account = (from j in context.SP_GETJointAccount(cif)
                                               where j.AccountNumber.Equals(accountNumber)
                                               select new CustomerAccountModel
                                               {
                                                   AccountNumber = j.AccountNumber,
                                                   Currency = new CurrencyModel
                                                   {
                                                       ID = j.CurrencyID.Value,
                                                       Code = j.CurrencyCode,
                                                       Description = j.CurrencyDescription
                                                   },
                                                   CustomerName = j.CustomerName,
                                                   IsJointAccount = j.IsJointAccount
                                               }).FirstOrDefault();
                        transaction.AccountNumber = deal.AccountNumber;

                    }

                    CurrencyModel AccountCurrency = new CurrencyModel();
                    AccountCurrency.ID = deal.DebitCurrency.CurrencyID;
                    AccountCurrency.Code = deal.DebitCurrency.CurrencyCode;
                    AccountCurrency.Description = deal.DebitCurrency.CurrencyDescription;

                    transaction.DebitCurrency = AccountCurrency;
                    transaction.AccountCurrencyCode = AccountCurrency.Code;

                    customerRepo.Dispose();
                    customer = null;


                }


                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.Collect();
            GC.SuppressFinalize(this);
        }


        public bool UpdateTransactionMakerIPE(Guid workflowInstanceID, long approverID, TransactionNettingMakerDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionNettingID = context.TransactionNettings
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionNettingID)
                        .SingleOrDefault();

                    if (transactionNettingID > 0)
                    {
                        DBS.Entity.TransactionNetting datanetting = context.TransactionNettings.Where(x => x.TransactionNettingID.Equals(transactionNettingID)).SingleOrDefault();
                        if (datanetting != null)
                        {

                            datanetting.ExpectedSubmissionDateSL = data.Transaction.ExpectedDateStatementLetter;
                            datanetting.ExpectedSubmissionDateUnd = data.Transaction.ExpectedDateUnderlying;
                            datanetting.ActualSubmissionDateSL = data.Transaction.ExpectedDateStatementLetter;
                            datanetting.ActualSubmissionDateUnd = data.Transaction.ExpectedDateUnderlying;
                            datanetting.TZRef = data.Transaction.TZReference;
                            datanetting.NettingPurposeID = data.Transaction.NettingPurpose.ID;
                            datanetting.NettingTransactionID = data.Transaction.NettingTZReference.ID;
                            datanetting.NettingDealNo = data.Transaction.TZReference;
                            datanetting.UtilizedAmount = data.Transaction.utilizationAmount;
                            datanetting.Remarks = data.Transaction.Remarks;
                            datanetting.UpdateDate = DateTime.UtcNow;
                            datanetting.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        }
                        context.SaveChanges();
                    }

                    ts.Complete();
                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }

        public bool AddTransactionCheckerIPE(Guid workflowInstanceID, long approverID, TransactionNettingCheckerDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionNettingID = context.TransactionNettings
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionNettingID)
                        .FirstOrDefault();
                    var dealNumber = data.Transaction.TZReference;
                    var nettingDealNumber = data.Transaction.NettingTZReference.TZReference;
                    if (transactionNettingID > 0)
                    {
                        IList<TransactionNettingChecker> Verify = context.TransactionNettingCheckers.Where(x => x.TransactionNettingID.Equals(transactionNettingID)).ToList();
                        if (Verify.Count() > 0)
                        {
                            foreach (var item in Verify)
                            {
                                item.IsDeleted = true;
                            }
                            context.SaveChanges();
                        }

                        foreach (var item in data.Verify)
                        {
                            context.TransactionNettingCheckers.Add(new TransactionNettingChecker()
                            {
                                ApproverID = approverID,
                                TransactionNettingID = transactionNettingID,
                                TransactionColumnID = item.ID,
                                IsVerified = item.IsVerified,
                                IsDeleted = false,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().DisplayName
                            });

                            context.SaveChanges();
                        }


                    }
                    if (dealNumber != null)
                    {
                        DBS.Entity.TransactionDeal dataDealNumber = context.TransactionDeals.Where(x => x.TZRef.Equals(dealNumber)).FirstOrDefault();
                        if (dataDealNumber != null)
                        {
                            dataDealNumber.IsNettingTransaction = true;
                            dataDealNumber.IsNettingComplete = true;

                        }
                        context.SaveChanges();
                    }

                    if (nettingDealNumber != null)
                    {
                        DBS.Entity.TransactionDeal dataNettingDealNumber = context.TransactionDeals.Where(x => x.TZRef.Equals(nettingDealNumber)).FirstOrDefault();
                        if (dataNettingDealNumber != null)
                        {
                            dataNettingDealNumber.IsNettingTransaction = true;

                            int? swapTypeNettingDealNumber = dataNettingDealNumber.SwapType;
                            if (swapTypeNettingDealNumber.Value != null || swapTypeNettingDealNumber.Value != 0)
                            {
                                DBS.Entity.TransactionDeal dataNettingDealNumberSwap = context.TransactionDeals.Where(x => x.SwapTZDealNo.Equals(dataNettingDealNumber.SwapTZDealNo) && !x.TZRef.Equals(nettingDealNumber)).FirstOrDefault();
                                if (dataNettingDealNumberSwap != null)
                                {
                                    dataNettingDealNumberSwap.IsNettingTransaction = true;
                                }
                            }
                        }
                        context.SaveChanges();

                    }
                    ts.Complete();
                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }
    }
    #endregion
}