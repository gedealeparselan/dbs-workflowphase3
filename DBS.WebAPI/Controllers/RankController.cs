﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class RankController : ApiController
    {
        private string message = string.Empty;

        private readonly IRankRepository repository = null;

        public RankController()
        {
            this.repository = new RankRepository();
        }

        public RankController(IRankRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all ranks.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<RankModel>))]
        public HttpResponseMessage Get()
        {
            IList<RankModel> output = new List<RankModel>();

            if (!repository.GetRank(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<RankModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get ranks with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<RankModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<RankModel> output = new List<RankModel>();

            if (!repository.GetRank(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<RankModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get ranks for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(RankGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<RankFilter> filters)
        {
            RankGrid output = new RankGrid();

            if (!repository.GetRank(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<RankGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find ranks using criterias Code or Description.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<RankModel>))]
        [HttpGet]
        [Route("api/Rank/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<RankModel> output = new List<RankModel>();

            if (!repository.GetRank(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<RankModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get rank by ID.
        /// </summary>
        /// <param name="id">Rank ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(RankModel))]
        [Route("api/Rank/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            RankModel output = new RankModel();

            if (!repository.GetRankByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<RankModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new rank.
        /// </summary>
        /// <param name="rank">Rank data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(RankModel rank)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddRank(rank, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New rank data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing rank.
        /// </summary>
        /// <param name="id">ID of rank to be modify.</param>
        /// <param name="rank">Rank data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/Rank/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, RankModel rank)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateRank(id, rank, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Rank data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting rank.
        /// </summary>
        /// <param name="id">ID of rank to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/Rank/{id}")]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteRank(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Rank data has been deleted." });
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}