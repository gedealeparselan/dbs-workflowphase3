//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    
    public partial class SP_GetCustomerList_Result
    {
        public string CIF { get; set; }
        public string CustomerName { get; set; }
        public Nullable<int> LocationID { get; set; }
        public string LocationName { get; set; }
        public int BizSegmentID { get; set; }
        public string BizSegmentName { get; set; }
        public Nullable<int> CustomerTypeID { get; set; }
        public string CustomerTypeName { get; set; }
        public string UpdateBy { get; set; }
        public System.DateTime UpdateDate { get; set; }
    }
}
