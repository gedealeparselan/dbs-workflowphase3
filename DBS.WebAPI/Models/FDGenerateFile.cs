﻿using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DBS.WebAPI.Models
{
    #region property
    [ModelName("FDMaturity")]
    public class FDMaturityModel
    {
        public int ID {set; get;}
	    public string CUST_ID {get; set;}
	    public string CUST_NAME {set; get;}
	    public string SCHEME_TYPE {set; get;}
	    public string SCHEME_CODE {set; get;}
	    public string PRODUCT_TYPE {set; get;}
	    public string PROD_DESC {set; get;}
	    public string ACCT_NO {set; get;}
	    public string CCY {set; get;}
	    public decimal? ORIGINAL_AMOUNT {set; get;}
	    public DateTime? ROLLOVER_DATE {set; get;}
	    public DateTime? MATURITY_DATE {set; get;}
	    public string RM_NAME {set; get;}
	    public string BRANCH_CODE {set; get;}
	    public string INT_RATE_CODE {set; get;}
	    public decimal? INTEREST_RATE {set; get;}
	    public decimal? AC_PREF_INT_CR {set; get;}
	    public decimal? NET_INT_RATE {set; get;}
	    public decimal? INTEREST_MARGIN {set; get;}
	    public decimal? FTP_RATE {set; get;}
	    public decimal? SPREAD_RATE {set; get;}
	    public decimal? NettInterestAmount {set; get;}
	    public string MaturityDateInstruction {set; get;}
	    public string ValueDateInstruction {set; get;}
	    public string RenewalOption {set; get;}
	    public string RepaymentAccount {set; get;}
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }  
    }
    public class FDMaturityRow : FDMaturityModel
    {
        public int RowID { get; set; }
    }

    public class FDMaturityGrid : Grid
    {
        public IList<FDMaturityRow> Rows { get; set; }
    }
    #endregion

    #region interface
    #endregion

    #region filter
    public class FDMaturityFilter : Filter
    {

    }
    #endregion

    #region repository
    public interface IFDMaturity : IDisposable
    {

    }
    #endregion

    public class FDMaturityRepository : IFDMaturity
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetFDMaturityByID(int id, ref FDMaturityModel FDMaturity, ref string message)
        {
            bool isSuccess = false;
            try
            {
                FDMaturity = (from matur in context.FDMaturities
                              where matur.FDMaturityID.Equals(id)
                              )
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetFDMaturity(ref IList<FDMaturityModel> FDMaturities, int limit, int index, ref string message)
        {
            bool isSuccess = false;
            return isSuccess;
        }

        public bool GetFDMaturity(ref IList<FDMaturityModel> FDMaturities, ref string message)
        {
            bool isSuccess = false;
            return isSuccess;
        }

        public bool GetFDMaturity(int page, int size, IList<FDMaturityFilter> filters, string sortColumn, string sortOrder, ref FDMaturityGrid fdMaturityGrid, ref string message)
        {
            bool isSuccess = false;
            return isSuccess;
        }

        public bool GetFDMaturity(string key, int limit, ref IList<FDMaturityModel> FDMaturities, ref string message)
        {
            bool isSuccess = false;
            return isSuccess;
        }

        public bool GetFDMaturity()
        {
            bool isSuccess = false;
            return isSuccess;
        }
        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}