﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;
using DBS.Entity;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class MISReportingPieChartController : ApiController
    {
        private string message = string.Empty;
        private readonly IMISReportingPieChart repositoryMISReportingPieChart = null;

        public MISReportingPieChartController()
        {
            this.repositoryMISReportingPieChart = new MISReportingPieChartRepository();
        }
        
        public MISReportingPieChartController(IMISReportingPieChart repositoryMISReportingPieChart)
        {
            this.repositoryMISReportingPieChart = repositoryMISReportingPieChart;
        }

        [ResponseType(typeof(MISReportingPieChartModel))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/MISReportingPieChart")]
        public HttpResponseMessage Get(int TransactionType, int Segmentation, int Channel, int Branch, DateTime StartDate, DateTime EndDate, DateTime ClientDateTime)
        {
            IList<MISReportingPieChartModel> Output = new List<MISReportingPieChartModel>();
            if (!repositoryMISReportingPieChart.GetMISReportingPieChart(TransactionType, Segmentation, Channel, Branch, StartDate, EndDate, ClientDateTime, ref Output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<MISReportingPieChartModel>>(HttpStatusCode.OK, Output);
            }
        }
    }
}
