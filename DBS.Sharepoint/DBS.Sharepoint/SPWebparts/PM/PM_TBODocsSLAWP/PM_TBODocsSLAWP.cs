﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace DBS.Sharepoint.SPWebparts.PM.PM_TBODocsSLAWP
{
    [ToolboxItemAttribute(false)]
    public class PM_TBODocsSLAWP : WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/15/DBS.Sharepoint.SPWebparts.PM/PM_TBODocsSLAWP/PM_TBODocsSLAWPUserControl.ascx";

        protected override void CreateChildControls()
        {
            Control control = Page.LoadControl(_ascxPath);
            Controls.Add(control);
        }
    }
}
