var HomeType = {
    Home: "home",
    Canceled: "canceled",
    Completed: "completed",
    Monitoring: "monitoring",
    AllTransaction: "alltransaction"
};
var CheckDummyCIF = "";

var IDOutcomeComplete = Const_Force.Complete; // di disesuaikan
var IDOutcomeCancel = Const_Force.Cancel; // di sesuaikan

/* dodit@2014.11.08:add format function */
var formatNumber = function (num) {
    if (num != null) {
        //num = num.toString().replace(/,/g, '');
        //num = parseFloat(num).toFixed(2);
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    } else { return 0; }
}
var DispatchModeData = [{ ID: 1, Name: 'BSPL Delivery and Email' }, { ID: 2, Name: 'Collect by Person' }, { ID: 3, Name: 'Email' }, { ID: 4, Name: 'No Dispatch' }, { ID: 5, Name: 'Post' }, { ID: 6, Name: 'SSPL Delivery' }, { ID: 9, Name: '-' }]
var formatNumberInterestRateFD = function (num) {
    if (num != null) {
        //num = num.toString().replace(/,/g, '');
        //num = parseFloat(num).toFixed(2);
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    }
}
//Tambah Agung
var SetPartialFull = function (partial) {
    if (partial == true) {
        return "Partial";
    }
    else
        return "Full";
}
//End
//add azam
var TransactionSKNBulkModel = {
    ID: ko.observable(),
    AccountNumber: ko.observable(),
    CustomerName: ko.observable(),
    ValueDate: ko.observable(),
    PaymentAmount: ko.observable(),
    BeneficiaryName: ko.observable(),
    BeneficiaryAccountNumber: ko.observable(),
    BankCode: ko.observable(),
    BranchCode: ko.observable(),
    Citizen: ko.observable(),
    Resident: ko.observable(),
    PaymentDetails: ko.observable(),
    Charges: ko.observable(),
    Type: ko.observable(),
    ParentTransactionID: ko.observable(),
    ParentWorkflowInstanceID: ko.observable(),
    IsChanged: ko.observable()
};
//end add azam

//henggar 180417
var Source = [{ ID: 1, Name: 'CSO ' }, { ID: 2, Name: 'Counter' }]
var Education = [{ ID: 1, Name: 'Degree' }, { ID: 2, Name: 'Diploma' }, { ID: 3, Name: 'Graduate' }, { ID: 4, Name: "High School" }, { ID: 5, Name: "Junior High School" }, { ID: 6, Name: 'Master' }, { ID: 7, Name: 'Other' }, { ID: 8, Name: 'Post Graduate' }, { ID: 9, Name: 'Specialist' }, { ID: 10, Name: 'Under Graduate' }]
var Religion = [{ ID: 1, Name: 'Budist' }, { ID: 2, Name: 'Catholic' }, { ID: 3, Name: 'Christian' }, { ID: 4, Name: "Hindu" }, { ID: 5, Name: "Khonghucu" }, { ID: 6, Name: 'Muslim' }, { ID: 7, Name: 'Other' }]
var FATCAReviewStatus = [{ ID: 1, Name: 'Completed' }, { ID: 2, Name: 'Pending' }]
var FATCACRSStatus = [{ ID: 1, Name: 'Yes' }, { ID: 2, Name: 'No' }]
var TaxPayer = [{ ID: 1, Name: 'SSN' }, { ID: 2, Name: 'ITIN' }, { ID: 3, Name: 'ATIN' }, { ID: 4, Name: 'PTIN' }, { ID: 5, Name: 'TIN' }]
var WithholdingCertificationType = [{ ID: 1, Name: 'NOT APLICABLE' }, { ID: 2, Name: 'SELF CERT' }, { ID: 3, Name: 'SELF CERT & LOSS OF CITIZENSHIP' }, { ID: 4, Name: 'W-9' }, { ID: 5, Name: 'W-8BEN' }, { ID: 6, Name: 'W-8BEN & LOSS OF CITIZENSHIP' }]
//end

var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;

    self.TransactionSKNBulks = ko.observable([TransactionSKNBulkModel]);
    self.TransactionSKNBulksOriginal = ko.observable([TransactionSKNBulkModel]);
    self.IsLimit = ko.observable(false);
    // Sharepoint User
    self.SPUser = ko.observable();
    self.IsPPURole = ko.observable(false);
    self.GetDefaultDate = function () { GetDefaultDate(); };
    function GetDefaultDate() {
        var newDateTime = new Date();
        var newMonth; var newDate;
        var newHour; var newMinute; var newSecond;

        newMonth = newDateTime.getMonth() + 1;
        newMonth = newMonth.toString().length == 2 ? newMonth : "0" + newMonth;
        newDate = newDateTime.getDate();
        newDate = newDate.toString().length == 2 ? newDate : "0" + newDate;
        newHour = newDateTime.getHours();
        newHour = newHour.toString().length == 2 ? newHour : "0" + newHour;
        newMinute = newDateTime.getMinutes();
        newMinute = newMinute.toString().length == 2 ? newMinute : "0" + newMinute;
        newSecond = newDateTime.getSeconds();
        newSecond = newSecond.toString().length == 2 ? newSecond : "0" + newSecond;


        if (viewModel.FormMenuCustom() == "Monitoring" || viewModel.FormMenuCustom() == "AllTransaction" || viewModel.FormMenuCustom() == "Canceled" || viewModel.FormMenuCustom() == "Completed") { //Modify By adi
            self.GridProperties().SortColumn("LastModifiedDate");
            self.GridProperties().SortOrder("DESC");
            self.GridProperties().AllowFilter(true);
            //self.FilterEntryTime(newDateTime.getFullYear() + "/" + newMonth + "/" + newDate);
            var fixedDate = newDateTime.getFullYear() + "/" + newMonth + "/" + newDate;
            self.FilterEntryTime(self.LocalDate(fixedDate, true, false));

        }


    }
    //AndriYanto 30 November 2015
    self.InitialProduct = ko.observable();
    self.ProductID = ko.observable();
    //end Andriyanto
    // rate param
    self.Rate = ko.observable(0);
    self.Amount = ko.observable(0);
    self.BeneAccNumberMask = ko.observable();
    self.AmountUSD_r = ko.observable(0);

    //add henggar
    self.FatcaReviewStatusDate = ko.observable();
    self.DateOnForm = ko.observable();
    self.Education = ko.observable();
    self.Religion = ko.observable();
    self.FATCAReviewStatus = ko.observable();
    self.FATCACRSStatus = ko.observable();
    self.TaxPayer = ko.observable();
    self.WithholdingCertificationType = ko.observable();
    self.TaxPayerID = ko.observable();
    //add henggar 210417
    self.IsStatusCRS = ko.observable(false);
    self.IsTaxPayerType = ko.observable(false);
    self.GrossIncomeccyName = ko.observable();
    self.GrossIncomeccyDescription = ko.observable();
    self.IsCustomerJoin = ko.observable();
    self.IsAccountJoin = ko.observable();
    self.IsCustomerFormJoin = ko.observable();

    //check tz number exist
    self.IsSearchTZ = ko.observable(false);
    self.TZModel = ko.observable(TZModel);

    self.instanceIdpayment = ko.observable();
    self.approverIdpayment = ko.observable();
    self.GetPaymentURL = ko.observable();

    //henggar 8 des 2016 skn bulk
    self.IsTransactionSKNBulks = ko.observable(false);
    self.IsTransactionSKNBulksOriginal = ko.observable(false);

    // count of document
    self.CountDoc = 0;

    // Custom Form for Menu Home
    self.FormMenuCustom = ko.observable();

    // Workflow task config
    self.WorkflowConfig = ko.observable();

    //aailable timelines tab
    self.IsTimelines = ko.observable(false);

    // utilize amount from underlying
    self.DealUnderlying = ko.observable(DealUnderlyingModel);
    self.UtilizationAmounts = ko.observable(0);

    //other account number selected
    self.IsOtherAccountNumber = ko.observable(false);

    // counter upload file ppu maker
    self.counterUpload = ko.observable(0);

    //underlying declare
    self.AmountModel = ko.observable(AmountModel);

    //file attachment edit
    self.SelectingUnderlying = ko.observable(true);

    //is fx transaction
    self.t_IsFxTransaction = ko.observable(false);
    self.t_IsFxTransactionToIDR = ko.observable(false);
    self.t_IsFxTransactionToIDRB = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerAttachUnderlyings = ko.observableArray([]);

    //added by dani 20-6-2016
    self.IsNewPlacement = ko.observable(false);
    self.IsPrematureBreak = ko.observable(false);
    self.IsBreakAtMaturity = ko.observable(false);
    self.IsChangeOfInstruction = ko.observable(false);
    self.IsFDMaintenance = ko.observable(false);

    //add by fandi
    self.Documents_Funding = ko.observableArray([]);
    self.ddlTransactionSubTypes = ko.observableArray([]);
    self.ddlRequestTypes = ko.observableArray([]);
    self.ddlTransactionTypes = ko.observableArray([]);
    self.ddlMaintenanceTypes = ko.observableArray([]);
    self.IsRequestTypeMaintenance = ko.observable(false);
    self.IsRequestTypeSuspendCIF = ko.observable(false);
    self.IsRequestTypeStandingInstruction = ko.observable(false);
    self.IsRequestTypeUnsuspendCIF = ko.observable(false);
    self.IsBranchMakerAfterCBOAccountCIF = ko.observable(false);
    self.IsCIFPengkinianData = ko.observable(false);
    self.IsCIFUpdateFXTier = ko.observable(false);
    self.IsCIFAtmCard = ko.observable(false);
    self.IsCIFRiskRating = ko.observable(false);
    self.IsCIFAddCurrency = ko.observable(false);
    self.IsCIFAdditionalAccount = ko.observable(false);
    self.IsCIFLinkFFD = ko.observable(false);
    self.IsCIFFreezeUnfreeze = ko.observable(false);
    self.IsLienUnlien = ko.observable(false);
    self.IsCIFActiveDormant = ko.observable(false);
    self.IsCIFLPS = ko.observable(false);
    self.IsCIFLOIPOIPOA = ko.observable(false);
    self.IsCIFChangeRM = ko.observable(false);
    self.IsCIFChangeRMCompleted = ko.observable(false);
    self.IsCIFActiveHPSP = ko.observable(false);
    self.IsCIFSuspendCIF = ko.observable(false);
    self.IsCIFUnsuspendCIF = ko.observable(false);
    self.IsCIFStandingInstruction = ko.observable(false);
    self.IsCIFTagUntagStaff = ko.observable(false);
    self.IsCIFDispatchMode = ko.observable(false);
    self.IsCIFATMClosure = ko.observable(false);
    self.ParameterCIF = ko.observable(ParameterCIF);
    self.isChangeRMS = ko.observable(false);
    self.isSegment = ko.observable(false);
    self.isSolID = ko.observable(false);
    self.IsCheckerCIF = ko.observable(false);
    //end

    // parameters
    self.Products = ko.observableArray([]);
    self.Currencies = ko.observableArray([]);
    self.DebitCurrencies = ko.observableArray([]);
    self.Channels = ko.observableArray([]);
    self.BizSegments = ko.observableArray([]);
    self.ProductTypes = ko.observableArray([]);
    self.LLDs = ko.observableArray([]);
    self.Banks = ko.observableArray([]);
    self.BankCharges = ko.observableArray([]);
    self.AgentCharges = ko.observableArray([]);
    self.FXCompliances = ko.observableArray([]);
    self.UnderlyingDocs = ko.observableArray([]);
    self.POAFunctions = ko.observableArray([]);
    self.DocumentTypes = ko.observableArray([]);
    self.DocumentPurposes = ko.observableArray([]);
    self.NettingPurpose = ko.observableArray([]);
    //headerHistory
    self.CreateDateHeader = ko.observable();
    self.ApplicationIDHeader = ko.observable();
    self.CIFHeaderHistory = ko.observable();
    self.CustomerNameHeader = ko.observable();
    self.ProductCodeHeader = ko.observable();
    self.ProductNamerHeader = ko.observable();
    self.CurrencyCodeHeader = ko.observable();
    self.CurrencyDescriptionHeader = ko.observable();
    self.ChannelNameHeader = ko.observable();
    self.ApplicationDateHeader = ko.observable();
    self.BizSegmentDescriptionDescHeader = ko.observable();
    self.BizSegmentNameDescHeader = ko.observable();
    self.IsNewCustomerHeader = ko.observable(false);
    self.SourceName = ko.observable();
    //end

    //decrale other condition
    self.isNewUnderlying = ko.observable(false);
    self.IsOtherBank = ko.observable(false);
    self.IsUploading = ko.observable(false);

    //#region Joint Account
    self.CIFJointAccounts = ko.observableArray([{ ID: false, Name: "Single" }, { ID: true, Name: "Join" }]);
    self.DynamicAccounts = ko.observableArray([]);
    self.IsJointAccount = ko.observable(false);
    self.IsHitThreshold = ko.observable(false);
    self.ThresholdValue = ko.observable(0);
    self.IsNoThresholdValue = ko.observable(false)
    self.IsEmptyAccountNumber = ko.observable(false);
    self.IsCanEditUnderlying = ko.observable(false);
    self.ThresholdType = ko.observable();
    self.Rounding = ko.observable(0);
    self.IsAnnualStatement = ko.observable(false);
    self.IsSwapTransaction = ko.observable(false);
    self.IsNettingTransaction = ko.observable(false);
    self.Swap_NettingDFNumber = ko.observable();
    self.MessageTransactionType = ko.observable();
    //#endregion
    self.ApprovalRole = ko.observable();
    self.ApprovalName = ko.observable();
    //Tambahan
    self.ShortTerm = ko.observable(true);
    self.IshiddenFinacleLoanMaker = function () {
        if (viewModel.TransactionAllLoanNormal().Disbursement.FinacleSchemeCodeID == ConsLoanSchemeCode.LongLTLAA || viewModel.TransactionAllLoanNormal().Disbursement.FinacleSchemeCodeID == ConsLoanSchemeCode.LongSLLAA) {
            viewModel.ShortTerm(true);
            //viewModel.RepricingPlan().RepricingPlanID('');
        }
        if (viewModel.TransactionAllLoanNormal().Disbursement.FinacleSchemeCodeID == ConsLoanSchemeCode.ShortSTLA || viewModel.TransactionAllLoanNormal().Disbursement.FinacleSchemeCodeID == ConsLoanSchemeCode.ShortSSLAA) {
            viewModel.ShortTerm(false);
            //viewModel.RepricingPlan().RepricingPlanID(118);
        }
    }
    //end

    // task properties
    self.ApproverId = ko.observable();
    self.SPWebID = ko.observable();
    self.SPSiteID = ko.observable();
    self.SPListID = ko.observable();
    self.SPListItemID = ko.observable();
    self.SPTaskListID = ko.observable();
    self.SPTaskListItemID = ko.observable();
    self.Initiator = ko.observable();
    self.WorkflowInstanceID = ko.observable();
    self.WorkflowID = ko.observable();
    self.WorkflowName = ko.observable();
    self.StartTime = ko.observable();
    self.StateID = ko.observable();
    self.StateDescription = ko.observable();
    self.IsAuthorized = ko.observable();
    self.TaskTypeID = ko.observable();
    self.TaskTypeDescription = ko.observable();
    self.Title = ko.observable();
    self.ActivityTitle = ko.observable();
    self.User = ko.observable();
    self.IsSPGroup = ko.observable();
    self.EntryTime = ko.observable();
    self.ExitTime = ko.observable();
    self.OutcomeID = ko.observable();
    self.OutcomeDescription = ko.observable();
    self.CustomOutcomeID = ko.observable();
    self.CustomOutcomeDescription = ko.observable();
    self.Comments = ko.observable();
    self.ModePayment = ko.observable();//added 20-09-16

    //For visibility
    self.IsTransactionType = ko.observable(true);
    self.IsInvestmentID = ko.observable(true);
    self.IsInvestmentNew = ko.observable(true);
    self.IsFnaCore = ko.observable(true);
    self.IsFunctionType = ko.observable(true);
    self.IsAccoutType = ko.observable(true);
    self.IsSolId = ko.observable(true);
    self.csriskefecDate = ko.observable(true);
    self.RiskScore = ko.observable(true);
    self.ProfileExpDate = ko.observable(true);
    self.Account = ko.observable(true);
    self.MutualFund = ko.observable(true);
    self.IsAccount = ko.observable(false);
    self.IsAccountFNANO = ko.observable(false);
    self.IsPartial = ko.observable(false);
    self.IsNumberOfUnit = ko.observable(false);
    self.MutualFundRedemtion = ko.observable(false);
    self.MutualFundSubcription = ko.observable(false);
    self.MutualFundSavePlan = ko.observable(false);

    // value CIF underlying
    self.Treshold = ko.observable(0);
    self.FCYIDRTreshold = ko.observable(0);
    self.TotalRemainigByCIF = ko.observable(0);
    self.TotalTransFX = ko.observable(0);
    self.TotalUtilization = ko.observable(0);
    // Transaction Details
    self.TransactionAllNormal = ko.observable(TransactionAllNormalModel);
    self.TransactionAllFDNormal = ko.observable();
    self.TransactionAllUTNormal = ko.observable();
    //transaction Detail CIF by fandi
    self.TransactionALLCIFNormal = ko.observable();
    self.TransactionMakerCBO = ko.observable(TransactionCBO);
    //transaction detail TMO
    self.TransactionCheckerTMO = ko.observable(TransactionCheckerTMOModel);
    //add by fandi for Transaction history
    self.TransactionHistory = ko.observable(TransactionHistoryModel);
    //end
    self.ChargingAccCurrency = ko.observable(CurrencyModel);

    self.TransactionCheckerNetting = ko.observable(TransactionCheckerNettingModel);
    self.TransactionMakerNetting = ko.observable(TransactionMakerNettingModel);


    //Afif
    self.TransactionAllLoanNormal = ko.observable();

    self.TransactionMaker = ko.observable(TransactionMakerModel);
    self.TransactionChecker = ko.observable(TransactionCheckerModel);
    self.TransactionCheckerCallback = ko.observable(TransactionCheckerCallbackModel);
    self.TransactionContact = ko.observable(TransactionContactModel);
    self.TransactionCallback = ko.observable(TransactionCallbackModel);
    self.PaymentMaker = ko.observable(PaymentMakerModel);
    self.PaymentChecker = ko.observable(PaymentCheckerModel);
    self.TransactionDeal = ko.observable(TransactionDealModel);

    // filter Attach Underlying File
    self.AttachFilterFileName = ko.observable("");
    self.AttachFilterDocumentPurpose = ko.observable("");
    self.AttachFilterModifiedDate = ko.observable("");
    self.AttachFilterDocumentRefNumber = ko.observable("");

    // filter underlying
    self.UnderlyingFilterCIF = ko.observable("");
    self.UnderlyingFilterParentID = ko.observable("");
    self.UnderlyingFilterIsSelected = ko.observable(false);
    self.UnderlyingFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingFilterDocumentType = ko.observable("");
    self.UnderlyingFilterCurrency = ko.observable("");
    self.UnderlyingFilterStatementLetter = ko.observable("");
    self.UnderlyingFilterAmount = ko.observable("");
    self.UnderlyingFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingFilterExpiredDate = ko.observable("");
    self.UnderlyingFilterReferenceNumber = ko.observable("");
    self.UnderlyingFilterSupplierName = ko.observable("");
    self.UnderlyingFilterIsProforma = ko.observable("");

    // Task status from Nintex custom REST API
    self.IsPendingNintex = ko.observable();
    self.IsAuthorizedNintex = ko.observable();
    self.MessageNintex = ko.observable();

    // Task activity properties
    self.PPUChecker = ko.observable();

    //// start Checked Calculate for Attach Grid and Underlying Grid
    self.TempSelectedAttachUnderlying = ko.observableArray([]);

    //is fx transaction
    self.t_IsFxTransaction = ko.observable(false);
    self.t_IsFxTransactionToIDR = ko.observable(false);
    self.t_IsFxTransactionToIDRB = ko.observable(false);

    //properties
    self.LLDCode = ko.observable();
    // New Data flag
    self.IsNewData = ko.observable(false);

    // check all data
    self.CheckAll = ko.observable(false);

    // temp payment maker confirm
    self.BeneBanktmp = ko.observable('');
    self.SwiftCodetmp = ko.observable('');
    self.AccNumbertmp = ko.observable('');

    self.IsResidenttmp = ko.observable('');
    self.IsCitizentmp = ko.observable('');

    self.agentChargestmp = ko.observable('');
    self.bankChargestmp = ko.observable('');

    self.CargerBankOrig = ko.observable('');
    self.AgentChargesOrig = ko.observable('');

    // customer contact
    self.ID_c = ko.observable();
    self.CIF_c = ko.observable("");
    self.SourceID_c = ko.observable("2");
    self.Name_c = ko.observable("");
    self.PhoneNumber_c = ko.observable("");
    self.DateOfBirth_c = ko.observable('');
    self.Address_c = ko.observable("");
    self.IDNumber_c = ko.observable("");
    self.POAFunction_c = ko.observable();
    self.OccupationInID_c = ko.observable("");
    self.PlaceOfBirth_c = ko.observable("");
    self.EffectiveDate_c = ko.observable("");
    self.CancellationDate_c = ko.observable("");
    self.POAFunctionOther_c = ko.observable("");
    self.LastModifiedBy_c = ko.observable("");
    self.LastModifiedDate_c = ko.observable('');


    // customer underlying
    self.ID_u = ko.observable("");
    self.UnderlyingDocument_u = ko.observable('');
    self.DocumentType_u = ko.observable('');
    self.AmountUSD_u = ko.observable(0);
    self.Currency_u = ko.observable('');
    self.Rate_u = ko.observable(0);
    self.StatementLetter_u = ko.observable('');
    self.Amount_u = ko.observable(0);
    self.AttachmentNo_u = ko.observable("");
    self.IsDeclarationOfException_u = ko.observable(false);
    self.StartDate_u = ko.observable('');
    self.EndDate_u = ko.observable('');
    self.DateOfUnderlying_u = ko.observable("");
    self.ExpiredDate_u = ko.observable("");
    self.ReferenceNumber_u = ko.observable("");
    self.SupplierName_u = ko.observable("");
    self.InvoiceNumber_u = ko.observable("");
    self.IsProforma_u = ko.observable(false);
    self.IsBulkUnderlying_u = ko.observable(false);
    self.Proformas = ko.observableArray([]);
    self.BulkUnderlyings = ko.observableArray([]);
    self.ProformaDetails = ko.observableArray([]);
    self.BulkDetails = ko.observableArray([]);
    //add by fandi transaction history
    //self.GetTransactionMaker = ko.observableArray([]);
    //end

    // Temporary documents
    self.Documents = ko.observableArray([]);
    self.DocumentPath = ko.observable();
    self.DocumentType = ko.observable();
    self.DocumentPurpose = ko.observable();
    self.DocumentFileName = ko.observable();

    self.CurrencyCode = ko.observable("");
    self.CurrencyAccCode = ko.observable("");

    // add by adi
    //self.IsForceComplete = ko.observable("");
    self.ForceCompleteTransaction = ko.observable(false);

    // Original value chandra
    self.OriginalValue = ko.observable((new OriginalValueModel(new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false)

    )));

    // Chandra 2015.02.24 attachment properties for maker checker
    // Attach Variable
    self.InsertedURL = ko.observable();
    self.FileName = ko.observable();
    self.IsSuccess = ko.observable();
    self.ID_a = ko.observable(0);
    self.ddlDocumentPurpose_a = ko.observableArray([]);
    self.tempddlDocumentPurpose_a = ko.observableArray([]);
    self.ddlDocumentType_a = ko.observableArray([]);
    self.DocumentPurpose_a = ko.observable();
    self.DocumentPath_a = ko.observable();
    self.DocumentType_a = ko.observable();
    self.CustomerUnderlyingMappings_a = ko.observableArray([CustomerUnderlyingMappingModel]);
    self.MakerDocuments = ko.observableArray([]);
    self.MakerUnderlyings = ko.observableArray([]);
    self.NewDocuments = ko.observableArray([]);
    self.TransactionDF = ko.observable();
    self.OthersForDF = ko.observable(false);
    self.CurrencyFormat = function (amount) {
        if (amount !== null && amount !== undefined) {
            var val = amount;//Math.round(Number(amount.replace(/,/g, '')) * 100) / 100;
            var parts = val.toString().split(".");
            var num = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
            if (num === "0" || num === 0) return num = '';
            return num;
        }
    };
    self.ApprovalProcess = function (data) {
        if (data.Name == "Generate CSV File") {
            var model = {
                CIF: ko.observable(),
                FileTipe: ko.observable("ChangeRMCSV"),
                dtimeFile: ko.observable(""),
                DataInformation: ko.observableArray([]),
                ID: ko.observable(viewModel.TransactionMakerCBO().Transaction.ID)
            }
            GenerateCRM_CSV_Process(model);
        } else if (data.Name == "Generate Excel File") {
            var model = {
                CIF: ko.observable(),
                FileTipe: ko.observable("ChangeRMExcel"),
                dtimeFile: ko.observable(""),
                DataInformation: ko.observableArray([]),
                ID: ko.observable(viewModel.TransactionMakerCBO().Transaction.ID)
            }
            GenerateCRM_CSV_Process(model);
        }
    }
    self.ApprovalButton = function (name) {
        if (name == 'Continue') {
            return 'Submit';
        } else {
            return name;
        }
    }
    var AttachDocuments = {
        Documents: self.Documents,
        DocumentPath: self.DocumentPath_a,
        Type: self.DocumentType_a,
        Purpose: self.DocumentPurpose_a
    }

    var CustomerUnderlyingFile = {
        ID: self.ID_a,
        FileName: DocumentModels().FileName,
        DocumentPath: DocumentModels().DocumentPath,
        DocumentPurpose: self.DocumentPurpose_a,
        DocumentType: self.DocumentType_a,
        DocumentRefNumber: self.DocumentRefNumber_a,
        LastModifiedBy: self.LastModifiedBy_a,
        LastModifiedDate: self.LastModifiedDate_a,
        CustomerUnderlyingMappings: self.CustomerUnderlyingMappings_a
    };
    function SetUTVisibilty(data) {
        viewModel.IsTransactionType = ko.observable(false);
        viewModel.IsInvestmentID = ko.observable(false);
        viewModel.IsInvestmentNew = ko.observable(false);
        viewModel.IsFnaCore = ko.observable(false);
        viewModel.IsFunctionType = ko.observable(false);
        viewModel.IsAccoutType = ko.observable(false);
        viewModel.IsSolId = ko.observable(false);
        viewModel.csriskefecDate = ko.observable(false);
        viewModel.RiskScore = ko.observable(false);
        viewModel.ProfileExpDate = ko.observable(false);
        viewModel.Account = ko.observable(false);
        viewModel.MutualFund = ko.observable(false);
        viewModel.IsAccount = ko.observable(false);
        viewModel.IsAccountFNANO = ko.observable(false);
        viewModel.IsPartial = ko.observable(false);
        viewModel.IsNumberOfUnit = ko.observable(false);
        viewModel.MutualFundRedemtion = ko.observable(false);
        viewModel.MutualFundSubcription = ko.observable(false);
        viewModel.MutualFundSavePlan = ko.observable(false);

        switch (data.Transaction.Product.ID) {
            case ConsProductID.IDInvestmentProductIDCons:
                if (data.Transaction.IsFNACore == true && data.Transaction.AccountType == ConsUTPar.accSingle) {
                    if (data.Transaction.FunctionType != ConsUTPar.funcAdd) {
                        viewModel.IsInvestmentID = ko.observable(true);
                        viewModel.IsInvestmentNew = ko.observable(false);
                    } else {
                        viewModel.IsInvestmentID = ko.observable(false);
                        viewModel.IsInvestmentNew = ko.observable(true);
                    }
                    viewModel.IsFnaCore = ko.observable(true);
                    viewModel.IsFunctionType = ko.observable(true);
                    viewModel.IsAccoutType = ko.observable(true);
                    viewModel.IsSolId = ko.observable(true);
                    viewModel.csriskefecDate = ko.observable(true);
                    viewModel.RiskScore = ko.observable(true);
                    viewModel.ProfileExpDate = ko.observable(true);
                    viewModel.Account = ko.observable(true);
                    viewModel.MutualFund = ko.observable(false);
                    viewModel.IsAccount = ko.observable(false);
                    viewModel.IsAccountFNANO = ko.observable(false);
                    viewModel.IsTransactionType = ko.observable(false);

                } else if (data.Transaction.IsFNACore == true && data.Transaction.AccountType == ConsUTPar.accJoin) {
                    if (data.Transaction.FunctionType != ConsUTPar.funcAdd) {
                        viewModel.IsInvestmentID = ko.observable(true);
                        viewModel.IsInvestmentNew = ko.observable(false);
                    } else {
                        viewModel.IsInvestmentID = ko.observable(false);
                        viewModel.IsInvestmentNew = ko.observable(true);
                    }
                    viewModel.IsFnaCore = ko.observable(true);
                    viewModel.IsFunctionType = ko.observable(true);
                    viewModel.IsAccoutType = ko.observable(true);
                    viewModel.IsSolId = ko.observable(true);
                    viewModel.csriskefecDate = ko.observable(true);
                    viewModel.RiskScore = ko.observable(true);
                    viewModel.ProfileExpDate = ko.observable(true);
                    viewModel.Account = ko.observable(true);
                    viewModel.MutualFund = ko.observable(false);
                    viewModel.IsAccount = ko.observable(true);
                    viewModel.IsAccountFNANO = ko.observable(false);
                    viewModel.IsTransactionType = ko.observable(false);

                } else if (data.Transaction.IsFNACore == false && data.Transaction.AccountType == ConsUTPar.accSingle) {
                    if (data.Transaction.FunctionType != ConsUTPar.funcAdd) {
                        viewModel.IsInvestmentID = ko.observable(true);
                        viewModel.IsInvestmentNew = ko.observable(false);
                    } else {
                        viewModel.IsInvestmentID = ko.observable(false);
                        viewModel.IsInvestmentNew = ko.observable(true);
                    }
                    viewModel.IsFnaCore = ko.observable(true);
                    viewModel.IsFunctionType = ko.observable(true);
                    viewModel.IsAccoutType = ko.observable(true);
                    viewModel.IsSolId = ko.observable(true);
                    viewModel.csriskefecDate = ko.observable(false);
                    viewModel.RiskScore = ko.observable(false);
                    viewModel.ProfileExpDate = ko.observable(false);
                    viewModel.Account = ko.observable(true);
                    viewModel.MutualFund = ko.observable(false);
                    viewModel.IsAccount = ko.observable(false);
                    viewModel.IsAccountFNANO = ko.observable(false);
                    viewModel.IsTransactionType = ko.observable(false);

                } else if (data.Transaction.IsFNACore == false && data.Transaction.AccountType == ConsUTPar.accJoin) {
                    if (data.Transaction.FunctionType != ConsUTPar.funcAdd) {
                        viewModel.IsInvestmentID = ko.observable(true);
                        viewModel.IsInvestmentNew = ko.observable(false);
                    } else {
                        viewModel.IsInvestmentID = ko.observable(false);
                        viewModel.IsInvestmentNew = ko.observable(true);
                    }
                    viewModel.IsFnaCore = ko.observable(true);
                    viewModel.IsFunctionType = ko.observable(true);
                    viewModel.IsAccoutType = ko.observable(true);
                    viewModel.IsSolId = ko.observable(true);
                    viewModel.csriskefecDate = ko.observable(false);
                    viewModel.RiskScore = ko.observable(false);
                    viewModel.ProfileExpDate = ko.observable(false);
                    viewModel.Account = ko.observable(true);
                    viewModel.MutualFund = ko.observable(false);
                    viewModel.IsAccount = ko.observable(false);
                    viewModel.IsAccountFNANO = ko.observable(true);
                    viewModel.IsTransactionType = ko.observable(false);
                }

                break;
            case ConsProductID.UTOnshoreproductIDCons:
            case ConsProductID.UTOffshoreProductIDCons:
            case ConsProductID.UTCPFProductIDCons:
                var isSubscription = false;
                var isRedemtion = false;
                var isSwitching = false;
                for (var i = 0; i < ConsTransactionType.utSubcription.length; i++) {
                    if (ConsTransactionType.utSubcription[i] == data.Transaction.transactiontypeID) {
                        isSubscription = true;
                        break;
                    }
                }
                for (var i = 0; i < ConsTransactionType.utRedemption.length; i++) {
                    if (ConsTransactionType.utRedemption[i] == data.Transaction.transactiontypeID) {
                        isRedemtion = true;
                        break;
                    }
                }
                for (var i = 0; i < ConsTransactionType.utSwitching.length; i++) {
                    if (ConsTransactionType.utSwitching[i] == data.Transaction.transactiontypeID) {
                        isSwitching = true;
                        break;
                    }
                }
                if (isSubscription == true) {

                    viewModel.IsTransactionType = ko.observable(true);
                    viewModel.IsInvestmentID = ko.observable(true);
                    viewModel.IsInvestmentNew = ko.observable(false);
                    viewModel.IsFnaCore = ko.observable(false);
                    viewModel.IsFunctionType = ko.observable(false);
                    viewModel.IsAccoutType = ko.observable(false);
                    viewModel.IsSolId = ko.observable(false);
                    viewModel.csriskefecDate = ko.observable(false);
                    viewModel.RiskScore = ko.observable(false);
                    viewModel.ProfileExpDate = ko.observable(false);
                    viewModel.Account = ko.observable(false);
                    viewModel.MutualFund = ko.observable(false);
                    viewModel.IsAccount = ko.observable(false);
                    viewModel.IsAccountFNANO = ko.observable(false);
                    viewModel.IsPartial = ko.observable(false);
                    viewModel.IsNumberOfUnit = ko.observable(false);
                    viewModel.MutualFundRedemtion = ko.observable(false);
                    viewModel.MutualFundSubcription = ko.observable(true);
                    viewModel.MutualFundSavePlan = ko.observable(false);

                } else if (isRedemtion == true) {
                    viewModel.IsTransactionType = ko.observable(true);
                    viewModel.IsInvestmentID = ko.observable(true);
                    viewModel.IsInvestmentNew = ko.observable(false);
                    viewModel.IsFnaCore = ko.observable(false);
                    viewModel.IsFunctionType = ko.observable(false);
                    viewModel.IsAccoutType = ko.observable(false);
                    viewModel.IsSolId = ko.observable(false);
                    viewModel.csriskefecDate = ko.observable(false);
                    viewModel.RiskScore = ko.observable(false);
                    viewModel.ProfileExpDate = ko.observable(false);
                    viewModel.Account = ko.observable(false);
                    viewModel.MutualFund = ko.observable(false);
                    viewModel.IsAccount = ko.observable(false);
                    viewModel.IsAccountFNANO = ko.observable(false);
                    viewModel.MutualFundRedemtion = ko.observable(true);
                    viewModel.MutualFundSubcription = ko.observable(false);
                    viewModel.MutualFundSavePlan = ko.observable(false);
                    viewModel.IsPartial = ko.observable(true);
                    viewModel.IsNumberOfUnit = ko.observable(true);
                } else if (isSwitching == true) {
                    viewModel.IsTransactionType = ko.observable(true);
                    viewModel.IsInvestmentID = ko.observable(true);
                    viewModel.IsInvestmentNew = ko.observable(false);
                    viewModel.IsFnaCore = ko.observable(false);
                    viewModel.IsFunctionType = ko.observable(false);
                    viewModel.IsAccoutType = ko.observable(false);
                    viewModel.IsSolId = ko.observable(false);
                    viewModel.csriskefecDate = ko.observable(false);
                    viewModel.RiskScore = ko.observable(false);
                    viewModel.ProfileExpDate = ko.observable(false);
                    viewModel.Account = ko.observable(false);
                    viewModel.MutualFund = ko.observable(true);
                    viewModel.IsAccount = ko.observable(false);
                    viewModel.IsAccountFNANO = ko.observable(false);
                    viewModel.IsPartial = ko.observable(true);
                    viewModel.IsNumberOfUnit = ko.observable(true);
                    viewModel.MutualFundRedemtion = ko.observable(false);
                    viewModel.MutualFundSubcription = ko.observable(false);
                    viewModel.MutualFundSavePlan = ko.observable(false);
                }
                break;
            case ConsProductID.SavingPlanProductIDCons:
                viewModel.IsTransactionType = ko.observable(true);
                viewModel.IsInvestmentID = ko.observable(true);
                viewModel.IsInvestmentNew = ko.observable(false);
                viewModel.IsFnaCore = ko.observable(false);
                viewModel.IsFunctionType = ko.observable(false);
                viewModel.IsAccoutType = ko.observable(false);
                viewModel.IsSolId = ko.observable(false);
                viewModel.csriskefecDate = ko.observable(false);
                viewModel.RiskScore = ko.observable(false);
                viewModel.ProfileExpDate = ko.observable(false);
                viewModel.Account = ko.observable(false);
                viewModel.MutualFund = ko.observable(false);
                viewModel.IsAccount = ko.observable(false);
                viewModel.IsAccountFNANO = ko.observable(false);
                viewModel.MutualFundRedemtion = ko.observable(false);
                viewModel.MutualFundSubcription = ko.observable(false);
                viewModel.MutualFundSavePlan = ko.observable(true);
                break;

        }

    }
    //Bangkit 20141119
    self.Contact = ko.observable();
    self.Contacts = ko.observableArray([]);
    self.CalledContacts = ko.observableArray([]);

    self.OnCloseApproval = function () {
        self.TransactionMaker(TransactionMakerModel);
        self.TransactionAllNormal(TransactionAllNormalModel); //addall
        self.TransactionChecker(TransactionCheckerModel);
        self.TransactionCheckerCallback(TransactionCheckerCallbackModel);
        self.TransactionContact(TransactionContactModel);
        self.TransactionCallback(TransactionCallbackModel);
        self.PaymentMaker(PaymentMakerModel);
        self.PaymentChecker(PaymentCheckerModel);
        self.TransactionDeal(TransactionDealModel);
        self.IsOtherAccountNumber(false);

    }

    //region force complete
    self.ButtonForceComplete = function () {
        var text = "<br/>Do you want to <b>Force Complete</b> this task?";
        bootbox.confirm(text, function (result) {
            if (result) {
                viewModel.Comments("{{{" + viewModel.SPUser.DisplayName + "}}} " + viewModel.Comments());
                ForceCompletingTask(viewModel.SPTaskListID(), viewModel.SPTaskListItemID(), viewModel.TaskTypeID(), IDOutcomeComplete, viewModel.Comments());
            }
        });
    }

    function ForceCompletingTask(taskListID, taskListItemID, taskTypeID, outcomeID, comments) {
        var task = {
            TaskListID: taskListID,
            TaskListItemID: taskListItemID,
            TaskTypeID: taskTypeID,
            OutcomeID: outcomeID,
            Comment: comments
        };

        var endPointURL;
        // Flexi Task
        endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondFlexiTaskService";

        var options = {
            url: endPointURL,
            data: ko.toJSON(task)
        };

        Helper.Sharepoint.Nintex.Post(options, OnSuccessForceCompletingTask, OnError, OnAlways);
    }

    function OnSuccessForceCompletingTask(data, textStatus, jqXHR) {
        if (data.IsSuccess) {
            ShowNotification('Completing Task Success', jqXHR.responseJSON.Message, 'gritter-success', false);
            $("#modal-form").modal('hide');
            $("#transaction-progress").show();
            viewModel.GetData();
        } else {
            ShowNotification('Completing Task Failed', jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    //region Force Cancelled
    self.ButtonForceCancel = function () {
        var text = "<br/>Do you want to <b>Force Cancel</b> this task?";
        bootbox.confirm(text, function (result) {
            if (result) {
                viewModel.Comments("{{{" + viewModel.SPUser.DisplayName + "}}} " + viewModel.Comments());
                ForceCanceledTask(viewModel.SPTaskListID(), viewModel.SPTaskListItemID(), viewModel.TaskTypeID(), IDOutcomeCancel, viewModel.Comments());
            }
        });
    }

    function ForceCanceledTask(taskListID, taskListItemID, taskTypeID, outcomeID, comments) {
        var task = {
            TaskListID: taskListID,
            TaskListItemID: taskListItemID,
            TaskTypeID: taskTypeID,
            OutcomeID: outcomeID,
            Comment: comments
        };

        var endPointURL;
        // Flexi Task
        endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondFlexiTaskService";

        var options = {
            url: endPointURL,
            data: ko.toJSON(task)
        };

        Helper.Sharepoint.Nintex.Post(options, OnSuccessForceCanceledTask, OnError, OnAlways);
    }

    function OnSuccessForceCanceledTask(data, textStatus, jqXHR) {
        if (data.IsSuccess) {
            ShowNotification('Cancel Task Success', jqXHR.responseJSON.Message, 'gritter-success', false);
            $("#modal-form").modal('hide');
            $("#transaction-progress").show();
            viewModel.GetData();
        } else {
            ShowNotification('Completing Task Failed', jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // add by adi
    function IsForceCompleteTransaction() {

        var options = {
            type: "GET",
            url: api.server + api.url.transactionforcecomplete,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetForceComplete, OnError, OnAlways);
    }

    // On success GetData callback
    function OnSuccessGetForceComplete(data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            if (data != null) {
                if (self.ActivityTitle() == "IPE ACK1 Task" || self.ActivityTitle() == "IPE ACK2 Task") {
                    if (data !== undefined && data > 0) {
                        self.ForceCompleteTransaction(true);
                    }
                }
            }


        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    //end by adi

    self.SelectedContacts = function (contact) {
        if (self.TransactionCallback().Callbacks.length == self.UtcAttemp()) {
            ShowNotification("Warning", "You can only tick " + self.UtcAttemp() + " contacts.", "gritter-warning", false);
            //document.getElementById('checkbox' + selectedIndex).checked = false;
            return;
        }

        $("#modal-form-callbacktime").modal('show');

        //set selected contact
        var tanggal = new Date();
        var jam = tanggal.getHours() > 9 ? tanggal.getHours() : '0' + tanggal.getHours();
        var menit = tanggal.getMinutes() > 9 ? tanggal.getMinutes() : '0' + tanggal.getMinutes();
        $('#TimePickerCallBack').val(jam + ':' + menit);
        $('#NameCallback').val(contact.Name);
        $('.bootstrap-timepicker-hour').val("00");
        $('.bootstrap-timepicker-minute').val("00");
        document.getElementById('IsUtc').checked = false;

        $('#backDrop').show();

        $('.time-picker').timepicker({
            defaultTime: "",
            minuteStep: 1,
            showSeconds: false,
            showMeridian: false
        }).next().on(ace.click_event, function () {
            $(this).prev().focus();
        });

        // validation
        $('#aspnetForm').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: true,

            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },

            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                $(e).remove();
            },

            errorPlacement: function (error, element) {
                if (element.is(':checkbox') || element.is(':radio')) {
                    var controls = element.closest('div[class*="col-"]');
                    if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if (element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if (element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            }
        });

        self.Contact(contact);
    }

    self.TempCallBackTime = ko.observableArray([]);

    self.CloseCallBackTime = function () {
        $("#modal-form-callbacktime").modal('hide');

        $('#backDrop').hide();
    }

    self.TimeFormat = function (data) {
        if (data.length > 5) {
            var tanggal = new Date(data);
            return tanggal.format('HH:mm');
            // var jam = tanggal.getUTCHours() > 9 ? tanggal.getUTCHours() : '0' + tanggal.getUTCHours();
            // var menit = tanggal.getUTCMinutes() > 9 ? tanggal.getUTCMinutes() : '0' + tanggal.getUTCMinutes();
            // var result = jam + ':' + menit;
            // return result;
        } else {
            return data;
        }
    }

    //start azam add grid sknbulk
    //add aridya 20161101 skn bulk ~OFFLINE~
    self.ShowDuplicateData = function () {
        //aridya 20161201 change to w2ui
        for (var ii = 0; ii < viewModel.DuplicateRowSKNBulk().length; ii++) {
            w2ui['SKNBulkWorksheetTable'].set(viewModel.DuplicateRowSKNBulk()[ii], { w2ui: { style: "background-color: red" } });
        }
        $("#messageDuplicateDataSKNBulk").modal('hide');
        $('#backDrop').hide();
        //end aridya

        //$('#SKNBulkWorksheetTable').removeClass("table-striped table-hover");
        //for (var ii = 0; ii < viewModel.DuplicateRowSKNBulk().length; ii++) {
        //    $('#SKNBulkWorksheetTable tr').eq(viewModel.DuplicateRowSKNBulk()[ii] + 1).css({ "background-color": "red" });
        //}
        //$("#messageDuplicateDataSKNBulk").modal('hide');
        //$('#backDrop').hide();

        //var text = $('#messageDuplicateDataSKNBulk').html(); //;"<h4><b style='color:#FF0000;'>Can not Submit Transaction !!!</b></h4><div><h5><b>There are duplicate data in the SKN Bulk Worksheet</b></h5></div>";
        //bootbox.alert(text, function (result) {
        //    $('#SKNBulkWorksheetTable').removeClass("table-striped table-hover");
        //    for (var ii = 0; ii < viewModel.DuplicateRowSKNBulk().length; ii++) {
        //        $('#SKNBulkWorksheetTable tr').eq(viewModel.DuplicateRowSKNBulk()[ii] + 1).css({ "background-color": "red" });
        //    }
        //});
    }

    //add aridya 20161201 render grid using w2ui for sknbulk
    self.RenderGridSKNBulk = function (data, gridName) {
        $().w2grid({
            name: gridName,
            show: {
                footer: true,
                //toolbar    : true
            },
            columns: [
                { field: 'recid', caption: 'No.', size: '50px', resizable: true },
                { field: 'AccountNumber', caption: 'Debit Account No', size: '140px', resizable: true },
                { field: 'CustomerName', caption: 'Customer Name', size: '130px' },
                {
                    field: 'ValueDate', caption: 'Value Date', size: '105px', resizable: true, render: function (record, index, col_index) {
                        var html = w2utils.formatDate(record.ValueDate, 'dd/mm/yyyy');
                        return html;
                    }
                },
                { field: 'PaymentAmount', caption: 'Payment Amount', size: '140px', resizable: true },
                { field: 'BeneficiaryName', caption: 'Beneficiary Name', size: '140px', resizable: true },
                { field: 'BeneficiaryAccountNumber', caption: 'Beneficiary Account Number', size: '215px', resizable: true },
                { field: 'BankCode', caption: 'Bank Code', size: '95px', resizable: true, style: "background-color: yellow" }, //editable: { type: 'text' },
                { field: 'BranchCode', caption: 'Branch Code', size: '105px', resizable: true, style: "background-color: yellow" }, //editable: { type: 'text' },
                { field: 'Citizen', caption: 'Citizen', size: '65px', resizable: true },
                { field: 'Resident', caption: 'Resident', size: '80px', resizable: true },
                { field: 'PaymentDetails', caption: 'Payment Details', size: '165px', resizable: true },
                { field: 'Charges', caption: 'Charges', size: '75px', resizable: true },
                { field: 'Type', caption: 'Type', size: '50px', resizable: true },
                { field: 'BeneficiaryCustomer', caption: 'Beneficiary Customer', size: '165px', resizable: true },
            ]
        });

        for (var ii = 0; ii < data.length; ii++) {
            data[ii].recid = ii + 1;
        }

        w2ui[gridName].records = data;
        w2ui[gridName].refresh();
        $('#' + gridName).w2render(gridName);
    }

    //add aridya 20161205 render grid using w2ui for sknbulk
    self.RenderGridSKNBulkChecker = function (data, gridName) {
        $().w2grid({
            name: gridName,
            show: {
                footer: true,
                //toolbar    : true
            },
            columns: [
                { field: 'recid', caption: 'No.', size: '50px', resizable: true },
                { field: 'AccountNumber', caption: 'Debit Account No', size: '140px', resizable: true },
                { field: 'CustomerName', caption: 'Customer Name', size: '130px' },
                {
                    field: 'ValueDate', caption: 'Value Date', size: '105px', resizable: true, render: function (record, index, col_index) {
                        var html = w2utils.formatDate(record.ValueDate, 'dd/mm/yyyy');
                        return html;
                    }
                },
                { field: 'PaymentAmount', caption: 'Payment Amount', size: '140px', resizable: true },
                { field: 'BeneficiaryName', caption: 'Beneficiary Name', size: '140px', resizable: true },
                { field: 'BeneficiaryAccountNumber', caption: 'Beneficiary Account Number', size: '215px', resizable: true },
                { field: 'BankCode', caption: 'Bank Code', size: '95px', resizable: true },
                { field: 'BranchCode', caption: 'Branch Code', size: '105px', resizable: true },
                { field: 'Citizen', caption: 'Citizen', size: '65px', resizable: true },
                { field: 'Resident', caption: 'Resident', size: '80px', resizable: true },
                { field: 'PaymentDetails', caption: 'Payment Details', size: '165px', resizable: true },
                { field: 'Charges', caption: 'Charges', size: '75px', resizable: true },
                { field: 'Type', caption: 'Type', size: '50px', resizable: true },
                { field: 'BeneficiaryCustomer', caption: 'Beneficiary Customer', size: '165px', resizable: true },
            ]
        });

        for (var ii = 0; ii < data.length; ii++) {
            data[ii].recid = ii + 1;
        }

        w2ui[gridName].records = data;
        w2ui[gridName].refresh();
        $('#' + gridName).w2render(gridName);
    }

    //end grid sknbulk

    self.SearchTZ = function () {
        viewModel.IsSearchTZ(true);
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckAvailbaleTZ/" + viewModel.PaymentMaker().Payment.TZNumber,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                viewModel.TZModel(ko.mapping.toJS(data));
                viewModel.IsSearchTZ(false);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                viewModel.IsSearchTZ(false);
            }
        });
    }

    self.ApprovalButton = function (name) {
        if (name == 'Continue') {
            return 'Submit';
        } else {
            return name;
        }
    }

    // bind clear filters
    self.UnderlyingClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.UnderlyingFilterIsSelected(false);
        self.UnderlyingFilterUnderlyingDocument("");
        self.UnderlyingFilterDocumentType("");
        self.UnderlyingFilterCurrency("");
        self.UnderlyingFilterStatementLetter("");
        self.UnderlyingFilterAmount("");
        self.UnderlyingFilterDateOfUnderlying("");
        self.UnderlyingFilterExpiredDate("");
        self.UnderlyingFilterReferenceNumber("");
        self.UnderlyingFilterSupplierName("");
        self.UnderlyingFilterIsProforma("");

        GetDataUnderlying();
    };

    self.PendingDocumentsClearFilters = function () {
        self.FilterApplicationID("");
        self.FilterSignatureVerified("");
        self.FilterDocument("");
        self.FilterCustomerName("");
        self.FilterProductName("");
        self.FilterCurrencyDesc("");
        self.FilterTransactionAmount("");
        self.FilterEqvUSD("");
        self.FilterFxTransaction("");
        self.FilterTransactionStatus("");
        self.FilterLastModifiedBy("");
        self.FilterLastModifiedDate("");

        GetPendingDocuments();
    }

    //init transaction callback ( set data if exist )
    self.initTransactionCallback = function () {
        for (var i = 0; i <= self.TransactionCallback().Callbacks.length - 1; i++) {

            //set grid contact
            var isSelected = ko.utils.arrayFirst(self.TransactionCallback().Transaction.Customer.Contacts, function (data) {

                var indexOfContact = self.TransactionCallback().Transaction.Customer.Contacts.indexOf(self.TransactionCallback().Callbacks[i].Contact);

                if (data.ID == self.TransactionCallback().Callbacks[i].Contact.ID) {
                    document.getElementById('checkbox' + indexOfContact).checked = true;
                } else {
                    document.getElementById('checkbox' + indexOfContact).checked = false;
                }
            });
        }
    }

    //add by fandi transaction History
    self.ViewHistory = function () {
        $('#backDrop').hide();
        $("#modal-form-VieHistory").attr('style', 'background-color: #000; opacity:1; z-index:1000;');
        $("#modal-form-VieHistory").modal('show');
        $("li").removeClass("active");
    }

    self.CloseHistory = function () {

        $("#modal-form-VieHistory").modal('hide');
        $('#backDrop').hide();
    }
    //end


    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyings = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyingFiles = ko.observableArray([]);

    // grid properties Underlying
    self.UnderlyingGridProperties = ko.observable();
    self.UnderlyingGridProperties(new GridPropertiesModel(GetDataUnderlying));

    // grid properties Attach Grid
    self.AttachGridProperties = ko.observable();
    self.AttachGridProperties(new GridPropertiesModel(GetDataAttachFile));

    // set default sorting for Underlying Grid
    self.UnderlyingGridProperties().SortColumn("ID");
    self.UnderlyingGridProperties().SortOrder("ASC");

    // set default sorting for Underlying File Grid
    self.AttachGridProperties().SortColumn("FileName");
    self.AttachGridProperties().SortOrder("ASC");

    // Get filtered columns value
    function GetUnderlyingFilteredColumns() {
        var filters = [];

        //if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingFilterIsSelected() });
        if (self.UnderlyingFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingFilterUnderlyingDocument() });
        if (self.UnderlyingFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingFilterDocumentType() });
        if (self.UnderlyingFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingFilterCurrency() });
        if (self.UnderlyingFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingFilterStatementLetter() });
        if (self.UnderlyingFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingFilterAmount() });
        if (self.UnderlyingFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingFilterDateOfUnderlying() });
        if (self.UnderlyingFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingFilterExpiredDate() });
        if (self.UnderlyingFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingFilterReferenceNumber() });
        if (self.UnderlyingFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingFilterSupplierName() });

        return filters;
    };

    self.GetCalculateFX = function (amountUSD) {
        GetCalculateFX(amountUSD);
    }

    self.GetDataUnderlying = function () {
        GetDataUnderlying();
    };

    self.GetDataAttachFile = function () {
        GetDataAttachFile();
    }

    self.GetTotalAmount = function (CIFData, AmountUSD) {
        GetTotalAmount(CIFData, AmountUSD);
    }

    self.GetTotalAmountFX = function (CIFData, currentAmount) {
        GetTotalAmountFX(CIFData, currentAmount);
    }

    self.GetFCYIDRTreshold = function () {
        GetFCYIDRTreshold();
    }

    function ResetDataAttachment(index, isSelect) {
        self.CustomerAttachUnderlyings()[index].IsSelectedAttach = isSelect;
        var dataRows = self.CustomerAttachUnderlyings().slice(0);
        self.CustomerAttachUnderlyings([]);
        self.CustomerAttachUnderlyings(dataRows);
    }

    function GetCalculateFX(amountUSD) {
        AmountModel.TotalPPUAmountsUSD(TotalPPUModel.Total.IDRFCYPPU);
        AmountModel.TotalDealAmountsUSD(TotalDealModel.Total.IDRFCYDeal);
        AmountModel.RemainingBalance(TotalDealModel.Total.RemainingBalance);
        AmountModel.TotalPPUUtilization(TotalPPUModel.Total.UtilizationPPU);
        AmountModel.TotalDealUtilization(TotalDealModel.Total.UtilizationDeal);
    }

    function SetCalculateFX(amountUSD) {
        if (GetModelTransaction().StatementLetter != null && (GetModelTransaction().StatementLetter.ID == 4)) {
            AmountModel.RemainingBalance(0.00);
        } else if (GetModelTransaction().StatementLetter != null && GetModelTransaction().StatementLetter.ID != 4) {
            AmountModel.RemainingBalance(TotalDealModel.Total.RemainingBalance);
        }
        AmountModel.TotalPPUAmountsUSD(TotalDealModel.Total.IDRFCYDeal);
        AmountModel.TotalDealAmountsUSD(TotalDealModel.Total.IDRFCYDeal);
        AmountModel.TotalPPUUtilization(TotalPPUModel.Total.UtilizationPPU);
        AmountModel.TotalDealUtilization(TotalDealModel.Total.UtilizationDeal);
    }

    function SetCalculateFXChecker(amountUSD) {
        if (GetModelTransaction().StatementLetter != null && (GetModelTransaction().StatementLetter.ID == 4)) {
            AmountModel.RemainingBalance(0.00);
        } else if (GetModelTransaction().StatementLetter != null && GetModelTransaction().StatementLetter.ID != 4) {
            AmountModel.RemainingBalance(TotalDealModel.Total.RemainingBalance + parseFloat(amountUSD));
        }
        AmountModel.TotalPPUAmountsUSD(TotalDealModel.Total.IDRFCYDeal);
        AmountModel.TotalDealAmountsUSD(TotalDealModel.Total.IDRFCYDeal);
        AmountModel.TotalPPUUtilization(TotalPPUModel.Total.UtilizationPPU);
        AmountModel.TotalDealUtilization(TotalDealModel.Total.UtilizationDeal);
    }

    self.DealCalculate = function () {

        var t_IsFxTransaction = (viewModel.TransactionDeal().Transaction.Currency.Code != 'IDR' && viewModel.TransactionDeal().Transaction.AccountCurrencyCode == 'IDR') ? true : false;
        var t_IsFxTransactionToIDR = (viewModel.TransactionDeal().Transaction.Currency.Code == 'IDR' && viewModel.TransactionDeal().Transaction.AccountCurrencyCode != 'IDR') ? true : false;
        var t_IsFxTransactionToIDRB = (viewModel.TransactionDeal().Transaction.Currency.Code == 'IDR' && viewModel.TransactionDeal().Transaction.AccountCurrencyCode != 'IDR' && viewModel.TransactionDeal().Transaction.ProductType.IsFlowValas == true) ? true : false;

        viewModel.t_IsFxTransaction(t_IsFxTransaction);
        viewModel.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);
        viewModel.t_IsFxTransactionToIDRB(t_IsFXTransactionToIDRB);

        var t_TotalAmountsUSD = 0;
        var t_TotalAmountsUSD_stB = 0;

        var t_RemainingBalance = 0;

        var t_TotalDealTransaction = parseFloat(viewModel.AmountModel().TotalAmounts());
        var t_TotalDealTransactionB = isNaN(parseFloat(viewModel.AmountModel().TotalAmountsStatementB())) ? 0 : parseFloat(viewModel.AmountModel().TotalAmountsStatementB()); //avoid NaN

        var t_FcyAmount = parseFloat(viewModel.TransactionDeal().Transaction.AmountUSD);
        var t_IsFxTransaction = viewModel.t_IsFxTransaction();
        var t_IsFxTransactionToIDR = viewModel.t_IsFxTransactionToIDR();
        var t_IsFXTransactionToIDRB = viewModel.t_IsFxTransactionToIDRB();

        var t_Treshold = parseFloat(viewModel.AmountModel().TreshHold());
        var t_RemainingBalanceByCIF = viewModel.AmountModel().TotalRemainigByCIF();

        /* START get Amount USD Trx Total */
        if (t_IsFxTransaction) {
            t_TotalAmountsUSD = t_TotalDealTransaction;
            t_TotalAmountsUSD_stB = t_TotalDealTransactionB;
        }
        else if (t_IsFxTransactionToIDR) {
            t_TotalAmountsUSD = t_TotalDealTransaction;
            t_TotalAmountsUSD_stB = t_TotalDealTransactionB;
        }
        else {
            t_TotalAmountsUSD = t_TotalDealTransaction;
            t_TotalAmountsUSD_stB = t_TotalDealTransactionB;
        }
        viewModel.AmountModel().TotalDealAmountsUSD(t_TotalAmountsUSD);
        /* END OF get Amount USD Trx Total */

        /* START get Remaining Underlying Balance */
        if (viewModel.TransactionDeal().Transaction.StatementLetter != undefined) {
            if (viewModel.TransactionDeal().Transaction.StatementLetter.ID == 1) {

                // begin if statement A
                if (t_TotalAmountsUSD > t_Treshold) {
                    //t_RemainingBalance = t_Treshold - t_TotalAmountsUSD;
                    t_RemainingBalance = t_RemainingBalanceByCIF - t_TotalAmountsUSD_stB;
                }
                else {
                    t_RemainingBalance = 0.00;
                }
            }
            else {
                // begin if statement B
                t_RemainingBalance = t_RemainingBalanceByCIF - t_TotalAmountsUSD_stB;
            }
            if (t_IsFxTransactionToIDR) {
                if (t_FcyAmount >= self.FCYIDRTreshold() && t_IsFxTransactionToIDRB == true) {
                    t_RemainingBalance = t_RemainingBalance - t_FcyAmount;
                } else {
                    t_RemainingBalance = t_RemainingBalance;
                }
            }

            viewModel.AmountModel().RemainingBalance(t_RemainingBalance);
        }
    }

    // Get filtered columns value
    function GetAttachFilteredColumns() {
        var filters = [];

        if (self.AttachFilterFileName() != "") filters.push({ Field: 'FileName', Value: self.AttachFilterFileName() });
        if (self.AttachFilterDocumentPurpose() != "") filters.push({ Field: 'DocumentPurpose', Value: self.AttachFilterDocumentPurpose() });
        if (self.AttachFilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.AttachFilterModifiedDate() });
        if (self.AttachFilterDocumentRefNumber() != "") filters.push({ Field: 'DocumentRefNumber', Value: self.AttachFilterDocumentRefNumber() });

        return filters;
    };

    function GetCustomerUnderlyingFilters() {
        var filters = [];
        var _cif = viewModel.TransactionAllNormal().Transaction.Customer.CIF;
        filters.push({ Field: 'CIF', Value: _cif });
        return filters;
    }

    function GetTreshold(cif_d) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.singlevalueparameter + "/FX_TRANSACTION_TRESHOLD",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }, success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.Treshold(data);
                    GetAvailableStatementB(cif_d);
                }
            }
        });
    }

    function GetFCYIDRTreshold() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.singlevalueparameter + "/FCY_IDR_TRANSACTION_TRESHOLD",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }, success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.FCYIDRTreshold(data);
                }
            }
        });
    }

    function GetTotalAmountFX(cif, currentAmountUSD) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.transaction + "/GetAmountTransaction=" + cif,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }, success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if (data == undefined) {
                        viewModel.AmountModel().TotalDealAmountsUSD(0);
                    } else {
                        //self.TransactionModel().TotalTransFX(parseFloat(data));
                        viewModel.AmountModel().TotalDealAmountsUSD(parseFloat(data) - parseFloat(currentAmountUSD));
                    }
                }
            }
        });

    }

    function GetTotalAmount(CIFData, AmountUSD) {
        var totalValue1 = $.ajax({
            type: "GET",
            url: api.server + api.url.transactiondeal + "/GetAmountTransaction=" + CIFData + "?isStatementB=false",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }
        });
        var totalValue2 = $.ajax({
            type: "GET",
            url: api.server + api.url.transactiondeal + "/GetAmountTransaction=" + CIFData + "?isStatementB=true",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }
        });
        $.when(totalValue1, totalValue2).done(function (data1, data2) {
            var t_TotalAmounts = 0;
            if (data1 != null || data1 != undefined) {
                var t_TotalAmounts1 = parseFloat(data1);
                var t_TotalAmounts2 = parseFloat(data2);

                self.AmountModel().TotalAmounts(t_TotalAmounts2);
                self.AmountModel().TotalAmountsStatementB(t_TotalAmounts1);
                self.DealCalculate();
            }
            else {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetTotalAmount(CIFData, IsStatementB) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.transactiondeal + "/GetAmountTransaction=" + CIFData + "?isStatementB=" + IsStatementB,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {

                var t_TotalAmounts = 0;
                if (jqXHR.status = 200) {
                    var t_TotalAmounts = parseFloat(data);
                    if (IsStatementB == true) {
                        self.AmountModel().TotalAmounts(t_TotalAmounts);
                    }
                    else {
                        self.AmountModel().TotalAmountsStatementB(t_TotalAmounts);
                    }
                }
                else if (jqXHR.status = 204) {
                    if (IsStatementB == true) {
                        self.AmountModel().TotalAmounts(t_TotalAmounts);
                    }
                    else {
                        self.AmountModel().TotalAmountsStatementB(t_TotalAmounts);
                    }
                }
                else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetAvailableStatementB(cif_d) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetAvailableStatementB/" + cif_d,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                var t_TotalRemainig = 0;
                if (jqXHR.status = 200) {
                    t_TotalRemainig = data;
                    viewModel.AmountModel().TotalRemainigByCIF(t_TotalRemainig);

                } else {

                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }


    function GetModelTransaction() {
        var model;
        switch (self.ActivityTitle()) {
            case "CBO Maker Task":
                model = self.TransactionContact().Transaction;
                break;
            case "CBO Checker Task":
                model = self.TransactionContact().Transaction;
                break;
            case "FX Deal Checker Task":
            case "FX Deal Maker Task":
            case "FX Deal TMO Checker Task":
            case "FX TMO Maker After Revise Task":
            case "FX Deal TMO Maker After Submit Task":
            case "FX Deal TMO Checker Request Cancel Task":
                model = self.TransactionDeal().Transaction;
                break;
            case "Pending Documents Checker Task":
                model = self.TransactionAllNormal().Transaction; // addall
                break;
            default:
                model = self.TransactionAllNormal().Transaction;
                break;
        }
        return model;
    }

    function GetUnderlyingFile() {
        var options = {
            url: api.server + api.url.customerunderlyingfile + "/TransactionDeal/" + DealID,
            token: accessToken
        };

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataAttachFile, OnError, OnAlways);

        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        }
    }

    function GetDataAttachFile() {
        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlyingfile + "/TransactionDeal/" + DealID,
            params: {
                page: self.AttachGridProperties().Page(),
                size: self.AttachGridProperties().Size(),
                sort_column: self.AttachGridProperties().SortColumn(),
                sort_order: self.AttachGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataAttachFile, OnError, OnAlways);

        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        }
    }

    // On success GetData callback
    function OnSuccessGetDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerUnderlyingFiles(data.Rows);

            self.AttachGridProperties().Page(data['Page']);
            self.AttachGridProperties().Size(data['Size']);
            self.AttachGridProperties().Total(data['Total']);
            self.AttachGridProperties().TotalPages(Math.ceil(self.AttachGridProperties().Total() / self.AttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }
    function SetSwapTypeTransaction() {
        var swap = GetModelTransaction().SwapType;
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetSwapDataTransaction/" + GetModelTransaction().NB + "/" + GetModelTransaction().TZNumber,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if (data != null) {
                        self.IsSwapTransaction(true);
                        self.MessageTransactionType('Swap transaction');
                        self.Swap_NettingDFNumber(data.DFNumber);
                        GetModelTransaction().SwapTransactionID = data.SwapTransactionID;
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });

    }


    function SetNettingTypeTransaction() {
        var isNetting = GetModelTransaction().IsNettingTransaction;
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetNettingDataTransaction/" + GetModelTransaction().TZNumber,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if (data != null) {
                        self.IsNettingTransaction(true);
                        self.MessageTransactionType('Netting transaction');
                        self.Swap_NettingDFNumber(data.DFNumber);
                        //GetModelTransaction().Transaction.SwapTransactionID(data.SwapTransactionID);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });

    }

    function GetDataUnderlying() {
        // widget reloader function start
        var options = {
            url: api.server + api.url.customerunderlying + "/TransactionDeal/" + DealID,
            params: {
                page: self.UnderlyingGridProperties().Page(),
                size: self.UnderlyingGridProperties().Size(),
                sort_column: self.UnderlyingGridProperties().SortColumn(),
                sort_order: self.UnderlyingGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetUnderlyingFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        }
    }

    // On success GetData callback 1
    function OnSuccessGetDataUnderlying(data, textStatus, jqXHR) {

        if (jqXHR.status = 200) {

            var UtilizationAmount = 0;
            for (i = 0; i < data['Total']; i++) {
                UtilizationAmount = UtilizationAmount + data.Rows[i].UtilizationAmount;

            }

            self.CustomerUnderlyings(data.Rows);
            self.UnderlyingGridProperties().Page(data['Page']);
            self.UnderlyingGridProperties().Size(data['Size']);
            self.UnderlyingGridProperties().Total(data['Total']);
            self.UnderlyingGridProperties().TotalPages(Math.ceil(self.UnderlyingGridProperties().Total() / self.UnderlyingGridProperties().Size()));

            self.UtilizationAmounts(UtilizationAmount);
            GetDataAttachFile();
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    //added chandra
    self.GetUnderlyingSelectedRow = function (data) {

        $("#modal-form-Underlying").modal('show');

        self.ID_u(ko.isObservable(data.ID) ? data.ID() : data.ID);
        self.Amount_u(ko.isObservable(data.Amount) ? data.Amount() : data.Amount);
        self.UnderlyingDocument_u((ko.isObservable(data.UnderlyingDocument.Code) ? data.UnderlyingDocument.Code() : data.UnderlyingDocument.Code) + " (" + (ko.isObservable(data.UnderlyingDocument.Name) ? data.UnderlyingDocument.Name() : data.UnderlyingDocument.Name) + ")");
        //self.OtherUnderlying_u(data.OtherUnderlying);
        self.DocumentType_u(ko.isObservable(data.DocumentType.Name) ? data.DocumentType.Name() : data.DocumentType.Name);

        //self.Currency_u(data.Currency);
        self.Currency_u((ko.isObservable(data.Currency.Code) ? data.Currency.Code() : data.Currency.Code) + " (" + (ko.isObservable(data.Currency.Description) ? data.Currency.Description() : data.Currency.Description) + ")");

        self.Rate_u(ko.isObservable(data.Rate) ? data.Rate() : data.Rate);
        self.AmountUSD_u(ko.isObservable(data.AmountUSD) ? data.AmountUSD() : data.AmountUSD);
        self.AttachmentNo_u(ko.isObservable(data.AttachmentNo) ? data.AttachmentNo() : data.AttachmentNo);

        //self.StatementLetter_u(data.StatementLetter);
        self.StatementLetter_u(ko.isObservable(data.StatementLetter.Name) ? data.StatementLetter.Name() : data.StatementLetter.Name);

        self.IsDeclarationOfException_u(ko.isObservable(data.IsDeclarationOfException) ? data.IsDeclarationOfException() : data.IsDeclarationOfException);
        self.StartDate_u(self.LocalDate(data.StartDate, true, false));
        self.EndDate_u(self.LocalDate(data.EndDate, true, false));
        self.DateOfUnderlying_u(self.LocalDate(data.DateOfUnderlying, true, false));
        self.ExpiredDate_u(self.LocalDate(data.ExpiredDate, true, false));
        self.ReferenceNumber_u(ko.isObservable(data.ReferenceNumber) ? data.ReferenceNumber() : data.ReferenceNumber);
        self.SupplierName_u(ko.isObservable(data.SupplierName) ? data.SupplierName() : data.SupplierName);
        self.InvoiceNumber_u(ko.isObservable(data.InvoiceNumber) ? data.InvoiceNumber() : data.InvoiceNumber);
        self.IsProforma_u(ko.isObservable(data.IsProforma) ? data.IsProforma() : data.IsProforma);
        self.IsBulkUnderlying_u(ko.isObservable(data.IsBulkUnderlying) ? data.IsBulkUnderlying() : data.IsBulkUnderlying);
        self.Proformas(ko.isObservable(data.Proformas) ? data.Proformas() : data.Proformas);
        self.BulkUnderlyings(ko.isObservable(data.BulkUnderlyings) ? data.BulkUnderlyings() : data.BulkUnderlyings);
        self.ProformaDetails(ko.isObservable(data.ProformaDetails) ? data.ProformaDetails() : data.ProformaDetails);
        self.BulkDetails(ko.isObservable(data.BulkDetails) ? data.BulkDetails() : data.BulkDetails);

        $('#backDrop').show();
    }


    self.ReplaceBoolean = function (value) {
        if (value == true) return "yes";
        else return "no";
    }

    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    self.GridProperties().SortColumn("LastModifiedDate");//Rizki - 2016-01-09
    self.GridProperties().SortOrder("DESC");

    // Options selected value
    self.Selected = ko.observable(SelectedModel);

    // filters
    self.FilterWorkflowName = ko.observable("");
    self.FilterInitiator = ko.observable("");
    self.FilterStateDescription = ko.observable("");
    self.FilterStartTime = ko.observable("");
    self.FilterTitle = ko.observable("");
    self.FilterTransactionStatus = ko.observable("");
    self.FilterUser = ko.observable("");
    self.FilterUserApprover = ko.observable("");
    self.FilterEntryTime = ko.observable("");
    self.FilterExitTime = ko.observable("");
    self.FilterOutcomeDescription = ko.observable("");
    self.FilterCustomOutcomeDescription = ko.observable("");
    self.FilterComments = ko.observable("");

    // filters transaction
    self.FilterApplicationID = ko.observable("");
    self.FilterCustomer = ko.observable("");
    self.FilterProduct = ko.observable("");
    self.FilterCurrency = ko.observable("");
    self.FilterAmount = ko.observable("");
    self.FilterAmountUSD = ko.observable("");
    self.FilterDebitAccNumber = ko.observable("");
    self.FilterFXTransaction = ko.observable("");
    self.FilterTopUrgent = ko.observable("");
    self.FilterTransactionStatus = ko.observable("");
    self.FilterBranchCode = ko.observable(""); //Rizki 2016-01-09
    self.FilterBranchName = ko.observable(""); //Rizki 2016-01-09

    //Declare an ObservableArray for Storing the JSON Response
    self.Tasks = ko.observableArray([]);
    self.Outcomes = ko.observableArray([]);

    // customer contact
    var CustomerContact = {
        ID: self.ID_c,
        CIF: self.CIF_c,
        SourceID: self.SourceID_c,
        Name: self.Name_c,
        PhoneNumber: self.PhoneNumber_c,
        DateOfBirth: self.DateOfBirth_c,
        Address: self.Address_c,
        IDNumber: self.IDNumber_c,
        POAFunction: self.POAFunction_c,
        OccupationInID: self.OccupationInID_c,
        PlaceOfBirth: self.PlaceOfBirth_c,
        EffectiveDate: self.EffectiveDate_c,
        CancellationDate: self.CancellationDate_c,
        POAFunctionOther: self.POAFunctionOther_c,
        LastModifiedBy: self.LastModifiedBy_c,
        LastModifiedDate: self.LastModifiedDate_c
    };

    self.AddContact = function () {
        self.IsNewData(true);
        self.ClearData();

        $("#modal-form-child").modal('show');

        $('#backDrop').show();

        // datepicker
        $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
            $(this).prev().focus();
        });

        // validation
        $('#aspnetForm').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: true,

            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },

            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                $(e).remove();
            },

            errorPlacement: function (error, element) {
                if (element.is(':checkbox') || element.is(':radio')) {
                    var controls = element.closest('div[class*="col-"]');
                    if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if (element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if (element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            }
        });
    }

    self.ClearData = function () {
        self.ID_c(0);
        self.Name_c('');
        self.PhoneNumber_c('');
        self.DateOfBirth_c('');
        self.Address_c('');
        self.IDNumber_c('');
        self.Selected().POAFunction('');
        self.OccupationInID_c('');
        self.PlaceOfBirth_c('');
        self.EffectiveDate_c('');
        self.CancellationDate_c('');
        self.POAFunctionOther_c('');
    }

    self.Close = function () {
        $("#modal-form-child").modal('hide');
        $("#modal-form-Attach1").modal('hide');
        $('#backDrop').hide();
    }
    self.CloseUnderlying = function () {
        $("#modal-form-Underlying").modal('hide');
        //$("#modal-form-upload").modal('hide');
        $('#backDrop').hide();
    }
    self.CloseDocument = function () {
        $("#modal-form-Attach1").modal('hide');
        $('#backDrop').hide();
        $('.remove').click();
    }

    self.GetSelectedContactRow = function (contact) {
        self.IsNewData(false);

        self.ID_c(contact.ID);
        self.SourceID_c('2');
        self.Name_c(contact.Name);
        self.PhoneNumber_c(contact.PhoneNumber);

        self.Address_c(contact.Address);
        self.IDNumber_c(contact.IDNumber);
        self.Selected().POAFunction(contact.POAFunction.ID);
        self.POAFunction_c(contact.POAFunction);
        self.OccupationInID_c(contact.OccupationInID);
        self.PlaceOfBirth_c(contact.PlaceOfBirth);
        self.DateOfBirth_c(self.LocalDate(contact.DateOfBirth, true, false));
        self.EffectiveDate_c(self.LocalDate(contact.EffectiveDate, true, false));
        self.CancellationDate_c(self.LocalDate(contact.CancellationDate, true, false));
        /*self.DateOfBirth_c(moment(contact.DateOfBirth).format('DD-M-YYYY'));
        self.EffectiveDate_c(moment(contact.EffectiveDate).format('DD-M-YYYY'));
        self.CancellationDate_c(moment(contact.CancellationDate).format('DD-M-YYYY'));*/
        self.POAFunctionOther_c(contact.POAFunctionOther);
        $("#modal-form-child").modal('show');
        $('#backDrop').show();
    }

    function getStyle(el, prop) {
        var x = document.getElementById(el);

        if (window.getComputedStyle) {
            var y = document.defaultView.getComputedStyle(x, null).getPropertyValue(prop);
        } else if (x.currentStyle) {
            var y = x.currentStyle[styleProp];
        }

        return y;
    }

    self.save_c = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();
        SetPOAFunctionOther();
        if (form.valid()) {
            //Ajax call to insert the CustomerContacts
            $.ajax({
                type: "POST",
                url: api.server + api.url.customercontact + '/Add/Workflow/' + self.WorkflowInstanceID() + '/ApproverID/' + self.ApproverId(),
                data: ko.toJSON(CustomerContact), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        // hide current popup window
                        $("#modal-form-child").modal('hide');
                        $('#backDrop').hide();

                        // refresh data
                        GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }
    };

    self.update_c = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();
        SetPOAFunctionOther();
        if (form.valid()) {
            //Ajax call to insert the CustomerContacts
            $.ajax({
                type: "PUT",
                url: api.server + api.url.customercontact + '/Update/Workflow/' + self.WorkflowInstanceID() + '/ApproverID/' + self.ApproverId(),
                data: ko.toJSON(CustomerContact), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        // hide current popup window
                        $("#modal-form-child").modal('hide');
                        $('#backDrop').hide();

                        // refresh data
                        GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }
    };

    self.delete_c = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            //Ajax call to insert the CustomerContacts
            $.ajax({
                type: "PUT",
                url: api.server + api.url.customercontact + '/Delete/Workflow/' + self.WorkflowInstanceID() + '/ApproverID/' + self.ApproverId(),
                data: ko.toJSON(CustomerContact), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        // hide current popup window
                        $("#modal-form-child").modal('hide');
                        $('#backDrop').hide();

                        // refresh data
                        GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }
    };

    //set POA Function Other Other
    function SetPOAFunctionOther() {
        if (self.Selected().POAFunction() != 9) {
            self.POAFunctionOther_c('');
        }
    }
    // upload document
    self.UploadDocument = function () {
        self.IsUploading(false);
        $("#modal-form-Attach1").modal('show');
        $('.remove').click();
        var item = ko.utils.arrayFilter(self.DocumentPurposes(), function (dta) { return dta.ID != 2 });
        self.ddlDocumentPurpose_a(item);
        self.ID_a(0);
        self.DocumentPurpose_a(null);
        self.Selected().DocumentPurpose_a(null);
        self.DocumentType_a(null);
        self.Selected().DocumentType_a(null);
        self.DocumentPath_a(null);

        $('#backDrop').show();
    }

    // Add new document handler
    self.AddDocument = function () {
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            var vDocumentType = ko.utils.arrayFirst(self.DocumentTypes(), function (item) {
                return item.ID == self.Selected().DocumentType();
            });

            var vDocumentPurpose = ko.utils.arrayFirst(self.DocumentPurposes(), function (item) {
                return item.ID == self.Selected().DocumentPurpose();
            });

            var file = ko.toJS(self.DocumentPath())

            var doc = {
                ID: 0,
                Type: vDocumentType,
                Purpose: vDocumentPurpose,
                FileName: file.name,
                DocumentPath: self.DocumentPath(),
                LastModifiedDate: new Date(),
                LastModifiedBy: null
            };

            var isDocExist = ko.utils.arrayFirst(self.TransactionAllNormal().Transaction.Documents, function (item) {
                return item.FileName == doc.FileName && item.Purpose.Name == doc.Purpose.Name && item.Type.Name == doc.Type.Name;
            });

            if (isDocExist) {
                ShowNotification('Warning', 'Data already exist.', 'gritter-warning', false);
            } else {
                self.TransactionAllNormal().Transaction.Documents.push(doc);

                var update = viewModel.TransactionAllNormal();
                viewModel.TransactionAllNormal(ko.mapping.toJS(update));

                // hide upload dialog
                $("#modal-form-Attach1").modal('hide');
                $('#backDrop').hide();
            }
        }

        $('.remove').click();
    };

    //is deleted document
    self.IsDeleteAllowed = function (id) {
        if (id != 0) {
            return false;
        } else {
            return true;
        }
    };

    //delete document
    self.RemoveDocument = function (data) {
        var result = ko.utils.arrayFilter(self.TransactionAllNormal().Transaction.Documents, function (item) {
            return ((item.DocumentPath != data.DocumentPath));
        });

        self.TransactionAllNormal().Transaction.Documents = [];

        for (var i = 0; i <= result.length - 1; i++) {
            self.TransactionAllNormal().Transaction.Documents.push(result[i]);
        }

        var update = viewModel.TransactionAllNormal();
        viewModel.TransactionAllNormal(ko.mapping.toJS(update));
        //console.log(self.TransactionAllNormal().Transaction.Documents);
    };

    // delete document on ppumaker
    self.RemoveDocumentData = function (data) {
        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (result) {
                if (data.Purpose.ID == 2) {
                    // delete underlying document
                    DeleteUnderlyingAttachment(data);
                    var item = ko.utils.arrayFilter(viewModel.TransactionAllNormal().Transaction.Documents, function (dta) { return dta.DocumentPath != data.DocumentPath; });
                    if (item != null) {
                        viewModel.TransactionAllNormal().Transaction.Documents = item;
                    }
                } else {
                    if (data.IsNewDocument == false) {
                        var item = ko.utils.arrayFilter(viewModel.TransactionAllNormal().Transaction.Documents, function (dta) { return dta.ID != data.ID; });
                        if (item != null) {
                            viewModel.TransactionAllNormal().Transaction.Documents = item;
                        }
                    }
                }
                self.MakerDocuments.remove(data);
            }
        });
    }

    function DeleteUnderlyingAttachment(item) {
        //Ajax call to delete the Customer

        $.ajax({
            type: "DELETE",
            url: api.server + api.url.customerunderlyingfile + "/" + item.ID,
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                // send notification
                if (jqXHR.status = 200) {
                    DeleteFile(item.DocumentPath);
                    if (!item.IsNewDocument) {
                        DeleteTransactionDocumentUnderlying(item);
                    }
                    ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                    // refresh data
                    //GetDataAttachFile();
                } else
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function DeleteTransactionDocumentUnderlying(item) {
        //Ajax call to delete the Customer
        var pathdocs = item.DocumentPath.split('/');
        var file = pathdocs[pathdocs.length - 1].split('.');
        var fileName = file[0];
        $.ajax({
            type: "DELETE",
            url: api.server + api.url.transaction + "/DocumentPath/Delete/" + fileName,
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                // send notification
                if (jqXHR.status = 200) {
                    //DeleteFile(item.DocumentPath);
                    //ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                    // refresh data
                    //GetDataAttachFile();
                } else
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function DeleteFile(documentPath) {
        // Get the server URL.
        var serverUrl = _spPageContextInfo.webAbsoluteUrl;
        // output variable
        var output;

        // Delete the file to the SharePoint folder.
        var deleteFile = deleteFileToFolder();
        deleteFile.done(function (file, status, xhr) {
            output = file.d;
        });
        deleteFile.fail(OnError);
        // Call function delete File Shrepoint Folder
        function deleteFileToFolder() {
            return $.ajax({
                url: serverUrl + "/_api/web/GetFileByServerRelativeUrl('" + documentPath + "')",
                type: "POST",
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                    "X-HTTP-Method": "DELETE",
                    "IF-MATCH": "*"
                    //"content-length": arrayBuffer.byteLength
                }
            });
        }
    }

    // bind get data function to view
    self.GetData = function () { GetData(); };
    self.GetParameters = function () { GetParameters(); };
    self.GetCustomerUnderlying = function () { GetCustomerUnderlying(); }
    // bind clear filters
    self.ClearFilters = function () {
        self.FilterWorkflowName("");
        self.FilterInitiator("");
        self.FilterStateDescription("");
        self.FilterStartTime("");
        self.FilterTitle("");
        self.FilterTransactionStatus("");
        self.FilterUser("");
        self.FilterUserApprover("");
        self.FilterEntryTime("");
        self.FilterExitTime("");
        self.FilterOutcomeDescription("");
        self.FilterCustomOutcomeDescription("");
        self.FilterComments("");

        self.FilterApplicationID("");
        self.FilterCustomer("");
        self.FilterProduct("");
        self.FilterCurrency("");
        self.FilterAmount("");
        self.FilterAmountUSD("");
        self.FilterDebitAccNumber("");
        self.FilterFXTransaction("");
        self.FilterTopUrgent("");
        self.FilterTransactionStatus("")
        self.FilterBranchCode(""); //Rizki 2016-01-09
        self.FilterBranchName(""); //Rizki 2016-01-09

        GetData();
    };

    // Color Status
    // Update Rizki 2015-03-06
    //self.SetColorStatus = function (id, applicationID, outcomeDescription, customOutcomeDescription) {
    //    if (outcomeDescription == null) {
    //        outcomeDescription = '';
    //    }

    //    if (customOutcomeDescription == null) {
    //        customOutcomeDescription = '';
    //    }

    //    if (id == 2) {
    //        return "active";
    //    }
    //    else if (id == 4) {
    //        //Untuk handle FX Checker reject dan PPU Checker Approve Cancellation
    //        if ((applicationID.toLowerCase().startsWith('fx') && outcomeDescription.toLowerCase() == 'rejected') || (customOutcomeDescription.toLowerCase() == 'approve cancellation')) {
    //            return "warning";
    //        }
    //        else {
    //            return "success";
    //        }
    //    }
    //    else if (id == 8) {
    //        return "warning";
    //    }
    //    else if (id == 64) {
    //        return "danger";
    //    }
    //    else {
    //        return "";
    //    }
    //};

    //Update Rizki 2016-01-11
    self.SetColorStatus = function (stateID, transactionStatus) {
        if (stateID == 2) {
            return "active";
        }
        else if (stateID == 4) {
            //Untuk handle PPU Checker Approve Cancellation
            if (transactionStatus.toLowerCase() == 'canceled' || transactionStatus.toLowerCase() == 'canceled by ipe') {
                return "warning";
            }
            else if (transactionStatus.toLowerCase() == 'completed') {
                return "success";
            }
            else {
                return "";
            }
        }
        else if (stateID == 8 || stateID == 64) {
            return "danger";
        }
        else {
            return "";
        }
    };

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        if (ko.isObservable(date)) {
            date = date();
        }
        /*
        var localDate = new Date(date);
        if (moment(localDate).isValid()) {
            if (date == '1970/01/01 00:00:00' || date == '1970-01-01T00:00:00' || date == null) {
                return "";
            }
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return ""
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    self.FormatNumber = function (num) {
        if (ko.isObservable(num)) {
            num = num();
        }
        if (num == null) {
            return 0;
        }
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    }

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        //update Rizki 2016-01-09
        console.log(data);
        CheckDummyCIF = data.Transaction.Customer;
        self.ApproverId(data.WorkflowContext.ApproverID);
        self.SPWebID(data.WorkflowContext.SPWebID);
        self.SPSiteID(data.WorkflowContext.SPSiteID);
        self.SPListID(data.WorkflowContext.SPListID);
        self.SPListItemID(data.WorkflowContext.SPListItemID);
        self.SPTaskListID(data.WorkflowContext.SPTaskListID);
        self.SPTaskListItemID(data.WorkflowContext.SPTaskListItemID);
        self.Initiator(data.WorkflowContext.Initiator);
        self.WorkflowInstanceID(data.WorkflowContext.WorkflowInstanceID);
        self.WorkflowID(data.WorkflowContext.WorkflowID);
        self.WorkflowName(data.WorkflowContext.WorkflowName);
        //self.StartTime(data.WorkflowContext.StartTime);
        //self.StateID(data.WorkflowContext.StateID);
        //self.StateDescription(data.WorkflowContext.StateDescription);
        //self.IsAuthorized(data.WorkflowContext.IsAuthorized);
        //AndriYanto 30 November 2015
        self.InitialProduct(data.Transaction.ApplicationID.substring(0, 2).toLowerCase());
        self.ProductID(data.Transaction.ProductID);
        //End Andriyanto 30 November 2015
        self.TaskTypeID(data.CurrentTask.TaskTypeID);
        //self.TaskTypeDescription(data.CurrentTask.TaskTypeDescription);
        //self.Title(data.CurrentTask.Title);
        self.ActivityTitle(data.Transaction.TransactionStatus);
        //self.ActivityTitle("FX Deal Maker Task");
        //self.User(data.CurrentTask.User);
        //self.IsSPGroup(data.CurrentTask.IsSPGroup);
        //self.EntryTime(data.CurrentTask.EntryTime);
        //self.ExitTime(data.CurrentTask.ExitTime);
        self.OutcomeID(data.CurrentTask.OutcomeID);
        //self.OutcomeDescription(data.CurrentTask.OutcomeDescription);
        //self.CustomOutcomeID(data.CurrentTask.CustomOutcomeID);
        //self.CustomOutcomeDescription(data.CurrentTask.CustomOutcomeDescription);
        //self.Comments(data.CurrentTask.Comments);
        self.ModePayment(data.Transaction.Mode); // use for read status payment mode
        self.ApprovalRole(null);
        self.ApprovalName(null);

        if (self.ModePayment() == "BCP2" || self.ModePayment() == null || self.InitialProduct() == "fx") {
            self.InitialProduct(data.Transaction.ApplicationID.substring(0, 2).toLowerCase());
        } else {
            if (data.Transaction.ProductID == ConsProductID.SKNBulkProductIDCons) {
                self.InitialProduct('sb');
            } else {
                self.InitialProduct(data.Transaction.Product.substring(0, 2).toLowerCase());
            }
        }
        self.Outcomes.removeAll();
        // add chandra set original value

        $(".TransHistory").hide();

        // show the dialog task form
        $("#modal-form").modal('show');

        //updated by dani
        $('#myTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });

        $('a[data-toggle="tab"]').on('shown', function (e) {
            console.log(e.target); // activated tab
            console.log(e.relatedTarget); // previous tab
        });

        $('a[data-toggle="tab"]:first').tab('show');

        //Rizki - 2016-01-11
        GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());
        //Tambah Agung
        GetInitiatorRole(viewModel.Initiator());
        //End
        //GetTaskOutcomes(data.WorkflowContext.SPTaskListID, data.WorkflowContext.SPTaskListItemID);
        IsForceCompleteTransaction();
        viewModel.Comments("");
    };


    //Tambah Agung
    self.IsCSO = ko.observable();
    function GetInitiatorRole(username) {
        var un = username.lastIndexOf("|");
        var UN = username.substring(un + 1)
        var options = {
            url: api.server + api.url.workflow.isCso(UN),
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetIsCSO, OnError, OnAlways);
    }
    function OnSuccessGetIsCSO(data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            viewModel.IsCSO(data);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }
    //End

    self.OpenTask = function (data) {

    };

    function GetProductName() {
        var pID = self.ProductID();
        var pName = '';
        switch (pID) {
            case ConsProductID.RTGSProductIDCons:
            case ConsProductID.SKNProductIDCons:
            case ConsProductID.OTTProductIDCons:
            case ConsProductID.OverbookingProductIDCons:
            case ConsProductID.FXProductIDCons:
            case ConsProductID.FXNettingProductIDCons:
                pName = ProductName.payment;
                break;
            case ConsProductID.SKNBulkProductIDCons: //udin add 20161107 skn bulk
                pName = ProductName.sknbulk;
                break;
            case ConsProductID.TMOProductIDCons:
                pName = ProductName.tmo;
                break;
            case ConsProductID.FDProductIDCons:
                pName = ProductName.fd;
                break;

            case ConsProductID.IDInvestmentProductIDCons:
            case ConsProductID.SavingPlanProductIDCons:
            case ConsProductID.UTOnshoreproductIDCons:
            case ConsProductID.UTOffshoreProductIDCons:
            case ConsProductID.UTCPFProductIDCons:
                pName = ProductName.ut;
                break;
            case ConsProductID.CollateralProductIDCons:
                pName = ProductName.collateral;
                break;
            case ConsProductID.CIFProductIDCons:
                pName = ProductName.cif;
                break;
            case ConsProductID.LoanDisbursmentProductIDCons:
            case ConsProductID.LoanRolloverProductIDCons:
            case ConsProductID.LoanIMProductIDCons:
            case ConsProductID.LoanSettlementProductIDCons:
                pName = ProductName.loan;
                break;
            case ConsProductID.DFProductIDCons:
                pName = ProductName.df;
                break;
            default:
                break;
        }
        return pName;
    }

    // set approval ui template
    self.ApprovalTemplate = function () {
        var output;
        //basri 03-12-2015
        var pName = GetProductName();
        var actTitle = self.ActivityTitle();
        switch (pName) {
            case ProductName.payment:
                if (self.ModePayment() == "BCP2" || self.ModePayment() == null || self.InitialProduct() == "fx") {
                    output = SetTemplatePayment(actTitle);
                } else {
                    output = SetTemplatePaymentIPE(actTitle);
                }
                break;
            case ProductName.tmo:
                output = SetTemplateTMO(actTitle);
                break;
            case ProductName.fd:
                output = SetTemplateFD(actTitle);
                break;
            case ProductName.ut:
                output = SetTemplateUT(actTitle);
                break;
            case ProductName.loan:
                output = SetTemplateLoan(actTitle);
                break;
            case ProductName.collateral:
                output = SetTemplateCollateral(actTitle);
                break;
            case ProductName.cif:
                output = SetTemplateCIF(actTitle);
                break;
            case ProductName.sknbulk:
                output = SetTemplateSKNBulk(actTitle);
                break;
            case ProductName.df:
                output = SetTemplateDF(actTitle);
                break;
            default:
                break

        }
        return output;
        //end basri
    };
    function SetTemplateDF(activitytitle) {
        var output = '';
        var actTitle = activitytitle;
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:                
            case HomeType.AllTransaction:               
            case HomeType.Monitoring:
                output = "DocumentFlow";
                break;
        }
        return output;
    };
    //basri 03-12-2015
    function SetTemplatePayment(activitytitle) {
        var output = '';
        var actTitle = activitytitle;
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                output = SetTemplatePaymentCompleted(actTitle);
                break;
            case HomeType.AllTransaction:
                output = SetTemplatePaymentAllTransaction(actTitle);
                break;
            case HomeType.Monitoring:
                if (actTitle != null && (actTitle.toLowerCase() == "completed" || actTitle.toLowerCase() == "canceled")) {
                    output = SetTemplatePaymentCompleted(actTitle);
                }
                else {
                    output = SetTemplatePaymentAllTransaction(actTitle);
                }
                break;
        }
        return output;
    }
    function SetTemplatePaymentIPE(activitytitle) {
        var output = '';
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                output = "TransactionAllIPE";
                break;
            case HomeType.AllTransaction:
                output = "TransactionAllIPE";
                break;
            case HomeType.Monitoring:
                if (activitytitle != null && (activitytitle.toLowerCase() == "completed" || activitytitle.toLowerCase() == "canceled")) {
                    output = "TransactionAllIPE";
                }
                else {
                    output = "TransactionAllIPE";
                }
                break
        }
        return output;
    }
    function SetTemplateSKNBulk(activityTitle) {
        var output = '';
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                output = "TransactionAllIPESKNBulk";
                break;
            case HomeType.AllTransaction:
                output = "TransactionAllIPESKNBulk";
                break;
            case HomeType.Monitoring:
                if (activityTitle != null && (activityTitle.toLowerCase() == "completed" || activityTitle.toLowerCase() == "canceled")) {
                    output = "TransactionAllIPESKNBulk";
                }
                else {
                    output = "TransactionAllIPESKNBulk";
                }
                break
        }
        return output;
    }


    function SetTemplatePaymentCompleted(actTitle) {
        var output = '';
        var pID = self.ProductID();
        switch (pID) {
            case ConsProductID.RTGSProductIDCons:
            case ConsProductID.SKNProductIDCons:
            case ConsProductID.OTTProductIDCons:
            case ConsProductID.OverbookingProductIDCons:
                output = "TransactionAll";
                break;
            case ConsProductID.FXProductIDCons:
                switch (actTitle) {
                    case "FX Deal Maker Task":
                    case "FX Deal Checker Task":
                        output = "TaskFXDealChecker";
                        break;
                    case "FX Deal TMO Checker Task":
                    case "FX TMO Maker After Revise Task":
                    case "FX Deal TMO Maker After Submit Task":
                    case "FX Deal TMO Checker Request Cancel Task":
                        output = "TaskFXTMOChecker";
                        break;
                    default:
                        output = "TaskFXDealChecker";
                        break;
                }
                break;
            case ConsProductID.FXNettingProductIDCons:
                output = "TaskAllNetting";
                break;
            default:
                output = "TransactionAll";

                break;
        }

        return output;
    }
    function SetTemplatePaymentAllTransaction(actTitle) {
        var output = '';
        var activitytitle = actTitle;
        switch (activitytitle) {
            case "CBO Maker Task":
                output = "TaskCBOMaker";
                break;
            case "CBO Checker Task":
                output = "TaskCBOChecker";
                break;
            case "FX Deal Checker Task":
            case "FX Deal Maker Task":
                output = "TaskFXDealChecker";
                break;
            case "FX Deal TMO Checker Task":
            case "FX TMO Maker After Revise Task":
            case "FX Deal TMO Maker After Submit Task":
            case "FX Deal TMO Checker Request Cancel Task":
                output = "TaskFXTMOChecker";
                break;
            case "Pending Documents Checker Task":
                output = "TaskPendingDocumentsChecker";
                break;
            case "FX Deal TMO Netting Maker Task":
            case "FX Deal TMO Netting Checker Task":
                output = "TaskAllNetting";
                break;
            default:
                output = "TransactionAll";
                break;
        }

        return output;
    }
    function SetTemplateTMO(actTitle) {
        var output = '';
        var actTitle = actTitle;
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                output = SetTemplateTMOCompleted(actTitle);
                break;
            case HomeType.Monitoring:
            case HomeType.AllTransaction:
                output = SetTemplateTMOAllTransaction(actTitle);
                break;
        }
        return output;
    }
    function SetTemplateUT(actTitle) {
        var output = '';
        var activitytitle = actTitle;
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                output = SetTemplateUTCompleted(activitytitle);
                break;
            case HomeType.Monitoring:
            case HomeType.AllTransaction:
                output = SetTemplateUTAllTransaction(activitytitle);
                break;
        }
        return output;
    }
    function SetTemplateUTCompleted(actTitle) {
        var output = '';
        output = "TaskUTcomplete";
        return output;
    }
    function SetTemplateUTAllTransaction(actTitle) {
        var output = '';
        output = "TaskUTcomplete";
        return output;
    }
    function SetTemplateFD(actTitle) {
        var output = '';
        var actTitle = actTitle;
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                output = SetTemplateFDCompleted(actTitle);
                break;
            case HomeType.Monitoring:
            case HomeType.AllTransaction:
                output = SetTemplateFDAllTransaction(actTitle);
                break;
        }
        return output;
    }
    function SetTemplateFDCompleted(actTitle) {
        var output = '';
        output = "TaskFDComplete";
        return output;
    }
    function SetTemplateFDAllTransaction(actTitle) {
        var output = '';
        output = "TaskFDComplete";
        return output;
    }
    function SetTemplateCollateral(actTitle) {
        var output = '';
        var actTitle = activitytitle;
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case "":
                output = "";
                break;
        }
        return output;
    }
    function SetTemplateLoan(actTitle) {
        var output = 'TransactionLoanComplete';
        return output;
    }
    function SetTemplateCIF(actTitle) {
        var output = '';
        var actTitle = actTitle;
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                output = SetTemplateCIFCompleted(actTitle);
                break;
            case HomeType.Monitoring:
            case HomeType.AllTransaction:
                output = SetTemplateCIFAllTransaction(actTitle);
                break;
        }
        return output;
    }
    //end basri
    //add by fandi
    function SetTemplateCIFCompleted(actTitle) {
        var output = '';
        if (CheckDummyCIF.toLowerCase().includes("DUMMY")) {
            output = "TaskCIFDUComplete";
        } else {
            output = "TaskCIFComplete";
        }
        return output;
    }
    function SetTemplateCIFAllTransaction(actTitle) {
        var output = '';
        if (CheckDummyCIF.toLowerCase().includes("DUMMY")) {
            output = "TaskCIFDUComplete";
        } else {
            output = "TaskCIFComplete";
        }
        return output;
    }
    function SetTemplateTMOCompleted(actTitle) {
        var output = '';
        output = "TaskTMOComplete";
        return output;
    }
    function SetTemplateTMOAllTransaction(actTitle) {
        var output = '';
        output = "TaskTMOComplete";
        return output;
    }
    //end
    self.UtcAttemp = ko.observable();
    function GetUtcAttemp() {
        var options = {
            url: api.server + api.url.utcattemps,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetUtcAttemp, OnError, OnAlways);
    }

    function OnSuccessGetUtcAttemp(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            self.UtcAttemp(data);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    // set approval data template
    self.ApprovalData = function () {
        var output;
        //basri 03-12-2015
        var pName = GetProductName();
        var actTitle = self.ActivityTitle();
        switch (pName) {
            case ProductName.sknbulk:
                output = SetDataSKNBulk(actTitle);
                break;
            case ProductName.payment:
                output = SetDataPayment(actTitle);
                break;
            case ProductName.tmo:
                output = SetDataTMO(actTitle);
                break;
            case ProductName.fd:
                output = SetDataFD(actTitle);
                break;
            case ProductName.ut:
                output = SetDataUT(actTitle);
                break;
            case ProductName.loan:
                output = SetDataLoan(actTitle);
                break;
            case ProductName.collateral:
                output = SetDataCollateral(actTitle);
                break;
            case ProductName.cif:
                output = SetDataCIF(actTitle);
                break;
            case ProductName.df:
                output = SetDataDF(actTitle);
                break;
            default:
                break;
        }
        return output;
        //end basri

    };
    function SetDataDF(activitytitle) {
        var output = '';
        var actTitle = activitytitle;
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
            case HomeType.Monitoring:
            case HomeType.AllTransaction:
                output = self.TransactionDF();
                break;
        }
        return output;
    }
    function SetDataPayment(activitytitle) {
        var output = '';
        var actTitle = activitytitle;
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                output = SetDataPaymentCompleted(actTitle);
                break;
            case HomeType.Monitoring:
            case HomeType.AllTransaction:
                output = SetDataPaymentAllTransaction(actTitle);
                break;
        }
        return output;
    }
    function SetDataSKNBulk(activitytitle) {
        var output = '';
        var actTitle = activitytitle;
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                output = SetDataSKNBulkCompleted(actTitle);
                break;
            case HomeType.Monitoring:
            case HomeType.AllTransaction:
                output = SetDataSKNBulkAllTransaction(actTitle);
                break;
        }
        return output;
    }
    function SetDataPaymentCompleted(actTitle) {
        var output = '';
        output = self.TransactionAllNormal();
        return output;
    }
    function SetDataPaymentAllTransaction(actTitle) {
        var output = '';
        switch (actTitle) {
            case "CBO Maker Task":
                output = self.TransactionContact();
                break;
            case "CBO Checker Task":
                output = self.TransactionContact();
                break;
                /*case "CBO Maker Reactivation Task":
                    output = self.TransactionChecker();
                    break;
                case "CBO Checker Reactivation Task":
                    output = self.TransactionChecker();
                    break; */
            case "FX Deal Checker Task":
            case "FX Deal Maker Task":
            case "FX Deal TMO Checker Task":
            case "FX TMO Maker After Revise Task":
            case "FX Deal TMO Maker After Submit Task":
            case "FX Deal TMO Checker Request Cancel Task":
                output = self.TransactionDeal();
                break;
            case "Pending Documents Checker Task":
                output = self.TransactionAllNormal(); // addall
            default:
                output = self.TransactionAllNormal();
                break;
        }
        return output;
    }
    function SetDataSKNBulkCompleted(actTitle) {
        var output = '';
        output = self.TransactionAllNormal();
        return output;
    }
    function SetDataSKNBulkAllTransaction(actTitle) {
        var output = '';
        switch (actTitle) {
            case "CBO Maker Task":
                output = self.TransactionContact();
                break;
            case "CBO Checker Task":
                output = self.TransactionContact();
                break;
                /*case "CBO Maker Reactivation Task":
                    output = self.TransactionChecker();
                    break;
                case "CBO Checker Reactivation Task":
                    output = self.TransactionChecker();
                    break; */
            case "FX Deal Checker Task":
            case "FX Deal Maker Task":
            case "FX Deal TMO Checker Task":
            case "FX TMO Maker After Revise Task":
            case "FX Deal TMO Maker After Submit Task":
            case "FX Deal TMO Checker Request Cancel Task":
                output = self.TransactionDeal();
                break;
            case "Pending Documents Checker Task":
                output = self.TransactionAllNormal(); // addall
            default:
                output = self.TransactionAllNormal();
                break;
        }
        return output;
    }
    function SetDataTMO(activitytitle) {
        var output;
        var actTitle = activitytitle;
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                output = SetDataTMOCompleted(actTitle);
                break;
            case HomeType.Monitoring:
            case HomeType.AllTransaction:
                output = SetDataTMOAlltransaction(actTitle);
                break;
        }
        return output;
    }
    function SetDataUT(activitytitle) {
        var output = '';
        var actTitle = activitytitle;
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                output = SetDataUTCompleted(actTitle);
                break;
            case HomeType.Monitoring:
            case HomeType.AllTransaction:
                output = SetDataUTAllTransaction(actTitle);
                break;
        }
        return output;
    }
    function SetDataUTCompleted(actTitle) {
        var output = '';
        output = self.TransactionAllUTNormal();
        return output;
    }
    function SetDataUTAllTransaction(actTitle) {
        var output = '';
        output = self.TransactionAllUTNormal();
        return output;
    }
    function SetDataFD(activitytitle) {
        var output = '';
        var actTitle = activitytitle;
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                output = SetDataFDCompleted(actTitle);
                break;
            case HomeType.Monitoring:
            case HomeType.AllTransaction:
                output = SetDataFDAllTransaction(actTitle);
                break;
        }
        return output;
    }
    function SetDataFDCompleted(actTitle) {
        var output = '';
        output = self.TransactionAllFDNormal();
        return output;
    }
    function SetDataFDAllTransaction(actTitle) {
        var output = '';
        output = self.TransactionAllFDNormal();
        return output;
    }
    function SetDataCollateral(activitytitle) {
        var output;
        switch (activitytitle) {
            case "":
                output = "";
                break;
            default:
                output = self.TransactionAllNormal();
                break;
        }
        return output;
    }
    function SetDataLoan(activitytitle) {
        var output;
        output = self.TransactionAllLoanNormal();
        return output;

    }
    function SetDataCIF(activitytitle) {
        var output;
        var actTitle = activitytitle;
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                output = SetDataCIFCompleted(actTitle);
                break;
            case HomeType.Monitoring:
            case HomeType.AllTransaction:
                output = SetDataCIFAllTransaction(actTitle);
                break;
        }
        return output;
    }
    //end basri

    //add by fandi
    function SetDataCIFCompleted(actTitle) {
        var output = '';
        //output = self.TransactionALLCIFNormal();
        output = self.TransactionMakerCBO();
        return output;
    }
    function SetDataCIFAllTransaction(actTitle) {
        var output = '';
        output = self.TransactionMakerCBO();
        //output = self.TransactionALLCIFNormal();
        return output;
    }
    function SetDataTMOCompleted(actTitle) {
        var output = '';
        output = self.TransactionCheckerTMO();
        return output;
    }
    function SetDataTMOAlltransaction(actTitle) {
        var output = '';
        output = self.TransactionCheckerTMO();
        return output;
    }
    //end

    function EnableMessageApprover() {
        self.OriginalValue().BeneAccNumberOri().Status(false);
        self.OriginalValue().AmountOri().Status(false);
        self.OriginalValue().RateOri().Status(false);
        self.OriginalValue().AmountUSDOri().Status(false);
        self.OriginalValue().BeneNameOri().Status(false);
        self.OriginalValue().BanksOri().Status(false);
        //self.OriginalValue().AccountOri().Status(false);

        self.OriginalValue().SwiftCodeOri().Status(false);
        self.OriginalValue().BankChargesOri().Status(false);
        self.OriginalValue().AgentChargesOri().Status(false);
        self.OriginalValue().IsCitizenOri().Status(false);
        self.OriginalValue().IsResidentOri().Status(false);
    }

    // rename status fx transaction & top urgent
    self.RenameStatus = function (type, value) {
        if (type == "FX Transaction") {
            if (value) {
                return "Yes";
            } else {
                return "No";
            }
        } else if (type == "Urgency") {
            if (value == 1) {
                return "Top Urgent";
            } else if (value == 2) {
                return "Top Urgent Chain";
            } else { return "Normal" }
        } else {
            return "Unknown";
        }
    };

    // rename status filter fx Transaction & top urgent : add chandra
    self.RenameStatusFilter = function (type, value) {
        if (type == "FX Transaction") {
            if (value != "") {
                if (("yes").indexOf(value.toLowerCase()) > -1) {
                    return true;
                } else if (("no").indexOf(value.toLowerCase()) > -1) {
                    return false;
                } else {
                    return "-";
                }
            } else { return "" };
        } else if (type == "Urgency") {
            if (value != "") {
                if (("top urgent").indexOf(value.toLowerCase()) > -1) {
                    return true;
                } else if (("normal").indexOf(value.toLowerCase()) > -1) {
                    return false;
                } else {
                    return "-";
                }
            } else { return "" };
        } else {
            // unknown parameter
            return false;
        }
    }
    // rename status fx transaction & top urgent
    self.CallbackStatus = function (type, value) {
        if (type == "FX Transaction") {
            if (value == "Yes") {
                return true;
            } else {
                return false;
            }
        } else if (type == "Urgency") {
            if (value == "Top Urgent") {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    };

    // Chandra 2015.02.24 : Get Underlying data for attach files
    function GetCustomerUnderlying() {
        var _cif = viewModel.TransactionAllNormal().Transaction.Customer.CIF;
        var options = {
            url: api.server + api.url.customerunderlying + "/Attachment",
            params: { cif: _cif },
            token: accessToken
        };
        // get filtered columns
        var filters = self.MakerUnderlyings();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataAttachFile1, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataAttachFile1, OnError, OnAlways);
        }
    }

    // On success GetData Underlying for AttachFile
    function OnSuccessGetDataAttachFile1(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            for (var i = 0 ; i < data.length; i++) {
                var selected = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (item) {
                    return item.ID == data[i].ID;
                });
                if (selected != null) {
                    data[i].IsSelectedAttach = true;
                }
                else {
                    data[i].IsSelectedAttach = false;
                }
            }
            self.CustomerAttachUnderlyings(data);

        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }
    // chandra 2015.02.26 : upload attachment of ppu maker task
    self.cancel_a = function () {
        $("#modal-form-Attach").modal('hide');
        $('#backDrop').hide();
    }

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterWorkflowName() != "") filters.push({ Field: 'WorkflowName', Value: self.FilterWorkflowName() });
        if (self.FilterInitiator() != "") filters.push({ Field: 'Initiator', Value: self.FilterInitiator() });
        if (self.FilterStateDescription() != "") filters.push({ Field: 'StateDescription', Value: self.FilterStateDescription() });
        if (self.FilterStartTime() != "") filters.push({ Field: 'StartTime', Value: self.FilterStartTime() });
        if (self.FilterTitle() != "") filters.push({ Field: 'Title', Value: self.FilterTitle() });
        if (self.FilterUser() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterUser() });
        if (self.FilterUserApprover() != "") filters.push({ Field: 'UserApprover', Value: self.FilterUserApprover() });
        if (self.FilterEntryTime() != "") filters.push({ Field: 'EntryTime', Value: self.FilterEntryTime() });
        if (self.FilterExitTime() != "") filters.push({ Field: 'ExitTime', Value: self.FilterExitTime() });
        if (self.FilterOutcomeDescription() != "") filters.push({ Field: 'OutcomeDescription', Value: self.FilterOutcomeDescription() });
        if (self.FilterCustomOutcomeDescription() != "") filters.push({ Field: 'CustomOutcomeDescription', Value: self.FilterCustomOutcomeDescription() });
        if (self.FilterComments() != "") filters.push({ Field: 'Comments', Value: self.FilterComments() });

        if (self.FilterBranchCode() != "") filters.push({ Field: 'BranchCode', Value: self.FilterBranchCode() });
        if (self.FilterBranchName() != "") filters.push({ Field: 'BranchName', Value: self.FilterBranchName() });

        if (self.FilterApplicationID() != "") filters.push({ Field: 'ApplicationID', Value: self.FilterApplicationID() });
        if (self.FilterCustomer() != "") filters.push({ Field: 'Customer', Value: self.FilterCustomer() });
        if (self.FilterProduct() != "") filters.push({ Field: 'Product', Value: self.FilterProduct() });
        if (self.FilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.FilterCurrency() });
        if (self.FilterAmount() != "") filters.push({ Field: 'Amount', Value: self.FilterAmount() });
        if (self.FilterAmountUSD() != "") filters.push({ Field: 'AmountUSD', Value: self.FilterAmountUSD() });
        if (self.FilterDebitAccNumber() != "") filters.push({ Field: 'DebitAccNumber', Value: self.FilterDebitAccNumber() });
        if (self.FilterFXTransaction() != "") filters.push({ Field: 'IsFXTransaction', Value: self.FilterFXTransaction() });
        if (self.FilterTopUrgent() != "") filters.push({ Field: 'IsTopUrgent', Value: self.FilterTopUrgent() });
        if (self.FilterTransactionStatus() != "") filters.push({ Field: 'TransactionStatus', Value: self.FilterTransactionStatus() });

        return filters;
    };

    // get all parameters need
    function GetParameters() {
        var options = {
            url: api.server + api.url.parameter,
            params: {
                select: "Product,Currency,Channel,BizSegment,BankCharge,AgentCharge,FXCompliance,ChargesType,ProductType,lld,UnderlyingDoc,POAFunction,DocType,PurposeDoc"
                //select: "Product,Currency,Channel,BizSegment,Bank,BankCharge,AgentCharge,FXCompliance,ChargesType,ProductType,lld,UnderlyingDoc,POAFunction,DocType,PurposeDoc"
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
    }

    // Get data / refresh data
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') { $remove = true; $box.addClass('position-relative'); }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        // widget reloader function end

        // declare options variable for ajax get request
        var options = null;

        var urlTuning = GetUrl(self.FormMenuCustom());
        options = {
            url: api.server + urlTuning,
            params: {
                webid: config.sharepoint.webId,
                siteid: config.sharepoint.siteId,
                workflowids: config.sharepoint.workflowId,
                state: self.WorkflowConfig().State,
                outcome: self.WorkflowConfig().Outcome,
                //customOutcome:  "1,2,3,4,5,6,7,8",
                customOutcome: self.WorkflowConfig().CustomOutcome,
                showContribute: self.WorkflowConfig().ShowContribute,
                showActiveTask: self.WorkflowConfig().ShowActiveTask,
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }

    //dani, 24-5-2016
    /*
    self.IsCSO = ko.observable();
    function GetInitiatorRole(username) {
        var un = username.lastIndexOf("|");
        var UN = username.substring(un + 1)
        var options = {
            url: api.server + api.url.workflow.isCso(UN),
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetIsCSO, OnError, OnAlways);
    }
    function OnSuccessGetIsCSO(data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            viewModel.IsCSO(data);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }
    */
    //end by dani

    // Get Url for Menu Home
    function GetUrl(CustomMenu) {
        var urlTuning;
        if (CustomMenu != null) {
            switch (CustomMenu.toLowerCase()) {
                case "completed":
                    urlTuning = api.url.tuning.completed;
                    break;
                case "home":
                    urlTuning = api.url.tuning.home;
                    break;
                case "canceled":
                    urlTuning = api.url.tuning.canceled;
                    break;
                case "monitoring":
                    urlTuning = api.url.tuning.monitoring;
                    break;
                case "alltransaction":
                    urlTuning = api.url.tuning.allTransaction;
                    break;
                default:
                    urlTuning = api.url.tuning.home;
                    break;
            }
        }
        return urlTuning;
    }

    // get Transaction data
    function GetTransactionData(instanceId, approverId) {
        var endPointURL;
        var pName = GetProductName();
        var actTitle = self.ActivityTitle();

        //end basri
        switch (pName) {
            case ProductName.sknbulk: //add udin 201611107 skn bulk monitoring        
                //add azam
                self.TransactionSKNBulks(null);
                self.TransactionSKNBulksOriginal(null);
                if (typeof self.DocumentSKNBulk != 'function') {
                    ko.mapping.fromJS({}, {}, self.DocumentSKNBulk);
                }
                //end add azam
            case ProductName.payment:
                if (self.ModePayment() == "BCP2" || self.ModePayment() == null || self.InitialProduct() == "fx") {
                    endPointURL = GetTransactionDataPayment(actTitle, instanceId, approverId);
                } else {
                    endPointURL = GetTransactionDataPaymentIPE(actTitle, instanceId, approverId);
                }
                break;
            case ProductName.tmo:
                endPointURL = GetTransactionDataTMO(actTitle, instanceId, approverId);
                break;
            case ProductName.fd:
                endPointURL = GetTransactionDataFD(actTitle, instanceId, approverId);
                break;
            case ProductName.ut:
                endPointURL = GetTransactionDataUT(actTitle, instanceId, approverId);
                break;
            case ProductName.loan:
                endPointURL = GetTransactionDataLoan(actTitle, instanceId, approverId);
                break;
            case ProductName.collateral:
                endPointURL = GetTransactionDataCollateral(actTitle, instanceId, approverId);
                break;
            case ProductName.cif:
                endPointURL = GetTransactionDataCIF(actTitle, instanceId, approverId);
                break;
            case ProductName.df:
                endPointURL = GetTransactionDataDF(actTitle, instanceId, approverId);
                break;
            default:
                break;
        }

        // declare options variable for ajax get request
        var options = {
            url: endPointURL,
            token: accessToken
        };

        // show progress bar
        $("#transaction-progress").show();

        // hide transaction data
        $("#transaction-data").hide();

        // call ajax
        Helper.Ajax.Get(options, OnSuccessGetTransactionData, OnError, function () {
            // hide progress bar
            $("#transaction-progress").hide();

            // show transaction data
            $("#transaction-data").show();
        });
    }

    function GetTransactionDataPayment(actTitle, instanceId, approverId) {
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        var output = '';
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                output = GetTransactionDataPaymentCompleted(instanceId, approverId);
                break;
            case HomeType.AllTransaction:
                output = GetTransactionDataPaymentAllTransaction(actTitle, instanceId, approverId);
                break;
            case HomeType.Monitoring:
                if (actTitle.toLowerCase() == "completed" || actTitle.toLowerCase() == "canceled") {
                    output = GetTransactionDataPaymentCompleted(instanceId, approverId);
                }
                else {
                    output = GetTransactionDataPaymentAllTransaction(actTitle, instanceId, approverId);
                }
                break;
        }
        return output;
    }

    function GetTransactionDataPaymentIPE(actTitle, instanceId, approverId) {
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        var output = '';
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                output = api.server + api.url.workflow.transactionMonitoringIPE(instanceId, approverId);
                break;
            case HomeType.AllTransaction:
                output = api.server + api.url.workflow.transactionMonitoringIPE(instanceId, approverId);
                break;
            case HomeType.Monitoring:
                if (actTitle.toLowerCase() == "completed" || actTitle.toLowerCase() == "canceled") {
                    output = api.server + api.url.workflow.transactionMonitoringIPE(instanceId, approverId);
                }
                else {
                    output = api.server + api.url.workflow.transactionMonitoringIPE(instanceId, approverId);
                }
                break;
        }
        self.IsTimelines(true);
        return output;
    }

    function GetTransactionDataPaymentCompleted(instanceId, approverId) {
        var endPointURL;
        var pID = self.ProductID();
        switch (pID) {
            case ConsProductID.RTGSProductIDCons:
            case ConsProductID.SKNProductIDCons:
            case ConsProductID.OTTProductIDCons:
            case ConsProductID.OverbookingProductIDCons:
                endPointURL = api.server + api.url.workflow.transactionAllNormal(instanceId, approverId);
                break;
            case ConsProductID.FXProductIDCons:
                endPointURL = api.server + api.url.workflow.transactionDeal(instanceId, approverId);
                break;
            case ConsProductID.FXNettingProductIDCons:
                endPointURL = api.server + api.url.workflow.transactionCheckerNetting(instanceId, approverId);
                break;
            default:
                break;
        }

        self.IsTimelines(true);
        return endPointURL;
    }
    function GetTransactionDataPaymentAllTransaction(actTitle, instanceId, approverId) {
        var endPointURL;
        switch (actTitle) {
            case "CBO Maker Task":
                endPointURL = api.server + api.url.workflow.transactionContact(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "CBO Checker Task":
                endPointURL = api.server + api.url.workflow.transactionContact(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "FX Deal Checker Task":
            case "FX Deal Maker Task":
                endPointURL = api.server + api.url.workflow.transactionDeal(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "FX Deal TMO Checker Task":
            case "FX TMO Maker After Revise Task":
            case "FX Deal TMO Maker After Submit Task":
            case "FX Deal TMO Checker Request Cancel Task":
                endPointURL = api.server + api.url.workflow.trackingTmoChecker(instanceId, approverId);
                //endPointURL = api.server + api.url.workflow.transactionDeal(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "Pending Documents Checker Task":
                endPointURL = api.server + api.url.workflow.transactionDocuments(instanceId, approverId);
                self.IsTimelines(false);
                break;
            case "FX Deal TMO Netting Maker Task":
            case "FX Deal TMO Netting Checker Task":
                endPointURL = api.server + api.url.workflow.transactionCheckerNetting(instanceId, approverId);
                self.IsTimelines(true);
                break;
            default:
                endPointURL = api.server + api.url.workflow.transactionAllNormal(instanceId, approverId);
                self.IsTimelines(true);
                break;
        }
        return endPointURL;
    }
    function GetTransactionDataTMO(actTitle, instanceId, approverId) {
        var endPointURL;
        endPointURL = api.server + api.url.workflow.transactionCheckerTMO(instanceId, approverId);
        self.IsTimelines(true);
        return endPointURL;
    }


    function GetTransactionDataFD(actTitle, instanceId, approverId) {
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        var output = '';
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                output = GetTransactionDataFDCompleted(actTitle, instanceId, approverId);
                break;
            case HomeType.Monitoring:
            case HomeType.AllTransaction:
                output = GetTransactionDataFDAllTransaction(actTitle, instanceId, approverId);
                break;
        }
        return output
    }
    function GetTransactionDataFDCompleted(actTitle, instanceId, approverId) {
        var endPointURL;
        endPointURL = api.server + api.url.workflow.CompleteFD(instanceId, approverId);
        self.IsTimelines(true);
        return endPointURL;
    }
    function GetTransactionDataFDAllTransaction(actTitle, instanceId, approverId) {
        var endPointURL;
        endPointURL = api.server + api.url.workflow.CompleteFD(instanceId, approverId);
        self.IsTimelines(true);
        return endPointURL;
    }

    function GetTransactionDataUT(actTitle, instanceId, approverId) {
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        var output = '';
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                output = GetTransactionDataUTCompleted(actTitle, instanceId, approverId);
                break;
            case HomeType.Monitoring:
            case HomeType.AllTransaction:
                output = GetTransactionDataUTAllTransaction(actTitle, instanceId, approverId);
                break;
        }
        return output
    }
    function GetTransactionDataUTCompleted(actTitle, instanceId, approverId) {
        var endPointURL;
        endPointURL = api.server + api.url.workflowut.UTBranchChecker(instanceId, approverId);
        self.IsTimelines(true);
        return endPointURL;
    }
    function GetTransactionDataUTAllTransaction(actTitle, instanceId, approverId) {
        var endPointURL;
        endPointURL = api.server + api.url.workflowut.UTBranchChecker(instanceId, approverId);
        self.IsTimelines(true);
        return endPointURL;
    }
    function GetTransactionDataLoan(actTitle, instanceId, approverId) {
        var endPointURL;
        endPointURL = api.server + api.url.workflowloan.LoanCompleteTransaction(instanceId, approverId);
        self.IsTimelines(true);
        return endPointURL;
    }
    function GetTransactionDataCollateral(actTitle, instanceId, approverId) {

    }
    function GetTransactionDataCIF(actTitle, instanceId, approverId) {
        var endPointURL;
        endPointURL = api.server + api.url.workflowcif.transactionContactCIF(instanceId, approverId);
        self.IsTimelines(true);
        return endPointURL;
    }

    function GetTransactionDataDF(actTitle, instanceId, approverId) {
        var endPointURL;
        endPointURL = api.server + api.url.documentFlow.getTransactionDF(instanceId, approverId);
        self.IsTimelines(true);
        return endPointURL;
    }
    // Get Nintex task outcomes
    function GetTaskOutcomes(taskListID, taskListItemID) {
        var task = {
            TaskListID: taskListID,
            TaskListItemID: taskListItemID
        };

        var options = {
            url: "/_vti_bin/DBSNintex/Services.svc/GetTaskOutcomesByListID",
            data: ko.toJSON(task)
        };

        Helper.Sharepoint.Nintex.Post(options, OnSuccessTaskOutcomes, OnError, OnAlways);
    }

    function GetGenerateFileNames(applicationID) {

        var options = {
            url: api.server + api.url.generatedfile + '/' + applicationID,
            token: accessToken
        };
        Helper.Ajax.Get(options, OnSuccessGetGenerateFileNames, OnError, OnAlways);
    }

    self.DownloadFile = function (data) { DownloadFile(data); };

    self.IsPaymentGroup = ko.observable(false);

    function DownloadFile(data) {
        try {
            //var arrfilename = data.FileName.split('_');
            //document.location.href = _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/download.aspx?SourceUrl=%2FShared%20Documents%2FReport%5F' + arrfilename[1] + '%5F' + arrfilename[2] + '%5F' + arrfilename[3] + '&FldUrl=&Source=http%3A%2F%2Fmysweetlife%3A90%2FShared%2520Documents%2FForms%2FAllItems%2Easpx';
            filename = data.FileName;
            document.location.href = _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/download.aspx?SourceUrl=%2FShared%20Documents%2F' + filename;
        } catch (e) {
            console.log(e);
        }
    }

    function OnSuccessGetGenerateFileNames(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.GenerateFileNames([]);
            for (var i = 0 ; i < data.length; i++) {
                var FileAttribute = {
                    ApplicationID: data[i].ApplicationID,
                    CreateDate: data[i].CreateDate,
                    FileNameView: i + 1 + '. ' + data[i].FileName,
                    FileName: data[i].FileName,
                    ItemID: data[i].ItemID,
                    URL: data[i].URL,
                };
                self.GenerateFileNames.push(FileAttribute);
            }
        }
        else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    // Event handlers declaration start
    function OnSuccessGetParameters(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            self.Products(data.Product);
            self.Channels(data.Channel);
            self.Currencies(data.Currency);
            self.DebitCurrencies(data.Currency);
            self.BizSegments(data.BizSegment);
            //self.Banks(data.Bank);
            self.FXCompliances(data.FXCompliance);
            self.BankCharges(data.ChargesType);
            self.AgentCharges(data.ChargesType);
            self.ProductTypes(data.ProductType);
            self.LLDs(data.LLD);
            self.UnderlyingDocs(data.UnderltyingDoc);
            self.POAFunctions(data.POAFunction);
            self.DocumentTypes(data.DocType);
            self.DocumentPurposes(data.PurposeDoc);
            self.ddlDocumentPurpose_a(data.PurposeDoc);
            self.ParameterCIF().ddlCellPhoneMethodIDs(ko.mapping.toJS(ModificationData));
            self.ParameterCIF().ddlFaxMethodIDs(ko.mapping.toJS(ModificationData));
            self.ParameterCIF().ddlRiskRatingResults(ko.mapping.toJS(RiskRatingResultData));
            self.ParameterCIF().ddlDispatchModeTypes(ko.mapping.toJS(DispatchModeData));
            self.ParameterCIF().ddlMaritalStatus(ko.mapping.toJS(MaritalStatusData));

            self.ddlDocumentType_a(data.DocType);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.Tasks(data.Rows); //Put the response in ObservableArray

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));

            //$("#transaction-progress").hide();
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    function GetIsFlowValas(FlowValas) {
        return (FlowValas == null ? false : FlowValas.IsFlowValas);
    }
    function GetProductTypeID(FlowValas) {
        return (FlowValas == null ? null : FlowValas.ID);
    }

    self.LLDFlag = ko.observable();

    function OnSuccessGetTransactionData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            var mapping = {
                //'ignore': ["LastModifiedDate", "LastModifiedBy"]
            };
            var pID = self.ProductID();
            var pName = GetProductName();
            var actTitle = self.ActivityTitle();

            $(".TransHistory").hide();
            viewModel.IsPPUOrBranchRole();
            switch (pName) {
                case ProductName.sknbulk:
                case ProductName.payment:
                    OnSuccessGetTransactionDataPayment(pID, actTitle, data, mapping);
                    break;
                case ProductName.tmo:
                    OnSuccessGetTransactionDataTMO(actTitle, data, mapping);
                    break;
                case ProductName.fd:
                    OnSuccessGetTransactionDataFD(actTitle, data, mapping);
                    break;
                case ProductName.ut:
                    OnSuccessGetTransactionDataUT(actTitle, data, mapping);
                    break;
                case ProductName.loan:
                    OnSuccessGetTransactionDataLoan(actTitle, data, mapping);
                    break;
                case ProductName.collateral:
                    OnSuccessGetTransactionDataCollateral(actTitle, data, mapping);
                    break;
                case ProductName.cif:
                    OnSuccessGetTransactionDataCIF(actTitle, data, mapping);
                    break;
                case ProductName.df:
                    OnSuccessGetTransactionDataDF(actTitle, data, mapping);
                    break;
                default:
                    break;
            }


        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    function OnSuccessGetTransactionDataPayment(pID, actTitle, data, mapping) {
        if (pID == ConsProductID.OTTProductIDCons) {
            if (data.Transaction.Currency.ID != Const_AmountLLD.NonIDRSelected) {
                if (data.Transaction.Amount > Const_AmountLLD.USDAmount) {
                    self.IsLimit(true);
                    console.log("LIMITNYA : TRUE");
                }
                else {
                    self.IsLimit(false);
                    console.log("LIMITNYA : False");
                }
            }
        } else {
            self.IsLimit(false);
        }

        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                OnSuccessGetTransactionDataPaymentCompleted(pID, data, mapping);
                if (pID == ConsProductID.SKNBulkProductIDCons) {
                    GetTransactionSKNBulk(data, mapping);
                }
                break;
            case HomeType.AllTransaction:
                OnSuccessGetTransactionDataPaymentAllTransaction(pID, actTitle, data, mapping);
                if (pID == ConsProductID.SKNBulkProductIDCons) {
                    GetTransactionSKNBulk(data, mapping);
                }
                break;
            case HomeType.Monitoring:
                if (actTitle.toLowerCase() == "completed" || actTitle.toLowerCase() == "canceled") {
                    OnSuccessGetTransactionDataPaymentCompleted(pID, data, mapping);
                    if (pID == ConsProductID.SKNBulkProductIDCons) {
                        GetTransactionSKNBulk(data, mapping);
                    }
                }
                else {
                    OnSuccessGetTransactionDataPaymentAllTransaction(pID, actTitle, data, mapping);
                    if (pID == ConsProductID.SKNBulkProductIDCons) {
                        GetTransactionSKNBulk(data, mapping);
                    }
                }
                break;
        }

        if (pID == ConsProductID.RTGSProductIDCons || pID == ConsProductID.SKNProductIDCons) {
            GetGenerateFileNames(data.Transaction.ApplicationID);
        }

        if (pID == ConsProductID.RTGSProductIDCons || pID == ConsProductID.SKNProductIDCons ||
            pID == ConsProductID.OTTProductIDCons || pID == ConsProductID.OverbookingProductIDCons) {
            GetTransactionHistoryPayment(data, mapping);
        }
    }

    function OnSuccessGetTransactionDataPaymentCompleted(pID, data, mapping) {
        self.IsTransactionSKNBulks(true);
        self.IsTransactionSKNBulksOriginal(true);
        SetTopUrgentChecklist(pID, data);
        SetFXTotalTransaction(pID, data);

        if (viewModel.ProductID() == ConsProductID.FXNettingProductIDCons) {
            GetNettingPurpose();
            self.Selected().NettingPurpose(data.Transaction.NettingPurpose.ID);
            var t_IsFxTransaction = (data.Transaction.Currency.Code != 'IDR' && data.Transaction.AccountCurrencyCode == 'IDR');
            var t_IsFxTransactionToIDR = (data.Transaction.Currency.Code == 'IDR' && data.Transaction.AccountCurrencyCode != 'IDR');

            self.t_IsFxTransaction(t_IsFxTransaction);
            self.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);

            if (self.t_IsFxTransaction() || self.t_IsFxTransactionToIDR()) {
                GetTotalTransaction(data);// For Get Total IDR - FCY & Utilization
            }

        }

        if (data.Transaction.ChargingAccountCurrency != null) {
            var ChargingAccountCurrency = ko.utils.arrayFirst(viewModel.Currencies(), function (item) {
                return item.ID == data.Transaction.ChargingAccountCurrency;
            })
            viewModel.ChargingAccCurrency(ChargingAccountCurrency);
        }

        if (viewModel.ProductID() == ConsProductID.FXProductIDCons) {
            if (data != null && data.UnderlyingCodeID == 999) {
                self.isNewUnderlying(true);
            }
            if (data.Transaction.IsOtherAccountNumber) {
                data.Transaction.Account.AccountNumber = data.Transaction.OtherAccountNumber;
            }

            if (data.Transaction.DealUnderlying == null) {
                self.DealUnderlying.ID = '-';
                self.DealUnderlying.Description = '-';
            }
            else {
                self.DealUnderlying.ID = data.Transaction.DealUnderlying.ID;
                self.DealUnderlying.Description = data.Transaction.DealUnderlying.Description;
            }
        }
        self.TransactionAllNormal(ko.mapping.toJS(data, mapping));
        if (viewModel.ProductID() == ConsProductID.FXProductIDCons) {
            self.IsSwapTransaction(false);
            self.IsNettingTransaction(false);
            self.Swap_NettingDFNumber('')
            if (data.Transaction.SwapType != null && data.Transaction.SwaptType != 0) {
                SetSwapTypeTransaction();
            }
            if (data.Transaction.IsNettingTransaction != null && data.Transaction.IsNettingTransaction == true) {
                SetNettingTypeTransaction();
            }
        }
    }

    function OnSuccessGetTransactionDataPaymentAllTransaction(pID, actTitle, data, mapping) {
        var activitytitle = actTitle;
        SetFXTotalTransaction(pID, data);
        SetTopUrgentChecklist(pID, data);
        switch (activitytitle) {
            case "Pending Documents Checker Task":
                if (data.Transaction.Bank.Code == '999') {
                    data.Transaction.Bank.Description = data.Transaction.OtherBeneBankName;
                    data.Transaction.Bank.SwiftCode = data.Transaction.OtherBeneBankSwift;
                }
                else {
                    $('#bank-code').prop('disabled', true);
                }

                self.TransactionAllNormal(ko.mapping.toJS(data, mapping));
                break;
            case "CBO Maker Task":
                self.TransactionContact(ko.mapping.toJS(data, mapping));
                self.CIF_c(self.TransactionContact().Transaction.Customer.CIF);
                $('#btnAddNewContact').hide();
                break;
            case "CBO Checker Task":
                self.TransactionContact(ko.mapping.toJS(data, mapping));
                break;
            case "FX Deal Checker Task":
            case "FX Deal Maker Task":
                if (data.Transaction.IsOtherAccountNumber) {
                    data.Transaction.Account.AccountNumber = data.Transaction.OtherAccountNumber;
                }

                if (data.Transaction.DealUnderlying == null) {
                    self.DealUnderlying.ID = '-';
                    self.DealUnderlying.Description = '-';
                }
                else {
                    self.DealUnderlying.ID = data.Transaction.DealUnderlying.ID;
                    self.DealUnderlying.Description = data.Transaction.DealUnderlying.Description;
                }
                self.TransactionDeal(ko.mapping.toJS(data, mapping));
                DealID = data.Transaction.ID;
                break;
            case "FX Deal TMO Netting Maker Task":
            case "FX Deal TMO Netting Checker Task":
                if (data.Transaction.Customer.Underlyings == null) {
                    data.Transaction.Customer.Underlyings = [];
                }
                GetNettingPurpose();
                self.Selected().NettingPurpose(data.Transaction.NettingPurpose.ID);
                self.TransactionAllNormal(ko.mapping.toJS(data, mapping));
                self.TransactionAllNormal().Transaction.Customer.Underlyings = [];
                self.TransactionAllNormal().Transaction.Documents = [];
                if (data.Transaction.Customer.Underlyings != null) {
                    $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                        $(this).prev().focus();
                    });

                    ko.utils.arrayForEach(data.Transaction.Customer.Underlyings, function (item) {
                        self.TransactionAllNormal().Transaction.Customer.Underlyings.push(item);
                    });

                }


                if (data.Transaction.Documents != null) {
                    ko.utils.arrayForEach(data.Transaction.Documents, function (item) {
                        self.TransactionAllNormal().Transaction.Documents.push(item);
                    });
                }


                var t_IsFxTransaction = (data.Transaction.Currency.Code != 'IDR' && data.Transaction.AccountCurrencyCode == 'IDR');
                var t_IsFxTransactionToIDR = (data.Transaction.Currency.Code == 'IDR' && data.Transaction.AccountCurrencyCode != 'IDR');

                self.t_IsFxTransaction(t_IsFxTransaction);
                self.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);

                if (self.t_IsFxTransaction() || self.t_IsFxTransactionToIDR()) {
                    GetTotalTransaction(data);// For Get Total IDR - FCY & Utilization
                }

                break;
            case "FX Deal TMO Checker Task":
            case "FX TMO Maker After Revise Task":
            case "FX Deal TMO Maker After Submit Task":
            case "FX Deal TMO Checker Request Cancel Task":
                if (data != null && data.UnderlyingCodeID == 999) {
                    self.isNewUnderlying(true);
                }
                if (data.Transaction.IsOtherAccountNumber) {
                    data.Transaction.Account.AccountNumber = data.Transaction.OtherAccountNumber;
                }

                if (data.Transaction.DealUnderlying == null) {
                    self.DealUnderlying.ID = '-';
                    self.DealUnderlying.Description = '-';
                }
                else {
                    self.DealUnderlying.ID = data.Transaction.DealUnderlying.ID;
                    self.DealUnderlying.Description = data.Transaction.DealUnderlying.Description;
                }

                self.TransactionDeal(ko.mapping.fromJS(data, mapping));
                DealID = data.Transaction.ID;
                break;
            default:
                //SKN BULK===================================
                switch (activitytitle) {
                    case "Payment Checker Task":
                        //muncul giri dua2 nya
                        self.IsTransactionSKNBulks(true);
                        self.IsTransactionSKNBulksOriginal(true);
                        break;
                    case "Payment Maker Task":
                        //skn grid salah satu
                        self.IsTransactionSKNBulks(true);
                        self.IsTransactionSKNBulksOriginal(false);
                        break;
                    case "IPE ACK1 Task":
                        //skn grid salah satu
                        self.IsTransactionSKNBulks(true);
                        self.IsTransactionSKNBulksOriginal(true);
                        break;
                    case "IPE ACK2 Task":
                        //skn grid salah satu
                        self.IsTransactionSKNBulks(true);
                        self.IsTransactionSKNBulksOriginal(true);
                        break;
                    default:
                        self.IsTransactionSKNBulks(true);
                        self.IsTransactionSKNBulksOriginal(true);
                        break;

                }

                //TRANSACTION================================
                if (data.Transaction.IsOtherAccountNumber) {
                    data.Transaction.Account.AccountNumber = data.Transaction.OtherAccountNumber;
                }

                if (data.Transaction.LLD != null) {
                    self.Selected().LLD(data.Transaction.LLD.ID);
                    self.LLDCode(data.Transaction.LLD.Description);
                }
                else {
                    self.Selected().LLD(null);
                }

                self.IsPPURole(true);
                if (data.Transaction.SourceID != null) {
                    if (data.Transaction.SourceID == 1) {
                        self.SourceName('CSO');
                    } else {
                        if (data.Transaction.SourceID == 2) {
                            self.SourceName('Counter');
                        } else {
                            self.SourceName('');
                            self.IsPPURole(false);
                        }
                    }
                } else {
                    self.IsPPURole(false);
                }

                if (data.Transaction.Bank.Code == '999') {
                    data.Transaction.Bank.Description = data.Transaction.OtherBeneBankName;
                    data.Transaction.Bank.SwiftCode = data.Transaction.OtherBeneBankSwift;
                }
                else {
                    $('#bank-code').prop('disabled', true);
                }

                if (data.Transaction.ChargingAccountCurrency != null) {
                    var ChargingAccountCurrency = ko.utils.arrayFirst(viewModel.Currencies(), function (item) {
                        return item.ID == data.Transaction.ChargingAccountCurrency;
                    })
                    viewModel.ChargingAccCurrency(ChargingAccountCurrency);
                }

                // PAYMENT===================================
                if (data.Payment != null) {
                    if (data.Payment.LLD != null) {
                        self.Selected().LLD(data.Payment.LLD.ID);
                        self.LLDCode(data.Payment.LLD.Description);
                    }
                    else {
                        self.Selected().LLD(null);
                    }
                    if (data.Payment.Bank != null && data.Payment.Bank.Code == '999') {
                        data.Payment.Bank.Description = data.Payment.OtherBeneBankName;
                        data.Payment.Bank.SwiftCode = data.Payment.OtherBeneBankSwift;
                    }
                }

                // CALLBACK=====================================
                if (data.Callback != null) {
                    if (data.Callback.Callback != null) {
                        if (data.Callback.Callback.Bank != null && data.Callback.Callback.Bank.Code == '999' == '999') {
                            data.Callback.Callback.Bank.Description = data.Callback.Callback.OtherBeneBankName;
                            data.Callback.Callback.Bank.SwiftCode = data.Callback.Callback.OtherBeneBankSwift;
                        }
                    }
                }

                if (data.Transaction.TransactionRate == 0) {
                    data.Transaction.TransactionRate = null;
                } else {
                    data.Transaction.TransactionRate = formatNumber_r(data.Transaction.TransactionRate);
                }
                self.TransactionAllNormal(ko.mapping.toJS(data, mapping));
        }
    }

    function OnSuccessGetTransactionDataTMO(actTitle, data, mapping) {
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                GetInitiatorRole(viewModel.Initiator());
                OnSuccessGetTransactionDataTMOCompleted(actTitle, data, mapping);
                break;
            case HomeType.Monitoring:
            case HomeType.AllTransaction:
                GetInitiatorRole(viewModel.Initiator());
                OnSuccessGetTransactionDataTMOAllTransaction(actTitle, data, mapping);
                break;

        }
    }
    function OnSuccessGetTransactionDataTMOCompleted(actTitle, data, mapping) {
        OnSuccessGetTransactionDataTMOAllTransaction(actTitle, data, mapping);
    }
    function OnSuccessGetTransactionDataTMOAllTransaction(actTitle, data, mapping) {
        self.TransactionCheckerTMO(ko.mapping.toJS(data, mapping));
    }

    function OnSuccessGetTransactionDataFD(actTitle, data, textStatus, mapping) {
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                OnSuccessGetTransactionDataFDCompleted(actTitle, data, mapping);
                break;
            case HomeType.Monitoring:
            case HomeType.AllTransaction:
                OnSuccessGetTransactionDataFDAllTransaction(actTitle, data, mapping);
                break;

        }

    }
    function OnSuccessGetTransactionDataFDCompleted(actTitle, data, mapping) {
        OnSuccessGetTransactionDataFDAllTransaction(actTitle, data, mapping);
    }
    function OnSuccessGetTransactionDataFDAllTransaction(actTitle, data, mapping) {
        //add henggar
        self.IsPPURole(true);
        if (data.Transaction.SourceID != null) {
            if (data.Transaction.SourceID == 1) {
                self.SourceName('CSO');
            } else {
                if (data.Transaction.SourceID == 2) {
                    self.SourceName('Counter');
                } else {
                    self.SourceName('');
                    self.IsPPURole(false);
                }
            }
        } else {
            self.IsPPURole(false);
        }
        //end
        //if (data.Transaction.Tenor == 0) {
        //    data.Transaction.Tenor = "";
        //}
        //if (data.Transaction.InterestRate == 0) {
        //    data.Transaction.InterestRate = "";
        //}
        HideShowByTypeTransaction(data.Transaction.TransactionType.TransTypeID);
        GetFDMatrixDOA(data.Transaction.ApprovalName);
        self.TransactionAllFDNormal(ko.mapping.toJS(data, mapping));        
    }

    function OnSuccessGetTransactionDataUT(actTitle, data, textStatus, mapping) {
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
                SetUTVisibilty(data);
                OnSuccessGetTransactionDataUTCompleted(actTitle, data, mapping);
                break;
            case HomeType.Monitoring:
            case HomeType.AllTransaction:
                OnSuccessGetTransactionDataUTAllTransaction(actTitle, data, mapping);
                break;
        }

    }
    function OnSuccessGetTransactionDataUTCompleted(actTitle, data, mapping) {
        OnSuccessGetTransactionDataUTAllTransaction(actTitle, data, mapping);
    }
    function OnSuccessGetTransactionDataUTAllTransaction(actTitle, data, mapping) {
        SetUTVisibilty(data);
        self.TransactionAllUTNormal(ko.mapping.toJS(data, mapping));
    }

    function OnSuccessGetTransactionDataLoan(actTitle, data, textStatus, mapping) {
        self.IsPPURole(true);
        if (data.Transaction.SourceID != null) {
            if (data.Transaction.SourceID == 1) {
                self.SourceName('CSO');
            } else {
                if (data.Transaction.SourceID == 2) {
                    self.SourceName('Counter');
                } else {
                    self.SourceName('');
                    self.IsPPURole(false);
                }
            }
        } else {
            self.IsPPURole(false);
        }
        self.TransactionAllLoanNormal(ko.mapping.toJS(data, mapping));
        self.IshiddenFinacleLoanMaker();
    }
    function OnSuccessGetTransactionDataCollateral(actTitle, data, textStatus, mapping) {

    }
    //add by fandi
    function OnSuccessGetTransactionDataCIF(actTitle, data, mapping) {
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        if (data.Transaction.IsPOA) {
            self.IsAccountJoin(true)
        } else {
            self.IsAccountJoin(false)
        }
        if (data.Transaction.IsPOI || data.Transaction.IsPOI) {
            self.IsAccountJoin(true)
        } else {
            self.IsAccountJoin(false)
        }
        if (data.Transaction.AccountTypeLoiPoiID == 2) {
            self.IsCustomerFormJoin(true)
        } else {
            self.IsCustomerFormJoin(false)
        }
        switch (homeType) {
            case HomeType.Canceled:
                OnSuccessGetTransactionDataCIFCompleted(actTitle, data, mapping);
                break;
            case HomeType.Completed:
                self.IsCIFChangeRMCompleted(true);
                if (self.Outcomes() !== null && self.Outcomes() !== undefined && self.Outcomes() !== '') {
                    if (self.Outcomes().length > 0) {
                        self.Outcomes() = [];
                        self.Outcomes().push({ ID: 0, Name: "Generate Excel File", Description: "Generate Excel File" });
                        self.Outcomes().push({ ID: 0, Name: "Generate CSV File", Description: "Generate CSV File" });
                        //self.Outcomes().unshift({ ID: 0, Name: "Generate Excel File", Description: "Generate Excel File" });
                        //self.Outcomes().unshift({ ID: 0, Name: "Generate CSV File", Description: "Generate CSV File" });
                    } else {
                        self.Outcomes().push({ ID: 0, Name: "Generate Excel File", Description: "Generate Excel File" });
                        self.Outcomes().push({ ID: 0, Name: "Generate CSV File", Description: "Generate CSV File" });
                    }
                    self.Outcomes(self.Outcomes());
                }
                OnSuccessGetTransactionDataCIFCompleted(actTitle, data, mapping);
                break;
            case HomeType.Monitoring:
            case HomeType.AllTransaction:
                OnSuccessGetTransactionDataCIFAllTransaction(actTitle, data, mapping);
                break;

        }

    }
    function OnSuccessGetTransactionDataDF(actTitle, data, mapping) {
        var homeType = viewModel.FormMenuCustom().toLowerCase();
        switch (homeType) {
            case HomeType.Canceled:
            case HomeType.Completed:
            case HomeType.Monitoring:
            case HomeType.AllTransaction:
                self.TransactionDF(ko.mapping.toJS(data, mapping));
                if (self.TransactionDF().Transaction.Others !== undefined && self.TransactionDF().Transaction.Others !== null && self.TransactionDF().Transaction.Others.trim() !== "") {
                    self.OthersForDF(true);
                } else {
                    self.OthersForDF(false);
                }

                break;

        }

    }
    function OnSuccessGetTransactionDataCIFCompleted(actTitle, data, mapping) {
        OnSuccessGetTransactionDataCIFAllTransaction(actTitle, data, mapping);
    }

    function GetFDMatrixDOA(idMatrixDoa) {
        var options = {
            url: api.server + api.url.getfdmatrixdoa,
            token: accessToken
        };

        Helper.Ajax.Get(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                if (data.length > 0) {
                    var DOA = ko.utils.arrayFirst(data,
                      function (item) {
                          return item.ID == idMatrixDoa;
                      });
                    if (DOA !== null && DOA !== undefined && DOA !== '') {
                        console.log(DOA);
                        self.ApprovalRole(DOA.Role);
                        self.ApprovalName(DOA.Name);
                    }                    
                }
            }
        }, OnError, OnAlways)
    }

    function OnSuccessGetTransactionDataCIFAllTransaction(actTitle, data, mapping) {
        var MaritalStatusType = '';
        var DispatchModeTypes = '';
        var RiskRatingResults = '';
        //AddchangeRM
        if (data.Transaction.RetailCIFCBO == null) {
            var tmpDate = ko.toJS(RetailCIFCBO);
            data.Transaction.RetailCIFCBO = tmpDate;
        }

        if (data.Transaction.Documents != null) {
            for (var i = 0; data.Transaction.Documents.length > i; i++) {
                self.Documents_Funding.push(data.Transaction.Documents[i]);
            }
        }

        //add by fandi changeRM >1 menandakan punya data di retailcifcbo
        if (data.Transaction.RetailCIFCBO.RetailCIFCBOID >= 1) {
            if (data.Transaction.RetailCIFCBO.MaritalStatusID != null && MaritalStatusData[data.Transaction.RetailCIFCBO.MaritalStatusID - 1] != undefined) {
                MaritalStatusType = MaritalStatusData[data.Transaction.RetailCIFCBO.MaritalStatusID - 1].Name;
                data.Transaction.RetailCIFCBO.MaritalStatus = MaritalStatusType;
            }
            if (data.Transaction.RetailCIFCBO.RiskRatingResultID > 0) {
                if (RiskRatingResultData[data.Transaction.RetailCIFCBO.RiskRatingResultID - 1] != null && RiskRatingResultData[data.Transaction.RetailCIFCBO.RiskRatingResultID - 1] != undefined) {//updated by dani 9-6-2016
                    RiskRatingResults = RiskRatingResultData[data.Transaction.RetailCIFCBO.RiskRatingResultID - 1].Name;
                    data.Transaction.RetailCIFCBO.RiskRatingResult = RiskRatingResults;
                }
            }
            if (data.Transaction.RetailCIFCBO.DispatchModeTypeID > 0 && data.Transaction.RetailCIFCBO.DispatchModeTypeID != null) {
                for (var i = 0; i < DispatchModeData.length; i++) {
                    if (DispatchModeData[i].ID == data.Transaction.RetailCIFCBO.DispatchModeTypeID) {
                        DispatchModeTypes = DispatchModeData[i].Name;
                        if (data.Transaction.RetailCIFCBO.DispatchModeTypeID != ConsDispatchModeType['-']) {
                            data.Transaction.RetailCIFCBO.DispatchModeOther = "";
                        }
                    }
                }
                data.Transaction.RetailCIFCBO.DispatchModeType = DispatchModeTypes;
                $("#DispatchModeType").text(DispatchModeTypes);
            }
            if (data.Transaction.RetailCIFCBO.RiskRatingResultID > 0) {
                if (RiskRatingResultData[data.Transaction.RetailCIFCBO.RiskRatingResultID - 1] != null && RiskRatingResultData[data.Transaction.RetailCIFCBO.RiskRatingResultID - 1] != undefined) {//updated by dani 9-6-2016
                    RiskRatingResults = RiskRatingResultData[data.Transaction.RetailCIFCBO.RiskRatingResultID - 1].Name;
                    data.Transaction.RetailCIFCBO.RiskRatingResult = RiskRatingResults;
                }
            }
        }
        if (data.Transaction.AccountNumberLienUnlien != null && data.Transaction.AccountNumberLienUnlien.length > 0) {
            var dataSelected = [];
            for (var i = 0; i < data.Transaction.AccountNumberLienUnlien.length; i++) {
                if (data.Transaction.AccountNumberLienUnlien[i].IsSelected == true) {
                    dataSelected.push(data.Transaction.AccountNumberLienUnlien[i]);
                }
            }
            data.Transaction.AccountNumberLienUnlien = dataSelected;
        }
        //end

        //if (data.Transaction.OfficePhoneNumbers.length != 0) {
        //    for (var i = 0; i < data.Transaction.OfficePhoneNumbers.length; i++) {
        //        $('#Office' + i).val(data.Transaction.OfficePhoneNumbers[i].ActionType);
        //    }
        //}
        //if (data.Transaction.SelularPhoneNumbers.length != 0) {
        //    for (var i = 0; i < data.Transaction.SelularPhoneNumbers.length; i++) {
        //        $('#Office' + i).val(data.Transaction.SelularPhoneNumbers[i].ActionType);
        //    }
        //}
        //if (data.Transaction.HomePhoneNumbers.length != 0) {
        //    for (var i = 0; i < data.Transaction.HomePhoneNumbers.length; i++) {
        //        $('#Office' + i).val(data.Transaction.HomePhoneNumbers[i].ActionType);
        //    }
        //}
        self.TransactionMakerCBO(ko.mapping.toJS(data, mapping));
        if (data.Transaction.SelularPhoneNumbers.length != 0) {
            for (var i = 0; i < data.Transaction.SelularPhoneNumbers.length; i++) {
                if (data.Transaction.SelularPhoneNumbers[i].ActionType == '2') {
                    $("#ActionType" + i).removeClass('hidden');
                    $("#CountryCode" + i).prop('disabled', true);
                    $("#CityCode" + i).prop('disabled', true);
                }
            }
        }
        if (data.Transaction.HomePhoneNumbers.length != 0) {
            for (var i = 0; i < data.Transaction.HomePhoneNumbers.length; i++) {
                if (data.Transaction.HomePhoneNumbers[i].ActionType == '2') {
                    $("#HomeActionType" + i).removeClass('hidden');
                    $("#HomeCountryCode" + i).prop('disabled', true);
                    $("#HomeCityCode" + i).prop('disabled', true);
                }
            }
        }
        if (data.Transaction.OfficePhoneNumbers.length != 0) {
            for (var i = 0; i < data.Transaction.OfficePhoneNumbers.length; i++) {
                if (data.Transaction.OfficePhoneNumbers[i].ActionType == '2') {
                    $("#OfficeActionType" + i).removeClass('hidden');
                    $("#OfficeCountryCode" + i).prop('disabled', true);
                    $("#OfficeCityCode" + i).prop('disabled', true);
                }
            }
        }
        self.Selected().Product(data.Transaction.Product.ID);
        self.Selected().TransactionType(data.Transaction.RetailCIFCBO.RequestTypeID);
        self.Selected().MaintenanceType(data.Transaction.RetailCIFCBO.MaintenanceTypeID);
        self.Selected().DispatchModeType(data.Transaction.RetailCIFCBO.DispatchModeTypeID);
        self.Selected().Currency(data.Transaction.Currency.ID);
        self.Selected().MaritalStatusID(data.Transaction.RetailCIFCBO.MaritalStatusID);
        self.TransactionMakerCBO().Transaction = data.Transaction;

        self.Selected().FaxMethodID(data.Transaction.RetailCIFCBO.FaxMethodID);
        self.Selected().RiskRatingResult(data.Transaction.RetailCIFCBO.RiskRatingResultID);
        self.Selected().TransactionSubType(data.Transaction.TransactionSubType.ID);
        self.Selected().CIFASSETBERSIH(data.Transaction.RetailCIFCBO.NetAsset);
        self.Selected().CIFPENDPATANBULANAN(data.Transaction.RetailCIFCBO.MonthlyIncome);
        self.Selected().CIFPENGHASILANTAMBAHAN(data.Transaction.RetailCIFCBO.MonthlyExtraIncome);
        self.Selected().CIFPEKERJAAN(data.Transaction.RetailCIFCBO.Job);
        self.Selected().CIFTUJUANPEMBUKAANREKENING(data.Transaction.RetailCIFCBO.AccountPurpose);
        self.Selected().CIFPERKIRAANDANAMASUK(data.Transaction.RetailCIFCBO.IncomeForecast);
        self.Selected().CIFPERKIRAANDANAKELUAR(data.Transaction.RetailCIFCBO.OutcomeForecast);
        self.Selected().CIFPERKIRAANTRANSAKSIKELUAR(data.Transaction.RetailCIFCBO.TransactionForecast);
        self.Selected().CIFJENISIDENTITAS(data.Transaction.RetailCIFCBO.IdentityTypeID);
        self.Selected().CIFSUMBERDANA(data.Transaction.RetailCIFCBO.FundSource);
        SetCIFMaintenanceVisibility();
        //self.IsAllChecked = ko.observable(true);
        //if (!self.IsAllChecked()) {
        //    self.CheckAll(false);
        //} else {
        //    self.CheckAll(true);
        //}
        //add by fandi condition changerm 
        if (data.Transaction.MaintenanceType.ID == ConsCIFMaintenanceType.ChangeRMCons) {
            self.IsAllChecked = ko.observable(true);
        }
        if (data.Transaction.IsChangeRM == true)
            self.isChangeRMS(true);
        if (data.Transaction.IsSolID == true)
            self.isSolID(true);
        if (data.Transaction.IsSegment == true)
            self.isSegment(true);
        self.Selected().TransactionType(data.Transaction.TransactionType.ID);
        self.Selected().MaintenanceType(data.Transaction.MaintenanceType.ID);

        //self.TransactionALLCIFNormal(ko.mapping.toJS(data, mapping));

        //add henggar 01052017
        self.Selected().Education(data.Transaction.RetailCIFCBO.Education);
        self.Selected().Religion(data.Transaction.RetailCIFCBO.Religion);
        self.Selected().FATCAReviewStatus(data.Transaction.RetailCIFCBO.FatcaReviewStatus);
        self.Selected().FATCACRSStatus(data.Transaction.RetailCIFCBO.FatcaCRSStatus);
        self.Selected().TaxPayer(data.Transaction.RetailCIFCBO.FatcaTaxPayer);
        self.Selected().WithholdingCertificationType(data.Transaction.RetailCIFCBO.FatcaWithHolding);
        self.Selected().BeneficiaryCountry(data.Transaction.RetailCIFCBO.FatcaCountryCode);
        self.Selected().GrossIncome(data.Transaction.RetailCIFCBO.GrosIncomeccy);
        self.TaxPayerID(data.Transaction.RetailCIFCBO.FatcaTaxPayerID);
        self.FatcaReviewStatusDate(viewModel.LocalDate(data.Transaction.RetailCIFCBO.FatcaReviewStatusDate, true, false));
        self.DateOnForm(viewModel.LocalDate(data.Transaction.RetailCIFCBO.FatcaDateOnForm, true, false));

        if (data.Transaction.RetailCIFCBO.FatcaTaxPayerID == 0) {
            self.TaxPayerID('');
        } else {
            self.TaxPayerID(data.Transaction.RetailCIFCBO.FatcaTaxPayerID);
        }

        var status = false;
        if (self.Selected().Education() != null) {
            for (var i = 0; i < Education.length; i++) {
                if (Education[i].ID == self.Selected().Education()) {
                    self.Education(Education[i].Name);
                    status = true;
                }
            }
        } else {
            self.Education('');
        }

        if (self.Selected().GrossIncome() != null) {
            for (var i = 0; i < viewModel.Currencies().length; i++) {
                if (viewModel.Currencies()[i].ID == self.Selected().GrossIncome()) {
                    self.GrossIncomeccyName(viewModel.Currencies()[i].Code);
                    self.GrossIncomeccyDescription(viewModel.Currencies()[i].CodeDescription);
                    status = true;
                }
            }
        } else {
            self.GrossIncomeccyName('');
            self.GrossIncomeccyDescription('');
        }

        if (self.Selected().Religion() != null) {
            for (var i = 0; i < Religion.length; i++) {
                if (Religion[i].ID == self.Selected().Religion()) {
                    self.Religion(Religion[i].Name);
                    status = true;
                }
            }
        } else {
            self.Religion('');
        }

        if (self.Selected().FATCAReviewStatus() != null) {
            for (var i = 0; i < FATCAReviewStatus.length; i++) {
                if (FATCAReviewStatus[i].ID == self.Selected().FATCAReviewStatus()) {
                    self.FATCAReviewStatus(FATCAReviewStatus[i].Name);
                    status = true;
                }
            }
        } else {
            self.FATCAReviewStatus('');
        }

        if (self.Selected().FATCACRSStatus() != null) {
            for (var i = 0; i < FATCACRSStatus.length; i++) {
                if (FATCACRSStatus[i].ID == self.Selected().FATCACRSStatus()) {
                    self.FATCACRSStatus(FATCACRSStatus[i].Name);
                    status = true;
                }
            }
        } else {
            self.FATCACRSStatus('');
        }

        if (self.Selected().TaxPayer() != null) {
            for (var i = 0; i < TaxPayer.length; i++) {
                if (TaxPayer[i].ID == self.Selected().TaxPayer()) {
                    self.TaxPayer(TaxPayer[i].Name);
                    status = true;
                }
            }
        } else {
            self.TaxPayer('');
        }

        if (self.Selected().WithholdingCertificationType() != null) {
            for (var i = 0; i < WithholdingCertificationType.length; i++) {
                if (WithholdingCertificationType[i].ID == self.Selected().WithholdingCertificationType()) {
                    self.WithholdingCertificationType(WithholdingCertificationType[i].Name);
                    status = true;
                }
            }
        } else {
            self.WithholdingCertificationType('');
        }

        viewModel.IsStatusCRS(false);
        var statusCRS = viewModel.Selected().FATCACRSStatus();
        if (statusCRS == 1) {
            viewModel.IsStatusCRS(true);
        } else {
            viewModel.IsStatusCRS(false);
        }

        viewModel.IsTaxPayerType(false);
        var statusCRS = viewModel.Selected().TaxPayer();
        if (statusCRS == null) {
            viewModel.IsTaxPayerType(false);
        } else {
            viewModel.IsTaxPayerType(true);
        }
        //end
    }

    function SetCIFMaintenanceVisibility() {
        var RequestType = '';
        var MaintenanceType = '';

        //if (self.TransactionMakerCBO().Transaction.RetailCIFCBO.RequestTypeID != null ||self.TransactionMakerCBO().Transaction.RetailCIFCBO.RequestTypeID != 0) {
        if (self.TransactionMakerCBO().Transaction.RetailCIFCBO.RequestTypeID != null) {
            RequestType = self.TransactionMakerCBO().Transaction.RetailCIFCBO.RequestTypeID;

        } else if (self.TransactionMakerCBO().Transaction.ChangeRMModel.length > 0) {
            RequestType = self.TransactionMakerCBO().Transaction.TransactionType.ID;

        } else {
            RequestType = null;
        }

        //if (self.TransactionMakerCBO().Transaction.RetailCIFCBO.MaintenanceTypeID != null ||self.TransactionMakerCBO().Transaction.RetailCIFCBO.MaintenanceTypeID != 0) {
        if (self.TransactionMakerCBO().Transaction.RetailCIFCBO.MaintenanceTypeID != null) {
            MaintenanceType = self.TransactionMakerCBO().Transaction.RetailCIFCBO.MaintenanceTypeID;

        } else if (self.TransactionMakerCBO().Transaction.ChangeRMModel.length > 0) {
            MaintenanceType = self.TransactionMakerCBO().Transaction.MaintenanceType.ID;
        }
        else {
            MaintenanceType = null;
        }

        self.IsRequestTypeMaintenance(false);
        self.IsRequestTypeSuspendCIF(false);
        self.IsRequestTypeStandingInstruction(false);
        self.IsRequestTypeUnsuspendCIF(false);
        self.IsCIFPengkinianData(false);
        self.IsCIFUpdateFXTier(false);
        self.IsCIFAtmCard(false);
        self.IsCIFRiskRating(false);
        self.IsCIFAddCurrency(false);
        self.IsCIFAdditionalAccount(false);
        self.IsCIFLinkFFD(false);
        self.IsCIFFreezeUnfreeze(false);
        self.IsLienUnlien(false);
        self.IsCIFActiveDormant(false);
        self.IsCIFLPS(false);
        self.IsCIFLOIPOIPOA(false);
        self.IsCIFChangeRM(false);
        self.IsCIFActiveHPSP(false);
        self.IsCIFSuspendCIF(false);
        self.IsCIFUnsuspendCIF(false);
        self.IsCIFStandingInstruction(false);
        self.IsCIFTagUntagStaff(false);
        self.IsCIFDispatchMode(false);
        self.IsCIFATMClosure(false);
        //console.log(RequestType + " " + MaintenanceType);
        if (RequestType != null) {
            switch (RequestType) {
                case ConsTransactionType.MaintenanceCons:
                    if (MaintenanceType != null) {
                        self.IsRequestTypeMaintenance(true);
                        switch (MaintenanceType) {
                            case ConsCIFMaintenanceType.PengkinianDataCons:
                                self.IsCIFPengkinianData(true);
                                break;
                            case ConsCIFMaintenanceType.UpdateFXTierCons:
                                self.IsCIFUpdateFXTier(true);
                                break;
                            case ConsCIFMaintenanceType.AtmCardCons:
                                self.IsCIFAtmCard(true);
                                break;
                            case ConsCIFMaintenanceType.RiskRatingCons:
                                self.IsCIFRiskRating(true);
                                break;
                            case ConsCIFMaintenanceType.AddCurrencyCons:
                                self.IsCIFAddCurrency(true);
                                break;
                            case ConsCIFMaintenanceType.AdditionalAccountCons:
                                self.IsCIFAdditionalAccount(true);
                                break;
                            case ConsCIFMaintenanceType.LinkFFDCons:
                                self.IsCIFLinkFFD(true);
                                break;
                            case ConsCIFMaintenanceType.FreezeUnfreezeCons:
                                self.IsCIFFreezeUnfreeze(true);
                                break;
                            case ConsCIFMaintenanceType.ActiveDormantCons:
                                self.IsCIFActiveDormant(true);
                                break;
                            case ConsCIFMaintenanceType.LPSCons:
                                self.IsCIFLPS(true);
                                break;
                            case ConsCIFMaintenanceType.LOIPOIPOACons:
                                self.IsCIFLOIPOIPOA(true);
                                break;
                            case ConsCIFMaintenanceType.ChangeRMCons:
                                self.IsCIFChangeRM(true);
                                break;
                            case ConsCIFMaintenanceType.ActiveHPSPCons:
                                self.IsCIFActiveHPSP(true);
                                break;
                            case ConsCIFMaintenanceType.SuspendCIF:
                                self.IsCIFSuspendCIF(true);
                                break;
                            case ConsCIFMaintenanceType.UnsuspendCIFCons:
                                self.IsCIFUnsuspendCIF(true);
                                break;
                            case ConsCIFMaintenanceType.StandingInstructionCons:
                                self.IsCIFStandingInstruction(true);
                                break;
                            case ConsCIFMaintenanceType.TagUntagStaffCons:
                                self.IsCIFTagUntagStaff(true);
                                break;
                            case ConsCIFMaintenanceType.DispatchMode:
                                self.IsCIFDispatchMode(true);
                                //self.SetCustomerAutoCompleteCIF();
                                break;
                            case ConsCIFMaintenanceType.ATMClosure:
                                self.IsCIFATMClosure(true);
                                //self.SetCustomerAutoCompleteCIF();
                                break;
                            case ConsCIFMaintenanceType.LienUnlien:
                                self.IsLienUnlien(true);
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case ConsTransactionType.SuspendCIFCons:
                    self.IsRequestTypeSuspendCIF(true);
                    break;
                case ConsTransactionType.UnsuspendCIFCons:
                    self.IsRequestTypeUnsuspendCIF(true);
                    break;
                case ConsTransactionType.StandingInstructionCons:
                    self.IsRequestTypeStandingInstruction(true);
                    break;
                default:
                    break;
            }
        }
    }


    //end 
    function SetTopUrgentChecklist(pID, data) {
        switch (pID) {
            case ConsProductID.RTGSProductIDCons:
            case ConsProductID.SKNProductIDCons:
            case ConsProductID.OTTProductIDCons:
            case ConsProductID.OverbookingProductIDCons:
                //aridya 20161119 skip untuk IPE sudah dihandle di cs ketika tarik data
                if (viewModel.ModePayment() == 'BCP2') {
                    data.Transaction.IsTopUrgentChain = data.Transaction.IsTopUrgent == 2 ? 1 : 0;
                }
                data.Transaction.IsTopUrgent = data.Transaction.IsTopUrgent == 1 ? 1 : 0;
                break;
            case ConsProductID.FXProductIDCons:
                break;
            case ConsProductID.FXNettingProductIDCons:
                break;
            case ConsProductID.TMOProductIDCons:
                break;
            case ConsProductID.FDProductIDCons:
                break;
            case ConsProductID.IDInvestmentProductIDCons:
            case ConsProductID.SavingPlanProductIDCons:
            case ConsProductID.UTOnshoreproductIDCons:
            case ConsProductID.UTOffshoreProductIDCons:
            case ConsProductID.UTCPFProductIDCons:
                break;
            case ConsProductID.CollateralProductIDCons:
                break;
            case ConsProductID.CIFProductIDCons:
                break;
            case ConsProductID.LoanDisbursmentProductIDCons:
            case ConsProductID.LoanRolloverProductIDCons:
            case ConsProductID.LoanIMProductIDCons:
            case ConsProductID.LoanSettlementProductIDCons:
                break;
            default:
                break;
        }
    }

    function SetMappingAttachment(customerName) {
        CustomerUnderlyingFile.CustomerUnderlyingMappings([]);
        for (var i = 0 ; i < self.TempSelectedAttachUnderlying().length; i++) {
            var sValue = {
                ID: 0,
                UnderlyingID: self.TempSelectedAttachUnderlying()[i].ID,
                UnderlyingFileID: 0,
                AttachmentNo: customerName + '-(A)'
            }
            CustomerUnderlyingFile.CustomerUnderlyingMappings.push(sValue);
        }
    }

    function SetFXTotalTransaction(pID, data) {
        switch (pID) {
            case ConsProductID.RTGSProductIDCons:
            case ConsProductID.OTTProductIDCons:
            case ConsProductID.SKNProductIDCons:
            case ConsProductID.OverbookingProductIDCons:
                cifData = data.Transaction.Customer.CIF;
                var t_IsFxTransaction = (data.Transaction.Currency.Code != 'IDR' && data.Transaction.DebitCurrency.Code == 'IDR');
                var t_IsFxTransactionToIDR = (data.Transaction.Currency.Code == 'IDR' && data.Transaction.DebitCurrency.Code != 'IDR');
                self.t_IsFxTransaction(t_IsFxTransaction);
                self.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);
                break;

            case ConsProductID.FXProductIDCons:
                cifData = data.Transaction.Customer.CIF;
                self.IsAnnualStatement(data.Transaction.StatementLetter.ID == CONST_STATEMENT.AnnualStatement_ID ? true : false);
                switch (self.ActivityTitle()) {
                    case "FX Deal TMO Checker Task":
                    case "FX TMO Maker After Revise Task":
                    case "FX Deal TMO Maker After Submit Task":
                    case "FX Deal TMO Checker Request Cancel Task":
                        var t_IsFxTransaction = (data.Transaction.BuyCurrency.Code != 'IDR' && data.Transaction.SellCurrency.Code == 'IDR');
                        var t_IsFxTransactionToIDR = (data.Transaction.BuyCurrency.Code == 'IDR' && data.Transaction.SellCurrency.Code != 'IDR');
                        self.t_IsFxTransaction(t_IsFxTransaction);
                        self.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);
                        break;
                    default:
                        var t_IsFxTransaction = (data.Transaction.Currency.Code != 'IDR' && data.Transaction.AccountCurrencyCode == 'IDR');
                        var t_IsFxTransactionToIDR = (data.Transaction.Currency.Code == 'IDR' && data.Transaction.AccountCurrencyCode != 'IDR');
                        self.t_IsFxTransaction(t_IsFxTransaction);
                        self.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);
                        break;
                }
                break;
            case ConsProductID.FXNettingProductIDCons:
                //Code here
                break;
            default:
                break;
        }

        FXModel.cif = cifData;
        FXModel.token = accessToken;
        if (self.t_IsFxTransaction() || self.t_IsFxTransactionToIDR()) {
            GetTotalTransaction(data);// For Get Total IDR - FCY & Utilization
        }
    }

    //Rizki 2016-01-23
    function GetTransactionHistoryPayment(data, mapping) {
        //add by fandi for transaction history
        var tmpDate = ko.toJS(TransactionHistoryModel);
        self.TransactionHistory().Transaction = tmpDate;
        self.TransactionHistory().Transaction.Customer = tmpDate;
        self.TransactionHistory().Transaction.Product = tmpDate;
        self.TransactionHistory().Transaction.Currency = tmpDate;
        self.TransactionHistory().Transaction.Channel = tmpDate;
        self.TransactionHistory().Transaction.BizSegment = tmpDate;
        self.TransactionHistory().Transaction.TransactionMaker = tmpDate;

        self.TransactionHistory().Payment = tmpDate;
        self.TransactionHistory().Payment.Customer = tmpDate;
        self.TransactionHistory().Payment.Product = tmpDate;
        self.TransactionHistory().Payment.Currency = tmpDate;
        self.TransactionHistory().Payment.Channel = tmpDate;
        self.TransactionHistory().Payment.BizSegment = tmpDate;
        self.TransactionHistory().Payment.TransactionMaker = tmpDate;

        if (data.Transaction != null) {
            if (data.Transaction.Product != null) {
                //Header for transaction history
                self.CreateDateHeader(data.Transaction.CreateDate);
                self.ApplicationIDHeader(data.Transaction.ApplicationID);
                self.CIFHeaderHistory(data.Transaction.Customer.CIF);
                self.CustomerNameHeader(data.Transaction.Customer.Name);
                self.ProductCodeHeader(data.Transaction.Product.Code);
                self.ProductNamerHeader(data.Transaction.Product.Name);
                self.CurrencyCodeHeader(data.Transaction.Currency.Code);
                self.CurrencyDescriptionHeader(data.Transaction.Currency.Description);
                self.ChannelNameHeader(data.Transaction.Channel.Name);
                //self.StartDate_u(self.LocalDate(data.StartDate, true, false));
                self.ApplicationDateHeader(self.LocalDate(data.Transaction.ApplicationDate, true, false));
                self.BizSegmentDescriptionDescHeader(data.Transaction.BizSegment.Description);
                self.BizSegmentNameDescHeader(data.Transaction.BizSegment.Name);
                self.IsNewCustomerHeader(data.Transaction.IsNewCustomer);

                //end
                $(".TransHistory").show();
                self.TransactionHistory(ko.mapping.toJS(data, mapping));
            }
        }
        else {
            if (data.Payment != null) {
                if (data.Payment.Product != null) {
                    //Header for transaction history
                    self.CreateDateHeader = ko.observable(data.Payment.CreateDate);
                    self.ApplicationIDHeader = ko.observable(data.Payment.ApplicationID);
                    self.CIFHeaderHistory(data.Payment.Customer.CIF);
                    self.CustomerNameHeader(data.Payment.Customer.Name);
                    self.ProductCodeHeader(data.Payment.Product.Code);
                    self.ProductNamerHeader(data.Payment.Product.Name);
                    self.CurrencyCodeHeader(data.Payment.Currency.Code);
                    self.CurrencyDescriptionHeader(data.Payment.Currency.Description);
                    self.ChannelNameHeader(data.Payment.Channel.Name);
                    self.ApplicationDateHeader(self.LocalDate(data.Payment.ApplicationDate, true, false));
                    self.BizSegmentDescriptionDescHeader(data.Payment.BizSegment.Description);
                    self.BizSegmentNameDescHeader(data.Payment.BizSegment.Name);
                    self.IsNewCustomerHeader(data.Payment.IsNewCustomer);
                    //end
                    $(".TransHistory").show();
                    self.TransactionHistory(ko.mapping.toJS(data, mapping));
                }
            }
        }
    }

    function SetVerifiedData(data, isVerified) {
        for (i = 0; i <= data.length - 1; i++) {
            data[i].IsVerified = isVerified;
        }

        self.Callback = function () {
            data;
        };
        self.Callback();
    }

    function OnSuccessSaveApprovalData(outcomeId) {
        // completing task
        CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
    }

    function DisableForm() {
        $("#transaction-form").find(" input, select, textarea, button").prop("disabled", true);
    }

    function OnSuccessTaskOutcomes(data, textStatus, jqXHR) {
        if (data.IsSuccess) {
            self.Outcomes(data.Outcomes);

            self.IsPendingNintex(data.IsPending);
            self.IsAuthorizedNintex(data.IsAuthorized);
            self.MessageNintex(data.Message);

            // Get transaction Data
            GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());

            // lock current task only if user had permission
            /*if (self.IsPendingNintex() == true && self.IsAuthorizedNintex() == true) {
             LockAndUnlockTaskHub("Lock", self.WorkflowInstanceID(), self.ApproverId(), self.SPTaskListItemID(), self.ActivityTitle());
             }*/
        } else {
            if (data.Outcomes == null) {
                self.MessageNintex(data.Message);
                GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());
            }
            else {
                ShowNotification("An error Occured", jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        }
    }

    function OnSuccessCompletingTask(data, textStatus, jqXHR) {
        if (data.IsSuccess) {
            // send notification
            ShowNotification('Completing Task Success', jqXHR.responseJSON.Message, 'gritter-success', false);

            // close dialog task
            $("#modal-form").modal('hide');

            // reload tasks & show progress bar
            $("#transaction-progress").show();

            self.IsOtherAccountNumber(false);

            self.GetData();
        } else {
            ShowNotification('Completing Task Failed', jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    //#region Threshold & Validation
    function GetTotalTransaction(transaction) {
        if (viewModel != null) {
            var paramhreshold = GetValueParamterThreshold(transaction);
            if (paramhreshold != undefined) {
                var options = {
                    url: api.server + api.url.helper + "/GetTotalTransactionIDRFCY",
                    token: accessToken,
                    params: {},
                    data: ko.toJSON(paramhreshold)
                };
                Helper.Ajax.Post(options, function (data, textStatus, jqXHR) {
                    if (jqXHR.status == 200) {
                        TotalPPUModel.Total.IDRFCYPPU = data.TotalTransaction;
                        TotalDealModel.Total.IDRFCYDeal = data.TotalTransaction;
                        self.TotalTransFX(data.TotalTransaction);

                        TotalPPUModel.Total.UtilizationPPU = data.TotalUtilizationAmount;
                        TotalDealModel.Total.UtilizationDeal = data.TotalUtilizationAmount;
                        TotalDealModel.Total.RemainingBalance = data.AvailableUnderlyingAmount;
                        self.Rounding(data.RoundingValue);
                        self.ThresholdType(data.ThresholdType);
                        self.ThresholdValue(data.ThresholdValue);
                        self.IsNoThresholdValue(data.ThresholdValue == 0 ? true : false);
                        //SetHitThreshold(parseFloat(GetAmountUSD()));
                        if (self.IsCanEditUnderlying()) {
                            SetCalculateFX(paramhreshold.AmountUSD);
                        } else {
                            SetCalculateFX(paramhreshold.AmountUSD);
                            //SetCalculateFXChecker(paramhreshold.AmountUSD);
                        }
                        //GetCalculateFX();
                    }
                }, OnError);
            }
        }
    }
    function GetValueParamterThreshold(transaction) {
        var paramhreshold;
        var Transaction;
        switch (self.ActivityTitle()) {
            case "PPU Maker Task":
            case "PPU Maker After Payment Task":
            case "PPU Maker After Checker and Caller Task":
            case "PPU Checker Task":
            case "Pending Documents Checker Task":
            case "PPU Checker After PPU Caller Task":
            case "PPU Checker Cancellation Task":
            case "PPU Caller Task":
            case "Caller Loan Task":
            case "CBO Maker Task":
            case "CBO Checker Task":
            case "Branch Maker Reactivation Task":
            case "CBO Maker Reactivation Task":
            case "CBO Checker Reactivation Task":
            case "Branch Checker Reactivation Task":
            case "Lazy Approval Task for Exceptional Handling Case":
            case "Lazy App fo Except":
            case "Lazy Approval Task for UTC Case":
            case "FX Deal Maker Task":
            case "FX Deal Checker Task":
            case "Completed":
            case "Canceled":
            case "FX Deal TMO Netting Maker Task":
            case "FX Deal TMO Netting Checker Task":
                Transaction = transaction.Transaction;
                break;
            case "FX Deal TMO Checker Task":
            case "FX TMO Maker After Revise Task":
            case "FX Deal TMO Maker After Submit Task":
            case "FX Deal TMO Checker Request Cancel Task":
                transaction.Transaction.DebitCurrency = transaction.Transaction.SellCurrency;
                transaction.Transaction.Currency = transaction.Transaction.BuyCurrency;
                Transaction = transaction.Transaction;
                break;
            case "Payment Checker Task":
            case "Payment Maker Task":
            case "Payment Maker Revise Task":
                //Transaction = transaction.Payment;
                Transaction = transaction.Transaction;
                break;
            default:
                break;
        }
        if (Transaction != null) {
            paramhreshold = {
                ProductTypeID: GetProductTypeID(Transaction.ProductType),
                DebitCurrencyCode: Transaction.DebitCurrency.Code,
                TransactionCurrencyCode: Transaction.Currency.Code,
                IsResident: Transaction.IsResident,
                AmountUSD: parseFloat(Transaction.AmountUSD),
                CIF: Transaction.Customer.CIF,
                AccountNumber: Transaction.IsOtherAccountNumber ? null : Transaction.Account == null ? Transaction.AccountNumber : Transaction.Account.AccountNumber,
                IsJointAccount: Transaction.IsJointAccount != null ? Transaction.IsJointAccount : Transaction.Account.IsJointAccount,
                StatementLetterID: (Transaction.StatementLetter != null ? Transaction.StatementLetter.ID : null),
                TZReference: null
            }
        }
        return paramhreshold;
    }

    //#endregion

    self.IsPPUOrBranchRole = function () {
        var i = 0;
        for (i = 0; i < viewModel.SPUser.Roles.length; i++) {
            if (Const_RoleName[viewModel.SPUser.Roles[i].ID].toLowerCase() == "dbs ppu head office maker") {
                self.IsPPURole(true);
                break;
            }
        }
    }

    //bangkit
    function GetRateAmount(form, CurrencyID) {
        var options = {
            url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, function OnSuccessGetRateAmount(data, textStatus, jqXHR) {
            if (jqXHR.status = 200) {
                // nothing todo
            } else {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
            }
        }, OnError, OnAlways);
    }

    self.idrrate = ko.observable();

    self.GetRateIDR = function () { GetRateIDR() };

    function GetRateIDR() {
        var options = {
            url: api.server + api.url.currency + "/CurrencyRate/" + "1",
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetRateIDR, OnError, OnAlways);
    }


    function OnSuccessGetRateIDR(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            self.idrrate(data.RupiahRate);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }


    //start azam sknbulk

    //add aridya 20161028 get transaction for skn bulk ~OFFLINE~
    function GetTransactionSKNBulk(data, mapping) {
        //console.log("minta data " + new Date());
        // viewModel.ChangedRowSKNBulk([]);// add aridya 20161206 reinitialise variable
        var options = {
            url: api.server + api.url.workflowsknbulk.transactionCheckerSKNBulk(data.Transaction.WorkflowInstanceID, data.Transaction.ID),
            token: accessToken
        };
        Helper.Ajax.Get(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                //console.log("data masuk " + new Date());
                //ko.mapping.fromJS(data.sknBulkData, {}, viewModel.TransactionSKNBulks);
                //ko.mapping.fromJS(data.sknBulkDataOriginal, {}, viewModel.TransactionSKNBulksOriginal);
                //console.log("mulai mapping ko.mapping.toJS " + new Date());
                if (self.ActivityTitle() == "Payment Maker Task" || self.ActivityTitle() == "Payment Maker Revise Task") {
                    viewModel.PaymentMaker().TransactionSKNBulk = ko.mapping.toJS(data.sknBulkData);
                    viewModel.PaymentMaker().TransactionSKNBulkOriginal = ko.mapping.toJS(data.sknBulkData);
                } else if (self.ActivityTitle() == "Payment Checker Task" || self.ActivityTitle() == "IPE ACK1 Task" || self.ActivityTitle() == "IPE ACK2 Task" || self.ActivityTitle() == "Canceled" || self.ActivityTitle() == "Completed" || self.ActivityTitle() == "Canceled By IPE") {
                    viewModel.PaymentChecker().TransactionSKNBulk = ko.mapping.toJS(data.sknBulkData);
                    if (data.sknBulkDataOriginal != null) {
                        viewModel.PaymentChecker().TransactionSKNBulkOriginal = ko.mapping.toJS(data.sknBulkDataOriginal);
                    }
                }
                //console.log("done mapping ko.mapping.toJS " + new Date());
                //aridya 20161201 change to w2ui table
                if (self.ActivityTitle() == "Payment Maker Task" || self.ActivityTitle() == "Payment Maker Revise Task") {
                    var intervalGridSKNBulkMaker = setInterval(function () {
                        //console.log("masuk proses render table " + new Date());
                        if ($('#SKNBulkWorksheetTable').length > 0) {
                            viewModel.RenderGridSKNBulk(viewModel.PaymentMaker().TransactionSKNBulk, 'SKNBulkWorksheetTable');
                            clearInterval(intervalGridSKNBulkMaker);
                            //console.log("done " + new Date());
                        }
                    }, 100)
                }
                //end add w2ui               

                //add aridya 20161130 tambah row jadi merah kalo diubah di maker
                if (self.ActivityTitle() == "Payment Checker Task" || self.ActivityTitle() == "IPE ACK1 Task" || self.ActivityTitle() == "IPE ACK2 Task" || self.ActivityTitle() == "Canceled" || self.ActivityTitle() == "Completed" || self.ActivityTitle() == "Canceled By IPE") {
                    viewModel.RenderGridSKNBulkChecker(viewModel.PaymentChecker().TransactionSKNBulk, 'SKNBulkWorksheetTable');
                    if (viewModel.PaymentChecker().TransactionSKNBulkOriginal != undefined) {
                        viewModel.RenderGridSKNBulkChecker(viewModel.PaymentChecker().TransactionSKNBulkOriginal, 'SKNBulkWorksheetTableOriginal');
                    }


                    //aridya add 20161130 tambah row jadi merah kalo di maker diubah
                    if (viewModel.PaymentChecker().TransactionSKNBulk.length > 0) { //if (viewModel.TransactionSKNBulks().length > 0) {
                        var intervalGridSKNBulk = setInterval(function () {
                            //aridya changed to w2ui grid
                            if (w2ui['SKNBulkWorksheetTable'] != undefined) {
                                //20161206 aridya ganti logic untuk pewarnaan pada checker
                                for (var ii = 0; ii < w2ui['SKNBulkWorksheetTable'].records.length; ii++) {
                                    if (viewModel.PaymentChecker().TransactionSKNBulk[ii].IsChanged) { //tuning aridya 20161207 viewModel.ChangedRowSKNBulk().indexOf(ii)
                                        w2ui['SKNBulkWorksheetTable'].set(ii + 1, { w2ui: { style: { 7: "background-color: red" } } });
                                        w2ui['SKNBulkWorksheetTable'].set(ii + 1, { w2ui: { style: { 8: "background-color: red" } } });
                                    } else {
                                        w2ui['SKNBulkWorksheetTable'].set(ii + 1, { w2ui: { style: { 7: "background-color: yellow" } } });
                                        w2ui['SKNBulkWorksheetTable'].set(ii + 1, { w2ui: { style: { 8: "background-color: yellow" } } });
                                    }
                                }
                                clearInterval(intervalGridSKNBulk);
                            }
                            //end add
                        }, 100);
                        var intervalGridSKNBulkOriginal = setInterval(function () {
                            //aridya changed to w2ui grid
                            if (w2ui['SKNBulkWorksheetTableOriginal'] != undefined) {
                                //20161206 aridya ganti logic untuk pewarnaan pada checker
                                for (var ii = 0; ii < w2ui['SKNBulkWorksheetTableOriginal'].records.length; ii++) {
                                    if (viewModel.PaymentChecker().TransactionSKNBulk[ii].IsChanged) { //tuning aridya 20161207 viewModel.ChangedRowSKNBulk().indexOf(ii) >= 0
                                        w2ui['SKNBulkWorksheetTableOriginal'].set(ii + 1, { w2ui: { style: { 7: "background-color: red" } } });
                                        w2ui['SKNBulkWorksheetTableOriginal'].set(ii + 1, { w2ui: { style: { 8: "background-color: red" } } });
                                    } else {
                                        w2ui['SKNBulkWorksheetTableOriginal'].set(ii + 1, { w2ui: { style: { 7: "background-color: yellow" } } });
                                        w2ui['SKNBulkWorksheetTableOriginal'].set(ii + 1, { w2ui: { style: { 8: "background-color: yellow" } } });
                                    }
                                }
                                clearInterval(intervalGridSKNBulkOriginal);
                            }
                        }, 100);
                    }
                }
            }
        }, OnError, OnAlways);
    }
    //end add
    //end azam sknbulk

    //Apply bank auto complete

    //Apply auto complete

    self.UploadReactivationDoc = ko.observable();

    function UploadReactivationDoc(SaveApprovalData, instanceId, approverId, outcomeId) {
        // uploading documents. after upload completed, see SaveTransaction()
        var data = {
            ApplicationID: self.TransactionAllNormal().Transaction.ApplicationID,
            CIF: self.TransactionAllNormal().Transaction.Customer.CIF,
            Name: self.TransactionAllNormal().Transaction.Customer.Name
        };

        //bangkit
        if (self.TransactionAllNormal().Transaction.Documents.length > 0) {
            for (var i = 0; i < self.TransactionAllNormal().Transaction.Documents.length; i++) {
                UploadFile(i, data, self.TransactionAllNormal().Transaction.Documents[i], SaveApprovalData, instanceId, approverId, outcomeId);
            }
        } else {
            SaveApprovalData(instanceId, approverId, outcomeId);
        }
    }

    // save underlying file

    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
    // Event handlers declaration end

    self.GenerateFileNames = ko.observableArray([GenerateFileModel]);
};

function GetNettingPurpose() {
    $.ajax({
        async: false,
        type: "GET",
        url: api.server + api.url.parametersystem + "/partype/" + ConsPARSYS.tmoNettingPurpose,
        contentType: "application/json; charset=utf-8; odata=verbose;",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + accessToken
        },
        success: function (data, textStatus, jqXHR) {
            viewModel.NettingPurpose(ko.mapping.toJS(data.Parsys));
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });

}
var GenerateFileModel = {
    ItemID: ko.observable(),
    FileName: ko.observable(),
    URL: ko.observable(),
    ApplicationID: ko.observable(),
    CreateDate: ko.observable()
};

//add by fandi retail cif
var ParameterSystemCIFModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var ParameterCIF = {
    ddlCellPhoneMethodIDs: ko.observableArray([ParameterSystemCIFModel]),
    ddlHomePhoneMethodIDs: ko.observableArray([ParameterSystemCIFModel]),
    ddlOfficePhoneMethodIDs: ko.observableArray([ParameterSystemCIFModel]),
    ddlFaxMethodIDs: ko.observableArray([ParameterSystemCIFModel]),
    ddlRiskRatingResults: ko.observableArray([ParameterSystemCIFModel]),
    ddlDispatchModeTypes: ko.observableArray([ParameterSystemCIFModel]),
    ddlMaritalStatus: ko.observableArray([ParameterSystemCIFModel])
};
var SelectedModel = {
    //Andryanto 16 November 2015
    FNACore: ko.observableArray(),
    FunctionType: ko.observable(),
    AccountType: ko.observable(),
    TransactionType: ko.observable(),
    InvestmentID: ko.observable(),
    //end
    Product: ko.observable(),
    Currency: ko.observable(),
    Channel: ko.observable(),
    Account: ko.observable(),
    BizSegment: ko.observable(),
    Bank: ko.observable(),
    DocumentType: ko.observable(),
    DocumentPurpose: ko.observable(),
    NewCustomer: ko.observable({
        Currency: ko.observable(),
        BizSegment: ko.observable()
    }),
    BankCharges: ko.observable(),
    AgentCharges: ko.observable(),
    FXCompliaance: ko.observable(),
    LLD: ko.observable(),
    POAFunction: ko.observable(),
    ProductType: ko.observable(),
    UnderlyingDoc: ko.observable(),
    DocumentPurpose_a: ko.observable(),
    DocumentType_a: ko.observable(),
    DebitCurrency: ko.observable(),
    ProgramType: ko.observable(),
    LoanID: ko.observable(),
    FinacleScheme: ko.observable(),
    SanctionLimitCurrID: ko.observable(),
    AmountDisburseID: ko.observable(),//ddlAmountDisburseID: ko.observable(),
    Interest: ko.observable(),
    RepricingPlan: ko.observable(),
    ddlBizSegmentDesc: ko.observable(),
    BizSegment: ko.observable(),
    utilization: ko.observable(),
    Outstanding: ko.observable(),
    Frequency: ko.observable(),//peggingFrequency
    PrincipalFrequency: ko.observable(),
    InterestFrequency: ko.observable(),
    StatementLetter: ko.observable(),
    //cbo by fandi
    RequestType: ko.observable(),
    TransactionType: ko.observable(),
    MaintenanceType: ko.observable(),
    CIFSUMBERDANA: ko.observable(),
    CIFASSETBERSIH: ko.observable(),
    CIFPENDPATANBULANAN: ko.observable(),
    CIFPENGHASILANTAMBAHAN: ko.observable(),
    CIFPEKERJAAN: ko.observable(),
    CIFTUJUANPEMBUKAANREKENING: ko.observable(),
    CIFPERKIRAANDANAMASUK: ko.observable(),
    CIFPERKIRAANDANAKELUAR: ko.observable(),
    CIFPERKIRAANTRANSAKSIKELUAR: ko.observable(),
    CIFJENISIDENTITAS: ko.observable(),
    //add by fandi paymentmaker1 nostro dan sundry
    Nostro: ko.observable(),
    DynamicNostro: ko.observable(),
    Sundry: ko.observable(),
    //end
    //basri 26-11-2015
    NettingPurpose: ko.observable(),
    //end basri
    MaintenanceType: ko.observable(),
    TransactionSubType: ko.observable(),
    CellPhoneMethodID: ko.observable(),
    HomePhoneMethodID: ko.observable(),
    OfficePhoneMethodID: ko.observable(),
    FaxMethodID: ko.observable(),
    MaritalStatusID: ko.observable(),
    IdentityTypeID: ko.observable(),
    FundSource: ko.observable(),
    NetAsset: ko.observable(),
    MonthlyIncome: ko.observable(),
    MonthlyExtraIncome: ko.observable(),
    Job: ko.observable(),
    AccountPurpose: ko.observable(),
    IncomeForecast: ko.observable(),
    OutcomeForecast: ko.observable(),
    TransactionForecast: ko.observable(),
    DispatchModeType: ko.observable(),
    RiskRatingResult: ko.observable(),

    Education: ko.observable(),
    Religion: ko.observable(),
    FATCAReviewStatus: ko.observable(),
    FATCACRSStatus: ko.observable(),
    TaxPayer: ko.observable(),
    WithholdingCertificationType: ko.observable(),
    BeneficiaryCountry: ko.observable(),
    GrossIncome: ko.observable()
};
//end

// View Model
var viewModel = new ViewModel();

$.holdReady(true);
var intervalRoleName = setInterval(function () {
    if (Object.keys(Const_RoleName).length != 0) {
        $.holdReady(false);
        clearInterval(intervalRoleName);
    }
}, 100);

// jQuery Init
//$(document).on("RoleNameReady", function () {
$(document).ready(function () {

    // get table parameter
    var workflow = {
        State: $("#workflow-task-table").attr("workflow-state"),
        Outcome: $("#workflow-task-table").attr("workflow-outcome"),
        CustomOutcome: $("#workflow-task-table").attr("workflow-custom-outcome"),
        ShowContribute: $("#workflow-task-table").attr("workflow-show-contribute"),
        ShowActiveTask: $("#workflow-task-table").attr("workflow-show-active-task")
    };
    // get custom form for menu home
    var FormMenuCustom = {
        form: $("#form-menu-custom").attr("form")
    };
    // set workflow inside vm
    viewModel.WorkflowConfig(workflow);
    // $('#aspnetForm').after('<form id="frmUnderlying"></form>');
    // $("#modal-form-Attach").appendTo("#frmUnderlying");
    // $("#modal-form-Attach").appendTo("#frmUnderlying");
    // widget loader

    //set formCustom for home menu
    viewModel.FormMenuCustom(FormMenuCustom.form);
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    // block enter key from user to prevent submitted data.
    $(this).keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

            return false;
        }
    });

    // Load all templates
    $('script[src][type="text/html"]').each(function () {
        //alert($(this).attr("src"))
        var src = $(this).attr("src");
        if (src != undefined) {
            $(this).load(src);
        }
    });

    // scrollables
    $('.slim-scroll').each(function () {
        var $this = $(this);
        $this.slimscroll({
            height: $this.data('height') || 100,
            railVisible: true,
            alwaysVisible: true,
            color: '#D15B47'
        });
    });

    // tooltip
    $('[data-rel=tooltip]').tooltip();

    // datepicker
    $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    $('.time-picker').timepicker({
        defaultTime: "",
        minuteStep: 1,
        showSeconds: false,
        showMeridian: false
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });
    // ace file upload
    $('#document-path-upload').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });
    // ace file upload
    $('#document-path-upload1').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });
    // ace file upload
    $('#document-path').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });

    // Knockout custom file handler
    ko.bindingHandlers.file = {
        init: function (element, valueAccessor) {
            $(element).change(function () {
                var file = this.files[0];

                if (ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };

    // validation
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,

        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });
    //basri 28-09-2015  
    self.InitialProduct = ko.observable();
    self.ProductID = ko.observable();
    //end basri
    // Knockout Datepicker custom binding handler
    ko.bindingHandlers.datepicker = {
        init: function (element) {
            $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).datepicker("refresh");
        }
    };

    // Knockout Timepicker custom binding handler
    ko.bindingHandlers.timepicker = {
        init: function (element) {
            $(element).timepicker({
                defaultTime: "",
                minuteStep: 1,
                showSeconds: false,
                showMeridian: false
            }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).timepicker('showWidget');
        }
    };

    // Knockout Bindings
    //bangkit
    //ko.applyBindings(viewModel);
    ko.applyBindings(viewModel, document.getElementById('home-transaction'));
    // -- test error login
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(TokenOnSuccess, TokenOnError);
    } else {
        accessToken = $.cookie(api.cookie.name);
        StartTaskHub();
        GetCurrentUser(viewModel);
        FXModel.token = accessToken;
        //GetParameterData(FXModel, OnSuccessGetTotal, OnError);
        viewModel.GetDefaultDate();
        viewModel.GetParameters();
        viewModel.GetData();
        viewModel.GetRateIDR();
    }

    //Check Role Is Payment Group
    for (i = 0; i < spUser.Roles.length; i++) {
        if (Const_RoleName[spUser.Roles[i].ID].toLowerCase() == "dbs payment maker" || Const_RoleName[spUser.Roles[i].ID].toLowerCase() == "dbs payment checker") {
            viewModel.IsPaymentGroup(true);
            break;
        }
    }

    // Modal form on close handler
    $("#modal-form").on('hidden.bs.modal', function () {
        //alert("close")

        LockAndUnlockTaskHub("unlock", viewModel.WorkflowInstanceID(), viewModel.ApproverId(), viewModel.SPTaskListItemID(), viewModel.ActivityTitle());
    });
});

function ResetTZNumber() {
    viewModel.TZModel().Message = '';
    $('#tz-message').text('');
}

function IsDocumentCompleteCheck(data) {
    //IsDocumentCompleteCheck

    if (viewModel.TransactionAllNormal().Verify != undefined) {

        var verify = ko.utils.arrayFilter(viewModel.TransactionAllNormal().Verify, function (item) {
            return item.Name == "Document Completeness";
        });

        if (verify != null) {
            var index = viewModel.TransactionAllNormal().Verify.indexOf(verify[0]);

            if (verify[0].IsVerified) {
                viewModel.TransactionAllNormal().Transaction.IsDocumentComplete = true;
            } else {
                viewModel.TransactionAllNormal().Transaction.IsDocumentComplete = false;
            }
        }
    }
}

// Function to display notification.
// class name : gritter-success, gritter-warning, gritter-error, gritter-info
function ShowNotification(title, text, className, isSticky) {
    $.gritter.add({
        title: title,
        text: text,
        class_name: className,
        sticky: isSticky
    });
}
// chandra
function UpdatePaymentModel(data) {
    switch (data.name) {
        case "bene-name":
            viewModel.PaymentMaker().Payment.BeneName = data.value;
            break;
        case "bene-acc-number":
            viewModel.PaymentMaker().Payment.BeneAccNumber = data.value;
            break;
        default:
            break;
    }
    var update = viewModel.PaymentMaker();
    viewModel.PaymentMaker(ko.mapping.toJS(update));
    //bind auto complete
}

// Get SPUser from cookies
function GetCurrentUser() {
    //if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
    if ($.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);
        viewModel.SPUser = spUser;
    }
}

// Token validation On Success function
function TokenOnSuccess(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // call spuser function
    GetCurrentUser(viewModel);

    // call get data inside view model
    FXModel.token = accessToken;
    //GetParameterData(FXModel, OnSuccessGetTotal, OnError);
    viewModel.GetDefaultDate();
    viewModel.GetParameters();
    viewModel.GetData();
    viewModel.GetRateIDR();
    viewModel.GetFCYIDRTreshold();
}

// Token validation On Error function
function TokenOnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

//
function OnSuccessSignal() {
    //ShowNotification("Connection Success", "Connected to signal server", "gritter-success", false);
    /*    var hub = $.connection.task;
     $.connection.hub.url = config.signal.server+ "task";

     $.connection.hub.start()
     .done(function () {
     alert("Connected");
     })
     .fail(function() {
     alert("Connection failed!");
     });*/
}

function OnErrorSignal() {
    ShowNotification("Connection Failed", "Failed connected to signal server", "gritter-error", true);
}

function OnReceivedSignal(data) {
    ShowNotification(data.Sender, data.Message, "gritter-info", false);
}

// SignalR ----------------------
var taskHub = $.hubConnection(config.signal.server + "hub");
var taskProxy = taskHub.createHubProxy("TaskService");
var IsTaskHubConnected = false;

function StartTaskHub() {
    taskHub.start()
        .done(function () {
            IsTaskHubConnected = true;
        })
        .fail(function () {
            //alert('fail');
            ShowNotification("Hub Connection Failed", "Fail while try connecting to hub server", "gritter-error", true);
        }
    );
}

function LockAndUnlockTaskHub(lockType, instanceId, approverID, taskId, activityTitle) {
    if (IsTaskHubConnected) {
        var data = {
            WorkflowInstanceID: instanceId,
            ApproverID: approverID,
            TaskID: taskId,
            ActivityTitle: activityTitle,
            LoginName: spUser.LoginName,
            DisplayName: spUser.DisplayName
        };

        var LockType = lockType.toLowerCase() == "lock" ? "LockTask" : "UnlockTask";

        // try lock current task
        taskProxy.invoke(LockType, data)
            .fail(function (err) {
                ShowNotification("LockType Task Failed", err, "gritter-error", true);
            }
        );
    } else {
        ShowNotification("Task Manager", "There is no active connection to task server manager.", "gritter-error", true);
    }
}


taskProxy.on("Message", function (data) {
    ShowNotification(data.From, data.Message, "gritter-info", false);
});

taskProxy.on("NotifyInfo", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-info", false);
});

taskProxy.on("NotifyWarning", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-warning", false);
});

taskProxy.on("NotifyError", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-error", true);
});

function OnSuccessGetTotal(dataCall) {
    idrrate = dataCall.RateIDR;
    viewModel.FCYIDRTreshold(dataCall.FCYIDRTrashHold);
    viewModel.Treshold(dataCall.TreshHold);
}
function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}
function OnSuccessTotal() {
    viewModel.GetCalculateFX(0);
}
function HideShowByTypeTransaction(TransactionTypeID) {
    //netralisasi start
    viewModel.IsNewPlacement(false);
    viewModel.IsPrematureBreak(false);
    viewModel.IsBreakAtMaturity(false);
    viewModel.IsChangeOfInstruction(false);
    viewModel.IsFDMaintenance(false);
    //netralisasi end

    switch (TransactionTypeID) {
        case ConsTransactionType.fdNewPlacement:
            viewModel.IsNewPlacement(true);
            break;
        case ConsTransactionType.fdPrematurebreak:
            viewModel.IsPrematureBreak(true);
            break;
        case ConsTransactionType.fdBreakmaturity:
            viewModel.IsBreakAtMaturity(true);
            break;
        case ConsTransactionType.fdChangeInstruction:
            viewModel.IsChangeOfInstruction(true);
            break;
        case ConsTransactionType.fdMaintenance:
            viewModel.IsFDMaintenance(true);
            break;
    }
}
function GenerateCRM_CSV_Process(GenarateCRMModel) {
    var options = {
        url: "/_vti_bin/DBSGenerateFileV2/GenerateFileV2Service.svc/GenerateFileInfoV2_2",
        data: ko.toJSON(GenarateCRMModel)
    };
    Helper.Sharepoint.Nintex.Post(options, OnSuccessGenerateFileCRM, OnError, OnAlways);
}
function OnSuccessGenerateFileCRM(data, textStatus, jqXHR) {
    if (data.IsSuccess == true) {
        ShowNotification("Success", "Generate File Success", 'gritter-success', false);
        self.InsertedURL(data.InsertedURL);
        self.FileName(data.FileName);
        self.IsSuccess(true);
    }
    else {
        ShowNotification("Attention", "Generate File failed", 'gritter-warning', false);
        self.InsertedURL(null);
        self.FileName(null);
        self.IsSuccess(false);
    }
}