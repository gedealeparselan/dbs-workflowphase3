﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Linq;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Owin;


namespace DBS.SignalR
{
    public class DBSHub : Startup
    {
        //private static IList<SignalConnection> signals = new List<SignalConnection>();
        //private static IList<ClientTaskData> taskDatas = new List<ClientTaskData>();

        [HubName("TaskService")]
        public class TaskHub : Hub
        {
            private string message = string.Empty;

            private readonly ISignalRepository repository = new SignalRepository();

            //public TaskHub() 
            //{
            //    this.repository = new SignalRepository();
            //}

            protected override void Dispose(bool disposing)
            {
                if (repository != null)
                    repository.Dispose();

                base.Dispose(disposing);
            }

            public override async Task OnConnected()
            {
                // insert current connection to signal static
                //signals.Add(new SignalConnection() { ConnectionID = Context.ConnectionId });

                var output = new BroadcastMessage()
                {
                    From = "Server",
                    Message = "New Connection " + Context.ConnectionId,
                    Time = DateTime.UtcNow
                };

                //await Clients.All.Message(output);
            }

            public override async Task OnDisconnected(bool stopCalled)
            {
                // remove all from this connection id
                ReleaseTask(Guid.Parse(Context.ConnectionId));
            }

            public override async Task OnReconnected()
            {
                //var output = new BroadcastMessage()
                //{
                //    From = "Server",
                //    Message = "Reconnected " + Context.ConnectionId,
                //    Time = DateTime.UtcNow
                //};

                //await Clients.All.Message(output);
            }

            [HubMethodName("Send")]
            public async Task Send(BroadcastMessage msg)
            {
                var output = new BroadcastMessage()
                {
                    From = msg.From,
                    Message = msg.Message,
                    Time = DateTime.UtcNow
                };

                // broadcast message to all clients
                await Clients.All.Message(msg);

                // send message to sender
                await Clients.Caller.Message(new BroadcastMessage()
                {
                    From = "Server",
                    Message = "Your message has been sent to others",
                    Time = msg.Time
                });
            }

            [HubMethodName("LockTask")]
            public async Task LockTask(ClientTaskData taskData)
            {
                // set connection as user
                //var data = signals.Where(a => a.ConnectionID.Equals(Context.ConnectionId)).SingleOrDefault();
                //if (data != null)
                //{
                //    data.LoginName = taskData.LoginName;
                //    data.DisplayName = taskData.DisplayName;
                //}

                // read task data from db
                ClientTaskData checkTaskData = null;
                if (repository.CheckTask(taskData, ref checkTaskData, ref message))
                {
                    // validate while task is not locked before
                    //var check = taskDatas.Where(a => a.WorkflowInstanceID.Equals(taskData.WorkflowInstanceID) && a.TaskID.Equals(taskData.TaskID)).SingleOrDefault();
                    //if (check == null)
                    if (checkTaskData == null)
                    {
                        ClientTaskData clientTaskData = new ClientTaskData()
                        {
                            ConnectionID = Guid.Parse(Context.ConnectionId),
                            WorkflowInstanceID = taskData.WorkflowInstanceID,
                            TransactionID = taskData.TransactionID,
                            ApproverID = taskData.ApproverID,
                            TaskID = taskData.TaskID,
                            ActivityTitle = taskData.ActivityTitle,
                            LoginName = taskData.LoginName,
                            DisplayName = taskData.DisplayName
                        };

                        // add to lock task data
                        //taskDatas.Add(clientTaskData);

                        // prepare object result
                        BroadcastMessage msg = new BroadcastMessage();

                        // store data to db
                        if (repository.AddTask(clientTaskData, ref message))
                        {
                            msg = new BroadcastMessage()
                            {
                                From = "Task Manager",
                                Message = string.Format("Hi {0}, This task has been mark for you.", taskData.DisplayName),
                                Time = DateTime.UtcNow
                            };
                        }
                        else
                        {
                            msg = new BroadcastMessage()
                            {
                                From = "Task Manager",
                                Message = message,
                                Time = DateTime.UtcNow
                            };
                        }

                        // send message to sender
                        await Clients.Caller.NotifyInfo(msg);
                    }
                    else
                    {
                        //if (taskData.LoginName == check.LoginName)
                        if (taskData.DisplayName == checkTaskData.DisplayName)                        
                        {
                            await Clients.Caller.NotifyInfo(new BroadcastMessage()
                            {
                                From = "Task Manager",
                                Message = string.Format("Hi {0}, This task already opened by you.", taskData.DisplayName),
                                Time = DateTime.UtcNow
                            });
                        }
                        else
                        {
                            await Clients.Caller.NotifyWarning(new BroadcastMessage()
                            {
                                From = "Task Manager",
                                //Message = string.Format("Hi {0}, This task already opened by {1}.", taskData.DisplayName, check.DisplayName),
                                Message = string.Format("Hi {0}, This task already opened by {1}.", taskData.DisplayName, checkTaskData.DisplayName),                                
                                Time = DateTime.UtcNow
                            });
                        }
                    }
                }
                else
                {
                    await Clients.Caller.NotifyError(new BroadcastMessage()
                    {
                        From = "Task Manager",
                        Message = message,
                        Time = DateTime.UtcNow
                    });
                }
            }

            [HubMethodName("UnlockTask")]
            public async Task UnlockTask(ClientTaskData taskData)
            {
                taskData.ConnectionID = Guid.Parse(Context.ConnectionId);

                // remove all from this connection id
                if(!repository.ReleaseTask(taskData, ref message)){
                    await Clients.Caller.NotifyError(new BroadcastMessage()
                    {
                        From = "Task Manager",
                        Message = message,
                        Time = DateTime.UtcNow
                    });
                }
            }

            private async Task ReleaseTask(Guid connectionId)
            {
                /*
                // remove from signals
                var signal = signals.Where(a => a.ConnectionID.Equals(connectionId)).SingleOrDefault();

                if (signal != null)
                    signals.Remove(signal);

                // remove from task data
                var taskData = taskDatas.Where(a => a.ConnectionID.Equals(connectionId)).SingleOrDefault();

                if (taskData != null)
                    taskDatas.Remove(taskData);
                */
 
                if(!repository.ReleaseTaskByConnectionID(connectionId, ref message))
                {
                    await Clients.Caller.NotifyError(new BroadcastMessage()
                    {
                        From = "Task Manager",
                        Message = message,
                        Time = DateTime.UtcNow
                    });
                }
            }

            #region ccu
            [HubMethodName("LockTaskCCU")]
            public async Task LockTaskCCU(ClientTaskCCUData taskData)
            {
                // set connection as user
                //var data = signals.Where(a => a.ConnectionID.Equals(Context.ConnectionId)).SingleOrDefault();
                //if (data != null)
                //{
                //    data.LoginName = taskData.LoginName;
                //    data.DisplayName = taskData.DisplayName;
                //}

                // read task data from db
                ClientTaskCCUData checkTaskData = null;
                if (repository.CheckTaskCCU(taskData, ref checkTaskData, ref message))
                {
                    // validate while task is not locked before
                    //var check = taskDatas.Where(a => a.WorkflowInstanceID.Equals(taskData.WorkflowInstanceID) && a.TaskID.Equals(taskData.TaskID)).SingleOrDefault();
                    //if (check == null)
                    if (checkTaskData == null)
                    {
                        ClientTaskCCUData clientTaskData = new ClientTaskCCUData()
                        {
                            ConnectionID = Guid.Parse(Context.ConnectionId),
                            ActivationID = taskData.ActivationID,
                            DocDetailID = taskData.DocDetailID,
                            WaiverDocID = taskData.WaiverDocID,
                            CoverNoteDocID = taskData.CoverNoteDocID,
                            IsWaiverListPage = taskData.IsWaiverListPage,
                            IsCoverNoteListPage = taskData.IsCoverNoteListPage,
                            ActivityTitle = taskData.ActivityTitle,
                            LoginName = taskData.LoginName,
                            DisplayName = taskData.DisplayName
                        };

                        // add to lock task data
                        //taskDatas.Add(clientTaskData);

                        // prepare object result
                        BroadcastMessage msg = new BroadcastMessage();

                        // store data to db
                        if (repository.AddTaskCCU(clientTaskData, ref message))
                        {
                            msg = new BroadcastMessage()
                            {
                                From = "Task Manager",
                                Message = string.Format("Hi {0}, This task has been mark for you.", taskData.DisplayName),
                                Time = DateTime.UtcNow
                            };
                        }
                        else
                        {
                            msg = new BroadcastMessage()
                            {
                                From = "Task Manager",
                                Message = message,
                                Time = DateTime.UtcNow
                            };
                        }

                        // send message to sender
                        await Clients.Caller.NotifyInfo(msg);
                    }
                    else
                    {
                        //if (taskData.LoginName == check.LoginName)
                        if (taskData.DisplayName == checkTaskData.DisplayName)
                        {
                            await Clients.Caller.NotifyInfo(new BroadcastMessage()
                            {
                                From = "Task Manager",
                                Message = string.Format("Hi {0}, This task already opened by you.", taskData.DisplayName),
                                Time = DateTime.UtcNow
                            });
                        }
                        else
                        {
                            await Clients.Caller.NotifyWarning(new BroadcastMessage()
                            {
                                From = "Task Manager",
                                //Message = string.Format("Hi {0}, This task already opened by {1}.", taskData.DisplayName, check.DisplayName),
                                Message = string.Format("Hi {0}, This task already opened by {1}.", taskData.DisplayName, checkTaskData.DisplayName),
                                Time = DateTime.UtcNow
                            });
                        }
                    }
                }
                else
                {
                    await Clients.Caller.NotifyError(new BroadcastMessage()
                    {
                        From = "Task Manager",
                        Message = message,
                        Time = DateTime.UtcNow
                    });
                }
            }

            [HubMethodName("UnlockTaskCCU")]
            public async Task UnlockTaskCCU(ClientTaskCCUData taskData)
            {
                taskData.ConnectionID = Guid.Parse(Context.ConnectionId);

                // remove all from this connection id
                if (!repository.ReleaseTaskCCU(taskData, ref message))
                {
                    await Clients.Caller.NotifyError(new BroadcastMessage()
                    {
                        From = "Task Manager",
                        Message = message,
                        Time = DateTime.UtcNow
                    });
                }
            }
            #endregion
        }
    }

    
}