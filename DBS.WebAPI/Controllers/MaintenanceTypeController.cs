﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class MaintenanceTypeController : ApiController
    {
        private string message = string.Empty;

        private readonly IMaintenanceTypeRepository repository = null;

        public MaintenanceTypeController()
        {
            this.repository = new MaintenanceTypeRepository();
        }

        public MaintenanceTypeController(IMaintenanceTypeRepository repository)
        {
            this.repository = repository;
        }

       
        [ResponseType(typeof(IList<MaintenanceTypeModel>))]
        public HttpResponseMessage Get()
        {
            IList<MaintenanceTypeModel> output = new List<MaintenanceTypeModel>();

            if (!repository.GetMaintenanceType(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<MaintenanceTypeModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(IList<MaintenanceTypeModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<MaintenanceTypeModel> output = new List<MaintenanceTypeModel>();

            if (!repository.GetMaintenanceType(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<MaintenanceTypeModel>>(HttpStatusCode.OK, output);
            }
        }


        [ResponseType(typeof(MaintenanceTypeGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<MaintenanceTypeFilter> filters)
        {
            MaintenanceTypeGrid output = new MaintenanceTypeGrid();

            if (!repository.GetMaintenanceType(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "ID" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<MaintenanceTypeGrid>(HttpStatusCode.OK, output);
            }
        }


        [ResponseType(typeof(IList<MaintenanceTypeModel>))]
        [HttpGet]
        [Route("api/MaintenanceType/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<MaintenanceTypeModel> output = new List<MaintenanceTypeModel>();

            if (!repository.GetMaintenanceType(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<MaintenanceTypeModel>>(HttpStatusCode.OK, output);
            }
        }


        [ResponseType(typeof(MaintenanceTypeModel))]
        [Route("api/MaintenanceType/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            MaintenanceTypeModel output = new MaintenanceTypeModel();

            if (!repository.GetMaintenanceTypeByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<MaintenanceTypeModel>(HttpStatusCode.OK, output);
            }
        }
      
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(MaintenanceTypeModel MaintenanceType)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddMaintenanceType(MaintenanceType, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Maintenance Type data has been created." });
                }
            }
        }


        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/MaintenanceType/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, MaintenanceTypeModel MaintenanceType)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateMaintenanceType(id, MaintenanceType, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Maintenance Type data has been updated." });
                }
            }
        }

       
        [HttpDelete]
        [Route("api/MaintenanceType/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteMaintenanceType(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Maintenance Type data has been deleted." });
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}