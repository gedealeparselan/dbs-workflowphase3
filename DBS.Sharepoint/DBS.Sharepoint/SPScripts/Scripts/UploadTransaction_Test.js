
var accessToken;

var ViewModel = function () {
    var self = this;
	
	self.DocumentPath = ko.observable("");
	
	//Parameter
	//self.DocumentPath = ko.observable();

	self.UploadTransaction = function() {
		// validation
		var form = $("#aspnetForm");
		form.validate();
		
		var UploadFileModel = {
			Path: $('input[type=file]')[0].files[0] != null ? $('input[type=file]')[0].files[0].name : "",
			Type: $('input[type=file]')[0].files[0] != null ? $('input[type=file]')[0].files[0].type : ""
		}

		if(form.valid()) {
			/*var options = {
				url: api.server + api.url.uploadTransaction,
				data: ko.toJSON(UploadFileModel),
				params: {
				},
				token: accessToken
			};

			Helper.Ajax.Post(options, OnSuccessUploadTransaction, OnError, OnAlways);*/
			UploadFile(UploadFileModel);
		}
	}

	function OnSuccessUploadTransaction(data, textStatus, jqXHR){
		if(jqXHR.status = 200){
			// bind result to observable array
			ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-success', false);
		}else{
			ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
		}
	}

	function OnError(jqXHR, textStatus, errorThrown) {
		ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
	}

	function OnAlways(){
		//$box.trigger('reloaded.ace.widget');
	}
}

// View Model
var viewModel = new ViewModel();

$(document).ready(function () {  

    // Knockout Bindings
    ko.applyBindings(viewModel);
	
	// Token Validation
    if($.cookie(api.cookie.name) == undefined){
        Helper.Token.Request(TokenOnSuccess, TokenOnError);
    }else{
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // call spuser function
        //GetCurrentUser(viewModel);

        // call get data inside view model
        //viewModel.GetUtcAttemp();
    }
	
	$('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });
	
});

function UploadFile(document) {

    // Define the folder path for this example.
    var serverRelativeUrlToFolder = '/Instruction Documents';
    var parts = document.Path.split('.');
    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBuffer();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolder(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            alert("Upload Success")
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBuffer() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }
        //bangkit
		var f = new File([""], document.Path)
		reader.readAsArrayBuffer(f);
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(arrayBuffer) {

        // Construct the endpoint.
        /*var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);*/
		var fileCollectionEndpoint = "C:\Solution BitBucket\DBS.WebAPI\Instruction Documents" + fileName;

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                //"content-length": arrayBuffer.byteLength
            }
        });
    }
}

function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

function GetCurrentUser() {
    //if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
	if ($.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);

        viewModel.SPUser = spUser;

        //alert(JSON.stringify(vm.SPUser));

        //Helper.Signal.Connect(OnSuccessSignal, OnErrorSignal, OnReceivedSignal);

        // load hub script
        //$.getScript(config.signal.server +"Scripts/hubs.js");

    }
}

function TokenOnSuccess(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // call spuser function
    GetCurrentUser(viewModel);
}

// Token validation On Error function
function TokenOnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}
