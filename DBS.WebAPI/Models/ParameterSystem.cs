﻿using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("ParameterSystem")]
    public class ParameterSystemModel
    {
        public int ID { get; set; }
        public ProductModel Product { get; set; }
        public ProductTypeModel ProductType { get; set; }
        public string ParameterType { get; set; }
        public string ParameterTypeDisplay { get; set; }
        public string ParameterValue { get; set; }
        public string Type { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }
    public class ParsysModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class CutOffModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string CuttofValue { get; set; }
        public bool IsCutOff { get; set; }
    }
    public class StaticDropdownModel
    {
        public string DropdownItem { get; set; }
        public string DropdownItemValue { get; set; }
    }

    public class ParameterSystemRow : ParameterSystemModel
    {
        public int RowID { get; set; }
    }

    public class ParameterSystemGrid : Grid
    {
        public IList<ParameterSystemRow> Rows { get; set; }
    }
    #endregion

    #region Filter
    public class ParameterSystemFilter : Filter
    {

    }
    #endregion

    #region Interface
    public interface IParameterSystemRepository : IDisposable
    {
        bool GetParamSystemByID(int id, ref ParameterSystemModel paramSystem, ref string message, string modul);
        bool GetParamSystem(ref IList<ParameterSystemModel> paramSystems, int limit, int index, ref string message, string modul);
        bool GetParamSystem(ref IList<ParameterSystemModel> paramSystems, ref string message, string modul);
        bool GetParamSystem(int page, int size, IList<ParameterSystemFilter> filters, string sortColumn, string sortOrder, ref ParameterSystemGrid paramSystem, ref string message, string modul);
        bool GetParamSystem(ParameterSystemFilter filter, ref IList<ParameterSystemModel> paramSystems, ref string message, string modul);
        bool GetParamSystem(string key, int limit, ref IList<ParameterSystemModel> paramSystems, ref string message, string modul);
        bool GetExistingFdMaxMinRate(string paramSystem, ref string message, string modul);
        bool AddParamSystem(ParameterSystemModel paramSystem, ref string message, string modul);
        bool UpdateParamSystem(int id, ParameterSystemModel paramSystem, ref string message, string modul);
        bool DeleteParamSystem(int id, ref string message, string modul);
        bool GetStaticDropdownByCategoryName(ref IList<StaticDropdownModel> dropdown, ref string message, string modul);
        bool GetParsysByCategory(string paramtype, ref IList<ParsysModel> parsys, ref string message);
        bool GetSingleParys(string paramtype, ref string parsysName, ref string parsysVal, ref string message);
        bool GetIsCuttOff(string paramtype, ref CutOffModel cutoff, ref string message);
    }
    #endregion

    #region Repository
    public class ParameterSystemRepository : IParameterSystemRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetParamSystemByID(int id, ref ParameterSystemModel paramSystem, ref string message, string modul)
        {
            bool isSuccess = false;
            try
            {
                paramSystem = (from parsys in context.ParameterSystems
                               join producttype in context.ProductTypes on new { ProductType = parsys.ProductTypeID } equals new { ProductType = (int?)producttype.ProductTypeID } into a
                               from gra in a.DefaultIfEmpty()
                               join staticDropdown in context.StaticDropdowns on parsys.ParameterType equals staticDropdown.DropdownItemvalue
                               where parsys.IsDeleted.Equals(false) && parsys.ParsysID.Equals(id) && staticDropdown.ModuleName.ToLower().Equals(modul.Trim().ToLower())
                               select new ParameterSystemModel
                               {
                                   ID = parsys.ParsysID,
                                   Product = parsys.Product != null ? new ProductModel { ID = parsys.Product.ProductID, Code = parsys.Product.ProductCode, Name = parsys.Product.ProductName } : new ProductModel { ID = 0, Code = "", Name = "" },
                                   ProductType = parsys.ProductType != null ? new ProductTypeModel { ID = parsys.ProductType.ProductTypeID, Code = parsys.ProductType.ProductTypeCode } : new ProductTypeModel { ID = 0, Code = "--" },
                                   ParameterType = parsys.ParameterType,
                                   ParameterTypeDisplay = parsys.ParameterTypeDisplay,
                                   ParameterValue = parsys.ParameterValue,
                                   Type = parsys.Type,
                                   LastModifiedDate = parsys.UpdateDate == null ? parsys.CreateDate : parsys.UpdateDate.Value,
                                   LastModifiedBy = parsys.UpdateDate == null ? parsys.CreateBy : parsys.UpdateBy
                               }).SingleOrDefault();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetParamSystem(ref IList<ParameterSystemModel> paramSystems, int limit, int index, ref string message, string modul)
        {
            bool isSuccess = false;
            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    paramSystems = (from parsys in context.ParameterSystems
                                    join producttype in context.ProductTypes on new { ProductType = parsys.ProductTypeID } equals new { ProductType = (int?)producttype.ProductTypeID } into a
                                    from gra in a.DefaultIfEmpty()
                                    join staticDd in context.StaticDropdowns on parsys.ParameterType equals staticDd.DropdownItemvalue
                                    where staticDd.ModuleName.ToLower().Equals(modul.Trim().ToLower()) && staticDd.DropdownCategoryname.Equals("PARSYS") && parsys.IsDeleted.Equals(false)
                                    orderby new { parsys.ParameterType }
                                    select new ParameterSystemModel
                                    {
                                        ID = parsys.ParsysID,
                                        Product = parsys.Product != null ? new ProductModel { ID = parsys.Product.ProductID, Code = parsys.Product.ProductCode, Name = parsys.Product.ProductName } : new ProductModel { ID = 0, Code = "", Name = "" },
                                        ProductType = parsys.ProductType != null ? new ProductTypeModel { ID = parsys.ProductType.ProductTypeID, Code = parsys.ProductType.ProductTypeCode } : new ProductTypeModel { ID = 0, Code = "--" },
                                        ParameterType = parsys.ParameterType,
                                        ParameterTypeDisplay = parsys.ParameterTypeDisplay,
                                        ParameterValue = parsys.ParameterValue,
                                        Type = parsys.Type,
                                        LastModifiedDate = parsys.UpdateDate == null ? parsys.CreateDate : parsys.UpdateDate.Value,
                                        LastModifiedBy = parsys.UpdateDate == null ? parsys.CreateBy : parsys.UpdateBy
                                    }).Skip(skip).Take(limit).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetParamSystem(ref IList<ParameterSystemModel> paramSystems, ref string message, string modul)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    paramSystems = (from parsys in context.ParameterSystems
                                    join producttype in context.ProductTypes on new { ProductType = parsys.ProductTypeID } equals new { ProductType = (int?)producttype.ProductTypeID } into a
                                    from gra in a.DefaultIfEmpty()
                                    join staticDd in context.StaticDropdowns on parsys.ParameterType equals staticDd.DropdownItemvalue
                                    where staticDd.ModuleName.ToLower().Equals(modul.Trim().ToLower()) && staticDd.DropdownCategoryname.Equals("PARSYS") && parsys.IsDeleted.Equals(false)
                                    orderby new { parsys.ParameterType }
                                    select new ParameterSystemModel
                                    {
                                        ID = parsys.ParsysID,
                                        Product = parsys.Product != null ? new ProductModel { ID = parsys.Product.ProductID, Code = parsys.Product.ProductCode, Name = parsys.Product.ProductName } : new ProductModel { ID = 0, Code = "", Name = "" },
                                        ProductType = parsys.ProductType != null ? new ProductTypeModel { ID = parsys.ProductType.ProductTypeID, Code = parsys.ProductType.ProductTypeCode, Description = parsys.ProductType.ProductTypeDesc } : new ProductTypeModel { ID = 0, Code = "--", Description = "--" },
                                        ParameterType = parsys.ParameterType,
                                        ParameterTypeDisplay = parsys.ParameterTypeDisplay,
                                        ParameterValue = parsys.ParameterValue,
                                        Type = parsys.Type,
                                        LastModifiedDate = parsys.UpdateDate == null ? parsys.CreateDate : parsys.UpdateDate.Value,
                                        LastModifiedBy = parsys.UpdateDate == null ? parsys.CreateBy : parsys.UpdateBy
                                    }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetParamSystem(int page, int size, IList<ParameterSystemFilter> filters, string sortColumn, string sortOrder, ref ParameterSystemGrid parameterSystemGrid, ref string message, string modul)
        {
            bool isSuccess = false;
            try
            {
                ParameterSystemModel filter = new ParameterSystemModel();
                filter.ProductType = new ProductTypeModel { Code = string.Empty };
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                if (filters != null)
                {
                    filter.ProductType = new ProductTypeModel { Code = (string)filters.Where(a => a.Field.Equals("ProductType")).Select(a => a.Value).SingleOrDefault() };
                    filter.ParameterType = filters.Where(a => a.Field.Equals("ParameterType")).Select(a => a.Value).SingleOrDefault();
                    filter.ParameterValue = filters.Where(a => a.Field.Equals("ParameterValue")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    if (modul != "system")
                    {
                        var data = (from parsys in context.ParameterSystems
                                    let isProductType = !string.IsNullOrEmpty(filter.ProductType.Code)
                                    let isParamType = !string.IsNullOrEmpty(filter.ParameterType)
                                    let isParamValue = !string.IsNullOrEmpty(filter.ParameterValue)
                                    let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                    let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                    join producttype in context.ProductTypes on new { ProductType = parsys.ProductTypeID } equals new { ProductType = (int?)producttype.ProductTypeID } into a
                                    from gra in a.DefaultIfEmpty()
                                    join staticDd in context.StaticDropdowns on parsys.ParameterType equals staticDd.DropdownItemvalue
                                    where staticDd.ModuleName.ToLower().Equals(modul.Trim().ToLower()) && staticDd.DropdownCategoryname.Equals("PARSYS") && parsys.IsDeleted.Equals(false)
                                    && (isProductType ? parsys.ProductType.ProductTypeCode.Contains(filter.ProductType.Code) : true)
                                    && (isParamType ? parsys.ParameterType.Contains(filter.ParameterType) : true)
                                    && (isParamValue ? parsys.ParameterValue.Contains(filter.ParameterValue) : true)
                                    && (isCreateBy ? (parsys.UpdateDate == null ? parsys.CreateBy.Contains(filter.LastModifiedBy) : parsys.UpdateBy.Contains(filter.LastModifiedBy)) : true)
                                    && (isCreateDate ? ((parsys.UpdateDate == null ? parsys.CreateDate : parsys.UpdateDate.Value) > filter.LastModifiedDate.Value && ((parsys.UpdateDate == null ? parsys.CreateDate : parsys.UpdateDate.Value) < maxDate)) : true)
                                    select new ParameterSystemModel
                                    {
                                        ID = parsys.ParsysID,
                                        Product = parsys.Product != null ? new ProductModel { ID = parsys.Product.ProductID, Code = parsys.Product.ProductCode, Name = parsys.Product.ProductName } : new ProductModel { ID = 0, Code = "", Name = "" },
                                        ProductType = parsys.ProductType != null ? new ProductTypeModel { ID = parsys.ProductType.ProductTypeID, Code = parsys.ProductType.ProductTypeCode } : new ProductTypeModel { ID = 0, Code = "--" },
                                        ParameterType = parsys.ParameterType,
                                        ParameterTypeDisplay = parsys.ParameterTypeDisplay,
                                        ParameterValue = parsys.ParameterValue,
                                        Type = parsys.Type,
                                        LastModifiedDate = parsys.UpdateDate == null ? parsys.CreateDate : parsys.UpdateDate.Value,
                                        LastModifiedBy = parsys.UpdateDate == null ? parsys.CreateBy : parsys.UpdateBy
                                    });

                        parameterSystemGrid.Page = page;
                        parameterSystemGrid.Size = size;
                        parameterSystemGrid.Total = data.Count();
                        parameterSystemGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                        parameterSystemGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new ParameterSystemRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Product = a.Product,
                            ProductType = a.ProductType,
                            ParameterType = a.ParameterType,
                            ParameterTypeDisplay = a.ParameterTypeDisplay,
                            ParameterValue = a.ParameterValue,
                            Type = a.Type,
                            LastModifiedDate = a.LastModifiedDate,
                            LastModifiedBy = a.LastModifiedBy,
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                    }
                    else
                    {
                        var data = (from parsys in context.ParameterSystems
                                    let isProductType = !string.IsNullOrEmpty(filter.ProductType.Code)
                                    let isParamType = !string.IsNullOrEmpty(filter.ParameterType)
                                    let isParamValue = !string.IsNullOrEmpty(filter.ParameterValue)
                                    let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                    let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                    join producttype in context.ProductTypes on new { ProductType = parsys.ProductTypeID } equals new { ProductType = (int?)producttype.ProductTypeID } into a
                                    from gra in a.DefaultIfEmpty()
                                    join staticDd in context.StaticDropdowns on parsys.ParameterType equals staticDd.DropdownItemvalue
                                    where staticDd.DropdownCategoryname.Equals("PARSYS") && parsys.IsDeleted.Equals(false)
                                    && (isProductType ? parsys.ProductType.ProductTypeCode.Contains(filter.ProductType.Code) : true)
                                    && (isParamType ? parsys.ParameterType.Contains(filter.ParameterType) : true)
                                    && (isParamValue ? parsys.ParameterValue.Contains(filter.ParameterValue) : true)
                                    && (isCreateBy ? (parsys.UpdateDate == null ? parsys.CreateBy.Contains(filter.LastModifiedBy) : parsys.UpdateBy.Contains(filter.LastModifiedBy)) : true)
                                    && (isCreateDate ? ((parsys.UpdateDate == null ? parsys.CreateDate : parsys.UpdateDate.Value) > filter.LastModifiedDate.Value && ((parsys.UpdateDate == null ? parsys.CreateDate : parsys.UpdateDate.Value) < maxDate)) : true)
                                    select new ParameterSystemModel
                                    {
                                        ID = parsys.ParsysID,
                                        Product = parsys.Product != null ? new ProductModel { ID = parsys.Product.ProductID, Code = parsys.Product.ProductCode, Name = parsys.Product.ProductName } : new ProductModel { ID = 0, Code = "", Name = "" },
                                        ProductType = parsys.ProductType != null ? new ProductTypeModel { ID = parsys.ProductType.ProductTypeID, Code = parsys.ProductType.ProductTypeCode } : new ProductTypeModel { ID = 0, Code = "--" },
                                        ParameterType = parsys.ParameterType,
                                        ParameterTypeDisplay = parsys.ParameterTypeDisplay,
                                        ParameterValue = parsys.ParameterValue,
                                        Type = parsys.Type,
                                        LastModifiedDate = parsys.UpdateDate == null ? parsys.CreateDate : parsys.UpdateDate.Value,
                                        LastModifiedBy = parsys.UpdateDate == null ? parsys.CreateBy : parsys.UpdateBy
                                    });

                        parameterSystemGrid.Page = page;
                        parameterSystemGrid.Size = size;
                        parameterSystemGrid.Total = data.Count();
                        parameterSystemGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                        parameterSystemGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new ParameterSystemRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Product = a.Product,
                            ProductType = a.ProductType,
                            ParameterType = a.ParameterType,
                            ParameterTypeDisplay = a.ParameterTypeDisplay,
                            ParameterValue = a.ParameterValue,
                            Type = a.Type,
                            LastModifiedDate = a.LastModifiedDate,
                            LastModifiedBy = a.LastModifiedBy,
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                    }
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetParamSystem(ParameterSystemFilter filter, ref IList<ParameterSystemModel> paramSystems, ref string message, string modul)
        {
            throw new NotImplementedException();
        }

        public bool GetParamSystem(string key, int limit, ref IList<ParameterSystemModel> paramSystems, ref string message, string modul)
        {
            bool isSuccess = false;
            try
            {
                paramSystems = (from parsys in context.ParameterSystems
                                join producttype in context.ProductTypes on new { ProductType = parsys.ProductTypeID } equals new { ProductType = (int?)producttype.ProductTypeID } into a
                                from gra in a.DefaultIfEmpty()
                                join staticDd in context.StaticDropdowns on parsys.ParameterType equals staticDd.DropdownItemvalue
                                where staticDd.ModuleName.ToLower().Equals(modul.Trim().ToLower())
                                && staticDd.DropdownCategoryname.Equals("PARSYS")
                                && parsys.IsDeleted.Equals(false)
                                && (parsys.ParameterType.ToLower().Contains(key.Trim().ToLower()) || parsys.ParameterValue.ToLower().Contains(key.Trim().ToLower()))
                                orderby new { parsys.ParameterType }
                                select new ParameterSystemModel
                                {
                                    ID = parsys.ParsysID,
                                    Product = parsys.Product != null ? new ProductModel { ID = parsys.Product.ProductID, Code = parsys.Product.ProductCode, Name = parsys.Product.ProductName } : new ProductModel { ID = 0, Code = "", Name = "" },
                                    ProductType = parsys.ProductType != null ? new ProductTypeModel { ID = parsys.ProductType.ProductTypeID, Code = parsys.ProductType.ProductTypeCode } : new ProductTypeModel { ID = 0, Code = "--" },
                                    ParameterType = parsys.ParameterType,
                                    ParameterTypeDisplay = parsys.ParameterTypeDisplay,
                                    ParameterValue = parsys.ParameterValue,
                                    Type = parsys.Type,
                                    LastModifiedDate = parsys.UpdateDate == null ? parsys.CreateDate : parsys.UpdateDate.Value,
                                    LastModifiedBy = parsys.UpdateDate == null ? parsys.CreateBy : parsys.UpdateBy
                                }).Take(limit).ToList();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetExistingFdMaxMinRate(string paramSystem, ref string message, string modul)
        {
            bool isSuccess = false;
            try
            {
                var join = context.ParameterSystems.Join(context.StaticDropdowns, ps => ps.ParameterType, sd => sd.DropdownItemvalue, (psResult, sdResult) => new { psR = psResult, sdR = sdResult })
                        .Where(pssd => pssd.psR.ParameterType.Trim().ToLower() == paramSystem.Trim().ToLower()
                         && pssd.psR.IsDeleted.Equals(false)
                         && pssd.psR.ParameterValue != null
                         && pssd.sdR.ModuleName.ToLower() == modul.Trim().ToLower()).Any();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool AddParamSystem(ParameterSystemModel paramSystem, ref string message, string modul)
        {
            bool isSuccess = false;
            if (paramSystem != null)
            {
                try
                {
                    //added by dani
                    dynamic join;
                    if (modul.ToLower().Trim() == "fd" && (paramSystem.ParameterType.Trim().ToLower() == "fd_min_rate" || paramSystem.ParameterType.Trim().ToLower() == "fd_max_rate"))
                    {
                        join = context.ParameterSystems.Join(context.StaticDropdowns, ps => ps.ParameterType, sd => sd.DropdownItemvalue, (psResult, sdResult) => new { psR = psResult, sdR = sdResult })
                        .Where(pssd => pssd.psR.ParameterType.Trim().ToLower() == paramSystem.ParameterType.Trim().ToLower()
                         && pssd.psR.IsDeleted.Equals(false)
                         && pssd.psR.ParameterValue != null
                         && pssd.sdR.ModuleName.ToLower() == modul.Trim().ToLower()).Any();
                    }
                    ////by dani 27-7-2016 end
                    else
                    {
                        join = context.ParameterSystems.Join(context.StaticDropdowns, ps => ps.ParameterType, sd => sd.DropdownItemvalue, (psResult, sdResult) => new { psR = psResult, sdR = sdResult })
                        .Where(pssd => pssd.psR.ParameterType.Trim().ToLower() == paramSystem.ParameterType.Trim().ToLower()
                         && pssd.psR.IsDeleted.Equals(false)
                         && pssd.psR.ParameterValue.ToLower() == paramSystem.ParameterValue.Trim().ToLower()
                         && pssd.sdR.ModuleName.ToLower() == modul.Trim().ToLower()).Any();
                    }

                    /*join = context.ParameterSystems.Join(context.StaticDropdowns, ps => ps.ParameterType, sd => sd.DropdownItemvalue, (psResult, sdResult) => new { psR = psResult, sdR = sdResult })
                        .Where(pssd => pssd.psR.ParameterType.Trim().ToLower() == paramSystem.ParameterType.Trim().ToLower()
                         && pssd.psR.IsDeleted.Equals(false)
                         && pssd.psR.ParameterValue.ToLower() == paramSystem.ParameterValue.Trim().ToLower()
                         && pssd.sdR.ModuleName.ToLower() == modul.Trim().ToLower()).Any();
                     */
                    //if (!context.ParameterSystems.Where(a => a.ParameterType.Trim().ToLower() == paramSystem.ParameterType.Trim().ToLower() && a.ParameterValue.Trim().ToLower() == paramSystem.ParameterValue.Trim().ToLower() && a.IsDeleted.Equals(false)).Any())
                    if (!join)
                    {
                        if (modul == "tmo")
                        {
                            context.ParameterSystems.Add(new Entity.ParameterSystem()
                            {
                                ParameterType = paramSystem.ParameterType.Trim(),
                                ProductTypeID = paramSystem.ProductType.ID,
                                ParameterTypeDisplay = null,
                                ParameterValue = paramSystem.ParameterValue.Trim(),
                                Type = string.IsNullOrEmpty(paramSystem.Type) ? "varchar" : paramSystem.Type.Trim(),
                                IsDeleted = false,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName
                            });
                        }
                        else
                        {
                            context.ParameterSystems.Add(new Entity.ParameterSystem()
                            {
                                ParameterType = paramSystem.ParameterType.Trim(),
                                ParameterTypeDisplay = null,
                                ParameterValue = paramSystem.ParameterValue.Trim(),
                                Type = string.IsNullOrEmpty(paramSystem.Type) ? "varchar" : paramSystem.Type.Trim(),
                                IsDeleted = false,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName
                            });
                        }
                        context.SaveChanges();

                        isSuccess = true;
                    }
                    else
                    {
                        //added by dani 27-7-2016
                        if (modul.Trim().ToLower() == "fd")
                        {
                            message = string.Format("Can not adding data to Parameter System for Parameter Type {0}, but you can updating this!", paramSystem.ParameterType);
                        }
                        //by dani 27-7-2016 end
                        else
                        {
                            message = string.Format("Parameter System data for Parameter Type {0} and Parameter Value {1} is already exist.", paramSystem.ParameterType, paramSystem.ParameterValue);
                        }                        
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }

            return isSuccess;
        }

        public bool UpdateParamSystem(int id, ParameterSystemModel paramSystem, ref string message, string modul)
        {
            bool isSuccess = false;
            try
            {
                Entity.ParameterSystem data = context.ParameterSystems.Where(a => a.ParsysID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    //if (!context.ParameterSystems.Where(a => a.ParsysID != paramSystem.ID && a.ParameterType.ToLower() == paramSystem.ParameterType.Trim().ToLower() && a.ParameterValue.ToLower() == paramSystem.ParameterValue.Trim().ToLower() && a.IsDeleted.Equals(false)).Any())
                    if (!context.ParameterSystems.Join(context.StaticDropdowns,
                        ps => ps.ParameterType, sd => sd.DropdownItemvalue,
                        (psResult, sdResult) => new { psR = psResult, sdR = sdResult })
                        .Where(pssd => pssd.psR.ParsysID != paramSystem.ID
                         && pssd.psR.ParameterType.Trim().ToLower() == paramSystem.ParameterType.Trim().ToLower()
                         && pssd.psR.IsDeleted.Equals(false)
                         && pssd.psR.ParameterValue.ToLower() == paramSystem.ParameterValue.Trim().ToLower()
                         && pssd.sdR.ModuleName.ToLower() == modul.Trim().ToLower()).Any())
                    {
                        if (modul == "tmo")
                        {
                            //data.ProductID = paramSystem.Product.ID;
                            data.ParameterType = paramSystem.ParameterType.Trim();
                            data.ProductTypeID = paramSystem.ProductType.ID;
                            data.ParameterTypeDisplay = null;
                            data.ParameterValue = paramSystem.ParameterValue.Trim();
                            data.Type = string.IsNullOrEmpty(paramSystem.Type) ? "varchar" : paramSystem.Type.Trim();
                            data.UpdateDate = DateTime.UtcNow;
                            data.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        }
                        else
                        {
                            data.ParameterType = paramSystem.ParameterType.Trim();
                            data.ParameterTypeDisplay = null;
                            data.ParameterValue = paramSystem.ParameterValue.Trim();
                            data.Type = string.IsNullOrEmpty(paramSystem.Type) ? "varchar" : paramSystem.Type.Trim();
                            data.UpdateDate = DateTime.UtcNow;
                            data.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        }
                        context.SaveChanges();
                        isSuccess = true;
                    }
                    else
                    {
                        message = string.Format("Parameter System data for Parameter Type {0} and Parameter Value {1}  is already exist.", paramSystem.ParameterType, paramSystem.ParameterValue);
                    }
                }
                else
                {
                    message = string.Format("Parameter System data for Parameter System ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool DeleteParamSystem(int id, ref string message, string modul)
        {
            bool isSuccess = false;
            try
            {
                Entity.ParameterSystem data = context.ParameterSystems.Where(a => a.ParsysID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().LoginName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Parameter System data for Parameter System ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetStaticDropdownByCategoryName(ref IList<StaticDropdownModel> dropdown, ref string message, string modul)
        {
            bool isSuccess = false;
            //if (modul == string.Empty) modul = "system";
            string key = modul.Trim().ToLower();
            if (key != "system")
            {
                try
                {
                    using (DBSEntities context = new DBSEntities())
                    {
                        dropdown = (from dd in context.StaticDropdowns
                                    where dd.IsDeleted.Equals(false) && dd.ModuleName.ToLower().Equals(key) && dd.DropdownCategoryname.Equals("PARSYS")
                                    select new StaticDropdownModel
                                    {
                                        DropdownItem = dd.DropdownItem.Trim(),
                                        DropdownItemValue = dd.DropdownItemvalue.Trim()
                                    }).ToList();
                    }
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            else
            {
                try
                {
                    using (DBSEntities context = new DBSEntities())
                    {
                        dropdown = (from dd in context.StaticDropdowns
                                    where dd.IsDeleted.Equals(false) && dd.DropdownCategoryname.Equals("PARSYS")
                                    select new StaticDropdownModel
                                    {
                                        DropdownItem = dd.DropdownItem.Trim(),
                                        DropdownItemValue = dd.DropdownItemvalue.Trim()
                                    }).ToList();
                    }
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return isSuccess;
        }

        public bool GetParsysByCategory(string paramtype, ref IList<ParsysModel> parsys, ref string message)
        {
            bool isSuccess = false;
            try
            {
                parsys = (from par in context.ParameterSystems
                          where par.IsDeleted.Equals(false) && par.ParameterType.Equals(paramtype)
                          select new ParsysModel
                          {
                              ID = par.ParsysID,
                              Name = par.ParameterValue,
                          }).ToList();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public bool GetSingleParys(string paramtype, ref string parsysName, ref string parsysVal, ref string message)
        {
            bool isSuccess = false;
            try
            {
                var parsys = (from par in context.ParameterSystems
                              where par.IsDeleted.Equals(false) && par.ParameterType.Equals(paramtype)
                              orderby par.CreateDate descending
                              select par).FirstOrDefault();
                if (parsys != null)
                {
                    parsysName = parsys.ParameterType;
                    parsysVal = parsys.ParameterValue;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetIsCuttOff(string paramtype, ref CutOffModel cutoff, ref string message)
        {
            bool isSuccess = false;
            try
            {
                var parsys = (from par in context.ParameterSystems
                              where par.IsDeleted.Equals(false) && par.ParameterType.Equals(paramtype)
                              orderby par.CreateDate descending
                              select par).FirstOrDefault();
                if (parsys != null)
                {
                    CutOffModel cutoffTemp = new CutOffModel();
                    cutoffTemp.ID = parsys.ParsysID;
                    cutoffTemp.Name = parsys.ParameterType;
                    cutoffTemp.CuttofValue = parsys.ParameterValue;
                    DateTime dtNow = DateTime.Now;
                    DateTime dtCut = DateTime.Now;
                    string[] parVal = parsys.ParameterValue.Split(':');
                    dtCut = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, int.Parse(parVal[0]), int.Parse(parVal[1]), 0);
                    if (dtNow > dtCut)
                        cutoffTemp.IsCutOff = true;
                    else
                        cutoffTemp.IsCutOff = false;
                    cutoff = cutoffTemp;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
    }

    #endregion

}