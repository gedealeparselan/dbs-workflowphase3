﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class SLASettingController : ApiController
    {
        private string message = string.Empty;

        private readonly ISLASettingRepository repository = null;

        public SLASettingController()
        {
            this.repository = new SLASettingRepository();
        }

        public SLASettingController(ISLASettingRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all SLA Settings.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<SLASettingModel>))]
        public HttpResponseMessage Get()
        {
            IList<SLASettingModel> output = new List<SLASettingModel>();

            if (!repository.GetSLASetting(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<SLASettingModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get SLA Settings with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<SLASettingModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<SLASettingModel> output = new List<SLASettingModel>();

            if (!repository.GetSLASetting(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<SLASettingModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get SLA Settings for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(SLASettingGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<SLASettingFilter> filters)
        {
            SLASettingGrid output = new SLASettingGrid();

            if (!repository.GetSLASetting(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<SLASettingGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find SLA Settings using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<SLASettingModel>))]
        [HttpGet]
        [Route("api/SLASetting/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<SLASettingModel> output = new List<SLASettingModel>();

            if (!repository.GetSLASetting(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<SLASettingModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get SLA Setting by ID.
        /// </summary>
        /// <param name="id">SLA Setting ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(SLASettingModel))]
        [Route("api/SLASetting/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            SLASettingModel output = new SLASettingModel();

            if (!repository.GetSLASettingByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<SLASettingModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new SLA Setting.
        /// </summary>
        /// <param name="SLASetting">SLA Setting data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(SLASettingModel SLASetting)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddSLASetting(SLASetting, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New SLA Setting data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing SLA Setting.
        /// </summary>
        /// <param name="id">ID of SLASetting to be modify.</param>
        /// <param name="SLASetting">SLA Setting data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/SLASetting/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, SLASettingModel SLASetting)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateSLASetting(id, SLASetting, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "SLA Setting data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting SLA Setting.
        /// </summary>
        /// <param name="id">ID of SLA Setting to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/SLASetting/{id}")]
        [Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteSLASetting(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "SLA Setting data has been deleted." });
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}