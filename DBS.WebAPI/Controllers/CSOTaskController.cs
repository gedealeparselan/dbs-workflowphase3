﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    public enum CSOTaskType
    {
        InterestMaintenance = 1,
        Rollover = 2,
        Settlement = 3
    }
    [Authorize]
    public class CSOTaskController : ApiController
    {
        private string message = string.Empty;

        private readonly ICSOTask repository = null;
        public CSOTaskController()
        {
            this.repository = new CSOTask();
        }
        public CSOTaskController(ICSOTask repository)
        {
            this.repository = repository;
        }

        [ResponseType(typeof(IList<InterestMaintenanceModel>))]
        public HttpResponseMessage Get(string logname, int taskType, string taskDate)
        {

            HttpResponseMessage result = null;
            DateTime _date;
            if (!DateTime.TryParse(taskDate, out _date))
            {
                _date = DateTime.Now;
            }

            switch (taskType)
            {
                case (int)CSOTaskType.InterestMaintenance:
                    IList<InterestMaintenanceModel> output1 = new List<InterestMaintenanceModel>();
                    if (!repository.GetInterestMaintenance(logname, ref output1, _date, ref message))
                    {
                        result = Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
                    }
                    else
                    {
                        result = Request.CreateResponse<IList<InterestMaintenanceModel>>(HttpStatusCode.OK, output1);
                    }
                    break;
                case (int)CSOTaskType.Rollover:
                    IList<RolloverSettlementModel> output2 = new List<RolloverSettlementModel>();
                    if (!repository.GetRollover(logname, ref output2, _date, ref message))
                    {
                        result = Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
                    }
                    else
                    {
                        result = Request.CreateResponse<IList<RolloverSettlementModel>>(HttpStatusCode.OK, output2);
                    }
                    break;
                case (int)CSOTaskType.Settlement:
                    IList<RolloverSettlementModel> output3 = new List<RolloverSettlementModel>();
                    if (!repository.GetSettlement(logname, ref output3, _date, ref message))
                    {
                        result = Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
                    }
                    else
                    {
                        result = Request.CreateResponse<IList<RolloverSettlementModel>>(HttpStatusCode.OK, output3);
                    }
                    break;
                default:
                    break;
            }
            return result;

        }

        #region Agung Get CSO Task
        //get detail transaction/by fandi
        [ResponseType(typeof(LoanPPUCheckerModel))]
        [HttpGet]
        [Route("api/LoanCSODetail/{instractionID}")]
        public HttpResponseMessage GetCSODetails(long instractionID)
        {
            TransactionLoanModel transaction = new TransactionLoanModel();
            LoanPPUCheckerModel output = new LoanPPUCheckerModel();

            try
            {
                if (!repository.GetLoanCSODetails(instractionID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    output.Transaction = transaction;
                    return Request.CreateResponse(HttpStatusCode.OK, output);
                }
            }
            finally
            {
                transaction = null;
            }
        }

        [ResponseType(typeof(IList<TransactionCSOTaskModel>))]
        [HttpGet]
        [Route("api/TransactionCSOTask")]
        public HttpResponseMessage GetAll(string cif, string productname)
        {
            IList<TransactionCSOTaskModel> output = new List<TransactionCSOTaskModel>();
            {
                // #1 Get Instraction
                if (!repository.GetTransactionCSOTask(ref output, cif, productname, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse<IList<TransactionCSOTaskModel>>(HttpStatusCode.OK, output);
                }
            }
        }

        [ResponseType(typeof(IList<InterestRateModel>))]
        [HttpGet]
        [Route("api/InterestRate")]
        public HttpResponseMessage GetAllInterestRate()
        {
            IList<InterestRateModel> output = new List<InterestRateModel>();
            {
                if (!repository.GetInterestRates(ref output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse<IList<InterestRateModel>>(HttpStatusCode.OK, output);
                }
            }
        }

        [ResponseType(typeof(IList<MaintenanceTypeLoanModel>))]
        [HttpGet]
        [Route("api/MaintenanceTypeLoan")]
        public HttpResponseMessage GetAllMaintenanceType()
        {
            IList<MaintenanceTypeLoanModel> output = new List<MaintenanceTypeLoanModel>();
            {
                if (!repository.GetMaintenanceTypes(ref output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse<IList<MaintenanceTypeLoanModel>>(HttpStatusCode.OK, output);
                }
            }
        }
        #endregion

        [ResponseType(typeof(string))]
        [HttpPost]
        [Route("api/CSOTask/InterestMaintenance")]
        public HttpResponseMessage AddInterestMaintenance(TransactionData InterestMaintenance)
        {
            List<DataID> dataID = new List<DataID>();

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddCSOInterestMaintenance(InterestMaintenance, ref dataID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { DataList = dataID, Message = "Selected Interest Maintenances have been submitted." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/CSOTask/RolloverSettlement")]
        public HttpResponseMessage AddRolloverSettlement(TransactionData RolloverSettlement)
        {
            List<DataID> dataID = new List<DataID>();

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddCSORolloverSettlement(RolloverSettlement, ref dataID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { DataList = dataID, Message = "Selected Rollovers/Settlements have been submitted." });
                }
            }
        }

        [ResponseType(typeof(string))]
        [HttpPost]
        [Route("api/CSOTask/IMBaseRate")]
        public HttpResponseMessage GetBaseRateInterestMaintenance(IList<InterestMaintenanceModel> InterestMaintenance)
        {
            IList<RolloverSettlementModel> RolloverSettlement = new List<RolloverSettlementModel>();
            IList<BaseRate> BaseRate = new List<BaseRate>();
            if (!repository.GetBaseRate(ref RolloverSettlement, ref InterestMaintenance, ref BaseRate, (int)DBSProductID.LoanIMProductIDCons, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Created, new { BaseRate = InterestMaintenance, Message = "Success" });
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/CSOTask/ROBaseRate")]
        public HttpResponseMessage GetBaseRateRolloverSettlement(IList<RolloverSettlementModel> RolloverSettlement)
        {
            IList<InterestMaintenanceModel> InterestMaintenance = new List<InterestMaintenanceModel>();
            IList<BaseRate> BaseRate = new List<BaseRate>();
            if (!repository.GetBaseRate(ref RolloverSettlement, ref InterestMaintenance, ref BaseRate, (int)DBSProductID.LoanRolloverProductIDCons, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Created, new { BaseRate = RolloverSettlement, Message = "Error Occurred." });
            }
        }

        [ResponseType(typeof(string))]
        [HttpPost]
        [Route("api/CSOTask/IMApprovalDOA")]
        public HttpResponseMessage GetApprovalDOAInterestMaintenance(InterestMaintenanceModel InterestMaintenance)
        {
            long transactionID = 0;
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                //if (!repository.AddCustomerUnderlying(CustomerUnderlying, ref message))
                RolloverSettlementModel RolloverSettlement = new RolloverSettlementModel();
                ApprovalDOA Approver = new ApprovalDOA();
                if (!repository.GetApprovalDOA(ref RolloverSettlement, ref InterestMaintenance, ref Approver, (int)DBSProductID.LoanIMProductIDCons, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { ApprovalDOA = Approver, Message = "Error Occurred." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/CSOTask/ROApprovalDOA")]
        public HttpResponseMessage GetApprovalDOARolloverSettlement(RolloverSettlementModel RolloverSettlement)
        {
            long transactionID = 0;
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                InterestMaintenanceModel InterestMaintenance = new InterestMaintenanceModel();
                ApprovalDOA Approver = new ApprovalDOA();
                if (!repository.GetApprovalDOA(ref RolloverSettlement, ref InterestMaintenance, ref Approver, (int)DBSProductID.LoanRolloverProductIDCons, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { ApprovalDOA = Approver, Message = "Error Occurred." });
                }
            }
        }
        [ResponseType(typeof(string))]
        [HttpGet]
        [Route("api/CSOTask/Release/LoanContract")]
        public HttpResponseMessage ReleaseWorksheet(string loanContractNo)
        {
            if (!repository.ReleaseWorksheetItem(loanContractNo, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Created, loanContractNo + " has been released.");
            }
        }

    }
}
