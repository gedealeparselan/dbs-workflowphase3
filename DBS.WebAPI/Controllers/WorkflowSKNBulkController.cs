﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class WorkflowSKNBulkController : ApiController
    {
        private string message = string.Empty;
        private readonly IWorkflowSKNBulkRepository repoSKNBulk = null;
        public WorkflowSKNBulkController()
        {
            this.repoSKNBulk = new WorkflowSKNBulkRepository();
        }

        public WorkflowSKNBulkController(IWorkflowSKNBulkRepository repoSKNBulk)
        {
            this.repoSKNBulk = repoSKNBulk;
        }

        protected override void Dispose(bool disposing)
        {
            if (repoSKNBulk != null)
                repoSKNBulk.Dispose();
            base.Dispose(disposing);
        }

        /// <summary>
        /// Get transaction sknbulk by parent transaction ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="transactionID">Transaction ID</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionSKNBulkModel))]
        [HttpGet]
        [Route("api/WorkflowSKNBulk/{workflowInstanceID}/Transaction/MakerSKNBulk/{transactionID}")]
        public HttpResponseMessage GetTransactionSKNBulk(Guid workflowInstanceID, long transactionID)
        {
            IList<TransactionSKNBulkModel> output = new List<TransactionSKNBulkModel>();

            if (!repoSKNBulk.GetTransactionSKNBulk(workflowInstanceID, transactionID, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Insert Excel transaction sknbulk by parent transaction ID to table
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="transactionID">Transaction ID</param>
        /// <param name="documentID">Transaction Document ID</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowSKNBulk/{workflowInstanceID}/Transaction/MakerSKNBulkAdd/{transactionID}/{documentID}")]
        public HttpResponseMessage AddTransactionSKNBulk(Guid workflowInstanceID, long transactionID, long documentID, IList<TransactionSKNBulkModel> excelSknBulkModel)
        {

            if (!repoSKNBulk.AddTransactionSKNBulk(workflowInstanceID, transactionID, documentID, excelSknBulkModel, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Excel data has been loaded." });
            }
        }

        /// <summary>
        /// Update transaction sknbulk by parent transaction ID to table
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="transactionID">Transaction ID</param>
        /// <param name="documentID">Transaction Document ID</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowSKNBulk/{workflowInstanceID}/Transaction/MakerSKNBulkUpdate/{transactionID}/{documentID}")]
        public HttpResponseMessage UpdateTransactionSKNBulk(Guid workflowInstanceID, long transactionID, long documentID, PaymentMakerSKNBulkDetailModel paymentMakerSKNBulkDetail)
        {

            if (!repoSKNBulk.UpdateTransactionSKNBulk(workflowInstanceID, transactionID, documentID, paymentMakerSKNBulkDetail, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction SKN Bulk has been updated." });
            }
        }

        /// <summary>
        /// Get payment checker skn bulk details of transaction by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="transactionID">Transaction ID</param>
        /// <returns></returns>
        [ResponseType(typeof(PaymentCheckerSKNBulkDetailModel))]
        [HttpGet]
        [Route("api/WorkflowSKNBulk/{workflowInstanceID}/Transaction/CheckerSKNBulk/{transactionID}")]
        public HttpResponseMessage GetPaymentCheckerSKNBulkDetails(Guid workflowInstanceID, long transactionID)
        {
            PaymentCheckerSKNBulkDetailModel output = new PaymentCheckerSKNBulkDetailModel();

            if (!repoSKNBulk.GetTransactionSKNBulkChecker(workflowInstanceID, transactionID, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(IList<DoubleTransactionModel>))]
        [HttpPost]
        [Route("api/WorkflowSKNBulk/CheckDoubleTransaction/{transactionID}")]
        public HttpResponseMessage GetDoubleTransactionSKNBulk(long transactionID, DataForCheckingDoubleTransactionsModel data)
        {
            ReturnDoubleTransactions output = new ReturnDoubleTransactions();
            output.Values = new List<string[]>();
            output.LineDuplicates = new List<int>();
            output.Columns = new List<string[]>();

            if(!repoSKNBulk.CheckDoubleTransactionSKNBulk(transactionID, data, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }
    }
}