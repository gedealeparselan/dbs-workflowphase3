/**
 * Created by SPAdmin on 19/11/2014.
 *1/ 

var accessToken;

/* dodit@2014.11.08:add format function */
var formatNumber = function (num) {
    var n = num.toString(), p = n.indexOf('.');
    return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
        return p < 0 || i < p ? ($0 + ',') : $0;
    });
};

var formatNumber_r = function (num) {
    if (num != null) {
        num = num.toString().replace(/,/g, '');
        num = parseFloat(num).toFixed(2);
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    } else { return 0; }
}

var formatAccount = function (num) {
    if (num != null) {
        num = num.replace(/-/g, '');
        sheet_a = (num.length > 0) ? num.substr(0, 4) : '';
        sheet_b = (num.length > 4) ? '-' + num.substr(4, 4) : '';
        sheet_c = (num.length > 8) ? '-' + num.substr(8) : '';

        return sheet_a + sheet_b + sheet_c;
    }
    else {
        return '';
    }
}

var $box;
var $remove = false;
var cifData = '0';
var customerNameData = '';
var treshHold = 0;
var totalTrxAmouont = 0;
var idrrate = 0;
var DocumentModels = ko.observable({ FileName: '', DocumentPath: '' });

var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};

var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};

var BizSegmentModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var BranchModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var TypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ProductModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    WorkflowID: ko.observable(),
    Workflow: ko.observable()
};

var LLDModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var UnderlyingDocModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ChannelModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var BankModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    BranchCode: ko.observable(),
    SwiftCode: ko.observable(),
    BankAccount: ko.observable(),
    Description: ko.observable(),
    CommonName: ko.observable(),
    Currency: ko.observable(),
    PGSL: ko.observable()
};

var RMModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    RankID: ko.observable(),
    Rank: ko.observable(),
    SegmentID: ko.observable(),
    Segment: ko.observable(),
    BranchID: ko.observable(),
    Branch: ko.observable(),
    Email: ko.observable()
};

var ContactModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    PhoneNumber: ko.observable(),
    DateOfBirth: ko.observable(),
    Address: ko.observable(),
    IDNumber: ko.observable()
};

var FunctionModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var CurrencyModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var CurrencyDebModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};
var CurrencyModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var AccountModel = {
    AccountNumber: ko.observable(),
    Currency: ko.observable(CurrencyDebModel)
};

var UpdatePendingDocumentsModel = {
    TransactionID: ko.observable(),
    IsCompleted: ko.observable(false)
}

var ProductTypeModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    IsFlowValas: ko.observable(),
    Description: ko.observable()
};

var AccountDebModel = {
    AccountNumber: ko.observable(),
    Currency: ko.observable(CurrencyDebModel)
};

var UnderlyingDocumentModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var DocumentTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var DocumentPurposeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var FXComplianceModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ChargesModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var UnderlyingModel = {
    ID: ko.observable(),
    UnderlyingDocument: ko.observable(UnderlyingDocumentModel),
    DocumentType: ko.observable(DocumentTypeModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    AttachmentNo: ko.observable(),
    IsStatementLetter: ko.observable(),
    IsDeclarationOfException: ko.observable(),
    StartDate: ko.observable(),
    EndDate: ko.observable(),
    DateOfUnderlying: ko.observable(),
    ExpiredDate: ko.observable(),
    ReferenceNumber: ko.observable(),
    SupplierName: ko.observable(),
    InvoiceNumber: ko.observable()
};

var DocumentModel = {
    ID: ko.observable(),
    Type: ko.observable(DocumentTypeModel),
    Purpose: ko.observable(DocumentPurposeModel),
    FileName: ko.observable(),
    DocumentPath: ko.observable(),
    LastModifiedDate: ko.observable(new Date),
    LastModifiedBy: ko.observable()
};

var CustomerModel = {
    CIF: ko.observable(),
    Name: ko.observable(),
    POAName: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Branch: ko.observable(BranchModel),
    Type: ko.observable(TypeModel),
    RM: ko.observable(RMModel),
    Contacts: ko.observableArray([ContactModel]),
    Functions: ko.observableArray([FunctionModel]),
    Accounts: ko.observableArray([AccountModel]),
    Underlyings: ko.observableArray([UnderlyingModel])
};

var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};

var SelectModel = function (cif, id) {
    var self = this;
    self.ID = ko.observable(0);
    self.CIF = ko.observable(cif);
    self.UnderlyingID = ko.observable(id);
    self.ParentID = ko.observable(0);
}

var UnderlyingDocumentModel2 = function (id, name, code) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Name = ko.observable(name);
    self.CodeName = ko.observable(code + ' (' + name + ')');
}

var DocumentTypeModel2 = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);

}

var StatementLetterModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}

var CurrencyModel2 = function (id, code, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Description = ko.observable(description);
}

var DocumentPurposeModel2 = function (id, name, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(description);
}

var CustomerUnderlyingMappingModel = function (FileID, UnderlyingID, attachNo) {
    var self = this;
    self.ID = ko.observable(0);
    self.UnderlyingID = ko.observable(UnderlyingID);
    self.UnderlyingFileID = ko.observable(FileID);
    self.AttachmentNo = ko.observable(attachNo);
}

var SelectUtillizeModel = function (id, enable, USDAmount) {
    var self = this;
    self.ID = ko.observable(id);
    self.Enable = ko.observable(enable);
    self.USDAmount = ko.observable(USDAmount);

}
var BeneficiaryCountryModel = {
    ID: ko.observable(),
    Code: ko.observable()
};
var BeneficiaryBusinesModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};
var TransactionRelationshipModel = {
    ID: ko.observable(),
    Code: ko.observable()
};
var UnderlyingDocModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};
var BranchModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Code: ko.observable()
};
var CityModel = {
    CityID: ko.observable(),
    CityCode: ko.observable(),
    Description: ko.observable()
};
var CBGCustomerModel = {
    ID: ko.observable(),
    Code: ko.observable()
};
var TransactionUsingDebitSundryModel = {
    ID: ko.observable(),
    Code: ko.observable()
};
var FXComplianceModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};
var PendingDocumentsModel = {
    ApplicationID: ko.observable(),
    signatureVerified: ko.observable(),
    Document: ko.observable(),
    customerName: ko.observable(),
    productName: ko.observable(),
    currencyDesc: ko.observable(),
    transactionAmount: ko.observable(),
    eqvUsd: ko.observable(),
    fxTransaction: ko.observable(),
    transactionStatus: ko.observable(),
    LastModifiedBy: ko.observable(),
    LastModifiedDate: ko.observable()
};
var VerifyModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    IsVerified: ko.observable()
};
var TransactionModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(),
    IsTopUrgentChain: ko.observable(),
    // add udin -IPE-
    CBGCustomer: ko.observable(CBGCustomerModel),
    TransactionUsingDebitSundry: ko.observable(TransactionUsingDebitSundryModel),
    TrxRate: ko.observable(),
    IsNormal: ko.observable(),
    ModePayment: ko.observable(),
    Sundry: ko.observable(),
    Nostro: ko.observable(),
    IsBeneficiaryResident: ko.observable(),
    BeneficiaryCountry: ko.observable(BeneficiaryCountryModel),
    ChargingAccountName: ko.observable(),
    ChargingAccountBank: ko.observable(),
    BeneficiaryAddress: ko.observable(),
    ChargingACCNumber: ko.observable(),
    ChargingAccountCurrency: ko.observable(),
    BeneficiaryBusines: ko.observableArray([BeneficiaryBusinesModel]),
    TransactionRelationship: ko.observable(TransactionRelationshipModel),
    UnderlyingDoc: ko.observable(UnderlyingDocModel),
    IsCallbackRequired: ko.observable(),
    IsStatementLetterCopy: ko.observable(),
    Branch: ko.observable(BranchModel),
    City: ko.observable(CityModel),
    Compliance: ko.observable(FXComplianceModel),
    Verify: ko.observableArray([VerifyModel]),
    IsDocumentComplete: ko.observable(false),
    //
    Customer: ko.observable(CustomerModel),
    IsNewCustomer: ko.observable(false),
    Product: ko.observable(ProductModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    Channel: ko.observable(ChannelModel),
    Account: ko.observable(AccountDebModel),
    ApplicationDate: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Bank: ko.observable(BankModel),
    BankCharges: ko.observable(ChargesModel),
    AgentCharges: ko.observable(ChargesModel),
    Type: ko.observable(20),
    IsResident: ko.observable(),
    IsCitizen: ko.observable(),
    PaymentDetails: ko.observable(null),
    LLDCode: ko.observable(null),
    LLDInfo: ko.observable(null),
    IsSignatureVerified: ko.observable(),
    IsDormantAccount: ko.observable(),
    IsFreezeAccount: ko.observable(),
    Others: ko.observable(null),
    IsDraft: ko.observable(),
    CreateDate: ko.observable(new Date),
    CreateBy: ko.observable(),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]), //DocumentModel
    DetailCompliance: ko.observable(),
    BeneName: ko.observable(),
    BeneAccNumber: ko.observable(),
    TZNumber: ko.observable(),
    utilizationAmount: ko.observable(0.00),
    underlyings: ko.observableArray([]),
    OtherUnderlyingDoc: ko.observable(),
    OtherAccountNumber: ko.observable(),
    IsOtherAccountNumber: ko.observable(false),
    DebitCurrency: ko.observable(CurrencyModel),
    ProductType: ko.observable(ProductTypeModel),
    LLD: ko.observable(LLDModel),

    LLDDocumentCode: ko.observable(),
    Description: ko.observable(),
    LLDUnderlyingAmount: ko.observable()
};

var Parameter = {
    Products: ko.observableArray([ProductModel]),
    ProductType: ko.observableArray([ProductTypeModel]),
    Currencies: ko.observableArray([CurrencyModel]),
    Channels: ko.observableArray([ChannelModel]),
    BizSegments: ko.observableArray([BizSegmentModel]),
    Banks: ko.observableArray([BankModel]),
    LLDs: ko.observableArray([LLDModel]),
    BankCharges: ko.observableArray([ChargesModel]),
    AgentCharges: ko.observableArray([ChargesModel]),
    FXCompliances: ko.observableArray([FXComplianceModel]),
    DocumentTypes: ko.observableArray([DocumentTypeModel]),
    DocumentPurposes: ko.observableArray([DocumentPurposeModel]),
    UnderlyingDocs: ko.observableArray([UnderlyingDocModel])
};

var SelectedModel = {
    Product: ko.observable(),
    Currency: ko.observable(),
    Channel: ko.observable(),
    Account: ko.observable(),
    BizSegment: ko.observable(),
    Bank: ko.observable(),
    LLD: ko.observable(),
    UnderlyingDoc: ko.observable(),
    DocumentType: ko.observable(),
    DocumentPurpose: ko.observable(),
    NewCustomer: ko.observable({
        Currency: ko.observable(),
        BizSegment: ko.observable()
    }),
    BankCharges: ko.observable(2),
    AgentCharges: ko.observable(2),
    ProductType: ko.observable(),
    DebitCurrency: ko.observable()
};

var ViewModel = function () {

    var self = this;

    // Sharepoint User
    self.SPUser = ko.observable(SPUser);

    // New Data flag
    self.IsNewDataUnderlying = ko.observable(false);

    //buat LLD Transaction Amount 
    self.IsLimit = ko.observable(false);

    // Is check attachments
    self.IsFXTransactionAttach = ko.observable(false);
    self.IsUploaded = ko.observable(false);
    self.SelectingUnderlying = ko.observable(false);
    self.fxTransaction = ko.observable(false);

    self.UpdatePendingDocuments = ko.observable(UpdatePendingDocumentsModel);

    self.IsPendingCompleted = ko.observable(false);


    // input form controls
    self.IsEditable = ko.observable(false);

    //is new underlying
    self.isNewUnderlying = ko.observable(false);

    // Parameters
    self.Parameter = ko.observable(Parameter);

    // all pending documents list model
    self.PendingDocuments = ko.observable(PendingDocumentsModel);
    self.ArrayDoc = ko.observable(0);
    // utilize amount from underlying
    self.UtilizationAmounts = ko.observable(0);

    // Debit Currency
    self.CurrencyDep = ko.observable(CurrencyDebModel);
    self.ChargingAccCurrency = ko.observable(CurrencyModel);

    // Main Model
    self.TransactionModel = ko.observable(TransactionModel);

    // Double Transaction
    self.DoubleTransactions = ko.observableArray([]);

    // Validation User
    self.UserValidation = ko.observable({
        UserID: ko.observable(),
        Password: ko.observable(),
        Error: ko.observable()
    });

    // Options selected value
    self.Selected = ko.observable(SelectedModel);

    // Temporary documents
    self.DocumentToDelete = ko.observableArray([]);
    self.Documents = ko.observableArray([]);
    self.DocumentPath = ko.observable();
    self.DocumentType = ko.observable();
    self.DocumentPurpose = ko.observable();
    self.DocumentFileName = ko.observable();

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);
        if (moment(localDate).isValid()) {
            if (date == '1970/01/01 00:00:00' || date == '1970-01-01T00:00:00' || date == null) {
                return "";
            }
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return ""
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    self.DateToFormat = function (date, isDateOnly, isDateLong) {
        var DateToFormat = new Date(date);

        if (moment(DateToFormat).isValid()) {
            //return moment(DateToFormat).format("YYYY-MM-DD");
            return moment(DateToFormat).format(config.format.date);
        } else {
            return "";
        }
    };

    //add by adi
    self.GetDefaultDate = function () { GetDefaultDate(); };
    function GetDefaultDate() {
        var newDateTime = new Date();
        var newMonth; var newDate;
        var newHour; var newMinute; var newSecond;

        newMonth = newDateTime.getMonth() + 1;
        newMonth = newMonth.toString().length == 2 ? newMonth : "0" + newMonth;
        newDate = newDateTime.getDate();
        newDate = newDate.toString().length == 2 ? newDate : "0" + newDate;
        newHour = newDateTime.getHours();
        newHour = newHour.toString().length == 2 ? newHour : "0" + newHour;
        newMinute = newDateTime.getMinutes();
        newMinute = newMinute.toString().length == 2 ? newMinute : "0" + newMinute;
        newSecond = newDateTime.getSeconds();
        newSecond = newSecond.toString().length == 2 ? newSecond : "0" + newSecond;

        var IsPendingDocument = $('#DefaultDatePendingDocument');
        if (IsPendingDocument.length > 0) {
            //self.GridProperties().SortColumn("LastModifiedDate");
            //self.GridProperties().SortOrder("DESC");
            //self.GridProperties().AllowFilter(true);
            self.PendingDocumentsGridProperties().SortColumn("LastModifiedDate");
            self.PendingDocumentsGridProperties().SortOrder("DESC");
            self.PendingDocumentsGridProperties().AllowFilter(true);
            //self.FilterLastModifiedDate(newDateTime.getFullYear() + "/" + newMonth + "/" + newDate);
            self.FilterLastModifiedDate(newDate + "/" + newMonth + "/" + newDateTime.getFullYear());
        }

    }

    //--------------------------- set Underlying function & variable start
    self.IsUnderlyingMode = ko.observable(true);
    self.ID_u = ko.observable("");
    self.CIF_u = ko.observable(cifData);
    self.CustomerName_u = ko.observable(customerNameData);
    self.UnderlyingDocument_u = ko.observable(new UnderlyingDocumentModel2('', '', ''));
    self.ddlUnderlyingDocument_u = ko.observableArray([]);
    self.DocumentType_u = ko.observable(new DocumentTypeModel2('', ''));
    self.ddlDocumentType_u = ko.observableArray([]);
    self.Currency_u = ko.observable(new CurrencyModel2('', '', ''));
    self.ddlCurrency_u = ko.observableArray([]);
    self.StatementLetter_u = ko.observable(new StatementLetterModel('', ''));
    self.ddlStatementLetter_u = ko.observableArray([]);
    self.AvailableAmount_u = ko.observable(0);
    self.Rate_u = ko.observable(0);
    self.AmountUSD_u = ko.observable(0);
    self.Amount_u = ko.observable(0);
    self.AttachmentNo_u = ko.observable("");
    self.SelectUnderlyingDocument_u = ko.observable("");
    self.IsDeclarationOfException_u = ko.observable(false);
    self.StartDate_u = ko.observable();
    self.EndDate_u = ko.observable();
    self.DateOfUnderlying_u = ko.observable();
    self.ExpiredDate_u = ko.observable();
    self.ReferenceNumber_u = ko.observable("");
    self.SupplierName_u = ko.observable("");
    self.InvoiceNumber_u = ko.observable("");
    self.IsStatementA = ko.observable(true);
    self.IsProforma_u = ko.observable(false);
    self.IsSelectedProforma = ko.observable(false);
    self.IsUtilize_u = ko.observable(false);
    self.IsEnable_u = ko.observable(false);
    self.IsProformaSet_u = ko.computed({
        read: function () {
            if (self.DocumentType_u().ID() == 2) {
                self.IsProforma_u(false);
            } else {
                self.IsProforma_u(true);
            }
        },
        write: function () {
        }
    });
    /*self.Amount_u = ko.computed({
     read: function () {
     if (self.StatementLetter_u().ID() == 1) {
     self.Currency_u().ID(1);
     return treshHold;
     } else {
     return 0;
     }
     },
     write: function (data) {
     alert(data);
     }
     }); */

    // Uploading document
    self.UploadDocumentUnderlying = function () {
        // show upload dialog
        // show the dialog task form
        $("#modal-form-Attach").modal('show');
        self.TempSelectedAttachUnderlying([]);
        self.ID_a(0);
        self.DocumentPurpose_a(new DocumentPurposeModel2('', '', ''));
        self.DocumentType_a(new DocumentTypeModel2('', ''));
        self.DocumentPath_a('');
        GetDataUnderlyingAttach();
    }

    self.UploadDocument = function () {
        self.DocumentPath(null);
        self.Selected().DocumentType(null);
        self.Selected().DocumentPurpose(null);
        self.DocumentType(null);
        self.DocumentPurpose(null);
        if (viewModel.TransactionModel().ModePayment != 'BCP2') {
            $("#modal-form-upload-ipe").modal('show');
        }
        else {
            $("#modal-form-upload").modal('show');
        }
    }

    self.EditDocument = function (data) {
        self.DocumentPath(data.DocumentPath);
        self.Selected().DocumentType(data.Type.ID);
        self.Selected().DocumentPurpose(data.Purpose.ID);

        // show upload dialog
        $("#modal-form-upload").modal('show');
    };

    self.RemoveDocument = function (data) {
        //alert(JSON.stringify(data))
        if (data.ID != 0) {
            self.DocumentToDelete.push(data);
            self.Documents.remove(data);
            //self.DeleteDocument(data.ID);
        }
        else {
            self.NewDocuments.remove(data);
            self.Documents.remove(data);
        }
    };

    self.DeleteDocument = function (id) {
        /*bootbox.confirm("Are you sure want to delete?", function(result) {
            if(result) {*/
        //Ajax call to delete the Customer
        $.ajax({
            type: "DELETE",
            url: api.server + api.url.transaction + "/Document/Delete/" + id,
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                // send notification
                if (jqXHR.status = 200) {

                    //ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                    // refresh data
                    //GetDataAttach();
                }
                //else
                //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
        /*}
    });*/
    };

    self.Submit = function () {
        // uploading documents. after upload completed, see SaveTransaction()
        var data = {
            ApplicationID: viewModel.TransactionModel().ApplicationID,
            CIF: viewModel.TransactionModel().Customer.CIF,
            Name: viewModel.TransactionModel().Customer.Name
        };

        //delete document in submit
        if (viewModel.DocumentToDelete().length > 0) {
            for (var i = 0; i < self.DocumentToDelete().length; i++) {
                self.DeleteDocument(viewModel.DocumentToDelete()[i].ID);
            }
        }
        if (viewModel.TransactionModel().Documents.length == 0) {
            ShowNotification("Task Validation Warning", "Document attachment is not available, please attach document.", "gritter-warning", false);
            return false;
        }

        //bangkit
        // chandra - underlying
        if (viewModel.NewDocuments().length > 0 && viewModel.UpdatePendingDocuments().IsCompleted() == false) {
            //if (viewModel.NewDocuments().length > 0) {
            //for (var i = 0; i < viewModel.NewDocuments().length; i++) {
            //    UploadFile(data, viewModel.NewDocuments()[i], SaveTransaction);
            //}
            if (viewModel.NewDocuments().length > 0) {
                UploadFiles(data, viewModel.NewDocuments(), SaveTransaction, viewModel.NewDocuments().length);
            }
        }
        else {
            if (viewModel.UpdatePendingDocuments().IsCompleted()) {
                //for (var i = 0; i < viewModel.NewDocuments().length; i++) {
                //UploadFile(data, viewModel.NewDocuments()[i], SaveTransaction);
                //}
                //viewModel.AddListItem();
                if (viewModel.NewDocuments().length > 0) {
                    UploadFiles(data, viewModel.NewDocuments(), SaveTransaction, viewModel.NewDocuments().length);
                }
                else {
                    SaveTransaction(); //add to list use existing doc
                }
            }
            else {
                SaveTransaction(); // save draft
            }
        }
    };

    self.AddListItem = function () {
        //var ID = viewModel.UpdatePendingDocuments().TransactionID().toString();
        var body = {
            Title: viewModel.UpdatePendingDocuments().TransactionID().toString(),
            __metadata: {
                type: config.sharepoint.metadata.listPending
            }
        };

        var options = {
            url: config.sharepoint.url.api + "/Lists(guid'" + config.sharepoint.listIdPending + "')/Items",
            data: JSON.stringify(body),
            digest: jQuery("#__REQUESTDIGEST").val()
        };

        Helper.Sharepoint.List.Add(options, OnSuccessAddListItem, OnError, OnAlways);
    }

    function OnSuccessAddListItem(data, textStatus, jqXHR) {
        self.IsEditable(false);
        //viewModel.UpdatePendingDocuments().IsCompleted(false);
        ShowNotification("Success", JSON.stringify(data.Message), "gritter-success", true);
        window.location = "/home";
    }

    self.Amounttmp_u = ko.computed({
        read: function () {
            if (self.StatementLetter_u().ID() == 1) {
                self.Currency_u().ID(1);
                self.Amount_u(treshHold);
                //return treshHold;
            }
            var value = self.Amount_u();
            var res = value * parseInt(self.Rate_u()) / parseInt(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res));

        },
        write: function (data) {
            var res = data * parseInt(self.Rate_u()) / parseInt(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res));

        }
    }, this);
    self.CaluclateRate_u = ko.computed({
        read: function () {
            var CurrencyID = self.Currency_u().ID();
            var AccountID = self.Selected();
            if (CurrencyID != '' && CurrencyID != undefined) {
                GetRateIDRAmount(CurrencyID);
            } else {
                self.Rate_u(0);
            }
            if (AccountID == '' && AccountID != undefined) {
                alert('test');
            }
        },
        write: function () { }
    });

    self.Proformas = ko.observableArray([new SelectModel('', '')]);
    self.CodeUnderlying = ko.computed({
        read: function () {
            if (self.UnderlyingDocument_u().ID() != undefined && self.UnderlyingDocument_u().ID() != '') {
                var item = ko.utils.arrayFirst(self.ddlUnderlyingDocument_u(), function (x) {
                    return self.UnderlyingDocument_u().ID() == x.ID();
                }
                )
                if (item != null) {
                    return item.Code();
                }
                else {
                    return ''
                }
            } else {
                return ''
            }
        },
        write: function (value) {
            return value;
        }
    });

    //filter pending documents
    self.FilterBranchCode = ko.observable("");
    self.FilterBranchName = ko.observable("");
    self.FilterApplicationID = ko.observable("");
    self.FilterCustomer = ko.observable("");
    self.FilterProduct = ko.observable("");
    self.FilterCurrency = ko.observable("");
    self.FilterAmount = ko.observable("");
    self.FilterDebitAccNumber = ko.observable("");
    self.FilterFXTransaction = ko.observable("");
    self.FilterTopUrgent = ko.observable("");
    self.FilterTransactionStatus = ko.observable("");
    self.FilterLastModifiedBy = ko.observable("");
    self.FilterLastModifiedDate = ko.observable(moment(new Date()).format(config.format.date));//ko.observable(new Date());//
    //self.FilterExitTime = ko.observable(""); // add by adi 27 okt 2016

    self.OtherUnderlying_u = ko.observable('');
    self.tmpOtherUnderlying = ko.observable('');
    self.IsSelected_u = ko.observable(false);
    self.LastModifiedBy_u = ko.observable("");
    self.LastModifiedDate_u = ko.observable(new Date());

    // Attach Variable
    self.ID_a = ko.observable(0);
    self.FileName_a = ko.observable('');
    self.DocumentPurpose_a = ko.observable(new DocumentPurposeModel2('', '', ''));
    self.ddlDocumentPurpose_a = ko.observableArray([]);
    self.DocumentPurposeValue = ko.observable(0);

    self.DocumentType_a = ko.observable(new DocumentTypeModel2('', ''));
    self.ddlDocumentType_a = ko.observableArray([]);
    self.DocumentTypeValue = ko.observable(0);

    self.DocumentRefNumber_a = ko.observable("");
    self.LastModifiedBy_a = ko.observable("");
    self.LastModifiedDate_a = ko.observable(new Date());
    self.CustomerUnderlyingMappings_a = ko.observableArray([new CustomerUnderlyingMappingModel('', '', '')]);

    self.Documents = ko.observableArray([]);
    self.NewDocuments = ko.observableArray([]);
    self.NewUploadDocuments = ko.observableArray([]);
    self.DocumentPath_a = ko.observable("");

    // filter Underlying
    self.UnderlyingFilterCIF = ko.observable("");
    self.UnderlyingFilterParentID = ko.observable("");
    self.UnderlyingFilterIsSelected = ko.observable(false);
    self.UnderlyingFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingFilterDocumentType = ko.observable("");
    self.UnderlyingFilterCurrency = ko.observable("");
    self.UnderlyingFilterStatementLetter = ko.observable("");
    self.UnderlyingFilterAmount = ko.observable("");
    self.UnderlyingFilterAvailableAmount = ko.observable("");
    self.UnderlyingFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingFilterExpiredDate = ko.observable("");
    self.UnderlyingFilterReferenceNumber = ko.observable("");
    self.UnderlyingFilterSupplierName = ko.observable("");
    self.UnderlyingFilterIsProforma = ko.observable("");
    self.UnderlyingFilterIsAvailable = ko.observable("");
    self.UnderlyingFilterIsExpiredDate = ko.observable("");

    // filter Attach Underlying File
    self.AttachFilterFileName = ko.observable("");
    self.AttachFilterDocumentPurpose = ko.observable("");
    self.AttachFilterModifiedDate = ko.observable("");
    self.AttachFilterDocumentRefNumber = ko.observable("");

    // filter Underlying for Attach
    self.UnderlyingAttachFilterIsSelected = ko.observable("");
    self.UnderlyingAttachFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingAttachFilterDocumentType = ko.observable("");
    self.UnderlyingAttachFilterCurrency = ko.observable("");
    self.UnderlyingAttachFilterStatementLetter = ko.observable("");
    self.UnderlyingAttachFilterAmount = ko.observable("");
    self.UnderlyingAttachFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingAttachFilterExpiredDate = ko.observable("");
    self.UnderlyingAttachFilterReferenceNumber = ko.observable("");
    self.UnderlyingAttachFilterSupplierName = ko.observable("");

    // filter Underlying for Attach
    self.ProformaFilterIsSelected = ko.observable(false);
    self.ProformaFilterUnderlyingDocument = ko.observable("");
    self.ProformaFilterDocumentType = ko.observable("Proforma");
    self.ProformaFilterCurrency = ko.observable("");
    self.ProformaFilterStatementLetter = ko.observable("");
    self.ProformaFilterAmount = ko.observable("");
    self.ProformaFilterDateOfUnderlying = ko.observable("");
    self.ProformaFilterExpiredDate = ko.observable("");
    self.ProformaFilterReferenceNumber = ko.observable("");
    self.ProformaFilterSupplierName = ko.observable("");
    self.ProformaFilterIsProforma = ko.observable(false);



    //// start Checked Calculate for Attach Grid and Underlying Grid
    self.TempSelectedAttachUnderlying = ko.observableArray([]);

    //// start Checked Calculate for Underlying Proforma Grid
    self.TempSelectedUnderlyingProforma = ko.observableArray([]);

    //// start Checked Calculate for Underlying Grid
    self.TempSelectedUnderlying = ko.observableArray([]);

    /*self.TransactionModel().UnderlyingDoc.subscribe(function(UnderlyingDoc) {
        //get underlying code based on selected
        if(UnderlyingDoc > 0)
        {
            GetUnderlyingDocName(UnderlyingDoc);
        }
    });*/

    // start Checked Calculate for Attach Grid and Underlying Grid
    self.TempSelectedAttachUnderlying = ko.observableArray([]);

    // start Checked Calculate for Underlying Proforma Grid
    self.TempSelectedUnderlyingProforma = ko.observableArray([]);

    // start Checked Calculate for Underlying Grid
    self.TempSelectedUnderlying = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.AllPendingDocuments = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyings = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyingProformas = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyingFiles = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerAttachUnderlyings = ko.observableArray([]);

    // grid properties Pending Documents
    self.PendingDocumentsGridProperties = ko.observable();
    self.PendingDocumentsGridProperties(new GridPropertiesModel(GetPendingDocuments));

    // grid properties Underlying
    self.UnderlyingGridProperties = ko.observable();
    self.UnderlyingGridProperties(new GridPropertiesModel(GetDataUnderlying));

    // grid properties Attach Grid
    self.AttachGridProperties = ko.observable();
    self.AttachGridProperties(new GridPropertiesModel(GetDataAttachFile));

    // grid properties Underlying File
    self.UnderlyingAttachGridProperties = ko.observable();
    self.UnderlyingAttachGridProperties(new GridPropertiesModel(GetDataUnderlyingAttach));

    // grid properties Proforma
    self.UnderlyingProformaGridProperties = ko.observable();
    self.UnderlyingProformaGridProperties(new GridPropertiesModel(GetDataUnderlyingProforma));

    // set default sorting for pending documents grid
    self.PendingDocumentsGridProperties().SortColumn("LastModifiedDate");
    self.PendingDocumentsGridProperties().SortOrder("DESC");

    // set default sorting for Underlying Grid
    self.UnderlyingGridProperties().SortColumn("ID");
    self.UnderlyingGridProperties().SortOrder("ASC");

    // set default sorting for Underlying File Grid
    self.AttachGridProperties().SortColumn("FileName");
    self.AttachGridProperties().SortOrder("ASC");

    // set default sorting for Attach Underlying Grid
    self.UnderlyingAttachGridProperties().SortColumn("ID");
    self.UnderlyingAttachGridProperties().SortOrder("ASC");

    // set default sorting for Proforma grid
    self.UnderlyingProformaGridProperties().SortColumn("ID");
    self.UnderlyingProformaGridProperties().SortOrder("ASC");

    function ResetDataAttachment(index, isSelect) {
        self.CustomerAttachUnderlyings()[index].IsSelectedAttach = isSelect;
        var dataRows = self.CustomerAttachUnderlyings().slice(0);
        self.CustomerAttachUnderlyings([]);
        self.CustomerAttachUnderlyings(dataRows);
    }

    function ResetDataProforma(index, isSelect) {
        self.CustomerUnderlyingProformas()[index].IsSelectedProforma = isSelect;
        var dataRows = self.CustomerUnderlyingProformas().slice(0);
        self.CustomerUnderlyingProformas([]);
        self.CustomerUnderlyingProformas(dataRows);
    }

    function ResetDataUnderlying(index, isSelect) {
        self.CustomerUnderlyings()[index].IsEnable = isSelect;
        var dataRows = self.CustomerUnderlyings().slice(0);
        self.CustomerUnderlyings([]);
        self.CustomerUnderlyings(dataRows);
    }

    self.onSelectionOther = function (underlyingID) {
        if (underlyingID > 0) {
            GetUnderlyingDocName(underlyingID);
        }
    };

    function GetUnderlyingDocName(underlyingID) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.underlyingdoc + "/" + underlyingID,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if (data != null) {

                        self.isNewUnderlying(false); //return default disabled

                        // available type to input for new underlying name if code is 999
                        if (data.Code == '999') {
                            self.TransactionDealDetailModel().bookunderlyingdesc = '';
                            self.isNewUnderlying(true);
                        }
                        else {
                            self.isNewUnderlying(false);
                            self.TransactionDealDetailModel().OtherUnderlying = '';
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function SetSelectedProforma(data) {
        // if(self.TempSelectedUnderlyingProforma().length>0) {
        for (var i = 0 ; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlyingProforma(), function (item) {
                return item.ID == data[i].ID;
            });
            if (selected != null) {
                data[i].IsSelectedProforma = true;
            }
            else {
                data[i].IsSelectedProforma = false;
            }
        }
    }

    function TotalUtilize() {
        var total = 0;
        ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (item) {
            if (item.Enable() == false)
                total += parseFloat(ko.utils.unwrapObservable(item.USDAmount()));
        });
        var fixTotal = parseFloat(total).toFixed(2);
        return fixTotal;
    }

    function GetRateIDRAmount(CurrencyID) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
            params: {
            },
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    self.Rate_u(data.RupiahRate);
                    //if(CurrencyID!=1){ // if not USD

                    var e = document.getElementById("Amount_u").value;
                    e = e.replace('.', '').replace('.', '').replace('.', '').replace('.', '');
                    self.Amount_u(e);

                    res = e * data.RupiahRate / idrrate;
                    res = Math.round(res * 100) / 100;
                    self.AmountUSD_u(res);
                    //}
                }
            }
        });
    }

    function GetUSDAmount(currencyID, amount, IdUnderlying) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetAmountUSD/" + currencyID + "/" + amount,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlying.push(new SelectUtillizeModel(IdUnderlying, false, data));
                    var total = TotalUtilize();
                    viewModel.UtilizationAmounts(total);
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
        //Helper.Ajax.Get(options, OnSuccessGetUSDAmount, OnError, OnAlways);
    }

    function SetSelectedUnderlying(data) {

        for (var i = 0 ; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (item) {
                return item.ID == data[i].ID;
            });
            if (selected != null) {
                data[i].IsUtilize = true;
            }
            else {
                data[i].IsUtilize = false;
            }
        }
    }

    self.onSelectionAttach = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            self.TempSelectedAttachUnderlying.remove(data);
            ResetDataAttachment(index, false);
        } else {
            self.TempSelectedAttachUnderlying.push({
                ID: item.ID
            });
            ResetDataAttachment(index, true);
        }
    }

    self.onSelectionProforma = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlyingProforma(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            self.TempSelectedUnderlyingProforma.remove(data);
            ResetDataProforma(index, false);
        } else {
            self.TempSelectedUnderlyingProforma.push({
                ID: item.ID
            });
            ResetDataProforma(index, true);
        }
    }

    self.onSelectionUtilize = function (index, item) {

        var data = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
            return x.ID() == item.ID;
        });

        if (data != null) { //uncheck condition
            self.TempSelectedUnderlying.remove(data);
            ResetDataUnderlying(index, false);
            setTotalUtilize();

        }
        else {
            var statementA = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                return x.StatementLetter() == 1;
            });
            var statementB = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                return x.StatementLetter() == 2;
            });

            var amount = document.getElementById("new-eqv-usd").value;
            if (amount != null) {
                amount = amount.replace(',', '').replace(',', '').replace(',', '').replace(',', '');
            }


            if (((statementA == null && item.StatementLetter.ID == 2) || (statementB == null && item.StatementLetter.ID == 1))
                && parseFloat(self.utilizationAmount()) < parseFloat(amount) &&
                viewModel.TransactionDealDetailModel().StatementLetter.ID == item.StatementLetter.ID) {

                self.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, false, item.AvailableAmount, item.StatementLetter.ID));
                ResetDataUnderlying(index, true);
                setTotalUtilize();
            } else {
                ResetDataUnderlying(index, false);
            }
        }

    }

    // bind clear filters
    self.UnderlyingClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.UnderlyingFilterIsSelected(false);
        self.UnderlyingFilterUnderlyingDocument("");
        self.UnderlyingFilterDocumentType("");
        self.UnderlyingFilterCurrency("");
        self.UnderlyingFilterStatementLetter("");
        self.UnderlyingFilterAmount("");
        self.UnderlyingFilterDateOfUnderlying("");
        self.UnderlyingFilterExpiredDate("");
        self.UnderlyingFilterReferenceNumber("");
        self.UnderlyingFilterSupplierName("");
        self.UnderlyingFilterIsProforma("");

        GetDataUnderlying();
    };

    self.PendingDocumentsClearFilters = function () {
        self.FilterBranchCode("");
        self.FilterBranchName("");
        self.FilterApplicationID("");
        self.FilterCustomer("");
        self.FilterProduct("");
        self.FilterCurrency("");
        self.FilterAmount("");
        self.FilterDebitAccNumber("");
        self.FilterFXTransaction("");
        self.FilterTopUrgent("");
        self.FilterTransactionStatus("");
        self.FilterLastModifiedBy("");
        self.FilterLastModifiedDate("");
        // self.FilterExitTime("");

        GetPendingDocuments();
    }

    self.ProformaClearFilters = function () {
        self.ProformaFilterIsSelected(false);
        self.ProformaFilterUnderlyingDocument("");
        self.ProformaFilterDocumentType("Proforma");
        self.ProformaFilterCurrency("");
        self.ProformaFilterStatementLetter("");
        self.ProformaFilterAmount("");
        self.ProformaFilterDateOfUnderlying("");
        self.ProformaFilterExpiredDate("");
        self.ProformaFilterReferenceNumber("");
        self.ProformaFilterSupplierName("");
        self.ProformaFilterIsProforma("");

        GetDataUnderlyingProforma();
    };

    self.GetDataCode = function (data) {
        alert(CurrencyDebModel.Code);
    }

    self.GetPendingDocuments = function () {
        GetPendingDocuments();
    };

    self.GetDataUnderlying = function () {
        GetDataUnderlying();
    };

    self.GetSelectedUtilizeID = function () {
        GetSelectedUtilizeID();
    }

    self.GetDataAttachFile = function () {
        GetDataAttachFile();
    }

    self.GetDataUnderlyingAttach = function () {
        GetDataUnderlyingAttach();
    }

    self.GetDataUnderlyingProforma = function () {
        GetDataUnderlyingProforma();
    }

    self.GetUnderlyingParameters = function (data) {
        GetUnderlyingParameters(data);
    }

    //The Object which stored data entered in the observables
    var CustomerUnderlying = {
        ID: self.ID_u,
        CIF: self.CIF_u,
        customerName: self.CustomerName_u,
        UnderlyingDocument: self.UnderlyingDocument_u,
        DocumentType: self.DocumentType_u,
        Currency: self.Currency_u,
        Amount: self.Amount_u,
        AttachmentNo: self.AttachmentNo_u,
        StatementLetter: self.StatementLetter_u,
        IsDeclarationOfException: self.IsDeclarationOfException_u,
        StartDate: self.StartDate_u,
        EndDate: self.EndDate_u,
        DateOfUnderlying: self.DateOfUnderlying_u,
        ExpiredDate: self.ExpiredDate_u,
        ReferenceNumber: self.ReferenceNumber_u,
        SupplierName: self.SupplierName_u,
        InvoiceNumber: self.InvoiceNumber_u,
        IsSelectedProforma: self.IsSelectedProforma,
        IsProforma: self.IsProforma_u,
        IsUtilize: self.IsUtilize_u,
        Proformas: self.Proformas,
        OtherUnderlying: self.OtherUnderlying_u,
        LastModifiedBy: self.LastModifiedBy_u,
        LastModifiedDate: self.LastModifiedDate_u
    };

    var NewDocuments = {
        Documents: self.Documents,
        DocumentPath: self.DocumentPath_a,
        Type: self.DocumentType_a,
        Purpose: self.DocumentPurpose_a
    }

    var CustomerUnderlyingFile = {
        ID: self.ID_a,
        FileName: DocumentModels().FileName,
        DocumentPath: DocumentModels().DocumentPath,
        DocumentPurpose: self.DocumentPurpose_a,
        DocumentType: self.DocumentType_a,
        DocumentRefNumber: self.DocumentRefNumber_a,
        LastModifiedBy: self.LastModifiedBy_a,
        LastModifiedDate: self.LastModifiedDate_a,
        CustomerUnderlyingMappings: self.CustomerUnderlyingMappings_a
    };

    function SetMappingAttachment() {
        CustomerUnderlyingFile.CustomerUnderlyingMappings([]);
        for (var i = 0 ; i < self.TempSelectedAttachUnderlying().length; i++) {
            CustomerUnderlyingFile.CustomerUnderlyingMappings.push(new CustomerUnderlyingMappingModel(0, self.TempSelectedAttachUnderlying()[i].ID, customerNameData + '-(A)'));
        }
    }

    function SetMappingProformas() {
        CustomerUnderlying.Proformas([]);
        for (var i = 0 ; i < self.TempSelectedUnderlyingProforma().length; i++) {
            CustomerUnderlying.Proformas.push(new SelectModel(cifData, self.TempSelectedUnderlyingProforma()[i].ID));
        }
    }

    self.save_a = function () {

        var form = $("#frmUnderlying");
        form.validate();
        if (form.valid()) {
            //viewModel.IsEditable(false);

            var FxWithUnderlying = (self.TempSelectedAttachUnderlying().length > 0);
            var FxWithNoUnderlying = (self.DocumentPurpose_a().ID() != 2);

            if (FxWithUnderlying || FxWithNoUnderlying) {
                var data = {
                    CIF: CustomerUnderlying.CIF,
                    Name: CustomerUnderlying.CustomerName,
                    Type: ko.utils.arrayFirst(self.ddlDocumentType_a(), function (item) {
                        return item.ID == self.DocumentType_a().ID();
                    }),
                    Purpose: ko.utils.arrayFirst(self.ddlDocumentPurpose_a(), function (item) {
                        return item.ID == self.DocumentPurpose_a().ID();
                    })
                };

                if (FxWithUnderlying) {
                    UploadFileUnderlying(data, NewDocuments, SaveDataFile);
                }
                else if (FxWithNoUnderlying) {
                    self.IsFXTransactionAttach(true);

                    var doc = {
                        ID: 0,
                        Type: data.Type,
                        Purpose: data.Purpose,
                        FileName: NewDocuments.DocumentPath().name,
                        DocumentPath: self.DocumentPath_a(),
                        LastModifiedDate: new Date(),
                        LastModifiedBy: null
                    };

                    //alert(JSON.stringify(self.DocumentPath()))
                    if (doc.Type == null || doc.Purpose == null || doc.DocumentPath == null) {
                        alert("Please complete the upload form fields.")
                    } else {
                        self.Documents.push(doc);
                        self.NewDocuments.push(doc);

                        // hide upload dialog
                        $("#modal-form-Attach").modal('hide');
                        $('.remove').click();
                    }

                    $("#modal-form-Attach").modal('hide');
                    $('.remove').click();
                }
            }
            else {
                //viewModel.IsEditable(true);
                alert('Please select the underlying document');
            }
        };
    };

    self.AddDocument = function () {
        var doc = {
            ID: 0,
            //Type: self.DocumentType(),
            Type: self.DocumentTypeValue(),
            //Purpose: self.DocumentPurpose(),
            Purpose: self.DocumentPurposeValue(),
            FileName: self.DocumentPath().name,
            //FileName:self.DocumentFileName(),
            DocumentPath: self.DocumentPath(),
            LastModifiedDate: new Date(),
            LastModifiedBy: null
        };

        if (doc.Type == null || doc.Purpose == null || doc.DocumentPath == null) {
            alert("Please complete the upload form fields.")
        } else {
            self.Documents.push(doc);
            self.NewDocuments.push(doc);
            //self.IsNewDocument(false);
            // hide upload dialog
            if (viewModel.TransactionModel().ModePayment != 'BCP2') {
                $("#modal-form-upload-ipe").modal('hide');
            }
            else {
                $("#modal-form-upload").modal('hide');
            }
            $('.remove').click();
        }
    };

    function SaveDataFile() {

        SetMappingAttachment();
        //Ajax call to insert the CustomerUnderlyings
        CustomerUnderlyingFile.FileName = DocumentModels().FileName;
        CustomerUnderlyingFile.DocumentPath = DocumentModels().DocumentPath;
        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlyingfile,
            data: ko.toJSON(CustomerUnderlyingFile), //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //viewModel.IsEditable(true);

                    // send notification
                    ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                    // hide current popup window
                    $("#modal-form-Attach").modal('hide');
                    $('.remove').click();

                    // refresh data
                    GetDataAttachFile();
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    //viewModel.IsEditable(true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                //viewModel.IsEditable(true);
            }
        });


    }

    self.delete_u = function () {
        // hide current popup window
        $("#modal-form-Underlying").modal('hide');
        $('.remove').click();

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form-Underlying").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customerunderlying + "/" + CustomerUnderlying.ID(),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {

                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                            // refresh data
                            GetDataUnderlying();

                            //new calculated
                            GetAvailableStatementB(cifData)
                            viewModel.GetTotalAmount(cifData, viewModel.TransactionDealDetailModel().AmountUSD);
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    self.cancel_u = function () {
    }

    self.cancel_a = function () {
        $('.remove').click();
    }

    self.delete_a = function (item) {

        //alert(JSON.stringify(item));

        if (item.ID == null) {
            self.TempFXTransactionAttach.remove(item);
            self.CustomerUnderlyingFiles.remove(item);
        }
        else {
            bootbox.confirm("Are you sure want to delete?", function (result) {
                if (!result) {
                    //$("#modal-form").modal('show');
                } else {
                    //Ajax call to delete the Customer
                    $.ajax({
                        type: "DELETE",
                        url: api.server + api.url.customerunderlyingfile + "/" + item.ID,
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            // send notification
                            if (jqXHR.status = 200) {
                                DeleteFile(item.DocumentPath);
                                ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // refresh data
                                GetDataAttachFile();
                            } else
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    }

    self.cancel_detail = function () {

        $('#transaction-parent').show();
        $('#transaction-child').hide();
        $('#transaction-child-ipe').hide();

        self.Documents([]);

        // self.PendingDocumentsClearFilters(); //azam
        self.ProformaClearFilters();
        self.UnderlyingClearFilters();

        $('#s4-workspace').animate({ scrollTop: 0 }, 'fast');
    };

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
    };

    //Function to Display record to be show
    self.GetUnderlyingSelectedRow = function (data) {


        $("#modal-form-Underlying").modal('show');

        self.ID_u(data.ID);
        self.CustomerName_u(data.customerName);
        self.UnderlyingDocument_u(new UnderlyingDocumentModel2(data.UnderlyingDocument.ID, data.UnderlyingDocument.Name, data.UnderlyingDocument.Code));
        self.OtherUnderlying_u(data.OtherUnderlying);
        self.DocumentType_u(new DocumentTypeModel2(data.DocumentType.ID, data.DocumentType.Name));
        self.Currency_u(data.Currency);
        self.Amount_u(data.Amount);
        self.AttachmentNo_u(data.AttachmentNo);
        self.StatementLetter_u(data.StatementLetter);
        self.IsDeclarationOfException_u(data.IsDeclarationOfException);
        self.StartDate_u(self.LocalDate(data.StartDate, true, false));
        self.EndDate_u(self.LocalDate(data.EndDate, true, false));
        self.DateOfUnderlying_u(self.LocalDate(data.DateOfUnderlying, true, false));
        self.ExpiredDate_u(self.LocalDate(data.ExpiredDate, true, false));
        self.ReferenceNumber_u(data.ReferenceNumber);
        self.SupplierName_u(data.SupplierName);
        self.InvoiceNumber_u(data.InvoiceNumber);
        self.IsProforma_u(data.IsProforma);
        self.IsSelectedProforma(data.IsSelectedProforma);
        self.LastModifiedBy_u(data.LastModifiedBy);
        self.LastModifiedDate_u(data.LastModifiedDate);
        //Call Grid Underlying for Check Proforma
        GetSelectedID();
        GetDataUnderlyingProforma();
    };

    //Add Attach File
    self.NewAttachFile = function () {
        // show the dialog task form
        $("#modal-form-Attach").modal('show');
        self.TempSelectedAttachUnderlying([]);
        self.ID_a(0);
        self.DocumentPurpose_a(new DocumentPurposeModel2('', '', ''));
        self.DocumentType_a(new DocumentTypeModel2('', ''));
        self.DocumentPath_a('');
        GetDataUnderlyingAttach();
    }

    // Function to Read parameter Underlying
    function GetUnderlyingParameters(data) {
        console.log(data);
        var underlying = data['UnderltyingDoc'];
        for (var i = 0; i < underlying.length; i++) {
            self.ddlUnderlyingDocument_u.push(new UnderlyingDocumentModel2(underlying[i].ID, underlying[i].Name, underlying[i].Code));
        }
        self.ddlDocumentType_u(data['DocType']);
        self.ddlDocumentType_a(data['DocType']);
        self.ddlCurrency_u(data['Currency']);
        self.ddlDocumentPurpose_a(data['PurposeDoc']);
        self.ddlStatementLetter_u(data['StatementLetter']);
    }

    //Function to Read All Pending Documents
    function GetPendingDocuments() {
        // widget reloader function start
        if ($box.css('position') == 'static') { $remove = true; $box.addClass('position-relative'); }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // define filter
        var filters = GetPendingDocumentsFilteredColumn();

        var options = {
            url: api.server + api.url.transaction + "/pending",
            params: {
                page: self.PendingDocumentsGridProperties().Page(),
                size: self.PendingDocumentsGridProperties().Size(),
                sort_column: self.PendingDocumentsGridProperties().SortColumn(),
                sort_order: self.PendingDocumentsGridProperties().SortOrder()
            },
            token: accessToken
        };

        // define method GET / POST
        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataPending, OnError, OnAlways);
        }
        else
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataPending, OnError, OnAlways);
    }

    //Function to Display record to be updated
    self.getSelectedPendingDocuments = function (data) {
        // reset validation

        //self.PendingDocumentsClearFilters(); //azam

        var validator = $("#aspnetForm").validate();
        validator.resetForm();

        self.fxTransaction(data.Transaction.IsFXTransaction);

        GetTransactionByID(data.Transaction.ApplicationID);
    };

    //Function to Read All Customers Underlying
    function GetDataUnderlying() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Transaction/" + self.TransactionModel().ID,
            params: {
                page: self.UnderlyingGridProperties().Page(),
                size: self.UnderlyingGridProperties().Size(),
                sort_column: self.UnderlyingGridProperties().SortColumn(),
                sort_order: self.UnderlyingGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetUnderlyingFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        }
    }

    //Function to Read All Customers Underlying File
    function GetDataAttachFile() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlyingfile + "/Transaction/" + self.TransactionModel().ID,
            params: {
                page: self.AttachGridProperties().Page(),
                size: self.AttachGridProperties().Size(),
                sort_column: self.AttachGridProperties().SortColumn(),
                sort_order: self.AttachGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        }
    }

    //Function to Read All Customers Underlying for Attach File
    function GetDataUnderlyingAttach() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            //url: api.server + api.url.customerunderlying + "/Attach/" + self.TransactionModel().ID,
            url: api.server + api.url.customerunderlying + "/Attach",
            params: {
                page: self.UnderlyingAttachGridProperties().Page(),
                size: self.UnderlyingAttachGridProperties().Size(),
                sort_column: self.UnderlyingAttachGridProperties().SortColumn(),
                sort_order: self.UnderlyingAttachGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetUnderlyingAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingDataAttachFile, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingDataAttachFile, OnError, OnAlways);
        }
    }

    //Function to Read All Customers Underlying Proforma
    function GetDataUnderlyingProforma() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Proforma/Transaction/" + self.TransactionModel().ID,
            params: {
                page: self.UnderlyingProformaGridProperties().Page(),
                size: self.UnderlyingProformaGridProperties().Size(),
                sort_column: self.UnderlyingProformaGridProperties().SortColumn(),
                sort_order: self.UnderlyingProformaGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetUnderlyingProformaFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingProforma, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingProforma, OnError, OnAlways);
        }
    }

    function GetSelectedUtilizeID() {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });

        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SelectedUtilizeID",
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON(filters),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlying([]);
                    for (var i = 0; i < data.length; i++) {
                        self.TempSelectedUnderlying.push(new SelectUtillizeModel(data[i].ID, true, 0));
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetSelectedID() {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.ProformaFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.ProformaFilterIsSelected() });
        if (self.ProformaFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.ProformaFilterUnderlyingDocument() });
        if (self.ProformaFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.ProformaFilterDocumentType() });


        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SelectedProformaID",
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON(filters),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlyingProforma([]);
                    for (var i = 0; i < data.length; i++) {
                        self.TempSelectedUnderlyingProforma.push({ ID: data[i].ID });
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetPendingDocumentsFilteredColumn() {
        var filters = [];
        if (self.FilterBranchCode() != "") filters.push({ Field: 'BranchCode', Value: self.FilterBranchCode() });
        if (self.FilterBranchName() != "") filters.push({ Field: 'BranchName', Value: self.FilterBranchName() });
        if (self.FilterApplicationID() != "") filters.push({ Field: 'ApplicationID', Value: self.FilterApplicationID() });
        if (self.FilterCustomer() != "") filters.push({ Field: 'Customer', Value: self.FilterCustomer() });
        if (self.FilterProduct() != "") filters.push({ Field: 'Product', Value: self.FilterProduct() });
        if (self.FilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.FilterCurrency() });
        if (self.FilterAmount() != "") filters.push({ Field: 'Amount', Value: self.FilterAmount() });
        if (self.FilterDebitAccNumber() != "") filters.push({ Field: 'DebitAccNumber', Value: self.FilterDebitAccNumber() });
        if (self.FilterFXTransaction() != "") filters.push({ Field: 'FXTransactionDesc', Value: self.FilterFXTransaction() });
        if (self.FilterTopUrgent() != "") filters.push({ Field: 'TopUrgentDesc', Value: self.FilterTopUrgent() });
        if (self.FilterTransactionStatus() != "") filters.push({ Field: 'TransactionStatus', Value: self.FilterTransactionStatus() });
        if (self.FilterLastModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterLastModifiedBy() });
        if (self.FilterLastModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterLastModifiedDate() });
        // if (self.FilterExitTime() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterExitTime() }); // add by adi
        return filters;
    }

    // Get filtered columns value
    function GetUnderlyingFilteredColumns() {
        var filters = [];
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingFilterIsSelected() });
        if (self.UnderlyingFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingFilterUnderlyingDocument() });
        if (self.UnderlyingFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingFilterDocumentType() });
        if (self.UnderlyingFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingFilterCurrency() });
        if (self.UnderlyingFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingFilterStatementLetter() });
        if (self.UnderlyingFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingFilterAmount() });
        if (self.UnderlyingFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingFilterDateOfUnderlying() });
        if (self.UnderlyingFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingFilterExpiredDate() });
        if (self.UnderlyingFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingFilterReferenceNumber() });
        if (self.UnderlyingFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingFilterSupplierName() });

        return filters;
    };

    // Get filtered columns value
    function GetUnderlyingAttachFilteredColumns() {
        var filters = [];
        self.UnderlyingFilterIsSelected(true); // flag for get all datas
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingAttachFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingAttachFilterIsSelected() });
        if (self.UnderlyingAttachFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingAttachFilterUnderlyingDocument() });
        if (self.UnderlyingAttachFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingFilterDocumentType() });
        if (self.UnderlyingAttachFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingFilterCurrency() });
        if (self.UnderlyingAttachFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingFilterStatementLetter() });
        if (self.UnderlyingAttachFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingFilterAmount() });
        if (self.UnderlyingAttachFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingFilterDateOfUnderlying() });
        if (self.UnderlyingAttachFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingFilterExpiredDate() });
        if (self.UnderlyingAttachFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingFilterReferenceNumber() });
        if (self.UnderlyingAttachFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingFilterSupplierName() });

        return filters;
    }

    // Get filtered columns value
    function GetAttachFilteredColumns() {
        var filters = [];
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.AttachFilterFileName() != "") filters.push({ Field: 'FileName', Value: self.AttachFilterFileName() });
        if (self.AttachFilterDocumentPurpose() != "") filters.push({ Field: 'DocumentPurpose', Value: self.AttachFilterDocumentPurpose() });
        if (self.AttachFilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.AttachFilterModifiedDate() });
        if (self.AttachFilterDocumentRefNumber() != "") filters.push({ Field: 'DocumentRefNumber', Value: self.AttachFilterDocumentRefNumber() });

        return filters;
    };

    // Get filtered columns value
    function GetUnderlyingProformaFilteredColumns() {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.ProformaFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.ProformaFilterIsSelected() });
        if (self.ProformaFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.ProformaFilterUnderlyingDocument() });
        if (self.ProformaFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.ProformaFilterDocumentType() });
        if (self.ProformaFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.ProformaFilterCurrency() });
        if (self.ProformaFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.ProformaFilterStatementLetter() });
        if (self.ProformaFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.ProformaFilterAmount() });
        if (self.ProformaFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.ProformaFilterDateOfUnderlying() });
        if (self.ProformaFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.ProformaFilterExpiredDate() });
        if (self.ProformaFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.ProformaFilterReferenceNumber() });
        if (self.ProformaFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.ProformaFilterSupplierName() });

        return filters;
    };

    // on success getdata document pending
    function OnSuccessGetDataPending(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.PendingDocuments(data.Rows); //Put the response in ObservableArray
            self.PendingDocumentsGridProperties().AllowFilter(true);
            //GetDefaultDate();
            self.PendingDocumentsGridProperties().Page(data['Page']);
            self.PendingDocumentsGridProperties().Size([data['Size']]);
            self.PendingDocumentsGridProperties().Total(data['Total']);
            self.PendingDocumentsGridProperties().TotalPages(Math.ceil(self.PendingDocumentsGridProperties().Total() / self.PendingDocumentsGridProperties().Size()));

        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetDataUnderlying(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            for (var i = 0; i < data.Rows.length; i++) {
                if (data.Rows[i].AvailableAmount == null) {
                    data.Rows[i].AvailableAmount = 0;
                }
            }

            self.CustomerUnderlyings(data.Rows);

            self.UnderlyingGridProperties().Page(data['Page']);
            self.UnderlyingGridProperties().Size(data['Size']);
            self.UnderlyingGridProperties().Total(data['Total']);
            self.UnderlyingGridProperties().TotalPages(Math.ceil(self.UnderlyingGridProperties().Total() / self.UnderlyingGridProperties().Size()));

            //alert(data.Rows[0].UtilizationAmount);

            var UtilizationAmount = 0;
            for (i = 0; i < data['Total']; i++) {
                UtilizationAmount = UtilizationAmount + data.Rows[i].UtilizationAmount;
            }

            self.UtilizationAmounts(UtilizationAmount);


        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetUnderlyingDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {

            for (var i = 0 ; i < data.Rows.length; i++) {
                var selected = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (item) {
                    return item.ID == data.Rows[i].ID;
                });
                if (selected != null) {
                    data.Rows[i].IsSelectedAttach = true;
                }
                else {
                    data.Rows[i].IsSelectedAttach = false;
                }
            }
            self.CustomerAttachUnderlyings(data.Rows);

            self.UnderlyingAttachGridProperties().Page(data['Page']);
            self.UnderlyingAttachGridProperties().Size(data['Size']);
            self.UnderlyingAttachGridProperties().Total(data['Total']);
            self.UnderlyingAttachGridProperties().TotalPages(Math.ceil(self.UnderlyingAttachGridProperties().Total() / self.UnderlyingAttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {

            self.CustomerUnderlyingFiles(data.Rows);

            self.AttachGridProperties().Page(data['Page']);
            self.AttachGridProperties().Size(data['Size']);
            self.AttachGridProperties().Total(data['Total']);
            self.AttachGridProperties().TotalPages(Math.ceil(self.AttachGridProperties().Total() / self.AttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetUnderlyingProforma(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {

            self.CustomerUnderlyingProformas(data.Rows);

            self.UnderlyingProformaGridProperties().Page(data['Page']);
            self.UnderlyingProformaGridProperties().Size(data['Size']);
            self.UnderlyingProformaGridProperties().Total(data['Total']);
            self.UnderlyingProformaGridProperties().TotalPages(Math.ceil(self.UnderlyingProformaGridProperties().Total() / self.UnderlyingProformaGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    function GetAvailableStatementB(CIFData) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetAvailableStatementB/" + CIFData,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                var t_TotalRemainig = 0;
                if (jqXHR.status = 200) {
                    t_TotalRemainig = data;
                    viewModel.AmountModel().TotalRemainigByCIF(t_TotalRemainig);

                } else {

                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        viewModel.IsEditable(false);
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }

    function GetTotalAmount(cif) {
        var options = {
            url: api.server + api.url.transaction + "/GetAmountTransaction=" + cif,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetTotalAmount, OnError, OnAlways);
    }

    function OnSuccessGetTotalAmount(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // return total underlying in this month
            return data;
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    //--------------------------- set Underlying Function & variable end
};

// Knockout View Model
var viewModel = new ViewModel();

// jQuery Init
$(document).ready(function () {

    // ace file upload
    $('#document-path').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });

    $('#aspnetForm').after('<form id="frmUnderlying"></form>');
    $("#modal-form-Underlying").appendTo("#frmUnderlying");
    $("#modal-form-Attach").appendTo("#frmUnderlying");
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    // datepicker
    $('.date-picker').datepicker({
        autoclose: true,
        onSelect: function () {
            this.focus();
        }
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    // validation
    //$('#aspnetForm1').validate({
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        focusCleanup: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });

    $('#frmUnderlying').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        focusCleanup: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });

    // Knockout Datepicker custom binding handler
    ko.bindingHandlers.datepicker = {
        init: function (element) {
            $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).datepicker("refresh");
        }
    };

    // Knockout custom file handler
    ko.bindingHandlers.file = {
        init: function (element, valueAccessor) {
            $(element).change(function () {
                var file = this.files[0];

                if (ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };

    // Dependant Observable for dropdown auto fill
    ko.dependentObservable(function () {
        var docPurpose = ko.utils.arrayFirst(viewModel.Parameter().DocumentPurposes(), function (item) { return item.ID == viewModel.Selected().DocumentPurpose(); });
        var docType = ko.utils.arrayFirst(viewModel.Parameter().DocumentTypes(), function (item) { return item.ID == viewModel.Selected().DocumentType(); });
        if (docType != null) viewModel.DocumentType(docType);
        if (docPurpose != null) viewModel.DocumentPurpose(docPurpose);

        if (viewModel.DocumentPurpose_a() != null) {
            if (viewModel.DocumentPurpose_a().ID() == 2) {
                viewModel.SelectingUnderlying(true);
            }
            else {
                viewModel.SelectingUnderlying(false);
            }
        }
    });

    // Token Validation
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(OnSuccessToken, OnError);
    } else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);

            viewModel.SPUser(ko.mapping.toJS(spUser));
        }

        // call get data inside view model
        // GetParameters(); //heavy load 15-11-2016
    }

    // Knockout Bindings
    ko.applyBindings(viewModel);

    viewModel.GetPendingDocuments();

    function GetRateAmount(CurrencyID) {
        var options = {
            url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetRateAmount, OnError, OnAlways);
    }

    function OnSuccessGetRateAmount(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            viewModel.TransactionModel().Rate(data.RupiahRate);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    var idrrate = 0;

    GetRateIDR();

    function GetRateIDR() {
        var options = {
            url: api.server + api.url.currency + "/CurrencyRate/" + "1",
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetRateIDR, OnError, OnAlways);
    }

    function OnSuccessGetRateIDR(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            idrrate = data.RupiahRate;
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    //================================= Eqv Calculation ====================================


    //var x = document.getElementById("trxn-amount");
    //var y = document.getElementById("rate");
    //var z = document.getElementById("currency");
    //var d = document.getElementById("eqv-usd");
    //var xstored = x.getAttribute("data-in");
    //var ystored = y.getAttribute("data-in");

    //setInterval(function () {
    //    if (x == document.activeElement) {
    //        var temp = x.value;
    //        if (xstored != temp) {
    //            xstored = temp;
    //            x.setAttribute("data-in", temp);
    //            calculate();
    //        }
    //    }
    //    if (y == document.activeElement) {
    //        var temp = y.value;
    //        if (ystored != temp) {
    //            ystored = temp;
    //            y.setAttribute("data-in", temp);
    //            calculate();
    //        }
    //    }
    //    /* dodit@2014.11.08:add posibility calculate if currency changed */
    //    if (z == document.activeElement) { // add by dodit 2014/11/8
    //        calculate();
    //    }
    //}, 100);

    //function calculate() {
    //    /* dodit@2014.11.08:altering calculate process to avoid NaN, avoid formula on USD, and formating feature */
    //    currency = viewModel.Selected().Currency();
    //    res = x.value;
    //    if (currency != 1) { // if not USD
    //        res = x.value * y.value / idrrate;
    //        res = Math.round(res * 100) / 100;
    //    }
    //    res = isNaN(res) ? 0 : res; //avoid NaN
    //    //$("#eqv-usd").val(res);

    //    viewModel.TransactionModel().AmountUSD(formatNumber(res));
    //}
    //x.onblur = calculate;
    //calculate();

    //================================= End Eqv Calculation ====================================
});

// get all parameters need
function GetParameters() {
    var options = {
        url: api.server + api.url.parameter,
        params: {
            select: "Product,Currency,Channel,BizSegment,Bank,BankCharge,AgentCharge,FXCompliance,ChargesType,DocType,PurposeDoc,statementletter,underlyingdoc,customer,lld,producttype"
        },
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
}

function SaveTransaction() {
    viewModel.IsEditable(true);
    var options = {
        url: api.server + api.url.transaction + "/Document/" + viewModel.TransactionModel().ID,
        token: accessToken,
        data: ko.toJSON(viewModel.NewUploadDocuments())
    };

    // Save to api
    Helper.Ajax.Put(options, OnSuccessUploadPending, OnError, OnAlways);
}

// delete & upload document underlying start
function DeleteFile(documentPath) {
    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Delete the file to the SharePoint folder.
    var deleteFile = deleteFileToFolder();
    deleteFile.done(function (file, status, xhr) {
        output = file.d;
    });
    deleteFile.fail(OnError);
    // Call function delete File Shrepoint Folder
    function deleteFileToFolder() {
        return $.ajax({
            url: serverUrl + "/_api/web/GetFileByServerRelativeUrl('" + documentPath + "')",
            type: "POST",
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "X-HTTP-Method": "DELETE",
                "IF-MATCH": "*"
                //"content-length": arrayBuffer.byteLength
            }
        });
    }
}

function UploadFileUnderlying(context, document, callBack) {

    // Define the folder path for this example.
    var serverRelativeUrlToFolder = '/Underlying Documents';

    // Get the file name from the file input control on the page.
    var parts = document.DocumentPath().name.split('.');

    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBufferUnderlying();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolderUnderlying(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItemUnderlying(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                // Change the display name and title of the list item.
                var changeItem = updateListItemUnderlying(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    DocumentModels({ "FileName": document.DocumentPath().name, "DocumentPath": output.ServerRelativeUrl });
                    //viewModel.TransactionDealDetailModel().Documents.push(doc);

                    //self.Documents.push(doc);
                    callBack();
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBufferUnderlying() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }

        //reader.readAsArrayBuffer(fileInput[0].files[0]);
        reader.readAsArrayBuffer(document.DocumentPath());
        //reader.readAsDataURL(document.DocumentPath());
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolderUnderlying(arrayBuffer) {

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                //"content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItemUnderlying(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItemUnderlying(itemMetadata) {

        // Define the list item changes. Use the FileLeafRef property to change the display name.
        // For simplicity, also use the name as the title.
        // The example gets the list item type from the item's metadata, but you can also get it from the
        // ListItemEntityTypeFullName property of the list.
        var body = {
            Title: document.DocumentPath().name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: context.Type.DocTypeName,
            Document_x0020_Purpose: context.Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}

function UploadFile(context, document, callBack) {

    // Define the folder path for this example.
    var serverRelativeUrlToFolder = '/Instruction Documents';

    var parts = document.DocumentPath.name.split('.');

    //var parts = document.DocumentPath.split('.');
    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBuffer();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolder(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                // Change the display name and title of the list item.
                var changeItem = updateListItem(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    //alert('file uploaded and updated');
                    //return output;
                    var doc = {
                        ID: 0,
                        Type: document.Type,
                        Purpose: document.Purpose,
                        LastModifiedDate: null,
                        LastModifiedBy: null,
                        DocumentPath: output.ServerRelativeUrl,
                        FileName: document.DocumentPath.name
                    };
                    viewModel.TransactionModel().Documents.push(doc);
                    viewModel.NewUploadDocuments.push(doc);

                    callBack();

                    viewModel.NewUploadDocuments([]);
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBuffer() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }
        //bangkit
        reader.readAsArrayBuffer(document.DocumentPath);
        //reader.readAsDataURL(document.DocumentPath);
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(arrayBuffer) {

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                //"content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItem(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItem(itemMetadata) {

        // Define the list item changes. Use the FileLeafRef property to change the display name.
        // For simplicity, also use the name as the title.
        // The example gets the list item type from the item's metadata, but you can also get it from the
        // ListItemEntityTypeFullName property of the list.
        //var body = String.format("{{'__metadata':{{'type':'{0}'}},'FileLeafRef':'{1}','Title':'{2}'}}", itemMetadata.type, fileName, fileName);
        var body = {
            Title: document.DocumentPath.name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: document.Type.Name,
            Document_x0020_Purpose: document.Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}
//
function UploadFiles(context, document, callBack, numFile) {
    // Define the folder path for this example.
    var serverRelativeUrlToFolder = '/Instruction Documents';
    viewModel.ArrayDoc(document.length - numFile);
    //var indexDocument = document.length - numFile;
    var parts = document[viewModel.ArrayDoc()].DocumentPath.name.split('.');
    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBuffer();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolder(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                // Change the display name and title of the list item.
                var changeItem = updateListItem(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    //alert('file uploaded and updated');
                    //return output;
                    var doc = {
                        ID: 0,
                        Type: document[viewModel.ArrayDoc()],
                        Purpose: document[viewModel.ArrayDoc()],
                        LastModifiedDate: null,
                        LastModifiedBy: null,
                        DocumentPath: output.ServerRelativeUrl,
                        FileName: document[viewModel.ArrayDoc()].DocumentPath.name
                    };
                    viewModel.TransactionModel().Documents.push(doc);
                    viewModel.NewUploadDocuments.push(doc);
                    if (numFile > 1) {
                        //todo
                        UploadFiles(context, viewModel.NewDocuments(), SaveTransaction, numFile - 1);
                    }
                    callBack();
                    //viewModel.NewUploadDocuments([]);
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBuffer() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }
        //bangkit
        reader.readAsArrayBuffer(document[[viewModel.ArrayDoc()]].DocumentPath);
        //reader.readAsDataURL(document.DocumentPath);
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(arrayBuffer) {

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                //"content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItem(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItem(itemMetadata) {

        // Define the list item changes. Use the FileLeafRef property to change the display name.
        // For simplicity, also use the name as the title.
        // The example gets the list item type from the item's metadata, but you can also get it from the
        // ListItemEntityTypeFullName property of the list.
        //var body = String.format("{{'__metadata':{{'type':'{0}'}},'FileLeafRef':'{1}','Title':'{2}'}}", itemMetadata.type, fileName, fileName);
        var body = {
            Title: document[viewModel.ArrayDoc()].DocumentPath.name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: document[viewModel.ArrayDoc()].Type.Name,
            Document_x0020_Purpose: document[viewModel.ArrayDoc()].Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}
//

function GetTransactionByID(ApplicationID) {

    var options = {
        url: api.server + api.url.transaction + "/" + ApplicationID + "/PendingTransaction",
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetApplicationID, OnError, OnAlways);
}

function OnSuccessGetApplicationID(data, textStatus, jqXHR) {
    if (data != null) {
        //alert(JSON.stringify(data));        

        GetPurposeDocument();
        GetDocumentType();
        viewModel.UpdatePendingDocuments().TransactionID(data.ID);

        if (data.IsOtherAccountNumber) {
            data.Account.AccountNumber = data.OtherAccountNumber;
        }
        //Modified By adi 24/11/2016
        if (data.LLDUnderlyingAmount == null || data.LLDUnderlyingAmount == 0) { 
            //data.LLDUnderlyingAmount = 0.00;
            data.LLDUnderlyingAmount = "";
            
        }
        if (data.Product.Name == "OTT") {
            if (parseFloat(data.AmountUSD) > parseFloat(Const_AmountLLD.USDAmount)) {
                if (data.Currency.ID != Const_AmountLLD.NonIDRSelected) {
                    viewModel.IsLimit = true;
                }
            } else {
                  viewModel.IsLimit = false;
            }
        }
        //end adi

        if (data.ChargingAccountCurrency !== null && data.ChargingAccountCurrency !== undefined && data.ChargingAccountCurrency > 0) {
            (function () {
                var options = {
                    url: api.server + api.url.currency + "/" + data.ChargingAccountCurrency,
                    /*params: {
                        id: data.ChargingAccountCurrency
                    },*/
                    token: accessToken
                };
                console.log(data.ChargingAccountCurrency);
                Helper.Ajax.Get(options, function (result, textStatus, jqXHR) {
                    if (jqXHR.status == 200) {
                        //console.log(result);
                        var mapping = {
                            'ignore': ["LastModifiedDate", "LastModifiedBy"]
                        };
                        viewModel.ChargingAccCurrency(ko.mapping.toJS(result, mapping));
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
                    }
                }, OnError, OnAlways);
            })();
            console.log(viewModel.ChargingAccCurrency());
        }

        if (data.UnderlyingDoc != null) {
            if (data.UnderlyingDoc.Code == '999') {
                viewModel.Selected().UnderlyingDoc(data.UnderlyingDoc.ID);
                data.UnderlyingDoc.Name = '';
                viewModel.isNewUnderlying(true);
            }
            else {
                viewModel.Selected().UnderlyingDoc(data.UnderlyingDoc.ID);
                data.UnderlyingDoc.Name = data.UnderlyingDoc.Name;
                viewModel.isNewUnderlying(false);
            }
        }

        if (data.Bank.Code == '999') {
            data.Bank.Description = data.OtherBeneBankName;
            data.Bank.SwiftCode = data.OtherBeneBankSwift;
        }

        viewModel.CurrencyDep(data.DebitCurrency);
        viewModel.TransactionModel(data);
        
        var DebitCurrencyID = viewModel.TransactionModel().DebitCurrency.ID;
        $('#currency-empty').val(DebitCurrencyID);


        if (viewModel.fxTransaction() == 'Yes') {
            for (var i = 0; i < data.Documents.length; i++) {
                if (data.Documents[i].Purpose.ID != '2') {
                    viewModel.Documents.push(data.Documents[i]);
                }
            }
        }
        else {
            viewModel.Documents(data.Documents);
        }

        cifData = data.Customer.CIF;
        customerNameData = data.Customer.Name;
        viewModel.GetSelectedUtilizeID();
        viewModel.GetDataUnderlying();
        viewModel.GetDataAttachFile();

        if (data.Product != null) {
            viewModel.Selected().Product(data.Product.ID);
        }
        if (data.Currency != null) {
            viewModel.Selected().Currency(data.Currency.ID);
        }
        if (data.Channel != null) {
            viewModel.Selected().Channel(data.Channel.ID);
        }
        if (data.Account != null) {
            viewModel.Selected().Account(data.Account.AccountNumber);
        }
        if (data.BizSegment != null) {
            viewModel.Selected().BizSegment(data.BizSegment.ID);
        }
        if (data.ProductType != null) {
            viewModel.Selected().ProductType(data.ProductType.ID);
        }
        if (data.BankCharges != null) {
            viewModel.Selected().BankCharges(data.BankCharges.ID);
        }
        if (data.AgentCharges != null) {
            viewModel.Selected().AgentCharges(data.AgentCharges.ID);
        }
        if (data.LLD != null) {
            viewModel.Selected().LLD(data.LLD.ID);
        }

        if (viewModel.TransactionModel().ModePayment != 'BCP2') {
            $('#transaction-parent').hide();
            $('#transaction-child').hide();
            $('#transaction-child-ipe').show();
        }
        else {
            $('#transaction-parent').hide();
            $('#transaction-child').show();
            $('#transaction-child-ipe').hide();
        }

        $('#document-path').ace_file_input({
            no_file: 'No File ...',
            btn_choose: 'Choose',
            btn_change: 'Change',
            droppable: false,
            //onchange:null,
            thumbnail: false, //| true | large
            //whitelist:'gif|png|jpg|jpeg'
            blacklist: 'exe|dll'
            //onchange:''
            //
        });

        $('#s4-workspace').animate({ scrollTop: 0 }, 'fast');
    }
}

//Adi 19/11/2016
function GetPurposeDocument() {
    var options = {
        url: api.server + api.url.purposedocs + "/Get",
        params: {
        },
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetPurposeDocument, OnError, OnAlways);
}

function OnSuccessGetPurposeDocument(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        console.log(data);
        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };
        //viewModel.ddlDocumentPurpose_a(ko.observable.toJS(result, mapping));
        // viewModel.ddlDocumentPurpose_a(ko.mapping.toJS(data.PurposeDoc, mapping));
        // viewModel.ddlDocumentPurpose_a(ko.observable.toJS(result, mapping));
        viewModel.ddlDocumentPurpose_a(ko.mapping.toJS(data, mapping));
        //viewModel.Parameter().DocumentPurposes(ko.mapping.toJS(data.PurposeDoc, mapping));
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function GetDocumentType() {
    var options = {
        url: api.server + api.url.doctype,
        params: {
        },
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetDocumentType, OnError, OnAlways);
}

function OnSuccessGetDocumentType(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        console.log(data);
        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };
        //viewModel.ChargingAccCurrency(ko.mapping.toJS(result, mapping));
        viewModel.ddlDocumentType_a(ko.mapping.toJS(data, mapping));
        // viewModel.Parameter().DocumentTypes(ko.mapping.toJS(data.DocType, mapping));
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

//end Adi



//udin 24092016
function IsDocumentCompleteCheck(data) {
    if (viewModel.UpdatePendingDocuments().IsCompleted()) {
        viewModel.UpdatePendingDocuments().IsCompleted(true);
    }
    else {
        viewModel.UpdatePendingDocuments().IsCompleted(false);
    }
}

function GetDataAttach() {

    // declare options variable for ajax get request
    var options = {
        url: api.server + api.url.transaction + "/Document/Attach/" + viewModel.TransactionModel().ID,
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetDataAttach, OnError, OnAlways);
}

function OnSuccessGetDataAttach(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        viewModel.Documents([]);
        for (var i = 0; i < viewModel.NewDocuments().length; i++) {
            data.push(viewModel.NewDocuments()[i]);
        }

        if (viewModel.fxTransaction() == 'Yes') {
            for (var i = 0; i < data.length; i++) {
                if (data[i].Purpose.ID != '2') {
                    viewModel.Documents.push(data[i]);
                }
            }
        }
        else {
            viewModel.Documents(data);
        }

    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
    }
}

function OnSuccessGetParameters(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        // bind result to observable array
        //self.Products(ko.toJS(data.Product));
        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };
        viewModel.Parameter().LLDs(ko.mapping.toJS(data.LLD, mapping));
        viewModel.Parameter().UnderlyingDocs(ko.mapping.toJS(data.UnderltyingDoc, mapping));

        viewModel.Parameter().Products(ko.mapping.toJS(data.Product, mapping));
        viewModel.Parameter().Channels(ko.mapping.toJS(data.Channel, mapping));
        viewModel.Parameter().Currencies(ko.mapping.toJS(data.Currency, mapping));
        viewModel.Parameter().BizSegments(ko.mapping.toJS(data.BizSegment, mapping));
        viewModel.Parameter().Banks(ko.mapping.toJS(data.Bank, mapping));

        viewModel.Parameter().FXCompliances(ko.mapping.toJS(data.FXCompliance, mapping));
        viewModel.Parameter().BankCharges(ko.mapping.toJS(data.ChargesType, mapping));
        viewModel.Parameter().AgentCharges(ko.mapping.toJS(data.ChargesType, mapping));
        viewModel.Parameter().DocumentTypes(ko.mapping.toJS(data.DocType, mapping));
        viewModel.Parameter().DocumentPurposes(ko.mapping.toJS(data.PurposeDoc, mapping));
        viewModel.Parameter().ProductType(ko.mapping.toJS(data.ProductType, mapping));

        // underlying parameter
        viewModel.GetUnderlyingParameters(data);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

// Token validation On Success function
function OnSuccessToken(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // read spuser from cookie
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);

        viewModel.SPUser(ko.mapping.toJS(spUser));
    }

    // get parameter values
    GetParameters();
}

function OnSuccessUploadPending(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        var options = {
            url: api.server + api.url.transaction + "/Document/Insert/" + viewModel.TransactionModel().ID,
            token: accessToken,
            data: ko.toJSON(viewModel.UpdatePendingDocuments())
        };

        // Save to api
        Helper.Ajax.Put(options, OnSuccessSavePending, OnError, OnAlways);
    } else {
        viewModel.IsEditable(false);
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
    }
}

function OnSuccessSavePending(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        if (viewModel.TransactionModel().Documents.length > 0 && viewModel.UpdatePendingDocuments().IsCompleted()) {
            viewModel.AddListItem();
        }
        viewModel.NewUploadDocuments([]);
        viewModel.PendingDocumentsClearFilters();
        viewModel.ProformaClearFilters();
        viewModel.UnderlyingClearFilters();

        viewModel.NewDocuments([]);
        viewModel.Documents([]);
        viewModel.DocumentToDelete([]);

        ShowNotification("Success", JSON.stringify(data.Message), "gritter-success", true);
        window.location = "/pending-documents";


        //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);

    } else {
        viewModel.IsEditable(false);
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
    }
}

// AJAX On Error Callback
function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

// AJAX On Alway Callback
function OnAlways() {
    // $box.trigger('reloaded.ace.widget');
    // enabling form input controls
}
