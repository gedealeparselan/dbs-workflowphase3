﻿
using DBS.Entity;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;

namespace DBS.WebAPI.Models
{
    #region Model
    public class TransactionUTINModel
    {
        /// <summary>
        /// Workflow Instance ID
        /// </summary>
        public Guid? WorkflowInstanceID { get; set; }

        /// <summary>
        /// Transaction ID.
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Application ID
        /// </summary>
        public string ApplicationID { get; set; }

        /// <summary>
        /// Top Urgent
        /// </summary>
        public int? IsTopUrgent { get; set; }
        /// <summary>
        /// Top Urgent
        /// </summary>
        public int? IsTopUrgentChain { get; set; }
        /// <summary>
        /// Customer Data Details
        /// </summary>
        public CustomerModel Customer { get; set; }

        /// <summary>
        /// New Customer
        /// </summary>
        public bool IsNewCustomer { get; set; }

        /// <summary>
        /// Product Details
        /// </summary>
        public ProductModel Product { get; set; }

        /// <summary>
        /// Product Type Details
        /// </summary>
        public ProductTypeModel ProductType { get; set; }

        /// <summary>
        /// LLD Details
        /// </summary>
        public LLDModel LLD { get; set; }

        /// <summary>
        /// LLD Details
        /// </summary>
        public UnderlyingDocModel UnderlyingDoc { get; set; }
        /// <summary>
        /// Other Underlying
        /// </summary>
        public string OtherUnderlyingDoc { get; set; }

        /// <summary>
        /// Currency Details
        /// </summary>
        public CurrencyModel Currency { get; set; }

        /// <summary>
        /// Transaction Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Rate
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Eqv.USD
        /// </summary>
        public decimal AmountUSD { get; set; }

        /// <summary>
        /// Channel Details
        /// </summary>
        public ChannelModel Channel { get; set; }

        /// <summary>
        /// Debit Acc Number
        /// </summary>
        public CustomerAccountModel Account { get; set; }

        /// <summary>
        /// Application Date
        /// </summary>
        public DateTime ApplicationDate { get; set; }

        /// <summary>
        /// Bene Segment Details
        /// </summary>
        public BizSegmentModel BizSegment { get; set; }

        /// <summary>
        /// Bank Details
        /// </summary>
        public BankModel Bank { get; set; }

        /// <summary>
        /// Bank Charges
        /// </summary>
        public ChargesTypeModel BankCharges { get; set; }

        /// <summary>
        /// Agent Charges
        /// </summary>
        public ChargesTypeModel AgentCharges { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        public decimal? Type { get; set; }

        /// <summary>
        /// Is Citizen
        /// </summary>
        public bool IsCitizen { get; set; }

        /// <summary>
        /// Is Resident
        /// </summary>
        public bool IsResident { get; set; }


        /// <summary>
        /// Signature Verification
        /// </summary>
        public bool IsSignatureVerified { get; set; }

        /// <summary>
        /// Dormant Account
        /// </summary>
        public bool IsDormantAccount { get; set; }

        /// <summary>
        /// Freeze Account
        /// </summary>
        public bool IsFreezeAccount { get; set; }

        /// basri 30-09-2015
        /// <summary>
        /// Callback Required
        /// </summary>
        public bool IsCallbackRequired { get; set; }

        /// <summary>
        /// Letter of Indemnity Available
        /// </summary>
        public bool IsLOIAvailable { get; set; }
        ///end basri


        /// <summary>
        /// Others
        /// </summary>
        public string Others { get; set; }

        /// <summary>
        /// Is Draft
        /// </summary>
        public bool IsDraft { get; set; }

        /// <summary>
        /// Create Date
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// List Transaction Document Details
        /// </summary>
        public IList<TransactionDocumentModel> Documents { get; set; }

        /// <summary>
        /// Purpose detail of compliance
        /// </summary>
        public string DetailCompliance { get; set; }

        /// <summary>
        /// Bene Name
        /// </summary>
        public string BeneName { get; set; }

        /// <summary>
        /// Bene Name
        /// </summary>
        public string BeneAccNumber { get; set; }

        /// <summary>
        /// TZ Number
        /// </summary>
        public string TZNumber { get; set; }

        public string PaymentDetails { get; set; }

        /// <summary>
        /// Underlyings
        /// </summary> //add chandra
        public IList<TransUnderlyingModel> Underlyings { get; set; }

        /// <summary>
        /// Transaction Create By
        /// </summary> //add reizvan
        public string CreateBy { get; set; }

        /// <summary>
        /// Transaction Is Other Bene Bank
        /// </summary> //add reizvan
        public bool IsOtherBeneBank { get; set; }

        /// <summary>
        /// Transaction Other Bene Bank Name
        /// </summary> //add reizvan
        public string OtherBeneBankName { get; set; }

        /// <summary>
        /// Transaction Other Bene Bank Swift
        /// </summary> //add reizvan
        public string OtherBeneBankSwift { get; set; }


        public CustomerDraftModel CustomerDraft { get; set; }
        public bool IsDocumentComplete { get; set; }

        //approver ID
        public long? ApproverID { get; set; }
        public bool? IsOtherAccountNumber { get; set; }
        public string OtherAccountNumber { get; set; }

        public CurrencyModel DebitCurrency { get; set; }

        /// <summary>
        /// List Transaction All Document reference to underlying by cif
        /// </summary>
        public IList<ReviseMakerDocumentModel> ReviseMakerDocuments { get; set; }

        //Agung Suhendar
        public FNACoreModel FNACore { get; set; }
        public bool FNACore2 { get; set; }
        public FunctionTypeModel FunctionType { get; set; }
        public AccountTypeModel AccountType { get; set; }
        public string SolID { get; set; }
        public DateTime? CustomerRiskEffectiveDate { get; set; }
        public int? RiskScore { get; set; }
        public DateTime? RiskProfileExpiryDate { get; set; }
        public string Investment { get; set; }
        public string OperativeAccount { get; set; }
        public IList<JoinAccountModel> UTJoin { get; set; }
        public bool IsBringupTask { get; set; }
        public string AttachmentRemarks { get; set; }

    }
    public class JoinAccountModel
    {
        public int? JoinOrder { get; set; }
        public JoinModel Join { get; set; }
        public CustomerModel CustomerJoin { get; set; }
        public DateTime? CustomerRiskEffectiveDateJoin { get; set; }
        public int? RiskScoreJoin { get; set; }
        public DateTime? RiskProfileExpiryDateJoin { get; set; }
        public string SolIDJoin { get; set; }
    }

    public class JoinModel
    {
        public bool? ID { get; set; }
        public string Name { get; set; }
    }
    public class TransactionUTSPModel
    {
        /// <summary>
        /// Workflow Instance ID
        /// </summary>
        public Guid? WorkflowInstanceID { get; set; }

        /// <summary>
        /// Transaction ID.
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Application ID
        /// </summary>
        public string ApplicationID { get; set; }

        /// <summary>
        /// Top Urgent
        /// </summary>
        public int? IsTopUrgent { get; set; }
        /// <summary>
        /// Top Urgent
        /// </summary>
        public int? IsTopUrgentChain { get; set; }
        /// <summary>
        /// Customer Data Details
        /// </summary>
        public CustomerModel Customer { get; set; }

        /// <summary>
        /// New Customer
        /// </summary>
        public bool IsNewCustomer { get; set; }

        /// <summary>
        /// Product Details
        /// </summary>
        public ProductModel Product { get; set; }

        /// <summary>
        /// Product Type Details
        /// </summary>
        public ProductTypeModel ProductType { get; set; }

        /// <summary>
        /// LLD Details
        /// </summary>
        public LLDModel LLD { get; set; }

        /// <summary>
        /// LLD Details
        /// </summary>
        public UnderlyingDocModel UnderlyingDoc { get; set; }
        /// <summary>
        /// Other Underlying
        /// </summary>
        public string OtherUnderlyingDoc { get; set; }

        /// <summary>
        /// Currency Details
        /// </summary>
        public CurrencyModel Currency { get; set; }

        /// <summary>
        /// Transaction Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Rate
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Eqv.USD
        /// </summary>
        public decimal AmountUSD { get; set; }

        /// <summary>
        /// Channel Details
        /// </summary>
        public ChannelModel Channel { get; set; }

        /// <summary>
        /// Debit Acc Number
        /// </summary>
        public CustomerAccountModel Account { get; set; }

        /// <summary>
        /// Application Date
        /// </summary>
        public DateTime ApplicationDate { get; set; }

        /// <summary>
        /// Bene Segment Details
        /// </summary>
        public BizSegmentModel BizSegment { get; set; }

        /// <summary>
        /// Bank Details
        /// </summary>
        public BankModel Bank { get; set; }

        /// <summary>
        /// Bank Charges
        /// </summary>
        public ChargesTypeModel BankCharges { get; set; }

        /// <summary>
        /// Agent Charges
        /// </summary>
        public ChargesTypeModel AgentCharges { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        public decimal? Type { get; set; }

        /// <summary>
        /// Is Citizen
        /// </summary>
        public bool IsCitizen { get; set; }

        /// <summary>
        /// Is Resident
        /// </summary>
        public bool IsResident { get; set; }


        /// <summary>
        /// Signature Verification
        /// </summary>
        public bool IsSignatureVerified { get; set; }

        /// <summary>
        /// Dormant Account
        /// </summary>
        public bool IsDormantAccount { get; set; }

        /// <summary>
        /// Freeze Account
        /// </summary>
        public bool IsFreezeAccount { get; set; }

        /// basri 30-09-2015
        /// <summary>
        /// Callback Required
        /// </summary>
        public bool IsCallbackRequired { get; set; }

        /// <summary>
        /// Letter of Indemnity Available
        /// </summary>
        public bool IsLOIAvailable { get; set; }
        ///end basri


        /// <summary>
        /// Others
        /// </summary>
        public string Others { get; set; }

        /// <summary>
        /// Is Draft
        /// </summary>
        public bool IsDraft { get; set; }

        /// <summary>
        /// Create Date
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// List Transaction Document Details
        /// </summary>
        public IList<TransactionDocumentModel> Documents { get; set; }

        /// <summary>
        /// MutualFund Details
        /// </summary>
        public IList<TransactitonMutualFundModel> MutualFundForms { get; set; }

        /// <summary>
        /// Purpose detail of compliance
        /// </summary>
        public string DetailCompliance { get; set; }

        /// <summary>
        /// Bene Name
        /// </summary>
        public string BeneName { get; set; }

        /// <summary>
        /// Bene Name
        /// </summary>
        public string BeneAccNumber { get; set; }

        /// <summary>
        /// TZ Number
        /// </summary>
        public string TZNumber { get; set; }

        public string PaymentDetails { get; set; }

        /// <summary>
        /// Underlyings
        /// </summary> //add chandra
        public IList<TransUnderlyingModel> Underlyings { get; set; }

        /// <summary>
        /// Transaction Create By
        /// </summary> //add reizvan
        public string CreateBy { get; set; }

        /// <summary>
        /// Transaction Is Other Bene Bank
        /// </summary> //add reizvan
        public bool IsOtherBeneBank { get; set; }

        /// <summary>
        /// Transaction Other Bene Bank Name
        /// </summary> //add reizvan
        public string OtherBeneBankName { get; set; }

        /// <summary>
        /// Transaction Other Bene Bank Swift
        /// </summary> //add reizvan
        public string OtherBeneBankSwift { get; set; }


        public CustomerDraftModel CustomerDraft { get; set; }
        public bool IsDocumentComplete { get; set; }

        //approver ID
        public long? ApproverID { get; set; }
        public bool? IsOtherAccountNumber { get; set; }
        public string OtherAccountNumber { get; set; }

        public CurrencyModel DebitCurrency { get; set; }

        /// <summary>
        /// List Transaction All Document reference to underlying by cif
        /// </summary>
        public IList<ReviseMakerDocumentModel> ReviseMakerDocuments { get; set; }

        //Agung Suhendar
        public Transaction_TypeModel Transaction_Type { get; set; }
        public string Investment { get; set; }
        public string Remarks { get; set; }
        //Agung Suhendar
        public bool IsBringupTask { get; set; }
        //add by adi
        public string AttachmentRemarks { get; set; }
        //end
    }
    public class TransactitonMutualFundModel
    {
        public CBOFundModel MutualFundList { get; set; }
        public CBOFundModel MutualFundSwitchFrom { get; set; }
        public CBOFundModel MutualFundSwitchTo { get; set; }
        public CurrencyModel MutualCurrency { get; set; }
        public decimal? MutualAmount { get; set; }
        public bool? MutualPartial { get; set; }
        public bool? MutualSelected { get; set; }
        public decimal? MutualUnitNumber { get; set; }
    }
    public class FNACoreModel
    {
        public int? ID { get; set; }
        public string Name { get; set; }
    }
    public class FunctionTypeModel
    {
        public int? ID { get; set; }
        public string Name { get; set; }
    }
    public class AccountTypeModel
    {
        public int? ID { get; set; }
        public string Name { get; set; }
    }
    public class Transaction_TypeModel
    {
        public int? TransTypeID { get; set; }
        public string TransactionTypeName { get; set; }
    }
    public class InvestmentIDModel
    {
        public int InvestmentID { get; set; }
        public string CIF { get; set; }
        public string Investment { get; set; }
        public Nullable<bool> IsUT { get; set; }
        public Nullable<bool> IsBond { get; set; }
        public string STNumber { get; set; }
        public string CheckIN { get; set; }
        public string CheckCIF { get; set; }
        public string CheckName { get; set; }
        public string CheckST { get; set; }
    }
    public class InvestmentIDGenerateModel
    {
        public long ID { get; set; }
        public bool FNACore { get; set; }
        public int AccountType { get; set; }
        public string SolID { get; set; }
        public DateTime? CustomerRiskEffectiveDate { get; set; }
        public int? RiskScore { get; set; }
        public DateTime? RiskProfileExpiryDate { get; set; }
        public string OperativeAccount { get; set; }
        public IList<JoinAccountModel> UTJoin { get; set; }
    }
    public class InvestmentIDSPGenerateModel
    {
        public long ID { get; set; }
        public IList<TransactitonMutualFundModel> MutualFundForms { get; set; }
    }
    public class TransactionUTINDraftModel
    {
        public string AttachmentRemarks { get; set; }
        public Guid? WorkflowInstanceID { get; set; }
        public long ID { get; set; }
        public string ApplicationID { get; set; }
        public int? IsTopUrgent { get; set; }
        public int? IsTopUrgentChain { get; set; }
        public CustomerModel Customer { get; set; }
        public bool? IsNewCustomer { get; set; }
        public ProductModel Product { get; set; }
        public CurrencyModel Currency { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public bool? IsDraft { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public IList<TransactionDocumentDraftModel> Documents { get; set; }
        public string CreateBy { get; set; }
        public bool? IsDocumentComplete { get; set; }
        public long? ApproverID { get; set; }
        public CurrencyModel DebitCurrency { get; set; }
        public FNACoreModel FNACore { get; set; }
        public bool? FNACore2 { get; set; }
        public FunctionTypeModel FunctionType { get; set; }
        public AccountTypeModel AccountType { get; set; }
        public string SolID { get; set; }
        public DateTime? CustomerRiskEffectiveDate { get; set; }
        public int? RiskScore { get; set; }
        public DateTime? RiskProfileExpiryDate { get; set; }
        public string Investment { get; set; }
        public string OperativeAccount { get; set; }
        public string DraftCustomerName { get; set; }
        public string DraftCIF { get; set; }
        public IList<JoinAccountModel> UTJoin { get; set; }
    }
    public class TransactionUTSPDraftModel
    {
        public Guid? WorkflowInstanceID { get; set; }
        public long ID { get; set; }
        public string ApplicationID { get; set; }
        public int? IsTopUrgent { get; set; }
        public int? IsTopUrgentChain { get; set; }
        public CustomerModel Customer { get; set; }
        public bool? IsNewCustomer { get; set; }
        public ProductModel Product { get; set; }
        public CurrencyModel Currency { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public bool? IsDraft { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public IList<TransactionDocumentDraftModel> Documents { get; set; }
        public string CreateBy { get; set; }
        public bool? IsDocumentComplete { get; set; }
        public long? ApproverID { get; set; }
        public CurrencyModel DebitCurrency { get; set; }
        public string SolID { get; set; }
        public string Investment { get; set; }
        public string Remarks { get; set; }
        public Transaction_TypeModel Transaction_Type { get; set; }
        public IList<TransactitonMutualFundModel> MutualFundForms { get; set; }
        public string AttachmentRemarks { get; set; }
    }
    #endregion

    #region Interface
    public interface INewTransactionUTRepository : IDisposable
    {
        bool AddTransactionUTIN(TransactionUTINModel transaction, ref long transactionID, ref string ApplicationID, ref string message);
        bool AddTransactionUTSP(TransactionUTSPModel transaction, ref long transactionID, ref string ApplicationID, ref string message);
        bool GetInvestmentID(string key, string cif, ref IList<InvestmentIDModel> investID, ref string Message);
        bool PopulateInvestmentID(string investmentID, ref InvestmentIDGenerateModel bindData, ref string Message);
        bool PopulateInvestmentIDMutual(string investmentID, ref IList<TransactitonMutualFundModel> bindData, ref string Message);
        bool GetTransactionDraftINByID(long id, ref TransactionUTINDraftModel transaction, ref string message);
        bool GetTransactionDraftSPByID(long id, ref TransactionUTSPDraftModel transaction, ref string message);
        bool DeleteTransactionDraftByID(int id, ref string Message);
        bool UpdateTransactionDraftIN(long id, TransactionUTINModel transaction, ref string message);
        bool UpdateTransactionDraftSP(long id, TransactionUTSPModel transaction, ref string message);
    }
    #endregion

    #region Repository
    public class NewTransactionUTRepository : INewTransactionUTRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool AddTransactionUTIN(TransactionUTINModel transaction, ref long transactionID, ref string ApplicationID, ref string message)
        {
            bool IsSuccess = false;
            long TrIDInserted = 0;
            using (DBSEntities context_ts = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        if (transaction.IsDraft)
                        {
                            if (transaction.ID != null && transaction.ID > 0)
                            {
                                Entity.TransactionDraft data = context.TransactionDrafts.Where(a => a.TransactionID.Equals(transaction.ID)).SingleOrDefault();
                                if (data != null)
                                {
                                    data.ApplicationDate = DateTime.UtcNow;
                                    data.CIF = transaction.Customer.CIF;
                                    data.ProductID = transaction.Product.ID;
                                    data.IsDraft = true;
                                    data.ApplicationID = string.Empty;
                                    data.StateID = (int)StateID.OnProgress;
                                    data.CreateDate = DateTime.UtcNow;
                                    data.CreateBy = currentUser.GetCurrentUser().LoginName;
                                    data.Amount = 0;
                                    data.CurrencyID = (int)CurrencyID.NullCurrency;
                                    data.IsFNACore = transaction.FNACore.Name.ToLower() == "yes" ? true : false;
                                    data.FunctionType = transaction.FunctionType.ID;
                                    data.AccountType = transaction.AccountType.ID;
                                    data.SolID = transaction.SolID;
                                    data.CRiskEfectiveDate = transaction.CustomerRiskEffectiveDate;
                                    data.RiskScore = transaction.RiskScore;
                                    data.RiskProfileExpiryDate = transaction.RiskProfileExpiryDate;
                                    data.OperativeAccount = transaction.OperativeAccount;
                                    data.IsNewCustomer = false;
                                    data.InvestmentID = transaction.Investment;
                                    data.AttachmentRemarks = transaction.AttachmentRemarks;
                                    context.SaveChanges();
                                    #region Save Document
                                    var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(transaction.ID)).ToList();
                                    if (existingDocs != null)
                                    {
                                        if (existingDocs.Count() > 0)
                                        {
                                            foreach (var item in existingDocs)
                                            {
                                                var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                                deleteDoc.IsDeleted = true;
                                                context.SaveChanges();
                                            }
                                        }
                                    }
                                    if (transaction.Documents.Count > 0)
                                    {
                                        foreach (var item in transaction.Documents)
                                        {
                                            Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                                            {
                                                TransactionID = data.TransactionID,
                                                DocTypeID = item.Type.ID,
                                                PurposeID = item.Purpose.ID,
                                                Filename = item.FileName,
                                                DocumentPath = item.DocumentPath,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName
                                            };
                                            context_ts.TransactionDocumentDrafts.Add(document);
                                        }
                                        context_ts.SaveChanges();
                                    }

                                    #endregion
                                    #region Join Account
                                    if (transaction.UTJoin != null)
                                    {
                                        if (transaction.UTJoin.Count > 0)
                                        {
                                            int i = 2;
                                            foreach (var item in transaction.UTJoin)
                                            {
                                                Entity.JoinAccountDraft join = new Entity.JoinAccountDraft()
                                                {
                                                    TransactionID = data.TransactionID,
                                                    JoinOrder = i,
                                                    JoinType = item.Join.ID.Value,
                                                    RiskEffectiveDate = item.CustomerRiskEffectiveDateJoin,
                                                    RiskProfileExpiryDate = item.RiskProfileExpiryDateJoin,
                                                    RiskScore = item.RiskScoreJoin,
                                                    SolID = item.SolIDJoin,
                                                    CIF = item.CustomerJoin.CIF,
                                                    CreateDate = DateTime.UtcNow,
                                                    CreateBy = currentUser.GetCurrentUser().LoginName,
                                                };
                                                context_ts.JoinAccountDrafts.Add(join);
                                                i++;
                                            }
                                            context_ts.SaveChanges();
                                        }
                                    }

                                    #endregion
                                }
                                ts.Complete();
                                IsSuccess = true;
                            }
                            else
                            {
                                #region Save As Draft
                                bool isBringup = false;
                                if (transaction.IsBringupTask == true)
                                    isBringup = true;
                                DBS.Entity.TransactionDraft data = new DBS.Entity.TransactionDraft();
                                if (data != null)
                                {
                                    #region Unused field but Not Null
                                    data.BeneName = "-";
                                    data.BankID = 1;
                                    data.IsResident = false;
                                    data.IsCitizen = false;
                                    data.AmountUSD = 0.00M;
                                    data.Rate = 0.00M;
                                    data.DebitCurrencyID = 1;
                                    data.BizSegmentID = 1;
                                    data.ChannelID = 1;
                                    #endregion

                                    #region Primary Field
                                    data.IsOtherAccountNumber = transaction.IsOtherAccountNumber;
                                    data.IsNewCustomer = transaction.IsNewCustomer;
                                    if (!transaction.IsNewCustomer)
                                    {
                                        data.CIF = transaction.Customer.CIF;

                                        if (transaction.Currency != null)
                                        {
                                            data.CurrencyID = transaction.Currency.ID;
                                            data.AccountNumber = transaction.Account.AccountNumber;
                                        }
                                    }
                                    else
                                    {
                                        data.DraftCIF = transaction.Customer.CIF;
                                        data.DraftAccountNumber = transaction.CustomerDraft.DraftAccountNumber.AccountNumber;
                                        data.DraftCustomerName = transaction.Customer.Name;
                                        data.DraftCurrencyID = transaction.CustomerDraft.DraftAccountNumber.Currency.ID;
                                        if (transaction.DebitCurrency != null)
                                        {
                                            data.DraftDebitCurrencyID = transaction.DebitCurrency.ID;
                                        }
                                        if (transaction.Currency != null)
                                        {
                                            data.CurrencyID = transaction.Currency.ID;
                                        }
                                    }
                                    data.ApplicationDate = DateTime.UtcNow;
                                    data.CIF = transaction.Customer.CIF;
                                    data.ProductID = transaction.Product.ID;
                                    data.IsDraft = true;
                                    data.ApplicationID = string.Empty;
                                    data.StateID = (int)StateID.OnProgress;
                                    data.CreateDate = DateTime.UtcNow;
                                    data.CreateBy = currentUser.GetCurrentUser().LoginName;
                                    data.Amount = 0;
                                    data.CurrencyID = (int)CurrencyID.NullCurrency;
                                    if (transaction.FNACore != null)
                                    {
                                        data.IsFNACore = transaction.FNACore.Name.ToLower() == "yes" ? true : false;
                                    }
                                    else
                                    {
                                        data.IsFNACore = null;
                                    }
                                    data.FunctionType = transaction.FunctionType.ID;
                                    if (transaction.AccountType != null)
                                    {
                                        data.AccountType = transaction.AccountType.ID;
                                    }
                                    else
                                    {
                                        data.AccountType = null;
                                    }
                                    data.SolID = transaction.SolID;
                                    data.CRiskEfectiveDate = transaction.CustomerRiskEffectiveDate;
                                    data.RiskScore = transaction.RiskScore;
                                    data.RiskProfileExpiryDate = transaction.RiskProfileExpiryDate;
                                    data.OperativeAccount = transaction.OperativeAccount;
                                    data.IsNewCustomer = transaction.IsNewCustomer;
                                    data.InvestmentID = transaction.Investment;
                                    data.AttachmentRemarks = transaction.AttachmentRemarks;
                                    data.IsBringUp = isBringup;
                                    #endregion
                                }
                                context_ts.TransactionDrafts.Add(data);
                                context_ts.SaveChanges();

                                transactionID = data.TransactionID;
                                TrIDInserted = transactionID;
                                #region Save Document
                                if (transaction.Documents.Count > 0)
                                {
                                    foreach (var item in transaction.Documents)
                                    {
                                        Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                                        {
                                            TransactionID = data.TransactionID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context_ts.TransactionDocumentDrafts.Add(document);
                                    }
                                    context_ts.SaveChanges();
                                }
                                #endregion
                                #region Join Account
                                if (transaction.UTJoin != null)
                                {
                                    if (transaction.UTJoin.Count > 0)
                                    {
                                        int i = 2;
                                        foreach (var item in transaction.UTJoin)
                                        {
                                            Entity.JoinAccountDraft join = new Entity.JoinAccountDraft()
                                            {
                                                TransactionID = data.TransactionID,
                                                JoinOrder = i,
                                                JoinType = item.Join.ID.Value,
                                                RiskEffectiveDate = item.CustomerRiskEffectiveDateJoin,
                                                RiskProfileExpiryDate = item.RiskProfileExpiryDateJoin,
                                                RiskScore = item.RiskScoreJoin,
                                                SolID = item.SolIDJoin,
                                                CIF = item.CustomerJoin.CIF,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName,
                                            };
                                            context_ts.JoinAccountDrafts.Add(join);
                                            i++;
                                        }
                                        context_ts.SaveChanges();
                                    }
                                }

                                #endregion
                                #region BringUp
                                if (transaction.IsBringupTask == true)
                                {
                                    Entity.TransactionBringupTask Bringup = new Entity.TransactionBringupTask()
                                    {
                                        TransactionID = TrIDInserted,
                                        TaskName = "Bring Up UT",
                                        AssignedTo = currentUser.GetCurrentUser().LoginName,
                                        AssignedFrom = currentUser.GetCurrentUser().LoginName,
                                        DateIn = DateTime.UtcNow,
                                        IsSubmitted = false,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName,
                                    };
                                    context_ts.TransactionBringupTasks.Add(Bringup);
                                    context_ts.SaveChanges();
                                }
                                #endregion
                                ts.Complete();
                                IsSuccess = true;
                                #endregion
                            }
                        }
                        else
                        {
                            #region New Customer
                            if (transaction.IsNewCustomer)
                            {
                                context_ts.Customers.Add(new Customer()
                                {
                                    CIF = transaction.Customer.CIF,
                                    CustomerName = transaction.Customer.Name,
                                    BizSegmentID = 1,
                                    LocationID = 1,
                                    CustomerTypeID = 1,
                                    SourceID = 1,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });
                                context_ts.SaveChanges();
                            }
                            #endregion

                            #region Submit New Transaction
                            DBS.Entity.Transaction data = new DBS.Entity.Transaction()
                            {
                                #region Unused field but Not Null
                                IsSignatureVerified = false,
                                IsDormantAccount = false,
                                IsFrezeAccount = false,
                                BeneName = "-",
                                BankID = 1,
                                IsResident = false,
                                IsCitizen = false,
                                AmountUSD = 0.00M,
                                Rate = 0.00M,
                                DebitCurrencyID = 1,
                                BizSegmentID = 1,
                                ChannelID = 1,
                                #endregion
                                #region Primary Field
                                ApplicationDate = DateTime.UtcNow,
                                IsTopUrgent = 0,
                                IsNewCustomer = false,
                                CIF = transaction.Customer.CIF,
                                ProductID = transaction.Product.ID,
                                IsDocumentComplete = false,
                                IsDraft = false,
                                ApplicationID = "-",
                                StateID = (int)StateID.OnProgress,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName,
                                Amount = 0,
                                CurrencyID = (int)CurrencyID.NullCurrency,
                                #endregion
                            };

                            if (transaction.FNACore != null)
                            {
                                data.IsFNACore = transaction.FNACore.Name.ToLower() == "yes" ? true : false;
                            }
                            if (transaction.FunctionType != null)
                            {
                                data.FunctionType = transaction.FunctionType.ID;
                            }
                            if (transaction.AccountType != null)
                            {
                                data.AccountType = transaction.AccountType.ID;
                            }
                            if (transaction.SolID != null)
                            {
                                data.SolID = transaction.SolID;
                            }
                            if (transaction.CustomerRiskEffectiveDate != null)
                            {
                                data.CRiskEfectiveDate = transaction.CustomerRiskEffectiveDate;
                            }
                            if (transaction.RiskScore != null)
                            {
                                data.RiskScore = transaction.RiskScore;
                            }
                            if (transaction.RiskProfileExpiryDate != null)
                            {
                                data.RiskProfileExpiryDate = transaction.RiskProfileExpiryDate;
                            }
                            if (transaction.OperativeAccount != null)
                            {
                                data.OperativeAccount = transaction.OperativeAccount;
                            }
                            if (transaction.Investment != null)
                            {
                                data.InvestmentID = transaction.Investment;
                            }
                            if (transaction.CreateDate != null)
                            {
                                data.TouchTimeStartDate = transaction.CreateDate;
                            }

                            context_ts.Transactions.Add(data);
                            context_ts.SaveChanges();

                            transactionID = data.TransactionID;
                            TrIDInserted = transactionID;
                            #region Document
                            if (transaction.Documents != null)
                            {
                                if (transaction.Documents.Count > 0)
                                {
                                    foreach (var item in transaction.Documents)
                                    {
                                        Entity.TransactionDocument document = new Entity.TransactionDocument()
                                        {
                                            TransactionID = data.TransactionID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context_ts.TransactionDocuments.Add(document);
                                    }
                                    context_ts.SaveChanges();
                                }
                            }

                            #endregion

                            #region Join Account
                            if (transaction.UTJoin != null)
                            {
                                if (transaction.UTJoin.Count > 0)
                                {
                                    int i = 2;
                                    foreach (var item in transaction.UTJoin)
                                    {
                                        Entity.JoinAccount join = new Entity.JoinAccount()
                                        {
                                            TransactionID = data.TransactionID,
                                            JoinOrder = i,
                                            JoinType = item.Join.ID.Value,
                                            RiskEffectiveDate = item.CustomerRiskEffectiveDateJoin,
                                            RiskProfileExpiryDate = item.RiskProfileExpiryDateJoin,
                                            RiskScore = item.RiskScoreJoin,
                                            SolID = item.SolIDJoin,
                                            CIF = item.CustomerJoin.CIF,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName,
                                        };
                                        context_ts.JoinAccounts.Add(join);
                                        i++;
                                    }
                                    context_ts.SaveChanges();
                                }
                            }

                            #endregion

                            ts.Complete();
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        ts.Dispose();
                        message = ex.Message;
                    }
                }
            }
            if (!transaction.IsDraft)
            {
                using (DBSEntities context = new DBSEntities())
                {
                    var getTR = (from TR in context.Transactions
                                 where TR.TransactionID == TrIDInserted
                                 select TR).SingleOrDefault();
                    ApplicationID = getTR.ApplicationID;
                    IsSuccess = true;
                }
            }
            return IsSuccess;
        }
        public bool AddTransactionUTSP(TransactionUTSPModel transaction, ref long transactionID, ref string ApplicationID, ref string message)
        {
            bool IsSuccess = false;
            long TrIDInserted = 0;
            using (DBSEntities context_ts = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        if (transaction.IsDraft)
                        {
                            if (transaction.ID != null && transaction.ID > 0)
                            {
                                Entity.TransactionDraft data = context.TransactionDrafts.Where(a => a.TransactionID == transaction.ID).SingleOrDefault();
                                if (data != null)
                                {
                                    data.ApplicationDate = DateTime.UtcNow;
                                    data.CIF = transaction.Customer.CIF;
                                    data.ProductID = transaction.Product.ID;
                                    data.IsDraft = true;
                                    data.ApplicationID = string.Empty;
                                    data.StateID = (int)StateID.OnProgress;
                                    data.CreateDate = DateTime.UtcNow;
                                    data.CreateBy = currentUser.GetCurrentUser().LoginName;
                                    data.Amount = 0;
                                    data.CurrencyID = (int)CurrencyID.NullCurrency;
                                    data.TransactionTypeID = transaction.Transaction_Type.TransTypeID;
                                    data.InvestmentID = transaction.Investment;
                                    data.Remarks = transaction.Remarks;
                                    data.AttachmentRemarks = transaction.AttachmentRemarks;
                                    context.SaveChanges();
                                    #region Save Document
                                    var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(transaction.ID)).ToList();
                                    if (existingDocs != null)
                                    {
                                        if (existingDocs.Count() > 0)
                                        {
                                            foreach (var item in existingDocs)
                                            {
                                                var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                                deleteDoc.IsDeleted = true;
                                                context.SaveChanges();
                                            }
                                        }
                                    }
                                    if (transaction.Documents.Count > 0)
                                    {
                                        foreach (var item in transaction.Documents)
                                        {
                                            Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                                            {
                                                TransactionID = data.TransactionID,
                                                DocTypeID = item.Type.ID,
                                                PurposeID = item.Purpose.ID,
                                                Filename = item.FileName,
                                                DocumentPath = item.DocumentPath,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName
                                            };
                                            context_ts.TransactionDocumentDrafts.Add(document);
                                        }
                                        context_ts.SaveChanges();
                                    }

                                    #endregion
                                    #region Mutual
                                    if (transaction.MutualFundForms != null)
                                    {
                                        if (transaction.MutualFundForms.Count > 0)
                                        {

                                            #region Data ditambahkan lagi
                                            foreach (var item in transaction.MutualFundForms)
                                            {
                                                Entity.MutualFundTransactionDraft mFund = new Entity.MutualFundTransactionDraft();
                                                mFund.TransactionID = data.TransactionID;
                                                mFund.FundID = item.MutualFundList.ID;
                                                if (item.MutualCurrency != null)
                                                    mFund.CurrencyID = item.MutualCurrency.ID;
                                                if (item.MutualFundSwitchTo != null)
                                                    if (item.MutualFundSwitchTo.ID > 0)
                                                        mFund.SwitchToFundID = item.MutualFundSwitchTo.ID;
                                                mFund.Amount = item.MutualAmount ?? null;//item.MutualAmount == null ? null : item.MutualAmount;
                                                mFund.IsPartial = item.MutualPartial ?? false; // item.MutualPartial == null ? false : item.MutualPartial;
                                                mFund.NumberofUnit = item.MutualUnitNumber ?? null; //item.MutualUnitNumber == null ? null : item.MutualUnitNumber;
                                                mFund.CreateDate = DateTime.UtcNow;
                                                mFund.CreateBy = currentUser.GetCurrentUser().LoginName;
                                                context_ts.MutualFundTransactionDrafts.Add(mFund);
                                            }

                                            context_ts.SaveChanges();
                                            #endregion
                                        }
                                    }

                                    #endregion
                                }
                                ts.Complete();
                                IsSuccess = true;
                            }
                            else
                            {
                                #region New Customer
                                if (transaction.IsNewCustomer)
                                {
                                    context_ts.Customers.Add(new Customer()
                                    {
                                        CIF = transaction.Customer.CIF,
                                        CustomerName = transaction.Customer.Name,
                                        BizSegmentID = 1,
                                        LocationID = 1,
                                        CustomerTypeID = 1,
                                        SourceID = 1,
                                        IsDeleted = false,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName
                                    });
                                    context_ts.SaveChanges();
                                }
                                #endregion
                                #region Save As New Draft
                                bool isBringup = false;
                                if (transaction.IsBringupTask == true)
                                    isBringup = true;
                                DBS.Entity.TransactionDraft data = new DBS.Entity.TransactionDraft()
                                {
                                    #region Unused field but Not Null
                                    BeneName = "-",
                                    BankID = 1,
                                    IsResident = false,
                                    IsCitizen = false,
                                    AmountUSD = 0.00M,
                                    Rate = 0.00M,
                                    DebitCurrencyID = 1,
                                    BizSegmentID = 1,
                                    ChannelID = 1,
                                    #endregion
                                    #region Primary Field
                                    ApplicationDate = DateTime.UtcNow,
                                    CIF = transaction.Customer.CIF,
                                    ProductID = transaction.Product.ID,
                                    IsDraft = true,
                                    ApplicationID = string.Empty,
                                    StateID = (int)StateID.OnProgress,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName,
                                    Amount = 0,
                                    CurrencyID = (int)CurrencyID.NullCurrency,
                                    TransactionTypeID = transaction.Transaction_Type.TransTypeID,
                                    InvestmentID = transaction.Investment,
                                    Remarks = transaction.Remarks,
                                    AttachmentRemarks = transaction.AttachmentRemarks,//add by adi
                                    IsNewCustomer = false,
                                    IsBringUp = isBringup
                                    #endregion
                                };
                                context_ts.TransactionDrafts.Add(data);
                                context_ts.SaveChanges();

                                transactionID = data.TransactionID;
                                TrIDInserted = transactionID;
                                #region Save Document
                                if (transaction.Documents.Count > 0)
                                {
                                    foreach (var item in transaction.Documents)
                                    {
                                        Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                                        {
                                            TransactionID = data.TransactionID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context_ts.TransactionDocumentDrafts.Add(document);
                                    }
                                    context_ts.SaveChanges();
                                }
                                #endregion
                                #region Mutual
                                if (transaction.MutualFundForms != null)
                                {
                                    if (transaction.MutualFundForms.Count > 0)
                                    {
                                        foreach (var item in transaction.MutualFundForms)
                                        {
                                            Entity.MutualFundTransactionDraft mFund = new Entity.MutualFundTransactionDraft();
                                            mFund.TransactionID = data.TransactionID;
                                            mFund.FundID = item.MutualFundList.ID;
                                            if (item.MutualCurrency != null)
                                                mFund.CurrencyID = item.MutualCurrency.ID;
                                            if (item.MutualFundSwitchTo != null)
                                                if (item.MutualFundSwitchTo.ID > 0)
                                                    mFund.SwitchToFundID = item.MutualFundSwitchTo.ID;
                                            mFund.Amount = item.MutualAmount ?? null; //item.MutualAmount == null ? null : item.MutualAmount;
                                            mFund.IsPartial = item.MutualPartial ?? false;//item.MutualPartial == null ? false : item.MutualPartial;
                                            mFund.NumberofUnit = item.MutualUnitNumber ?? null;//item.MutualUnitNumber == null ? null : item.MutualUnitNumber;
                                            mFund.CreateDate = DateTime.UtcNow;
                                            mFund.CreateBy = currentUser.GetCurrentUser().LoginName;
                                            context_ts.MutualFundTransactionDrafts.Add(mFund);
                                        }

                                        context_ts.SaveChanges();
                                    }
                                }

                                #endregion
                                #region BringUp
                                if (transaction.IsBringupTask == true)
                                {
                                    Entity.TransactionBringupTask Bringup = new Entity.TransactionBringupTask()
                                    {
                                        TransactionID = TrIDInserted,
                                        TaskName = "Bring Up UT",
                                        AssignedTo = currentUser.GetCurrentUser().LoginName,
                                        AssignedFrom = currentUser.GetCurrentUser().LoginName,
                                        DateIn = DateTime.UtcNow,
                                        IsSubmitted = false,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName,
                                    };
                                    context_ts.TransactionBringupTasks.Add(Bringup);
                                    context_ts.SaveChanges();
                                }
                                #endregion
                                ts.Complete();
                                IsSuccess = true;
                                #endregion
                            }
                        }
                        else
                        {

                            #region New Customer
                            if (transaction.IsNewCustomer)
                            {
                                context_ts.Customers.Add(new Customer()
                                {
                                    CIF = transaction.Customer.CIF,
                                    CustomerName = transaction.Customer.Name,
                                    BizSegmentID = 1,
                                    LocationID = 1,
                                    CustomerTypeID = 1,
                                    SourceID = 1,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });
                                context_ts.SaveChanges();
                            }
                            #endregion

                            #region Submit New Transaction
                            DBS.Entity.Transaction data = new DBS.Entity.Transaction();
                            if (data != null)
                            {
                                data.IsSignatureVerified = false;
                                data.IsDormantAccount = false;
                                data.IsFrezeAccount = false;
                                data.BeneName = "-";
                                data.BankID = 1;
                                data.IsResident = false;
                                data.IsCitizen = false;
                                data.AmountUSD = 0.00M;
                                data.Rate = 0.00M;
                                data.DebitCurrencyID = 1;
                                data.BizSegmentID = 1;
                                data.ChannelID = 1;
                                data.ApplicationDate = DateTime.UtcNow;
                                data.IsTopUrgent = 0;
                                data.IsNewCustomer = false;
                                data.CIF = transaction.Customer.CIF;
                                data.ProductID = transaction.Product.ID;
                                data.IsDocumentComplete = false;
                                data.IsDraft = false;
                                data.ApplicationID = "-";
                                data.StateID = (int)StateID.OnProgress;
                                data.CreateDate = DateTime.UtcNow;
                                data.CreateBy = currentUser.GetCurrentUser().LoginName;
                                data.Amount = 0;
                                data.CurrencyID = (int)CurrencyID.NullCurrency;
                                data.TransactionTypeID = transaction.Transaction_Type.TransTypeID;
                                data.InvestmentID = transaction.Investment;
                                data.Remarks = transaction.Remarks;
                                data.TouchTimeStartDate = transaction.CreateDate;
                                data.AttachmentRemarks = transaction.AttachmentRemarks;
                            }
                            context_ts.Transactions.Add(data);
                            context_ts.SaveChanges();

                            transactionID = data.TransactionID;
                            TrIDInserted = transactionID;
                            #region Document
                            if (transaction.Documents != null)
                            {
                                if (transaction.Documents.Count > 0)
                                {
                                    foreach (var item in transaction.Documents)
                                    {
                                        Entity.TransactionDocument document = new Entity.TransactionDocument()
                                        {
                                            TransactionID = data.TransactionID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context_ts.TransactionDocuments.Add(document);
                                    }
                                    context_ts.SaveChanges();
                                }
                            }

                            #endregion

                            #region Mutual
                            if (transaction.MutualFundForms != null)
                            {
                                if (transaction.MutualFundForms.Count > 0)
                                {
                                    foreach (var item in transaction.MutualFundForms)
                                    {
                                        Entity.MutualFundTransaction mFund = new Entity.MutualFundTransaction();
                                        mFund.TransactionID = data.TransactionID;
                                        mFund.FundID = item.MutualFundList.ID;
                                        if (item.MutualCurrency != null)
                                            mFund.CurrencyID = item.MutualCurrency.ID;
                                        if (item.MutualFundSwitchTo != null)
                                            if (item.MutualFundSwitchTo.ID > 0)
                                                mFund.SwitchToFundID = item.MutualFundSwitchTo.ID;
                                        mFund.Amount = item.MutualAmount == null ? null : item.MutualAmount;
                                        mFund.IsPartial = item.MutualPartial == null ? false : item.MutualPartial;
                                        mFund.NumberofUnit = item.MutualUnitNumber == null ? null : item.MutualUnitNumber;
                                        mFund.CreateDate = DateTime.UtcNow;
                                        mFund.CreateBy = currentUser.GetCurrentUser().LoginName;
                                        context_ts.MutualFundTransactions.Add(mFund);
                                    }

                                    context_ts.SaveChanges();
                                }
                            }

                            #endregion

                            ts.Complete();
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        ts.Dispose();
                        message = ex.Message;
                    }
                }
            }
            if (!transaction.IsDraft)
            {
                using (DBSEntities context = new DBSEntities())
                {
                    var getTR = (from TR in context.Transactions
                                 where TR.TransactionID == TrIDInserted
                                 select TR).SingleOrDefault();
                    ApplicationID = getTR.ApplicationID;
                    IsSuccess = true;
                }
            }
            return IsSuccess;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);

            GC.Collect();
            GC.SuppressFinalize(this);
        }
        public bool GetInvestmentID(string key, string cif, ref IList<InvestmentIDModel> investID, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities cont = new DBSEntities())
                {
                    IList<InvestmentIDModel> investIDTemp = new List<InvestmentIDModel>();
                    investIDTemp = (from inv in cont.InvestmentIDs
                                    where inv.IsDelete.Equals(false) && (inv.Investment.Contains(key)) && inv.IsUT == true && inv.CIF == cif
                                    select new InvestmentIDModel
                                    {
                                        InvestmentID = inv.InvestmentID1,
                                        Investment = inv.Investment,
                                        CIF = inv.CIF,
                                        IsUT = inv.IsUT,
                                        IsBond = inv.IsBond,
                                        STNumber = inv.STNumber,
                                        CheckIN = inv.CheckIN,
                                        CheckCIF = inv.CheckCIF,
                                        CheckName = inv.CheckName,
                                        CheckST = inv.CheckST
                                    }).ToList();
                    foreach (var item in investIDTemp)
                    {
                        var dt = investID.Where(x => x.Investment.Equals(item.Investment)).FirstOrDefault();
                        if (dt == null)
                            investID.Add(item);
                    }
                }
                isSuccess = true;
            }
            catch (Exception e)
            {
                Message = e.Message;
            }
            return isSuccess;
        }
        public bool PopulateInvestmentID(string investmentID, ref InvestmentIDGenerateModel bindData, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities cont = new DBSEntities())
                {
                    var tempbind = (from tr in cont.Transactions
                                    where tr.InvestmentID.Equals(investmentID) && tr.ProductID == (int)DBSProductID.IDInvestmentProductIDCons
                                    select tr).OrderByDescending(x => x.CreateDate).FirstOrDefault();
                    if (tempbind != null)
                    {
                        bindData = (from tr in cont.Transactions
                                    //where tr.InvestmentID.Equals(investmentID) && tr.FunctionType == (int)ConsFunctionType.UTAdd
                                    where tr.TransactionID.Equals(tempbind.TransactionID)
                                    select new InvestmentIDGenerateModel
                                    {
                                        ID = tr.TransactionID,
                                        AccountType = tr.AccountType == null ? 0 : (int)tr.AccountType,
                                        CustomerRiskEffectiveDate = tr.CRiskEfectiveDate,
                                        FNACore = tr.IsFNACore == null ? false : (bool)tr.IsFNACore,
                                        OperativeAccount = tr.OperativeAccount,
                                        RiskProfileExpiryDate = tr.RiskProfileExpiryDate,
                                        RiskScore = tr.RiskScore == null ? 0 : tr.RiskScore,
                                        SolID = tr.SolID
                                    }).FirstOrDefault();
                        if (bindData != null)
                        {
                            long TRid = bindData.ID;
                            IList<JoinAccountModel> UTJoin = (from uj in cont.JoinAccounts
                                                              where uj.TransactionID == TRid
                                                              select new JoinAccountModel
                                                              {
                                                                  Join = uj.JoinType.Value != null ? (uj.JoinType.Value == false ? new JoinModel { ID = uj.JoinType.Value, Name = "And" } : new JoinModel { ID = uj.JoinType.Value, Name = "Or" }) : new JoinModel { ID = false, Name = "And" },
                                                                  CustomerJoin = new CustomerModel { CIF = uj.Customer.CIF, Name = uj.Customer.CustomerName },
                                                                  CustomerRiskEffectiveDateJoin = uj.RiskEffectiveDate,
                                                                  RiskProfileExpiryDateJoin = uj.RiskProfileExpiryDate,
                                                                  RiskScoreJoin = uj.RiskScore == null ? 0 : uj.RiskScore,
                                                                  SolIDJoin = uj.SolID,
                                                                  JoinOrder = uj.JoinOrder
                                                              }).ToList<JoinAccountModel>();
                            bindData.UTJoin = UTJoin;
                        }
                    }
                    else
                    {
                        var CheckInvestmentID = (from Cust in cont.Customers
                                                 join Inv in cont.InvestmentIDs on Cust.CIF equals Inv.CIF
                                                 where Inv.IsDelete.Equals(false) && Inv.Investment.Equals(investmentID) && Inv.IsUT == true
                                                 select Cust).FirstOrDefault();
                        if (CheckInvestmentID != null)
                        {
                            InvestmentIDGenerateModel GetInvModel = new InvestmentIDGenerateModel();
                            GetInvModel.CustomerRiskEffectiveDate = CheckInvestmentID.CustomerRiskEffectiveDate;
                            GetInvModel.RiskProfileExpiryDate = CheckInvestmentID.RiskProfileExpiryDate;
                            if (CheckInvestmentID.RiskProfileScore == null || CheckInvestmentID.RiskProfileScore == 0)
                                GetInvModel.FNACore = false;
                            else
                                GetInvModel.FNACore = true;
                            GetInvModel.RiskScore = CheckInvestmentID.RiskProfileScore;

                            if (CheckInvestmentID.LocationID.HasValue)
                            {
                                var GetSolId = (from x in cont.Locations
                                                where x.LocationID.Equals(CheckInvestmentID.LocationID.Value)
                                                select x).FirstOrDefault();
                                GetInvModel.SolID = GetSolId.SolID;
                            }
                            bindData = GetInvModel;
                            IList<JoinAccountModel> UTJoin = new List<JoinAccountModel>();
                            bindData.UTJoin = UTJoin;
                        }
                    }
                }
                isSuccess = true;
            }
            catch (Exception e)
            {
                Message = e.Message;
            }
            return isSuccess;
        }
        public bool PopulateInvestmentIDMutual(string investmentID, ref IList<TransactitonMutualFundModel> bindData, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities cont = new DBSEntities())
                {
                    var dta = (from tr in cont.MutualLists
                               where tr.InvestmentID.Equals(investmentID)
                               select new TransactitonMutualFundModel
                               {
                                   MutualFundList = tr.Fund.FundID == null ? new CBOFundModel { ID = 0, FundName = "", FundCode = "" } : new CBOFundModel { ID = tr.Fund.FundID, FundName = tr.Fund.FundName, FundCode = tr.Fund.FundCode },
                                   MutualFundSwitchFrom = tr.Fund.FundID == null ? new CBOFundModel { ID = 0, FundName = "", FundCode = "" } : new CBOFundModel { ID = tr.Fund.FundID, FundName = tr.Fund.FundName, FundCode = tr.Fund.FundCode },
                                   MutualFundSwitchTo = new CBOFundModel { ID = 0, FundName = "", FundCode = "" },
                                   MutualPartial = false,
                                   MutualUnitNumber = tr.ConfirmedUnit == null ? 0 : tr.ConfirmedUnit,
                                   MutualAmount = tr.ClosingBalanceAmount == null ? 0 : tr.ClosingBalanceAmount,
                                   MutualCurrency = tr.Currency == null ? new CurrencyModel { ID = 0, Code = "" } : new CurrencyModel { ID = (int)tr.Currency.CurrencyID, Code = tr.Currency.CurrencyCode },
                                   MutualSelected = false
                               }).ToList();
                    bindData = dta;
                }
                isSuccess = true;
            }
            catch (Exception e)
            {
                Message = e.Message;
            }
            return isSuccess;
        }
        public bool GetTransactionDraftINByID(long id, ref TransactionUTINDraftModel transaction, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                context.Transactions.AsNoTracking();
                TransactionUTINDraftModel datatransaction = (from a in context.TransactionDrafts
                                                             where a.TransactionID.Equals(id)
                                                             select new TransactionUTINDraftModel
                                                             {
                                                                 ID = a.TransactionID,
                                                                 ApplicationID = a.ApplicationID,
                                                                 ApplicationDate = a.ApplicationDate,
                                                                 Amount = a.Amount,
                                                                 IsTopUrgent = a.IsTopUrgent,
                                                                 IsNewCustomer = a.IsNewCustomer,
                                                                 CreateDate = a.CreateDate,
                                                                 LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                 LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                                                 CustomerRiskEffectiveDate = a.CRiskEfectiveDate,
                                                                 FNACore = new FNACoreModel { ID = (a.IsFNACore == true ? 1 : 0), Name = "" },
                                                                 AccountType = new AccountTypeModel { ID = 0, Name = "" },
                                                                 SolID = a.SolID,
                                                                 RiskScore = a.RiskScore,
                                                                 RiskProfileExpiryDate = a.RiskProfileExpiryDate,
                                                                 OperativeAccount = a.OperativeAccount,
                                                                 Investment = a.InvestmentID,
                                                                 AttachmentRemarks = a.AttachmentRemarks,//add by adi
                                                                 DraftCustomerName = a.DraftCustomerName,
                                                                 DraftCIF = a.DraftCIF
                                                             }).SingleOrDefault();

                if (datatransaction != null)
                {
                    var data = context.TransactionDrafts.Where(x => x.TransactionID.Equals(datatransaction.ID)).Select(x => x).FirstOrDefault();
                    if (datatransaction.IsNewCustomer != null && datatransaction.IsNewCustomer != true)
                    {
                        string cif = context.TransactionDrafts.Where(a => a.TransactionID.Equals(id)).Select(a => a.CIF).FirstOrDefault();
                        if (!string.IsNullOrEmpty(cif))
                        {
                            ICustomerRepository customerRepo = new CustomerRepository();
                            CustomerModel customer = new CustomerModel();
                            if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                            {
                                datatransaction.Customer = customer;
                            }

                            customerRepo.Dispose();
                            customer = null;
                        }
                    }
                    else
                    {
                        datatransaction.Customer = new CustomerModel();
                        datatransaction.Customer.CIF = datatransaction.DraftCIF;
                        datatransaction.Customer.Name = datatransaction.DraftCustomerName;
                    }

                    if (data.Product != null)
                    {
                        ProductModel p = new ProductModel();

                        p.ID = data.Product.ProductID;
                        p.Code = data.Product.ProductCode;
                        p.Name = data.Product.ProductName;

                        datatransaction.Product = p;
                    }
                    if (data.FunctionType != null)
                    {
                        FunctionTypeModel fModel = new FunctionTypeModel();
                        var dFunction = context.ParameterSystems.Where(x => x.ParsysID == data.FunctionType).Select(x => x).FirstOrDefault();
                        if (dFunction != null)
                        {
                            fModel.ID = dFunction.ParsysID;
                            fModel.Name = dFunction.ParameterValue;
                        }
                        datatransaction.FunctionType = fModel;
                    }
                    if (data.AccountType != null)
                    {
                        AccountTypeModel accModel = new AccountTypeModel();
                        var dAcc = context.ParameterSystems.Where(x => x.ParsysID == data.AccountType).Select(x => x).FirstOrDefault();
                        if (dAcc != null)
                        {
                            accModel.ID = dAcc.ParsysID;
                            accModel.Name = dAcc.ParameterValue;
                        }
                        datatransaction.AccountType = accModel;
                    }
                    var docDraft = (from doc in context.TransactionDocumentDrafts
                                    where doc.TransactionID == id && doc.IsDeleted.Equals(false)
                                    select new TransactionDocumentDraftModel
                                    {
                                        ID = doc.TransactionDocumentID,
                                        IsDormant = doc.IsDormant,
                                        LastModifiedBy = doc.UpdateBy == null ? doc.CreateBy : doc.UpdateBy,
                                        LastModifiedDate = doc.UpdateDate == null ? doc.CreateDate : doc.CreateDate,
                                        Purpose = new DocumentPurposeModel { ID = doc.DocumentPurpose.PurposeID, Name = doc.DocumentPurpose.PurposeName },
                                        Type = new DocumentTypeModel { ID = doc.DocumentType.DocTypeID, Name = doc.DocumentType.DocTypeName },
                                        FileName = doc.Filename,
                                        DocumentPath = new DocumentPathModel { name = doc.Filename, lastModified = string.Empty, lastModifiedDate = DateTime.Now, DocPath = doc.DocumentPath, type = Util.ExistingDoctype }
                                    }).ToList();
                    if (docDraft != null)
                    {
                        datatransaction.Documents = docDraft;
                    }
                    var joinAcc = (from acc in context.JoinAccountDrafts
                                   where acc.TransactionID == id
                                   select new JoinAccountModel
                                   {
                                       JoinOrder = acc.JoinOrder,
                                       Join = acc.JoinType.Value != null ? (acc.JoinType.Value == false ? new JoinModel { ID = acc.JoinType.Value, Name = "And" } : new JoinModel { ID = acc.JoinType.Value, Name = "Or" }) : new JoinModel { ID = false, Name = "And" },
                                       CustomerRiskEffectiveDateJoin = acc.RiskEffectiveDate,
                                       RiskProfileExpiryDateJoin = acc.RiskProfileExpiryDate,
                                       RiskScoreJoin = acc.RiskScore,
                                       SolIDJoin = acc.SolID,
                                       CustomerJoin = new CustomerModel { CIF = acc.CIF, Name = acc.Customer.CustomerName }
                                   }).ToList();
                    if (joinAcc != null)
                    {
                        datatransaction.UTJoin = joinAcc;
                    }
                    transaction = datatransaction;
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionDraftSPByID(long id, ref TransactionUTSPDraftModel transaction, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                context.Transactions.AsNoTracking();
                TransactionUTSPDraftModel datatransaction = (from a in context.TransactionDrafts
                                                             where a.TransactionID.Equals(id)
                                                             select new TransactionUTSPDraftModel
                                                             {
                                                                 ID = a.TransactionID,
                                                                 ApplicationID = a.ApplicationID,
                                                                 ApplicationDate = a.ApplicationDate,
                                                                 Amount = a.Amount,
                                                                 IsTopUrgent = a.IsTopUrgent,
                                                                 IsNewCustomer = a.IsNewCustomer,
                                                                 CreateDate = a.CreateDate,
                                                                 LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                 LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                                                 SolID = a.SolID,
                                                                 Investment = a.InvestmentID,
                                                                 Remarks = a.Remarks,
                                                                 AttachmentRemarks = a.AttachmentRemarks //add by adi
                                                             }).SingleOrDefault();

                if (datatransaction != null)
                {
                    var data = context.TransactionDrafts.Where(x => x.TransactionID.Equals(datatransaction.ID)).Select(x => x).FirstOrDefault();
                    if (datatransaction.IsNewCustomer != null && datatransaction.IsNewCustomer != true)
                    {
                        //string cif = context.TransactionDrafts.Where(a => a.TransactionID.Equals(id)).Select(a => a.CIF).FirstOrDefault();
                        if (!string.IsNullOrEmpty(data.CIF))
                        {
                            ICustomerRepository customerRepo = new CustomerRepository();
                            CustomerModel customer = new CustomerModel();
                            if (customerRepo.GetCustomerByCIF(data.CIF, ref customer, ref message))
                            {
                                datatransaction.Customer = customer;
                            }

                            customerRepo.Dispose();
                            customer = null;
                        }
                    }
                    else
                    {
                        datatransaction.Customer = new CustomerModel();
                    }

                    if (data.Product != null)
                    {
                        ProductModel p = new ProductModel();

                        p.ID = data.Product.ProductID;
                        p.Code = data.Product.ProductCode;
                        p.Name = data.Product.ProductName;

                        datatransaction.Product = p;
                    }
                    if (data.TransactionTypeID != null)
                    {
                        Transaction_TypeModel ttModel = new Transaction_TypeModel();
                        var dFunction = context.TransactionTypes.Where(x => x.TransTypeID == data.TransactionTypeID).Select(x => x).FirstOrDefault();
                        if (dFunction != null)
                        {
                            ttModel.TransTypeID = dFunction.TransTypeID;
                            ttModel.TransactionTypeName = dFunction.TransactionType1;
                        }
                        datatransaction.Transaction_Type = ttModel;
                    }

                    var docDraft = (from doc in context.TransactionDocumentDrafts
                                    where doc.TransactionID == id && doc.IsDeleted.Equals(false)
                                    select new TransactionDocumentDraftModel
                                    {
                                        ID = doc.TransactionDocumentID,
                                        IsDormant = doc.IsDormant,
                                        LastModifiedBy = doc.UpdateBy == null ? doc.CreateBy : doc.UpdateBy,
                                        LastModifiedDate = doc.UpdateDate == null ? doc.CreateDate : doc.CreateDate,
                                        Purpose = new DocumentPurposeModel { ID = doc.DocumentPurpose.PurposeID, Name = doc.DocumentPurpose.PurposeName },
                                        Type = new DocumentTypeModel { ID = doc.DocumentType.DocTypeID, Name = doc.DocumentType.DocTypeName },
                                        FileName = doc.Filename,
                                        DocumentPath = new DocumentPathModel { name = doc.Filename, lastModified = string.Empty, lastModifiedDate = DateTime.Now, DocPath = doc.DocumentPath, type = Util.ExistingDoctype }
                                    }).ToList();
                    if (docDraft != null)
                    {
                        datatransaction.Documents = docDraft;
                    }

                    var Mutual = (from mf in context.MutualFundTransactionDrafts
                                  join fundto in context.Funds on mf.SwitchToFundID equals fundto.FundID into ft
                                  from fundto in ft.DefaultIfEmpty()
                                  where mf.TransactionID == id
                                  select new TransactitonMutualFundModel
                                  {
                                      MutualAmount = mf.Amount,
                                      MutualCurrency = mf.Currency == null ? new CurrencyModel { ID = 0, Code = "" } : new CurrencyModel { ID = mf.Currency.CurrencyID, Code = mf.Currency.CurrencyCode },
                                      MutualFundList = mf.FundID == null ? new CBOFundModel { ID = 0, FundCode = "", FundName = "" } : new CBOFundModel { ID = mf.FundID, FundCode = mf.Fund.FundCode, FundName = mf.Fund.FundName },
                                      MutualFundSwitchFrom = mf.FundID == null ? new CBOFundModel { ID = 0, FundCode = "", FundName = "" } : new CBOFundModel { ID = mf.FundID, FundCode = mf.Fund.FundCode, FundName = mf.Fund.FundName },
                                      MutualFundSwitchTo = fundto.FundID == null ? new CBOFundModel { ID = 0, FundCode = "", FundName = "" } : new CBOFundModel { ID = fundto.FundID, FundCode = fundto.FundCode, FundName = fundto.FundName },
                                      MutualPartial = mf.IsPartial,
                                      MutualSelected = false,
                                      MutualUnitNumber = mf.NumberofUnit,

                                  }).ToList();
                    if (Mutual != null)
                    {
                        datatransaction.MutualFundForms = Mutual;
                    }

                    transaction = datatransaction;
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        public bool DeleteTransactionDraftByID(int id, ref string Message)
        {
            bool isDelete = false;
            using (DBSEntities context_ts = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        Entity.TransactionDraft data = context.TransactionDrafts.Where(a => a.TransactionID.Equals(id)).SingleOrDefault();
                        if (data != null)
                        {
                            Entity.TransactionBringupTask dataBringup = context.TransactionBringupTasks.Where(a => a.TransactionID.Equals(id)).FirstOrDefault();
                            if (dataBringup != null)
                            {
                                data.IsDeleted = true;
                                context.SaveChanges();

                                dataBringup.DateOut = DateTime.UtcNow;
                                dataBringup.IsSubmitted = true;
                                dataBringup.SubmittedBy = currentUser.GetCurrentUser().LoginName;
                                dataBringup.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                dataBringup.UpdateDate = DateTime.UtcNow;
                                context.SaveChanges();
                            }
                            else
                            {
                                var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(id)).ToList();
                                if (existingDocs != null)
                                {
                                    if (existingDocs.Count() > 0)
                                    {
                                        foreach (var item in existingDocs)
                                        {
                                            var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                            context.TransactionDocumentDrafts.Remove(deleteDoc);
                                            context.SaveChanges();
                                        }
                                    }
                                }
                                var existingjoin = context.JoinAccountDrafts.Where(a => a.TransactionID.Equals(id)).ToList();
                                if (existingjoin != null)
                                {
                                    if (existingjoin.Count() > 0)
                                    {
                                        foreach (var item in existingjoin)
                                        {
                                            var deletejoin = context.JoinAccountDrafts.Where(a => a.JoinAccountID.Equals(item.JoinAccountID)).SingleOrDefault();
                                            context.JoinAccountDrafts.Remove(deletejoin);
                                            context.SaveChanges();
                                        }
                                    }
                                }
                                var existingMutual = context.MutualFundTransactionDrafts.Where(a => a.TransactionID.Equals(id)).ToList();
                                if (existingMutual != null)
                                {
                                    if (existingMutual.Count() > 0)
                                    {
                                        foreach (var item in existingMutual)
                                        {
                                            var deletemutual = context.MutualFundTransactionDrafts.Where(a => a.MutualFundTransactionID.Equals(item.MutualFundTransactionID)).SingleOrDefault();
                                            context.MutualFundTransactionDrafts.Remove(deletemutual);
                                            context.SaveChanges();
                                        }
                                    }
                                }
                                context.TransactionDrafts.Remove(data);
                                context.SaveChanges();
                            }
                            isDelete = true;
                        }
                        else
                        {
                            Message = string.Format("Draft ID {0} is does not exist.", id);
                            isDelete = false;
                        }
                        ts.Complete();
                    }
                    catch (Exception ex)
                    {
                        ts.Dispose();
                        Message = ex.Message;
                    }
                }
            }
            return isDelete;
        }
        public bool UpdateTransactionDraftIN(long id, TransactionUTINModel transaction, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.TransactionDraft data = context.TransactionDrafts.Where(a => a.TransactionID.Equals(id)).SingleOrDefault();
                    if (data != null)
                    {
                        data.ApplicationDate = DateTime.UtcNow;
                        data.CIF = transaction.Customer.CIF;
                        data.ProductID = transaction.Product.ID;
                        data.IsDraft = true;
                        data.ApplicationID = string.Empty;
                        data.StateID = (int)StateID.OnProgress;
                        data.CreateDate = DateTime.UtcNow;
                        data.CreateBy = currentUser.GetCurrentUser().LoginName;
                        data.Amount = 0;
                        data.CurrencyID = (int)CurrencyID.NullCurrency;
                        data.IsFNACore = transaction.FNACore.Name.ToLower() == "yes" ? true : false;
                        data.FunctionType = transaction.FunctionType.ID;
                        data.AccountType = transaction.AccountType.ID;
                        data.SolID = transaction.SolID;
                        data.CRiskEfectiveDate = transaction.CustomerRiskEffectiveDate;
                        data.RiskScore = transaction.RiskScore;
                        data.RiskProfileExpiryDate = transaction.RiskProfileExpiryDate;
                        data.OperativeAccount = transaction.OperativeAccount;
                        data.IsNewCustomer = false;


                        if (transaction.Product != null && transaction.Product.Code != null)
                        {
                            data.ProductID = transaction.Product.ID;
                        }

                        if (!transaction.IsNewCustomer)
                        {
                            data.CIF = transaction.Customer.CIF;

                            if (transaction.Currency != null && transaction.Currency.Code != null)
                            {
                                data.CurrencyID = transaction.Currency.ID;
                            }

                            data.DebitCurrencyID = 1;
                        }
                        else
                        {
                            data.DraftCIF = transaction.CustomerDraft.DraftCIF;
                            data.DraftAccountNumber = transaction.CustomerDraft.DraftAccountNumber.AccountNumber;
                            data.DraftCustomerName = transaction.CustomerDraft.DraftCustomerName;
                            data.DraftCurrencyID = transaction.CustomerDraft.DraftAccountNumber.Currency.ID;

                            if (transaction.Currency != null && transaction.Currency.Code != null)
                            {
                                data.CurrencyID = transaction.Currency.ID;
                            }
                        }
                    }
                    context.SaveChanges();
                    #region Save Document
                    var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(transaction.ID)).ToList();
                    if (existingDocs != null)
                    {
                        if (existingDocs.Count() > 0)
                        {
                            foreach (var item in existingDocs)
                            {
                                var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                deleteDoc.IsDeleted = true;
                                context.SaveChanges();
                            }
                        }
                    }
                    if (transaction.Documents.Count > 0)
                    {
                        foreach (var item in transaction.Documents)
                        {
                            Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                            {
                                TransactionID = data.TransactionID,
                                DocTypeID = item.Type.ID,
                                PurposeID = item.Purpose.ID,
                                Filename = item.FileName,
                                DocumentPath = item.DocumentPath,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName
                            };
                            context.TransactionDocumentDrafts.Add(document);
                        }
                        context.SaveChanges();
                    }
                    #endregion
                    ts.Complete();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }

            return isSuccess;
        }
        public bool UpdateTransactionDraftSP(long id, TransactionUTSPModel transaction, ref string message)
        {
            bool isSuccess = false;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.TransactionDraft data = context.TransactionDrafts.Where(a => a.TransactionID.Equals(id)).SingleOrDefault();
                    if (data != null)
                    {
                        data.ApplicationDate = DateTime.UtcNow;
                        data.CIF = transaction.Customer.CIF;
                        data.ProductID = transaction.Product.ID;
                        data.IsDraft = true;
                        data.ApplicationID = string.Empty;
                        data.StateID = (int)StateID.OnProgress;
                        data.CreateDate = DateTime.UtcNow;
                        data.CreateBy = currentUser.GetCurrentUser().LoginName;
                        data.Amount = 0;
                        data.CurrencyID = (int)CurrencyID.NullCurrency;
                        data.TransactionTypeID = transaction.Transaction_Type.TransTypeID;
                        data.InvestmentID = transaction.Investment;
                        data.Remarks = transaction.Remarks;

                        if (transaction.Product != null && transaction.Product.Code != null)
                        {
                            data.ProductID = transaction.Product.ID;
                        }

                        if (!transaction.IsNewCustomer)
                        {
                            data.CIF = transaction.Customer.CIF;

                            if (transaction.Currency != null && transaction.Currency.Code != null)
                            {
                                data.CurrencyID = transaction.Currency.ID;
                            }

                            data.DebitCurrencyID = 1;
                        }
                        else
                        {
                            data.DraftCIF = transaction.CustomerDraft.DraftCIF;
                            data.DraftAccountNumber = transaction.CustomerDraft.DraftAccountNumber.AccountNumber;
                            data.DraftCustomerName = transaction.CustomerDraft.DraftCustomerName;
                            data.DraftCurrencyID = transaction.CustomerDraft.DraftAccountNumber.Currency.ID;

                            if (transaction.Currency != null && transaction.Currency.Code != null)
                            {
                                data.CurrencyID = transaction.Currency.ID;
                            }
                        }
                    }
                    context.SaveChanges();
                    #region Save Document
                    var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(transaction.ID)).ToList();
                    if (existingDocs != null)
                    {
                        if (existingDocs.Count() > 0)
                        {
                            foreach (var item in existingDocs)
                            {
                                var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                deleteDoc.IsDeleted = true;
                                context.SaveChanges();
                            }
                        }
                    }
                    if (transaction.Documents.Count > 0)
                    {
                        foreach (var item in transaction.Documents)
                        {
                            Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                            {
                                TransactionID = data.TransactionID,
                                DocTypeID = item.Type.ID,
                                PurposeID = item.Purpose.ID,
                                Filename = item.FileName,
                                DocumentPath = item.DocumentPath,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName
                            };
                            context.TransactionDocumentDrafts.Add(document);
                        }
                        context.SaveChanges();
                    }
                    #endregion
                    #region Mutual
                    if (transaction.MutualFundForms != null)
                    {
                        if (transaction.MutualFundForms.Count > 0)
                        {
                            //added by dani 19-02-2016
                            #region Hapus data lama
                            var mutualFundList = (from mf in context.MutualFundTransactionDrafts
                                                  where mf.TransactionID == data.TransactionID
                                                  select mf);
                            //kalau ada data maka dihapus
                            if (mutualFundList != null && mutualFundList.ToList().Count > 0)
                            {
                                foreach (var item in mutualFundList.ToList())
                                {
                                    context.MutualFundTransactionDrafts.Remove(item);
                                }
                                context.SaveChanges();
                            }
                            #endregion
                            foreach (var item in transaction.MutualFundForms)
                            {
                                Entity.MutualFundTransactionDraft mFund = new Entity.MutualFundTransactionDraft();
                                mFund.TransactionID = data.TransactionID;
                                mFund.FundID = item.MutualFundList.ID;
                                if (item.MutualCurrency != null)
                                    mFund.CurrencyID = item.MutualCurrency.ID;
                                if (item.MutualFundSwitchTo != null)
                                    if (item.MutualFundSwitchTo.ID > 0)
                                        mFund.SwitchToFundID = item.MutualFundSwitchTo.ID;
                                mFund.Amount = item.MutualAmount == null ? null : item.MutualAmount;
                                mFund.IsPartial = item.MutualPartial == null ? false : item.MutualPartial;
                                mFund.NumberofUnit = item.MutualUnitNumber == null ? null : item.MutualUnitNumber;
                                mFund.CreateDate = DateTime.UtcNow;
                                mFund.CreateBy = currentUser.GetCurrentUser().LoginName;
                                context.MutualFundTransactionDrafts.Add(mFund);
                            }

                            context.SaveChanges();
                        }
                    }

                    #endregion
                    ts.Complete();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.InnerException.Message;
                }
            }
            return isSuccess;
        }
    }
    #endregion
}