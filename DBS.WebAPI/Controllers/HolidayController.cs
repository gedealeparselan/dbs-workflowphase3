﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
     [Authorize]
    public class HolidayController:ApiController
    {
         private string message = string.Empty;

        private readonly IHolidayRepository repository = null;
        public HolidayController()
        {
            this.repository = new HolidayRepository();
        }
        public HolidayController(IHolidayRepository repository)
        {
            this.repository = repository;
        }

        [ResponseType(typeof(HolidayModel))]
        [Route("api/Holiday/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            HolidayModel output = new HolidayModel();

            if (!repository.GetHolidayByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<HolidayModel>(HttpStatusCode.OK, output);
            }
        }
        [ResponseType(typeof(IList<HolidayModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<HolidayModel> output = new List<HolidayModel>();

            if (!repository.GetHoliday(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<HolidayModel>>(HttpStatusCode.OK, output);
            }
        }
        [ResponseType(typeof(HolidayGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<HolidayFilter> filters)
        {
            HolidayGrid output = new HolidayGrid();

            if (!repository.GetHoliday(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "HolidayName" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<HolidayGrid>(HttpStatusCode.OK, output);
            }
        }
        [ResponseType(typeof(IList<HolidayModel>))]
        [HttpGet]
        [Route("api/Holiday/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<HolidayModel> output = new List<HolidayModel>();

            if (!repository.GetHoliday(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<HolidayModel>>(HttpStatusCode.OK, output);
            }
        }
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(HolidayModel holiday)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddHoliday(holiday, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Holiday data has been created." });
                }
            }
        }
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/Holiday/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, HolidayModel holiday)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateHoliday(id, holiday, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Holiday data has been updated." });
                }
            }
        }
        [HttpDelete]
        [Route("api/Holiday/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteHoliday(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Holiday data has been deleted." });
                }
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}