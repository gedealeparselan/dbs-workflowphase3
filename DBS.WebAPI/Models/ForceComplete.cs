﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Dynamic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using DBS.Entity;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;

namespace DBS.WebAPI.Models
{

    #region property
    [ModelName("ForceComplete")]
    
    public class ForceCompleteModel
    {
        public int ID { get; set; }
        public string UserCategoryCode { get; set; }
        public RoleModel Group { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }

    public class ForceCompleteRow : ForceCompleteModel
    {
        public int RowID { get; set; }
    }
    public class ForceCompleteGrid : Grid
    {
        public IList<ForceCompleteRow> Rows { get; set; }
    }
    #endregion

    #region filter
    public class ForceCompleteFilter : Filter { }
    #endregion
    #region interface
    public interface IForceCompleteRepo : IDisposable
    {
        bool GetForcedCompleteByID(int id, ref ForceCompleteModel ForceCompleteID, ref string message);
        bool GetForceComplete(ref IList<ForceCompleteModel> ForceComplete, int limit, int index, ref string message);
        bool GetForceComplete(ref IList<ForceCompleteModel> ForceComplete, ref string message);
        bool GetForceComplete(int page, int size, IList<ForceCompleteFilter> filters, string sortColumn, string sortOrder, ref ForceCompleteGrid ForceComplete, ref string message);
        bool GetForceComplete(ForceCompleteFilter filter, ref IList<ForceCompleteModel> ForceCompletes, ref string message);
        bool GetForceComplete(string key, int limit, ref IList<ForceCompleteModel> ForceCompletes, ref string message);
        bool AddForceComplete(ForceCompleteModel ForceComplete, ref string message);
        bool UpdateForceComplete(int id, ForceCompleteModel ForceComplete, ref string message);
        bool DeleteForceComplete(int id, ref string message);
    }
    #endregion


    //public class ForceComplete
    //{
    //}
    #region Repository
    public class ForceCompleteRepository : IForceCompleteRepo
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetForcedCompleteByID(int id, ref ForceCompleteModel ForceCompleteID, ref string message)
        {
            bool isSuccess = false;

            try
            {
                ForceCompleteID = (from a in context.RoleForceCompleteMappings
                                   join b in context.Roles on a.RoleID equals b.RoleID
                                   where a.IsDelete.Equals(false) && a.ID.Equals(id)
                                   select new ForceCompleteModel
                                   {
                                       ID = a.ID,
                                       UserCategoryCode = a.UserCategoryCode,
                                       Group = new RoleModel
                                       {
                                           ID = (a.RoleID.Value != null) ? a.RoleID.Value : 0,
                                           Name = (a.RoleID != null) ? b.RoleName : "-"
                                       },
                                       LastModifiedDate = a.CreateDate,
                                       LastModifiedBy = a.CreateBy
                                   }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetForceComplete(ref IList<ForceCompleteModel> ForceComplete, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    ForceComplete = (from a in context.RoleForceCompleteMappings
                                     join b in context.Roles on a.RoleID equals b.RoleID
                                     where a.IsDelete.Equals(false)
                                     orderby new { a.UserCategoryCode, a.RoleID }
                                     select new ForceCompleteModel
                                     {
                                         ID = a.ID,
                                         UserCategoryCode = a.UserCategoryCode,
                                         Group = new RoleModel
                                         {
                                             ID = (a.RoleID.Value != null) ? a.RoleID.Value : 0,
                                             Name = (a.RoleID != null) ? b.RoleName : "-"
                                         },
                                         LastModifiedBy = a.CreateBy,
                                         LastModifiedDate = a.CreateDate
                                     }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetForceComplete(ref IList<ForceCompleteModel> ForceComplete, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    ForceComplete = (from a in context.RoleForceCompleteMappings
                                     join b in context.Roles on a.RoleID equals b.RoleID
                                     where a.IsDelete.Equals(false)
                                     orderby new { a.UserCategoryCode, a.RoleID }
                                     select new ForceCompleteModel
                                     {
                                         ID = a.ID,
                                         UserCategoryCode = a.UserCategoryCode,
                                         Group = new RoleModel
                                         {
                                             ID = (a.RoleID.Value != null) ? a.RoleID.Value : 0,
                                             Name = (a.RoleID != null) ? b.RoleName : "-"
                                         },
                                         LastModifiedBy = a.CreateBy,
                                         LastModifiedDate = a.CreateDate
                                     }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetForceComplete(int page, int size, IList<ForceCompleteFilter> filters, string sortColumn, string sortOrder, ref ForceCompleteGrid ForceComplete, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                ForceCompleteModel filter = new ForceCompleteModel();
                filter.Group = new RoleModel { Name = string.Empty };
                if (filters != null)
                {
                    filter.UserCategoryCode = (string)filters.Where(a => a.Field.Equals("UserCategoryCode")).Select(a => a.Value).SingleOrDefault();
                    filter.Group = new RoleModel { Name = (string)filters.Where(a => a.Field.Equals("Group")).Select(a => a.Value).SingleOrDefault() };
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.RoleForceCompleteMappings
                                join b in context.Roles on a.RoleID.Value equals b.RoleID

                                let isUsercategoryCode = !string.IsNullOrEmpty(filter.UserCategoryCode)
                                let isGroup = !string.IsNullOrEmpty(filter.Group.Name)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDelete == false &&
                                    (isUsercategoryCode ? a.UserCategoryCode.Contains(filter.UserCategoryCode) : true) &&
                                    (isGroup ? b.RoleName.Contains(filter.Group.Name) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new ForceCompleteModel
                                {
                                    ID = a.ID,
                                    UserCategoryCode = a.UserCategoryCode,
                                    Group = new RoleModel()
                                    {
                                        ID = (a.RoleID.Value != null) ? a.RoleID.Value : 0,
                                        Name = (a.RoleID.Value != null) ? b.RoleName : "-",

                                    },
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    ForceComplete.Page = page;
                    ForceComplete.Size = size;
                    ForceComplete.Total = data.Count();
                    ForceComplete.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    ForceComplete.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new ForceCompleteRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            UserCategoryCode = a.UserCategoryCode,
                            Group = a.Group,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetForceComplete(ForceCompleteFilter filter, ref IList<ForceCompleteModel> ForceCompletes, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetForceComplete(string key, int limit, ref IList<ForceCompleteModel> ForceCompletes, ref string message)
        {
            //throw new NotImplementedException();
            bool isSuccess = false;

            try
            {
                ForceCompletes = (from a in context.RoleForceCompleteMappings
                                  where a.IsDelete.Equals(false) &&
                                  a.CreateBy.Equals(key)
                                  orderby new { a.CreateBy }
                                  select new ForceCompleteModel
                                  {
                                      UserCategoryCode = a.UserCategoryCode,
                                      LastModifiedBy = a.CreateBy
                                  }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddForceComplete(ForceCompleteModel ForceComplete, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.RoleForceCompleteMappings.Where(a => a.ID.Equals(ForceComplete.ID) && a.IsDelete == false).Any())
                {
                    RoleForceCompleteMapping forceComplete = new RoleForceCompleteMapping();

                    forceComplete.UserCategoryCode = ForceComplete.UserCategoryCode;
                    if (!string.IsNullOrEmpty(ForceComplete.Group.Name))
                    {
                        forceComplete.RoleID = ForceComplete.Group.ID;
                    }

                    forceComplete.IsDelete = false;
                    forceComplete.CreateDate = DateTime.UtcNow;
                    forceComplete.CreateBy = currentUser.GetCurrentUser().DisplayName;


                    context.RoleForceCompleteMappings.Add(forceComplete);
                    context.SaveChanges();
                    isSuccess = true;
                }
                else
                {
                    message = string.Format("ForceComplete data for ForceComplete  {0} is already exist.", ForceComplete.Group.Name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateForceComplete(int id, ForceCompleteModel ForceComplete, ref string message)
        {
            // throw new NotImplementedException();
            bool isSuccess = false;

            try
            {
                Entity.RoleForceCompleteMapping data = context.RoleForceCompleteMappings.Where(a => a.ID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.RoleForceCompleteMappings.Where(a => a.ID != ForceComplete.ID && a.UserCategoryCode.Equals(ForceComplete.UserCategoryCode) && a.IsDelete == false).Count() >= 1)
                    {
                        message = string.Format("ForceComplete {0} is exist.", ForceComplete.ID);
                    }
                    else
                    {
                        // data.ProductID = transactionLimit.Product;
                        data.UserCategoryCode = ForceComplete.UserCategoryCode;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        if (!string.IsNullOrEmpty(ForceComplete.Group.Name))
                        {
                            data.RoleID = ForceComplete.Group.ID;
                        }
                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("ForceComplete with id {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteForceComplete(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.RoleForceCompleteMapping data = context.RoleForceCompleteMappings.Where(a => a.ID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDelete = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("ForceComplete data for ForceComplete ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    }

    #endregion

}