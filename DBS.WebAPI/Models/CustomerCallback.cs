﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using DBS.Entity;
using DBS.WebAPI.Models;
using DBS.WebAPI.Helpers;

namespace DBS.WebAPI.Models
{
    #region Property
    public class CustomerCallbackModel
    {
        public long ID { get; set; }
        public string CIF { get; set; }
        public long ContactID { get; set; }
        public string ContactName { get; set; }
        public int ProductID { get; set; }
        public string ProductName { get; set; }
	    public DateTime Time { get; set; }
	    public bool IsUTC { get; set; }
        public string IsUTCDescription { get; set; }
	    public string Remark { get; set; }
        public long? TransactionID { get; set; }
	    public string ApplicationID { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
    }

    public class CustomerCallbackRow : CustomerCallbackModel
    {
        public int RowID { get; set; }

    }

    public class CustomerCallbackGrid : Grid
    {
        public IList<CustomerCallbackRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class CustomerCallbackFilter : Filter
    {
    }

    #endregion

    #region Interface

    public interface ICustomerCallbackRepository : IDisposable
    {
        bool GetCustomerCallback(int page, int size, IList<CustomerCallbackFilter> filters, string sortColumn, string sortOrder, ref CustomerCallbackGrid CustomerCallback, ref string message);
        bool GetCustomerCallbackByProductID(int page, int size, IList<CustomerCallbackFilter> filters, string sortColumn, string sortOrder, ref CustomerCallbackGrid CustomerCallback, ref string message);
        bool Add(CustomerCallbackModel CustomerCallback, ref string message);
        bool Utilize(IList<CustomerCallbackModel> CustomerCallback, ref string message);
    }

    #endregion

    #region Repository

    public class CustomerCallbackRepository : ICustomerCallbackRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        
        public bool GetCustomerCallback(int page, int size, IList<CustomerCallbackFilter> filters, string sortColumn, string sortOrder, ref CustomerCallbackGrid CustomerCallback, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;

                CustomerCallbackModel filter = new CustomerCallbackModel();
                string filterUTC = string.Empty;
                DateTime? filterStartTime = null;
                DateTime? filterEndTime = null;
                DateTime? filterModifiedStartDate = null;
                DateTime? filterModifiedEndDate = null;

                if (filters != null)
                {
                    filter.CIF = filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.ContactName = filters.Where(a => a.Field.Equals("ContactName")).Select(a => a.Value).SingleOrDefault();
                    filter.ProductName = filters.Where(a => a.Field.Equals("ProductName")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("Time")).Any())
                    {
                        filterStartTime = DateTime.Parse(filters.Where(a => a.Field.Equals("Time")).Select(a => a.Value).SingleOrDefault());
                        filterEndTime = filterStartTime.Value.AddDays(1);
                    }
                    filterUTC = filters.Where(a => a.Field.Equals("IsUTC")).Select(a => a.Value).SingleOrDefault();
                    filter.Remark = filters.Where(a => a.Field.Equals("Remark")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("ModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    
                    if (filters.Where(a => a.Field.Equals("ModifiedDate")).Any())
                    {
                        filterModifiedStartDate = DateTime.Parse(filters.Where(a => a.Field.Equals("ModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        filterModifiedEndDate = filterModifiedStartDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.CustomerCallbacks
                                join b in context.CustomerContacts on a.ContactID equals b.ContactID
                                join c in context.Customers on b.CIF equals c.CIF
                                join d in context.Products on a.ProductID equals d.ProductID
                                join e in context.Employees on (a.UpdateBy != null ? a.UpdateBy : a.CreateBy) equals e.EmployeeUsername into ae
                                from e in ae.DefaultIfEmpty()
                                let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                let isContactName = !string.IsNullOrEmpty(filter.ContactName)
                                let isProductName = !string.IsNullOrEmpty(filter.ProductName)
                                let isTime = filterStartTime.HasValue ? true : false
                                let isUTC = !string.IsNullOrEmpty(filterUTC)
                                let isRemark = !string.IsNullOrEmpty(filter.Remark)
                                let isLastModifiedBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isLastModifiedDate = filterModifiedStartDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    a.TransactionID == null &&
                                    (e.EmployeeID != null ? e.IsDeleted.Equals(false) : true) &&
                                    (isCIF ? c.CIF.Equals(filter.CIF) : true) &&
                                    (isContactName ? b.ContactName.Contains(filter.ContactName) : true) &&
                                    (isProductName ? d.ProductName.Contains(filter.ProductName) : true) &&
                                    (isTime ? a.Time >= filterStartTime.Value && a.Time < filterEndTime.Value : true) &&
                                    (isUTC ? "Yes".Contains(filterUTC) ? a.IsUTC.Equals(true) : ("No".Contains(filterUTC) ? a.IsUTC.Equals(false) : false) : true) &&
                                    (isRemark ? a.Remark.Contains(filter.Remark) : true) &&
                                    (isLastModifiedBy ? (e.EmployeeName != null ? e.EmployeeName : (a.UpdateBy != null ? a.UpdateBy : a.CreateBy)).Contains(filter.LastModifiedBy) : true) &&
                                    (isLastModifiedDate ? (a.UpdateDate != null ? a.UpdateDate.Value : a.CreateDate) >= filterModifiedStartDate.Value && (a.UpdateDate != null ? a.UpdateDate.Value : a.CreateDate) < filterModifiedEndDate.Value : true)
                                select new CustomerCallbackModel
                                {
                                    ID = a.CallbackID,
                                    CIF = c.CIF,
                                    ContactName = b.ContactName,
                                    ProductName = d.ProductName,
                                    Time = a.Time,
	                                IsUTC = a.IsUTC,
                                    IsUTCDescription = a.IsUTC ? "Yes" : "No",
	                                Remark = a.Remark,
                                    LastModifiedBy = e.EmployeeName != null ? e.EmployeeName : (a.UpdateBy != null ? a.UpdateBy : a.CreateBy),
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    CustomerCallback.Page = page;
                    CustomerCallback.Size = size;
                    CustomerCallback.Total = data.Count();
                    CustomerCallback.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    CustomerCallback.Rows = data.OrderBy(orderBy).AsEnumerable().Select((a, i) => new CustomerCallbackRow
                    {
                        RowID = i + 1,
                        ID = a.ID,
                        CIF = a.CIF,
                        ContactName = a.ContactName,
                        ProductName = a.ProductName,
                        Time = a.Time,
                        IsUTC = a.IsUTC,
                        IsUTCDescription = a.IsUTCDescription,
                        Remark = a.Remark,
                        TransactionID = a.TransactionID,
                        ApplicationID = a.ApplicationID,
                        LastModifiedBy = a.LastModifiedBy,
                        LastModifiedDate = a.LastModifiedDate
                    })
                    .Skip(skip)
                    .Take(size)
                    .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerCallbackByProductID(int page, int size, IList<CustomerCallbackFilter> filters, string sortColumn, string sortOrder, ref CustomerCallbackGrid CustomerCallback, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;

                CustomerCallbackModel filter = new CustomerCallbackModel();
                string filterUTC = string.Empty;
                DateTime? filterStartTime = null;
                DateTime? filterEndTime = null;
                DateTime? filterModifiedStartDate = null;
                DateTime? filterModifiedEndDate = null;

                if (filters != null)
                {
                    filter.CIF = filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();

                    string filterProductID = filters.Where(sp => sp.Field.Equals("ProductID")).Select(sp => sp.Value).SingleOrDefault();
                    if (!string.IsNullOrEmpty(filterProductID))
                    {
                        int valueAmount;
                        bool resultAmount = int.TryParse(filterProductID, out valueAmount);
                        filter.ProductID = valueAmount;
                    }
                    
                    filter.ContactName = filters.Where(a => a.Field.Equals("ContactName")).Select(a => a.Value).SingleOrDefault();
                    filter.ProductName = filters.Where(a => a.Field.Equals("ProductName")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("Time")).Any())
                    {
                        filterStartTime = DateTime.Parse(filters.Where(a => a.Field.Equals("Time")).Select(a => a.Value).SingleOrDefault());
                        filterEndTime = filterStartTime.Value.AddDays(1);
                    }
                    filterUTC = filters.Where(a => a.Field.Equals("IsUTC")).Select(a => a.Value).SingleOrDefault();
                    filter.Remark = filters.Where(a => a.Field.Equals("Remark")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("ModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("ModifiedDate")).Any())
                    {
                        filterModifiedStartDate = DateTime.Parse(filters.Where(a => a.Field.Equals("ModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        filterModifiedEndDate = filterModifiedStartDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.CustomerCallbacks
                                join b in context.CustomerContacts on a.ContactID equals b.ContactID
                                join c in context.Customers on b.CIF equals c.CIF
                                join d in context.Products on a.ProductID equals d.ProductID
                                join e in context.Employees on (a.UpdateBy != null ? a.UpdateBy : a.CreateBy) equals e.EmployeeUsername into ae
                                from e in ae.DefaultIfEmpty()
                                let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                let isProductID = filter.ProductID != null && filter.ProductID != 0
                                let isContactName = !string.IsNullOrEmpty(filter.ContactName)
                                let isProductName = !string.IsNullOrEmpty(filter.ProductName)
                                let isTime = filterStartTime.HasValue ? true : false
                                let isUTC = !string.IsNullOrEmpty(filterUTC)
                                let isRemark = !string.IsNullOrEmpty(filter.Remark)
                                let isLastModifiedBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isLastModifiedDate = filterModifiedStartDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    a.TransactionID == null &&
                                    (e.EmployeeID != null ? e.IsDeleted.Equals(false) : true) &&
                                    (isCIF ? c.CIF.Equals(filter.CIF) : true) &&
                                    (isProductID ? a.ProductID.Equals(filter.ProductID) : true) &&
                                    (isContactName ? b.ContactName.Contains(filter.ContactName) : true) &&
                                    (isProductName ? d.ProductName.Contains(filter.ProductName) : true) &&
                                    (isTime ? a.Time >= filterStartTime.Value && a.Time < filterEndTime.Value : true) &&
                                    (isUTC ? "Yes".Contains(filterUTC) ? a.IsUTC.Equals(true) : ("No".Contains(filterUTC) ? a.IsUTC.Equals(false) : false) : true) &&
                                    (isRemark ? a.Remark.Contains(filter.Remark) : true) &&
                                    (isLastModifiedBy ? (e.EmployeeName != null ? e.EmployeeName : (a.UpdateBy != null ? a.UpdateBy : a.CreateBy)).Contains(filter.LastModifiedBy) : true) &&
                                    (isLastModifiedDate ? (a.UpdateDate != null ? a.UpdateDate.Value : a.CreateDate) >= filterModifiedStartDate.Value && (a.UpdateDate != null ? a.UpdateDate.Value : a.CreateDate) < filterModifiedEndDate.Value : true)
                                select new CustomerCallbackModel
                                {
                                    ID = a.CallbackID,
                                    CIF = c.CIF,
                                    ContactID = a.ContactID,
                                    ContactName = b.ContactName,
                                    ProductName = d.ProductName,
                                    Time = a.Time,
                                    IsUTC = a.IsUTC,
                                    IsUTCDescription = a.IsUTC ? "Yes" : "No",
                                    Remark = a.Remark,
                                    LastModifiedBy = e.EmployeeName != null ? e.EmployeeName : (a.UpdateBy != null ? a.UpdateBy : a.CreateBy),
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                }).ToList();

                    CustomerCallback.Page = page;
                    CustomerCallback.Size = size;
                    CustomerCallback.Total = data.Count();
                    CustomerCallback.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    CustomerCallback.Rows = data.Select((a, i) => new CustomerCallbackRow
                    {
                        RowID = i + 1,
                        ID = a.ID,
                        CIF = a.CIF,
                        ContactID = a.ContactID,
                        ContactName = a.ContactName,
                        ProductName = a.ProductName,
                        Time = a.Time,
                        IsUTC = a.IsUTC,
                        IsUTCDescription = a.IsUTCDescription,
                        Remark = a.Remark,
                        TransactionID = a.TransactionID,
                        ApplicationID = a.ApplicationID,
                        LastModifiedBy = a.LastModifiedBy,
                        LastModifiedDate = a.LastModifiedDate
                    })
                    .Skip(skip)
                    .Take(size)
                    .Distinct()
                    .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool Add(CustomerCallbackModel CustomerCallback, ref string message)
        {
            bool isSuccess = false;
            try
            {
                context.CustomerCallbacks.Add(new Entity.CustomerCallback { 
                    ContactID = CustomerCallback.ContactID,
                    ProductID = CustomerCallback.ProductID,
                    Time = CustomerCallback.Time.ToUniversalTime(),
                    IsUTC = CustomerCallback.IsUTC,
                    Remark = CustomerCallback.Remark,
                    IsDeleted = false,
                    CreateDate = DateTime.UtcNow,
                    CreateBy = currentUser.GetCurrentUser().LoginName
                });
                context.SaveChanges();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool Utilize(IList<CustomerCallbackModel> CustomerCallback, ref string message)
        {
            bool isSuccess = false;
            try
            {
                foreach (var item in CustomerCallback)
	            {
		            Entity.CustomerCallback call = context.CustomerCallbacks.Where(a => a.CallbackID.Equals(item.ID)).SingleOrDefault();

                    if (call != null)
                    {
                        call.TransactionID = item.TransactionID;
                        call.UpdateDate = DateTime.UtcNow;
                        call.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        context.SaveChanges();
                    }
	            }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                        context.Dispose();
                }
            }
            this.disposed = true;
        }

        void IDisposable.Dispose()
        {

            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

        #endregion
    
}