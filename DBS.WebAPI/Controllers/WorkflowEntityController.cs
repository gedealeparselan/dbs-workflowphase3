﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class WorkflowEntityController : ApiController
    {
        private string message = string.Empty;

        private readonly IWorkflowEntityRepository repository = null;

        public WorkflowEntityController()
        {
            this.repository = new WorkflowEntityRepository();
        }

        public WorkflowEntityController(IWorkflowEntityRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all Roles.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<WorkflowEntityModel>))]
        public HttpResponseMessage Get()
        {
            IList<WorkflowEntityModel> output = new List<WorkflowEntityModel>();

            if (!repository.Get(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<WorkflowEntityModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Roles with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<WorkflowEntityModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<WorkflowEntityModel> output = new List<WorkflowEntityModel>();

            if (!repository.Get(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<WorkflowEntityModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Roles for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(WorkflowEntityGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, bool isHomeMenu, IList<WorkflowEntityFilter> filters)
        {
            WorkflowEntityGrid output = new WorkflowEntityGrid();
            if (isHomeMenu)
            {
                if (!repository.GetSPHome(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
                }
            }
            else {
                if (!repository.GetSP(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
                 {
                    return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
                }
            }
            return Request.CreateResponse<WorkflowEntityGrid>(HttpStatusCode.OK, output);            
        }

        /// <summary>
        /// Get Roles for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(WorkflowEntityGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<WorkflowEntityFilter> filters)
        {
            WorkflowEntityGrid output = new WorkflowEntityGrid();

            //if (!repository.Get(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            if (!repository.GetSP(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<WorkflowEntityGrid>(HttpStatusCode.OK, output);
            }
        }


        /// <summary>
        /// Find Roles using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<WorkflowEntityModel>))]
        [HttpGet]
        [Route("api/WorkflowEntity/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<WorkflowEntityModel> output = new List<WorkflowEntityModel>();

            if (!repository.Get(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<WorkflowEntityModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Role by ID.
        /// </summary>
        /// <param name="id">Role ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(WorkflowEntityModel))]
        [Route("api/WorkflowEntity/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(string id)
        {
            WorkflowEntityModel output = new WorkflowEntityModel();

            if (!repository.GetByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<WorkflowEntityModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Role by ID.
        /// </summary>
        /// <param name="id">Role ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(WorkflowEntityModel))]
        [Route("api/WorkflowEntity/Approve")]
        [HttpPost]
        public HttpResponseMessage Approve(WorkflowEntityModel rec)
        {
            WorkflowEntityModel output = new WorkflowEntityModel();

            if (!repository.ApproveByID(rec.WorkflowID.ToString(), ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<WorkflowEntityModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new Role.
        /// </summary>
        /// <param name="Role">Role data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(WorkflowEntityModel rec)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {

                if (!repository.Add(rec, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { 
                        Message = "New Workflow Entity data has been created.",
                        ID = message
                    });
                }
            }
        }

        /// <summary>
        /// Modify existing Role.
        /// </summary>
        /// <param name="id">ID of Role to be modify.</param>
        /// <param name="Role">Role data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/WorkflowEntity/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, WorkflowEntityModel role)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.Update(id, role, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Workflow Entity data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting Role.
        /// </summary>
        /// <param name="id">ID of Role to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/WorkflowEntity/{id}")]
        [Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.Delete(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Workflow Entity data has been deleted." });
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}