var cifData = '0';
var customerNameData = '';
var idrrate = 0;
var treshHold = 0;
var DocumentModels = ko.observable({ FileName: '', DocumentPath: '' });
//add by fandi
var Currency = [];
var StatementLetter = [];
var JoinAccount = [];
var JoinAccountNumber = [];
var UnderlyingDocument = [];
var ReferenceNumber = null;
var DocumentType = [];
var Rate_underworksheet = 0;
var Rate = 0;
var RateTemp = 0;
var $container;
var attachRow = 0;
var idrrate = 0;


var formatNumber = function (num) {
    if (ko.isObservable(num)) {
        num = num();
    }
    if (num != null) {
        num = num.toString().replace(/,/g, '');
        num = parseFloat(num).toFixed(2);
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    } else { return 0; }
}


var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};

var BizSegmentModel = function (id, name, desc) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(desc);
}

var BranchModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}

var CustomerTypeModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}

var UserApprovalModel = function (id, name, beneid, rankid, branchid) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.SegmentID = ko.observable(beneid);
    self.RankID = ko.observable(rankid);
    self.BranchID = ko.observable(branchid);
    self.FullName = ko.observable(id + ' - ' + name);
    self.Email = '';
}

var CurrencyModel = function (id, code, desc) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Description = ko.observable(desc);
    self.FullName = ko.observable(code + ' (' + desc + ')');
}

var EmployeeModel = function (id, name, username, email) {
    var self = this;
    self.EmployeeID = ko.observable(id);
    self.EmployeeName = ko.observable(name);
    self.EmployeeUserName = ko.observable(username);
    self.EmployeeEmail = ko.observable(email);
}

var SelectModel = function (cif, id) {
    var self = this;
    self.ID = ko.observable(0);
    self.CIF = ko.observable(cif);
    self.UnderlyingID = ko.observable(id);
    self.ParentID = ko.observable(0);
}

var SelectBulkModel = function (cif, id) {
    var self = this;
    self.ID = ko.observable(0);
    self.CIF = ko.observable(cif);
    self.UnderlyingID = ko.observable(id);
    self.ParentID = ko.observable(0);
}

var UnderlyingDocumentModel = function (id, name, code) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Name = ko.observable(name);
    self.CodeName = ko.observable(code + ' - ' + name);
    self.LastModifiedDate = ko.observable('');
    self.LastModifiedBy = ko.observable("");
}

var DocumentTypeModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.LastModifiedDate = ko.observable('');
    self.LastModifiedBy = ko.observable("");
}

var StatementLetterModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}

var DocumentPurposeModel = function (id, name, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(description);
    self.LastModifiedDate = ko.observable('');
    self.LastModifiedBy = ko.observable("");
}

var CustomerUnderlyingMappingModel = function (FileID, UnderlyingID, attachNo) {
    var self = this;
    self.ID = ko.observable(0);
    self.UnderlyingID = ko.observable(UnderlyingID);
    self.UnderlyingFileID = ko.observable(FileID);
    self.AttachmentNo = ko.observable(attachNo);
}

var POAFunctionModel = function (id, name, description, other) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(description);
    self.POAFunctionOther = ko.observable(other);

}

var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;
    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);
        if (date == '1970/01/01 00:00:00' || date == null) {
            return "";
        }

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return ""
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };
    $(".date-picker").each(function () {
        $(this).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
            $(this).prev().focus();
        });
    });

    $('.time-picker').timepicker({
        defaultTime: "",
        minuteStep: 1,
        showSeconds: false,
        showMeridian: false
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    self.CustomerCsoID = ko.observable(0);//add azam

    self.CIFJointAccounts = ko.observableArray([{ ID: false, Name: "Single" }, { ID: true, Name: "Join" }]);
    self.IsJointAccount = ko.observable(false);
    self.IsLocalGetSelectedRow = ko.observable(false);
    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);
    self.OnAfterGetParameters = null;
    self.IsEditTableUnderlying = ko.observable(true);
    self.IsInisiator = ko.observable(false);
    self.IsAnnualStatement = ko.observable(true);
    //Declare observable which will be bind with UI
    self.CIF = ko.observable();
    self.Name = ko.observable();
    self.POAName = ko.observable();
    self.BizSegment = ko.observable(new BizSegmentModel('', '', ''));
    self.Branch = ko.observable(new BranchModel('', ''));
    self.Type = ko.observable(new CustomerTypeModel('', ''));
    self.UserApproval = ko.observable(new UserApprovalModel(0, '', 0, 0, 0));
    self.RMCode = ko.observable();
    self.TotalEqvUsd = ko.observable();
    //self.RMName = ko.observable();
    self.LastModifiedBy = ko.observable();
    self.LastModifiedDate = ko.observable('');
    self.IsCitizen = ko.observable();
    self.IsResident = ko.observable();
    self.IsDormant = ko.observable();
    self.IsFreeze = ko.observable();

    //Declare observable which will be bind with UI
    self.ID_c = ko.observable("");
    self.CIF_c = ko.observable(cifData);
    self.SourceID_c = ko.observable("2");
    self.Name_c = ko.observable("");
    self.PhoneNumber_c = ko.observable("");
    self.DateOfBirth_c = ko.observable('');
    self.Address_c = ko.observable("");
    self.IDNumber_c = ko.observable("");
    self.OccupationInID_c = ko.observable("");
    self.PlaceOfBirth_c = ko.observable("");
    self.EffectiveDate_c = ko.observable("");
    self.CancellationDate_c = ko.observable("");
    self.ddlPOAFunction_c = ko.observableArray([]);
    self.cblPOAEmailProduct = ko.observableArray([]);

    self.LastModifiedBy_c = ko.observable("");
    self.LastModifiedDate_c = ko.observable('');
    self.IsFUnctionOther = ko.observable(false);

    self.CityCode_p = ko.observable();
    self.ContactType_p = ko.observable();
    self.CountryCode_p = ko.observable();
    self.PhoneNumber_p = ko.observable();
    self.UpdateBy_p = ko.observable();
    self.CreateBy = ko.observable();
    self.UpdateDate_p = ko.observable();
    //Declare observable which will be bind with UI
    self.ID_f = ko.observable(cifData);
    self.CIF_f = ko.observable("");
    self.POAFunction_f = ko.observable(new POAFunctionModel('', '', ''));
    self.ddlPOAFunction_f = ko.observableArray([]);
    self.LastModifiedBy_f = ko.observable("");
    self.LastModifiedDate_f = ko.observable('');

    // Underlying //Declare observable which will be bind with UI
    self.ID_u = ko.observable("");
    self.CIF_u = ko.observable(cifData);
    self.CustomerName_u = ko.observable(customerNameData);
    self.UnderlyingDocument_u = ko.observable(new UnderlyingDocumentModel('', '', ''));
    self.ddlUnderlyingDocument_u = ko.observableArray([]);
    self.DocumentType_u = ko.observable(new DocumentTypeModel('', ''));
    self.ddlDocumentType_u = ko.observableArray([]);
    self.Currency_u = ko.observable(new CurrencyModel('', '', ''));
    self.ddlCurrency_u = ko.observableArray([]);
    self.StatementLetter_u = ko.observable(new StatementLetterModel('', ''));
    self.ddlStatementLetter_u = ko.observableArray([]);
    self.DynamicStatementLetter_u = ko.observableArray([]);
    self.Amount_u = ko.observable(0);
    self.formattedAmount_u = ko.computed({
        read: function () {
            var n = self.Amount_u().toString(), p = n.indexOf('.');
            n = n.replace(/,/g, "");
            //var n = value.toString(), p = n.indexOf('.');
            var nVal = n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
                return p < 0 || i < p ? ($0 + ',') : $0;
            });
            self.Amount_u(nVal);
            //return self.Amount_u().toFixed(2);
        },
        write: function (value) {
            // Strip out unwanted characters, parse as float, then write the raw data back to the underlying "price" observable
            // value = parseFloat(value.replace(/[^\.\d]/g, ""));
            var n = value.toString(), p = n.indexOf('.');
            n = n.replace(/,/g, "");
            var nVal = n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
                return p < 0 || i < p ? ($0 + ',') : $0;
            });
            self.Amount_u(nVal); // Write to underlying storage
        },
        owner: self
    });
    self.Rate_u = ko.observable(0);
    self.AvailableAmount_u = ko.observable(0);
    self.AmountUSD_u = ko.observable(0);
    self.AttachmentNo_u = ko.observable("");
    self.SelectUnderlyingDocument_u = ko.observable("");
    self.IsDeclarationOfException_u = ko.observable(false);
    self.StartDate_u = ko.observable("");
    self.EndDate_u = ko.observable("");
    self.DateOfUnderlying_u = ko.observable("");
    self.ExpiredDate_u = ko.observable("");
    self.ReferenceNumber_u = ko.observable("");
    self.SupplierName_u = ko.observable("");
    self.InvoiceNumber_u = ko.observable("");
    self.IsStatementA = ko.observable(true);
    self.IsProforma_u = ko.observable(false);
    self.IsSelectedProforma = ko.observable(false);
    self.IsUtilize_u = ko.observable(false);
    self.IsJointAccount_u = ko.observable(false);
    self.AccountNumber_u = ko.observable("");
    // add bulkUnderlying // add 2015.03.09
    self.IsBulkUnderlying_u = ko.observable(false);
    self.IsSelectedBulk = ko.observable(false);
    // temp amount_u for bulk underlying
    self.tempAmountBulk = ko.observable(0);

    self.Proformas = ko.observableArray([new SelectModel('', '')]);
    self.BulkUnderlyings = ko.observableArray([new SelectBulkModel('', '')]);

    self.CodeUnderlying = ko.computed({
        read: function () {
            if (self.UnderlyingDocument_u().ID() != undefined && self.UnderlyingDocument_u().ID() != '') {
                var item = ko.utils.arrayFirst(self.ddlUnderlyingDocument_u(), function (x) {
                    return self.UnderlyingDocument_u().ID() == x.ID();
                }
                )
                if (item != null) {
                    return item.Code();
                }
                else {
                    return ''
                }
            } else {
                return ''
            }
        },
        write: function (value) {
            return value;
        }
    });
    self.OnChangeStatementLetterUnderlying = function () {
        if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementA_ID) {
            self.Currency_u().ID(1);
            self.Amount_u(treshHold);
            var date = new Date();
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var day = lastDay.getDate();
            var month = lastDay.getMonth() + 1;
            var year = lastDay.getFullYear();
            if (month > 12) {
                month -= 12;
                year += 1;
            }
            var fixLastDay = year + "/" + month + "/" + day;
            self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));

            SetDefaultValueStatementA(); // set default value untuk statement A
            GetRateIDRAmount(1); // USD
        } else if (self.StatementLetter_u().ID() == CONST_STATEMENT.AnnualStatement_ID) {
            self.Currency_u().ID(1);
            self.Amount_u(0);
            var date = new Date();
            var lastDay = new Date(date.getFullYear(), 12, 0);
            var day = lastDay.getDate();
            var month = lastDay.getMonth() + 1;
            var year = lastDay.getFullYear();
            if (month > 12) {
                month -= 12;
                year += 1;
            }
            var fixLastDay = year + "/" + month + "/" + day;
            self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
            SetDefaultValueStatementA();
        }

        else if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementB_ID && self.DateOfUnderlying_u() != "" && self.IsNewDataUnderlying()) {
            if (self.LocalDate(self.DateOfUnderlying_u()) != "") {
                var date = new Date(self.DateOfUnderlying_u());
                var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                var day = date.getDate();
                var month = lastDay.getMonth() + 13;
                var year = lastDay.getFullYear();
                if (month > 12) {
                    month -= 12;
                    year += 1;
                }
                var fixLastDay = year + "/" + month + "/" + day;
                if (!moment(fixLastDay, 'YYYY/MM/DD').isValid()) {
                    var LastDay_ = new Date(year, month, 0);
                    day = LastDay_.getDate();
                }
                self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
            }
        }
        if (typeof self.Amount_u() != 'number') {
            var value = parseFloat(self.Amount_u().replace(',', '').replace(',', '').replace(',', '').replace(',', ''));
        } else {
            var value = parseFloat(self.Amount_u());
        }
        var res = parseFloat(value) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
        res = Math.round(res * 100) / 100;
        res = isNaN(res) ? 0 : res; //avoid NaN
        self.AmountUSD_u(parseFloat(res).toFixed(2));
    }

    self.onChangeDateOfUnderlying = function () {
        if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementA_ID) {
            self.Currency_u().ID(1);
            self.Amount_u(treshHold);
            var date = new Date();
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var day = lastDay.getDate();
            var month = lastDay.getMonth() + 1;
            var year = lastDay.getFullYear();
            if (month > 12) {
                month -= 12;
                year += 1;
            }
            var fixLastDay = year + "/" + month + "/" + day;
            self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));

            //SetDefaultValueStatementA(); // set default value untuk statement A
            GetRateIDRAmount(1); // USD
        } else if (self.StatementLetter_u().ID() == CONST_STATEMENT.AnnualStatement_ID) {
            self.Currency_u().ID(1);
            self.Amount_u(0);
            var date = new Date();
            var lastDay = new Date(date.getFullYear(), 12, 0);
            var day = lastDay.getDate();
            var month = lastDay.getMonth() + 1;
            var year = lastDay.getFullYear();
            if (month > 12) {
                month -= 12;
                year += 1;
            }
            var fixLastDay = year + "/" + month + "/" + day;
            self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
            //SetDefaultValueStatementA();
        }

        else if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementB_ID && self.DateOfUnderlying_u() != "" && self.IsNewDataUnderlying()) {
            if (self.LocalDate(self.DateOfUnderlying_u()) != "") {
                var date = new Date(self.DateOfUnderlying_u());
                var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                var day = date.getDate();
                var month = lastDay.getMonth() + 13;
                var year = lastDay.getFullYear();
                if (month > 12) {
                    month -= 12;
                    year += 1;
                }
                var fixLastDay = year + "/" + month + "/" + day;
                if (!moment(fixLastDay, 'YYYY/MM/DD').isValid()) {
                    var LastDay_ = new Date(year, month, 0);
                    day = LastDay_.getDate();
                }
                self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
            }
        }
        if (typeof self.Amount_u() != 'number') {
            var value = parseFloat(self.Amount_u().replace(',', '').replace(',', '').replace(',', '').replace(',', ''));
        } else {
            var value = parseFloat(self.Amount_u());
        }
        var res = parseFloat(value) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
        res = Math.round(res * 100) / 100;
        res = isNaN(res) ? 0 : res; //avoid NaN
        self.AmountUSD_u(parseFloat(res).toFixed(2));
    }


    self.OtherUnderlying_u = ko.observable('');
    self.tmpOtherUnderlying = ko.observable('');
    self.IsSelected_u = ko.observable(false);
    self.LastModifiedBy_u = ko.observable("");
    self.LastModifiedDate_u = ko.observable('');

    //Account Declare observable which will be bind with UI
    self.CIF_a = ko.observable(cifData);
    self.AccountNumber_a = ko.observable("");
    self.Currency_a = ko.observable(new CurrencyModel('', '', ''));
    self.ddlCurrency_a = ko.observableArray([]);
    self.LastModifiedBy_a = ko.observable("");
    self.LastModifiedDate_a = ko.observable('');

    // Attach //Declare observable which will be bind with UI
    self.ID_a = ko.observable(0);
    self.FileName_a = ko.observable('');
    self.DocumentPurpose_a = ko.observable(new DocumentPurposeModel('', '', ''));
    self.ddlDocumentPurpose_a = ko.observableArray([]);
    self.DocumentType_a = ko.observable(new DocumentTypeModel('', ''));
    self.ddlDocumentType_a = ko.observableArray([]);
    self.DocumentRefNumber_a = ko.observable("");
    self.AttachmentReference_a = ko.observable("");
    self.LastModifiedBy_a = ko.observable("");
    self.LastModifiedDate_a = ko.observable('');
    self.CustomerUnderlyingMappings_a = ko.observableArray([new CustomerUnderlyingMappingModel('', '', '')]);
    self.Documents = ko.observableArray([]);
    self.DocumentPath_a = ko.observable("");


    // Declare parameters
    self.Branchs = ko.observableArray([]);
    self.BizSegments = ko.observableArray([]);
    self.Types = ko.observableArray([]);
    self.POAFunctions = ko.observableArray([]);
    self.UserApprovals = ko.observableArray([]);
    //add by fandi handsontable for dropdown StatmentLetter,UnderLayDoc,TypeOfDoc,TransCurr
    self.statementletter = ko.observableArray([]);
    self.underlayDoc = ko.observableArray([]);
    self.TypeofDoc = ko.observableArray([]);
    self.TransCurrency = ko.observableArray([]);
    self.Products = ko.observableArray([]);

    //Parameter for Callback
    self.CallbackReadOnly = ko.observable(false);
    self.CallbackCIF = ko.observable("");
    self.CallbackContactID = ko.observable("");
    self.CallbackContactName = ko.observable("");
    self.CallbackProductID = ko.observable("");
    self.CallbackProductName = ko.observable("");
    self.CallbackTime = ko.observable("");
    self.CallbackIsUTC = ko.observable("");
    self.CallbackRemark = ko.observable("");
    //end by fandi
    self.fixUserApprovals = ko.computed(function () {
        return ko.utils.arrayFilter(self.UserApprovals(), function (x) {
            return x.SegmentID() == self.BizSegment().ID() && x.BranchID() == self.Branch().ID();
        });
    });

    // filter
    self.FilterCIF = ko.observable('');
    self.FilterName = ko.observable("");
    self.FilterPOAName = ko.observable("");
    self.FilterBizSegment = ko.observable("");
    self.FilterBranch = ko.observable("");
    self.FilterType = ko.observable("");
    self.FilterLastModifiedBy = ko.observable("");
    self.FilterLastModifiedDate = ko.observable('');

    // filter POA Function
    self.FilterPOAFunctionCIF = ko.observable(cifData);
    self.FilterPOAFunctionName = ko.observable("");
    self.FilterPOAFunctionDescription = ko.observable("");

    // filter Customer Contact
    self.FilterCIF_c = ko.observable(cifData);
    self.FilterName_c = ko.observable("");
    self.FilterPhoneNumber_c = ko.observable("");
    self.FilterDateOfBirth_c = ko.observable("");
    //self.FilterAddress_c = ko.observable("");
    self.FilterOccupationInID_c = ko.observable("");
    self.FilterPlaceOfBirth_c = ko.observable("");
    self.FilterEffectiveDate_c = ko.observable("");
    self.FilterCancellationDate_c = ko.observable("");
    self.FilterIDNumber_c = ko.observable("");
    self.FilterPOAFunction_c = ko.observable("");
    //filter Callback Customer Contact
    self.FilterCallbackContactCIF = ko.observable(cifData);
    self.FilterCallbackContactName = ko.observable("");
    self.FilterCallbackContactPhoneNumber = ko.observable("");
    self.FilterCallbackContactDateOfBirth = ko.observable("");
    self.FilterCallbackContactIDNumber = ko.observable("");
    self.FilterCallbackContactPOAFunction = ko.observable("");
    self.FilterCallbackContactOccupationInID = ko.observable("");
    self.FilterCallbackContactPlaceOfBirth = ko.observable("");
    self.FilterCallbackContactEffectiveDate = ko.observable("");
    self.FilterCallbackContactCancellationDate = ko.observable("");
    //filter Callback Time
    self.FilterCallbackCIF = ko.observable(cifData);
    self.FilterCallbackNameContact = ko.observable("");
    self.FilterCallbackProductName = ko.observable("");
    self.FilterCallbackTime = ko.observable("");
    self.FilterCallbackRemark = ko.observable("");
    self.FilterCallbackIsUTC = ko.observable("");
    self.FilterCallbackModifiedBy = ko.observable("");
    self.FilterCallbackModifiedDate = ko.observable("");
    //filter Callback Time Draft
    self.FilterCallbackDraftCIF = ko.observable(cifData);
    self.FilterPOAEmailDraftCIF = ko.observable(cifData);
    self.FilterCallbackDraftTaskName = ko.observable("");
    self.FilterCallbackDraftActionType = ko.observable("");
    self.FilterCallbackDraftModifiedBy = ko.observable("");
    self.FilterCallbackDraftModifiedDate = ko.observable("");
    //filter CustomerPhoneNumber
    self.FilterCityCode = ko.observable("");
    self.FilterContactType = ko.observable("");
    self.FilterCountryCode = ko.observable("");
    self.FilterPhoneNumber = ko.observable("");
    self.FilterUpdateBy = ko.observable("");
    self.FilterCreateBy = ko.observable("");
    self.FilterUpdateDate = ko.observable("");
    //filter Customer_POAE
    self.FilterPOAEmail_POAE = ko.observable("");
    self.FilterUpdateBy_POAE = ko.observable("");
    self.FilterCreateBy_POAE = ko.observable("");
    self.FilterUpdateDate_POAE = ko.observable("");
    // filter Underlying
    self.UnderlyingFilterCIF = ko.observable("");
    self.UnderlyingFilterParentID = ko.observable("");
    self.UnderlyingFilterIsSelected = ko.observable(false);
    self.UnderlyingFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingFilterDocumentType = ko.observable("");
    self.UnderlyingFilterCurrency = ko.observable("");
    self.UnderlyingFilterStatementLetter = ko.observable("");
    self.UnderlyingFilterAmount = ko.observable("");
    self.UnderlyingFilterAvailableAmount = ko.observable("");
    self.UnderlyingFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingFilterExpiredDate = ko.observable("");
    self.UnderlyingFilterReferenceNumber = ko.observable("");
    self.UnderlyingFilterSupplierName = ko.observable("");
    self.UnderlyingFilterIsProforma = ko.observable("");
    self.UnderlyingFilterInvoiceNumber = ko.observable("");
    self.UnderlyingFilterAccountNumber = ko.observable("");
    self.UnderlyingFilterIsJointAccount = ko.observable("");
    self.UnderlyingFilterIsExpired = ko.observable("");
    self.UnderlyingFilterAttachmentNo = ko.observable("");
    self.UnderlyingFilterAccountNumber = ko.observable("");
    self.UnderlyingFilterShow = ko.observable(1);

    // filter Underlying Draft
    self.UnderlyingDraftFilterIsSelected = ko.observable(false);
    self.UnderlyingDraftFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingDraftFilterDocumentType = ko.observable("");
    self.UnderlyingDraftFilterCurrency = ko.observable("");
    self.UnderlyingDraftFilterStatementLetter = ko.observable("");
    self.UnderlyingDraftFilterAmount = ko.observable("");
    self.UnderlyingDraftFilterAvailableAmount = ko.observable("");
    self.UnderlyingDraftFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingDraftFilterExpiredDate = ko.observable("");
    self.UnderlyingDraftFilterReferenceNumber = ko.observable("");
    self.UnderlyingDraftFilterSupplierName = ko.observable("");
    self.UnderlyingDraftFilterIsProforma = ko.observable("");
    self.UnderlyingDraftFilterInvoiceNumber = ko.observable("");
    self.UnderlyingDraftFilterAccountNumber = ko.observable("");
    self.UnderlyingDraftFilterIsJointAccount = ko.observable("");
    self.UnderlyingDraftFilterAttachmentNo = ko.observable("");
    self.UnderlyingDraftFilterShow = ko.observable(1);

    // filter Attach Underlying File
    self.AttachFilterFileName = ko.observable("");
    self.AttachFilterDocumentPurpose = ko.observable("");
    self.AttachFilterModifiedDate = ko.observable('');
    self.AttachFilterDocumentRefNumber = ko.observable("");
    self.AttachFilterAttachmentReference = ko.observable("");
    self.AttachFilterAccountNumber = ko.observable("");

    // filter Underlying for Attach
    self.UnderlyingAttachFilterIsSelected = ko.observable("");
    self.UnderlyingAttachFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingAttachFilterDocumentType = ko.observable("");
    self.UnderlyingAttachFilterCurrency = ko.observable("");
    self.UnderlyingAttachFilterStatementLetter = ko.observable("");
    self.UnderlyingAttachFilterAmount = ko.observable("");
    self.UnderlyingAttachFilterAvailableAmount = ko.observable("");
    self.UnderlyingAttachFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingAttachFilterExpiredDate = ko.observable("");
    self.UnderlyingAttachFilterReferenceNumber = ko.observable("");
    self.UnderlyingAttachFilterSupplierName = ko.observable("");
    self.UnderlyingAttachFilterAccountNumber = ko.observable("");

    // filter Underlying for Attach
    self.ProformaFilterIsSelected = ko.observable(false);
    self.ProformaFilterUnderlyingDocument = ko.observable("");
    self.ProformaFilterDocumentType = ko.observable("Proforma");
    self.ProformaFilterCurrency = ko.observable("");
    self.ProformaFilterStatementLetter = ko.observable("");
    self.ProformaFilterAmount = ko.observable("");
    self.ProformaFilterDateOfUnderlying = ko.observable("");
    self.ProformaFilterExpiredDate = ko.observable("");
    self.ProformaFilterReferenceNumber = ko.observable("");
    self.ProformaFilterSupplierName = ko.observable("");
    self.ProformaFilterInvoiceNumber = ko.observable("");
    self.ProformaFilterIsProforma = ko.observable(false);

    // filter Underlying for Attach // add 2015.03.09
    self.BulkFilterIsSelected = ko.observable(false);
    self.BulkFilterUnderlyingDocument = ko.observable("");
    self.BulkFilterDocumentType = ko.observable("");
    self.BulkFilterCurrency = ko.observable("");
    self.BulkFilterStatementLetter = ko.observable("");
    self.BulkFilterAmount = ko.observable("");
    self.BulkFilterDateOfUnderlying = ko.observable("");
    self.BulkFilterExpiredDate = ko.observable("");
    self.BulkFilterReferenceNumber = ko.observable("");
    self.BulkFilterSupplierName = ko.observable("");
    self.BulkFilterInvoiceNumber = ko.observable("");
    self.BulkFilterIsBulkUnderlying = ko.observable(false);


    // filter Customer Account
    self.FilterCIF_a = ko.observable(cifData);
    self.FilterAccountNumber_a = ko.observable("");
    self.FilterCurrencyCode_a = ko.observable("");
    self.FilterCurrencyDesc_a = ko.observable("");
    self.FilterIsJointAccount_a = ko.observable("")
    self.FilterModifiedBy_a = ko.observable("");
    self.FilterModifiedDate_a = ko.observable('');

    self.Employee = ko.observable(new EmployeeModel('', '', '', ''));
    self.EmployeeCSOID_c = ko.observable("0");
    self.FilterEmployeeName_c = ko.observable("");
    self.FilterEmployeeEmail_c = ko.observable("");
    self.FilterBizSegmentName_c = ko.observable("");
    self.FilterLocationName_c = ko.observable("");
    self.FilterModifiedBy_c = ko.observable("");
    self.FilterModifiedDate_c = ko.observable("");


    //start add azam
    self.FilterTaskName = ko.observable("");
    self.FilterValue = ko.observable("");
    self.FilterActionType = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");
    //end azam

    //// start Checked Calculate for Attach Grid and Underlying Grid
    self.TempSelectedAttachUnderlying = ko.observableArray([]);

    //// start Checked Calculate for Underlying Proforma Grid
    self.TempSelectedUnderlyingProforma = ko.observableArray([]);

    //// start Checked Calculate for Underlying Bulk Grid // add 2015.03.09
    self.TempSelectedUnderlyingBulk = ko.observableArray([]);

    // disable when proses data
    self.IsEditable = ko.observable(true);

    // New Data flag
    self.IsGridEnable = ko.observable(true);
    self.IsEditCustomerContact = ko.observable(true);
    self.IsEditCustomerUnderlying = ko.observable(true);
    self.IsNewData = ko.observable(true);
    self.IsNewFunctionData = ko.observable(false);
    self.IsNewData_c = ko.observable(false);
    self.IsNewData_POAE = ko.observable(false);
    self.IsNewDataUnderlying = ko.observable(false);
    self.IsViewWorkflow = ko.observable(false);
    self.IsNewAccountData = ko.observable(false);
    self.IsNewCSOData = ko.observable(false);
    self.IsEditCSOData = ko.observable(false);

    self.IsCustomerCSOViewer = ko.observable(false);
    self.IsCustomerCallbackViewer = ko.observable(false);
    self.IsCustomerPhoneNumberViewer = ko.observable(false);
    self.IsCustomerPOAEmailViewer = ko.observable(false);

    self.IsCustomerCSOMaker = ko.observable(false);
    self.IsCustomerCallbackMaker = ko.observable(false);
    self.IsCustomerPOAEmailMaker = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.Customers = ko.observableArray([]);
    self.CustomerAccounts = ko.observableArray([]);
    self.CustomerFunctions = ko.observableArray([]);
    self.CustomerContacts = ko.observableArray([]);
    self.CustomerPOAEmails = ko.observableArray([]);
    self.CustomerCallbackContacts = ko.observableArray([]);
    self.CustomerCallbacks = ko.observableArray([]);
    self.CustomerCallbackDrafts = ko.observableArray([]);
    self.CustomerPhoneNumbers = ko.observableArray([]);
    self.CustomerUnderlyings = ko.observableArray([]);
    self.CustomerUnderlyingDrafts = ko.observableArray([]);
    self.CustomerCsoDrafts = ko.observableArray([]); // add azam
    self.CustomerUnderlyingProformas = ko.observableArray([]);
    self.CustomerUnderlyingFiles = ko.observableArray([]);
    self.CustomerAttachUnderlyings = ko.observableArray([]);
    self.CustomerBulkUnderlyings = ko.observableArray([]); // add 2015.03.09
    self.CustomerCSO = ko.observableArray([]);
    self.CustomerUnderlyingHistories = ko.observableArray([]);
    self.CustomerUnderlyingTransactionHistories = ko.observableArray([]);
    self.IsContactMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));
    // customer function grid properties
    self.FunctionsGridProperties = ko.observable();
    self.FunctionsGridProperties(new GridPropertiesModel(GetFunctionData));
    // grid properties
    self.ConctactGridProperties = ko.observable();
    self.ConctactGridProperties(new GridPropertiesModel(GetDataContact));
    self.CallbackContactGridProperties = ko.observable();
    self.CallbackContactGridProperties(new GridPropertiesModel(GetDataCallbackContact));
    self.CallbackGridProperties = ko.observable();
    self.CallbackGridProperties(new GridPropertiesModel(GetDataCallback));
    self.CallbackDraftGridProperties = ko.observable();
    self.CallbackDraftGridProperties(new GridPropertiesModel(GetDataCallbackDraft));
    self.PhoneNumberGridProperties = ko.observable();
    self.PhoneNumberGridProperties(new GridPropertiesModel(GetDataPhoneNumber));
    self.POAEmailGridProperties = ko.observable();
    self.POAEmailGridProperties(new GridPropertiesModel());
    // grid properties Underlying
    self.UnderlyingGridProperties = ko.observable();
    self.UnderlyingGridProperties(new GridPropertiesModel(GetDataUnderlying));
    // grid properties Underlying Draft
    self.UnderlyingDraftGridProperties = ko.observable();
    self.UnderlyingDraftGridProperties(new GridPropertiesModel(GetDataUnderlyingDraft));
    // grid properties Attach Grid
    self.AttachGridProperties = ko.observable();
    self.AttachGridProperties(new GridPropertiesModel(GetDataAttachFile));
    // grid properties Underlying File
    self.UnderlyingAttachGridProperties = ko.observable();
    self.UnderlyingAttachGridProperties(new GridPropertiesModel(GetDataUnderlyingAttach));
    // grid properties Proforma
    self.UnderlyingProformaGridProperties = ko.observable();
    self.UnderlyingProformaGridProperties(new GridPropertiesModel(GetDataUnderlyingProforma));
    // grid properties Bulk // 2015.03.09
    self.UnderlyingBulkGridProperties = ko.observable();
    self.UnderlyingBulkGridProperties(new GridPropertiesModel(GetDataBulkUnderlying));
    // grid properties Customer Account
    self.AccountGridProperties = ko.observable();
    self.AccountGridProperties(new GridPropertiesModel(GetAccountData));

    self.CSOGridProperties = ko.observable();
    self.CSOGridProperties(new GridPropertiesModel(GetDataCSO));
    self.CSOGridProperties().SortColumn("Employee.EmployeeName");
    self.CSOGridProperties().SortOrder("ASC");


    //start add azam
    // grid properties
    self.CustomerCsoDraftGridProperties = ko.observable();
    self.CustomerCsoDraftGridProperties(new GridPropertiesModel(GetDataCsoDraft));

    // set default sorting
    self.CustomerCsoDraftGridProperties().SortColumn("LastModifiedDate");
    self.CustomerCsoDraftGridProperties().SortOrder("DESC");

    //end

    // set default sorting
    self.GridProperties().SortColumn("Name");
    self.GridProperties().SortOrder("ASC");
    // set default sorting
    self.FunctionsGridProperties().SortColumn("CIF");
    self.FunctionsGridProperties().SortOrder("ASC");
    // set default sorting
    self.ConctactGridProperties().SortColumn("NameContact");
    self.ConctactGridProperties().SortOrder("ASC");
    self.CallbackContactGridProperties().SortColumn("NameContact");
    self.CallbackContactGridProperties().SortOrder("ASC");
    self.CallbackGridProperties().SortColumn("ContactName");
    self.CallbackGridProperties().SortOrder("ASC");
    self.CallbackDraftGridProperties().SortColumn("LastModifiedDate");
    self.CallbackDraftGridProperties().SortOrder("DESC");
    // set default sorting for Underlying Grid
    self.UnderlyingGridProperties().SortColumn("ID");
    self.UnderlyingGridProperties().SortOrder("ASC");
    // set default sorting for Underlying Draft Grid
    self.UnderlyingDraftGridProperties().SortColumn("ID");
    self.UnderlyingDraftGridProperties().SortOrder("DESC");
    // set default sorting for Underlying File Grid
    self.AttachGridProperties().SortColumn("FileName");
    self.AttachGridProperties().SortOrder("ASC");
    // set default sorting for Attach Underlying Grid
    self.UnderlyingAttachGridProperties().SortColumn("ID");
    self.UnderlyingAttachGridProperties().SortOrder("ASC");
    // set default sorting for Proforma grid
    self.UnderlyingProformaGridProperties().SortColumn("IsSelectedProforma");
    self.UnderlyingProformaGridProperties().SortOrder("DESC");
    // set default sorting for Bulk grid //add 2015.03.09
    self.UnderlyingBulkGridProperties().SortColumn("IsSelectedBulk");
    self.UnderlyingBulkGridProperties().SortOrder("DESC");
    // set default sorting
    self.AccountGridProperties().SortColumn("AccountNumber");
    self.AccountGridProperties().SortOrder("ASC");
    self.PhoneNumberGridProperties().SortColumn("PhoneNumber");
    self.PhoneNumberGridProperties().SortOrder("ASC");
    self.POAEmailGridProperties().SortColumn("POAEmail");
    self.POAEmailGridProperties().SortOrder("ASC");
    //start add azam

    //hidayat  Declare Properties
    self.Area_c = ko.observable("");
    self.Phone_c = ko.observable("");
    self.Ext_c = ko.observable("");
    var PhoneNumber = self.Area_c("") + "-" + self.Phone_c("") + "-" + self.Ext_c("");
    self.PhoneNumber_c = ko.observable(PhoneNumber);
    self.POAFunctionOther_c = ko.observable("");
    self.POAFunction_c = ko.observable(POAFunctionModel);
    self.POAFunctionID_c = ko.observable(0);
    self.POAFunctions_c = ko.observableArray([]);
    self.AccountNumbers_c = ko.observableArray([]);
    self.AccountNumber_c = ko.observable();
    self.IsNewData_AccountNumber_c = ko.observable();
    self.IsNewData_POAFunction_c = ko.observable();
    self.AccNumIndex = ko.observable();
    self.POAIndex = ko.observable();

    //End hidayat  Declare Properties

    //hidayat Declare Function
    self.AddAccountNumber_c = function () {
        console.log("dayat")
        self.IsNewData_AccountNumber_c(true);
        self.AccountNumber_c("");
    }
    self.UpdateAccountNumber_c = function (index) {
        self.IsNewData_AccountNumber_c(false);
        self.AccountNumber_c(self.AccountNumbers_c()[index]);
        self.AccNumIndex(index);
    }
    self.RemoveAccountNumber_c = function (index) {
        var item = self.AccountNumbers_c()[index]
        self.AccountNumbers_c.remove(item);
        console.log(self.AccountNumbers_c())
    }
    self.AccountNumberSave = function () {
        console.log("aa")
        if (self.AccountNumber_c() != null && self.AccountNumber_c() != undefined && self.AccountNumber_c() != "") {
            if (self.AccountNumbers_c().length > 0) {
                var item = ko.utils.arrayFirst(self.AccountNumbers_c(), function (x) { return x == self.AccountNumber_c(); });
                if (item) {
                    ShowNotification("Attention", "Account Number Other Can't Duplicate or Empty", 'gritter-warning', false);
                    return false
                } else {
                    self.AccountNumbers_c.push(self.AccountNumber_c());
                }
            } else {
                self.AccountNumbers_c.push(self.AccountNumber_c());
            }

            $("#AccountNumber-modal-form").modal('hide');
        } else {
            ShowNotification("Attention", "Account Number Can't Empty", 'gritter-warning', false);
            return false
        }

    }
    self.AccountNumberUpdate = function () {
        var xx;
        if (self.AccountNumber_c() != null && self.AccountNumber_c() != undefined && self.AccountNumber_c() != "") {
            for (var i = 0; i < self.AccountNumbers_c().length ; i++) {
                if (self.AccountNumber_c().toLowerCase() == self.AccountNumbers_c()[i].toLowerCase()) {
                    if (i == self.AccNumIndex()) {
                        self.AccountNumbers_c()[self.AccNumIndex()] = self.AccountNumber_c();
                        $("#AccNum" + index).text(self.AccountNumbers_c()[self.AccNumIndex()]);
                        $("#AccountNumber-modal-form").modal('hide');
                    } else {
                        ShowNotification("Attention", "Account Number Can't Duplicate", 'gritter-warning', false);
                        return false;
                    }
                } else {
                    xx = true;
                }
            }

            for (var i = 0; i < self.AccountNumbers_c().length ; i++) {
                if (i == self.AccNumIndex() && xx == true) {
                    self.AccountNumbers_c()[self.AccNumIndex()] = self.AccountNumber_c();
                    $("#AccountNumber-modal-form").modal('hide');
                    $("#AccNum" + self.AccNumIndex()).text(self.AccountNumbers_c()[self.AccNumIndex()]);
                }

            }
        } else {
            ShowNotification("Attention", "Account Number Can't Empty", 'gritter-warning', false);
            return false
        }
    }


    //POAFunction
    self.AddPOAFunction_c = function () {
        self.POAFunctionID_c(void 0);
        console.log("dayat");
        self.IsNewData_POAFunction_c(true);
        self.POAFunction_c(new POAFunctionModel(0, "", "", ""));
        $("#functionOther").val("");
        $("#Other").hide();
        $("#matrix").val("");

    }
    self.UpdatePOAFunction_c = function (index) {
        self.IsNewData_POAFunction_c(false);
        self.POAFunction_c(self.POAFunctions_c()[index]);
        if (!ko.isObservable(self.POAFunction_c())) {
            $('#matrix').val(self.POAFunctions_c()[index].ID())
        }
        if (self.POAFunctions_c()[index].ID() == 9) {
            self.POAFunctionID_c(9);
            self.POAFunctionOther_c(self.POAFunctions_c()[index].POAFunctionOther());
        } else {
            self.POAFunctionID_c(0);
            self.POAFunctionOther_c("")
        }
        self.POAIndex(index);
    }
    self.RemovePOAFunction_c = function (index) {
        var item = self.POAFunctions_c()[index]
        self.POAFunctions_c.splice(index, 1);
        console.log(self.POAFunctions_c())
    }
    self.POAFunctionSave = function () {
        if (self.POAFunctionID_c() != null && self.POAFunctionID_c() != undefined && self.POAFunctionID_c() != "") {
            if (self.POAFunctions_c().length > 0) {
                var item = ko.utils.arrayFirst(self.POAFunctions_c(), function (x) {
                    var result;
                    if (self.POAFunction_c().ID == 9) {
                        result = x.POAFunctionOther() == self.POAFunctionOther_c();
                    } else {
                        result = x.ID() == self.POAFunction_c().ID;
                    }
                    return result
                });
                if (item) {
                    ShowNotification("Attention", "POA Function Other Can't Duplicate or Empty", 'gritter-warning', false);
                    return false
                } else {
                    self.POAFunctions_c.push(new POAFunctionModel(self.POAFunction_c().ID, self.POAFunction_c().Name, self.POAFunction_c().Description, self.POAFunctionOther_c()));
                    $("#POAFunction-modal-form").modal('hide');
                    $("#POAF" + (self.POAFunctions_c().length - 1)).text(self.POAFunctionID_c() == 9 ? (self.POAFunctions_c()[(self.POAFunctions_c().length - 1)].Description() + " - " + self.POAFunctions_c()[(self.POAFunctions_c().length - 1)].POAFunctionOther()) : self.POAFunctions_c()[(self.POAFunctions_c().length - 1)].Description());
                }
            } else {
                if (self.POAFunction_c().ID == 9 && self.POAFunctionOther_c() == "") {
                    ShowNotification("Attention", "POA Function Other Can't Empty", 'gritter-warning', false);
                    return false
                } else {
                    self.POAFunctions_c.push(new POAFunctionModel(self.POAFunction_c().ID, self.POAFunction_c().Name, self.POAFunction_c().Description, self.POAFunctionOther_c()));
                    $("#POAFunction-modal-form").modal('hide');
                    $("#POAF" + (self.POAFunctions_c().length - 1)).text(self.POAFunctionID_c() == 9 ? (self.POAFunctions_c()[(self.POAFunctions_c().length - 1)].Description() + " - " + self.POAFunctions_c()[(self.POAFunctions_c().length - 1)].POAFunctionOther()) : self.POAFunctions_c()[(self.POAFunctions_c().length - 1)].Description());

                }
            }
        } else {
            ShowNotification("Attention", "POA Function Can't Empty", 'gritter-warning', false);
            return false
        }
        for (var i = 0; i < self.POAFunctions_c().length; i++) {
            $("#POAF" + i).text(self.POAFunctions_c()[i].ID() == 9 ? (self.POAFunctions_c()[i].Description() + " - " + self.POAFunctions_c()[i].POAFunctionOther()) : self.POAFunctions_c()[i].Description());
        }
    }
    self.POAFunctionUpdate = function () {
        var Description = ko.isObservable(self.POAFunction_c().Description) ? self.POAFunction_c().Description() : self.POAFunction_c().Description;
        if (self.POAFunctionID_c() != null && self.POAFunctionID_c() != undefined && self.POAFunctionID_c() != "") {
            if (self.POAFunctions_c().length > 0) {
                var xx;
                for (var i = 0; i < self.POAFunctions_c().length ; i++) {
                    if (self.POAFunctions_c()[i].ID() == self.POAFunctionID_c()) {
                        if (i == self.POAIndex()) {
                            if (self.POAFunctionID_c() == 9 && self.POAFunctionOther_c() == "") {
                                ShowNotification("Attention", "POA Function Other Can't Empty", 'gritter-warning', false);
                                return false
                            } else {
                                self.POAFunctions_c()[self.POAIndex()] = new POAFunctionModel(self.POAFunctionID_c(), self.POAFunction_c().Name, Description, self.POAFunctionOther_c());
                                $("#POAFunction-modal-form").modal('hide');
                                $("#POAF" + self.POAIndex()).text(self.POAFunctionID_c() == 9 ? (self.POAFunctions_c()[self.POAIndex()].Description() + " - " + self.POAFunctionOther_c()) : self.POAFunctions_c()[self.POAIndex()].Description());

                            }
                        }
                        else {
                            if (self.POAFunctions_c()[i].ID == 9) {
                                if (self.POAFunctionOther_c() == self.POAFunctions_c()[i].POAFunctionOther || self.POAFunctionOther_c() == "") {
                                    ShowNotification("Attention", "POA Function Other Can't Duplicate or Empty", 'gritter-warning', false);
                                    return false
                                } else {
                                    self.POAFunctions_c.push(new POAFunctionModel(self.POAFunctionID_c(), self.POAFunction_c().Name(), Description, self.POAFunctionOther_c()));
                                    $("#POAFunction-modal-form").modal('hide');
                                    $("#POAF" + self.POAIndex()).text(self.POAFunctionID_c() == 9 ? (self.POAFunctions_c()[self.POAIndex()].Description() + " - " + self.POAFunctionOther_c()) : self.POAFunctions_c()[self.POAIndex()].Description());
                                }
                            } else {
                                ShowNotification("Attention", "POA Function Can't Duplicate or Empty", 'gritter-warning', false);
                                return false
                            }
                        }
                    }
                    else {
                        xx = true;
                    }

                    //if (self.POAFunctions_c().length) {
                    //    //$("#POAFunction-modal-form").modal('hide');
                    //    //$("#POAF" + self.POAIndex()).text(self.POAFunctions_c()[self.POAIndex()].Description());

                    //}
                }


                for (var i = 0; i < self.POAFunctions_c().length ; i++) {
                    if (i == self.POAIndex() && xx == true) {
                        if (self.POAFunctionID_c() == 9 && self.POAFunctionOther_c() == "") {
                            ShowNotification("Attention", "POA Function Other Can't Empty", 'gritter-warning', false);
                            return false
                        } else {
                            self.POAFunctions_c()[self.POAIndex()] = new POAFunctionModel(self.POAFunctionID_c(), self.POAFunction_c().Name, Description, self.POAFunctionOther_c());
                            $("#POAFunction-modal-form").modal('hide');
                            $("#POAF" + self.POAIndex()).text(self.POAFunctionID_c() == 9 ? (self.POAFunctions_c()[self.POAIndex()].Description() + " - " + self.POAFunctionOther_c()) : self.POAFunctions_c()[self.POAIndex()].Description());
                        }
                    }
                }
            }
        } else {
            ShowNotification("Attention", "POA Function Can't Empty", 'gritter-warning', false);
            return false
        }
    }

    self.OnchangePOAFunction = function (absc, sValue) {
        if (sValue.currentTarget.selectedIndex != null && sValue.currentTarget.selectedIndex != undefined) {
            var POAItem = ko.utils.arrayFirst(self.ddlPOAFunction_c(), function (item) {
                return item.ID == sValue.currentTarget.selectedIndex;
            });

            if (POAItem != null) {
                self.POAFunction_c(POAItem);
            }
            self.POAFunctionID_c(sValue.currentTarget.selectedIndex);
            if (sValue.currentTarget.selectedIndex != 9) {
                self.POAFunctionOther_c('');
            }
        }
    }

    //End hidayat Declare Function
    self.ClearCustomerCsoFilters = function () {
        self.FilterTaskName("");
        self.FilterActionType("");
        self.FilterValue("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");
        GetDataCsoDraft();
    };
    self.Rows = ko.observableArray([]);
    //end
    // bind clear filters
    self.ClearFilters = function () {
        self.FilterCIF("");
        self.FilterName("");
        self.FilterPOAName("");
        self.FilterBizSegment("");
        self.FilterBranch("");
        self.FilterType("");
        self.FilterLastModifiedBy("");
        self.FilterLastModifiedDate('');

        GetData();
    };

    // bind Customer Function clear filters
    self.ClearFunctionFilters = function () {
        self.FilterPOAFunctionCIF(cifData);
        self.FilterPOAFunctionName("");
        self.FilterPOAFunctionDescription("");

        GetFunctionData();
    };

    // bind Customer Contact clear filters
    self.ClearContactFilters = function () {
        self.FilterCIF_c(cifData);
        self.FilterName_c("");
        self.FilterPhoneNumber_c("");
        self.FilterDateOfBirth_c("");
        //self.FilterAddress_c("");
        self.FilterIDNumber_c("");
        self.FilterPOAFunction_c("");
        self.FilterOccupationInID_c("");
        self.FilterPlaceOfBirth_c("");
        self.FilterEffectiveDate_c("");
        self.FilterCancellationDate_c("");

        GetDataContact();
    };

    self.ClearCallbackContactFilters = function () {
        self.FilterCallbackContactCIF(cifData);
        self.FilterCallbackContactName("");
        self.FilterCallbackContactPhoneNumber("");
        self.FilterCallbackContactDateOfBirth("");
        self.FilterCallbackContactIDNumber("");
        self.FilterCallbackContactPOAFunction("");
        self.FilterCallbackContactOccupationInID("");
        self.FilterCallbackContactPlaceOfBirth("");
        self.FilterCallbackContactEffectiveDate("");
        self.FilterCallbackContactCancellationDate("");

        GetDataCallbackContact();
    };

    self.ClearCallbackFilters = function () {
        self.FilterCallbackCIF(cifData);
        self.FilterCallbackNameContact("");
        self.FilterCallbackProductName("");
        self.FilterCallbackTime("");
        self.FilterCallbackRemark("");
        self.FilterCallbackIsUTC("");
        self.FilterCallbackModifiedBy("");
        self.FilterCallbackModifiedDate("");

        GetDataCallback();
    };

    self.ClearCallbackDraftFilters = function () {
        self.FilterCallbackCIF(cifData);
        self.FilterCallbackDraftTaskName("");
        self.FilterCallbackDraftActionType("");
        self.FilterCallbackDraftModifiedBy("");
        self.FilterCallbackDraftModifiedDate("");

        GetDataCallbackDraft();
    };

    self.ClearPhoneNumberFilters = function () {
        self.FilterCityCode("");
        self.FilterContactType("");
        self.FilterCountryCode("");
        self.FilterPhoneNumber("");
        self.FilterCreateBy("");
        self.FilterUpdateBy("");
        self.FilterUpdateDate("");
        GetDataPhoneNumber();
    };
    self.ClearPOAEmailFilters = function () {
        self.FilterPOAEmail_POAE("");
        self.FilterCreateBy_POAE("");
        self.FilterUpdateBy_POAE("");
        self.FilterUpdateDate_POAE("");
        GetDataPOAEmailGrid();
    };
    // bind clear filters
    self.UnderlyingClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.UnderlyingFilterIsSelected(false);
        self.UnderlyingFilterUnderlyingDocument("");
        self.UnderlyingFilterDocumentType("");
        self.UnderlyingFilterCurrency("");
        self.UnderlyingFilterStatementLetter("");
        self.UnderlyingFilterAmount("");
        self.UnderlyingFilterAvailableAmount("");
        self.UnderlyingFilterDateOfUnderlying("");
        self.UnderlyingFilterExpiredDate("");
        self.UnderlyingFilterReferenceNumber("");
        self.UnderlyingFilterSupplierName("");
        self.UnderlyingFilterIsProforma("");
        self.UnderlyingFilterInvoiceNumber("");
        self.UnderlyingFilterAccountNumber("");
        self.UnderlyingFilterIsJointAccount("");
        self.UnderlyingFilterIsExpired("");
        self.UnderlyingFilterAttachmentNo("");

        GetDataUnderlying();
        GetDataUnderlyingDraft();
        GetDataCsoDraft(); //add azam
    };

    self.UnderlyingDraftClearFilters = function () {
        self.UnderlyingDraftFilterIsSelected(false);
        self.UnderlyingDraftFilterUnderlyingDocument("");
        self.UnderlyingDraftFilterDocumentType("");
        self.UnderlyingDraftFilterCurrency("");
        self.UnderlyingDraftFilterStatementLetter("");
        self.UnderlyingDraftFilterAmount("");
        self.UnderlyingDraftFilterAvailableAmount("");
        self.UnderlyingDraftFilterDateOfUnderlying("");
        self.UnderlyingDraftFilterExpiredDate("");
        self.UnderlyingDraftFilterReferenceNumber("");
        self.UnderlyingDraftFilterSupplierName("");
        self.UnderlyingDraftFilterIsProforma("");
        self.UnderlyingDraftFilterInvoiceNumber("");
        self.UnderlyingDraftFilterAccountNumber("");
        self.UnderlyingDraftFilterIsJointAccount("");
        self.UnderlyingDraftFilterAttachmentNo("");

        GetDataUnderlying();
        GetDataUnderlyingDraft();
        GetDataCsoDraft(); //add azam
    };

    self.ProformaClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.ProformaFilterIsSelected(false);
        self.ProformaFilterUnderlyingDocument("");
        self.ProformaFilterDocumentType("Proforma");
        self.ProformaFilterCurrency("");
        self.ProformaFilterStatementLetter("");
        self.ProformaFilterAmount("");
        self.ProformaFilterDateOfUnderlying("");
        self.ProformaFilterExpiredDate("");
        self.ProformaFilterReferenceNumber("");
        self.ProformaFilterInvoiceNumber("");
        self.ProformaFilterSupplierName("");
        self.ProformaFilterIsProforma("");

        GetDataUnderlyingProforma();
    };

    //add 2015.03.09
    self.BulkClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.BulkFilterIsSelected(false);
        self.BulkFilterUnderlyingDocument("");
        self.BulkFilterDocumentType("");
        self.BulkFilterCurrency("");
        self.BulkFilterStatementLetter("");
        self.BulkFilterAmount("");
        self.BulkFilterDateOfUnderlying("");
        self.BulkFilterExpiredDate("");
        self.BulkFilterReferenceNumber("");
        self.BulkFilterInvoiceNumber("");
        self.BulkFilterSupplierName("");
        self.BulkFilterIsBulkUnderlying("");

        GetDataBulkUnderlying();
    };

    // bind clear filters
    self.AttachClearFilters = function () {
        self.AttachFilterFileName("");
        self.AttachFilterDocumentPurpose("");
        self.AttachFilterModifiedDate("");
        self.AttachFilterDocumentRefNumber("");
        self.AttachFilterAttachmentReference("");

        GetDataAttachFile();
    };

    // bind clear filters
    self.UnderlyingAttachClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.UnderlyingAttachFilterIsSelected(false);
        self.UnderlyingAttachFilterUnderlyingDocument("");
        self.UnderlyingAttachFilterDocumentType("");
        self.UnderlyingAttachFilterCurrency("");
        self.UnderlyingAttachFilterStatementLetter("");
        self.UnderlyingAttachFilterAmount("");
        self.UnderlyingAttachFilterDateOfUnderlying("");
        self.UnderlyingAttachFilterExpiredDate("");
        self.UnderlyingAttachFilterReferenceNumber("");
        self.UnderlyingAttachFilterSupplierName("");

        GetDataUnderlyingAttach();
    };

    // bind clear filters Account
    self.ClearAccountFilters = function () {
        self.FilterCIF_a(cifData);
        self.FilterAccountNumber_a("");
        self.FilterCurrencyCode_a("");
        self.FilterCurrencyDesc_a("");
        self.FilterIsJointAccount("");
        self.FilterModifiedBy_a("");
        self.FilterModifiedDate_a("");

        GetAccountData();
    };

    //clear CSO Grid filter
    self.ClearCSOFilters = function () {
        self.FilterCIF_c(cifData);
        self.FilterEmployeeName_c("");
        self.FilterEmployeeEmail_c("");
        self.FilterLocationName_c("");
        self.FilterModifiedBy_c("");
        self.FilterModifiedDate_c("");

        GetDataCSO();
    };
    // bind get data function to view
    self.GetData = function () {
        GetData();
    };
    self.Read_u = function () {
        Read_u();
    }
    // bind Customer Function get data function to view
    self.GetFunctionData = function () {
        GetFunctionData();
    };

    // bind Customer Contact get data function to view
    self.GetDataContact = function () {
        GetDataContact();
    };
    self.GetDataPhoneNumber = function () {
        GetDataPhoneNumber();
    };
    self.GetDataPOAEmail = function () {
        GetDataPOAEmail();
    };
    self.GetDataPOAEmailDraft = function () {
        GetDataPOAEmailDraft();
    }
    self.GetDataPOAEmailGrid = function () {
        GetDataPOAEmailGrid();
    };
    self.GetDataCSO = function () {
        GetDataCSO();
    };
    self.MergingProductID = function () {
        var POAE = {};
        $(self.CustomerPOAEmails()).each(function (a, b) {
            var item = POAE[b.POAEmail];
            if (item != null) {
                item[b.POAEmailProductID] = b;
                //item.push(b)
            }
            else {
                //POAE[b.POAEmail] = [];
                //POAE[b.POAEmail].push(b);
                POAE[b.POAEmail] = {};
                POAE[b.POAEmail][b.POAEmailProductID] = b;
            }
        });

        self.CustomerPOAEmails(POAE)
        console.log('POAEmail', self.CustomerPOAEmails())
        console.log('POAEmailx', Object.keys(self.CustomerPOAEmails()))
    }

    self.GetDataCallbackContact = function () {
        GetDataCallbackContact();
    };

    self.GetDataCallback = function () {
        GetDataCallback();
    };

    self.GetDataCallbackDraft = function () {
        GetDataCallbackDraft();
    };

    //Permission for Contact Maker
    self.IsPermited = function (IsPermitedResult) {
        spUser = $.cookie(api.cookie.spUser);

        for (i = 0; i < spUser.Roles.length; i++) {
            if (Const_RoleName[spUser.Roles[i].ID].toLowerCase() == api.permission.customerContactMakerGroup.toLowerCase()) { //if (spUser.Roles[i].Name.toLowerCase() == api.permission.customerContactMakerGroup.toLowerCase()) {
                self.IsContactMaker(true);
                return;
            }
            else {
                self.IsContactMaker(false);
            }
        }
    }

    // bind get data function to view
    self.GetDataUnderlying = function () {
        GetDataUnderlying();
    };

    // bind get data function to view
    self.GetDataUnderlyingDraft = function () {
        GetDataUnderlyingDraft();
    };

    //start add azam
    self.GetDataCsoDraft = function () {
        GetDataCsoDraft();
    };

    //end azam

    self.GetDataAttachFile = function () {
        GetDataAttachFile();
    }

    self.GetDataUnderlyingAttach = function () {
        GetDataUnderlyingAttach();
    }

    self.GetDataUnderlyingProforma = function () {
        GetDataUnderlyingProforma();
    }
    // 2015.03.09
    self.GetDataBulkUnderlying = function () {
        GetDataBulkUnderlying();
    }

    // bind get data function to view
    self.GetAccountData = function () {
        GetAccountData();
    };

    // bind parameter data function to set dropdownlist item
    self.GetParameters = function () {
        GetParameters();
        GetPOAEmailProduct();
        GetRateIDR();
        GetTreshold();
        //add by fandi for worksheet
        GetMaintenanceTypes();
        //end
    }

    // bind get data IDR RATE
    self.GetRateIDR = function () {
        GetRateIDR();
    }

    self.GetTreshold = function () {
        GetTreshold();
    }

    self.OnCurrencyChange = function (CurrencyID) {
        if (CurrencyID != null) {
            var item = ko.utils.arrayFirst(self.ddlCurrency_u(), function (x) { return x.ID() == CurrencyID; });

            if (item != null) {
                self.Currency_u(new CurrencyModel(item.ID(), item.Code(), item.Description()));
            }

            if (!self.IsNewDataUnderlying()) {
                GetSelectedBulkID(GetDataBulkUnderlying);
            } else {
                self.TempSelectedUnderlyingBulk([]);
                GetDataBulkUnderlying();
            }
            var CurrencyID = self.Currency_u().ID();
            if (CurrencyID != '' && CurrencyID != undefined) {
                GetRateIDRAmount(CurrencyID);
            } else {
                self.Rate_u(0);
            }
        }
    }

    //add by fandi
    self.GetMaintenanceTypes = function () {
        GetMaintenanceTypes();
    }
    //end

    //#region Joint Account
    self.JointAccountNumbers = ko.observableArray([]);
    self.OnChangeJointAcc = function (obj, event) {
        OnChangeJointAcc(obj);
    }
    self.CheckEmptyAccountNumber = function () {
        var accNumber = CustomerUnderlying.AccountNumber() != null ? CustomerUnderlying.AccountNumber() : null;
        if (accNumber != null) {
            //SetStatementA(true,accNumber);
        }
    }
    //#endregion
    //The Object which stored data entered in the observables
    var Customer = {
        CIF: self.CIF,
        Name: self.Name,
        POAName: self.POAName,
        BizSegment: self.BizSegment,
        Branch: self.Branch,
        Type: self.Type,
        RM: self.UserApproval,
        Accounts: []
    };

    var CustomerFunction = {
        ID: self.ID_f,
        CIF: self.CIF_f,
        POAFunction: self.POAFunction_f,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
    };
    var CustomerContact = {
        ID: self.ID_c,
        CIF: self.CIF,
        Name: self.Name,
        BizSegment: self.BizSegment,
        Branch: self.Branch,
        Type: self.Type,
        RM: self.UserApproval,
        NameContact: self.Name_c,
        SourceID: self.SourceID_c,
        AccountNumber: self.AccountNumbers_c,
        PhoneNumber: self.PhoneNumber_c,
        POAFunction: self.POAFunctions_c,
        DateOfBirth: self.DateOfBirth_c,
        IDNumber: self.IDNumber_c,
        OccupationInID: self.OccupationInID_c,
        PlaceOfBirth: self.PlaceOfBirth_c,
        EffectiveDate: self.EffectiveDate_c,
        CancellationDate: self.CancellationDate_c,
        LastModifiedBy: self.LastModifiedBy_c,
        LastModifiedDate: self.LastModifiedDate_c,
    };
    //------------------------------------------Codingan dayat------------------------------------
    //*****************************************Declare Properties*********************************
    var POAEmailModel = {
        POAEmailID: self.POAEmailID_POAE,
        POAEmail: self.POAEmail_POAE,
        POAEmailProductID: self.POAEmailProductID_POAE,
        UpdateDate: self.UpdateDate_POAE,
        UpdateBy: self.UpdateBy_POAE,
        CreateDate: self.CreateDate_POAE,
        CreateBy: self.CreateBy_POAE,
        IsChecked: self.IsChecked_POAE,
        CIF: self.CIF_POAE

    }
    var GenaratePOAEmailModel = {
        CIF: ko.observable(),
        FileTipe: ko.observable(),
        dtimeFile: ko.observable(),
        DataInformation: ko.observableArray([])
    }
    self.POAEmailRows = ko.observableArray([]);
    self.CustomerPOAEmailID_POAE = ko.observable();
    self.POAEmail_POAE = ko.observable();
    self.POAOldEmail_POAE = ko.observable();
    self.POAEmailProductID_POAE = ko.observable();
    self.UpdateDate_POAE = ko.observable();
    self.UpdateBy_POAE = ko.observable();
    self.CreateDate_POAE = ko.observable();
    self.CreateBy_POAE = ko.observable();
    self.CIF_POAE = ko.observable();
    self.POAFunctionTemp = ko.observable([]);
    self.POAFunctionOtherTemp = ko.observable();
    self.SelectedPOAEmailsTemp = ko.observableArray([]);
    self.POAEmails = ko.observableArray([]);
    self.IsClicked = ko.observable(false);
    self.InsertedURL = ko.observable('');
    self.FileName = ko.observable('');
    self.IsSuccess = ko.observable();
    self.SearchDate = ko.observable(self.LocalDate(new Date(), true, false));
    self.CustomerPOAEmailDraftGridProperties = ko.observable();
    self.CustomerPOAEmailDraftGridProperties(new GridPropertiesModel(GetDataPOAEmailDraft));
    self.CustomerPOAEmailDraftGridProperties().SortColumn("LastModifiedDate");
    self.CustomerPOAEmailDraftGridProperties().SortOrder("DESC");
    self.FilterTaskName_POAE = ko.observable("");
    self.FilterValue_POAE = ko.observable("");
    self.FilterActionType_POAE = ko.observable("");
    self.FilterModifiedBy_POAE = ko.observable("");
    self.FilterModifiedDate_POAE = ko.observable("");
    self.FilterPOAEmailDraftCIF();
    //*******************************************Declare Properties End *************************

    //*********************************************Start Function********************************
    self.checkedPOAFunction = function () {
        ko.utils.arrayForEach(self.POAFunctionTemp(), function (SelectedItems) {
            ko.utils.arrayFilter(self.ddlPOAFunction_c(), function (items) {
                if (items.ID == SelectedItems.ID)
                    $('#checkBox' + items.ID).prop('checked', true);
                if (items.ID == 9) {
                    self.IsFunctionOther_c(true);
                } else {
                    self.IsFunctionOther_c(false);
                }
            });
        });
    }

    self.addPOAFunction = function () {
        var isChecked;
        var isAllChecked = [];
        $("#ChkAll").prop('checked', false);
        $("input:checked").each(function () {

            isAllChecked.push(this.value);
        });
        if (isAllChecked.length < self.POAFunctionTemp().length) {
            isChecked = false;
        } else if (isAllChecked.length > self.POAFunctionTemp().length) {
            isChecked = true;
        }
        var notFound;
        //If it is checked and not in the array, add it
        if (isChecked) {
            var isFound = false;
            for (var xx = 0; xx < isAllChecked.length; xx++) {
                isFound = false;
                for (var yy = 0; yy < self.POAFunctionTemp().length; yy++) {
                    if (self.POAFunctionTemp()[yy].ID == isAllChecked[xx]) {
                        isFound = true;
                        break;
                    }
                }
                if (!isFound) {
                    ko.utils.arrayFilter(self.ddlPOAFunction_c(), function (items) {
                        if (items.ID == isAllChecked[xx]) {
                            notFound = {
                                ID: items.ID,
                                Name: items.Description,
                                LastModifiedBy: items.LastModifiedBy,
                                LastModifiedDate: items.LastModifiedDate,
                                Description: ""
                            }

                        }
                    });

                }
            }
            if (typeof notFound !== 'undefined') {
                self.POAFunctionTemp().push(notFound);
                if (notFound.ID == 9) {
                    self.IsFunctionOther_c(true);
                }
            }

        }
            //If it is in the array and not checked remove it                
        else {
            var isFound = false;
            //self.POAFunction_c.remove(POAFunctionModel);
            for (var ii = 0; ii < self.POAFunctionTemp().length; ii++) {
                isFound = false;
                for (var jj = 0; jj < isAllChecked.length; jj++) {
                    if (self.POAFunctionTemp()[ii].ID == isAllChecked[jj]) {
                        isFound = true;
                        break;
                    }
                }
                if (!isFound) {
                    notFound = self.POAFunctionTemp()[ii];
                }
            }

            if (typeof notFound !== 'undefined') {
                self.POAFunctionTemp.remove(notFound);
                if (notFound.ID == 9) {
                    self.IsFunctionOther_c(false);
                    self.POAFunctionOtherTemp = ko.observable()
                }
            }
        }
        //Need to return to to allow the Checkbox to process checked/unchecked
        return true;
    }

    self.POAFunctionCheckAll = function () {
        var chkAll = document.getElementById("ChkAll").checked;
        self.POAFunctionTemp = ko.observableArray([]);
        if (chkAll == true) {
            $("#POATable input[type='checkbox']").prop('checked', true);
            ko.utils.arrayFilter(self.ddlPOAFunction_c(), function (items) {
                self.POAFunctionTemp.push(items)
                if (items.ID == 9) {
                    self.IsFunctionOther_c(true);
                    self.POAFunctionOtherTemp = ko.observable()
                } else {
                    self.IsFunctionOther_c(false);
                }
            });

        } else if (chkAll == false) {
            $("#POATable input[type='checkbox']").prop('checked', false);
            ko.utils.arrayFilter(self.ddlPOAFunction_c(), function (items) {
                self.POAFunctionTemp.remove(items)

            });
        }
    }

    var POAEmailModelTemp = function (POAEmailProductID, ProductName, cifData, Name) {
        var self = this;
        self.POAEmailProductID = ko.observable(POAEmailProductID);
        self.POAEmail = ko.observable();
        self.ProductName = ko.observable(ProductName);
        self.CreateBy = ko.observable();
        self.UpdateBy = ko.observable();
        self.CreateDate = ko.observable();
        self.UpdateDate = ko.observable();
        self.CIF = ko.observable(cifData)
        self.CustomerName = ko.observable(Name)

    }

    self.checkedPOAEmail = function () {
        ko.utils.arrayForEach(self.SelectedPOAEmailsTemp(), function (SelectedItems) {
            ko.utils.arrayFilter(self.cblPOAEmailProduct(), function (items) {
                if (items.POAEmailProductID == SelectedItems.POAEmailProductID) {
                    $('#POAEmailcheckBox' + items.POAEmailProductID).prop('checked', true);
                }
            });
        });
    }

    self.POAEmailCheckAll = function () {
        var chkAll = document.getElementById("POAEmailChkAll").checked;
        self.SelectedPOAEmailsTemp = ko.observableArray([]);
        if (chkAll == true) {
            $("#POAEmailTable input[type='checkbox']").prop('checked', true);
            ko.utils.arrayFilter(self.cblPOAEmailProduct(), function (items) {
                self.SelectedPOAEmailsTemp.push(new POAEmailModelTemp(items.POAEmailProductID, items.ProductName, cifData))
            });

        } else if (chkAll == false) {
            $("#POAEmailTable input[type='checkbox']").prop('checked', false);
            ko.utils.arrayFilter(self.cblPOAEmailProduct(), function (items) {
                self.SelectedPOAEmailsTemp.remove(items)
            });
        }
    }

    self.AddPOAEmail = function (item) {
        var data = ko.utils.arrayFirst(self.SelectedPOAEmailsTemp(), function (x) {
            return x.POAEmailProductID() == item.POAEmailProductID;
        });
        var POAEmail = self.POAEmail_POAE();
        if (data != null) { //uncheck condition
            self.SelectedPOAEmailsTemp.remove(data);
            $('#POAEmailcheckBox' + item.POAEmailProductID).prop('checked', false);
        } else {
            self.SelectedPOAEmailsTemp.push(new POAEmailModelTemp(item.POAEmailProductID, item.ProductName, cifData, self.Name));
            $('#POAEmailcheckBox' + item.POAEmailProductID).prop('checked', true);
        }

    }

    function ValidateEmail(email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
    };

    self.GetSelectedRowCustomerDraft = function (data) {
        self.IsNewCSOData(false);
        self.IsEditCSOData(false);
        $("#CSO-modal-form").modal('show');
        self.CustomerCsoID(data.ID);
        self.CIF_c(cifData);
        self.EmployeeCSOID_c(data.ID);
        self.Employee(new EmployeeModel(data.Employee.EmployeeID, data.Employee.EmployeeName, data.Employee.EmployeeUserName, data.Employee.EmployeeEmail));


    };

    self.GenaratePOAEmail = function () {
        GenaratePOAEmailModel.DataInformation([]);
        GenaratePOAEmailModel.dtimeFile("");
        GenaratePOAEmailModel.FileTipe("POAEmail");
        GenaratePOAEmailModel.CIF(cifData);
        GenaratePOAEmailModel.dtimeFile(self.SearchDate());
        GenerateProcess(GenaratePOAEmailModel);
    }
    function GenerateProcess(GenaratePOAEmailModel) {
        var options = {
            url: "/_vti_bin/DBSGenerateFileV2/GenerateFileV2Service.svc/GenerateFileV2",
            data: ko.toJSON(GenaratePOAEmailModel)
        };
        Helper.Sharepoint.Nintex.Post(options, OnSuccessGenerateFile, OnError, OnAlways);
    }
    function OnSuccessGenerateFile(data, textStatus, jqXHR) {
        if (data.IsSuccess == true) {
            ShowNotification("Success", "Generate File Success", 'gritter-success', false);
            self.InsertedURL(data.InsertedURL);
            self.FileName(data.FileName);
            self.IsSuccess(true);
        }
        else {
            ShowNotification("Attention", "Generate File failed", 'gritter-warning', false);
            self.InsertedURL(null);
            self.FileName(null);
            self.IsSuccess(false);
        }
    }
    //End Genarate
    function GetCutomerPOAEmailDraftFilteredColumns() {
        var filters = [];
        self.FilterPOAEmailDraftCIF(cifData);
        filters.push({ Field: 'EntityName', Value: 'CustomerPOAEmail' });
        filters.push({ Field: 'FilterValue', Value: self.FilterPOAEmailDraftCIF() });
        if (self.FilterTaskName_POAE() != "") filters.push({ Field: 'TaskName', Value: self.FilterTaskName_POAE() });
        if (self.FilterActionType_POAE() != "") filters.push({ Field: 'ActionType', Value: self.FilterActionType_POAE() });
        if (self.FilterValue_POAE() != "") filters.push({ Field: 'FilterValue', Value: self.FilterValue_POAE() });
        if (self.FilterModifiedBy_POAE() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy_POAE() });
        if (self.FilterModifiedDate_POAE() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate_POAE() });

        return filters;
    };
    function GetDataPOAEmailDraft() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.workflowEntity,
            params: {
                page: self.CustomerPOAEmailDraftGridProperties().Page(),
                size: self.CustomerPOAEmailDraftGridProperties().Size(),
                sort_column: self.CustomerPOAEmailDraftGridProperties().SortColumn(),
                sort_order: self.CustomerPOAEmailDraftGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetCutomerPOAEmailDraftFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataPOAEmailDraft, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataPOAEmailDraft, OnError, OnAlways);
        }
    }
    function OnSuccessGetDataPOAEmailDraft(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.POAEmailRows(data.Rows);
            self.CustomerPOAEmailDraftGridProperties().Page(data['Page']);
            self.CustomerPOAEmailDraftGridProperties().Size(data['Size']);
            self.CustomerPOAEmailDraftGridProperties().Total(data['Total']);
            self.CustomerPOAEmailDraftGridProperties().TotalPages(Math.ceil(self.CustomerPOAEmailDraftGridProperties().Total() / self.CustomerPOAEmailDraftGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    self.ClearCustomerPOAEmailFilters = function () {
        self.FilterTaskName_POAE("");
        self.FilterActionType_POAE("");
        self.FilterValue_POAE("");
        self.FilterModifiedBy_POAE("");
        self.FilterModifiedDate_POAE("");
        GetDataPOAEmailDraft();
    };
    //******************************************Function End*********************************************
    //-----------------------------------------Codingan dayat end----------------------------------------

    //The Object which stored data entered in the observable
    var CustomerUnderlying = {
        ID: self.ID_u,
        CIF: self.CIF_u,
        CustomerName: self.CustomerName_u,
        UnderlyingDocument: self.UnderlyingDocument_u,
        DocumentType: self.DocumentType_u,
        Currency: self.Currency_u,
        Amount: self.Amount_u,
        Rate: self.Rate_u,
        AmountUSD: self.AmountUSD_u,
        AvailableAmountUSD: self.AvailableAmount_u,
        AttachmentNo: self.AttachmentNo_u,
        StatementLetter: self.StatementLetter_u,
        IsDeclarationOfException: self.IsDeclarationOfException_u,
        StartDate: self.StartDate_u,
        EndDate: self.EndDate_u,
        DateOfUnderlying: self.DateOfUnderlying_u,
        ExpiredDate: self.ExpiredDate_u,
        ReferenceNumber: self.ReferenceNumber_u,
        SupplierName: self.SupplierName_u,
        InvoiceNumber: self.InvoiceNumber_u,
        IsSelectedProforma: self.IsSelectedProforma,
        IsProforma: self.IsProforma_u,
        IsUtilize: self.IsUtilize_u,
        IsJointAccount: self.IsJointAccount_u,
        AccountNumber: self.AccountNumber_u,
        Proformas: self.Proformas,
        IsSelectedBulk: self.IsSelectedBulk,
        IsBulkUnderlying: self.IsBulkUnderlying_u,
        BulkUnderlyings: self.BulkUnderlyings,
        OtherUnderlying: self.OtherUnderlying_u,
        LastModifiedBy: self.LastModifiedBy_u,
        LastModifiedDate: self.LastModifiedDate_u
    };

    //The Object which stored data entered in the observables
    var CustomerUnderlyingDraft = {
        ID: self.ID,
        CIF: self.CIF,
        CustomerName: self.CustomerName,
        UnderlyingDocument: self.UnderlyingDocument,
        DocumentType: self.DocumentType,
        Currency: self.Currency,
        Amount: self.Amount,
        Rate: self.Rate,
        AmountUSD: self.AmountUSD,
        AvailableAmountUSD: self.AvailableAmount,
        AttachmentNo: self.AttachmentNo,
        StatementLetter: self.StatementLetter,
        IsDeclarationOfException: self.IsDeclarationOfException,
        StartDate: self.StartDate,
        EndDate: self.EndDate,
        DateOfUnderlying: self.DateOfUnderlying,
        ExpiredDate: self.ExpiredDate,
        ReferenceNumber: self.ReferenceNumber,
        SupplierName: self.SupplierName,
        InvoiceNumber: self.InvoiceNumber,
        IsSelectedProforma: self.IsSelectedProforma,
        IsProforma: self.IsProforma,
        IsUtilize: self.IsUtilize,
        Proformas: self.Proformas,
        IsSelectedBulk: self.IsSelectedBulk,
        IsBulkUnderlying: self.IsBulkUnderlying,
        BulkUnderlyings: self.BulkUnderlyings,
        OtherUnderlying: self.OtherUnderlying,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate

    };
    //The Object which stored data entered in the observables
    var Documents = {
        Documents: self.Documents,
        DocumentPath: self.DocumentPath_a,
        Type: self.DocumentType_a,
        Purpose: self.DocumentPurpose_a
    }

    //The Object which stored data entered in the observables
    var CustomerUnderlyingFile = {
        ID: self.ID_a,
        FileName: DocumentModels().FileName,
        DocumentPath: DocumentModels().DocumentPath,
        DocumentPurpose: self.DocumentPurpose_a,
        DocumentType: self.DocumentType_a,
        DocumentRefNumber: self.DocumentRefNumber_a,
        AttachmentReference: self.AttachmentReference_a,
        LastModifiedBy: self.LastModifiedBy_a,
        LastModifiedDate: self.LastModifiedDate_a,
        CustomerUnderlyingMappings: self.CustomerUnderlyingMappings_a
    };

    var CustomerAccount = {
        CIF: self.CIF_a,
        AccountNumber: self.AccountNumber_a,
        Currency: self.Currency_a,
        IsJointAccount: self.IsJointAccount,
        LastModifiedBy: self.LastModifiedBy_a,
        LastModifiedDate: self.LastModifiedDate_a

    };


    self.save = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            //Ajax call to insert the Customers
            $.ajax({
                type: "POST",
                url: api.server + api.url.customer,
                data: ko.toJSON(Customer), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);


                        // refresh data
                        GetData();
                        // $('#s4-workspace').scrollTop(10);
                        self.IsGridEnable(true);
                        self.IsNewData(true);
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }
    };

    self.update = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            // hide current popup window

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.customer + "/" + Customer.CIF(),
                        data: ko.toJSON(Customer),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);


                                // refresh data
                                GetData();
                                // $('#s4-workspace').scrollTop(10);
                                self.IsGridEnable(true);
                                self.IsNewData(true);
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.delete = function () {
        // hide current popup window
        $("#modal-form").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customer + "/" + Customer.CIF(),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };


    self.cancel = function () {
        ClearFieldCustomer();
        //$('#s4-workspace').scrollTop(0);
        self.IsGridEnable(true);

        self.IsNewData(true);
        cifData = "0";
        customerNameData = "";
        self.CustomerAccounts([]);

        self.CustomerFunctions([]);
        self.CustomerContacts([]);
        self.CustomerCallbackContacts([]);
        self.CustomerCallbacks([]);
        self.CustomerCallbackDrafts([]);
        self.CustomerPhoneNumbers([]);
        self.CustomerPOAEmails([]);
        self.CustomerUnderlyings([]);
        self.CustomerUnderlyingDrafts([]);
        self.CustomerCsoDrafts([]); // add azam
        self.CustomerUnderlyingProformas([]);
        self.CustomerBulkUnderlyings([]);
        self.CustomerUnderlyingFiles([]);
        self.CustomerAttachUnderlyings([]);
        self.CustomerCSO([]);
        //GetFunctionData();
        //GetDataContact();
        //GetDataUnderlying();
        //GetDataAttachFile();
    };

    self.save_f = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            self.IsEditable(false);
            //Ajax call to insert the CustomerFunctions
            $.ajax({
                type: "POST",
                url: api.server + api.url.customerFunction,
                data: ko.toJSON(CustomerFunction), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        // hide current popup window
                        $("#function-modal-form").modal('hide');

                        // refresh data
                        GetFunctionData();
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
            self.IsEditable(true);
        }

    };

    self.update_f = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            // hide current popup window
            $("#function-modal-form").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#function-modal-form").modal('show');
                } else {
                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.customerFunction + "/" + CustomerFunction.ID(),
                        data: ko.toJSON(CustomerFunction),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#function-modal-form").modal('hide');

                                // refresh data
                                GetFunctionData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }

    };

    self.delete_f = function () {
        // hide current popup window
        $("#function-modal-form").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#function-modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customerFunction + "/" + CustomerFunction.ID(),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetFunctionData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    self.save_c = function () {
        // validation
        if (self.POAFunctions_c().length == 0) {
            ShowNotification("Attention", "POA Function Can't Empty", 'gritter-warning', false);
            return false;
        }
        var PhoneNumber = "";
        if ((self.Area_c() != "" && self.Area_c() != undefined) || (self.Phone_c() != undefined && self.Phone_c() != "") || (self.Ext_c() != "" && self.Ext_c() != undefined)) {
            PhoneNumber = (self.Area_c() != "" && self.Area_c() != undefined ? self.Area_c() : "") + "-" +
                      (self.Phone_c() != undefined && self.Phone_c() != "" ? self.Phone_c() : "") + "-" +
                      (self.Ext_c() != "" && self.Ext_c() != undefined ? self.Ext_c() : "");
        }
        var POAFilter = "";
        for (var i = 0 ; i < CustomerContact.POAFunction().length ; i++) {
            POAFilter += CustomerContact.POAFunction()[i].ID() == 9 ? CustomerContact.POAFunction()[i].POAFunctionOther()+"; " : CustomerContact.POAFunction()[i].Description() + "; ";
        }
        if (CustomerContact.POAFunction().length == 0) {
            CustomerContact['POAFunctionVW'] = "";
        }
        else {
            CustomerContact['POAFunctionVW'] = POAFilter;
        }

        self.PhoneNumber_c(PhoneNumber);
        var form = $("#aspnetForm");
        form.validate();
        console.log('save contact');
        if (form.valid()) {
            self.IsEditable(false);
            SetPOAFunctionOther();
            $.ajax({
                type: "POST",
                url: api.server + api.url.customercontact,
                data: ko.toJSON(CustomerContact), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        // hide current popup window
                        $("#contact-modal-form").modal('hide');

                        // refresh data
                        GetDataContact();
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                        //self.IsEditable(true);
                    }
                    self.IsEditable(true);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    self.IsEditable(true);
                }
            });
        }

    };

    self.update_c = function () {
        // validation
        if (self.POAFunctions_c().length == 0) {
            ShowNotification("Attention", "POA Function Can't Empty", 'gritter-warning', false);
            return false;
        }
        var form = $("#aspnetForm");
        form.validate();
        var PhoneNumber = "";
        if ((self.Area_c() != "" && self.Area_c() != undefined) || (self.Phone_c() != undefined && self.Phone_c() != "") || (self.Ext_c() != "" && self.Ext_c() != undefined)) {
            PhoneNumber = (self.Area_c() != "" && self.Area_c() != undefined ? self.Area_c() : "") + "-" +
                      (self.Phone_c() != undefined && self.Phone_c() != "" ? self.Phone_c() : "") + "-" +
                      (self.Ext_c() != "" && self.Ext_c() != undefined ? self.Ext_c() : "");
        }
        var POAFilter = "";
        for (var i = 0 ; i < CustomerContact.POAFunction().length ; i++) {
            POAFilter += CustomerContact.POAFunction()[i].ID() == 9 ? CustomerContact.POAFunction()[i].POAFunctionOther() + "; " : CustomerContact.POAFunction()[i].Description() + "; ";
        }
        if (CustomerContact.POAFunction().length == 0) {
            CustomerContact['POAFunctionVW'] = "";
        }
        else {
            CustomerContact['POAFunctionVW'] = POAFilter;
        }
        self.PhoneNumber_c(PhoneNumber)
        if (form.valid()) {
            SetPOAFunctionOther();
            // hide current popup window
            $("#contact-modal-form").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#contact-modal-form").modal('show');
                } else {
                    self.IsEditable(false);
                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.customercontact + "/" + CustomerContact.ID(),
                        data: ko.toJSON(CustomerContact),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#contact-modal-form").modal('hide');

                                // refresh data
                                GetDataContact();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                            self.IsEditable(true);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                            self.IsEditable(false);
                        }
                    });
                }
            });
        }

    };

    self.delete_c = function () {
        // hide current popup window
        $("#contact-modal-form").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#contact-modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customercontact + "/" + CustomerContact.ID(),
                    data: ko.toJSON(CustomerContact),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetDataContact();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    self.save_POAE = function () {
        // validation
        self.IsEditable(false);
        if (self.POAEmail_POAE() != null && self.POAEmail_POAE() != '') {
            if (!ValidateEmail(self.POAEmail_POAE())) {
                ShowNotification("Attention", "Invalid Email Address", 'gritter-warning', true);
                self.IsEditable(true);
                return false;
            }
        }
        POAEmailModel = self.SelectedPOAEmailsTemp();
        var form = $("#aspnetForm");
        form.validate();
        if (form.valid()) {
            self.IsEditable(false);
            $.ajax({
                type: "POST",
                url: api.server + api.url.poaemailProduct,
                data: ko.toJSON({
                    POAEmail: POAEmailModel,
                    Email: self.POAEmail_POAE()
                }), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {

                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                        $("#poaEmail-modal-form").modal('hide');
                        self.IsEditable(true);
                        // refresh data
                        GetDataPOAEmail();
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                        self.IsEditable(true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    self.IsEditable(true);
                }
            });
        }
        else {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', true);
            self.IsEditable(true);
        }
    };

    self.update_POAE = function () {
        POAEmailModel = self.SelectedPOAEmailsTemp();
        console.log(self.SelectedPOAEmailsTemp())
        var form = $("#aspnetForm");
        form.validate();
        if (form.valid()) {
            $("#poaEmail-modal-form").modal('hide')
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#poaEmail-modal-form").modal('show');
                } else {
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.poaemailProduct + "/UpdateDataPOAEmail",
                        data: ko.toJSON(POAEmailModel),
                        data: ko.toJSON({
                            OldEmail: self.POAOldEmail_POAE(),
                            Email: self.POAEmail_POAE(),
                            POAEmail: POAEmailModel,
                        }),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                                GetDataPOAEmail();
                                $("#poaEmail-modal-form").modal('hide')
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }

    };

    self.delete_POAE = function () {
        POAEmailModel = self.SelectedPOAEmailsTemp()
        // hide current popup window
        $("#poaEmail-modal-form").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#poaEmail-modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.poaemailProduct + "/DeleteDataPOAEmail",
                    data: ko.toJSON({
                        CIF: cifData,
                        Email: self.POAOldEmail_POAE,
                        POAEmail: POAEmailModel,
                    }),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            self.IsClicked(true)
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                            $("#poaEmail-modal-form").modal('hide');
                            // refresh data
                            GetDataPOAEmail();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    self.save_u = function () {
        // validation

        //alert(CustomerUnderlying.StatementLetter_u());
        self.IsEditTableUnderlying(false);
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            if (CustomerUnderlying.StatementLetter().ID() == CONST_STATEMENT.StatementB_ID && CustomerUnderlying.UnderlyingDocument().ID() == 36) {
                ShowNotification("Form Validation Warning", "Please Change Document Underlying other 998(tanpa underlying). ", 'gritter-warning', false);
                self.IsEditTableUnderlying(true);
                return false;
            }
            var other = self.CodeUnderlying() == '999' ? self.OtherUnderlying_u() : '';
            CustomerUnderlying.OtherUnderlying(other);
            SetMappingProformas();
            SetMappingBulks(); // add 2015.03.09
            var sAmount = CustomerUnderlying.Amount().toString().replace(/,/g, "");
            sAmount = isNaN(parseFloat(sAmount)) ? 0 : parseFloat(sAmount)
            CustomerUnderlying.Amount(sAmount);
            //Ajax call to insert the CustomerUnderlyings
            $.ajax({
                type: "POST",
                url: api.server + api.url.customerunderlying + "/SaveDraft",
                data: ko.toJSON(CustomerUnderlying), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {

                        AddListItem("Add", data.UnderlyingDraftID)

                        // send notification
                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        // hide current popup window
                        $("#modal-form-Underlying").modal('hide');
                        self.IsEditTableUnderlying(true);
                        // refresh data
                        GetDataUnderlying();
                        GetDataUnderlyingDraft();
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                        self.IsEditTableUnderlying(true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    //var message = (jqXHR.responseJSON.Message == "Underlying does exist.") ? 'gritter-warning' : 'gritter-error';
                    if (jqXHR.responseJSON.Message.toLowerCase().startsWith("double underlying")) {
                        ShowNotification("", jqXHR.responseJSON.Message, 'gritter-warning', true);
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    } self.IsEditTableUnderlying(true);
                }
            });
        } else {
            self.IsEditTableUnderlying(true);
        }
    };

    self.save_cu = function () {
        var $container = $("#dataTable");
        var handsontable = $container.data('handsontable');
        var data = handsontable.getData();
        for (var i = 0; i < data.length; i++) {

            var Join = handsontable.getDataAtRowProp(i, "JointAccount");
            var StatementLetter = handsontable.getDataAtRowProp(i, "StatementLetter");
            var UnderlyingDocument = handsontable.getDataAtRowProp(i, "UnderlyingDocument");
            var DocType = handsontable.getDataAtRowProp(i, "DocumentType");
            var currency = handsontable.getDataAtRowProp(i, "Currency");
            var InvoiceAmount = handsontable.getDataAtRowProp(i, "InvoiceAmount");
            var AmountUSD = handsontable.getDataAtRowProp(i, "AmountUSD");
            var DateOfUnderlying2 = handsontable.getDataAtRowProp(i, "DateOfUnderlying2");
            var ExpiredDate2 = handsontable.getDataAtRowProp(i, "ExpiredDate2");
            var ReferenceNumber = handsontable.getDataAtRowProp(i, "ReferenceNumber");
            var SupplierName = handsontable.getDataAtRowProp(i, "SupplierName");
            var InvoiceNumber = handsontable.getDataAtRowProp(i, "InvoiceNumber");

            if (handsontable.getDataAtRowProp(i, "StatementLetter") != "Annual Statement Letter") {
                if (Join == '' || Join == null) {
                    ShowNotification("Attention", "Please Select Single Or Join Account", "gritter-warning", true);
                    return false;
                }
                if (StatementLetter == '' || StatementLetter == null || StatementLetter == 'Choose...') {
                    ShowNotification("Attention", "Please Select Statement Letter", "gritter-warning", true);
                    return false;
                }
                if (DocType == '' || DocType == null || DocType == 'Choose...') {
                    ShowNotification("Attention", "Please Select Document Type", "gritter-warning", true);
                    return false;
                }
                if (currency == '' || currency == null || currency == 'Choose...') {
                    ShowNotification("Attention", "Please Select Currency", "gritter-warning", true);
                    return false;
                }
                if (UnderlyingDocument == '' || UnderlyingDocument == null || UnderlyingDocument == 'Choose...') {
                    ShowNotification("Attention", "Please Select Underlying Document", "gritter-warning", true);
                    return false;
                }
                if (InvoiceAmount == '' || InvoiceAmount == null) {
                    ShowNotification("Attention", "Please insert Invoice Amount", "gritter-warning", true);
                    return false;
                }
                if (DateOfUnderlying2 == '' || DateOfUnderlying2 == null) {
                    ShowNotification("Attention", "Please Date Of Underlying", "gritter-warning", true);
                    return false;
                }
                if (ExpiredDate2 == '' || ExpiredDate2 == null) {
                    ShowNotification("Attention", "Please Expired Date", "gritter-warning", true);
                    return false;
                }
                if (ReferenceNumber == '' || ReferenceNumber == null) {
                    ShowNotification("Attention", "Please Reference Number", "gritter-warning", true);
                    return false;
                }
                if (SupplierName == '' || SupplierName == null) {
                    ShowNotification("Attention", "Please Supplie rName", "gritter-warning", true);
                    return false;
                }
                if (InvoiceNumber == '' || InvoiceNumber == null) {
                    ShowNotification("Attention", "Please Invoice Number", "gritter-warning", true);
                    return false;
                }
            } else {
                if (Join == '' || Join == null) {
                    ShowNotification("Attention", "Please Select Single Or Join Account", "gritter-warning", true);
                    return false;
                }
                if (StatementLetter == '' || StatementLetter == null || StatementLetter == 'Please Choose...') {
                    ShowNotification("Attention", "Please Select Statement Letter", "gritter-warning", true);
                    return false;
                }
                if (DateOfUnderlying2 == '' || DateOfUnderlying2 == null) {
                    ShowNotification("Attention", "Please Date Of Underlying", "gritter-warning", true);
                    return false;
                }
                if (ExpiredDate2 == '' || ExpiredDate2 == null) {
                    ShowNotification("Attention", "Please Expired Date", "gritter-warning", true);
                    return false;
                }
            }

        }
        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SaveExcel",
            data: ko.toJSON(data),
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if (data != 0) {
                        // send notification
                        ShowNotification('WorkFlow Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                        // add to list
                        for (var index = 0; index < data.UnderlyingData.length; index++) {
                            AddListItem("Add", data.UnderlyingData[index].UnderlyingID);
                        }
                        //end list
                    }
                    else {
                        ShowNotification('Warning', jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }

                    // hide current popup window
                    $("#Underlying-modal-form").modal('hide');
                    self.IsEditTableUnderlying(true);
                    // refresh data
                    GetDataUnderlying();
                    GetDataUnderlyingDraft();
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    self.IsEditTableUnderlying(true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                //var message = (jqXHR.responseJSON.Message == "Underlying does exist.") ? 'gritter-warning' : 'gritter-error';
                if (jqXHR.responseJSON.Message.toLowerCase().startsWith("double underlying")) {
                    ShowNotification("", jqXHR.responseJSON.Message, 'gritter-warning', true);
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                } self.IsEditTableUnderlying(true);
            }
        });

    };

    self.update_u = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            if (CustomerUnderlying.StatementLetter().ID() == CONST_STATEMENT.StatementB_ID && CustomerUnderlying.UnderlyingDocument().ID() == 36) {
                ShowNotification("Form Validation Warning", "Please Change Document Underlying other 998(tanpa underlying). ", 'gritter-warning', false);
                //self.IsEditTableUnderlying(true);
                return false;
            }
            // hide current popup window
            $("#modal-form-Underlying").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form-Underlying").modal('show');
                } else {
                    var other = self.CodeUnderlying() == '999' ? self.OtherUnderlying_u() : '';
                    CustomerUnderlying.OtherUnderlying(other);
                    var sAmount = CustomerUnderlying.Amount().toString().replace(/,/g, "");
                    sAmount = isNaN(parseFloat(sAmount)) ? 0 : parseFloat(sAmount)
                    CustomerUnderlying.Amount(sAmount);
                    SetMappingProformas();
                    SetMappingBulks(); // Add 2015.03.09
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.customerunderlying + "/Draft/" + CustomerUnderlying.ID(),
                        data: ko.toJSON(CustomerUnderlying),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // add to list
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                AddListItem("Update", data.UnderlyingDraftID);

                                // hide current popup window
                                $("#modal-form-Underlying").modal('hide');

                                // refresh data
                                GetDataUnderlying();
                                GetDataUnderlyingDraft();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            if (jqXHR.responseJSON.Message.toLowerCase().startsWith("double underlying")) {
                                ShowNotification("", jqXHR.responseJSON.Message, 'gritter-warning', true);
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                            }
                        }
                    });
                }
            });
        }

    };

    self.save_a = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            self.IsEditable(false);
            if (self.TempSelectedAttachUnderlying().length > 0) {
                var data = {
                    CIF: CustomerUnderlying.CIF,
                    Name: CustomerUnderlying.CustomerName,
                    Type: ko.utils.arrayFirst(self.ddlDocumentType_a(), function (item) {
                        return item.ID == self.DocumentType_a().ID();
                    }),
                    Purpose: ko.utils.arrayFirst(self.ddlDocumentPurpose_a(), function (item) {
                        return item.ID == self.DocumentPurpose_a().ID();
                    })
                };
                UploadFile(data, Documents, SaveDataFile);
                // refresh data
                GetDataUnderlying();
                GetDataUnderlyingDraft();
            } else {
                alert('Please select the underlying document');
            }
            self.IsEditable(true);
        };

    };

    self.delete_u = function () {
        // hide current popup window
        $("#modal-form-Underlying").modal('hide');
        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form-Underlying").modal('show');
            } else {
                var other = self.CodeUnderlying() == '999' ? self.OtherUnderlying_u() : '';
                CustomerUnderlying.OtherUnderlying(other);
                SetMappingProformas();
                SetMappingBulks(); // Add 2015.03.09
                //Ajax call to update the Customer
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customerunderlying + "/" + CustomerUnderlying.ID(),
                    data: ko.toJSON(CustomerUnderlying), //Convert the Observable Data into JSON
                    contentType: "application/json",
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {

                        if (jqXHR.status = 200) {
                            // add to list
                            AddListItem("Delete", data.UnderlyingDraftID);
                            // send notification
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                            // refresh data
                            GetDataUnderlying();
                            GetDataUnderlyingDraft();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    self.delete_a = function (item) {
        // hide current popup window
        //$("#modal-form").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                //$("#modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customerunderlyingfile + "/" + item.ID,
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            DeleteFile(item.DocumentPath);
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetDataAttachFile();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    }


    self.save_account = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            self.IsEditable(false);
            //Ajax call to insert the CustomerAccounts
            $.ajax({
                type: "POST",
                url: api.server + api.url.customerAccount + '/' + cifData,
                data: ko.toJSON(CustomerAccount), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        // hide current popup window
                        $("#account-modal-form").modal('hide');

                        // refresh data
                        GetAccountData();
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
            self.IsEditable(true);
        }

    };

    self.update_account = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            // hide current popup window
            $("#account-modal-form").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#account-modal-form").modal('show');
                } else {
                    self.IsEditable(false);
                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.customerAccount + "/" + cifData,
                        data: ko.toJSON(CustomerAccount),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#account-modal-form").modal('hide');

                                // refresh data
                                GetAccountData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                    self.IsEditable(true);
                }
            });
        }

    };

    self.delete_account = function () {
        // hide current popup window
        $("#account-modal-form").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#account-modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customerAccount + "/" + cifData + "/Account/" + CustomerAccount.AccountNumber(),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetAccountData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    self.SaveCallBackTime = function () {
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                }
                else {
                    var callbackTime = document.getElementById('CallbackTimePicker').value;
                    callbackTime = moment(callbackTime, "HH:mm");
                    CustomerCallbackTime.Time = moment.utc(callbackTime).format(); //dalam UTC

                    var chooseProduct = ko.utils.arrayFirst(self.Products(), function (item) {
                        return item.ID == self.CallbackProductID();
                    });

                    CustomerCallbackTime.ProductName = chooseProduct.Name;

                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.customerCallback + '/' + cifData,
                        data: ko.toJSON(CustomerCallbackTime), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                //$("#CSO-modal-form").modal('hide');

                                // refresh data
                                //GetDataCSO();
                                //GetDataCsoDraft();
                            }
                            else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                    self.IsEditable(true);
                }

            });
        }
    };

    self.CloseCallBackTime = function () {
        $("#modal-form-CallbackTime").modal('hide');
        $('#backDrop').hide();
    }

    var CustomerCallbackTime = {
        CIF: self.CIF,
        CustomerName: self.Name,
        ContactID: self.CallbackContactID,
        ContactName: self.CallbackContactName,
        ProductID: self.CallbackProductID,
        ProductName: self.CallbackProductName,
        Time: "",
        IsUTC: self.CallbackIsUTC,
        Remark: self.CallbackRemark
    };


    self.FormatNumber = function (num) {
        if (ko.isObservable(num)) {
            num = num();
        }
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    }

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        cifData = data.CIF;
        self.GetParameters();
        customerNameData = data.Name;
        // fill properties
        self.IsNewData(false);
        self.CIF(data.CIF);
        self.Name(data.Name);

        GetDataByCIF(cifData);

        // fill Observable Arrays object

        self.IsGridEnable(false);
        self.IsEditCustomerContact(GetPermissionContact());
        self.IsEditCustomerUnderlying(GetPermissionUnderlying());
        self.IsViewWorkflow(false);
        self.IsCustomerCSOViewer(IsCustomerCSOViewer());
        self.IsCustomerCallbackViewer(IsCustomerCallbackViewer());
        self.IsCustomerPhoneNumberViewer(IsCustomerPhoneNumberViewer());
        self.IsCustomerPOAEmailViewer(IsCustomerPOAEmailViewer());

        self.IsCustomerCSOMaker(IsCustomerCSOMaker());
        self.IsCustomerCallbackMaker(IsCustomerCallbackMaker());
        self.IsCustomerPOAEmailMaker(IsCustomerPOAEmailMaker());

        // get Total Customer by cif
        GetTotalEqvUSD(cifData);
        // populate data customer function, customer contact, customer Underlying
        GetFunctionData();
        GetDataContact();

        if (self.IsCustomerCSOViewer()) {
            GetDataCSO();
            GetDataCsoDraft();
        }

        if (self.IsCustomerCallbackViewer()) {
            if (self.IsCustomerCallbackMaker()) {
                GetDataCallbackContact();
            }
            GetDataCallback();
            GetDataCallbackDraft();
        }

        if (self.IsCustomerPhoneNumberViewer()) {
            GetDataPhoneNumber();
        }

        if (self.IsCustomerPOAEmailViewer()) {
            GetDataPOAEmail();
            GetDataPOAEmailDraft();
        }

        GetDataUnderlying();
        GetDataUnderlyingDraft();

        GetDataAttachFile();
        GetAccountData();

        return true;
    };

    self.GetSelectedRowCustomerPOAEmailDraft = function (data) {
        self.Readonly(true);
        $("#BSave").hide();
        $("#BDelete").hide();
        $("#BUpdate").hide();
        $("#POAEmail").prop('disabled', true);

        $("#poaEmail-modal-form").modal('show');
        var length = self.cblPOAEmailProduct().length;
        var rec = eval('(' + data.Data + ')');
        self.POAEmail_POAE(rec.Email)
        //self.POAEmailTemp(rec);
        var length = self.cblPOAEmailProduct().length;
        for (var i = 0; i <= length; i++) {
            if (rec.POAEmail[i]) {
                $('#POAEmailcheckBox' + rec.POAEmail[i].POAEmailProductID).prop('checked', true);
            }
            else {
                $('#POAEmailcheckBox' + '0').prop('checked', false);
            }
        }

    };

    //Function to Display record to be updated
    self.GetFunctionSelectedRow = function (data) {
        self.IsNewFunctionData(false);
        $("#function-modal-form").modal('show');
        self.ID_f(data.ID);
        self.CIF_f(data.CIF);
        self.POAFunction_f(data.POAFunction);
        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);
    }

    //Function to Display record to be updated
    self.SetParameters = function (data) {
        data = eval('(' + data.Data + ')');
        self.BizSegment().ID(data.BizSegment.ID);
        self.Branch().ID(data.Branch.ID);
        self.Type().ID(data.Type.ID);
        self.UserApproval().ID(data.RM.ID);
    }

    self.GetContactSelectedRow = function (data) {
        self.Readonly(false)
        self.POAFunctions_c([]);
        self.AccountNumbers_c([]);
        $("#POAFunctionTabel").empty()
        $("#AccountNumberTabel").empty()
        self.Phone_c("");
        self.Area_c("");
        self.Ext_c("")
        self.IsNewData_c(false);
        self.IsEditable(true);
        if (self.IsWorkflow()) {
            $("#modal-form-workflow").modal('show');
            self.Readonly(true);
        } else {
            $("#contact-modal-form").modal('show');
            self.Readonly(self.IsLocalGetSelectedRow());
        }
        self.ID_c(data.ID);
        // data customer 
        self.CIF(data.CIF);
        self.Name(data.Name);
        self.POAName(data.POAName);
        self.BizSegment(new BizSegmentModel(data.BizSegment.ID, data.BizSegment.Name, data.BizSegment.Description));
        self.Branch(new BranchModel(data.Branch.ID, data.Branch.Name));
        self.Type(new CustomerTypeModel(data.Type.ID, data.Type.Name));
        if (data.RM != null) {
            self.UserApproval(new UserApprovalModel(data.RM.ID, data.RM.Name, data.RM.SegmentID, data.RM.RankID, data.RM.BranchID));
        } else {
            self.UserApproval(new UserApprovalModel(0, '', 0, 0, 0));
        }
        //data contact
        self.CIF_c(data.CIF);
        self.Name_c(data.NameContact);
        self.Area_c(data.PhoneNumber.split("-")[0]);
        self.Phone_c(data.PhoneNumber.split("-")[1]);
        self.Ext_c(data.PhoneNumber.split("-")[2]);
        self.DateOfBirth_c(self.LocalDate(data.DateOfBirth, true, false));
        self.Address_c(data.Address);
        self.IDNumber_c(data.IDNumber);
        self.OccupationInID_c(data.OccupationInID);
        self.PlaceOfBirth_c(data.PlaceOfBirth);
        self.EffectiveDate_c(self.LocalDate(data.EffectiveDate, true, false));
        self.CancellationDate_c(self.LocalDate(data.CancellationDate, true, false));
        self.LastModifiedBy_c(data.LastModifiedBy);
        self.LastModifiedDate_c(data.LastModifiedDate);
        if (data.POAFunction.length != 0) {
            for (var i = 0; i < data.POAFunction.length; i++) {
                self.POAFunctions_c.push(new POAFunctionModel(data.POAFunction[i].ID, data.POAFunction[i].Name, data.POAFunction[i].Description, data.POAFunction[i].POAFunctionOther));
                if (data.POAFunction[i].ID == 9) {
                    self.POAFunctionOther_c(data.POAFunction[i].POAFunctionOther);
                }
            }
        }
        if (data.AccountNumber.length != 0) {
            for (var i = 0; i < data.AccountNumber.length; i++) {
                self.AccountNumbers_c.push(data.AccountNumber[i]);
            }
        }
        for (var i = 0; i < self.POAFunctions_c().length; i++) {
            $("#POAF" + i).text(self.POAFunctions_c()[i].ID() == 9 ? (self.POAFunctions_c()[i].Description() + " - " + self.POAFunctions_c()[i].POAFunctionOther()) : self.POAFunctions_c()[i].Description());
        }

    };
    self.GetPOAEmailSelectedRow = function (data) {
        self.Readonly(false);
        var length = self.cblPOAEmailProduct().length;
        for (var i = 0; i < length; i++) {
            $('#POAEmailcheckBox' + self.cblPOAEmailProduct()[i].POAEmailProductID).prop('checked', false);
        }

        if (IsCustomerPOAEmailMaker()) {
            $("#BDelete").show();
            $("#BUpdate").show();
        }
        else {
            $("#BDelete").hide();
            $("#BUpdate").hide();
        }
        self.EmailsTemp = ko.observable();
        self.SelectedPOAEmailsTemp = ko.observableArray([]);
        for (var ii = 0; ii < Object.keys(self.CustomerPOAEmails()).length; ii++) {
            if (Object.keys(self.CustomerPOAEmails())[ii] === data) {
                self.EmailsTemp(self.CustomerPOAEmails()[Object.keys(self.CustomerPOAEmails())[ii]]);
                break;
            }
        }
        self.POAEmail_POAE(data);
        self.POAOldEmail_POAE(data);
        console.log(self.POAEmail_POAE())
        console.log(self.EmailsTemp())

        for (var i = 1; i <= length; i++) {
            if (self.EmailsTemp()[i]) {
                $('#POAEmailcheckBox' + self.EmailsTemp()[i].POAEmailProductID).prop('checked', true);
                self.SelectedPOAEmailsTemp.push(new POAEmailModelTemp(self.EmailsTemp()[i].POAEmailProductID, self.EmailsTemp()[i].ProductName, cifData));
            }
            else {
                $('#POAEmailcheckBox' + '0').prop('checked', false);
            }
        }
        self.IsNewData_POAE(false);
        self.IsEditable(true);
        if (self.IsWorkflow()) {
            $("#modal-form-workflow").modal('show');
            self.Readonly(true);
        } else {
            $("#poaEmail-modal-form").modal('show');
        }
    };
    self.close_POAE = function () {
        ko.utils.arrayForEach(self.cblPOAEmailProduct(), function (item) {
            $('#POAEmailcheckBox' + item.POAEmailProductID).prop('checked', false);
        }
        )
    };

    self.GetCallbackContactSelectedRow = function (data) {
        self.CallbackReadOnly(false);

        self.CallbackCIF(data.CIF);
        self.CallbackContactID(data.ID);
        self.CallbackContactName(data.NameContact);
        self.CallbackProductID("");
        self.CallbackProductName("");
        self.CallbackIsUTC(false);
        self.CallbackRemark("");

        var tanggal = new Date();
        var jam = tanggal.getHours() > 9 ? tanggal.getHours() : '0' + tanggal.getHours();
        var menit = tanggal.getMinutes() > 9 ? tanggal.getMinutes() : '0' + tanggal.getMinutes();

        $('#CallbackTimePicker').val(jam + ':' + menit);
        $('.bootstrap-timepicker-hour').val("00");
        $('.bootstrap-timepicker-minute').val("00");

        $("#modal-form-CallbackTime").modal('show');
        $('#backDrop').show();
    };

    self.GetCallbackDraftSelectedRow = function (data) {
        self.CallbackReadOnly(true);

        var callbackTimeData = JSON.parse(data.Data);

        self.CallbackContactID(callbackTimeData.ID);
        self.CallbackContactName(callbackTimeData.ContactName);
        self.CallbackProductID(callbackTimeData.ProductID);
        self.CallbackProductName(callbackTimeData.ProductName);
        self.CallbackIsUTC(callbackTimeData.IsUTC);
        self.CallbackRemark(callbackTimeData.Remark);

        var callbackTime = self.LocalDate(callbackTimeData.Time); //moment.utc(callbackTimeData.Time);
        //callbackTime = moment(callbackTime).local();

        self.CallbackTime(callbackTime);

        //$('#CallbackTimePicker').val(callbackTime);
        //$('.bootstrap-timepicker-hour').val("00");
        //$('.bootstrap-timepicker-minute').val("00");

        $("#modal-form-CallbackTime").modal('show');
        $('#backDrop').show();
    };

    //Function to Display record to be updated
    self.GetUnderlyingSelectedRow = function (data) {

        self.IsNewDataUnderlying(false);
        self.IsViewWorkflow(false);
        self.IsInisiator(false);
        $('#modal-form-Underlying').animate({ scrollTop: 0 }, 'fast');
        $("#modal-form-Underlying").modal('show');

        //set popup underlying
        $('#tabPopupUnderlying').show();
        $('#liUnderlyingDetails').addClass('active');
        $('#liUnderlyingHistory').removeClass('active');
        $('#liTransactionHistory').removeClass('active');
        $('#tabunderlyingdetails').addClass('active');
        $('#tabunderlyinghistory').removeClass('active');
        $('#tabtransactionhistory').removeClass('active');

        self.DynamicStatementLetter_u(self.ddlStatementLetter_u());
        self.ID_u(data.ID);
        self.IsUtilize_u(data.IsUtilize);
        self.CustomerName_u(data.CustomerName);
        self.UnderlyingDocument_u(new UnderlyingDocumentModel(data.UnderlyingDocument.ID, data.UnderlyingDocument.Name, data.UnderlyingDocument.Code));
        self.OtherUnderlying_u(data.OtherUnderlying);
        self.DocumentType_u(new DocumentTypeModel(data.DocumentType.ID, data.DocumentType.Name));
        self.Currency_u(new CurrencyModel(data.Currency.ID, data.Currency.Code, data.Currency.Description));
        self.Amount_u(data.Amount);
        self.Rate_u(data.Rate);
        self.AmountUSD_u(data.AmountUSD);
        self.AvailableAmount_u(data.AvailableAmount);
        self.AttachmentNo_u(data.AttachmentNo);
        self.StatementLetter_u(new StatementLetterModel(data.StatementLetter.ID, data.StatementLetter.Name));
        self.IsDeclarationOfException_u(data.IsDeclarationOfException);
        self.StartDate_u(self.LocalDate(data.StartDate, true, false));
        self.EndDate_u(self.LocalDate(data.EndDate, true, false));
        self.DateOfUnderlying_u(self.LocalDate(data.DateOfUnderlying, true, false));
        self.ExpiredDate_u(self.LocalDate(data.ExpiredDate, true, false));
        self.ReferenceNumber_u(data.ReferenceNumber);
        self.SupplierName_u(data.SupplierName);
        self.InvoiceNumber_u(data.InvoiceNumber);
        self.IsProforma_u(data.IsProforma);
        self.IsSelectedProforma(data.IsSelectedProforma);
        self.IsBulkUnderlying_u(data.IsBulkUnderlying); // add 2015.03.09
        self.IsSelectedBulk(data.IsSelectedBulk); // add 2015.03.09
        self.tempAmountBulk(0);//add 2015.03.09
        self.IsJointAccount_u(data.IsJointAccount);
        self.AccountNumber_u(data.AccountNumber);
        self.LastModifiedBy_u(data.LastModifiedBy);
        self.LastModifiedDate_u(data.LastModifiedDate);

        //Call Grid Underlying for Check Proforma
        GetSelectedProformaID(GetDataUnderlyingProforma);
        //GetDataUnderlyingProforma();
        //Call Grid Underlying for Check Bullk // add 2015.03.09
        GetSelectedBulkID(GetDataBulkUnderlying);
        //GetDataBulkUnderlying();
        //SetSelectedProforma();
        GetSelectedUnderlyingHistory(data.ID);

        self.IsInisiator(data.CreatedBy == spUser.LoginName.substr(spUser.LoginName.lastIndexOf('|') + 1) ? true : false);
    };

    //start add azam
    self.GetCsoSelectedViewRow = function (data) {

        //self.IsNewDataUnderlying(false); remark dapat hide link new underlying
        //self.IsEditCustomerUnderlying(false); remark dapat hide link new underlying
        self.IsViewWorkflow(true);
        self.IsInisiator(false);
        $("#CSO-modal-form").modal('show');
        self.CustomerCsoID(data.ID);
        self.CIF_c(cifData);
        self.EmployeeCSOID_c(data.ID);
        self.Employee(new EmployeeModel(data.Employee.EmployeeID, data.Employee.EmployeeName, data.Employee.EmployeeUserName, data.Employee.EmployeeEmail));


    };
    //end azam

    //Function to Display record to be updated
    self.GetUnderlyingSelectedViewRow = function (data) {

        //self.IsNewDataUnderlying(false); remark dapat hide link new underlying
        //self.IsEditCustomerUnderlying(false); remark dapat hide link new underlying
        self.IsViewWorkflow(true);
        self.IsInisiator(false);
        console.log('trace');
        $('#modal-form-Underlying').animate({ scrollTop: 0 }, 'fast');
        $("#modal-form-Underlying").modal('show');

        //set popup underlying
        $('#tabPopupUnderlying').show();
        $('#liUnderlyingDetails').addClass('active');
        $('#liUnderlyingHistory').removeClass('active');
        $('#liTransactionHistory').removeClass('active');
        $('#tabunderlyingdetails').addClass('active');
        $('#tabunderlyinghistory').removeClass('active');
        $('#tabtransactionhistory').removeClass('active');

        self.ID_u(data.ID);
        self.IsUtilize_u(data.IsUtilize);
        self.CustomerName_u(data.CustomerName);
        self.UnderlyingDocument_u(new UnderlyingDocumentModel(data.UnderlyingDocument.ID, data.UnderlyingDocument.Name, data.UnderlyingDocument.Code));
        self.OtherUnderlying_u(data.OtherUnderlying);
        self.DocumentType_u(new DocumentTypeModel(data.DocumentType.ID, data.DocumentType.Name));
        self.Currency_u(new CurrencyModel(data.Currency.ID, data.Currency.Code, data.Currency.Description));
        self.Amount_u(data.Amount);
        self.AvailableAmount_u(data.AvailableAmount);
        self.AttachmentNo_u(data.AttachmentNo);
        self.StatementLetter_u(new StatementLetterModel(data.StatementLetter.ID, data.StatementLetter.Name));
        self.IsDeclarationOfException_u(data.IsDeclarationOfException);
        self.StartDate_u(self.LocalDate(data.StartDate, true, false));
        self.EndDate_u(self.LocalDate(data.EndDate, true, false));
        self.DateOfUnderlying_u(self.LocalDate(data.DateOfUnderlying, true, false));
        self.ExpiredDate_u(self.LocalDate(data.ExpiredDate, true, false));
        self.ReferenceNumber_u(data.ReferenceNumber);
        self.SupplierName_u(data.SupplierName);
        self.InvoiceNumber_u(data.InvoiceNumber);
        self.IsProforma_u(data.IsProforma);
        self.IsSelectedProforma(data.IsSelectedProforma);
        self.IsBulkUnderlying_u(data.IsBulkUnderlying); // add 2015.03.09
        self.IsSelectedBulk(data.IsSelectedBulk); // add 2015.03.09
        self.tempAmountBulk(0);//add 2015.03.09
        self.LastModifiedBy_u(data.LastModifiedBy);
        self.LastModifiedDate_u(data.LastModifiedDate);
        //Call Grid Underlying for Check Proforma
        GetSelectedProformaID(GetDataUnderlyingProforma);
        //GetDataUnderlyingProforma();
        //Call Grid Underlying for Check Bullk // add 2015.03.09
        GetSelectedBulkID(GetDataBulkUnderlying);
        //GetDataBulkUnderlying();
        //SetSelectedProforma();
        self.IsInisiator(data.CreatedBy == spUser.LoginName.substr(spUser.LoginName.lastIndexOf('|') + 1) ? true : false);
    };

    //Function to Display record to be updated
    self.GetAccountSelectedRow = function (data) {
        self.IsNewAccountData(false);

        $("#account-modal-form").modal('show');

        self.CIF_a(cifData);
        self.AccountNumber_a(data.AccountNumber);
        self.Currency_a(new CurrencyModel(data.Currency.ID, data.Currency.Code, data.Currency.Description));
        self.IsJointAccount_a(data.IsJointAccount);
        self.LastModifiedBy_a(data.LastModifiedBy);
        self.LastModifiedDate_a(data.LastModifiedDate);
    };

    self.GetCSOSelectedRow = function (data) {
        self.IsNewCSOData(false);
        self.IsEditCSOData(true);
        $("#CSO-modal-form").modal('show');
        self.CustomerCsoID(data.ID);
        self.CIF_c(cifData);
        self.EmployeeCSOID_c(data.ID);
        self.Employee(new EmployeeModel(data.Employee.EmployeeID, data.Employee.EmployeeName, data.Employee.EmployeeUserName, data.Employee.EmployeeEmail));
    };
    self.NewCSOData = function () {
        self.IsNewCSOData(true);
        self.IsEditCSOData(false);
        $("#CSO-modal-form").modal('show');
        self.CIF_c(cifData);
        self.EmployeeCSOID_c("");
        self.Employee(new EmployeeModel('', '', '', ''));

    };

    var CustomerCSO = {
        //CustCSOID: self.EmployeeCSOID_c,
        CustCSOID: self.CustomerCsoID,
        CIF: self.CIF_c,
        Employee: self.Employee

    };
    self.save_cso = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();
        if (countResult === 0) {
            ShowNotification("Form Validation Warning", "Employee Name not found in Active Directory", 'gritter-warning', true);
            return false;
        }

        else {
            if (form.valid()) {
                //$("#modal-form").modal('hide');
                bootbox.confirm("Are you sure?", function (result) {
                    if (!result) {
                        $("#modal-form").modal('show');
                    } else {

                        //Ajax call to insert the CSO
                        $.ajax({
                            type: "POST",
                            url: api.server + api.url.customerCSO + '/' + cifData,
                            data: ko.toJSON(CustomerCSO), //Convert the Observable Data into JSON
                            contentType: "application/json",
                            headers: {
                                "Authorization": "Bearer " + accessToken
                            },
                            success: function (data, textStatus, jqXHR) {
                                if (jqXHR.status = 200) {
                                    // send notification
                                    ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                    // hide current popup window
                                    $("#CSO-modal-form").modal('hide');

                                    // refresh data
                                    GetDataCSO();
                                    GetDataCsoDraft();
                                } else {
                                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                // send notification
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                            }
                        });
                        self.IsEditable(true);
                    }

                });
            }
        }
    };

    self.update_cso = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();
        if (countResult === 0) {
            ShowNotification("Form Validation Warning", "Employee Name not found in Active Directory", 'gritter-warning', true);
            return false;
        }

        else {

            if (form.valid()) {
                // hide current popup window

                bootbox.confirm("Are you sure?", function (result) {
                    if (!result) {
                        $("#modal-form").modal('show');
                    } else {
                        self.IsEditable(false);
                        //Ajax call to update the Customer
                        $.ajax({
                            type: "PUT",
                            url: api.server + api.url.customerCSO + "/" + self.EmployeeCSOID_c() + "/" + cifData,
                            data: ko.toJSON(CustomerCSO),
                            contentType: "application/json",
                            headers: {
                                "Authorization": "Bearer " + accessToken
                            },
                            success: function (data, textStatus, jqXHR) {
                                if (jqXHR.status = 200) {
                                    // send notification
                                    ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                    // hide current popup window
                                    $("#CSO-modal-form").modal('hide');

                                    // refresh data
                                    GetDataCSO();
                                    GetDataCsoDraft();
                                } else {
                                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                // send notification
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                            }
                        });
                        self.IsEditable(true);
                    }
                });
            }
        }
    };

    self.delete_cso = function () {
        // hide current popup window
        // $("#CSO-modal-form").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#CSO-modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customerCSO + "/" + self.EmployeeCSOID_c() + "/" + cifData,
                    data: ko.toJSON(CustomerCSO),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetDataCSO();
                            GetDataCsoDraft(); // add azam
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    // Customer insert new
    self.NewData = function () {
        ClearFieldCustomer();
        self.IsGridEnable(false);
        self.IsNewData(true);
        self.Readonly(false);
        cifData = "0";
        customerNameData = "";
        self.CustomerAccounts([]);
        self.CustomerFunctions([]);
        self.CustomerContacts([]);
        self.CustomerPhoneNumbers([]);
        self.CustomerPOAEmails([]);
        self.CustomerUnderlyings([]);
        self.CustomerUnderlyingDrafts([]);
        self.CustomerCsoDrafts([]); // add azam
        self.CustomerUnderlyingProformas([]);
        self.CustomerBulkUnderlyings([]);//add 2015.03.09
        self.CustomerUnderlyingFiles([]);
        self.CustomerAttachUnderlyings([]);
        self.CustomerCSO([]);
    };

    // Customer Function insert new
    self.NewFunctionData = function () {
        // flag as new Data
        self.IsNewFunctionData(true);
        // bind empty data
        self.ID_f(0);
        self.CIF_f(cifData);
        self.POAFunction_f(new POAFunctionModel('', '', ''));
        self.LastModifiedBy('');
        self.LastModifiedDate('');
    };

    //Customer Contact insert new
    self.NewData_c = function () {
        // flag as new Data

        $('input:checkbox').prop('checked', false);
        self.IsNewData_c(true);
        self.Readonly(false);
        self.IsEditable(true);
        // bind empty data
        self.ID_c(0);
        self.CIF_c(cifData);
        self.Name_c("");
        self.Area_c("");
        self.Phone_c("");
        self.Ext_c("");
        self.POAFunctions_c([]);
        self.AccountNumbers_c([]);
        self.AccountNumber_c("");
        self.DateOfBirth_c("");
        self.IDNumber_c("");
        self.POAFunction_c(new POAFunctionModel(0, '', ''));
        self.POAFunctionID_c('');
        self.OccupationInID_c("");
        self.PlaceOfBirth_c("");
        self.EffectiveDate_c("");
        self.CancellationDate_c("");
        self.POAFunctionOther_c("");

    };
    self.NewData_POAE = function () {
        self.Readonly(false);
        self.IsNewData_POAE(true);
        self.IsEditable(true);
        $("#POAEmailChkAll").val('checked', false)
        $("#POAEmail").prop('disabled', false);
        $("#BSave").show();
        ko.utils.arrayForEach(self.cblPOAEmailProduct(), function (items) {
            $('#POAEmailcheckBox' + items.POAEmailProductID).prop('checked', false);
        });
        self.SelectedPOAEmailsTemp = ko.observableArray([]);
        self.CIF_POAE(cifData)
        self.CustomerPOAEmailID_POAE("");
        self.POAEmail_POAE("");
        self.POAEmailProductID_POAE("");
        self.UpdateDate_POAE("");
        self.UpdateBy_POAE("");
        self.CreateDate_POAE("");
        self.CreateBy_POAE("");
    }
    //Add Attach File
    self.NewAttachFile = function () {
        // show the dialog task form
        $("#modal-form-Attach").modal('show');
        self.TempSelectedAttachUnderlying([]);
        self.ID_a(0);
        self.DocumentPurpose_a(new DocumentPurposeModel('', '', ''));
        self.DocumentType_a(new DocumentTypeModel('', ''));
        self.DocumentPath_a('');
        GetDataUnderlyingAttach();
    }
    //Customer Underlying insert new
    self.NewDataUnderlying = function () {
        //Remove required filed if show
        $('.form-group').removeClass('has-error').addClass('has-info');
        $('.help-block').remove();

        //Set underlying popup
        $('#tabunderlyingdetails').addClass('active');
        $('#tabunderlyinghistory').removeClass('active');
        $('#tabtransactionhistory').removeClass('active');
        $('#tabPopupUnderlying').hide();

        // flag as new Data
        self.IsNewDataUnderlying(true);
        self.IsViewWorkflow(false);
        self.Readonly(false);
        // bind empty data
        self.ID_u(0);
        self.UnderlyingDocument_u(new UnderlyingDocumentModel('', '', ''));
        self.OtherUnderlying_u('');
        self.DocumentType_u(new DocumentTypeModel('', ''));
        self.StatementLetter_u(new StatementLetterModel('', ''));
        self.Currency_u(new CurrencyModel('', '', ''));
        self.Amount_u(0);
        self.AttachmentNo_u('');
        self.IsDeclarationOfException_u(false);
        self.StartDate_u('');
        self.EndDate_u('');
        self.DateOfUnderlying_u('');
        self.ExpiredDate_u('');
        self.ReferenceNumber_u('');
        self.SupplierName_u('');
        self.InvoiceNumber_u('');
        self.IsProforma_u(false);
        self.IsSelectedProforma(false);
        self.IsBulkUnderlying_u(false); //add 2015.03.09
        self.IsSelectedBulk(false); //add 2015.03.09
        //Call Grid Underlying for Check Proforma
        GetDataUnderlyingProforma();
        //Call Grid Underlying fo check Bulk //add 2015.03.09
        GetDataBulkUnderlying();
        self.TempSelectedUnderlyingProforma([]);
        //SetStatementA(false,null);
        setRefID();


    };

    //Customer Underlying insert new
    self.NewDataUnderlyingExcel = function () {
        // show current popup window
        $("#Underlying-modal-form").modal('show');
        GetDataCustomerUnderlying();
        setRefID();
        //$('td:nth-child(2),th:nth-child(2)').hide();
    };

    //insert new Customer Account
    self.NewAccountData = function () {
        // flag as new Data
        self.IsNewAccountData(true);
        // bind empty data
        self.CIF_a(cifData);
        self.AccountNumber_a('');
        self.Currency_a(new CurrencyModel('', '', ''));
    };

    self.GetFilterValue = function () {
        return GetFilterValue(cifData);
    }

    // function to Clear value properties
    function ClearFieldCustomer() {
        // bind empty data
        self.IsNewData(true);
        self.CIF('');
        self.Name('');
        self.POAName('');
        self.BizSegment(new BizSegmentModel('', '', ''));
        self.Branch(new BranchModel('', ''));
        self.Type(new CustomerTypeModel('', ''));
        self.UserApproval(new UserApprovalModel(0, '', 0, 0, 0));
        //self.RMCode('');
        self.CustomerAccounts([]);
    }

    //Function to Read All Customers
    function GetData() {
        console.log('trace this pages');
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customer,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }
    // Function to Calculate Amount USD
    function Read_u() {
        if (typeof self.Amount_u() != 'number') {
            var value = parseFloat(self.Amount_u().replace(',', '').replace(',', '').replace(',', '').replace(',', ''));
        } else {
            var value = parseFloat(self.Amount_u());
        }
        var res = parseFloat(value) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
        res = Math.round(res * 100) / 100;
        res = isNaN(res) ? 0 : res; //avoid NaN
        self.AmountUSD_u(parseFloat(res).toFixed(2));
    }
    //Function to Read Customer data by CIF
    function GetDataByCIF(sCIF) {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customer + "/" + sCIF,
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetDataCustomer, OnError, OnAlways);
    }

    //function to Get Total Eqv USD
    function GetTotalEqvUSD(sCif) {

        var requestAjax = $.ajax({
            type: "GET",
            url: api.server + api.url.helper + '/GetCalculatePPU/' + sCif,
            contentType: "application/json; charset=utf-8; odata=verbose;",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                self.TotalEqvUsd(formatNumber(data.Total.IDRFCYPPU));
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });
        // execute ajax request
        $.ajax(requestAjax);

    }
    //Function to Read All Customers
    function GetFunctionData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerFunction,
            params: {
                page: self.FunctionsGridProperties().Page(),
                size: self.FunctionsGridProperties().Size(),
                sort_column: self.FunctionsGridProperties().SortColumn(),
                sort_order: self.FunctionsGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFunctionFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetFunctionData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetFunctionData, OnError, OnAlways);
        }
    }

    //Function to Read All Customers
    function GetDataContact() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customercontact,
            params: {
                page: self.ConctactGridProperties().Page(),
                size: self.ConctactGridProperties().Size(),
                sort_column: self.ConctactGridProperties().SortColumn(),
                sort_order: self.ConctactGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetContactFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataContact, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataContact, OnError, OnAlways);
        }
    }

    function GetDataCallbackContact() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customercontact,
            params: {
                page: self.CallbackContactGridProperties().Page(),
                size: self.CallbackContactGridProperties().Size(),
                sort_column: self.CallbackContactGridProperties().SortColumn(),
                sort_order: self.CallbackContactGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetCallbackContactFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataCallbackContact, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataCallbackContact, OnError, OnAlways);
        }
    }

    function GetDataCallback() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerCallback,
            params: {
                page: self.CallbackGridProperties().Page(),
                size: self.CallbackGridProperties().Size(),
                sort_column: self.CallbackGridProperties().SortColumn(),
                sort_order: self.CallbackGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetCallbackFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataCallback, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataCallback, OnError, OnAlways);
        }
    }

    function GetDataCallbackDraft() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.workflowEntity,
            params: {
                page: self.CallbackDraftGridProperties().Page(),
                size: self.CallbackDraftGridProperties().Size(),
                sort_column: self.CallbackDraftGridProperties().SortColumn(),
                sort_order: self.CallbackDraftGridProperties().SortOrder(),
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetCallbackDraftFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataCallbackDraft, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataCallbackDraft, OnError, OnAlways);
        }
    }

    function GetDataPhoneNumber() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerPhoneNumber,
            params: {
                CIF: cifData,
                page: self.PhoneNumberGridProperties().Page(),
                size: self.PhoneNumberGridProperties().Size(),
                sort_column: self.PhoneNumberGridProperties().SortColumn(),
                sort_order: self.PhoneNumberGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetPhoneNumberFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataPhoneNumber, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataPhoneNumber, OnError, OnAlways);
        }
    }
    function GetDataPOAEmail() {
        var options = {
            url: api.server + api.url.poaemailProduct + "/GetDataPOAEmail",
            params: {
                CIF: cifData
            },
            token: accessToken
        };
        Helper.Ajax.Get(options, OnSuccessGetDataPOAEmail, OnError, OnAlways);

    }
    function GetDataPOAEmailGrid() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.poaemailProduct,
            params: {
                CIF: cifData,
                page: self.POAEmailGridProperties().Page(),
                size: self.POAEmailGridProperties().Size(),
                sort_column: self.POAEmailGridProperties().SortColumn(),
                sort_order: self.POAEmailGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetPOAEmailFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataPOAEmailGrid, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataPOAEmailGrid, OnError, OnAlways);
        }
    }
    //Function to Read All Customers Underlying
    function GetDataUnderlying() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Underlying",
            params: {
                page: self.UnderlyingGridProperties().Page(),
                size: self.UnderlyingGridProperties().Size(),
                sort_column: self.UnderlyingGridProperties().SortColumn(),
                sort_order: self.UnderlyingGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetUnderlyingFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        }
    }

    //start add azam
    function GetDataCsoDraft() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerCsoDraft + '/' + cifData + '/CSO',
            params: {
                page: self.CustomerCsoDraftGridProperties().Page(),
                size: self.CustomerCsoDraftGridProperties().Size(),
                sort_column: self.CustomerCsoDraftGridProperties().SortColumn(),
                sort_order: self.CustomerCsoDraftGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetCutomerCsoDraftFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataCsoDraft, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataCsoDraft, OnError, OnAlways);
        }
    }
    //end azam

    //Function to Read All Customers Underlying Draft
    function GetDataUnderlyingDraft() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/UnderlyingDraft",
            params: {
                page: self.UnderlyingDraftGridProperties().Page(),
                size: self.UnderlyingDraftGridProperties().Size(),
                sort_column: self.UnderlyingDraftGridProperties().SortColumn(),
                sort_order: self.UnderlyingDraftGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetUnderlyingDraftFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataUnderlyingDraft, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataUnderlyingDraft, OnError, OnAlways);
        }
    }

    //Function to Read All Customers Underlying File
    function GetDataAttachFile() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlyingfile,
            params: {
                page: self.AttachGridProperties().Page(),
                size: self.AttachGridProperties().Size(),
                sort_column: self.AttachGridProperties().SortColumn(),
                sort_order: self.AttachGridProperties().SortOrder()
            },
            token: accessToken
        };
        self.AttachFilterAccountNumber(self.UnderlyingFilterAccountNumber());
        // get filtered columns
        var filters = GetAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        }
    }

    //Function to Read All Customers Underlying for Attach File
    function GetDataUnderlyingAttach() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Attach",
            params: {
                page: self.UnderlyingAttachGridProperties().Page(),
                size: self.UnderlyingAttachGridProperties().Size(),
                sort_column: self.UnderlyingAttachGridProperties().SortColumn(),
                sort_order: self.UnderlyingAttachGridProperties().SortOrder()
            },
            token: accessToken
        };
        self.UnderlyingAttachFilterAccountNumber(self.UnderlyingFilterAccountNumber());
        // get filtered columns
        var filters = GetUnderlyingAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingDataAttachFile, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingDataAttachFile, OnError, OnAlways);
        }
    }

    //Function to Read All Customers Underlying Proforma
    function GetDataUnderlyingProforma() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Proforma",
            params: {
                page: self.UnderlyingProformaGridProperties().Page(),
                size: self.UnderlyingProformaGridProperties().Size(),
                sort_column: self.UnderlyingProformaGridProperties().SortColumn(),
                sort_order: self.UnderlyingProformaGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetUnderlyingProformaFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingProforma, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingProforma, OnError, OnAlways);
        }
    }

    //Function to Read All Customers Bulk Underlying // add 2015.03.09
    function GetDataBulkUnderlying() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Bulk",
            params: {
                page: self.UnderlyingBulkGridProperties().Page(),
                size: self.UnderlyingBulkGridProperties().Size(),
                sort_column: self.UnderlyingBulkGridProperties().SortColumn(),
                sort_order: self.UnderlyingBulkGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns

        var filters = GetBulkUnderlyingFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingBulk, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingBulk, OnError, OnAlways);
        }
    }

    //Function to Read All Customers
    function GetAccountData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            //url: api.server + api.url.customerAccount,api/Customer/{cif}/Account
            url: api.server + api.url.customer + '/' + cifData + '/Account',
            params: {
                page: self.AccountGridProperties().Page(),
                size: self.AccountGridProperties().Size(),
                sort_column: self.AccountGridProperties().SortColumn(),
                sort_order: self.AccountGridProperties().SortOrder()
            },
            token: accessToken
        };


        // get filtered columns
        var filters = GetAccountFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetAccountData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetAccountData, OnError, OnAlways);
        }
    }

    //Function to Read All CSO
    function GetDataCSO() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customer + '/' + cifData + '/CSO',
            params: {
                page: self.CSOGridProperties().Page(),
                size: self.CSOGridProperties().Size(),
                sort_column: self.CSOGridProperties().SortColumn(),
                sort_order: self.CSOGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetCSOFilteredColumns();
        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataCSO, OnError, OnAlways);
        } else {
            // GET           
            Helper.Ajax.Get(options, OnSuccessGetDataCSO, OnError, OnAlways);
        }

    }

    // Function to get All selected Proforma
    function GetSelectedProformaID(callback) {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: self.CIF_u() });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.ProformaFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.ProformaFilterIsSelected() });
        if (self.ProformaFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.ProformaFilterUnderlyingDocument() });
        if (self.ProformaFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.ProformaFilterDocumentType() });


        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SelectedProformaID",
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON(filters),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlyingProforma([]);
                    for (var i = 0; i < data.length; i++) {
                        self.TempSelectedUnderlyingProforma.push({ ID: data[i].ID });
                    }
                    if (callback != null) {
                        callback();
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    // Function to get All selected Bulk Underlying // add 2015.03.09
    function GetSelectedBulkID(callback) {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: self.CIF_u() });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.BulkFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.BulkFilterIsSelected() });
        if (self.BulkFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.BulkFilterUnderlyingDocument() });
        if (self.BulkFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.BulkFilterDocumentType() });


        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SelectedBulkID",
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON(filters),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlyingBulk([]);
                    for (var i = 0; i < data.length; i++) {
                        self.TempSelectedUnderlyingBulk.push({ ID: data[i].ID, Amount: data[i].Amount });
                    }
                    if (callback != null) {
                        callback();
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetSelectedUnderlyingHistory(underlyingID) {
        var options = {
            url: api.server + api.url.customerunderlying + "/History/" + underlyingID,
            token: accessToken
        };

        Helper.Ajax.Get(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status = 200) {
                self.CustomerUnderlyingHistories(ko.mapping.toJS(data[0]));
                self.CustomerUnderlyingTransactionHistories(ko.mapping.toJS(data[1]));
            }
            else {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
            }
        }, OnError, OnAlways);

    }

    // get all parameters need
    function GetParameters() {
        // widget reloader function start
        if ($box.css('position') == 'static') { $remove = true; $box.addClass('position-relative'); }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        var options = {
            url: api.server + api.url.parameter,
            params: {
                select: "Branch,BizSegment,CustomerType,POAFunction,UserApproval,statementletter,doctype,underlyingdoc,currency,purposedoc,customer,product"
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
    }
    function GetPOAEmailProduct() {
        var options = {
            url: api.server + api.url.poaemailProduct + "/MasterProduct",
            token: accessToken
        };
        Helper.Ajax.Get(options, OnSuccessGetPOAEmailProduct, OnError, OnAlways);

    }

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterCIF() != "") filters.push({ Field: 'CIF', Value: self.FilterCIF() });
        if (self.FilterName() != "") filters.push({ Field: 'Name', Value: self.FilterName() });
        if (self.FilterPOAName() != "") filters.push({ Field: 'POAName', Value: self.FilterPOAName() });
        if (self.FilterBizSegment() != "") filters.push({ Field: 'BizSegment', Value: self.FilterBizSegment() });
        if (self.FilterBranch() != "") filters.push({ Field: 'Branch', Value: self.FilterBranch() });
        if (self.FilterType() != "") filters.push({ Field: 'Type', Value: self.FilterType() });
        if (self.FilterLastModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterLastModifiedBy() });
        if (self.FilterLastModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterLastModifiedDate() });

        return filters;
    };

    // Get filtered columns value
    function GetFunctionFilteredColumns() {
        // define filter
        var filters = [];
        self.FilterPOAFunctionCIF(cifData);
        if (self.FilterPOAFunctionCIF() != "") filters.push({ Field: 'POAFunctionCIF', Value: self.FilterPOAFunctionCIF() });
        if (self.FilterPOAFunctionName() != "") filters.push({ Field: 'POAFunctionName', Value: self.FilterPOAFunctionName() });
        if (self.FilterPOAFunctionDescription() != "") filters.push({ Field: 'POAFunctionDescription', Value: self.FilterPOAFunctionDescription() });
        return filters;
    };

    // Get filtered columns value
    function GetPhoneNumberFilteredColumns() {
        // define filter
        var filters = [];
        if (self.FilterCityCode() != "") filters.push({ Field: 'CityCode', value: self.FilterCityCode() });
        if (self.FilterContactType() != "") filters.push({ Field: 'ContactType', value: self.FilterContactType() });
        if (self.FilterCountryCode() != "") filters.push({ Field: 'CountryCode', value: self.FilterCountryCode() });
        if (self.FilterPhoneNumber() != "") filters.push({ Field: 'PhoneNumber', value: self.FilterPhoneNumber() });
        if (self.FilterCreateBy() != "") filters.push({ Field: 'CreateBy', value: self.FilterUpdateBy() });
        if (self.FilterUpdateBy() != "") filters.push({ Field: 'UpdateBy', value: self.FilterUpdateBy() });
        if (self.FilterUpdateDate() != "") filters.push({ Field: 'UpdateDate', value: self.FilterUpdateDate() });
        return filters;
    };
    function GetPOAEmailFilteredColumns() {
        // define filter
        var filters = [];
        if (self.FilterPOAEmail_POAE() != "") filters.push({ Field: 'POAEmail', value: self.FilterPOAEmail_POAE() });
        if (self.FilterCreateBy_POAE() != "") filters.push({ Field: 'CreateBy', value: self.FilterUpdateBy_POAE() });
        if (self.FilterUpdateBy_POAE() != "") filters.push({ Field: 'UpdateBy', value: self.FilterUpdateBy_POAE() });
        if (self.FilterUpdateDate_POAE() != "") filters.push({ Field: 'UpdateDate', value: self.FilterUpdateDate_POAE() });
        return filters;
    };
    function GetContactFilteredColumns() {
        // define filter
        var filters = [];
        self.FilterCIF_c(cifData);
        if (self.FilterCIF_c() != "") filters.push({ Field: 'CIF', Value: self.FilterCIF_c() });
        if (self.FilterName_c() != "") filters.push({ Field: 'NameContact', Value: self.FilterName_c() });
        if (self.FilterPhoneNumber_c() != "") filters.push({ Field: 'PhoneNumber', Value: self.FilterPhoneNumber_c() });
        if (self.FilterDateOfBirth_c() != "") filters.push({ Field: 'DateOfBirth', Value: self.FilterDateOfBirth_c() });
        if (self.FilterIDNumber_c() != "") filters.push({ Field: 'IDNumber', Value: self.FilterIDNumber_c() });
        if (self.FilterPOAFunction_c() != "") filters.push({ Field: 'POAFunctionVW', Value: self.FilterPOAFunction_c() });
        if (self.FilterOccupationInID_c() != "") filters.push({ Field: 'OccupationInID', Value: self.FilterOccupationInID_c() });
        if (self.FilterPlaceOfBirth_c() != "") filters.push({ Field: 'PlaceOfBirth', Value: self.FilterPlaceOfBirth_c() });
        if (self.FilterEffectiveDate_c() != "") filters.push({ Field: 'EffectiveDate', Value: self.FilterEffectiveDate_c() });
        if (self.FilterCancellationDate_c() != "") filters.push({ Field: 'CancellationDate', Value: self.FilterCancellationDate_c() });

        return filters;
    };

    function GetCallbackContactFilteredColumns() {
        var filters = [];
        self.FilterCallbackContactCIF(cifData);
        if (self.FilterCallbackContactCIF() != "") filters.push({ Field: 'CIF', Value: self.FilterCallbackContactCIF() });
        if (self.FilterCallbackContactName() != "") filters.push({ Field: 'NameContact', Value: self.FilterCallbackContactName() });
        if (self.FilterCallbackContactPhoneNumber() != "") filters.push({ Field: 'PhoneNumber', Value: self.FilterCallbackContactPhoneNumber() });
        if (self.FilterCallbackContactDateOfBirth() != "") filters.push({ Field: 'DateOfBirth', Value: self.FilterCallbackContactDateOfBirth() });
        if (self.FilterCallbackContactIDNumber() != "") filters.push({ Field: 'IDNumber', Value: self.FilterCallbackContactIDNumber() });
        if (self.FilterCallbackContactPOAFunction() != "") filters.push({ Field: 'POAFunction', Value: self.FilterCallbackContactPOAFunction() });
        if (self.FilterCallbackContactOccupationInID() != "") filters.push({ Field: 'OccupationInID', Value: self.FilterCallbackContactOccupationInID() });
        if (self.FilterCallbackContactPlaceOfBirth() != "") filters.push({ Field: 'PlaceOfBirth', Value: self.FilterCallbackContactPlaceOfBirth() });
        if (self.FilterCallbackContactEffectiveDate() != "") filters.push({ Field: 'EffectiveDate', Value: self.FilterCallbackContactEffectiveDate() });
        if (self.FilterCallbackContactCancellationDate() != "") filters.push({ Field: 'CancellationDate', Value: self.FilterCallbackContactCancellationDate() });

        return filters;
    };

    function GetCallbackFilteredColumns() {
        var filters = [];
        self.FilterCallbackCIF(cifData);

        if (self.FilterCallbackCIF() != "") filters.push({ Field: 'CIF', Value: self.FilterCallbackCIF() });
        if (self.FilterCallbackNameContact() != "") filters.push({ Field: 'ContactName', Value: self.FilterCallbackNameContact() });
        if (self.FilterCallbackProductName() != "") filters.push({ Field: 'ProductName', Value: self.FilterCallbackProductName() });
        if (self.FilterCallbackTime() != "") filters.push({ Field: 'Time', Value: self.FilterCallbackTime() });
        if (self.FilterCallbackRemark() != "") filters.push({ Field: 'Remark', Value: self.FilterCallbackRemark() });
        if (self.FilterCallbackIsUTC() != "") filters.push({ Field: 'IsUTC', Value: self.FilterCallbackIsUTC() });
        if (self.FilterCallbackModifiedBy() != "") filters.push({ Field: 'ModifiedBy', Value: self.FilterCallbackModifiedBy() });
        if (self.FilterCallbackModifiedDate() != "") filters.push({ Field: 'ModifiedDate', Value: self.FilterCallbackModifiedDate() });

        return filters;
    };

    function GetCallbackDraftFilteredColumns() {
        var filters = [];
        self.FilterCallbackDraftCIF(cifData);

        filters.push({ Field: 'EntityName', Value: 'CustomerCallback' });
        filters.push({ Field: 'FilterValue', Value: self.FilterCallbackDraftCIF() });

        if (self.FilterCallbackDraftTaskName() != "") filters.push({ Field: 'TaskName', Value: self.FilterCallbackDraftTaskName() });
        if (self.FilterCallbackDraftActionType() != "") filters.push({ Field: 'ActionType', Value: self.FilterCallbackDraftActionType() });
        if (self.FilterCallbackDraftModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterCallbackDraftModifiedBy() });
        if (self.FilterCallbackDraftModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterCallbackDraftModifiedDate() });

        return filters;
    };

    // Get filtered columns value
    function GetUnderlyingFilteredColumns() {
        // define filter

        var filters = [];
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: self.CIF_u() });
        if (self.UnderlyingFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingFilterIsSelected() });
        if (self.UnderlyingFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingFilterUnderlyingDocument() });
        if (self.UnderlyingFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingFilterDocumentType() });
        if (self.UnderlyingFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingFilterCurrency() });
        if (self.UnderlyingFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingFilterStatementLetter() });
        if (self.UnderlyingFilterAvailableAmount() != "") filters.push({ Field: 'AvailableAmount', Value: self.UnderlyingFilterAvailableAmount() });
        if (self.UnderlyingFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingFilterAmount() });
        if (self.UnderlyingFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingFilterDateOfUnderlying() });
        if (self.UnderlyingFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingFilterExpiredDate() });
        if (self.UnderlyingFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingFilterReferenceNumber() });
        if (self.UnderlyingFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingFilterSupplierName() });
        if (self.UnderlyingFilterInvoiceNumber() != "") filters.push({ Field: 'InvoiceNumber', Value: self.UnderlyingFilterInvoiceNumber() });
        if (self.UnderlyingFilterAccountNumber() != "") filters.push({ Field: 'AccountNumber', Value: self.UnderlyingFilterAccountNumber() });
        if (self.UnderlyingFilterIsJointAccount() != "") filters.push({ Field: 'IsJointAccount', Value: self.UnderlyingFilterIsJointAccount() });
        if (self.UnderlyingFilterIsExpired() != "") filters.push({ Field: 'IsExpired', Value: self.UnderlyingFilterIsExpired() });
        if (self.UnderlyingFilterAttachmentNo() != "") filters.push({ Field: 'AttachmentNo', Value: self.UnderlyingFilterAttachmentNo() });
        filters.push({ Field: 'StatusShowData', Value: 0 });
        return filters;
    };

    function GetUnderlyingDraftFilteredColumns() {
        // define filter

        var filters = [];
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: self.CIF_u() });
        if (self.UnderlyingDraftFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingDraftFilterIsSelected() });
        if (self.UnderlyingDraftFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingDraftFilterUnderlyingDocument() });
        if (self.UnderlyingDraftFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingDraftFilterDocumentType() });
        if (self.UnderlyingDraftFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingDraftFilterCurrency() });
        if (self.UnderlyingDraftFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingDraftFilterStatementLetter() });
        if (self.UnderlyingDraftFilterAvailableAmount() != "") filters.push({ Field: 'AvailableAmount', Value: self.UnderlyingDraftFilterAvailableAmount() });
        if (self.UnderlyingDraftFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingDraftFilterAmount() });
        if (self.UnderlyingDraftFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingDraftFilterDateOfUnderlying() });
        if (self.UnderlyingDraftFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingDraftFilterExpiredDate() });
        if (self.UnderlyingDraftFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingDraftFilterReferenceNumber() });
        if (self.UnderlyingDraftFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingDraftFilterSupplierName() });
        if (self.UnderlyingDraftFilterInvoiceNumber() != "") filters.push({ Field: 'InvoiceNumber', Value: self.UnderlyingDraftFilterInvoiceNumber() });
        if (self.UnderlyingDraftFilterAccountNumber() != "") filters.push({ Field: 'AccountNumber', Value: self.UnderlyingDraftFilterAccountNumber() });
        if (self.UnderlyingDraftFilterIsJointAccount() != "") filters.push({ Field: 'IsJointAccount', Value: self.UnderlyingDraftFilterIsJointAccount() });
        if (self.UnderlyingDraftFilterAttachmentNo() != "") filters.push({ Field: 'AttachmentNo', Value: self.UnderlyingDraftFilterAttachmentNo() });
        filters.push({ Field: 'StatusShowData', Value: 0 });
        return filters;
    };

    // Get filtered columns value
    function GetUnderlyingAttachFilteredColumns() {
        var filters = [];
        self.UnderlyingFilterIsSelected(true); // flag for get all datas
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: self.CIF_u() });
        if (self.UnderlyingAttachFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingAttachFilterIsSelected() });
        if (self.UnderlyingAttachFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingAttachFilterUnderlyingDocument() });
        if (self.UnderlyingAttachFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingAttachFilterDocumentType() });
        if (self.UnderlyingAttachFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingAttachFilterCurrency() });
        if (self.UnderlyingAttachFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingAttachFilterStatementLetter() });
        if (self.UnderlyingAttachFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingAttachFilterAmount() });
        if (self.UnderlyingAttachFilterAvailableAmount() != "") filters.push({ Field: 'AvailableAmount', Value: self.UnderlyingAttachFilterAvailableAmount() });
        if (self.UnderlyingAttachFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingAttachFilterDateOfUnderlying() });
        if (self.UnderlyingAttachFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingAttachFilterExpiredDate() });
        if (self.UnderlyingAttachFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingAttachFilterReferenceNumber() });
        if (self.UnderlyingAttachFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingAttachFilterSupplierName() });
        if (self.UnderlyingAttachFilterAccountNumber() != "") filters.push({ Field: 'AccountNumber', Value: self.UnderlyingAttachFilterAccountNumber() });

        filters.push({ Field: 'StatusShowData', Value: self.UnderlyingFilterShow() });
        return filters;
    }

    // Get filtered columns value
    function GetAttachFilteredColumns() {
        // define filter

        var filters = [];
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: self.CIF_u() });
        if (self.AttachFilterFileName() != "") filters.push({ Field: 'FileName', Value: self.AttachFilterFileName() });
        if (self.AttachFilterDocumentPurpose() != "") filters.push({ Field: 'DocumentPurpose', Value: self.AttachFilterDocumentPurpose() });
        if (self.AttachFilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.AttachFilterModifiedDate() });
        if (self.AttachFilterDocumentRefNumber() != "") filters.push({ Field: 'DocumentRefNumber', Value: self.AttachFilterDocumentRefNumber() });
        if (self.AttachFilterAttachmentReference() != "") filters.push({ Field: 'AttachmentReference', Value: self.AttachFilterAttachmentReference() });
        if (self.AttachFilterAccountNumber() != "") filters.push({ Field: 'AccountNumber', Value: self.AttachFilterAccountNumber() });

        filters.push({ Field: 'StatusShowData', Value: self.UnderlyingFilterShow() });
        return filters;
    };
    //start add azam
    // Get filtered columns value
    function GetCutomerCsoDraftFilteredColumns() {
        // define filter

        /* self.FilterValue("");
         if (self.Config.view != undefined) {
             if (self.Config.view.GetFilterValue != undefined) {
                 var valFileter = self.Config.view.GetFilterValue();
                 self.FilterValue(valFileter);
             }
         }
         */



        var filters = [];

        // FORCE FILTER BY ENTITY NAME BY URL
        // if (self.Config.url != '') filters.push({ Field: 'EntityName', Value: self.Config.url.substr(self.Config.url.lastIndexOf('/') + 1) });

        //change by fandi
        if (self.FilterTaskName() != "") filters.push({ Field: 'TaskName', Value: self.FilterTaskName() });
        if (self.FilterActionType() != "") filters.push({ Field: 'ActionType', Value: self.FilterActionType() });
        // if (self.TaskName() != "") filters.push({ Field: 'TaskName', Value: self.TaskName() });
        //  if (self.ActionType() != "") filters.push({ Field: 'ActionType', Value: self.ActionType() });
        //end
        if (self.FilterValue() != "") filters.push({ Field: 'FilterValue', Value: self.FilterValue() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

        return filters;
    };
    //end azam
    //Get filtered columns value
    function GetCSOFilteredColumns() {
        // define filter
        var filters = [];
        self.FilterCIF_c(cifData);
        if (self.FilterCIF_c() != "") filters.push({ Field: 'CIF', Value: self.FilterCIF_c() });
        if (self.FilterEmployeeName_c() != "") filters.push({ Field: 'EmployeeName', Value: self.FilterEmployeeName_c() });
        if (self.FilterEmployeeEmail_c() != "") filters.push({ Field: 'EmployeeEmail', Value: self.FilterEmployeeEmail_c() });
        if (self.FilterLocationName_c() != "") filters.push({ Field: 'LocationName', Value: self.FilterLocationName_c() });
        if (self.FilterModifiedBy_c() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy_c() });
        if (self.FilterModifiedDate_c() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate_c() });

        return filters;
    };

    function GetPermissionContact() {
        var _roles = spUser.Roles;
        if (_roles != null) {
            for (var i = 0 ; _roles.length > i; i++) {
                if (_roles[i] != null) {
                    var _roleMaker = config.permission.customercontact.rolemaker;
                    if (_roleMaker != null) {
                        for (var j = 0; _roleMaker.length > j; j++)
                            if (Const_RoleName[_roles[i].ID].toLowerCase() == _roleMaker[j].toLowerCase()) { //if (_roles[i].Name.toLowerCase() == _roleMaker[j].toLowerCase()) {
                                return true;
                            }
                    }
                }
            }
        }
        return false;
    }

    function GetPermissionUnderlying() {
        var _roles = spUser.Roles;
        if (_roles != null) {
            for (var i = 0 ; _roles.length > i; i++) {
                if (_roles[i] != null) {
                    var _roleMaker = config.permission.customerunderlying.rolemaker;
                    if (_roleMaker != null) {
                        for (var j = 0; _roleMaker.length > j; j++)
                            if (Const_RoleName[_roles[i].ID].toLowerCase() == _roleMaker[j].toLowerCase()) { //if (_roles[i].Name.toLowerCase() == _roleMaker[j].toLowerCase()) {
                                return true;
                            }
                    }
                }
            }
        }
        return false;
    }

    function IsCustomerCSOViewer() {
        return Helper.IsUserRoleExistInConfigRoles(Const_RoleName, config.permission.customercso.viewer);
    }

    function IsCustomerCallbackViewer() {
        return Helper.IsUserRoleExistInConfigRoles(Const_RoleName, config.permission.customercallback.viewer);
    }

    function IsCustomerPhoneNumberViewer() {
        return Helper.IsUserRoleExistInConfigRoles(Const_RoleName, config.permission.customerphonenumber.viewer);
    }

    function IsCustomerPOAEmailViewer() {
        return Helper.IsUserRoleExistInConfigRoles(Const_RoleName, config.permission.customerpoaemail.viewer);
    }

    function IsCustomerCSOMaker() {
        return Helper.IsUserRoleExistInConfigRoles(Const_RoleName, config.permission.customercso.rolemaker);
    }

    function IsCustomerCallbackMaker() {
        return Helper.IsUserRoleExistInConfigRoles(Const_RoleName, config.permission.customercallback.rolemaker);
    }

    function IsCustomerPOAEmailMaker() {
        return Helper.IsUserRoleExistInConfigRoles(Const_RoleName, config.permission.customerpoaemail.rolemaker);
    }

    // Get filtered columns value
    function GetUnderlyingProformaFilteredColumns() {
        // define filter
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: self.CIF_u() });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.ProformaFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.ProformaFilterIsSelected() });
        if (self.ProformaFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.ProformaFilterUnderlyingDocument() });
        if (self.ProformaFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.ProformaFilterDocumentType() });
        if (self.ProformaFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.ProformaFilterCurrency() });
        if (self.ProformaFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.ProformaFilterStatementLetter() });
        if (self.ProformaFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.ProformaFilterAmount() });
        if (self.ProformaFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.ProformaFilterDateOfUnderlying() });
        if (self.ProformaFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.ProformaFilterExpiredDate() });
        if (self.ProformaFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.ProformaFilterReferenceNumber() });
        if (self.ProformaFilterInvoiceNumber() != "") filters.push({ Field: 'InvoiceNumber', Value: self.ProformaFilterInvoiceNumber() });
        if (self.ProformaFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.ProformaFilterSupplierName() });

        return filters;
    };

    // Get filtered columns value // add 2015.03.09
    function GetBulkUnderlyingFilteredColumns() {
        // define filter
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.BulkFilterCurrency(self.Currency_u().Code() != "" && self.Currency_u().Code() != null ? self.Currency_u().Code() : "xyz");
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: self.CIF_u() });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.BulkFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.BulkFilterIsSelected() });
        if (self.BulkFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.BulkFilterUnderlyingDocument() });
        if (self.BulkFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.BulkFilterDocumentType() });
        if (self.BulkFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.BulkFilterCurrency() });
        if (self.BulkFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.BulkFilterStatementLetter() });
        if (self.BulkFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.BulkFilterAmount() });
        if (self.BulkFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.BulkFilterDateOfUnderlying() });
        if (self.BulkFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.BulkFilterExpiredDate() });
        if (self.BulkFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.BulkFilterReferenceNumber() });
        if (self.BulkFilterInvoiceNumber() != "") filters.push({ Field: 'InvoiceNumber', Value: self.BulkFilterInvoiceNumber() });
        if (self.BulkFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.BulkFilterSupplierName() });

        return filters;
    };

    // SetPOAFunction Other Contact
    function SetPOAFunctionOther() {
        if (self.POAFunctionID_c() != 9) {
            self.POAFunctionOther_c('');
        }
    }

    // Get filtered columns value Customer Account
    function GetAccountFilteredColumns() {
        // define filter
        var filters = [];
        self.FilterCIF_a(cifData);
        if (self.FilterCIF_a() != "") filters.push({ Field: 'CIF', Value: self.FilterCIF_a() });
        if (self.FilterAccountNumber_a() != "") filters.push({ Field: 'AccountNumber', Value: self.FilterAccountNumber_a() });
        if (self.FilterCurrencyCode_a() != "") filters.push({ Field: 'CurrencyCode', Value: self.FilterCurrencyCode_a() });
        if (self.FilterCurrencyDesc_a() != "") filters.push({ Field: 'CurrencyDesc', Value: self.FilterCurrencyDesc_a() });
        if (self.FilterIsJointAccount_a() != "") filters.push({ Field: 'IsJointAccount', Value: self.FilterIsJointAccount_a() });
        if (self.FilterModifiedBy_a() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy_a() });
        if (self.FilterModifiedDate_a() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate_a() });

        return filters;
    };

    // All Function for Underlying & attach file Start
    function ResetDataAttachment(index, isSelect) {
        self.CustomerAttachUnderlyings()[index].IsSelectedAttach = isSelect;
        var dataRows = self.CustomerAttachUnderlyings().slice(0);
        self.CustomerAttachUnderlyings([]);
        self.CustomerAttachUnderlyings(dataRows);
    }

    function ResetDataProforma(index, isSelect) {
        self.CustomerUnderlyingProformas()[index].IsSelectedProforma = isSelect;
        var dataRows = self.CustomerUnderlyingProformas().slice(0);
        self.CustomerUnderlyingProformas([]);
        self.CustomerUnderlyingProformas(dataRows);
    }

    function ResetBulkData(index, isSelect) { //add 2015.03.09
        self.CustomerBulkUnderlyings()[index].IsSelectedBulk = isSelect;
        var dataRows = self.CustomerBulkUnderlyings().slice(0);
        self.CustomerBulkUnderlyings([]);
        self.CustomerBulkUnderlyings(dataRows);
    }

    function SetSelectedProforma(data) {
        // if(self.TempSelectedUnderlyingProforma().length>0) {
        for (var i = 0 ; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlyingProforma(), function (item) {
                return item.ID == data[i].ID;
            });
            if (selected != null) {
                data[i].IsSelectedProforma = true;
            }
            else {
                data[i].IsSelectedProforma = false;
            }
        }
    }
    //#region Joint Account Function
    function OnChangeJointAcc() {
        if (!CustomerUnderlying.IsJointAccount()) {
            CustomerUnderlying.AccountNumber(null);
            //SetStatementA(false,null);
        }

    }
    //#endregion

    function SetSelectedBulk(data) { //add 2015.03.09
        // if(self.TempSelectedUnderlyingProforma().length>0) {
        for (var i = 0 ; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlyingBulk(), function (item) {
                return item.ID == data[i].ID;
            });
            if (selected != null) {
                data[i].IsSelectedBulk = true;
            }
            else {
                data[i].IsSelectedBulk = false;
            }
        }
    }
    self.onSelectionAttach = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            //self.IsSelectedAttach(false);
            self.TempSelectedAttachUnderlying.remove(data);
            ResetDataAttachment(index, false);
        } else {
            //self.IsSelectedAttach(true);
            self.TempSelectedAttachUnderlying.push({
                ID: item.ID
            });
            ResetDataAttachment(index, true);
        }
    }

    self.onSelectionProforma = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlyingProforma(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            //self.IsSelectedAttach(false);
            self.TempSelectedUnderlyingProforma.remove(data);
            ResetDataProforma(index, false);
        } else {
            //self.IsSelectedAttach(true);
            self.TempSelectedUnderlyingProforma.push({
                ID: item.ID
            });
            ResetDataProforma(index, true);
        }
    }

    self.onSelectionBulk = function (index, item) {
        var total = 0;
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlyingBulk(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            //self.IsSelectedAttach(false);
            self.TempSelectedUnderlyingBulk.remove(data);
            ResetBulkData(index, false);
        } else {
            //self.IsSelectedAttach(true);
            self.TempSelectedUnderlyingBulk.push({
                ID: item.ID,
                Amount: item.Amount
            });
            ResetBulkData(index, true);
        }
        for (var i = 0; self.TempSelectedUnderlyingBulk.push() > i; i++) {
            total += self.TempSelectedUnderlyingBulk()[i].Amount;
        }
        if (total > 0) {
            var e = total;
            var res = parseInt(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res).toFixed(2));
            self.Amount_u(formatNumber(e));
        } else {
            var e = self.tempAmountBulk();
            var res = parseInt(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res).toFixed(2));
            self.Amount_u(formatNumber(e));
        }
    }

    function SaveDataFile() {

        SetMappingAttachment();
        //Ajax call to insert the CustomerUnderlyings
        CustomerUnderlyingFile.FileName = DocumentModels().FileName;
        CustomerUnderlyingFile.DocumentPath = DocumentModels().DocumentPath;
        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlyingfile,
            data: ko.toJSON(CustomerUnderlyingFile), //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    // send notification
                    ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                    // hide current popup window
                    $("#modal-form-Attach").modal('hide');

                    // refresh data
                    GetDataAttachFile();
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });


    }

    function SetMappingAttachment() {
        CustomerUnderlyingFile.CustomerUnderlyingMappings([]);
        for (var i = 0 ; i < self.TempSelectedAttachUnderlying().length; i++) {
            CustomerUnderlyingFile.CustomerUnderlyingMappings.push(new CustomerUnderlyingMappingModel(0, self.TempSelectedAttachUnderlying()[i].ID, customerNameData + '-(A)'));
        }
    }

    function SetMappingProformas() {
        CustomerUnderlying.Proformas([]);
        for (var i = 0 ; i < self.TempSelectedUnderlyingProforma().length; i++) {
            CustomerUnderlying.Proformas.push(new SelectModel(cifData, self.TempSelectedUnderlyingProforma()[i].ID));
        }
    }

    function SetMappingBulks() { //add 2015.03.09
        CustomerUnderlying.BulkUnderlyings([]);
        for (var i = 0 ; i < self.TempSelectedUnderlyingBulk().length; i++) {
            CustomerUnderlying.BulkUnderlyings.push(new SelectBulkModel(cifData, self.TempSelectedUnderlyingBulk()[i].ID));
        }
    }

    function SetDefaultValueStatementA() {
        var today = Date.now();
        var documentType = ko.utils.arrayFirst(self.ddlDocumentType_u(), function (item) { return item.Name == '-' });
        var underlyingDocument = ko.utils.arrayFirst(self.ddlUnderlyingDocument_u(), function (item) { return item.Code() == 998 });

        if (documentType != null) {
            self.DocumentType_u(new DocumentTypeModel(documentType.ID, documentType.Name));
        }

        if (underlyingDocument != null) {
            self.UnderlyingDocument_u(new UnderlyingDocumentModel(underlyingDocument.ID(), underlyingDocument.Name(), underlyingDocument.Code()));
        }
        self.DateOfUnderlying_u(self.LocalDate(today, true, false));
        self.SupplierName_u('-');
        self.InvoiceNumber_u('-');

    }

    function SetStatementA(isJointAccount, accountNumber) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetStatementA/" + cifData + "/" + isJointAccount + "/" + accountNumber,
            data: null, //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            }, success: function (data) {
                self.DynamicStatementLetter_u([]);
                if (data.IsStatementA) {
                    self.StatementLetter_u(new StatementLetterModel(CONST_STATEMENT.StatementB_ID, 'Statement B'));
                    ko.utils.arrayForEach(self.ddlStatementLetter_u(), function (item) {
                        if (item.ID != CONST_STATEMENT.StatementA_ID) {
                            self.DynamicStatementLetter_u.push(item);
                        }
                    });
                }
                else {
                    self.DynamicStatementLetter_u(self.ddlStatementLetter_u());
                    //self.IsStatementA(true);
                }
            }
        });
    }

    function setRefID() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetRefID/" + cifData,
            data: null, //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            }, success: function (data) {
                self.ReferenceNumber_u(customerNameData + '-' + data.ID);
                ReferenceNumber = customerNameData + '-' + data.ID;
            }
        });

    }

    function GetTreshold() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.singlevalueparameter + "/FX_TRANSACTION_TRESHOLD",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }, success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    treshHold = data;
                }
            }
        });
    }

    //add by fandi for worksheet excel
    function GetMaintenanceTypes() {
        var options = {
            url: api.server + api.url.parameter,
            params: {
                select: "StatementLetter,UnderlyingDoc,DocType,currency"
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetMaintenanceTypes, OnError, OnAlways);
    }

    function OnSuccessGetMaintenanceTypes(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            StatementLetter = data.StatementLetter;
            UnderlyingDocument = data.UnderltyingDoc;
            DocumentType = data.DocType;
            Currency = data.Currency;


        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }
    //end


    function GetRateIDRAmount(CurrencyID) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
            params: {
            },
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    self.Rate_u(data.RupiahRate);
                    //if(CurrencyID!=1){ // if not USD
                    var e = document.getElementById("Amount_u");
                    if (e.value != null && typeof e.value == "string") {
                        res = parseFloat(e.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '')).toFixed(2) * data.RupiahRate / idrrate;
                    } else {
                        res = e.value * data.RupiahRate / idrrate;
                    }

                    res = Math.round(res * 100) / 100;
                    self.AmountUSD_u(parseFloat(res).toFixed(2));
                    //}
                }
            }
        });
    }

    function GetRateIDR() {
        var options = {
            url: api.server + api.url.currency + "/CurrencyRate/" + "1",
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetRateIDR, OnError, OnAlways);
    }

    function GetFilterValue(cifData) {
        if (cifData != 0 && cifData != undefined) {
            return cifData;
        } else {
            return "";
        }
    }

    function AddListItem(ActionType, UnderlyingID) {
        var body = {
            Title: UnderlyingID.toString(),
            ActionType: ActionType,
            __metadata: {
                type: config.sharepoint.metadata.listMasterUnderlying
            }
        };

        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token

        var options = {
            url: config.sharepoint.url.api + "/Lists(guid'" + config.sharepoint.listIdMasterUnderlying + "')/Items",
            data: JSON.stringify(body),
            digest: jQuery("#__REQUESTDIGEST").val()
        };
        Helper.Sharepoint.List.Add(options, OnSuccessAddListItem, OnError, OnAlways);
    }

    function OnSuccessAddListItem(data, textStatus, jqXHR) {
        //window.location = "/customer-list";
    }

    function OnSuccessGetRateIDR(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            idrrate = data.RupiahRate;
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }
    function OnSuccessGetPOAEmailProduct(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.cblPOAEmailProduct(data);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }
    // On Success GetData Parameter for dwopdownlist
    function OnSuccessGetParameters(data, textStatus, jqXHR) {
        console.log('data', data)

        if (jqXHR.status = 200) {
            // bind result to observable array
            self.ddlPOAFunction_c(data["POAFunction"]); // customer contact
            self.Branchs(data.Branch);
            self.BizSegments(data.BizSegment);
            self.Types(data.CustomerType);
            self.POAFunctions(data.POAFunction);
            self.Products(data.Product);
            //add by fandi for dropdown worksheet
            self.TransCurrency(data.Currency);
            //end


            self.ddlPOAFunction_f([]);
            for (var i = 0; i < data["POAFunction"].length; i++) {
                self.ddlPOAFunction_f.push(new POAFunctionModel(data["POAFunction"][i].ID, data["POAFunction"][i].Name, data["POAFunction"][i].Description));
            }
            self.UserApprovals([]);
            if (data.UserApproval != undefined) {
                for (var i = 0; i < data.UserApproval.length; i++) {
                    if (data.UserApproval[i] != null) {
                        self.UserApprovals.push(new UserApprovalModel(data.UserApproval[i].EmployeeID, data.UserApproval[i].EmployeeName, data.UserApproval[i].SegmentID, data.UserApproval[i].RankID, data.UserApproval[i].Branch.ID));
                    }
                }
            } else {
                self.UserApprovals.push(new UserApprovalModel(0, '', 0, 0, 0));
            }
            self.ddlCurrency_a([]);
            self.ddlCurrency_u([]);
            ko.utils.arrayForEach(data['Currency'], function (item) {
                self.ddlCurrency_u.push(new CurrencyModel(item.ID, item.Code, item.Description));
                self.ddlCurrency_a.push(new CurrencyModel(item.ID, item.Code, item.Description));
            });

            // tab customer underlying start
            var underlying = data['UnderltyingDoc'];
            for (var i = 0 ; i < underlying.length; i++) {
                self.ddlUnderlyingDocument_u.push(new UnderlyingDocumentModel(underlying[i].ID, underlying[i].Name, underlying[i].Code));
            }
            self.ddlDocumentType_u(data['DocType']);
            self.ddlDocumentType_a(data['DocType']);


            //self.ddlDocumentPurpose_a(data['PurposeDoc']);
            var purposeDoc = ko.utils.arrayFilter(data['PurposeDoc'], function (item) {
                return item.Description.trim() == "Underlying Document";
            });
            if (purposeDoc != null && purposeDoc.length > 0) {
                self.ddlDocumentPurpose_a(purposeDoc);
            }
            for (var i = 0 ; i < underlying.length; i++) {
                self.ddlUnderlyingDocument_u.push(new UnderlyingDocumentModel(underlying[i].ID, underlying[i].Name, underlying[i].Code));
            }

            self.ddlStatementLetter_u([]);
            self.DynamicStatementLetter_u([]);
            ko.utils.arrayForEach(data['StatementLetter'], function (item) {
                if (item.ID == CONST_STATEMENT.StatementA_ID || item.ID == CONST_STATEMENT.StatementB_ID || item.ID == CONST_STATEMENT.AnnualStatement_ID) {
                    self.ddlStatementLetter_u.push(item);
                    self.DynamicStatementLetter_u.push(item);
                }
            });
            //self.ddlStatementLetter_u(data['StatementLetter']);
            // tab customer underlying end
            if (self.OnAfterGetParameters) self.OnAfterGetParameters();
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.Customers(data.Rows);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    function OnSuccessGetDataCustomer(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.BizSegment(new BizSegmentModel(data.BizSegment.ID, data.BizSegment.Name, data.BizSegment.Description));
            self.Branch(new BranchModel(data.Branch.ID, data.Branch.Name));
            self.Type(new CustomerTypeModel(data.Type.ID, data.Type.Name));
            if (data.RM != null) {
                self.UserApproval(new UserApprovalModel(data.RM.ID, data.RM.Name, data.RM.SegmentID, data.RM.RankID, data.RM.BranchID));
            } else {
                self.UserApproval(new UserApprovalModel(0, '', 0, 0, 0));
            }
            self.IsJointAccount(true);
            self.JointAccountNumbers(ko.utils.arrayFilter(data.Accounts, function (item) { return item.IsJointAccount == true }));
            JoinAccountNumber = ko.utils.arrayFilter(data.Accounts, function (item) { return item.IsJointAccount == true });
            // checking havd joint account
            var isJointAcc = ko.utils.arrayFilter(data.Accounts, function (item) {
                return true == item.IsJointAccount;
            });
            if (isJointAcc != null & isJointAcc.length == 0) {
                self.IsJointAccount(false);
            }
            self.IsCitizen(data.IsCitizen);
            self.IsResident(data.IsResident);
            self.IsDormant(data.IsDormant);
            self.IsFreeze(data.IsFreeze);
        }
        else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetFunctionData callback
    function OnSuccessGetFunctionData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerFunctions(data.Rows);

            self.FunctionsGridProperties().Page(data['Page']);
            self.FunctionsGridProperties().Size(data['Size']);
            self.FunctionsGridProperties().Total(data['Total']);
            self.FunctionsGridProperties().TotalPages(Math.ceil(self.FunctionsGridProperties().Total() / self.FunctionsGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback

    function OnSuccessGetDataContact(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            var dataR = data.Rows;
            var POAFilter;
            for (var xx = 0; xx < dataR.length; xx++) {
                POAFilter = '';
                for (var i = 0 ; i < dataR[xx].POAFunction.length; i++) {
                    POAFilter += dataR[xx].POAFunction[i].Description + ";";
                }
                if (dataR[xx].POAFunction.length == 0) {
                    dataR[xx]['POAFunctionView'] = "";
                }
                else {
                    dataR[xx]['POAFunctionView'] = POAFilter;
                }
            }
            console.log(dataR)
            self.CustomerContacts(dataR);
            self.ConctactGridProperties().Page(data['Page']);
            self.ConctactGridProperties().Size(data['Size']);
            self.ConctactGridProperties().Total(data['Total']);
            self.ConctactGridProperties().TotalPages(Math.ceil(self.ConctactGridProperties().Total() / self.ConctactGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    function OnSuccessGetDataCallbackContact(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerCallbackContacts(data.Rows);
            self.CallbackContactGridProperties().Page(data['Page']);
            self.CallbackContactGridProperties().Size(data['Size']);
            self.CallbackContactGridProperties().Total(data['Total']);
            self.CallbackContactGridProperties().TotalPages(Math.ceil(self.CallbackContactGridProperties().Total() / self.CallbackContactGridProperties().Size()));
        }
        else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    function OnSuccessGetDataCallback(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerCallbacks(data.Rows);
            self.CallbackGridProperties().Page(data['Page']);
            self.CallbackGridProperties().Size(data['Size']);
            self.CallbackGridProperties().Total(data['Total']);
            self.CallbackGridProperties().TotalPages(Math.ceil(self.CallbackGridProperties().Total() / self.CallbackGridProperties().Size()));
        }
        else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    function OnSuccessGetDataCallbackDraft(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerCallbackDrafts(data.Rows);
            self.CallbackDraftGridProperties().Page(data['Page']);
            self.CallbackDraftGridProperties().Size(data['Size']);
            self.CallbackDraftGridProperties().Total(data['Total']);
            self.CallbackDraftGridProperties().TotalPages(Math.ceil(self.CallbackDraftGridProperties().Total() / self.CallbackDraftGridProperties().Size()));
        }
        else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    function OnSuccessGetDataPhoneNumber(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerPhoneNumbers(data.Rows);
            self.PhoneNumberGridProperties().Page(data['Page']);
            self.PhoneNumberGridProperties().Size(data['Size']);
            self.PhoneNumberGridProperties().Total(data['Total']);
            self.PhoneNumberGridProperties().TotalPages(Math.ceil(self.PhoneNumberGridProperties().Total() / self.PhoneNumberGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }
    function OnSuccessGetDataPOAEmail(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerPOAEmails(data);
            self.MergingProductID();
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }

    }
    function OnSuccessGetDataPOAEmailGrid(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            //self.CustomerPOAEmails(data.Rows);
            self.POAEmailGridProperties().Page(data['Page']);
            self.POAEmailGridProperties().Size(data['Size']);
            self.POAEmailGridProperties().Total(data['Total']);
            self.POAEmailGridProperties().TotalPages(Math.ceil(self.POAEmailGridProperties().Total() / self.POAEmailGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }

    }
    function OnSuccessGetDataCSO(data, textStatus, jqXHR) {

        if (jqXHR.status == 200) {
            self.CustomerCSO(data.Rows);
            self.CSOGridProperties().Page(data['Page']);
            self.CSOGridProperties().Size(data['Size']);
            self.CSOGridProperties().Total(data['Total']);
            self.CSOGridProperties().TotalPages(Math.ceil(self.CSOGridProperties().Total() / self.CSOGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetDataUnderlying(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerUnderlyings(data.Rows);

            self.UnderlyingGridProperties().Page(data['Page']);
            self.UnderlyingGridProperties().Size(data['Size']);
            self.UnderlyingGridProperties().Total(data['Total']);
            self.UnderlyingGridProperties().TotalPages(Math.ceil(self.UnderlyingGridProperties().Total() / self.UnderlyingGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }
    //start add azam
    function OnSuccessGetDataCsoDraft(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            $.each(data.Rows, function (index, item) {
                if (item.Data != "") {
                    var value = '';
                    ar = item.LastModifiedBy.split('|');
                    item.LastModifiedBy = ar[ar.length - 1];
                    rec = eval('(' + item.Data + ')');
                    if (rec.length) {
                        value = '';
                        $.each(rec, function (index, item) {

                            if (item != null && item.Value != undefined)
                                value += item.Value + ' ';
                        });
                        item.Value = value;
                    }
                    var except = {};
                    except['ID'] = true;
                    except['LastModifiedBy'] = true;
                    except['LastModifiedDate'] = true;
                    $.each(rec, function (name, value) {
                        if (!except[name])
                            item[name] = value;
                    });
                }
            });

            self.Rows(data.Rows);
            self.CustomerCsoDraftGridProperties().Page(data['Page']);
            self.CustomerCsoDraftGridProperties().Size(data['Size']);
            self.CustomerCsoDraftGridProperties().Total(data['Total']);
            self.CustomerCsoDraftGridProperties().TotalPages(Math.ceil(self.CustomerCsoDraftGridProperties().Total() / self.CustomerCsoDraftGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
        //if (jqXHR.status = 200) {
        //    self.CustomerCsoDrafts(data.Rows);

        //    self.CSOGridProperties().Page(data['Page']);
        //    self.CSOGridProperties().Size(data['Size']);
        //    self.CSOGridProperties().Total(data['Total']);
        //    self.CSOGridProperties().TotalPages(Math.ceil(self.CSOGridProperties().Total() / self.CSOGridProperties().Size()));
        //} else {
        //    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        //}
    }
    //end azam

    function OnSuccessGetDataUnderlyingDraft(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerUnderlyingDrafts(data.Rows);

            self.UnderlyingDraftGridProperties().Page(data['Page']);
            self.UnderlyingDraftGridProperties().Size(data['Size']);
            self.UnderlyingDraftGridProperties().Total(data['Total']);
            self.UnderlyingDraftGridProperties().TotalPages(Math.ceil(self.UnderlyingDraftGridProperties().Total() / self.UnderlyingDraftGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetUnderlyingDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {

            for (var i = 0 ; i < data.Rows.length; i++) {
                var selected = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (item) {
                    return item.ID == data.Rows[i].ID;
                });
                if (selected != null) {
                    data.Rows[i].IsSelectedAttach = true;
                }
                else {
                    data.Rows[i].IsSelectedAttach = false;
                }
            }
            self.CustomerAttachUnderlyings(data.Rows);

            self.UnderlyingAttachGridProperties().Page(data['Page']);
            self.UnderlyingAttachGridProperties().Size(data['Size']);
            self.UnderlyingAttachGridProperties().Total(data['Total']);
            self.UnderlyingAttachGridProperties().TotalPages(Math.ceil(self.UnderlyingAttachGridProperties().Total() / self.UnderlyingAttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerUnderlyingFiles(data.Rows);

            self.AttachGridProperties().Page(data['Page']);
            self.AttachGridProperties().Size(data['Size']);
            self.AttachGridProperties().Total(data['Total']);
            self.AttachGridProperties().TotalPages(Math.ceil(self.AttachGridProperties().Total() / self.AttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetUnderlyingProforma(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            SetSelectedProforma(data.Rows);

            self.CustomerUnderlyingProformas(data.Rows);

            self.UnderlyingProformaGridProperties().Page(data['Page']);
            self.UnderlyingProformaGridProperties().Size(data['Size']);
            self.UnderlyingProformaGridProperties().Total(data['Total']);
            self.UnderlyingProformaGridProperties().TotalPages(Math.ceil(self.UnderlyingProformaGridProperties().Total() / self.UnderlyingProformaGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback // Add 2015.03.09
    function OnSuccessGetUnderlyingBulk(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            SetSelectedBulk(data.Rows);

            self.CustomerBulkUnderlyings(data.Rows);

            self.UnderlyingBulkGridProperties().Page(data['Page']);
            self.UnderlyingBulkGridProperties().Size(data['Size']);
            self.UnderlyingBulkGridProperties().Total(data['Total']);
            self.UnderlyingBulkGridProperties().TotalPages(Math.ceil(self.UnderlyingBulkGridProperties().Total() / self.UnderlyingBulkGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetAccountData callback
    function OnSuccessGetAccountData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerAccounts(data.Rows);

            self.AccountGridProperties().Page(data['Page']);
            self.AccountGridProperties().Size(data['Size']);
            self.AccountGridProperties().Total(data['Total']);
            self.AccountGridProperties().TotalPages(Math.ceil(self.AccountGridProperties().Total() / self.AccountGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
    //$("#aspnetForm").validate({onsubmit: false});
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });

    $("#employeecso").autocomplete({
        source: function (request, response) {
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.userapproval + "/Search",
                data: {
                    query: request.term,
                    limit: 20
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, TokenOnError);
        },
        minLength: 2,
        select: function (event, ui) {
            // set customer data model
            if (ui.item.data != undefined || ui.item.data != null) {

                self.Employee(new EmployeeModel(ui.item.data.EmployeeID, ui.item.data.EmployeeName, ui.item.data.EmployeeUserName, ui.item.data.EmployeeEmail));

                $('#employeecso').val(ui.item.data.EmployeeName);
                $('#employeeemailcso').val(ui.item.data.EmployeeEmail);

            }
        },
        response: function (event, ui) {
            if (ui.content.length === 0) {
                countResult = 0;
            } else {
                countResult = 1;
            }
        }
    });
    function OnSuccessAutoComplete(response, data, textStatus, jqXHR) {
        response($.map(data, function (item) {
            return {
                // default autocomplete object
                label: item.EmployeeName + " - " + item.EmployeeEmail,
                value: item.EmployeeName,

                // custom object binding
                data: {
                    EmployeeID: item.EmployeeID,
                    EmployeeName: item.EmployeeName,
                    EmployeeUserName: item.EmployeeUserName,
                    EmployeeEmail: item.EmployeeEmail
                }
            }
        })
        )

    };

};

// Knockout custom file handler
ko.bindingHandlers.file = {
    init: function (element, valueAccessor) {
        $(element).change(function () {
            var file = this.files[0];

            if (ko.isObservable(valueAccessor()))
                valueAccessor()(file);
        });
    }
};

function DeleteFile(documentPath) {
    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;
    // output variable
    var output;

    // Delete the file to the SharePoint folder.
    var deleteFile = deleteFileToFolder();
    deleteFile.done(function (file, status, xhr) {
        output = file.d;
    });
    deleteFile.fail(OnError);
    // Call function delete File Shrepoint Folder
    function deleteFileToFolder() {
        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token

        return $.ajax({
            url: serverUrl + "/_api/web/GetFileByServerRelativeUrl('" + documentPath + "')",
            type: "POST",
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "X-HTTP-Method": "DELETE",
                "IF-MATCH": "*"
                //"content-length": arrayBuffer.byteLength
            }
        });
    }
}

function UploadFile(context, document, callBack) {

    // Define the folder path for this example.
    var serverRelativeUrlToFolder = '/Underlying Documents';


    // Get the file name from the file input control on the page.

    var parts = document.DocumentPath().name.split('.');

    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBuffer();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolder(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                // Change the display name and title of the list item.
                var changeItem = updateListItem(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    DocumentModels({ "FileName": document.DocumentPath().name, "DocumentPath": output.ServerRelativeUrl });
                    //viewModel.TransactionModel().Documents.push(kodok);

                    //self.Documents.push(kodok);
                    callBack();
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBuffer() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }
        //reader.readAsArrayBuffer(fileInput[0].files[0]);
        reader.readAsArrayBuffer(document.DocumentPath());
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(arrayBuffer) {

        // Get the file name from the file input control on the page.
        //var parts = fileInput[0].value.split('\\');
        //var fileName = parts[parts.length - 1];

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                //"content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItem(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItem(itemMetadata) {
        var body = {
            Title: document.DocumentPath().name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: context.Type.DocTypeName,
            Document_x0020_Purpose: context.Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };

        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token

        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}

var Stoped = false;
function GetDataCustomerUnderlying() {
    var $container;
    if ($container != null) {
        $container.handsontable('destroy');
    }
    console.log("Test");

    var disAnnual = [3, 4, 5, 6, 12, 13];
    var disStatementA = [5, 6];

    function getDIsabled() {
        return function (instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            if ($("#dataTable").handsontable('getDataAtCell', row, 2) == "Annual Statement Letter") {
                if (disAnnual.indexOf(col) > -1) {
                    cellProperties.readOnly = true;
                }
            }
            if ($("#dataTable").handsontable('getDataAtCell', row, 2) == "Statement A") {
                if (disStatementA.indexOf(col) > -1) {
                    cellProperties.readOnly = true;
                }
            }
        }
    }

    var $container = $("#dataTable");
    $container.handsontable({
        data: [
            {
                CIF: cifData, StatementLetter: "Please Choose...", UnderlyingDocument: "Please Choose...",
                DocumentType: "Choose...", Currency: "Choose...", InvoiceAmount: 0, Rate: 0,
                AmountUSD: 0, DateOfUnderlying2: null, ExpiredDate2: null, ReferenceNumber: null,
                SupplierName: null, InvoiceNumber: null
            }
        ],
        minRows: 2,
        maxRows: 15,
        allowInsertRow: true,
        colHeaders: ["Join Account", "Account", "Statement<br>Letter", "Underlying Document", "Type of Document", "<center>Transaction<br>Currency</center>", "Invoice Amount", "Rate",
                     "Eqv. USD", "<center>Date of<br>Underlying</center>", "Expiry Date", "<center>Reference<br>Number</center>", "Supplier Name", "<center>Invoice<br>Number</center>"],
        contextMenu: true,
        columns: [
                {
                    data: 'JointAccount',
                    type: 'dropdown',
                    source: dataDropDownJoin(),
                    width: 150
                },
                {
                    data: 'Account',
                    type: 'dropdown',
                    source: dataDropDownAccount(),
                    width: 150
                },
                {
                    data: 'StatementLetter',
                    type: 'dropdown',
                    source: dataDropDownStatement(),
                    renderer: getDIsabled(),
                    width: 150
                },
                {
                    data: 'UnderlyingDocument',
                    type: 'dropdown',
                    source: dataDropDownUnderDoc(),
                    renderer: getDIsabled(),
                    width: 200
                },
                {
                    data: 'DocumentType',
                    type: 'dropdown',
                    source: dataDropDownTypeOfDoc(),
                    renderer: getDIsabled(),
                    width: 150
                },
                {
                    data: 'Currency',
                    type: 'dropdown',
                    source: dataDropDownCurrency(),
                    renderer: getDIsabled(),
                    width: 150
                },
                {
                    data: 'InvoiceAmount',
                    type: 'numeric',
                    format: '0,0.00',
                    renderer: getDIsabled(),
                    width: 150
                },
                {
                    data: 'Rate',
                    type: 'numeric',
                    readOnly: true,
                    format: '0,0.00',
                    width: 150
                },
                {
                    data: 'AmountUSD',
                    type: 'numeric',
                    readOnly: true,
                    format: '0,0.00',
                    width: 150
                },
                {
                    data: 'DateOfUnderlying2',
                    type: 'date',
                    dateFormat: 'DD-MM-YYYY',
                    correctFormat: true,
                    width: 150
                },
                {
                    data: 'ExpiredDate2',
                    type: 'date',
                    dateFormat: 'DD-MM-YYYY',
                    correctFormat: true,
                    width: 150
                },
                {
                    data: 'ReferenceNumber',
                    width: 150
                },
                {
                    data: 'SupplierName',
                    renderer: getDIsabled(),
                    width: 150
                },
                {
                    data: 'InvoiceNumber',
                    renderer: getDIsabled(),
                    width: 150
                }
        ],
        afterChange: function (changes, source) {
            if (!changes) {
                return;
            }
            var row = changes[0][0];

            if (changes[0][1] == "StatementLetter") {
                var SL = this.getDataAtCell(row, 2);
                if (SL == "Statement A") {
                    $("#dataTable").handsontable('setDataAtCell', row, 3, "statement A");
                    $("#dataTable").handsontable('setDataAtCell', row, 4, "-");
                    $("#dataTable").handsontable('setDataAtCell', row, 5, "USD");
                    $("#dataTable").handsontable('setDataAtCell', row, 6, treshHold);
                    var date = new Date();
                    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                    var day = lastDay.getDate();
                    var month = lastDay.getMonth() + 1;
                    var year = lastDay.getFullYear();
                    if (month > 12) {
                        month -= 12;
                        year += 1;
                    }
                    var fixLastDay = year + "/" + month + "/" + day;

                    $("#dataTable").handsontable('setDataAtCell', row, 9, date);
                    $("#dataTable").handsontable('setDataAtCell', row, 10, fixLastDay);
                    var REF = ReferenceNumber.split("-");
                    var CustName = REF[0];
                    var Number = parseInt(REF[1]) + parseInt(row);
                    $("#dataTable").handsontable('setDataAtCell', row, 11, CustName + '-' + Number);
                    $("#dataTable").handsontable('setDataAtCell', row, 12, "-");
                    $("#dataTable").handsontable('setDataAtCell', row, 13, "-");

                } else if (SL == "Statement B") {
                    var DataUnder = this.getDataAtCell(row, 9);
                    if (DataUnder != null) {
                        var date = new Date();
                        var lastDay = new Date(date.getFullYear(), 13, 0);
                        var day = lastDay.getDate();
                        var month = lastDay.getMonth() + 1;
                        var year = lastDay.getFullYear();
                        if (month > 12) {
                            month -= 12;
                            year += 1;
                        }
                        var fixLastDay = year + "/" + month + "/" + day;
                        $("#dataTable").handsontable('setDataAtCell', row, 10, fixLastDay);
                    }
                    var REF = ReferenceNumber.split("-");
                    var CustName = REF[0];
                    var Number = parseInt(REF[1]) + parseInt(row);
                    $("#dataTable").handsontable('setDataAtCell', row, 11, CustName + '-' + Number);
                } else if (SL == "Annual Statement Letter") {
                    var date = new Date();
                    var lastDay = new Date(date.getFullYear(), 12, 0);
                    var day = lastDay.getDate();
                    var month = lastDay.getMonth() + 1;
                    var year = lastDay.getFullYear();
                    if (month > 12) {
                        month -= 12;
                        year += 1;
                    }
                    var fixLastDay = year + "/" + month + "/" + day;
                    $("#dataTable").handsontable('setDataAtCell', row, 3, "");
                    $("#dataTable").handsontable('setDataAtCell', row, 4, "");
                    $("#dataTable").handsontable('setDataAtCell', row, 5, "");
                    $("#dataTable").handsontable('setDataAtCell', row, 6, 0);
                    $("#dataTable").handsontable('setDataAtCell', row, 7, 0);
                    $("#dataTable").handsontable('setDataAtCell', row, 8, 0);
                    $("#dataTable").handsontable('setDataAtCell', row, 9, date);
                    $("#dataTable").handsontable('setDataAtCell', row, 10, fixLastDay);

                    var REF = ReferenceNumber.split("-");
                    var CustName = REF[0];
                    var Number = parseInt(REF[1]) + parseInt(row);
                    $("#dataTable").handsontable('setDataAtCell', row, 11, CustName + '-' + Number);
                }
            }
            if (changes[0][1] == "DateOfUnderlying2") {
                var SL = this.getDataAtCell(row, 2);
                if (SL == "Statement A") {
                    var date = new Date();
                    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                    var day = lastDay.getDate();
                    var month = lastDay.getMonth() + 1;
                    var year = lastDay.getFullYear();
                    if (month > 12) {
                        month -= 12;
                        year += 1;
                    }
                    var fixLastDay = year + "/" + month + "/" + day;
                    $("#dataTable").handsontable('setDataAtCell', row, 10, fixLastDay);

                } else if (SL == "Statement B") {
                    var DataUnder = this.getDataAtCell(row, 9);
                    if (DataUnder != null) {
                        var date = new Date();
                        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                        var day = date.getDate();
                        var month = lastDay.getMonth() + 13;
                        var year = lastDay.getFullYear();
                        if (month > 12) {
                            month -= 12;
                            year += 1;
                        }
                        var fixLastDay = year + "/" + month + "/" + day;
                        $("#dataTable").handsontable('setDataAtCell', row, 10, fixLastDay);
                    }
                    var REF = ReferenceNumber.split("-");
                    var CustName = REF[0];
                    var Number = parseInt(REF[1]) + parseInt(row);
                    $("#dataTable").handsontable('setDataAtCell', row, 11, CustName + '-' + Number);
                } else if (SL == "Annual Statement Letter") {
                    var date = new Date();
                    var lastDay = new Date(date.getFullYear(), 12, 0);
                    var day = lastDay.getDate();
                    var month = lastDay.getMonth() + 1;
                    var year = lastDay.getFullYear();
                    if (month > 12) {
                        month -= 12;
                        year += 1;
                    }
                    var fixLastDay = year + "/" + month + "/" + day;
                    $("#dataTable").handsontable('setDataAtCell', row, 10, fixLastDay);
                }
            }
            if (changes[0][1] == "Currency") {
                var isi = this.getDataAtCell(row, 5);

                var selectCurID = onChangeCurrency(isi);
                if (selectCurID != null) {
                    GetRateIDRAmountWorksheet(row, 7, selectCurID);
                }
            }
            if (changes[0][1] == "InvoiceAmount") {
                var invamount = this.getDataAtCell(row, 6);
                var rate = this.getDataAtCell(row, 7);
                var amountusd = invamount * rate / idrrate;
                $("#dataTable").handsontable('setDataAtCell', row, 8, amountusd);
            }
        },
    });
    $('div.ht_clone_top.handsontable').css('display', 'none');
    $('div.ht_master.handsontable>div.wtHolder').css('min-width', 1220 + 'px');
    $('div.ht_master.handsontable>div.wtHolder').css('max-width', 1290 + 'px');
    $('div.ht_master.handsontable>div.wtHolder').css('min-height', 200 + 'px');
    $('div.ht_master.handsontable>div.wtHolder').css('max-height', 400 + 'px');
}

//Edit Agung
function dataDropDownJoin() {
    var selectedId;

    if (JoinAccountNumber.length > 0) {
        JoinAccount = ko.observableArray([{ ID: false, Name: "Single" }, { ID: true, Name: "Join" }]);
    } else {
        JoinAccount = ko.observableArray([{ ID: false, Name: "Single" }]);
    }
    var optionsList = JoinAccount();

    var values = ("ID" + "").split(",");
    var value = [];
    for (var index = 0; index < optionsList.length; index++) {
        if ((optionsList[index].ID) > -1) {
            selectedId = optionsList[index].ID;
            value.push(optionsList[index].Name);
        }
    }
    return value;
}

function dataDropDownAccount() {
    var optionsList = JoinAccountNumber;

    var value = [];
    for (var index = 0; index < optionsList.length; index++) {
        value.push(optionsList[index].AccountNumber);
    }
    return value;
}

function dataDropDownCurrency() {
    var selectedId;
    var optionsList = Currency;


    var values = ("ID" + "").split(",");
    var value = [];
    for (var index = 0; index < optionsList.length; index++) {
        if ((optionsList[index].ID) > -1) {
            selectedId = optionsList[index].ID;
            value.push(optionsList[index].Code);

        }
    }
    return value;
}

function dataDropDownStatement() {
    var selectedId;
    var optionsList = StatementLetter;


    var values = ("ID" + "").split(",");
    var value = [];
    for (var index = 0; index < optionsList.length; index++) {
        if ((optionsList[index].ID) > -1) {
            selectedId = optionsList[index].ID;
            value.push(optionsList[index].Name);
        }
    }
    return value;
}

function dataDropDownUnderDoc() {
    var selectedId;
    var optionsList = UnderlyingDocument;


    var values = ("ID" + "").split(",");
    var value = [];
    for (var index = 0; index < optionsList.length; index++) {
        if ((optionsList[index].ID) > -1) {
            selectedId = optionsList[index].ID;
            value.push(optionsList[index].Name);
        }
    }
    return value;
}

function dataDropDownTypeOfDoc() {
    var selectedId;
    var optionsList = DocumentType;


    var values = ("ID" + "").split(",");
    var value = [];
    for (var index = 0; index < optionsList.length; index++) {
        if ((optionsList[index].ID) > -1) {
            selectedId = optionsList[index].ID;
            value.push(optionsList[index].Name);
        }
    }
    return value;
}

function onChangeCurrency(curcode) {
    var selectedId;
    var selectchangeId;
    var optionsList = Currency;
    //var Change = changes;

    var values = ("ID" + "").split(",");
    var value = [];
    for (var index = 0; index < optionsList.length; index++) {
        if ((optionsList[index].ID) > -1) {
            if (curcode == optionsList[index].Code) {
                selectedId = optionsList[index].ID;
                if (selectedId)
                    value.push(optionsList[index].ID);
            }
        }
    }
    if (selectedId == null) {
        selectedId = null;
    }
    return selectedId;

}

function GetRateIDRAmountWorksheet(row, col, CurrencyID) {
    $.ajax({
        type: "GET",
        url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
        params: {
        },
        headers: {
            "Authorization": "Bearer " + accessToken
        },
        success: function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                $("#dataTable").handsontable('setDataAtCell', row, 7, data.RupiahRate);
                var invamount = $("#dataTable").handsontable('getDataAtCell', row, 6);
                var res = invamount * data.RupiahRate / parseFloat(idrrate);
                res = Math.round(res * 100) / 100;
                res = isNaN(res) ? 0 : res;
                var jumlah = (parseFloat(res).toFixed(2));
                $("#dataTable").handsontable('setDataAtCell', row, 8, jumlah);
            }
        }
    });
}
//end
