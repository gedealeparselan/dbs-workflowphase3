/**
 * Created by Roman Bangkit S on 10/9/2014.
 */
var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;
    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);
    //Declare observable which will be bind with UI
    self.ID = ko.observable("");
    self.Products = ko.observableArray([]);
    /*self.CustomerTypes = ko.observableArray([]);
    self.BizSegments = ko.observableArray([]);
    self.Branchs = ko.observableArray([]);*/
    self.SLATime = ko.observable();
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");

    // filter
    self.FilterProduct = ko.observable("");
    /*self.FilterCustomerType = ko.observable("");
    self.FilterBizSegment = ko.observable("");
    self.FilterBranch = ko.observable("");*/
    self.FilterSLA = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");

    // New Data flag
    self.IsNewData = ko.observable(false);

    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.SLASettings = ko.observableArray([]);

    // Options selected value
    self.Selected = ko.observable(SelectedModel);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    // set default sorting
    self.GridProperties().SortColumn("Product");
    self.GridProperties().SortOrder("ASC");

    // bind clear filters
    self.ClearFilters = function () {
        self.FilterProduct("");
        /*self.FilterCustomerType("");
        self.FilterBizSegment("");
        self.FilterBranch("");*/
        self.FilterSLA("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");

        GetData();
    };

    // bind get data function to view
    self.GetData = function () {
        GetData();
    };

    //The Object which stored data entered in the observables
    /*var SLASetting = {
        ID: self.ID,
        Product: self.Selected().Product(),
        CustomerType: self.Selected().CustomerType(),
        BizSegment: self.Selected().BizSegment(),
        Branch: self.Selected().Branch(),
        SLA: self.SLATime,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
    };*/

    var SLASetting = {
        ID: self.ID,
        Product: self.Selected().Product,
        ProductID: self.Selected().ProductID,
        /*CustomerType: self.Selected().CustomerType,
        CustomerTypeID: self.Selected().CustomerTypeID,
        BizSegment: self.Selected().BizSegment,
        BizSegmentID: self.Selected().BizSegmentID,
        Location: self.Selected().Branch,
        LocationID: self.Selected().BranchID,*/
        SLATime: self.SLATime,
        LastModifiedDate: ko.observable(new Date()),
        LastModifiedBy: ko.observable()
    };
    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    console.log("spUser Roles : " + ko.toJSON(spUser.Roles));
                    console.log("spUser Permited : " + data);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
    self.save = function () {
        // validation
        var vProduct = ko.utils.arrayFirst(self.Products(), function (item) {
            return item.ID == self.Selected().ProductID;
        });

        SLASetting.Product($('#Product option:selected').text());
        /*SLASetting.CustomerType($('#CustomerType option:selected').text());
		SLASetting.BizSegment($('#BizSegment option:selected').text());
		SLASetting.Location($('#Branch option:selected').text());*/

        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {

                    //Ajax call to insert the SLASettings			
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.slasetting,
                        data: ko.toJSON(SLASetting), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.update = function () {
        // validation

        SLASetting.Product($('#Product option:selected').text());
        /*SLASetting.CustomerType($('#CustomerType option:selected').text());
		SLASetting.BizSegment($('#BizSegment option:selected').text());
		SLASetting.Location($('#Branch option:selected').text());*/

        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            // hide current popup window
            //$("#modal-form").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    //Ajax call to update the Customer
                    //console.log(SLASetting);
                    //console.log(ko.toJSON(SLASetting));
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.slasetting + "/" + SLASetting.ID(),
                        data: ko.toJSON(SLASetting),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.delete = function () {
        // hide current popup window

        SLASetting.Product($('#Product option:selected').text());
        /*SLASetting.CustomerType($('#CustomerType option:selected').text());
		SLASetting.BizSegment($('#BizSegment option:selected').text());
		SLASetting.Location($('#Branch option:selected').text());*/

        //$("#modal-form").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.slasetting + "/" + SLASetting.ID(),
                    data: ko.toJSON(SLASetting),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-18
    };

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        self.IsNewData(false);

        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');

        self.ID(data.ID);
        self.Selected().ProductID(data.ProductID);
        /*self.Selected().CustomerTypeID(data.CustomerTypeID);
        self.Selected().BizSegmentID(data.BizSegmentID);
        self.Selected().BranchID(data.LocationID);*/
        self.SLATime(data.SLATime);
        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);
    };

    //insert new
    self.NewData = function () {
        // flag as new Data
        self.IsNewData(true);
        self.Readonly(false);
        // bind empty data
        self.ID(0);
        self.Selected().ProductID('');
        /*self.Selected().CustomerTypeID('');
        self.Selected().BizSegmentID('');
        self.Selected().BranchID('');*/
        self.SLATime('');
    };

    self.GetData = function () { GetData(); }

    //Function to Read All Customers
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.slasetting,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.SLASettings(data.Rows);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterProduct() != "") filters.push({ Field: 'Product', Value: self.FilterProduct() });
        //if (self.FilterCustomerType() != "") filters.push({ Field: 'CustomerType', Value: self.FilterCustomerType() });
        //if (self.FilterBizSegment() != "") filters.push({ Field: 'BizSegment', Value: self.FilterBizSegment() });
        //if (self.FilterBranch() != "") filters.push({ Field: 'Location', Value: self.FilterBranch() });
        if (self.FilterSLA() != "") filters.push({ Field: 'SLATime', Value: self.FilterSLA() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

        return filters;
    };

    self.GetParameters = function () { GetParameters(); }

    //Get parameters
    function GetParameters() {
        var options = {
            url: api.server + api.url.parameter,
            params: {
                select: "Product" //,CustomerType,BizSegment,Branch"
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
    }

    self.OnAfterGetParameters = null;

    function OnSuccessGetParameters(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            self.Products(ko.toJS(data.Product));
            /*self.CustomerTypes(ko.toJS(data.CustomerType));
            self.BizSegments(ko.toJS(data.BizSegment));
            self.Branchs(ko.toJS(data.Branch));*/
            if (self.OnAfterGetParameters) self.OnAfterGetParameters();
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    self.HelperGetSelectedProduct = ko.dependentObservable(function () {
        var product = ko.utils.arrayFirst(self.Products(), function (item) { return item.ID == self.Selected().ProductID(); });
        /*var customertype = ko.utils.arrayFirst(self.CustomerTypes(), function (item) { return item.ID == self.Selected().CustomerTypeID(); });
        var bizsegment = ko.utils.arrayFirst(self.BizSegments(), function (item) { return item.ID == self.Selected().BizSegmentID(); });
        var branch = ko.utils.arrayFirst(self.Branchs(), function (item) { return item.ID == self.Selected().BranchID(); });*/

        if (product != null) { self.Selected().ProductID(product.ID); }
        /*if(customertype != null) { self.Selected().CustomerTypeID(customertype.ID); }
        if(bizsegment != null) { self.Selected().BizSegmentID(bizsegment.ID); }
        if(branch != null) { self.Selected().BranchID(branch.ID); }*/
    });

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });
};