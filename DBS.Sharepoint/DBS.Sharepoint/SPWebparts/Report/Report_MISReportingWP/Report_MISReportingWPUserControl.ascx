﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Report_MISReportingWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.Report.Report_MISReportingWP.Report_MISReportingWPUserControl" %>


<br/>
<br/>
<div class="col-xs-12 col-sm-12">
	<div class="widget-box">
		<div class="widget-header">
			<h4>MIS Reporting</h4>
		</div>

		<div class="widget-body">
			<div class="widget-main">	
				<div class="row">
					<div class="col-lg-4">
						<div class="form-group">
							<label for ="form-field-transaction-type" class="col-sm-5 control-label no-padding-right">
							Transaction Type</label>
							<span class="block input-icon input-icon-right">
								<select data-bind="options: ddlTransactionType, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" class="width-55" id="form-field-transaction-type">
									
								</select>
							</span>
						</div>
						<div class="form-group">						
							<label for ="form-field-report-period" class="col-sm-5 control-label no-padding-right">
							Report Period</label>
							<span class="block input-icon input-icon-right">
								<select data-bind="options: ddlReportPeriod, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" class="width-55" id="form-field-report-period">									
								</select>
							</span>
						</div>
						<div class="form-group">						
							<label for ="form-field-segmentation" class="col-sm-5 control-label no-padding-right">
							Segmentation</label>
							<span class="block input-icon input-icon-right">
								<select data-bind="options: ddlSegmentation, optionsText: 'Description', optionsValue: 'ID', optionsCaption: 'Please Select...'" class="width-55" id="form-field-segmentation">
									
								</select>
							</span>
						</div>
					</div>
				
					<div class="col-lg-3">
						<div class="form-group">
							<label for ="form-field-channel" class="col-sm-5 control-label no-padding-right">
							Channel</label>
							<span class="block input-icon input-icon-right">
								<select data-bind="options: ddlChannel, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" class="width-55" id="form-field-channel">
									
								</select>
							</span>
						</div>
						<div class="form-group">						
							<label for ="form-field-branch" class="col-sm-5 control-label no-padding-right">
							Branch</label>
							<span class="block input-icon input-icon-right">
								<select data-bind="options: ddlBranch, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'," class="width-55" id="form-field-branch">
									
								</select>
							</span>
						</div>
					</div>
					
					<div class="col-lg-3">
						<div class="form-group">
							<label for ="form-field-start-date" class="col-sm-5 control-label no-padding-right">
							Start Date</label>
							<span class="block input-icon input-icon-right">
	 							<input class="date-picker width-50" type="text" data-date-format="dd-M-yyyy" id="start-date" name="start-date" data-rule-required="true" data-rule-date="true">
                            		<span class="input-append date">
                                		<i class="icon-calendar bigger-110"></i>
                            		</span>
	                        </span>
							</div>
						<div class="form-group"> 						
							<label for ="form-field-end-date" class="col-sm-5 control-label no-padding-right">
							End Date</label>
							<span class="block input-icon input-icon-right">
	 							<input class="date-picker width-50" type="text" data-date-format="dd-M-yyyy" id="end-date" name="end-date" data-rule-required="true" data-rule-date="true">
                            		<span class="input-append date">
                                		<i class="icon-calendar bigger-110"></i>
                            		</span>
	                        </span>						
							</div>
					</div>
					
					<div class="col-lg-2">
						<div class="form-group" style="padding:5px;">
							<input id="chkDateNull" type="checkbox" value="startDateNull"/>&nbsp;NULL
						</div>
						<div class="form-group" style="padding:13px;">
											
						</div>
					</div>
					
					<div class="col-lg-2">
						<div class="form-group">
							<button type="button" id="btnViewMIS" class="btn btn-sm btn-primary">
							View Report</button>					
						</div>
					</div>


				</div>					
		</div>
	</div>
</div>
<br/>
<div class="row" id="lblInfoPeriod" style="margin-left:1px;margin-right:1px;"><label  id="lblMISReporting" class="coll-sm4 pull-left"></label><label  id="lblLastUpdate" class="coll-sm4 pull-right"></label></div>
<div class="row">
	<div id="chartholder" style="height:400px; width:40%;" class="col-xs-12 col-sm-7"></div>
	<div id="separatorholder" style="height:400px; width:10%;" class="col-xs-12 col-sm-7"></div>
	<div id="pieholder" style="height:370px; width:40%;" class="col-xs-12 col-sm-7"></div>
</div>
<div class="row">
	<div id="legendplaceholder" class="col-xs-12 col-sm-5"></div>
</div>

<div id="widget-box-holder">

</div>
</div>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout-3.1.0.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/Plugins/Moment/moment.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/Plugins/Moment/moment-with-locales.min.js"></script>

<!-- Don't touch this! -->


    <script class="include" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.jqplot.min.js"></script>
    <!--<script type="text/javascript" src="syntaxhighlighter/scripts/shCore.min.js"></script>
    <script type="text/javascript" src="syntaxhighlighter/scripts/shBrushJScript.min.js"></script>
    <script type="text/javascript" src="syntaxhighlighter/scripts/shBrushXml.min.js"></script>-->
<!-- Additional plugins go here -->

<!-- jqplot library -->
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/excanvas.min.js"></script><![endif]-->
  <script class="include" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jqplot.barRenderer.min.js"></script> 
  <script class="include" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jqplot.pieRenderer.min.js"></script> 
  <script class="include" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jqplot.categoryAxisRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jqplot.pointLabels.min.js"></script>  
  <script class="include" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jqplot.canvasAxisLabelRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jqplot.canvasAxisTickRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jqplot.canvasTextRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jqplot.highlighter.js"></script>
  <script class="include" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jqplot.dateAxisRenderer.min.js"></script>  
  <link href="/_catalogs/masterpage/Ace/assets/css/jquery.jqplot.min.css" rel="stylesheet"/>

<!-- End additional plugins -->
<script type="text/javascript" src="/SiteAssets/Scripts/MISReporting.js"></script>

<style type="text/css">
	.jqplot-point-label,.jqplot-data-label{
		color: #eaeaea;	
	}	
	.jqplot-highlighter{
		color: #d15b47;
		font-size:14px;
		font-weight:bold;
	}
	th{
		text-align:center;
	}
       .ms-rte-embedcode.ms-rte-embedwp{
                word-wrap:break-word;
      }
</style>

