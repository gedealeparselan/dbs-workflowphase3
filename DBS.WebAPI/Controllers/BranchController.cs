﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class BranchController : ApiController
    {
        private string message = string.Empty;

        private readonly IBranchRepository repository = null;

        public BranchController()
        {
            this.repository = new BranchRepository();
        }

        public BranchController(IBranchRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all currencies.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<BranchModel>))]
        public HttpResponseMessage Get()
        {
            IList<BranchModel> output = new List<BranchModel>();

            if (!repository.GetBranch(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<BranchModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get channels with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<BranchModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<BranchModel> output = new List<BranchModel>();

            if (!repository.GetBranch(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<BranchModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get channels for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(BranchGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<BranchFilter> filters)
        {
            BranchGrid output = new BranchGrid();

            if (!repository.GetBranch(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<BranchGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find channels using criterias Name.
        /// </summary>
        /// <param name="query">Name </param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<ChannelModel>))]
        [HttpGet]
        [Route("api/Branch/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<BranchModel> output = new List<BranchModel>();

            if (!repository.GetBranchDetail(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<BranchModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(IList<BranchModel>))]
        [HttpGet]
        [Route("api/Branch/SearchDetail")]
        public HttpResponseMessage SearchDetail(string query, int? limit, int id)
        {
            IList<BranchModel> output = new List<BranchModel>();

            if (!repository.GetBankBranchSelected(query, limit.HasValue ? limit.Value : 10, id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<BranchModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Channel by ID.
        /// </summary>
        /// <param name="id">Channel ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(BranchModel))]
        [Route("api/Branch/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            BranchModel output = new BranchModel();

            if (!repository.GetBranchByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<BranchModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new Channel.
        /// </summary>
        /// <param name="Channel">Channel data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(BranchModel branch)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddBranch(branch, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Branch data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing Channel.
        /// </summary>
        /// <param name="id">ID of channel to be modify.</param>
        /// <param name="customer">Channel data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/Branch/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, BranchModel branch)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateBranch(id, branch, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Branch data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting channel.
        /// </summary>
        /// <param name="id">ID of channel to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/Branch/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteBranch(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Branch data has been deleted." });
                }
            }
        }

        [ResponseType(typeof(BranchModel))]
        [Route("api/Branch/SOLID")]
        [HttpGet]
        public HttpResponseMessage GetParam(string query)
        {
            IList<BranchModel> result = new List<BranchModel>();

            if (!repository.GetSolID(query, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<BranchModel>>(HttpStatusCode.OK, result);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}