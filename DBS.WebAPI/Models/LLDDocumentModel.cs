﻿using DBS.WebAPI.Models;
using DBS.WebAPI.Helpers;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Dynamic;

namespace DBS.WebAPI.Models
{
    #region property
    public class LLDDocumentModel
    {
        public int LLDDocumentID { get; set; }
        public string LLDDocumentCode { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }
    public class LLDDocumentRow : LLDDocumentModel
    {
        public int RowID { get; set; }

    }
    public class LLDDocumentGrid : Grid
    {
        public IList<LLDDocumentRow> Rows { get; set; }
    }
    #endregion

    #region Filter
    public class LLDDocumentFilter : Filter { }
    #endregion

    #region interface
    public interface ILLDDocumentRepository : IDisposable
    {
        bool GetLLDDocumentByID(int id, ref LLDDocumentModel LLDDocument, ref string message);
        bool GetLLDDocument(ref IList<LLDDocumentModel> LLDDocument, ref string message);
        bool GetLLDDocument(int page, int size, IList<LLDDocumentFilter> filters, string sortColumn, string sortOrder, ref LLDDocumentGrid LLDDocument, ref string message);
        bool AddLLDDocument(LLDDocumentModel LLDDocument, ref string message);
        bool UpdateLLDDocument(int id, LLDDocumentModel LLDDocument, ref string message);
        bool DeleteLLDDocument(int id, ref string message);
        bool GetLLDDocument(string key, int limit, ref IList<LLDDocumentModel> LLD, ref string message);
     }
    #endregion

    #region Repository
    public class LDDDocumentRepository : ILLDDocumentRepository 
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetLLDDocumentByID(int id, ref LLDDocumentModel LLDDocument, ref string message)
        {
            bool isSuccess = false;

            try
            {
                LLDDocument = (from a in context.LLDDocuments
                       where a.IsDeleted.Value.Equals(false) && a.LLDDocumentID.Equals(id)
                       select new LLDDocumentModel
                       {
                           LLDDocumentID = a.LLDDocumentID,
                           LLDDocumentCode = a.LLDDocumentCode,
                           Description = a.Description,
                           LastModifiedDate = a.CreateDate,
                           LastModifiedBy = a.CreateBy,
                       }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetLLDDocument(ref IList<LLDDocumentModel> LLDDocument, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    LLDDocument = (from a in context.LLDDocuments
                           where a.IsDeleted.Value.Equals(false)
                           orderby new { a.LLDDocumentCode, a.Description }
                           select new LLDDocumentModel
                           {
                               LLDDocumentID = a.LLDDocumentID,
                               LLDDocumentCode = a.LLDDocumentCode,
                               Description = a.Description,
                               LastModifiedBy = a.CreateBy,
                               LastModifiedDate = a.CreateDate,
                           }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetLLDDocument(int page, int size, IList<LLDDocumentFilter> filters, string sortColumn, string sortOrder, ref LLDDocumentGrid LLDDocument, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                LLDDocumentModel filter = new LLDDocumentModel();
                if (filters != null)
                {
                    filter.LLDDocumentCode = (string)filters.Where(a => a.Field.Equals("LLDDocumentCode")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.LLDDocuments
                                let isLLDDocumentCode = !string.IsNullOrEmpty(filter.LLDDocumentCode)
                                let isDescription = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted == false &&
                                    (isLLDDocumentCode ? a.LLDDocumentCode.Contains(filter.LLDDocumentCode) : true) &&
                                    (isDescription ? a.Description.Contains(filter.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new LLDDocumentModel
                                {
                                    LLDDocumentID = a.LLDDocumentID,
                                    LLDDocumentCode = a.LLDDocumentCode,
                                    Description = a.Description,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    LLDDocument.Page = page;
                    LLDDocument.Size = size;
                    LLDDocument.Total = data.Count();
                    LLDDocument.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    LLDDocument.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new LLDDocumentRow
                        {
                            RowID = i + 1,
                            LLDDocumentID = a.LLDDocumentID,
                            LLDDocumentCode = a.LLDDocumentCode,
                            Description = a.Description,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddLLDDocument(LLDDocumentModel LLDDocument, ref string message)
        {
            bool isSuccess = false;
            try
            {
                if (!context.LLDDocuments.Where(a => a.LLDDocumentID == LLDDocument.LLDDocumentID && a.IsDeleted == false).Any())
                {
                    LLDDocument LLDDoc = new LLDDocument();

                    LLDDoc.LLDDocumentCode = LLDDocument.LLDDocumentCode;
                    LLDDoc.Description = LLDDocument.Description;
                    LLDDoc.IsDeleted = false;
                    LLDDoc.CreateDate = DateTime.UtcNow;
                    LLDDoc.CreateBy = currentUser.GetCurrentUser().DisplayName;

                    context.LLDDocuments.Add(LLDDoc);
                    context.SaveChanges();
                    isSuccess = true;
                }
                else
                {
                    message = string.Format("LLDDocument  {0} is already exist.", LLDDocument.LLDDocumentID);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateLLDDocument(int id, LLDDocumentModel LLDDocument, ref string message)
        {
            bool isSucces = false;
            try
            {
                Entity.LLDDocument data = context.LLDDocuments.Where(a => a.LLDDocumentID.Equals(id)).SingleOrDefault();
                if (data != null)
                {

                    data.LLDDocumentCode = LLDDocument.LLDDocumentCode;
                    data.Description = LLDDocument.Description;
                    data.CreateBy = currentUser.GetCurrentUser().DisplayName;
                    data.CreateDate = DateTime.UtcNow;
                    context.SaveChanges();
                    isSucces = true;
                }
                else
                {
                    message = string.Format("Data LLDDocument {0} is does not exist.", id);
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool DeleteLLDDocument(int id, ref string message)
        {
            bool isSucces = false;
            try
            {
                Entity.LLDDocument data = context.LLDDocuments.Where(a => a.LLDDocumentID.Equals(id)).SingleOrDefault();
                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                    context.SaveChanges();
                    isSucces = true;
                }
                else
                {
                    message = string.Format("LLD Document data for LLD Document {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public bool GetLLDDocument(string key, int limit, ref IList<LLDDocumentModel> LLD, ref string message)
        {
            throw new NotImplementedException();
        }
    }
    #endregion
}