﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Net.Http;
using System.Web.Script.Serialization;
using System.Transactions;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("AvailableCustomer")]
    public class AvailableCustomerModel
    {
        /// <summary>
        /// EcalationCustomer ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// EcalationCustomer Name.
        /// </summary>        
        public string CIF { get; set; }

        /// <summary>
        /// EcalationCustomer.
        /// </summary> 
        public string Name { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }
    public class AvailableCustomerRow : AvailableCustomerModel
    {
        public int RowID { get; set; }

    }

    public class AvailableCustomerGrid : Grid
    {
        public IList<AvailableCustomerRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class AvailableCustomerFilter : Filter { }
    #endregion

    #region Interface
    public interface IAvailableCustomerRepository : IDisposable
    {
        bool GetAvailableCustomer(int page, int size, IList<AvailableCustomerFilter> filters, string sortColumn, string sortOrder, ref AvailableCustomerGrid availableCustomersGrid, ref string message);
       
    }
    #endregion

    #region Repository
    public class AvailableCustomerRepository : IAvailableCustomerRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetAvailableCustomer(int page, int size, IList<AvailableCustomerFilter> filters, string sortColumn, string sortOrder, ref AvailableCustomerGrid availableCustomersGrid, ref string message)
        {
            bool isSuccess = false;
            bool typeCustomer = false;
           
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                AvailableCustomerModel filter = new AvailableCustomerModel();
                if (filters != null)
                {
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("CIFAvailableCustomer")).Select(a => a.Value).SingleOrDefault();
                    filter.Name = (string)filters.Where(a => a.Field.Equals("CustomerNameAvailableCustomer")).Select(a => a.Value).SingleOrDefault();
                }
                string CIF = filter.CIF;
                string CustomerName = filter.Name;
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.SP_GetEscalationCustomer(CIF, CustomerName, typeCustomer)                                                                     
                                select new AvailableCustomerModel
                                {
                                    CIF = a.CIF,
                                    Name = a.CustomerName
                                }).ToList();

                    availableCustomersGrid.Page = page;
                    availableCustomersGrid.Size = size;
                    availableCustomersGrid.Total = data.Count();
                    availableCustomersGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    availableCustomersGrid.Rows = data.AsQueryable().OrderBy(orderBy)
                        .Select((a, i) => new AvailableCustomerRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            CIF = a.CIF,
                            Name = a.Name,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                        context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

    #endregion
}