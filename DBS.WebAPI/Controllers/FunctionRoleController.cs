﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class FunctionRoleController : ApiController
    {
        private string message = string.Empty;

        private readonly IFunctionRoleRepository repository = null;

        public FunctionRoleController()
        {
            this.repository = new FunctionRoleRepository();
        }

        public FunctionRoleController(IFunctionRoleRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all Function Roles.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<FunctionRoleModel>))]
        public HttpResponseMessage Get()
        {
            IList<FunctionRoleModel> output = new List<FunctionRoleModel>();

            if (!repository.GetFunctionRole(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<FunctionRoleModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Function Roles with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<FunctionRoleModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<FunctionRoleModel> output = new List<FunctionRoleModel>();

            if (!repository.GetFunctionRole(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<FunctionRoleModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Function Roles for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(BankGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<FunctionRoleFilter> filters)
        {
            FunctionRoleGrid output = new FunctionRoleGrid();

            if (!repository.GetFunctionRole(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<FunctionRoleGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find Function Roles using criterias Code or Description.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<FunctionRoleModel>))]
        [HttpGet]
        [Route("api/FunctionRole/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<FunctionRoleModel> output = new List<FunctionRoleModel>();

            if (!repository.GetFunctionRole(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<FunctionRoleModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Function Role by ID.
        /// </summary>
        /// <param name="id">Function Role ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(FunctionRoleModel))]
        [Route("api/FunctionRole/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            FunctionRoleModel output = new FunctionRoleModel();

            if (!repository.GetFunctionRoleByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<FunctionRoleModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new Function Role.
        /// </summary>
        /// <param name="FunctionRole">Function Role data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(FunctionRoleModel FunctionRole)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddFunctionRole(FunctionRole, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Function Role data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing FunctionRole.
        /// </summary>
        /// <param name="id">ID of Function Role to be modify.</param>
        /// <param name="FunctionRole">Function Role data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        // [Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        [Route("api/FunctionRole/{id}")]
        public HttpResponseMessage Put(int id, FunctionRoleModel FunctionRole)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateFunctionRole(id, FunctionRole, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Function Role data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting Function Role.
        /// </summary>
        /// <param name="id">ID of Function Role to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/FunctionRole/{id}")]
        [ResponseType(typeof(string))]
        // [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteFunctionRole(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "FunctionRole data has been deleted." });
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}