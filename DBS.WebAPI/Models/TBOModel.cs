﻿using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;

namespace DBS.WebAPI.Models
{
    #region property
    [ModelName("TBOModel")]
    public class TBOModel
    {
        #region property
        public long ID { get; set; }
        public long TBOTransactionDraftID { get; set; }
        public long TransactionID { get; set; }
        public string ApplicationID { get; set; }
        public int? TBOSLAID { get; set; }
        public DateTime? ExptDate { get; set; }
        public DateTime? ActDate { get; set; }
        public string CIF { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int? Days { get; set; }
        public int DocumentCount { get; set; }
        public IList<DocumentCount> CountDoc { get; set; }
        public bool IsApprove { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public DateTime? Modified { get; set; }
        public DocumentTypeTBOModel TypeofDoc { get; set; }
        public DocumentPurposeTBOModel PurposeofDoc { get; set; }
        public DocumentModelTBO FileName { get; set; }
        public string DocumentPath { get; set; }
        public IList<TransactionTBODocumentModel> Documents { get; set; }
        public string Status { get; set; }
        public string CustomerName { get; set; }
        public int TypeDocID { get; set; }
        public int DocumentPurposeID { get; set; }
        public long DocumentID { get; set; }
        public string TBOApplicationID { get; set; }
        public Guid? SPTaskListID { get; set; }
        public int? SPTaskListItemID { get; set; }
        public Guid? WorkflowInstanceID { get; set; }
    }
        #endregion
    public class TBOModelWF
    {
        public IList<TransactionTBODocumentModelWF> Documents { get; set; }
    }

    public class TransactionTBODocumentModel
    {
        public long ID { get; set; }
        public string ApplicationID { get; set; }
        public string TBOApplicationID { get; set; }
        public string CIF { get; set; }
        public string Name { get; set; }
        public DateTime ActDate { get; set; }
        public DateTime ExptDate { get; set; }
        public int TBOSLAID { get; set; }
        public DocumentTypeModel Type { get; set; }
        public DocumentPurposeTBOModel Purpose { get; set; }
        public string FileName { get; set; }
        public string DocumentPath { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public string Status { get; set; }
    }

    public class TransactionTBOTimelineModel
    {
        public long ApproverID { get; set; }
        public string Activity { get; set; }
        public DateTime? Time { get; set; }
        public string Outcome { get; set; }
        public string UserOrGroup { get; set; }
        public bool IsGroup { get; set; }
        public string DisplayName { get; set; }
        public string Comment { get; set; }
    }

    public class TransactionTBODocumentModelWF
    {
        public long ID { get; set; }
        public string ApplicationID { get; set; }
        public string CIF { get; set; }
        public string Name { get; set; }
        public DateTime ActDate { get; set; }
        public DateTime ExptDate { get; set; }
        public int TBOSLAID { get; set; }
        public DocumentTypeModel Type { get; set; }
        public DocumentPurposeTBOModel Purpose { get; set; }
        public DocumentModelTBO FileName { get; set; }
        public string DocumentPath { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public string Status { get; set; }
        public string TBOApplicationID { get; set; }
        public bool IsChecklistSubmit { get; set; }
    }

    public class DocumentPurposeTBOModel 
    {
        public int? ID { get; set; }
        public int Purpose { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }

    public class DocumentModelTBO
    {
        public long? ID { get; set; }
        public DocumentTypeTBOModel Type { get; set; }
        public DocumentPurposeTBOModel Purpose { get; set; }
        public string name { get; set; }
        public string DocumentPath { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }

    public class DocumentTypeTBOModel
    {
        public long? ID { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public int? Days { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }

    public class DocumentCount
    {
        public int Count { get; set; }
    }
    public class TBOModelRow : TBOModel
    {
        public int RowID { get; set; }
    }

    public class TBOModelGrid : Grid
    {
        public IList<TBOModelRow> Rows { get; set; }

        public IList<TBOModelRow> Data { get; set; }
    }
        #endregion

    #region Filter
    public class TBOModelFilter : Filter
    {

    }
    #endregion

    #region interface
    public interface ITBOModelInterface : IDisposable
    {
        bool GetTBOByID(int id, ref TBOModel TBO, ref string message);
        bool GetTBO(ref IList<TBOModel> TBO, int limit, int index, ref string message);
        bool GetTBO(ref IList<TBOModel> TBO, ref string message);
        bool GetTBO(ref IList<DocumentTypeTBOModel> TBO, ref string message);
        bool GetTBO(int page, int size, IList<TBOModelFilter> filters, string sortColumn, string sortOrder, ref TBOModelGrid TBO, ref string message);
        bool GetTBOWF(int page, int size, IList<TBOModelFilter> filters, string sortColumn, string sortOrder, ref TBOModelGrid TBO, ref string message);
        bool GetTBO(TBOModelFilter filter, ref IList<TBOModel> TBO, ref string message);
        bool GetTBO(ref IList<TBOModel> TBO, string cif, ref string message);
        bool GetTBOWF(ref IList<TBOModel> TBO, string cif, ref string message);
        bool GetTBOWFID(ref IList<TBOModel> TBO, string spUserLoginName, ref string message);
        bool GetTransactionTimeline(ref IList<TransactionTBOTimelineModel> timelines, Guid workflowInstanceID, ref string message);
        bool UpdateTBO(TBOModel TBO, ref string message);
        bool SaveTBO(TBOModelWF TBO, ref string message);
        bool GetTransactionTBOByCif(ref IList<TBOModel> TBO, string id, ref string message);
    }
    #endregion
    #region repository
    public class TBOModelRepository : ITBOModelInterface
    {

        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();


        public bool GetTBOByID(int id, ref TBOModel TBO, ref string message)
        {
            bool isSucces = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                    TBO = (from a in context.TBOTransactionDrafts
                           join b in context.Transactions on a.TransactionID equals b.TransactionID
                           join c in context.Customers on b.CIF equals c.CIF

                           where a.TBOTransactionDraftID.Equals(id)
                           select new TBOModel
                           {
                               ID = a.TBOTransactionDraftID,
                               Name = c.CustomerName,
                               CIF = c.CIF,
                               LastModifiedBy = a.ModifiedDate == null ? a.CreatedBy : a.ModifiedBy,
                               LastModifiedDate = a.ModifiedDate == null ? a.CreatedDate : a.ModifiedDate.Value,
                           }).FirstOrDefault();
                if (TBO != null)
                {
                    TBOModel TBOData = new TBOModel();
                    TBO.DocumentCount = (from a in context.TBOTransactions where a.TransactionID.Equals(id) select a).Count();
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool GetTBO(ref IList<TBOModel> TBO, int limit, int index, ref string message)
        {
            bool isSucces = false;
            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    TBO = (from a in context.TBOTransactionDrafts
                           join b in context.Transactions on a.TransactionID equals b.TransactionID
                           join c in context.Customers on b.CIF equals c.CIF
                           select new TBOModel
                                {
                                    ID = a.TransactionID.Value,
                                    Name = c.CustomerName,
                                    CIF = c.CIF,
                                    TransactionID = b.TransactionID,
                                    LastModifiedBy = a.ModifiedDate == null ? a.CreatedBy : a.ModifiedBy,
                                    LastModifiedDate = a.ModifiedDate == null ? a.CreatedDate : a.ModifiedDate.Value,
                                }).Skip(skip).Take(limit).ToList();
                    TBOModel TBOData = new TBOModel();
                    TBOData.DocumentCount = TBO.Count();
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool GetTBO(ref IList<TBOModel> TBO, ref string message)
        {
            bool isSucces = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    TBO = (from a in context.TBOSLAs
                           select new TBOModel
                           {
                               ID = a.TBOSLAID,
                               Code = a.TBOCode,
                               Name = a.Description,
                               Days = a.Days
                           }).ToList();
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool GetTBO(int page, int size, IList<TBOModelFilter> filters, string sortColumn, string sortOrder, ref TBOModelGrid TBO, ref string message)
        {
            bool isSucces = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                TBOModel filter = new TBOModel();
                if (filters != null)
                {
                    filter.CIF = filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.Name = filters.Where(a => a.Field.Equals("CustomerName")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("ModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("ModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("ModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.TBOTransactionDrafts
                                join b in context.Transactions on a.TransactionID equals b.TransactionID
                                join c in context.Customers on b.CIF equals c.CIF
                                let isCif = !string.IsNullOrEmpty(filter.CIF)
                                let isName = !string.IsNullOrEmpty(filter.Name)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where
                                (isCif ? c.CIF.Contains(filter.CIF) : true) &&
                                (isName ? c.CustomerName.Contains(filter.Name) : true) &&
                                (isCreateBy ? (a.ModifiedDate == null ? a.CreatedBy.Contains(filter.LastModifiedBy) : a.ModifiedBy.Contains(filter.LastModifiedBy)) : true) &&
                                (isCreateDate ? ((a.ModifiedDate == null ? a.CreatedDate : a.ModifiedDate) > filter.LastModifiedDate.Value && ((a.ModifiedDate == null ? a.CreatedDate : a.ModifiedDate.Value) < maxDate)) : true)
                                && a.Status != "Complete"
                                select new TBOModel
                                {
                                    ID = a.TransactionID.Value,
                                    TBOTransactionDraftID = a.TBOTransactionDraftID,
                                    ApplicationID = b.ApplicationID,
                                    Name = c.CustomerName,
                                    CIF = c.CIF,
                                    CreatedBy = a.CreatedBy,
                                    CreatedDate = a.CreatedDate,
                                    LastModifiedBy = a.ModifiedDate == null ? a.CreatedBy : a.ModifiedBy,
                                    LastModifiedDate = a.ModifiedDate == null ? a.CreatedDate : a.ModifiedDate.Value,
                                });

                    TBO.Page = page;
                    TBO.Size = data.Count();
                    TBO.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    TBO.Rows = data.OrderBy(orderBy).AsEnumerable().Select((a, i) => new TBOModelRow
                    {
                        ID = a.ID,
                        TBOTransactionDraftID = a.TBOTransactionDraftID,
                        ApplicationID = a.ApplicationID,
                        Name = a.Name,
                        CIF = a.CIF,
                        DocumentCount = (from ab in context.TBOTransactionDrafts
                                         join bc in context.Transactions on ab.TransactionID equals bc.TransactionID
                                         join cd in context.Customers on bc.CIF equals cd.CIF
                                         where cd.CIF == a.CIF && (ab.Status.Contains("Add") || ab.Status.Contains("Revise") || string.IsNullOrEmpty(ab.Status))
                                         select ab.TBOSLAID.Value).Count(),
                        CreatedBy = a.CreatedBy,
                        CreatedDate = a.CreatedDate,
                        LastModifiedBy = a.LastModifiedBy,
                        LastModifiedDate = a.LastModifiedDate,
                    }).Skip(skip)
                    .Take(data.Count()).ToList();
                    TBO.Data = TBO.Rows.GroupBy(a => a.CIF).Select(g => g.FirstOrDefault()).ToList();
                    TBO.Total = TBO.Data.Count();
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool GetTBO(TBOModelFilter filter, ref IList<TBOModel> TBO, ref string message)
        {
            throw new NotImplementedException();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public bool GetTBO(ref IList<TBOModel> TBO, int id, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    TBO = (from a in context.TBOTransactionDrafts
                           where a.TransactionID.Equals(id)
                           select new TBOModel
                           {
                               ID = a.TBOTransactionDraftID,
                               TBOSLAID = a.TBOSLAID.Value,
                               ExptDate = a.TBOSubmissionDate.Value,
                               CreatedBy = a.CreatedBy,
                               CreatedDate = a.CreatedDate.Value,
                               LastModifiedBy = a.ModifiedBy,
                               LastModifiedDate = a.ModifiedDate
                           }
                                         ).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetTBO(ref IList<TBOModel> TBO, string cif, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    var tbo = (from a in context.TBOTransactionDrafts
                               from b in context.Transactions.Where(x => a.TransactionID == x.TransactionID).DefaultIfEmpty()
                               from c in context.Customers.Where(x => b.CIF == x.CIF).DefaultIfEmpty()
                               from d in context.TransactionDocuments.Where(x => a.TransactionDocumentID == x.TransactionDocumentID).DefaultIfEmpty()
                               from e in context.DocumentPurposes.Where(x => d.PurposeID == x.PurposeID).DefaultIfEmpty()
                               from f in context.TBOSLAs.Where(x => a.TBOSLAID.Value == x.TBOSLAID).DefaultIfEmpty()
                               where b.CIF == cif && a.Status != "Complete"
                               select new TBOModel()
                               {
                                   TBOTransactionDraftID = a.TBOTransactionDraftID,
                                   TransactionID = b.TransactionID,
                                   ApplicationID = b.ApplicationID,
                                   CIF = b.CIF,
                                   CustomerName = c.CustomerName,
                                   ID = a.TBOTransactionDraftID,
                                   TBOSLAID = a.TBOSLAID.Value,
                                   ActDate = a.TBOActualDate.HasValue ? a.TBOActualDate : null,
                                   ExptDate = a.TBOSubmissionDate,
                                   Status = a.Status == null ? a.Status : a.Status,
                                   TypeDocID = a.DocumentTypeID.HasValue ? a.DocumentTypeID.Value : 0,
                                   DocumentPurposeID = a.DocumentPurposeID.HasValue ? a.DocumentPurposeID.Value : 0,
                                   DocumentID = a.TransactionDocumentID.HasValue ? a.TransactionDocumentID.Value : 0,
                                   CreatedBy = a.CreatedBy,
                                   CreatedDate = a.CreatedDate,
                                   LastModifiedBy = a.ModifiedBy,
                                   LastModifiedDate = a.ModifiedDate,
                                   TBOApplicationID = a.TBOApplicationID
                               }).ToList();


                    foreach (var item in tbo)
                    {
                        if (item.TypeDocID != 0)
                        {
                            var TypeofDoc = (from a in context.TBOSLAs
                                             where a.TBOSLAID == item.TypeDocID
                                             select a).SingleOrDefault();
                            if (TypeofDoc != null)
                            {
                                item.TypeofDoc = new DocumentTypeTBOModel()
                                 {
                                     ID = TypeofDoc.TBOSLAID,
                                     Name = TypeofDoc.Description,
                                     Description = TypeofDoc.Description
                                 };
                            }
                            else
                            {
                                item.TypeofDoc = new DocumentTypeTBOModel()
                                {
                                    ID = 0,
                                    Name = "",
                                    Description = ""
                                };
                            }
                        }
                        else
                        {
                            item.TypeofDoc = new DocumentTypeTBOModel()
                            {
                                ID = 0,
                                Name = "",
                                Description = ""
                            };
                        }

                        if (item.DocumentPurposeID != 0)
                        {
                            var PurposeDoc = (from a in context.DocumentPurposes
                                              where a.PurposeID == item.DocumentPurposeID
                                              select a).SingleOrDefault();
                            if (PurposeDoc != null)
                            {
                                item.PurposeofDoc = new DocumentPurposeTBOModel()
                                {
                                    ID = PurposeDoc.PurposeID,
                                    Name = PurposeDoc.PurposeName,
                                    Description = PurposeDoc.PurposeDescription
                                };
                            }
                            else
                            {
                                item.PurposeofDoc = new DocumentPurposeTBOModel()
                                {
                                    ID = 0,
                                    Name = "",
                                    Description = ""
                                };
                            }
                        }
                        else
                        {
                            item.PurposeofDoc = new DocumentPurposeTBOModel()
                            {
                                ID = 0,
                                Name = "",
                                Description = ""
                            };
                        }


                        if (item.DocumentID != 0)
                        {
                            var Document = (from a in context.TransactionDocuments
                                            where a.TransactionDocumentID == item.DocumentID
                                            select a).SingleOrDefault();
                            if (Document != null)
                            {
                                item.FileName = new DocumentModelTBO()
                                {
                                    ID = Document.TransactionDocumentID,
                                    name = Document.Filename,
                                    DocumentPath = Document.DocumentPath
                                };
                            }
                            else
                            {
                                item.FileName = new DocumentModelTBO()
                                {
                                    ID = 0,
                                    name = "",
                                    DocumentPath = ""
                                };
                            }
                        }
                        else
                        {
                            item.FileName = new DocumentModelTBO()
                            {
                                ID = 0,
                                name = "",
                                DocumentPath = ""
                            };
                        }
                    }
                    TBO = tbo;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


        public bool UpdateTBO(TBOModel TBO, ref string message)
        {
            bool isSuccess = false;
            IList<TBOTransactionDraft> TBODraft = new List<TBOTransactionDraft>();
            try
            {
                DateTime utcTime = DateTime.UtcNow;
                string strUtcTime_o = utcTime.ToString("o").Replace("-", "").Replace(":", "").Replace(".", "").Replace("T", "").Replace("Z", "");

                long id;
                foreach (var item in TBO.Documents)
                {
                    id = context.Transactions.Where(a => a.ApplicationID == item.ApplicationID).Select(a => a.TransactionID).FirstOrDefault();

                    #region Transaction Document
                    Entity.TransactionDocumentTBO document = new Entity.TransactionDocumentTBO()
                    {
                        TransactionDocumentID = id,
                        DocTypeID = item.Type.ID,
                        PurposeID = item.Purpose.ID,
                        Filename = item.FileName,
                        DocumentPath = item.DocumentPath,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    };
                    context.TransactionDocumentTBOes.Add(document);
                    context.SaveChanges();
                    #endregion

                    #region Update flag TBO Transaction Draft
                    Entity.TBOTransactionDraft data = new TBOTransactionDraft()
                    {
                        TBOActualDate = item.ActDate,
                        DocumentTypeID = item.Type.ID,
                        DocumentPurposeID = item.Purpose.ID,
                        ModifiedBy = currentUser.GetCurrentUser().DisplayName,
                        ModifiedDate = DateTime.UtcNow,
                        TBOSLAID = item.TBOSLAID,
                        TransactionDocumentID = document.TransactionDocumentID,
                        TransactionID = document.TransactionDocumentID,
                        TBOApplicationID = item.TBOApplicationID//strUtcTime_o
                    };
                    #endregion
                    TBODraft.Add(data);
                    context.SaveChanges();
                }

                foreach (var item in TBODraft)
                {
                    Entity.TBOTransactionDraft data = context.TBOTransactionDrafts.Where(a => a.TransactionID == item.TransactionID && a.TBOSLAID == item.TBOSLAID).SingleOrDefault();
                    if (data != null)
                    {
                        data.TBOActualDate = item.TBOActualDate;
                        data.DocumentTypeID = item.DocumentTypeID;
                        data.DocumentPurposeID = item.DocumentPurposeID;
                        data.ModifiedBy = item.ModifiedBy;
                        data.ModifiedDate = item.ModifiedDate;
                        data.Status = "Add";
                        data.TransactionDocumentID = item.TransactionDocumentID;
                        data.TBOApplicationID = item.TBOApplicationID;
                        context.SaveChanges();
                    }
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }


        public bool GetTBOWF(int page, int size, IList<TBOModelFilter> filters, string sortColumn, string sortOrder, ref TBOModelGrid TBO, ref string message)
        {
            bool isSucces = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                TBOModel filter = new TBOModel();
                if (filters != null)
                {
                    filter.Name = filters.Where(a => a.Field.Equals("CustomerName")).Select(a => a.Value).SingleOrDefault();
                    filter.CIF = filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.Status = filters.Where(a => a.Field.Equals("Status")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("ModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("ModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.TBOTransactionDrafts
                                join b in context.Transactions on a.TransactionID equals b.TransactionID
                                join c in context.Customers on b.CIF equals c.CIF
                                let isCif = !string.IsNullOrEmpty(filter.CIF)
                                let isName = !string.IsNullOrEmpty(filter.Name)
                                let isStatus = !string.IsNullOrEmpty(filter.Status)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where
                                (isCif ? c.CIF.Contains(filter.CIF) : true) &&
                                (isName ? c.CustomerName.Contains(filter.Name) : true) &&
                                (isStatus ? a.Status.Contains(filter.Status) : true) &&
                                (isCreateBy ? (a.ModifiedDate == null ? a.CreatedBy.Contains(filter.LastModifiedBy) : a.ModifiedBy.Contains(filter.LastModifiedBy)) : true) &&
                                (isCreateDate ? ((a.ModifiedDate == null ? a.CreatedDate : a.ModifiedDate) > filter.LastModifiedDate.Value && ((a.ModifiedDate == null ? a.CreatedDate : a.ModifiedDate.Value) < maxDate)) : true)
                                && a.Status == "Add" && a.WorkflowInstanceID != null
                                select new TBOModel
                                {
                                    TransactionID = b.TransactionID,
                                    TBOTransactionDraftID = a.TBOTransactionDraftID,
                                    ApplicationID = b.ApplicationID,
                                    CIF = c.CIF,
                                    ID = a.TBOTransactionDraftID,
                                    TBOSLAID = a.TBOSLAID.Value,
                                    CustomerName = c.CustomerName,
                                    Status = a.Status,
                                    TBOApplicationID = a.TBOApplicationID,
                                    WorkflowInstanceID = a.WorkflowInstanceID,
                                    CreatedBy = a.CreatedBy,
                                    CreatedDate = a.CreatedDate,
                                    LastModifiedBy = a.ModifiedDate == null ? a.CreatedBy : a.ModifiedBy,
                                    LastModifiedDate = a.ModifiedDate == null ? a.CreatedDate : a.ModifiedDate.Value,
                                });

                    TBO.Page = page;
                    TBO.Size = data.Count();
                    TBO.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    TBO.Rows = data.OrderBy(orderBy).AsEnumerable().Select((a, i) => new TBOModelRow
                    {
                        TransactionID = a.TransactionID,
                        TBOTransactionDraftID = a.TBOTransactionDraftID,
                        ApplicationID = a.ApplicationID,
                        CIF = a.CIF,
                        ID = a.ID,
                        TBOSLAID = a.TBOSLAID,
                        CustomerName = a.CustomerName,
                        Status = a.Status,
                        TBOApplicationID = a.TBOApplicationID,
                        WorkflowInstanceID = a.WorkflowInstanceID,
                        CreatedBy = a.CreatedBy,
                        CreatedDate = a.CreatedDate,
                        LastModifiedBy = a.LastModifiedBy,
                        LastModifiedDate = a.LastModifiedDate,
                    }).Skip(skip)
                    .Take(data.Count()).ToList();
                    TBO.Data = TBO.Rows.GroupBy(a => a.TBOApplicationID).Select(g => g.FirstOrDefault()).ToList();
                    TBO.Total = TBO.Data.Count();
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }


        public bool GetTBOWF(ref IList<TBOModel> TBO, string cif, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    TBO = (from a in context.TBOTransactionDrafts
                           join b in context.Transactions on a.TransactionID equals b.TransactionID
                           join c in context.Customers on b.CIF equals c.CIF
                           join d in context.TransactionDocuments on a.TransactionDocumentID equals d.TransactionDocumentID
                           join e in context.DocumentPurposes on a.DocumentPurposeID equals e.PurposeID
                           join f in context.TBOSLAs on a.TBOSLAID.Value equals f.TBOSLAID
                           where b.CIF == cif && a.Status == "Add"
                           select new TBOModel
                           {
                               TransactionID = b.TransactionID,
                               TBOTransactionDraftID = a.TBOTransactionDraftID,
                               ApplicationID = b.ApplicationID,
                               CIF = b.CIF,
                               CustomerName = c.CustomerName,
                               ID = a.TBOTransactionDraftID,
                               TBOSLAID = a.TBOSLAID.Value,
                               ActDate = a.TBOActualDate,
                               ExptDate = a.TBOSubmissionDate,
                               Status = a.Status,
                               TypeofDoc = new DocumentTypeTBOModel()
                               {
                                   ID = f.TBOSLAID,
                                   Name = f.Description,
                                   Description = f.Description
                               },
                               PurposeofDoc = new DocumentPurposeTBOModel()
                               {
                                   ID = e.PurposeID,
                                   Name = e.PurposeName,
                                   Description = e.PurposeDescription
                               },
                               FileName = new DocumentModelTBO()
                               {
                                   ID = d.TransactionDocumentID,
                                   name = d.Filename,
                                   DocumentPath = d.DocumentPath
                               },
                               CreatedBy = a.CreatedBy,
                               CreatedDate = a.CreatedDate,
                               LastModifiedBy = a.ModifiedBy,
                               LastModifiedDate = a.ModifiedDate,
                               TBOApplicationID = a.TBOApplicationID
                           }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool SaveTBO(TBOModelWF TBO, ref string message)
        {
            bool isSuccess = false;
            long id;
            try
            {
                foreach (var item in TBO.Documents)
                {
                    id = context.Transactions.Where(a => a.ApplicationID == item.ApplicationID).Select(a => a.TransactionID).FirstOrDefault();
                    Entity.TBOTransactionDraft data = context.TBOTransactionDrafts.Where(a => a.TBOApplicationID == item.TBOApplicationID && a.TransactionID == id && a.TBOSLAID == item.TBOSLAID).SingleOrDefault();
                    if (data != null)
                    {
                        if (item.IsChecklistSubmit == true)
                        {
                            data.ModifiedBy = currentUser.GetCurrentUser().DisplayName;
                            data.ModifiedDate = DateTime.UtcNow;
                            data.Status = "Complete";
                        }
                        else
                        {
                            data.ModifiedBy = currentUser.GetCurrentUser().DisplayName;
                            data.ModifiedDate = DateTime.UtcNow;
                            data.Status = "Revise";
                        }
                        context.SaveChanges();
                    }
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetTransactionTimeline(ref IList<TransactionTBOTimelineModel> timelines, Guid workflowInstanceID, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                timelines = (from a in context.SP_GetNintexTaskTimeline(workflowInstanceID)
                             select new TransactionTBOTimelineModel
                             {
                                 ApproverID = a.WorkflowApproverID,
                                 Activity = a.ActivityTitle,
                                 Time = a.ActivityTime,
                                 UserOrGroup = a.UserOrGroup,
                                 IsGroup = a.IsSPGroup.Value,
                                 Outcome = a.Outcome,
                                 DisplayName = a.DisplayName,
                                 Comment = a.Comment
                             }).ToList();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        public bool GetTransactionTBOByCif(ref IList<TBOModel> TBO, string id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    TBO = (from a in context.TBOTransactionDrafts
                           join b in context.Transactions on a.TransactionID equals b.TransactionID
                           join c in context.Customers on b.CIF equals c.CIF
                           where b.CIF == id
                           select new TBOModel
                           {
                               TransactionID = b.TransactionID,
                               TBOTransactionDraftID = a.TBOTransactionDraftID,
                               ApplicationID = b.ApplicationID,
                               CIF = b.CIF,
                               CustomerName = c.CustomerName,
                               ID = a.TBOTransactionDraftID,
                               TBOSLAID = a.TBOSLAID.Value,
                               ActDate = a.TBOActualDate,
                               ExptDate = a.TBOSubmissionDate,
                               Status = a.Status,
                               CreatedBy = a.CreatedBy,
                               CreatedDate = a.CreatedDate,
                               LastModifiedBy = a.ModifiedBy,
                               LastModifiedDate = a.ModifiedDate,
                               TBOApplicationID = a.TBOApplicationID
                           }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


        public bool GetTBO(ref IList<DocumentTypeTBOModel> TBO, ref string message)
        {
            bool isSucces = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    TBO = (from a in context.TBOSLAs
                           select new DocumentTypeTBOModel
                           {
                               ID = a.TBOSLAID,
                               Code = a.TBOCode,
                               Name = a.Description,
                               Days = a.Days
                           }).ToList();
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }


        public bool GetTBOWFID(ref IList<TBOModel> TBO, string spUserLoginName, ref string message)
        {
            bool isSuccess = false;
            var spUser = "i:0#.f|dbsmembership|" + spUserLoginName;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    context.Database.CommandTimeout = 600;
                    var tbo = (from a in context.SP_GetTaskHomeTBO(spUser)
                               select new TBOModel
                               {
                                   TransactionID = a.TransactionID,
                                   ApplicationID = a.ApplicationID,
                                   CIF = a.CIF,
                                   CustomerName = a.CustomerName,
                                   ID = a.TBOTransactionDraftID,
                                   TBOSLAID = a.TBOSLAID,
                                   ActDate = a.TBOActualDate,
                                   ExptDate = a.TBOSubmissionDate,
                                   Status = a.Status,
                                   TypeofDoc = new DocumentTypeTBOModel()
                                   {
                                       ID = a.DocTypeID,
                                       Name = a.DocTypeName,
                                       Description = a.DocTypeDescription
                                   },
                                   PurposeofDoc = new DocumentPurposeTBOModel() 
                                   {
                                       ID = a.PurposeID,
                                       Name = a.PurposeName,
                                       Description = a.PurposeDescription
                                   },
                                   FileName = new DocumentModelTBO()
                                   {
                                       ID = a.TransactionDocumentID,
                                       name = a.Filename,
                                       DocumentPath = a.DocumentPath
                                   },
                                   CreatedBy = a.CreatedBy,
                                   CreatedDate = a.CreatedDate,
                                   LastModifiedBy = a.ModifiedBy,
                                   LastModifiedDate = a.ModifiedDate,
                                   TBOApplicationID = a.TBOApplicationID,
                                   SPTaskListID = a.SPTaskListID,
                                   SPTaskListItemID = a.SPTaskListItemID,
                                   WorkflowInstanceID = a.WorkflowInstanceID
                               }).ToList();

                    if (tbo != null)
                    {
                        TBO = tbo;
                    }
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
    }
    #endregion
}