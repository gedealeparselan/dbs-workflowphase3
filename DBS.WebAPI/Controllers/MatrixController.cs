﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;
using DBS.Entity;


namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class MatrixController : ApiController
    {
        private string message = string.Empty;

        private readonly IMatrixRepository repository = null;

        public MatrixController()
        {
            this.repository = new MatrixRepository();
        }

        public MatrixController(IMatrixRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all Matrix s.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<MatrixModel>))]
        public HttpResponseMessage Get()
        {
            IList<MatrixModel> output = new List<MatrixModel>();

            if (!repository.GetMatrix(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<MatrixModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Matrix Approvals with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<MatrixModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<MatrixModel> output = new List<MatrixModel>();

            if (!repository.GetMatrix(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<MatrixModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Matrix Approvals for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(MatrixGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<MatrixFilter> filters)
        {
            MatrixGrid output = new MatrixGrid();

            if (!repository.GetMatrix(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<MatrixGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find Matrix s using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<MatrixModel>))]
        [HttpGet]
        [Route("api/Matrix/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<MatrixModel> output = new List<MatrixModel>();

            if (!repository.GetMatrix(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<MatrixModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find Matrix s using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<ExceptionHandlingParameter>))]
        [HttpGet]
        [Route("api/Matrix/Parameters")]
        public HttpResponseMessage Search()
        {
            IList<ExceptionHandlingParameter> output = new List<ExceptionHandlingParameter>();
            
            if (!repository.GetParameters(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ExceptionHandlingParameter>>(HttpStatusCode.OK, output);
            }
        }       


        /// <summary>
        /// Get Matrix  by ID.
        /// </summary>
        /// <param name="id">Matrix  ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(MatrixModel))]
        [Route("api/Matrix/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            MatrixModel output = new MatrixModel();

            if (!repository.GetMatrixByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<MatrixModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new Matrix .
        /// </summary>
        /// <param name="Matrix">Matrix  data.</param>
        /// <returns></returns>
        [HttpPost]        
        [ResponseType(typeof(string))]        
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(MatrixModel matrix)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddMatrix(matrix, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {           
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Matrix Approval data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing Matrix Approval.
        /// </summary>
        /// <param name="id">ID of MatrixApproval to be modify.</param>
        /// <param name="MatrixApproval">Matrix Approval data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/Matrix/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, MatrixModel matrix)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateMatrix(id, matrix, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Matrix Approval data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting Matrix Approval.
        /// </summary>
        /// <param name="id">ID of Matrix Approval to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/Matrix/{id}")]
        [Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteMatrix(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "MatrixApproval data has been deleted." });
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}