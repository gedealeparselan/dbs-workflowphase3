﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace DBS.Sharepoint
{
    #region Interfaces

    [ServiceContract]
    public interface IServices
    {
        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Test HelloWorld();

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetCurrentUser();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        TaskApproval TestTask(TaskApproval approval);


        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Result RespondApprovalTask(Task task);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Result RespondReviewTask(Task task);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Result RespondFlexiTask(Task task);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Result RespondFlexiTaskService(Task task);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Result RespondFlexiTaskByUsername(Task task);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultOutcome FlexiTaskOutcomes(Task task);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultOutcome GetTaskOutcomes(Task task);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ResultOutcome GetTaskOutcomesByListID(SPTask spTask);
    }

    #endregion




    #region Properties
    [DataContract]
    public class Test
    {
        [DataMember]
        public string RemoteName { get; set; }
        [DataMember]
        public string LocalName { get; set; }
        [DataMember]
        public Int64 Size { get; set; }
    }

    [DataContract]
    public class Result
    {
        [DataMember(Order = 5)]
        public string Message { get; set; }

        [DataMember(Order = 1)]
        public bool IsSuccess { get; set; }

        [DataMember(Order = 2)]
        public bool IsAuthorized { get; set; }

        [DataMember(Order = 3)]
        public bool IsPending { get; set; }
    }

    [DataContract]
    public class ResultOutcome : Result
    {
        [DataMember(Order = 4)]
        public IList<OutcomeTask> Outcomes { get; set; }
    }

    [DataContract]
    public class OutcomeTask
    {
        [DataMember(Order = 1)]
        public int ID { get; set; }

        [DataMember(Order = 2)]
        public string Name { get; set; }
        
        [DataMember(Order = 3)]
        public string Description { get; set; }
    }

    [DataContract]
    public class Task
    {
        [DataMember(IsRequired = true)]
        public Guid TaskListID { get; set; }

        [DataMember(IsRequired = true)]
        public int TaskListItemID { get; set; }

        [DataMember(IsRequired = true)]
        public int OutcomeID { get; set; }

        [DataMember(IsRequired = true)]
        public string Comment { get; set; }

        [DataMember(IsRequired = false)]
        public string Username { get; set; }
    }


    [DataContract]
    public class TaskService
    {
        [DataMember(IsRequired = true)]
        public Guid TaskListID { get; set; }

        [DataMember(IsRequired = true)]
        public int TaskListItemID { get; set; }

        [DataMember(IsRequired = true)]
        public int OutcomeID { get; set; }

        [DataMember(IsRequired = true)]
        public string Comment { get; set; }

        [DataMember(IsRequired = true)]
        public string urlforserviceresponse { get; set; }
    }

    [DataContract]
    public class SPTask
    {
        [DataMember(IsRequired = true)]
        public Guid TaskListID { get; set; }

        [DataMember(IsRequired = true)]
        public int TaskListItemID { get; set; }
    }

    [DataContract]
    public sealed class TaskApproval : Task
    {
        [DataMember(IsRequired = true)]
        public bool IsApprove { get; set; }

        [DataMember(IsRequired = true)]
        public string Comment { get; set; }
    }

    [DataContract]
    public class TaskReview : Task
    {
        [DataMember(IsRequired = true)]
        public string Comment { get; set; }
    }

    [DataContract]
    public class TaskFlexi : Task
    {
        [DataMember(IsRequired = true)]
        public int OutcomeID { get; set; }

        [DataMember(IsRequired = true)]
        public string Comment { get; set; }
    }

    #endregion
}
