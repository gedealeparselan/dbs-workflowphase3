﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class TransactionSubTypeController : ApiController
    {
        private string message = string.Empty;

        private readonly ITransactionSubTypeRepository repository = null;

        public TransactionSubTypeController()
        {
            this.repository = new TransactionSubTypeRepository();
        }

        public TransactionSubTypeController(ITransactionSubTypeRepository repository)
        {
            this.repository = repository;
        }
       
        [ResponseType(typeof(IList<TransactionSubTypeModel>))]
        public HttpResponseMessage Get()
        {
            IList<TransactionSubTypeModel> output = new List<TransactionSubTypeModel>();
            if (!repository.GetTransactionSubType(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TransactionSubTypeModel>>(HttpStatusCode.OK, output);
            }
        }
       
        [ResponseType(typeof(IList<TransactionSubTypeModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<TransactionSubTypeModel> output = new List<TransactionSubTypeModel>();
            if (!repository.GetTransactionSubType(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<TransactionSubTypeModel>>(HttpStatusCode.OK, output);
            }
        }
       
        [ResponseType(typeof(TransactionSubTypeGrid))]
        [AcceptVerbs("GET", "POST")]

        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<TransactionSubTypeFilter> filters)
        {
            TransactionSubTypeGrid output = new TransactionSubTypeGrid();

            if (!repository.GetTransactionSubType(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<TransactionSubTypeGrid>(HttpStatusCode.OK, output);

            }
        }
        

        [ResponseType(typeof(IList<TransactionSubTypeModel>))]
        [HttpGet]
        [Route("api/TransactionSubType/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<TransactionSubTypeModel> output = new List<TransactionSubTypeModel>();

            if (!repository.GetTransactionSubType(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TransactionSubTypeModel>>(HttpStatusCode.OK, output);
            }
        }
        
        [ResponseType(typeof(TransactionSubTypeModel))]
        [Route("api/TransactionSubType/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            TransactionSubTypeModel output = new TransactionSubTypeModel();
            if (!repository.GetTransactionSubTypeByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<TransactionSubTypeModel>(HttpStatusCode.OK, output);
            }
        }
       
        [HttpPost]
        [ResponseType(typeof(string))]
        
        public HttpResponseMessage Post(TransactionSubTypeModel transactionsubtype)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionSubType(transactionsubtype, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Transaction sub type Data Has Been Created." });
                }

            }
        }
        
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/TransactionSubType/{id}")]

        public HttpResponseMessage put(int id, TransactionSubTypeModel transactionsubtype)
        {
            if(!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTransactionSubType(id, transactionsubtype, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction Sub Type data has been updated" });
                }
            }
        }
        
       [HttpDelete]
       [Route("api/TransactionSubType/{id}")] 
       [ResponseType(typeof(string))]

       public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteTransactionSubType(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction Sub Type Data Has Been Deleted" });
                }
            }
        }

       protected override void Dispose(bool disposing)
       {
           if (repository != null)
               repository.Dispose();
           base.Dispose(disposing);
       }
    }
}