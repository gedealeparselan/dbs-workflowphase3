﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("Channel")]
    public class ChannelModel
    {
        /// <summary>
        /// Channel ID.
        /// </summary>
        public int ID { get; set; }


        /// <summary>
        /// Channel Name.
        /// </summary>
        //[MaxLength(50, ErrorMessage = "Maximum length is {1} characters.")]
        public string Name { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class ChannelRow : ChannelModel
    {
        public int RowID { get; set; }

    }

    public class ChannelGrid : Grid
    {
        public IList<ChannelRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class ChannelFilter : Filter { }
    #endregion

    #region Interface
    public interface IChannelRepository : IDisposable
    {
        bool GetChannelByID(int id, ref ChannelModel channel, ref string message);
        bool GetChannel(ref IList<ChannelModel> channels, int limit, int index, ref string message);
        bool GetChannel(ref IList<ChannelModel> channels, ref string message);
        bool GetChannel(int page, int size, IList<ChannelFilter> filters, string sortColumn, string sortOrder, ref ChannelGrid Channel, ref string message);
        bool GetChannel(ChannelFilter filter, ref IList<ChannelModel> channels, ref string message);
        bool GetChannel(string key, int limit, ref IList<ChannelModel> channels, ref string message);
        bool AddChannel(ChannelModel channel, ref string message);
        bool UpdateChannel(int id, ChannelModel channel, ref string message);
        bool DeleteChannel(int id, ref string message);
    }
    #endregion

    #region Repository
    public class ChannelRepository : IChannelRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetChannelByID(int id, ref ChannelModel channel, ref string message)
        {
            bool isSuccess = false;

            try
            {
                channel = (from a in context.Channels
                           where a.IsDeleted.Equals(false) && a.ChannelID.Equals(id)
                           select new ChannelModel
                           {
                               ID = a.ChannelID,
                               Name = a.ChannelName,
                               LastModifiedDate = a.CreateDate,
                               LastModifiedBy = a.CreateBy
                           }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetChannel(ref IList<ChannelModel> channels, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    channels = (from a in context.Channels
                                where a.IsDeleted.Equals(false)
                                orderby new { a.ChannelName }
                                select new ChannelModel
                                {
                                    ID = a.ChannelID,
                                    Name = a.ChannelName,
                                    LastModifiedBy = a.CreateBy,
                                    LastModifiedDate = a.CreateDate
                                }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetChannel(ref IList<ChannelModel> channels, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    channels = (from a in context.Channels
                                where a.IsDeleted.Equals(false)
                                orderby new { a.ChannelName }
                                select new ChannelModel
                                {
                                    ID = a.ChannelID,
                                    Name = a.ChannelName,
                                    LastModifiedBy = a.CreateBy,
                                    LastModifiedDate = a.CreateDate
                                }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetChannel(int page, int size, IList<ChannelFilter> filters, string sortColumn, string sortOrder, ref ChannelGrid customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                ChannelModel filter = new ChannelModel();
                if (filters != null)
                {
                    filter.Name = filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.Channels
                                let isName = !string.IsNullOrEmpty(filter.Name)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isName ? a.ChannelName.Contains(filter.Name) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new ChannelModel
                                {
                                    ID = a.ChannelID,
                                    Name = a.ChannelName,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    customer.Page = page;
                    customer.Size = size;
                    customer.Total = data.Count();
                    customer.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    customer.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new ChannelRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Name = a.Name,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetChannel(ChannelFilter filter, ref IList<ChannelModel> channels, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetChannel(string key, int limit, ref IList<ChannelModel> channels, ref string message)
        {
            bool isSuccess = false;

            try
            {
                channels = (from a in context.Channels
                            where a.IsDeleted.Equals(false) &&
                                a.ChannelName.Contains(key)
                            orderby new { a.ChannelName }
                            select new ChannelModel
                            {
                                Name = a.ChannelName,

                            }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddChannel(ChannelModel channel, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.Channels.Where(a => a.ChannelName.Equals(channel.Name) && a.IsDeleted.Equals(false)).Any())
                {
                    context.Channels.Add(new Entity.Channel()
                    {
                        ChannelName = channel.Name,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Channel data for channel name {0} is already exist.", channel.Name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateChannel(int id, ChannelModel channel, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Channel data = context.Channels.Where(a => a.ChannelID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.Channels.Where(a => a.ChannelID != channel.ID && a.ChannelName.Equals(channel.Name) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("channel name {0} is exist.", channel.Name);
                    }
                    else
                    {
                        data.ChannelName = channel.Name;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Channel data for Channel ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteChannel(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Channel data = context.Channels.Where(a => a.ChannelID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Channel data for Channel ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}