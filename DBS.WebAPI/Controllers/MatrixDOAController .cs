﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class MatrixDOAController : ApiController
    {
        private string message = string.Empty;

        private readonly IMatrixDOARepository repository = null;

        public MatrixDOAController()
        {
            this.repository = new MatrixDOARepository();
        }

        public MatrixDOAController(IMatrixDOARepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all MatrixDOAs.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<MatrixDOAModel>))]
        public HttpResponseMessage Get()
        {
            IList<MatrixDOAModel> output = new List<MatrixDOAModel>();

            if (!repository.GetMatrixDOA(ref output,ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<MatrixDOAModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get MatrixDOA Approvals with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<MatrixDOAModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<MatrixDOAModel> output = new List<MatrixDOAModel>();

            if (!repository.GetMatrixDOA(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<MatrixDOAModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get MatrixDOA Approvals for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(MatrixDOAGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<MatrixDOAFilter> filters)
        {
            MatrixDOAGrid output = new MatrixDOAGrid();

            if (!repository.GetMatrixDOA(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<MatrixDOAGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find MatrixDOA s using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<MatrixDOAModel>))]
        [HttpGet]
        [Route("api/MatrixDOA/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<MatrixDOAModel> output = new List<MatrixDOAModel>();

            if (!repository.GetMatrixDOA(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<MatrixDOAModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get MatrixDOA  by ID.
        /// </summary>
        /// <param name="id">MatrixDOA  ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(MatrixDOAModel))]
        [Route("api/MatrixDOA/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            MatrixDOAModel output = new MatrixDOAModel();

            if (!repository.GetMatrixDOAByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<MatrixDOAModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new MatrixDOA .
        /// </summary>
        /// <param name="MatrixDOA">MatrixDOA  data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(MatrixDOAModel MatrixDOA)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddMatrixDOA(MatrixDOA, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Exception Handling data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing MatrixDOA Approval.
        /// </summary>
        /// <param name="id">ID of MatrixDOAApproval to be modify.</param>
        /// <param name="MatrixDOAApproval">MatrixDOA Approval data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/MatrixDOA/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, MatrixDOAModel MatrixDOA)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateMatrixDOA(id, MatrixDOA,ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Exception Handling data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting MatrixDOA Approval.
        /// </summary>
        /// <param name="id">ID of MatrixDOA Approval to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/MatrixDOA/{id}")]
        [Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteMatrixDOA(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Exception Handling  data has been deleted." });
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}