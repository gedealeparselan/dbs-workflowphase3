﻿using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DBS.WebAPI.Models
{
    #region property
    [ModelName("Holiday")]
    public class HolidayModel
    {
        public int HolidayID { get; set; }
        public DateTime? HolidayDate { get; set; }
        public string HolidayName { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }     

    }
    public class HolidayRow : HolidayModel
    {
        public int RowID { get; set; }
    }

    public class HolidayGrid : Grid
    {
        public IList<HolidayRow> Rows { get; set; }
    }
     #endregion

    #region filter
    public class HolidayFilter : Filter
    {

    }
    #endregion

    #region interface
    public interface IHolidayRepository : IDisposable
    {
        bool GetHolidayByID(int id, ref HolidayModel Holiday, ref string message);
        bool GetHoliday(ref IList<HolidayModel> Holidays, int limit, int index, ref string message);
        bool GetHoliday(ref IList<HolidayModel> Holidays, ref string message);
        bool GetHoliday(int page, int size, IList<HolidayFilter> filters, string sortColumn, string sortOrder, ref HolidayGrid Nostro, ref string message);
        bool GetHoliday(HolidayFilter filter, ref IList<HolidayModel> holidays, ref string message);
        bool GetHoliday(string key, int limit, ref IList<HolidayModel> holidays, ref string message);
        bool AddHoliday(HolidayModel Holiday, ref string message);
        bool UpdateHoliday(int id, HolidayModel Holiday, ref string message);
        bool DeleteHoliday(int Id, ref string Message);
    }
    #endregion

    #region repository
    public class HolidayRepository : IHolidayRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetHolidayByID(int id, ref HolidayModel Holiday, ref string message)
        {
            bool isSuccess = false;
            try
            {
                Holiday = (from holiday in context.Holidays
                          where holiday.IsDeleted.Equals(false) 
                          select new HolidayModel
                          {
                              HolidayID=holiday.HolidayID,
                              HolidayDate=holiday.HolidayDate,
                              HolidayName=holiday.HolidayName,
                              Description=holiday.Description,
                              LastModifiedDate = holiday.CreateDate,
                              LastModifiedBy = holiday.CreateBy
                          }).SingleOrDefault();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetHoliday(ref IList<HolidayModel> Holidays, int limit, int index, ref string message)
        {
            bool isSuccess = false;
            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    Holidays = (from holiday in context.Holidays
                                where holiday.IsDeleted.Equals(false)
                                orderby new { holiday.HolidayName }
                               select new HolidayModel
                               {
                                   HolidayID = holiday.HolidayID,
                                   HolidayDate = holiday.HolidayDate,
                                   HolidayName = holiday.HolidayName,
                                   Description = holiday.Description,
                                   LastModifiedDate = holiday.CreateDate,
                                   LastModifiedBy = holiday.CreateBy
                               }).Skip(skip).Take(limit).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetHoliday(ref IList<HolidayModel> Holidays, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    Holidays = (from holiday in context.Holidays
                                where holiday.IsDeleted.Equals(false)
                                select new HolidayModel
                                {
                                    HolidayID = holiday.HolidayID,
                                    HolidayDate = holiday.HolidayDate,
                                    HolidayName = holiday.HolidayName,
                                    Description = holiday.Description,
                                    LastModifiedDate = holiday.CreateDate,
                                    LastModifiedBy = holiday.CreateBy
                                }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetHoliday(int page, int size, IList<HolidayFilter> filters, string sortColumn, string sortOrder, ref HolidayGrid Holiday, ref string message)
        {
            bool isSuccess = false;
            try
            {
                HolidayModel filter = new HolidayModel();
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();


                if (filters != null)
                {
                   
                    //filter.HolidayDate = DateTime.Parse(filters.Where(a => a.Field.Equals("HolidayDate")).Select(a => a.Value).SingleOrDefault());
                    if (filters.Where(a => a.Field.Equals("HolidayDate")).Any())
                    {
                        filter.HolidayDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("HolidayDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.HolidayDate.Value.AddDays(1);
                    }
                    filter.HolidayName = filters.Where(a => a.Field.Equals("HolidayName")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from holiday in context.Holidays
                                let isHolidayDate = filter.HolidayDate.HasValue ? true : false
                                let isHolidayName = !string.IsNullOrEmpty(filter.HolidayName)
                                let isDescription = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where holiday.IsDeleted==false
                                && (isHolidayDate ? (holiday.HolidayDate == filter.HolidayDate.Value) : true)//((holiday.HolidayDate==null?holiday.HolidayDate:holiday.HolidayDate)>filter.HolidayDate.Value)
                                && (isHolidayName ? holiday.HolidayName.Contains(filter.HolidayName) : true)
                                && (isDescription ? holiday.Description.Contains(filter.Description) : true)
                               // && (isHolidayDate ? ((holiday.HolidayDate==null?holiday.HolidayDate:holiday.HolidayDate)>filter.HolidayDate.Value)
                                && (isCreateBy ? (holiday.UpdateDate == null ? holiday.CreateBy.Contains(filter.LastModifiedBy) : holiday.UpdateBy.Contains(filter.LastModifiedBy)) : true)
                                && (isCreateDate ? ((holiday.UpdateDate == null ? holiday.CreateDate : holiday.UpdateDate) > filter.LastModifiedDate.Value && ((holiday.UpdateDate == null ? holiday.CreateDate : holiday.UpdateDate.Value) < maxDate)) : true)
                                select new HolidayModel
                                {
                                    HolidayID = holiday.HolidayID,
                                    HolidayDate = holiday.HolidayDate,
                                    HolidayName = holiday.HolidayName,
                                    Description = holiday.Description,
                                    LastModifiedBy = holiday.UpdateDate == null ? holiday.CreateBy : holiday.UpdateBy,
                                    LastModifiedDate = holiday.UpdateDate == null ? holiday.CreateDate : holiday.UpdateDate.Value
                                });

                    Holiday.Page = page;
                    Holiday.Size = size;
                    Holiday.Total = data.Count();
                    Holiday.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    Holiday.Rows = data.OrderBy(orderBy).AsEnumerable()
                    .Select((a, i) => new HolidayRow
                    {
                        RowID = i + 1,
                        HolidayID = a.HolidayID,
                        HolidayDate = a.HolidayDate,
                        HolidayName = a.HolidayName,
                        Description = a.Description,
                        LastModifiedBy = a.LastModifiedBy,
                        LastModifiedDate = a.LastModifiedDate
                    })
                    .Skip(skip)
                    .Take(size)
                    .ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetHoliday(HolidayFilter filter, ref IList<HolidayModel> holidays, ref string message)
        {
            bool isSuccess = false;
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetHoliday(string key, int limit, ref IList<HolidayModel> holidays, ref string message)
        {
            bool isSuccess = false;
            try
            {
                holidays = (from holiday in context.Holidays
                            where holiday.IsDeleted.Equals(false)
                            orderby new { holiday.HolidayName }
                           select new HolidayModel
                           {
                               HolidayID = holiday.HolidayID,
                               HolidayDate = holiday.HolidayDate,
                               HolidayName = holiday.HolidayName,
                               Description = holiday.Description,
                               LastModifiedDate = holiday.CreateDate,
                               LastModifiedBy = holiday.CreateBy
                           }).Take(limit).ToList();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool AddHoliday(HolidayModel Holiday, ref string message)
        {
            bool isSuccess = false;
            if (Holiday != null)
            {
                try
                {
                    if (!context.Holidays.Where(a => a.HolidayID == Holiday.HolidayID && a.IsDeleted==false).Any())
                    {
                        context.Holidays.Add(new Entity.Holiday()
                        {
                            HolidayName=Holiday.HolidayName,
                            HolidayDate=Convert.ToDateTime(Holiday.HolidayDate),
                            Description=Holiday.Description,
                            IsDeleted = false,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().DisplayName
                        });

                        context.SaveChanges();

                        isSuccess = true;
                    }
                    else
                    {
                        message = string.Format("Holiday data is already exist.", Holiday.HolidayID);
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }

            return isSuccess;
        }
        public bool UpdateHoliday(int id, HolidayModel Holiday, ref string message)
        {
            bool isSuccess = false;
            try
            {
                Entity.Holiday data = context.Holidays.Where(a => a.HolidayID.Equals(id)).SingleOrDefault();

                if (data != null)
                {

                        data.HolidayName = Holiday.HolidayName;
                        data.HolidayDate = Convert.ToDateTime(Holiday.HolidayDate);
                        data.Description = Holiday.Description;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        context.SaveChanges();
                        isSuccess = true;
                   
                }
                else
                {
                    message = string.Format("Holiday data  is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool DeleteHoliday(int id, ref string message)
        {
            bool isSuccess = false;
            try
            {
                Entity.Holiday data = context.Holidays.Where(a => a.HolidayID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Holiday data is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
    #endregion

}