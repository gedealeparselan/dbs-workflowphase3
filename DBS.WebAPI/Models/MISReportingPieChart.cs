﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Models;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.Entity.SqlServer;

namespace DBS.WebAPI.Models
{
    [DataContract]
    [ModelName("MISReportingPieChart")]
    public class MISReportingPieChartModel
    {
        /// <summary>
        /// Count Breach Transaction
        /// </summary>
        public int breachTransactions { get; set; }

        /// <summary>
        /// Count SLA Transactions
        /// </summary>
        public int slaTransactions { get; set; }

        /// <summary>
        /// Count Exceptional Transactions
        /// </summary>
        public int exceptionalTransactions { get; set; }

        /// <summary>
        /// Count Cancelled Transactions
        /// </summary>
        public int cancelledTransactions { get; set; }


        /// <summary>
        /// Created Date Transactions
        /// </summary>
        public DateTime createdDate { get; set; }

        /// <summary>
        /// TAT Time
        /// </summary>
        public int TATTime { get; set; }

        /// <summary>
        /// Date Time Today
        /// </summary>
        public DateTime currentDate { get; set; }

        /// <summary>
        /// String for buffering SLA Transactions
        /// </summary>
        [DataMember(Name = "breachTransactions")]
        public string strBreachTransactions { get; set; }

        /// <summary>
        /// String for buffering SLA Transactions
        /// </summary>
        [DataMember(Name = "slaTransactions")]
        public string strSLATransactions { get; set; }

        /// <summary>
        /// String for buffering Exceptional Transactions
        /// </summary>
        [DataMember(Name = "exceptionalTransactions")]
        public string strExceptionalTransactions { get; set; }

        /// <summary>
        /// String for buffering Cancelled Transactions
        /// </summary>
        [DataMember(Name = "cancelledTransactions")]
        public string strCancelledTransactions { get; set; }

        /// <summary>
        /// String for buffering Created Date
        /// </summary>
        [DataMember(Name = "createdDate")]
        public string strCreatedDate { get; set; }

        /// <summary>
        /// String for buffering  TAT Time
        /// </summary>
        [DataMember(Name = "TATTime")]
        public string strTATTime { get; set; }

        [OnSerializing]
        void OnSerializing(StreamingContext context)
        {
            this.strBreachTransactions = this.breachTransactions.ToString();
            this.strSLATransactions = this.slaTransactions.ToString();
            this.strExceptionalTransactions = this.exceptionalTransactions.ToString();
            this.strCancelledTransactions = this.cancelledTransactions.ToString();
            this.strCreatedDate = this.createdDate.ToString();
            this.strTATTime = this.TATTime.ToString();
        }

    }

    public interface IMISReportingPieChart : IDisposable
    {
        bool GetMISReportingPieChart(int transactionType, int segmentation, int channel, int branch, DateTime startDate, DateTime endDate, DateTime clientDateTime, ref IList<MISReportingPieChartModel> resultOutput, ref string message);
    }


    public class MISReportingPieChartRepository : IMISReportingPieChart
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();


        public bool GetMISReportingPieChart(int transactionType, int segmentation, int channel, int branch, DateTime startDate, DateTime endDate, DateTime clientDateTime, ref IList<MISReportingPieChartModel> resultOutput, ref string message)
        {
            bool isSuccess = false;
            DateTime serverUTC = DateTime.UtcNow;
            int hourDiff, oneDayHour = 24;
            if (clientDateTime > serverUTC)
            {
                hourDiff = clientDateTime.Hour - serverUTC.Hour;
            }
            else if (clientDateTime < serverUTC)
            {
                hourDiff = oneDayHour + clientDateTime.Hour - serverUTC.Hour;
            }
            else
            {
                hourDiff = 0;
            }
            try
            {
                using (DBSEntities context = new DBSEntities())
                {

                    var ProductCount = (from a in context.Products select new { a.ProductID }).Count();
                    var BizSegmentCount = (from a in context.BizSegments select new { a.BizSegmentID }).Count();
                    var ChannelCount = (from a in context.Channels select new { a.ChannelID }).Count();
                    var BranchCount = (from a in context.Locations select new { a.LocationID }).Count();

                    bool isAllProduct = false; bool isAllSegment = false;
                    bool isAllChannel = false; bool isAllBranch = false;

                    bool isStartDateNull = false; bool isEndDateNull = false;

                    if (transactionType == ProductCount + 1)
                    {
                        isAllProduct = true;
                    }
                    if (segmentation == BizSegmentCount + 1)
                    {
                        isAllSegment = true;
                    }
                    if (channel == ChannelCount + 1)
                    {
                        isAllChannel = true;
                    }
                    if (branch == BranchCount + 1)
                    {
                        isAllBranch = true;
                    }
                    if (startDate == null)
                    {
                        isStartDateNull = true;
                    }
                    if (endDate == null)
                    {
                        isEndDateNull = true;
                    }

                    if ((startDate != null) && (endDate != null))
                    {

                        resultOutput = (from a in context.SP_GetMISReportingPieChart(transactionType, segmentation, channel, branch, startDate, endDate, isAllProduct, isAllSegment, isAllChannel, isAllBranch)                                      
                                        select new
                                        {
                                            transactionsID = a.TransactionID.Value,
                                            createdDate = a.CreateDate.Value,
                                            updateDate = a.WorkflowTaskExitTime.Value,
                                            slaTime = a.SLATime != null ? a.SLATime.Value : 0,
                                            TATTime = a.TATTime != null ? a.TATTime.Value : 0,
                                            isExceptionHandling = a.IsExceptionHandling != null ? a.IsExceptionHandling.Value : false,
                                            stateID = a.StateID != null ? a.StateID.Value : 0
                                        }
                                            into aa
                                            group aa by new
                                            {
                                                TransactionID = aa.transactionsID,
                                                CreatedDate = aa.createdDate,
                                                SLATime = aa.slaTime,
                                                TATTIme = aa.TATTime,
                                                IsExceptionHandling = aa.isExceptionHandling,
                                                StateID = aa.stateID
                                            } into bb
                                            select new MISReportingPieChartModel
                                            {

                                                breachTransactions = (from bbb in bb
                                                                      let TATTime = bbb.TATTime
                                                                      where ((TATTime > (bbb.slaTime * 3600)) && (TATTime >= 0))
                                                                              && (bbb.isExceptionHandling.Equals(false) || bbb.isExceptionHandling == null)
                                                                      select bbb).Count(),

                                                slaTransactions = (from bbb in bb
                                                                   let TATTime = bbb.TATTime
                                                                   where ((TATTime <= (bbb.slaTime * 3600)) && (TATTime >= 0))
                                                                               && (bbb.isExceptionHandling.Equals(false) || bbb.isExceptionHandling == null)
                                                                   select bbb).Count(),

                                                exceptionalTransactions = (from bbb in bb
                                                                           where (bbb.isExceptionHandling.Equals(true))
                                                                           select bbb).Count(),

                                                cancelledTransactions = (from bbb in bb
                                                                         where (bbb.stateID == 3)
                                                                         select bbb).Count(),

                                                createdDate = (from bbb in bb select bbb.createdDate).FirstOrDefault(),

                                                TATTime = (from bbb in bb where (bbb.stateID == 2) && (bbb.isExceptionHandling.Equals(false) || bbb.isExceptionHandling == null) select bbb.TATTime).FirstOrDefault()

                                            }).ToList();
                    }

                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;

        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}