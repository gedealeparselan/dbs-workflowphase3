﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Data.Entity.SqlServer;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("TransactionType")]
    public class TransactionTypeModel
    {
        public int ID { get; set; }
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public string Name { get; set; }
        public string ParameterTypeDisplay { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }
    public class TransactionTypeParameterModel
    {
        public int TransTypeID { get; set; }
        public int ProductID { get; set; }
        public string TransactionTypeName { get; set; }
    }
    public class TransactionTypeRow : TransactionTypeModel
    {
        public int RowID { get; set; }
    }
    public class TransactionTypeGrid : Grid
    {
        public IList<TransactionTypeRow> Rows { get; set; }
    }
    #endregion
    #region Filter
    public class TransactionTypeFilter : Filter { }
    #endregion
    #region Interface
    public interface ITransactionTypeRepository : IDisposable
    {
        bool GetTransactionTypeByID(int id, ref TransactionTypeModel TransactionType, ref string message);
        bool GetTransactionType(ref IList<TransactionTypeModel> transactionTypes, int limit, int index, ref string message);
        bool GetTransactionType(ref IList<TransactionTypeModel> transactionTypes, ref string message);
        bool GetTransactionType(int page, int size, IList<TransactionTypeFilter> filters, string sortColumn, string sortOrder, ref TransactionTypeGrid transactionTypeGrid, ref string message);
        bool GetTransactionType(TransactionTypeFilter filter, ref IList<TransactionTypeModel> transactionTypes, ref string message);
        bool GetTransactionType(string key, int limit, ref IList<TransactionTypeModel> transactionTypes, ref string message);
        bool AddTransactionType(TransactionTypeModel transactiontype, ref string message);
        bool UpdateTransactionType(int id, TransactionTypeModel transactionType, ref string message);
        bool DeleteTransactionType(int id, ref string message);
        bool GetTransactionTypeByProductID(int id, ref IList<TransactionTypeParameterModel> TransactionType, ref string message);
    }
    #endregion
    #region Repository
    public class TransactionTypeRepository : ITransactionTypeRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetTransactionTypeByID(int id, ref TransactionTypeModel TransactionType, ref string message)
        {
            bool isSuccess = false;
            try
            {
                TransactionType = (from a in context.TransactionTypes
                                   join b in context.Products on a.ProductID equals b.ProductID
                                   where a.IsDeleted.Equals(false) && a.TransTypeID.Equals(id)
                                   select new TransactionTypeModel
                                   {
                                       ID = a.TransTypeID,
                                       ProductID = a.ProductID,
                                       ProductName = b.ProductName,
                                       Name = a.TransactionType1,
                                       UpdateBy = a.UpdateBy,
                                       UpdateDate = a.UpdateDate,
                                       CreateBy = a.CreateBy,
                                       CreateDate = a.CreateDate
                                   }).SingleOrDefault();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTransactionType(ref IList<TransactionTypeModel> transactionTypes, int limit, int index, ref string message)
        {
            bool isSuccess = false;
            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    transactionTypes = (from a in context.TransactionTypes
                                        join b in context.Products on a.ProductID equals b.ProductID
                                        where a.IsDeleted.Equals(false)
                                        orderby new { a.TransTypeID, a.TransactionType1, a.ProductID, a.CreateDate, a.CreateBy, a.UpdateBy, a.UpdateDate }
                                        select new TransactionTypeModel
                                        {
                                            ID = a.TransTypeID,
                                            ProductName = b.ProductName,
                                            ProductID = a.ProductID,
                                            Name = a.TransactionType1,
                                            UpdateBy = a.UpdateBy,
                                            UpdateDate = a.UpdateDate,
                                            CreateBy = a.CreateBy,
                                            CreateDate = a.CreateDate
                                        }).Skip(skip).Take(limit).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTransactionType(ref IList<TransactionTypeModel> transactionTypes, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    transactionTypes = (from a in context.TransactionTypes
                                        join b in context.Products on a.ProductID equals b.ProductID
                                        where a.IsDeleted.Equals(false)
                                        orderby new { a.TransactionType1, a.TransTypeID }
                                        select new TransactionTypeModel
                                        {
                                            ID = a.TransTypeID,
                                            ProductID = a.ProductID,
                                            ProductName = b.ProductName,
                                            Name = a.TransactionType1,
                                            UpdateBy = a.UpdateBy,
                                            UpdateDate = a.UpdateDate,
                                            CreateBy = a.CreateBy,
                                            CreateDate = a.CreateDate
                                        }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTransactionType(int page, int size, IList<TransactionTypeFilter> filters, string sortColumn, string sortOrder, ref TransactionTypeGrid transactionTypeGrid, ref string message)
        {
            bool isSuccess = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();
                TransactionTypeModel filter = new TransactionTypeModel();

                if (filters != null)
                {
                    filter.ID = Convert.ToInt32((string)filters.Where(a => a.Field.Equals("ID")).Select(a => a.Value == null ? "0" : a.Value).SingleOrDefault());
                    filter.Name = (string)filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.ProductID = Convert.ToInt32((string)filters.Where(a => a.Field.Equals("ProductID")).Select(a => a.Value == null ? "0":a.Value).SingleOrDefault());
                    filter.ProductName = (string)filters.Where(a => a.Field.Equals("ProductName")).Select(a => a.Value).SingleOrDefault();
                    filter.CreateBy = ((string)filters.Where(a => a.Field.Equals("CreateBy")).Select(a => a.Value).SingleOrDefault());
                    //filter.CreateDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("CreateDate")).Select(a => a.Value).SingleOrDefault());
                    filter.UpdateBy = ((string)filters.Where(a => a.Field.Equals("UpdateBy")).Select(a => a.Value).SingleOrDefault());


                    if (filters.Where(a => a.Field.Equals("UpdateDate")).Any())
                    {
                        filter.UpdateDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("UpdateDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.UpdateDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.TransactionTypes
                                join b in context.Products on a.ProductID equals b.ProductID
                                let isProductName =!string.IsNullOrEmpty(filter.ProductName)
                                let isTransactionType = !string.IsNullOrEmpty(filter.Name)
                                //let isCreateBy = !string.IsNullOrEmpty(filter.CreateBy)
                                let isUpdateDate = filter.UpdateDate.HasValue ? true : false
                                let isUpdateBy = !string.IsNullOrEmpty(filter.UpdateBy)
                                where a.IsDeleted.Equals(false) &&
                                (isProductName ? b.ProductName.Contains(filter.ProductName) : true)&&
                                (isTransactionType ? a.TransactionType1.Contains(filter.Name) : true)&&
                                (isUpdateBy ? (a.UpdateBy == null ? a.CreateBy.Contains(filter.UpdateBy) : a.UpdateBy.Contains(filter.UpdateBy)) : true)&&
                                (isUpdateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.UpdateDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new TransactionTypeModel
                                {
                                    ID = a.TransTypeID,
                                    ProductID = a.ProductID,
                                    ProductName = b.ProductName,
                                    Name = a.TransactionType1,
                                    LastModifiedBy=a.UpdateBy==null?a.CreateBy:a.UpdateBy,
                                    LastModifiedDate=a.UpdateDate==null?a.CreateDate:a.UpdateDate.Value

                                });
                    transactionTypeGrid.Page = page;
                    transactionTypeGrid.Size = size;
                    transactionTypeGrid.Total = data.Count();
                    transactionTypeGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    transactionTypeGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new TransactionTypeRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            ProductID = a.ProductID,
                            ProductName = a.ProductName,
                            Name = a.Name,
                            LastModifiedBy =a.LastModifiedBy,
                            LastModifiedDate=a.LastModifiedDate
                            //UpdateBy = a.UpdateBy,
                            //UpdateDate = a.UpdateDate,
                            //CreateBy = a.CreateBy,
                            //CreateDate = a.CreateDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTransactionType(TransactionTypeFilter filter, ref IList<TransactionTypeModel> transactionTypes, ref string message)
        {
            throw new NotImplementedException();
        }
        public bool GetTransactionType(string key, int limit, ref IList<TransactionTypeModel> transactionTypes, ref string message)
        {
            bool isSuccess = false;
            try
            {
                transactionTypes = (from a in context.TransactionTypes
                                    join b in context.Products on a.ProductID equals b.ProductID
                                    where (a.TransactionType1.Contains(key) && a.IsDeleted.Equals(false))
                                    orderby new { a.ProductID, a.TransTypeID }
                                    select new TransactionTypeModel
                                    {
                                        ID = a.TransTypeID,
                                        ProductID = a.ProductID,
                                        ProductName = b.ProductName,
                                        Name = a.TransactionType1,
                                        UpdateBy = a.UpdateBy,
                                        UpdateDate = a.UpdateDate,
                                        CreateBy = a.CreateBy,
                                        CreateDate = a.CreateDate
                                    }).Take(limit).ToList();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool AddTransactionType(TransactionTypeModel transactiontype, ref string message)
        {
            bool isSuccess = false;
            if(transactiontype !=null)
            {
                try
                {
                    if (!context.TransactionTypes.Where(a => a.TransTypeID.Equals(transactiontype.ID) && a.IsDeleted.Equals(false)).Any())
                    {
                        Entity.TransactionType data = new Entity.TransactionType()
                        {
                            TransTypeID = transactiontype.ID,
                            ProductID = transactiontype.ProductID,
                            //ProductName = transactiontype.ProductName,
                            TransactionType1 = transactiontype.Name,
                            UpdateBy = transactiontype.UpdateBy,
                            UpdateDate = transactiontype.UpdateDate,
                            CreateBy = currentUser.GetCurrentUser().DisplayName,
                            CreateDate = DateTime.UtcNow
                        };
                        context.TransactionTypes.Add(data);
                        context.SaveChanges();

                        isSuccess = true;
                    }
                    else
                    {
                        message = string.Format("TransactionType Data for TransactionType Code {0} is already exist.", transactiontype.ID);

                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return isSuccess;
        }
        public bool UpdateTransactionType(int id, TransactionTypeModel transactionType, ref string message)
        {
            bool isSuccess = false;
            try
            {
                Entity.TransactionType data = context.TransactionTypes.Where(a => a.TransTypeID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.TransTypeID = transactionType.ID;
                    data.TransactionType1 = transactionType.Name;
                    data.ProductID = transactionType.ProductID;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                    data.UpdateDate = DateTime.UtcNow;
                    context.SaveChanges();
                    isSuccess = true;

                }
                else
                {
                    message = string.Format("TransactionType data for transactionType ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;

            }
            return isSuccess;
        }
        public bool DeleteTransactionType(int id, ref string message)
        {
            bool isSuccess = false;
            try
            {
                Entity.TransactionType data = context.TransactionTypes.Where(a => a.TransTypeID.Equals(id)).SingleOrDefault();
                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("TransactionType data for TransactionType ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTransactionTypeByProductID(int id, ref IList<TransactionTypeParameterModel> TransactionType, ref string message)
        {
            bool isSuccess = false;
            try
            {
                TransactionType = (from a in context.TransactionTypes
                                   join b in context.Products on a.ProductID equals b.ProductID
                                   where a.IsDeleted.Equals(false) && a.ProductID.Equals(id)
                                   select new TransactionTypeParameterModel
                                   {
                                       TransTypeID = a.TransTypeID,
                                       ProductID = a.ProductID,
                                       TransactionTypeName = a.TransactionType1
                                   }).ToList();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}