﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;
using DBS.Entity;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class MISReportingTableSummaryController : ApiController
    {
        private string message = string.Empty;
        private readonly IMISReportingTableSummary repositoryMISReportingTableSummary = null;
        
        public MISReportingTableSummaryController()
        {
            this.repositoryMISReportingTableSummary = new MISReportingTableSummaryRepository();
        }

        public MISReportingTableSummaryController(IMISReportingTableSummary repositoryMISReportingTableSummary)
        {
            this.repositoryMISReportingTableSummary = repositoryMISReportingTableSummary;
        }

        [ResponseType(typeof(MISReportingTableSummaryModel))]
        [AcceptVerbs("GET","POST")]
        [Route("api/MISReportingTableSummary")]
        public HttpResponseMessage Get(int TransactionType, int Segmentation, int Channel, int Branch, DateTime StartDate, DateTime EndDate, DateTime ClientDateTime)
        {
            IList<MISReportingTableSummaryModel> Output = new List<MISReportingTableSummaryModel>();
            if (!repositoryMISReportingTableSummary.GetMISReportingTableSummary(TransactionType, Segmentation, Channel, Branch, StartDate, EndDate, ClientDateTime, ref Output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<MISReportingTableSummaryModel>>(HttpStatusCode.OK, Output);
            }
        }
        
    }
}
