﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Models;
using DBS.WebAPI.Helpers;

namespace DBS.WebAPI.Models
{
    #region Property
    public class CustomerContactModel
    {
        /// <summary>
        /// Customer Indentification File (unique).
        /// </summary>
        public long ID { get; set; }

        public string CIF { get; set; }

        public string Name { get; set; }


        // [MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string PhoneNumber { get; set; }


        public DateTime? DateOfBirth { get; set; }


        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Address { get; set; }


        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string IDNumber { get; set; }

        public int SourceID { get; set; }

        public POAFunctionModel POAFunction { get; set; }
        public string POAFunctionVW { get; set; }

        public string OccupationInID { get; set; }

        public string PlaceOfBirth { get; set; }

        public DateTime? EffectiveDate { get; set; }

        public DateTime? CancellationDate { get; set; }

        public string POAFunctionOther { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedDate { get; set; }
    }

    public class CustomerContactWFModel
    {
        public long ID { get; set; }
        public string CIF { get; set; }
        public string Name { get; set; }
        public BizSegmentModel BizSegment { get; set; }
        public BranchModel Branch { get; set; }
        public CustomerTypeModel Type { get; set; }
        public RMModel RM { get; set; }
        public string NameContact { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string IDNumber { get; set; }
        public int SourceID { get; set; }
        public List<string> AccountNumber { get; set; }
        public List<POAFunctionModel> POAFunction { get; set; }
        public string OccupationInID { get; set; }
        public string PlaceOfBirth { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? CancellationDate { get; set; }
        //public string POAFunctionOther { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string POAFunctionVW { get; set; }
    }

    //DAYAT
    public class CustomerContactWFModelFilter
    {
        public long ID { get; set; }
        public string CIF { get; set; }
        public string Name { get; set; }

        public BizSegmentModel BizSegment { get; set; }

        public BranchModel Branch { get; set; }

        public CustomerTypeModel Type { get; set; }

        public RMModel RM { get; set; }

        public string NameContact { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string IDNumber { get; set; }

        public int SourceID { get; set; }

        public POAFunctionModel POAFunction { get; set; }

        public string OccupationInID { get; set; }

        public string PlaceOfBirth { get; set; }

        public DateTime? EffectiveDate { get; set; }

        public DateTime? CancellationDate { get; set; }

        public List<string> AccountNumber { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedDate { get; set; }
    }

    public class CustomerContactRow : CustomerContactModel
    {
        public int RowID { get; set; }

    }


    public class CustomerContactGrid : Grid
    {
        public IList<CustomerContactRow> Rows { get; set; }
    }

    public class CustomerContactWFGrid : Grid
    {
        public IList<CustomerContactWFRow> Rows { get; set; }
    }

    public class CustomerContactWFRow : CustomerContactWFModel
    {
        public int RowID { get; set; }

    }

    #endregion

    #region Filter
    public class CustomerContactFilter : Filter
    {
        //public string Field { get; set; }
        //public string Value { get; set; }
    }
    #endregion

    #region Interface
    public interface ICustomerContactRepository : IDisposable
    {
        bool GetCustomerContactByID(int id, ref CustomerContactModel CustomerContact, ref string message);
        bool GetCustomerContact(ref IList<CustomerContactModel> CustomerContacts, int limit, int index, ref string message);
        bool GetCustomerContact(ref IList<CustomerContactModel> CustomerContacts, ref string message);
        bool GetCustomerContact(int page, int size, IList<CustomerContactFilter> filters, string sortColumn, string sortOrder, ref CustomerContactGrid CustomerContact, ref string message);

        bool GetCustomerContact(string key, int limit, ref IList<CustomerContactModel> CustomerContacts, ref string message);
        bool AddCustomerContact(CustomerContactModel CustomerContact, ref string message);
        bool AddTransactionContact(Guid workflowInstanceID, long approverID, CustomerContactModel CustomerContact, ref string message);
        bool UpdateTransactionContact(Guid workflowInstanceID, long approverID, CustomerContactModel CustomerContact, ref string message);
        bool DeleteTransactionContact(Guid workflowInstanceID, long approverID, CustomerContactModel CustomerContact, ref string message);
        bool UpdateCustomerContact(int id, CustomerContactModel CustomerContact, ref string message);
        bool DeleteCustomerContact(int id, ref string message);

        bool GetCustomerContactWF(int page, int size, IList<CustomerContactFilter> filters, string sortColumn, string sortOrder, ref CustomerContactWFGrid CustomerContact, ref string message);
        bool GetCustomerContactWFByID(int id, ref CustomerContactWFModel CustomerContact, ref string message);
        bool AddCustomerContactWF(CustomerContactWFModel CustomerContact, ref string message);
        bool UpdateCustomerContactWF(int id, CustomerContactWFModel CustomerContact, ref string message);
        bool AutoCompeleteSelularPhoneNumber(string CIF, string key, ref IList<CustomerPhoneNumberModel> Country, ref string Message);
        bool AutoCompeleteOfficePhoneNumber(string CIF, string key, ref IList<CustomerPhoneNumberModel> Country, ref string Message);
        bool AutoCompeleteHomePhoneNumber(string CIF, string key, ref IList<CustomerPhoneNumberModel> Country, ref string Message);
    }
    #endregion

    #region Repository
    public class CustomerContactRepository : ICustomerContactRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetCustomerContactByID(int id, ref CustomerContactModel CustomerContact, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    CustomerContact = (from a in context.CustomerContacts
                                       where a.IsDeleted.Equals(false) && a.ContactID.Equals(id)
                                       select new CustomerContactModel
                                       {
                                           ID = a.ContactID,
                                           POAFunction = new POAFunctionModel
                                           {
                                               ID = a.POAFunction.POAFunctionID,
                                               Name = a.POAFunction.POAFunctionName,
                                               Description = a.POAFunction.POAFunctionDescription,
                                               LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                               LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                           },
                                           SourceID = a.SourceID,
                                           Name = a.ContactName,
                                           PhoneNumber = a.PhoneNumber,
                                           DateOfBirth = a.DateOfBirth,
                                           OccupationInID = a.OccupationInId,
                                           PlaceOfBirth = a.PlaceOfBirth,
                                           EffectiveDate = a.EffectiveDate,
                                           CancellationDate = a.CancellationDate,
                                           IDNumber = a.IdentificationNumber,
                                           POAFunctionOther = a.POAFunctionOther,
                                           LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                           LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                       }).SingleOrDefault();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerContactWFByID(int id, ref CustomerContactWFModel CustomerContact, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    CustomerContact = (from a in context.CustomerContacts
                                       join b in context.Customers on a.CIF equals b.CIF
                                       where a.IsDeleted.Equals(false) && a.ContactID.Equals(id)
                                       select new CustomerContactWFModel
                                       {
                                           ID = a.ContactID,
                                           CIF = a.CIF,
                                           Name = b.CustomerName,
                                           Branch = new BranchModel()
                                           {
                                               ID = b.Location.LocationID,
                                               Name = b.Location.LocationName,
                                               LastModifiedBy = b.Location.UpdateDate == null ? b.Location.CreateBy : b.Location.UpdateBy,
                                               LastModifiedDate = b.Location.UpdateDate == null ? b.Location.CreateDate : b.Location.UpdateDate.Value
                                           },
                                           BizSegment = new BizSegmentModel()
                                           {
                                               ID = b.BizSegment.BizSegmentID,
                                               Name = b.BizSegment.BizSegmentName,
                                               Description = b.BizSegment.BizSegmentDescription,
                                               LastModifiedBy = b.BizSegment.UpdateDate == null ? b.BizSegment.CreateBy : b.BizSegment.UpdateBy,
                                               LastModifiedDate = b.BizSegment.UpdateDate == null ? b.BizSegment.CreateDate : b.BizSegment.UpdateDate.Value
                                           },
                                           Type = new CustomerTypeModel()
                                           {
                                               ID = b.CustomerType.CustomerTypeID,
                                               Name = b.CustomerType.CustomerTypeName,
                                               Description = b.CustomerType.CustomerTypeDescription,
                                               LastModifiedBy = b.CustomerType.UpdateDate == null ? b.CustomerType.CreateBy : b.CustomerType.UpdateBy,
                                               LastModifiedDate = b.CustomerType.UpdateDate == null ? b.CustomerType.CreateDate : b.CustomerType.UpdateDate.Value
                                           },
                                           RM = b.CustomerMappings.Where(x => x.IsDeleted.Equals(false)).Select(x => new RMModel()
                                           {
                                               ID = x.Employee.EmployeeID,
                                               Name = x.Employee.EmployeeName,
                                               SegmentID = x.Employee.BizSegmentID,
                                               BranchID = x.Employee.LocationID,
                                               RankID = x.Employee.RankID,
                                               Email = x.Employee.EmployeeEmail,
                                               LastModifiedBy = x.Employee.UpdateDate == null ? x.Employee.CreateBy : x.Employee.UpdateBy,
                                               LastModifiedDate = x.Employee.UpdateDate == null ? x.Employee.CreateDate : x.Employee.UpdateDate.Value
                                           }).FirstOrDefault(),
                                           POAFunction = context.CustomerContactFunctions.Join(context.POAFunctions, c => c.POAFunctionID, p => p.POAFunctionID, (c, p) => new
                                           {
                                               ContactID = c.ContactID,
                                               POAFunctionID = c.POAFunctionID,
                                               POAFunctionName = p.POAFunctionName,
                                               POAFunctionDescription = p.POAFunctionDescription,
                                               LastModifiedBy = c.UpdateDate == null ? c.CreateBy : c.UpdateBy,
                                               LastModifiedDate = c.UpdateDate == null ? c.CreateDate : c.UpdateDate,
                                               IsDeleted = c.IsDeleted,
                                               POAFunctionIsDeleted = p.IsDeleted
                                           }).Where(c => c.ContactID == a.ContactID && c.IsDeleted == false && c.POAFunctionIsDeleted).Select(x => new POAFunctionModel()
                                           {
                                               ID = x.POAFunctionID,
                                               Name = x.POAFunctionName,
                                               Description = x.POAFunctionDescription,
                                               LastModifiedBy = x.LastModifiedBy,
                                               LastModifiedDate = x.LastModifiedDate
                                           }).ToList(),
                                           SourceID = a.SourceID,
                                           NameContact = a.ContactName,
                                           PhoneNumber = a.PhoneNumber,
                                           DateOfBirth = a.DateOfBirth,
                                           OccupationInID = a.OccupationInId,
                                           PlaceOfBirth = a.PlaceOfBirth,
                                           EffectiveDate = a.EffectiveDate,
                                           CancellationDate = a.CancellationDate,
                                           IDNumber = a.IdentificationNumber,
                                           //POAFunctionOther = a.POAFunctionOther,
                                           LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                           LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                       }).SingleOrDefault();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerContact(ref IList<CustomerContactModel> CustomerContacts, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    CustomerContacts = (from a in context.CustomerContacts
                                        where a.IsDeleted.Equals(false)
                                        orderby new { a.ContactName, a.ContactID }
                                        select new CustomerContactModel
                                        {
                                            ID = a.ContactID,
                                            Name = a.ContactName,
                                            POAFunction = new POAFunctionModel
                                            {
                                                ID = a.POAFunction.POAFunctionID,
                                                Name = a.POAFunction.POAFunctionName,
                                                Description = a.POAFunction.POAFunctionDescription,
                                                LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                            },
                                            SourceID = a.SourceID,
                                            PhoneNumber = a.PhoneNumber,
                                            DateOfBirth = a.DateOfBirth,
                                            IDNumber = a.IdentificationNumber,
                                            OccupationInID = a.OccupationInId,
                                            PlaceOfBirth = a.PlaceOfBirth,
                                            EffectiveDate = a.EffectiveDate,
                                            CancellationDate = a.CancellationDate,
                                            POAFunctionOther = a.POAFunctionOther,
                                            LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                            LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                        }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerContact(ref IList<CustomerContactModel> CustomerContacts, ref string message)
        {
            bool isSuccess = false;

            try
            {

                using (DBSEntities context = new DBSEntities())
                {
                    CustomerContacts = (from a in context.CustomerContacts
                                        where a.IsDeleted.Equals(false)
                                        orderby new { a.ContactName, a.ContactID }
                                        select new CustomerContactModel
                                        {
                                            ID = a.ContactID,
                                            POAFunction = new POAFunctionModel
                                            {
                                                ID = a.POAFunction.POAFunctionID,
                                                Name = a.POAFunction.POAFunctionName,
                                                Description = a.POAFunction.POAFunctionDescription,
                                                LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                            },
                                            SourceID = a.SourceID,
                                            Name = a.ContactName,
                                            PhoneNumber = a.PhoneNumber,
                                            DateOfBirth = a.DateOfBirth,
                                            IDNumber = a.IdentificationNumber,
                                            OccupationInID = a.OccupationInId,
                                            PlaceOfBirth = a.PlaceOfBirth,
                                            EffectiveDate = a.EffectiveDate,
                                            CancellationDate = a.CancellationDate,
                                            POAFunctionOther = a.POAFunctionOther,
                                            LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                            LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                        }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerContact(int page, int size, IList<CustomerContactFilter> filters, string sortColumn, string sortOrder, ref CustomerContactGrid CustomerContact, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;

                CustomerContactModel filter = new CustomerContactModel();
                filter.POAFunction = new POAFunctionModel { ID = 0, Description = "" };

                if (filters != null)
                {
                    filter.CIF = filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.POAFunction = new POAFunctionModel { Description = filters.Where(a => a.Field.Equals("POAFunction")).Select(a => a.Value).SingleOrDefault() };
                    filter.Name = filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.PhoneNumber = filters.Where(a => a.Field.Equals("PhoneNumber")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("DateOfBirth")).Any())
                    {
                        filter.DateOfBirth = DateTime.Parse(filters.Where(a => a.Field.Equals("DateOfBirth")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.OccupationInID = filters.Where(a => a.Field.Equals("OccupationInID")).Select(a => a.Value).SingleOrDefault();

                    filter.PlaceOfBirth = filters.Where(a => a.Field.Equals("PlaceOfBirth")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("EffectiveDate")).Any())
                    {
                        filter.EffectiveDate = DateTime.Parse(filters.Where(a => a.Field.Equals("EffectiveDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("CancellationDate")).Any())
                    {
                        filter.CancellationDate = DateTime.Parse(filters.Where(a => a.Field.Equals("CancellationDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.IDNumber = filters.Where(a => a.Field.Equals("IDNumber")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                    }
                }


                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.CustomerContacts
                                let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                let isPOAFunction = !string.IsNullOrEmpty(filter.POAFunction.Description)
                                let isName = !string.IsNullOrEmpty(filter.Name)
                                let isPhoneNumber = !string.IsNullOrEmpty(filter.PhoneNumber)
                                let isDateOfBirth = filter.DateOfBirth.HasValue ? true : false
                                let isIDNumber = !string.IsNullOrEmpty(filter.IDNumber)
                                let isOccupationInID = !string.IsNullOrEmpty(filter.OccupationInID)
                                let isPlaceOfBirth = !string.IsNullOrEmpty(filter.PlaceOfBirth)

                                let isEffectiveDate = filter.EffectiveDate.HasValue ? true : false
                                let isCancellationDate = filter.CancellationDate.HasValue ? true : false

                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isCIF ? a.CIF.Equals(filter.CIF) : true) &&
                                    (isPOAFunction ? a.POAFunction.POAFunctionDescription.Contains(filter.POAFunction.Description) : true) &&
                                    (isName ? a.ContactName.Contains(filter.Name) : true) &&
                                    (isPhoneNumber ? a.PhoneNumber.Contains(filter.PhoneNumber) : true) &&
                                    (isDateOfBirth ? a.DateOfBirth.Equals(filter.DateOfBirth) : true) &&
                                    (isOccupationInID ? a.OccupationInId.Equals(filter.OccupationInID) : true) &&
                                    (isIDNumber ? a.IdentificationNumber.Contains(filter.IDNumber) : true) &&
                                    (isPlaceOfBirth ? a.PlaceOfBirth.Contains(filter.PlaceOfBirth) : true) &&

                                    (isEffectiveDate ? a.EffectiveDate.Equals(filter.EffectiveDate.Value) : true) &&
                                    (isCancellationDate ? a.CancellationDate.Equals(filter.CancellationDate.Value) : true) &&

                                    (isCreateDate ? a.CreateDate.Equals(filter.LastModifiedDate.Value) : true)
                                select new CustomerContactModel
                                {
                                    ID = a.ContactID,
                                    Name = a.ContactName,
                                    POAFunction = a.POAFunction != null ? new POAFunctionModel()
                                    {
                                        ID = a.POAFunction.POAFunctionID,
                                        Name = a.POAFunction.POAFunctionName,
                                        Description = a.POAFunction.POAFunctionDescription,
                                        LastModifiedBy = a.POAFunction.UpdateDate == null ? a.POAFunction.CreateBy : a.POAFunction.UpdateBy,
                                        LastModifiedDate = a.POAFunction.UpdateDate == null ? a.POAFunction.CreateDate : a.POAFunction.UpdateDate.Value
                                    } :
                                   new POAFunctionModel
                                   {
                                       ID = 0,
                                       Name = "",
                                       Description = "",
                                       LastModifiedBy = a.POAFunction.UpdateDate == null ? a.POAFunction.CreateBy : a.POAFunction.UpdateBy,
                                       LastModifiedDate = a.POAFunction.UpdateDate == null ? a.POAFunction.CreateDate : a.POAFunction.UpdateDate.Value
                                   },
                                    SourceID = a.SourceID,
                                    PhoneNumber = a.PhoneNumber,
                                    DateOfBirth = a.DateOfBirth,
                                    OccupationInID = a.OccupationInId,
                                    PlaceOfBirth = a.PlaceOfBirth,
                                    EffectiveDate = a.EffectiveDate,
                                    CancellationDate = a.CancellationDate,
                                    IDNumber = a.IdentificationNumber,
                                    POAFunctionOther = a.POAFunctionOther,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    CustomerContact.Page = page;
                    CustomerContact.Size = size;
                    CustomerContact.Total = data.Count();
                    CustomerContact.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    CustomerContact.Rows = data.OrderBy(orderBy).AsEnumerable().Select((a, i) => new CustomerContactRow
                    {
                        RowID = i + 1,
                        ID = a.ID,
                        POAFunction = a.POAFunction,
                        SourceID = a.SourceID,
                        Name = a.Name,
                        PhoneNumber = a.PhoneNumber,
                        DateOfBirth = a.DateOfBirth,
                        OccupationInID = a.OccupationInID,
                        PlaceOfBirth = a.PlaceOfBirth,
                        EffectiveDate = a.EffectiveDate,
                        CancellationDate = a.CancellationDate,
                        IDNumber = a.IDNumber,
                        POAFunctionOther = a.POAFunctionOther,
                        LastModifiedDate = a.LastModifiedDate,
                        LastModifiedBy = a.LastModifiedBy
                    })
                    .Skip(skip)
                    .Take(size)
                    .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerContactWF(int page, int size, IList<CustomerContactFilter> filters, string sortColumn, string sortOrder, ref CustomerContactWFGrid CustomerContact, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;

                CustomerContactWFModel filter = new CustomerContactWFModel();

                if (filters != null)
                {
                    filter.CIF = filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.POAFunctionVW = filters.Where(a => a.Field.Equals("POAFunctionVW")).Select(a => a.Value).SingleOrDefault();
                    filter.NameContact = filters.Where(a => a.Field.Equals("NameContact")).Select(a => a.Value).SingleOrDefault();
                    filter.PhoneNumber = filters.Where(a => a.Field.Equals("PhoneNumber")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("DateOfBirth")).Any())
                    {
                        filter.DateOfBirth = DateTime.Parse(filters.Where(a => a.Field.Equals("DateOfBirth")).Select(a => a.Value).FirstOrDefault());
                    }
                    filter.OccupationInID = filters.Where(a => a.Field.Equals("OccupationInID")).Select(a => a.Value).SingleOrDefault();
                    filter.PlaceOfBirth = filters.Where(a => a.Field.Equals("PlaceOfBirth")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("EffectiveDate")).Any())
                    {
                        filter.EffectiveDate = DateTime.Parse(filters.Where(a => a.Field.Equals("EffectiveDate")).Select(a => a.Value).FirstOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("CancellationDate")).Any())
                    {
                        filter.CancellationDate = DateTime.Parse(filters.Where(a => a.Field.Equals("CancellationDate")).Select(a => a.Value).FirstOrDefault());
                    }
                    filter.IDNumber = filters.Where(a => a.Field.Equals("IDNumber")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                    }
                }


                using (DBSEntities context = new DBSEntities())
                {
                    IList<CustomerContactWFModel> data = (from a in context.SP_GETContact(
                                                              filter.CIF,
                                                              filter.NameContact,
                                                              filter.POAFunctionVW,
                                                              filter.PhoneNumber,
                                                              filter.DateOfBirth,
                                                              filter.OccupationInID,
                                                              filter.PlaceOfBirth,
                                                              filter.EffectiveDate,
                                                              filter.CancellationDate,
                                                              filter.IDNumber,
                                                              filter.LastModifiedDate)
                                                          select new CustomerContactWFModel
                                                          {
                                                              CIF = a.CIF,
                                                              Name = a.CustomerName,
                                                              Branch = a.LocationID != null ? new BranchModel()
                                                              {
                                                                  ID = a.LocationID.Value,
                                                                  Name = a.LocationName,
                                                                  LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                  LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                              } : new BranchModel()
                                                              {
                                                                  ID = 0,
                                                                  Name = "",
                                                                  LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                  LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                              },
                                                              BizSegment = new BizSegmentModel()
                                                              {
                                                                  ID = a.BizSegmentID,
                                                                  Name = a.BizSegmentName,
                                                                  Description = a.BizSegmentDescription,
                                                                  LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                  LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                              },
                                                              Type = a.CustomerTypeID != null ? new CustomerTypeModel()
                                                              {
                                                                  ID = a.CustomerTypeID.Value,
                                                                  Name = a.CustomerTypeName,
                                                                  Description = a.CustomerTypeDescription,
                                                                  LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                  LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                              } : new CustomerTypeModel()
                                                              {
                                                                  ID = 0,
                                                                  Name = "",
                                                                  Description = "",
                                                                  LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                  LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                              },
                                                              RM = a.EmployeeID != null ? new RMModel()
                                                              {

                                                                  ID = a.EmployeeID,
                                                                  Name = a.EmployeeName,
                                                                  RankID = a.RankID,
                                                                  BranchID = a.LocationIDRM.HasValue ? a.LocationIDRM.Value : 0,
                                                                  SegmentID = a.BizSegmentID,
                                                                  Email = a.EmployeeEmail,
                                                                  LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                  LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                              } : new RMModel()
                                                              {
                                                                  ID = 0,
                                                                  Name = "",
                                                                  RankID = 0,
                                                                  BranchID = 0,
                                                                  SegmentID = 0,
                                                                  Email = "",
                                                                  LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                  LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                              },
                                                              AccountNumber = context.CustomerContactAccountNumbers.Where(x => x.ContactID == a.ContactID && x.IsDeleted == false).Select(x => x.CustomerAccountNumber).ToList(),
                                                              POAFunction = context.CustomerContactFunctions.Where(x => x.ContactID == a.ContactID && x.IsDeleted == false).Select(z => new POAFunctionModel()
                                                              {
                                                                  ID = z.POAFunctionID,
                                                                  POAFunctionOther = z.POAFunctionOther,
                                                                  Description = context.POAFunctions.Where(s => s.POAFunctionID.Equals(z.POAFunctionID)).Select(x => x.POAFunctionDescription).FirstOrDefault(),
                                                              }).ToList<POAFunctionModel>(),
                                                              ID = a.ContactID,
                                                              NameContact = a.ContactName,
                                                              POAFunctionVW = a.POADescription,
                                                              SourceID = a.SourceID,
                                                              PhoneNumber = a.PhoneNumber,
                                                              DateOfBirth = a.DateOfBirth,
                                                              OccupationInID = a.OccupationInID,
                                                              PlaceOfBirth = a.PlaceOfBirth,
                                                              EffectiveDate = a.EffectiveDate,
                                                              CancellationDate = a.CancellationDate,
                                                              IDNumber = a.IdentificationNumber,
                                                              //POAFunctionOther = a.POAFunctionOther,
                                                              LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                              LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                          }).ToList();
                    CustomerContact.Page = page;
                    CustomerContact.Size = size;
                    CustomerContact.Total = data.Count();
                    CustomerContact.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    CustomerContact.Rows = data.AsQueryable().OrderBy(orderBy).Select((a, i) => new CustomerContactWFRow
                    {
                        RowID = i + 1,
                        ID = a.ID,
                        CIF = a.CIF,
                        BizSegment = a.BizSegment,
                        Branch = a.Branch,
                        Type = a.Type,
                        RM = a.RM,
                        POAFunction = a.POAFunction,
                        AccountNumber = a.AccountNumber,
                        SourceID = a.SourceID,
                        Name = a.Name,
                        NameContact = a.NameContact,
                        PhoneNumber = a.PhoneNumber,
                        DateOfBirth = a.DateOfBirth,
                        OccupationInID = a.OccupationInID,
                        PlaceOfBirth = a.PlaceOfBirth,
                        EffectiveDate = a.EffectiveDate,
                        CancellationDate = a.CancellationDate,
                        IDNumber = a.IDNumber,
                        LastModifiedDate = a.LastModifiedDate,
                        LastModifiedBy = a.LastModifiedBy,
                        POAFunctionVW = a.POAFunctionVW
                    })
                    .Skip(skip)
                    .Take(size)
                    .Distinct()
                    .ToList();

                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetCustomerContact(string key, int limit, ref IList<CustomerContactModel> CustomerContacts, ref string message)
        {
            bool isSuccess = false;
            try
            {
                CustomerContacts = (from a in context.CustomerContacts
                                    where a.IsDeleted.Equals(false) && a.ContactName.Contains(key)
                                    orderby a.ContactName
                                    select new CustomerContactModel
                                    {
                                        ID = a.ContactID,
                                        POAFunction = new POAFunctionModel
                                        {
                                            ID = a.POAFunction.POAFunctionID,
                                            Name = a.POAFunction.POAFunctionName,
                                            Description = a.POAFunction.POAFunctionDescription,
                                            LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                            LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                        },
                                        SourceID = a.SourceID,
                                        Name = a.ContactName,
                                        PhoneNumber = a.PhoneNumber,
                                        DateOfBirth = a.DateOfBirth,
                                        OccupationInID = a.OccupationInId,
                                        PlaceOfBirth = a.PlaceOfBirth,
                                        EffectiveDate = a.EffectiveDate,
                                        CancellationDate = a.CancellationDate,
                                        IDNumber = a.IdentificationNumber,
                                        POAFunctionOther = a.POAFunctionOther,
                                        LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                        LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                    }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddCustomerContact(CustomerContactModel CustomerContact, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.CustomerContacts.Where(a => a.ContactName.Equals(CustomerContact.Name) && a.PhoneNumber.Equals(CustomerContact.PhoneNumber)).Any())
                {
                    context.CustomerContacts.Add(new Entity.CustomerContact()
                    {
                        CIF = CustomerContact.CIF,
                        POAFunctionID = CustomerContact.POAFunction.ID,
                        SourceID = CustomerContact.SourceID,
                        ContactName = CustomerContact.Name,
                        PhoneNumber = CustomerContact.PhoneNumber,
                        DateOfBirth = CustomerContact.DateOfBirth,
                        OccupationInId = CustomerContact.OccupationInID,
                        PlaceOfBirth = CustomerContact.PlaceOfBirth,
                        EffectiveDate = CustomerContact.EffectiveDate,
                        CancellationDate = CustomerContact.CancellationDate,
                        IdentificationNumber = CustomerContact.IDNumber,
                        POAFunctionOther = CustomerContact.POAFunctionOther,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });
                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Customer Contact data for Contact Name and Phone Number {0}, {1} is already exist.", CustomerContact.Name, CustomerContact.PhoneNumber);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddCustomerContactWF(CustomerContactWFModel CustomerContact, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.CustomerContacts.Where(a => a.ContactName.Equals(CustomerContact.Name) && a.PhoneNumber.Equals(CustomerContact.PhoneNumber)).Any())
                {
                    context.CustomerContacts.Add(new Entity.CustomerContact()
                    {
                        CIF = CustomerContact.CIF,
                        //POAFunctionID = CustomerContact.POAFunction.ID,
                        SourceID = CustomerContact.SourceID,
                        ContactName = CustomerContact.NameContact,
                        PhoneNumber = CustomerContact.PhoneNumber,
                        DateOfBirth = CustomerContact.DateOfBirth,
                        OccupationInId = CustomerContact.OccupationInID,
                        PlaceOfBirth = CustomerContact.PlaceOfBirth,
                        EffectiveDate = CustomerContact.EffectiveDate,
                        CancellationDate = CustomerContact.CancellationDate,
                        IdentificationNumber = CustomerContact.IDNumber,
                        //POAFunctionOther = CustomerContact.POAFunctionOther,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });
                    context.SaveChanges();
                    var ContactID = context.CustomerContacts.Where(a => a.CIF.Equals(CustomerContact.CIF) && a.ContactName == CustomerContact.NameContact).Select(a => a.ContactID).FirstOrDefault();
                    if (CustomerContact.POAFunction != null)
                    {
                        try
                        {
                            var Data = new CustomerContactFunction();
                            foreach (var item in CustomerContact.POAFunction)
                            {
                                Data.ContactID = ContactID;
                                Data.CreateBy = currentUser.GetCurrentUser().DisplayName;
                                Data.CreateDate = DateTime.UtcNow;
                                Data.IsDeleted = false;
                                Data.POAFunctionID = item.ID;
                                Data.POAFunctionOther = item.POAFunctionOther;

                                context.CustomerContactFunctions.Add(Data);
                                context.SaveChanges();
                            }

                        }
                        catch (Exception ex)
                        {
                            var err = ex.Message;
                        }
                    }
                    if (CustomerContact.AccountNumber != null)
                    {
                        try
                        {
                            var Data = new CustomerContactAccountNumber();
                            foreach (var item in CustomerContact.AccountNumber)
                            {
                                Data.ContactID = ContactID;
                                Data.CustomerAccountNumber = item;
                                Data.CreateBy = currentUser.GetCurrentUser().DisplayName;
                                Data.CreateDate = DateTime.UtcNow;
                                Data.IsDeleted = false;

                                context.CustomerContactAccountNumbers.Add(Data);
                                context.SaveChanges();
                            }

                        }
                        catch (Exception ex)
                        {
                            var err = ex.Message;
                        }

                    }
                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Customer Contact data for Contact Name and Phone Number {0}, {1} is already exist.", CustomerContact.Name, CustomerContact.PhoneNumber);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddTransactionContact(Guid workflowInstanceID, long approverID, CustomerContactModel CustomerContact, ref string message)
        {
            bool isSuccess = false;

            try
            {
                var transaction = (from a in context.Transactions
                                   where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                                   select a.TransactionID).SingleOrDefault();

                if (!context.TransactionContacts.Where(a => a.ContactName.ToLower().Equals(CustomerContact.Name)
                    && a.IdentificationNumber.Equals(CustomerContact.IDNumber)
                    && a.CIF.Equals(CustomerContact.CIF)
                    && a.IsDeleted.Equals(false)).Any())
                {
                    context.TransactionContacts.Add(new Entity.TransactionContact()
                    {
                        CIF = CustomerContact.CIF,
                        TransactionID = transaction,
                        ApproverID = approverID,
                        ContactName = CustomerContact.Name,
                        PhoneNumber = CustomerContact.PhoneNumber,
                        DateOfBirth = CustomerContact.DateOfBirth,
                        Address = CustomerContact.Address,
                        IdentificationNumber = CustomerContact.IDNumber,
                        POAFunctionID = CustomerContact.POAFunction.ID,
                        OccupationInID = CustomerContact.OccupationInID,
                        PlaceOfBirth = CustomerContact.PlaceOfBirth,
                        EffectiveDate = CustomerContact.EffectiveDate,
                        CancellationDate = CustomerContact.CancellationDate,
                        POAFunctionOther = CustomerContact.POAFunctionOther,
                        IsDeleted = false,
                        SourceID = 1,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });
                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Customer Contact data for Contact Name and ID Number {0}, {1} is already exist.", CustomerContact.Name, CustomerContact.IDNumber);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateTransactionContact(Guid workflowInstanceID, long approverID, CustomerContactModel CustomerContact, ref string message)
        {
            bool isSuccess = false;

            try
            {
                var transaction = (from a in context.Transactions
                                   where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                                   select a.TransactionID).SingleOrDefault();

                var updatedContact = context.TransactionContacts.Where(a => a.TransactionID.Equals(transaction) && a.ContactName.Equals(CustomerContact.Name)).SingleOrDefault();

                // if exist: update exisiting contact
                if (updatedContact != null)
                {
                    updatedContact.ContactName = CustomerContact.Name;
                    updatedContact.PhoneNumber = CustomerContact.PhoneNumber;
                    updatedContact.DateOfBirth = CustomerContact.DateOfBirth;
                    updatedContact.Address = "-";
                    updatedContact.IdentificationNumber = CustomerContact.IDNumber;
                    updatedContact.POAFunctionID = CustomerContact.POAFunction.ID;
                    updatedContact.OccupationInID = CustomerContact.OccupationInID;
                    updatedContact.PlaceOfBirth = CustomerContact.PlaceOfBirth;
                    updatedContact.EffectiveDate = CustomerContact.EffectiveDate;
                    updatedContact.CancellationDate = CustomerContact.CancellationDate;
                    updatedContact.POAFunctionOther = CustomerContact.POAFunctionOther;
                    updatedContact.IsDeleted = false;
                    updatedContact.UpdateDate = DateTime.UtcNow;
                    updatedContact.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();
                    isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteTransactionContact(Guid workflowInstanceID, long approverID, CustomerContactModel CustomerContact, ref string message)
        {
            bool isSuccess = false;

            try
            {
                var transaction = (from a in context.Transactions
                                   where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                                   select a.TransactionID).SingleOrDefault();

                var updatedContact = context.TransactionContacts.Where(a => a.TransactionID.Equals(transaction) && a.ContactName.Equals(CustomerContact.Name)).SingleOrDefault();

                // if exist: update exisiting contact
                if (updatedContact != null)
                {
                    updatedContact.IsDeleted = true;
                    updatedContact.UpdateDate = DateTime.UtcNow;
                    updatedContact.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();
                    isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateCustomerContact(int id, CustomerContactModel CustomerContact, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.CustomerContact data = context.CustomerContacts.Where(a => a.ContactID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.POAFunctionID = CustomerContact.POAFunction.ID;
                    data.SourceID = CustomerContact.SourceID;
                    data.ContactName = CustomerContact.Name;
                    data.PhoneNumber = CustomerContact.PhoneNumber;
                    data.DateOfBirth = CustomerContact.DateOfBirth;
                    data.OccupationInId = CustomerContact.OccupationInID;
                    data.PlaceOfBirth = CustomerContact.PlaceOfBirth;
                    data.EffectiveDate = CustomerContact.EffectiveDate;
                    data.CancellationDate = CustomerContact.CancellationDate;
                    data.IdentificationNumber = CustomerContact.IDNumber;
                    data.POAFunctionOther = CustomerContact.POAFunctionOther;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                    data.ContactName = CustomerContact.Name;
                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Customer data for Contact ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateCustomerContactWF(int id, CustomerContactWFModel CustomerContact, ref string message)
        {
            bool isSuccess = false;

            try
            {
                DateTime createDate = DateTime.UtcNow;
                string currentUserTemp = currentUser.GetCurrentUser().DisplayName;
                Entity.CustomerContact data = context.CustomerContacts.Where(a => a.ContactID.Equals(id)).SingleOrDefault();
                if (data != null)
                {
                    data.SourceID = CustomerContact.SourceID;
                    data.ContactName = CustomerContact.NameContact;
                    data.PhoneNumber = CustomerContact.PhoneNumber;
                    data.DateOfBirth = CustomerContact.DateOfBirth;
                    data.OccupationInId = CustomerContact.OccupationInID;
                    data.PlaceOfBirth = CustomerContact.PlaceOfBirth;
                    data.EffectiveDate = CustomerContact.EffectiveDate;
                    data.CancellationDate = CustomerContact.CancellationDate;
                    data.IdentificationNumber = CustomerContact.IDNumber;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();
                    #region POAFunction Update
                    List<CustomerContactFunction> POAFunctionData = context.CustomerContactFunctions.Where(a => a.ContactID == id && a.IsDeleted == false).ToList();
                    if (POAFunctionData != null)
                    {
                        foreach (var item in POAFunctionData)
                        {
                            item.IsDeleted = true;
                            item.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                            item.UpdateDate = DateTime.UtcNow;
                        }
                        context.SaveChanges();
                    }
                    if (CustomerContact.POAFunction != null)
                    {
                        try
                        {
                            var Data = new CustomerContactFunction();
                            foreach (var item in CustomerContact.POAFunction)
                            {
                                Data.ContactID = id;
                                Data.CreateBy = currentUser.GetCurrentUser().DisplayName;
                                Data.CreateDate = DateTime.UtcNow;
                                Data.IsDeleted = false;
                                Data.POAFunctionID = item.ID;
                                Data.POAFunctionOther = item.POAFunctionOther;

                                context.CustomerContactFunctions.Add(Data);
                                context.SaveChanges();
                            }
                        }
                        catch (Exception ex)
                        {
                            var err = ex.Message;
                        }
                    }
                    #endregion
                    #region AccountNumber
                    List<CustomerContactAccountNumber> AccountNumberData = context.CustomerContactAccountNumbers.Where(a => a.ContactID == id && a.IsDeleted == false).ToList();
                    if (AccountNumberData != null)
                    {
                        foreach (var item in AccountNumberData)
                        {
                            item.IsDeleted = true;
                            item.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                            item.UpdateDate = DateTime.UtcNow;
                            context.SaveChanges();
                        }
                    }
                    if (CustomerContact.AccountNumber != null)
                    {
                        try
                        {
                            var Data = new CustomerContactAccountNumber();
                            foreach (var item in CustomerContact.AccountNumber)
                            {
                                Data.IsDeleted = false;
                                Data.ContactID = id;
                                Data.CustomerAccountNumber = item;
                                Data.CreateBy = currentUser.GetCurrentUser().DisplayName;
                                Data.CreateDate = DateTime.UtcNow;

                                context.CustomerContactAccountNumbers.Add(Data);
                                context.SaveChanges();
                            }

                        }
                        catch (Exception ex)
                        {
                            var err = ex.Message;
                        }

                    }
                    #endregion

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Customer data for Contact ID {0} is does not exist.", id);
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteCustomerContact(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.CustomerContact data = context.CustomerContacts.Where(a => a.ContactID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                    context.SaveChanges();

                    List<CustomerContactFunction> POAFunctionData = context.CustomerContactFunctions.Where(a => a.ContactID.Equals(id)).ToList();
                    foreach (var item in POAFunctionData)
                    {
                        item.IsDeleted = true;
                        item.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        item.UpdateDate = DateTime.UtcNow;
                        context.SaveChanges();
                    }

                    List<CustomerContactAccountNumber> AccountNumberData = context.CustomerContactAccountNumbers.Where(a => a.ContactID.Equals(id)).ToList();
                    if (AccountNumberData != null)
                    {
                        foreach (var item in AccountNumberData)
                        {
                            item.IsDeleted = true;
                            item.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                            item.UpdateDate = DateTime.UtcNow;
                            context.SaveChanges();
                        }
                    }

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Customer data for Contact ID {0} is does not exist.", id.ToString());
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool AutoCompeleteSelularPhoneNumber(string CIF, string key, ref IList<CustomerPhoneNumberModel> PhoneNumber, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities cont = new DBSEntities())
                {
                    PhoneNumber = (from ct in cont.CustomerPhoneNumbers
                                   where ct.CIF == CIF && (ct.IsDeleted == false || ct.IsDeleted == null) && ct.ContactType == "Selular" && ct.PhoneNumber.Contains(key)
                                   select new CustomerPhoneNumberModel
                                   {
                                       CIF = ct.CIF,
                                       PhoneNumber = ct.PhoneNumber,
                                       RetailCIFCBOContactID = ct.RetailCIFCBOContactID,
                                       RetailCIFCBOID = ct.RetailCIFCBOID,
                                       CountryCode = ct.CountryCode,
                                       CityCode = ct.CityCode,
                                       CustomerPhoneNumberID = ct.CustomerPhoneNumberID
                                   }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception e)
            {
                Message = e.Message;
            }
            return isSuccess;
        }
        public bool AutoCompeleteOfficePhoneNumber(string CIF, string key, ref IList<CustomerPhoneNumberModel> PhoneNumber, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities cont = new DBSEntities())
                {
                    PhoneNumber = (from ct in cont.CustomerPhoneNumbers
                                   where ct.CIF == CIF && (ct.IsDeleted == false || ct.IsDeleted == null) && ct.ContactType == "Office" && ct.PhoneNumber.Contains(key)
                                   select new CustomerPhoneNumberModel
                                   {
                                       CIF = ct.CIF,
                                       PhoneNumber = ct.PhoneNumber,
                                       RetailCIFCBOContactID = ct.RetailCIFCBOContactID,
                                       RetailCIFCBOID = ct.RetailCIFCBOID,
                                       CountryCode = ct.CountryCode,
                                       CityCode = ct.CityCode,
                                       CustomerPhoneNumberID = ct.CustomerPhoneNumberID
                                   }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception e)
            {
                Message = e.Message;
            }
            return isSuccess;
        }
        public bool AutoCompeleteHomePhoneNumber(string CIF, string key, ref IList<CustomerPhoneNumberModel> PhoneNumber, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities cont = new DBSEntities())
                {
                    PhoneNumber = (from ct in cont.CustomerPhoneNumbers
                                   where ct.CIF == CIF && (ct.IsDeleted == false || ct.IsDeleted == null) && (ct.ContactType == "Home") && ct.PhoneNumber.Contains(key)
                                   select new CustomerPhoneNumberModel
                                   {
                                       CIF = ct.CIF,
                                       PhoneNumber = ct.PhoneNumber,
                                       RetailCIFCBOContactID = ct.RetailCIFCBOContactID,
                                       RetailCIFCBOID = ct.RetailCIFCBOID,
                                       CountryCode = ct.CountryCode,
                                       CityCode = ct.CityCode,
                                       CustomerPhoneNumberID = ct.CustomerPhoneNumberID
                                   }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception e)
            {
                Message = e.Message;
            }
            return isSuccess;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                        context.Dispose();
                }
            }
            this.disposed = true;
        }

        void IDisposable.Dispose()
        {

            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

    #endregion
}
