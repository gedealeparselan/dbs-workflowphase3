﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Protocols.WSTrust;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Description;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    public class AuthController : ApiController
    {
        private string message = string.Empty;

        private readonly ISharepointUserRepository repository = null;

        public AuthController()
        {
            this.repository = new SharepointUserRepository();
        }

        public AuthController(ISharepointUserRepository repository)
        {
            this.repository = repository;
        }

        // get request domain
        //string audience = HttpContext.Current.Request.Url.Host;

        private const string ISSUER = "DBS.WebAPI";
        private const string AUDIENCE = "http://localhost:58592/api";
        

        /// <summary>
        /// Request access token for Sharepoint users.
        /// </summary>
        /// <param name="user">Sharepoint user information.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(APITokenModel))]
        public HttpResponseMessage Post(SharepointUserModel user)
        {
            //HttpRequest request = context.Request;
            //string userName = request["username"];
            //string password = request["password"];
            //bool isAuthentic = !String.IsNullOrEmpty(userName) && userName.Equals(password);
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (true)
            {
                // I use a hard-coded key
                byte[] key = Convert.FromBase64String("qqO5yXcbijtAdYmS2Otyzeze2XQedqy+Tp37wQ3sgTQ=");

                var signingCredentials = new SigningCredentials(new InMemorySymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature, SecurityAlgorithms.Sha256Digest);
                
                Lifetime lifeTime = new Lifetime(DateTime.UtcNow, DateTime.UtcNow.AddMonths(1));

                // Generate claims, all spuser information will be stored for next proccess
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim("ID", user.ID.ToString()));
                claims.Add(new Claim("LoginName", user.LoginName));
                claims.Add(new Claim("DisplayName", user.DisplayName));
                claims.Add(new Claim("Email", user.Email));

                //Remark by Rizki - 2017-05-17
                //if (user.Roles.Any())
                //{
                //    foreach (var item in user.Roles)
                //    {
                //        claims.Add(new Claim(ClaimTypes.Role, item.ID.ToString()));                            
                //    }                    
                //}

                // Generate security token descriptor
                var descriptor = new SecurityTokenDescriptor()
                {
                    TokenIssuerName = ISSUER,
                    AppliesToAddress = AUDIENCE,
                    Lifetime = lifeTime,
                    SigningCredentials = signingCredentials,
                    Subject = new ClaimsIdentity(claims.AsEnumerable())
                };
                
                var tokenHandler = new JwtSecurityTokenHandler();
                var token = tokenHandler.CreateToken(descriptor);

                // store user claims into db
                if (repository.StoreClaims(user, ref message))
                {
                    //context.Response.Write(tokenHandler.WriteToken(token));
                    APITokenModel apiToken = new APITokenModel()
                    {
                        Audience = AUDIENCE,
                        Issuer = ISSUER,
                        LifeTime = lifeTime,
                        AccessToken = tokenHandler.WriteToken(token),
                        User = user
                    };
                    
                    return Request.CreateResponse(apiToken);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
        }

        /// <summary>
        /// Get authorized user information
        /// </summary>
        /// <returns></returns>
        /* Remark by Rizki - 2017-05-17 : Fungsi tidak terpakai
        [ResponseType(typeof(SharepointUserModel))]
        [Authorize]
        public HttpResponseMessage Get()
        {
            //return Request.CreateResponse(HttpStatusCode.OK, GetClaims(User));
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            var claims = from c in identity.Claims select new { CType = c.Type, Value = c.Value, ValueType = c.ValueType };
            SharepointUserModel spuser = new SharepointUserModel();
            IList<UserRole> roles = new List<UserRole>();

            foreach (var item in identity.Claims)
            {
                switch (item.Type)
                {
                    case "ID" :
                        spuser.ID = Convert.ToInt32(item.Value);
                        break;
                    case "LoginName":
                        spuser.LoginName = item.Value;
                        break;
                    case "DisplayName":
                        spuser.DisplayName = item.Value;
                        break;
                    case "Email":
                        spuser.Email = item.Value;
                        break;
                    case ClaimTypes.Role:
                        roles.Add(new UserRole { Name = item.Value});
                        break;
                    default:
                        break;
                }
            }
            spuser.Roles = roles;

            return Request.CreateResponse(HttpStatusCode.OK, spuser);
        }
        */

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}
