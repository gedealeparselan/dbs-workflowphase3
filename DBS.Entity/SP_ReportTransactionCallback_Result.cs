//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    
    public partial class SP_ReportTransactionCallback_Result
    {
        public string TransactionCallbackID { get; set; }
        public Nullable<System.DateTime> ApplicationDate { get; set; }
        public string ApplicationID { get; set; }
        public string TransactionID { get; set; }
        public Nullable<System.DateTime> Time { get; set; }
        public string ContactName { get; set; }
        public string PhoneNumber { get; set; }
        public string IsUTC { get; set; }
        public string Rank { get; set; }
        public string Attemp { get; set; }
        public string UTC { get; set; }
    }
}
