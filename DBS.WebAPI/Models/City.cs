﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Linq.Dynamic;


namespace DBS.WebAPI.Models
{
    #region property

    public class ProvinsiModel
    {
        public int? ProvinceID { get; set; }
        public string ProvinceCode { get; set; }
        public string Description { get; set; }

    }
    public class CityModel
    {
        public int CityID { get; set; }
        public string CityCode { get; set; }
        public string Description { get; set; }
        public int? ProvinceID { get; set; }

        public ProvinsiModel Provinsi { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }

    public class CityModelRow : CityModel
    {
        public int RowID { get; set; }
    }
    public class CityModelGrid : Grid
    {
        public IList<CityModelRow> Rows { get; set; }
    }
    #endregion

    #region Filter
    public class CityModelFilter : Filter { }
    #endregion

    #region interface
    public interface ICityModel : IDisposable
    {
        bool GetCityByID(int ID, ref CityModel City, ref string message);
        bool GetCity(ref IList<CityModel> City, ref string message);
        bool GetCity(string key, int limit, ref IList<ProvinsiModel> provinsi, ref string message);
        bool GetCity(int page, int size, IList<CityModelFilter> filters, string sortColumn, string sortOrder, ref CityModelGrid City, ref string message);
        bool AddCity(CityModel City, ref string message);

        bool UpdateCity(int ID, ref CityModel City, ref string message);

        bool DeleteCity(int id, ref string message);
        bool GetCity(string key, int limit, ref IList<CityModel> banks, ref string message);
    }
    #endregion

    #region Repository
    public class CityRepository : ICityModel
    {

        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetCityByID(int ID, ref CityModel City, ref string message)
        {
            bool isSuccess = false;

            try
            {
                City = (from a in context.Cities
                        //join b in context.Branches on a.CityCode equals b.CityCode
                        // where a.IsDeleted.Equals(false) && a.CityID.Equals(ID)
                        select new CityModel
                        {
                            CityID = a.CityID,
                            CityCode = a.CityCode,
                            Description = a.Description,
                            ProvinceID = a.ProvinceID,

                            LastModifiedBy = a.CreateBy,
                            LastModifiedDate = a.CreateDate
                        }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCity(ref IList<CityModel> City, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    City = (from a in context.Cities

                            where a.IsDeleted.Equals(false)
                            orderby new { a.CityCode }
                            select new CityModel
                            {
                                CityID = a.CityID,
                                CityCode = a.CityCode,
                                ProvinceID = a.ProvinceID,
                                //ClearingCode = a.ClearingCode,
                                LastModifiedBy = a.CreateBy,
                                LastModifiedDate = a.CreateDate
                            }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCity(ref IList<ProvinsiModel> City, int limit, int index, ref string message)
        {
            bool isSuuces = false;
            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    City = (from a in context.Provinces

                            select new ProvinsiModel
                            {
                                ProvinceID = a.ProvinceID,
                                ProvinceCode = a.ProvinceCode,
                                Description = a.Description,

                            }).Skip(skip).Take(limit).ToList();
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuuces;
        }

        public bool GetCity(int page, int size, IList<CityModelFilter> filters, string sortColumn, string sortOrder, ref CityModelGrid City, ref string message)
        {
            bool isSucces = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                CityModel filter = new CityModel();
                if (filters != null)
                {

                    filter.CityCode = filters.Where(a => a.Field.Equals("CityCode")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    //filter.ProvinceID = Convert.ToInt64(filter.Where(a => a.Field == "ProvinceID").Select(a => a.Value).SingleOrDefault());


                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.Cities
                                join b in context.Provinces on a.ProvinceID equals b.ProvinceID

                                let isCityCode = !string.IsNullOrEmpty(filter.CityCode)
                                let isDescription = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false

                                where a.IsDeleted == false &&


                                 (isCityCode ? a.CityCode.Contains(filter.CityCode) : true) &&
                                 (isDescription ? a.Description.Contains(filter.Description) : true) &&
                                 (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                 (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)


                                select new CityModel
                                {
                                    CityID = a.CityID,
                                    CityCode = a.CityCode,
                                    Description = a.Description,
                                    Provinsi = new ProvinsiModel
                                    {
                                        ProvinceID = a.ProvinceID != null ? a.ProvinceID : 0,
                                        Description = a.ProvinceID != null ? b.Description : "-"
                                    },

                                    LastModifiedBy = a.CreateBy,
                                    LastModifiedDate = a.CreateDate
                                });

                    City.Page = page;
                    City.Size = size;
                    City.Total = data.Count();
                    City.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    City.Rows = data.OrderBy(orderBy).AsEnumerable().Select((a, i) => new CityModelRow
                    {
                        RowID = i + 1,
                        CityID = a.CityID,
                        CityCode = a.CityCode,
                        Description = a.Description,
                        // ProvinceID = a.ProvinceID,
                        Provinsi = a.Provinsi,
                        // ClearingCode = a.ClearingCode,
                        LastModifiedBy = a.LastModifiedBy,
                        LastModifiedDate = a.LastModifiedDate,
                    }).Skip(skip).Take(size).ToList();
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool AddCity(CityModel City, ref string message)
        {
            //throw new NotImplementedException();
            bool isSuccess = false;

            try
            {
                if (!context.Cities.Where(a => a.CityID == City.CityID && a.IsDeleted == false).Any())
                {
                    City city = new City();

                    city.CityCode = City.CityCode;
                    city.Description = City.Description;
                    city.IsDeleted = false;
                    city.CreateDate = DateTime.UtcNow;
                    city.CreateBy = currentUser.GetCurrentUser().DisplayName;
                    if (!string.IsNullOrEmpty(City.Provinsi.Description))
                    {
                        city.ProvinceID = City.Provinsi.ProvinceID;
                    }


                    context.Cities.Add(city);
                    context.SaveChanges();
                    isSuccess = true;


                }
                else
                {
                    message = string.Format("Citis  {0} is already exist.", City.Provinsi.ProvinceID);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
            //bool IsSucces = false;
            //try
            //{
            //context.Cities.Add(new Entity.City()
            //{
            //    CityCode = City.CityCode,
            //    Description = City.Description,
            //    ProvinceID = City.ProvinceID,
            //    IsDeleted =false,
            //    CreateDate = DateTime.UtcNow,
            //    CreateBy = currentUser.GetCurrentUser().DisplayName
            //});
            //    //context.SaveChanges();
            //    IsSucces = true;
            //}
            //catch (Exception ex)
            //{
            //    message = ex.Message;
            //}
            //return IsSucces;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        currentUser = null;
                        context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public bool UpdateCity(int ID, ref CityModel City, ref string message)
        {
            bool isSucces = false;
            try
            {
                Entity.City data = context.Cities.Where(a => a.CityID.Equals(ID)).SingleOrDefault();
                if (data != null)
                {

                    data.CityCode = City.CityCode;
                    data.Description = City.Description;
                    // data.ProvinceID = City.ProvinceID;
                    if (!string.IsNullOrEmpty(City.Provinsi.Description))
                    {
                        data.ProvinceID = City.Provinsi.ProvinceID;
                    }
                    // data.ClearingCode = City.ClearingCode;
                    data.CreateBy = currentUser.GetCurrentUser().DisplayName;

                    data.CreateDate = DateTime.UtcNow;
                    context.SaveChanges();
                    isSucces = true;
                }
                else
                {
                    message = string.Format("Data City {0} is does not exist.", ID);
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }


        public bool DeleteCity(int id, ref string message)
        {
            bool isSucces = false;
            try
            {
                Entity.City data = context.Cities.Where(a => a.CityID.Equals(id)).SingleOrDefault();
                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                    context.SaveChanges();
                    isSucces = true;
                }
                else
                {
                    message = string.Format("City data for FindingEmailTimeID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }


        public bool GetCity(string key, int limit, ref IList<ProvinsiModel> provinsi, ref string message)
        {
            bool isSuccess = false;
            try
            {
                provinsi = (from a in context.Provinces
                            where a.Description.Contains(key)
                            select new ProvinsiModel
                            {
                                ProvinceID = a.ProvinceID,
                                Description = a.Description,
                                ProvinceCode = a.ProvinceCode
                            }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


        public bool GetCity(string key, int limit, ref IList<CityModel> banks, ref string message)
        {
            string param = key.Trim().ToLower();
            bool isSuccess = false;
            try
            {
                banks = (from a in context.Cities
                         where a.CityCode.Trim().ToLower().Contains(param)
                         || a.Description.Trim().ToLower().Contains(param)
                         //&& a.IsDeleted.Equals(false)
                         //orderby new {a.CityCode, a.Description}

                         select new CityModel
                         {
                             CityID = a.CityID,
                             CityCode = a.CityCode,
                             Description = a.Description,
                             ProvinceID = a.ProvinceID
                         }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
    }
    #endregion
}