﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Models;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using System.Runtime.Serialization;
using System.Data.Entity;

namespace DBS.WebAPI.Models
{
    [DataContract]
    [ModelName("MISReportingTransactionType")]
    public class MISReportingTransactionTypeModel
    {
        public int ParamTransactionType  {get; set; }
    }

    [ModelName("MISReportingSegmentation")]
    public class  MISReportingSegmentationModel
    {
        public int ParamSegmentation  {get; set; }
    }

    [ModelName("MISReportingChannel")]
    public class  MISReportingChannelModel
    {
        public int ParamChannel  {get; set; }
    }

    [ModelName("MISReportingBranch")]
    public class  MISReportingBranchModel
    {
        public int ParamBranch  {get; set; }
    }

    public interface IMISTransactionType : IDisposable
    {
        bool GetMISReportingTransactionType(ref IList<MISReportingTransactionTypeModel> TransactionType, ref string message);
    }

    public interface IMISSegmentation : IDisposable
    {
        bool GetMISReportingSegmentation(ref IList<MISReportingSegmentationModel> Segmentation, ref string message);
    }

    public interface IMISChannel : IDisposable
    {
        bool GetMISReportingChannel(ref IList<MISReportingChannelModel> Channel, ref string message);
    }

    public interface IMISBranch : IDisposable
    {
        bool GetMISReportingBranch(ref IList<MISReportingBranchModel> Branch, ref string message);
    }

    
    public class MISTransactionTypeRepository : IMISTransactionType
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetMISReportingTransactionType(ref IList<MISReportingTransactionTypeModel> TransactionType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    TransactionType = (from a in context.Products
                                       select new MISReportingTransactionTypeModel
                                       {
                                           ParamTransactionType = (from b in context.Products select b.ProductID).Count()

                                       }).ToList();

                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

    public class MISSegmentationRepository : IMISSegmentation
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetMISReportingSegmentation(ref IList<MISReportingSegmentationModel> Segmentation, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    Segmentation = (from a in context.BizSegments
                                       select new MISReportingSegmentationModel
                                       {
                                           ParamSegmentation = (from b in context.BizSegments select b.BizSegmentID).Count()

                                       }).ToList();

                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

    public class MISChannelRepository : IMISChannel
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetMISReportingChannel(ref IList<MISReportingChannelModel> Channel, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    Channel = (from a in context.Channels
                                       select new MISReportingChannelModel
                                       {
                                           ParamChannel = (from b in context.Channels select b.ChannelID).Count()

                                       }).ToList();

                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

    public class MISBranchRepository : IMISBranch
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetMISReportingBranch(ref IList<MISReportingBranchModel> Branch, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    Branch = (from a in context.Locations
                                       select new MISReportingBranchModel
                                       {
                                           ParamBranch = (from b in context.Locations select b.LocationID).Count()

                                       }).ToList();

                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }


}