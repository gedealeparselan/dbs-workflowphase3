﻿var accessToken;
var DocumentModel = ko.observable({ FileName: '', DocumentPath: '' });
var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};
var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};

var ViewModel = function () {
    var self = this;
    self.SPUser = ko.observable(SPUser);
    self.ID = ko.observable("");
    self.Document = ko.observable("");
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");
    self.LoanContractNo = ko.observable("");

    // grid properties
    self.availableSizes = ko.observableArray([10, 25, 50, 75, 100]);
    self.Page = ko.observable(1);
    self.Size = ko.observableArray([10]);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.resultMessage = ko.observable("");
    self.resultHeader = ko.observable("");
    self.RowFailed = ko.observableArray([]);
    self.IsUploading = ko.observable(false);
    self.Column1Name = ko.observable("");
    self.Column2Name = ko.observable("");
    self.CloseProgres = function () {
        $("#modal-form-Upload").modal('hide');
    }
    // sorting
    self.SortColumn = ko.observable("Name");
    self.SortOrder = ko.observable("ASC");

    

    self.btnRelease = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            self.resultHeader('');
            self.resultMessage('');
            self.RowFailed([]);
            viewModel.IsUploading(true);
            $("#modal-form-Upload").modal('show');
            $("#transaction-progress").show();
            $("#transaction-result").hide();
            ReleaseLoanWorksheet(self.LoanContractNo());
        }
    }

    function ReleaseLoanWorksheet(loanNo) {

        var options = {
            url: api.server + api.url.csoTask + "/Release/LoanContract",
            params: {
                loanContractNo: loanNo
            },
            token: accessToken
        };


        Helper.Ajax.Get(options, function (result, textStatus, jqXHR) {
            viewModel.IsUploading(false);
            self.resultHeader("Release Worksheet Item");
            self.resultMessage(result);
            $("#transaction-progress").hide();
            $("#transaction-result").show();
            self.LoanContractNo('');
        }, OnError);
    }

    
    
    // Function to display notification. 
    // class name : gritter-success, gritter-warning, gritter-error, gritter-info
    function ShowNotification(title, text, className, isSticky) {
        $.gritter.add({
            title: title,
            text: text,
            class_name: className,
            sticky: isSticky,
        });
    }

    self.onPageSizeChange = function () {
        self.Page(1);

        GetData();
    };

    self.onPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }
        GetData();
    };

    self.nextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);
            GetData();
        }
    }

    self.previousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);
            GetData();
        }
    }

    self.firstPage = function () {
        self.Page(1);
        GetData();
    }

    self.lastPage = function () {
        self.Page(self.TotalPages());
        GetData();
    }

    self.filter = function () {
        self.Page(1);
        GetData();
    }

    self.sorting = function (column) {
        //alert(column)

        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }
        self.SortColumn(column);
        self.Page(1);
        GetData();
    }


    function Confirm(text) {
        bootbox.confirm(text, function (result) {
            return result;
        });
    }

    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    function OnAlways() {
        //$box.trigger('reloaded.ace.widget');
    }
    // Event handlers declaration end
};
// View Model
var viewModel = new ViewModel();
$(document).ready(function () {
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(OnSuccessToken, OnError);
    } else {
        accessToken = $.cookie(api.cookie.name);
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);
            viewModel.SPUser(ko.mapping.toJS(spUser));
        }
    }

    //$("#aspnetForm").validate({onsubmit: false});
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });
    
    ko.applyBindings(new ViewModel());
});