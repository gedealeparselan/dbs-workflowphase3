﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InterestMaintenanceWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.Others.InterestMaintenanceWP.InterestMaintenanceWPUserControl" %>
<div id="gridCustomerList" data-bind="visible: $root.IsGridEnable()">
    <h1 class="header smaller lighter dark">Interest Maintenance List</h1>
    
    <!-- widget box start -->
    <div id="widget-box" class="widget-box">
        <!-- widget header start -->
        <div class="widget-header">
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="IMDate">
                    Start Date</label>
                <div class="col-sm-2">
                    <div class="clearfix">
                        <div class="input-group">
                            <input id="IMDate" name="IMDate"  data-date-format="dd-M-yyyy" class="form-control date-picker col-xs-6" type="text" />
                            <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <div id="dataTable" class="widget-box"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/InterestMaintenance.js"></script>

<link rel="stylesheet" media="screen" href="/SiteAssets/Scripts/handsontable/dist/handsontable.full.css">
<script src="/SiteAssets/Scripts/handsontable/dist/handsontable.full.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/parser.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/ruleJS.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/lodash.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/underscore.string.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/moment.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/numeral.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/md5.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/jstat.js"></script>

