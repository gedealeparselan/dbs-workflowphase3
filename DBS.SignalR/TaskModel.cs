﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DBS.Entity;
using DBS.EntityCCU;

namespace DBS.SignalR
{
    public class TaskModel
    {
    }

    public class BroadcastMessage
    {
        public string From { get; set; }
        public string Message { get; set; }
        public DateTime? Time { get; set; }
    }

    public class ClientTaskData
    {
        public Guid ConnectionID { get; set; }
        public Guid? WorkflowInstanceID { get; set; }
        public long TransactionID { get; set; }

        public long ApproverID { get; set; }
        public long TaskID { get; set; }
        public string ActivityTitle { get; set; }
        public string LoginName { get; set; }
        public string DisplayName { get; set; }
    }

    public class SignalConnection
    {
        public string ConnectionID { get; set; }
        public string LoginName { get; set; }
        public string DisplayName { get; set; }
    }

    public class CheckTaskData
    {
        public string WorkflowInstanceID { get; set; }
        public long TaskID { get; set; }
    }

    public class ReturnTaskData
    {
        public bool IsOnProccess { get; set; }
        public string LoginName { get; set; }
        public string DisplayName { get; set; }
    }

    public class ClientTaskCCUData
    {
        public Guid ConnectionID { get; set; }
        public long? ActivationID { get; set; }
        public long? DocDetailID { get; set; }
        public long? WaiverDocID { get; set; }
        public long? CoverNoteDocID { get; set; }
        public bool IsWaiverListPage { get; set; }
        public bool IsCoverNoteListPage { get; set; }
        public string ActivityTitle { get; set; }
        public string LoginName { get; set; }
        public string DisplayName { get; set; }
    }

    #region Interface
    public interface ISignalRepository : IDisposable
    {
        bool AddTask(ClientTaskData clientTaskData, ref string message);

        bool CheckTask(ClientTaskData taskData, ref ClientTaskData checkTaskData, ref string message);

        bool ReleaseTaskByConnectionID(Guid connectionId, ref string message);

        bool ReleaseTask(ClientTaskData taskData, ref string message);

        bool CheckTaskCCU(ClientTaskCCUData taskData, ref ClientTaskCCUData checkTaskData, ref string message);

        bool AddTaskCCU(ClientTaskCCUData clientTaskData, ref string message);

        bool ReleaseTaskCCU(ClientTaskCCUData taskData, ref string message);
    }
    #endregion

    #region Repository
    public class SignalRepository : ISignalRepository
    {
        private DBSEntities context = new DBSEntities();
        private DBSCCUEntities contextCCU = new DBSCCUEntities();

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                        context.Dispose();
                    if (contextCCU != null)
                    {
                        contextCCU.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        void IDisposable.Dispose()
        {

            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public bool AddTask(ClientTaskData clientTaskData, ref string message)
        {
            bool isSuccess = false;

            try
            {
                context.TransactionTasks.Add(new TransactionTask()
                {
                    CreateDate = DateTime.UtcNow,
                    CreatedBy = clientTaskData.DisplayName,
                    TransactionID = clientTaskData.TransactionID,
                    ConnectionID = clientTaskData.ConnectionID,
                    TaskID = clientTaskData.TaskID,
                    ActivityTitle = clientTaskData.ActivityTitle,
                    ApproverID = clientTaskData.ApproverID,
                    IsActive = true,
                    WorkflowInstanceID = clientTaskData.WorkflowInstanceID //Rizki - 2017-02-24
                });

                context.SaveChanges();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool CheckTask(ClientTaskData taskData, ref ClientTaskData checkTaskData, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (taskData.ActivityTitle.ToLower() == "payment checker task") //Rizki - 2017-02-24
                {
                    checkTaskData = context.TransactionTasks
                        .Where(a => a.IsActive.Equals(true)
                            && a.WorkflowInstanceID == taskData.WorkflowInstanceID.Value
                            && a.ActivityTitle.ToLower() == "payment checker task"
                            )
                        .OrderByDescending(a => a.CreateDate)
                        .Select(a => new ClientTaskData()
                        {
                            DisplayName = a.CreatedBy
                        })
                        .FirstOrDefault();
                }
                else
                {
                    checkTaskData = context.TransactionTasks
                        .Where(a => a.IsActive.Equals(true)
                            && a.ApproverID.Equals(taskData.ApproverID)
                            && a.TaskID == taskData.TaskID
                        //&& a.TransactionID == taskData.WorkflowInstanceID
                        )
                        .OrderByDescending(a => a.CreateDate)
                        .Select(a => new ClientTaskData()
                        {
                            //WorkflowInstanceID = a.WorkflowInstanceID,
                            //ApproverID = a.ApproverID,
                            //ConnectionID = a.ConnectionID,
                            //TaskID = a.TaskID,
                            DisplayName = a.CreatedBy
                        })
                        .FirstOrDefault();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool ReleaseTaskByConnectionID(Guid connectionId, ref string message)
        {
            bool isSuccess = false;

            try
            {
                var data = context.TransactionTasks.Where(a => a.IsActive == true && a.ConnectionID.Equals(connectionId)).ToList();
                var dataCCU = contextCCU.TransactionTaskCCUs.Where(ttc => ttc.IsActive == true && ttc.ConnectionID == connectionId).ToList(); //aridya 20170803 untuk handle ccu

                //aridya 20170803 untuk handle ccu
                if (data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        item.IsActive = false;
                        item.UpdateBy = "Task Service";
                        item.UpdateDate = DateTime.UtcNow;
                    }
                }

                if (dataCCU.Count > 0)
                {
                    foreach (var itemCCU in dataCCU)
                    {
                        itemCCU.IsActive = false;
                        itemCCU.ModifiedBy = "Task Service";
                        itemCCU.ModifiedDate = DateTime.UtcNow;
                    }
                }
                //end aridya

                context.SaveChanges();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool ReleaseTask(ClientTaskData taskData, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (taskData.ActivityTitle != null && taskData.ActivityTitle.ToLower() == "payment checker task") //Rizki - 2017-02-24
                {
                    var data = context.TransactionTasks.Where(a => a.IsActive == true
                        && a.ConnectionID == taskData.ConnectionID
                        && a.WorkflowInstanceID == taskData.WorkflowInstanceID.Value
                        && a.ActivityTitle.ToLower() == "payment checker task").SingleOrDefault();

                    if (data != null)
                    {
                        data.IsActive = false;
                        data.UpdateBy = "Task Service";
                        data.UpdateDate = DateTime.UtcNow;

                        context.SaveChanges();
                    }
                }
                else
                {
                    var data = context.TransactionTasks.Where(a => a.IsActive == true
                    && a.ApproverID == taskData.ApproverID
                    && a.ConnectionID == taskData.ConnectionID
                    && a.TaskID == taskData.TaskID).SingleOrDefault();

                    if (data != null)
                    {
                        data.IsActive = false;
                        data.UpdateBy = "Task Service";
                        data.UpdateDate = DateTime.UtcNow;

                        context.SaveChanges();
                    }
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool CheckTaskCCU(ClientTaskCCUData taskData, ref ClientTaskCCUData checkTaskData, ref string message)
        {
            bool isSuccess = false;

            try
            {
                checkTaskData = contextCCU.TransactionTaskCCUs
                    .Where(a => a.IsActive.Equals(true)
                        && a.ActivationID == taskData.ActivationID
                        && a.DocDetailID == taskData.DocDetailID
                        && a.WaiverDocID == taskData.WaiverDocID
                        && a.CoverNoteDocID == taskData.CoverNoteDocID
                        && a.IsWaiverListPage == taskData.IsWaiverListPage
                        && a.IsCoverNoteListPage == taskData.IsCoverNoteListPage
                    )
                    .OrderByDescending(a => a.CreatedDate)
                    .Select(a => new ClientTaskCCUData()
                    {
                        DisplayName = a.CreatedBy
                    }).FirstOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddTaskCCU(ClientTaskCCUData clientTaskData, ref string message)
        {
            bool isSuccess = false;

            try
            {
                contextCCU.TransactionTaskCCUs.Add(new TransactionTaskCCU()
                {
                    CreatedDate = DateTime.UtcNow,
                    CreatedBy = clientTaskData.DisplayName,
                    ActivationID = clientTaskData.ActivationID,
                    ConnectionID = clientTaskData.ConnectionID,
                    DocDetailID = clientTaskData.DocDetailID,
                    IsWaiverListPage = clientTaskData.IsWaiverListPage,
                    IsCoverNoteListPage = clientTaskData.IsCoverNoteListPage,
                    ActivityTitle = clientTaskData.ActivityTitle,
                    WaiverDocID = clientTaskData.WaiverDocID,
                    IsActive = true,
                    CoverNoteDocID = clientTaskData.CoverNoteDocID
                });

                contextCCU.SaveChanges();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool ReleaseTaskCCU(ClientTaskCCUData taskData, ref string message)
        {
            bool isSuccess = false;

            try
            {
                var data = contextCCU.TransactionTaskCCUs.Where(a => a.IsActive == true
                && a.ActivationID == taskData.ActivationID
                && a.DocDetailID == taskData.DocDetailID
                && a.WaiverDocID == taskData.WaiverDocID
                && a.CoverNoteDocID == taskData.CoverNoteDocID
                && a.ConnectionID == taskData.ConnectionID
                ).SingleOrDefault();

                if (data != null)
                {
                    data.IsActive = false;
                    data.ModifiedBy = "Task Service";
                    data.ModifiedDate = DateTime.UtcNow;

                    contextCCU.SaveChanges();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
    }
    #endregion

}