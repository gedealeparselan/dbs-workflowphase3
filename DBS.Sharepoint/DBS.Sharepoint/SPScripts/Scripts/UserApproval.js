var accessToken;
var $box;
var $remove = false;
var viewModelMaster = null;
var viewModelDraft = null;
var existingBranchID = null; var existingBranchName = null;
var existingRankID = null; var existingRankName = null;
var existingSegmentID = null; var existingSegmentName = null;
var existingUserCategoryID = null; var existingUserCategoryCode = null;
var existingFunctionRoleID = null; var existingFunctionRoleName = null;
var roleDeleted = [];
var countResult;

var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};

var RankModel = function (id, code, desc) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Description = ko.observable(desc);
};

var SegmentModel = function (id, name, desc) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(desc);
};

var BranchModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
};

var UserCategoryModel = function (id, code) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
};

var FunctionRoleModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
};

var ViewModelMaster = function () {
    //Make the self as 'this' reference
    var self = this;

    self.updateCallback = function () { };

    self.SetUpdateCallback = function (callback) {
        self.updateCallback = callback;
    };

    self.BarLoad = ko.observable(false);
    self.IsRoleMaker = ko.observable(false);
    self.ExistingBranch = ko.observable(new BranchModel('', ''));
    //Declare observable which will be bind with UI 
    self.EmployeeUsername = ko.observable("");
    self.EmployeeName = ko.observable("");
    self.EmployeeEmail = ko.observable("");
    self.Rank = ko.observable(new RankModel('', '', ''));
    self.Segment = ko.observable(new SegmentModel('', '', ''));
    self.Branch = ko.observable(new BranchModel('', ''));
    self.UserCategory = ko.observable(new UserCategoryModel('', ''));
    self.FunctionRole = ko.observable(new FunctionRoleModel('', ''));
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");
    self.UserCategoryCode = ko.observable("");

    //Dropdown
    self.ddlBranch = ko.observableArray([]);
    self.ddlRank = ko.observableArray([]);
    self.ddlSegment = ko.observableArray([]);
    self.ddlUserCategory = ko.observableArray([]);
    self.ddlFunctionRole = ko.observableArray([]);

    //Select
    self.Role = ko.observableArray([]);

    //Rizki - 2016-01-14
    self.DraftID = ko.observable("");
    self.ActionType = ko.observable("");

    // filter
    self.FilterEmployeeUsername = ko.observable("");
    self.FilterEmployeeName = ko.observable("");
    self.FilterEmployeeEmail = ko.observable("");
    self.FilterRank = ko.observable("");
    self.FilterSegment = ko.observable("");
    self.FilterBranch = ko.observable("");
    self.FilterUserCategoryCode = ko.observable("");
    self.FilterFunctionRole = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");

    // New Data flag
    self.IsNewData = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.UserApprovals = ko.observableArray([]);

    // Draft
    self.UserDrafts = ko.observableArray([]);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    // set default sorting
    self.GridProperties().SortColumn("EmployeeName");
    self.GridProperties().SortOrder("ASC");

    // bind clear filters
    self.ClearFilters = function () {
        self.FilterEmployeeUsername("");
        self.FilterEmployeeName("");
        self.FilterEmployeeEmail("");
        self.FilterRank("");
        self.FilterSegment("");
        self.FilterBranch("");
        self.FilterUserCategoryCode("");
        self.FilterFunctionRole("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");

        GetData();
    };

    //get val ddl
    self.OnChangeMode = function () {
        //var ParameterValueID = parseInt($("#ParameterValue option:selected").text(), 10);
        var ParameterValueID = $("#user-category option:selected").text();       
        self.UserCategoryCode(ParameterValueID);
    };

    // bind get data function to view
    self.GetData = function () {
        GetData();
    };

    self.GetDropdown = function () {
        GetDropdown();
    };

    //The Object which stored data entered in the observables
    var UserApproval = {
        EmployeeID: ko.observable(),
        WorkflowInstanceID: ko.observable(),
        EmployeeUsername: self.EmployeeUsername,
        EmployeeName: self.EmployeeName,
        EmployeeEmail: self.EmployeeEmail,
        Rank: self.Rank,
        Segment: self.Segment,
        Branch: self.Branch,
        UserCategoryCode: self.UserCategoryCode,
        FunctionRole: self.FunctionRole,
        Roles: ko.observableArray([]),
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
    };

    self.save = function (event, ui) {
        //console.log("save user category" + self.UserCategory());
        //console.log(self.UserCategory().ID());
        //return false;
        // validation
        var form = $("#aspnetForm");
        form.validate();
        if (countResult === 0) {
            ShowNotification("Form Validation Warning", "Employee Name not found in Active Directory", 'gritter-warning', true);
            return false;
        }
        if (UserApproval.Roles().length <= 0) {
            ShowNotification("Form Validation Warning", "Employee Role must be selected.", 'gritter-warning', true);
            return false;
        }
        else {
            if (form.valid()) {
                //$("#modal-form").modal('hide');
                bootbox.confirm("Are you sure?", function (result) {

                    if (!result) {
                        $("#modal-form").modal('show');
                    } else {
                        //console.log(ko.toJSON(UserApproval));
                        //console.log(" ID " + UserApproval.UserCategory().ID() + " Kode " + UserApproval.UserCategory().Code());
                        //return false;
               
                        if (UserApproval.Rank().ID() != null) UserApproval.Rank().Code("null");
                        if (UserApproval.Segment().ID() != null) UserApproval.Segment().Name("null");
                        if (UserApproval.UserCategoryCode() == "Choose...") UserApproval.UserCategoryCode(null);
                        if (UserApproval.FunctionRole().ID() != null) UserApproval.FunctionRole().Name("null");
                        console.log("log user approval "+UserApproval.UserCategoryCode());
                        //Ajax call to insert the UserApprovals
                        $.ajax({
                            type: "POST",
                            url: api.server + api.url.userapproval,
                            data: ko.toJSON(UserApproval), //Convert the Observable Data into JSON                           
                            contentType: "application/json",
                            headers: {
                                "Authorization": "Bearer " + accessToken
                            },
                            success: function (data, textStatus, jqXHR) {
                                if (jqXHR.status = 200) {
                                    // send notification
                                    if (data.ID != 0) {
                                        //ShowNotification('Create Success', 'Request to Add Employee has been created', 'gritter-success', false);
                                        self.DraftID(data.ID);
                                        self.ActionType("Add");
                                        // add to list
                                        AddListItem(ui.currentTarget.children[0].id, data.ID);
                                    } else {
                                        ShowNotification('Warning', jqXHR.responseJSON.Message, 'gritter-warning', true);
                                    }

                                    // hide current popup window
                                    //$("#modal-form").modal('hide');

                                    // refresh data
                                    //GetData();
                                    //viewModelDraft.GetData();
                                } else {
                                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                // send notification
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                            }
                        });

                    }
                });
            }
        }
    };

    self.update = function (event, ui) {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        switch (ui.currentTarget.children[0].id) {
            case "UPDATE":
                if (UserApproval.Roles().length <= 0) {
                    ShowNotification("Form Validation Warning", "Employee Role must be selected.", 'gritter-warning', true);
                    return false;
                }
                break;
        }

        if (form.valid()) {

            //$("#modal-form").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {

                if (!result) {

                    $("#modal-form").modal('show');

                } else {
                    switch (ui.currentTarget.children[0].id) {
                        case "UPDATE":
                            if (UserApproval.UserCategoryCode() == "Choose...") UserApproval.UserCategoryCode(null);
                            if (UserApproval.Roles().length <= 0) {
                                ShowNotification("Form Validation Warning", "Employee Role must be selected.", 'gritter-warning', true);
                                return false;
                            }
                            break;
                        case "DELETE":
                            self.Rank(new RankModel('', '', ''));
                            self.Segment(new SegmentModel('', '', ''));
                            self.Branch(new BranchModel('', ''));
                            self.UserCategoryCode('');
                           // self.UserCategoryCode(new UserCategoryModel('', ''));
                            self.FunctionRole(new FunctionRoleModel('', ''));

                            self.Rank(new RankModel(existingRankID, existingRankName, ''));
                            self.Segment(new SegmentModel(existingSegmentID, existingSegmentName, ''));
                            self.Branch(new BranchModel(existingBranchID, existingBranchName));
                            self.UserCategoryCode(existingUserCategoryCode);
                            self.FunctionRole(new FunctionRoleModel(existingFunctionRoleID, existingFunctionRoleName));
                            UserApproval.Roles([]);
                            for (var i = 0, max = roleDeleted.length; i < max; i++) {
                                UserApproval.Roles.push(roleDeleted[i]);
                            }
                            //console.log(ui.currentTarget.children[0].id);                         	

                            break;
                    }
                    //console.log(ko.toJSON(UserApproval));
                    //Ajax call to insert the UserApprovals
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.userapproval + '/Update',
                        data: ko.toJSON(UserApproval), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                if (data.ID != 0) {
                                    self.DraftID(data.ID);
                                    // add to list
                                    var statusRequest;
                                    if (ui.currentTarget.children[0].id == "UPDATE") {
                                        self.ActionType("Update");
                                    }
                                    else if (ui.currentTarget.children[0].id == "DELETE") {
                                        self.ActionType("Delete");
                                    }
                                    else {
                                        self.ActionType("Update / Delete");
                                    }
                                    //ShowNotification('Create Request', 'Request to ' + statusRequest + ' Employee has been created', 'gritter-success', false);
                                    AddListItem(ui.currentTarget.children[0].id, data.ID);

                                } else {
                                    ShowNotification('Warning', jqXHR.responseJSON.Message, 'gritter-warning', true);
                                }

                                // hide current popup window
                                //$("#modal-form").modal('hide');

                                // refresh data
                                //GetData();
                                //viewModelDraft.GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });

                }
            });
        }
    };

    function AddListItem(ActionType, EmployeeID) {
        var body = {
            Title: EmployeeID.toString(),
            ActionType: ActionType,
            //EmployeeID: EmployeeID,
            __metadata: {
                type: config.sharepoint.metadata.listMaster
            }
        };

        var options = {
            url: config.sharepoint.url.api + "/Lists(guid'" + config.sharepoint.listIdMaster + "')/Items",
            data: JSON.stringify(body),
            digest: jQuery("#__REQUESTDIGEST").val()
        };

        Helper.Sharepoint.List.Add(options, OnSuccessAddListItem, OnErrorAddItem, OnAlways);
    }

    function OnSuccessAddListItem(data, textStatus, jqXHR) {
        $("#modal-form").modal('hide');
        GetData();
        viewModelDraft.GetData();

        ShowNotification('Create Request', 'Request to ' + self.ActionType() + ' Employee has been created', 'gritter-success', false);

        self.DraftID("");
        self.ActionType("");
    }

    function OnErrorAddItem(jqXHR, textStatus, errorThrown) {
        DeleteEmployeeDraft();
        //ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    function DeleteEmployeeDraft() {
        $.ajax({
            type: "DELETE",
            url: api.server + api.url.userapproval + "/" + self.DraftID(),
            data: ko.toJSON(UserApproval),
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                // send notification
                if (jqXHR.status = 200) {
                    ShowNotification('Failed on Creating Request', "Request to " + self.ActionType() + " Employee has been failed, please resubmit the request", 'gritter-warning', true);
                    self.DraftID("");
                    self.ActionType("");
                    //ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);						
                } else
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    //clear data
    self.clear = function () {
        /*$.each(UserApproval.Roles,function(index,item){
         $(':checkbox[value='+item.ID+']').prop('checked',false);
         });*/

        //UserApproval.Roles = ko.observableArray([]);
        if (UserApproval.Roles != undefined) {
            UserApproval.Roles([]);
        }
    }

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-18
    };

    self.ClearDDL = function () {
        self.Rank(new RankModel('', '', ''));
        self.Segment(new SegmentModel('', '', ''));
        self.Branch(new BranchModel('', ''));
        self.UserCategory(new UserCategoryModel('', ''));
        self.FunctionRole(new FunctionRoleModel('', ''));
    }

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        //console.log('user cat code' + data.UserCategory.ID+'='+ data.UserCategory.Code);

        //console.log(data);
        //Declare observable which will be bind with UI
        self.clear();
        self.ClearDDL();

        self.IsNewData(false);

        $(':checkbox').prop('checked', false);

        $("#modal-form").modal('show');

        self.Branch(new BranchModel('', ''));
        self.EmployeeUsername(data.EmployeeUsername);
        self.EmployeeName(data.EmployeeName);
        self.EmployeeEmail(data.EmployeeEmail);
        self.Rank(new RankModel(data.RankID, data.RankCode, data.RankDescription));
        self.Segment(new SegmentModel(data.SegmentID, data.SegmentName, data.SegmentDescription));
        self.Branch(new BranchModel(data.Branch.ID, data.Branch.Name));

        existingBranchID = data.Branch.ID;
        existingBranchName = data.Branch.Name;
        existingRankID = data.RankID;
        existingRankName = data.RankCode;
        existingSegmentID = data.SegmentID;
        existingSegmentName = data.SegmentName;
        //existingUserCategoryID = data.UserCategory.ID;
        existingUserCategoryCode = data.UserCategoryCode;
        existingFunctionRoleID = data.FunctionRole.ID;
        existingFunctionRoleName = data.FunctionRole.Name;

        self.UserCategory(new UserCategoryModel('', data.UserCategoryCode));
        self.FunctionRole(new FunctionRoleModel(data.FunctionRole.ID, data.FunctionRole.Name));
        //role
        $.each(data.Roles, function (index, item) {
            $(':checkbox[value=' + item.ID + ']').prop('checked', true);
            UserApproval.Roles.push(item);
            roleDeleted.push(item);

        });


        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);
    };

    //select role
    //self.SelectedRole = ko.observableArray([]);
    self.SelectRole = function (selectedItem) {
        var selectedIndex = self.Role().indexOf(selectedItem);

        //find is contact selected?
        var isSelected = ko.utils.arrayFirst(UserApproval.Roles(), function (selectedData) {
            return selectedData.ID == selectedItem.ID;
        });

        if (!isSelected) {
            UserApproval.Roles.push(selectedItem);
        } else {
            UserApproval.Roles.remove(function (selectedItem) { return selectedItem.ID == isSelected.ID });
        }
    }

    //insert new
    self.NewData = function () {
        // flag as new Data
        self.IsNewData(true);
        self.EmployeeUsername('');
        self.EmployeeName('');
        self.EmployeeEmail('');
        self.Rank(new RankModel('', '', '', ''));
        self.Segment(new SegmentModel('', '', ''));
        self.UserCategory(new UserCategoryModel('', '', ''));
        self.FunctionRole(new FunctionRoleModel('', '', ''));
        self.Branch(new BranchModel('', ''));
        //

        var inp = $('#EmployeeUsername').get(0);
        inp.removeAttribute('disabled');

        //UserApproval.Roles = ko.observableArray([]);
        self.clear();

        $(':checkbox').prop('checked', false);
    };

    // Function to get Biz Segment for Dropdownlist
    function GetDropdown() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.parameter + '?select=BizSegment,Branch,Rank,Role,UserCategory,FunctionRole',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.ddlSegment(data['BizSegment']);
                    self.ddlBranch(data['Branch']);
                    self.ddlRank(data['Rank']);
                    self.ddlUserCategory(data['UserCategory']);
                    self.ddlFunctionRole(data['FunctionRole']);
                    self.Role(data['Role']);
                    //console.log("User Category");
                    //console.log(self.ddlUserCategory(data['UserCategory']));
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    // get is data drafted
    self.SetColorStatus = function (username) {
        var isSelected = ko.utils.arrayFirst(self.UserDrafts(), function (selectedData) {
            return selectedData.EmployeeUsername == username;
        });

        if (isSelected) {
            return "danger";
        }
    }

    //Function to Read All Employee
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        //get draft
        var optionsDraft = {
            url: api.server + api.url.userapproval + '/Draft/All',
            token: accessToken
        };

        Helper.Ajax.Get(optionsDraft, OnSuccessGetDraft, OnError, OnAlways);

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.userapproval,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();
        //console.log(filters);
        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];
        if (self.FilterEmployeeUsername() != "") filters.push({ Field: 'EmployeeUsername', Value: self.FilterEmployeeUsername() });
        if (self.FilterEmployeeName() != "") filters.push({ Field: 'EmployeeName', Value: self.FilterEmployeeName() });
        if (self.FilterEmployeeEmail() != "") filters.push({ Field: 'EmployeeEmail', Value: self.FilterEmployeeEmail() });
        if (self.FilterRank() != "") filters.push({ Field: 'RankCode', Value: self.FilterRank() });
        if (self.FilterSegment() != "") filters.push({ Field: 'SegmentName', Value: self.FilterSegment() });
        if (self.FilterBranch() != "") filters.push({ Field: 'Branch', Value: self.FilterBranch() });
        if (self.FilterUserCategoryCode() != "") filters.push({ Field: 'UserCategoryCode', Value: self.FilterUserCategoryCode() });
        if (self.FilterFunctionRole() != "") filters.push({ Field: 'FunctionRole', Value: self.FilterFunctionRole() });
        //role
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

        return filters;
    };

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.UserApprovals(data.Rows);
            //console.log(data.Rows);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetDraft(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.UserDrafts(data);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
};

var ViewModelDraft = function (parent) {
    //Make the self as 'this' reference
    var self = this;

    self.Parent = parent;

    self.BarLoad = ko.observable(false);
    self.IsRoleMaker = ko.observable(false);

    //Declare observable which will be bind with UI 
    self.EmployeeUsername = ko.observable("");
    self.EmployeeName = ko.observable("");
    self.EmployeeEmail = ko.observable("");
    self.Rank = ko.observable(new RankModel('', '', ''));
    self.Segment = ko.observable(new SegmentModel('', '', ''));
    self.Branch = ko.observable(new BranchModel('', ''));
    self.UserCategory = ko.observable(new UserCategoryModel('', ''));
    self.FunctionRole = ko.observable(new FunctionRoleModel('', ''));
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");

    //Dropdown
    self.ddlBranch = ko.observableArray([]);
    self.ddlRank = ko.observableArray([]);
    self.ddlSegment = ko.observableArray([]);
    self.ddlUserCategory = ko.observableArray([]);
    self.ddlFunctionRole = ko.observableArray([]);

    //Select
    self.Role = ko.observableArray([]);

    // filter
    self.FilterEmployeeUsername = ko.observable("");
    self.FilterEmployeeName = ko.observable("");
    self.FilterEmployeeEmail = ko.observable("");
    self.FilterRank = ko.observable("");
    self.FilterSegment = ko.observable("");
    self.FilterBranch = ko.observable("");
    self.FilterUserCategory = ko.observable("");
    self.FilterFunctionRole = ko.observable("");
    self.FilterRole = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");

    // New Data flag
    self.IsNewData = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.UserApprovals = ko.observableArray([]);

    // Draft
    self.UserDrafts = ko.observableArray([]);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    // set default sorting
    self.GridProperties().SortColumn("EmployeeName");
    self.GridProperties().SortOrder("ASC");

    // bind clear filters
    self.ClearFilters = function () {
        self.FilterEmployeeUsername("");
        self.FilterEmployeeName("");
        self.FilterEmployeeEmail("");
        self.FilterRank("");
        self.FilterSegment("");
        self.FilterBranch("");
        self.FilterUserCategory("");
        self.FilterFunctionRole("");
        self.FilterRole("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");

        GetData();
    };


    // bind get data function to view
    self.GetData = function () {
        GetData();
    };

    self.GetDropdown = function () {
        GetDropdown();
    };

    //The Object which stored data entered in the observables
    var UserApproval = {
        EmployeeID: ko.observable(),
        WorkflowInstanceID: ko.observable(),
        EmployeeUsername: self.EmployeeUsername,
        EmployeeName: self.EmployeeName,
        EmployeeEmail: self.EmployeeEmail,
        Rank: self.Rank,
        Segment: self.Segment,
        Branch: self.Branch,
        UserCategory: self.UserCategory,
        FunctionRole: self.FunctionRole,
        Roles: ko.observableArray([]),
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
    };

    //clear data
    self.clear = function () {
        /*$.each(UserApproval.Roles,function(index,item){
         $(':checkbox[value='+item.ID+']').prop('checked',false);
         });*/

        //UserApproval.Roles = ko.observableArray([]);
        if (UserApproval.Roles != undefined) {
            UserApproval.Roles([]);
        }
    }

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
    };

    self.ClearDDL = function () {
        self.Rank(new RankModel('', '', ''));
        self.Segment(new SegmentModel('', '', ''));
        self.Branch(new BranchModel('', ''));
        self.UserCategory(new UserCategoryModel('', ''));
        self.FunctionRole(new FunctionRoleModel('', ''));
    }

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        //Declare observable which will be bind with UI
        //console.log('user cat code' + data.UserCategory.ID + '=' + data.UserCategory.Code);
        self.clear();
        self.ClearDDL();
        self.IsNewData(false);
        $(':checkbox').prop('checked', false);

        $("#modal-form-draft").modal('show');

        self.EmployeeUsername(data.EmployeeUsername);
        self.EmployeeName(data.EmployeeName);
        self.EmployeeEmail(data.EmployeeEmail);
        self.Rank(new RankModel(data.RankID, data.RankCode, data.RankDescription));
        self.Segment(new SegmentModel(data.SegmentID, data.SegmentName, data.SegmentDescription));
        self.Branch(new BranchModel(data.Branch.ID, data.Branch.Name));
        self.UserCategory(new UserCategoryModel('', data.UserCategoryCode));
        self.FunctionRole(new FunctionRoleModel(data.FunctionRole.ID, data.FunctionRole.Name));
        //role
        $.each(data.Roles, function (index, item) {
            $(':checkbox[value=' + item.ID + ']').prop('checked', true);
            UserApproval.Roles.push(item);
        });

        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);
    };

    //select role
    //self.SelectedRole = ko.observableArray([]);
    self.SelectRole = function (selectedItem) {
        var selectedIndex = self.Role().indexOf(selectedItem);

        //find is contact selected?
        var isSelected = ko.utils.arrayFirst(UserApproval.Roles(), function (selectedData) {
            return selectedData.ID == selectedItem.ID;
        });

        if (!isSelected) {
            UserApproval.Roles.push(selectedItem);
        } else {
            UserApproval.Roles.remove(function (selectedItem) { return selectedItem.ID == isSelected.ID });
        }
    }

    //insert new
    self.NewData = function () {
        // flag as new Data
        self.IsNewData(true);
        self.EmployeeUsername('');
        self.EmployeeName('');
        self.EmployeeEmail('');
        self.Rank(new RankModel('', '', '', ''));
        self.Segment(new SegmentModel('', '', ''));
        self.UserCategory(new UserCategoryModel('', '', ''));
        self.FunctionRole(new FunctionRoleModel('', '', ''));
        self.Branch(new BranchModel('', ''));
        //

        var inp = $('#EmployeeUsername').get(0);
        inp.removeAttribute('disabled');

        UserApproval.Roles = ko.observableArray([]);

        $(':checkbox').prop('checked', false);
    };

    // Function to get Biz Segment for Dropdownlist
    function GetDropdown() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.parameter + '?select=BizSegment,Branch,Rank,Role,UserCategory,FunctionRole',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.ddlSegment(data['BizSegment']);
                    self.ddlBranch(data['Branch']);
                    self.ddlRank(data['Rank']);
                    self.ddlUserCategory(data['UserCategory']);
                    self.ddlFunctionRole(data['FunctionRole']);
                    self.Role(data['Role']);
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    // get is data drafted
    self.SetColorStatus = function (username) {
        var isSelected = ko.utils.arrayFirst(self.UserDrafts(), function (selectedData) {
            return selectedData.EmployeeUsername == username;
        });

        if (isSelected) {
            return "danger";
        }
    }

    //Function to Read All Employee
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        //get draft
        var optionsDraft = {
            url: api.server + api.url.userapproval + '/Draft/All',
            token: accessToken
        };

        Helper.Ajax.Get(optionsDraft, OnSuccessGetDraft, OnError, OnAlways);

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.userapproval + '/Draft',
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();
        //console.log(filters);
        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];
        if (self.FilterEmployeeUsername() != "") filters.push({ Field: 'EmployeeUsername', Value: self.FilterEmployeeUsername() });
        if (self.FilterEmployeeName() != "") filters.push({ Field: 'EmployeeName', Value: self.FilterEmployeeName() });
        if (self.FilterEmployeeEmail() != "") filters.push({ Field: 'EmployeeEmail', Value: self.FilterEmployeeEmail() });
        if (self.FilterRank() != "") filters.push({ Field: 'RankCode', Value: self.FilterRank() });
        if (self.FilterSegment() != "") filters.push({ Field: 'SegmentName', Value: self.FilterSegment() });
        if (self.FilterBranch() != "") filters.push({ Field: 'Branch', Value: self.FilterBranch() });
        if (self.FilterUserCategory() != "") filter.push({ Field: 'UserCategory', Value: self.FilterUserCategory() });
        if (self.FilterFunctionRole() != "") filters.push({ Field: 'FunctionRole', Value: self.FilterFunctionRole() });
        //role
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

        return filters;
    };

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {

            self.UserApprovals(data.Rows);
            console.log(data.Rows);
            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetDraft(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.UserDrafts(data);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
};

$.holdReady(true);
var intervalRoleName = setInterval(function () {
    if (Object.keys(Const_RoleName).length != 0) {
        $.holdReady(false);
        clearInterval(intervalRoleName);
    }
}, 100);

//$(document).on("RoleNameReady", function () {
$(document).ready(function () {
    // widget loader
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    /// block enter key from user to prevent submitted data.
    $("input").keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

            return false;
        }
    });

    // scrollables
    $('.slim-scroll').each(function () {
        var $this = $(this);
        $this.slimscroll({
            height: $this.data('height') || 100,
            //width: $this.data('width') || 100,
            railVisible: true,
            alwaysVisible: true,
            color: '#D15B47'
        });
    });

    viewModelMaster = new ViewModelMaster();
    ko.applyBindings(viewModelMaster, document.getElementById('master'));

    viewModelDraft = new ViewModelDraft();
    ko.applyBindings(viewModelDraft, document.getElementById('draft'));

    $("#EmployeeUsername").autocomplete({
        source: function (request, response) {
            viewModelMaster.BarLoad(true);
            viewModelDraft.BarLoad(true);
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.helper + "/GetUserAD",
                data: {
                    username: request.term,
                    limit: 20
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, TokenOnError);
        },
        minLength: 2,
        select: function (event, ui) {
            // set customer data model
            if (ui.item.data != undefined || ui.item.data != null) {
                viewModelMaster.EmployeeUsername(ui.item.data.ID);
                viewModelMaster.EmployeeName(ui.item.data.Name);
                viewModelMaster.EmployeeEmail(ui.item.data.Email);

                /*user.PhysicalDeliveryOfficeName = "Makassar Branch";
                user.Department = "IBG 4";
                user.Title = "Sr. Associate*/

                if (ui.item.data.PhysicalDeliveryOfficeName != null) {
                    viewModelMaster.Branch().ID(ui.item.data.PhysicalDeliveryOfficeName);
                }

                if (ui.item.data.Department != null) {
                    viewModelMaster.Segment().ID(ui.item.data.Department);
                }

                if (ui.item.data.Title != null) {
                    viewModelMaster.Rank().ID(ui.item.data.Title);
                }

                $('#EmployeeUsername').val(ui.item.data.ID);
                $('#EmployeeName').val(ui.item.data.Name);
                $('#EmployeeEmail').val(ui.item.data.Email);

                var inp = $('#EmployeeUsername').get(0);
                inp.setAttribute('disabled', 'disabled');
            } else {

            }
        },
        response: function (event, ui) {
            if (ui.content.length === 0) {
                countResult = 0;
            } else {
                countResult = 1;
            }
        }
    });

    // Token Validation
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(TokenOnSuccess, TokenOnError);
    } else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // call spuser function
        GetCurrentUser(viewModelMaster);

        // call get data inside view model
        viewModelMaster.GetData();
        viewModelMaster.GetDropdown();
        viewModelDraft.GetData();
        viewModelDraft.GetDropdown();

        //viewModelDraft.IsRoleMaker(false);
        //viewModelMaster.IsRoleMaker(false);
    }

    $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    //$("#aspnetForm").validate({onsubmit: false});
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });
});

// Get SPUser from cookies
function GetCurrentUser(vm) {
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);

        vm.SPUser = spUser;

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    console.log("spUser Roles : " + ko.toJSON(spUser.Roles));
                    console.log("spUser Permited : " + data);
                    //compare user role to page permission to get checker validation
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    viewModelDraft.IsRoleMaker(true);
                                    viewModelMaster.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    viewModelDraft.IsRoleMaker(false);
                                    viewModelMaster.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
};

// Token validation On Success function
function TokenOnSuccess(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // call spuser function
    GetCurrentUser();

    // call get data inside view model
    viewModelMaster.GetParameters();
    viewModelMaster.GetData();
    viewModelDraft.GetData();
    viewModelDraft.GetParameters();
};

// Token validation On Error function
function TokenOnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
};

function OnSuccessAutoComplete(response, data, textStatus, jqXHR) {
    response($.map(data, function (item) {
        return {
            // default autocomplete object
            label: item.UserName + " - " + item.DisplayName,
            value: item.UserName,

            // custom object binding
            data: {
                ID: item.UserName,
                Name: item.DisplayName,
                Email: item.Email,
                PhysicalDeliveryOfficeName: item.PhysicalDeliveryOfficeName,
                Department: item.Department,
                Title: item.Title
            }
        }
    })
    );

    viewModelMaster.BarLoad(false);
    viewModelDraft.BarLoad(false);
};