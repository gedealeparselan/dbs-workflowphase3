﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class ExceptionHandlingParamController : ApiController
    {
        private string message = string.Empty;

        private readonly IExceptionHandlingParameterRepository repository = null;

        public ExceptionHandlingParamController()
        {
            this.repository = new ExceptionHandlingParameterRepository();
        }

        public ExceptionHandlingParamController(IExceptionHandlingParameterRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all currencies.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<ExceptionHandlingParameterModel>))]
        public HttpResponseMessage Get()
        {
            IList<ExceptionHandlingParameterModel> output = new List<ExceptionHandlingParameterModel>();

            if (!repository.GetExceptionHandlingParameter(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ExceptionHandlingParameterModel>>(HttpStatusCode.OK, output);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}