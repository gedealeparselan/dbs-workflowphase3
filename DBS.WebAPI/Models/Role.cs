﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property

    [ModelName("RolMenuDetail")]
    public class RoleMenuDetailModel
    {
        /// <summary>
        /// Menu ID.
        /// </summary>
        public string ID { get; set; }
    }

    [ModelName("Role")]
    public class RoleModel
    {
        /// <summary>
        /// Role ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Roles Name.
        /// </summary>
        // [MaxLength(50, ErrorMessage = "Role Name is must be in {1} characters.")]
        public string Name { get; set; }

        /// <summary>
        /// Role Description.
        /// </summary>
        // [MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Role Menu Detail.
        /// </summary>
        public IEnumerable<RoleMenuDetailModel> Menus { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class RoleRow : RoleModel
    {
        public int RowID { get; set; }

    }

    public class RoleGrid : Grid
    {
        public IList<RoleRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class RoleFilter : Filter { }
    #endregion

    #region Interface
    public interface IRoleRepository : IDisposable
    {
        bool GetRoleByID(int id, ref RoleModel role, ref string message);
        bool GetRole(ref IList<RoleModel> roles, int limit, int index, ref string message);
        bool GetRole(ref IList<RoleModel> roles, ref string message);
        bool GetRole(int page, int size, IList<RoleFilter> filters, string sortColumn, string sortOrder, ref RoleGrid roleGrid, ref string message);
        bool GetRole(RoleFilter filter, ref IList<RoleModel> roles, ref string message);
        bool GetRole(string key, int limit, ref IList<RoleModel> roles, ref string message);
        bool AddRole(RoleModel role, ref string message);
        bool UpdateRole(int id, RoleModel role, ref string message);
        bool DeleteRole(int id, ref string message);
        bool GetRoleByUsername(string Username, ref IList<RoleModel> roles, ref string message);
    }
    #endregion

    #region Repository
    public class RoleRepository : IRoleRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetRoleByID(int id, ref RoleModel role, ref string message)
        {
            bool isSuccess = false;

            try
            {
                role = (from a in context.Roles
                        where a.IsDeleted.Equals(false) && a.RoleID.Equals(id)
                        select new RoleModel
                        {
                            ID = a.RoleID,
                            Name = a.RoleName,
                            Description = a.RoleDescription,
                            LastModifiedDate = a.CreateDate,
                            LastModifiedBy = a.CreateBy
                        }).SingleOrDefault();

                int roleID = role.ID;

                var details = from a in context.RoleMenuMappings
                              where a.RoleID == roleID
                              select a;

                var items = new List<RoleMenuDetailModel>();
                foreach (var item in details)
                {
                    items.Add(new RoleMenuDetailModel()
                    {
                        ID = item.MenuID.ToString()
                    });
                }
                role.Menus = items;

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRole(ref IList<RoleModel> roles, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    roles = (from a in context.Roles
                             where a.IsDeleted.Equals(false)
                             orderby new { a.RoleName, a.RoleDescription }
                             select new RoleModel
                             {
                                 ID = a.RoleID,
                                 Name = a.RoleName,
                                 Description = a.RoleDescription,
                                 LastModifiedBy = a.CreateBy,
                                 LastModifiedDate = a.CreateDate
                             }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRole(ref IList<RoleModel> roles, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    roles = (from a in context.Roles
                             where a.IsDeleted.Equals(false)
                             orderby new { a.RoleName, a.RoleDescription }
                             select new RoleModel
                             {
                                 ID = a.RoleID,
                                 Name = a.RoleName,
                                 Description = a.RoleDescription,
                                 LastModifiedBy = a.CreateBy,
                                 LastModifiedDate = a.CreateDate
                             }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRole(int page, int size, IList<RoleFilter> filters, string sortColumn, string sortOrder, ref RoleGrid roleGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                RoleModel filter = new RoleModel();
                if (filters != null)
                {
                    filter.Name = (string)filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.Roles
                                let isRoleName = !string.IsNullOrEmpty(filter.Name)
                                let isRoleDescription = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isRoleName ? a.RoleName.Contains(filter.Name) : true) &&
                                    (isRoleDescription ? a.RoleDescription.Contains(filter.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new RoleModel
                                {
                                    ID = a.RoleID,
                                    Name = a.RoleName,
                                    Description = a.RoleDescription,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    roleGrid.Page = page;
                    roleGrid.Size = size;
                    roleGrid.Total = data.Count();
                    roleGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    roleGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new RoleRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Name = a.Name,
                            Description = a.Description,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRole(RoleFilter filter, ref IList<RoleModel> roles, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetRole(string key, int limit, ref IList<RoleModel> roles, ref string message)
        {
            bool isSuccess = false;

            try
            {
                roles = (from a in context.Roles
                         where a.IsDeleted.Equals(false) &&
                             a.RoleName.Contains(key) || a.RoleDescription.Contains(key)
                         orderby new { a.RoleName, a.RoleDescription }
                         select new RoleModel
                         {
                             Name = a.RoleName,
                             Description = a.RoleDescription
                         }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddRole(RoleModel role, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.Roles.Where(a => a.RoleName.Equals(role.Name) && a.IsDeleted.Equals(false)).Any())
                {
                    Entity.Role item = new Entity.Role()
                    {
                        RoleName = role.Name,
                        RoleDescription = role.Description,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    };
                    context.Roles.Add(item);

                    foreach (var sitem in role.Menus)
                    {
                        item.RoleMenuMappings.Add(new RoleMenuMapping()
                        {
                            MenuID = Guid.Parse(sitem.ID),
                            IsHasAccess = true,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().DisplayName
                        });
                    }

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Role data for Role Name {0} is already exist.", role.Name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateRole(int id, RoleModel role, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Role data = context.Roles.Where(a => a.RoleID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.Roles.Where(a => a.RoleID != role.ID && a.RoleName.Equals(role.Name) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Role Name {0} is exist.", role.Name);
                    }
                    else
                    {
                        data.RoleName = role.Name;
                        data.RoleDescription = role.Description;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Role data for Role ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteRole(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Role data = context.Roles.Where(a => a.RoleID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Role data for Role ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public bool GetRoleByUsername(string Username, ref IList<RoleModel> roles, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    roles = (from a in context.Employees
                             join b in context.EmployeeRoleMappings on a.EmployeeID equals b.EmployeeID
                             join c in context.Roles on b.RoleID equals c.RoleID
                             where a.IsDeleted.Equals(false) && a.EmployeeUsername == Username
                             select new RoleModel
                             {
                                 ID = c.RoleID,
                                 Name = c.RoleName,
                                 Description = c.RoleDescription,
                                 LastModifiedBy = c.CreateBy,
                                 LastModifiedDate = c.CreateDate
                             }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
    }
    #endregion
}